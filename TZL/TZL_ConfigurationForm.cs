using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.TZL.Properties;

namespace BEMN.TZL
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private bool _mess;
        private TZL _device;
        private bool _validatingOk = true;
        private bool _connectingErrors;
        double _vers;
        bool _butReadClick;
        DataGridView[] _logicSignalsDataGrids;
        CheckedListBox[] _outputLogicCheckLists;

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(TZL device)
        {
            this._device = device;
            this.InitializeComponent();
            this._groupSelector = new RadioButtonSelector(this._mainRadioBtnGroup, this._reserveRadioBtnGrop, this._groupChangeButton);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            this.Init();
        }

        private void Init()
        {
            try
            {
                Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
                this._vers = Common.VersionConverter(this._device.DeviceVersion);
            }
            catch (Exception)
            {
                string[] sVersion = this._device.DeviceVersion.Split('.');
                string version = sVersion[0] + '.' + sVersion.Take(2);
                double.TryParse(version, out this._vers);
            }

            if (this._vers == 1.1)
            {
                this._exDefOSCcol.Visible = true;
                this._tokDefenseOSCCol.Visible = true;
                this._tokDefense3OSCCol.Visible = true;
                this._tokDefense4OSCCol.Visible = true;
                this._voltageDefensesOSCCol1.Visible = true;
                this._voltageDefensesOSCCol2.Visible = true;
                this._voltageDefensesOSCCol3.Visible = true;
                this._frequenceDefensesOSC.Visible = true;
                this._Sost_ON_OFF_Label.Visible = true;
                this._Sost_ON_OFF.Visible = true;

            }
            else if (this._vers >= 1.11)
            {
                this._exDefOSCcol.Visible = true;
                this._tokDefenseOSCv11Col.Visible = true;
                this._tokDefense3OSCv11Col.Visible = true;
                this._tokDefense4OSCv11Col.Visible = true;
                this._frequenceDefensesOSCv11.Visible = true;
                this._voltageDefensesOSCv11Col1.Visible = true;

                this._voltageDefensesOSC�11Col2.Visible = true;
                this._voltageDefensesOSCv11Col3.Visible = true;
                this._blockDDL.Visible = true;
                this._U1CB.SelectedIndex = 0;
                this._U2CB.SelectedIndex = 0;
                this._Sost_ON_OFF_Label.Visible = true;
                this._Sost_ON_OFF.Visible = true;
                this._TT_Box.Tag = "3000";
                if (this._vers >= 1.14)
                {
                    this._TT_Box.Tag = "5000";

                    this._TTNP_Box.Tag = _vers >= 3.07 ? "2500" : "1000";

                    this.lzsh_conf.Visible = false;
                    this.lzsh_confV114.Visible = true;
                    this._exDefResetcol.Visible = true;
                    this._voltageDefensesResetCol1.Visible = true;
                    this._voltageDefensesResetCol2.Visible = true;
                    this._voltageDefensesResetcol3.Visible = true;
                    this._frequenceDefensesReset.Visible = true;
                    if (this._vers >= 1.15)
                    {
                        this.groupBox23.Visible = false;
                        this.oscGroupBox.Visible = true;
                    }
                }
                if (this._vers <= 3.00)
                {
                    this._exBlockSdtuCombo.Visible = false;
                    this.label29.Visible = false;
                }
            }
            else
            {
                this._voltageDefensesOSCv11Col1.Visible = false;
                this._voltageDefensesOSC�11Col2.Visible = false;
                this._voltageDefensesOSCv11Col3.Visible = false;
                this._frequenceDefensesOSCv11.Visible = false;
                this._tokDefenseOSCv11Col.Visible = false;
                this._tokDefense3OSCv11Col.Visible = false;
                this._tokDefense4OSCv11Col.Visible = false;
                this._exDefOSCcol.Visible = false;
                this._tokDefenseOSCCol.Visible = false;
                this._tokDefense3OSCCol.Visible = false;
                this._tokDefense4OSCCol.Visible = false;
                this._voltageDefensesOSCCol1.Visible = false;
                this._voltageDefensesOSCCol2.Visible = false;
                this._voltageDefensesOSCCol3.Visible = false;
                this._frequenceDefensesOSC.Visible = false;
                this._Sost_ON_OFF_Label.Visible = false;
                this._Sost_ON_OFF.Visible = false;
                this._blockDDL.Visible = false;
                this._exBlockSdtuCombo.Visible = true;
                this.label29.Visible = true;
                if (this._vers == 0)
                {
                    this._Sost_ON_OFF_Label.Visible = true;
                    this._Sost_ON_OFF.Visible = true;
                }
            }
            this._exchangeProgressBar.Value = 0;
            this._device.InputSignalsLoadOK += HandlerHelper.CreateHandler(this, this.InputSignalsLoadOK);
            this._device.InputSignalsLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.KonfCountLoadOK += HandlerHelper.CreateHandler(this, this.ReadKonfCount);
            this._device.KonfCountLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.OutputSignalsLoadOK += HandlerHelper.CreateHandler(this, this.ReadOutputSignals);
            this._device.OutputSignalsLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.ExternalDefensesLoadOK += HandlerHelper.CreateHandler(this, this.ReadExternalDefenses);
            this._device.ExternalDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.AutomaticsPageLoadOK += HandlerHelper.CreateHandler(this, this.OnAutomaticsPageLoadOk);
            this._device.AutomaticsPageLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.TokDefensesLoadOK += HandlerHelper.CreateHandler(this, this.OnTokDefenseLoadOk);
            this._device.TokDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.VoltageDefensesLoadOk += HandlerHelper.CreateHandler(this, this.OnVoltageDefensesLoadOk);
            this._device.VoltageDefensesLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.FrequenceDefensesLoadOk += HandlerHelper.CreateHandler(this, this.FrequenceDefensesLoadOk);
            this._device.FrequenceDefensesLoadFail += HandlerHelper.CreateHandler(this, this.FrequenceDefensesLoadFail);
            this._device.LogicLoadFail += HandlerHelper.CreateHandler(this, this.OnLoadFail);
            this._device.LogicSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.LogicSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this.SubscriptOnSaveHandlers();

            this.PrepareInputSignals();
            this.PrepareOutputSignals();
            this.PrepareExternalDefenses();
            this.PrepareAutomaticsSignals();
            this.PrepareTokDefenses();
            this.PrepareVoltageDefenses();
            this.PrepareFrequenceDefenses();
        }
        //���� 0, �������� �������� � ���������
        void _groupSelector_NeedCopyGroup()
        {
            if (this._groupSelector.SelectedGroup == 0)
            {
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
            else
            {
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
        }
        void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                if (combo.Items[combo.Items.Count - 1].ToString() == "XXXXX")
                {
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(TZL); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._butReadClick = true;
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this._device.Deserialize(this._openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    this._processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ��741 ��� ���������";
                    return;
                }

                // ���������� ��� � ���� ������� �����
                if (this._mainRadioBtnGroup.Checked)
                {
                    this.ShowTokDefenses(this._device.TokDefensesMain);
                    this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
                    this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
                }
                else
                {
                    this.ShowTokDefenses(this._device.TokDefensesReserve);
                    this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);
                    this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
                }
                this.ReadInputSignals();
                this.ReadExternalDefenses();
                this.ReadOutputSignals();
                this.ReadAutomaticsPage();
                this.ReadKonfCount();

                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
            }
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._saveConfigurationDlg.FileName = string.Format("��741_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._exchangeProgressBar.Value = 0;
                    this._device.Serialize(this._saveConfigurationDlg.FileName);
                    this._processLabel.Text = "���� " + this._saveConfigurationDlg.FileName + " ��������";
                }
            }
        }
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._connectingErrors = false;
            this._exchangeProgressBar.Value = 0;
            this._validatingOk = true;

            if (this.oscPercent.Text == "0")
            {
                this.oscPercent.Text = "1";
            }

            this.ValidateAll();
            if (this._validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ ��741 �" + this._device.DeviceNumber + " ?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "���� ������";
                    
                    this._device.LoadTypeInterfaces();
                    Thread.Sleep(1000);
                    this._device.SaveInputSignals();
                    Thread.Sleep(1000);
                    this._device.SaveTypeInterface();
                    Thread.Sleep(1000);
                    this._device.SaveOutputSignals();
                    
                    this._device.SaveExternalDefenses();
                    this._device.SaveTokDefenses();
                    this._device.SaveVoltageDefenses();

                    this._device.SaveAutomaticsPage();
                    this._device.SaveFrequenceDefenses();

                    this._device.ConfirmConstraint();
                    
                }
            }
        }
        private void ValidateAll()
        {
            this._validatingOk = true;
            //�������� MaskedTextBox
            this.ValidateInputSignals();
            this.ValidateTokDefenses();

            this.ValidateAutomatics();
            //�������� DataGrid
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteOutputSignals()
                                          && this.WriteExternalDefenses()
                                          && this.WriteInputSignals()
                                          && this.WriteAutomaticsPage();

                this._validatingOk &= this._mainRadioBtnGroup.Checked ? this.WriteTokDefenses(this._device.TokDefensesMain) : this.WriteTokDefenses(this._device.TokDefensesReserve);
                this._validatingOk &= this._mainRadioBtnGroup.Checked ? this.WriteVoltageDefenses(this._device.VoltageDefensesMain) : this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this._validatingOk &= this._mainRadioBtnGroup.Checked ? this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain) : this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }

        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������...";
            this._connectingErrors = false;
            this._device.LoadInputSignals();
            this._device.LoadTypeInterfaces();
            this._device.LoadOutputSignals();
            this._device.LoadExternalDefenses();
            this._device.LoadAutomaticsPage();
            this._device.LoadTokDefenses();
            this._device.LoadVoltageDefenses();
            this._device.LoadFrequenceDefenses();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��741. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��741. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveInputSygnalsOk()
        {
            this._device.TT = this._device.TTtoDevice;
            this._device.TTNP = this._device.TTNPtoDevice;
            this._exchangeProgressBar.PerformStep();
        }

        private void OnSaveTypeInterfacesOk()
        {
            this._device.TypeInterfaces = this._device.TypeInterfacesToDevice;
        }

        void OnLoadComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._processLabel.Text = "������ ������� ���������";
            }
        }

        void OnSaveComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "������ ������� ���������";
            }
        }

        public double ConvertVersion(string oldVersion)
        {
            return double.Parse(oldVersion.Replace('.', ','), System.Globalization.NumberStyles.AllowDecimalPoint);
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartRead();
        }
        
        private void SubscriptOnSaveHandlers()
        {
            this._device.LogicSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.LogicSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.OutputSignalsSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.OutputSignalsSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.InputSignalsSaveOK += HandlerHelper.CreateHandler(this, this.OnSaveInputSygnalsOk);
            this._device.InputSignalsSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.TypeInterfacesSaveOk += HandlerHelper.CreateHandler(this, this.OnSaveTypeInterfacesOk);
            this._device.TypeInterfacesSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.KonfCountSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.KonfCountSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.ExternalDefensesSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.ExternalDefensesSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.AutomaticsPageSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.AutomaticsPageSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.TokDefensesSaveOK += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.TokDefensesSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.VoltageDefensesSaveOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._device.VoltageDefensesSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveFail);
            this._device.FrequenceDefensesSaveOk += HandlerHelper.CreateHandler(this, this.OnSaveComplete);
            this._device.FrequenceDefensesSaveFail += HandlerHelper.CreateHandler(this, this.OnSaveComplete);
        }

        private void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    this.ShowToolTip(box);

                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.Name == "oscPercent" && double.Parse(box.Text) == 0)
                    {
                        this.ShowToolTip(box);
                    }
                }
            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            TabPage page = box.Parent.Parent as TabPage;
            if (page != null)
            {
                if (this._tabControl.TabPages.Contains(page))
                {
                    this._tabControl.SelectedTab = page;
                }
                else
                {
                    ((TabControl) page.Parent).SelectedTab = page;
                }
            }

            if (box.Name == "oscPercent")
            {
                this._toolTip.Show("������� ����� ����� � ��������� [1-" + box.Tag + "]", box, 2000);
            }
            else
            {
                this._toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
            }
           
            box.Focus();
            box.SelectAll();
            this._validatingOk = false;
        }
        
        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage outerPage)
        {
            this._tabControl.SelectedTab = outerPage;
            Point cPoint = cell.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Location;
            Point gPoint = cell.DataGridView.Location;
            this._toolTip.Show(msg, outerPage, cPoint.X + gPoint.X, cPoint.Y + gPoint.Y + cell.OwningRow.Height, 2000);
            cell.Selected = true;
            this._validatingOk = false;
        }

        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage outerPage, TabPage innerPage)
        {
            this._tabControl.SelectedTab = outerPage;
            this.tabControl1.SelectedTab = innerPage;
            Point cPoint = cell.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Location;
            Point gPoint = cell.DataGridView.Location;
            this._toolTip.Show(msg, innerPage, cPoint.X + gPoint.X, cPoint.Y + gPoint.Y + cell.OwningRow.Height, 2000);
            cell.Selected = true;
            this._validatingOk = false;
        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            try
            {
                ComboBox combo = (ComboBox)sender;
                combo.DropDown -= new EventHandler(this.Combo_DropDown);
                if (combo.SelectedIndex == combo.Items.Count - 1)
                {
                    combo.SelectedIndex = 0;
                }
                if (combo.Items.Contains("XXXXX"))
                {
                    combo.Items.Remove("XXXXX");
                }
            }
            catch { }
        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                if (combos[i].Items.Count != 0)
                {
                    combos[i].SelectedIndex = 0;
                }
            }
        }

        void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            foreach (MaskedTextBox box in boxes)
            {
                box.TypeValidationCompleted += this.TypeValidation;
                box.ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            foreach (MaskedTextBox box in boxes)
            {
                box.ValidateText();
            }
        }

        #region �������� �������
        private void PrepareOutputSignals()
        {
            this._releTypeCol.Items.Clear();
            this._releTypeCol.Items.AddRange(Strings.RelayIndType.ToArray());
            this._outIndTypeCol.Items.Clear();
            this._outIndTypeCol.Items.AddRange(Strings.RelayIndType.ToArray());
            this._releSignalCol.Items.Clear();
            this._outIndSignalCol.Items.Clear();
            List<string> list = new List<string>(Strings.All);
            if (this._vers >= 3.03)
            {
                int index = list.IndexOf("������");
                list[index - 1] = "���� �1 ���.";
                list[index] = "���� �1";
                index = list.IndexOf("������");
                list[index - 1] = "���� �2 ���.";
                list[index] = "���� �2";
            }
            this._releSignalCol.Items.AddRange(list.ToArray());
            this._outIndSignalCol.Items.AddRange(list.ToArray());

            this._outputLogicCheckLists = new[]
            {
                this._outputLogicCheckList1, this._outputLogicCheckList2, this._outputLogicCheckList3, this._outputLogicCheckList4,
                this._outputLogicCheckList5, this._outputLogicCheckList6, this._outputLogicCheckList7, this._outputLogicCheckList8
            };
            for (int i = 0; i < this.VLSTabControl.TabPages.Count; i++)
            {
                this._device.SetOutputLogicSignals(i, new BitArray(120));
                this._outputLogicCheckLists[i].ClearSelected();
                this._outputLogicCheckLists[i].Items.Clear();
                if (this._outputLogicCheckLists[i].Items.Count == 0)
                {
                    List<string> outputSignals = new List<string>(Strings.OutputSignals);
                    //if (this._vers >= 3.03)
                    //{
                    //    int index = outputSignals.IndexOf("������");
                    //    outputSignals[index] = "���� �1";
                    //    index = outputSignals.IndexOf("������");
                    //    outputSignals[index] = "���� �2";
                    //}
                    this._outputLogicCheckLists[i].Items.AddRange(outputSignals.ToArray());
                }
            }

            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < TZL.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(i + 1, "�����������", "���", 0 );
            }
            this._outputIndicatorsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputIndicatorsGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            this._outputReleGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputReleGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < TZL.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(i + 1, "�����������", "���",false,false,false);
            }
        }

        private void ReadOutputSignals()
        {
            this._exchangeProgressBar.PerformStep();
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < TZL.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, this._device.OutputRele[i].Type, this._device.OutputRele[i].RelaySignalString, this._device.OutputRele[i].ImpulseUlong });
            }
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < TZL.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            this._device.OutputIndicator[i].Type,
                                                            this._device.OutputIndicator[i].SignalString,
                                                            this._device.OutputIndicator[i].Indication,
                                                            this._device.OutputIndicator[i].Alarm,
                                                            this._device.OutputIndicator[i].System});
            }

            this.ShowOutputLogicSignals();
        }

        private void ShowOutputLogicSignals()
        {
            for (int j = 0; j < this.VLSTabControl.TabPages.Count; j++)
            {
                BitArray bits = this._device.GetOutputLogicSignals(j);
                for (int i = 0; i < bits.Count; i++)
                {
                    this._outputLogicCheckLists[j].SetItemChecked(i, bits[i]);
                }
            }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;
            if (this._device.OutputRele.Count == TZL.COutputRele.COUNT && this._outputReleGrid.Rows.Count <= TZL.COutputRele.COUNT)
            {
                for (int i = 0; i < this._outputReleGrid.Rows.Count; i++)
                {
                    this._device.OutputRele[i].Type = this._outputReleGrid["_releTypeCol", i].Value.ToString();
                    this._device.OutputRele[i].RelaySignalString = this._outputReleGrid["_releSignalCol", i].Value.ToString();
                    try
                    {
                        ulong value = ulong.Parse(this._outputReleGrid["_releImpulseCol", i].Value.ToString());
                        if (value > TZL.TIMELIMIT)
                        {
                            this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                            ret = false;
                        }
                        else
                        {
                            this._device.OutputRele[i].ImpulseUlong = value;
                        }
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                        ret = false;

                    }
                }
            }

            if (this._device.OutputIndicator.Count == TZL.COutputIndicator.COUNT && this._outputIndicatorsGrid.Rows.Count <= TZL.COutputIndicator.COUNT)
            {
                for (int i = 0; i < this._outputIndicatorsGrid.Rows.Count; i++)
                {
                    this._device.OutputIndicator[i].Type = this._outputIndicatorsGrid["_outIndTypeCol", i].Value.ToString();
                    this._device.OutputIndicator[i].SignalString = this._outputIndicatorsGrid["_outIndSignalCol", i].Value.ToString();
                    this._device.OutputIndicator[i].Indication = (bool)this._outputIndicatorsGrid["_outIndResetCol", i].Value;
                    this._device.OutputIndicator[i].Alarm = (bool)this._outputIndicatorsGrid["_outIndAlarmCol", i].Value;
                    this._device.OutputIndicator[i].System = (bool)this._outputIndicatorsGrid["_outIndSystemCol", i].Value;
                }
            }
            this.SaveOutputLogicSignals();
            return ret;
        }
        
        private void SaveOutputLogicSignals()
        {
            for (int j = 0; j < this._outputLogicCheckLists.Length; j++)
            {
                BitArray bits = new BitArray(this._outputLogicCheckLists[j].Items.Count);
                for (int i = 0; i < this._outputLogicCheckLists[j].Items.Count; i++)
                {
                    bits[i] = this._outputLogicCheckLists[j].GetItemChecked(i);
                }
                this._device.SetOutputLogicSignals(j, bits);
            }
        }

        #endregion

        #region ������� ������

        private void PrepareExternalDefenses()
        {
            try
            {
                this._vers = Common.VersionConverter(this._device.DeviceVersion);
            }
            catch (Exception)
            {
                this._vers = 1.11;
            }
            this._externalDefenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._exDefBlockingCol.Items.Clear();
            this._exDefBlockingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            this._exDefModeCol.Items.Clear();
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.12)
                {
                    this._exDefModeCol.Items.AddRange(Strings.ModesNew.ToArray());
                }
                else
                {
                    this._exDefModeCol.Items.AddRange(Strings.ModesExternalNew.ToArray());
                }
            }
            else
            {
                this._exDefModeCol.Items.AddRange(Strings.ModesExternal.ToArray());
            }

            this._exDefReturnNumberCol.Items.Clear();
            this._exDefReturnNumberCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            this._exDefWorkingCol.Items.Clear();
            this._exDefWorkingCol.Items.AddRange(Strings.ExternalDefense.ToArray());

            this.ResetExtDef();
            this._externalDefenseGrid.Height = this._externalDefenseGrid.ColumnHeadersHeight + this._externalDefenseGrid.Rows.Count * this._externalDefenseGrid.RowTemplate.Height + 7;
            this._externalDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._externalDefenseGrid_CellStateChanged);
            this._externalDefenseGrid.CellBeginEdit += this.GridOnCellBeginEdit;
        }

        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[this._externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(this._externalDefenseGrid, row, "��������", 1, columns);
        }

        void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (this._exDefModeCol == e.Cell.OwningColumn || this._exDefReturnCol == e.Cell.OwningColumn)
            {
                if (!this.OnExternalDefenseGridModeChanged(e.Cell.RowIndex))
                {
                    GridManager.ChangeCellDisabling(this._externalDefenseGrid, e.Cell.RowIndex, "��������", 1, new int[] { 6, 7, 8 });
                }
            }
        }

        private void ReadExternalDefenses()
        {
            try
            {
                this._vers = Common.VersionConverter(this._device.DeviceVersion);
            }
            catch (Exception)
            {
                this._vers = 1.11;
            }

            this._exchangeProgressBar.PerformStep();
            this._externalDefenseGrid.Rows.Clear();

            for (int i = 0; i < TZL.CExternalDefenses.COUNT; i++)
            {
                if (this._vers == 1.1)
                {
                    this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           this._device.ExternalDefenses[i].Osc,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR,
                                                            false});
                    if (!this.OnExternalDefenseGridModeChanged(i))
                    {
                        GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, "��������", 1, new int[] { 6, 7, 8 });
                    }
                }
                else
                {
                    if (this._vers > 1.11)
                    {
                        if (this._vers >= 1.12)
                        {
                            if (this._vers >= 1.14)
                            {
                                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode1_12,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           this._device.ExternalDefenses[i].Osc,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR,
                                                           this._device.ExternalDefenses[i].Reset});
                            }
                            else
                            {
                                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode1_12,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           this._device.ExternalDefenses[i].Osc,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR,
                                                           false});
                            }

                        }
                        else
                        {
                            this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode1_11,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           this._device.ExternalDefenses[i].Osc,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR,
                                                            false});
                        }
                        if (!this.OnExternalDefenseGridModeChanged(i))
                        {
                            GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, "��������", 1, new int[] { 6, 7, 8 });
                        }
                    }
                    else
                    {
                        this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           false,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR,
                                                            false});
                        if (!this.OnExternalDefenseGridModeChanged(i))
                        {
                            GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, "��������", 1, new int[] { 6, 7, 8 });
                        }
                    }
                }
            }
        }



        private bool WriteExternalDefenses()
        {
            bool ret = true;
            if (this._device.ExternalDefenses.Count == TZL.CExternalDefenses.COUNT && this._externalDefenseGrid.Rows.Count <= TZL.CExternalDefenses.COUNT)
            {
                for (int i = 0; i < this._externalDefenseGrid.Rows.Count; i++)
                {
                    if (this._vers >= 1.11)
                    {
                        if (this._vers > 1.12)
                        {
                            this._device.ExternalDefenses[i].Mode1_12 = this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].Mode1_11 = this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                        }
                    }
                    else
                    {
                        this._device.ExternalDefenses[i].Mode = this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                    }

                    this._device.ExternalDefenses[i].BlockingNumber = this._externalDefenseGrid["_exDefBlockingCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].WorkingNumber = this._externalDefenseGrid["_exDefWorkingCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].Return = (bool)this._externalDefenseGrid["_exDefReturnCol", i].Value;
                    this._device.ExternalDefenses[i].APV_Return = (bool)this._externalDefenseGrid["_exDefAPVreturnCol", i].Value;
                    this._device.ExternalDefenses[i].ReturnNumber = this._externalDefenseGrid["_exDefReturnNumberCol", i].Value.ToString();

                    if (this._vers >= 1.1)
                    {
                        this._device.ExternalDefenses[i].Osc = (bool)this._externalDefenseGrid[9, i].Value;
                        this._device.ExternalDefenses[i].UROV = (bool)this._externalDefenseGrid[10, i].Value;
                        this._device.ExternalDefenses[i].APV = (bool)this._externalDefenseGrid[11, i].Value;
                        this._device.ExternalDefenses[i].AVR = (bool)this._externalDefenseGrid[12, i].Value;
                        if (this._vers >= 1.14)
                        {
                            this._device.ExternalDefenses[i].Reset = (bool)this._externalDefenseGrid[13, i].Value;
                        }
                    }
                    else
                    {
                        this._device.ExternalDefenses[i].UROV = (bool)this._externalDefenseGrid["_exDefUROVcol", i].Value;
                        this._device.ExternalDefenses[i].APV = (bool)this._externalDefenseGrid["_exDefAPVcol", i].Value;
                        this._device.ExternalDefenses[i].AVR = (bool)this._externalDefenseGrid["_exDefAVRcol", i].Value;
                    }

                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefReturnTimeCol", i].Value.ToString());
                        if (value > TZL.TIMELIMIT)
                        {
                            throw new OverflowException(TZL.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].ReturnTime = value;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefReturnTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        if (value > TZL.TIMELIMIT)
                        {
                            throw new OverflowException(TZL.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        }
                        this._device.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefWorkingTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }

                }

            }
            return ret;
        }

        #endregion

        #region ������� �������

        private void ReadInputSignals()
        {
            this.ClearCombos(this._inputSignalsCombos);
            this.FillInputSignalsCombos();
            this.SubscriptCombos(this._inputSignalsCombos);
            this._exchangeProgressBar.PerformStep();
            this._TT_Box.Text = this._device.TTtoDevice.ToString();
            this._TTNP_Box.Text = this._device.TTNPtoDevice.ToString();
            this._TN_Box.Text = this._device.TNtoDevice.ToString();
            this._TNNP_Box.Text = this._device.TNNPtoDevice.ToString();
            double res;
            if (this._device.MaxTok > 16) { res = this._device.MaxTok - 0.01; } else { res = this._device.MaxTok; }
            this._maxTok_Box.Text = res.ToString();
            this._switcherDurationBox.Text = this._device.SwitcherDuration.ToString();
            this._switcherImpulseBox.Text = this._device.SwitcherImpulse.ToString();
            this._switcherTimeBox.Text = this._device.SwitcherTimeUROV.ToString();
            if (this._device.SwitcherTokUROV > 24) { res = this._device.SwitcherTokUROV - 0.01; } else { res = this._device.SwitcherTokUROV; }
            this._switcherTokBox.Text = res.ToString();

            this._extOffCombo.SelectedItem = this._device.ExternalOff;
            this._extOnCombo.SelectedItem = this._device.ExternalOn;
            this._constraintGroupCombo.SelectedItem = this._device.ConstraintGroup;
            this._keyOffCombo.SelectedItem = this._device.KeyOff;
            this._keyOnCombo.SelectedItem = this._device.KeyOn;
            this._signalizationCombo.SelectedItem = this._device.SignalizationReset;
            this._switcherBlockCombo.SelectedItem = this._device.SwitcherBlock;
            this._switcherErrorCombo.SelectedItem = this._device.SwitcherError;
            this._switcherStateOffCombo.SelectedItem = this._device.SwitcherOff;
            this._switcherStateOnCombo.SelectedItem = this._device.SwitcherOn;
            this._exBlockSdtuCombo.SelectedItem = this._device.ExBlockSdtu;

            this._manageSignalsSDTU_Combo.SelectedItem = this._device.ManageSignalSDTU;
            this._manageSignalsKeyCombo.SelectedItem = this._device.ManageSignalKey;
            this._manageSignalsExternalCombo.SelectedItem = this._device.ManageSignalExternal;
            this._manageSignalsButtonCombo.SelectedItem = this._device.ManageSignalButton;

            this._TNNP_dispepairCombo.SelectedItem = this._device.TNNP_Dispepair;
            this._TN_dispepairCombo.SelectedItem = this._device.TN_Dispepair;
            this._TN_typeCombo.SelectedItem = this._device.TN_Type;
            this._TT_typeCombo.SelectedItem = this._device.TT_Type;
            this._OMP_TypeBox.SelectedItem = this._device.OMP_Type;
            if (this._Sost_ON_OFF.Visible)
            {
                this._Sost_ON_OFF.SelectedItem = this._device.Sost_ON_OFF;
            }
            this._HUD_Box.Text = (this._device.HUD / 1000).ToString();
            this._releDispepairBox.Text = this._device.DispepairImpulse.ToString();

            for (int i = 0; i < this._device.DispepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.DispepairSignal[i]);
            }
            this.ShowInputLogicSignals();
            this.ShowLogicKeys(this._device.Keys);
        }

        private void ReadKonfCount()
        {
            if (this._vers < 1.15)
            {
                if ((int)this._device.OscilloscopeKonfCount == 65535)
                {
                    this._oscKonfComb.SelectedIndex = 0;
                }
                else
                {
                    if ((int)this._device.OscilloscopeKonfCount <= this._oscKonfComb.SelectedIndex)
                    {
                        this._oscKonfComb.SelectedIndex = (int)this._device.OscilloscopeKonfCount;
                    }
                }
            }
            else
            {
                this.oscFix.SelectedItem = this._device.OscilloscopeFix;
                this.oscPercent.Text = this._device.OscilloscopePercent.ToString();
                if (this._vers >= 2)
                {
                    if (Common.VersionLiteral(this._device.DeviceVersion) == "S" || Common.VersionLiteral(this._device.DeviceVersion) == "H")
                    {
                        this.oscLength.SelectedItem = this._device.OscilloscopePeriodS;
                    }
                    else
                    {
                        this.oscLength.SelectedItem = this._device.OscilloscopePeriod3_0;
                    }
                }
                else
                {
                    this.oscLength.SelectedItem = this._device.OscilloscopePeriod;
                }
            }
        }

        private void InputSignalsLoadOK()
        {
            try
            {
                this._device.TT = this._device.TTtoDevice;
                this._device.TTNP = this._device.TTNPtoDevice;
                this.ReadInputSignals();
            }
            catch (Exception)
            {
            }
        }

        private ComboBox[] _inputSignalsCombos;
        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;
        private MaskedTextBox[] _inputSignalsUlongMaskBoxes;

        private void PrepareInputSignals()
        {
            if (this._Sost_ON_OFF.Visible)
            {
                this._inputSignalsCombos = new[] {this._Sost_ON_OFF, this._OMP_TypeBox, this._extOffCombo,this._extOnCombo,this._constraintGroupCombo,
                                                  this._keyOffCombo,this._keyOnCombo,this._oscKonfComb,this._signalizationCombo,this._exBlockSdtuCombo,
                                                  this._switcherBlockCombo,this._switcherErrorCombo,this._switcherStateOffCombo,
                                                  this._switcherStateOnCombo,this._TN_dispepairCombo,this._TN_typeCombo,this._TT_typeCombo,this._TNNP_dispepairCombo,
                                                  this._manageSignalsButtonCombo,this._manageSignalsExternalCombo,this._manageSignalsSDTU_Combo,this._manageSignalsKeyCombo, this.oscFix,this.oscLength };
            }
            else
            {
                this._inputSignalsCombos = new[] {this._OMP_TypeBox, this._extOffCombo,this._extOnCombo,this._constraintGroupCombo,
                                                  this._keyOffCombo,this._keyOnCombo,this._oscKonfComb,this._signalizationCombo,this._exBlockSdtuCombo,
                                                  this._switcherBlockCombo,this._switcherErrorCombo,this._switcherStateOffCombo,
                                                  this._switcherStateOnCombo,this._TN_dispepairCombo,this._TN_typeCombo,this._TT_typeCombo,this._TNNP_dispepairCombo,
                                                  this._manageSignalsButtonCombo,this._manageSignalsExternalCombo,this._manageSignalsSDTU_Combo,this._manageSignalsKeyCombo,this.oscFix,this.oscLength };
            }

            this._inputSignalsDoubleMaskBoxes = new[] { this._HUD_Box, this._maxTok_Box, this._TN_Box, this._TNNP_Box, this._switcherTokBox };
            this._inputSignalsUlongMaskBoxes = new[] { this._TTNP_Box, this._TT_Box, this._switcherTimeBox, this._switcherImpulseBox,
                this._switcherDurationBox, this._releDispepairBox, this.oscPercent };

            this.ClearCombos(this._inputSignalsCombos);
            this.FillInputSignalsCombos();
            
            this.SubscriptCombos(this._inputSignalsCombos);
            this.ShowLogicKeys(this._device.Keys);
            this._logicSignalsDataGrids = new[]
            {
                this._logicSignalsDataGrid1, this._logicSignalsDataGrid2, this._logicSignalsDataGrid3,this._logicSignalsDataGrid4,
                this._logicSignalsDataGrid5, this._logicSignalsDataGrid6, this._logicSignalsDataGrid7,this._logicSignalsDataGrid8
            };
            this.PrepareInputLogicSignals();
            this.PrepareMaskedBoxes(this._inputSignalsDoubleMaskBoxes, typeof(double));
            this.PrepareMaskedBoxes(this._inputSignalsUlongMaskBoxes, typeof(ulong));
        }

        private void PrepareInputLogicSignals()
        {
            foreach (DataGridView grid in this._logicSignalsDataGrids)
            {
                grid.Rows.Clear();
                for (int i = 0; i < 16; i++)
                {
                    grid.Rows.Add("�" + (i + 1), Strings.LogycValues[0]);
                }
            }
        }

        private void FillInputSignalsCombos()
        {
            this._extOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._extOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._constraintGroupCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._oscKonfComb.Items.AddRange(Strings.OscKonf.ToArray());
            this._exBlockSdtuCombo.Items.AddRange(Strings.Logic.ToArray());

            if (this._vers >= 2)
            {
                if (Common.VersionLiteral(this._device.DeviceVersion) == "S" || Common.VersionLiteral(this._device.DeviceVersion) == "H")
                {
                    this.oscLength.Items.AddRange(Strings.OscLengthS.ToArray());
                }
                else
                {
                    this.oscLength.Items.AddRange(Strings.OscLength3_0.ToArray());
                }
            }
            else
            {
                this.oscLength.Items.AddRange(Strings.OscLength.ToArray());
            }


            this.oscFix.Items.AddRange(Strings.OscFix.ToArray());
            this._signalizationCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherBlockCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherErrorCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TNNP_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TN_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TN_typeCombo.Items.AddRange(Strings.TN_Type.ToArray());
            this._TT_typeCombo.Items.AddRange(Strings.TT_Type.ToArray());
            this._OMP_TypeBox.Items.AddRange(Strings.ModesLight.ToArray());
            if (this._Sost_ON_OFF.Items.Count == 0)
            {
                this._Sost_ON_OFF.Items.AddRange(Strings.ModesLight.ToArray());
                this._Sost_ON_OFF.SelectedIndex = 0;
            }
            this._manageSignalsButtonCombo.Items.AddRange(Strings.Forbidden.ToArray());
            this._manageSignalsKeyCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsExternalCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsSDTU_Combo.Items.AddRange(Strings.Forbidden.ToArray());
        }
        
        private void ShowLogicKeys(string keys)
        {
            this._keysDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                this._keysDataGrid.Rows.Add(keys[i] == '0'
                    ? new object[] {i + 1, Strings.YesNo[0]}
                    : new object[] {i + 1, Strings.YesNo[1]});
            }
        }

        private void ShowInputLogicSignals()
        {
            for (int i = 0; i < this._logicSignalsDataGrids.Length; i++)
            {
                this._logicSignalsDataGrids[i].Rows.Clear();
                LogicState[] logicSignals = this._device.GetInputLogicSignals(i);
                for (int j = 0; j < 16; j++)
                {
                    this._logicSignalsDataGrids[i].Rows.Add("�" + (j + 1), logicSignals[j].ToString());
                }
            }
        }

        private void ValidateInputSignals()
        {
            this.ValidateMaskedBoxes(this._inputSignalsUlongMaskBoxes);
            this.ValidateMaskedBoxes(this._inputSignalsDoubleMaskBoxes);
        }

        private bool WriteInputSignals()
        {
            this._device.TTtoDevice = ushort.Parse(this._TT_Box.Text);
            this._device.TTNPtoDevice = ushort.Parse(this._TTNP_Box.Text);
            this._device.TNNPtoDevice = double.Parse(this._TNNP_Box.Text);
            this._device.TNtoDevice = double.Parse(this._TN_Box.Text);
            if (double.Parse(this._maxTok_Box.Text) > 16)
            {
                this._device.MaxTok = double.Parse(this._maxTok_Box.Text) + 0.01;
            }
            else
            {
                this._device.MaxTok = double.Parse(this._maxTok_Box.Text);
            }
            this._device.SwitcherDuration = ulong.Parse(this._switcherDurationBox.Text);
            this._device.SwitcherImpulse = ulong.Parse(this._switcherImpulseBox.Text);
            this._device.SwitcherTimeUROV = ulong.Parse(this._switcherTimeBox.Text);
            if (double.Parse(this._switcherTokBox.Text) > 24)
            {
                this._device.SwitcherTokUROV = double.Parse(this._switcherTokBox.Text) + 0.01;
            }
            else
            {
                this._device.SwitcherTokUROV = double.Parse(this._switcherTokBox.Text);
            }

            this._device.ExternalOff = this._extOffCombo.SelectedItem.ToString();
            this._device.ExternalOn = this._extOnCombo.SelectedItem.ToString();
            this._device.ConstraintGroup = this._constraintGroupCombo.SelectedItem.ToString();
            this._device.KeyOff = this._keyOffCombo.SelectedItem.ToString();
            this._device.KeyOn = this._keyOnCombo.SelectedItem.ToString();
            this._device.SignalizationReset = this._signalizationCombo.SelectedItem.ToString();
            this._device.SwitcherBlock = this._switcherBlockCombo.SelectedItem.ToString();
            this._device.SwitcherError = this._switcherErrorCombo.SelectedItem.ToString();
            this._device.SwitcherOff = this._switcherStateOffCombo.SelectedItem.ToString();
            this._device.SwitcherOn = this._switcherStateOnCombo.SelectedItem.ToString();
            this._device.ExBlockSdtu = this._exBlockSdtuCombo.SelectedItem.ToString();

            CheckedListBoxManager checkManager = new CheckedListBoxManager();
            checkManager.CheckList = this._dispepairCheckList;
            this._device.DispepairSignal = checkManager.ToBitArray();
            this._device.DispepairImpulse = ulong.Parse(this._releDispepairBox.Text);

            this._device.TNNP_Dispepair = this._TNNP_dispepairCombo.SelectedItem.ToString();
            this._device.TN_Dispepair = this._TN_dispepairCombo.SelectedItem.ToString();
            this._device.TN_Type = this._TN_typeCombo.SelectedItem.ToString();
            this._device.TT_Type = this._TT_typeCombo.SelectedItem.ToString();
            this._device.OMP_Type = this._OMP_TypeBox.SelectedItem.ToString();
            if (this._Sost_ON_OFF.Visible)
            {
                try
                {
                    this._device.Sost_ON_OFF = this._Sost_ON_OFF.SelectedItem.ToString();
                }
                catch
                {
                }
            }
            this._device.HUD = double.Parse(this._HUD_Box.Text);
            this._device.ManageSignalButton = this._manageSignalsButtonCombo.SelectedItem.ToString();
            this._device.ManageSignalExternal = this._manageSignalsExternalCombo.SelectedItem.ToString();
            this._device.ManageSignalKey = this._manageSignalsKeyCombo.SelectedItem.ToString();
            this._device.ManageSignalSDTU = this._manageSignalsSDTU_Combo.SelectedItem.ToString();
            this.WriteOscKonf();
            this.SaveKeys();
            this.SaveInputLogicSignals();
            return true;
        }

        private void SaveInputLogicSignals()
        {
            for (int j = 0; j < this._logicSignalsDataGrids.Length; j++)
            {
                DataGridView grid = this._logicSignalsDataGrids[j];
                LogicState[] logicSignals = new LogicState[16];
                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    object o = Enum.Parse(typeof (LogicState), grid[1, i].Value.ToString());
                    logicSignals[i] = (LogicState) o;
                }
                this._device.SetInputLogicSignals(j, logicSignals);
            }
        }

        #endregion

        #region �������� ����������
        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;
        private RadioButtonSelector _groupSelector;

        private void ValidateAutomatics()
        {
            this.ValidateMaskedBoxes(this._automaticsMaskedBoxes);
            this.ValidateMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint });
        }

        private void PrepareAutomaticsSignals()
        {
            this._automaticCombos = new ComboBox[] {this.apv_self_off,this.apv_conf,this.apv_blocking,
                                               this.avr_abrasion,this.avr_blocking,this.avr_reset_blocking,
                                               this.avr_return,this.avr_start,this.lzsh_conf,this.lzsh_confV114};
            this._automaticsMaskedBoxes = new MaskedTextBox[]{this.apv_time_1krat,this.apv_time_2krat,this.apv_time_3krat,this.apv_time_4krat,
                                                         this.apv_time_blocking,this.apv_time_ready,this.avr_disconnection,
                                                         this.avr_time_abrasion,this.avr_time_return,
                                                         };

            this.PrepareMaskedBoxes(this._automaticsMaskedBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint }, typeof(double));
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);



        }

        private void FillAutomaticCombo()
        {
            this.apv_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_start.Items.AddRange(Strings.Logic.ToArray());
            this.avr_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_reset_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_abrasion.Items.AddRange(Strings.Logic.ToArray());
            this.avr_return.Items.AddRange(Strings.Logic.ToArray());
            this.apv_conf.Items.AddRange(Strings.Crat.ToArray());
            this.lzsh_conf.Items.AddRange(Strings.ModesLight.ToArray());
            this.lzsh_confV114.Items.AddRange(Strings.LZSH114.ToArray());
            this.apv_self_off.Items.AddRange(Strings.YesNo.ToArray());


        }

        private void ReadAutomaticsPage()
        {
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.APV_Cnf;
            this.apv_blocking.Text = this._device.APV_Blocking;
            this.apv_self_off.Text = this._device.APV_Start;
            this.avr_start.Text = this._device.AVR_Start;
            this.avr_blocking.Text = this._device.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.AVR_Reset;
            this.avr_abrasion.Text = this._device.AVR_Abrasion;
            this.avr_return.Text = this._device.AVR_Return;
            this.lzsh_conf.Text = this._device.LZSH_Cnf;
            this.lzsh_confV114.Text = this._device.LZSH_CnfV114;

            this.apv_time_blocking.Text = this._device.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.AVR_Time_Off.ToString();
            double res;
            if (this._device.LZSH_Constraint > 24)
            { res = this._device.LZSH_Constraint - 0.01; }
            else { res = this._device.LZSH_Constraint; }
            this.lzsh_constraint.Text = res.ToString();
        }


        private void OnAutomaticsPageLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.APV_Cnf;
            this.apv_blocking.Text = this._device.APV_Blocking;
            this.apv_self_off.Text = this._device.APV_Start;
            this.avr_start.Text = this._device.AVR_Start;
            this.avr_blocking.Text = this._device.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.AVR_Reset;
            this.avr_abrasion.Text = this._device.AVR_Abrasion;
            this.avr_return.Text = this._device.AVR_Return;
            this.lzsh_conf.Text = this._device.LZSH_Cnf;
            this.lzsh_confV114.Text = this._device.LZSH_CnfV114;

            this.apv_time_blocking.Text = this._device.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.AVR_Time_Off.ToString();
            double res;
            if (this._device.LZSH_Constraint > 24)
            { res = this._device.LZSH_Constraint - 0.01; }
            else { res = this._device.LZSH_Constraint; }
            this.lzsh_constraint.Text = res.ToString();
            //lzsh_constraint.Text = _device.LZSH_Constraint.ToString();

        }

        private bool WriteAutomaticsPage()
        {
            this._device.APV_Cnf = this.apv_conf.Text;
            this._device.APV_Blocking = this.apv_blocking.Text;
            this._device.APV_Time_Blocking = ulong.Parse(this.apv_time_blocking.Text);
            this._device.APV_Time_Ready = ulong.Parse(this.apv_time_ready.Text);
            this._device.APV_Time_1Krat = ulong.Parse(this.apv_time_1krat.Text);
            this._device.APV_Time_2Krat = ulong.Parse(this.apv_time_2krat.Text);
            this._device.APV_Time_3Krat = ulong.Parse(this.apv_time_3krat.Text);
            this._device.APV_Time_4Krat = ulong.Parse(this.apv_time_4krat.Text);
            this._device.APV_Start = this.apv_self_off.Text;

            this._device.AVR_Supply_Off = this.avr_supply_off.Checked;
            this._device.AVR_Self_Off = this.avr_self_off.Checked;
            this._device.AVR_Switch_Off = this.avr_switch_off.Checked;
            this._device.AVR_Abrasion_Switch = this.avr_abrasion_switch.Checked;
            this._device.AVR_Reset_Switch = this.avr_permit_reset_switch.Checked;
            this._device.AVR_Start = this.avr_start.Text;
            this._device.AVR_Blocking = this.avr_blocking.Text;
            this._device.AVR_Reset = this.avr_reset_blocking.Text;
            this._device.AVR_Abrasion = this.avr_abrasion.Text;
            this._device.AVR_Time_Abrasion = ulong.Parse(this.avr_time_abrasion.Text);
            this._device.AVR_Return = this.avr_return.Text;
            this._device.AVR_Time_Return = ulong.Parse(this.avr_time_return.Text);
            this._device.AVR_Time_Off = ulong.Parse(this.avr_disconnection.Text);

            this._device.LZSH_Cnf = this.lzsh_conf.Text;
            this._device.LZSH_CnfV114 = this.lzsh_confV114.Text;
            if (double.Parse(this.lzsh_constraint.Text) > 24)
            { this._device.LZSH_Constraint = double.Parse(this.lzsh_constraint.Text) + 0.01; }
            else { this._device.LZSH_Constraint = double.Parse(this.lzsh_constraint.Text); }
            //_device.LZSH_Constraint = double.Parse(lzsh_constraint.Text);
            return true;
        }
        #endregion

        #region ������� ������

        void ChangeTokDefenseCellDisabling1(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                GridManager.ChangeCellDisabling(grid, row, "��������", 1, 4);
                GridManager.ChangeCellDisabling(grid, row, "��������", 1, 6);
                GridManager.ChangeCellDisabling(grid, row, "��������", 1, 12);
            }
        }

        void ChangeTokDefenseCellDisabling3(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                if (0 == row)
                {
                    GridManager.ChangeCellDisabling(grid, row, "��������", 1, 4);
                    GridManager.ChangeCellDisabling(grid, row, "��������", 1, 8);
                }
            }
        }


        void tokdef4(TZL.CTokDefenses tokDefenses, int i)
        {
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    if (tokDefenses[i].Feature.ToString() == "���������")
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode1_11,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       false,
                                                       tokDefenses[i].Oscv1_11,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);

                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode1_11,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       false,
                                                       tokDefenses[i].Oscv1_11,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }
                else
                {
                    if (tokDefenses[i].Feature.ToString() == "���������")
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].Osc,
                                                       "��������",
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);

                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].Osc,
                                                       "��������",
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }
            }
            else
            {
                if (tokDefenses[i].Feature.ToString() == "���������")
                {
                    this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       false,
                                                       "��������",
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);

                }
                else
                {
                    this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       false,
                                                       "��������",
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                }
            }

        }

        void tokdef4_10(TZL.CTokDefenses tokDefenses, int i)
        {
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode1_11,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       false,
                                                       tokDefenses[i].Oscv1_11,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }
                else
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].Osc,
                                                       "��������",
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }
            }
            else
            {
                this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       false,
                                                       "��������",
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
            }

        }

        void ShowTokDefenses(TZL.CTokDefenses tokDefenses)
        {
            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();

            this._tokDefenseIbox.Text = tokDefenses.I.ToString();
            this._tokDefenseInbox.Text = tokDefenses.In.ToString();
            this._tokDefenseI0box.Text = tokDefenses.I0.ToString();
            this._tokDefenseI2box.Text = tokDefenses.I2.ToString();

            for (int i = 0; i < 4; i++)
            {
                this.tokdef4(tokDefenses, i);
            }
            for (int i = 4; i < 10; i++)
            {
                this.tokdef4_10(tokDefenses, i);
            }

            const int Ig_index = 10;
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[Ig_index].Name,
                                                       tokDefenses[Ig_index].Mode1_11,
                                                       tokDefenses[Ig_index].BlockingNumber,
                                                       tokDefenses[Ig_index].PuskU,
                                                       tokDefenses[Ig_index].PuskU_Constraint,
                                                       tokDefenses[Ig_index].WorkConstraint,
                                                       tokDefenses[Ig_index].WorkTime,
                                                       tokDefenses[Ig_index].Speedup,
                                                       tokDefenses[Ig_index].SpeedupTime,
                                                       false,
                                                       tokDefenses[Ig_index].Oscv1_11,
                                                       tokDefenses[Ig_index].UROV,
                                                       tokDefenses[Ig_index].APV,
                                                       tokDefenses[Ig_index].AVR
                                                        });
                }
                else
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[Ig_index].Name,
                                                       tokDefenses[Ig_index].Mode,
                                                       tokDefenses[Ig_index].BlockingNumber,
                                                       tokDefenses[Ig_index].PuskU,
                                                       tokDefenses[Ig_index].PuskU_Constraint,
                                                       tokDefenses[Ig_index].WorkConstraint,
                                                       tokDefenses[Ig_index].WorkTime,
                                                       tokDefenses[Ig_index].Speedup,
                                                       tokDefenses[Ig_index].SpeedupTime,
                                                       tokDefenses[Ig_index].Osc,
                                                       "�������",
                                                       tokDefenses[Ig_index].UROV,
                                                       tokDefenses[Ig_index].APV,
                                                       tokDefenses[Ig_index].AVR
                                                        });
                }
            }
            else
            {
                this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[Ig_index].Name,
                                                       tokDefenses[Ig_index].Mode,
                                                       tokDefenses[Ig_index].BlockingNumber,
                                                       tokDefenses[Ig_index].PuskU,
                                                       tokDefenses[Ig_index].PuskU_Constraint,
                                                       tokDefenses[Ig_index].WorkConstraint,
                                                       tokDefenses[Ig_index].WorkTime,
                                                       tokDefenses[Ig_index].Speedup,
                                                       tokDefenses[Ig_index].SpeedupTime,
                                                       false,
                                                       "�������",
                                                       tokDefenses[Ig_index].UROV,
                                                       tokDefenses[Ig_index].APV,
                                                       tokDefenses[Ig_index].AVR
                                                        });
            }

            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);
            const int I12_index = 11;
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[I12_index].Name,
                                                       tokDefenses[I12_index].Mode1_11,
                                                       tokDefenses[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].WorkConstraint,
                                                       tokDefenses[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       false,
                                                       tokDefenses[I12_index].Oscv1_11,
                                                       tokDefenses[I12_index].UROV,
                                                       tokDefenses[I12_index].APV,
                                                       tokDefenses[I12_index].AVR  });
                }
                else
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[I12_index].Name,
                                                       tokDefenses[I12_index].Mode,
                                                       tokDefenses[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].WorkConstraint,
                                                       tokDefenses[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].Osc,
                                                       "��������",
                                                       tokDefenses[I12_index].UROV,
                                                       tokDefenses[I12_index].APV,
                                                       tokDefenses[I12_index].AVR  });
                }
            }
            else
            {
                this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[I12_index].Name,
                                                       tokDefenses[I12_index].Mode,
                                                       tokDefenses[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].WorkConstraint,
                                                       tokDefenses[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       false,
                                                       "��������",
                                                       tokDefenses[I12_index].UROV,
                                                       tokDefenses[I12_index].APV,
                                                       tokDefenses[I12_index].AVR  });
            }

            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);


            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid1.Rows.Count; i++)
            {
                this._tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        }

        void PrepareTokDefenses()
        {
            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();
            if (this._vers >= 1.1)
            {
                this._tokDefenseModeCol.Items.Clear();
                this._tokDefenseModeCol.Items.AddRange(Strings.ModesNew.ToArray());
                this._tokDefense3ModeCol.Items.Clear();
                this._tokDefense3ModeCol.Items.AddRange(Strings.ModesNew.ToArray());
                this._tokDefense4ModeCol.Items.Clear();
                this._tokDefense4ModeCol.Items.AddRange(Strings.ModesNew.ToArray());
                if (this._vers >= 1.1)
                {
                    this._tokDefenseOSCv11Col.Items.Clear();
                    this._tokDefenseOSCv11Col.Items.AddRange(Strings.Osc_v1_11.ToArray());
                    this._tokDefense3OSCv11Col.Items.Clear();
                    this._tokDefense3OSCv11Col.Items.AddRange(Strings.Osc_v1_11.ToArray());
                    this._tokDefense4OSCv11Col.Items.Clear();
                    this._tokDefense4OSCv11Col.Items.AddRange(Strings.Osc_v1_11.ToArray());
                }
            }
            else
            {
                this._tokDefenseModeCol.Items.Clear();
                this._tokDefenseModeCol.Items.AddRange(Strings.Modes.ToArray());
                this._tokDefense3ModeCol.Items.Clear();
                this._tokDefense3ModeCol.Items.AddRange(Strings.Modes.ToArray());
                this._tokDefense4ModeCol.Items.Clear();
                this._tokDefense4ModeCol.Items.AddRange(Strings.Modes.ToArray());
            }
            this._tokDefenseParameterCol.Items.Clear();
            this._tokDefenseParameterCol.Items.AddRange(Strings.TokParameter.ToArray());
            this._tokDefenseBlockExistCol.Items.Clear();
            this._tokDefenseBlockExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefenseDirectionCol.Items.Clear();
            this._tokDefenseDirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefenseFeatureCol.Items.Clear();
            this._tokDefenseFeatureCol.Items.AddRange(Strings.FeatureLight.ToArray());
            this._tokDefenseBlockNumberCol.Items.Clear();
            this._tokDefenseBlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefense3ParameterCol.Items.Clear();
            this._tokDefense3ParameterCol.Items.AddRange(Strings.EngineParameter.ToArray());
            this._tokDefense3BlockingExistCol.Items.Clear();
            this._tokDefense3BlockingExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefense3DirectionCol.Items.Clear();
            this._tokDefense3DirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefense3FeatureCol.Items.Clear();
            this._tokDefense3FeatureCol.Items.AddRange(Strings.Feature.ToArray());
            this._tokDefense3BlockingNumberCol.Items.Clear();
            this._tokDefense3BlockingNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefense4BlockNumberCol.Items.Clear();
            this._tokDefense4BlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefenseI0box.ValidatingType = this._tokDefenseI2box.ValidatingType =
            this._tokDefenseInbox.ValidatingType = this._tokDefenseIbox.ValidatingType = typeof(ulong);

            this._tokDefenseIbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI2box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI0box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseInbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);


            this.ShowTokDefenses(this._device.TokDefensesMain);

            this._tokDefenseGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid4.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);

            this._tokDefenseGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid4.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);

            this._tokDefenseGrid1.CellBeginEdit += this.GridOnCellBeginEdit;
            this._tokDefenseGrid3.CellBeginEdit += this.GridOnCellBeginEdit;
            this._tokDefenseGrid4.CellBeginEdit += this.GridOnCellBeginEdit;
        }

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs cellCancelEventArgs)
        {
            this.contextMenu.Tag = sender;
        }

        void _tokDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (this._tokDefenseModeCol == e.Cell.OwningColumn ||
                this._tokDefenseU_PuskCol == e.Cell.OwningColumn ||
                this._tokDefenseSpeedUpCol == e.Cell.OwningColumn ||
                this._tokDefenseDirectionCol == e.Cell.OwningColumn ||

                this._tokDefense3ModeCol == e.Cell.OwningColumn ||
                this._tokDefense3UpuskCol == e.Cell.OwningColumn ||
                this._tokDefense3SpeedupCol == e.Cell.OwningColumn ||
                this._tokDefense3DirectionCol == e.Cell.OwningColumn
                )
            {
                this.ChangeTokDefenseCellDisabling1(sender as DataGridView, e.Cell.RowIndex);
            }
            if (this._tokDefense4ModeCol == e.Cell.OwningColumn ||
                (this._tokDefense4SpeedupCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex) ||
                this._tokDefense4UpuskCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex)
            {
                this.ChangeTokDefenseCellDisabling3(sender as DataGridView, e.Cell.RowIndex);
            }

        }

        private void OnTokDefenseLoadOk()
        {
            this._exchangeProgressBar.PerformStep();

            if (this._mainRadioBtnGroup.Checked)
            {
                this.ShowTokDefenses(this._device.TokDefensesMain);
            }
            else
            {
                this.ShowTokDefenses(this._device.TokDefensesReserve);
            }
        }

        private bool OnTokDefenseGridModeChanged(DataGridView grid, int row)
        {
            int[] columns = new int[grid.Columns.Count - 2];
            if (grid == this._tokDefenseGrid4 && 1 == row)
            {
                columns = new int[6] { 2, 5, 6, 9, 10, 11 };
            }
            else
            {
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
            }

            return GridManager.ChangeCellDisabling(grid, row, "��������", 1, columns);
        }

        private bool WriteTokDefenseItem(TZL.CTokDefenses tokDefenses, DataGridView grid, int rowIndex, int itemIndex)
        {
            bool ret = true;
            bool enabled = false;
            tokDefenses[itemIndex].Mode = grid[1, rowIndex].Value.ToString();
            enabled = tokDefenses[itemIndex].Mode == "��������";
            tokDefenses[itemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
            tokDefenses[itemIndex].PuskU = (bool)grid[3, rowIndex].Value;
            try
            {
                double value = double.Parse(grid[4, rowIndex].Value.ToString());
                if ((value < 0 || value > 256) && !enabled)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256"+ CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator+"0]",
                        grid[4, rowIndex], this._defendTabPage, this._tokDefendTabPage);
                    ret = false;
                }
                else
                {
                    tokDefenses[itemIndex].PuskU_Constraint = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]",
                    grid[4, rowIndex], this._defendTabPage, this._tokDefendTabPage);
                ret = false;
            }

            if (grid != this._tokDefenseGrid4)
            {
                tokDefenses[itemIndex].Direction = grid[5, rowIndex].Value.ToString();
                tokDefenses[itemIndex].BlockingExist = grid[6, rowIndex].Value.ToString();
                tokDefenses[itemIndex].Parameter = grid[7, rowIndex].Value.ToString();
            }

            int workConstraintIndex = (grid == this._tokDefenseGrid4) ? 5 : 8;
            int workTimeIndex = (grid == this._tokDefenseGrid4) ? 6 : 10;
            int speedupIndex = (grid == this._tokDefenseGrid4) ? 7 : 11;
            int speedupTimeIndex = (grid == this._tokDefenseGrid4) ? 8 : 12;
            int OSCIndex = (grid == this._tokDefenseGrid4) ? 9 : 13;
            int OSCv11Index = (grid == this._tokDefenseGrid4) ? 10 : 14;
            int UROVIndex = (grid == this._tokDefenseGrid4) ? 11 : 15;
            int APVIndex = (grid == this._tokDefenseGrid4) ? 12 : 16;
            int AVRIndex = (grid == this._tokDefenseGrid4) ? 13 : 17;

            double limit = 40;
            if (grid == this._tokDefenseGrid1)
            {
                limit = 40;
            }
            if (grid == this._tokDefenseGrid4)
            {
                if (0 == rowIndex)
                {
                    limit = 5;
                }
                else
                {
                    limit = 100;
                }
            }
            if (grid == this._tokDefenseGrid3)
            {
                if (4 == rowIndex || 5 == rowIndex)
                {
                    limit = 5;
                }
            }

            try
            {
                double value;
                if (double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()) > 24)
                {
                    value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()) - 0.01;
                }
                else { value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()); }
                if ((value < 0 || value > limit) && !enabled)
                {
                    string message = string.Format("������� ����� � ��������� [0{0}0-{1}{2}0]", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                        limit, CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    this.ShowToolTip(message, grid[workConstraintIndex, rowIndex],
                        this._defendTabPage, this._tokDefendTabPage);
                    ret = false;
                }
                else
                {
                    if (value > 24)
                    {
                        tokDefenses[itemIndex].WorkConstraint = (value + 0.01);
                    }
                    else
                    { tokDefenses[itemIndex].WorkConstraint = value; }
                }
            }
            catch (Exception)
            {
                string message = string.Format("������� ����� � ��������� [0{0}0-{1}{2}0]", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                        limit, CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                this.ShowToolTip(message, grid[workConstraintIndex, rowIndex],
                    this._defendTabPage, this._tokDefendTabPage);
                ret = false;
            }
            tokDefenses[itemIndex].Feature = grid[9, rowIndex].Value.ToString();
            try
            {
                ulong value;
                if (grid[workTimeIndex, rowIndex].Value == null)
                {
                    value = 0;
                }
                else
                {
                    value = ulong.Parse(grid[workTimeIndex, rowIndex].Value.ToString());
                }

                if ("���������" == grid[9, rowIndex].Value.ToString())
                {
                    if (value > TZL.KZLIMIT && !enabled)
                    {
                        this.ShowToolTip(TZL.KZLIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._defendTabPage,
                            this._tokDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value * 10;
                    }

                }
                else
                {
                    if (value > TZL.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._defendTabPage,
                            this._tokDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value;
                    }
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._defendTabPage, this._tokDefendTabPage);
                ret = false;
            }
            tokDefenses[itemIndex].Speedup = (bool)grid[speedupIndex, rowIndex].Value;
            try
            {
                ulong value = ulong.Parse(grid[speedupTimeIndex, rowIndex].Value.ToString());
                if (value > TZL.TIMELIMIT && !enabled)
                {
                    this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._defendTabPage,
                        this._tokDefendTabPage);
                    ret = false;
                }
                else
                {
                    tokDefenses[itemIndex].SpeedupTime = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._defendTabPage, this._tokDefendTabPage);
                ret = false;
            }
            if (this._vers == 1.1)
            {
                tokDefenses[itemIndex].Osc = (bool)grid[OSCIndex, rowIndex].Value;
                tokDefenses[itemIndex].UROV = (bool)grid[UROVIndex, rowIndex].Value;
                tokDefenses[itemIndex].APV = (bool)grid[APVIndex, rowIndex].Value;
                tokDefenses[itemIndex].AVR = (bool)grid[AVRIndex, rowIndex].Value;
            }
            else
            {
                if (this._vers >= 1.11)
                {
                    tokDefenses[itemIndex].Oscv1_11 = grid[OSCv11Index, rowIndex].Value.ToString();
                    tokDefenses[itemIndex].UROV = (bool)grid[UROVIndex, rowIndex].Value;
                    tokDefenses[itemIndex].APV = (bool)grid[APVIndex, rowIndex].Value;
                    tokDefenses[itemIndex].AVR = (bool)grid[AVRIndex, rowIndex].Value;
                }
                else
                {
                    tokDefenses[itemIndex].UROV = (bool)grid[UROVIndex, rowIndex].Value;
                    tokDefenses[itemIndex].APV = (bool)grid[APVIndex, rowIndex].Value;
                    tokDefenses[itemIndex].AVR = (bool)grid[AVRIndex, rowIndex].Value;
                }
            }
            //}
            return ret;
        }

        private bool WriteTokDefenses(TZL.CTokDefenses tokDefenses)
        {
            bool ret = true;
            tokDefenses.I = ushort.Parse(this._tokDefenseIbox.Text);
            tokDefenses.I0 = ushort.Parse(this._tokDefenseI0box.Text);
            tokDefenses.I2 = ushort.Parse(this._tokDefenseI2box.Text);
            tokDefenses.In = ushort.Parse(this._tokDefenseInbox.Text);
            for (int i = 0; i < 12; i++)
            {
                if (i >= 0 && i < 4)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid1, i, i);
                }
                if (i >= 4 && i < 10)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid3, i - 4, i);
                }
                if (i >= 10 && i < 12)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid4, i - 10, i);
                }
            }

            return ret;
        }

        private void ValidateTokDefenses()
        {
            this._tokDefenseIbox.ValidateText();
            this._tokDefenseI2box.ValidateText();
            this._tokDefenseI0box.ValidateText();
            this._tokDefenseInbox.ValidateText();
        }

        #endregion

        #region ������ �� ����������
        
        private void OnVoltageDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainRadioBtnGroup.Checked)
            {
                this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
            }
            else
            {
                this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);
            }

        }

        private void PrepareVoltageDefenses()
        {
            this._voltageDefensesGrid3.Rows.Clear();
            this._voltageDefensesGrid2.Rows.Clear();
            this._voltageDefensesGrid1.Rows.Clear();
            this._voltageDefensesGrid1.Rows.Add(4);
            this._voltageDefensesGrid1[0, 0].Value = "U>";
            this._voltageDefensesGrid1[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 1].Value = "U>>";
            this._voltageDefensesGrid1[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 2].Value = "U<";
            this._voltageDefensesGrid1[0, 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 3].Value = "U<<";
            this._voltageDefensesGrid1[0, 3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2.Rows.Add(2);
            this._voltageDefensesGrid2[0, 0].Value = "U2>";
            this._voltageDefensesGrid2[0, 1].Value = "U2>>";
            this._voltageDefensesGrid2[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3.Rows.Add(2);
            this._voltageDefensesGrid3[0, 0].Value = "U0>";
            this._voltageDefensesGrid3[0, 1].Value = "U0>>";
            this._voltageDefensesGrid3[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this._voltageDefensesGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._voltageDefensesGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._voltageDefensesGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);

            this._voltageDefensesGrid1.CellBeginEdit += this.GridOnCellBeginEdit;
            this._voltageDefensesGrid2.CellBeginEdit += this.GridOnCellBeginEdit;
            this._voltageDefensesGrid3.CellBeginEdit += this.GridOnCellBeginEdit;

            this._voltageDefensesModeCol1.Items.Clear();

            if (this._vers >= 1.1)
            {
                this._voltageDefensesModeCol1.Items.AddRange(Strings.ModesNew.ToArray());
                if (this._vers >= 1.11)
                {
                    this._voltageDefensesOSCv11Col1.Items.Clear();
                    this._voltageDefensesOSCv11Col1.Items.AddRange(Strings.Osc_v1_11.ToArray());
                }
            }
            else
            {
                this._voltageDefensesModeCol1.Items.AddRange(Strings.Modes.ToArray());
            }
            this._voltageDefensesBlockingNumberCol1.Items.Clear();
            this._voltageDefensesBlockingNumberCol1.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol1.Items.Clear();
            this._voltageDefensesParameterCol1.Items.AddRange(Strings.VoltageParameterU.ToArray());

            this._voltageDefensesModeCol2.Items.Clear();
            if (this._vers >= 1.1)
            {
                this._voltageDefensesModeCol2.Items.AddRange(Strings.ModesNew.ToArray());
                if (this._vers >= 1.11)
                {
                    this._voltageDefensesOSC�11Col2.Items.Clear();
                    this._voltageDefensesOSC�11Col2.Items.AddRange(Strings.Osc_v1_11.ToArray());
                }
            }
            else
            {
                this._voltageDefensesModeCol2.Items.AddRange(Strings.Modes.ToArray());
            }

            this._voltageDefensesBlockingNumberCol2.Items.Clear();
            this._voltageDefensesBlockingNumberCol2.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol2.Items.Clear();
            this._voltageDefensesParameterCol2.Items.AddRange(Strings.VoltageParameterU2.ToArray());

            this._voltageDefensesModeCol3.Items.Clear();
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    this._voltageDefensesOSCv11Col3.Items.Clear();
                    this._voltageDefensesOSCv11Col3.Items.AddRange(Strings.Osc_v1_11.ToArray());
                }
                this._voltageDefensesModeCol3.Items.AddRange(Strings.ModesNew.ToArray());
            }
            else
            {
                this._voltageDefensesModeCol3.Items.AddRange(Strings.Modes.ToArray());
            }

            this._voltageDefensesBlockingNumberCol3.Items.Clear();
            this._voltageDefensesBlockingNumberCol3.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol3.Items.Clear();
            this._voltageDefensesParameterCol3.Items.AddRange(Strings.VoltageParameterU0.ToArray());

            this._voltageDefensesGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);
            this._voltageDefensesGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);
            this._voltageDefensesGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);

            this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
        }

        void OnVoltageDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._voltageDefensesModeCol1 ||
                e.Cell.OwningColumn == this._voltageDefensesModeCol2 ||
                e.Cell.OwningColumn == this._voltageDefensesModeCol3 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol1 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol2 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol3
                )
            {
                DataGridView voltageGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnVoltageDefenseModeChange(voltageGrid, rowIndex);
            }
        }

        private static void OnVoltageDefenseModeChange(DataGridView voltageGrid, int rowIndex)
        {
            int[] columns = new int[voltageGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            if (!GridManager.ChangeCellDisabling(voltageGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(voltageGrid, rowIndex, false, 6, 7, 8, 9);
            }
        }

        private void SetVoltageDefenseGridItem(DataGridView grid, TZL.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            if (this._vers >= 1.1)
            {
                grid[1, rowIndex].Value = voltageDefenses[defenseItemIndex].Mode1_11;
                grid[2, rowIndex].Value = voltageDefenses[defenseItemIndex].BlockingNumber;
                if (this._vers >= 1.11)
                {
                    grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].Parameter1_11;

                }
                else
                {
                    grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].Parameter;
                }
                grid[4, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkConstraint;
                grid[5, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkTime;
                grid[6, rowIndex].Value = voltageDefenses[defenseItemIndex].Return;
                grid[7, rowIndex].Value = voltageDefenses[defenseItemIndex].APV_Return;
                grid[8, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnConstraint;
                grid[9, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnTime;
                if (this._vers >= 1.11)
                {
                    grid[11, rowIndex].Value = voltageDefenses[defenseItemIndex].Oscv1_11;
                }
                else
                {
                    grid[10, rowIndex].Value = voltageDefenses[defenseItemIndex].Osc;
                }

                grid[12, rowIndex].Value = voltageDefenses[defenseItemIndex].UROV;
                grid[13, rowIndex].Value = voltageDefenses[defenseItemIndex].APV;
                grid[14, rowIndex].Value = voltageDefenses[defenseItemIndex].AVR;
                if (this._vers >= 1.14)
                {
                    grid[15, rowIndex].Value = voltageDefenses[defenseItemIndex].Reset;
                }
                if (this._vers >= 1.11 && grid[0, rowIndex].Value.ToString() == "U<")
                {
                    this._U1CB.SelectedItem = voltageDefenses[defenseItemIndex].Block1_11;

                }
                if (this._vers >= 1.11 && grid[0, rowIndex].Value.ToString() == "U<<")
                {
                    this._U2CB.SelectedItem = voltageDefenses[defenseItemIndex].Block1_11;

                }
            }
            else
            {
                grid[1, rowIndex].Value = voltageDefenses[defenseItemIndex].Mode;
                grid[2, rowIndex].Value = voltageDefenses[defenseItemIndex].BlockingNumber;
                grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].Parameter;
                grid[4, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkConstraint;
                grid[5, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkTime;
                grid[6, rowIndex].Value = voltageDefenses[defenseItemIndex].Return;
                grid[7, rowIndex].Value = voltageDefenses[defenseItemIndex].APV_Return;
                grid[8, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnConstraint;
                grid[9, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnTime;
                grid[12, rowIndex].Value = voltageDefenses[defenseItemIndex].UROV;
                grid[13, rowIndex].Value = voltageDefenses[defenseItemIndex].APV;
                grid[14, rowIndex].Value = voltageDefenses[defenseItemIndex].AVR;
            }
        }

        private void ShowVoltageDefenses(TZL.CVoltageDefenses voltageDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid1, voltageDefenses, i, i);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid1, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid2, voltageDefenses, i, i + 4);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid2, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid3, voltageDefenses, i, i + 6);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid3, i);
            }

        }

        private bool WriteVoltageDefenseGridItem(DataGridView grid, TZL.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            bool enabled = false;
            if (this._vers >= 1.11)
            {
                voltageDefenses[defenseItemIndex].Mode1_11 = grid[1, rowIndex].Value.ToString();
            }
            else
            {
                voltageDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            }

            enabled = voltageDefenses[defenseItemIndex].Mode == "��������";
            //if (!enabled)
            //{
            voltageDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();

            if (this._vers >= 1.11)
            {
                voltageDefenses[defenseItemIndex].Parameter1_11 = grid[3, rowIndex].Value.ToString();
            }
            else
            {
                voltageDefenses[defenseItemIndex].Parameter = grid[3, rowIndex].Value.ToString();
            }

            try
            {
                double value = double.Parse(grid[4, rowIndex].Value.ToString());
                if ((value < 0 || value > 256) && !enabled)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]",
                        grid[4, rowIndex], this._defendTabPage,
                        this._defendVoltageTabPage);
                    ret = false;
                }
                else
                {
                    voltageDefenses[defenseItemIndex].WorkConstraint = double.Parse(grid[4, rowIndex].Value.ToString());
                }

            }
            catch (Exception)
            {
                this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]", 
                    grid[4, rowIndex], this._defendTabPage,
                    this._defendVoltageTabPage);
                ret = false;
            }
            try
            {
                ulong value;
                if (grid[5, rowIndex].Value == null)
                {
                    value = 0;
                }
                else
                {
                    value = ulong.Parse(grid[5, rowIndex].Value.ToString());
                }

                if (value > TZL.TIMELIMIT && !enabled)
                {
                    this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], this._defendTabPage, this._defendVoltageTabPage);
                    ret = false;
                }
                else
                {
                    voltageDefenses[defenseItemIndex].WorkTime = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], this._defendTabPage, this._defendVoltageTabPage);
                ret = false;
            }
            voltageDefenses[defenseItemIndex].Return = (bool)grid[6, rowIndex].Value;
            voltageDefenses[defenseItemIndex].APV_Return = (bool)grid[7, rowIndex].Value;

            try
            {
                double value = double.Parse(grid[8, rowIndex].Value.ToString());
                if ((value < 0 || value > 256) && !enabled)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]", 
                        grid[8, rowIndex], this._defendTabPage,this._defendVoltageTabPage);
                    ret = false;
                }
                else
                {
                    voltageDefenses[defenseItemIndex].ReturnConstraint = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]",
                    grid[8, rowIndex], this._defendTabPage, this._defendVoltageTabPage);
                ret = false;
            }

            try
            {
                ulong value = ulong.Parse(grid[9, rowIndex].Value.ToString());
                if (value > TZL.TIMELIMIT && !enabled)
                {
                    this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], this._defendTabPage, this._defendVoltageTabPage);
                    ret = false;
                }
                else
                {
                    voltageDefenses[defenseItemIndex].ReturnTime = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], this._defendTabPage, this._defendVoltageTabPage);
                ret = false;
            }
            if (this._vers == 1.1)
            {
                voltageDefenses[defenseItemIndex].Osc = (bool)grid[10, rowIndex].Value;

            }
            if (this._vers >= 1.11)
            {
                voltageDefenses[defenseItemIndex].Oscv1_11 = grid[11, rowIndex].Value.ToString();

            }
            voltageDefenses[defenseItemIndex].UROV = (bool)grid[12, rowIndex].Value;
            voltageDefenses[defenseItemIndex].APV = (bool)grid[13, rowIndex].Value;
            voltageDefenses[defenseItemIndex].AVR = (bool)grid[14, rowIndex].Value;
            if (this._vers >= 1.14)
            {
                voltageDefenses[defenseItemIndex].Reset = (bool)grid[15, rowIndex].Value;
            }
            if (this._vers >= 1.11)
            {
                switch (grid[0, rowIndex].Value.ToString())
                {
                    case "U<":
                        {
                            voltageDefenses[defenseItemIndex].Block1_11 = this._U1CB.SelectedItem.ToString();
                            break;
                        }
                    case "U<<":
                        {
                            voltageDefenses[defenseItemIndex].Block1_11 = this._U2CB.SelectedItem.ToString();
                            break;
                        }
                    default: break;
                }
            }
            //}
            return ret;
        }

        public bool WriteVoltageDefenses(TZL.CVoltageDefenses voltageDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid1, voltageDefenses, i, i);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid2, voltageDefenses, i, i + 4);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid3, voltageDefenses, i, i + 6);
            }
            return ret;
        }

        #endregion

        #region �������� ����� �� �������

        private void FrequenceDefensesLoadFail()
        {
            this.OnLoadFail();
            this.OnLoadComplete();
        }

        private void FrequenceDefensesLoadOk()
        {
            this.OnFrequenceDefensesLoadOk();
            this.OnLoadComplete();
        }

        private void OnFrequenceDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainRadioBtnGroup.Checked)
            {
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
            else
            {
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }

        }

        private void PrepareFrequenceDefenses()
        {
            this._frequenceDefensesGrid.Rows.Clear();
            this._frequenceDefensesGrid.Rows.Add(4);

            this._frequenceDefensesMode.Items.Clear();
            this._frequenceDefensesOSCv11.Items.Clear();
            if (this._vers >= 1.1)
            {
                this._frequenceDefensesMode.Items.AddRange(Strings.ModesNew.ToArray());
                if (this._vers >= 1.11)
                {
                    this._frequenceDefensesOSCv11.Items.AddRange(Strings.Osc_v1_11.ToArray());
                }
            }
            else
            {
                this._frequenceDefensesMode.Items.AddRange(Strings.Modes.ToArray());
            }
            this._frequenceDefensesBlockingNumber.Items.Clear();
            this._frequenceDefensesBlockingNumber.Items.AddRange(Strings.Logic.ToArray());
            this._frequenceDefensesGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._frequenceDefensesGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnFrequenceDefenseCellStateChanged);
            this._frequenceDefensesGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            this._frequenceDefensesGrid.Height = this._frequenceDefensesGrid.ColumnHeadersHeight + (this._frequenceDefensesGrid.Rows.Count + 1) * this._frequenceDefensesGrid.RowTemplate.Height;
            this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
        }

        void OnFrequenceDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._frequenceDefensesMode || e.Cell.OwningColumn == this._frequenceDefensesReturn)
            {
                DataGridView frequenceGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnFrequenceDefenseModeChanged(frequenceGrid, rowIndex);
            }
        }

        private static void OnFrequenceDefenseModeChanged(DataGridView frequenceGrid, int rowIndex)
        {
            int[] columns = new int[frequenceGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }

            //if (rowIndex != 0)
            //{
            if (!GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, false, 5, 6, 7, 8);
            }
            //}
        }

        private void SetFrequenceDefenseGridItem(DataGridView grid, TZL.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Value = frequenceDefenses[defenseItemIndex].Name;
            if (this._vers >= 1.11)
            {
                grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode1_11;
            }
            else
            {
                if (this._vers == 1.1)
                {
                    grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode1_1;
                }
                else
                {
                    grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode;
                }
            }
            grid[2, rowIndex].Value = frequenceDefenses[defenseItemIndex].BlockingNumber;
            grid[3, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkConstraint;
            grid[4, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkTime;
            grid[5, rowIndex].Value = frequenceDefenses[defenseItemIndex].Return;
            grid[6, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV_Return;
            grid[7, rowIndex].Value = frequenceDefenses[defenseItemIndex].ReturnTime;
            grid[8, rowIndex].Value = frequenceDefenses[defenseItemIndex].ConstraintAPV;
            if (this._vers == 1.1)
            {
                grid[9, rowIndex].Value = frequenceDefenses[defenseItemIndex].Osc;
            }
            if (this._vers >= 1.11)
            {
                grid[10, rowIndex].Value = frequenceDefenses[defenseItemIndex].Oscv1_11;
            }
            grid[11, rowIndex].Value = frequenceDefenses[defenseItemIndex].UROV;
            grid[12, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV;
            grid[13, rowIndex].Value = frequenceDefenses[defenseItemIndex].AVR;
            grid[14, rowIndex].Value = frequenceDefenses[defenseItemIndex].Reset;
        }

        private void ShowFrequenceDefenses(TZL.CFrequenceDefenses frequenceDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                this.SetFrequenceDefenseGridItem(this._frequenceDefensesGrid, frequenceDefenses, i, i);
                OnFrequenceDefenseModeChanged(this._frequenceDefensesGrid, i);
            }
        }

        private bool WriteFrequenceDefenseGridItem(DataGridView grid, TZL.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            int minVal;
            int maxVal;
            try
            {
                this._vers = Common.VersionConverter(this._device.DeviceVersion);
            }
            catch (Exception)
            {
                this._vers = 1.11;
            }
            if (this._vers >= 1.1)
            {
                minVal = 40;
                maxVal = 60;
            }
            else
            {
                minVal = 47;
                maxVal = 52;
            }

            bool ret = true;
            bool rowEnabled = false;
            if (this._vers >= 1.11)
            {
                frequenceDefenses[defenseItemIndex].Mode1_11 = grid[1, rowIndex].Value.ToString();
            }
            else
            {
                frequenceDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            }
            rowEnabled = ("��������" == frequenceDefenses[defenseItemIndex].Mode);
            //if (rowEnabled)
            //{
            frequenceDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
            try
            {

                double value = double.Parse(grid[3, rowIndex].Value.ToString());

                if ((value < minVal || value > maxVal) && !rowEnabled)
                {
                    string message = string.Format("������� ����� � ��������� [{0}{1}0-{2}{3}0]", minVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, maxVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    this.ShowToolTip(message, grid[3, rowIndex], this._defendTabPage, this._frequencyDefendTabPage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].WorkConstraint = value;
                }

            }
            catch (Exception)
            {
                string message = string.Format("������� ����� � ��������� [{0}{1}0-{2}{3}0]", minVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, maxVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                this.ShowToolTip(message, grid[3, rowIndex], this._defendTabPage, this._frequencyDefendTabPage);
                ret = false;
            }
            try
            {
                ulong value;
                if (grid[4, rowIndex].Value == null)
                {
                    value = 0;
                }
                else
                {
                    value = ulong.Parse(grid[4, rowIndex].Value.ToString());
                }

                if (value > TZL.TIMELIMIT && !rowEnabled)
                {
                    throw new OverflowException(TZL.TIMELIMIT_ERROR_MSG);
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].WorkTime = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[4, rowIndex], this._defendTabPage, this._frequencyDefendTabPage);
                ret = false;
            }
            frequenceDefenses[defenseItemIndex].Return = (bool)grid[5, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].APV_Return = (bool)grid[6, rowIndex].Value;

            try
            {
                ulong value = ulong.Parse(grid[7, rowIndex].Value.ToString());
                if (value > TZL.TIMELIMIT && !rowEnabled)
                {
                    throw new OverflowException(TZL.TIMELIMIT_ERROR_MSG);
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].ReturnTime = value;
                }
            }
            catch (Exception)
            {
                this.ShowToolTip(TZL.TIMELIMIT_ERROR_MSG, grid[7, rowIndex], this._defendTabPage, this._frequencyDefendTabPage);
                ret = false;
            }
            try
            {
                if (frequenceDefenses[defenseItemIndex].Return)
                {
                    double value = double.Parse(grid[8, rowIndex].Value.ToString());

                    if ((value < minVal || value > maxVal) && !rowEnabled)
                    {
                        string message = string.Format("������� ����� � ��������� [{0}{1}0-{2}{3}0]", minVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, maxVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                        this.ShowToolTip(message, grid[8, rowIndex], this._defendTabPage, this._frequencyDefendTabPage);
                        ret = false;
                    }
                    else
                    {
                        frequenceDefenses[defenseItemIndex].ConstraintAPV = value;
                    }
                }

            }
            catch (Exception)
            {
                string message = string.Format("������� ����� � ��������� [{0}{1}0-{2}{3}0]", minVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, maxVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                this.ShowToolTip(message, grid[8, rowIndex], this._defendTabPage, this._frequencyDefendTabPage);
                ret = false;
            }
            if (this._vers == 1.1)
            {
                frequenceDefenses[defenseItemIndex].Osc = (bool)grid[9, rowIndex].Value;
            }
            if (this._vers >= 1.11)
            {
                frequenceDefenses[defenseItemIndex].Oscv1_11 = grid[10, rowIndex].Value.ToString();
            }
            frequenceDefenses[defenseItemIndex].UROV = (bool)grid[11, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].APV = (bool)grid[12, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].AVR = (bool)grid[13, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].Reset = (bool)grid[14, rowIndex].Value;
            //}
            return ret;
        }

        public bool WriteFrequenceDefenses(TZL.CFrequenceDefenses frequenceDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= this.WriteFrequenceDefenseGridItem(this._frequenceDefensesGrid, frequenceDefenses, i, i);
            }
            return ret;
        }

        #endregion

        private void _OMP_TypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._OMP_TypeBox.Text == "�������")
            {
                this._HUD_Box.ReadOnly = false;
            }
            else
            {
                this._HUD_Box.Text = "0";
                this._HUD_Box.ReadOnly = true;
            }
        }

        private void SaveKeys()
        {
            string keys = "";
            for (int i = 0; i < 16; i++)
            {
                if (this._keysDataGrid.Rows[i].Cells[1].Value.ToString() == "��")
                {
                    keys += "1";
                }
                else
                {
                    keys += "0";
                }
            }

            this._device.Keys = keys;
        }

        public void WriteOscKonf()
        {
            if (this._vers < 1.15)
            {
                this._device.OscilloscopeKonfCount = this._oscKonfComb.SelectedIndex;
            }
            else
            {
                this._device.OscilloscopeFix = this.oscFix.SelectedItem.ToString();
                this._device.OscilloscopePercent = Convert.ToUInt16(this.oscPercent.Text);

                if (this._vers >= 2)
                {
                    if (Common.VersionLiteral(this._device.DeviceVersion) == "S" || Common.VersionLiteral(this._device.DeviceVersion) == "H")
                    {
                        this._device.OscilloscopePeriodS = this.oscLength.SelectedItem.ToString();
                    }
                    else
                    {
                        this._device.OscilloscopePeriod3_0 = this.oscLength.SelectedItem.ToString();
                    }
                }
                else
                {
                    this._device.OscilloscopePeriod = this.oscLength.SelectedItem.ToString();
                }

            }
        }

        private void _oscKonfComb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._butReadClick)
            {
                this._device.OscilloscopeKonfCount = this._oscKonfComb.SelectedIndex;
            }
            this._butReadClick = false;
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            SaveToHtml();
        }

        private void SaveToHtml()
        {
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._processLabel.Text = HtmlExport.Export(Resources.MR741Main, Resources.MR741Res, this._device, "��741",
                    this._device.DeviceVersion);
            }
        }

        //����������� ��� ��������� ������ �������
        private void _mainRadioBtnGroup_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioBtnGroup.Checked)
            {
                this._device.TokDefensesReserve = new TZL.CTokDefenses();
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.ShowTokDefenses(this._device.TokDefensesMain);

                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.ShowVoltageDefenses(this._device.VoltageDefensesMain);

                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);

            }
            else
            {
                this._device.TokDefensesMain = new TZL.CTokDefenses();
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.ShowTokDefenses(this._device.TokDefensesReserve);

                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);

                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip) sender;
            if(menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.resetSetPointStruct)
            {
                this.ResetSetpoints();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }
        
        private void TzlConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }


        #region [����� �������]

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints();
        }

        private void ResetSetpoints()
        {
            DialogResult res = MessageBox.Show(@"�������� �������?", @"�������",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Question);

            if (res == DialogResult.No) return;

            this.ResetInputSignals();
            this.ResetOutputSignals();
            this.ResetExtDef();
            this.ResetAutomat();
            this.ResetCorners();
            this.ResetTokDef();
            this.ResetVoltDef();
            this.ResetFreqDef();
        }


        #region [������� �������]
        private void ResetInputSignals()
        {
            //��������� ����
            this._TT_Box.Text = 0.ToString();
            this._TTNP_Box.Text = 0.ToString();
            this._maxTok_Box.Text = 0.ToString();
            this._TT_typeCombo.SelectedIndex = 0;

            //��� ��
            this._TN_Box.Text = 0.ToString();
            this._TNNP_Box.Text = 0.ToString();
            this._TN_dispepairCombo.SelectedIndex = 0;
            this._TNNP_dispepairCombo.SelectedIndex = 0;
            this._TN_typeCombo.SelectedIndex = 0;

            //������� �������
            this._keyOffCombo.SelectedIndex = 0;
            this._keyOnCombo.SelectedIndex = 0;
            this._extOffCombo.SelectedIndex = 0;
            this._extOnCombo.SelectedIndex = 0;
            this._signalizationCombo.SelectedIndex = 0;
            this._constraintGroupCombo.SelectedIndex = 0;
            this._exBlockSdtuCombo.SelectedIndex = 0;

            //������� ����������
            this._manageSignalsButtonCombo.SelectedIndex = 0;
            this._manageSignalsKeyCombo.SelectedIndex = 0;
            this._manageSignalsExternalCombo.SelectedIndex = 0;
            this._manageSignalsSDTU_Combo.SelectedIndex = 0;

            //��������� ������������
            this.oscLength.SelectedIndex = 0;
            this.oscFix.SelectedIndex = 0;
            this.oscPercent.Text = 1.ToString();

            //�����������
            this._switcherStateOffCombo.SelectedIndex = 0;
            this._switcherStateOnCombo.SelectedIndex = 0;
            this._switcherErrorCombo.SelectedIndex = 0;
            this._switcherBlockCombo.SelectedIndex = 0;
            this._switcherTimeBox.Text = 0.ToString();
            this._switcherTokBox.Text = 0.ToString();
            this._switcherImpulseBox.Text = 0.ToString();
            this._switcherDurationBox.Text = 0.ToString();
            this._Sost_ON_OFF.SelectedIndex = 0;

            //����������� ����� �����������
            this._OMP_TypeBox.SelectedIndex = 0;
            this._HUD_Box.Text = 0.ToString();
            this.PrepareInputLogicSignals();
            //����� ��������������
            this._releDispepairBox.Text = 0.ToString();

            for (int i = 0; i < this._device.DispepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.DispepairSignal[i]);
            }
            this._dispepairCheckList.ClearSelected();
            
            //����� ������
            this._keysDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                this._keysDataGrid.Rows.Add(i + 1, Strings.YesNo[0]);
            }
        }
        #endregion

        #region [�������� �������]
        private void ResetOutputSignals()
        {
            //����� ���
            for (int i = 0; i < this._outputLogicCheckLists.Length; i++)
            {
                this._device.SetOutputLogicSignals(i, new BitArray(120));
                foreach (int ind in this._outputLogicCheckLists[i].CheckedIndices)
                {
                    this._outputLogicCheckLists[i].SetItemCheckState(ind, CheckState.Unchecked);
                }
            }

            //����� �������� ����
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < TZL.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(i + 1, "�����������", "���", 0 );
            }

            //����� �����������
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < TZL.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(i + 1,"�����������","���",false,false,false);
            }

            //����� ���� �������������           
            for (int i = 0; i < this._dispepairCheckList.Items.Count; i++)
            {
                this._dispepairCheckList.SetItemCheckState(i, CheckState.Unchecked);
            }
        }
        #endregion

        #region [������� ������]
        private void ResetExtDef()
        {
            this._externalDefenseGrid.Rows.Clear();
            for (int i = 0; i < TZL.CExternalDefenses.COUNT; i++)
            {
                if (this._vers >= 1.1)
                {
                    this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           "��������",
                                                           "���",
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           false,
                                                           false,
                                                           false});
                }
                else
                {
                    this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           "��������",
                                                           "���",
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           false,
                                                           false,
                                                           false});
                    //}

                }

                this.OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, "��������", 1, new int[] { 6, 7, 8 });

            }
        }
        #endregion

        #region [����������]
        private void ResetAutomat()
        {
            this.apv_conf.SelectedIndex = 0;
            this.apv_blocking.SelectedIndex = 0;
            this.apv_time_blocking.Text = 0.ToString();
            this.apv_time_ready.Text = 0.ToString();
            this.apv_time_1krat.Text = 0.ToString();
            this.apv_time_2krat.Text = 0.ToString();
            this.apv_time_3krat.Text = 0.ToString();
            this.apv_time_4krat.Text = 0.ToString();
            this.apv_self_off.SelectedIndex = 0;

            this.lzsh_confV114.SelectedIndex = 0;
            this.lzsh_constraint.Text = 0.ToString();

            this.avr_supply_off.Checked = false;
            this.avr_switch_off.Checked = false;
            this.avr_self_off.Checked = false;
            this.avr_abrasion_switch.Checked = false;

            this.avr_permit_reset_switch.Checked = false;

            this.avr_start.SelectedIndex = 0;
            this.avr_blocking.SelectedIndex = 0;
            this.avr_reset_blocking.SelectedIndex = 0;
            this.avr_abrasion.SelectedIndex = 0;
            this.avr_time_abrasion.Text = 0.ToString();
            this.avr_return.SelectedIndex = 0;
            this.avr_time_return.Text = 0.ToString();
            this.avr_disconnection.Text = 0.ToString();
        }
        #endregion

        #region [����]
        private void ResetCorners()
        {
            this._device.TokDefensesMain = new TZL.CTokDefenses();

            this._tokDefenseIbox.Text = 0.ToString();
            this._tokDefenseInbox.Text = 0.ToString();
            this._tokDefenseI0box.Text = 0.ToString();
            this._tokDefenseI2box.Text = 0.ToString();

            this._tokDefenseIbox.Text = this._device.TokDefensesMain.I.ToString();
            this._tokDefenseInbox.Text = this._device.TokDefensesMain.In.ToString();
            this._tokDefenseI0box.Text = this._device.TokDefensesMain.I0.ToString();
            this._tokDefenseI2box.Text = this._device.TokDefensesMain.I2.ToString();

            this._device.TokDefensesReserve = new TZL.CTokDefenses();
        }
        #endregion

        #region [������� ������]
        private void ResetTokDef()
        {
            this._device.TokDefensesMain = new TZL.CTokDefenses();

            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();
            
            for (int i = 0; i < 4; i++)
            {
                this.tokdef4(this._device.TokDefensesMain, i);
            }

            for (int i = 4; i < 10; i++)
            {
                this.tokdef4_10(this._device.TokDefensesMain, i);
            }

            const int Ig_index = 10;
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ this._device.TokDefensesMain[Ig_index].Name,
                                                       this._device.TokDefensesMain[Ig_index].Mode1_11,
                                                       this._device.TokDefensesMain[Ig_index].BlockingNumber,
                                                       this._device.TokDefensesMain[Ig_index].PuskU,
                                                       this._device.TokDefensesMain[Ig_index].PuskU_Constraint,
                                                       this._device.TokDefensesMain[Ig_index].WorkConstraint,
                                                       this._device.TokDefensesMain[Ig_index].WorkTime,
                                                       this._device.TokDefensesMain[Ig_index].Speedup,
                                                       this._device.TokDefensesMain[Ig_index].SpeedupTime,
                                                       false,
                                                       this._device.TokDefensesMain[Ig_index].Oscv1_11,
                                                       this._device.TokDefensesMain[Ig_index].UROV,
                                                       this._device.TokDefensesMain[Ig_index].APV,
                                                       this._device.TokDefensesMain[Ig_index].AVR
                                                        });
                }
                else
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ this._device.TokDefensesMain[Ig_index].Name,
                                                       this._device.TokDefensesMain[Ig_index].Mode,
                                                       this._device.TokDefensesMain[Ig_index].BlockingNumber,
                                                       this._device.TokDefensesMain[Ig_index].PuskU,
                                                       this._device.TokDefensesMain[Ig_index].PuskU_Constraint,
                                                       this._device.TokDefensesMain[Ig_index].WorkConstraint,
                                                       this._device.TokDefensesMain[Ig_index].WorkTime,
                                                       this._device.TokDefensesMain[Ig_index].Speedup,
                                                       this._device.TokDefensesMain[Ig_index].SpeedupTime,
                                                       this._device.TokDefensesMain[Ig_index].Osc,
                                                       "�������",
                                                       this._device.TokDefensesMain[Ig_index].UROV,
                                                       this._device.TokDefensesMain[Ig_index].APV,
                                                       this._device.TokDefensesMain[Ig_index].AVR
                                                        });
                }
            }
            else
            {
                this._tokDefenseGrid4.Rows.Add(new object[]{ this._device.TokDefensesMain[Ig_index].Name,
                                                       this._device.TokDefensesMain[Ig_index].Mode,
                                                       this._device.TokDefensesMain[Ig_index].BlockingNumber,
                                                       this._device.TokDefensesMain[Ig_index].PuskU,
                                                       this._device.TokDefensesMain[Ig_index].PuskU_Constraint,
                                                       this._device.TokDefensesMain[Ig_index].WorkConstraint,
                                                       this._device.TokDefensesMain[Ig_index].WorkTime,
                                                       this._device.TokDefensesMain[Ig_index].Speedup,
                                                       this._device.TokDefensesMain[Ig_index].SpeedupTime,
                                                       false,
                                                       "�������",
                                                       this._device.TokDefensesMain[Ig_index].UROV,
                                                       this._device.TokDefensesMain[Ig_index].APV,
                                                       this._device.TokDefensesMain[Ig_index].AVR
                                                        });
            }

            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);
            const int I12_index = 11;
            if (this._vers >= 1.1)
            {
                if (this._vers >= 1.11)
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ this._device.TokDefensesMain[I12_index].Name,
                                                       this._device.TokDefensesMain[I12_index].Mode1_11,
                                                       this._device.TokDefensesMain[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       this._device.TokDefensesMain[I12_index].WorkConstraint,
                                                       this._device.TokDefensesMain[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       false,
                                                       this._device.TokDefensesMain[I12_index].Oscv1_11,
                                                       this._device.TokDefensesMain[I12_index].UROV,
                                                       this._device.TokDefensesMain[I12_index].APV,
                                                       this._device.TokDefensesMain[I12_index].AVR  });
                }
                else
                {
                    this._tokDefenseGrid4.Rows.Add(new object[]{ this._device.TokDefensesMain[I12_index].Name,
                                                       this._device.TokDefensesMain[I12_index].Mode,
                                                       this._device.TokDefensesMain[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       this._device.TokDefensesMain[I12_index].WorkConstraint,
                                                       this._device.TokDefensesMain[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       this._device.TokDefensesMain[I12_index].Osc,
                                                       "��������",
                                                       this._device.TokDefensesMain[I12_index].UROV,
                                                       this._device.TokDefensesMain[I12_index].APV,
                                                       this._device.TokDefensesMain[I12_index].AVR  });
                }
            }
            else
            {
                this._tokDefenseGrid4.Rows.Add(new object[]{ this._device.TokDefensesMain[I12_index].Name,
                                                       this._device.TokDefensesMain[I12_index].Mode,
                                                       this._device.TokDefensesMain[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       this._device.TokDefensesMain[I12_index].WorkConstraint,
                                                       this._device.TokDefensesMain[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       false,
                                                       "��������",
                                                       this._device.TokDefensesMain[I12_index].UROV,
                                                       this._device.TokDefensesMain[I12_index].APV,
                                                       this._device.TokDefensesMain[I12_index].AVR  });
            }

            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);


            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid1.Rows.Count; i++)
            {
                this._tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }

        }
        #endregion

        #region [����������]
        private void ResetVoltDef()
        {
            this._device.VoltageDefensesMain = new TZL.CVoltageDefenses();

            this._voltageDefensesGrid3.Rows.Clear();
            this._voltageDefensesGrid2.Rows.Clear();
            this._voltageDefensesGrid1.Rows.Clear();
            this._voltageDefensesGrid1.Rows.Add(4);
            this._voltageDefensesGrid1[0, 0].Value = "U>";
            this._voltageDefensesGrid1[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 1].Value = "U>>";
            this._voltageDefensesGrid1[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 2].Value = "U<";
            this._voltageDefensesGrid1[0, 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 3].Value = "U<<";
            this._voltageDefensesGrid1[0, 3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2.Rows.Add(2);
            this._voltageDefensesGrid2[0, 0].Value = "U2>";
            this._voltageDefensesGrid2[0, 1].Value = "U2>>";
            this._voltageDefensesGrid2[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3.Rows.Add(2);
            this._voltageDefensesGrid3[0, 0].Value = "U0>";
            this._voltageDefensesGrid3[0, 1].Value = "U0>>";
            this._voltageDefensesGrid3[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

            for (int i = 0; i < 4; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid1, this._device.VoltageDefensesMain, i, i);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid1, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid2, this._device.VoltageDefensesMain, i, i + 4);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid2, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid3, this._device.VoltageDefensesMain, i, i + 6);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid3, i);
            }

            this._device.VoltageDefensesReserve = new TZL.CVoltageDefenses();
        }
        #endregion 

        #region [�������]
        private void ResetFreqDef()
        {
            this._frequenceDefensesGrid.Rows.Clear();
            this._frequenceDefensesGrid.Rows.Add(4);
            this._device.FrequenceDefensesMain = new TZL.CFrequenceDefenses();

            for (int i = 0; i < 4; i++)
            {
                this.SetFrequenceDefenseGridItem(this._frequenceDefensesGrid, this._device.FrequenceDefensesMain, i, i);
                OnFrequenceDefenseModeChanged(this._frequenceDefensesGrid, i);
            }
            
            this._device.FrequenceDefensesReserve = new TZL.CFrequenceDefenses();
        }
        #endregion

        #endregion

        private void dataErrorGrid(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}