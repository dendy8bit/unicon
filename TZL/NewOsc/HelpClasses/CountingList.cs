﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.MBServer;
using BEMN.TZL.Configuration;
using BEMN.TZL.Configuration.Structers;
using BEMN.TZL.NewOsc.Structers;

namespace BEMN.TZL.NewOsc.HelpClasses
{
    public class CountingList
    {
         #region [Constants]
        /// <summary>
        /// Размер одного отсчёта(в словах)
        /// </summary>
        public const int COUNTING_SIZE = 16;
        /// <summary>
        /// Кол-во дискрет
        /// </summary>
        public const int DISCRETS_COUNT = 128;
        /// <summary>
        /// Кол-во токов
        /// </summary>
        public const int CURRENTS_COUNT = 4;
        /// <summary>
        /// Кол-во напряжений
        /// </summary>
        public const int VOLTAGES_COUNT = 4;

        protected const string SAVE_OSC_FAIL = "Ошибка сохранения файла";

        public static readonly string[] INames = { "Ia", "Ib", "Ic", "In" };
        public static readonly string[] UNames = { "Ua", "Ub", "Uc", "Un" };
        #endregion [Constants]


        #region [protected fields]
        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        protected int _alarm;
        protected OscJurnalStruct _oscJournalStruct;
        /// <summary>
        /// Массив отсчётов разбитый на слова
        /// </summary>
        protected ushort[][] _countingArray;
        /// <summary>
        /// Массив дискрет 1-16
        /// </summary>
        protected ushort[][] _discrets;
        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        protected int _count;

        #region [Токи]
        /// <summary>
        /// Токи
        /// </summary>
        protected double[][] _currents;
        /// <summary>
        /// Неприведённые значения токов
        /// </summary>
        protected ushort[][] _baseCurrents;

        /// <summary>
        /// Минимальный ток
        /// </summary>
        protected double _minI;
        /// <summary>
        /// Максимальный ток
        /// </summary>
        protected double _maxI;

        /// <summary>
        /// Минимальный ток
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// Максимальный ток
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [Токи]


        #region [Напряжения]
        /// <summary>
        /// Напряжения
        /// </summary>
        protected double[][] _voltages;
        /// <summary>
        /// Неприведённые значения напряжений
        /// </summary>
        protected ushort[][] _baseVoltages;

        /// <summary>
        /// Минимальное напряжение
        /// </summary>
        protected double _minU;
        /// <summary>
        /// Максимальное напряжение
        /// </summary>
        protected double _maxU;
        /// <summary>
        /// Минимальное напряжение
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// Максимальное напряжение
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }  
        #endregion [Напряжения]
        
        #endregion [protected fields]

        /// <summary>
        /// Дискреты
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        public bool IsLoad { get; protected set; }
        public string FilePath { get; protected set; }


        protected string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }
        /// <summary>
        /// Коэффициенты перещёта для токов
        /// </summary>
        protected double[,] CurrentsKoefs { get; set; }
        /// <summary>
        /// Коэффициенты перещёта для напряжений
        /// </summary>
        protected double[,] VoltageKoefs { get; set; }

        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJurnalStruct oscJournalStruct, MeasureTransStruct measure)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //Общее количество отсчётов
            this._count = pageValue.Length/COUNTING_SIZE;
            
            this.InitCountingArray(pageValue, COUNTING_SIZE);
            this.InitAnalog(measure);
            this.InitDiscrets(COUNTING_SIZE - CURRENTS_COUNT - VOLTAGES_COUNT);
        }

        protected void InitCountingArray(ushort[] pageValue, int countingSize)
        {
            //Инициализация массива
            for (int i = 0; i < countingSize; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == countingSize)
                {
                    m++;
                    n = 0;
                }
            }
        }

        protected void InitAnalog(MeasureTransStruct measure)
        {
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new ushort[CURRENTS_COUNT][];
            double[,] currentsKoefs = new double[CURRENTS_COUNT, 2];
            currentsKoefs[0, 0] = currentsKoefs[1, 0] = currentsKoefs[2, 0] = measure.Tt * 40.0 * Math.Sqrt(2.0) / 32768.0;
            currentsKoefs[0, 1] = currentsKoefs[1, 1] = currentsKoefs[2, 1] = -(32768.0 * currentsKoefs[0, 0]);
            currentsKoefs[3, 0] = measure.Ttnp * 5.0 * Math.Sqrt(2.0) / 32768.0;
            currentsKoefs[3, 1] = -(32768.0 * currentsKoefs[3, 0]);
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i];
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i, 0] * (double)a + currentsKoefs[i, 1]).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new ushort[VOLTAGES_COUNT][];
            double[,] voltageKoefs = new double[VOLTAGES_COUNT, 2];
            voltageKoefs[0, 0] = voltageKoefs[1, 0] = voltageKoefs[2, 0] = measure.Tn * 256.0 * Math.Sqrt(2.0) / 32768.0;
            voltageKoefs[0, 1] = voltageKoefs[1, 1] = voltageKoefs[2, 1] = -(32768.0 * voltageKoefs[0, 0]);
            voltageKoefs[3, 0] = measure.Tnnp * 256.0 * Math.Sqrt(2.0) / 32768.0;
            voltageKoefs[3, 1] = -(32768.0 * voltageKoefs[3, 0]);
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + 4];

                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i, 0] * (double)a + voltageKoefs[i, 1]).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;
        }

        protected void InitDiscrets(int countDiscrets)
        {
            List<ushort[]> discrets = new List<ushort[]>();
            for (int i = 0; i < countDiscrets; i++)
            {
                ushort[][] d = this.DiscretArrayInit(this._countingArray[CURRENTS_COUNT+VOLTAGES_COUNT + i]);
                discrets.AddRange(d);
            }
            this._discrets = discrets.ToArray();
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// Превращает каждое слово в инвертированный массив бит(значения 0/1) 
        /// </summary>
        /// <param pinName="sourseArray">Массив массивов бит</param>
        /// <returns></returns>
        protected ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public virtual void Save(string filePath)
        {
            try
            {
                string hdrPath = Path.ChangeExtension(filePath, "hdr");

                using (StreamWriter hdrFile = new StreamWriter(hdrPath))
                {
                    hdrFile.WriteLine("МР741  {0} {1} ступень - {2}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                    hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                    hdrFile.WriteLine("Alarm = {0}", this._alarm);
                    hdrFile.WriteLine(1251);
                }

                string cgfPath = Path.ChangeExtension(filePath, "cfg");

                Encoding encoding = Encoding.GetEncoding(1251);

                using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, encoding))
                {
                    cgfFile.WriteLine("MP741,1");
                    cgfFile.WriteLine("136,8A,128D");
                    int index = 1;
                    for (int i = 0; i < this.Currents.Length; i++)
                    {
                        NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                        cgfFile.WriteLine("{0},{1},,,A,{2},{3},0,-32768,32767,1,1,P", index, INames[i],
                                          this.CurrentsKoefs[i, 0].ToString(format),
                                          this.CurrentsKoefs[i, 1].ToString(format));
                        index++;
                    }

                    for (int i = 0; i < this.Voltages.Length; i++)
                    {
                        NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                        cgfFile.WriteLine("{0},{1},,,V,{2},{3},0,-32768,32767,1,1,P", index, UNames[i],
                                          this.VoltageKoefs[i, 0].ToString(format),
                                          this.VoltageKoefs[i, 1].ToString(format));
                        index++;
                    }

                    for (int i = 0; i < this.Discrets.Length; i++)
                    {
                        cgfFile.WriteLine("{0},{1},0", index, StringsConfig.OscDiscretsList[i]);
                        index++;
                    }
                    cgfFile.WriteLine("50");
                    cgfFile.WriteLine("1");
                    cgfFile.WriteLine("1000,{0}", this._count);

                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                    
                    cgfFile.WriteLine("ASCII");
                }

                string datPath = Path.ChangeExtension(filePath, "dat");
                using (StreamWriter datFile = new StreamWriter(datPath))
                {
                    for (int i = 0; i < this._count; i++)
                    {
                        datFile.Write("{0:D6},{1:D6}", i, i*1000);
                        foreach (ushort[] current in this._baseCurrents)
                        {
                            datFile.Write(",{0}", current[i]);
                        }
                        foreach (ushort[] voltage in this._baseVoltages)
                        {
                            datFile.Write(",{0}", voltage[i]);
                        }

                        foreach (ushort[] discret in this.Discrets)
                        {
                            datFile.Write(",{0}", discret[i]);
                        }
                        datFile.WriteLine();
                    }
                }
            }
            catch
            {
                MessageBox.Show(SAVE_OSC_FAIL);
            }
        }

        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);

            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else if (hdrStrings[0].StartsWith("МР741"))
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[1], buff[2]);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ', ',');
                timeString = string.Format("{0} {1}", buff[buff.Length-2], buff[buff.Length-1]);
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            string[] info = cfgStrings[1].Split(',');
            int countDiscrets =
                int.Parse(info.Select(s => s).First(s => s.Contains("D")).Replace("D", string.Empty).Replace(" ", string.Empty));
            double[,] factors = new double[VOLTAGES_COUNT + CURRENTS_COUNT,2];

            Regex factorRegex = new Regex(@"\d+\,[IU]\w+\,[\w]*\,\,[AV]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT + CURRENTS_COUNT; i++)
            {
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                string res = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                factors[i - 2, 0] = double.Parse(res, format);
                res = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                factors[i - 2, 1] = double.Parse(res, format);
            }

            List<string> cfgList = new List<string>(cfgStrings);
            int index = cfgList.IndexOf("ASCII");
            if (index != cfgList.Count-1)
            {
                cfgList.RemoveAll(s => cfgList.IndexOf(s) > index);
                cfgStrings = cfgList.ToArray();
            }
            int counts = int.Parse(cfgStrings[cfgStrings.Length-4].Replace("1000,", string.Empty));
          
            int alarm = hdrStrings[2].Contains("Alarm")
                ? int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty))
                : CalculateAlarm(cfgStrings[cfgStrings.Length - 3], cfgStrings[cfgStrings.Length - 2]);

            CountingList result = new CountingList(counts) {_alarm = alarm};

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);
            
            double[][] currents = new double[CURRENTS_COUNT][];
            double[][] voltages = new double[VOLTAGES_COUNT][];
            ushort[][] discrets = new ushort[countDiscrets][];

            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            
            for (int i = 0; i < counts; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture)*factors[j, 0] + factors[j, 1];
                }

                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    voltages[j][i] = Convert.ToDouble(means[j + 2 + CURRENTS_COUNT], CultureInfo.InvariantCulture) * factors[j + CURRENTS_COUNT, 0] + factors[j + CURRENTS_COUNT, 1];
                }

                for (int j = 0; j < countDiscrets; j++)
                {
                    discrets[j][i] = ushort.Parse(means[j + 2 + CURRENTS_COUNT + VOLTAGES_COUNT]);
                }
            }

            result._maxI = double.MinValue;
            result._minI = double.MaxValue;
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());                
            }

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, voltages[i].Max());
                result._minU = Math.Min(result.MinU, voltages[i].Min());
            }

            result.Currents = currents;
            result.Discrets = discrets;
            result.Voltages = voltages;
            result.FilePath = filePath;
            result.IsLoad = true;
            result._dateTime = timeString;

            return result;
        }

        protected static int CalculateAlarm(string start, string alarm)
        {
            //заглушка для старых версий уникона, где неправильно считало время аварии
            //возвращаем 0, чтобы линия пуска была в 0, на работу осциллограммы не влияет
            if (start == "ERROR!" || alarm == "ERROR!") return 0;  
            List<int> dateTimeStart = start.Split(new[] {'/', ',', '.', ':'}, StringSplitOptions.RemoveEmptyEntries).Select(s => Convert.ToInt32(s)).ToList();
            string[] strAlarm = alarm.Split(new[] {'/', ',', '.', ':'}, StringSplitOptions.RemoveEmptyEntries);
            if (strAlarm[strAlarm.Length-1].Length > 3)
            {
                strAlarm[strAlarm.Length - 1] = strAlarm[strAlarm.Length - 1].Substring(0, 3);
            }
            List<int> dateTimeAlarm = strAlarm.Select(s => Convert.ToInt32(s)).ToList();

            DateTime startTime = new DateTime(1, 1, 1, dateTimeStart[dateTimeStart.Count - 4],
                dateTimeStart[dateTimeStart.Count - 3], dateTimeStart[dateTimeStart.Count - 2],
                dateTimeStart[dateTimeStart.Count - 1]);
            DateTime alarmTime = new DateTime(1, 1, 1, dateTimeAlarm[dateTimeAlarm.Count - 4],
                dateTimeAlarm[dateTimeAlarm.Count - 3], dateTimeAlarm[dateTimeAlarm.Count - 2],
                dateTimeAlarm[dateTimeAlarm.Count - 1]);
            TimeSpan alarmCount = alarmTime - startTime;
            return (int) alarmCount.TotalMilliseconds;
        }

        protected CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new ushort[CURRENTS_COUNT][];

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new ushort[VOLTAGES_COUNT][];

            this._count = count;
        }
    }
}
