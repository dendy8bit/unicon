﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using BEMN.TZL.Configuration.Structers;
using BEMN.TZL.NewOsc.Structers;

namespace BEMN.TZL.NewOsc.HelpClasses
{
    public class OldCountingList : CountingList
    {
        public new const int COUNTING_SIZE = 9;
        public new const int DISCRETS_COUNT = 16;

        private string[] _discretsNames;
 
        public OldCountingList(ushort[] pageValue, OscJurnalStruct oscJournalStruct, MeasureTransStruct measure) : base(oscJournalStruct.Len)
        {
            _oscJournalStruct = oscJournalStruct;
            _dateTime = oscJournalStruct.GetFormattedDateTime;
            _alarm = _oscJournalStruct.Len - _oscJournalStruct.After;
            _countingArray = new ushort[COUNTING_SIZE][];
            //Общее количество отсчётов
            _count = pageValue.Length / COUNTING_SIZE;
            InitCountingArray(pageValue, COUNTING_SIZE);
            InitAnalog(measure);
            InitDiscrets(COUNTING_SIZE-CURRENTS_COUNT-VOLTAGES_COUNT);
            this._discretsNames = new []
            {
                "Д1","Д2","Д3","Д4","Д5","Д6","Д7","Д8",
                "Д9","Д10","Д11","Д12","Д13","Д14","Д15","Д16"
            };
        }

        public override void Save(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException();
            }
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("МР741  {0} {1} ступень - {2}", _oscJournalStruct.GetDate,
                    _oscJournalStruct.GetTime, _oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", _oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", _alarm);
                hdrFile.WriteLine(1251);
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP741,1");
                cgfFile.WriteLine("24,8A,16D");
                int index = 1;
                for (int i = 0; i < Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,A,{2},{3},0,-32768,32767,1,1,P", index, INames[i],
                        CurrentsKoefs[i, 0].ToString(format),
                        CurrentsKoefs[i, 1].ToString(format));
                    index++;
                }

                for (int i = 0; i < Voltages.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,V,{2},{3},0,-32768,32767,1,1,P", index, UNames[i],
                        VoltageKoefs[i, 0].ToString(format),
                        VoltageKoefs[i, 1].ToString(format));
                    index++;
                }

                for (int i = 0; i < Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},{1},0", index, this._discretsNames[i]);
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", _count);

                cgfFile.WriteLine(_oscJournalStruct.GetFormattedDateTimeAlarm(-Alarm));
                cgfFile.WriteLine(_oscJournalStruct.GetFormattedDateTime);
                
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < _count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (ushort[] current in _baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (ushort[] voltage in _baseVoltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    foreach (ushort[] discret in Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }
    }
}
