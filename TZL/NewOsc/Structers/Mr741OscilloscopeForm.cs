using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.TZL.Configuration.Structers;
using BEMN.TZL.NewOsc.HelpClasses;
using BEMN.TZL.NewOsc.Loaders;
using BEMN.TZL.Properties;
using BEMN.MBServer;
using BEMN.TZL.Configuration;

namespace BEMN.TZL.NewOsc.Structers
{
    public partial class Mr741OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly MemoryEntity<MeasureTransStruct> _currentTransform;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private TZL _device;
        private OscJurnalStruct _journalStruct;
        private readonly DataTable _table;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr741OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public Mr741OscilloscopeForm(TZL device)
        {
            this.InitializeComponent();
            this._device = device;
            StringsConfig.CurrentVersion= Common.VersionConverter(this._device.DeviceVersion); 
            //��������� �������
            this._oscJournalLoader = new OscJournalLoader(device);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            //��������� �������
            this._pageLoader = new OscPageLoader(device);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
            //��������� ������� �����
            this._currentTransform = this._device.Transformator;
            this._currentTransform.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            this._currentTransform.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            this._table = this.GetJournalDataTable();
        }

        #endregion [Ctor's]

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
            this.EnableButton = true;
        }
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(BEMN.TZL.TZL); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (BEMN.TZL.TZL); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButton = true;
        }
        
        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            int number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }


        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.RecordNumber == 0)
            {
                this._statusLabel.Text ="������ ����";
            }
            this.EnableButton = true;
        }



        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                if (this._journalStruct.SizeReference == CountingList.COUNTING_SIZE)
                {
                    this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct,
                        this._currentTransform.Value);
                }
                else
                {
                    this.CountingList = new OldCountingList(this._pageLoader.ResultArray, this._journalStruct,
                        this._currentTransform.Value);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
                this.EnableButton = true;
            }
            

            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscReadButton.Enabled = true;

            this.EnableButton = true;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("��741_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[12000], new OscJurnalStruct(), new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��741 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                MessageBox.Show("��� ����������� ������������� ���������� Framework .net 4.0", "��������",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButton = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._currentTransform.LoadStruct();
        }

        private bool EnableButton
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                                this._oscLoadButton.Enabled =
                                    this._oscilloscopeCountCb.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButton = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void MrscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._pageLoader.Close();
            this._oscJournalLoader.Close();
        }
        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
    }
}