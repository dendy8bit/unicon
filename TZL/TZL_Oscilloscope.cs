using System;
using System.Collections;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;


namespace BEMN.TZL
{
    public class TzlOscilloscope : Oscilloscope
    {
        private TZL _device;

        public TzlOscilloscope(Device device)
            : base(device)
        {
            _device = (TZL)device;
        }

        public override void Initialize()
        {
            base.Initialize();

            _analogChannelsCnt = 5;
            _uAnalogChannelsCnt = 5;
            _diskretChannelsCnt = 17;
            _exchangeCnt = 256;
            _oscilloscopeSize = 0x8000;
            _pointCount = 3276;
            _oscJournal = new Device.slot(0x3800, 0x3818);
            _journalLength = 0x18;

            AnalogChannels.Add("Ia");
            AnalogChannels.Add("Ib");
            AnalogChannels.Add("Ic");
            AnalogChannels.Add("In");
            AnalogChannels.Add("����");

            AnalogUChannels.Add("Ua");
            AnalogUChannels.Add("Ub");
            AnalogUChannels.Add("Uc");
            AnalogUChannels.Add("Un");
            AnalogUChannels.Add("����");

            for (int i = 0; i < 17; i++)
            {
                if (i == 16)
                {
                    DiskretChannels.Add("����");
                }
                else
                {
                    DiskretChannels.Add("�" + (i + 1));
                }
            }
        }

        public override void LoadOscilloscopeSettings()
        {
            _device.LoadInputSignals();
            base.LoadOscilloscopeSettings();
        }

        public override void CreateJournal()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            TZL.CAlarmJournal helper = new TZL.CAlarmJournal(_device);
            byte[] buffer = Common.TOBYTES(_journalBuffer, false);
            byte[] buf = Common.TOBYTES(_oscSettings.Value, true);
            _BUF = Common.TOBYTES(_oscSettings.Value, true);

            if (buf[1] != 0)
            {
                ret.Add("���������", helper.GetMessageOsc(buf[1]));
            }

            #region ����-�����
            string str = "";
            int i = 7;
            int j = 9;
            do
            {
                if (buf[i] > 9)
                {
                    if (i != 3)
                    {
                        str += buf[i] + "-";
                    }
                    else
                    {
                        str += buf[i];
                    }

                }
                else
                {
                    if (i != 3)
                    {
                        str += "0" + buf[i] + "-";
                    }
                    else
                    {
                        str += "0" + buf[i];
                    }
                }
                i -= 2;
            } while (i >= 3);
            str += "   ";

            do
            {
                if (buf[j] > 9)
                {
                    if (j != 15)
                    {
                        str += buf[j] + ":";
                    }
                    else
                    {
                        str += buf[j];
                    }

                }
                else
                {
                    if (j != 15)
                    {
                        str += "0" + buf[j] + ":";
                    }
                    else
                    {
                        str += "0" + buf[j];
                    }
                }
                j += 2;
            } while (j <= 15);
            #endregion

            DateTime = str;
            DeviceKtt = _device.TT.ToString();
            DeviceKttnp = _device.TTNP.ToString();
            DeviceKtn = _device.TN.ToString();
            DeviceKtnnp = _device.TNNP.ToString();
            ret.Add("����-�����", str);
            ret.Add("������_�������������", OscilloscopeSize.ToString());
            ret.Add("������", KoefAvary.ToString());
            ret.Add("����_�������", KoefZashiti.ToString());
            ret.Add("TT", _device.TT.ToString());
            ret.Add("TT��", _device.TTNP.ToString());
            ret.Add("T�", _device.TN.ToString());
            ret.Add("T���", _device.TNNP.ToString());
            _journal = ret;

        }

        protected override void PrepareData(byte[] data)
        {
            if (Device.DeviceType.ToString() == "")
            {
                Dev = "1";
            }
            else
            {
                Dev = Device.DeviceType.ToString();
            }
            if (_journal.ContainsKey("TT"))
            {
                _device.TT = ushort.Parse(_journal["TT"]);
            }
            if (_journal.ContainsKey("TT��"))
            {
                _device.TTNP = ushort.Parse(_journal["TT��"]);
            }
            if (_journal.ContainsKey("T�"))
            {
                this._device.TNtoDevice = double.Parse(_journal["T�"]);
            }
            if (_journal.ContainsKey("T���"))
            {
                this._device.TNNPtoDevice = double.Parse(_journal["T���"]);
            }
            if (_journal.ContainsKey("������_�������������"))
            {
                _oscilloscopeSize = ushort.Parse(_journal["������_�������������"]);
                PointCount = _oscilloscopeSize / 18;
            }
            if (_journal.ContainsKey("������"))
            {
                KoefAvary = ushort.Parse(_journal["������"]);
                RezervAvary = ushort.Parse(_journal["������"]);
            }
            if (_journal.ContainsKey("����_�������"))
            {
                KoefZashiti = ushort.Parse(_journal["����_�������"]);
            }
            //NewInitialize();
            DeviceName = Device.DeviceType.ToString();
            
            int _KoefAvary = KoefAvary;
            int _KoefZashiti = KoefZashiti;
            

            double kTT = _device.TT;
            double kTTNP = _device.TTNP;
            KoefI = kTT / kTTNP * 8.0;
            KoefShkaliI = 40 * kTT * 1.41;


            double ukTN = _device.TN;
            double ukTNNP = _device.TNNP;
            KoefU = ukTN / ukTNNP;
            KoefShkaliU = 256 * ukTN * 1.41;

            for (int i = 0; i < 17; i++)
            {
                _diskretData.Add(new BitArray(_pointCount));
            }

            for (int i = 0; i < (PointCount-1); i++)
            {
                int channel = 0;
                int channelCfg = 0;
                int uChannel = 0;
                int cChannel = 0;
                //�������
                double value = BitConverter.ToUInt16(data, i * _KoefZashiti + 0);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = kTT * 40 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * kTT * 40 * Math.Sqrt(2);
                _analogData[channel++][i] = ((value - 32768) / 32768) * 40 * kTT * 1.41;

                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 2);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = kTT * 40 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * kTT * 40 * Math.Sqrt(2);
                _analogData[channel++][i] = ((value - 32768) / 32768) * 40 * kTT * 1.41;

                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 4);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = kTT * 40 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * kTT * 40 * Math.Sqrt(2);
                _analogData[channel++][i] = ((value - 32768) / 32768) * 40 * kTT * 1.41;

                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 6);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = kTTNP * 5 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * kTTNP * 5 * Math.Sqrt(2);
                _analogData[channel][i] = ((value - 32768) / 32768) * 5 * kTTNP * 1.41 * KoefI;
                if (i <= (_KoefAvary / _KoefZashiti))
                {
                    _analogData[4][i] = -KoefShkaliI;
                }
                else
                {
                    _analogData[4][i] = KoefShkaliI;
                }

                //����������
                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 8);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = ukTN * 256 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * ukTN * 256 * Math.Sqrt(2);
                _analogUData[uChannel++][i] = ((value - 32768) / 32768) * 256 * ukTN * 1.41;

                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 10);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = ukTN * 256 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * ukTN * 256 * Math.Sqrt(2);
                _analogUData[uChannel++][i] = ((value - 32768) / 32768) * 256 * ukTN * 1.41;

                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 12);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = ukTN * 256 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * ukTN * 256 * Math.Sqrt(2);
                _analogUData[uChannel++][i] = ((value - 32768) / 32768) * 256 * ukTN * 1.41;

                value = BitConverter.ToUInt16(data, i * _KoefZashiti + 14);
                ComTradeData[i][cChannel++] = value;
                ComTradeDataCfg[channelCfg][0] = ukTNNP * 256 * Math.Sqrt(2) / 32768;
                ComTradeDataCfg[channelCfg++][1] = -1 * ukTNNP * 256 * Math.Sqrt(2);
                _analogUData[uChannel][i] = ((value - 32768) / 32768) * 256 * ukTNNP * 1.41 * KoefU;
                if (i <= (_KoefAvary / _KoefZashiti))
                {
                    _analogUData[4][i] = -KoefShkaliU;
                }
                else
                {
                    _analogUData[4][i] = KoefShkaliU;
                }

                //��������
                double high = BitConverter.ToInt16(data, i * KoefZashiti + 16);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j][i] = (0 == ((int)high >> j & 1)) ? false : true;
                    ComTradeData[i][cChannel++] = (0 == ((int)high >> j & 1)) ? 0 : 1;
                }
                double low = BitConverter.ToInt16(data, i * KoefZashiti + 17);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j + 8][i] = (0 == ((int)low >> j & 1)) ? false : true;
                    ComTradeData[i][cChannel++] = (0 == ((int)low >> j & 1)) ? 0 : 1;
                }
                if (i <= (_KoefAvary / _KoefZashiti))
                {
                    _diskretData[16][i] = true;
                }
                else
                {
                    _diskretData[16][i] = false;
                }
            }
        }
    }
}