using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.TZL.Properties;

namespace BEMN.TZL
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        private TZL _device;
        private DataTable _table;
        private DataTable _tableOld;
        double _vers;
        private bool _isTableOld;

        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(TZL device)
        {
            this.InitializeComponent();
            this._device = device;
            this._vers = Common.VersionConverter(this._device.DeviceVersion);
            this._table = this.MakeTable();
            this._journalGrid.DataSource = this._table;
            this._journalProgress.Maximum = TZL.ALARMJOURNAL_RECORD_CNT;
            this._device.AlarmJournalLoadOk += HandlerHelper.CreateHandler(this, this.OnAlarmJournalLoadOk);
            this._device.AlarmJournalRecordLoadOk += this._device_AlarmJournalRecordLoadOk;
            this._device.AlarmJournalRecordLoadFail += this._device_AlarmJournalRecordLoadFail;
        }
        
        private void OnAlarmJournalLoadOk()
        {
            this._readBut.Enabled = true;
            _deserializeBut.Enabled = true;
            this._journalProgress.Value = TZL.ALARMJOURNAL_RECORD_CNT;
            if (this._journalGrid.Rows.Count == 0) 
            {
                MessageBox.Show("������ ����.");
                EnableButtons = false;
                return;
            }

            EnableButtons = true;
        }

        private void _device_AlarmJournalRecordLoadFail(object sender, int index)
        {
            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            _readBut.Enabled = true;
            _deserializeBut.Enabled = true;
        }

        void _device_AlarmJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnAlarmJournalRecordLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnAlarmJournalRecordLoadOk(int i)
        {
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            try
            {
                this._table.Rows.Add(i + 1, 
                    this._device.AlarmJournal[i].time,
                    this._device.AlarmJournal[i].msg,
                    this._device.AlarmJournal[i].code,
                    this._device.AlarmJournal[i].type_value,
                    this._device.AlarmJournal[i].Ia, 
                    this._device.AlarmJournal[i].Ib, 
                    this._device.AlarmJournal[i].Ic,
                    this._device.AlarmJournal[i].I0, 
                    this._device.AlarmJournal[i].I1, 
                    this._device.AlarmJournal[i].I2,
                    this._device.AlarmJournal[i].Ig, 
                    this._device.AlarmJournal[i].In, 
                    this._device.AlarmJournal[i].F,
                    this._device.AlarmJournal[i].Uab,
                    this._device.AlarmJournal[i].Ubc,
                    this._device.AlarmJournal[i].Uca,
                    this._device.AlarmJournal[i].U0,
                    this._device.AlarmJournal[i].U1, 
                    this._device.AlarmJournal[i].U2,
                    this._device.AlarmJournal[i].Un, 
                    this._device.AlarmJournal[i].inSignals1,
                    this._device.AlarmJournal[i].inSignals2);
                    
            }
            catch
            {
                this._table.Rows.Add(i + 1, "������!");
            }
        }

        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveAlarmJournal();
        }
        
        private void _serializeBut_Click(object sender, EventArgs e)
        {
                if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
                {
                    this._table.WriteXml(this._saveSysJournalDlg.FileName);
                    this._journalCntLabel.Text = "������ ��������";
                }
        }

        private DataTable MakeOldTable()
        {
            DataTable table = new DataTable("��700_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig");
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.�������_1-8");
            table.Columns.Add("��.�������_9-16");
            return table;
        }

        private DataTable MakeTable()
        {
            DataTable table = new DataTable("��741_������_������");
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].HeaderText);
            }
            return table;
        }
        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._table.Rows.Clear();
                try
                {
                    XmlDocument xd = new XmlDocument();
                    xd.Load(this._openSysJounralDlg.FileName);

                    XmlElement xRoot = xd.DocumentElement;

                    if (xRoot.FirstChild.Name != this._table.TableName)
                    {
                        this._isTableOld = true;
                        this._tableOld = this.MakeOldTable();
                        this._journalGrid.DataSource = this._tableOld;
                        this._tableOld.ReadXml(this._openSysJounralDlg.FileName);
                        this._journalCntLabel.Text = $"������� � �������: {this._tableOld.Rows.Count}";
                    }
                    else
                    {
                        this._isTableOld = false;
                        this._journalGrid.DataSource = this._table;
                        this._table.ReadXml(this._openSysJounralDlg.FileName);
                        this._journalCntLabel.Text = $"������� � �������: {this._table.Rows.Count}";
                        EnableButtons = true;
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("���������� ��������� ������ ������.",
                        "��������!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    this._journalCntLabel.Text = "���������� ��������� ������ ������";
                }
                
            }
            this._journalGrid.Update();
        }

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        private void LoadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                EnableButtons = false;
                return;
            }
            this._journalGrid.DataSource = this._isTableOld ? this._tableOld : this._table;
            this._device.AlarmJournal = new TZL.CAlarmJournal(this._device);
            this._device.LoadInputSignals();
            this._readBut.Enabled = false;
            EnableButtons = false;
            _deserializeBut.Enabled = false;
            this._device.RemoveAlarmJournal();
            this._journalProgress.Value = 0;
            this._table.Rows.Clear();
            this._device.LoadAlarmJournal();
        }

        private bool EnableButtons
        {
            set
            {
                _exportButton.Enabled = value;
                _serializeBut.Enabled = value;
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                if (this._isTableOld)
                {
                    HtmlExport.Export(this._tableOld, this._saveJournalHtmlDialog.FileName, Resources.MR741AJOld);
                }
                else
                {
                    HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR741AJ);
                }
                this._journalCntLabel.Text = "������ ��������";
            }
        }

         #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(TZL); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.ja;
            }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
