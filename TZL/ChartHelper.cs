using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using BEMN_XY_Chart;

namespace BEMN.TZL
{
    public class ChartHelper
    {


        private int _density = 1;
        private double ky = 1;
        private double kx = 1;
        private double oy = 0;
        private double ox = 0;
        private double _Ymax;
        private double _Ymin;
        private bool _isAnalog;
        public bool MagnifTool = true;
        private double _xstep;
        private double _ystep;

        private DAS_Net_XYChart _chart;
        private Int32 _xmax;
        private List<double[]> _data = new List<double[]>();
        private List<int> _channels;

        public Int32 XMax
        {
            get
            {
                return _xmax;
            }
        }

        public double XStep
        {
            get
            {
                return _xstep;
            }
        }
        public double YStep
        {
            get
            {
                return _ystep;
            }
        }

        public List<int> Channles
        {
            get { return _channels; }
            set { _channels = value; }
        }

        private StringCollection _channelNames;

        public int Density
        {
            get
            {
                return _density;
            }
            set
            {
                _density = value;
            }
        }

        private double _koeffDomnogeniya;

        public double KoefDomnogeniya
        {
            get { return _koeffDomnogeniya; }
        }

        private double _avary;

        public double Avary
        {
            get { return _avary; }
            set { _avary = value; }
        }

        private double _koef;

        public double _Koef
        {
            get { return _koef; }
            set { _koef = value; }
        }

        public ChartHelper(DAS_Net_XYChart chart, Int32 xmax, List<double[]> data,
                           StringCollection channelNames, bool isAnanlog, double Koef, double KoefAvary)
        {
            _chart = chart;
            _xmax = xmax;
            _data = data;
            _channelNames = channelNames;
            _isAnalog = isAnanlog;
            _avary = KoefAvary;
            _Koef = Koef;

            if (_chart.Name.ToString() == "_diskretChart")
            {
                _Ymax = 100;
                _Ymin = GetLimitValue(false);
            }
            else
            {
                _Ymax = /*GetLimitValue(true)*/ +Koef;
                _Ymin = /*GetLimitValue(false)*/ -Koef;
            }
            _xstep = Math.Abs(xmax / 100);
            _ystep = (Math.Abs(Y_MAX) + Math.Abs(Y_MIN)) / 100;
            _koeffDomnogeniya = Koef / GetLimitValue(true);
        }

        private void InitChannels()
        {
            _channels = new List<int>(_channelNames.Count);
            for (int i = 0; i < _channels.Capacity; i++)
            {
                _channels.Add(i);
            }
        }

        public double KX
        {
            get { return kx; }
            set
            {
                kx = value;

                if (kx < 1)
                {
                    kx = 1;
                }
                if (kx == 1)
                {
                    ox = 0;
                }

            }
        }

        public double KY
        {
            get { return ky; }
            set
            {

                ky = value;
                if (ky < 1)
                {
                    ky = 1;
                }
                if (ky == 1)
                {
                    oy = _Ymin;
                }
            }
        }

        public double OX
        {
            get { return ox; }
            set
            {
                ox = value;
                if (ox < 0)
                {
                    ox = 0;
                }
                if (ox > (_xmax - _xmax / kx))
                {
                    ox = _xmax - _xmax / kx;
                }
            }
        }

        public double OY
        {
            get { return oy; }
            set
            {
                oy = value;
                if (oy > _Ymax)
                {
                    oy = (int)_Ymax;
                }
                if (oy < _Ymin)
                {
                    oy = (int)_Ymin;
                }
            }
        }

        public double Y_MAX
        {
            get
            {
                return _Ymax;
            }
        }

        public double Y_MIN
        {
            get
            {
                return _Ymin;
            }
        }

        private double GetLimitValue(bool max)
        {
            double limit = max ? Double.MinValue : Double.MaxValue;

            for (int i = 0; i < _channelNames.Count - 1; i++)
            {
                for (int j = 0; j < _data[i].Length - 1; j++)
                {
                    if (max)
                    {
                        limit = _data[i][j] > limit ? _data[i][j] : limit;
                    }
                    else
                    {
                        limit = _data[i][j] < limit ? _data[i][j] : limit;
                    }
                }
            }

            return limit;
        }

        public static Color GetColor(int i)
        {
            Color color = new Color();
            switch (i)
            {
                case 0:
                    color = Color.Yellow;
                    break;
                case 1:
                    color = Color.Green;
                    break;
                case 2:
                    color = Color.Red;
                    break;
                case 3:
                    color = Color.Blue;
                    break;
                case 4:
                    color = Color.LightPink;
                    break;
                case 5:
                    color = Color.LightGoldenrodYellow;
                    break;
                case 6:
                    color = Color.LightGreen;
                    break;
                case 7:
                    color = Color.LightBlue;
                    break;
                case 8:
                    color = Color.Indigo;
                    break;
                case 9:
                    color = Color.DarkViolet;
                    break;
                case 10:
                    color = Color.DarkOrange;
                    break;
                case 11:
                    color = Color.DarkBlue;
                    break;
                case 12:
                    color = Color.DarkGreen;
                    break;
                case 13:
                    color = Color.DarkGray;
                    break;
                case 14:
                    color = Color.DarkViolet;
                    break;
                case 15:
                    color = Color.DarkRed;
                    break;
                case 16:
                    color = Color.DarkTurquoise;
                    break;
                case 22:
                    color = Color.Black;
                    break;
                case 23:
                    color = Color.Gray;
                    break;
                default:
                    break;
            }
            return color;
        }

        public DAS_Net_XYChart.DAS_XYCurveVariable CreateCurveItem(int i, int color)
        {
            DAS_Net_XYChart.DAS_XYCurveVariable CurveItem = new DAS_Net_XYChart.DAS_XYCurveVariable();

            CurveItem.cColor = GetColor(color);
            CurveItem.strCurveName = _channelNames[i];
            CurveItem.bLineVisible = true;
            CurveItem.iPointNumber = _xmax;
            CurveItem.iPointSize = 5;
            CurveItem.iLineWidth = 2;
            CurveItem.ePointStyle = DAS_CurvePointStyle.BPTS_NONE;
            CurveItem.bVisible = true;
            CurveItem.iCurPriority = 0;

            if (_isAnalog)
            {
                CurveItem.dblMax = 1000.0;
                CurveItem.dblMin = -1000.0;
            }
            else
            {
                CurveItem.dblMax = 100;
                CurveItem.dblMin = 0;
            }


            CurveItem.bYScaleVisible = true;
            CurveItem.bYAtStart = true;
            CurveItem.bAreaMode = false;
            CurveItem.dblAreaBaseValue = 0.0;
            return CurveItem;
        }

        public void CenterToCoords(DAS_Net_XYChart chart, Point p)
        {
            ox = p.X - ((_xmax / kx) / 2);

            oy = (p.Y - (Math.Abs(chart.CoordinateYMax) - Math.Abs(((Math.Abs(chart.CoordinateYMax) + Math.Abs(chart.CoordinateYMin))) / 2)) / ky);
        }

        public virtual void InitChartLimits()
        {
            if (_xmax - ox < _xmax / kx)
            {
                ox = _chart.XMin + _xmax / kx;
            }
            if (ox < 0)
            {
                ox = 0;
            }
            double yMax = _Ymax;

            yMax /= ky;
            yMax += oy;

            double yMin = _Ymin;

            yMin /= ky;
            yMin += oy;

            if (yMax > _Ymax)
            {
                yMax = _Ymax;
                oy = yMax - yMax / ky;
                yMin = oy;
            }

            if (yMin < _Ymin)
            {
                yMin = _Ymin;
                oy = yMin - yMin / ky;
                yMax = oy;
            }

            if (!_isAnalog)
            {
                yMax = 17;
                yMin = 0;
            }
            if (_isAnalog)
            {
                if (1 == KY)
                {
                    yMax = _Ymax;
                    yMin = _Ymin;
                }
            }

            _chart.CoordinateYMin = yMin;
            _chart.CoordinateYMax = yMax;
            _chart.XMin = ox;
            _chart.XMax = kx == 1 ? _xmax : ox + _xmax / kx;

            _chart.CoordinateYOrigin = yMax;
            _chart.CoordinateXOrigin = _chart.XMin;


            if (ox < 0 || 1 == kx)
            {
                ox = 0;
            }
            if (ox + _xmax / kx > _xmax)
            {
                ox = (int)(_xmax / kx / 2);
            }

        }

        public void UpdateChart()
        {
            InitChartLimits();

            for (int i = 0; i < _channelNames.Count; i++)
            {
                string curveName = _channelNames[i];
                if (_channels.Contains(i))
                {
                    ArrayList x = new ArrayList((int)((ox + _xmax / kx + 1) / _density));
                    ArrayList y = new ArrayList(x.Capacity);
                    for (int j = (int)ox; j < ox + _xmax / kx; j += _density)
                    {
                        if ((_data[i][j] > (Y_MIN - 0.01)) && (_data[i][j] < Y_MAX + 0.01))
                        {
                            if (curveName == "������" || curveName == "����")
                            {
                                x.Add(Avary);
                            }
                            else
                            {
                                x.Add(j);
                            }
                            if (_isAnalog)
                            {
                                y.Add(_data[i][j]);
                            }
                            else
                            {

                                if (_data[i][j] == 0)
                                {
                                    if (curveName == "������" || curveName == "����")
                                    {
                                        y.Add(-100);
                                    }
                                    else
                                    {
                                        y.Add(_data[i][j] + i * 1 + 1);
                                    }
                                }
                                else
                                {
                                    if (curveName == "������" || curveName == "����")
                                    {
                                        y.Add(100);
                                    }
                                    else
                                    {
                                        y.Add(_data[i][j] + i * 1 + 0.5);
                                    }

                                }
                            }
                        }
                    }

                    if (!_chart.RefreshCurvePoint(curveName, x, y))
                    {
                        throw new ApplicationException();
                    }
                }
                else
                {
                    _chart.ResetCurve(curveName);
                }
            }


        }


        public void AddPoints()
        {
            InitChannels();

            for (int i = 0; i < _data.Count; i++)
            {
                if (_channels.Contains(i))
                {
                    string curveName = _channelNames[i];

                    for (int j = (int)ox; j < ox + _xmax / kx; j += _density)
                    {
                        if (_isAnalog)
                        {
                            if (curveName == "������" || curveName == "����")
                            {
                                if (!_chart.AddCurvePoint(curveName, Avary, _data[i][j]))
                                {
                                    throw new ApplicationException();
                                }
                            }
                            else
                            {
                                if (!_chart.AddCurvePoint(curveName, j, _data[i][j]))
                                {
                                    throw new ApplicationException();
                                }
                            }
                        }
                        else
                        {
                            if (_data[i][j] == 0)
                            {
                                if (curveName == "������" || curveName == "����")
                                {

                                    if (!_chart.AddCurvePoint(curveName, Avary, -100))
                                    {
                                        throw new ApplicationException();
                                    }
                                }
                                else
                                {

                                    if (!_chart.AddCurvePoint(curveName, j, _data[i][j] + i * 1 + 1))
                                    {
                                        throw new ApplicationException();
                                    }
                                }
                            }
                            else
                            {
                                if (curveName == "������" || curveName == "����")
                                {

                                    if (!_chart.AddCurvePoint(curveName, Avary, 100))
                                    {
                                        throw new ApplicationException();
                                    }
                                }
                                else
                                {

                                    if (!_chart.AddCurvePoint(curveName, j, _data[i][j] + i * 1 + 0.5))
                                    {
                                        throw new ApplicationException();
                                    }
                                }

                            }
                        }

                    }
                }
            }
        }

        public Point GetCoordsAtPoint(Point p)
        {

            Point ret = new Point();
            ret.X = (int)(ox + (p.X * (_xmax / kx)) / (_chart.Width - 3));

            int Yheight = (int)(Math.Abs(_Ymax) + Math.Abs(_Ymin));
            ret.Y = (int)(_chart.CoordinateYMax - (p.Y * Yheight / ky) / _chart.Height);

            return ret;
        }

    }
}