﻿namespace BEMN.MR741
{
    partial class Mr741OscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this._discretsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._discrestsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.button73 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.button76 = new System.Windows.Forms.Button();
            this.button77 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button79 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button81 = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.button85 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button60 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this._discrete5Button = new System.Windows.Forms.Button();
            this._discrete24Button = new System.Windows.Forms.Button();
            this._discrete23Button = new System.Windows.Forms.Button();
            this._discrete22Button = new System.Windows.Forms.Button();
            this._discrete21Button = new System.Windows.Forms.Button();
            this._discrete20Button = new System.Windows.Forms.Button();
            this._discrete19Button = new System.Windows.Forms.Button();
            this._discrete18Button = new System.Windows.Forms.Button();
            this._discrete17Button = new System.Windows.Forms.Button();
            this._discrete16Button = new System.Windows.Forms.Button();
            this._discrete15Button = new System.Windows.Forms.Button();
            this._discrete14Button = new System.Windows.Forms.Button();
            this._discrete13Button = new System.Windows.Forms.Button();
            this._discrete12Button = new System.Windows.Forms.Button();
            this._discrete11Button = new System.Windows.Forms.Button();
            this._discrete10Button = new System.Windows.Forms.Button();
            this._discrete9Button = new System.Windows.Forms.Button();
            this._discrete8Button = new System.Windows.Forms.Button();
            this._discrete7Button = new System.Windows.Forms.Button();
            this._discrete6Button = new System.Windows.Forms.Button();
            this._discrete2Button = new System.Windows.Forms.Button();
            this._discrete3Button = new System.Windows.Forms.Button();
            this._discrete4Button = new System.Windows.Forms.Button();
            this._discrete1Button = new System.Windows.Forms.Button();
            this._discrete25Button = new System.Windows.Forms.Button();
            this._discrete26Button = new System.Windows.Forms.Button();
            this._discrete27Button = new System.Windows.Forms.Button();
            this._discrete28Button = new System.Windows.Forms.Button();
            this._discrete29Button = new System.Windows.Forms.Button();
            this._discrete30Button = new System.Windows.Forms.Button();
            this._discrete31Button = new System.Windows.Forms.Button();
            this._discrete32Button = new System.Windows.Forms.Button();
            this._discrete33Button = new System.Windows.Forms.Button();
            this._discrete34Button = new System.Windows.Forms.Button();
            this._discrete35Button = new System.Windows.Forms.Button();
            this._discrete36Button = new System.Windows.Forms.Button();
            this._discrete37Button = new System.Windows.Forms.Button();
            this._discrete38Button = new System.Windows.Forms.Button();
            this._discrete39Button = new System.Windows.Forms.Button();
            this._discrete40Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this._voltageLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._uScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._u1Button = new System.Windows.Forms.Button();
            this._u4Button = new System.Windows.Forms.Button();
            this._u5Button = new System.Windows.Forms.Button();
            this._u2Button = new System.Windows.Forms.Button();
            this._u3Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._uChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._voltageChartDecreaseButton = new System.Windows.Forms.Button();
            this._voltageChartIncreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this._currentsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._iScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this._i3Button = new System.Windows.Forms.Button();
            this._i4Button = new System.Windows.Forms.Button();
            this._i1Button = new System.Windows.Forms.Button();
            this._i2Button = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this._iChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this._currentChartDecreaseButton = new System.Windows.Forms.Button();
            this._currentChartIncreaseButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this._marker1TrackBar = new System.Windows.Forms.TrackBar();
            this._marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this._measuringChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._markerScrollPanel = new System.Windows.Forms.Panel();
            this._marker2Box = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._marker2D40 = new System.Windows.Forms.Label();
            this._marker2D31 = new System.Windows.Forms.Label();
            this._marker2D30 = new System.Windows.Forms.Label();
            this._marker2D29 = new System.Windows.Forms.Label();
            this._marker2D39 = new System.Windows.Forms.Label();
            this._marker2D38 = new System.Windows.Forms.Label();
            this._marker2D37 = new System.Windows.Forms.Label();
            this._marker2D36 = new System.Windows.Forms.Label();
            this._marker2D35 = new System.Windows.Forms.Label();
            this._marker2D34 = new System.Windows.Forms.Label();
            this._marker2D33 = new System.Windows.Forms.Label();
            this._marker2D32 = new System.Windows.Forms.Label();
            this._marker2D28 = new System.Windows.Forms.Label();
            this._marker2D27 = new System.Windows.Forms.Label();
            this._marker2D26 = new System.Windows.Forms.Label();
            this._marker2D25 = new System.Windows.Forms.Label();
            this._marker2D24 = new System.Windows.Forms.Label();
            this._marker2D23 = new System.Windows.Forms.Label();
            this._marker2D22 = new System.Windows.Forms.Label();
            this._marker2D21 = new System.Windows.Forms.Label();
            this._marker2D20 = new System.Windows.Forms.Label();
            this._marker2D11 = new System.Windows.Forms.Label();
            this._marker2D10 = new System.Windows.Forms.Label();
            this._marker2D9 = new System.Windows.Forms.Label();
            this._marker2D19 = new System.Windows.Forms.Label();
            this._marker2D18 = new System.Windows.Forms.Label();
            this._marker2D17 = new System.Windows.Forms.Label();
            this._marker2D16 = new System.Windows.Forms.Label();
            this._marker2D15 = new System.Windows.Forms.Label();
            this._marker2D14 = new System.Windows.Forms.Label();
            this._marker2D13 = new System.Windows.Forms.Label();
            this._marker2D12 = new System.Windows.Forms.Label();
            this._marker2D8 = new System.Windows.Forms.Label();
            this._marker2D7 = new System.Windows.Forms.Label();
            this._marker2D6 = new System.Windows.Forms.Label();
            this._marker2D5 = new System.Windows.Forms.Label();
            this._marker2D4 = new System.Windows.Forms.Label();
            this._marker2D3 = new System.Windows.Forms.Label();
            this._marker2D2 = new System.Windows.Forms.Label();
            this._marker2D1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._marker2U4 = new System.Windows.Forms.Label();
            this._marker2U3 = new System.Windows.Forms.Label();
            this._marker2U2 = new System.Windows.Forms.Label();
            this._marker2U1 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._marker2I4 = new System.Windows.Forms.Label();
            this._marker2I3 = new System.Windows.Forms.Label();
            this._marker2I2 = new System.Windows.Forms.Label();
            this._marker2I1 = new System.Windows.Forms.Label();
            this._marker1Box = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._marker1U4 = new System.Windows.Forms.Label();
            this._marker1U3 = new System.Windows.Forms.Label();
            this._marker1U2 = new System.Windows.Forms.Label();
            this._marker1U1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this._marker1D40 = new System.Windows.Forms.Label();
            this._marker1D31 = new System.Windows.Forms.Label();
            this._marker1D30 = new System.Windows.Forms.Label();
            this._marker1D29 = new System.Windows.Forms.Label();
            this._marker1D39 = new System.Windows.Forms.Label();
            this._marker1D38 = new System.Windows.Forms.Label();
            this._marker1D37 = new System.Windows.Forms.Label();
            this._marker1D36 = new System.Windows.Forms.Label();
            this._marker1D35 = new System.Windows.Forms.Label();
            this._marker1D34 = new System.Windows.Forms.Label();
            this._marker1D33 = new System.Windows.Forms.Label();
            this._marker1D32 = new System.Windows.Forms.Label();
            this._marker1D28 = new System.Windows.Forms.Label();
            this._marker1D27 = new System.Windows.Forms.Label();
            this._marker1D26 = new System.Windows.Forms.Label();
            this._marker1D25 = new System.Windows.Forms.Label();
            this._marker1D24 = new System.Windows.Forms.Label();
            this._marker1D23 = new System.Windows.Forms.Label();
            this._marker1D22 = new System.Windows.Forms.Label();
            this._marker1D21 = new System.Windows.Forms.Label();
            this._marker1D20 = new System.Windows.Forms.Label();
            this._marker1D11 = new System.Windows.Forms.Label();
            this._marker1D10 = new System.Windows.Forms.Label();
            this._marker1D9 = new System.Windows.Forms.Label();
            this._marker1D19 = new System.Windows.Forms.Label();
            this._marker1D18 = new System.Windows.Forms.Label();
            this._marker1D17 = new System.Windows.Forms.Label();
            this._marker1D16 = new System.Windows.Forms.Label();
            this._marker1D15 = new System.Windows.Forms.Label();
            this._marker1D14 = new System.Windows.Forms.Label();
            this._marker1D13 = new System.Windows.Forms.Label();
            this._marker1D12 = new System.Windows.Forms.Label();
            this._marker1D8 = new System.Windows.Forms.Label();
            this._marker1D7 = new System.Windows.Forms.Label();
            this._marker1D6 = new System.Windows.Forms.Label();
            this._marker1D5 = new System.Windows.Forms.Label();
            this._marker1D4 = new System.Windows.Forms.Label();
            this._marker1D3 = new System.Windows.Forms.Label();
            this._marker1D2 = new System.Windows.Forms.Label();
            this._marker1D1 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._marker1I4 = new System.Windows.Forms.Label();
            this._marker1I3 = new System.Windows.Forms.Label();
            this._marker1I2 = new System.Windows.Forms.Label();
            this._marker1I1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._deltaTimeBox = new System.Windows.Forms.GroupBox();
            this._markerTimeDelta = new System.Windows.Forms.Label();
            this._marker2TimeBox = new System.Windows.Forms.GroupBox();
            this._marker2Time = new System.Windows.Forms.Label();
            this._marker1TimeBox = new System.Windows.Forms.GroupBox();
            this._marker1Time = new System.Windows.Forms.Label();
            this._xDecreaseButton = new System.Windows.Forms.Button();
            this._currentСheckBox = new System.Windows.Forms.CheckBox();
            this._discrestsСheckBox = new System.Windows.Forms.CheckBox();
            this._channelsСheckBox = new System.Windows.Forms.CheckBox();
            this._markCheckBox = new System.Windows.Forms.CheckBox();
            this._markerCheckBox = new System.Windows.Forms.CheckBox();
            this._newMarkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._xIncreaseButton = new System.Windows.Forms.Button();
            this._oscRunСheckBox = new System.Windows.Forms.CheckBox();
            this._voltageСheckBox = new System.Windows.Forms.CheckBox();
            this.button89 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.button91 = new System.Windows.Forms.Button();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this._discretsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this._voltageLayoutPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this._currentsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._markerScrollPanel.SuspendLayout();
            this._marker2Box.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this._marker1Box.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._deltaTimeBox.SuspendLayout();
            this._marker2TimeBox.SuspendLayout();
            this._marker1TimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1364, 568);
            this.MAINTABLE.TabIndex = 1;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 548);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1064, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1364, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1058, 517);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1058, 427);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this._discretsLayoutPanel);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this._voltageLayoutPanel);
            this.panel2.Controls.Add(this.splitter3);
            this.panel2.Controls.Add(this._currentsLayoutPanel);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1038, 3996);
            this.panel2.TabIndex = 0;
            // 
            // _discretsLayoutPanel
            // 
            this._discretsLayoutPanel.ColumnCount = 3;
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._discretsLayoutPanel.Controls.Add(this._discrestsChart, 1, 1);
            this._discretsLayoutPanel.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this._discretsLayoutPanel.Controls.Add(this.label2, 1, 0);
            this._discretsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._discretsLayoutPanel.Location = new System.Drawing.Point(0, 1082);
            this._discretsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._discretsLayoutPanel.Name = "_discretsLayoutPanel";
            this._discretsLayoutPanel.RowCount = 2;
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.Size = new System.Drawing.Size(1038, 2735);
            this._discretsLayoutPanel.TabIndex = 29;
            // 
            // _discrestsChart
            // 
            this._discrestsChart.BkGradient = false;
            this._discrestsChart.BkGradientAngle = 90;
            this._discrestsChart.BkGradientColor = System.Drawing.Color.White;
            this._discrestsChart.BkGradientRate = 0.5F;
            this._discrestsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discrestsChart.BkRestrictedToChartPanel = false;
            this._discrestsChart.BkShinePosition = 1F;
            this._discrestsChart.BkTransparency = 0F;
            this._discrestsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discrestsChart.BorderExteriorLength = 0;
            this._discrestsChart.BorderGradientAngle = 225;
            this._discrestsChart.BorderGradientLightPos1 = 1F;
            this._discrestsChart.BorderGradientLightPos2 = -1F;
            this._discrestsChart.BorderGradientRate = 0.5F;
            this._discrestsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discrestsChart.BorderLightIntermediateBrightness = 0F;
            this._discrestsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discrestsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discrestsChart.ChartPanelBkTransparency = 0F;
            this._discrestsChart.ControlShadow = false;
            this._discrestsChart.CoordinateAxesVisible = true;
            this._discrestsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._discrestsChart.CoordinateXOrigin = 0D;
            this._discrestsChart.CoordinateYMax = 100D;
            this._discrestsChart.CoordinateYMin = -100D;
            this._discrestsChart.CoordinateYOrigin = 0D;
            this._discrestsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrestsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discrestsChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._discrestsChart.FooterColor = System.Drawing.Color.Black;
            this._discrestsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discrestsChart.FooterVisible = false;
            this._discrestsChart.GridColor = System.Drawing.Color.MistyRose;
            this._discrestsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discrestsChart.GridVisible = true;
            this._discrestsChart.GridXSubTicker = 0;
            this._discrestsChart.GridXTicker = 10;
            this._discrestsChart.GridYSubTicker = 0;
            this._discrestsChart.GridYTicker = 2;
            this._discrestsChart.HeaderColor = System.Drawing.Color.Black;
            this._discrestsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discrestsChart.HeaderVisible = false;
            this._discrestsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.InnerBorderLength = 0;
            this._discrestsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.LegendBkColor = System.Drawing.Color.White;
            this._discrestsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discrestsChart.LegendVisible = false;
            this._discrestsChart.Location = new System.Drawing.Point(113, 26);
            this._discrestsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discrestsChart.MiddleBorderLength = 0;
            this._discrestsChart.Name = "_discrestsChart";
            this._discrestsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.OuterBorderLength = 0;
            this._discrestsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.Precision = 0;
            this._discrestsChart.RoundRadius = 10;
            this._discrestsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discrestsChart.ShadowDepth = 8;
            this._discrestsChart.ShadowRate = 0.5F;
            this._discrestsChart.Size = new System.Drawing.Size(902, 2706);
            this._discrestsChart.TabIndex = 29;
            this._discrestsChart.Text = "daS_Net_XYChart2";
            this._discrestsChart.XMax = 100D;
            this._discrestsChart.XMin = 0D;
            this._discrestsChart.XScaleColor = System.Drawing.Color.White;
            this._discrestsChart.XScaleVisible = false;
            this._discrestsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.button91, 0, 127);
            this.tableLayoutPanel5.Controls.Add(this.button90, 0, 126);
            this.tableLayoutPanel5.Controls.Add(this.button73, 0, 109);
            this.tableLayoutPanel5.Controls.Add(this.button74, 0, 110);
            this.tableLayoutPanel5.Controls.Add(this.button75, 0, 111);
            this.tableLayoutPanel5.Controls.Add(this.button76, 0, 112);
            this.tableLayoutPanel5.Controls.Add(this.button77, 0, 113);
            this.tableLayoutPanel5.Controls.Add(this.button78, 0, 114);
            this.tableLayoutPanel5.Controls.Add(this.button79, 0, 115);
            this.tableLayoutPanel5.Controls.Add(this.button80, 0, 116);
            this.tableLayoutPanel5.Controls.Add(this.button81, 0, 117);
            this.tableLayoutPanel5.Controls.Add(this.button82, 0, 118);
            this.tableLayoutPanel5.Controls.Add(this.button83, 0, 119);
            this.tableLayoutPanel5.Controls.Add(this.button84, 0, 120);
            this.tableLayoutPanel5.Controls.Add(this.button85, 0, 121);
            this.tableLayoutPanel5.Controls.Add(this.button86, 0, 122);
            this.tableLayoutPanel5.Controls.Add(this.button87, 0, 123);
            this.tableLayoutPanel5.Controls.Add(this.button88, 0, 124);
            this.tableLayoutPanel5.Controls.Add(this.button89, 0, 125);
            this.tableLayoutPanel5.Controls.Add(this.button55, 0, 91);
            this.tableLayoutPanel5.Controls.Add(this.button56, 0, 92);
            this.tableLayoutPanel5.Controls.Add(this.button57, 0, 93);
            this.tableLayoutPanel5.Controls.Add(this.button58, 0, 94);
            this.tableLayoutPanel5.Controls.Add(this.button59, 0, 95);
            this.tableLayoutPanel5.Controls.Add(this.button60, 0, 96);
            this.tableLayoutPanel5.Controls.Add(this.button61, 0, 97);
            this.tableLayoutPanel5.Controls.Add(this.button62, 0, 98);
            this.tableLayoutPanel5.Controls.Add(this.button63, 0, 99);
            this.tableLayoutPanel5.Controls.Add(this.button64, 0, 100);
            this.tableLayoutPanel5.Controls.Add(this.button65, 0, 101);
            this.tableLayoutPanel5.Controls.Add(this.button66, 0, 102);
            this.tableLayoutPanel5.Controls.Add(this.button67, 0, 103);
            this.tableLayoutPanel5.Controls.Add(this.button68, 0, 104);
            this.tableLayoutPanel5.Controls.Add(this.button69, 0, 105);
            this.tableLayoutPanel5.Controls.Add(this.button70, 0, 106);
            this.tableLayoutPanel5.Controls.Add(this.button71, 0, 107);
            this.tableLayoutPanel5.Controls.Add(this.button72, 0, 108);
            this.tableLayoutPanel5.Controls.Add(this.button45, 0, 81);
            this.tableLayoutPanel5.Controls.Add(this.button46, 0, 82);
            this.tableLayoutPanel5.Controls.Add(this.button47, 0, 83);
            this.tableLayoutPanel5.Controls.Add(this.button48, 0, 84);
            this.tableLayoutPanel5.Controls.Add(this.button49, 0, 85);
            this.tableLayoutPanel5.Controls.Add(this.button50, 0, 86);
            this.tableLayoutPanel5.Controls.Add(this.button51, 0, 87);
            this.tableLayoutPanel5.Controls.Add(this.button52, 0, 88);
            this.tableLayoutPanel5.Controls.Add(this.button53, 0, 89);
            this.tableLayoutPanel5.Controls.Add(this.button54, 0, 90);
            this.tableLayoutPanel5.Controls.Add(this.button34, 0, 70);
            this.tableLayoutPanel5.Controls.Add(this.button35, 0, 71);
            this.tableLayoutPanel5.Controls.Add(this.button36, 0, 72);
            this.tableLayoutPanel5.Controls.Add(this.button37, 0, 73);
            this.tableLayoutPanel5.Controls.Add(this.button38, 0, 74);
            this.tableLayoutPanel5.Controls.Add(this.button39, 0, 75);
            this.tableLayoutPanel5.Controls.Add(this.button40, 0, 76);
            this.tableLayoutPanel5.Controls.Add(this.button41, 0, 77);
            this.tableLayoutPanel5.Controls.Add(this.button42, 0, 78);
            this.tableLayoutPanel5.Controls.Add(this.button43, 0, 79);
            this.tableLayoutPanel5.Controls.Add(this.button44, 0, 80);
            this.tableLayoutPanel5.Controls.Add(this.button28, 0, 64);
            this.tableLayoutPanel5.Controls.Add(this.button29, 0, 65);
            this.tableLayoutPanel5.Controls.Add(this.button30, 0, 66);
            this.tableLayoutPanel5.Controls.Add(this.button31, 0, 67);
            this.tableLayoutPanel5.Controls.Add(this.button32, 0, 68);
            this.tableLayoutPanel5.Controls.Add(this.button33, 0, 69);
            this.tableLayoutPanel5.Controls.Add(this.button24, 0, 60);
            this.tableLayoutPanel5.Controls.Add(this.button26, 0, 62);
            this.tableLayoutPanel5.Controls.Add(this.button27, 0, 63);
            this.tableLayoutPanel5.Controls.Add(this.button16, 0, 55);
            this.tableLayoutPanel5.Controls.Add(this.button17, 0, 56);
            this.tableLayoutPanel5.Controls.Add(this.button18, 0, 57);
            this.tableLayoutPanel5.Controls.Add(this.button19, 0, 58);
            this.tableLayoutPanel5.Controls.Add(this.button20, 0, 59);
            this.tableLayoutPanel5.Controls.Add(this.button9, 0, 48);
            this.tableLayoutPanel5.Controls.Add(this.button10, 0, 49);
            this.tableLayoutPanel5.Controls.Add(this.button11, 0, 50);
            this.tableLayoutPanel5.Controls.Add(this.button12, 0, 51);
            this.tableLayoutPanel5.Controls.Add(this.button13, 0, 52);
            this.tableLayoutPanel5.Controls.Add(this.button14, 0, 53);
            this.tableLayoutPanel5.Controls.Add(this.button15, 0, 54);
            this.tableLayoutPanel5.Controls.Add(this.button1, 0, 40);
            this.tableLayoutPanel5.Controls.Add(this.button2, 0, 41);
            this.tableLayoutPanel5.Controls.Add(this.button3, 0, 42);
            this.tableLayoutPanel5.Controls.Add(this.button4, 0, 43);
            this.tableLayoutPanel5.Controls.Add(this.button5, 0, 44);
            this.tableLayoutPanel5.Controls.Add(this.button6, 0, 45);
            this.tableLayoutPanel5.Controls.Add(this.button7, 0, 46);
            this.tableLayoutPanel5.Controls.Add(this.button8, 0, 47);
            this.tableLayoutPanel5.Controls.Add(this._discrete5Button, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this._discrete24Button, 0, 23);
            this.tableLayoutPanel5.Controls.Add(this._discrete23Button, 0, 22);
            this.tableLayoutPanel5.Controls.Add(this._discrete22Button, 0, 21);
            this.tableLayoutPanel5.Controls.Add(this._discrete21Button, 0, 20);
            this.tableLayoutPanel5.Controls.Add(this._discrete20Button, 0, 19);
            this.tableLayoutPanel5.Controls.Add(this._discrete19Button, 0, 18);
            this.tableLayoutPanel5.Controls.Add(this._discrete18Button, 0, 17);
            this.tableLayoutPanel5.Controls.Add(this._discrete17Button, 0, 16);
            this.tableLayoutPanel5.Controls.Add(this._discrete16Button, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this._discrete15Button, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this._discrete14Button, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this._discrete13Button, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this._discrete12Button, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this._discrete11Button, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this._discrete10Button, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this._discrete9Button, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this._discrete8Button, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this._discrete7Button, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this._discrete6Button, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this._discrete2Button, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this._discrete3Button, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this._discrete4Button, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this._discrete1Button, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this._discrete25Button, 0, 24);
            this.tableLayoutPanel5.Controls.Add(this._discrete26Button, 0, 25);
            this.tableLayoutPanel5.Controls.Add(this._discrete27Button, 0, 26);
            this.tableLayoutPanel5.Controls.Add(this._discrete28Button, 0, 27);
            this.tableLayoutPanel5.Controls.Add(this._discrete29Button, 0, 28);
            this.tableLayoutPanel5.Controls.Add(this._discrete30Button, 0, 29);
            this.tableLayoutPanel5.Controls.Add(this._discrete31Button, 0, 30);
            this.tableLayoutPanel5.Controls.Add(this._discrete32Button, 0, 31);
            this.tableLayoutPanel5.Controls.Add(this._discrete33Button, 0, 32);
            this.tableLayoutPanel5.Controls.Add(this._discrete34Button, 0, 33);
            this.tableLayoutPanel5.Controls.Add(this._discrete35Button, 0, 34);
            this.tableLayoutPanel5.Controls.Add(this._discrete36Button, 0, 35);
            this.tableLayoutPanel5.Controls.Add(this._discrete37Button, 0, 36);
            this.tableLayoutPanel5.Controls.Add(this._discrete38Button, 0, 37);
            this.tableLayoutPanel5.Controls.Add(this._discrete39Button, 0, 38);
            this.tableLayoutPanel5.Controls.Add(this._discrete40Button, 0, 39);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 129;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(104, 2706);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // button73
            // 
            this.button73.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button73.BackColor = System.Drawing.Color.LightSlateGray;
            this.button73.ForeColor = System.Drawing.Color.Black;
            this.button73.Location = new System.Drawing.Point(0, 2244);
            this.button73.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(101, 20);
            this.button73.TabIndex = 142;
            this.button73.Text = "ССЛ7";
            this.button73.UseVisualStyleBackColor = false;
            this.button73.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button74
            // 
            this.button74.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button74.BackColor = System.Drawing.Color.LightSlateGray;
            this.button74.ForeColor = System.Drawing.Color.Black;
            this.button74.Location = new System.Drawing.Point(0, 2264);
            this.button74.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(101, 20);
            this.button74.TabIndex = 143;
            this.button74.Text = "ССЛ8";
            this.button74.UseVisualStyleBackColor = false;
            this.button74.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button75
            // 
            this.button75.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button75.BackColor = System.Drawing.Color.LightSlateGray;
            this.button75.ForeColor = System.Drawing.Color.Black;
            this.button75.Location = new System.Drawing.Point(0, 2284);
            this.button75.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(101, 20);
            this.button75.TabIndex = 144;
            this.button75.Text = "ССЛ9";
            this.button75.UseVisualStyleBackColor = false;
            this.button75.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button76
            // 
            this.button76.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button76.BackColor = System.Drawing.Color.LightSlateGray;
            this.button76.ForeColor = System.Drawing.Color.Black;
            this.button76.Location = new System.Drawing.Point(0, 2304);
            this.button76.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(101, 20);
            this.button76.TabIndex = 145;
            this.button76.Text = "ССЛ10";
            this.button76.UseVisualStyleBackColor = false;
            this.button76.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button77
            // 
            this.button77.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button77.BackColor = System.Drawing.Color.LightSlateGray;
            this.button77.ForeColor = System.Drawing.Color.Black;
            this.button77.Location = new System.Drawing.Point(0, 2324);
            this.button77.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(101, 20);
            this.button77.TabIndex = 146;
            this.button77.Text = "ССЛ11";
            this.button77.UseVisualStyleBackColor = false;
            this.button77.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button78
            // 
            this.button78.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button78.BackColor = System.Drawing.Color.LightSlateGray;
            this.button78.ForeColor = System.Drawing.Color.Black;
            this.button78.Location = new System.Drawing.Point(0, 2344);
            this.button78.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(101, 20);
            this.button78.TabIndex = 137;
            this.button78.Text = "ССЛ12";
            this.button78.UseVisualStyleBackColor = false;
            this.button78.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button79
            // 
            this.button79.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button79.BackColor = System.Drawing.Color.LightSlateGray;
            this.button79.ForeColor = System.Drawing.Color.Black;
            this.button79.Location = new System.Drawing.Point(0, 2364);
            this.button79.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(101, 20);
            this.button79.TabIndex = 138;
            this.button79.Text = "ССЛ13";
            this.button79.UseVisualStyleBackColor = false;
            this.button79.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button80
            // 
            this.button80.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button80.BackColor = System.Drawing.Color.LightSlateGray;
            this.button80.ForeColor = System.Drawing.Color.Black;
            this.button80.Location = new System.Drawing.Point(0, 2384);
            this.button80.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(101, 20);
            this.button80.TabIndex = 139;
            this.button80.Text = "ССЛ14";
            this.button80.UseVisualStyleBackColor = false;
            this.button80.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button81
            // 
            this.button81.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button81.BackColor = System.Drawing.Color.LightSlateGray;
            this.button81.ForeColor = System.Drawing.Color.Black;
            this.button81.Location = new System.Drawing.Point(0, 2404);
            this.button81.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(101, 20);
            this.button81.TabIndex = 140;
            this.button81.Text = "ССЛ15";
            this.button81.UseVisualStyleBackColor = false;
            this.button81.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button82
            // 
            this.button82.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button82.BackColor = System.Drawing.Color.LightSlateGray;
            this.button82.ForeColor = System.Drawing.Color.Black;
            this.button82.Location = new System.Drawing.Point(0, 2424);
            this.button82.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(101, 20);
            this.button82.TabIndex = 141;
            this.button82.Text = "ССЛ16";
            this.button82.UseVisualStyleBackColor = false;
            this.button82.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button83
            // 
            this.button83.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button83.BackColor = System.Drawing.Color.LightSlateGray;
            this.button83.ForeColor = System.Drawing.Color.Black;
            this.button83.Location = new System.Drawing.Point(0, 2444);
            this.button83.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(101, 20);
            this.button83.TabIndex = 135;
            this.button83.Text = "ССЛ17";
            this.button83.UseVisualStyleBackColor = false;
            this.button83.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button84
            // 
            this.button84.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button84.BackColor = System.Drawing.Color.LightSlateGray;
            this.button84.ForeColor = System.Drawing.Color.Black;
            this.button84.Location = new System.Drawing.Point(0, 2464);
            this.button84.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(101, 20);
            this.button84.TabIndex = 136;
            this.button84.Text = "ССЛ18";
            this.button84.UseVisualStyleBackColor = false;
            this.button84.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button85
            // 
            this.button85.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button85.BackColor = System.Drawing.Color.LightSlateGray;
            this.button85.ForeColor = System.Drawing.Color.Black;
            this.button85.Location = new System.Drawing.Point(0, 2484);
            this.button85.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(101, 20);
            this.button85.TabIndex = 129;
            this.button85.Text = "ССЛ19";
            this.button85.UseVisualStyleBackColor = false;
            this.button85.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button86
            // 
            this.button86.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button86.BackColor = System.Drawing.Color.LightSlateGray;
            this.button86.ForeColor = System.Drawing.Color.Black;
            this.button86.Location = new System.Drawing.Point(0, 2504);
            this.button86.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(101, 20);
            this.button86.TabIndex = 130;
            this.button86.Text = "ССЛ20";
            this.button86.UseVisualStyleBackColor = false;
            this.button86.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button87
            // 
            this.button87.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button87.BackColor = System.Drawing.Color.LightSlateGray;
            this.button87.ForeColor = System.Drawing.Color.Black;
            this.button87.Location = new System.Drawing.Point(0, 2524);
            this.button87.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(101, 20);
            this.button87.TabIndex = 131;
            this.button87.Text = "ССЛ21";
            this.button87.UseVisualStyleBackColor = false;
            this.button87.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button88
            // 
            this.button88.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button88.BackColor = System.Drawing.Color.LightSlateGray;
            this.button88.ForeColor = System.Drawing.Color.Black;
            this.button88.Location = new System.Drawing.Point(0, 2544);
            this.button88.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(101, 20);
            this.button88.TabIndex = 132;
            this.button88.Text = "ССЛ21";
            this.button88.UseVisualStyleBackColor = false;
            this.button88.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button55
            // 
            this.button55.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button55.BackColor = System.Drawing.Color.LightSlateGray;
            this.button55.ForeColor = System.Drawing.Color.Black;
            this.button55.Location = new System.Drawing.Point(0, 1884);
            this.button55.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(101, 20);
            this.button55.TabIndex = 124;
            this.button55.Text = "U0> ИО";
            this.button55.UseVisualStyleBackColor = false;
            this.button55.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button56
            // 
            this.button56.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button56.BackColor = System.Drawing.Color.LightSlateGray;
            this.button56.ForeColor = System.Drawing.Color.Black;
            this.button56.Location = new System.Drawing.Point(0, 1904);
            this.button56.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(101, 20);
            this.button56.TabIndex = 125;
            this.button56.Text = "U0> ";
            this.button56.UseVisualStyleBackColor = false;
            this.button56.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button57
            // 
            this.button57.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button57.BackColor = System.Drawing.Color.LightSlateGray;
            this.button57.ForeColor = System.Drawing.Color.Black;
            this.button57.Location = new System.Drawing.Point(0, 1924);
            this.button57.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(101, 20);
            this.button57.TabIndex = 126;
            this.button57.Text = "U0>> v";
            this.button57.UseVisualStyleBackColor = false;
            this.button57.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button58
            // 
            this.button58.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button58.BackColor = System.Drawing.Color.LightSlateGray;
            this.button58.ForeColor = System.Drawing.Color.Black;
            this.button58.Location = new System.Drawing.Point(0, 1944);
            this.button58.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(101, 20);
            this.button58.TabIndex = 127;
            this.button58.Text = "U0>> ";
            this.button58.UseVisualStyleBackColor = false;
            this.button58.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button59
            // 
            this.button59.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button59.BackColor = System.Drawing.Color.LightSlateGray;
            this.button59.ForeColor = System.Drawing.Color.Black;
            this.button59.Location = new System.Drawing.Point(0, 1964);
            this.button59.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(101, 20);
            this.button59.TabIndex = 128;
            this.button59.Text = "ВЗ-1";
            this.button59.UseVisualStyleBackColor = false;
            this.button59.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button60
            // 
            this.button60.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button60.BackColor = System.Drawing.Color.LightSlateGray;
            this.button60.ForeColor = System.Drawing.Color.Black;
            this.button60.Location = new System.Drawing.Point(0, 1984);
            this.button60.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(101, 20);
            this.button60.TabIndex = 119;
            this.button60.Text = "ВЗ-2";
            this.button60.UseVisualStyleBackColor = false;
            this.button60.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button61
            // 
            this.button61.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button61.BackColor = System.Drawing.Color.LightSlateGray;
            this.button61.ForeColor = System.Drawing.Color.Black;
            this.button61.Location = new System.Drawing.Point(0, 2004);
            this.button61.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(101, 20);
            this.button61.TabIndex = 120;
            this.button61.Text = "ВЗ-3";
            this.button61.UseVisualStyleBackColor = false;
            this.button61.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button62
            // 
            this.button62.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button62.BackColor = System.Drawing.Color.LightSlateGray;
            this.button62.ForeColor = System.Drawing.Color.Black;
            this.button62.Location = new System.Drawing.Point(0, 2024);
            this.button62.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(101, 20);
            this.button62.TabIndex = 121;
            this.button62.Text = "ВЗ-4";
            this.button62.UseVisualStyleBackColor = false;
            this.button62.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button63
            // 
            this.button63.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button63.BackColor = System.Drawing.Color.LightSlateGray;
            this.button63.ForeColor = System.Drawing.Color.Black;
            this.button63.Location = new System.Drawing.Point(0, 2044);
            this.button63.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(101, 20);
            this.button63.TabIndex = 122;
            this.button63.Text = "ВЗ-5";
            this.button63.UseVisualStyleBackColor = false;
            this.button63.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button64
            // 
            this.button64.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button64.BackColor = System.Drawing.Color.LightSlateGray;
            this.button64.ForeColor = System.Drawing.Color.Black;
            this.button64.Location = new System.Drawing.Point(0, 2064);
            this.button64.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(101, 20);
            this.button64.TabIndex = 123;
            this.button64.Text = "ВЗ-6";
            this.button64.UseVisualStyleBackColor = false;
            this.button64.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button65
            // 
            this.button65.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button65.BackColor = System.Drawing.Color.LightSlateGray;
            this.button65.ForeColor = System.Drawing.Color.Black;
            this.button65.Location = new System.Drawing.Point(0, 2084);
            this.button65.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(101, 20);
            this.button65.TabIndex = 117;
            this.button65.Text = "ВЗ-7";
            this.button65.UseVisualStyleBackColor = false;
            this.button65.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button66
            // 
            this.button66.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button66.BackColor = System.Drawing.Color.LightSlateGray;
            this.button66.ForeColor = System.Drawing.Color.Black;
            this.button66.Location = new System.Drawing.Point(0, 2104);
            this.button66.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(101, 20);
            this.button66.TabIndex = 118;
            this.button66.Text = "ВЗ-8";
            this.button66.UseVisualStyleBackColor = false;
            this.button66.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button67
            // 
            this.button67.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button67.BackColor = System.Drawing.Color.LightSlateGray;
            this.button67.ForeColor = System.Drawing.Color.Black;
            this.button67.Location = new System.Drawing.Point(0, 2124);
            this.button67.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(101, 20);
            this.button67.TabIndex = 111;
            this.button67.Text = "ССЛ1";
            this.button67.UseVisualStyleBackColor = false;
            this.button67.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button68
            // 
            this.button68.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button68.BackColor = System.Drawing.Color.LightSlateGray;
            this.button68.ForeColor = System.Drawing.Color.Black;
            this.button68.Location = new System.Drawing.Point(0, 2144);
            this.button68.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(101, 20);
            this.button68.TabIndex = 112;
            this.button68.Text = "ССЛ2";
            this.button68.UseVisualStyleBackColor = false;
            this.button68.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button69
            // 
            this.button69.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button69.BackColor = System.Drawing.Color.LightSlateGray;
            this.button69.ForeColor = System.Drawing.Color.Black;
            this.button69.Location = new System.Drawing.Point(0, 2164);
            this.button69.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(101, 20);
            this.button69.TabIndex = 113;
            this.button69.Text = "ССЛ3";
            this.button69.UseVisualStyleBackColor = false;
            this.button69.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button70
            // 
            this.button70.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button70.BackColor = System.Drawing.Color.LightSlateGray;
            this.button70.ForeColor = System.Drawing.Color.Black;
            this.button70.Location = new System.Drawing.Point(0, 2184);
            this.button70.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(101, 20);
            this.button70.TabIndex = 114;
            this.button70.Text = "ССЛ4";
            this.button70.UseVisualStyleBackColor = false;
            this.button70.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button71
            // 
            this.button71.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button71.BackColor = System.Drawing.Color.LightSlateGray;
            this.button71.ForeColor = System.Drawing.Color.Black;
            this.button71.Location = new System.Drawing.Point(0, 2204);
            this.button71.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(101, 20);
            this.button71.TabIndex = 115;
            this.button71.Text = "ССЛ5";
            this.button71.UseVisualStyleBackColor = false;
            this.button71.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button72
            // 
            this.button72.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button72.BackColor = System.Drawing.Color.LightSlateGray;
            this.button72.ForeColor = System.Drawing.Color.Black;
            this.button72.Location = new System.Drawing.Point(0, 2224);
            this.button72.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(101, 20);
            this.button72.TabIndex = 116;
            this.button72.Text = "ССЛ6";
            this.button72.UseVisualStyleBackColor = false;
            this.button72.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button45
            // 
            this.button45.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button45.BackColor = System.Drawing.Color.LightSlateGray;
            this.button45.ForeColor = System.Drawing.Color.Black;
            this.button45.Location = new System.Drawing.Point(0, 1684);
            this.button45.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(101, 20);
            this.button45.TabIndex = 106;
            this.button45.Text = "U> ";
            this.button45.UseVisualStyleBackColor = false;
            this.button45.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button46
            // 
            this.button46.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button46.BackColor = System.Drawing.Color.LightSlateGray;
            this.button46.ForeColor = System.Drawing.Color.Black;
            this.button46.Location = new System.Drawing.Point(0, 1704);
            this.button46.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(101, 20);
            this.button46.TabIndex = 107;
            this.button46.Text = "U>> ИО";
            this.button46.UseVisualStyleBackColor = false;
            this.button46.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button47
            // 
            this.button47.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button47.BackColor = System.Drawing.Color.LightSlateGray;
            this.button47.ForeColor = System.Drawing.Color.Black;
            this.button47.Location = new System.Drawing.Point(0, 1724);
            this.button47.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(101, 20);
            this.button47.TabIndex = 108;
            this.button47.Text = "U>>";
            this.button47.UseVisualStyleBackColor = false;
            this.button47.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button48
            // 
            this.button48.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button48.BackColor = System.Drawing.Color.LightSlateGray;
            this.button48.ForeColor = System.Drawing.Color.Black;
            this.button48.Location = new System.Drawing.Point(0, 1744);
            this.button48.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(101, 20);
            this.button48.TabIndex = 109;
            this.button48.Text = "U< ИО";
            this.button48.UseVisualStyleBackColor = false;
            this.button48.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button49
            // 
            this.button49.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button49.BackColor = System.Drawing.Color.LightSlateGray;
            this.button49.ForeColor = System.Drawing.Color.Black;
            this.button49.Location = new System.Drawing.Point(0, 1764);
            this.button49.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(101, 20);
            this.button49.TabIndex = 110;
            this.button49.Text = "U< ";
            this.button49.UseVisualStyleBackColor = false;
            this.button49.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button50
            // 
            this.button50.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button50.BackColor = System.Drawing.Color.LightSlateGray;
            this.button50.ForeColor = System.Drawing.Color.Black;
            this.button50.Location = new System.Drawing.Point(0, 1784);
            this.button50.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(101, 20);
            this.button50.TabIndex = 100;
            this.button50.Text = "U<< ИО";
            this.button50.UseVisualStyleBackColor = false;
            this.button50.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button51
            // 
            this.button51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button51.BackColor = System.Drawing.Color.LightSlateGray;
            this.button51.ForeColor = System.Drawing.Color.Black;
            this.button51.Location = new System.Drawing.Point(0, 1804);
            this.button51.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(101, 20);
            this.button51.TabIndex = 101;
            this.button51.Text = "U<<";
            this.button51.UseVisualStyleBackColor = false;
            this.button51.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button52
            // 
            this.button52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button52.BackColor = System.Drawing.Color.LightSlateGray;
            this.button52.ForeColor = System.Drawing.Color.Black;
            this.button52.Location = new System.Drawing.Point(0, 1824);
            this.button52.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(101, 20);
            this.button52.TabIndex = 102;
            this.button52.Text = "U2> ИО";
            this.button52.UseVisualStyleBackColor = false;
            this.button52.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button53
            // 
            this.button53.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button53.BackColor = System.Drawing.Color.LightSlateGray;
            this.button53.ForeColor = System.Drawing.Color.Black;
            this.button53.Location = new System.Drawing.Point(0, 1844);
            this.button53.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(101, 20);
            this.button53.TabIndex = 103;
            this.button53.Text = "U2> ";
            this.button53.UseVisualStyleBackColor = false;
            this.button53.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button54
            // 
            this.button54.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button54.BackColor = System.Drawing.Color.LightSlateGray;
            this.button54.ForeColor = System.Drawing.Color.Black;
            this.button54.Location = new System.Drawing.Point(0, 1864);
            this.button54.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(101, 20);
            this.button54.TabIndex = 104;
            this.button54.Text = "U2>> ИО";
            this.button54.UseVisualStyleBackColor = false;
            this.button54.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button34
            // 
            this.button34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button34.BackColor = System.Drawing.Color.LightSlateGray;
            this.button34.ForeColor = System.Drawing.Color.Black;
            this.button34.Location = new System.Drawing.Point(0, 1464);
            this.button34.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(101, 20);
            this.button34.TabIndex = 95;
            this.button34.Text = "I2/I1";
            this.button34.UseVisualStyleBackColor = false;
            this.button34.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button35
            // 
            this.button35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button35.BackColor = System.Drawing.Color.LightSlateGray;
            this.button35.ForeColor = System.Drawing.Color.Black;
            this.button35.Location = new System.Drawing.Point(0, 1484);
            this.button35.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(101, 20);
            this.button35.TabIndex = 96;
            this.button35.Text = "In>> ИО";
            this.button35.UseVisualStyleBackColor = false;
            this.button35.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button36
            // 
            this.button36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button36.BackColor = System.Drawing.Color.LightSlateGray;
            this.button36.ForeColor = System.Drawing.Color.Black;
            this.button36.Location = new System.Drawing.Point(0, 1504);
            this.button36.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(101, 20);
            this.button36.TabIndex = 97;
            this.button36.Text = "F> ИО";
            this.button36.UseVisualStyleBackColor = false;
            this.button36.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button37
            // 
            this.button37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button37.BackColor = System.Drawing.Color.LightSlateGray;
            this.button37.ForeColor = System.Drawing.Color.Black;
            this.button37.Location = new System.Drawing.Point(0, 1524);
            this.button37.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(101, 20);
            this.button37.TabIndex = 98;
            this.button37.Text = "F> ";
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button38
            // 
            this.button38.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button38.BackColor = System.Drawing.Color.LightSlateGray;
            this.button38.ForeColor = System.Drawing.Color.Black;
            this.button38.Location = new System.Drawing.Point(0, 1544);
            this.button38.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(101, 20);
            this.button38.TabIndex = 99;
            this.button38.Text = "F>> ИО";
            this.button38.UseVisualStyleBackColor = false;
            this.button38.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button39
            // 
            this.button39.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button39.BackColor = System.Drawing.Color.LightSlateGray;
            this.button39.ForeColor = System.Drawing.Color.Black;
            this.button39.Location = new System.Drawing.Point(0, 1564);
            this.button39.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(101, 20);
            this.button39.TabIndex = 89;
            this.button39.Text = "F>> ";
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button40
            // 
            this.button40.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button40.BackColor = System.Drawing.Color.LightSlateGray;
            this.button40.ForeColor = System.Drawing.Color.Black;
            this.button40.Location = new System.Drawing.Point(0, 1584);
            this.button40.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(101, 20);
            this.button40.TabIndex = 90;
            this.button40.Text = "F< ИО";
            this.button40.UseVisualStyleBackColor = false;
            this.button40.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button41
            // 
            this.button41.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button41.BackColor = System.Drawing.Color.LightSlateGray;
            this.button41.ForeColor = System.Drawing.Color.Black;
            this.button41.Location = new System.Drawing.Point(0, 1604);
            this.button41.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(101, 20);
            this.button41.TabIndex = 91;
            this.button41.Text = "F< ";
            this.button41.UseVisualStyleBackColor = false;
            this.button41.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button42
            // 
            this.button42.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button42.BackColor = System.Drawing.Color.LightSlateGray;
            this.button42.ForeColor = System.Drawing.Color.Black;
            this.button42.Location = new System.Drawing.Point(0, 1624);
            this.button42.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(101, 20);
            this.button42.TabIndex = 92;
            this.button42.Text = "F<< ИО";
            this.button42.UseVisualStyleBackColor = false;
            this.button42.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button43
            // 
            this.button43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button43.BackColor = System.Drawing.Color.LightSlateGray;
            this.button43.ForeColor = System.Drawing.Color.Black;
            this.button43.Location = new System.Drawing.Point(0, 1644);
            this.button43.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(101, 20);
            this.button43.TabIndex = 93;
            this.button43.Text = "F<<";
            this.button43.UseVisualStyleBackColor = false;
            this.button43.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button44
            // 
            this.button44.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button44.BackColor = System.Drawing.Color.LightSlateGray;
            this.button44.ForeColor = System.Drawing.Color.Black;
            this.button44.Location = new System.Drawing.Point(0, 1664);
            this.button44.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(101, 20);
            this.button44.TabIndex = 94;
            this.button44.Text = "U> ИО";
            this.button44.UseVisualStyleBackColor = false;
            this.button44.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button28
            // 
            this.button28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button28.BackColor = System.Drawing.Color.LightSlateGray;
            this.button28.ForeColor = System.Drawing.Color.Black;
            this.button28.Location = new System.Drawing.Point(0, 1344);
            this.button28.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(101, 20);
            this.button28.TabIndex = 83;
            this.button28.Text = "In> ";
            this.button28.UseVisualStyleBackColor = false;
            this.button28.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button29
            // 
            this.button29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button29.BackColor = System.Drawing.Color.LightSlateGray;
            this.button29.ForeColor = System.Drawing.Color.Black;
            this.button29.Location = new System.Drawing.Point(0, 1364);
            this.button29.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(101, 20);
            this.button29.TabIndex = 84;
            this.button29.Text = "In>> ИО";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button30
            // 
            this.button30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button30.BackColor = System.Drawing.Color.LightSlateGray;
            this.button30.ForeColor = System.Drawing.Color.Black;
            this.button30.Location = new System.Drawing.Point(0, 1384);
            this.button30.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(101, 20);
            this.button30.TabIndex = 85;
            this.button30.Text = "In>> ";
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button31
            // 
            this.button31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button31.BackColor = System.Drawing.Color.LightSlateGray;
            this.button31.ForeColor = System.Drawing.Color.Black;
            this.button31.Location = new System.Drawing.Point(0, 1404);
            this.button31.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(101, 20);
            this.button31.TabIndex = 86;
            this.button31.Text = "Ig> ИО";
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button32
            // 
            this.button32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button32.BackColor = System.Drawing.Color.LightSlateGray;
            this.button32.ForeColor = System.Drawing.Color.Black;
            this.button32.Location = new System.Drawing.Point(0, 1424);
            this.button32.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(101, 20);
            this.button32.TabIndex = 87;
            this.button32.Text = "Ig>";
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button33
            // 
            this.button33.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button33.BackColor = System.Drawing.Color.LightSlateGray;
            this.button33.ForeColor = System.Drawing.Color.Black;
            this.button33.Location = new System.Drawing.Point(0, 1444);
            this.button33.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(101, 20);
            this.button33.TabIndex = 88;
            this.button33.Text = "I2/I1 ИО";
            this.button33.UseVisualStyleBackColor = false;
            this.button33.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button24
            // 
            this.button24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button24.BackColor = System.Drawing.Color.LightSlateGray;
            this.button24.ForeColor = System.Drawing.Color.Black;
            this.button24.Location = new System.Drawing.Point(0, 1284);
            this.button24.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(101, 20);
            this.button24.TabIndex = 79;
            this.button24.Text = "I0> ";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button26
            // 
            this.button26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button26.BackColor = System.Drawing.Color.LightSlateGray;
            this.button26.ForeColor = System.Drawing.Color.Black;
            this.button26.Location = new System.Drawing.Point(0, 1304);
            this.button26.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(101, 20);
            this.button26.TabIndex = 81;
            this.button26.Text = "I0>> ";
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button27
            // 
            this.button27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button27.BackColor = System.Drawing.Color.LightSlateGray;
            this.button27.ForeColor = System.Drawing.Color.Black;
            this.button27.Location = new System.Drawing.Point(0, 1324);
            this.button27.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(101, 20);
            this.button27.TabIndex = 82;
            this.button27.Text = "In> ИО";
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button16
            // 
            this.button16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button16.BackColor = System.Drawing.Color.LightSlateGray;
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.Location = new System.Drawing.Point(0, 1182);
            this.button16.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(101, 20);
            this.button16.TabIndex = 71;
            this.button16.Text = "I>>>> ";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button17
            // 
            this.button17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button17.BackColor = System.Drawing.Color.LightSlateGray;
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Location = new System.Drawing.Point(0, 1202);
            this.button17.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(101, 20);
            this.button17.TabIndex = 72;
            this.button17.Text = "I2> ИО";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button18
            // 
            this.button18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button18.BackColor = System.Drawing.Color.LightSlateGray;
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Location = new System.Drawing.Point(0, 1222);
            this.button18.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(101, 20);
            this.button18.TabIndex = 73;
            this.button18.Text = "I2>";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button19
            // 
            this.button19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button19.BackColor = System.Drawing.Color.LightSlateGray;
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.Location = new System.Drawing.Point(0, 1242);
            this.button19.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(101, 20);
            this.button19.TabIndex = 74;
            this.button19.Text = "I2>> ИО";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button20
            // 
            this.button20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button20.BackColor = System.Drawing.Color.LightSlateGray;
            this.button20.ForeColor = System.Drawing.Color.Black;
            this.button20.Location = new System.Drawing.Point(0, 1262);
            this.button20.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(101, 20);
            this.button20.TabIndex = 75;
            this.button20.Text = "I2>> ИО";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.BackColor = System.Drawing.Color.LightSlateGray;
            this.button9.ForeColor = System.Drawing.Color.Black;
            this.button9.Location = new System.Drawing.Point(0, 1042);
            this.button9.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(101, 20);
            this.button9.TabIndex = 64;
            this.button9.Text = "I> ИО";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.BackColor = System.Drawing.Color.LightSlateGray;
            this.button10.ForeColor = System.Drawing.Color.Black;
            this.button10.Location = new System.Drawing.Point(0, 1062);
            this.button10.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(101, 20);
            this.button10.TabIndex = 65;
            this.button10.Text = "I>";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button11.BackColor = System.Drawing.Color.LightSlateGray;
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Location = new System.Drawing.Point(0, 1082);
            this.button11.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(101, 20);
            this.button11.TabIndex = 66;
            this.button11.Text = "I>> ИО";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.BackColor = System.Drawing.Color.LightSlateGray;
            this.button12.ForeColor = System.Drawing.Color.Black;
            this.button12.Location = new System.Drawing.Point(0, 1102);
            this.button12.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(101, 20);
            this.button12.TabIndex = 67;
            this.button12.Text = "I>> ";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button13.BackColor = System.Drawing.Color.LightSlateGray;
            this.button13.ForeColor = System.Drawing.Color.Black;
            this.button13.Location = new System.Drawing.Point(0, 1122);
            this.button13.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(101, 20);
            this.button13.TabIndex = 68;
            this.button13.Text = "I>>> ИО";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button14
            // 
            this.button14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button14.BackColor = System.Drawing.Color.LightSlateGray;
            this.button14.ForeColor = System.Drawing.Color.Black;
            this.button14.Location = new System.Drawing.Point(0, 1142);
            this.button14.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(101, 20);
            this.button14.TabIndex = 69;
            this.button14.Text = "I>>> ";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button15
            // 
            this.button15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button15.BackColor = System.Drawing.Color.LightSlateGray;
            this.button15.ForeColor = System.Drawing.Color.Black;
            this.button15.Location = new System.Drawing.Point(0, 1162);
            this.button15.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(101, 20);
            this.button15.TabIndex = 70;
            this.button15.Text = "I>>>> ИО";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.LightSlateGray;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(0, 880);
            this.button1.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 22);
            this.button1.TabIndex = 56;
            this.button1.Text = "ВЛС1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.LightSlateGray;
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(0, 902);
            this.button2.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 20);
            this.button2.TabIndex = 57;
            this.button2.Text = "ВЛС2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.LightSlateGray;
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(0, 922);
            this.button3.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 20);
            this.button3.TabIndex = 58;
            this.button3.Text = "ВЛС3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.Color.LightSlateGray;
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(0, 942);
            this.button4.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 20);
            this.button4.TabIndex = 59;
            this.button4.Text = "ВЛС4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.BackColor = System.Drawing.Color.LightSlateGray;
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(0, 962);
            this.button5.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(101, 20);
            this.button5.TabIndex = 60;
            this.button5.Text = "ВЛС5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.BackColor = System.Drawing.Color.LightSlateGray;
            this.button6.ForeColor = System.Drawing.Color.Black;
            this.button6.Location = new System.Drawing.Point(0, 982);
            this.button6.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(101, 20);
            this.button6.TabIndex = 61;
            this.button6.Text = "ВЛС6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.BackColor = System.Drawing.Color.LightSlateGray;
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Location = new System.Drawing.Point(0, 1002);
            this.button7.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(101, 20);
            this.button7.TabIndex = 62;
            this.button7.Text = "ВЛС7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.BackColor = System.Drawing.Color.LightSlateGray;
            this.button8.ForeColor = System.Drawing.Color.Black;
            this.button8.Location = new System.Drawing.Point(0, 1022);
            this.button8.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(101, 20);
            this.button8.TabIndex = 63;
            this.button8.Text = "ВЛС8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete5Button
            // 
            this._discrete5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete5Button.AutoSize = true;
            this._discrete5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete5Button.ForeColor = System.Drawing.Color.Black;
            this._discrete5Button.Location = new System.Drawing.Point(0, 88);
            this._discrete5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete5Button.Name = "_discrete5Button";
            this._discrete5Button.Size = new System.Drawing.Size(101, 22);
            this._discrete5Button.TabIndex = 16;
            this._discrete5Button.Text = "Резерв";
            this._discrete5Button.UseVisualStyleBackColor = false;
            this._discrete5Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete24Button
            // 
            this._discrete24Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete24Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete24Button.ForeColor = System.Drawing.Color.Black;
            this._discrete24Button.Location = new System.Drawing.Point(0, 506);
            this._discrete24Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete24Button.Name = "_discrete24Button";
            this._discrete24Button.Size = new System.Drawing.Size(101, 22);
            this._discrete24Button.TabIndex = 35;
            this._discrete24Button.Text = "Д8";
            this._discrete24Button.UseVisualStyleBackColor = false;
            this._discrete24Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete23Button
            // 
            this._discrete23Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete23Button.AutoSize = true;
            this._discrete23Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete23Button.ForeColor = System.Drawing.Color.Black;
            this._discrete23Button.Location = new System.Drawing.Point(0, 484);
            this._discrete23Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete23Button.Name = "_discrete23Button";
            this._discrete23Button.Size = new System.Drawing.Size(101, 22);
            this._discrete23Button.TabIndex = 34;
            this._discrete23Button.Text = "Д7";
            this._discrete23Button.UseVisualStyleBackColor = false;
            this._discrete23Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete22Button
            // 
            this._discrete22Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete22Button.AutoSize = true;
            this._discrete22Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete22Button.ForeColor = System.Drawing.Color.Black;
            this._discrete22Button.Location = new System.Drawing.Point(0, 462);
            this._discrete22Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete22Button.Name = "_discrete22Button";
            this._discrete22Button.Size = new System.Drawing.Size(101, 22);
            this._discrete22Button.TabIndex = 33;
            this._discrete22Button.Text = "Д6";
            this._discrete22Button.UseVisualStyleBackColor = false;
            this._discrete22Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete21Button
            // 
            this._discrete21Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete21Button.AutoSize = true;
            this._discrete21Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete21Button.ForeColor = System.Drawing.Color.Black;
            this._discrete21Button.Location = new System.Drawing.Point(0, 440);
            this._discrete21Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete21Button.Name = "_discrete21Button";
            this._discrete21Button.Size = new System.Drawing.Size(101, 22);
            this._discrete21Button.TabIndex = 32;
            this._discrete21Button.Text = "Д5";
            this._discrete21Button.UseVisualStyleBackColor = false;
            this._discrete21Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete20Button
            // 
            this._discrete20Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete20Button.AutoSize = true;
            this._discrete20Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete20Button.ForeColor = System.Drawing.Color.Black;
            this._discrete20Button.Location = new System.Drawing.Point(0, 418);
            this._discrete20Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete20Button.Name = "_discrete20Button";
            this._discrete20Button.Size = new System.Drawing.Size(101, 22);
            this._discrete20Button.TabIndex = 31;
            this._discrete20Button.Text = "Д4";
            this._discrete20Button.UseVisualStyleBackColor = false;
            this._discrete20Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete19Button
            // 
            this._discrete19Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete19Button.AutoSize = true;
            this._discrete19Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete19Button.ForeColor = System.Drawing.Color.Black;
            this._discrete19Button.Location = new System.Drawing.Point(0, 396);
            this._discrete19Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete19Button.Name = "_discrete19Button";
            this._discrete19Button.Size = new System.Drawing.Size(101, 22);
            this._discrete19Button.TabIndex = 30;
            this._discrete19Button.Text = "Д3";
            this._discrete19Button.UseVisualStyleBackColor = false;
            this._discrete19Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete18Button
            // 
            this._discrete18Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete18Button.AutoSize = true;
            this._discrete18Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete18Button.ForeColor = System.Drawing.Color.Black;
            this._discrete18Button.Location = new System.Drawing.Point(0, 374);
            this._discrete18Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete18Button.Name = "_discrete18Button";
            this._discrete18Button.Size = new System.Drawing.Size(101, 22);
            this._discrete18Button.TabIndex = 29;
            this._discrete18Button.Text = "Д1";
            this._discrete18Button.UseVisualStyleBackColor = false;
            this._discrete18Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete17Button
            // 
            this._discrete17Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete17Button.AutoSize = true;
            this._discrete17Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete17Button.ForeColor = System.Drawing.Color.Black;
            this._discrete17Button.Location = new System.Drawing.Point(0, 352);
            this._discrete17Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete17Button.Name = "_discrete17Button";
            this._discrete17Button.Size = new System.Drawing.Size(101, 22);
            this._discrete17Button.TabIndex = 28;
            this._discrete17Button.Text = "Д1";
            this._discrete17Button.UseVisualStyleBackColor = false;
            this._discrete17Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete16Button
            // 
            this._discrete16Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete16Button.AutoSize = true;
            this._discrete16Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete16Button.ForeColor = System.Drawing.Color.Black;
            this._discrete16Button.Location = new System.Drawing.Point(0, 330);
            this._discrete16Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete16Button.Name = "_discrete16Button";
            this._discrete16Button.Size = new System.Drawing.Size(101, 22);
            this._discrete16Button.TabIndex = 27;
            this._discrete16Button.Text = "Ускорение";
            this._discrete16Button.UseVisualStyleBackColor = false;
            this._discrete16Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete15Button
            // 
            this._discrete15Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete15Button.AutoSize = true;
            this._discrete15Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete15Button.ForeColor = System.Drawing.Color.Black;
            this._discrete15Button.Location = new System.Drawing.Point(0, 308);
            this._discrete15Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete15Button.Name = "_discrete15Button";
            this._discrete15Button.Size = new System.Drawing.Size(101, 22);
            this._discrete15Button.TabIndex = 26;
            this._discrete15Button.Text = "АПВ";
            this._discrete15Button.UseVisualStyleBackColor = false;
            this._discrete15Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete14Button
            // 
            this._discrete14Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete14Button.AutoSize = true;
            this._discrete14Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete14Button.ForeColor = System.Drawing.Color.Black;
            this._discrete14Button.Location = new System.Drawing.Point(0, 286);
            this._discrete14Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete14Button.Name = "_discrete14Button";
            this._discrete14Button.Size = new System.Drawing.Size(101, 22);
            this._discrete14Button.TabIndex = 25;
            this._discrete14Button.Text = "УРОВ";
            this._discrete14Button.UseVisualStyleBackColor = false;
            this._discrete14Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete13Button
            // 
            this._discrete13Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete13Button.AutoSize = true;
            this._discrete13Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete13Button.ForeColor = System.Drawing.Color.Black;
            this._discrete13Button.Location = new System.Drawing.Point(0, 264);
            this._discrete13Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete13Button.Name = "_discrete13Button";
            this._discrete13Button.Size = new System.Drawing.Size(101, 22);
            this._discrete13Button.TabIndex = 24;
            this._discrete13Button.Text = "ЛЗШ";
            this._discrete13Button.UseVisualStyleBackColor = false;
            this._discrete13Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete12Button
            // 
            this._discrete12Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete12Button.AutoSize = true;
            this._discrete12Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete12Button.ForeColor = System.Drawing.Color.Black;
            this._discrete12Button.Location = new System.Drawing.Point(0, 242);
            this._discrete12Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete12Button.Name = "_discrete12Button";
            this._discrete12Button.Size = new System.Drawing.Size(101, 22);
            this._discrete12Button.TabIndex = 23;
            this._discrete12Button.Text = "Резерв";
            this._discrete12Button.UseVisualStyleBackColor = false;
            this._discrete12Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete11Button
            // 
            this._discrete11Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete11Button.AutoSize = true;
            this._discrete11Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete11Button.ForeColor = System.Drawing.Color.Black;
            this._discrete11Button.Location = new System.Drawing.Point(0, 220);
            this._discrete11Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete11Button.Name = "_discrete11Button";
            this._discrete11Button.Size = new System.Drawing.Size(101, 22);
            this._discrete11Button.TabIndex = 22;
            this._discrete11Button.Text = "АВР блок.";
            this._discrete11Button.UseVisualStyleBackColor = false;
            this._discrete11Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete10Button
            // 
            this._discrete10Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete10Button.AutoSize = true;
            this._discrete10Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete10Button.ForeColor = System.Drawing.Color.Black;
            this._discrete10Button.Location = new System.Drawing.Point(0, 198);
            this._discrete10Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete10Button.Name = "_discrete10Button";
            this._discrete10Button.Size = new System.Drawing.Size(101, 22);
            this._discrete10Button.TabIndex = 21;
            this._discrete10Button.Text = "АВР откл.";
            this._discrete10Button.UseVisualStyleBackColor = false;
            this._discrete10Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete9Button
            // 
            this._discrete9Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete9Button.AutoSize = true;
            this._discrete9Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete9Button.ForeColor = System.Drawing.Color.Black;
            this._discrete9Button.Location = new System.Drawing.Point(0, 176);
            this._discrete9Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete9Button.Name = "_discrete9Button";
            this._discrete9Button.Size = new System.Drawing.Size(101, 22);
            this._discrete9Button.TabIndex = 20;
            this._discrete9Button.Text = "АВР вкл.";
            this._discrete9Button.UseVisualStyleBackColor = false;
            this._discrete9Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete8Button
            // 
            this._discrete8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete8Button.AutoSize = true;
            this._discrete8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete8Button.ForeColor = System.Drawing.Color.Black;
            this._discrete8Button.Location = new System.Drawing.Point(0, 154);
            this._discrete8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete8Button.Name = "_discrete8Button";
            this._discrete8Button.Size = new System.Drawing.Size(101, 22);
            this._discrete8Button.TabIndex = 19;
            this._discrete8Button.Text = "Замля";
            this._discrete8Button.UseVisualStyleBackColor = false;
            this._discrete8Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete7Button
            // 
            this._discrete7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete7Button.AutoSize = true;
            this._discrete7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete7Button.ForeColor = System.Drawing.Color.Black;
            this._discrete7Button.Location = new System.Drawing.Point(0, 132);
            this._discrete7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete7Button.Name = "_discrete7Button";
            this._discrete7Button.Size = new System.Drawing.Size(101, 22);
            this._discrete7Button.TabIndex = 18;
            this._discrete7Button.Text = "Откл.";
            this._discrete7Button.UseVisualStyleBackColor = false;
            this._discrete7Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete6Button
            // 
            this._discrete6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete6Button.AutoSize = true;
            this._discrete6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete6Button.ForeColor = System.Drawing.Color.Black;
            this._discrete6Button.Location = new System.Drawing.Point(0, 110);
            this._discrete6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete6Button.Name = "_discrete6Button";
            this._discrete6Button.Size = new System.Drawing.Size(101, 22);
            this._discrete6Button.TabIndex = 17;
            this._discrete6Button.Text = "Сигнал-я";
            this._discrete6Button.UseVisualStyleBackColor = false;
            this._discrete6Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete2Button
            // 
            this._discrete2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete2Button.AutoSize = true;
            this._discrete2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete2Button.ForeColor = System.Drawing.Color.Black;
            this._discrete2Button.Location = new System.Drawing.Point(0, 22);
            this._discrete2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete2Button.Name = "_discrete2Button";
            this._discrete2Button.Size = new System.Drawing.Size(101, 22);
            this._discrete2Button.TabIndex = 37;
            this._discrete2Button.Text = "Вкл. выкл.";
            this._discrete2Button.UseVisualStyleBackColor = false;
            this._discrete2Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete3Button
            // 
            this._discrete3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete3Button.AutoSize = true;
            this._discrete3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete3Button.ForeColor = System.Drawing.Color.Black;
            this._discrete3Button.Location = new System.Drawing.Point(0, 44);
            this._discrete3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete3Button.Name = "_discrete3Button";
            this._discrete3Button.Size = new System.Drawing.Size(101, 22);
            this._discrete3Button.TabIndex = 38;
            this._discrete3Button.Text = "Неиспр.";
            this._discrete3Button.UseVisualStyleBackColor = false;
            this._discrete3Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete4Button
            // 
            this._discrete4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete4Button.AutoSize = true;
            this._discrete4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete4Button.ForeColor = System.Drawing.Color.Black;
            this._discrete4Button.Location = new System.Drawing.Point(0, 66);
            this._discrete4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete4Button.Name = "_discrete4Button";
            this._discrete4Button.Size = new System.Drawing.Size(101, 22);
            this._discrete4Button.TabIndex = 39;
            this._discrete4Button.Text = "Гр. уставок";
            this._discrete4Button.UseVisualStyleBackColor = false;
            this._discrete4Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete1Button
            // 
            this._discrete1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete1Button.AutoSize = true;
            this._discrete1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete1Button.ForeColor = System.Drawing.Color.Black;
            this._discrete1Button.Location = new System.Drawing.Point(0, 0);
            this._discrete1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete1Button.Name = "_discrete1Button";
            this._discrete1Button.Size = new System.Drawing.Size(101, 22);
            this._discrete1Button.TabIndex = 36;
            this._discrete1Button.Text = "Откл. выкл.";
            this._discrete1Button.UseVisualStyleBackColor = false;
            this._discrete1Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete25Button
            // 
            this._discrete25Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete25Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete25Button.ForeColor = System.Drawing.Color.Black;
            this._discrete25Button.Location = new System.Drawing.Point(0, 528);
            this._discrete25Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete25Button.Name = "_discrete25Button";
            this._discrete25Button.Size = new System.Drawing.Size(101, 22);
            this._discrete25Button.TabIndex = 40;
            this._discrete25Button.Text = "Д9";
            this._discrete25Button.UseVisualStyleBackColor = false;
            this._discrete25Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete26Button
            // 
            this._discrete26Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete26Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete26Button.ForeColor = System.Drawing.Color.Black;
            this._discrete26Button.Location = new System.Drawing.Point(0, 550);
            this._discrete26Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete26Button.Name = "_discrete26Button";
            this._discrete26Button.Size = new System.Drawing.Size(101, 22);
            this._discrete26Button.TabIndex = 41;
            this._discrete26Button.Text = "Д10";
            this._discrete26Button.UseVisualStyleBackColor = false;
            this._discrete26Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete27Button
            // 
            this._discrete27Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete27Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete27Button.ForeColor = System.Drawing.Color.Black;
            this._discrete27Button.Location = new System.Drawing.Point(0, 572);
            this._discrete27Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete27Button.Name = "_discrete27Button";
            this._discrete27Button.Size = new System.Drawing.Size(101, 22);
            this._discrete27Button.TabIndex = 42;
            this._discrete27Button.Text = "Д11";
            this._discrete27Button.UseVisualStyleBackColor = false;
            this._discrete27Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete28Button
            // 
            this._discrete28Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete28Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete28Button.ForeColor = System.Drawing.Color.Black;
            this._discrete28Button.Location = new System.Drawing.Point(0, 594);
            this._discrete28Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete28Button.Name = "_discrete28Button";
            this._discrete28Button.Size = new System.Drawing.Size(101, 22);
            this._discrete28Button.TabIndex = 43;
            this._discrete28Button.Text = "Д12";
            this._discrete28Button.UseVisualStyleBackColor = false;
            this._discrete28Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete29Button
            // 
            this._discrete29Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete29Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete29Button.ForeColor = System.Drawing.Color.Black;
            this._discrete29Button.Location = new System.Drawing.Point(0, 616);
            this._discrete29Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete29Button.Name = "_discrete29Button";
            this._discrete29Button.Size = new System.Drawing.Size(101, 22);
            this._discrete29Button.TabIndex = 44;
            this._discrete29Button.Text = "Д13";
            this._discrete29Button.UseVisualStyleBackColor = false;
            this._discrete29Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete30Button
            // 
            this._discrete30Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete30Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete30Button.ForeColor = System.Drawing.Color.Black;
            this._discrete30Button.Location = new System.Drawing.Point(0, 638);
            this._discrete30Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete30Button.Name = "_discrete30Button";
            this._discrete30Button.Size = new System.Drawing.Size(101, 22);
            this._discrete30Button.TabIndex = 45;
            this._discrete30Button.Text = "Д14";
            this._discrete30Button.UseVisualStyleBackColor = false;
            this._discrete30Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete31Button
            // 
            this._discrete31Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete31Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete31Button.ForeColor = System.Drawing.Color.Black;
            this._discrete31Button.Location = new System.Drawing.Point(0, 660);
            this._discrete31Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete31Button.Name = "_discrete31Button";
            this._discrete31Button.Size = new System.Drawing.Size(101, 22);
            this._discrete31Button.TabIndex = 46;
            this._discrete31Button.Text = "Д15";
            this._discrete31Button.UseVisualStyleBackColor = false;
            this._discrete31Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete32Button
            // 
            this._discrete32Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete32Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete32Button.ForeColor = System.Drawing.Color.Black;
            this._discrete32Button.Location = new System.Drawing.Point(0, 682);
            this._discrete32Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete32Button.Name = "_discrete32Button";
            this._discrete32Button.Size = new System.Drawing.Size(101, 22);
            this._discrete32Button.TabIndex = 47;
            this._discrete32Button.Text = "Д16";
            this._discrete32Button.UseVisualStyleBackColor = false;
            this._discrete32Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete33Button
            // 
            this._discrete33Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete33Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete33Button.ForeColor = System.Drawing.Color.Black;
            this._discrete33Button.Location = new System.Drawing.Point(0, 704);
            this._discrete33Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete33Button.Name = "_discrete33Button";
            this._discrete33Button.Size = new System.Drawing.Size(101, 22);
            this._discrete33Button.TabIndex = 48;
            this._discrete33Button.Text = "ЛС1";
            this._discrete33Button.UseVisualStyleBackColor = false;
            this._discrete33Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete34Button
            // 
            this._discrete34Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete34Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete34Button.ForeColor = System.Drawing.Color.Black;
            this._discrete34Button.Location = new System.Drawing.Point(0, 726);
            this._discrete34Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete34Button.Name = "_discrete34Button";
            this._discrete34Button.Size = new System.Drawing.Size(101, 22);
            this._discrete34Button.TabIndex = 49;
            this._discrete34Button.Text = "ЛС2";
            this._discrete34Button.UseVisualStyleBackColor = false;
            this._discrete34Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete35Button
            // 
            this._discrete35Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete35Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete35Button.ForeColor = System.Drawing.Color.Black;
            this._discrete35Button.Location = new System.Drawing.Point(0, 748);
            this._discrete35Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete35Button.Name = "_discrete35Button";
            this._discrete35Button.Size = new System.Drawing.Size(101, 22);
            this._discrete35Button.TabIndex = 50;
            this._discrete35Button.Text = "ЛС3";
            this._discrete35Button.UseVisualStyleBackColor = false;
            this._discrete35Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete36Button
            // 
            this._discrete36Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete36Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete36Button.ForeColor = System.Drawing.Color.Black;
            this._discrete36Button.Location = new System.Drawing.Point(0, 770);
            this._discrete36Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete36Button.Name = "_discrete36Button";
            this._discrete36Button.Size = new System.Drawing.Size(101, 22);
            this._discrete36Button.TabIndex = 51;
            this._discrete36Button.Text = "ЛС4";
            this._discrete36Button.UseVisualStyleBackColor = false;
            this._discrete36Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete37Button
            // 
            this._discrete37Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete37Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete37Button.ForeColor = System.Drawing.Color.Black;
            this._discrete37Button.Location = new System.Drawing.Point(0, 792);
            this._discrete37Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete37Button.Name = "_discrete37Button";
            this._discrete37Button.Size = new System.Drawing.Size(101, 22);
            this._discrete37Button.TabIndex = 52;
            this._discrete37Button.Text = "ЛС5";
            this._discrete37Button.UseVisualStyleBackColor = false;
            this._discrete37Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete38Button
            // 
            this._discrete38Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete38Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete38Button.ForeColor = System.Drawing.Color.Black;
            this._discrete38Button.Location = new System.Drawing.Point(0, 814);
            this._discrete38Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete38Button.Name = "_discrete38Button";
            this._discrete38Button.Size = new System.Drawing.Size(101, 22);
            this._discrete38Button.TabIndex = 53;
            this._discrete38Button.Text = "ЛС6";
            this._discrete38Button.UseVisualStyleBackColor = false;
            this._discrete38Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete39Button
            // 
            this._discrete39Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete39Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete39Button.ForeColor = System.Drawing.Color.Black;
            this._discrete39Button.Location = new System.Drawing.Point(0, 836);
            this._discrete39Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete39Button.Name = "_discrete39Button";
            this._discrete39Button.Size = new System.Drawing.Size(101, 22);
            this._discrete39Button.TabIndex = 54;
            this._discrete39Button.Text = "ЛС7";
            this._discrete39Button.UseVisualStyleBackColor = false;
            this._discrete39Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete40Button
            // 
            this._discrete40Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete40Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete40Button.ForeColor = System.Drawing.Color.Black;
            this._discrete40Button.Location = new System.Drawing.Point(0, 858);
            this._discrete40Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete40Button.Name = "_discrete40Button";
            this._discrete40Button.Size = new System.Drawing.Size(101, 22);
            this._discrete40Button.TabIndex = 55;
            this._discrete40Button.Text = "ЛС8";
            this._discrete40Button.UseVisualStyleBackColor = false;
            this._discrete40Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Дискреты";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point(0, 1079);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1038, 3);
            this.splitter1.TabIndex = 28;
            this.splitter1.TabStop = false;
            // 
            // _voltageLayoutPanel
            // 
            this._voltageLayoutPanel.ColumnCount = 3;
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.Controls.Add(this._uScroll, 2, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this._voltageLayoutPanel.Controls.Add(this._uChart, 1, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this._voltageLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._voltageLayoutPanel.Location = new System.Drawing.Point(0, 541);
            this._voltageLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._voltageLayoutPanel.Name = "_voltageLayoutPanel";
            this._voltageLayoutPanel.RowCount = 2;
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.Size = new System.Drawing.Size(1038, 538);
            this._voltageLayoutPanel.TabIndex = 27;
            // 
            // _uScroll
            // 
            this._uScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._uScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._uScroll.LargeChange = 1;
            this._uScroll.Location = new System.Drawing.Point(1020, 35);
            this._uScroll.Minimum = 100;
            this._uScroll.Name = "_uScroll";
            this._uScroll.Size = new System.Drawing.Size(15, 503);
            this._uScroll.TabIndex = 32;
            this._uScroll.Value = 100;
            this._uScroll.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this._voltageLayoutPanel.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 611F));
            this.tableLayoutPanel3.Controls.Add(this._u1Button, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this._u4Button, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this._u5Button, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this._u2Button, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this._u3Button, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // _u1Button
            // 
            this._u1Button.BackColor = System.Drawing.Color.Yellow;
            this._u1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u1Button.Location = new System.Drawing.Point(103, 3);
            this._u1Button.Name = "_u1Button";
            this._u1Button.Size = new System.Drawing.Size(49, 23);
            this._u1Button.TabIndex = 15;
            this._u1Button.Text = "Ua";
            this._u1Button.UseVisualStyleBackColor = false;
            this._u1Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u4Button
            // 
            this._u4Button.BackColor = System.Drawing.Color.Indigo;
            this._u4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u4Button.Location = new System.Drawing.Point(268, 3);
            this._u4Button.Name = "_u4Button";
            this._u4Button.Size = new System.Drawing.Size(49, 23);
            this._u4Button.TabIndex = 13;
            this._u4Button.Text = "Un";
            this._u4Button.UseVisualStyleBackColor = false;
            this._u4Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u5Button
            // 
            this._u5Button.BackColor = System.Drawing.Color.DarkOliveGreen;
            this._u5Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u5Button.Location = new System.Drawing.Point(323, 3);
            this._u5Button.Name = "_u5Button";
            this._u5Button.Size = new System.Drawing.Size(49, 23);
            this._u5Button.TabIndex = 11;
            this._u5Button.Text = "Un1";
            this._u5Button.UseVisualStyleBackColor = false;
            this._u5Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u2Button
            // 
            this._u2Button.BackColor = System.Drawing.Color.Green;
            this._u2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u2Button.Location = new System.Drawing.Point(158, 3);
            this._u2Button.Name = "_u2Button";
            this._u2Button.Size = new System.Drawing.Size(49, 23);
            this._u2Button.TabIndex = 10;
            this._u2Button.Text = "Ub";
            this._u2Button.UseVisualStyleBackColor = false;
            this._u2Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u3Button
            // 
            this._u3Button.BackColor = System.Drawing.Color.Red;
            this._u3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u3Button.Location = new System.Drawing.Point(213, 3);
            this._u3Button.Name = "_u3Button";
            this._u3Button.Size = new System.Drawing.Size(49, 23);
            this._u3Button.TabIndex = 9;
            this._u3Button.Text = "Uc";
            this._u3Button.UseVisualStyleBackColor = false;
            this._u3Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Напряжения";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _uChart
            // 
            this._uChart.BkGradient = false;
            this._uChart.BkGradientAngle = 90;
            this._uChart.BkGradientColor = System.Drawing.Color.White;
            this._uChart.BkGradientRate = 0.5F;
            this._uChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._uChart.BkRestrictedToChartPanel = false;
            this._uChart.BkShinePosition = 1F;
            this._uChart.BkTransparency = 0F;
            this._uChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._uChart.BorderExteriorLength = 0;
            this._uChart.BorderGradientAngle = 225;
            this._uChart.BorderGradientLightPos1 = 1F;
            this._uChart.BorderGradientLightPos2 = -1F;
            this._uChart.BorderGradientRate = 0.5F;
            this._uChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._uChart.BorderLightIntermediateBrightness = 0F;
            this._uChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._uChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._uChart.ChartPanelBkTransparency = 0F;
            this._uChart.ControlShadow = false;
            this._uChart.CoordinateAxesVisible = true;
            this._uChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._uChart.CoordinateXOrigin = 0D;
            this._uChart.CoordinateYMax = 100D;
            this._uChart.CoordinateYMin = -100D;
            this._uChart.CoordinateYOrigin = 0D;
            this._uChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._uChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._uChart.FooterColor = System.Drawing.Color.Black;
            this._uChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._uChart.FooterVisible = false;
            this._uChart.GridColor = System.Drawing.Color.MistyRose;
            this._uChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._uChart.GridVisible = true;
            this._uChart.GridXSubTicker = 0;
            this._uChart.GridXTicker = 10;
            this._uChart.GridYSubTicker = 0;
            this._uChart.GridYTicker = 10;
            this._uChart.HeaderColor = System.Drawing.Color.Black;
            this._uChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._uChart.HeaderVisible = false;
            this._uChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.InnerBorderLength = 0;
            this._uChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._uChart.LegendBkColor = System.Drawing.Color.White;
            this._uChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._uChart.LegendVisible = false;
            this._uChart.Location = new System.Drawing.Point(65, 38);
            this._uChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._uChart.MiddleBorderLength = 0;
            this._uChart.Name = "_uChart";
            this._uChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.OuterBorderLength = 0;
            this._uChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._uChart.Precision = 0;
            this._uChart.RoundRadius = 10;
            this._uChart.ShadowColor = System.Drawing.Color.DimGray;
            this._uChart.ShadowDepth = 8;
            this._uChart.ShadowRate = 0.5F;
            this._uChart.Size = new System.Drawing.Size(950, 497);
            this._uChart.TabIndex = 33;
            this._uChart.Text = "daS_Net_XYChart1";
            this._uChart.XMax = 100D;
            this._uChart.XMin = 0D;
            this._uChart.XScaleColor = System.Drawing.Color.Black;
            this._uChart.XScaleVisible = true;
            this._uChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this._voltageChartDecreaseButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._voltageChartIncreaseButton, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(56, 497);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // _voltageChartDecreaseButton
            // 
            this._voltageChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartDecreaseButton.Enabled = false;
            this._voltageChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartDecreaseButton.Location = new System.Drawing.Point(3, 251);
            this._voltageChartDecreaseButton.Name = "_voltageChartDecreaseButton";
            this._voltageChartDecreaseButton.Size = new System.Drawing.Size(50, 24);
            this._voltageChartDecreaseButton.TabIndex = 1;
            this._voltageChartDecreaseButton.Text = "-";
            this._voltageChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _voltageChartIncreaseButton
            // 
            this._voltageChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartIncreaseButton.Location = new System.Drawing.Point(3, 221);
            this._voltageChartIncreaseButton.Name = "_voltageChartIncreaseButton";
            this._voltageChartIncreaseButton.Size = new System.Drawing.Size(50, 24);
            this._voltageChartIncreaseButton.TabIndex = 0;
            this._voltageChartIncreaseButton.Text = "+";
            this._voltageChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(20, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Black;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Enabled = false;
            this.splitter3.Location = new System.Drawing.Point(0, 538);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1038, 3);
            this.splitter3.TabIndex = 37;
            this.splitter3.TabStop = false;
            // 
            // _currentsLayoutPanel
            // 
            this._currentsLayoutPanel.ColumnCount = 3;
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._currentsLayoutPanel.Controls.Add(this._iScroll, 2, 1);
            this._currentsLayoutPanel.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this._currentsLayoutPanel.Controls.Add(this._iChart, 1, 1);
            this._currentsLayoutPanel.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this._currentsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._currentsLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._currentsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._currentsLayoutPanel.Name = "_currentsLayoutPanel";
            this._currentsLayoutPanel.RowCount = 2;
            this._currentsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._currentsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._currentsLayoutPanel.Size = new System.Drawing.Size(1038, 538);
            this._currentsLayoutPanel.TabIndex = 38;
            // 
            // _iScroll
            // 
            this._iScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._iScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._iScroll.LargeChange = 1;
            this._iScroll.Location = new System.Drawing.Point(1020, 35);
            this._iScroll.Minimum = 100;
            this._iScroll.Name = "_iScroll";
            this._iScroll.Size = new System.Drawing.Size(15, 503);
            this._iScroll.TabIndex = 32;
            this._iScroll.Value = 100;
            this._iScroll.Visible = false;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this._currentsLayoutPanel.SetColumnSpan(this.tableLayoutPanel7, 3);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 666F));
            this.tableLayoutPanel7.Controls.Add(this._i3Button, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this._i4Button, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this._i1Button, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this._i2Button, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel7.TabIndex = 23;
            // 
            // _i3Button
            // 
            this._i3Button.BackColor = System.Drawing.Color.Red;
            this._i3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i3Button.Location = new System.Drawing.Point(213, 3);
            this._i3Button.Name = "_i3Button";
            this._i3Button.Size = new System.Drawing.Size(49, 23);
            this._i3Button.TabIndex = 13;
            this._i3Button.Text = "Ic";
            this._i3Button.UseVisualStyleBackColor = false;
            this._i3Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // _i4Button
            // 
            this._i4Button.BackColor = System.Drawing.Color.Indigo;
            this._i4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i4Button.Location = new System.Drawing.Point(268, 3);
            this._i4Button.Name = "_i4Button";
            this._i4Button.Size = new System.Drawing.Size(49, 23);
            this._i4Button.TabIndex = 11;
            this._i4Button.Text = "In";
            this._i4Button.UseVisualStyleBackColor = false;
            this._i4Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // _i1Button
            // 
            this._i1Button.BackColor = System.Drawing.Color.Yellow;
            this._i1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i1Button.Location = new System.Drawing.Point(103, 3);
            this._i1Button.Name = "_i1Button";
            this._i1Button.Size = new System.Drawing.Size(49, 23);
            this._i1Button.TabIndex = 10;
            this._i1Button.Text = "Ia";
            this._i1Button.UseVisualStyleBackColor = false;
            this._i1Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // _i2Button
            // 
            this._i2Button.BackColor = System.Drawing.Color.Green;
            this._i2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i2Button.Location = new System.Drawing.Point(158, 3);
            this._i2Button.Name = "_i2Button";
            this._i2Button.Size = new System.Drawing.Size(49, 23);
            this._i2Button.TabIndex = 9;
            this._i2Button.Text = "Ib";
            this._i2Button.UseVisualStyleBackColor = false;
            this._i2Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Токи";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _iChart
            // 
            this._iChart.BkGradient = false;
            this._iChart.BkGradientAngle = 90;
            this._iChart.BkGradientColor = System.Drawing.Color.White;
            this._iChart.BkGradientRate = 0.5F;
            this._iChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._iChart.BkRestrictedToChartPanel = false;
            this._iChart.BkShinePosition = 1F;
            this._iChart.BkTransparency = 0F;
            this._iChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._iChart.BorderExteriorLength = 0;
            this._iChart.BorderGradientAngle = 225;
            this._iChart.BorderGradientLightPos1 = 1F;
            this._iChart.BorderGradientLightPos2 = -1F;
            this._iChart.BorderGradientRate = 0.5F;
            this._iChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._iChart.BorderLightIntermediateBrightness = 0F;
            this._iChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._iChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._iChart.ChartPanelBkTransparency = 0F;
            this._iChart.ControlShadow = false;
            this._iChart.CoordinateAxesVisible = true;
            this._iChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._iChart.CoordinateXOrigin = 0D;
            this._iChart.CoordinateYMax = 100D;
            this._iChart.CoordinateYMin = -100D;
            this._iChart.CoordinateYOrigin = 0D;
            this._iChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._iChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._iChart.FooterColor = System.Drawing.Color.Black;
            this._iChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._iChart.FooterVisible = false;
            this._iChart.GridColor = System.Drawing.Color.MistyRose;
            this._iChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._iChart.GridVisible = true;
            this._iChart.GridXSubTicker = 0;
            this._iChart.GridXTicker = 10;
            this._iChart.GridYSubTicker = 0;
            this._iChart.GridYTicker = 10;
            this._iChart.HeaderColor = System.Drawing.Color.Black;
            this._iChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._iChart.HeaderVisible = false;
            this._iChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._iChart.InnerBorderLength = 0;
            this._iChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._iChart.LegendBkColor = System.Drawing.Color.White;
            this._iChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._iChart.LegendVisible = false;
            this._iChart.Location = new System.Drawing.Point(43, 38);
            this._iChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._iChart.MiddleBorderLength = 0;
            this._iChart.Name = "_iChart";
            this._iChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._iChart.OuterBorderLength = 0;
            this._iChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._iChart.Precision = 0;
            this._iChart.RoundRadius = 10;
            this._iChart.ShadowColor = System.Drawing.Color.DimGray;
            this._iChart.ShadowDepth = 8;
            this._iChart.ShadowRate = 0.5F;
            this._iChart.Size = new System.Drawing.Size(972, 497);
            this._iChart.TabIndex = 33;
            this._iChart.Text = "daS_Net_XYChart1";
            this._iChart.XMax = 100D;
            this._iChart.XMin = 0D;
            this._iChart.XScaleColor = System.Drawing.Color.Black;
            this._iChart.XScaleVisible = true;
            this._iChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this._currentChartDecreaseButton, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this._currentChartIncreaseButton, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 4;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(34, 497);
            this.tableLayoutPanel9.TabIndex = 31;
            // 
            // _currentChartDecreaseButton
            // 
            this._currentChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentChartDecreaseButton.Enabled = false;
            this._currentChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentChartDecreaseButton.Location = new System.Drawing.Point(3, 251);
            this._currentChartDecreaseButton.Name = "_currentChartDecreaseButton";
            this._currentChartDecreaseButton.Size = new System.Drawing.Size(28, 24);
            this._currentChartDecreaseButton.TabIndex = 1;
            this._currentChartDecreaseButton.Text = "-";
            this._currentChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentChartIncreaseButton
            // 
            this._currentChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentChartIncreaseButton.Location = new System.Drawing.Point(3, 221);
            this._currentChartIncreaseButton.Name = "_currentChartIncreaseButton";
            this._currentChartIncreaseButton.Size = new System.Drawing.Size(28, 24);
            this._currentChartIncreaseButton.TabIndex = 0;
            this._currentChartIncreaseButton.Text = "+";
            this._currentChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.Location = new System.Drawing.Point(9, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "Y";
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 3;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.MarkersTable.Controls.Add(this._marker1TrackBar, 1, 0);
            this.MarkersTable.Controls.Add(this._marker2TrackBar, 1, 1);
            this.MarkersTable.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.MarkersTable.Controls.Add(this.panel4, 0, 2);
            this.MarkersTable.Controls.Add(this.label6, 2, 2);
            this.MarkersTable.Controls.Add(this.label9, 0, 0);
            this.MarkersTable.Controls.Add(this.label10, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 3;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.Size = new System.Drawing.Size(1058, 90);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // _marker1TrackBar
            // 
            this._marker1TrackBar.BackColor = System.Drawing.Color.LightSkyBlue;
            this._marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker1TrackBar.Location = new System.Drawing.Point(66, 4);
            this._marker1TrackBar.Name = "_marker1TrackBar";
            this._marker1TrackBar.Size = new System.Drawing.Size(927, 24);
            this._marker1TrackBar.TabIndex = 2;
            this._marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker1TrackBar.Scroll += new System.EventHandler(this._marker1TrackBar_Scroll);
            // 
            // _marker2TrackBar
            // 
            this._marker2TrackBar.BackColor = System.Drawing.Color.Violet;
            this._marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker2TrackBar.LargeChange = 0;
            this._marker2TrackBar.Location = new System.Drawing.Point(66, 35);
            this._marker2TrackBar.Maximum = 3400;
            this._marker2TrackBar.Name = "_marker2TrackBar";
            this._marker2TrackBar.Size = new System.Drawing.Size(927, 24);
            this._marker2TrackBar.TabIndex = 3;
            this._marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker2TrackBar.Scroll += new System.EventHandler(this._marker2TrackBar_Scroll);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel4.Controls.Add(this._measuringChart, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(63, 63);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(933, 30);
            this.tableLayoutPanel4.TabIndex = 35;
            // 
            // _measuringChart
            // 
            this._measuringChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._measuringChart.BkGradient = false;
            this._measuringChart.BkGradientAngle = 90;
            this._measuringChart.BkGradientColor = System.Drawing.Color.White;
            this._measuringChart.BkGradientRate = 0.5F;
            this._measuringChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._measuringChart.BkRestrictedToChartPanel = false;
            this._measuringChart.BkShinePosition = 1F;
            this._measuringChart.BkTransparency = 0F;
            this._measuringChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._measuringChart.BorderExteriorLength = 0;
            this._measuringChart.BorderGradientAngle = 225;
            this._measuringChart.BorderGradientLightPos1 = 1F;
            this._measuringChart.BorderGradientLightPos2 = -1F;
            this._measuringChart.BorderGradientRate = 0.5F;
            this._measuringChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._measuringChart.BorderLightIntermediateBrightness = 0F;
            this._measuringChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._measuringChart.ChartPanelBackColor = System.Drawing.Color.White;
            this._measuringChart.ChartPanelBkTransparency = 0F;
            this._measuringChart.ControlShadow = false;
            this._measuringChart.CoordinateAxesVisible = true;
            this._measuringChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._measuringChart.CoordinateXOrigin = 0D;
            this._measuringChart.CoordinateYMax = 1D;
            this._measuringChart.CoordinateYMin = -1D;
            this._measuringChart.CoordinateYOrigin = 0D;
            this._measuringChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._measuringChart.FooterColor = System.Drawing.Color.Black;
            this._measuringChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._measuringChart.FooterVisible = false;
            this._measuringChart.GridColor = System.Drawing.Color.Gray;
            this._measuringChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._measuringChart.GridVisible = true;
            this._measuringChart.GridXSubTicker = 0;
            this._measuringChart.GridXTicker = 10;
            this._measuringChart.GridYSubTicker = 0;
            this._measuringChart.GridYTicker = 2;
            this._measuringChart.HeaderColor = System.Drawing.Color.Black;
            this._measuringChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._measuringChart.HeaderVisible = false;
            this._measuringChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._measuringChart.InnerBorderLength = 0;
            this._measuringChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._measuringChart.LegendBkColor = System.Drawing.Color.White;
            this._measuringChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._measuringChart.LegendVisible = false;
            this._measuringChart.Location = new System.Drawing.Point(16, 0);
            this._measuringChart.Margin = new System.Windows.Forms.Padding(0);
            this._measuringChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._measuringChart.MiddleBorderLength = 0;
            this._measuringChart.Name = "_measuringChart";
            this._measuringChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._measuringChart.OuterBorderLength = 0;
            this._measuringChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._measuringChart.Precision = 0;
            this._measuringChart.RoundRadius = 10;
            this._measuringChart.ShadowColor = System.Drawing.Color.DimGray;
            this._measuringChart.ShadowDepth = 8;
            this._measuringChart.ShadowRate = 0.5F;
            this._measuringChart.Size = new System.Drawing.Size(900, 30);
            this._measuringChart.TabIndex = 34;
            this._measuringChart.Text = "daS_Net_XYChart1";
            this._measuringChart.XMax = 100D;
            this._measuringChart.XMin = 0D;
            this._measuringChart.XScaleColor = System.Drawing.Color.Black;
            this._measuringChart.XScaleVisible = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(4, 66);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(44, 24);
            this.panel4.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Время";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1000, 69);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "мс";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 30);
            this.label9.TabIndex = 37;
            this.label9.Text = "Маркер 1";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 30);
            this.label10.TabIndex = 38;
            this.label10.Text = "Маркер 2";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1067, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(294, 537);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this._markerScrollPanel);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(293, 535);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мгновенные значения";
            // 
            // _markerScrollPanel
            // 
            this._markerScrollPanel.AutoScroll = true;
            this._markerScrollPanel.Controls.Add(this._marker2Box);
            this._markerScrollPanel.Controls.Add(this._marker1Box);
            this._markerScrollPanel.Controls.Add(this.groupBox4);
            this._markerScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._markerScrollPanel.Location = new System.Drawing.Point(0, 13);
            this._markerScrollPanel.Name = "_markerScrollPanel";
            this._markerScrollPanel.Size = new System.Drawing.Size(293, 522);
            this._markerScrollPanel.TabIndex = 3;
            // 
            // _marker2Box
            // 
            this._marker2Box.Controls.Add(this.groupBox5);
            this._marker2Box.Controls.Add(this.groupBox3);
            this._marker2Box.Controls.Add(this.groupBox12);
            this._marker2Box.Location = new System.Drawing.Point(194, 3);
            this._marker2Box.Name = "_marker2Box";
            this._marker2Box.Size = new System.Drawing.Size(129, 1001);
            this._marker2Box.TabIndex = 3;
            this._marker2Box.TabStop = false;
            this._marker2Box.Text = "Маркер 2";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._marker2D40);
            this.groupBox5.Controls.Add(this._marker2D31);
            this.groupBox5.Controls.Add(this._marker2D30);
            this.groupBox5.Controls.Add(this._marker2D29);
            this.groupBox5.Controls.Add(this._marker2D39);
            this.groupBox5.Controls.Add(this._marker2D38);
            this.groupBox5.Controls.Add(this._marker2D37);
            this.groupBox5.Controls.Add(this._marker2D36);
            this.groupBox5.Controls.Add(this._marker2D35);
            this.groupBox5.Controls.Add(this._marker2D34);
            this.groupBox5.Controls.Add(this._marker2D33);
            this.groupBox5.Controls.Add(this._marker2D32);
            this.groupBox5.Controls.Add(this._marker2D28);
            this.groupBox5.Controls.Add(this._marker2D27);
            this.groupBox5.Controls.Add(this._marker2D26);
            this.groupBox5.Controls.Add(this._marker2D25);
            this.groupBox5.Controls.Add(this._marker2D24);
            this.groupBox5.Controls.Add(this._marker2D23);
            this.groupBox5.Controls.Add(this._marker2D22);
            this.groupBox5.Controls.Add(this._marker2D21);
            this.groupBox5.Controls.Add(this._marker2D20);
            this.groupBox5.Controls.Add(this._marker2D11);
            this.groupBox5.Controls.Add(this._marker2D10);
            this.groupBox5.Controls.Add(this._marker2D9);
            this.groupBox5.Controls.Add(this._marker2D19);
            this.groupBox5.Controls.Add(this._marker2D18);
            this.groupBox5.Controls.Add(this._marker2D17);
            this.groupBox5.Controls.Add(this._marker2D16);
            this.groupBox5.Controls.Add(this._marker2D15);
            this.groupBox5.Controls.Add(this._marker2D14);
            this.groupBox5.Controls.Add(this._marker2D13);
            this.groupBox5.Controls.Add(this._marker2D12);
            this.groupBox5.Controls.Add(this._marker2D8);
            this.groupBox5.Controls.Add(this._marker2D7);
            this.groupBox5.Controls.Add(this._marker2D6);
            this.groupBox5.Controls.Add(this._marker2D5);
            this.groupBox5.Controls.Add(this._marker2D4);
            this.groupBox5.Controls.Add(this._marker2D3);
            this.groupBox5.Controls.Add(this._marker2D2);
            this.groupBox5.Controls.Add(this._marker2D1);
            this.groupBox5.Location = new System.Drawing.Point(16, 212);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(119, 421);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Дискреты";
            // 
            // _marker2D40
            // 
            this._marker2D40.AutoSize = true;
            this._marker2D40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D40.Location = new System.Drawing.Point(62, 396);
            this._marker2D40.Name = "_marker2D40";
            this._marker2D40.Size = new System.Drawing.Size(40, 13);
            this._marker2D40.TabIndex = 55;
            this._marker2D40.Text = "Д40 = ";
            // 
            // _marker2D31
            // 
            this._marker2D31.AutoSize = true;
            this._marker2D31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D31.Location = new System.Drawing.Point(63, 216);
            this._marker2D31.Name = "_marker2D31";
            this._marker2D31.Size = new System.Drawing.Size(40, 13);
            this._marker2D31.TabIndex = 54;
            this._marker2D31.Text = "Д31 = ";
            // 
            // _marker2D30
            // 
            this._marker2D30.AutoSize = true;
            this._marker2D30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D30.Location = new System.Drawing.Point(63, 196);
            this._marker2D30.Name = "_marker2D30";
            this._marker2D30.Size = new System.Drawing.Size(40, 13);
            this._marker2D30.TabIndex = 53;
            this._marker2D30.Text = "Д30 = ";
            // 
            // _marker2D29
            // 
            this._marker2D29.AutoSize = true;
            this._marker2D29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D29.Location = new System.Drawing.Point(63, 176);
            this._marker2D29.Name = "_marker2D29";
            this._marker2D29.Size = new System.Drawing.Size(40, 13);
            this._marker2D29.TabIndex = 52;
            this._marker2D29.Text = "Д29 = ";
            // 
            // _marker2D39
            // 
            this._marker2D39.AutoSize = true;
            this._marker2D39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D39.Location = new System.Drawing.Point(62, 376);
            this._marker2D39.Name = "_marker2D39";
            this._marker2D39.Size = new System.Drawing.Size(40, 13);
            this._marker2D39.TabIndex = 51;
            this._marker2D39.Text = "Д39 = ";
            // 
            // _marker2D38
            // 
            this._marker2D38.AutoSize = true;
            this._marker2D38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D38.Location = new System.Drawing.Point(62, 356);
            this._marker2D38.Name = "_marker2D38";
            this._marker2D38.Size = new System.Drawing.Size(40, 13);
            this._marker2D38.TabIndex = 50;
            this._marker2D38.Text = "Д38 = ";
            // 
            // _marker2D37
            // 
            this._marker2D37.AutoSize = true;
            this._marker2D37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D37.Location = new System.Drawing.Point(62, 336);
            this._marker2D37.Name = "_marker2D37";
            this._marker2D37.Size = new System.Drawing.Size(40, 13);
            this._marker2D37.TabIndex = 49;
            this._marker2D37.Text = "Д37 = ";
            // 
            // _marker2D36
            // 
            this._marker2D36.AutoSize = true;
            this._marker2D36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D36.Location = new System.Drawing.Point(62, 316);
            this._marker2D36.Name = "_marker2D36";
            this._marker2D36.Size = new System.Drawing.Size(40, 13);
            this._marker2D36.TabIndex = 48;
            this._marker2D36.Text = "Д36 = ";
            // 
            // _marker2D35
            // 
            this._marker2D35.AutoSize = true;
            this._marker2D35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D35.Location = new System.Drawing.Point(62, 296);
            this._marker2D35.Name = "_marker2D35";
            this._marker2D35.Size = new System.Drawing.Size(40, 13);
            this._marker2D35.TabIndex = 47;
            this._marker2D35.Text = "Д35 = ";
            // 
            // _marker2D34
            // 
            this._marker2D34.AutoSize = true;
            this._marker2D34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D34.Location = new System.Drawing.Point(62, 276);
            this._marker2D34.Name = "_marker2D34";
            this._marker2D34.Size = new System.Drawing.Size(40, 13);
            this._marker2D34.TabIndex = 46;
            this._marker2D34.Text = "Д34 = ";
            // 
            // _marker2D33
            // 
            this._marker2D33.AutoSize = true;
            this._marker2D33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D33.Location = new System.Drawing.Point(62, 256);
            this._marker2D33.Name = "_marker2D33";
            this._marker2D33.Size = new System.Drawing.Size(40, 13);
            this._marker2D33.TabIndex = 45;
            this._marker2D33.Text = "Д33 = ";
            // 
            // _marker2D32
            // 
            this._marker2D32.AutoSize = true;
            this._marker2D32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D32.Location = new System.Drawing.Point(62, 236);
            this._marker2D32.Name = "_marker2D32";
            this._marker2D32.Size = new System.Drawing.Size(40, 13);
            this._marker2D32.TabIndex = 44;
            this._marker2D32.Text = "Д32 = ";
            // 
            // _marker2D28
            // 
            this._marker2D28.AutoSize = true;
            this._marker2D28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D28.Location = new System.Drawing.Point(63, 156);
            this._marker2D28.Name = "_marker2D28";
            this._marker2D28.Size = new System.Drawing.Size(40, 13);
            this._marker2D28.TabIndex = 43;
            this._marker2D28.Text = "Д28 = ";
            // 
            // _marker2D27
            // 
            this._marker2D27.AutoSize = true;
            this._marker2D27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D27.Location = new System.Drawing.Point(63, 136);
            this._marker2D27.Name = "_marker2D27";
            this._marker2D27.Size = new System.Drawing.Size(40, 13);
            this._marker2D27.TabIndex = 42;
            this._marker2D27.Text = "Д27 = ";
            // 
            // _marker2D26
            // 
            this._marker2D26.AutoSize = true;
            this._marker2D26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D26.Location = new System.Drawing.Point(63, 116);
            this._marker2D26.Name = "_marker2D26";
            this._marker2D26.Size = new System.Drawing.Size(40, 13);
            this._marker2D26.TabIndex = 41;
            this._marker2D26.Text = "Д26 = ";
            // 
            // _marker2D25
            // 
            this._marker2D25.AutoSize = true;
            this._marker2D25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D25.Location = new System.Drawing.Point(63, 96);
            this._marker2D25.Name = "_marker2D25";
            this._marker2D25.Size = new System.Drawing.Size(40, 13);
            this._marker2D25.TabIndex = 40;
            this._marker2D25.Text = "Д25 = ";
            // 
            // _marker2D24
            // 
            this._marker2D24.AutoSize = true;
            this._marker2D24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D24.Location = new System.Drawing.Point(63, 76);
            this._marker2D24.Name = "_marker2D24";
            this._marker2D24.Size = new System.Drawing.Size(40, 13);
            this._marker2D24.TabIndex = 39;
            this._marker2D24.Text = "Д24 = ";
            // 
            // _marker2D23
            // 
            this._marker2D23.AutoSize = true;
            this._marker2D23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D23.Location = new System.Drawing.Point(63, 56);
            this._marker2D23.Name = "_marker2D23";
            this._marker2D23.Size = new System.Drawing.Size(40, 13);
            this._marker2D23.TabIndex = 38;
            this._marker2D23.Text = "Д23 = ";
            // 
            // _marker2D22
            // 
            this._marker2D22.AutoSize = true;
            this._marker2D22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D22.Location = new System.Drawing.Point(62, 36);
            this._marker2D22.Name = "_marker2D22";
            this._marker2D22.Size = new System.Drawing.Size(40, 13);
            this._marker2D22.TabIndex = 37;
            this._marker2D22.Text = "Д22 = ";
            // 
            // _marker2D21
            // 
            this._marker2D21.AutoSize = true;
            this._marker2D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D21.Location = new System.Drawing.Point(62, 16);
            this._marker2D21.Name = "_marker2D21";
            this._marker2D21.Size = new System.Drawing.Size(40, 13);
            this._marker2D21.TabIndex = 36;
            this._marker2D21.Text = "Д21 = ";
            // 
            // _marker2D20
            // 
            this._marker2D20.AutoSize = true;
            this._marker2D20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D20.Location = new System.Drawing.Point(5, 396);
            this._marker2D20.Name = "_marker2D20";
            this._marker2D20.Size = new System.Drawing.Size(40, 13);
            this._marker2D20.TabIndex = 35;
            this._marker2D20.Text = "Д20 = ";
            // 
            // _marker2D11
            // 
            this._marker2D11.AutoSize = true;
            this._marker2D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D11.Location = new System.Drawing.Point(6, 216);
            this._marker2D11.Name = "_marker2D11";
            this._marker2D11.Size = new System.Drawing.Size(40, 13);
            this._marker2D11.TabIndex = 34;
            this._marker2D11.Text = "Д11 = ";
            // 
            // _marker2D10
            // 
            this._marker2D10.AutoSize = true;
            this._marker2D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D10.Location = new System.Drawing.Point(6, 196);
            this._marker2D10.Name = "_marker2D10";
            this._marker2D10.Size = new System.Drawing.Size(40, 13);
            this._marker2D10.TabIndex = 33;
            this._marker2D10.Text = "Д10 = ";
            // 
            // _marker2D9
            // 
            this._marker2D9.AutoSize = true;
            this._marker2D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D9.Location = new System.Drawing.Point(6, 176);
            this._marker2D9.Name = "_marker2D9";
            this._marker2D9.Size = new System.Drawing.Size(34, 13);
            this._marker2D9.TabIndex = 32;
            this._marker2D9.Text = "Д9 = ";
            // 
            // _marker2D19
            // 
            this._marker2D19.AutoSize = true;
            this._marker2D19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D19.Location = new System.Drawing.Point(5, 376);
            this._marker2D19.Name = "_marker2D19";
            this._marker2D19.Size = new System.Drawing.Size(40, 13);
            this._marker2D19.TabIndex = 31;
            this._marker2D19.Text = "Д19 = ";
            // 
            // _marker2D18
            // 
            this._marker2D18.AutoSize = true;
            this._marker2D18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D18.Location = new System.Drawing.Point(5, 356);
            this._marker2D18.Name = "_marker2D18";
            this._marker2D18.Size = new System.Drawing.Size(40, 13);
            this._marker2D18.TabIndex = 30;
            this._marker2D18.Text = "Д18 = ";
            // 
            // _marker2D17
            // 
            this._marker2D17.AutoSize = true;
            this._marker2D17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D17.Location = new System.Drawing.Point(5, 336);
            this._marker2D17.Name = "_marker2D17";
            this._marker2D17.Size = new System.Drawing.Size(40, 13);
            this._marker2D17.TabIndex = 29;
            this._marker2D17.Text = "Д17 = ";
            // 
            // _marker2D16
            // 
            this._marker2D16.AutoSize = true;
            this._marker2D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D16.Location = new System.Drawing.Point(5, 316);
            this._marker2D16.Name = "_marker2D16";
            this._marker2D16.Size = new System.Drawing.Size(40, 13);
            this._marker2D16.TabIndex = 28;
            this._marker2D16.Text = "Д16 = ";
            // 
            // _marker2D15
            // 
            this._marker2D15.AutoSize = true;
            this._marker2D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D15.Location = new System.Drawing.Point(5, 296);
            this._marker2D15.Name = "_marker2D15";
            this._marker2D15.Size = new System.Drawing.Size(40, 13);
            this._marker2D15.TabIndex = 27;
            this._marker2D15.Text = "Д15 = ";
            // 
            // _marker2D14
            // 
            this._marker2D14.AutoSize = true;
            this._marker2D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D14.Location = new System.Drawing.Point(5, 276);
            this._marker2D14.Name = "_marker2D14";
            this._marker2D14.Size = new System.Drawing.Size(40, 13);
            this._marker2D14.TabIndex = 26;
            this._marker2D14.Text = "Д14 = ";
            // 
            // _marker2D13
            // 
            this._marker2D13.AutoSize = true;
            this._marker2D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D13.Location = new System.Drawing.Point(5, 256);
            this._marker2D13.Name = "_marker2D13";
            this._marker2D13.Size = new System.Drawing.Size(40, 13);
            this._marker2D13.TabIndex = 25;
            this._marker2D13.Text = "Д13 = ";
            // 
            // _marker2D12
            // 
            this._marker2D12.AutoSize = true;
            this._marker2D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D12.Location = new System.Drawing.Point(5, 236);
            this._marker2D12.Name = "_marker2D12";
            this._marker2D12.Size = new System.Drawing.Size(40, 13);
            this._marker2D12.TabIndex = 24;
            this._marker2D12.Text = "Д12 = ";
            // 
            // _marker2D8
            // 
            this._marker2D8.AutoSize = true;
            this._marker2D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D8.Location = new System.Drawing.Point(6, 156);
            this._marker2D8.Name = "_marker2D8";
            this._marker2D8.Size = new System.Drawing.Size(34, 13);
            this._marker2D8.TabIndex = 23;
            this._marker2D8.Text = "Д8 = ";
            // 
            // _marker2D7
            // 
            this._marker2D7.AutoSize = true;
            this._marker2D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D7.Location = new System.Drawing.Point(6, 136);
            this._marker2D7.Name = "_marker2D7";
            this._marker2D7.Size = new System.Drawing.Size(34, 13);
            this._marker2D7.TabIndex = 22;
            this._marker2D7.Text = "Д7 = ";
            // 
            // _marker2D6
            // 
            this._marker2D6.AutoSize = true;
            this._marker2D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D6.Location = new System.Drawing.Point(6, 116);
            this._marker2D6.Name = "_marker2D6";
            this._marker2D6.Size = new System.Drawing.Size(34, 13);
            this._marker2D6.TabIndex = 21;
            this._marker2D6.Text = "Д6 = ";
            // 
            // _marker2D5
            // 
            this._marker2D5.AutoSize = true;
            this._marker2D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D5.Location = new System.Drawing.Point(6, 96);
            this._marker2D5.Name = "_marker2D5";
            this._marker2D5.Size = new System.Drawing.Size(34, 13);
            this._marker2D5.TabIndex = 20;
            this._marker2D5.Text = "Д5 = ";
            // 
            // _marker2D4
            // 
            this._marker2D4.AutoSize = true;
            this._marker2D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D4.Location = new System.Drawing.Point(6, 76);
            this._marker2D4.Name = "_marker2D4";
            this._marker2D4.Size = new System.Drawing.Size(34, 13);
            this._marker2D4.TabIndex = 19;
            this._marker2D4.Text = "Д4 = ";
            // 
            // _marker2D3
            // 
            this._marker2D3.AutoSize = true;
            this._marker2D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D3.Location = new System.Drawing.Point(6, 56);
            this._marker2D3.Name = "_marker2D3";
            this._marker2D3.Size = new System.Drawing.Size(34, 13);
            this._marker2D3.TabIndex = 18;
            this._marker2D3.Text = "Д3 = ";
            // 
            // _marker2D2
            // 
            this._marker2D2.AutoSize = true;
            this._marker2D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D2.Location = new System.Drawing.Point(6, 36);
            this._marker2D2.Name = "_marker2D2";
            this._marker2D2.Size = new System.Drawing.Size(34, 13);
            this._marker2D2.TabIndex = 17;
            this._marker2D2.Text = "Д2 = ";
            // 
            // _marker2D1
            // 
            this._marker2D1.AutoSize = true;
            this._marker2D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D1.Location = new System.Drawing.Point(6, 16);
            this._marker2D1.Name = "_marker2D1";
            this._marker2D1.Size = new System.Drawing.Size(34, 13);
            this._marker2D1.TabIndex = 16;
            this._marker2D1.Text = "Д1 = ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._marker2U4);
            this.groupBox3.Controls.Add(this._marker2U3);
            this.groupBox3.Controls.Add(this._marker2U2);
            this.groupBox3.Controls.Add(this._marker2U1);
            this.groupBox3.Location = new System.Drawing.Point(7, 111);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(119, 95);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Напряжения";
            // 
            // _marker2U4
            // 
            this._marker2U4.AutoSize = true;
            this._marker2U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U4.Location = new System.Drawing.Point(6, 78);
            this._marker2U4.Name = "_marker2U4";
            this._marker2U4.Size = new System.Drawing.Size(28, 13);
            this._marker2U4.TabIndex = 3;
            this._marker2U4.Text = "In = ";
            // 
            // _marker2U3
            // 
            this._marker2U3.AutoSize = true;
            this._marker2U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U3.Location = new System.Drawing.Point(6, 58);
            this._marker2U3.Name = "_marker2U3";
            this._marker2U3.Size = new System.Drawing.Size(28, 13);
            this._marker2U3.TabIndex = 2;
            this._marker2U3.Text = "Ic = ";
            // 
            // _marker2U2
            // 
            this._marker2U2.AutoSize = true;
            this._marker2U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U2.Location = new System.Drawing.Point(6, 38);
            this._marker2U2.Name = "_marker2U2";
            this._marker2U2.Size = new System.Drawing.Size(28, 13);
            this._marker2U2.TabIndex = 1;
            this._marker2U2.Text = "Ib = ";
            // 
            // _marker2U1
            // 
            this._marker2U1.AutoSize = true;
            this._marker2U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U1.Location = new System.Drawing.Point(6, 18);
            this._marker2U1.Name = "_marker2U1";
            this._marker2U1.Size = new System.Drawing.Size(28, 13);
            this._marker2U1.TabIndex = 0;
            this._marker2U1.Text = "Ia = ";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._marker2I4);
            this.groupBox12.Controls.Add(this._marker2I3);
            this.groupBox12.Controls.Add(this._marker2I2);
            this.groupBox12.Controls.Add(this._marker2I1);
            this.groupBox12.Location = new System.Drawing.Point(7, 17);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(119, 95);
            this.groupBox12.TabIndex = 40;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Токи";
            // 
            // _marker2I4
            // 
            this._marker2I4.AutoSize = true;
            this._marker2I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I4.Location = new System.Drawing.Point(6, 78);
            this._marker2I4.Name = "_marker2I4";
            this._marker2I4.Size = new System.Drawing.Size(25, 13);
            this._marker2I4.TabIndex = 3;
            this._marker2I4.Text = "In =";
            // 
            // _marker2I3
            // 
            this._marker2I3.AutoSize = true;
            this._marker2I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I3.Location = new System.Drawing.Point(6, 58);
            this._marker2I3.Name = "_marker2I3";
            this._marker2I3.Size = new System.Drawing.Size(25, 13);
            this._marker2I3.TabIndex = 2;
            this._marker2I3.Text = "Ic =";
            // 
            // _marker2I2
            // 
            this._marker2I2.AutoSize = true;
            this._marker2I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I2.Location = new System.Drawing.Point(6, 38);
            this._marker2I2.Name = "_marker2I2";
            this._marker2I2.Size = new System.Drawing.Size(25, 13);
            this._marker2I2.TabIndex = 1;
            this._marker2I2.Text = "Ib =";
            // 
            // _marker2I1
            // 
            this._marker2I1.AutoSize = true;
            this._marker2I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I1.Location = new System.Drawing.Point(6, 18);
            this._marker2I1.Name = "_marker2I1";
            this._marker2I1.Size = new System.Drawing.Size(25, 13);
            this._marker2I1.TabIndex = 0;
            this._marker2I1.Text = "Ia =";
            // 
            // _marker1Box
            // 
            this._marker1Box.Controls.Add(this.groupBox2);
            this._marker1Box.Controls.Add(this.groupBox11);
            this._marker1Box.Controls.Add(this.groupBox8);
            this._marker1Box.Location = new System.Drawing.Point(3, 3);
            this._marker1Box.Name = "_marker1Box";
            this._marker1Box.Size = new System.Drawing.Size(185, 1520);
            this._marker1Box.TabIndex = 0;
            this._marker1Box.TabStop = false;
            this._marker1Box.Text = "Маркер 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._marker1U4);
            this.groupBox2.Controls.Add(this._marker1U3);
            this.groupBox2.Controls.Add(this._marker1U2);
            this.groupBox2.Controls.Add(this._marker1U1);
            this.groupBox2.Location = new System.Drawing.Point(5, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(174, 95);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Напряжения";
            // 
            // _marker1U4
            // 
            this._marker1U4.AutoSize = true;
            this._marker1U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U4.Location = new System.Drawing.Point(6, 78);
            this._marker1U4.Name = "_marker1U4";
            this._marker1U4.Size = new System.Drawing.Size(28, 13);
            this._marker1U4.TabIndex = 3;
            this._marker1U4.Text = "In = ";
            // 
            // _marker1U3
            // 
            this._marker1U3.AutoSize = true;
            this._marker1U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U3.Location = new System.Drawing.Point(6, 58);
            this._marker1U3.Name = "_marker1U3";
            this._marker1U3.Size = new System.Drawing.Size(28, 13);
            this._marker1U3.TabIndex = 2;
            this._marker1U3.Text = "Ic = ";
            // 
            // _marker1U2
            // 
            this._marker1U2.AutoSize = true;
            this._marker1U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U2.Location = new System.Drawing.Point(6, 38);
            this._marker1U2.Name = "_marker1U2";
            this._marker1U2.Size = new System.Drawing.Size(28, 13);
            this._marker1U2.TabIndex = 1;
            this._marker1U2.Text = "Ib = ";
            // 
            // _marker1U1
            // 
            this._marker1U1.AutoSize = true;
            this._marker1U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U1.Location = new System.Drawing.Point(6, 18);
            this._marker1U1.Name = "_marker1U1";
            this._marker1U1.Size = new System.Drawing.Size(28, 13);
            this._marker1U1.TabIndex = 0;
            this._marker1U1.Text = "Ia = ";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.label31);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.label3);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.label13);
            this.groupBox11.Controls.Add(this.label14);
            this.groupBox11.Controls.Add(this.label15);
            this.groupBox11.Controls.Add(this.label16);
            this.groupBox11.Controls.Add(this.label17);
            this.groupBox11.Controls.Add(this.label18);
            this.groupBox11.Controls.Add(this.label19);
            this.groupBox11.Controls.Add(this.label20);
            this.groupBox11.Controls.Add(this.label21);
            this.groupBox11.Controls.Add(this.label22);
            this.groupBox11.Controls.Add(this.label23);
            this.groupBox11.Controls.Add(this.label24);
            this.groupBox11.Controls.Add(this.label25);
            this.groupBox11.Controls.Add(this.label26);
            this.groupBox11.Controls.Add(this.label27);
            this.groupBox11.Controls.Add(this.label28);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Controls.Add(this._marker1D40);
            this.groupBox11.Controls.Add(this._marker1D31);
            this.groupBox11.Controls.Add(this._marker1D30);
            this.groupBox11.Controls.Add(this._marker1D29);
            this.groupBox11.Controls.Add(this._marker1D39);
            this.groupBox11.Controls.Add(this._marker1D38);
            this.groupBox11.Controls.Add(this._marker1D37);
            this.groupBox11.Controls.Add(this._marker1D36);
            this.groupBox11.Controls.Add(this._marker1D35);
            this.groupBox11.Controls.Add(this._marker1D34);
            this.groupBox11.Controls.Add(this._marker1D33);
            this.groupBox11.Controls.Add(this._marker1D32);
            this.groupBox11.Controls.Add(this._marker1D28);
            this.groupBox11.Controls.Add(this._marker1D27);
            this.groupBox11.Controls.Add(this._marker1D26);
            this.groupBox11.Controls.Add(this._marker1D25);
            this.groupBox11.Controls.Add(this._marker1D24);
            this.groupBox11.Controls.Add(this._marker1D23);
            this.groupBox11.Controls.Add(this._marker1D22);
            this.groupBox11.Controls.Add(this._marker1D21);
            this.groupBox11.Controls.Add(this._marker1D20);
            this.groupBox11.Controls.Add(this._marker1D11);
            this.groupBox11.Controls.Add(this._marker1D10);
            this.groupBox11.Controls.Add(this._marker1D9);
            this.groupBox11.Controls.Add(this._marker1D19);
            this.groupBox11.Controls.Add(this._marker1D18);
            this.groupBox11.Controls.Add(this._marker1D17);
            this.groupBox11.Controls.Add(this._marker1D16);
            this.groupBox11.Controls.Add(this._marker1D15);
            this.groupBox11.Controls.Add(this._marker1D14);
            this.groupBox11.Controls.Add(this._marker1D13);
            this.groupBox11.Controls.Add(this._marker1D12);
            this.groupBox11.Controls.Add(this._marker1D8);
            this.groupBox11.Controls.Add(this._marker1D7);
            this.groupBox11.Controls.Add(this._marker1D6);
            this.groupBox11.Controls.Add(this._marker1D5);
            this.groupBox11.Controls.Add(this._marker1D4);
            this.groupBox11.Controls.Add(this._marker1D3);
            this.groupBox11.Controls.Add(this._marker1D2);
            this.groupBox11.Controls.Add(this._marker1D1);
            this.groupBox11.Location = new System.Drawing.Point(5, 214);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(174, 1300);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дискреты";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(5, 1277);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 13);
            this.label30.TabIndex = 79;
            this.label30.Text = "Д40 = ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(5, 1257);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 13);
            this.label31.TabIndex = 78;
            this.label31.Text = "Д39 = ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(5, 1237);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(40, 13);
            this.label32.TabIndex = 77;
            this.label32.Text = "Д38 = ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(5, 1217);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(40, 13);
            this.label33.TabIndex = 76;
            this.label33.Text = "Д37 = ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(4, 1198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 75;
            this.label3.Text = "Д40 = ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(5, 1018);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 74;
            this.label11.Text = "I>> = ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(5, 998);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 73;
            this.label12.Text = "I>> ИО = ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(5, 978);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 72;
            this.label13.Text = "I>  = ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(4, 1178);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 71;
            this.label14.Text = "I2>> = ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(4, 1158);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 70;
            this.label15.Text = "I2>> ИО = ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(4, 1138);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 69;
            this.label16.Text = "I2>  = ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(4, 1118);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 68;
            this.label17.Text = "I2> ИО = ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(4, 1098);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 67;
            this.label18.Text = "I>>>>  = ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(4, 1078);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 66;
            this.label19.Text = "I>>>> ИО = ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(4, 1058);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 65;
            this.label20.Text = "I>>> = ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(4, 1038);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 64;
            this.label21.Text = "I>>> ИО = ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(5, 958);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 63;
            this.label22.Text = "I> ИО = ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(5, 938);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 62;
            this.label23.Text = "ВЛС8 = ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(5, 918);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 13);
            this.label24.TabIndex = 61;
            this.label24.Text = "ВЛС7 = ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(5, 898);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 13);
            this.label25.TabIndex = 60;
            this.label25.Text = "ВЛС6 = ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(5, 878);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 13);
            this.label26.TabIndex = 59;
            this.label26.Text = "ВЛС5 = ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(5, 858);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 58;
            this.label27.Text = "ВЛС4 = ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(4, 838);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 13);
            this.label28.TabIndex = 57;
            this.label28.Text = "ВЛС3 = ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(4, 818);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 13);
            this.label29.TabIndex = 56;
            this.label29.Text = "ВЛС2 = ";
            // 
            // _marker1D40
            // 
            this._marker1D40.AutoSize = true;
            this._marker1D40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D40.Location = new System.Drawing.Point(6, 798);
            this._marker1D40.Name = "_marker1D40";
            this._marker1D40.Size = new System.Drawing.Size(47, 13);
            this._marker1D40.TabIndex = 55;
            this._marker1D40.Text = "ВЛС1 = ";
            // 
            // _marker1D31
            // 
            this._marker1D31.AutoSize = true;
            this._marker1D31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D31.Location = new System.Drawing.Point(7, 618);
            this._marker1D31.Name = "_marker1D31";
            this._marker1D31.Size = new System.Drawing.Size(40, 13);
            this._marker1D31.TabIndex = 54;
            this._marker1D31.Text = "Д16 = ";
            // 
            // _marker1D30
            // 
            this._marker1D30.AutoSize = true;
            this._marker1D30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D30.Location = new System.Drawing.Point(7, 598);
            this._marker1D30.Name = "_marker1D30";
            this._marker1D30.Size = new System.Drawing.Size(40, 13);
            this._marker1D30.TabIndex = 53;
            this._marker1D30.Text = "Д15 = ";
            // 
            // _marker1D29
            // 
            this._marker1D29.AutoSize = true;
            this._marker1D29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D29.Location = new System.Drawing.Point(7, 578);
            this._marker1D29.Name = "_marker1D29";
            this._marker1D29.Size = new System.Drawing.Size(40, 13);
            this._marker1D29.TabIndex = 52;
            this._marker1D29.Text = "Д14 = ";
            // 
            // _marker1D39
            // 
            this._marker1D39.AutoSize = true;
            this._marker1D39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D39.Location = new System.Drawing.Point(6, 778);
            this._marker1D39.Name = "_marker1D39";
            this._marker1D39.Size = new System.Drawing.Size(40, 13);
            this._marker1D39.TabIndex = 51;
            this._marker1D39.Text = "ЛС8 = ";
            // 
            // _marker1D38
            // 
            this._marker1D38.AutoSize = true;
            this._marker1D38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D38.Location = new System.Drawing.Point(6, 758);
            this._marker1D38.Name = "_marker1D38";
            this._marker1D38.Size = new System.Drawing.Size(40, 13);
            this._marker1D38.TabIndex = 50;
            this._marker1D38.Text = "ЛС7 = ";
            // 
            // _marker1D37
            // 
            this._marker1D37.AutoSize = true;
            this._marker1D37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D37.Location = new System.Drawing.Point(6, 738);
            this._marker1D37.Name = "_marker1D37";
            this._marker1D37.Size = new System.Drawing.Size(40, 13);
            this._marker1D37.TabIndex = 49;
            this._marker1D37.Text = "ЛС6 = ";
            // 
            // _marker1D36
            // 
            this._marker1D36.AutoSize = true;
            this._marker1D36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D36.Location = new System.Drawing.Point(6, 718);
            this._marker1D36.Name = "_marker1D36";
            this._marker1D36.Size = new System.Drawing.Size(40, 13);
            this._marker1D36.TabIndex = 48;
            this._marker1D36.Text = "ЛС5 = ";
            // 
            // _marker1D35
            // 
            this._marker1D35.AutoSize = true;
            this._marker1D35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D35.Location = new System.Drawing.Point(6, 698);
            this._marker1D35.Name = "_marker1D35";
            this._marker1D35.Size = new System.Drawing.Size(40, 13);
            this._marker1D35.TabIndex = 47;
            this._marker1D35.Text = "ЛС4 = ";
            // 
            // _marker1D34
            // 
            this._marker1D34.AutoSize = true;
            this._marker1D34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D34.Location = new System.Drawing.Point(6, 678);
            this._marker1D34.Name = "_marker1D34";
            this._marker1D34.Size = new System.Drawing.Size(40, 13);
            this._marker1D34.TabIndex = 46;
            this._marker1D34.Text = "ЛС3 = ";
            // 
            // _marker1D33
            // 
            this._marker1D33.AutoSize = true;
            this._marker1D33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D33.Location = new System.Drawing.Point(6, 658);
            this._marker1D33.Name = "_marker1D33";
            this._marker1D33.Size = new System.Drawing.Size(40, 13);
            this._marker1D33.TabIndex = 45;
            this._marker1D33.Text = "ЛС2 = ";
            // 
            // _marker1D32
            // 
            this._marker1D32.AutoSize = true;
            this._marker1D32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D32.Location = new System.Drawing.Point(6, 638);
            this._marker1D32.Name = "_marker1D32";
            this._marker1D32.Size = new System.Drawing.Size(40, 13);
            this._marker1D32.TabIndex = 44;
            this._marker1D32.Text = "ЛС1 = ";
            // 
            // _marker1D28
            // 
            this._marker1D28.AutoSize = true;
            this._marker1D28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D28.Location = new System.Drawing.Point(7, 558);
            this._marker1D28.Name = "_marker1D28";
            this._marker1D28.Size = new System.Drawing.Size(40, 13);
            this._marker1D28.TabIndex = 43;
            this._marker1D28.Text = "Д13 = ";
            // 
            // _marker1D27
            // 
            this._marker1D27.AutoSize = true;
            this._marker1D27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D27.Location = new System.Drawing.Point(7, 538);
            this._marker1D27.Name = "_marker1D27";
            this._marker1D27.Size = new System.Drawing.Size(40, 13);
            this._marker1D27.TabIndex = 42;
            this._marker1D27.Text = "Д12 = ";
            // 
            // _marker1D26
            // 
            this._marker1D26.AutoSize = true;
            this._marker1D26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D26.Location = new System.Drawing.Point(7, 518);
            this._marker1D26.Name = "_marker1D26";
            this._marker1D26.Size = new System.Drawing.Size(40, 13);
            this._marker1D26.TabIndex = 41;
            this._marker1D26.Text = "Д11 = ";
            // 
            // _marker1D25
            // 
            this._marker1D25.AutoSize = true;
            this._marker1D25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D25.Location = new System.Drawing.Point(7, 498);
            this._marker1D25.Name = "_marker1D25";
            this._marker1D25.Size = new System.Drawing.Size(40, 13);
            this._marker1D25.TabIndex = 40;
            this._marker1D25.Text = "Д10 = ";
            // 
            // _marker1D24
            // 
            this._marker1D24.AutoSize = true;
            this._marker1D24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D24.Location = new System.Drawing.Point(7, 478);
            this._marker1D24.Name = "_marker1D24";
            this._marker1D24.Size = new System.Drawing.Size(34, 13);
            this._marker1D24.TabIndex = 39;
            this._marker1D24.Text = "Д9 = ";
            // 
            // _marker1D23
            // 
            this._marker1D23.AutoSize = true;
            this._marker1D23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D23.Location = new System.Drawing.Point(7, 458);
            this._marker1D23.Name = "_marker1D23";
            this._marker1D23.Size = new System.Drawing.Size(34, 13);
            this._marker1D23.TabIndex = 38;
            this._marker1D23.Text = "Д8 = ";
            // 
            // _marker1D22
            // 
            this._marker1D22.AutoSize = true;
            this._marker1D22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D22.Location = new System.Drawing.Point(6, 438);
            this._marker1D22.Name = "_marker1D22";
            this._marker1D22.Size = new System.Drawing.Size(34, 13);
            this._marker1D22.TabIndex = 37;
            this._marker1D22.Text = "Д7 = ";
            // 
            // _marker1D21
            // 
            this._marker1D21.AutoSize = true;
            this._marker1D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D21.Location = new System.Drawing.Point(6, 418);
            this._marker1D21.Name = "_marker1D21";
            this._marker1D21.Size = new System.Drawing.Size(34, 13);
            this._marker1D21.TabIndex = 36;
            this._marker1D21.Text = "Д6 = ";
            // 
            // _marker1D20
            // 
            this._marker1D20.AutoSize = true;
            this._marker1D20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D20.Location = new System.Drawing.Point(5, 396);
            this._marker1D20.Name = "_marker1D20";
            this._marker1D20.Size = new System.Drawing.Size(34, 13);
            this._marker1D20.TabIndex = 35;
            this._marker1D20.Text = "Д5 = ";
            // 
            // _marker1D11
            // 
            this._marker1D11.AutoSize = true;
            this._marker1D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D11.Location = new System.Drawing.Point(6, 216);
            this._marker1D11.Name = "_marker1D11";
            this._marker1D11.Size = new System.Drawing.Size(70, 13);
            this._marker1D11.TabIndex = 34;
            this._marker1D11.Text = "АВР блок. = ";
            // 
            // _marker1D10
            // 
            this._marker1D10.AutoSize = true;
            this._marker1D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D10.Location = new System.Drawing.Point(6, 196);
            this._marker1D10.Name = "_marker1D10";
            this._marker1D10.Size = new System.Drawing.Size(69, 13);
            this._marker1D10.TabIndex = 33;
            this._marker1D10.Text = "АВР откл. = ";
            // 
            // _marker1D9
            // 
            this._marker1D9.AutoSize = true;
            this._marker1D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D9.Location = new System.Drawing.Point(6, 176);
            this._marker1D9.Name = "_marker1D9";
            this._marker1D9.Size = new System.Drawing.Size(64, 13);
            this._marker1D9.TabIndex = 32;
            this._marker1D9.Text = "АВР вкл. = ";
            // 
            // _marker1D19
            // 
            this._marker1D19.AutoSize = true;
            this._marker1D19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D19.Location = new System.Drawing.Point(5, 376);
            this._marker1D19.Name = "_marker1D19";
            this._marker1D19.Size = new System.Drawing.Size(34, 13);
            this._marker1D19.TabIndex = 31;
            this._marker1D19.Text = "Д4 = ";
            // 
            // _marker1D18
            // 
            this._marker1D18.AutoSize = true;
            this._marker1D18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D18.Location = new System.Drawing.Point(5, 356);
            this._marker1D18.Name = "_marker1D18";
            this._marker1D18.Size = new System.Drawing.Size(34, 13);
            this._marker1D18.TabIndex = 30;
            this._marker1D18.Text = "Д3 = ";
            // 
            // _marker1D17
            // 
            this._marker1D17.AutoSize = true;
            this._marker1D17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D17.Location = new System.Drawing.Point(5, 336);
            this._marker1D17.Name = "_marker1D17";
            this._marker1D17.Size = new System.Drawing.Size(34, 13);
            this._marker1D17.TabIndex = 29;
            this._marker1D17.Text = "Д2 = ";
            // 
            // _marker1D16
            // 
            this._marker1D16.AutoSize = true;
            this._marker1D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D16.Location = new System.Drawing.Point(5, 316);
            this._marker1D16.Name = "_marker1D16";
            this._marker1D16.Size = new System.Drawing.Size(34, 13);
            this._marker1D16.TabIndex = 28;
            this._marker1D16.Text = "Д1 = ";
            // 
            // _marker1D15
            // 
            this._marker1D15.AutoSize = true;
            this._marker1D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D15.Location = new System.Drawing.Point(5, 296);
            this._marker1D15.Name = "_marker1D15";
            this._marker1D15.Size = new System.Drawing.Size(75, 13);
            this._marker1D15.TabIndex = 27;
            this._marker1D15.Text = "Ускорение = ";
            // 
            // _marker1D14
            // 
            this._marker1D14.AutoSize = true;
            this._marker1D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D14.Location = new System.Drawing.Point(5, 276);
            this._marker1D14.Name = "_marker1D14";
            this._marker1D14.Size = new System.Drawing.Size(49, 13);
            this._marker1D14.TabIndex = 26;
            this._marker1D14.Text = "УРОВ = ";
            // 
            // _marker1D13
            // 
            this._marker1D13.AutoSize = true;
            this._marker1D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D13.Location = new System.Drawing.Point(5, 256);
            this._marker1D13.Name = "_marker1D13";
            this._marker1D13.Size = new System.Drawing.Size(43, 13);
            this._marker1D13.TabIndex = 25;
            this._marker1D13.Text = "ЛЗШ = ";
            // 
            // _marker1D12
            // 
            this._marker1D12.AutoSize = true;
            this._marker1D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D12.Location = new System.Drawing.Point(5, 236);
            this._marker1D12.Name = "_marker1D12";
            this._marker1D12.Size = new System.Drawing.Size(44, 13);
            this._marker1D12.TabIndex = 24;
            this._marker1D12.Text = "Резерв";
            // 
            // _marker1D8
            // 
            this._marker1D8.AutoSize = true;
            this._marker1D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D8.Location = new System.Drawing.Point(6, 156);
            this._marker1D8.Name = "_marker1D8";
            this._marker1D8.Size = new System.Drawing.Size(52, 13);
            this._marker1D8.TabIndex = 23;
            this._marker1D8.Text = "Замля = ";
            // 
            // _marker1D7
            // 
            this._marker1D7.AutoSize = true;
            this._marker1D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D7.Location = new System.Drawing.Point(6, 136);
            this._marker1D7.Name = "_marker1D7";
            this._marker1D7.Size = new System.Drawing.Size(47, 13);
            this._marker1D7.TabIndex = 22;
            this._marker1D7.Text = "Откл. = ";
            // 
            // _marker1D6
            // 
            this._marker1D6.AutoSize = true;
            this._marker1D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D6.Location = new System.Drawing.Point(6, 116);
            this._marker1D6.Name = "_marker1D6";
            this._marker1D6.Size = new System.Drawing.Size(64, 13);
            this._marker1D6.TabIndex = 21;
            this._marker1D6.Text = "Сигнал-я = ";
            // 
            // _marker1D5
            // 
            this._marker1D5.AutoSize = true;
            this._marker1D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D5.Location = new System.Drawing.Point(6, 96);
            this._marker1D5.Name = "_marker1D5";
            this._marker1D5.Size = new System.Drawing.Size(56, 13);
            this._marker1D5.TabIndex = 20;
            this._marker1D5.Text = "Резерв = ";
            // 
            // _marker1D4
            // 
            this._marker1D4.AutoSize = true;
            this._marker1D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D4.Location = new System.Drawing.Point(6, 76);
            this._marker1D4.Name = "_marker1D4";
            this._marker1D4.Size = new System.Drawing.Size(77, 13);
            this._marker1D4.TabIndex = 19;
            this._marker1D4.Text = "Гр. уставок = ";
            // 
            // _marker1D3
            // 
            this._marker1D3.AutoSize = true;
            this._marker1D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D3.Location = new System.Drawing.Point(6, 56);
            this._marker1D3.Name = "_marker1D3";
            this._marker1D3.Size = new System.Drawing.Size(60, 13);
            this._marker1D3.TabIndex = 18;
            this._marker1D3.Text = "Неиспр. = ";
            // 
            // _marker1D2
            // 
            this._marker1D2.AutoSize = true;
            this._marker1D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D2.Location = new System.Drawing.Point(6, 36);
            this._marker1D2.Name = "_marker1D2";
            this._marker1D2.Size = new System.Drawing.Size(73, 13);
            this._marker1D2.TabIndex = 17;
            this._marker1D2.Text = "Вкл. выкл. = ";
            // 
            // _marker1D1
            // 
            this._marker1D1.AutoSize = true;
            this._marker1D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D1.Location = new System.Drawing.Point(6, 16);
            this._marker1D1.Name = "_marker1D1";
            this._marker1D1.Size = new System.Drawing.Size(79, 13);
            this._marker1D1.TabIndex = 16;
            this._marker1D1.Text = "Откл. выкл. = ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._marker1I4);
            this.groupBox8.Controls.Add(this._marker1I3);
            this.groupBox8.Controls.Add(this._marker1I2);
            this.groupBox8.Controls.Add(this._marker1I1);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(174, 95);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Токи";
            // 
            // _marker1I4
            // 
            this._marker1I4.AutoSize = true;
            this._marker1I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I4.Location = new System.Drawing.Point(6, 78);
            this._marker1I4.Name = "_marker1I4";
            this._marker1I4.Size = new System.Drawing.Size(28, 13);
            this._marker1I4.TabIndex = 3;
            this._marker1I4.Text = "In = ";
            // 
            // _marker1I3
            // 
            this._marker1I3.AutoSize = true;
            this._marker1I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I3.Location = new System.Drawing.Point(6, 58);
            this._marker1I3.Name = "_marker1I3";
            this._marker1I3.Size = new System.Drawing.Size(28, 13);
            this._marker1I3.TabIndex = 2;
            this._marker1I3.Text = "Ic = ";
            // 
            // _marker1I2
            // 
            this._marker1I2.AutoSize = true;
            this._marker1I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I2.Location = new System.Drawing.Point(6, 38);
            this._marker1I2.Name = "_marker1I2";
            this._marker1I2.Size = new System.Drawing.Size(28, 13);
            this._marker1I2.TabIndex = 1;
            this._marker1I2.Text = "Ib = ";
            // 
            // _marker1I1
            // 
            this._marker1I1.AutoSize = true;
            this._marker1I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I1.Location = new System.Drawing.Point(6, 18);
            this._marker1I1.Name = "_marker1I1";
            this._marker1I1.Size = new System.Drawing.Size(28, 13);
            this._marker1I1.TabIndex = 0;
            this._marker1I1.Text = "Ia = ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._deltaTimeBox);
            this.groupBox4.Controls.Add(this._marker2TimeBox);
            this.groupBox4.Controls.Add(this._marker1TimeBox);
            this.groupBox4.Location = new System.Drawing.Point(8, 1535);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(402, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Время";
            // 
            // _deltaTimeBox
            // 
            this._deltaTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._deltaTimeBox.Controls.Add(this._markerTimeDelta);
            this._deltaTimeBox.Location = new System.Drawing.Point(6, 46);
            this._deltaTimeBox.Name = "_deltaTimeBox";
            this._deltaTimeBox.Size = new System.Drawing.Size(257, 33);
            this._deltaTimeBox.TabIndex = 2;
            this._deltaTimeBox.TabStop = false;
            this._deltaTimeBox.Text = "Дельта";
            // 
            // _markerTimeDelta
            // 
            this._markerTimeDelta.AutoSize = true;
            this._markerTimeDelta.Location = new System.Drawing.Point(118, 16);
            this._markerTimeDelta.Name = "_markerTimeDelta";
            this._markerTimeDelta.Size = new System.Drawing.Size(30, 13);
            this._markerTimeDelta.TabIndex = 2;
            this._markerTimeDelta.Text = "0 мс";
            // 
            // _marker2TimeBox
            // 
            this._marker2TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker2TimeBox.Controls.Add(this._marker2Time);
            this._marker2TimeBox.Location = new System.Drawing.Point(144, 13);
            this._marker2TimeBox.Name = "_marker2TimeBox";
            this._marker2TimeBox.Size = new System.Drawing.Size(119, 33);
            this._marker2TimeBox.TabIndex = 1;
            this._marker2TimeBox.TabStop = false;
            this._marker2TimeBox.Text = "Маркер 2";
            // 
            // _marker2Time
            // 
            this._marker2Time.AutoSize = true;
            this._marker2Time.Location = new System.Drawing.Point(41, 16);
            this._marker2Time.Name = "_marker2Time";
            this._marker2Time.Size = new System.Drawing.Size(30, 13);
            this._marker2Time.TabIndex = 1;
            this._marker2Time.Text = "0 мс";
            // 
            // _marker1TimeBox
            // 
            this._marker1TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker1TimeBox.Controls.Add(this._marker1Time);
            this._marker1TimeBox.Location = new System.Drawing.Point(6, 13);
            this._marker1TimeBox.Name = "_marker1TimeBox";
            this._marker1TimeBox.Size = new System.Drawing.Size(118, 33);
            this._marker1TimeBox.TabIndex = 0;
            this._marker1TimeBox.TabStop = false;
            this._marker1TimeBox.Text = "Маркер 1";
            // 
            // _marker1Time
            // 
            this._marker1Time.AutoSize = true;
            this._marker1Time.Location = new System.Drawing.Point(39, 16);
            this._marker1Time.Name = "_marker1Time";
            this._marker1Time.Size = new System.Drawing.Size(30, 13);
            this._marker1Time.TabIndex = 0;
            this._marker1Time.Text = "0 мс";
            // 
            // _xDecreaseButton
            // 
            this._xDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xDecreaseButton.Location = new System.Drawing.Point(148, 3);
            this._xDecreaseButton.Name = "_xDecreaseButton";
            this._xDecreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xDecreaseButton.TabIndex = 3;
            this._xDecreaseButton.Text = "X -";
            this._xDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentСheckBox
            // 
            this._currentСheckBox.AutoSize = true;
            this._currentСheckBox.BackColor = System.Drawing.Color.Silver;
            this._currentСheckBox.Checked = true;
            this._currentСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._currentСheckBox.Location = new System.Drawing.Point(195, 3);
            this._currentСheckBox.Name = "_currentСheckBox";
            this._currentСheckBox.Size = new System.Drawing.Size(51, 17);
            this._currentСheckBox.TabIndex = 4;
            this._currentСheckBox.Text = "Токи";
            this._currentСheckBox.UseVisualStyleBackColor = false;
            this._currentСheckBox.CheckedChanged += new System.EventHandler(this._currentСonnectionsСheckBox_CheckedChanged);
            // 
            // _discrestsСheckBox
            // 
            this._discrestsСheckBox.AutoSize = true;
            this._discrestsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._discrestsСheckBox.Checked = true;
            this._discrestsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._discrestsСheckBox.Location = new System.Drawing.Point(361, 3);
            this._discrestsСheckBox.Name = "_discrestsСheckBox";
            this._discrestsСheckBox.Size = new System.Drawing.Size(78, 17);
            this._discrestsСheckBox.TabIndex = 5;
            this._discrestsСheckBox.Text = "Дискреты";
            this._discrestsСheckBox.UseVisualStyleBackColor = false;
            this._discrestsСheckBox.CheckedChanged += new System.EventHandler(this._discrestsСheckBox_CheckedChanged);
            // 
            // _channelsСheckBox
            // 
            this._channelsСheckBox.AutoSize = true;
            this._channelsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._channelsСheckBox.Checked = true;
            this._channelsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._channelsСheckBox.Location = new System.Drawing.Point(451, 3);
            this._channelsСheckBox.Name = "_channelsСheckBox";
            this._channelsСheckBox.Size = new System.Drawing.Size(65, 17);
            this._channelsСheckBox.TabIndex = 6;
            this._channelsСheckBox.Text = "Каналы";
            this._channelsСheckBox.UseVisualStyleBackColor = false;
            this._channelsСheckBox.CheckedChanged += new System.EventHandler(this._channelsСheckBox_CheckedChanged);
            // 
            // _markCheckBox
            // 
            this._markCheckBox.AutoSize = true;
            this._markCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markCheckBox.Location = new System.Drawing.Point(691, 3);
            this._markCheckBox.Name = "_markCheckBox";
            this._markCheckBox.Size = new System.Drawing.Size(58, 17);
            this._markCheckBox.TabIndex = 7;
            this._markCheckBox.Text = "Метки";
            this._markCheckBox.UseVisualStyleBackColor = false;
            // 
            // _markerCheckBox
            // 
            this._markerCheckBox.AutoSize = true;
            this._markerCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markerCheckBox.Location = new System.Drawing.Point(761, 3);
            this._markerCheckBox.Name = "_markerCheckBox";
            this._markerCheckBox.Size = new System.Drawing.Size(73, 17);
            this._markerCheckBox.TabIndex = 8;
            this._markerCheckBox.Text = "Маркеры";
            this._markerCheckBox.UseVisualStyleBackColor = false;
            this._markerCheckBox.CheckedChanged += new System.EventHandler(this._markerCheckBox_CheckedChanged);
            // 
            // _xIncreaseButton
            // 
            this._xIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xIncreaseButton.Location = new System.Drawing.Point(108, 3);
            this._xIncreaseButton.Name = "_xIncreaseButton";
            this._xIncreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xIncreaseButton.TabIndex = 2;
            this._xIncreaseButton.Text = "X +";
            this._xIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // _oscRunСheckBox
            // 
            this._oscRunСheckBox.AutoSize = true;
            this._oscRunСheckBox.BackColor = System.Drawing.Color.Silver;
            this._oscRunСheckBox.Location = new System.Drawing.Point(521, 3);
            this._oscRunСheckBox.Name = "_oscRunСheckBox";
            this._oscRunСheckBox.Size = new System.Drawing.Size(127, 17);
            this._oscRunСheckBox.TabIndex = 9;
            this._oscRunСheckBox.Text = "Пуск осциллографа";
            this._oscRunСheckBox.UseVisualStyleBackColor = false;
            this._oscRunСheckBox.CheckedChanged += new System.EventHandler(this._oscRunСheckBox_CheckedChanged);
            // 
            // _voltageСheckBox
            // 
            this._voltageСheckBox.AutoSize = true;
            this._voltageСheckBox.BackColor = System.Drawing.Color.Silver;
            this._voltageСheckBox.Checked = true;
            this._voltageСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._voltageСheckBox.Location = new System.Drawing.Point(256, 3);
            this._voltageСheckBox.Name = "_voltageСheckBox";
            this._voltageСheckBox.Size = new System.Drawing.Size(90, 17);
            this._voltageСheckBox.TabIndex = 10;
            this._voltageСheckBox.Text = "Напряжения";
            this._voltageСheckBox.UseVisualStyleBackColor = false;
            this._voltageСheckBox.CheckedChanged += new System.EventHandler(this._voltageСheckBox_CheckedChanged);
            // 
            // button89
            // 
            this.button89.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button89.BackColor = System.Drawing.Color.LightSlateGray;
            this.button89.ForeColor = System.Drawing.Color.Black;
            this.button89.Location = new System.Drawing.Point(0, 2564);
            this.button89.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(101, 20);
            this.button89.TabIndex = 133;
            this.button89.Text = "ССЛ22";
            this.button89.UseVisualStyleBackColor = false;
            this.button89.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button90
            // 
            this.button90.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button90.BackColor = System.Drawing.Color.LightSlateGray;
            this.button90.ForeColor = System.Drawing.Color.Black;
            this.button90.Location = new System.Drawing.Point(0, 2584);
            this.button90.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(101, 20);
            this.button90.TabIndex = 147;
            this.button90.Text = "ССЛ23";
            this.button90.UseVisualStyleBackColor = false;
            this.button90.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // button91
            // 
            this.button91.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button91.BackColor = System.Drawing.Color.LightSlateGray;
            this.button91.ForeColor = System.Drawing.Color.Black;
            this.button91.Location = new System.Drawing.Point(0, 2604);
            this.button91.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(101, 20);
            this.button91.TabIndex = 148;
            this.button91.Text = "ССЛ24";
            this.button91.UseVisualStyleBackColor = false;
            this.button91.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // Mr741OscilloscopeResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 568);
            this.Controls.Add(this._voltageСheckBox);
            this.Controls.Add(this._oscRunСheckBox);
            this.Controls.Add(this._markerCheckBox);
            this.Controls.Add(this._markCheckBox);
            this.Controls.Add(this._channelsСheckBox);
            this.Controls.Add(this._discrestsСheckBox);
            this.Controls.Add(this._currentСheckBox);
            this.Controls.Add(this._xDecreaseButton);
            this.Controls.Add(this._xIncreaseButton);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "Mr741OscilloscopeResultForm";
            this.Text = "Mr901OscilloscopeResultForm";
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this._discretsLayoutPanel.ResumeLayout(false);
            this._discretsLayoutPanel.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this._voltageLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this._currentsLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._markerScrollPanel.ResumeLayout(false);
            this._marker2Box.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this._marker1Box.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this._deltaTimeBox.ResumeLayout(false);
            this._deltaTimeBox.PerformLayout();
            this._marker2TimeBox.ResumeLayout(false);
            this._marker2TimeBox.PerformLayout();
            this._marker1TimeBox.ResumeLayout(false);
            this._marker1TimeBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel _discretsLayoutPanel;
        private BEMN_XY_Chart.DAS_Net_XYChart _discrestsChart;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TableLayoutPanel _voltageLayoutPanel;
        private System.Windows.Forms.VScrollBar _uScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _voltageChartDecreaseButton;
        private System.Windows.Forms.Button _voltageChartIncreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button _u4Button;
        private System.Windows.Forms.Button _u5Button;
        private System.Windows.Forms.Button _u2Button;
        private System.Windows.Forms.Button _u3Button;
        private System.Windows.Forms.Label label5;
        private BEMN_XY_Chart.DAS_Net_XYChart _uChart;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.TrackBar _marker1TrackBar;
        private System.Windows.Forms.TrackBar _marker2TrackBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox _deltaTimeBox;
        private System.Windows.Forms.GroupBox _marker2TimeBox;
        private System.Windows.Forms.GroupBox _marker1TimeBox;
        private System.Windows.Forms.GroupBox _marker1Box;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _discrete5Button;
        private System.Windows.Forms.Button _discrete6Button;
        private System.Windows.Forms.Button _discrete7Button;
        private System.Windows.Forms.Button _discrete8Button;
        private System.Windows.Forms.Button _discrete9Button;
        private System.Windows.Forms.Button _discrete10Button;
        private System.Windows.Forms.Button _discrete11Button;
        private System.Windows.Forms.Button _discrete12Button;
        private System.Windows.Forms.Button _discrete13Button;
        private System.Windows.Forms.Button _discrete14Button;
        private System.Windows.Forms.Button _discrete15Button;
        private System.Windows.Forms.Button _discrete16Button;
        private System.Windows.Forms.Button _discrete17Button;
        private System.Windows.Forms.Button _discrete18Button;
        private System.Windows.Forms.Button _discrete19Button;
        private System.Windows.Forms.Button _discrete20Button;
        private System.Windows.Forms.Button _discrete21Button;
        private System.Windows.Forms.Button _discrete22Button;
        private System.Windows.Forms.Button _discrete23Button;
        private System.Windows.Forms.Button _discrete24Button;
        private System.Windows.Forms.Button _discrete1Button;
        private System.Windows.Forms.Button _discrete2Button;
        private System.Windows.Forms.Button _discrete3Button;
        private System.Windows.Forms.Button _discrete4Button;
        private System.Windows.Forms.Button _xDecreaseButton;
        private System.Windows.Forms.CheckBox _currentСheckBox;
        private System.Windows.Forms.CheckBox _discrestsСheckBox;
        private System.Windows.Forms.CheckBox _channelsСheckBox;
        private System.Windows.Forms.CheckBox _markCheckBox;
        private System.Windows.Forms.CheckBox _markerCheckBox;
        private System.Windows.Forms.ToolTip _newMarkToolTip;
        private System.Windows.Forms.Label _marker1I4;
        private System.Windows.Forms.Label _marker1I3;
        private System.Windows.Forms.Label _marker1I2;
        private System.Windows.Forms.Label _marker1I1;
        private System.Windows.Forms.Label _marker1D22;
        private System.Windows.Forms.Label _marker1D21;
        private System.Windows.Forms.Label _marker1D20;
        private System.Windows.Forms.Label _marker1D11;
        private System.Windows.Forms.Label _marker1D10;
        private System.Windows.Forms.Label _marker1D9;
        private System.Windows.Forms.Label _marker1D19;
        private System.Windows.Forms.Label _marker1D18;
        private System.Windows.Forms.Label _marker1D17;
        private System.Windows.Forms.Label _marker1D16;
        private System.Windows.Forms.Label _marker1D15;
        private System.Windows.Forms.Label _marker1D14;
        private System.Windows.Forms.Label _marker1D13;
        private System.Windows.Forms.Label _marker1D12;
        private System.Windows.Forms.Label _marker1D8;
        private System.Windows.Forms.Label _marker1D7;
        private System.Windows.Forms.Label _marker1D6;
        private System.Windows.Forms.Label _marker1D5;
        private System.Windows.Forms.Label _marker1D4;
        private System.Windows.Forms.Label _marker1D3;
        private System.Windows.Forms.Label _marker1D2;
        private System.Windows.Forms.Label _marker1D1;
        private System.Windows.Forms.Label _marker1D24;
        private System.Windows.Forms.Label _marker1D23;
        private System.Windows.Forms.Panel _markerScrollPanel;
        private System.Windows.Forms.GroupBox _marker2Box;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label _marker2I4;
        private System.Windows.Forms.Label _marker2I3;
        private System.Windows.Forms.Label _marker2I2;
        private System.Windows.Forms.Label _marker2I1;
        private System.Windows.Forms.Button _xIncreaseButton;
        private System.Windows.Forms.Label _markerTimeDelta;
        private System.Windows.Forms.Label _marker2Time;
        private System.Windows.Forms.Label _marker1Time;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private BEMN_XY_Chart.DAS_Net_XYChart _measuringChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.CheckBox _oscRunСheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.TableLayoutPanel _currentsLayoutPanel;
        private System.Windows.Forms.VScrollBar _iScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button _i3Button;
        private System.Windows.Forms.Button _i4Button;
        private System.Windows.Forms.Button _i1Button;
        private System.Windows.Forms.Button _i2Button;
        private System.Windows.Forms.Label label7;
        private BEMN_XY_Chart.DAS_Net_XYChart _iChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button _currentChartDecreaseButton;
        private System.Windows.Forms.Button _currentChartIncreaseButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button _discrete25Button;
        private System.Windows.Forms.Button _discrete26Button;
        private System.Windows.Forms.Button _discrete27Button;
        private System.Windows.Forms.Button _discrete28Button;
        private System.Windows.Forms.Button _discrete29Button;
        private System.Windows.Forms.Button _discrete30Button;
        private System.Windows.Forms.Button _discrete31Button;
        private System.Windows.Forms.Button _discrete32Button;
        private System.Windows.Forms.Button _discrete33Button;
        private System.Windows.Forms.Button _discrete34Button;
        private System.Windows.Forms.Button _discrete35Button;
        private System.Windows.Forms.Button _discrete36Button;
        private System.Windows.Forms.Button _discrete37Button;
        private System.Windows.Forms.Button _discrete38Button;
        private System.Windows.Forms.Button _discrete39Button;
        private System.Windows.Forms.Button _discrete40Button;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label _marker2D40;
        private System.Windows.Forms.Label _marker2D31;
        private System.Windows.Forms.Label _marker2D30;
        private System.Windows.Forms.Label _marker2D29;
        private System.Windows.Forms.Label _marker2D39;
        private System.Windows.Forms.Label _marker2D38;
        private System.Windows.Forms.Label _marker2D37;
        private System.Windows.Forms.Label _marker2D36;
        private System.Windows.Forms.Label _marker2D35;
        private System.Windows.Forms.Label _marker2D34;
        private System.Windows.Forms.Label _marker2D33;
        private System.Windows.Forms.Label _marker2D32;
        private System.Windows.Forms.Label _marker2D28;
        private System.Windows.Forms.Label _marker2D27;
        private System.Windows.Forms.Label _marker2D26;
        private System.Windows.Forms.Label _marker2D25;
        private System.Windows.Forms.Label _marker2D24;
        private System.Windows.Forms.Label _marker2D23;
        private System.Windows.Forms.Label _marker2D22;
        private System.Windows.Forms.Label _marker2D21;
        private System.Windows.Forms.Label _marker2D20;
        private System.Windows.Forms.Label _marker2D11;
        private System.Windows.Forms.Label _marker2D10;
        private System.Windows.Forms.Label _marker2D9;
        private System.Windows.Forms.Label _marker2D19;
        private System.Windows.Forms.Label _marker2D18;
        private System.Windows.Forms.Label _marker2D17;
        private System.Windows.Forms.Label _marker2D16;
        private System.Windows.Forms.Label _marker2D15;
        private System.Windows.Forms.Label _marker2D14;
        private System.Windows.Forms.Label _marker2D13;
        private System.Windows.Forms.Label _marker2D12;
        private System.Windows.Forms.Label _marker2D8;
        private System.Windows.Forms.Label _marker2D7;
        private System.Windows.Forms.Label _marker2D6;
        private System.Windows.Forms.Label _marker2D5;
        private System.Windows.Forms.Label _marker2D4;
        private System.Windows.Forms.Label _marker2D3;
        private System.Windows.Forms.Label _marker2D2;
        private System.Windows.Forms.Label _marker2D1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _marker2U4;
        private System.Windows.Forms.Label _marker2U3;
        private System.Windows.Forms.Label _marker2U2;
        private System.Windows.Forms.Label _marker2U1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label _marker1U4;
        private System.Windows.Forms.Label _marker1U3;
        private System.Windows.Forms.Label _marker1U2;
        private System.Windows.Forms.Label _marker1U1;
        private System.Windows.Forms.Label _marker1D40;
        private System.Windows.Forms.Label _marker1D31;
        private System.Windows.Forms.Label _marker1D30;
        private System.Windows.Forms.Label _marker1D29;
        private System.Windows.Forms.Label _marker1D39;
        private System.Windows.Forms.Label _marker1D38;
        private System.Windows.Forms.Label _marker1D37;
        private System.Windows.Forms.Label _marker1D36;
        private System.Windows.Forms.Label _marker1D35;
        private System.Windows.Forms.Label _marker1D34;
        private System.Windows.Forms.Label _marker1D33;
        private System.Windows.Forms.Label _marker1D32;
        private System.Windows.Forms.Label _marker1D28;
        private System.Windows.Forms.Label _marker1D27;
        private System.Windows.Forms.Label _marker1D26;
        private System.Windows.Forms.Label _marker1D25;
        private System.Windows.Forms.CheckBox _voltageСheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button _u1Button;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button90;



    }
}