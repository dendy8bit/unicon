﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.Devices;
using BEMN.MBServer.Queries;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Drawing.Design;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using DescriptionAttribute=System.ComponentModel.DescriptionAttribute;
using BEMN.TZL.BSBGL;
using BEMN.TZL.Configuration.Structers;
using BEMN.TZL.NewOsc.Structers;
using BEMN.TZL.OldOsc;
using BEMN.TZL.Properties;
using BEMN.Framework;

namespace BEMN.TZL
{
    public class TZL : Device, IDeviceView, IDeviceVersion
    {
        #region Константы

        public const ulong TIMELIMIT = 3000000;
        public const ulong KZLIMIT = 4000;
        public const string TIMELIMIT_ERROR_MSG = "Введите число в диапазоне [0 - 3000000]";
        public const string KZLIMIT_ERROR_MSG = "Введите число в диапазоне [0 - 4000]";
        private const int OUTPUTLOGIC_BITS_CNT = 120;
        private const string VER_2 = "2.00 - 2.10";
        private const string VER_117_124 = "1.17 - 1.24";
        private const string VER_2_S = "2.00S - 2.10S";

        #endregion

        #region Системный журнал

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;

        //[XmlIgnore]
        //[Browsable(false)]
        //public MemoryEntity<SystemJournalStructTZL> SystemJournal { get; private set; }
        //[XmlIgnore]
        //[Browsable(false)]
        //public MemoryEntity<OneWordStruct> IndexSystemJournal { get; private set; }

        [Browsable(false)]
        [XmlIgnore]
        private bool _devicesInitialised;

        public struct SystemRecord
        {
            public string msg;
            public string time;
        }
        
        public CSystemJournal SystemJournal
        {
            get { return this._systemJournalRecords; }
            set { this._systemJournalRecords = value; }
        }

        public class CSystemJournal : ICollection
        {
            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);
            
            public SystemRecord this[int i]
            {
                get { return this._messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "Журнал пуст" == this._messages[i].msg;
            }

            public bool AddMessage(ushort[] value)
            {
                bool ret;
                //Common.SwapArrayItems(ref value);
                SystemRecord msg = this.CreateMessage(value);
                if ("Журнал пуст" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    this._messages.Add(msg);
                }
                return ret;

            }

            private SystemRecord CreateMessage(ushort[] value)
            {
                SystemRecord msg = new SystemRecord();

                switch (value[0])
                {
                    case 0:
                        msg.msg = "Журнал пуст";
                        break;
                    case 1:
                        msg.msg = "Потеря данных осциллографа";
                        break;
                    case 2:
                        msg.msg = "Ошибка хранения данных";
                        break;
                    case 3:
                        msg.msg = "Неисправность вн. шины";
                        break;
                    case 4:
                        msg.msg = "Вн. шина исправна";
                        break;
                    case 5:
                        msg.msg = "Температура выше нормы";
                        break;
                    case 6:
                        msg.msg = "Температура в норме";
                        break;
                    case 7:
                        msg.msg = "Входа I неисправны";
                        break;
                    case 8:
                        msg.msg = "Входа I исправны";
                        break;
                    case 9:
                        msg.msg = "Входа U неисправны";
                        break;
                    case 10:
                        msg.msg = "Входа U исправны";
                        break;
                    case 11:
                        msg.msg = "МРВ неисправен";
                        break;
                    case 12:
                        msg.msg = "МРВ исправен";
                        break;
                    case 13:
                        msg.msg = "Вход Д1-Д8 неисправен";
                        break;
                    case 14:
                        msg.msg = "Вход Д1-Д8 исправен";
                        break;
                    case 15:
                        msg.msg = "Вход Д9-Д16 неисправен";
                        break;
                    case 16:
                        msg.msg = "Вход Д9-Д16 исправен";
                        break;
                    case 17:
                        msg.msg = "Ошибка контрольной суммы уставок";
                        break;
                    case 18:
                    case 19:
                        msg.msg = "Ошибка контрольной суммы данных";
                        break;
                    case 20:
                        msg.msg = "Ошибка журнала системы";
                        break;
                    case 21:
                        msg.msg = "Ошибка журнала аварий";
                        break;
                    case 22:
                        msg.msg = "Остановка часов";
                        break;
                    case 23:
                        msg.msg = "Ошибка данных часов";
                        break;
                    case 25:
                        msg.msg = "Меню - уставки изменены";
                        break;
                    case 26:
                        msg.msg = "Пароль изменен";
                        break;
                    case 27:
                        msg.msg = "Сброс журнала системы";
                        break;
                    case 28:
                        msg.msg = "Сброс журнала аварий";
                        break;
                    case 29:
                        msg.msg = "Сброс ресурса выключателя";
                        break;
                    case 30:
                        msg.msg = "Сброс индикации";
                        break;
                    case 31:
                        msg.msg = "Изменена группа уставок";
                        break;
                    case 32:
                        msg.msg = "СДТУ – уставки изменены";
                        break;
                    case 33:
                        msg.msg = "Ошибка задающего генератора";
                        break;
                    case 34:
                        msg.msg = "Рестарт устройства";
                        break;
                    case 35:
                        msg.msg = "Устройство выключено";
                        break;
                    case 36:
                        msg.msg = "Устройство включено";
                        break;
                    case 38:
                        msg.msg = "Меню сброс осциллографа";
                        break;
                    case 39:
                        msg.msg = "СДТУ – сброс осциллографа";
                        break;
                    case 40:
                        msg.msg = "Критическая ошибка устройства";
                        break;
                    case 44:
                        msg.msg = "Неисправность цепей включения";
                        break;
                    case 45:
                        msg.msg = "";
                        break;
                    case 46:
                        msg.msg = "Небаланс АЦП  Iabc";
                        break;
                    case 47:
                        msg.msg = "Баланс АЦП  Iabc";
                        break;
                    case 48:
                        msg.msg = "Несимметрия  Iabc";
                        break;
                    case 49:
                        msg.msg = "Симметрия  Iabc";
                        break;
                    case 50:
                        msg.msg = "ТН внеш. неисправность";
                        break;
                    case 51:
                        msg.msg = "ТН исправен";
                        break;
                    case 52:
                        msg.msg = "Небаланс АЦП Uabc";
                        break;
                    case 53:
                        msg.msg = "Баланс АЦП Uabc";
                        break;
                    case 54:
                        msg.msg = "Несимметрия Uabc";
                        break;
                    case 55:
                        msg.msg = "Симметрия Uabc";
                        break;
                    case 56:
                        msg.msg = "Uавс < 5В";
                        break;
                    case 57:
                        msg.msg = "Uавс > 5В";
                        break;
                    case 58:
                        msg.msg = "ТННП внеш. неисправность";
                        break;
                    case 59:
                        msg.msg = "ТННП исправен";
                        break;
                    case 60:
                        msg.msg = "Частота вне диапазона";
                        break;
                    case 61:
                        msg.msg = "Частота в норме";
                        break;
                    case 62:
                        msg.msg = "Выключатель отключен";
                        break;
                    case 63:
                        msg.msg = "Выключатель включен";
                        break;
                    case 64:
                        msg.msg = "Блокировка выключателя";
                        break;
                    case 65:
                        msg.msg = "Отказ выключателя";
                        break;
                    case 66:
                        msg.msg = "Неисправность выключателя";
                        break;
                    case 67:
                        msg.msg = "Внеш.неиспр. выключателя";
                        break;
                    case 68:
                        msg.msg = "Неиспр.управ. выключателя";
                        break;
                    case 69:
                        msg.msg = "Работа УРОВ";
                        break;
                    case 70:
                        msg.msg = "Пуск ЛЗШ";
                        break;
                    case 71:
                        msg.msg = "Защита отключить";
                        break;
                    case 72:
                        msg.msg = "АПВ блокировано";
                        break;
                    case 73:
                        msg.msg = "АПВ вн.блокировка";
                        break;
                    case 74:
                        msg.msg = "Запуск АПВ 1 крат";
                        break;
                    case 75:
                        msg.msg = "Запуск АПВ 2 крат";
                        break;
                    case 76:
                        msg.msg = "Запуск АПВ 3 крат";
                        break;
                    case 77:
                        msg.msg = "Запуск АПВ 4 крат";
                        break;
                    case 78:
                        msg.msg = "АПВ включить";
                        break;
                    case 85:
                        msg.msg = "AВР блокирован";
                        break;
                    case 86:
                        msg.msg = "АВР внеш. блокировка";
                        break;
                    case 87:
                        msg.msg = "АВР готовность";
                        break;
                    case 88:
                        msg.msg = "АВР отключить";
                        break;
                    case 89:
                        msg.msg = "АВР включить";
                        break;
                    case 90:
                        msg.msg = "АВР вкл. резерв";
                        break;
                    case 91:
                        msg.msg = "АВР откл. резерв";
                        break;
                    case 92:
                        msg.msg = "АВР запуск от защиты";
                        break;
                    case 93:
                        msg.msg = "АВР запуск команда откл.";
                        break;
                    case 94:
                        msg.msg = "АВР запуск по питанию";
                        break;
                    case 95:
                        msg.msg = "АВР запуск самооткл.";
                        break;
                    case 96:
                        msg.msg = "Кнопка отключить";
                        break;
                    case 97:
                        msg.msg = "Кнопка включить";
                        break;
                    case 98:
                        msg.msg = "Ключ отключить";
                        break;
                    case 99:
                        msg.msg = "Ключ включить";
                        break;
                    case 100:
                        msg.msg = "Внешнее отключить";
                        break;
                    case 101:
                        msg.msg = "Внешнее включить";
                        break;
                    case 102:
                        msg.msg = "СДТУ отключить";
                        break;
                    case 103:
                        msg.msg = "СДТУ включить";
                        break;
                    case 104:
                        msg.msg = "Основные уставки";
                        break;
                    case 105:
                        msg.msg = "Резервные уставки";
                        break;
                    case 106:
                        msg.msg = "Внеш.резерв. уставки";
                        break;
                    case 107:
                        msg.msg = "Внеш. блок-ка команд СДТУ";
                        break;
                    case 108:
                        msg.msg = "Меню-основные уставки";
                        break;
                    case 109:
                        msg.msg = "Меню-резервные уставки";
                        break;
                    case 110:
                        msg.msg = "СДТУ-основные уставки";
                        break;
                    case 111:
                        msg.msg = "СДТУ-резервные уставки";
                        break;
                    case 112:
                        msg.msg = "АПВ возврат";
                        break;
                    case 113:
                        msg.msg = "АПВ возврат F>";
                        break;
                    case 114:
                        msg.msg = "АПВ возврат F>>";
                        break;
                    case 115:
                        msg.msg = "АПВ возврат F<";
                        break;
                    case 116:
                        msg.msg = "АПВ возврат F<<";
                        break;
                    case 117:
                        msg.msg = "АПВ возврат U>";
                        break;
                    case 118:
                        msg.msg = "АПВ возврат U>>";
                        break;
                    case 119:
                        msg.msg = "АПВ возврат U<";
                        break;
                    case 120:
                        msg.msg = "АПВ возврат U<<";
                        break;
                    case 121:
                        msg.msg = "АПВ возврат U2>";
                        break;
                    case 122:
                        msg.msg = "АПВ возврат U2>>";
                        break;
                    case 123:
                        msg.msg = "АПВ возврат Uo>";
                        break;
                    case 124:
                        msg.msg = "АПВ возврат Uo>>";
                        break;
                    case 125:
                        msg.msg = "АПВ возврат ВЗ-1";
                        break;
                    case 126:
                        msg.msg = "АПВ возврат ВЗ-2";
                        break;
                    case 127:
                        msg.msg = "АПВ возврат ВЗ-3";
                        break;
                    case 128:
                        msg.msg = "АПВ возврат ВЗ-4";
                        break;
                    case 129:
                        msg.msg = "АПВ возврат ВЗ-5";
                        break;
                    case 130:
                        msg.msg = "АПВ возврат ВЗ-6";
                        break;
                    case 131:
                        msg.msg = "АПВ возврат ВЗ-7";
                        break;
                    case 132:
                        msg.msg = "АПВ возврат ВЗ-8";
                        break;
                    case 133:
                        msg.msg = "U<10B Частота недостоверна";
                        break;
                    case 134:
                        msg.msg = "U>10B Частота достоверна";
                        break;
                    case 135:
                        msg.msg = "АВР Меню блокировка";
                        break;
                    case 136:
                        msg.msg = "АВР СДТУ блокировка";
                        break;
                    case 137:
                        msg.msg = "СДТУ: Логика изменена";
                        break;
                    case 138:
                        msg.msg = "Меню: запуск логики";
                        break;
                    case 139:
                        msg.msg = "СДТУ: запуск логики";
                        break;
                    case 140:
                        msg.msg = "Меню: остановка логики";
                        break;
                    case 141:
                        msg.msg = "СДТУ: остановка логики";
                        break;
                    case 142:
                        msg.msg = "Ошибка логики по старту";
                        break;
                    case 143:
                        msg.msg = "Ошибка логики тайм аут";
                        break;
                    case 144:
                        msg.msg = "Ошибка логики размер";
                        break;
                    case 145:
                        msg.msg = "Ошибка логики команда";
                        break;
                    case 146:
                        msg.msg = "Ошибка логики аргумент";
                        break;
                    case 147:
                        msg.msg = "Ошибка размера ППЗУ";
                        break;
                    case 149:
                        msg.msg = "Сообщение спл № 1";
                        break;
                    case 150:
                        msg.msg = "Сообщение спл № 2";
                        break;
                    case 151:
                        msg.msg = "Сообщение спл № 3";
                        break;
                    case 152:
                        msg.msg = "Сообщение спл № 4";
                        break;
                    case 153:
                        msg.msg = "Сообщение спл № 5";
                        break;
                    case 154:
                        msg.msg = "Сообщение спл № 6";
                        break;
                    case 155:
                        msg.msg = "Сообщение спл № 7";
                        break;
                    case 156:
                        msg.msg = "Сообщение спл № 8";
                        break;
                    case 157:
                        msg.msg = "Сообщение спл № 9";
                        break;
                    case 158:
                        msg.msg = "Сообщение спл № 10";
                        break;
                    case 159:
                        msg.msg = "Сообщение спл № 11";
                        break;
                    case 160:
                        msg.msg = "Сообщение спл № 12";
                        break;
                    case 161:
                        msg.msg = "Сообщение спл № 13";
                        break;
                    case 162:
                        msg.msg = "Сообщение спл № 14";
                        break;
                    case 163:
                        msg.msg = "Сообщение спл № 15";
                        break;
                    case 164:
                        msg.msg = "Сообщение спл № 16";
                        break;
                    case 165:
                        msg.msg = "Сообщение спл № 17";
                        break;
                    case 166:
                        msg.msg = "Сообщение спл № 18";
                        break;
                    case 167:
                        msg.msg = "Сообщение спл № 19";
                        break;
                    case 168:
                        msg.msg = "Сообщение спл № 20";
                        break;
                    case 169:
                        msg.msg = "Сообщение спл № 21";
                        break;
                    case 170:
                        msg.msg = "Сообщение спл № 22";
                        break;
                    case 171:
                        msg.msg = "Сообщение спл № 23";
                        break;
                    case 172:
                        msg.msg = "Сообщение спл № 24";
                        break;
                    case 173:
                        msg.msg = "Сообщение спл № 25";
                        break;
                    case 174:
                        msg.msg = "Сообщение спл № 26";
                        break;
                    case 175:
                        msg.msg = "Сообщение спл № 27";
                        break;
                    case 176:
                        msg.msg = "Сообщение спл № 28";
                        break;
                    case 177:
                        msg.msg = "Сообщение спл № 29";
                        break;
                    case 178:
                        msg.msg = "Сообщение спл № 30";
                        break;
                    case 179:
                        msg.msg = "Сообщение спл № 31";
                        break;
                    case 180:
                        msg.msg = "Сообщение спл № 32";
                        break;
                    case 181:
                        msg.msg = "Сообщение спл № 33";
                        break;
                    case 182:
                        msg.msg = "Сообщение спл № 34";
                        break;
                    case 183:
                        msg.msg = "Сообщение спл № 35";
                        break;
                    case 184:
                        msg.msg = "Сообщение спл № 36";
                        break;
                    case 185:
                        msg.msg = "Сообщение спл № 37";
                        break;
                    case 186:
                        msg.msg = "Сообщение спл № 38";
                        break;
                    case 187:
                        msg.msg = "Сообщение спл № 39";
                        break;
                    case 188:
                        msg.msg = "Сообщение спл № 40";
                        break;
                    case 189:
                        msg.msg = "Сообщение спл № 41";
                        break;
                    case 190:
                        msg.msg = "Сообщение спл № 42";
                        break;
                    case 191:
                        msg.msg = "Сообщение спл № 43";
                        break;
                    case 192:
                        msg.msg = "Сообщение спл № 44";
                        break;
                    case 193:
                        msg.msg = "Сообщение спл № 45";
                        break;
                    case 194:
                        msg.msg = "Сообщение спл № 46";
                        break;
                    case 195:
                        msg.msg = "Сообщение спл № 47";
                        break;
                    case 196:
                        msg.msg = "Сообщение спл № 48";
                        break;
                    case 197:
                        msg.msg = "Сообщение спл № 49";
                        break;
                    case 198:
                        msg.msg = "Сообщение спл № 50";
                        break;
                    case 199:
                        msg.msg = "Сообщение спл № 51";
                        break;
                    case 200:
                        msg.msg = "Сообщение спл № 52";
                        break;
                    case 201:
                        msg.msg = "Сообщение спл № 53";
                        break;
                    case 202:
                        msg.msg = "Сообщение спл № 54";
                        break;
                    case 203:
                        msg.msg = "Сообщение спл № 55";
                        break;
                    case 204:
                        msg.msg = "Сообщение спл № 56";
                        break;
                    case 205:
                        msg.msg = "Сообщение спл № 57";
                        break;
                    case 206:
                        msg.msg = "Сообщение спл № 58";
                        break;
                    case 207:
                        msg.msg = "Сообщение спл № 59";
                        break;
                    case 208:
                        msg.msg = "Сообщение спл № 60";
                        break;
                    case 209:
                        msg.msg = "Сообщение спл № 61";
                        break;
                    case 210:
                        msg.msg = "Сообщение спл № 62";
                        break;
                    case 211:
                        msg.msg = "Сообщение спл № 63";
                        break;
                    case 212:
                        msg.msg = "Сообщение спл № 64";
                        break;
                    case 213:
                        msg.msg = "Внеш. блокировка команд СДТУ";
                        break;


                }
                msg.time = value[3].ToString("d2") + "-" + value[2].ToString("d2") + "-" + value[1].ToString("d2") + " " +
                           value[4].ToString("d2") + ":" + value[5].ToString("d2") + ":" + value[6].ToString("d2") + ":" +
                           value[7].ToString("d2");

                return msg;
            }

            #region ICollection Members

            public int Count
            {
                get { return this._messages.Count; }
            }

            public void CopyTo(Array array, int index)
            {

            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return this._messages; }
            }

            public void Add(SystemRecord item)
            {
                this._messages?.Add(item);
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._messages.GetEnumerator();
            }

            #endregion
        }

        #endregion

        #region Журнал аварий

        public const int ALARMJOURNAL_RECORD_CNT = 32;

        [Browsable(false)]
        [XmlIgnore]
        public CAlarmJournal AlarmJournal
        {
            get { return this._alarmJournalRecords; }
            set { this._alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ia;
            public string Ib;
            public string Ic;
            public string I0;
            public string I1;
            public string I2;
            public string In;
            public string Ig;
            public string F;
            public string Uab;
            public string Ubc;
            public string Uca;
            public string U0;
            public string U1;
            public string U2;
            public string Un;
            public string inSignals1;
            public string inSignals2;
        };

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            private TZL _device;

            public CAlarmJournal(TZL device)
            {
                this._device = device;
                this._msgDictionary = new Dictionary<ushort, string>(7);
                this._codeDictionary = new Dictionary<byte, string>(32);
                this._typeDictionary = new Dictionary<byte, string>(28);

                this._msgDictionary.Add(0, "Журнал пуст");
                this._msgDictionary.Add(1, "Сигнализация");
                this._msgDictionary.Add(2, "Отключение");
                this._msgDictionary.Add(3, "Работа");
                this._msgDictionary.Add(4, "Неуспешное АПВ");
                this._msgDictionary.Add(5, "Возврат");
                this._msgDictionary.Add(6, "Включение");
                this._msgDictionary.Add(7, "ОМП");

                this._codeDictionary.Add(0, "");
                this._codeDictionary.Add(1, "I>");
                this._codeDictionary.Add(2, "I>>");
                this._codeDictionary.Add(3, "I>>>");
                this._codeDictionary.Add(4, "I>>>>");
                this._codeDictionary.Add(5, "I2>");
                this._codeDictionary.Add(6, "I2>>");
                this._codeDictionary.Add(7, "I0>");
                this._codeDictionary.Add(8, "I0>>");
                this._codeDictionary.Add(9, "In>");
                this._codeDictionary.Add(10, "In>>");
                this._codeDictionary.Add(11, "Ig>");
                this._codeDictionary.Add(12, "I2/I1");
                this._codeDictionary.Add(13, "F>");
                this._codeDictionary.Add(14, "F>>");
                this._codeDictionary.Add(15, "F<");
                this._codeDictionary.Add(16, "F<<");
                this._codeDictionary.Add(17, "U>");
                this._codeDictionary.Add(18, "U>>");
                this._codeDictionary.Add(19, "U<");
                this._codeDictionary.Add(20, "U<<");
                this._codeDictionary.Add(21, "U2>");
                this._codeDictionary.Add(22, "U2>>");
                this._codeDictionary.Add(23, "U0>");
                this._codeDictionary.Add(24, "U0>>");
                this._codeDictionary.Add(25, "ВЗ-1");
                this._codeDictionary.Add(26, "ВЗ-2");
                this._codeDictionary.Add(27, "ВЗ-3");
                this._codeDictionary.Add(28, "ВЗ-4");
                this._codeDictionary.Add(29, "ВЗ-5");
                this._codeDictionary.Add(30, "ВЗ-6");
                this._codeDictionary.Add(31, "ВЗ-7");
                this._codeDictionary.Add(32, "ВЗ-8");
                this._codeDictionary.Add(33, "Резерв");
                this._codeDictionary.Add(34, "Резерв");
                this._codeDictionary.Add(35, "Резерв");
                this._codeDictionary.Add(36, "Резерв");
                this._codeDictionary.Add(37, "Резерв");
                this._codeDictionary.Add(38, "Резерв");
                this._codeDictionary.Add(39, "Резерв");
                this._codeDictionary.Add(40, "Резерв");

                this._typeDictionary.Add(0, "");
                this._typeDictionary.Add(1, "Ig");
                this._typeDictionary.Add(2, "In");
                this._typeDictionary.Add(3, "Ia");
                this._typeDictionary.Add(4, "Ib");
                this._typeDictionary.Add(5, "Ic");
                this._typeDictionary.Add(6, "I0");
                this._typeDictionary.Add(7, "I1");
                this._typeDictionary.Add(8, "I2");
                this._typeDictionary.Add(9, "Pn");
                this._typeDictionary.Add(10, "Pa");
                this._typeDictionary.Add(11, "Pb");
                this._typeDictionary.Add(12, "Pc");
                this._typeDictionary.Add(13, "P0");
                this._typeDictionary.Add(14, "P1");
                this._typeDictionary.Add(15, "P2");
                this._typeDictionary.Add(16, "F");
                this._typeDictionary.Add(17, "Un");
                this._typeDictionary.Add(18, "Ua");
                this._typeDictionary.Add(19, "Ub");
                this._typeDictionary.Add(20, "Uc");
                this._typeDictionary.Add(21, "U0");
                this._typeDictionary.Add(22, "U1");
                this._typeDictionary.Add(23, "U2");
                this._typeDictionary.Add(24, "Uab");
                this._typeDictionary.Add(25, "Ubc");
                this._typeDictionary.Add(26, "Uca");
                this._typeDictionary.Add(27, "Обрыв провода");
                this._typeDictionary.Add(28, "Lkz");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

            public AlarmRecord this[int i]
            {
                get { return this._messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                bool rez;
                try
                {
                    rez = ("Журнал пуст") == this._messages[i].msg;
                }
                catch
                {
                    rez = false;
                }
                return rez;
            }

            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = this.CreateMessage(value);
                if ("Журнал пуст" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    this._messages.Add(msg);
                }
                return ret;
            }

            public string GetMessage(byte b)
            {
                try
                {
                    return this._msgDictionary[b];
                }
                catch (Exception)
                {
                    return b.ToString();
                }
            }

            public string GetMessageOsc(byte b)
            {
                return this._codeDictionary[b];
            }

            public string GetDateTime(byte[] datetime)
            {
                return datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
                       " " +
                       datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
                       ":" + datetime[12].ToString("d2");
            }

            public string GetDateTimeOSC(byte[] datetime)
            {
                return
                    datetime[2].ToString("d2") + "-" + datetime[1].ToString("d2") + "-" + datetime[0].ToString("d2") +
                    " " +
                    datetime[3].ToString("d2") + ":" + datetime[4].ToString("d2") + ":" + datetime[5].ToString("d2") +
                    ":" + datetime[6].ToString("d2");
            }

            public string GetDateTimeINT(int[] datetime)
            {
                return
                    datetime[2].ToString("d2") + "-" + datetime[1].ToString("d2") + "-" + datetime[0].ToString("d2") +
                    " " +
                    datetime[3].ToString("d2") + ":" + datetime[4].ToString("d2") + ":" + datetime[5].ToString("d2") +
                    ":" + datetime[6].ToString("d2");
            }

            public string GetCode(byte codeByte, byte phaseByte)
            {
                string group = (0 == (codeByte & 0x80)) ? "основная" : "резерв.";
                string phase = "";
                phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                phase += (0 == (phaseByte & 0x04)) ? " " : "C";
                byte code = (byte) (codeByte & 0x7F);
                try
                {
                    return this._codeDictionary[code] + " " + group + " " + phase;
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }

            public string GetTypeValue(ushort damageWord, byte typeByte, string code)
            {
                bool OB = false;
                double damageValue = 0;
                switch (code)
                {
                    case "Ia":
                    case "Ib":
                    case "Ic":
                    case "I0":
                    case "I1":
                    case "I2":
                        damageValue = Measuring.GetI(damageWord, this._device.TT, true);
                        break;
                    case "Pn":
                        damageValue = Measuring.GetP(damageWord, (ushort) (this._device.TTNP*this._device.TNNP), false);
                        break;
                    case "Pa":
                    case "Pb":
                    case "Pc":
                    case "P0":
                    case "P1":
                    case "P2":
                        damageValue = Measuring.GetP(damageWord, (ushort) (this._device.TT*this._device.TN), true);
                        break;
                    case "In":
                    case "Ig":
                        damageValue = Measuring.GetI(damageWord, this._device.TTNP, false);
                        break;
                    case "I2/I1":
                        damageValue = Measuring.GetU(damageWord, 65536);
                        break;
                    case "F":
                    case "Lkz":
                        damageValue = Measuring.GetConstraintOnly(damageWord, ConstraintKoefficient.K_25600);
                        break;
                    case "Un":
                        damageValue = Math.Round(Measuring.GetU(damageWord, this._device.TNNP), 1);
                        break;
                    case "Ua":
                    case "Ub":
                    case "Uc":
                    case "U0":
                    case "U1":
                    case "U2":
                    case "Uab":
                    case "Ubc":
                    case "Uca":
                        damageValue = Math.Round(Measuring.GetU(damageWord, this._device.TN), 1);
                        break;
                    case "Обрыв провода":
                        OB = true;
                        break;
                    default:
                        break;
                }

                try
                {
                    if (OB)
                    {
                        return this._typeDictionary[typeByte];
                    }
                    else
                    {
                        return this._typeDictionary[typeByte] + string.Format(" = {0:F2}", damageValue);
                    }
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }

            private AlarmRecord CreateMessage(byte[] buffer)
            {
                Measuring.SetConsGA = true;
                AlarmRecord rec = new AlarmRecord();
                rec.msg = this.GetMessage(buffer[0]);
                byte[] datetime = new byte[14];
                Array.ConstrainedCopy(buffer, 2, datetime, 0, 14);
                rec.time = this.GetDateTime(datetime);

                if (buffer[0] == 0)
                {
                    rec.code = "";
                }
                else
                {
                    rec.code = this.GetCode(buffer[16], buffer[18]);
                }
                double damageValue = Measuring.GetConstraintOnly(Common.TOWORD(buffer[21], buffer[20]),
                    ConstraintKoefficient.K_25600);
                if (buffer[19] == 0)
                {
                    rec.type_value = "";
                }
                else
                {
                    rec.type_value = this.GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19],
                        Validator.GetJornal(buffer[19], this._typeDictionary));
                }

                double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), this._device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), this._device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), this._device.TT, true);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), this._device.TT, true);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), this._device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), this._device.TT, true);
                double In = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), this._device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), this._device.TTNP, false);
                double F = Measuring.GetF(Common.TOWORD(buffer[39], buffer[38]));
                double Uab = Measuring.GetU(Common.TOWORD(buffer[41], buffer[40]), this._device.TN);
                double Ubc = Measuring.GetU(Common.TOWORD(buffer[43], buffer[42]), this._device.TN);
                double Uca = Measuring.GetU(Common.TOWORD(buffer[45], buffer[44]), this._device.TN);
                double U0 = Measuring.GetU(Common.TOWORD(buffer[47], buffer[46]), this._device.TN);
                double U1 = Measuring.GetU(Common.TOWORD(buffer[49], buffer[48]), this._device.TN);
                double U2 = Measuring.GetU(Common.TOWORD(buffer[51], buffer[50]), this._device.TN);
                double Un = Measuring.GetU(Common.TOWORD(buffer[53], buffer[52]), this._device.TNNP);
                Measuring.SetConsGA = false;

                rec.Ia = string.Format("{0:F2}", Ia);
                rec.Ib = string.Format("{0:F2}", Ib);
                rec.Ic = string.Format("{0:F2}", Ic);
                rec.I0 = string.Format("{0:F2}", I0);
                rec.I1 = string.Format("{0:F2}", I1);
                rec.I2 = string.Format("{0:F2}", I2);
                rec.In = string.Format("{0:F2}", In);
                rec.Ig = string.Format("{0:F2}", Ig);
                rec.F = string.Format("{0:F2}", F);
                rec.Uab = string.Format("{0:F2}", Uab);
                rec.Ubc = string.Format("{0:F2}", Ubc);
                rec.Uca = string.Format("{0:F2}", Uca);
                rec.U0 = string.Format("{0:F2}", U0);
                rec.U1 = string.Format("{0:F2}", U1);
                rec.U2 = string.Format("{0:F2}", U2);
                rec.Un = string.Format("{0:F2}", Un);
                byte[] b1 = new byte[] {buffer[54]};
                byte[] b2 = new byte[] {buffer[55]};
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }

        #endregion

        #region Поля

        private slot _diagnostic = new slot(0x1800, 0x1817);
        private slot _inputSignals = new slot(0x1000, 0x103C);
        private slot _typeInterfaces = new slot(0x1017, 0x1018);
        private slot _outputSignals = new slot(0x1200, 0x1270);
        private slot _externalDefenses = new slot(0x1050, 0x1080);
        private slot _datetime = new slot(0x200, 0x207);
        private slot _analog = new slot(0x1900, 0x1918);
        private slot _analogAdd = new slot(0x1B00, 0x1B06);

        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];

        private bool _stopAlarmJournal;
        private bool _stopSystemJournal;

        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private CAlarmJournal _alarmJournalRecords;
        private COutputRele _outputRele;
        private COutputIndicator _outputIndicator;

        private slot _konfCount = new slot(0x1274, 0x1275);
        private slot _oscCrush = new slot(0x3800, 0x3801);

        #region Осциллограф

        private MemoryEntity<MeasureTransStruct> _transformator;

        public MemoryEntity<MeasureTransStruct> Transformator
        {
            get { return this._transformator; }
        }

        private MemoryEntity<OscJurnalStruct> _oscJournal;

        public MemoryEntity<OscJurnalStruct> OscJurnal
        {
            get { return this._oscJournal; }
        }

        private MemoryEntity<SetOscStartPageStruct> _setStartPage;

        public MemoryEntity<SetOscStartPageStruct> SetStartPage
        {
            get { return this._setStartPage; }
        }

        private MemoryEntity<OscPage> _oscPage;

        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        private MemoryEntity<OneWordStruct> _refreshOscJournal;

        public MemoryEntity<OneWordStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        #region Старый осциллограф

        private slot _countOsc = new slot(0x3F00, 0x3F01); //(0x1000, 0x103C);
        private slot _numOsc = new slot(0x3F02, 0x3F03);

        #endregion Старый осциллограф

        #region Новый осциллограф

        private static ushort _startOscJournalAdress = 0x0800;
        private static ushort _startOscJournal = _startOscJournalAdress;

        private static ushort _sizeOscJournal =
            (ushort) (System.Runtime.InteropServices.Marshal.SizeOf(typeof (OscJournal))/2 - 2);

        private static ushort _endOscJournal = (ushort) (_startOscJournal + _sizeOscJournal);

        private static ushort _startOscAdress = 0x0900;
        private static ushort _startOscilloscope = _startOscAdress;
        private static ushort _sizeOscilloscope = 0x7c; //Слова
        private static ushort _endOscilloscope = (ushort) (_startOscilloscope + _sizeOscilloscope);

        //Журнал осциллограммы
        private slot _oscilloscopeJournalReadSlot = new slot(_startOscJournal, _endOscJournal);

        //Номер записи журнала осциллогрфа
        private slot _oscilloscopeJournalWriteSlot = new slot(_startOscJournal, (ushort) (_startOscJournal + 1));

        //Журнал осциллограммы
        private List<slot> CurrentOscilloscope = new List<slot>();
        private slot _oscilloscopeRead = new slot(_startOscilloscope, _endOscilloscope);

        //Номер записи журнала осциллогрфа
        private slot _oscilloscopePageWrite = new slot(_startOscilloscope, (ushort) (_startOscilloscope + 1));

        private MemoryEntity<OneWordStruct> _indJournalOcs;

        public MemoryEntity<OneWordStruct> IndJournalOcs
        {
            get { return this._indJournalOcs; }
        }



        #endregion Новый осциллограф

        #endregion Осциллограф

        #endregion

        #region События

        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;

        public event Handler TypeInterfacesSaveOk;
        public event Handler TypeInterfacesSaveFail;

        public event Handler KonfCountLoadOK;
        public event Handler KonfCountLoadFail;
        public event Handler KonfCountSaveOK;
        public event Handler KonfCountSaveFail;

        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler TokDefensesLoadOK;
        public event Handler TokDefensesLoadFail;
        public event Handler TokDefensesSaveOK;
        public event Handler TokDefensesSaveFail;
        public event Handler LogicLoadOK;
        public event Handler LogicLoadFail;
        public event Handler LogicSaveOK;
        public event Handler LogicSaveFail;


        public event Handler AnalogSignalsLoadOK;
        public event Handler AnalogSignalsLoadFail;

        public event Handler AnalogAddSignalsLoadOK;
        public event Handler AnalogAddSignalsLoadFail;

        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;

        #region Новый осциллограф

        public event Handler OscJournalLoadOk;
        public event Handler OscJournalLoadFail;
        public event Handler OscJournalSaveOk;
        public event Handler OscJournalSaveFail;

        public event Handler OscilloscopeLoadOk;
        public event Handler OscilloscopeLoadFail;
        public event Handler OscilloscopeSaveOK;
        public event Handler OscilloscopeSaveFail;

        #endregion

        #endregion

        #region Конструкторы инициализация

        public double Version
        {
            get { return Common.VersionConverter(DeviceVersion); }
            set { }
        }

        public TZL()
        {
            HaveVersion = true;
        }

        public TZL(Modbus mb)
        {
            this.MB = mb;
            this.Init();
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        private void Init()
        {
            HaveVersion = true;
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0xB000);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0001);
            this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0xA000);
            this._startSpl = new MemoryEntity<OneWordStruct>("Запуск логики", this, 0x1809);
            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логики", this, 0x1808);
            this._stateSpl = new MemoryEntity<OneWordStruct>("Состояние ошибок логики", this, 0x1805);
            this._isSplOn = new MemoryEntity<OneWordStruct>("Состояние логики", this, 0x1800);
            this._indJournalOcs = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x800);
            this._oscJournal = new MemoryEntity<OscJurnalStruct>("Журнал осциллографа v70", this, 0x800);
            this._setStartPage =
                new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы v70", this, 0x900);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа v70", this, 0x900);
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Обновление журнала осциллографа v70", this, 0x800);
            this._transformator = new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖА ", this, 0x1000);
            this._resetOsc = new MemoryEntity<OneWordStruct>("Сброс осциллограммы", this, 0x3800);

            this._alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2000;
            ushort size = 0x8;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                this._systemJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x10;
            }
            start = 0x2800;
            size = 0x1C;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                this._alarmJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x40;
            }

            this._oscilloscope = new TzlOscilloscope(this);
            this._oscilloscope.Initialize();
        }

        #endregion

        #region Выходные реле

        public class COutputRele : ICollection
        {
            public static int LENGTH;
            public static int COUNT;
            
            public COutputRele(Device device)
            {
                double vers = Common.VersionConverter(device.DeviceVersion);
                if ((vers >= 1.1) || (vers == 0))
                {
                    LENGTH = 26;
                    COUNT = 13;
                }
                else
                {
                    LENGTH = 16;
                    COUNT = 8;
                }

                this.SetBuffer(new ushort[LENGTH], vers);
            }

            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);
            
            public int Count
            {
                get { return this._releList.Count; }
            }

            public void SetBuffer(ushort[] values, double vers)
            {
                this._releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", LENGTH,
                        "Output rele length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    this._releList.Add(new OutputReleItem(values[i], values[i + 1], vers));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = this._releList[i].Value[0];
                    ret[i*2 + 1] = this._releList[i].Value[1];
                }
                return ret;
            }

            public OutputReleItem this[int i]
            {
                get { return this._releList[i]; }
                set { this._releList[i] = value; }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                this._releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._releList.GetEnumerator();
            }

            #endregion
        }

        public class OutputReleItem
        {
            private ushort _hiWord;
            private ushort _loWord;
            private double _version;

            public override string ToString()
            {
                return "Выходное реле";
            }

            [XmlAttribute("Значения")]
            public ushort[] Value
            {
                get { return new [] {this._hiWord, this._loWord}; }
                set
                {
                    this._hiWord = value[0];
                    this._loWord = value[1];
                }
            }

            public OutputReleItem()
            {
            }

            public OutputReleItem(ushort hiWord, ushort loWord, double version)
            {
                this.SetItem(hiWord, loWord);
                this._version = version;
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                this._hiWord = hiWord;
                this._loWord = loWord;
            }
            
            [XmlAttribute("Тип")]
            public string Type
            {
                get { return Validator.Get(this._hiWord, Strings.RelayIndType, 15); }
                set { this._hiWord = Validator.Set(value, Strings.RelayIndType, this._hiWord, 15); }
            }
            
            [XmlAttribute("Сигнал_реле")]
            public string RelaySignalString
            {
                get
                {
                    List<string> list = new List<string>(Strings.All);
                    //приходиться костылить, потому что весь этот проект сплошной костыль
                    if (this._version >= 3.03)
                    {
                        int index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К1 Инв.";
                        list[index] = "Вход К1";
                        index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К2 Инв.";
                        list[index] = "Вход К2";
                    }
                    return Validator.Get(this._hiWord, list, 0, 1, 2, 3, 4, 5, 6, 7, 8);
                }
                set
                {
                    List<string> list = new List<string>(Strings.All);
                    if (this._version >= 3.03)
                    {
                        int index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К1 Инв.";
                        list[index] = "Вход К1";
                        index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К2 Инв.";
                        list[index] = "Вход К2";
                    }
                    this._hiWord = Validator.Set(value, list, this._hiWord, 0, 1, 2, 3, 4, 5, 6, 7, 8);
                }
            }
            
            [XmlAttribute("Импульс")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(this._loWord); }
                set { this._loWord = Measuring.SetTime(value); }
            }
        }
        
        [XmlElement("Выходные_реле")]
        public COutputRele OutputRele
        {
            get { return this._outputRele; }
            set { this._outputRele = value; }
        }

        #endregion

        #region Выходные индикаторы

        public class COutputIndicator : ICollection
        {
            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                this._indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._indList.GetEnumerator();
            }

            #endregion
            
            public int Count
            {
                get { return this._indList.Count; }
            }


            public const int LENGTH = 16;
            public const int COUNT = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator(Device device)
            {
                this.SetBuffer(new ushort[LENGTH], Common.VersionConverter(device.DeviceVersion));
            }

            public void SetBuffer(ushort[] values, double vers)
            {
                this._indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values", LENGTH,
                        "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    this._indList.Add(new OutputIndicatorItem(values[i], values[i + 1], vers));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = this._indList[i].Value[0];
                    ret[i*2 + 1] = this._indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get { return this._indList[i]; }
                set { this._indList[i] = value; }
            }
        }

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;
            private double _version;
            public override string ToString()
            {
                return "Выходной индикатор";
            }
            
            [XmlAttribute("Значения")]
            public ushort[] Value
            {
                get { return new [] {this._hiWord, this._loWord}; }
            }

            public OutputIndicatorItem()
            {
            }

            public OutputIndicatorItem(ushort hiWord, ushort loWord, double version)
            {
                this.SetItem(hiWord, loWord);
                this._version = version;
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                this._hiWord = hiWord;
                this._loWord = loWord;
            }
            
            [XmlAttribute("Тип")]
            public string Type
            {
                get { return Validator.Get(this._hiWord, Strings.RelayIndType, 15); }
                set { this._hiWord = Validator.Set(value, Strings.RelayIndType, this._hiWord, 15); }
            }
            
            [XmlAttribute("Сигнал_индикатора")]
            public string SignalString
            {
                get
                {
                    List<string> list = new List<string>(Strings.All);
                    if (this._version >= 3.03)
                    {
                        int index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К1 Инв.";
                        list[index] = "Вход К1";
                        index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К2 Инв.";
                        list[index] = "Вход К2";
                    }
                    return Validator.Get(this._hiWord, list, 0, 1, 2, 3, 4, 5, 6, 7, 8);
                }
                set
                {
                    List<string> list = new List<string>(Strings.All);
                    if (this._version >= 3.03)
                    {
                        int index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К1 Инв.";
                        list[index] = "Вход К1";
                        index = list.IndexOf("Резерв");
                        list[index - 1] = "Вход К2 Инв.";
                        list[index] = "Вход К2";
                    }
                    this._hiWord = Validator.Set(value, list, this._hiWord, 0, 1, 2, 3, 4, 5, 6, 7, 8);
                }
            }
            
            [XmlAttribute("Индикация")]
            public bool Indication
            {
                get { return 0 != (this._loWord & 0x1); }
                set { this._loWord = value ? (ushort) (this._loWord | 0x1) : (ushort) (this._loWord & ~0x1); }
            }

            [XmlAttribute("Авария")]
            public bool Alarm
            {
                get { return 0 != (this._loWord & 0x2); }
                set { this._loWord = value ? (ushort) (this._loWord | 0x2) : (ushort) (this._loWord & ~0x2); }
            }

            [XmlAttribute("Система")]
            public bool System
            {
                get { return 0 != (this._loWord & 0x4); }
                set { this._loWord = value ? (ushort) (this._loWord | 0x4) : (ushort) (this._loWord & ~0x4); }
            }
        }

        [XmlElement("Выходные_индикаторы")]

        public COutputIndicator OutputIndicator
        {
            get { return this._outputIndicator; }
            set { this._outputIndicator = value; }
        }

        #endregion
        //-
        #region Аналоговые сигналы

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double In
        {
            get { return Measuring.GetI(this._analog.Value[0], this.TTNP, false); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ia
        {
            get { return Measuring.GetI(this._analog.Value[1], this.TT, true); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ib
        {
            get { return Measuring.GetI(this._analog.Value[2], this.TT, true); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ic
        {
            get { return Measuring.GetI(this._analog.Value[3], this.TT, true); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double I0
        {
            get { return Measuring.GetI(this._analog.Value[4], this.TT, true); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double I1
        {
            get { return Measuring.GetI(this._analog.Value[5], this.TT, true); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double I2
        {
            get { return Measuring.GetI(this._analog.Value[6], this.TT, true); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ig
        {
            get { return Measuring.GetI(this._analog.Value[7], this.TTNP, false); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Un
        {
            get { return Measuring.GetU(this._analog.Value[8], this.TNNP); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ua
        {
            get { return Measuring.GetU(this._analog.Value[9], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ub
        {
            get { return Measuring.GetU(this._analog.Value[10], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Uc
        {
            get { return Measuring.GetU(this._analog.Value[11], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Uab
        {
            get { return Measuring.GetU(this._analog.Value[12], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Ubc
        {
            get { return Measuring.GetU(this._analog.Value[13], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double Uca
        {
            get { return Measuring.GetU(this._analog.Value[14], this.TN); }
        }

        #endregion

        public double AddUab
        {
            get
            {
                var added = (uint) Common.UshortUshortToInt(this._analogAdd.Value[1], this._analogAdd.Value[0]);
                return Measuring.GetU(added, this.TN);
            }
        }

        public double AddUbc
        {
            get
            {
                var added = (uint) Common.UshortUshortToInt(this._analogAdd.Value[3], this._analogAdd.Value[2]);
                return Measuring.GetU(added, this.TN);
            }
        }

        public double AddUca
        {
            get
            {
                var added = (uint) Common.UshortUshortToInt(this._analogAdd.Value[5], this._analogAdd.Value[4]);
                return Measuring.GetU(added, this.TN);
            }
        }

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double U0
        {
            get { return Measuring.GetU(this._analog.Value[15], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double U1
        {
            get { return Measuring.GetU(this._analog.Value[16], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double U2
        {
            get { return Measuring.GetU(this._analog.Value[17], this.TN); }
        }

        #endregion

        #region

        [Category("Аналоговые сигналы")]

        #endregion

            #region

            public double F
        {
            get { return Measuring.GetF(this._analog.Value[18]); }
        }

        public double Cos
        {
            get { return Measuring.GetF(this._analog.Value[19]); }
        }

        public double P
        {
            get { return this._analog.Value[20]; }
        }

        public double Q
        {
            get { return this._analog.Value[21]; }
        }


        #endregion


        #endregion
        //-
        #region Дискретные сигналы

        public string LoadZnaki(ushort[] masInputSignals)
        {
            byte[] buffer1 = Common.TOBYTES(masInputSignals, false);
            byte[] b1 = new byte[] {buffer1[34]};
            byte[] b2 = new byte[] {buffer1[35]};
            string r1 = Common.BitsToString(new BitArray(b1));
            string r2 = Common.BitsToString(new BitArray(b2));
            string res = r1 + r2;
            return res;
        }

        public string BdZnaki
        {
            get { return this.LoadZnaki(this._diagnostic.Value); }
        }

        public string QZnak
        {
            get
            {
                byte[] buffer1 = Common.TOBYTES(this._diagnostic.Value, false);
                return Common.GetBit(buffer1[35], 7) ? "-" : "";
            }
        }

        public string PZnak
        {
            get
            {
                byte[] buffer1 = Common.TOBYTES(this._diagnostic.Value, false);
                return Common.GetBit(buffer1[35], 6) ? "-" : "";
            }
        }

        #region [Discrets]

        [XmlIgnore]
        public BitArray ManageSignals
        {
            get
            {
                return
                    new BitArray(new []{Common.LOBYTE(this._diagnostic.Value[0]), Common.HIBYTE(this._diagnostic.Value[0])});
            }
        }

        [XmlIgnore]
        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[2])});
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }

        [XmlIgnore]
        public BitArray Indicators
        {
            get { return new BitArray(new byte[] {Common.HIBYTE(this._diagnostic.Value[2])}); }
        }

        [XmlIgnore]
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(this._diagnostic.Value[0x9]),
                    Common.HIBYTE(this._diagnostic.Value[0x9]),
                    Common.LOBYTE(this._diagnostic.Value[0xA])
                });
            }
        }

        [XmlIgnore]
        public BitArray Rele
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(this._diagnostic.Value[3]),
                    Common.HIBYTE(this._diagnostic.Value[3])
                });
            }
        }

        [XmlIgnore]
        public BitArray OutputSignals
        {
            get
            {
                return new BitArray(new byte[] {Common.HIBYTE(this._diagnostic.Value[0xA])});
            }
        }

        [XmlIgnore]
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(this._diagnostic.Value[0xB]),
                    Common.HIBYTE(this._diagnostic.Value[0xB]),
                    Common.LOBYTE(this._diagnostic.Value[0xC]),
                    Common.HIBYTE(this._diagnostic.Value[0xC]),
                    Common.LOBYTE(this._diagnostic.Value[0xD]),
                    Common.HIBYTE(this._diagnostic.Value[0xD]),
                    Common.LOBYTE(this._diagnostic.Value[0xE])
                });
            }
        }

        [XmlIgnore]
        public BitArray Ssl
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.HIBYTE(this._diagnostic.Value[0xE]),
                    Common.LOBYTE(this._diagnostic.Value[0xF]),
                    Common.HIBYTE(this._diagnostic.Value[0xF])
                });
            }
        }

        [XmlIgnore]
        public BitArray FaultState
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(this._diagnostic.Value[4])}); }
        }

        List<bool> _faultSignals = new List<bool>();
        [XmlIgnore]
        public bool[] FaultSignals
        {
            get
            {
                this._faultSignals.Clear();
                this._faultSignals.AddRange(Common.GetBitsArray(new [] {this._diagnostic.Value[5]}, 0, 15));
                this._faultSignals.Add(Common.GetBit(this._diagnostic.Value[6], 6));
                this._faultSignals.Add(Common.GetBit(this._diagnostic.Value[6], 7));
                return this._faultSignals.ToArray();
            }
        }

        [XmlIgnore]
        public BitArray ManageBitsDublicate
        {
            get { return new BitArray(new [] {Common.LOBYTE(this._diagnostic.Value[8])}); }
        }

        [XmlIgnore]
        public BitArray Automation
        {
            get { return new BitArray(new [] {Common.HIBYTE(this._diagnostic.Value[8])}); }
        }

        #endregion

        #endregion
        //-
        #region Параметры тока и напряжений

        #region ТТ

        [XmlElement("ТТ")]
        public ushort TT { get; set; }

        public ushort TTtoDevice
        {
            get { return this._inputSignals.Value[1]; }
            set { this._inputSignals.Value[1] = value; }
        }

        #endregion

        #region ТТНП

        [XmlElement("ТТНП")]
        public ushort TTNP { get; set; }

        public ushort TTNPtoDevice
        {
            get { return this._inputSignals.Value[2]; }
            set { this._inputSignals.Value[2] = value; }
        }


        public ushort TypeInterfaces { get; set; }

        public ushort TypeInterfacesToDevice
        {
            get { return this._typeInterfaces.Value[0]; }
            set { this._typeInterfaces.Value[0] = value; }
        }
        #endregion

        #region Тип ТT

        [XmlElement("Тип_ТT")]
        public string TT_Type
        {
            get { return Strings.TT_Type[this._inputSignals.Value[0]]; }
            set { this._inputSignals.Value[0] = (ushort) Strings.TT_Type.IndexOf(value); }
        }

        #endregion

        #region Тип ОМП

        [XmlElement("Тип_ОМП")]
        public string OMP_Type
        {
            get { return Validator.Get(this._inputSignals.Value[13], Strings.ModesLight); }
            set { this._inputSignals.Value[13] = Validator.Set(value, Strings.ModesLight); }
        }

        #endregion

        #region HUD

        [XmlElement("ХУД")]
        public double HUD
        {
            get { return this._inputSignals.Value[14]; }
            set { this._inputSignals.Value[14] = (ushort) (value*1000); }
        }

        #endregion

        #region Тип ТН

        [XmlElement("Тип_ТН")]
        public string TN_Type
        {
            get { return Strings.TN_Type[this._inputSignals.Value[8]]; }
            set { this._inputSignals.Value[8] = (ushort) Strings.TN_Type.IndexOf(value); }
        }

        #endregion

        #region ТН

        [XmlElement("ТН")]
        public double TN
        {
            get
            {
                double ktn = ValuesConverterCommon.GetKth(this._inputSignals.Value[9]);
                return Common.GetBit(this._inputSignals.Value[9], 15)
                    ? ktn * 1000
                    : ktn;
            }
            set
            {
                //this._inputSignals.Value[9] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine);
            }
        }

        [XmlIgnore]
        public double TNtoDevice
        {
            //get { return Measuring.GetConstraint(this._inputSignals.Value[9], ConstraintKoefficient.K_Undefine); }
            //set { this._inputSignals.Value[9] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine); }

            get { return ValuesConverterCommon.GetKthFull(this._inputSignals.Value[9]); }
            set { this._inputSignals.Value[9] = ValuesConverterCommon.SetKthFull(value); }
        }

        #endregion

        #region Неисправность ТН

        [XmlElement("Неисправность_ТН")]
        public string TN_Dispepair
        {
            get { return Validator.Get(this._inputSignals.Value[10], Strings.Logic); }
            set { this._inputSignals.Value[10] = Validator.Set(value, Strings.Logic); }
        }

        #endregion

        #region Неисправность ТННП

        [XmlElement("Неисправность_ТННП")]
        public string TNNP_Dispepair
        {
            get { return Validator.Get(this._inputSignals.Value[12], Strings.Logic); }
            set { this._inputSignals.Value[12] = Validator.Set(value, Strings.Logic); }
        }

        #endregion

        #region ТННП

        [XmlElement("ТННП")]
        public double TNNP
        {
            get
            {
                double ktn = ValuesConverterCommon.GetKth(this._inputSignals.Value[0xB]);
                return Common.GetBit(this._inputSignals.Value[0xB], 15)
                    ? ktn * 1000
                    : ktn;
            }
            set
            {
                //this._inputSignals.Value[0xB] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine);
            }
            //get; set;
        }

        [XmlIgnore]
        public double TNNPtoDevice
        {
            get { return ValuesConverterCommon.GetKthFull(this._inputSignals.Value[0xB]); }
            set { this._inputSignals.Value[0xB] = ValuesConverterCommon.SetKthFull(value); }
        }

        #endregion

        #region Максимальный_ток

        [XmlElement("Максимальный_ток")]
        public double MaxTok
        {
            get { return Measuring.GetConstraint(this._inputSignals.Value[3], ConstraintKoefficient.K_4000); }
            set { this._inputSignals.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        #endregion

        #endregion
        //-
        #region Входные сигналы
        public virtual double OscilloscopeKonfCount
        {
            get { return this._konfCount.Value[0]; }
            set { this._konfCount.Value[0] = (ushort) value; }
        }

        public virtual string OscilloscopeFix
        {
            get
            {
                int rez = 0;

                if (Common.GetBit(this._konfCount.Value[0], 7))
                {
                    rez = 1;
                }

                return Strings.OscFix[rez];
            }
            set
            {
                bool rez = true;
                if (Strings.OscFix.IndexOf(value) == 0)
                {
                    rez = false;
                }

                this._konfCount.Value[0] = Common.SetBit(this._konfCount.Value[0], 7, rez);
            }
        }

        public virtual ushort OscilloscopePercent
        {
            get { return (ushort) (Common.GetBits(this._konfCount.Value[0], 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set
            {
                this._konfCount.Value[0] = Common.SetBits(this._konfCount.Value[0], value, 8, 9, 10, 11, 12, 13, 14, 15);
            }
        }


        public virtual string OscilloscopePeriod
        {
            get
            {
                try
                {
                    int temp = Common.GetBits(this._konfCount.Value[0], 0, 1, 2, 3, 4);
                    return Strings.OscLength[temp];
                }
                catch
                {
                    return "XXXXXX";
                }
            }
            set
            {
                this._konfCount.Value[0] = Common.SetBits(this._konfCount.Value[0],
                    (ushort) Strings.OscLength.IndexOf(value), 0, 1, 2, 3, 4);
            }
        }

        public virtual string OscilloscopePeriod3_0
        {
            get
            {
                try
                {
                    return Strings.OscLength3_0[Common.GetBits(this._konfCount.Value[0], 0, 1, 2, 3, 4)];
                }
                catch
                {
                    return "XXXXXX";
                }
            }
            set
            {
                this._konfCount.Value[0] = Common.SetBits(this._konfCount.Value[0],
                    (ushort) Strings.OscLength3_0.IndexOf(value), 0, 1, 2, 3, 4);
            }
        }

        public virtual string OscilloscopePeriodS
        {
            get
            {
                try
                {
                    return Strings.OscLengthS[Common.GetBits(this._konfCount.Value[0], 0, 1, 2, 3, 4)];
                }
                catch
                {
                    return "XXXXXX";
                }
            }
            set
            {
                this._konfCount.Value[0] = Common.SetBits(this._konfCount.Value[0],
                    (ushort) Strings.OscLengthS.IndexOf(value), 0, 1, 2, 3, 4);
            }
        }
        
        #region Свойство - _inputSignals.Value[16]
        [XmlElement("Ключ_включить")]
        public string KeyOn
        {
            get
            {
                ushort index = this._inputSignals.Value[0x11];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x11] = (ushort) (Strings.Logic.IndexOf(value)); }

        }

        #endregion

        #region Свойство - _inputSignals.Value[17]

        [XmlElement("Ключ_выключить")]
        public string KeyOff
        {
            get
            {
                ushort index = this._inputSignals.Value[0x10];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x10] = (ushort) Strings.Logic.IndexOf(value); }
        }

        #endregion

        #region Свойство - _inputSignals.Value[18]

        [XmlElement("Внешние_включить")]
        public string ExternalOn
        {
            get
            {
                ushort index = this._inputSignals.Value[0x12];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x12] = (ushort)Strings.Logic.IndexOf(value); }
        }

        #endregion

        #region Свойство - _inputSignals.Value[19]

        [XmlElement("Внешние_выключить")]
        public string ExternalOff
        {
            get
            {
                ushort index = this._inputSignals.Value[0x13];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x13] = (ushort) Strings.Logic.IndexOf(value); }
        }

        #endregion

        #region Свойство - _inputSignals.Value[20]

        [XmlElement("Сброс_сигнализации")]
        public string SignalizationReset
        {
            get
            {
                ushort index = this._inputSignals.Value[0x14];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x14] = (ushort) Strings.Logic.IndexOf(value); }
        }

        #endregion

        #region Свойство - _inputSignals.Value[21]

        [XmlElement("Переключение_уставок")]
        public string ConstraintGroup
        {
            get
            {
                ushort index = this._inputSignals.Value[0x15];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { this._inputSignals.Value[0x15] = (ushort)Strings.Logic.IndexOf(value); }
        }

        #endregion

        #region Свойство - _inputSignals.Value[24]

        [XmlElement("Блокировка_СДТУ")]
        public string ExBlockSdtu
        {
            get { return Validator.Get(this._inputSignals.Value[24], Strings.Logic); }
            set { this._inputSignals.Value[24] = Validator.Set(value, Strings.Logic); }
        }

        #endregion

        #region Свойство - _inputSignals.Value[29]

        [XmlIgnore]
        public BitArray DispepairSignal
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(this._inputSignals.Value[0x1D])}); }
            set { this._inputSignals.Value[0x1D] = Common.BitsToUshort(value); }
        }

        [XmlElement("Неисправности")]
        public bool[] DispepairSignalXml
        {
            get { return this.DispepairSignal.Cast<bool>().ToArray(); }
            set { }
        }

        #endregion

        #region Свойство - _inputSignals.Value[30]

        [XmlElement("Импульс_неисправности")]
        public ulong DispepairImpulse
        {
            get { return Measuring.GetTime(this._inputSignals.Value[0x1E]); }
            set { this._inputSignals.Value[0x1E] = Measuring.SetTime(value); }
        }

        #endregion

        #region Ключи
        

        [XmlElement("Ключи")]
        public string Keys
        {
            get { return this.LoadKeys(this._inputSignals.Value); }
            set { this._inputSignals.Value[15] = this.SetKeys(value); }
        }

        public string LoadKeys(ushort[] masInputSignals)
        {
            byte[] buffer1 = Common.TOBYTES(masInputSignals, false);
            byte[] b1 = new byte[] {buffer1[30]};
            byte[] b2 = new byte[] {buffer1[31]};
            string r1 = Common.BitsToString(new BitArray(b1));
            string r2 = Common.BitsToString(new BitArray(b2));
            string res = r1 + r2;
            return res;
        }

        public ushort SetKeys(string masInputSignals)
        {
            return Common.BitsToUshort(Common.StringToBits(masInputSignals));
        }

        #endregion

        #endregion

        #region Programming

        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;
        private MemoryEntity<OneWordStruct> _isSplOn;


        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return this._programStorageStruct; }
        }

        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }

        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return this._stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return this._startSpl; }
        }

        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return this._stateSpl; }
        }

        public MemoryEntity<OneWordStruct> IsSplOn
        {
            get { return this._isSplOn; }
        }

        #endregion

        #region Сигналы управления

        [XmlElement("Сигнал_от_кнопок")]
        public string ManageSignalButton
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 0)
                    ? Strings.Forbidden[0]
                    : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = value == Strings.Forbidden[0];
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 0, bit);
            }
        }

        [XmlElement("Сигнал_от_ключа")]
        public string ManageSignalKey
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 1) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = value == Strings.Control[0];
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 1, bit);
            }
        }

        [XmlElement("Сигнал_от_внешнего")]
        public string ManageSignalExternal
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 2) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = value == Strings.Control[0];
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 2, bit);
            }
        }

        [XmlElement("Сигнал_от_СДТУ")]
        public string ManageSignalSDTU
        {
            get
            {
                string ret = Common.GetBit(this._inputSignals.Value[0x3B], 3)
                    ? Strings.Forbidden[0]
                    : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = value == Strings.Forbidden[0];
                this._inputSignals.Value[0x3B] = Common.SetBit(this._inputSignals.Value[0x3B], 3, bit);
            }
        }

        #endregion

        #region Выключатель

        [XmlElement("Выключатель_отключить")]
        public string SwitcherOff
        {
            get { return Validator.Get(this._inputSignals.Value[50], Strings.Logic); }
            set { this._inputSignals.Value[50] = Validator.Set(value, Strings.Logic); }
        }

        [XmlElement("Выключатель_включить")]
        public string SwitcherOn
        {
            get { return Validator.Get(this._inputSignals.Value[51], Strings.Logic); }
            set { this._inputSignals.Value[51] = Validator.Set(value, Strings.Logic); }
        }

        [XmlElement("Выключатель_ошибка")]
        public string SwitcherError
        {
            get { return Validator.Get(this._inputSignals.Value[52], Strings.Logic); }
            set { this._inputSignals.Value[52] = Validator.Set(value, Strings.Logic); }
        }

        [XmlElement("Выключатель_блокировка")]
        public string SwitcherBlock
        {
            get { return Validator.Get(this._inputSignals.Value[53], Strings.Logic); }
            set { this._inputSignals.Value[53] = Validator.Set(value, Strings.Logic); }
        }

        [XmlElement("Выключатель_время_УРОВ")]
        public ulong SwitcherTimeUROV
        {
            get { return Measuring.GetTime(this._inputSignals.Value[54]); }
            set { this._inputSignals.Value[54] = Measuring.SetTime(value); }
        }

        [XmlElement("Выключатель_ток_УРОВ")]
        public double SwitcherTokUROV
        {
            get { return Measuring.GetConstraint(this._inputSignals.Value[55], ConstraintKoefficient.K_4000); }
            set { this._inputSignals.Value[55] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }

        }

        [XmlElement("Выключатель_импульс")]
        public ulong SwitcherImpulse
        {
            get { return Measuring.GetTime(this._inputSignals.Value[56]); }
            set { this._inputSignals.Value[56] = Measuring.SetTime(value); }

        }

        [XmlElement("Выключатель_длительность")]
        public ulong SwitcherDuration
        {
            get { return Measuring.GetTime(this._inputSignals.Value[57]); }
            set { this._inputSignals.Value[57] = Measuring.SetTime(value); }

        }

        [XmlElement("Состояние")]
        public string Sost_ON_OFF
        {
            get { return Validator.Get(this._inputSignals.Value[58], Strings.ModesLight); }
            set { this._inputSignals.Value[58] = Validator.Set(value, Strings.ModesLight); }
        }

        #endregion

        #region Выходные логические сигналы
        public string[][] VlsXml
        {
            get
            {
                List<bool[]> allVLS = new List<bool[]>();
                for (int i = 0; i < 8; i++)
                {
                    BitArray array = this.GetOutputLogicSignals(i);
                    allVLS.Add(array.Cast<bool>().ToArray());
                }
                string[][] result = new string[8][];
                for (int i = 0; i < 8; i++)
                {
                    result[i] = Strings.OutputSignals.Where((t, j) => allVLS[i][j]).ToArray();
                }
                return result;
            }
            set { }
        }

        public BitArray GetOutputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }

            ushort[] logicBuffer = new ushort[8];
            Array.ConstrainedCopy(this._outputSignals.Value, channel*8, logicBuffer, 0, 8);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(OUTPUTLOGIC_BITS_CNT);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;

        }


        public void SetOutputLogicSignals(int channel, BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (120 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i*16 + j < 120)
                    {
                        if (values[i*16 + j])
                        {
                            logicBuffer[i] += (ushort) Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, this._outputSignals.Value, channel*8, 8);

        }

        #endregion

        #region Входные логические сигналы
        //Для сериализации в HTML
        public string[][] InpSignals
        {
            get
            {
                List<string[]> allLS = new List<string[]>();
                for (int i = 0; i < 8; i++)
                {
                    LogicState[] state = this.GetInputLogicSignals(i);
                    string[] st = state.Select(s => s.ToString()).ToArray();
                    allLS.Add(st);
                }
                return allLS.ToArray();
            }
            set
            {
            }
        }

        public LogicState[] GetInputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[16];
            //_logiсSignals.Add(ret);
            this.SetLogicStates(this._inputSignals.Value[0x22 + channel*2], true, ref ret);
            this.SetLogicStates(this._inputSignals.Value[0x22 + channel*2 + 1], false, ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            //_logiсSignals[channel] = LogicSignals;
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < logicSignals.Length; i++)
            {
                if (i < 8)
                {
                    if (LogicState.Да == logicSignals[i])
                    {
                        firstWord += (ushort) Math.Pow(2, i);
                    }
                    if (LogicState.Инверс == logicSignals[i])
                    {
                        firstWord += (ushort) Math.Pow(2, i);
                        firstWord += (ushort) Math.Pow(2, i + 8);
                    }
                }
                else
                {
                    if (LogicState.Да == logicSignals[i])
                    {
                        secondWord += (ushort) Math.Pow(2, i - 8);
                    }
                    if (LogicState.Инверс == logicSignals[i])
                    {
                        secondWord += (ushort) Math.Pow(2, i - 8);
                        secondWord += (ushort) Math.Pow(2, i);
                    }
                }
            }
            this._inputSignals.Value[0x22 + channel*2] = firstWord;
            this._inputSignals.Value[0x22 + channel*2 + 1] = secondWord;
        }

        void SetLogicStates(ushort value, bool firstWord, ref LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = firstWord ? 0 : 8;
            int end = firstWord ? 8 : 16;

            for (int i = start; i < end; i++)
            {
                int pow = firstWord ? i : i - 8;
                logicArray[i] = (byte) (Math.Pow(2, pow)) != ((lowByte) & (byte) (Math.Pow(2, pow)))
                    ? logicArray[i]
                    : LogicState.Да;
                //logicArray[i] = (byte)(Math.Pow(2, pow)) != ((hiByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.Инверс;
                logicArray[i] = this.IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.Да)
                    ? LogicState.Инверс
                    : logicArray[i];
            }
        }

        private bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte) (Math.Pow(2, pow)) == ((hiByte) & (byte) (Math.Pow(2, pow)));
        }



        #endregion

        #region Страница автоматики

        //Поле автоматики
        private slot _automaticsPage = new slot(0x103c, 0x1050);
        private BitArray _bits = new BitArray(8);
        //События страницы автоматики
        public event Handler AutomaticsPageLoadOK;
        public event Handler AutomaticsPageLoadFail;
        public event Handler AutomaticsPageSaveOK;
        public event Handler AutomaticsPageSaveFail;

        public void LoadAutomaticsPage()
        {
            LoadSlot(DeviceNumber, this._automaticsPage, "LoadAutomaticsPage" + DeviceNumber, this);
        }

        public void SaveAutomaticsPage()
        {
            SaveSlot(DeviceNumber, this._automaticsPage, "SaveAutomaticsPage" + DeviceNumber, this);
        }

        [XmlElement("Конфигурация_АПВ")]
        public string APV_Cnf
        {
            get { return Validator.Get(this._automaticsPage.Value[0], Strings.Crat, 0, 1, 2); }
            set
            {
                this._automaticsPage.Value[0] = Validator.Set(value, Strings.Crat, this._automaticsPage.Value[0], 0, 1, 2);
            }
        }

        [XmlElement("Блокировка_АПВ")]
        public string APV_Blocking
        {
            get { return Validator.Get(this._automaticsPage.Value[1], Strings.Logic); }
            set { this._automaticsPage.Value[1] = Validator.Set(value, Strings.Logic); }
        }

        [XmlElement("Время_блокировки_АПВ")]
        public ulong APV_Time_Blocking
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[2]); }
            set { this._automaticsPage.Value[2] = Measuring.SetTime(value); }
        }

        [XmlElement("Время_готовности_АПВ")]
        public ulong APV_Time_Ready
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[3]); }
            set { this._automaticsPage.Value[3] = Measuring.SetTime(value); }
        }

        [XmlElement("Время_1_крата_АПВ")]
        public ulong APV_Time_1Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[4]); }
            set { this._automaticsPage.Value[4] = Measuring.SetTime(value); }
        }

        [XmlElement("Время_2_крата_АПВ")]
        public ulong APV_Time_2Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[5]); }
            set { this._automaticsPage.Value[5] = Measuring.SetTime(value); }
        }

        [XmlElement("Время_3_крата_АПВ")]
        public ulong APV_Time_3Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[6]); }
            set { this._automaticsPage.Value[6] = Measuring.SetTime(value); }
        }

        [XmlElement("Время_4_крата_АПВ")]
        public ulong APV_Time_4Krat
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[7]); }
            set { this._automaticsPage.Value[7] = Measuring.SetTime(value); }
        }

        [XmlElement("Запуск_АПВ_по_самоотключению")]
        public string APV_Start
        {
            get { return Validator.Get(this._automaticsPage.Value[0], Strings.YesNo, 8); }
            set { this._automaticsPage.Value[0] = Validator.Set(value, Strings.YesNo, this._automaticsPage.Value[0], 8); }
        }

        [XmlElement("Пропадание_питания_АВР")]
        public bool AVR_Supply_Off
        {
            get { return this.AVR_Add_Signals[0]; }
            set
            {
                this.SetCurrentBit(8, 0, value);
                this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
            }
        }

        private void SetCurrentBit(byte num_word, byte num_bit, bool val)
        {
            if (val)
            {
                if (((this._automaticsPage.Value[num_word] >> num_bit) & 1) == 0)
                {
                    this._automaticsPage.Value[num_word] += (ushort) Math.Pow(2, num_bit);
                }
            }
            else
            {
                if (((this._automaticsPage.Value[num_word] >> num_bit) & 1) == 1)
                {
                    this._automaticsPage.Value[num_word] -= (ushort) Math.Pow(2, num_bit);
                }
            }
        }

        [XmlElement("Команда_отключения_выключателя_АВР")]
        public bool AVR_Switch_Off
        {
            get { return this.AVR_Add_Signals[2]; }
            set
            {
                this.SetCurrentBit(8, 2, value);
                this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
            }

        }

        [XmlElement("Самоотключение_АВР")]
        public bool AVR_Self_Off
        {
            get { return this.AVR_Add_Signals[1]; }
            set
            {
                this.SetCurrentBit(8, 1, value);
                this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
            }
        }

        [XmlElement("Срабатывание_защиты_АВР")]
        public bool AVR_Abrasion_Switch
        {
            get { return this.AVR_Add_Signals[3]; }
            set
            {
                this.SetCurrentBit(8, 3, value);
                this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
            }
        }

        [XmlElement("Разрешение_сброса_АВР_по_включению_выключателя")]

        public bool AVR_Reset_Switch
        {
            get { return this.AVR_Add_Signals[7]; }
            set
            {
                this.SetCurrentBit(8, 7, value);
                this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
            }
        }

        [XmlIgnore]
        private BitArray AVR_Add_Signals
        {
            get { return this._bits; }
            set { this._bits = value; }

        }

        [XmlElement("Блокировка_АВР")]
        public string AVR_Blocking
        {
            get
            {
                ushort index = this._automaticsPage.Value[9];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[9] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("Сброс_блокировки_АВР")]
        public string AVR_Reset
        {
            get
            {
                ushort index = this._automaticsPage.Value[10];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[10] = (ushort) Strings.Logic.IndexOf(value); }

        }

        [XmlElement("Запуск_АВР")]
        public string AVR_Start
        {
            get
            {
                ushort index = this._automaticsPage.Value[11];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[11] = (ushort) Strings.Logic.IndexOf(value); }

        }

        [XmlElement("Срабатывание_АВР")]
        public string AVR_Abrasion
        {
            get
            {
                ushort index = this._automaticsPage.Value[12];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[12] = (ushort) Strings.Logic.IndexOf(value); }

        }

        [XmlElement("Время_срабатывания_АВР")]
        public ulong AVR_Time_Abrasion
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[13]); }
            set { this._automaticsPage.Value[13] = Measuring.SetTime(value); }
        }

        [XmlElement("Возврат_АВР")]
        public string AVR_Return
        {
            get
            {
                ushort index = this._automaticsPage.Value[14];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._automaticsPage.Value[14] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("Время_возврата_АВР")]
        public ulong AVR_Time_Return
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[15]); }
            set { this._automaticsPage.Value[15] = Measuring.SetTime(value); }
        }

        [XmlElement("Время_отключения_АВР")]
        public ulong AVR_Time_Off
        {
            get { return Measuring.GetTime(this._automaticsPage.Value[16]); }
            set { this._automaticsPage.Value[16] = Measuring.SetTime(value); }
        }

        [XmlElement("Конфигурация_ЛЗШ")]
        public string LZSH_Cnf
        {
            get
            {
                ushort index = this._automaticsPage.Value[18];
                if (index >= Strings.ModesLight.Count)
                {
                    index = (byte) (Strings.ModesLight.Count - 1);
                }
                return Strings.ModesLight[index];
            }
            set { this._automaticsPage.Value[18] = (ushort) Strings.ModesLight.IndexOf(value); }
        }

        [XmlElement("Конфигурация_ЛЗШ114")]
        public string LZSH_CnfV114
        {
            get
            {
                ushort index = this._automaticsPage.Value[18];
                if (index >= Strings.LZSH114.Count)
                {
                    index = (byte) (Strings.LZSH114.Count - 1);
                }
                return Strings.LZSH114[index];
            }
            set { this._automaticsPage.Value[18] = (ushort) Strings.LZSH114.IndexOf(value); }
        }

        [XmlElement("Уставка_ЛЗШ")]
        public double LZSH_Constraint
        {
            get { return Measuring.GetConstraint(this._automaticsPage.Value[19], ConstraintKoefficient.K_4000); }
            set { this._automaticsPage.Value[19] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        #endregion

        #region Внешние защиты

        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 48;
            public const int COUNT = 8;

            #region ICollection Members
            
            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            [DisplayName("Количество")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public void Add(ExternalDefenseItem item)
            {
                this._defenseList?.Add(item);
            }
            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", LENGTH,
                        "External defense length must be 48");
                }
                for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                    this._defenseList.Add(new ExternalDefenseItem(temp));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, i*ExternalDefenseItem.LENGTH,
                        ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ExternalDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }
        }

        public class ExternalDefenseItem
        {
            public const int LENGTH = 6;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem()
            {
            }

            public ExternalDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            public override string ToString()
            {
                return "Внешняя защита";
            }

            [XmlAttribute("Значения")]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value", LENGTH,
                        "External defense item length must be 6");
                }
                this._values = buffer;
            }

            [XmlAttribute("Режим")]
            public string Mode
            {
                get { return Validator.Get(this._values[0], Strings.ModesExternal, 0, 1, 2); }
                set { this._values[0] = Validator.Set(value, Strings.ModesExternal, this._values[0], 0, 1, 2); }
            }

            [XmlAttribute("Режим_v1.11")]
            public string Mode1_11
            {
                get { return Validator.Get(this._values[0], Strings.ModesExternalNew, 0, 1); }
                set { this._values[0] = Validator.Set(value, Strings.ModesExternalNew, this._values[0], 0, 1); }
            }

            [XmlAttribute("Режим_v1.12")]
            public string Mode1_12
            {
                get { return Validator.Get(this._values[0], Strings.ModesNew, 0, 1); }
                set { this._values[0] = Validator.Set(value, Strings.ModesNew, this._values[0], 0, 1); }
            }

            [XmlAttribute("Осц")]
            public bool Osc
            {
                get { return 0 != (this._values[0] & 16); }
                set { this._values[0] = value ? (this._values[0] |= 16) : (ushort) (this._values[0] & ~16); }
            }

            [XmlAttribute("АВР")]
            public bool AVR
            {
                get { return 0 != (this._values[0] & 32); }
                set { this._values[0] = value ? (this._values[0] |= 32) : (ushort) (this._values[0] & ~32); }
            }


            [XmlAttribute("Сброс")]
            public bool Reset
            {
                get { return Common.GetBit(this._values[0], 13); }
                set { this._values[0] = Common.SetBit(this._values[0], 13, value); }
            }

            [XmlAttribute("АПВ")]
            public bool APV
            {
                get { return 0 != (this._values[0] & 64); }
                set { this._values[0] = value ? (this._values[0] |= 64) : (ushort) (this._values[0] & ~64); }
            }

            [XmlAttribute("УРОВ")]
            public bool UROV
            {
                get { return 0 != (this._values[0] & 128); }
                set { this._values[0] = value ? (this._values[0] |= 128) : (ushort) (this._values[0] & ~128); }
            }

            [XmlAttribute("Возврат_АПВ")]
            public bool APV_Return
            {
                get { return 0 != (this._values[0] & 0x8000); }
                set
                {
                    this._values[0] = value ? (this._values[0] |= 0x8000) : (ushort) (this._values[0] & ~0x8000);
                }
            }

            [XmlAttribute("Возврат")]
            public bool Return
            {
                get { return 0 != (this._values[0] & 0x4000); }
                set
                {
                    this._values[0] = value ? (this._values[0] |= 0x4000) : (ushort) (this._values[0] & ~0x4000);
                }
            }


            [XmlAttribute("Блокировка_номер")]
            public string BlockingNumber
            {
                get
                {
                    ushort value = this._values[1];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { this._values[1] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("Срабатывание_номер")]
            public string WorkingNumber
            {
                get
                {
                    ushort value = this._values[2];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { this._values[2] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("Срабатывание_время")]
            public ulong WorkingTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("Возврат_номер")]
            public string ReturnNumber
            {
                get
                {
                    ushort value = this._values[4];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { this._values[4] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("Возврат_время")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }
        }

        private CExternalDefenses _cexternalDefenses = new CExternalDefenses();

        [XmlElement("Внешние_защиты")]
        public CExternalDefenses ExternalDefenses
        {
            get { return this._cexternalDefenses; }
            set { this._cexternalDefenses = value; }
        }


        #endregion

        #region Токовые защиты

        private slot _tokDefensesMain = new slot(0x1080, 0x10C0);
        private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
        private slot _tokDefenses2Main = new slot(0x1100, 0x1120);
        private slot _tokDefenses2Reserve = new slot(0x1120, 0x1140);

        public void LoadTokDefenses()
        {
            LoadSlot(DeviceNumber, this._tokDefenses2Reserve, "LoadTokDefensesReserve2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._tokDefensesReserve, "LoadTokDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._tokDefenses2Main, "LoadTokDefensesMain2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._tokDefensesMain, "LoadTokDefensesMain" + DeviceNumber, this);
        }

        public void SaveTokDefenses()
        {
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort1(), 0, this._tokDefensesMain.Value, 0,
                CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort2(), 0, this._tokDefenses2Main.Value, 0,
                CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort1(), 0, this._tokDefensesReserve.Value, 0,
                CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort2(), 0, this._tokDefenses2Reserve.Value, 0,
                CTokDefenses.LENGTH2);

            SaveSlot(DeviceNumber, this._tokDefenses2Main, "SaveTokDefensesMain2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._tokDefenses2Reserve, "SaveTokDefensesReserve2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._tokDefensesReserve, "SaveTokDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._tokDefensesMain, "SaveTokDefensesMain" + DeviceNumber, this);
        }

        private CTokDefenses _ctokDefensesMain = new CTokDefenses();
        private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

        [XmlElement("Токовые_защиты_основные")]
        public CTokDefenses TokDefensesMain
        {
            get { return this._ctokDefensesMain; }
            set { this._ctokDefensesMain = value; }
        }

        [XmlElement("Токовые_защиты_резервные")]
        public CTokDefenses TokDefensesReserve
        {
            get { return this._ctokDefensesReserve; }
            set { this._ctokDefensesReserve = value; }
        }

        [XmlElement("Угол_I_осн")]
        public ushort Imain
        {
            get { return this._ctokDefensesMain.I; }
            set { }
        }
        [XmlElement("Угол_I0_осн")]
        public ushort I0main
        {
            get { return this._ctokDefensesMain.I0; }
            set { }
        }
        [XmlElement("Угол_In_осн")]
        public ushort Inmain
        {
            get { return this._ctokDefensesMain.In; }
            set { }
        }
        [XmlElement("Угол_I2_осн")]
        public ushort I2main
        {
            get { return this._ctokDefensesMain.I2; }
            set { }
        }

        [XmlElement("Угол_I_рез")]
        public ushort Ires
        {
            get { return this._ctokDefensesReserve.I; }
            set { }
        }
        [XmlElement("Угол_I0_рез")]
        public ushort I0res
        {
            get { return this._ctokDefensesReserve.I0; }
            set { }
        }
        [XmlElement("Угол_In_рез")]
        public ushort Inres
        {
            get { return this._ctokDefensesReserve.In; }
            set { }
        }
        [XmlElement("Угол_I2_рез")]
        public ushort I2res
        {
            get { return this._ctokDefensesReserve.I2; }
            set { }
        }

        public class CTokDefenses : ICollection
        {
            public const int LENGTH1 = 64;
            public const int COUNT1 = 10;
            public const int LENGTH2 = 16;
            public const int COUNT2 = 2;
            private ushort[] _buffer;


            #region ICollection Members

            [DisplayName("Количество")]
            public int Count
            {
                get { return this._items.Count; }
            }

            public void Add(TokDefenseItem item)
            {
                this._items.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._items.GetEnumerator();
            }

            #endregion
            
            public ushort I
            {
                get { return this._buffer[0]; }
                set { this._buffer[0] = value; }
            }
            public ushort I0
            {
                get { return this._buffer[1]; }
                set { this._buffer[1] = value; }
            }
            public ushort In
            {
                get { return this._buffer[2]; }
                set { this._buffer[2] = value; }
            }
            
            public ushort I2
            {
                get { return this._buffer[3]; }
                set { this._buffer[3] = value; }
            }

            private List<TokDefenseItem> _items = new List<TokDefenseItem>(12);

            public CTokDefenses()
            {
                this.SetBuffer(new ushort[160]);
            }

            public TokDefenseItem this[int i]
            {
                get { return this._items[i]; }
                set { this._items[i] = value; }
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._buffer = buffer;
                this._items.Clear();
                for (int i = 0; i < TokDefenseItem.LENGTH*12; i += TokDefenseItem.LENGTH)
                {
                    if (i == TokDefenseItem.LENGTH*11)
                    {
                        i += 2;
                    }
                    ushort[] item = new ushort[TokDefenseItem.LENGTH];
                    Array.ConstrainedCopy(this._buffer, i + 4, item, 0, TokDefenseItem.LENGTH);
                    this._items.Add(new TokDefenseItem(item));
                }

                for (int i = 0; i < 12; i++)
                {
                    this._items[i].IsIncrease = i < 4;
                    this._items[i].IsKoeff500 = i >= 8;
                    this._items[i].IsI12 = false;
                }

                this._items[11].IsI12 = true;

                this._items[0].Name = "I>";
                this._items[1].Name = "I>>";
                this._items[2].Name = "I>>>";
                this._items[3].Name = "I>>>>";
                this._items[4].Name = "I2>";
                this._items[5].Name = "I2>>";
                this._items[6].Name = "I0>";
                this._items[7].Name = "I0>>";
                this._items[8].Name = "In>";
                this._items[9].Name = "In>>";
                this._items[10].Name = "Ig";
                this._items[11].Name = "I2/I1";
            }

            public ushort[] ToUshort1()
            {
                ushort[] buffer = new ushort[LENGTH1];
                buffer[0] = this.I;
                buffer[1] = this.I0;
                buffer[2] = this.In;
                buffer[3] = this.I2;

                for (int i = 0; i < COUNT1; i++)
                {
                    Array.ConstrainedCopy(this._items[i].Values, 0, buffer, 4 + i*TokDefenseItem.LENGTH,
                        TokDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ushort[] ToUshort2()
            {
                ushort[] buffer = new ushort[LENGTH2];

                Array.ConstrainedCopy(this._items[10].Values, 0, buffer, 0, TokDefenseItem.LENGTH);
                Array.ConstrainedCopy(this._items[11].Values, 0, buffer, 2 + TokDefenseItem.LENGTH,
                    TokDefenseItem.LENGTH);
                return buffer;
            }
        }

        public class TokDefenseItem
        {
            public const int LENGTH = 6;
            private bool _isKoeff500;

            [XmlIgnore]
            public bool IsKoeff500
            {
                get { return this._isKoeff500; }
                set { this._isKoeff500 = value; }
            }

            private bool _isIncrease;
            private string _name;
            private ushort[] _values = new ushort[LENGTH];

            public TokDefenseItem()
            {
            }

            public TokDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            public bool IsI12 { get; set; }
            
            [XmlIgnore]
            public bool IsIncrease
            {
                get { return this._isIncrease; }
                set { this._isIncrease = value; }
            }

            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Tok defense increase1 item value", LENGTH,
                        "Tok defense increase1 item length must be 6");
                }
                this._values = buffer;
            }

            [XmlAttribute("Значения")]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            [XmlAttribute("Режим")]
            public string Mode
            {
                get { return Validator.Get(this._values[0], Strings.Modes, 0, 1, 2); }
                set { this._values[0] = Validator.Set(value, Strings.Modes, this._values[0], 0, 1, 2); }
            }

            [XmlAttribute("Режим_v1.11")]
            public string Mode1_11
            {
                get { return Validator.Get(this._values[0], Strings.ModesNew, 0, 1); }
                set { this._values[0] = Validator.Set(value, Strings.ModesNew, this._values[0], 0, 1); }
            }

            [XmlAttribute("Осц")]
            public bool Osc
            {
                get { return Common.GetBit(this._values[0], 4); }
                set { this._values[0] = Common.SetBit(this._values[0], 4, value); }
            }

            [XmlAttribute("Осциллограф_v1_11")]
            public string Oscv1_11
            {
                get
                {
                    ushort temp = Common.GetBits(this._values[0], 3, 4);
                    byte value = (byte) (Common.LOBYTE(temp) >> 3);
                    value = value >= Strings.Osc_v1_11.Count ? (byte) (Strings.Osc_v1_11.Count - 1) : value;
                    return Strings.Osc_v1_11[value];
                }
                set
                {
                    this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Osc_v1_11.IndexOf(value), 3, 4);
                }
            }

            [XmlAttribute("АВР")]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("АПВ")]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("УРОВ")]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("Блокировка_введение")]
            public string BlockingExist
            {
                get
                {
                    string ret = "";
                    ret = Common.GetBit(this._values[0], 9) ? Strings.Blocking[1] : Strings.Blocking[0];
                    return ret;
                }
                set
                {
                    bool b = true;
                    if (Strings.Blocking[0] == value)
                    {
                        b = false;
                    }
                    this._values[0] = Common.SetBit(this._values[0], 9, b);
                }
            }

            [XmlAttribute("Блокировка_номер")]
            public string BlockingNumber
            {
                get
                {
                    ushort value = this._values[1];
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { this._values[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("Пуск_по_U")]
            public bool PuskU
            {
                get { return Common.GetBit(this._values[0], 14); }
                set { this._values[0] = Common.SetBit(this._values[0], 14, value); }
            }

            [XmlAttribute("Ускорение")]
            public bool Speedup
            {
                get { return Common.GetBit(this._values[0], 15); }
                set { this._values[0] = Common.SetBit(this._values[0], 15, value); }
            }

            [XmlAttribute("Направление")]
            public string Direction
            {
                get
                {
                    ushort temp = Common.GetBits(this._values[0], 10, 11);
                    byte value = (byte) (Common.HIBYTE(temp) >> 2);
                    value = value >= Strings.BusDirection.Count ? (byte) (Strings.BusDirection.Count - 1) : value;
                    return Strings.BusDirection[value];
                }
                set
                {
                    ushort index = (ushort) Strings.BusDirection.IndexOf(value);
                    this._values[0] = Common.SetBits(this._values[0], index, 10, 11);
                }
            }

            [XmlAttribute("Параметр")]
            public string Parameter
            {
                get
                {
                    string ret = "";
                    if (this.IsIncrease)
                    {
                        ret = Common.GetBit(this._values[0], 8) ? Strings.TokParameter[1] : Strings.TokParameter[0];
                    }
                    else
                    {
                        ret = Common.GetBit(this._values[0], 8)
                            ? Strings.EngineParameter[1]
                            : Strings.EngineParameter[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (this.IsIncrease)
                    {
                        if (Strings.TokParameter[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.EngineParameter[0] == value)
                        {
                            b = false;
                        }
                    }

                    this._values[0] = Common.SetBit(this._values[0], 8, b);
                }
            }

            [XmlAttribute("Двигатель")]
            public string Engine
            {
                get { return Validator.Get(this._values[0], Strings.EngineMode, 15); }
                set { this._values[0] = Validator.Set(value, Strings.EngineMode, this._values[0], 15); }
            }

            [XmlAttribute("Характеристика")]
            public string Feature
            {
                get
                {
                    string ret;
                    if (this.IsIncrease)
                    {
                        ret = Common.GetBit(this._values[0], 12) ? Strings.FeatureLight[1] : Strings.Feature[0];
                    }
                    else
                    {
                        ret = Common.GetBit(this._values[0], 12) ? Strings.FeatureLight[0] : Strings.Feature[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (this.IsIncrease)
                    {
                        if (Strings.FeatureLight[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.Feature[0] == value)
                        {
                            b = false;
                        }
                    }

                    this._values[0] = Common.SetBit(this._values[0], 12, b);
                }
            }

            [XmlAttribute("Срабатывание_уставка")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                    if (this.IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (this.IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    ret = Measuring.GetConstraint(this._values[2], koeff);
                    return ret;
                }
                set
                {
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                    if (this.IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (this.IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    this._values[2] = Measuring.SetConstraint(value, koeff);
                }
            }

            [XmlAttribute("Срабатывание_время")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("Пуск_по_U_уставка")]
            public double PuskU_Constraint
            {
                get { return Measuring.GetConstraint(this._values[4], ConstraintKoefficient.K_25600); }
                set { this._values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("Ускорение_время")]
            public ulong SpeedupTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }
        }

        #endregion

        #region Защиты по напряжению

        private slot _voltageDefensesMain = new slot(0x1180, 0x11C0);
        private slot _voltageDefensesReserve = new slot(0x11C0, 0x1200);

        public void LoadVoltageDefenses()
        {
            LoadSlot(DeviceNumber, this._voltageDefensesReserve, "LoadVoltageDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._voltageDefensesMain, "LoadVoltageDefensesMain" + DeviceNumber, this);
        }

        public void SaveVoltageDefenses()
        {
            Array.ConstrainedCopy(this.VoltageDefensesMain.ToUshort(), 0, this._voltageDefensesMain.Value, 0,
                CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(this.VoltageDefensesReserve.ToUshort(), 0, this._voltageDefensesReserve.Value, 0,
                CVoltageDefenses.LENGTH);
            SaveSlot(DeviceNumber, this._voltageDefensesReserve, "SaveVoltageDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._voltageDefensesMain, "SaveVoltageDefensesMain" + DeviceNumber, this);
        }

        public event Handler VoltageDefensesLoadOk;
        public event Handler VoltageDefensesLoadFail;
        public event Handler VoltageDefensesSaveOk;
        public event Handler VoltageDefensesSaveFail;

        private CVoltageDefenses _cvoltageDefensesMain = new CVoltageDefenses();
        private CVoltageDefenses _cvoltageDefensesReserve = new CVoltageDefenses();

        [XmlElement("Защиты_напряжения_основные")]
        [DisplayName("Основная группа")]
        [Description("Защиты по напряжению - основная группа уставок")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("Защиты по напряжению")]
        public CVoltageDefenses VoltageDefensesMain
        {
            get { return this._cvoltageDefensesMain; }
            set { this._cvoltageDefensesMain = value; }
        }

        [XmlElement("Защиты_напряжения_резервные")]
        [DisplayName("Резервная группа")]
        [Description("Защиты по напряжению - резервная группа уставок")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("Защиты по напряжению")]
        public CVoltageDefenses VoltageDefensesReserve
        {
            get { return this._cvoltageDefensesReserve; }
            set { this._cvoltageDefensesReserve = value; }
        }

        public enum VoltageDefenseType
        {
            U,
            U2,
            U0
        };

        public class CVoltageDefenses : ICollection
        {
            public const int LENGTH = 64;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(VoltageDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            private List<VoltageDefenseItem> _defenseList = new List<VoltageDefenseItem>(COUNT);

            public CVoltageDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Защиты по напряжению", LENGTH,
                        "Буфер для защит по напряжению должен быть " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += VoltageDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[VoltageDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, VoltageDefenseItem.LENGTH);
                    this._defenseList.Add(new VoltageDefenseItem(temp));
                }
                this._defenseList[4].DefenseType = this._defenseList[5].DefenseType = VoltageDefenseType.U2;
                this._defenseList[6].DefenseType = this._defenseList[7].DefenseType = VoltageDefenseType.U0;

                this._defenseList[0].Name = "U>";
                this._defenseList[1].Name = "U>>";
                this._defenseList[2].Name = "U<";
                this._defenseList[3].Name = "U<<";
                this._defenseList[4].Name = "U2>";
                this._defenseList[5].Name = "U2>>";
                this._defenseList[6].Name = "U0>";
                this._defenseList[7].Name = "U0>>";
            }

            [DisplayName("Количество")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, i*VoltageDefenseItem.LENGTH,
                        VoltageDefenseItem.LENGTH);
                }
                return buffer;
            }

            public VoltageDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }
        };

        public class VoltageDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "Защита по напряжению";
            }

            public VoltageDefenseItem()
            {
            }

            public VoltageDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            [XmlAttribute("Значения")]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Буфер защиты по напряжению", LENGTH,
                        "Буфер защит должен быть длиной" + LENGTH);
                }
                this._values = buffer;
            }

            private VoltageDefenseType _type = VoltageDefenseType.U;

            [XmlAttribute("Тип")]
            [Browsable(false)]
            public VoltageDefenseType DefenseType
            {
                get { return this._type; }
                set { this._type = value; }
            }

            [XmlAttribute("Режим")]
            public string Mode
            {
                get { return Validator.Get(this._values[0], Strings.Modes, 0, 1, 2); }
                set { this._values[0] = Validator.Set(value, Strings.Modes, this._values[0], 0, 1, 2); }
            }

            [XmlAttribute("Режим_v1.11")]
            public string Mode1_11
            {
                get { return Validator.Get(this._values[0], Strings.ModesNew, 0, 1); }
                set { this._values[0] = Validator.Set(value, Strings.ModesNew, this._values[0], 0, 1); }
            }

            [XmlAttribute("Блокировка_v1.11")]
            public string Block1_11
            {
                get { return Validator.Get(this._values[0], Strings.ModesLight, 11); }
                set { this._values[0] = Validator.Set(value, Strings.ModesLight, this._values[0], 11); }
            }

            [XmlAttribute("Осц")]
            public bool Osc
            {
                get { return Common.GetBit(this._values[0], 4); }
                set { this._values[0] = Common.SetBit(this._values[0], 4, value); }
            }


            [XmlAttribute("Осциллограф_v1_11")]
            public string Oscv1_11
            {
                get
                {
                    ushort temp = Common.GetBits(this._values[0], 3, 4);
                    byte value = (byte) (Common.LOBYTE(temp) >> 3);
                    value = value >= Strings.Osc_v1_11.Count ? (byte) (Strings.Osc_v1_11.Count - 1) : value;
                    return Strings.Osc_v1_11[value];
                }
                set
                {
                    this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Osc_v1_11.IndexOf(value), 3, 4);
                }
            }

            [XmlAttribute("АВР")]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("Reset")]
            public bool Reset
            {
                get { return Common.GetBit(this._values[0], 13); }
                set { this._values[0] = Common.SetBit(this._values[0], 13, value); }
            }

            [XmlAttribute("АПВ")]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("УРОВ")]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("АПВ_возврат")]
            public bool APV_Return
            {
                get { return Common.GetBit(this._values[0], 15); }
                set { this._values[0] = Common.SetBit(this._values[0], 15, value); }
            }

            [XmlAttribute("Возврат")]
            public bool Return
            {
                get { return Common.GetBit(this._values[0], 14); }
                set { this._values[0] = Common.SetBit(this._values[0], 14, value); }
            }

            [XmlAttribute("Параметр")]
            public string Parameter
            {
                get
                {
                    string ret = "";
                    int index = (Common.GetBits(this._values[0], 8, 9, 10, 11) >> 8);
                    switch (this._type)
                    {
                        case VoltageDefenseType.U:
                            if (index >= Strings.VoltageParameterU.Count)
                            {
                                index = Strings.VoltageParameterU.Count - 1;
                            }
                            ret = Strings.VoltageParameterU[index];
                            break;
                        case VoltageDefenseType.U0:
                            if (index >= Strings.VoltageParameterU0.Count)
                            {
                                index = Strings.VoltageParameterU0.Count - 1;
                            }
                            ret = Strings.VoltageParameterU0[index];
                            break;
                        case VoltageDefenseType.U2:
                            if (index >= Strings.VoltageParameterU2.Count)
                            {
                                index = Strings.VoltageParameterU2.Count - 1;
                            }
                            ret = Strings.VoltageParameterU2[index];
                            break;
                        default:
                            break;
                    }
                    return ret;
                }
                set
                {
                    switch (this._type)
                    {
                        case VoltageDefenseType.U:
                            this._values[0] = Common.SetBits(this._values[0],
                                (ushort) Strings.VoltageParameterU.IndexOf(value), 8, 9, 10, 11);
                            break;
                        case VoltageDefenseType.U2:
                            this._values[0] = Common.SetBits(this._values[0],
                                (ushort) Strings.VoltageParameterU2.IndexOf(value), 8, 9, 10, 11);
                            break;
                        case VoltageDefenseType.U0:
                            this._values[0] = Common.SetBits(this._values[0],
                                (ushort) Strings.VoltageParameterU0.IndexOf(value), 8, 9, 10, 11);
                            break;
                        default:
                            break;
                    }

                }
            }

            [XmlAttribute("Параметр_v1.11")]
            [DisplayName("Параметр v1.11")]
            [Category("Текстовый вид")]
            public string Parameter1_11
            {
                get
                {
                    string ret = "";
                    int index = (Common.GetBits(this._values[0], 8, 9, 10) >> 8);
                    switch (this._type)
                    {
                        case VoltageDefenseType.U:
                            if (index >= Strings.VoltageParameterU.Count)
                            {
                                index = Strings.VoltageParameterU.Count - 1;
                            }
                            ret = Strings.VoltageParameterU[index];
                            break;
                        case VoltageDefenseType.U0:
                            if (index >= Strings.VoltageParameterU0.Count)
                            {
                                index = Strings.VoltageParameterU0.Count - 1;
                            }
                            ret = Strings.VoltageParameterU0[index];
                            break;
                        case VoltageDefenseType.U2:
                            if (index >= Strings.VoltageParameterU2.Count)
                            {
                                index = Strings.VoltageParameterU2.Count - 1;
                            }
                            ret = Strings.VoltageParameterU2[index];
                            break;
                        default:
                            break;
                    }
                    return ret;
                }
                set
                {
                    switch (this._type)
                    {
                        case VoltageDefenseType.U:
                            this._values[0] = Common.SetBits(this._values[0],
                                (ushort) Strings.VoltageParameterU.IndexOf(value), 8, 9, 10);
                            break;
                        case VoltageDefenseType.U2:
                            this._values[0] = Common.SetBits(this._values[0],
                                (ushort) Strings.VoltageParameterU2.IndexOf(value), 8, 9, 10);
                            break;
                        case VoltageDefenseType.U0:
                            this._values[0] = Common.SetBits(this._values[0],
                                (ushort) Strings.VoltageParameterU0.IndexOf(value), 8, 9, 10);
                            break;
                        default:
                            break;
                    }

                }
            }

            [XmlAttribute("Блокировка_номер")]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (this._values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { this._values[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("Срабатывание_уставка")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(this._values[2], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("Срабатывание_время")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("Возврат_уставка")]
            public double ReturnConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(this._values[4], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("Возврат_время")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }

            private string _name;
            
            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }
        }

        #endregion

        #region Страница защит по частоте

        private slot _frequenceDefensesMain = new slot(0x1140, 0x1160);
        private slot _frequenceDefensesReserve = new slot(0x1160, 0x1180);

        public void LoadFrequenceDefenses()
        {
            LoadSlot(DeviceNumber, this._frequenceDefensesReserve, "LoadFrequenceDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._frequenceDefensesMain, "LoadFrequenceDefensesMain" + DeviceNumber, this);
        }

        public void SaveFrequenceDefenses()
        {
            Array.ConstrainedCopy(this.FrequenceDefensesMain.ToUshort(), 0, this._frequenceDefensesMain.Value, 0,
                CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(this.FrequenceDefensesReserve.ToUshort(), 0, this._frequenceDefensesReserve.Value, 0,
                CFrequenceDefenses.LENGTH);
            SaveSlot(DeviceNumber, this._frequenceDefensesReserve, "SaveFrequenceDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._frequenceDefensesMain, "SaveFrequenceDefensesMain" + DeviceNumber, this);
        }

        public event Handler FrequenceDefensesLoadOk;
        public event Handler FrequenceDefensesLoadFail;
        public event Handler FrequenceDefensesSaveOk;
        public event Handler FrequenceDefensesSaveFail;

        private CFrequenceDefenses _cFrequenceDefensesMain = new CFrequenceDefenses();
        private CFrequenceDefenses _cFrequenceDefensesReserve = new CFrequenceDefenses();

        [XmlElement("Защиты_по_частоте_основные")]
        [DisplayName("Основная группа")]
        [Description("Защиты по частоте - основная группа уставок")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("Защиты по частоте")]
        public CFrequenceDefenses FrequenceDefensesMain
        {
            get { return this._cFrequenceDefensesMain; }
            set { this._cFrequenceDefensesMain = value; }
        }

        [XmlElement("Защиты_по_частоте_резервные")]
        [DisplayName("Резервная группа")]
        [Description("Защиты по частоте - резервная группа уставок")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("Защиты по частоте")]
        public CFrequenceDefenses FrequenceDefensesReserve
        {
            get { return this._cFrequenceDefensesReserve; }
            set { this._cFrequenceDefensesReserve = value; }
        }

        public class CFrequenceDefenses : ICollection
        {
            public const int LENGTH = 0x20;
            public const int COUNT = 4;


            #region ICollection Members

            public void Add(FrequenceDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            private List<FrequenceDefenseItem> _defenseList = new List<FrequenceDefenseItem>(COUNT);

            public CFrequenceDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Защиты по частоте", LENGTH,
                        "Буфер для защит по частоте должен быть " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += FrequenceDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[FrequenceDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, FrequenceDefenseItem.LENGTH);
                    this._defenseList.Add(new FrequenceDefenseItem(temp));
                }
                this._defenseList[0].Name = "F>";
                this._defenseList[1].Name = "F>>";
                this._defenseList[2].Name = "F<";
                this._defenseList[3].Name = "F<<";


            }

            [DisplayName("Количество")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, i*FrequenceDefenseItem.LENGTH,
                        FrequenceDefenseItem.LENGTH);
                }
                return buffer;
            }

            public FrequenceDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }

        };

        public class FrequenceDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }

            public FrequenceDefenseItem()
            {
            }

            public FrequenceDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            public override string ToString()
            {
                return "Защита по частоте";
            }

            [XmlAttribute("Значения")]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Буфер защиты по частоте", LENGTH,
                        "Буфер частоте должен быть длиной" + LENGTH);
                }
                this._values = buffer;
            }

            [XmlAttribute("Режим")]
            public string Mode
            {
                get { return Validator.Get(this._values[0], Strings.Modes, 0, 1, 2); }
                set { this._values[0] = Validator.Set(value, Strings.Modes, this._values[0], 0, 1, 2); }
            }

            [XmlAttribute("Режим_v1.1")]
            public string Mode1_1
            {
                get { return Validator.Get(this._values[0], Strings.ModesNew, 0, 1); }
                set { this._values[0] = Validator.Set(value, Strings.ModesNew, this._values[0], 0, 1); }
            }

            [XmlAttribute("Режим_v1.11")]
            public string Mode1_11
            {
                get { return Validator.Get(this._values[0], Strings.ModesNew, 0, 1); }
                set { this._values[0] = Validator.Set(value, Strings.ModesNew, this._values[0], 0, 1); }
            }

            [XmlAttribute("Осц")]
            public bool Osc
            {
                get { return Common.GetBit(this._values[0], 4); }
                set { this._values[0] = Common.SetBit(this._values[0], 4, value); }
            }

            [XmlAttribute("Осциллограф_v1_11")]
            public string Oscv1_11
            {
                get
                {
                    ushort temp = Common.GetBits(this._values[0], 3, 4);
                    byte value = (byte) (Common.LOBYTE(temp) >> 3);
                    value = value >= Strings.Osc_v1_11.Count ? (byte) (Strings.Osc_v1_11.Count - 1) : value;
                    return Strings.Osc_v1_11[value];
                }
                set
                {
                    this._values[0] = Common.SetBits(this._values[0], (ushort) Strings.Osc_v1_11.IndexOf(value), 3, 4);
                }
            }

            [XmlAttribute("АВР")]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("Reset")]
            public bool Reset
            {
                get { return Common.GetBit(this._values[0], 13); }
                set { this._values[0] = Common.SetBit(this._values[0], 13, value); }
            }

            [XmlAttribute("АПВ")]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("УРОВ")]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("Возврат_АПВ")]
            public bool APV_Return
            {
                get { return Common.GetBit(this._values[0], 15); }
                set { this._values[0] = Common.SetBit(this._values[0], 15, value); }
            }

            [XmlAttribute("Возврат")]
            public bool Return
            {
                get { return Common.GetBit(this._values[0], 14); }
                set { this._values[0] = Common.SetBit(this._values[0], 14, value); }
            }

            [XmlAttribute("Блокировка_номер")]
            public string BlockingNumber
            {
                get { return Validator.Get(this._values[1], Strings.Logic); }
                set { this._values[1] = Validator.Set(value, Strings.Logic); }
            }

            [XmlAttribute("Срабатывание_уставка")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(this._values[2], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("Срабатывание_время")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(this._values[3]); }
                set { this._values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("Срабатывание_АПВ")]
            public double ConstraintAPV
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(this._values[4], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }

            [XmlAttribute("Возврат_время")]
            public ulong ReturnTime
            {
                get { return Measuring.GetTime(this._values[5]); }
                set { this._values[5] = Measuring.SetTime(value); }
            }
        }

        #endregion

        #region Защита двигателя

        private slot _engineDefenses = new slot(0x1120, 0x1133);

        public void LoadEngineDefenses()
        {
            LoadSlot(DeviceNumber, this._engineDefenses, "LoadEngineDefenses" + DeviceNumber, this);
        }

        public void SaveEngineDefenses()
        {
            Array.ConstrainedCopy(this.EngineDefenses.ToUshort(), 0, this._engineDefenses.Value, 0,
                CEngingeDefenses.LENGTH);
            SaveSlot(DeviceNumber, this._engineDefenses, "SaveEngineDefenses" + DeviceNumber, this);
        }




        private CEngingeDefenses _cengineDefenses = new CEngingeDefenses();

        [XmlElement("Защита_двигателя")]
        public CEngingeDefenses EngineDefenses
        {
            get { return this._cengineDefenses; }
            set { this._cengineDefenses = value; }
        }

        [XmlElement("Время_нагрева_двигателя")]
        public ushort EngineHeatingTime
        {
            get { return this._engineDefenses.Value[0]; }
            set { this._engineDefenses.Value[0] = value; }
        }

        [XmlElement("Время_охлаждения_двигателя")]
        public ushort EngineCoolingTime
        {
            get { return this._engineDefenses.Value[1]; }
            set { this._engineDefenses.Value[1] = value; }
        }

        [XmlElement("Ток_двигателя")]
        public double EngineIn
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[2], ConstraintKoefficient.K_4000); }
            set { this._engineDefenses.Value[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        [XmlElement("Уставка_срабатывания_двигателя")]
        public double EngineWorkConstraint
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[3], ConstraintKoefficient.K_4000); }
            set { this._engineDefenses.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        [XmlElement("Время_пуска_двигателя")]
        public ulong EnginePuskTime
        {
            get { return Measuring.GetTime(this._engineDefenses.Value[4]); }
            set { this._engineDefenses.Value[4] = Measuring.SetTime(value); }
        }

        [XmlElement("Гор_состояние_двигателя")]
        [Category("Защиты двигателя")]
        public double EngineHeatPuskConstraint
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[5], ConstraintKoefficient.K_25600); }
            set { this._engineDefenses.Value[5] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }

        [XmlElement("Вход_Q_сброс")]
        [Category("Защиты двигателя")]
        public string EngineResetQ
        {
            get
            {
                ushort index = (ushort) (this._engineDefenses.Value[6] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._engineDefenses.Value[6] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("Вход_N_пуск")]
        [Category("Защиты двигателя")]
        public string EnginePuskN
        {
            get
            {
                ushort index = (ushort) (this._engineDefenses.Value[7] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { this._engineDefenses.Value[7] = (ushort) Strings.Logic.IndexOf(value); }
        }

        [XmlElement("Режим_Q")]
        [Category("Защиты двигателя")]
        public string QMode
        {
            get { return Validator.Get(this._engineDefenses.Value[12], Strings.ModesLight, 0); }
            set { this._engineDefenses.Value[12] = Validator.Set(value, Strings.ModesLight, this._engineDefenses.Value[12], 0); }
        }

        [XmlElement("Уставка_Q")]
        public double QConstraint
        {
            get { return Measuring.GetConstraint(this._engineDefenses.Value[13], ConstraintKoefficient.K_25600); }
            set { this._engineDefenses.Value[13] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }

        [XmlElement("Время_Q")]
        public ushort QTime
        {
            get { return this._engineDefenses.Value[14]; }
            set { this._engineDefenses.Value[14] = value; }
        }

        [XmlElement("N_пуск")]
        public ushort BlockingPuskCount
        {
            get { return this._engineDefenses.Value[16]; }
            set { this._engineDefenses.Value[16] = value; }
        }

        [XmlElement("N_гор")]
        public ushort BlockingHeatCount
        {
            get { return this._engineDefenses.Value[15]; }
            set { this._engineDefenses.Value[15] = value; }
        }

        [XmlElement("T_длит")]
        public ushort BlockingDuration
        {
            get { return this._engineDefenses.Value[17]; }
            set { this._engineDefenses.Value[17] = value; }
        }

        [XmlElement("T_блок")]
        public ushort BlockingTime
        {
            get { return this._engineDefenses.Value[18]; }
            set { this._engineDefenses.Value[18] = value; }
        }

        public class CEngingeDefenses : ICollection
        {
            public const int LENGTH = 19;
            public const int COUNT = 2;

            private ushort[] _buffer = new ushort[LENGTH];
            private List<EngineDefenseItem> _defenseList = new List<EngineDefenseItem>(COUNT);

            public CEngingeDefenses()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Защиты двигателя", LENGTH,
                        "Буфер для защит двигателя должен быть " + LENGTH);
                }
                this._buffer = buffer;
                for (int i = 8; i < 12; i += EngineDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[EngineDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, EngineDefenseItem.LENGTH);
                    this._defenseList.Add(new EngineDefenseItem(temp));
                }
                this._defenseList[0].Name = "Q>";
                this._defenseList[1].Name = "Q>>";
            }

            [DisplayName("Количество")]
            public int Count
            {
                get { return this._defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                Array.ConstrainedCopy(this._buffer, 0, buffer, 0, LENGTH);

                for (int i = 0; i < COUNT; i++)
                {
                    Array.ConstrainedCopy(this._defenseList[i].Values, 0, buffer, 8 + EngineDefenseItem.LENGTH*i,
                        EngineDefenseItem.LENGTH);
                }
                return buffer;
            }

            public EngineDefenseItem this[int i]
            {
                get { return this._defenseList[i]; }
                set { this._defenseList[i] = value; }
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {

            }

            public void Add(EngineDefenseItem item)
            {
                this._defenseList.Add(item);
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion
        }

        public class EngineDefenseItem
        {
            public const int LENGTH = 2;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "Защита двигателя";
            }

            public EngineDefenseItem()
            {
            }

            public EngineDefenseItem(ushort[] buffer)
            {
                this.SetItem(buffer);
            }

            private string _name;

            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }


            [XmlAttribute("Значения")]
            public ushort[] Values
            {
                get { return this._values; }
                set { this.SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Буфер защиты по напряжению", LENGTH,
                        "Буфер защит должен быть длиной" + LENGTH);
                }
                this._values = buffer;
            }

            [XmlAttribute("Режим")]
            public string Mode
            {
                get { return Validator.Get(this._values[0], Strings.Modes, 0, 1, 2); }
                set { this._values[0] = Validator.Set(value, Strings.Modes, this._values[0], 0, 1, 2); }
            }

            [XmlAttribute("Осц")]
            public bool Osc
            {
                get { return Common.GetBit(this._values[0], 4); }
                set { this._values[0] = Common.SetBit(this._values[0], 4, value); }
            }

            [XmlAttribute("АВР")]
            public bool AVR
            {
                get { return Common.GetBit(this._values[0], 5); }
                set { this._values[0] = Common.SetBit(this._values[0], 5, value); }
            }

            [XmlAttribute("АПВ")]
            public bool APV
            {
                get { return Common.GetBit(this._values[0], 6); }
                set { this._values[0] = Common.SetBit(this._values[0], 6, value); }
            }

            [XmlAttribute("УРОВ")]
            public bool UROV
            {
                get { return Common.GetBit(this._values[0], 7); }
                set { this._values[0] = Common.SetBit(this._values[0], 7, value); }
            }

            [XmlAttribute("Уставка")]
            public double WorkConstraint
            {
                get
                {
                    double ret;
                    ret = Measuring.GetConstraint(this._values[1], ConstraintKoefficient.K_25600);
                    return ret;
                }
                set { this._values[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
            }
        }

        #endregion

        #region Осциллограмма

        private MemoryEntity<OneWordStruct> _resetOsc;

        public MemoryEntity<OneWordStruct> ResetOsc
        {
            get { return this._resetOsc; }
        }

        private TzlOscilloscope _oscilloscope;

        public TzlOscilloscope Oscilloscope
        {
            get { return this._oscilloscope; }
        }

        #region Новый Осциллограф

        #region Журнал

        ushort[] _oscilloscopeJournalRecord = new ushort[_sizeOscJournal];

        public ushort[] OscJournal
        {
            get { return this._oscilloscopeJournalReadSlot.Value; }
        }

        public bool OscExist
        {
            get
            {
                return this._oscilloscopeJournalReadSlot.Value[19] != 0
                       && this._oscilloscopeJournalReadSlot.Value[8] == 0
                       && this._oscilloscopeJournalReadSlot.Value[9] == 0;
            }
        }

        public ushort[] OscJournalRecord
        {
            get { return this._oscilloscopeJournalRecord; }
            set { this._oscilloscopeJournalRecord = value; }
        }

        public string OscYear
        {
            get
            {
                if (this._oscilloscopeJournalRecord[1] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[1].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[1].ToString();
                }
            }
        }

        public string OscMonth
        {
            get
            {
                if (this._oscilloscopeJournalRecord[2] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[2].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[2].ToString();
                }
            }
        }

        public string OscDay
        {
            get
            {
                if (this._oscilloscopeJournalRecord[3] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[3].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[3].ToString();
                }
            }
        }

        public string OscHours
        {
            get
            {
                if (this._oscilloscopeJournalRecord[4] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[4].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[4].ToString();
                }
            }
        }

        public string OscMinutes
        {
            get
            {
                if (this._oscilloscopeJournalRecord[5] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[5].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[5].ToString();
                }
            }
        }

        public string OscSeconds
        {
            get
            {
                if (this._oscilloscopeJournalRecord[6] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[6].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[6].ToString();
                }
            }
        }

        public string OscMiliseconds
        {
            get
            {
                if (this._oscilloscopeJournalRecord[7] < 10)
                {
                    return "0" + (this._oscilloscopeJournalRecord[7]*10).ToString();
                }
                else
                {
                    return (this._oscilloscopeJournalRecord[7]*10).ToString();
                }
            }
        }

        public int OscReady
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[9], this._oscilloscopeJournalRecord[8]);
            }
        }

        public int OscPoint
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[11], this._oscilloscopeJournalRecord[10]);
            }
        }

        public int OscBegin
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[13], this._oscilloscopeJournalRecord[12]);
            }
        }

        public int OscLen
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[15], this._oscilloscopeJournalRecord[14]);
            }
        }

        public int OscAfter
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[17], this._oscilloscopeJournalRecord[16]);
            }
        }

        public ushort OscAlm
        {
            get { return this._oscilloscopeJournalRecord[18]; }
        }

        ushort temp = 0;

        public ushort OscRez
        {
            get
            {
                if (this._oscilloscopeJournalRecord[19] == 0)
                {
                    return this.temp;
                }
                else
                {
                    return this._oscilloscopeJournalRecord[19];
                }
            }
            set { this.temp = value; }
        }

        #endregion

        #region Осциллогрмма

        private static int _enableOscPageSize = 0x7c; //0x64;//

        public int VEnableOscPageSize
        {
            get { return _enableOscPageSize; }
            set { _enableOscPageSize = value; }
        }

        private static int _oscPageSize = 0x400;

        public int VOscPageSize
        {
            get { return _oscPageSize; }
            set { _oscPageSize = value; }
        }

        private static int _fullOscSize = 0x1A000; //106496

        public int VFullOscSize
        {
            get { return _fullOscSize; }
            set { _fullOscSize = value; }
        }

        private static int _oscSize = 0x19FF8; //106488

        public int VOscSize
        {
            get { return _oscSize; }
            set { _oscSize = value; }
        }

        public ushort[] VOscilloscope
        {
            get { return this._oscilloscopeRead.Value; }
        }

        #endregion

        #endregion

        #endregion

        #region Дата - время

        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, this._datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(0, 0, 0, 0, 50), 10,
                this);
        }

        [Browsable(false)]
        public byte[] DateTime
        {
            get { return Common.TOBYTES(this._datetime.Value, true); }
            set { this._datetime.Value = Common.TOWORDS(value, true); }
        }

        #endregion

        #region Функции загрузки/сохранения

        #region Аналоговая и дискретная базы данных

        public void LoadDiagnostic()
        {
            LoadBitSlot(DeviceNumber, this._diagnostic, "LoadDiagnostic" + DeviceNumber, this);
        }

        public void LoadDiagnosticCycle()
        {
            LoadSlotCycle(DeviceNumber, this._diagnostic, "LoadDiagnostic" + DeviceNumber, new TimeSpan(0, 0, 0, 0, 50),
                10, this);
        }

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, this._analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(0, 0, 0, 0, 50),
                10, this);
            if (Common.VersionConverter(DeviceVersion) >= 2.03)
            {
                LoadSlotCycle(DeviceNumber, this._analogAdd, "LoadAnalogSignalsAdd" + DeviceNumber,
                    new TimeSpan(0, 0, 0, 0, 50), 10, this);
            }
        }

        public void LoadAnalogSignals()
        {
            LoadSlot(DeviceNumber, this._analog, "LoadAnalogSignals" + DeviceNumber, this);
        }

        #endregion

        public void LoadInputSignals(string queueName = "LoadInputSignals")
        {
            LoadSlot(DeviceNumber, this._inputSignals, queueName + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._konfCount, "№" + DeviceNumber + " загрузить конфигурацию", this);
        }

        public void LoadTypeInterfaces()
        {
            LoadSlot(DeviceNumber, this._typeInterfaces, "LoadTypeInterfaces" + DeviceNumber, this, new TimeSpan(3000));
        }

        //Журнал осциллографа
        int _recordIndex = 0;

        public void LoadOscilloscopeJournal(int recordIndex)
        {
            this._recordIndex = recordIndex;
            LoadSlot(DeviceNumber, this._oscilloscopeJournalReadSlot,
                "LoadOscilloscopeJournal" + recordIndex + DeviceNumber, this);
        }

        //Осциллограмма
        private int _oscIndexRead = 0;
        int _temp = 0;

        public void LoadOscilloscopePage(int oscIndexRead)
        {
            if (_oscPageSize%_enableOscPageSize != 0)
            {
                this._temp = 1;
            }
            this._oscilloscopeRead = new slot(_startOscilloscope, _endOscilloscope);
            this.CurrentOscilloscope.Clear();
            this._oscIndexRead = oscIndexRead;
            for (int i = 0; i < _oscPageSize/_enableOscPageSize + this._temp; i++)
            {
                LoadSlot(DeviceNumber, this._oscilloscopeRead, "LoadOscilloscope" + oscIndexRead + i + DeviceNumber,
                    this);
                this.CurrentOscilloscope.Add(this._oscilloscopeRead);
                if ((i + 1) < (_oscPageSize/_enableOscPageSize) || (_oscPageSize%_enableOscPageSize == 0))
                {
                    this._oscilloscopeRead = new slot((ushort) (this._oscilloscopeRead.Start + _enableOscPageSize),
                        (ushort) (this._oscilloscopeRead.End + _enableOscPageSize));
                }
                else
                {
                    if ((i + 1) == (_oscPageSize/_enableOscPageSize))
                    {
                        this._oscilloscopeRead = new slot((ushort) (this._oscilloscopeRead.Start + _enableOscPageSize),
                            (ushort) (this._oscilloscopeRead.End + _oscPageSize%_enableOscPageSize));
                    }
                }
            }
        }

        #region Выходные сигналы и внешние защиты

        public void LoadOutputSignals()
        {
            this.OutputRele = new COutputRele(this);
            LoadSlot(DeviceNumber, this._outputSignals, "LoadOutputSignals" + DeviceNumber, this);
        }

        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, this._externalDefenses, "LoadExternalDefenses" + DeviceNumber, this);
        }

        public void SaveExternalDefenses()
        {
            if (CExternalDefenses.COUNT == this.ExternalDefenses.Count)
            {
                Array.ConstrainedCopy(this.ExternalDefenses.ToUshort(), 0, this._externalDefenses.Value, 0,
                    CExternalDefenses.LENGTH);
            }
            SaveSlot(DeviceNumber, this._externalDefenses, "SaveExternalDefenses" + DeviceNumber, this);
        }

        public void SaveOutputSignals()
        {
            if (COutputRele.COUNT == this.OutputRele.Count)
            {
                Array.ConstrainedCopy(this.OutputRele.ToUshort(), 0, this._outputSignals.Value, 0x40, COutputRele.LENGTH);
            }
            if (COutputIndicator.COUNT == this.OutputIndicator.Count)
            {
                Array.ConstrainedCopy(this.OutputIndicator.ToUshort(), 0, this._outputSignals.Value, 0x60,
                    COutputIndicator.LENGTH);
            }
            SaveSlot(DeviceNumber, this._outputSignals, "SaveOutputSignals" + DeviceNumber, this);
        }

        #endregion

        public void SaveInputSignals(string queueName = "SaveInputSignals")
        {
            SaveSlot(DeviceNumber, this._inputSignals, queueName + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._konfCount, "SaveKonfCount" + DeviceNumber, this);
        }

        public void SaveTypeInterface()
        {
            SaveSlot(DeviceNumber, this._typeInterfaces, "SaveTypeInterface" + DeviceNumber, this, new TimeSpan(3000));
        }

        public void CrushOsc()
        {
            SaveSlot6(DeviceNumber, this._oscCrush, "Сброс осциллограммы - Осциллогрмм нету", this);
        }

        #region Удаление и замедление/ускорение запросов

        public void RemoveDiagnostic()
        {
            this.MB.RemoveQuery("LoadDiagnostic" + DeviceNumber);
        }

        public void RemoveAnalogSignals()
        {
            this.MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
            this.MB.RemoveQuery("LoadAnalogSignalsAdd" + DeviceNumber);
        }

        public void RemoveDateTime()
        {
            this.MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            this.MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        public void SuspendSystemJournal(bool suspend)
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.RemoveQuery(q[i].name);
            }

        }

        public void RemoveSystemJournal()
        {
            List<Query> q = this.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                this.MB.RemoveQuery(q[i].name);
            }
            this._stopSystemJournal = true;
        }

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, this._datetime, "SaveDateTime" + DeviceNumber, this);
        }

        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, this._systemJournal[0], "LoadSJRecord0_" + DeviceNumber, this);
            this._stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            if (!this._inputSignals.Loaded)
            {
                this.LoadInputSignals();
            }
            this._stopAlarmJournal = false;
            LoadSlot(DeviceNumber, this._alarmJournal[0], "LoadALRecord0_" + DeviceNumber, this);
        }

        #endregion

        //Номер журнала осциллографа
        public void SaveOscilloscopeJournal(int index)
        {
            this._oscilloscopeJournalWriteSlot.Value[0] = (ushort) index;
            SaveSlot6(DeviceNumber, this._oscilloscopeJournalWriteSlot, "SaveOscilloscopeJournal" + DeviceNumber, this);
        }

        private int _oscIndexWrite = 0;

        public void SaveOscilloscopePage(int oscIndexWrite)
        {
            this._oscIndexWrite = oscIndexWrite;
            this._oscilloscopePageWrite.Value[0] = (ushort) oscIndexWrite;
            SaveSlot6(DeviceNumber, this._oscilloscopePageWrite, "SaveOscilloscope" + oscIndexWrite + DeviceNumber, this);
        }


        #endregion

        #region Сериализация

        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("Файл уставок МР741 поврежден", binFileName);
            }

            try
            {
                this.DeserializeSlot(doc, "/МР74X_уставки/Версия", _infoSlot);
                this.DeserializeSlot(doc, "/МР74X_уставки/Дата_время", this._datetime);
                this.DeserializeSlot(doc, "/МР74X_уставки/Входные_сигналы", this._inputSignals);
                this.DeserializeSlot(doc, "/МР74X_уставки/Выходные_сигналы", this._outputSignals);
                this.DeserializeSlot(doc, "/МР74X_уставки/Диагностика", this._diagnostic);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_двигателя", this._engineDefenses);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_внешние", this._externalDefenses);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_токовые_основные", this._tokDefensesMain);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_токовые_резервные", this._tokDefensesReserve);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_токовые2_основные", this._tokDefenses2Main);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_токовые2_резервные", this._tokDefenses2Reserve);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_напряжения_основные", this._voltageDefensesMain);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_напряжения_резервные", this._voltageDefensesReserve);
                this.DeserializeSlot(doc, "/МР74X_уставки/Автоматика", this._automaticsPage);
                this.DeserializeBit(doc, "/МР74X_уставки/Автоматика_разрешение_запуска", out this._bits);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_частоты_основные", this._frequenceDefensesMain);
                this.DeserializeSlot(doc, "/МР74X_уставки/Защиты_частоты_резервные", this._frequenceDefensesReserve);
                this.DeserializeSlot(doc, "/МР74X_уставки/Конфигурация_осцилографа", this._konfCount);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("Файл уставок МР741 поврежден", binFileName);
            }

            try
            {
                this.ExternalDefenses.SetBuffer(this._externalDefenses.Value);
                ushort[] buffer = new ushort[this._tokDefensesReserve.Size + this._tokDefenses2Reserve.Size];
                Array.ConstrainedCopy(this._tokDefensesReserve.Value, 0, buffer, 0, this._tokDefensesReserve.Size);
                Array.ConstrainedCopy(this._tokDefenses2Reserve.Value, 0, buffer, this._tokDefensesReserve.Size,
                    this._tokDefenses2Reserve.Size);
                this.TokDefensesReserve.SetBuffer(buffer);

                buffer = new ushort[this._tokDefensesMain.Size + this._tokDefenses2Main.Size];
                Array.ConstrainedCopy(this._tokDefensesMain.Value, 0, buffer, 0, this._tokDefensesMain.Size);
                Array.ConstrainedCopy(this._tokDefenses2Main.Value, 0, buffer, this._tokDefensesMain.Size,
                    this._tokDefenses2Main.Size);
                this.TokDefensesMain.SetBuffer(buffer);
                this.VoltageDefensesMain.SetBuffer(this._voltageDefensesMain.Value);
                this.VoltageDefensesReserve.SetBuffer(this._voltageDefensesReserve.Value);
                this.EngineDefenses.SetBuffer(this._engineDefenses.Value);
                ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                Array.ConstrainedCopy(this._outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                this.OutputRele.SetBuffer(releBuffer, Common.VersionConverter(DeviceVersion));
                ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                Array.ConstrainedCopy(this._outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                this.OutputIndicator.SetBuffer(outputIndicator, Common.VersionConverter(DeviceVersion));
                this.FrequenceDefensesMain.SetBuffer(this._frequenceDefensesMain.Value);
                this.FrequenceDefensesReserve.SetBuffer(this._frequenceDefensesReserve.Value);
            }
            catch
            {
                this.TokDefensesReserve = new CTokDefenses();
                this.TokDefensesMain = new CTokDefenses();
                this.VoltageDefensesMain = new CVoltageDefenses();
                this.VoltageDefensesReserve = new CVoltageDefenses();
                this.EngineDefenses = new CEngingeDefenses();
                this.OutputRele = new COutputRele(this);
                this.OutputIndicator = new COutputIndicator(this);
                this.FrequenceDefensesMain = new CFrequenceDefenses();
                this.FrequenceDefensesReserve = new CFrequenceDefenses();
                throw new FileLoadException("Файл уставок МР741 поврежден", binFileName);
            }
        }

        void DeserializeBit(XmlDocument doc, string nodePath, out BitArray Bits)
        {
            Bits = Common.StringToBits(doc.SelectSingleNode(nodePath).InnerText);
        }

        void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }

        public void Serialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("МР74X_уставки"));
            
            Array.ConstrainedCopy(this.EngineDefenses.ToUshort(), 0, this._engineDefenses.Value, 0,
                CEngingeDefenses.LENGTH);
            Array.ConstrainedCopy(this.ExternalDefenses.ToUshort(), 0, this._externalDefenses.Value, 0,
                CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(this.FrequenceDefensesMain.ToUshort(), 0, this._frequenceDefensesMain.Value, 0,
                CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(this.FrequenceDefensesReserve.ToUshort(), 0, this._frequenceDefensesReserve.Value, 0,
                CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(this.OutputRele.ToUshort(), 0, this._outputSignals.Value, 0x40, COutputRele.LENGTH);
            Array.ConstrainedCopy(this.OutputIndicator.ToUshort(), 0, this._outputSignals.Value, 0x60,
                COutputIndicator.LENGTH);
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort1(), 0, this._tokDefensesMain.Value, 0,
                CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesMain.ToUshort2(), 0, this._tokDefenses2Main.Value, 0,
                CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort1(), 0, this._tokDefensesReserve.Value, 0,
                CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(this.TokDefensesReserve.ToUshort2(), 0, this._tokDefenses2Reserve.Value, 0,
                CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(this.VoltageDefensesMain.ToUshort(), 0, this._voltageDefensesMain.Value, 0,
                CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(this.VoltageDefensesReserve.ToUshort(), 0, this._voltageDefensesReserve.Value, 0,
                CVoltageDefenses.LENGTH);

            this.SerializeSlot(doc, "Версия", _infoSlot);
            this.SerializeSlot(doc, "Дата_время", this._datetime);
            this.SerializeSlot(doc, "Входные_сигналы", this._inputSignals);
            this.SerializeSlot(doc, "Выходные_сигналы", this._outputSignals);
            this.SerializeSlot(doc, "Диагностика", this._diagnostic);
            this.SerializeSlot(doc, "Защиты_двигателя", this._engineDefenses);
            this.SerializeSlot(doc, "Защиты_внешние", this._externalDefenses);
            this.SerializeSlot(doc, "Защиты_токовые_основные", this._tokDefensesMain);
            this.SerializeSlot(doc, "Защиты_токовые_резервные", this._tokDefensesReserve);
            this.SerializeSlot(doc, "Защиты_токовые2_основные", this._tokDefenses2Main);
            this.SerializeSlot(doc, "Защиты_токовые2_резервные", this._tokDefenses2Reserve);
            this.SerializeSlot(doc, "Защиты_напряжения_основные", this._voltageDefensesMain);
            this.SerializeSlot(doc, "Защиты_напряжения_резервные", this._voltageDefensesReserve);
            this.SerializeSlot(doc, "Автоматика", this._automaticsPage);
            this.SerializeBits(doc, "Автоматика_разрешение_запуска", this._bits);
            this.SerializeSlot(doc, "Защиты_частоты_основные", this._frequenceDefensesMain);
            this.SerializeSlot(doc, "Защиты_частоты_резервные", this._frequenceDefensesReserve);
            this.SerializeSlot(doc, "Конфигурация_осцилографа", this._konfCount);

            doc.Save(binFileName);
        }

        void SerializeBits(XmlDocument doc, string nodeName, BitArray Bits)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Common.BitsToString(Bits);
            doc.DocumentElement.AppendChild(element);
        }

        void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }

        #endregion

        #region Группа уставок

        private slot _constraintSelector = new slot(0x400, 0x401);

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0, true, "МР741 запрос подтверждения", this);
        }

        public void SelectConstraintGroup(bool mainGroup)
        {
            this._constraintSelector.Value[0] = mainGroup ? (ushort) 0 : (ushort) 1;
            SaveSlot(DeviceNumber, this._constraintSelector, "МР741 №" + DeviceNumber + " переключение группы уставок",
                this);
        }

        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (TZL); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr741; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР741"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        private new void mb_CompleteExchange(object sender, Query query)
        {
            if (this._oscilloscope == null)
                // при загрузке проекта на данный метод подписывается буферный девайс (рефлексия девайса с контруктором без параметров
                // и последующей десериализации с файла.
            {
                this.MB.CompleteExchange -= this.mb_CompleteExchange;
                return;
            }
            this._oscilloscope.OnMbCompleteExchange(sender, query);

            if (query.name == "МР741 запрос подтверждения")
            {
                if (query.fail == 0)
                {
                    MessageBox.Show("Конфигурация записана успешно", "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Конфигурация не сохранена", "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            #region Защиты по частоте

            if ("LoadFrequenceDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._frequenceDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    this.FrequenceDefensesReserve.SetBuffer(this._frequenceDefensesReserve.Value);
                }
            }
            if ("LoadFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._frequenceDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    this.FrequenceDefensesMain.SetBuffer(this._frequenceDefensesMain.Value);
                    if (null != this.FrequenceDefensesLoadOk)
                    {
                        this.FrequenceDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != this.FrequenceDefensesLoadFail)
                    {
                        this.FrequenceDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveFrequenceDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, this.FrequenceDefensesSaveOk, this.FrequenceDefensesSaveFail);
            }

            #endregion

            #region Защиты напряжения

            if ("LoadVoltageDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._voltageDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    this.VoltageDefensesReserve.SetBuffer(this._voltageDefensesReserve.Value);
                }
            }
            if ("LoadVoltageDefensesMain" + DeviceNumber == query.name)
            {

                if (0 == query.fail)
                {
                    this._voltageDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);

                    this.VoltageDefensesMain.SetBuffer(this._voltageDefensesMain.Value);
                    if (null != this.VoltageDefensesLoadOk)
                    {
                        this.VoltageDefensesLoadOk(this);
                    }
                }
                else
                {
                    if (null != this.VoltageDefensesLoadFail)
                    {
                        this.VoltageDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveVoltageDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, this.VoltageDefensesSaveOk, this.VoltageDefensesSaveFail);
            }

            #endregion

            #region Токовые защиты

            if ("LoadTokDefensesMain2" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._tokDefenses2Main.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }
            if ("LoadTokDefensesReserve2" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._tokDefenses2Reserve.Value = Common.TOWORDS(query.readBuffer, true);
                }
            }
            if ("LoadTokDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    ushort[] buffer = new ushort[this._tokDefensesReserve.Size + this._tokDefenses2Reserve.Size];
                    this._tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    Array.ConstrainedCopy(this._tokDefensesReserve.Value, 0, buffer, 0, this._tokDefensesReserve.Size);
                    Array.ConstrainedCopy(this._tokDefenses2Reserve.Value, 0, buffer, this._tokDefensesReserve.Size,
                        this._tokDefenses2Reserve.Size);
                    this._ctokDefensesReserve.SetBuffer(buffer);
                }
            }
            if ("LoadTokDefensesMain" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    ushort[] buffer = new ushort[this._tokDefensesMain.Size + this._tokDefenses2Main.Size];
                    this._tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    Array.ConstrainedCopy(this._tokDefensesMain.Value, 0, buffer, 0, this._tokDefensesMain.Size);
                    Array.ConstrainedCopy(this._tokDefenses2Main.Value, 0, buffer, this._tokDefensesMain.Size,
                        this._tokDefenses2Main.Size);
                    this._ctokDefensesMain.SetBuffer(buffer);
                    if (null != this.TokDefensesLoadOK)
                    {
                        this.TokDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.TokDefensesLoadFail)
                    {
                        this.TokDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveTokDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, this.TokDefensesSaveOK, this.TokDefensesSaveFail);
            }

            #endregion

            #region Журнал аварий

            if (IsIndexQuery(query.name, "LoadALRecord"))
            {
                int index = GetIndex(query.name, "LoadALRecord");
                if (0 == query.fail)
                {
                    bool add = false;
                    add = this._alarmJournalRecords.AddMessage(query.readBuffer);
                    if (add)
                    {
                        if (this._alarmJournalRecords.IsMessageEmpty(index))
                        {
                            this.AlarmJournalLoadOk?.Invoke(this);
                        }
                        else
                        {
                            this.AlarmJournalRecordLoadOk?.Invoke(this, index);
                            index += 1;
                            if (index < ALARMJOURNAL_RECORD_CNT && !this._stopAlarmJournal)
                            {
                                LoadSlot(DeviceNumber, this._alarmJournal[index],
                                    "LoadALRecord" + index + "_" + DeviceNumber, this);
                            }
                            else
                            {
                                if (null != this.AlarmJournalLoadOk)
                                {
                                    this.AlarmJournalLoadOk(this);
                                }
                            }

                        }
                    }
                    else
                    {
                        if (null != this.AlarmJournalLoadOk)
                        {
                            this.AlarmJournalLoadOk(this);
                        }
                    }
                }
                else
                {
                    if (null != this.AlarmJournalRecordLoadFail)
                    {
                        this.AlarmJournalRecordLoadFail(this, index);
                    }
                }

            }

            #endregion

            #region Журнал системы

            if (IsIndexQuery(query.name, "LoadSJRecord"))
            {
                int index = GetIndex(query.name, "LoadSJRecord");
                if (0 == query.fail)
                {
                    if (false == this._systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                    {
                        if (null != this.SystemJournalLoadOk)
                        {
                            this.SystemJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != this.SystemJournalRecordLoadOk)
                        {
                            this.SystemJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < SYSTEMJOURNAL_RECORD_CNT && !this._stopSystemJournal)
                        {
                            LoadSlot(DeviceNumber, this._systemJournal[index]
                                , "LoadSJRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (null != this.SystemJournalLoadOk)
                            {
                                this.SystemJournalLoadOk(this);
                            }
                        }

                    }
                }
                else
                {
                    if (null != this.SystemJournalRecordLoadFail)
                    {
                        this.SystemJournalRecordLoadFail(this, index);
                    }
                }

            }

            #endregion

            #region Выходные сигналы

            if ("LoadOutputSignals" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                    ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                    Array.ConstrainedCopy(this._outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                    this._outputRele.SetBuffer(releBuffer, Common.VersionConverter(DeviceVersion));

                    ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                    Array.ConstrainedCopy(this._outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                    this._outputIndicator.SetBuffer(outputIndicator, Common.VersionConverter(DeviceVersion));
                    if (this.OutputSignalsLoadOK != null)
                    {
                        this.OutputSignalsLoadOK(this);
                    }
                }
                else
                {
                    if (this.OutputSignalsLoadFail != null)
                    {
                        this.OutputSignalsLoadFail(this);
                    }
                }
            }
            if ("SaveOutputSignals" + DeviceNumber == query.name)
            {
                Raise(query, this.OutputSignalsSaveOK, this.OutputSignalsSaveFail);
            }

            #endregion

            #region Внешние защиты

            if ("LoadExternalDefenses" + DeviceNumber == query.name)
            {

                if (0 == query.fail)
                {
                    this._externalDefenses.Value = Common.TOWORDS(query.readBuffer, true);
                    this.ExternalDefenses.SetBuffer(this._externalDefenses.Value);
                    if (null != this.ExternalDefensesLoadOK)
                    {
                        this.ExternalDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.ExternalDefensesLoadFail)
                    {
                        this.ExternalDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveExternalDefenses" + DeviceNumber == query.name)
            {
                Raise(query, this.ExternalDefensesSaveOK, this.ExternalDefensesSaveFail);
            }

            #endregion

            #region Входные сигналы

            #region

            if ("LoadInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, this.InputSignalsLoadOK, this.InputSignalsLoadFail, ref this._inputSignals);
                //this.TN = this.TNtoDevice;
                //this.TNNP = this.TNNPtoDevice;
                this.TT = this.TTtoDevice;
                this.TTNP = this.TTNPtoDevice;
            }
            if ("LoadTypeInterfaces" + DeviceNumber == query.name)
            {
                Raise(query, null, null, ref this._typeInterfaces);
            }

            if ("SaveInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, this.InputSignalsSaveOK, this.InputSignalsSaveFail);
            }

            if ("SaveTypeInterfaces" + DeviceNumber == query.name)
            {
                Raise(query, this.TypeInterfacesSaveOk, this.TypeInterfacesSaveFail);
            }

            #endregion

            if (query.name == "№" + DeviceNumber + " загрузить конфигурацию")
            {
                Raise(query, this.KonfCountLoadOK, this.KonfCountLoadFail, ref this._konfCount);
            }
            if ("SaveKonfCount" + DeviceNumber == query.name)
            {
                Raise(query, this.KonfCountSaveOK, this.KonfCountSaveFail);
            }

            #endregion

            #region Осциллограмма

            if ("LoadCount" + DeviceNumber == query.name)
            {
                Raise(query, this.LogicLoadOK, this.LogicLoadFail, ref this._countOsc);
            }

            #region Новый Осциллограф

            if ("LoadOscilloscopeJournal" + this._recordIndex + DeviceNumber == query.name)
            {
                Raise(query, this.OscJournalLoadOk, this.OscJournalLoadFail, ref this._oscilloscopeJournalReadSlot);
            }
            if ("SaveOscilloscopeJournal" + DeviceNumber == query.name)
            {
                Raise(query, this.OscJournalSaveOk, this.OscJournalSaveFail);
            }

            for (int i = 0; i < _oscPageSize/_enableOscPageSize + this._temp; i++)
            {
                if ("LoadOscilloscope" + this._oscIndexRead + i + DeviceNumber == query.name)
                {
                    this._oscilloscopeRead = this.CurrentOscilloscope[i];
                    Raise(query, this.OscilloscopeLoadOk, this.OscilloscopeLoadFail, ref this._oscilloscopeRead,
                        query.error);
                    this.CurrentOscilloscope[i] = this._oscilloscopeRead;
                }
            }
            if ("SaveOscilloscope" + this._oscIndexWrite + DeviceNumber == query.name)
            {
                Raise(query, this.OscilloscopeSaveOK, this.OscilloscopeSaveFail);
            }

            #endregion

            #endregion

            #region Для страницы автоматики

            if ("LoadAutomaticsPage" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._automaticsPage.Value = Common.TOWORDS(query.readBuffer, true);
                    this.AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(this._automaticsPage.Value[8])});
                    if (null != this.AutomaticsPageLoadOK)
                    {
                        this.AutomaticsPageLoadOK(this);
                    }
                }
                else
                {
                    if (null != this.AutomaticsPageLoadFail)
                    {
                        this.AutomaticsPageLoadFail(this);
                    }
                }

            }
            if ("SaveAutomaticsPage" + DeviceNumber == query.name)
            {
                Raise(query, this.AutomaticsPageSaveOK, this.AutomaticsPageSaveFail);
            }

            #endregion

            #region  Диагностика Дата Время Аналоги


            if ("LoadDiagnostic" + DeviceNumber == query.name)
            {
                Raise(query, this.DiagnosticLoadOk, this.DiagnosticLoadFail, ref this._diagnostic);
            }
            if ("LoadDateTime" + DeviceNumber == query.name)
            {
                Raise(query, this.DateTimeLoadOk, this.DateTimeLoadFail, ref this._datetime);
            }


            if ("LoadAnalogSignals" + DeviceNumber == query.name)
            {

                if (!this._inputSignals.Loaded)
                {
                    this.LoadInputSignals();
                }
                else
                {
                    Raise(query, this.AnalogSignalsLoadOK, this.AnalogSignalsLoadFail, ref this._analog);
                }
            }

            if ("LoadAnalogSignalsAdd" + DeviceNumber == query.name)
            {



                Raise(query, this.AnalogAddSignalsLoadOK, this.AnalogAddSignalsLoadFail, ref this._analogAdd);

            }

            #endregion

            base.mb_CompleteExchange(sender, query);
        }

        //[XmlIgnore]
        //[Browsable(false)]
        //private MemoryEntity<SystemJournalStructTZL> _systemJournal;

        public Type[] Forms
        {
            get
            {
                double ver;
                //this.SystemJournal = new MemoryEntity<SystemJournalStructTZL>("ЖС", this, 0x600);
                //this.IndexSystemJournal = new MemoryEntity<OneWordStruct>("Номер журнала системы", this, 0x600);
                
                if (DeviceVersion == VER_2)
                {
                    ver = 2.0;
                    DeviceVersion = "2.00";
                }
                else if (DeviceVersion == VER_2_S)
                {
                    ver = 2.0;
                    DeviceVersion = "2.00S";
                }
                else if (DeviceVersion == VER_117_124)
                {
                    ver = 1.17;
                    DeviceVersion = "1.17";
                }
                else
                {
                    ver = Common.VersionConverter(DeviceVersion);
                }
                if (ver >= 3.00)
                {
                    ver = 3.0;
                    DefaultTimeSpan = new TimeSpan(0, 0, 0, 0, 15);
                }
                this._outputRele = new COutputRele(this);
                this._outputIndicator = new COutputIndicator(this);
                if (ver <= 1.14)
                {
                    return new[]
                    {
                        typeof (BSBGLEF),
                        typeof (AlarmJournalForm),
                        typeof (ConfigurationForm),
                        typeof (MeasuringForm),
                        typeof (SystemJournalForm),
                        typeof (MR741OldOscilloscopeForm)
                    };
                }
                else
                {
                    return new[]
                    {
                        typeof (BSBGLEF),
                        typeof (AlarmJournalForm),
                        typeof (ConfigurationForm),
                        typeof (MeasuringForm),
                        typeof (SystemJournalForm),
                        typeof (Mr741OscilloscopeForm)
                    };
                }
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.10",
                    "1.11",
                    "1.12",
                    "1.13",
                    "1.14",
                    "1.15",
                    "1.16",
                    VER_117_124,
                    VER_2,
                    VER_2_S,
                    "3.00",
                    "3.00S",
                    "3.01S",
                    "3.02S",
                    "3.03S",
                    "3.04S",
                    "3.05S",
                    "3.06S",
                    "3.07"
                };
            }
        }
    }
}
