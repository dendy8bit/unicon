using System.Drawing;
using System.Windows.Forms;
using BEMN_XY_Chart;

namespace BEMN.TZL
{
    
    public abstract class Tool
    {
        protected Point _lastMouseXY;
        protected string _toolInfo;
        protected DAS_Net_XYChart _chart;
        private bool _activated;
        protected int _mouseDown;
        protected  ChartHelper _helper;
        protected ChartForm _form;

        public MouseEventHandler OnMouseDown;
        public MouseEventHandler OnMouseUp;
        public MouseEventHandler OnMouseMove;
        public MouseEventHandler OnMouseWheel;

        public Tool(ChartForm form)
        {
            _form = form;
        }
        
        public abstract Image ToolbarIcon
        {
            get;
        }
        
        public virtual void Activate(ChartHelper helper,DAS_Net_XYChart chart)
        {
            _helper = helper;
            _chart = chart;
            
            _oldCursor = _chart.Cursor;
            _chart.Cursor = _activeCursor;
                        
            Cursor.Show();
            _chart.MouseDown += new MouseEventHandler(ChartMouseDown);
            _chart.MouseUp += new MouseEventHandler(ChartMouseUp);
            _chart.MouseMove += new MouseEventHandler(ChartMouseMove);
        }

        public virtual void Deactivate()
        {
            Cursor.Current = _oldCursor;
            _chart.MouseDown -= new MouseEventHandler(ChartMouseDown);
            _chart.MouseUp -= new MouseEventHandler(ChartMouseUp);
            _chart.MouseMove -= new MouseEventHandler(ChartMouseMove);
        }
                


        protected  Cursor _activeCursor;
        protected Cursor _mouseDownCursor;
        protected Cursor _oldCursor;
                      
        public bool Activated
        {
            get { return _activated; }
            set { _activated = value; }
        }
        
        public  void ChartMouseDown(object sender,MouseEventArgs e)
        {
            if (null != _mouseDownCursor)
            {
                Cursor.Current = _mouseDownCursor;
            }
            _lastMouseXY = e.Location;
            _mouseDown++;
            if (null != OnMouseDown)
            {
                OnMouseDown(sender, e);
            }
        }

        public  void ChartMouseUp(object sender, MouseEventArgs e)
        {
            _mouseDown--;
            Cursor.Current = _activeCursor;
            if (null != OnMouseUp)
            {
                OnMouseUp(sender, e);
            }
        }

        public  void ChartMouseMove(object sender, MouseEventArgs e)
        {
            if (null != OnMouseMove)
            {
                OnMouseMove(sender, e);
            }
        }

        public void ChartMouseWheel(object sender, MouseEventArgs e)
        {
            if (null != OnMouseWheel)
            {
                OnMouseWheel(sender, e);
            }   
        }

    }
}
