using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN_XY_Chart;

namespace BEMN.TZL
{
    public partial class ChartForm : Form
    {
        public ChartForm()
        {
            InitializeComponent();
        }

        private Oscilloscope _oscilloscope;

        public Oscilloscope Oscilloscope
        {
            get { return _oscilloscope; }
            set { _oscilloscope = value; }
        }

        #region Fields

        private ChartHelper _analogHelper;
        private ChartHelper _analogUHelper;
        private ChartHelper _diskretHelper;

        private int _xmax;
        private CheckBox[] _analogChChecks;
        private CheckBox[] _analogUChChecks;
        private CheckBox[] _diskretChChecks;

        private Tool _activeTool;
        private ToolStripButton _oldButton;

        private ChartHelper _activeHelper;
        #endregion
      
        private bool _checked = false;
        bool Checked
        {
            get { return _checked; }
            set { _checked = value; }
        }

        private int Clicked = 0;
        bool MouseClicked = false;
        bool MouseMoved = false;
        int[] coords = new int[2];
        DAS_Net_XYChart.DAS_XYCurveVariable curve = new DAS_Net_XYChart.DAS_XYCurveVariable();
        int mark = 0;

        private int Split1;
        private int Split2;
        private int Split3;
        private int Split4;
        private int Split5;
        private int Split6;
        private int Split7;
        private int Split8;

        private static double[] FromBitArray(BitArray bits)
        {
            double[] ret = new double[bits.Count];
            for (int i = 0; i < bits.Count; i++)
            {
                ret[i] = bits[i] ? 1 : 0;
            }
            return ret;
        }

        #region Helpers
        
        public void UpdateYScroll()
        {
            _oyScroll.Minimum = 0;
            _oyScroll.Maximum = (int) (_activeHelper.KY - 1);
            bool tr1 = true;
            for (int i = 0; i < _activeHelper.KY; i++)
            {
                if ((((((Math.Abs(_activeHelper.Y_MAX) + Math.Abs(_activeHelper.Y_MIN))/2/_activeHelper.KY) +_activeHelper.OY)
                    < (Math.Abs(_activeHelper.Y_MAX) - ((i + 1)*(Math.Abs(_activeHelper.Y_MAX) + Math.Abs(_activeHelper.Y_MIN))/_activeHelper.KY)))
                    && (tr1)
                    && ((((Math.Abs(_activeHelper.Y_MAX) + Math.Abs(_activeHelper.Y_MIN))/2/_activeHelper.KY) + _activeHelper.OY)
                    > (Math.Abs(_activeHelper.Y_MAX) - ((i + 2)*((Math.Abs(_activeHelper.Y_MAX) + Math.Abs(_activeHelper.Y_MIN))/_activeHelper.KY))))))
                {
                    if ((_activeHelper.KY - 1) > 0)
                    {
                        if ((i + 1) <= _oyScroll.Maximum)
                        {
                            _oyScroll.Value = i + 1;
                            tr1 = false;
                        }
                    }
                }
            }
        }
        
        public void UpdateXScroll()
        {
            //analogHelper
            _oxScroll.Minimum = 0;
            _oxScroll.Maximum = (int) (_activeHelper.KX - 1);
            bool tr = true;
            for (int i = 0; i < _activeHelper.KX; i++)
            {
                if (((_activeHelper.OX > ((i + 1)*(_xmax/_activeHelper.KX))) && (tr) &&
                     (_activeHelper.OX < ((i + 2)*(_xmax/_activeHelper.KX)))))
                {
                    if ((i + 1) <= _oxScroll.Maximum)
                    {
                        _oxScroll.Value = i + 1;
                        tr = false;
                    }
                }
            }
        }

        public ToolStripButton CreateToolButton(Type toolType)
        {
            Tool tool = (Tool)Activator.CreateInstance(toolType, new object[] { this });
            ToolStripButton button = new ToolStripButton(tool.ToolbarIcon);
            button.ImageTransparentColor = Color.Magenta;
            button.Tag = tool;
            button.CheckOnClick = true;
            button.Click += new EventHandler(tool_Click);
            return button;
        }
        #endregion

        #region UI handlers
        void tool_Click(object sender, EventArgs e)
        {
            if (_activeTool != null)
            {
                _activeTool.Deactivate();
            }
            if (null != _oldButton)
            {
                _oldButton.Checked = false;
            }
            Tool tool = (Tool)(((ToolStripButton)sender).Tag);
            tool.Activate(_activeHelper, _activeHelper == _analogHelper ? _analogChart : _diskretChart);
            _activeTool = tool;
            _oldButton = (ToolStripButton)sender;

        }

        private void SplitInitialiser()
        {
            Split1 = splitContainer1.SplitterDistance;
            Split2 = splitContainer2.SplitterDistance;
            Split3 = splitContainer3.SplitterDistance;
            Split4 = splitContainer4.SplitterDistance;
            Split5 = splitContainer5.SplitterDistance;
            Split6 = splitContainer6.SplitterDistance;
            Split7 = splitContainer7.SplitterDistance;
            Split8 = splitContainer8.SplitterDistance;
        }

        private void ChartForm_Load(object sender, EventArgs e)
        {
            if (_oscilloscope.Dev == null)
            {
                _oscilloscope.Dev = "";
            }
            int _avarya = _oscilloscope.RezervAvary;
            Checked = true;
            toolStripButton6.Click += new EventHandler(toolStripButton6_Click);
            SplitInitialiser();
            _xmax = _oscilloscope.PointCount;
            _analogChart.MouseClick += new MouseEventHandler(Chart_MouseEnter);
            _analogUChart.MouseClick += new MouseEventHandler(Chart_MouseEnter);
            _diskretChart.MouseClick += new MouseEventHandler(Chart_MouseEnter);

            List<double[]> diskrets = GetDiskrets();

            _analogHelper = new ChartHelper(_analogChart, _xmax - 1, _oscilloscope.AnalogData,
                _oscilloscope.AnalogChannels, true, _oscilloscope.KoefShkaliI, _avarya/_oscilloscope.KoefZashiti);
            _analogUHelper = new ChartHelper(_analogUChart, _xmax - 1, _oscilloscope.AnalogUData,
                _oscilloscope.AnalogUChannels, true, _oscilloscope.KoefShkaliU, _avarya/_oscilloscope.KoefZashiti);
            _diskretHelper = new ChartHelper(_diskretChart, _xmax - 1, diskrets, _oscilloscope.DiskretChannels, false, 0,
                _avarya/_oscilloscope.KoefZashiti);

            _activeHelper = _analogHelper;
            _analogChart.BorderExteriorLength = 2;
            _analogUChart.BorderExteriorLength = 2;
            _diskretChart.BorderExteriorLength = 2;
            _analogHelper.InitChartLimits();
            _analogUHelper.InitChartLimits();
            _diskretHelper.InitChartLimits();

            CreateAnalogCurves();
            CreateAnalogUCurves();
            CreateDiskretCurves();

            _analogHelper.AddPoints();
            _analogUHelper.AddPoints();
            _diskretHelper.AddPoints();

            _analogChChecks = CreateChChecks(_oscilloscope.AnalogChannels, _analogChannelsPanel,
                    new EventHandler(AnalogChannel_CheckedChanged), false);
            _analogUChChecks = CreateChChecks(_oscilloscope.AnalogUChannels, _uAnalogChannelsUPanel,
                    new EventHandler(AnalogUChannel_CheckedChanged), false);
            _diskretChChecks = CreateChChecks(_oscilloscope.DiskretChannels, _diskretChannelsUPanel,
                    new EventHandler(DiskretChannel_CheckedChanged), true);

            _labelShkalaI.Text = "Ia,b,c - A , In - A /" + _oscilloscope.KoefI; //+ " * A";
            _labelShkalaU.Text = "Ua,b,c - � , Un - B /" + _oscilloscope.KoefU; //+ " * �";
            _labelShkalaI.Visible = true;
            _labelShkalaU.Visible = true;

            curve.bLineVisible = true;
            curve.iPointNumber = _xmax;
            curve.iPointSize = 5;
            curve.iLineWidth = 2;
            curve.ePointStyle = DAS_CurvePointStyle.BPTS_NONE;
            curve.bVisible = true;
            curve.iCurPriority = 0;
            curve.dblMax = 1000.0;
            curve.dblMin = -1000.0;
            curve.bYScaleVisible = true;
            curve.bYAtStart = true;
            curve.bAreaMode = false;
            curve.dblAreaBaseValue = 0.0;
        }

        private static CheckBox[] CreateChChecks(StringCollection channels, TableLayoutPanel panel, EventHandler handler, bool B)
        {
            CheckBox[] checks = new CheckBox[channels.Count];
            panel.RowCount = channels.Count;
            
            if (B)
            {
                panel.Top = 420;
            }
            
            for (int i = 0; i < channels.Count; i++)
            {
                CheckBox check = new CheckBox();
                
                check.Text = channels[i];
                check.Checked = true;
                check.Appearance = Appearance.Button;
                check.TextAlign = ContentAlignment.TopCenter;
                if (i==channels.Count-1)
                {
                    check.BackColor = ChartHelper.GetColor(23);
                }else
                {
                    check.BackColor = ChartHelper.GetColor(i);
                }
                
                panel.Controls.Add(check);
                checks[i] = check;
                checks[i].CheckedChanged += new EventHandler(handler);
            }
            return checks;
        }

        private List<double[]> GetDiskrets()
        {
            List<double[]> diskrets = new List<double[]>(_oscilloscope.DiskretData.Count);
            for (int i = 0; i < _oscilloscope.DiskretData.Count; i++)
            {
                diskrets.Add(new double[_xmax]);
                diskrets[i] = FromBitArray(_oscilloscope.DiskretData[i]);
            }
            return diskrets;
        }

        private void CreateDiskretCurves()
        {
            DAS_Net_XYChart.DAS_XYCurveVariable CurveItem;
            for (int i = 0; i < _oscilloscope.DiskretChannels.Count; i++)
            {
                if (i == _oscilloscope.DiskretChannels.Count - 1)
                {
                    CurveItem = _diskretHelper.CreateCurveItem(i, 22);
                }
                else
                {
                    CurveItem = _diskretHelper.CreateCurveItem(i, i);
                }

                if (!_diskretChart.AddCurve(CurveItem.strCurveName, CurveItem))
                {

                        throw new ApplicationException(); 
                }
            }
        }

        private void CreateAnalogCurves()
        {
            DAS_Net_XYChart.DAS_XYCurveVariable CurveItem;

            for (int i = 0; i < _oscilloscope.AnalogChannels.Count; i++)
            {
                if (i == _oscilloscope.AnalogChannels.Count - 1)
                {
                    CurveItem = _analogHelper.CreateCurveItem(i, 22);
                }
                else
                {
                    CurveItem = _analogHelper.CreateCurveItem(i, i);
                }

                if (!_analogChart.AddCurve(CurveItem.strCurveName, CurveItem))
                {
                    throw new ApplicationException();
                }
            }
        }

        private void CreateAnalogUCurves()
        {
            DAS_Net_XYChart.DAS_XYCurveVariable CurveItem;
            for (int i = 0; i < _oscilloscope.AnalogUChannels.Count; i++)
            {
                CurveItem = _analogUHelper.CreateCurveItem(i, i == _oscilloscope.AnalogUChannels.Count-1 ? 22 : i);

                if (!_analogUChart.AddCurve(CurveItem.strCurveName, CurveItem))
                {
                    throw new ApplicationException(); 
                }
            }
        }

        void Chart_MouseEnter(object sender, EventArgs e)
        {
            if (Checked)
            {
                toolStripButton6.Checked = false;
                Checked = false;
            }
            DAS_Net_XYChart activeChart;

            if (sender == _analogChart)
            {
                _activeHelper = _analogHelper;
                activeChart = _analogChart;
                _analogChart.BorderExteriorLength = 2;
                _analogUChart.BorderExteriorLength = 0;
                _diskretChart.BorderExteriorLength = 0;
            }
            else
            {
                if (sender == _analogUChart)
                {
                    _activeHelper = _analogUHelper;
                    activeChart = _analogUChart;
                    _analogChart.BorderExteriorLength = 0;
                    _analogUChart.BorderExteriorLength = 2;
                    _diskretChart.BorderExteriorLength = 0;
                }
                else
                {
                    _activeHelper = _diskretHelper;
                    activeChart = _diskretChart;
                    _analogChart.BorderExteriorLength = 0;
                    _analogUChart.BorderExteriorLength = 0;
                    _diskretChart.BorderExteriorLength = 2;
                }
            }
            UpdateYScroll();
            UpdateXScroll();

            if (null == _activeTool) return;
            _activeTool.Deactivate();
            _activeTool.Activate(_activeHelper, activeChart);
        }

        private void DiskretChannel_CheckedChanged(object sender, EventArgs e)
        {
            _diskretHelper.Channles.Clear();
            for (int i = 0; i < _diskretChChecks.Length; i++)
            {
                if (_diskretChChecks[i].Checked)
                {
                    if (i == _diskretChChecks.Length - 1)
                    {
                        _diskretChChecks[i].BackColor = ChartHelper.GetColor(23);
                        _diskretHelper.Channles.Add(i);
                    }
                    else
                    {
                        _diskretChChecks[i].BackColor = ChartHelper.GetColor(i);
                        _diskretHelper.Channles.Add(i);
                    }

                }
                else
                {
                    _diskretChChecks[i].BackColor = SystemColors.Control;
                }
            }
            _diskretHelper.UpdateChart();

        }

        private void AnalogChannel_CheckedChanged(object sender, EventArgs e)
        {
            _analogHelper.Channles.Clear();
            for (int i = 0; i < _analogChChecks.Length; i++)
            {
                if (_analogChChecks[i].Checked)
                {
                    if (i == _analogChChecks.Length-1)
                    {
                        _analogChChecks[i].BackColor = ChartHelper.GetColor(23);
                        _analogHelper.Channles.Add(i);
                    }else
                    {
                        _analogChChecks[i].BackColor = ChartHelper.GetColor(i);
                        _analogHelper.Channles.Add(i);
                    }

                }
                else
                {
                    _analogChChecks[i].BackColor = SystemColors.Control;
                }
            }
            _analogHelper.UpdateChart();

        }

        private void AnalogUChannel_CheckedChanged(object sender, EventArgs e)
        {
            _analogUHelper.Channles.Clear();
            for (int i = 0; i < _analogUChChecks.Length; i++)
            {
                if (_analogUChChecks[i].Checked)
                {
                    if (i == _analogUChChecks.Length - 1)
                    {
                        _analogUChChecks[i].BackColor = ChartHelper.GetColor(23);
                        _analogUHelper.Channles.Add(i);
                    }
                    else
                    {
                        _analogUChChecks[i].BackColor = ChartHelper.GetColor(i);
                        _analogUHelper.Channles.Add(i);
                    }
                }
                else
                {
                    _analogUChChecks[i].BackColor = SystemColors.Control;
                }
            }
            _analogUHelper.UpdateChart();

        }

        private void _oyScroll_Scroll(object sender, ScrollEventArgs e)
        {
            double ystep = 0;
            ystep = (Math.Abs(_activeHelper.Y_MIN) + Math.Abs(_activeHelper.Y_MAX)) / _activeHelper.KY;
            _activeHelper.OY = _activeHelper.Y_MAX - ystep * (e.NewValue + 1);
            _activeHelper.UpdateChart();
        }

        private void _oxScroll_Scroll(object sender, ScrollEventArgs e)
        {
            int OldCl = Clicked;
            visAnalog(OldCl);
            visAnalogU(OldCl);
            visDiskret(OldCl);
            visTime();
            double xstep2 = _xmax/_analogUHelper.KX;
            _analogUHelper.OX = (int) (e.NewValue*xstep2);
            _analogUHelper.UpdateChart();

            double xstep1 = _xmax/_analogHelper.KX;
            _analogHelper.OX = (int) (e.NewValue*xstep1);
            _analogHelper.UpdateChart();

            double xstep3 = _xmax/_diskretHelper.KX;
            _diskretHelper.OX = (int) (e.NewValue*xstep3);
            _diskretHelper.UpdateChart();
        }

        #endregion

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            int OldCl = Clicked;
            visAnalog(OldCl);
            visAnalogU(OldCl);
            visDiskret(OldCl);
            visTime();

            _analogHelper.KY = 1;
            _analogHelper.KX = 1;
            _analogHelper.OY = 0;
            _analogHelper.OX = 0;
            _analogHelper.Density = 1;
            _oxScroll.Maximum = 0;
            _oxScroll.Minimum = 0;
            _oyScroll.Maximum = 0;
            _oyScroll.Minimum = 0;
            double d = 1;
            if (_analogHelper.KY <= d)
            {
                _analogHelper.KY *= _analogHelper.KoefDomnogeniya;
                _analogHelper.KX = 1;
                _analogHelper.OY = 0;
                _analogHelper.OX = 0;
                _analogHelper.Density = 1;
                _oxScroll.Maximum = 0;
                _oxScroll.Minimum = 0;
                _oyScroll.Maximum *= (int) _analogHelper.KoefDomnogeniya;
                _oyScroll.Minimum = 0;
                _analogHelper.UpdateChart();
                UpdateYScroll();
            }
            _analogUHelper.KY = 1;
            _analogUHelper.KX = 1;
            _analogUHelper.OY = 0;
            _analogUHelper.OX = 0;
            _analogUHelper.Density = 1;
            _oxScroll.Maximum = 0;
            _oxScroll.Minimum = 0;
            _oyScroll.Maximum = 0;
            _oyScroll.Minimum = 0;
            double d1 = 1;
            if (_analogUHelper.KY <= d1)
            {
                _analogUHelper.KY *= _analogUHelper.KoefDomnogeniya;
                _analogUHelper.KX = 1;
                _analogUHelper.OY = 0;
                _analogUHelper.OX = 0;
                _analogUHelper.Density = 1;
                _oxScroll.Maximum = 1;
                _oxScroll.Minimum = 1;
                _oyScroll.Maximum *= (int) _analogUHelper.KoefDomnogeniya;
                _oyScroll.Minimum = 1;
                _analogUHelper.UpdateChart();
                UpdateYScroll();
            }
            _diskretHelper.KY = 1;
            _diskretHelper.KX = 1;
            _diskretHelper.OY = 0;
            _diskretHelper.OX = 0;
            _diskretHelper.Density = 1;
            _oxScroll.Maximum = 0;
            _oxScroll.Minimum = 0;
            _oyScroll.Maximum = 0;
            _oyScroll.Minimum = 0;
            double d2 = 1;
            if (_diskretHelper.KY <= d2)
            {
                _diskretHelper.KY *= _diskretHelper.KoefDomnogeniya;
                _diskretHelper.KX = 1;
                _diskretHelper.OY = 0;
                _diskretHelper.OX = 0;
                _diskretHelper.Density = 1;
                _oxScroll.Maximum = 0;
                _oxScroll.Minimum = 0;
                _oyScroll.Maximum *= (int) _diskretHelper.KoefDomnogeniya;
                _oyScroll.Minimum = 0;
                _diskretHelper.UpdateChart();
                UpdateYScroll();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (!Checked)
            {
                _activeHelper.KY *= 2;
                UpdateYScroll();
                _activeHelper.UpdateChart();
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (!Checked)
            {
                _activeHelper.KY /= 2;
                UpdateYScroll();
                _activeHelper.UpdateChart();
            }

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            int OldCl = Clicked;
            visAnalog(OldCl);
            visAnalogU(OldCl);
            visDiskret(OldCl);
            visTime();
            _analogHelper.KX /= 2;
            _analogHelper.UpdateChart();

            _analogUHelper.KX /= 2;
            _analogUHelper.UpdateChart();

            _diskretHelper.KX /= 2;
            UpdateXScroll();
            _diskretHelper.UpdateChart();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            int OldCl = Clicked;
            visAnalog(OldCl);
            visAnalogU(OldCl);
            visDiskret(OldCl);
            visTime();

            _analogHelper.KX *= 2;
            _analogHelper.UpdateChart();

            _analogUHelper.KX *= 2;
            _analogUHelper.UpdateChart();

            _diskretHelper.KX *= 2;
            _diskretHelper.UpdateChart();
            UpdateXScroll();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            int OldCl = Clicked;
            visAnalog(OldCl);
            visAnalogU(OldCl);
            visDiskret(OldCl);
            visTime();
            
            splitContainer1.SplitterDistance = Split1;
            splitContainer2.SplitterDistance = Split2;
            splitContainer3.SplitterDistance = Split3;
            splitContainer4.SplitterDistance = Split4;
            splitContainer5.SplitterDistance = Split5;
            splitContainer6.SplitterDistance = Split6;
            splitContainer7.SplitterDistance = Split7;
            splitContainer8.SplitterDistance = Split8;
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            int OldCl = Clicked;
            visAnalog(OldCl);
            visAnalogU(OldCl);
            visDiskret(OldCl);
            visTime();

            _analogHelper.KY = 1;
            _analogHelper.KX = 1;
            _analogHelper.OY = 0;
            _analogHelper.OX = 0;
            _analogHelper.Density = 1;
            _oxScroll.Maximum = 0;
            _oxScroll.Minimum = 0;
            _oyScroll.Maximum = 0;
            _oyScroll.Minimum = 0;
            _analogHelper.UpdateChart();

            _analogUHelper.KY = 1;
            _analogUHelper.KX = 1;
            _analogUHelper.OY = 0;
            _analogUHelper.OX = 0;
            _analogUHelper.Density = 1;
            _oxScroll.Maximum = 0;
            _oxScroll.Minimum = 0;
            _oyScroll.Maximum = 0;
            _oyScroll.Minimum = 0;
            _analogUHelper.UpdateChart();

            _diskretHelper.KY = 1;
            _diskretHelper.KX = 1;
            _diskretHelper.OY = 0;
            _diskretHelper.OX = 0;
            _diskretHelper.Density = 1;
            _oxScroll.Maximum = 0;
            _oxScroll.Minimum = 0;
            _oyScroll.Maximum = 0;
            _oyScroll.Minimum = 0;
            _diskretHelper.UpdateChart();
        }

        private void ChartForm_SizeChanged(object sender, EventArgs e)
        {
            SplitInitialiser();
        }

        private Color getCol(int i) 
        {
            Color col = new Color();
            switch (i) 
            {
                case 1: 
                    {
                        col = Color.White;
                        break;
                    }
                case 2:
                    {
                        col = Color.Red;
                        break;
                    }
            }
            return col;
        }

        public double AddPointToActiveChart(DAS_Net_XYChart activeChart, ChartHelper activeHelper, int I, int X) 
        {
            double _point = 0;
            
            curve.cColor = getCol(I);
            curve.strCurveName = "Marker" + I.ToString();
            
            activeChart.AddCurve(curve.strCurveName, curve);
            ArrayList ay = new ArrayList(2);
            ay.Add(activeHelper.Y_MAX);
            ay.Add(activeHelper.Y_MIN);
            _point = Math.Round((((double)activeHelper.OX + ((double)X * ((double)_xmax / (double)activeHelper.KX)) / ((double)activeChart.Width)) - ((0.01451 * ((double)_xmax / activeChart.Width)) / activeHelper.KX * (((double)activeChart.Width / 2) - X))), 0);
            if (_point < 0) 
            {
                _point = 0;
            }
            if (_point >= activeHelper.XMax)
            {
                _point = activeHelper.XMax-1;
            }
            for (int i = 0; i < ay.Count; i++)
            {
                activeChart.AddCurvePoint(curve.strCurveName, Math.Abs(_point), Convert.ToDouble(ay[i]));
            }
            return _point;
        }

        public void analogLables(double _point, int X, int Y, int Click)
        {
            switch (Click)
            {
                case 1:
                    {
                        if (_point < _oscilloscope.AnalogData[0].Length && _point > 0)
                        {
                            labelI1.Text = Math.Round(_oscilloscope.AnalogData[0][(int)_point], 2).ToString();
                            labelI2.Text = Math.Round(_oscilloscope.AnalogData[1][(int)_point], 2).ToString();
                            labelI3.Text = Math.Round(_oscilloscope.AnalogData[2][(int)_point], 2).ToString();
                            labelI4.Text = Math.Round(_oscilloscope.AnalogData[3][(int)_point], 2).ToString();
                        }
                        break;
                    }
                case 2:
                    {
                        if (_point < _oscilloscope.AnalogData[0].Length && _point > 0)
                        {
                            labelI1_1.Text = Math.Round(_oscilloscope.AnalogData[0][(int)_point], 2).ToString();
                            labelI2_1.Text = Math.Round(_oscilloscope.AnalogData[1][(int)_point], 2).ToString();
                            labelI3_1.Text = Math.Round(_oscilloscope.AnalogData[2][(int)_point], 2).ToString();
                            labelI4_1.Text = Math.Round(_oscilloscope.AnalogData[3][(int)_point], 2).ToString();
                        }
                        break;
                    }
            }
        }

        public void analogULables(double _point, int X, int Y, int Click)
        {
            switch (Click)
            {
                case 1:
                    {
                        if (_point < _oscilloscope.AnalogUData[0].Length && _point > 0)
                        {
                            UaLabel1.Text = Math.Round(_oscilloscope.AnalogUData[0][(int)_point], 2).ToString();
                            UbLabel1.Text = Math.Round(_oscilloscope.AnalogUData[1][(int)_point], 2).ToString();
                            UcLabel1.Text = Math.Round(_oscilloscope.AnalogUData[2][(int)_point], 2).ToString();
                            UoLabel1.Text = Math.Round(_oscilloscope.AnalogUData[3][(int)_point], 2).ToString();
                        }
                        break;
                    }
                case 2:
                    {
                        if (_point < _oscilloscope.AnalogUData[0].Length && _point > 0)
                        {
                            UaLabel2.Text = Math.Round(_oscilloscope.AnalogUData[0][(int)_point], 2).ToString();
                            UbLabel2.Text = Math.Round(_oscilloscope.AnalogUData[1][(int)_point], 2).ToString();
                            UcLabel2.Text = Math.Round(_oscilloscope.AnalogUData[2][(int)_point], 2).ToString();
                            UoLabel2.Text = Math.Round(_oscilloscope.AnalogUData[3][(int)_point], 2).ToString();
                        }
                        break;
                    }
            }
        }

        public void diskretLables(double _point, int X, int Y, int Click)
        {
            switch (Clicked)
            {
                case 1:
                    {
                        if (_point < _oscilloscope.DiskretData[0].Count && _point > 0)
                        {
                            D1Label1.Text = convertBoolToInt(_oscilloscope.DiskretData[0][(int)_point]).ToString();
                            D1Label2.Text = convertBoolToInt(_oscilloscope.DiskretData[1][(int)_point]).ToString();
                            D1Label3.Text = convertBoolToInt(_oscilloscope.DiskretData[2][(int)_point]).ToString();
                            D1Label4.Text = convertBoolToInt(_oscilloscope.DiskretData[3][(int)_point]).ToString();
                            D1Label5.Text = convertBoolToInt(_oscilloscope.DiskretData[4][(int)_point]).ToString();
                            D1Label6.Text = convertBoolToInt(_oscilloscope.DiskretData[5][(int)_point]).ToString();
                            D1Label7.Text = convertBoolToInt(_oscilloscope.DiskretData[6][(int)_point]).ToString();
                            D1Label8.Text = convertBoolToInt(_oscilloscope.DiskretData[7][(int)_point]).ToString();
                            D1Label9.Text = convertBoolToInt(_oscilloscope.DiskretData[8][(int)_point]).ToString();
                            D1Label10.Text = convertBoolToInt(_oscilloscope.DiskretData[9][(int)_point]).ToString();
                            D1Label11.Text = convertBoolToInt(_oscilloscope.DiskretData[10][(int)_point]).ToString();
                            D1Label12.Text = convertBoolToInt(_oscilloscope.DiskretData[11][(int)_point]).ToString();
                            D1Label13.Text = convertBoolToInt(_oscilloscope.DiskretData[12][(int)_point]).ToString();
                            D1Label14.Text = convertBoolToInt(_oscilloscope.DiskretData[13][(int)_point]).ToString();
                            D1Label15.Text = convertBoolToInt(_oscilloscope.DiskretData[14][(int)_point]).ToString();
                            D1Label16.Text = convertBoolToInt(_oscilloscope.DiskretData[15][(int)_point]).ToString();
                        }
                        break;
                    }
                case 2:
                    {
                        if (_point < _oscilloscope.DiskretData[0].Count && _point > 0)
                        {
                            D2Label1.Text = convertBoolToInt(_oscilloscope.DiskretData[0][(int)_point]).ToString();
                            D2Label2.Text = convertBoolToInt(_oscilloscope.DiskretData[1][(int)_point]).ToString();
                            D2Label3.Text = convertBoolToInt(_oscilloscope.DiskretData[2][(int)_point]).ToString();
                            D2Label4.Text = convertBoolToInt(_oscilloscope.DiskretData[3][(int)_point]).ToString();
                            D2Label5.Text = convertBoolToInt(_oscilloscope.DiskretData[4][(int)_point]).ToString();
                            D2Label6.Text = convertBoolToInt(_oscilloscope.DiskretData[5][(int)_point]).ToString();
                            D2Label7.Text = convertBoolToInt(_oscilloscope.DiskretData[6][(int)_point]).ToString();
                            D2Label8.Text = convertBoolToInt(_oscilloscope.DiskretData[7][(int)_point]).ToString();
                            D2Label9.Text = convertBoolToInt(_oscilloscope.DiskretData[8][(int)_point]).ToString();
                            D2Label10.Text = convertBoolToInt(_oscilloscope.DiskretData[9][(int)_point]).ToString();
                            D2Label11.Text = convertBoolToInt(_oscilloscope.DiskretData[10][(int)_point]).ToString();
                            D2Label12.Text = convertBoolToInt(_oscilloscope.DiskretData[11][(int)_point]).ToString();
                            D2Label13.Text = convertBoolToInt(_oscilloscope.DiskretData[12][(int)_point]).ToString();
                            D2Label14.Text = convertBoolToInt(_oscilloscope.DiskretData[13][(int)_point]).ToString();
                            D2Label15.Text = convertBoolToInt(_oscilloscope.DiskretData[14][(int)_point]).ToString();
                            D2Label16.Text = convertBoolToInt(_oscilloscope.DiskretData[15][(int)_point]).ToString();
                        }
                        break;
                    }
            }
        }

        public void timeLabels(double point,int cl) 
        {
            switch (cl)
            {
                case 1:
                    {
                        TimeLabel1.Text = Math.Round(point,0).ToString();
                        break;
                    }
                case 2:
                    {
                        TimeLabel2.Text = Math.Round(point,0).ToString();
                        break;
                    }
            }
            if (Clicked == 2)
            {
                DeltaLabel.Text = Math.Abs(Convert.ToInt32(TimeLabel2.Text) - Convert.ToInt32(TimeLabel1.Text)).ToString();
            }
        }

        public void visAnalog(int oldClick) 
        {
            ArrayList ay = new ArrayList();
            ArrayList ax = new ArrayList();
            ay.Add(0);
            ay.Add(0);
            ax.Add(0);
            ax.Add(0);
            labelI1.Text = "";
            labelI2.Text = "";
            labelI3.Text = "";
            labelI4.Text = "";
            labelI1_1.Text = "";
            labelI2_1.Text = "";
            labelI3_1.Text = "";
            labelI4_1.Text = "";
            for (int j = 0; j < oldClick; j++)
            {
                for (int i = 0; i < ay.Count; i++)
                {
                    _analogChart.RefreshCurvePoint("Marker" + (j + 1).ToString(), ax, ay);
                }
            }
            Clicked = 0;
        }

        public void visAnalogU(int oldClick)
        {
            ArrayList ay = new ArrayList();
            ArrayList ax = new ArrayList();
            ay.Add(0);
            ay.Add(0);
            ax.Add(0);
            ax.Add(0);
            UaLabel1.Text = "";
            UbLabel1.Text = "";
            UcLabel1.Text = "";
            UoLabel1.Text = "";
            UaLabel2.Text = "";
            UbLabel2.Text = "";
            UcLabel2.Text = "";
            UoLabel2.Text = "";
            for (int j = 0; j < oldClick; j++)
            {
                for (int i = 0; i < ay.Count; i++)
                {
                    _analogUChart.RefreshCurvePoint("Marker" + (j + 1).ToString(), ax, ay);
                }
            }
            Clicked = 0;
        }

        public void visDiskret(int oldClick)
        {
            ArrayList ay = new ArrayList();
            ArrayList ax = new ArrayList();
            ay.Add(0);
            ay.Add(0);
            ax.Add(0);
            ax.Add(0);
            D1Label1.Text = "";
            D1Label2.Text = "";
            D1Label3.Text = "";
            D1Label4.Text = "";
            D1Label5.Text = "";
            D1Label6.Text = "";
            D1Label7.Text = "";
            D1Label8.Text = "";
            D1Label9.Text = "";
            D1Label10.Text = "";
            D1Label11.Text = "";
            D1Label12.Text = "";
            D1Label13.Text = "";
            D1Label14.Text = "";
            D1Label15.Text = "";
            D1Label16.Text = "";
            D2Label1.Text = "";
            D2Label2.Text = "";
            D2Label3.Text = "";
            D2Label4.Text = "";
            D2Label5.Text = "";
            D2Label6.Text = "";
            D2Label7.Text = "";
            D2Label8.Text = "";
            D2Label9.Text = "";
            D2Label10.Text = "";
            D2Label11.Text = "";
            D2Label12.Text = "";
            D2Label13.Text = "";
            D2Label14.Text = "";
            D2Label15.Text = "";
            D2Label16.Text = "";
            for (int j = 0; j < oldClick; j++)
            {
                for (int i = 0; i < ay.Count; i++)
                {
                    _diskretChart.RefreshCurvePoint("Marker" + (j + 1).ToString(), ax, ay);
                }
            }
            Clicked = 0;
        }

        public void visTime()
        {
            TimeLabel1.Text = "";
            TimeLabel2.Text = "";
            DeltaLabel.Text = "";
        }

        public int convertBoolToInt(bool b) 
        {
            int ret = 0;
            switch (b) 
            {
                case true: 
                    {
                        ret = 1;
                        break;
                    }
                case false:
                    {
                        ret = 0;
                        break;
                    }
            }
            return ret;
        }

        private void _analogChart_MouseClick(object sender, MouseEventArgs e)
        {
            double point = 0;

            if (!MouseMoved)
            {
                Clicked++;
                if (e.Button.ToString() == "Left")
                {
                    if (Clicked < 3)
                    {
                        if (_analogChart.Visible)
                        {
                            point = AddPointToActiveChart(_analogChart, _analogHelper, Clicked, e.X);
                            analogLables(point, e.X, e.Y, Clicked);
                        }
                        if (_analogUChart.Visible)
                        {
                            point = AddPointToActiveChart(_analogUChart, _analogUHelper, Clicked, e.X);
                            analogULables(point, e.X, e.Y, Clicked);
                        }
                        if (_diskretChart.Visible)
                        {
                            point = AddPointToActiveChart(_diskretChart, _diskretHelper, Clicked, e.X);
                            diskretLables(point, e.X, e.Y, Clicked);
                        }
                        timeLabels(point, Clicked);
                        coords[Clicked - 1] = (int)point;
                    }
                }
                else
                {
                    int OldCl = Clicked;
                    visAnalog(OldCl);
                    visAnalogU(OldCl);
                    visDiskret(OldCl);
                    visTime();
                }
            }
        }

        private void _analogUChart_MouseClick(object sender, MouseEventArgs e)
        {
            double point = 0;

            if (!MouseMoved)
            {
                Clicked++;
                if (e.Button.ToString() == "Left")
                {
                    if (Clicked < 3)
                    {
                        if (_analogChart.Visible)
                        {
                            point = AddPointToActiveChart(_analogChart, _analogHelper, Clicked, e.X);
                            analogLables(point, e.X, e.Y, Clicked);
                        }
                        if (_analogUChart.Visible)
                        {
                            point = AddPointToActiveChart(_analogUChart, _analogUHelper, Clicked, e.X);
                            analogULables(point, e.X, e.Y, Clicked);
                        }
                        if (_diskretChart.Visible)
                        {
                            point = AddPointToActiveChart(_diskretChart, _diskretHelper, Clicked, e.X);
                            diskretLables(point, e.X, e.Y, Clicked);
                        }
                        timeLabels(point, Clicked);
                        coords[Clicked - 1] = (int)point;
                    }
                }
                else
                {
                    int OldCl = Clicked;
                    visAnalog(OldCl);
                    visAnalogU(OldCl);
                    visDiskret(OldCl);
                    visTime();
                }
            }
        }

        private void _diskretChart_MouseClick(object sender, MouseEventArgs e)
        {
            double point = 0;

            if (!MouseMoved)
            {
                Clicked++;
                if (e.Button.ToString() == "Left")
                {
                    if (Clicked < 3)
                    {
                        if (_analogChart.Visible)
                        {
                            point = AddPointToActiveChart(_analogChart, _analogHelper, Clicked, e.X);
                            analogLables(point, e.X, e.Y, Clicked);
                        }
                        if (_analogUChart.Visible)
                        {
                            point = AddPointToActiveChart(_analogUChart, _analogUHelper, Clicked, e.X);
                            analogULables(point, e.X, e.Y, Clicked);
                        }
                        if (_diskretChart.Visible)
                        {
                            point = AddPointToActiveChart(_diskretChart, _diskretHelper, Clicked, e.X);
                            diskretLables(point, e.X, e.Y, Clicked);
                        }
                        timeLabels(point, Clicked);
                        coords[Clicked - 1] = (int)point;
                    }
                }
                else
                {
                    int OldCl = Clicked;
                    visAnalog(OldCl);
                    visAnalogU(OldCl);
                    visDiskret(OldCl);
                    visTime();
                }
            }
        }

        private void _analogChart_MouseMove(object sender, MouseEventArgs e)
        {
            double point = 0;

            if (MouseClicked)
            {
                if (mark != 0)
                {
                    if (_analogChart.Visible)
                    {
                        point = AddPointToActiveChart(_analogChart, _analogHelper, mark, e.X);
                    }
                    if (_analogUChart.Visible)
                    {
                        point = AddPointToActiveChart(_analogUChart, _analogUHelper, mark, e.X);
                    }
                    if (_diskretChart.Visible)
                    {
                        point = AddPointToActiveChart(_diskretChart, _diskretHelper, mark, e.X);
                    }
                    if (point < 0)
                    {
                        if (_analogChart.Visible)
                        {
                            analogLables(0, e.X, e.Y, mark);
                        }
                        if (_analogUChart.Visible)
                        {
                            analogULables(0, e.X, e.Y, mark);
                        }
                        if (_diskretChart.Visible)
                        {
                            diskretLables(0, e.X, e.Y, mark);
                        }
                        coords[mark - 1] = (int)point;
                    }
                    else
                    {
                        if (_analogChart.Visible)
                        {
                            analogLables(point, e.X, e.Y, mark);
                        }
                        if (_analogUChart.Visible)
                        {
                            analogULables(point, e.X, e.Y, mark);
                        }
                        if (_diskretChart.Visible)
                        {
                            diskretLables(point, e.X, e.Y, mark);
                        }
                        coords[mark - 1] = (int)point;
                    }
                    timeLabels(point, mark);
                }
                MouseMoved = true;
            }
        }

        private void _analogChart_MouseDown(object sender, MouseEventArgs e)
        {
            MouseClicked = true;
            MouseMoved = false;
            double point = Math.Round((((double)_analogHelper.OX + ((double)e.X * ((double)_xmax / (double)_analogHelper.KX)) / ((double)_analogChart.Width)) - ((0.01451 * ((double)_xmax / _analogChart.Width)) / _analogHelper.KX * (((double)_analogChart.Width / 2) - e.X))),0);
            if (coords != null)
            {
                if ((point - 1) <= coords[0] && (point + 1) >= coords[0])
                {
                    mark = 1;
                }
                else if ((point - 1) <= coords[1] && (point + 1) >= coords[1])
                {
                    mark = 2;
                }
            }
        }

        private void _analogChart_MouseUp(object sender, MouseEventArgs e)
        {
            MouseClicked = false;
            mark = 0;
        }

        private void _analogUChart_MouseDown(object sender, MouseEventArgs e)
        {
            MouseClicked = true;
            MouseMoved = false;
            double point = Math.Round((((double)_analogUHelper.OX + ((double)e.X * ((double)_xmax / (double)_analogUHelper.KX)) / ((double)_analogUChart.Width)) - ((0.01451 * ((double)_xmax / _analogUChart.Width)) / _analogUHelper.KX * (((double)_analogUChart.Width / 2) - e.X))), 0);
            if (coords != null)
            {
                if ((point - 1) <= coords[0] && (point + 1) >= coords[0])
                {
                    mark = 1;
                }
                else if ((point - 1) <= coords[1] && (point + 1) >= coords[1])
                {
                    mark = 2;
                }
            }
        }

        private void _analogUChart_MouseUp(object sender, MouseEventArgs e)
        {
            MouseClicked = false;
            mark = 0;
        }

        private void _analogUChart_MouseMove(object sender, MouseEventArgs e)
        {
            double point = 0;

            if (MouseClicked)
            {
                if (mark != 0)
                {
                    if (_analogChart.Visible)
                    {
                        point = AddPointToActiveChart(_analogChart, _analogHelper, mark, e.X);
                    }
                    if (_analogUChart.Visible)
                    {
                        point = AddPointToActiveChart(_analogUChart, _analogUHelper, mark, e.X);
                    }
                    if (_diskretChart.Visible)
                    {
                        point = AddPointToActiveChart(_diskretChart, _diskretHelper, mark, e.X);
                    }
                    if (point < 0)
                    {
                        if (_analogChart.Visible)
                        {
                            analogLables(0, e.X, e.Y, mark);
                        }
                        if (_analogUChart.Visible)
                        {
                            analogULables(0, e.X, e.Y, mark);
                        }
                        if (_diskretChart.Visible)
                        {
                            diskretLables(0, e.X, e.Y, mark);
                        }
                        coords[mark - 1] = (int)point;
                    }
                    else
                    {
                        if (_analogChart.Visible)
                        {
                            analogLables(point, e.X, e.Y, mark);
                        }
                        if (_analogUChart.Visible)
                        {
                            analogULables(point, e.X, e.Y, mark);
                        }
                        if (_diskretChart.Visible)
                        {
                            diskretLables(point, e.X, e.Y, mark);
                        }
                        coords[mark - 1] = (int)point;
                    }
                    timeLabels(point, mark);
                }
                MouseMoved = true;
            }
        }

        private void _diskretChart_MouseDown(object sender, MouseEventArgs e)
        {
            MouseClicked = true;
            MouseMoved = false;
            double point = Math.Round((((double)_diskretHelper.OX + ((double)e.X * ((double)_xmax / (double)_diskretHelper.KX)) / ((double)_diskretChart.Width)) - ((0.01451 * ((double)_xmax / _diskretChart.Width)) / _diskretHelper.KX * (((double)_diskretChart.Width / 2) - e.X))), 0);
            if (coords != null)
            {
                if ((point - 1) <= coords[0] && (point + 1) >= coords[0])
                {
                    mark = 1;
                }
                else if ((point - 1) <= coords[1] && (point + 1) >= coords[1])
                {
                    mark = 2;
                }
            }
        }

        private void _diskretChart_MouseUp(object sender, MouseEventArgs e)
        {
            MouseClicked = false;
            mark = 0;
        }

        private void _diskretChart_MouseMove(object sender, MouseEventArgs e)
        {
            double point = 0;

            if (MouseClicked)
            {
                if (mark != 0)
                {
                    if (_analogChart.Visible)
                    {
                        point = AddPointToActiveChart(_analogChart, _analogHelper, mark, e.X);
                    }
                    if (_analogUChart.Visible)
                    {
                        point = AddPointToActiveChart(_analogUChart, _analogUHelper, mark, e.X);
                    }
                    if (_diskretChart.Visible)
                    {
                        point = AddPointToActiveChart(_diskretChart, _diskretHelper, mark, e.X);
                    }
                    if (point < 0)
                    {
                        if (_analogChart.Visible)
                        {
                            analogLables(0, e.X, e.Y, mark);
                        }
                        if (_analogUChart.Visible)
                        {
                            analogULables(0, e.X, e.Y, mark);
                        }
                        if (_diskretChart.Visible)
                        {
                            diskretLables(0, e.X, e.Y, mark);
                        }
                        coords[mark - 1] = (int)point;
                    }
                    else
                    {
                        if (_analogChart.Visible)
                        {
                            analogLables(point, e.X, e.Y, mark);
                        }
                        if (_analogUChart.Visible)
                        {
                            analogULables(point, e.X, e.Y, mark);
                        }
                        if (_diskretChart.Visible)
                        {
                            diskretLables(point, e.X, e.Y, mark);
                        }
                        coords[mark - 1] = (int)point;
                    }
                    timeLabels(point, mark);
                }
                MouseMoved = true;
            }
        }
    }
}