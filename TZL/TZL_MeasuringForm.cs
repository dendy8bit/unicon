using System;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.TZL.Properties;

namespace BEMN.TZL
{
    public partial class MeasuringForm : Form, IFormView
    {
        private TZL _device;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _ssl;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _automationLeds;
        private Timer _timer;

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(TZL device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this.Init();
        }

        private void Init()
        {
            if (Settings.Default.BreakerEnable)
            {
                this.breakerCheckBox.Checked = Settings.Default.BreakerEnable;
            }
            else
            {
                this.SetEnableBreaker(Settings.Default.BreakerEnable);
            }

            this._dateTimeBox.TZLFlag = true;
            this._manageLeds = new LedControl[]
            {
                this._manageLed1,this._manageLed2,this._manageLed3,this._manageLed4,this._k1,
                this._manageLed6,this._manageLed7,this._manageLed8
            };
            this._additionalLeds = new LedControl[] { this._addIndLed1, this._addIndLed2, this._addIndLed3, this._addIndLed4 };
            this._indicatorLeds = new LedControl[]{this._indLed1,this._indLed2,this._indLed3,this._indLed4,
                                              this._indLed5,this._indLed6,this._indLed7,this._indLed8};
            this._inputLeds = new LedControl[] { this._inD_led1,this._inD_led2,this._inD_led3,this._inD_led4,
                                            this._inD_led5,this._inD_led6,this._inD_led7,this._inD_led8,
                                            this._inD_led9,this._inD_led10,this._inD_led11,this._inD_led12,
                                            this._inD_led13,this._inD_led14,this._inD_led15,this._inD_led16,
                                            this._inL_led1,this._inL_led2,this._inL_led3,this._inL_led4,
                                            this._inL_led5,this._inL_led6,this._inL_led7,this._inL_led8};
            this._outputLeds = new LedControl[]{this._outLed1,this._outLed2,this._outLed3,this._outLed4,
                                            this._outLed5,this._outLed6,this._outLed7,this._outLed8};

            this._releLeds = new LedControl[]{this._releLed1,this._releLed2,this._releLed3,this._releLed4,
                                        this._releLed5,this._releLed6,this._releLed7,this._releLed8,
                                        this._releLed9,this._releLed10,this._releLed11,this._releLed12,this._releLed13,};

            this._limitLeds = new LedControl[]
            {
                this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4,this._ImaxLed5, this._ImaxLed6, this._ImaxLed7, this._ImaxLed8,
                this._I0maxLed1, this._I0maxLed2, this._I0maxLed3, this._I0maxLed4,this._I0maxLed5, this._I0maxLed6, this._I0maxLed7, this._I0maxLed8,
                this._InmaxLed1, this._InmaxLed2, this._InmaxLed3, this._InmaxLed4,this._InmaxLed5, this._InmaxLed6, this._InmaxLed7, this._InmaxLed8,
                this._FmaxLed1, this._FmaxLed2, this._FmaxLed3, this._FmaxLed4,this._FmaxLed5, this._FmaxLed6, this._FmaxLed7, this._FmaxLed8,
                this._UmaxLed1, this._UmaxLed2, this._UmaxLed3, this._UmaxLed4,this._UmaxLed5, this._UmaxLed6, this._UmaxLed7, this._UmaxLed8,
                this._U0maxLed1, this._U0maxLed2, this._U0maxLed3, this._U0maxLed4,this._U0maxLed5, this._U0maxLed6, this._U0maxLed7, this._U0maxLed8,
                this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8
            };

            this._ssl = new[]
            {
                this.sslLed1, this.sslLed2, this.sslLed3, this.sslLed4, this.sslLed5, this.sslLed6, this.sslLed7, this.sslLed8,
                this.sslLed9, this.sslLed10, this.sslLed11, this.sslLed12, this.sslLed13, this.sslLed14, this.sslLed15, this.sslLed16,
                this.sslLed17, this.sslLed18, this.sslLed19, this.sslLed20, this.sslLed21, this.sslLed22, this.sslLed23, this.sslLed24
            };

            this._faultStateLeds = new LedControl[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, this._faultStateLed4,
                this._faultStateLed5, this._faultStateLed6, this._faultStateLed7, this._faultStateLed8
            };
            this._faultSignalsLeds = new LedControl[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, this._faultSignalLed4,this._faultSignalLed5, 
                this._faultSignalLed6, this._faultSignalLed7, this._faultSignalLed8,this._faultSignalLed9, this._faultSignalLed10, 
                this._faultSignalLed11, this._faultSignalLed12,this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15,
                this._faultSignalLed16, this._faultSignalLed17, this._faultSignalLed18
            };
            this._automationLeds = new []
            {
                this._autoLed1, this._autoLed2, this._autoLed3 ,this._k2,
                this._autoLed5, this._autoLed6, this._autoLed7, this._autoLed8
            };

            this._device.DiagnosticLoadOk += HandlerHelper.CreateHandler(this, this.OnAnalogSignalsLoadOk);
            this._device.DiagnosticLoadFail += HandlerHelper.CreateHandler(this, this.OnAnalogSignalsLoadFail);
            this._device.DateTimeLoadOk += HandlerHelper.CreateHandler(this, this.OnDateTimeLoadOk);
            this._device.DateTimeLoadFail += HandlerHelper.CreateHandler(this, this.OnDateTimeLoadFail);
            this._device.AnalogSignalsLoadOK += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadOk);
            this._device.AnalogSignalsLoadFail += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadFail);

            this._device.AnalogAddSignalsLoadOK += HandlerHelper.CreateHandler(this, this.AddAnalogSignalsLoadOK);
            this._device.AnalogAddSignalsLoadFail += HandlerHelper.CreateHandler(this, this.AddAnalogSignalsLoadFail);
            double vers = Common.VersionConverter(this._device.DeviceVersion);

            if (vers < 1.1)
            {
                this._CosBox.Visible = false;
                this._PBox.Visible = false;
                this._QBox.Visible = false;
            }

            if (vers < 3.03)
            {
                this.Kgroup.Visible = false;
            }

            this._timer = new Timer();
            this._timer.Interval = 100;
            this._timer.Tick += new EventHandler(this._timer_Tick);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(TZL); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveDiagnostic();
            this._device.RemoveDateTime();
            this._device.RemoveAnalogSignals();
            this._device.ConnectionModeChanged -= this.StartStopRead;
            Settings.Default.BreakerEnable = this.breakerCheckBox.Checked;
            Settings.Default.Save();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();   
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.LoadAnalogSignalsCycle();
                this._device.LoadDiagnosticCycle();
                this._device.LoadTimeCycle();
            }
            else
            {
                this._device.RemoveDiagnostic();
                this._device.RemoveDateTime();
                this._device.RemoveAnalogSignals();
                this.AddAnalogSignalsLoadFail();
                this.OnAnalogSignalsLoadFail();
                this.OnDiagnosticLoadFail();
                this.OnDateTimeLoadFail();
            }
        }
        
        private void AddAnalogSignalsLoadFail()
        {
            if (Common.VersionConverter(this._device.Info.Version) >= 2.03)
            {
                this._UabBox.Text = string.Empty;
                this._UbcBox.Text = string.Empty;
                this._UcaBox.Text = string.Empty;
            }
        }
        
        private void AddAnalogSignalsLoadOK()
        {
            if (Common.VersionConverter(this._device.Info.Version) >= 2.03)
            {
                this._UabBox.Text = string.Format("Uab = {0}", this.DoubleToVolt(this._device.AddUab));
                this._UbcBox.Text = string.Format("Ubc = {0}", this.DoubleToVolt(this._device.AddUbc));
                this._UcaBox.Text = string.Format("Uca = {0}", this.DoubleToVolt(this._device.AddUca));
            }
        }

        private void OnAnalogSignalsLoadFail()
        {
            this._InBox.Text = string.Empty;
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;
            this._UnBox.Text = string.Empty;
            this._UaBox.Text = string.Empty;
            this._UbBox.Text = string.Empty;
            this._UcBox.Text = string.Empty;
            this._U0Box.Text = string.Empty;
            this._U1Box.Text = string.Empty;
            this._U2Box.Text = string.Empty;
            this._F_Box.Text = string.Empty;
            this._PBox.Text = string.Empty;
            this._QBox.Text = string.Empty;
            this._CosBox.Text = string.Empty;
        }
        
        private string Znak(string b, int first, int last)
        {
            string s = "";
            if (b[last] != '1')
            {
                if (b[first] == '0')
                {
                    s = "+";
                }
                else
                {
                    s = "-";
                }
            }
            return s;
        }

        private string DoubleToVolt(double value)
        {
            if (value<1000)
            {
                return string.Format("{0:F2} �", value);
            }
            else
            {
                return string.Format("{0:F2} ��", value/1000);
            }
        }

        private void OnAnalogSignalsLoadOk()
        {
            double version = Common.VersionConverter(this._device.DeviceVersion);
            string s = string.Empty;

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 0, 1);
            }
            this._InBox.Text = string.Format("In = " + s + " {0:F2} �", this._device.In);

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 2, 3);
            }
            this._IaBox.Text = string.Format("Ia = " + s + " {0:F2} �", this._device.Ia);

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 4, 5);
            }
            this._IbBox.Text = string.Format("Ib = " + s + " {0:F2} �", this._device.Ib);

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 6, 7);
            }
            this._IcBox.Text = string.Format("Ic = " + s + " {0:F2} �", this._device.Ic);

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 8, 9);
            }
            this._I0Box.Text = string.Format("I0 = " + s + " {0:F2} �", this._device.I0);

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 10, 11);
            }
            this._I1Box.Text = string.Format("I1 = " + s + " {0:F2} �", this._device.I1);

            if (version >= 1.00)
            {
                s = this.Znak(this._device.BdZnaki, 12, 13);
            }
            this._I2Box.Text = string.Format("I2 = " + s + " {0:F2} �", this._device.I2);

            this._IgBox.Text = string.Format("Ig = {0:F2} �", this._device.Ig);
            this._UnBox.Text = string.Format("Un = {0}",this.DoubleToVolt (this._device.Un));
            this._UaBox.Text = string.Format("Ua = {0}",this.DoubleToVolt (this._device.Ua));
            this._UbBox.Text = string.Format("Ub = {0}",this.DoubleToVolt (this._device.Ub));
            this._UcBox.Text = string.Format("Uc = {0}",this.DoubleToVolt (this._device.Uc));
            this._U0Box.Text = string.Format("U0 = {0}",this.DoubleToVolt (this._device.U0));
            if (version < 2.03)
            {
                _UabBox.Text = string.Format("Uab = {0}", DoubleToVolt(_device.Uab));
                _UbcBox.Text = string.Format("Ubc = {0}", DoubleToVolt(_device.Ubc));
                _UcaBox.Text = string.Format("Uca = {0}", DoubleToVolt(_device.Uca));
            }
            this._U1Box.Text = string.Format("U1 = {0}",this.DoubleToVolt (this._device.U1));
            this._U2Box.Text = string.Format("U2 = {0}",this.DoubleToVolt (this._device.U2));
            this._F_Box.Text = string.Format("F = {0:F2} ��", this._device.F);
            string razm = "���";
            double P;
            if (this._device.P > 999)
            {
                P = (double)(this._device.P / 1000);
                razm = "���";
            }
            else 
            {
                P = this._device.P;
            }
            this._PBox.Text = string.Format("P = {1}{0:F2} " + razm, P,this._device.PZnak);

            razm = "����";
            double Q;
            if (this._device.Q > 999)
            {
                Q = (double)(this._device.Q / 1000);
                razm = "����";
            }
            else
            {
                Q = this._device.Q;
            }
            this._QBox.Text = string.Format("Q = {1}{0:F2} "+razm, Q,this._device.QZnak);
            this._CosBox.Text = string.Format("Cos = {0:F2}", this._device.Cos);
        }
        
        private void OnDateTimeLoadFail()
        {
            this._dateTimeBox.Text = "";
        }

        private void OnDateTimeLoadOk()
        {
            this._dateTimeBox.DateTime = this._device.DateTime;
        }
        
        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._additionalLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._ssl);
            LedManager.TurnOffLeds(this._automationLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
            this._manageLed9.State = LedState.Off;
            this._k1.State= LedState.Off;
        }
        
        private void OnDiagnosticLoadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._device.ManageSignals);
            LedManager.SetLeds(this._additionalLeds, this._device.AdditionalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._device.Indicators);
            LedManager.SetLeds(this._inputLeds, this._device.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._device.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._device.Rele);
            LedManager.SetLeds(this._limitLeds, this._device.LimitSignals);
            LedManager.SetLeds(this._ssl, this._device.Ssl);
            LedManager.SetLeds(this._automationLeds, this._device.Automation);
            LedManager.SetLeds(this._faultSignalsLeds, this._device.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._device.FaultState);
            this._manageLed9.State = !this._device.FaultSignals[15] && this._device.ManageSignals[9]
                ? LedState.NoSignaled
                : LedState.Signaled;
            this._k1.State = this._device.ManageBitsDublicate[4]
                ? LedState.NoSignaled
                : LedState.Signaled;
        }

        private void _readTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            this._device.SuspendDateTime(!this._readTimeCheck.Checked);
            if (this._readTimeCheck.Checked)
            {
                this._systemTimeCheck.Checked = false;
            }
        }

        private void _writeTimeBut_Click(object sender, EventArgs e)
        {
            if (this._dateTimeBox.DateTime[3] < 13 && this._dateTimeBox.DateTime[3] > 0 &&
                this._dateTimeBox.DateTime[5] < 32 && this._dateTimeBox.DateTime[5] > 0 &&
                this._dateTimeBox.DateTime[7] < 24 && this._dateTimeBox.DateTime[7] >= 0 &&
                this._dateTimeBox.DateTime[9] < 60 && this._dateTimeBox.DateTime[9] >= 0 &&
                this._dateTimeBox.DateTime[11] < 60 && this._dateTimeBox.DateTime[11] >= 0)
            {
                this._device.DateTime = this._dateTimeBox.DateTime;
                this._device.SaveDateTime();
                this._readTimeCheck.Checked = this._systemTimeCheck.Checked = false;
            }
            else
            {
                MessageBox.Show("���������� ������� ����");
            }
        }

        private void _systemTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._systemTimeCheck.Checked)
            {
                this._readTimeCheck.Checked = false;
                this._timer.Start();
            }
            else
            {
                this._timer.Stop();
            }
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            string[] dates = System.Text.RegularExpressions.Regex.Split(DateTime.Now.ToShortDateString(), "\\.");
            if (null != dates[2]) //Year
            {
                dates[2] = dates[2].Remove(0, 2);
            }
            if (DateTime.Now.Hour < 10)
            {
                this._dateTimeBox.Text = string.Concat(dates[0], dates[1], dates[2]) + "0" + DateTime.Now.ToLongTimeString() + DateTime.Now.Millisecond.ToString();
            }
            else
            {
                this._dateTimeBox.Text = string.Concat(dates[0], dates[1], dates[2]) + DateTime.Now.ToLongTimeString() + DateTime.Now.Millisecond.ToString();
            }
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void ConfirmSDTU(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��741", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber,this._device);
            }
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "������������ ������� �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "������������ ������� ������");
        }

        private void _resetIndicatBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1804, "�������� ��������");
        }

        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "������ ���", this._device);
        }

        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "������� ���", this._device);
        }

        private void _selectConstraintGroupBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string msg;
            bool constraintGroup = false;
            if (this._manageLed4.State == LedState.NoSignaled)
            {
                constraintGroup = true;
                msg = "����������� �� �������� ������";
            }
            else if (this._manageLed4.State == LedState.Signaled)
            {
                msg = "����������� �� ��������� ������";
            }
            else
            {
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��741", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SelectConstraintGroup(constraintGroup);
            }
        }

        private void breakerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.SetEnableBreaker(this.breakerCheckBox.Checked);
        }

        private void SetEnableBreaker(bool enable)
        {
            this._breakerOffBut.Enabled = enable;
            this._breakerOnBut.Enabled = enable;
        }
    }
}