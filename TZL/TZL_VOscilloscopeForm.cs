﻿using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.TZL.Forms;

namespace BEMN.TZL
{
    public partial class VOscilloscopeForm : Form, IFormView
    {
        private List<int> _avaryBeginList;
        private List<List<int>> _avaryBeginOnOscList;
        private List<int[]> _avaryStartEndList;
        private int _currentOscilloscope;
        private TZL _device;
        private int _end;
        private StartEndOscilloscope _osc;
        private List<string[]> _oscilloscopeDateTimeList;
        private List<ushort[]> _oscilloscopeJournalList;
        private List<StartEndOscilloscope> _oscilloscopeList;
        private List<int[]> _oscilloscopeStartEndList;
        private FileStream _ReadCFGFile;
        private FileStream _ReadDATFile;
        private FileStream _ReadHDRFile;
        private bool _readOsc;
        private int _recordIndex;
        private int _start;
        private FileStream _WriteCFGFile;
        private FileStream _WriteDATFile;
        private FileStream _WriteHDRFile;
        private List<string> _almList;

        public VOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public VOscilloscopeForm(TZL device)
        {
            this.InitializeComponent();
            this._recordIndex = 0;
            this._oscilloscopeJournalList = new List<ushort[]>();
            this._avaryStartEndList = new List<int[]>();
            this._oscilloscopeStartEndList = new List<int[]>();
            this._oscilloscopeDateTimeList = new List<string[]>();
            this._avaryBeginList = new List<int>();
            this._avaryBeginOnOscList = new List<List<int>>();
            this._oscilloscopeList = new List<StartEndOscilloscope>();
            this._currentOscilloscope = 0;
            this._start = 0;
            this._end = 0;
            this._readOsc = true;
            this.components = null;
            this._almList = this.InitAlarmList();
            
            this._device = device;

            this._device.IndJournalOcs.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, OscilloscopeJournalIndWriteOk);
            this._device.IndJournalOcs.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, OscilloscopeJournalLoadFail);

            this._device.OscJournalLoadOk += HandlerHelper.CreateHandler(this, this.ReadJournalRecord);
            this._device.OscJournalLoadFail += HandlerHelper.CreateHandler(this, this.OscilloscopeJournalLoadFail);
            
            this._device.OscilloscopeLoadOk += HandlerHelper.CreateHandler(this, this.OnLoadOsc);
            this._device.OscilloscopeLoadFail += HandlerHelper.CreateHandler(this, this.OnOscilloscopeLoadFail);
            this._device.OscilloscopeSaveOK += HandlerHelper.CreateHandler(this, this.OscilloscopeSaveOK);
            this._device.OscilloscopeSaveFail += HandlerHelper.CreateHandler(this, this.OnOscilloscopeSaveFail);
        }

        private List<string> InitAlarmList()
        {
            return new List<string>
            {
                "",
                "I>",
                "I>>", 
                "I>>>",
                "I>>>>",
                "I2>", 
                "I2>>",
                "I0>", 
                "I0>>", 
                "In>", 
                "In>>", 
                "Ig>", 
                "I2/I1", 
                "F>",
                "F>>", 
                "F<", 
                "F<<", 
                "U>",
                "U>>",
                "U<", 
                "U<<",
                "U2>", 
                "U2>>",
                "U0>", 
                "U0>>",
                "ВЗ-1", 
                "ВЗ-2",
                "ВЗ-3", 
                "ВЗ-4", 
                "ВЗ-5",
                "ВЗ-6", 
                "ВЗ-7",
                "ВЗ-8",
                "Резерв1", 
                "Резерв2", 
                "Резерв3",
                "Резерв4", 
                "Резерв5",
                "Резерв6", 
                "Резерв7", 
                "Резерв8"
            };
        }

        private void OnOscilloscopeLoadFail()
        {
            MessageBox.Show("Осциллограмма прочитана с ошибками", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void OnOscilloscopeSaveFail()
        {
            MessageBox.Show("Невозможно прочитать осциллограмму", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void OscilloscopeJournalIndWriteOk()
        {
            this._recordIndex = this._device.IndJournalOcs.Value.Word;
            this._device.LoadOscilloscopeJournal(this._recordIndex);
        }

        private void OscilloscopeJournalLoadFail()
        {
            MessageBox.Show("Невозможно прочитать журнал осциллографа", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void OscilloscopeSaveOK()
        {
            if (!this._osc.OscilloscopeLoaded)
            {
                _device.LoadOscilloscopePage(this._osc.Index);
            }
        }

        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartLoad();
        }

        private void StartLoad()
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscilloscopeCountLabel.Visible = false;
            this._oscilloscopeCountCB.Visible = false;
            this._oscilloscopeCountCB.Items.Clear();
            this._oscilloscopeJournalList.Clear();
            this._avaryStartEndList.Clear();
            this._avaryBeginOnOscList.Clear();
            this.ReadJournal();
        }

        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            this._oscShowButton.Enabled = false;
            this.ShovLoadWindow();
        }

        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            this._oscShowButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._readOsc = true;
            this._oscilloscopeList.Clear();
            this._currentOscilloscope = this._oscilloscopeCountCB.SelectedIndex;
            _device.LoadInputSignals();
            try
            {
                this._osc = new StartEndOscilloscope(this._oscilloscopeStartEndList, this._oscilloscopeDateTimeList,
                    this._currentOscilloscope, this._device)
                {
                    PageLoaded = false,
                    OscilloscopeLoaded = false
                };
                this._oscProgressBar.Value = 0;
                int num = 0;
                if ((_device.VOscPageSize % _device.VEnableOscPageSize) != 0)
                {
                    num = 1;
                }
                this._oscProgressBar.Maximum = this._osc.PagesCount * ((_device.VOscPageSize / _device.VEnableOscPageSize) + num);
                this._oscProgressBar.Step = 1;
                this._oscStatus.Text = "Идет загрузка осциллограммы";
                _device.SaveOscilloscopePage(this._osc.StartPage);
                this._stopReadOsc.Enabled = true;
                this._oscJournalReadButton.Enabled = false;
            }
            catch
            {
            }
        }

        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this.ShovSaveWindow();
        }

        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Common.VersionConverter(_device.DeviceVersion) >= 2.0)
                {
                    VVOscilloscopeResultForm form;
                    if (this._currentOscilloscope <= (this._avaryBeginOnOscList.Count - 1))
                    {
                        form = new VVOscilloscopeResultForm(this._osc, this._avaryBeginOnOscList[this._currentOscilloscope]);
                    }
                    else
                    {
                        this._currentOscilloscope = 0;
                        this._avaryBeginOnOscList.Clear();
                        List<int> item = new List<int>();
                        item.Add(0);
                        this._avaryBeginOnOscList.Add(item);
                        form = new VVOscilloscopeResultForm(this._osc, this._avaryBeginOnOscList[this._currentOscilloscope]);
                    }
                    form.Show();
                }
                else
                {
                    OscilloscopeResultForm form;
                    if (this._currentOscilloscope <= (this._avaryBeginOnOscList.Count - 1))
                    {
                        form = new OscilloscopeResultForm(this._osc, this._avaryBeginOnOscList[this._currentOscilloscope]);
                    }
                    else
                    {
                        this._currentOscilloscope = 0;
                        this._avaryBeginOnOscList.Clear();
                        List<int> item = new List<int>();
                        item.Add(0);
                        this._avaryBeginOnOscList.Add(item);
                        form = new OscilloscopeResultForm(this._osc, this._avaryBeginOnOscList[this._currentOscilloscope]);
                    }
                    form.Show();
                }
            }
            catch
            {
                new OscilloscopeResultForm().Show();
            }
        }

        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._osc.InitArrays(Common.VersionConverter(this._device.DeviceVersion));
            for (int i = this._osc.FirstByteIndex; i < this._osc.LastByteIndex; i += _device.OscRez)
            {
                this._osc.CalculateOscParams(this._device, i);
            }
            this._osc.CalculateOscKoeffs();
            this._readOsc = false;
            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
        }
        
        public string ConvertToComtradeIndex1(string val)
        {
            if (val.Length < 6)
            {
                string str = "";
                for (int i = 0; i < (6 - val.Length); i++)
                {
                    str = str + "0";
                }
                val = str + val;
            }
            return val;
        }

        public string ConvertToComtradeIndex2(string val)
        {
            if (val.Length < 8)
            {
                string str = "";
                for (int i = 0; i < (8 - val.Length); i++)
                {
                    str = str + "0";
                }
                val = str + val;
            }
            return val;
        }

        private void Exception(OnDeviceEventHandler _func)
        {
            try
            {
                base.Invoke(_func);
            }
            catch
            {
                MessageBox.Show("Произошла критическая ошибка\nОбратитесь к разработчику", "ВНИМАНИЕ!");
            }
        }

        private void OnLoadOsc()
        {
            if (this._readOsc)
            {
                this._osc.ArrayToValues(_device);
                if (this._osc.PageLoaded)
                {
                    this._osc.Index++;
                    _device.SaveOscilloscopePage(this._osc.CurrentPage);
                    this._osc.PageLoaded = false;
                }
                if (this._osc.OscilloscopeLoaded)
                {
                    this._oscilloscopeList.Add(this._osc);
                    this._oscStatus.Text = "Загрузка осциллограммы завершена";
                    this._oscStatus.Text = "Идет преобразование данных";
                    this._osc.InitArrays(Common.VersionConverter(this._device.DeviceVersion));
                    try
                    {
                        int num2;
                        ushort[] destinationArray = new ushort[this._osc.Values.Length];

                        int num = (this._osc.Len - this._osc.After) * this._osc.Rez;
                        if (this._osc.Begin < this._osc.Point)
                        {
                            num2 = (this._osc.Begin + (_device.VOscSize / 2)) - this._osc.Point;
                        }
                        else
                        {
                            num2 = this._osc.Begin - this._osc.Point;
                        }
                        int sourceIndex = num2 - num;
                        if (sourceIndex < 0)
                        {
                            sourceIndex += this._osc.Len * this._osc.Rez;
                        }
                        Array.ConstrainedCopy(this._osc.Values, sourceIndex, destinationArray, 0, this._osc.Values.Length - sourceIndex);
                        Array.ConstrainedCopy(this._osc.Values, 0, destinationArray, this._osc.Values.Length - sourceIndex, sourceIndex);
                        Array.ConstrainedCopy(destinationArray, 0, this._osc.Values, 0, this._osc.Values.Length);
                    }
                    catch
                    {
                        MessageBox.Show("Ошибка переворота.");
                    }
                    for (int i = 0; i < this._osc.Values.Length; i += _device.OscRez)
                    {
                        this._osc.CalculateOscParams(this._device, i);
                    }
                    this._osc.CalculateOscKoeffs();
                    this._oscStatus.Text = "Преобразование данных завершено";
                    if (MessageBox.Show("Осциллограмма готова.", "!!!", MessageBoxButtons.OK) == DialogResult.OK)
                    {
                        Exception(new OnDeviceEventHandler(ShowOscButtonEnaibled));
                    }
                }
                this.Exception(new OnDeviceEventHandler(ProgressBarIncriment));
            }
        }

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._readOsc = false;
        }

        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            string vers;
            if (_device.DeviceVersion.Contains("-"))
            {
                vers = _device.DeviceVersion.Substring(0,
                    _device.DeviceVersion.IndexOf("-", System.StringComparison.Ordinal));
            }
            else
            {
                vers = _device.DeviceVersion;
            }
            if (Common.VersionConverter(vers) >= 2.0)
            {
                if (Common.VersionLiteral(_device.DeviceVersion) == "S")
                {
                    _device.VOscSize = 1015808;
                    _device.VFullOscSize = 1015808;
                }
                else
                {
                    _device.VOscSize = 2064384;
                    _device.VFullOscSize = 2064384;
                }
            }
            this.StartLoad();
        }

        private bool ParseCFG(out string _msg)
        {
            bool flag = true;
            StreamReader reader = null;
            _msg = string.Empty;
            try
            {
                reader = new StreamReader(this._ReadCFGFile);
                List<string> list = new List<string>();
                bool flag2 = false;
                do
                {
                    string item = reader.ReadLine();
                    if (item == null)
                    {
                        flag2 = true;
                    }
                    else
                    {
                        list.Add(item);
                    }
                }
                while (!flag2);
            }
            catch
            {
                flag = false;
                _msg = "Файл .cfg поврежден и не может быть открыт.";
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return flag;
        }

        private bool ParseDAT(out List<string[]> _datFileLines, out string _msg)
        {
            bool flag = true;
            StreamReader reader = null;
            _msg = string.Empty;
            _datFileLines = null;
            try
            {
                reader = new StreamReader(this._ReadDATFile);
                _datFileLines = new List<string[]>();
                bool flag2 = false;
                do
                {
                    string str = reader.ReadLine();
                    if (str == null)
                    {
                        flag2 = true;
                    }
                    else
                    {
                        string[] sourceArray = str.Split(new char[] { ',' });
                        string[] destinationArray = new string[sourceArray.Length - 2];
                        Array.Copy(sourceArray, 2, destinationArray, 0, destinationArray.Length);
                        _datFileLines.Add(destinationArray);
                    }
                }
                while (!flag2);
            }
            catch
            {
                flag = false;
                _msg = "Файл .dat поврежден и не может быть открыт.";
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return flag;
        }

        private bool ParseHDR(out string[] _avaryList, out string _msg)
        {
            bool flag = true;
            StreamReader reader = null;
            _avaryList = null;
            _msg = string.Empty;
            try
            {
                reader = new StreamReader(this._ReadHDRFile);
                List<string> list = new List<string>();
                bool flag2 = false;
                do
                {
                    string item = reader.ReadLine();
                    if (item == null)
                    {
                        flag2 = true;
                    }
                    else
                    {
                        list.Add(item);
                    }
                }
                while (!flag2);
                if (list[7] != "BEMN")
                {
                    flag = false;
                    _avaryList = new string[] { "0" };
                    _msg = "Эта осциллограмма не поддерживается Уникон'ом.";
                }
                this._device.OscRez = Convert.ToUInt16( list[8]);
                if ((list.Count >= 8) && flag)
                {
                    _avaryList = list[6].Split(new char[] { ',' });
                }
                for (int i = 0; i < _avaryList.Length; i++)
                {
                    try
                    {
                        int num2 = Convert.ToInt32(_avaryList[i]);
                    }
                    catch
                    {
                        _msg = "Журнал аварий осциллограммы не загружен!";
                        _avaryList = new string[] { "0" };
                    }
                }
            }
            catch
            {
                flag = false;
                _avaryList = new string[] { "0" };
                _msg = "Файл .cfg поврежден и не может быть открыт.";
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return flag;
        }

        private bool PrepareAllFiles(out string[] _avaryList, out List<string[]> _datFileLines, out string _msg)
        {
            bool flag = true;
            _avaryList = null;
            _datFileLines = null;
            _msg = string.Empty;
            try
            {
                if (this.ParseHDR(out _avaryList, out _msg))
                {
                    if (_msg != string.Empty)
                    {
                        MessageBox.Show(_msg, "Внимание!");
                    }
                    flag = this.ParseCFG(out _msg);
                    return this.ParseDAT(out _datFileLines, out _msg);
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public void PrepareOscArray(int avaryIndex)
        {
            this._start = this._avaryStartEndList[avaryIndex][0];
            this._end = this._avaryStartEndList[avaryIndex][1];
            this._oscilloscopeStartEndList.Add(new int[] { this._start, this._end, Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[3].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[9].Value.ToString()) });
            this._oscilloscopeDateTimeList.Add(new string[] { this._oscJournalDataGrid.Rows[avaryIndex].Cells[1].Value.ToString(), this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString() });
            this._avaryBeginList.Add(Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()) - Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()));
            this._avaryBeginOnOscList.Add(this._avaryBeginList);
            this._avaryBeginList = new List<int>();
        }

        private void PrepareReadComTradeFiles(string path)
        {
            try
            {
                this._avaryBeginOnOscList.Clear();
                this._ReadDATFile = new FileStream(Path.ChangeExtension(path, "dat"), FileMode.Open);
                this._ReadCFGFile = new FileStream(Path.ChangeExtension(path, "cfg"), FileMode.Open);
                this._ReadHDRFile = new FileStream(Path.ChangeExtension(path, "hdr"), FileMode.Open);
                List<string[]> list = new List<string[]>();
                string[] strArray2 = new string[] { "0" };
                string str = string.Empty;
                if (this.PrepareAllFiles(out strArray2, out list, out str))
                {
                    this._osc = new StartEndOscilloscope(list, this._device.OscRez,
                        Common.VersionConverter(this._device.DeviceVersion), this._device.VFullOscSize,
                        this._device.VOscPageSize);
                    this._currentOscilloscope = 0;
                    List<int> item = new List<int>();
                    for (int i = 0; i < strArray2.Length; i++)
                    {
                        item.Add(Convert.ToInt32(strArray2[i]));
                    }
                    this._avaryBeginOnOscList.Add(item);
                    MessageBox.Show("Осциллограмма загружена", "!!!");
                    this._oscShowButton.Enabled = true;
                }
                else
                {
                    MessageBox.Show(str, "Ошибка");
                }
            }
            catch
            {
            }
            finally
            {
                if (this._ReadDATFile != null)
                {
                    this._ReadDATFile.Close();
                }
                if (this._ReadCFGFile != null)
                {
                    this._ReadCFGFile.Close();
                }
                if (this._ReadHDRFile != null)
                {
                    this._ReadHDRFile.Close();
                }
            }
        }

        private void PrepareWriteComTradeFiles(string path, string filename)
        {
            try
            {
                this._WriteDATFile = new FileStream(path + ".dat", FileMode.CreateNew);
                this._WriteCFGFile = new FileStream(path + ".cfg", FileMode.CreateNew);
                this._WriteHDRFile = new FileStream(path + ".hdr", FileMode.CreateNew);
                this.SaveComTradeFiles();
            }
            catch
            {
                if (MessageBox.Show("Файл " + filename + " уже существует.\n\nЗаменить?", "WARNING!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    this._WriteDATFile = new FileStream(path + ".dat", FileMode.Create);
                    this._WriteCFGFile = new FileStream(path + ".cfg", FileMode.Create);
                    this._WriteHDRFile = new FileStream(path + ".hdr", FileMode.Create);
                    this.SaveComTradeFiles();
                }
                else
                {
                    this.ShovSaveWindow();
                }
            }
            finally
            {
                if (this._WriteDATFile != null)
                {
                    this._WriteDATFile.Close();
                }
                if (this._WriteCFGFile != null)
                {
                    this._WriteCFGFile.Close();
                }
                if (this._WriteHDRFile != null)
                {
                    this._WriteHDRFile.Close();
                }
            }
        }

        public void ProgressBarClear()
        {
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Step = 1;
            this._oscProgressBar.Maximum = this._osc.Values.Length / this._osc.OtschetLengs;
            this._oscProgressBar.Minimum = 0;
        }

        public void ProgressBarIncriment()
        {
            this._oscProgressBar.PerformStep();
        }

        public void ReadJournal()
        {
            this._recordIndex = 0;
            this._oscJournalDataGrid.Rows.Clear();
            this._oscStatus.Text = "Идет чтение журнала осциллографа";
            OneWordStruct str = this._device.IndJournalOcs.Value;
            str.Word = (ushort)this._recordIndex;
            this._device.IndJournalOcs.Value = str;
            this._device.IndJournalOcs.SaveStruct6();
        }

        public void ReadJournalRecord()
        {
            if (_device.OscExist)
            {
                this._oscilloscopeJournalList.Add(_device.OscJournal);
                _device.OscJournalRecord = _device.OscJournal;
                string str = _device.OscDay+"-"  +  _device.OscMonth + "-" +_device.OscYear + ", " + _device.OscHours + ":" + _device.OscMinutes + ":" + _device.OscSeconds + "." + _device.OscMiliseconds;
                int num = _device.VOscSize / 2;
                int num2 = _device.OscPoint + (_device.OscLen * _device.OscRez);
                string str2 = string.Empty;
                string str3 = string.Empty;
                if (num2 > num)
                {
                    str2 = string.Concat(new object[] { num2 / _device.OscRez, " [", (num2 - num) / _device.OscRez, "]" });
                    str3 = (num2 - num).ToString();
                }
                else
                {
                    str2 = num2.ToString();
                    str3 = num2.ToString();
                }
                this._avaryStartEndList.Add(new int[] { Convert.ToInt32(_device.OscPoint), Convert.ToInt32(str3) });
                string str4 = "";
                if (this._recordIndex == 0)
                {
                    str4 = "Посл.";
                }
                string str5 = this._device.OscAlm < this._almList.Count ? this._almList[this._device.OscAlm] : this._device.OscAlm.ToString();
                this._oscJournalDataGrid.Rows.Add(new object[]
                {
                    string.Concat(new object[] { this._recordIndex + 1, " / [", this._recordIndex, "] ", str4 }),
                    str,
                    _device.OscReady,
                    _device.OscPoint.ToString(),
                    str2, 
                    _device.OscBegin.ToString(),
                    _device.OscLen, 
                    _device.OscAfter,
                    str5,
                    _device.OscRez
                });
                this._recordIndex++;
                OneWordStruct wstr = _device.IndJournalOcs.Value;
                wstr.Word = (ushort)_recordIndex;
                _device.IndJournalOcs.Value = wstr;
                this._device.IndJournalOcs.SaveStruct6();
            }
            else
            {
                this._oscStatus.Text = "Чтение журнала осциллографа завершено";
                this._oscilloscopeStartEndList.Clear();
                this._oscilloscopeDateTimeList.Clear();
                this._start = 0;
                this._end = 0;
                if (this._avaryStartEndList.Count != 0)
                {
                    int num3;
                    this._oscReadButton.Enabled = true;
                    this._oscilloscopeCountLabel.Visible = true;
                    this._oscilloscopeCountCB.Visible = true;
                    for (num3 = 0; num3 < this._avaryStartEndList.Count; num3++)
                    {
                        this.PrepareOscArray(num3);
                    }
                    if (this._oscilloscopeStartEndList.Count != 0)
                    {
                        for (num3 = 0; num3 < this._oscilloscopeStartEndList.Count; num3++)
                        {
                            this._oscilloscopeCountCB.Items.Add(num3 + 1);
                        }
                        this._oscilloscopeCountCB.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("Журнал осциллографа пуст.", "Внимание!");
                }
            }
        }

        public bool SaveCFG()
        {
            bool flag = true;
            StreamWriter writer = new StreamWriter(this._WriteCFGFile);
            try
            {
                writer.Write(_device.NodeName.ToString() + "," + _device.DeviceNumber.ToString() + "\r\n");
                string[] strArray = new string[] { (_osc.DiskretData.Count+8).ToString(), ",", 8.ToString(), "A, ", _osc.DiskretData.Count.ToString(), " D\r\n" };
                writer.Write(string.Concat(strArray));
                writer.Write("1,Ia,a,,A,1,0,0,-32768,32767\r\n");
                writer.Write("2,Ib,b,,A,1,0,0,-32768,32767\r\n");
                writer.Write("3,Ic,c,,A,1,0,0,-32768,32767\r\n");
                writer.Write("4,In,n,,A,1,0,0,-32768,32767\r\n");
                writer.Write("5,Ua,a,,V,1,0,0,-32768,32767\r\n");
                writer.Write("6,Ub,b,,V,1,0,0,-32768,32767\r\n");
                writer.Write("7,Uc,c,,V,1,0,0,-32768,32767\r\n");
                writer.Write("8,Un,n,,V,1,0,0,-32768,32767\r\n");
                for (int i = 0; i < _osc.DiskretData.Count ; i++)
                {
                    writer.Write((i + 9).ToString() + ",D" + (i+1).ToString() + ",0\r\n");
                }
                writer.Write("50\r\n");
                writer.Write("1\r\n");
                writer.Write("1000," + ((this._osc.OscilloscopeLengs - 1)).ToString() + "\r\n");
                writer.Write(this._osc.Time + "\r\n");
                writer.Write(this._osc.AvaryTime + "\r\n");
                writer.Write("ASCII\r\n");
            }
            catch
            {
                MessageBox.Show("Файл " + this._WriteCFGFile + " не сохранен!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                flag = false;
            }
            finally
            {
                writer.Close();
            }
            return flag;
        }

        public void SaveComTradeFiles()
        {
            if ((this.SaveHDR() && this.SaveCFG()) && this.SaveDAT())
            {
                MessageBox.Show("Файлы ComTrade успешно сохранены.");
            }
        }

        private string prepareComTr(double val) 
        {
            string res = string.Empty;
            if (val == 0)
            {
                res = "0.0";
            }
            else 
            {
                string[] value = val.ToString().Split(',');
                res = value[0] + "." + value[1];
            }
            return res;
        }

        public bool SaveDAT()
        {
            bool flag = true;
            StreamWriter writer = new StreamWriter(this._WriteDATFile);
            try
            {
                for (int i = 0; i < this._osc.OscilloscopeLengs; i++)
                {
                    writer.Write(this.ConvertToComtradeIndex1(i.ToString()) + ",");
                    writer.Write(this.ConvertToComtradeIndex2(i.ToString() + "000"));
                    writer.Write("," + prepareComTr(this._osc.S1PageIaValues[i]));
                    writer.Write("," + prepareComTr(this._osc.S1PageIbValues[i]));
                    writer.Write("," + prepareComTr(this._osc.S1PageIcValues[i]));
                    writer.Write("," + prepareComTr(this._osc.S1PageInValues[i]));
                    writer.Write("," + prepareComTr(this._osc.UaValues[i]));
                    writer.Write("," + prepareComTr(this._osc.UbValues[i]));
                    writer.Write("," + prepareComTr(this._osc.UcValues[i]));
                    writer.Write("," + prepareComTr(this._osc.UnValues[i]));
                    for (int j = 0; j < this._osc.DiskretData.Count; j++)
                    {
                        if (this._osc.DiskretData[j][i])
                        {
                            writer.Write(",1");
                        }
                        else
                        {
                            writer.Write(",0");
                        }
                    }
                    writer.Write("\r\n");
                }
            }
            catch
            {
                MessageBox.Show("Файл " + this._WriteDATFile + " не сохранен!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                flag = false;
            }
            finally
            {
                writer.Close();
            }
            return flag;
        }

        public bool SaveHDR()
        {
            bool flag = true;
            StreamWriter writer = new StreamWriter(this._WriteHDRFile);
            try
            {
                writer.Write("Время срабатывания : " + this._osc.Time + "\r\n");
                writer.Write("Размер осциллограммы = " + this._osc.OscilloscopeLengs + "\r\n");
                writer.Write("Kтт = \r\n");
                writer.Write("Kттнп = \r\n");
                writer.Write("Kтн = \r\n");
                writer.Write("Kтннп = \r\n");
                for (int i = 0; i < this._avaryBeginOnOscList[this._currentOscilloscope].Count; i++)
                {
                    if (i != (this._avaryBeginOnOscList[this._currentOscilloscope].Count - 1))
                    {
                        int num2 = this._avaryBeginOnOscList[this._currentOscilloscope][i];
                        writer.Write(num2.ToString() + ",");
                    }
                    else
                    {
                        writer.Write(this._avaryBeginOnOscList[this._currentOscilloscope][i].ToString() + "\r\n");
                    }
                }
                writer.Write("BEMN\r\n");
                writer.Write(_osc.OtschetLengs.ToString() + "\r\n");
            }
            catch
            {
                MessageBox.Show("Файл " + this._WriteHDRFile + " не сохранен!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                flag = false;
            }
            finally
            {
                writer.Close();
            }
            return flag;
        }

        public void ShovLoadWindow()
        {
            IDeviceView view = _device;
            this._openOscilloscopeDlg.FileName = view.NodeName + "_Осциллограмма";
            this._openOscilloscopeDlg.Filter = "(*.hdr) | *.hdr";
            this._openOscilloscopeDlg.Title = "Открыть осциллограмму " + view.NodeName;
            if (DialogResult.OK == this._openOscilloscopeDlg.ShowDialog())
            {
                this.PrepareReadComTradeFiles(this._openOscilloscopeDlg.FileName);
            }
        }

        public void ShovSaveWindow()
        {
            IDeviceView view = _device;
            this._saveOscilloscopeDlg.FileName = view.NodeName + "_Осциллограмма в." + _device.DeviceVersion;
            this._saveOscilloscopeDlg.Title = "Сохранить осциллограмму " + view.NodeName;
            if (DialogResult.OK == this._saveOscilloscopeDlg.ShowDialog())
            {
                this.PrepareWriteComTradeFiles(this._saveOscilloscopeDlg.FileName, view.NodeName + "_Осциллограмма");
            }
        }

        public void ShowOscButtonEnaibled()
        {
            this._oscShowButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
        }

        #region INodeView
        public INodeView[] ChildNodes
        {
            get
            {
                return new INodeView[0];
            }
        }

        public Type ClassType
        {
            get
            {
                return typeof(VOscilloscopeForm);
            }
        }

        public bool Deletable
        {
            get
            {
                return false;
            }
        }

        public bool ForceShow
        {
            get
            {
                return false;
            }
        }

        public Type FormDevice
        {
            get
            {
                return typeof(TZL);
            }
        }

        public bool Multishow { get; private set; }

        public Image NodeImage
        {
            get
            {
                return AssemblyResources.Resources.oscilloscope.ToBitmap();
            }
        }

        public string NodeName
        {
            get
            {
                return "Осциллограмма";
            }
        }
        #endregion

        public class StartEndOscilloscope
        {
            #region Поля
            private int _after;
            private int _len;
            private int _begin;
            private int _point;
            private int _rez;
            private string _avaryTime;
            private int _currentPage;
            private List<BitArray> _diskretData;
            private int _endByteOnEndPage;
            private int _endPage;
            private int _firstByteIndex;
            private int _index;
            private int _lastByteIndex;
            private int _oscilloscopeLengs;
            private bool _oscilloscopeLoaded;
            private int _otschetLengs;
            private bool _pageLoaded;
            private int _pagesCount;
            private string _S1PageIaName;
            private double[] _S1PageIaValues;
            private string _S1PageIbName;
            private double[] _S1PageIbValues;
            private string _S1PageIcName;
            private double[] _S1PageIcValues;
            private string _S1PageInName;
            private double[] _S1PageInValues;
            private int _sourceIndex;
            private int _sourcePageIndex;
            private int _startByteOnStartPage;
            private int _startPage;
            private string _time;
            private string _UaName;
            private double[] _UaValues;
            private string _UbName;
            private double[] _UbValues;
            private string _UcName;
            private double[] _UcValues;
            private string _UnName;
            private double[] _UnValues;
            private ushort[] _values;
            private double[] koeffs;
            private int _fullOscSize;
            private int _oscPageSize;
            #endregion

            public StartEndOscilloscope(List<string[]> _mas, ushort oscRez, double version, int fullOscSize, int oscPageSize)
            {
                this._fullOscSize = fullOscSize;
                this._oscPageSize = oscPageSize;
                this.koeffs = new double[8];
                this._time = "";
                this._avaryTime = "";
                this._currentPage = 0;
                this._index = 0;
                this._startPage = 0;
                this._startByteOnStartPage = 0;
                this._endPage = 0;
                this._endByteOnEndPage = 0;
                this._lastByteIndex = 0;
                this._firstByteIndex = 0;
                this._pagesCount = 0;
                this._pageLoaded = false;
                this._oscilloscopeLoaded = false;
                this._sourceIndex = 0;
                this._sourcePageIndex = 0;
                this._otschetLengs = oscRez;
                this._oscilloscopeLengs = 0;
                this._S1PageIaName = "S1Ia";
                this._S1PageIbName = "S1Ib";
                this._S1PageIcName = "S1Ic";
                this._S1PageInName = "S1In";
                this._UaName = "Ua";
                this._UbName = "Ub";
                this._UcName = "Uc";
                this._UnName = "Un";
                this._values = new ushort[_mas.Count];
                this._firstByteIndex = 0;
                this._lastByteIndex = _mas.Count;
                this._otschetLengs = 1;
                this.InitArrays(version);
                for (int i = 0; i < _mas.Count; i++)
                {
                    this.CalculateOscParamsFromFile(i, _mas, version);
                }
            }

            public StartEndOscilloscope(List<int[]> oscStartEndList, List<string[]> oscTimeList, int index, TZL device)
            {
                this._fullOscSize = device.VFullOscSize;
                this._oscPageSize = device.VOscPageSize;
                this.koeffs = new double[8];
                this._time = "";
                this._avaryTime = "";
                this._currentPage = 0;
                this._index = 0;
                this._startPage = 0;
                this._startByteOnStartPage = 0;
                this._endPage = 0;
                this._endByteOnEndPage = 0;
                this._lastByteIndex = 0;
                this._firstByteIndex = 0;
                this._pagesCount = 0;
                this._pageLoaded = false;
                this._oscilloscopeLoaded = false;
                this._sourceIndex = 0;
                this._sourcePageIndex = 0;
                this._otschetLengs = device.OscRez;
                this._oscilloscopeLengs = 0;
                this._S1PageIaName = "S1Ia";
                this._S1PageIbName = "S1Ib";
                this._S1PageIcName = "S1Ic";
                this._S1PageInName = "S1In";
                this._UaName = "Ua";
                this._UbName = "Ub";
                this._UcName = "Uc";
                this._UnName = "Un";
                this._point = oscStartEndList[index][2];
                this._begin = oscStartEndList[index][3];
                this._len = oscStartEndList[index][4];
                this._after = oscStartEndList[index][5];
                this._rez = oscStartEndList[index][6];
                this._time = this.ConvertOscTimeToAvaryTime(oscTimeList[index][0], oscTimeList[index][1]);
                this._avaryTime = this.ConvertOscTimeToComTradeTime(Time);
                this._startPage = this.CalculateStartPage(oscStartEndList[index][0], out this._startByteOnStartPage);
                this._endPage = this.CalculateEndPage(oscStartEndList[index][1], out this._endByteOnEndPage);
                this._currentPage = this._startPage;
                if (this._endPage > this._startPage)
                {
                    this._pagesCount = (this._endPage - this._startPage) + 1;
                }
                else
                {
                    this._pagesCount = (this._endPage + (((device.VFullOscSize / device.VOscPageSize) / 2) - this._startPage)) + 1;
                }
                this._values = new ushort[this._pagesCount * device.VOscPageSize];
                this._firstByteIndex = this._startByteOnStartPage;
                this._lastByteIndex = this._values.Length - (device.VOscPageSize - this._endByteOnEndPage);
            }
            
            public void ArrayToValues(TZL device)
            {
                int num = 4;
                if (this._sourceIndex > 258800) 
                {
                    int f123 = 0;
                }
                try
                {
                    if ((this._sourcePageIndex + device.VOscilloscope.Length) >= 0x400)
                    {
                        if (this._currentPage < device.VFullOscSize / device.VOscPageSize / 2 - 1)
                        {
                            this._pageLoaded = true;
                            Array.ConstrainedCopy(device.VOscilloscope, 0, this._values, this._sourceIndex, device.VOscilloscope.Length);
                            this._sourceIndex += device.VOscilloscope.Length;
                        }
                        else
                        {
                            if (Common.VersionConverter(device.DeviceVersion) > 2.0)
                            {
                                num = 0;
                            }
                            this._pageLoaded = true;
                            Array.ConstrainedCopy(device.VOscilloscope, 0, this._values, this._sourceIndex, device.VOscilloscope.Length - num);
                            this._sourceIndex += device.VOscilloscope.Length - num;
                            if (this._endPage != this._currentPage)
                            {
                                this._lastByteIndex -= num;
                            }
                        }
                        if ((this._currentPage >= this._endPage) && (device.VOscSize / device.VOscPageSize - this._currentPage >= device.VOscSize / device.VOscPageSize - this._endPage))
                        {
                            this._oscilloscopeLoaded = true;
                            this._pageLoaded = false;
                        }
                        
                    }
                    else
                    {
                        Array.ConstrainedCopy(device.VOscilloscope, 0, this._values, this._sourceIndex, device.VOscilloscope.Length);
                        this._sourceIndex += device.VOscilloscope.Length;
                        this._sourcePageIndex += device.VOscilloscope.Length;
                    }
                    if (this._sourceIndex == this._values.Length) 
                    {
                        this._oscilloscopeLoaded = true;
                        this._pageLoaded = false;
                    }
                }
                catch
                {
                    int g = 5;
                }
            }

            private int CalculateEndPage(int End, out int EndByteOnEndPage)
            {
                int num = 0;
                num = End /this._oscPageSize;
                EndByteOnEndPage = End %this._oscPageSize;
                return num;
            }

            public void CalculateOscKoeffs()
            {
                try
                {
                    this.koeffs[0] = (4000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[1] = (4000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[2] = (4000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[3] = (4000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[4] = (4000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[5] = (4000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[6] = (400000.0 * Math.Sqrt(2.0)) / 32768.0;
                    this.koeffs[7] = (400000.0 * Math.Sqrt(2.0)) / 32768.0;
                }
                catch
                {
                }
            }

            public void CalculateOscParams(TZL device, int OtschetNum)
            {
                try
                {
                    this._S1PageIaValues[OtschetNum / this._otschetLengs] = (this._values[OtschetNum] - 32768.0) * device.TT * 40.0 * Math.Sqrt(2.0) / 32768.0;
                    this._S1PageIbValues[OtschetNum / this._otschetLengs] = (this._values[1 + OtschetNum] - 32768.0) * device.TT * 40.0 * Math.Sqrt(2.0) / 32768.0;
                    this._S1PageIcValues[OtschetNum / this._otschetLengs] = (this._values[2 + OtschetNum] - 32768.0) * device.TT * 40.0 * Math.Sqrt(2.0) / 32768.0;
                    this._S1PageInValues[OtschetNum / this._otschetLengs] = (this._values[3 + OtschetNum] - 32768.0) * device.TTNP * 5.0 * Math.Sqrt(2.0) / 32768.0;
                    this._UaValues[OtschetNum / this._otschetLengs] = (this._values[4 + OtschetNum] - 32768.0) * device.TN * 256.0 * Math.Sqrt(2.0) / 32768.0;
                    
                    this._UbValues[OtschetNum / this._otschetLengs] = (this._values[5 + OtschetNum] - 32768.0) * device.TN * 256.0 * Math.Sqrt(2.0) / 32768.0;
                    this._UcValues[OtschetNum / this._otschetLengs] = (this._values[6 + OtschetNum] - 32768.0) * device.TN * 256.0 * Math.Sqrt(2.0) / 32768.0;
                    this._UnValues[OtschetNum / this._otschetLengs] = (this._values[7 + OtschetNum] - 32768.0) * device.TNNP * 256.0 * Math.Sqrt(2.0) / 32768.0;
                    if (Common.VersionConverter(device.DeviceVersion) > 2.0)
                    {
                        int index = 0;
                        for (int j = 8; j < 16; j++)
                        {
                            index = j % 8 * 16;
                            byte num9 = Common.LOBYTE(this._values[j + OtschetNum]);
                            for (int i = 0; i < 8; i++)
                            {
                                this._diskretData[i + index][OtschetNum / this._otschetLengs] = ((num9 >> i) & 1) != 0;
                            }
                            byte num11 = Common.HIBYTE(this._values[j + OtschetNum]);
                            for (int i = 0; i < 8; i++)
                            {
                                this._diskretData[i + 8 + index][OtschetNum / this._otschetLengs] = ((num11 >> i) & 1) != 0;
                            }
                        }
                    }
                    else
                    {
                        byte num9 = Common.LOBYTE(this._values[8 + OtschetNum]);
                        for (int i = 0; i < 8; i++)
                        {
                            this._diskretData[i][OtschetNum / this._otschetLengs] = ((num9 >> i) & 1) != 0;
                        }
                        byte num11 = Common.HIBYTE(this._values[8 + OtschetNum]);
                        for (int i = 0; i < 8; i++)
                        {
                            this._diskretData[i + 8][OtschetNum / this._otschetLengs] = ((num11 >> i) & 1) != 0;
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            public string PrepareString(string val) 
            {
                string[] res = val.Split('.', ',');
                return res[0] + "," + res[1];
            }

            public void CalculateOscParamsFromFile(int OtschetNum, List<string[]> Values, double vers)
            {
                try
                {
                    _S1PageIaValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][0]));
                    _S1PageIbValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][1]));
                    _S1PageIcValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][2]));
                    _S1PageInValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][3]));
                    _UaValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][4]));
                    _UbValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][5]));
                    _UcValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][6]));
                    _UnValues[OtschetNum] = Convert.ToDouble(PrepareString(Values[OtschetNum][7]));

                    if (vers >= 2.0)
                    {
                        for (int i = 0; i < 128; i++)
                        {
                            this._diskretData[i][OtschetNum] = "0" != Values[OtschetNum][8 + i];
                        }
                    }
                    else
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            this._diskretData[i][OtschetNum] = "0" != Values[OtschetNum][8 + i];
                        }
                    }
                }
                catch (Exception)
                {
                    int f = 0;
                }
            }

            private int CalculateStartPage(int Start, out int StartByteOnStartPage)
            {
                int num = 0;
                num = Start / this._oscPageSize;
                StartByteOnStartPage = Start % this._oscPageSize;
                return num;
            }

            public string ConvertOscTimeToAvaryTime(string Time, string AvaryTime)
            {
                try
                {
                    string str = Time;
                    try
                    {
                        str = Time.Split(new char[] {' '})[0] + Time.Split(new char[] {' '})[1];
                    }
                    catch
                    {
                    }
                    string[] strArray = str.Split(new char[] {'-', ',', ':', '.'});
                    int[] numArray = new int[strArray.Length];
                    int num = Convert.ToInt32(AvaryTime);
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        numArray[i] = Convert.ToInt32(strArray[i]);
                    }

                    DateTime a = new DateTime(numArray[0], numArray[1], numArray[2], numArray[3], numArray[4],
                        numArray[5], numArray[6]);
                    DateTime res = a.Subtract(new TimeSpan(0, 0, 0, 0, num/100));

                    return (this.ConvertToComTradeTimeValue(res.Day.ToString()) + "/" +
                            this.ConvertToComTradeTimeValue(res.Month.ToString()) + "/" +
                            this.ConvertToComTradeTimeValue(res.Year.ToString()) + "," +
                            this.ConvertToComTradeTimeValue(res.Hour.ToString()) + ":" +
                            this.ConvertToComTradeTimeValue(res.Minute.ToString()) + ":" +
                            this.ConvertToComTradeTimeValue(res.Second.ToString()) + "." +
                            this.ConvertToComTradeTimeValue(res.Millisecond.ToString()));
                }
                catch
                {
                    return "ERROR!";
                }
            }

            public string ConvertOscTimeToComTradeTime(string Time)
            {
                try
                {
                    string str = Time;
                    try
                    {
                        str = Time.Split(new char[] { ' ' })[0] + Time.Split(new char[] { ' ' })[1];
                    }
                    catch
                    {
                    }
                    string[] strArray = str.Split(new char[] { '-', ',', ':', '.' });
                    return (strArray[0] + "/" + strArray[1] + "/" + strArray[2] + "," + strArray[3] + ":" + strArray[4] + ":" + strArray[5] + "." + strArray[6]);
                }
                catch
                {
                    return "ERROR!";
                }
            }

            public string ConvertToComTradeTimeValue(string val)
            {
                if (val.Length < 2)
                {
                    return ("0" + val);
                }
                return val;
            }

            public void InitArrays(double vers)
            {
                try
                {
                    this._oscilloscopeLengs = (this._lastByteIndex - this._firstByteIndex) / this._otschetLengs;
                    // значения осцилоги, прочитанные без переворота
                    ushort[] destinationArray = new ushort[this._lastByteIndex - this._firstByteIndex];
                    Array.ConstrainedCopy(this._values, this._firstByteIndex, destinationArray, 0, this._lastByteIndex - this._firstByteIndex);
                    this._values = destinationArray;
                    this._S1PageIaValues = new double[this._oscilloscopeLengs];
                    this._S1PageIbValues = new double[this._oscilloscopeLengs];
                    this._S1PageIcValues = new double[this._oscilloscopeLengs];
                    this._S1PageInValues = new double[this._oscilloscopeLengs];
                    this._UaValues = new double[this._oscilloscopeLengs];
                    this._UbValues = new double[this._oscilloscopeLengs];
                    this._UcValues = new double[this._oscilloscopeLengs];
                    this._UnValues = new double[this._oscilloscopeLengs];
                    this._diskretData = new List<BitArray>();
                    if (vers >= 2.0)
                    {
                        for (int i = 0; i < 128; i++)
                        {
                            this._diskretData.Add(new BitArray(this._oscilloscopeLengs));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            this._diskretData.Add(new BitArray(this._oscilloscopeLengs));
                        }
                    }
                }
                catch{}
            }

            public int After
            {
                get
                {
                    return this._after;
                }
            }
            public int Begin
            {
                get
                {
                    return this._begin;
                }
            }
            public int Len
            {
                get
                {
                    return this._len;
                }
            }
            public int Point
            {
                get
                {
                    return this._point;
                }
            }
            public int Rez
            {
                get
                {
                    return this._rez;
                }
            }

            public string AvaryTime
            {
                get
                {
                    return this._avaryTime;
                }
            }

            public int ByteOnEndPage
            {
                get
                {
                    return this._endByteOnEndPage;
                }
            }

            public int ByteOnStartPage
            {
                get
                {
                    return this._startByteOnStartPage;
                }
            }

            public int CurrentPage
            {
                get
                {
                    if (this._currentPage < this._fullOscSize / this._oscPageSize / 2 - 1)
                    {
                        this._currentPage++;
                    }
                    else
                    {
                        this._currentPage = 0;
                    }
                    return this._currentPage;
                }
            }

            public List<BitArray> DiskretData
            {
                get
                {
                    return this._diskretData;
                }
            }

            public int EndPage
            {
                get
                {
                    return this._endPage;
                }
            }

            public int FirstByteIndex
            {
                get
                {
                    return this._firstByteIndex;
                }
            }

            public int Index
            {
                get
                {
                    return this._index;
                }
                set
                {
                    this._index = value;
                }
            }

            public double[] Koeffs
            {
                get
                {
                    return this.koeffs;
                }
            }

            public int LastByteIndex
            {
                get
                {
                    return this._lastByteIndex;
                }
            }

            

            public int OscilloscopeLengs
            {
                get
                {
                    return this._oscilloscopeLengs;
                }
            }

            public bool OscilloscopeLoaded
            {
                get
                {
                    return this._oscilloscopeLoaded;
                }
                set
                {
                    this._oscilloscopeLoaded = false;
                }
            }

            public int OtschetLengs
            {
                get
                {
                    return this._otschetLengs;
                }
            }

            public bool PageLoaded
            {
                get
                {
                    return this._pageLoaded;
                }
                set
                {
                    this._sourcePageIndex = 0;
                    this._pageLoaded = value;
                }
            }

            public int PagesCount
            {
                get
                {
                    return this._pagesCount;
                }
            }



            public string S1PageIaName
            {
                get
                {
                    return this._S1PageIaName;
                }
            }

            public double[] S1PageIaValues
            {
                get
                {
                    return this._S1PageIaValues;
                }
            }

            public string S1PageIbName
            {
                get
                {
                    return this._S1PageIbName;
                }
            }

            public double[] S1PageIbValues
            {
                get
                {
                    return this._S1PageIbValues;
                }
            }

            public string S1PageIcName
            {
                get
                {
                    return this._S1PageIcName;
                }
            }

            public double[] S1PageIcValues
            {
                get
                {
                    return this._S1PageIcValues;
                }
            }

            public string S1PageInName
            {
                get
                {
                    return this._S1PageInName;
                }
            }

            public double[] S1PageInValues
            {
                get
                {
                    return this._S1PageInValues;
                }
            }

            public int StartPage
            {
                get
                {
                    return this._startPage;
                }
            }

            public string Time
            {
                get
                {
                    return this._time;
                }
            }

            public string UaName
            {
                get
                {
                    return this._UaName;
                }
            }

            public double[] UaValues
            {
                get
                {
                    return this._UaValues;
                }
            }

            public string UbName
            {
                get
                {
                    return this._UbName;
                }
            }

            public double[] UbValues
            {
                get
                {
                    return this._UbValues;
                }
            }

            public string UcName
            {
                get
                {
                    return this._UcName;
                }
            }

            public double[] UcValues
            {
                get
                {
                    return this._UcValues;
                }
            }

            public string UnName
            {
                get
                {
                    return this._UnName;
                }
            }

            public double[] UnValues
            {
                get
                {
                    return this._UnValues;
                }
            }

            public ushort[] Values
            {
                get
                {
                    return this._values;
                }
            }
        }

        private void _showAllInfo_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this._showAllInfo.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this._showAllInfo.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this._showAllInfo.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this._showAllInfo.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this._showAllInfo.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this._showAllInfo.Checked;
        }
    }
}

