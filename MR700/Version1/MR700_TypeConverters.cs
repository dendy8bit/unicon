using System.ComponentModel;

namespace BEMN.MR700.Version1
{
    public class AllSignalsTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.All);
        }
    }
    public class BlinkerTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(new string[] { "�����������", "�������" });
        }
    }
    public class ModeTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Modes);
        }
    }
    public class ModeLightTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ModesLight);
        }
    }
    public class ExternalDefensesTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ExternalDefense);
        }
    }
    public class BlockingTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Blocking);
        }
    }
    public class LogicTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Logic);
        }
    }
    public class TN_TypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.TN_Type);
        }
    }
    public class ForbiddenTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Forbidden);
        }
    }
    public class ControlTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Control);
        }
    }
    public class BusDirectionTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.BusDirection);
        }
    }
    public class FeatureTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Feature);
        }
    }
    public class YesNoTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.YesNo);
        }
    }

}