using System;
using System.Collections;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR700.Version1
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private bool mess = false;
        private MR700 _device;
        private bool _validatingOk = true;
        private bool _connectingErrors = false;
        private double _devVersion = 1.17;

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(MR700 device)
        {
            this._device = device;
            this.InitializeComponent();
            this._groupSelector = new RadioButtonSelector(this._mainRadioButtonDefend, this._reserveRadioButtonDefend,
                this._changeRadioButtonGroup);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            this._devVersion = Common.VersionConverter(this._device.DeviceVersion);
            if (this._devVersion > 1.11)
            {
                this.groupBox18.Visible = true;
                this.label27.Visible = true;
                this.label26.Visible = true;
                this._OMP_TypeBox.Visible = true;
                this._HUD_Box.Visible = true;
                this.labelKD.Visible = true;
                this.labelIO.Visible = true;
                this._kontrDiskrCB.Visible = true;
                this._kontrIOCB.Visible = true;
                this.lzsh_conf.Visible = true;
                if (this._devVersion >= 1.16)
                {
                    this._TT_Box.Tag = "3000";
                }
                if (this._devVersion >= 1.17)
                {
                    this._TT_Box.Tag = "5000";
                    this._TTNP_Box.Tag = "1000";
                    this.lzsh_conf.Visible = false;
                    this.lzsh_conf1_17.Visible = true;
                    this._exDefResetcol.Visible = true;
                    this._voltageDefensesResetCol1.Visible = true;
                    this._voltageDefensesResetCol2.Visible = true;
                    this._voltageDefensesResetCol3.Visible = true;
                    this._frequenceDefensesReset.Visible = true;
                }
            }
            else
            {
                this._TN_Box.Tag = 256;
            }

            this._exchangeProgressBar.Value = 0;
            this._device.InputSignalsLoadOK += new Handler(this._device_InputSignalsLoadOK);
            this._device.InputSignalsLoadFail += new Handler(this._device_InputSignalsLoadFail);
            this._device.KonfCountLoadOK += new Handler(this._device_KonfCountLoadOK);
            this._device.KonfCountLoadFail += new Handler(this._device_KonfCountLoadFail);
            this._device.OutputSignalsLoadOK += new Handler(this._device_OutputSignalsLoadOK);
            this._device.OutputSignalsLoadFail += new Handler(this._device_OutputSignalsLoadFail);
            this._device.ExternalDefensesLoadOK += new Handler(this._device_ExternalDefensesLoadOK);
            this._device.ExternalDefensesLoadFail += new Handler(this._device_ExternalDefensesLoadFail);
            this._device.AutomaticsPageLoadOK += new Handler(this._device_AutomaticsPageLoadOK);
            this._device.AutomaticsPageLoadFail += new Handler(this._device_AutomaticsPageLoadFail);
            this._device.TokDefensesLoadOK += new Handler(this._device_TokDefensesMainLoadOK);
            this._device.TokDefensesLoadFail += new Handler(this._device_TokDefensesMainLoadFail);
            this._device.VoltageDefensesLoadOk += new Handler(this._device_VoltageDefensesLoadOk);
            this._device.VoltageDefensesLoadFail += new Handler(this._device_VoltageDefensesLoadFail);
            this._device.FrequenceDefensesLoadOk += new Handler(this._device_FrequenceDefensesLoadOk);
            this._device.FrequenceDefensesLoadFail += new Handler(this._device_FrequenceDefensesLoadFail);

            this.SubscriptOnSaveHandlers();

            this.PrepareInputSignals();
            this.PrepareOutputSignals();
            this.PrepareExternalDefenses();
            this.PrepareAutomaticsSignals();
            this.PrepareTokDefenses();
            this.PrepareVoltageDefenses();
            this.PrepareFrequenceDefenses();

            this._oscKonfComb.SelectedIndex = 0;
        }

        private void _groupSelector_NeedCopyGroup()
        {
            //�������� � ���������, ����� ��������
            if (this._groupSelector.SelectedGroup == 0)
            {
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
            else
            {
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
        }

        private void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                combo.Items.RemoveAt(combo.Items.Count - 1);
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (MR700); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof (ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this._device.Deserialize(this._openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    this._processLabel.Text = string.Format("���� " + System.IO.Path.GetFileName(exc.FileName) +
                                                            " �� �������� ������ ������� ��700 ��� ���������");
                    return;
                }
                if (this._mainRadioButtonDefend.Checked)
                {
                    this.ShowTokDefenses(this._device.TokDefensesMain);

                    this.ShowVoltageDefenses(this._device.VoltageDefensesMain);

                    this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
                }
                else
                {
                    this.ShowTokDefenses(this._device.TokDefensesReserve);
                    this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);
                    this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
                }
                this.ReadInputSignals();
                this.ReadExternalDefenses();
                this.ReadOutputSignals();
                this.ReadAutomaticsPage();
                this.ReadKonfCount();

                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
            }
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this._saveConfigBut.Focus(); // �������� ����� ������� ������, ����� ����������� �������� � datagridview
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._saveConfigurationDlg.FileName = string.Format("��700_�������_������ {0:F1}.bin",
                    this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._exchangeProgressBar.Value = 0;
                    this._device.Serialize(this._saveConfigurationDlg.FileName);
                    this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
                }
            }  
        }
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig(); 
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._writeConfigBut.Focus(); // �������� ����� ������� ������, ����� ����������� �������� � datagridview
            this._connectingErrors = false;
            this._exchangeProgressBar.Value = 0;
            this._validatingOk = true;
            this.ValidateAll();
            if (this._validatingOk)
            {
                if (DialogResult.Yes ==
                    MessageBox.Show("�������� ������������ ��700 �" + this._device.DeviceNumber + " ?", "��������",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "���� ������";
                    this._device.SaveOutputSignals();
                    this._device.SaveInputSignals();
                    this._device.SaveExternalDefenses();
                    this._device.SaveTokDefenses();
                    this._device.SaveVoltageDefenses();
                    this._device.SaveAutomaticsPage();
                    this._device.SaveFrequenceDefenses();
                    this._device.ConfirmConstraint();
                }
            }  
        }
        private void ValidateAll()
        {
            this._validatingOk = true;
            //�������� MaskedTextBox
            this.ValidateInputSignals();
            this.ValidateTokDefenses();
            this.ValidateAutomatics();
            //�������� DataGrid
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteOutputSignals()
                                && this.WriteExternalDefenses()
                                && this.WriteInputSignals()
                                && this.WriteAutomaticsPage();

                this._validatingOk &= this._mainRadioButtonDefend.Checked
                    ? this.WriteTokDefenses(this._device.TokDefensesMain)
                    : this.WriteTokDefenses(this._device.TokDefensesReserve);
                this._validatingOk &= this._mainRadioButtonDefend.Checked
                    ? this.WriteVoltageDefenses(this._device.VoltageDefensesMain)
                    : this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this._validatingOk &= this._mainRadioButtonDefend.Checked
                    ? this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain)
                    : this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }

        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������...";
            this._connectingErrors = false;
            this._device.LoadInputSignals();
            this._device.LoadOutputSignals();
            this._device.LoadExternalDefenses();
            this._device.LoadAutomaticsPage();
            this._device.LoadTokDefenses();
            this._device.LoadVoltageDefenses();
            this._device.LoadFrequenceDefenses();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��700. ������",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��700. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveOk()
        {
            this._exchangeProgressBar.PerformStep();
        }

        void OnLoadComplete()
        {
            this._processLabel.Text = this._connectingErrors ? "������ ��������� ���������" : "������ ������� ���������";
        }

        void OnSaveComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "������ ������� ���������";
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.StartRead();
        }

        private void SubscriptOnSaveHandlers()
        {
            this._device.OutputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.OutputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.InputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.InputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            this._device.KonfCountSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.KonfCountSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            this._device.ExternalDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.ExternalDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.AutomaticsPageSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.AutomaticsPageSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TokDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.TokDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.VoltageDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.VoltageDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            this._device.FrequenceDefensesSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.FrequenceDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
        }

        void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput && box.ValidatingType == typeof(ulong))
                {
                    this.ShowToolTip(box);
                    return;
                }

                if (!e.IsValidInput && box.ValidatingType == typeof(double))
                {
                    this.ShowToolTipDouble(box);
                    return;
                }

                if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                {
                    this.ShowToolTip(box);
                    return;
                }

                if (box.ValidatingType == typeof(double) &&
                    (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                {
                    this.ShowToolTipDouble(box);
                }

            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            if (this._validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    this._tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                this._toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                this._validatingOk = false;
            }
        }

        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage outerPage)
        {
            if (!this._validatingOk) return;
            this._tabControl.SelectedTab = outerPage;
            Point cPoint = cell.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Location;
            Point gPoint = cell.DataGridView.Location;
            this._toolTip.Show(msg, outerPage, cPoint.X + gPoint.X, cPoint.Y + gPoint.Y + cell.OwningRow.Height, 2000);
            cell.Selected = true;
            this._validatingOk = false;
        }

        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage outerPage, TabPage innerPage)
        {
            if (!this._validatingOk) return;
            this._tabControl.SelectedTab = outerPage;
            this._newTabControlDefending.SelectedTab = innerPage;
            Point cPoint = cell.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Location;
            Point gPoint = cell.DataGridView.Location;
            this._toolTip.Show(msg, innerPage, cPoint.X + gPoint.X, cPoint.Y + gPoint.Y + cell.OwningRow.Height, 2000);
            cell.Selected = true;
            this._validatingOk = false;
        }

        private void ShowToolTipDouble(MaskedTextBox box)
        {
            if (this._validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    this._tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                this._toolTip.Show("������� ����� � ��������� [0.0-" + box.Tag + ".0]", box, 2000);
                box.Focus();
                box.SelectAll();
                this._validatingOk = false;
            }
        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(this.Combo_DropDown);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }

        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                combos[i].SelectedIndex = 0;
            }
        }

        void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            foreach (MaskedTextBox box in boxes)
            {
                box.ValidateText();
            }
        }

        #region �������� �������
        void _device_OutputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_OutputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadOutputSignals));
            }
            catch (Exception)
            { }
        }

        private void PrepareOutputSignals()
        {
            this._releSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outIndSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outputLogicCheckList.Items.AddRange(Strings.OutputSignals.ToArray());
            this._outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < MR700.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, "�����������", "���", 0 });
            }
            this._outputIndicatorsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputReleGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR700.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "�����������",
                    "���",
                    false,
                    false,
                    false
                });
            }
        }

        private void ReadOutputSignals()
        {
            this._exchangeProgressBar.PerformStep();
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR700.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, this._device.OutputRele[i].Type, this._device.OutputRele[i].SignalString, this._device.OutputRele[i].ImpulseUlong });
            }
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR700.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            this._device.OutputIndicator[i].Type,
                                                            this._device.OutputIndicator[i].SignalString,
                                                            this._device.OutputIndicator[i].Indication,
                                                            this._device.OutputIndicator[i].Alarm,
                                                            this._device.OutputIndicator[i].System});
            }
            this._outputLogicCombo.SelectedIndex = 0;
            this.ShowOutputLogicSignals(0);
        }

        private void ShowOutputLogicSignals(int channel)
        {
            BitArray bits = this._device.GetOutputLogicSignals(channel);
            for (int i = 0; i < bits.Count; i++)
            {
                this._outputLogicCheckList.SetItemChecked(i, bits[i]);
            }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;
            if (this._device.OutputRele.Count == MR700.COutputRele.COUNT && this._outputReleGrid.Rows.Count <= MR700.COutputRele.COUNT)
            {
                for (int i = 0; i < this._outputReleGrid.Rows.Count; i++)
                {
                    this._device.OutputRele[i].Type = this._outputReleGrid["_releTypeCol", i].Value.ToString();
                    this._device.OutputRele[i].SignalString = this._outputReleGrid["_releSignalCol", i].Value.ToString();
                    try
                    {
                        ulong value = ulong.Parse(this._outputReleGrid["_releImpulseCol", i].Value.ToString());
                        if (value > MR700.TIMELIMIT)
                        {
                            this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                            ret = false;
                        }
                        else
                        {
                            this._device.OutputRele[i].ImpulseUlong = value;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                        ret = false;

                    }
                }
            }

            if (this._device.OutputIndicator.Count == MR700.COutputIndicator.COUNT && this._outputIndicatorsGrid.Rows.Count <= MR700.COutputIndicator.COUNT)
            {
                for (int i = 0; i < this._outputIndicatorsGrid.Rows.Count; i++)
                {
                    this._device.OutputIndicator[i].Type = this._outputIndicatorsGrid["_outIndTypeCol", i].Value.ToString();
                    this._device.OutputIndicator[i].SignalString = this._outputIndicatorsGrid["_outIndSignalCol", i].Value.ToString();
                    this._device.OutputIndicator[i].Indication = (bool)this._outputIndicatorsGrid["_outIndResetCol", i].Value;
                    this._device.OutputIndicator[i].Alarm = (bool)this._outputIndicatorsGrid["_outIndAlarmCol", i].Value;
                    this._device.OutputIndicator[i].System = (bool)this._outputIndicatorsGrid["_outIndSystemCol", i].Value;
                }

            }
            return ret;
        }

        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowOutputLogicSignals(this._outputLogicCombo.SelectedIndex);
        }

        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            BitArray bits = new BitArray(104);
            for (int i = 0; i < this._outputLogicCheckList.Items.Count; i++)
            {
                bits[i] = this._outputLogicCheckList.GetItemChecked(i);
            }
            this._device.SetOutputLogicSignals(this._outputLogicCombo.SelectedIndex, bits);
        }
        #endregion

        #region ������� ������

        void _device_ExternalDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_ExternalDefensesLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadExternalDefenses));
            }
            catch (InvalidOperationException)
            { }
        }

        private void PrepareExternalDefenses()
        {
            this._externalDefenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._exDefBlockingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            this._exDefModeCol.Items.AddRange(Strings.ModesExternal.ToArray());
            this._exDefReturnNumberCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            this._exDefWorkingCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            for (int i = 0; i < MR700.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           "��������",
                                                           "���",
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           "���",
                                                           0,
                                                           false,
                                                           false,
                                                           false,
                                                            false});
                this.OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });

            }
            this._externalDefenseGrid.Height = this._externalDefenseGrid.ColumnHeadersHeight + this._externalDefenseGrid.Rows.Count * this._externalDefenseGrid.RowTemplate.Height + 10;
            this._externalDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._externalDefenseGrid_CellStateChanged);
        }

        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[this._externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(this._externalDefenseGrid, row, "��������", 1, columns);
        }

        void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (this._exDefModeCol == e.Cell.OwningColumn || this._exDefReturnCol == e.Cell.OwningColumn)
            {
                if (!this.OnExternalDefenseGridModeChanged(e.Cell.RowIndex))
                {
                    GridManager.ChangeCellDisabling(this._externalDefenseGrid, e.Cell.RowIndex, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }

        private void ReadExternalDefenses()
        {
            this._exchangeProgressBar.PerformStep();
            this._externalDefenseGrid.Rows.Clear();
            for (int i = 0; i < MR700.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]{i + 1,
                                                           this._device.ExternalDefenses[i].Mode,
                                                           this._device.ExternalDefenses[i].BlockingNumber,
                                                           this._device.ExternalDefenses[i].WorkingNumber,
                                                           this._device.ExternalDefenses[i].WorkingTime,
                                                           this._device.ExternalDefenses[i].Return,
                                                           this._device.ExternalDefenses[i].APV_Return,
                                                           this._device.ExternalDefenses[i].ReturnNumber,
                                                           this._device.ExternalDefenses[i].ReturnTime,
                                                           this._device.ExternalDefenses[i].UROV,
                                                           this._device.ExternalDefenses[i].APV,
                                                           this._device.ExternalDefenses[i].AVR,
                                                           this._device.ExternalDefenses[i].Reset });
                if (!this.OnExternalDefenseGridModeChanged(i))
                {
                    GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }



        private bool WriteExternalDefenses()
        {
            bool ret = true;
            if (this._device.ExternalDefenses.Count == MR700.CExternalDefenses.COUNT && this._externalDefenseGrid.Rows.Count <= MR700.CExternalDefenses.COUNT)
            {
                for (int i = 0; i < this._externalDefenseGrid.Rows.Count; i++)
                {
                    this._device.ExternalDefenses[i].Mode = this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].BlockingNumber = this._externalDefenseGrid["_exDefBlockingCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].WorkingNumber = this._externalDefenseGrid["_exDefWorkingCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].Return = (bool)this._externalDefenseGrid["_exDefReturnCol", i].Value;
                    this._device.ExternalDefenses[i].APV_Return = (bool)this._externalDefenseGrid["_exDefAPVreturnCol", i].Value;
                    this._device.ExternalDefenses[i].ReturnNumber = this._externalDefenseGrid["_exDefReturnNumberCol", i].Value.ToString();
                    this._device.ExternalDefenses[i].UROV = (bool)this._externalDefenseGrid["_exDefUROVcol", i].Value;
                    this._device.ExternalDefenses[i].APV = (bool)this._externalDefenseGrid["_exDefAPVcol", i].Value;
                    this._device.ExternalDefenses[i].AVR = (bool)this._externalDefenseGrid["_exDefAVRcol", i].Value;
                    this._device.ExternalDefenses[i].Reset = (bool)this._externalDefenseGrid["_exDefResetcol", i].Value;
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefReturnTimeCol", i].Value.ToString());
                        if (value > MR700.TIMELIMIT)
                        {
                            throw new OverflowException(MR700.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].ReturnTime = value;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefReturnTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        if (value > MR700.TIMELIMIT)
                        {
                            throw new OverflowException(MR700.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        }
                        this._device.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefWorkingTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }

                }

            }
            return ret;
        }

        #endregion

        #region ������� �������

        private void ReadInputSignals()
        {
            this._exchangeProgressBar.PerformStep();
            this._TT_Box.Text = this._device.TT.ToString();
            this._TTNP_Box.Text = this._device.TTNP.ToString();
            this._TN_Box.Text = this._device.TN.ToString();
            this._TNNP_Box.Text = this._device.TNNP.ToString();
            this._maxTok_Box.Text = this._device.MaxTok.ToString();

            this._switcherDurationBox.Text = this._device.SwitcherDuration.ToString();
            this._switcherImpulseBox.Text = this._device.SwitcherImpulse.ToString();
            this._switcherTimeBox.Text = this._device.SwitcherTimeUROV.ToString();
            this._switcherTokBox.Text = this._device.SwitcherTokUROV.ToString();

            this._extOffCombo.SelectedItem = this._device.ExternalOff;
            this._extOnCombo.SelectedItem = this._device.ExternalOn;
            this._constraintGroupCombo.SelectedItem = this._device.ConstraintGroup;
            this._keyOffCombo.SelectedItem = this._device.KeyOff;
            this._keyOnCombo.SelectedItem = this._device.KeyOn;
            this._signalizationCombo.SelectedItem = this._device.SignalizationReset;
            this._switcherBlockCombo.SelectedItem = this._device.SwitcherBlock;
            this._switcherErrorCombo.SelectedItem = this._device.SwitcherError;
            this._switcherStateOffCombo.SelectedItem = this._device.SwitcherOff;
            this._switcherStateOnCombo.SelectedItem = this._device.SwitcherOn;
            this._kontrDiskrCB.SelectedItem = this._device.KontrDiskr;
            this._kontrIOCB.SelectedItem = this._device.KontrIO;

            this._manageSignalsSDTU_Combo.SelectedItem = this._device.ManageSignalSDTU;
            this._manageSignalsKeyCombo.SelectedItem = this._device.ManageSignalKey;
            this._manageSignalsExternalCombo.SelectedItem = this._device.ManageSignalExternal;
            this._manageSignalsButtonCombo.SelectedItem = this._device.ManageSignalButton;

            this._TNNP_dispepairCombo.SelectedItem = this._device.TNNP_Dispepair;
            this._TN_dispepairCombo.SelectedItem = this._device.TN_Dispepair;
            this._TN_typeCombo.SelectedItem = this._device.TN_Type;
            this._OMP_TypeBox.SelectedItem = this._device.OMP_Type;
            this._HUD_Box.Text = (this._device.HUD / 1000).ToString();
            this._releDispepairBox.Text = this._device.DispepairImpulse.ToString();

            for (int i = 0; i < this._device.DispepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.DispepairSignal[i]);
            }

            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ReadKonfCount()
        {
            if ((int)this._device.OscilloscopeKonfCount == 65535)
            {
                this._oscKonfComb.Text = "XXXXX";
            }
            else 
            {
                this._oscKonfComb.SelectedIndex = (int)this._device.OscilloscopeKonfCount;
            }
            this._exchangeProgressBar.PerformStep();
        }

        void _device_InputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_InputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadInputSignals));
            }
            catch (Exception)
            { }
        }

        void _device_KonfCountLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_KonfCountLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadKonfCount));
            }
            catch (InvalidOperationException)
            { }
            catch (Exception mm) 
            {

            }
        }

        private ComboBox[] _inputSignalsCombos;
        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;
        private MaskedTextBox[] _inputSignalsUlongMaskBoxes;

        private void PrepareInputSignals()
        {
            this._inputSignalsCombos = new ComboBox[] {this._kontrDiskrCB,this._kontrIOCB, this._OMP_TypeBox, this._extOffCombo,this._extOnCombo,this._constraintGroupCombo,
                                                  this._keyOffCombo,this._keyOnCombo,this._signalizationCombo,
                                                  this._switcherBlockCombo,this._switcherErrorCombo,this._switcherStateOffCombo,
                                                  this._switcherStateOnCombo,this._TN_dispepairCombo,this._TN_typeCombo,this._TNNP_dispepairCombo,
                                                  this._manageSignalsButtonCombo,this._manageSignalsExternalCombo,this._manageSignalsSDTU_Combo,this._manageSignalsKeyCombo};
            this._inputSignalsDoubleMaskBoxes = new MaskedTextBox[] { this._HUD_Box, this._maxTok_Box, this._TN_Box, this._TNNP_Box, this._switcherTokBox };
            this._inputSignalsUlongMaskBoxes = new MaskedTextBox[] { this._TTNP_Box, this._TT_Box, this._switcherTimeBox, this._switcherImpulseBox, this._switcherDurationBox, this._releDispepairBox };
            this.ClearCombos(this._inputSignalsCombos);
            this.FillInputSignalsCombos();
            this.SubscriptCombos(this._inputSignalsCombos);
            this._logicChannelsCombo.SelectedIndex = 0;
            this.PrepareMaskedBoxes(this._inputSignalsDoubleMaskBoxes, typeof(double));
            this.PrepareMaskedBoxes(this._inputSignalsUlongMaskBoxes, typeof(ulong));
        }

        private void FillInputSignalsCombos()
        {
            this._extOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._extOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._constraintGroupCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._signalizationCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherBlockCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherErrorCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TNNP_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TN_dispepairCombo.Items.AddRange(Strings.Logic.ToArray());
            this._TN_typeCombo.Items.AddRange(Strings.TN_Type.ToArray());
            this._OMP_TypeBox.Items.AddRange(Strings.ModesLight.ToArray());
            this._manageSignalsButtonCombo.Items.AddRange(Strings.Forbidden.ToArray());
            this._manageSignalsKeyCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsExternalCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsSDTU_Combo.Items.AddRange(Strings.Forbidden.ToArray());
            this._kontrDiskrCB.Items.AddRange(Strings.ModesLight.ToArray());
            this._kontrIOCB.Items.AddRange(Strings.ModesLight.ToArray());
        }

        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ShowInputLogicSignals(int channel)
        {
            this._logicSignalsDataGrid.Rows.Clear();
            LogicState[] logicSignals = this._device.GetInputLogicSignals(channel);
            for (int i = 0; i < 16; i++)
            {
                this._logicSignalsDataGrid.Rows.Add(new object[] { i + 1, logicSignals[i].ToString() });
            }

        }

        private void ValidateInputSignals()
        {
            this.ValidateMaskedBoxes(this._inputSignalsUlongMaskBoxes);
            this.ValidateMaskedBoxes(this._inputSignalsDoubleMaskBoxes);
        }

        private bool WriteInputSignals()
        {
            try
            {
                this._device.TT = ushort.Parse(this._TT_Box.Text);
                this._device.TTNP = ushort.Parse(this._TTNP_Box.Text);
                this._device.TNNP = double.Parse(this._TNNP_Box.Text);
                this._device.TN = double.Parse(this._TN_Box.Text);

                this._device.MaxTok = double.Parse(this._maxTok_Box.Text);
                this._device.SwitcherDuration = ulong.Parse(this._switcherDurationBox.Text);
                this._device.SwitcherImpulse = ulong.Parse(this._switcherImpulseBox.Text);
                this._device.SwitcherTimeUROV = ulong.Parse(this._switcherTimeBox.Text);

                this._device.SwitcherTokUROV = double.Parse(this._switcherTokBox.Text);

                this._device.ExternalOff = this._extOffCombo.SelectedItem.ToString();
                this._device.ExternalOn = this._extOnCombo.SelectedItem.ToString();
                this._device.ConstraintGroup = this._constraintGroupCombo.SelectedItem.ToString();
                this._device.KeyOff = this._keyOffCombo.SelectedItem.ToString();
                this._device.KeyOn = this._keyOnCombo.SelectedItem.ToString();
                this._device.SignalizationReset = this._signalizationCombo.SelectedItem.ToString();
                this._device.SwitcherBlock = this._switcherBlockCombo.SelectedItem.ToString();
                this._device.SwitcherError = this._switcherErrorCombo.SelectedItem.ToString();
                this._device.SwitcherOff = this._switcherStateOffCombo.SelectedItem.ToString();
                this._device.SwitcherOn = this._switcherStateOnCombo.SelectedItem.ToString();
                this._device.KontrDiskr = this._kontrDiskrCB.SelectedItem.ToString();
                this._device.KontrIO = this._kontrIOCB.SelectedItem.ToString();


                CheckedListBoxManager checkManager = new CheckedListBoxManager();
                checkManager.CheckList = this._dispepairCheckList;
                this._device.DispepairSignal = checkManager.ToBitArray();
                this._device.DispepairImpulse = ulong.Parse(this._releDispepairBox.Text);

                this._device.TNNP_Dispepair = this._TNNP_dispepairCombo.SelectedItem.ToString();
                this._device.TN_Dispepair = this._TN_dispepairCombo.SelectedItem.ToString();
                this._device.TN_Type = this._TN_typeCombo.SelectedItem.ToString();
                this._device.OMP_Type = this._OMP_TypeBox.SelectedItem.ToString();
                this._device.HUD = double.Parse(this._HUD_Box.Text);

                this._device.ManageSignalButton = this._manageSignalsButtonCombo.SelectedItem.ToString();
                this._device.ManageSignalExternal = this._manageSignalsExternalCombo.SelectedItem.ToString();
                this._device.ManageSignalKey = this._manageSignalsKeyCombo.SelectedItem.ToString();
                this._device.ManageSignalSDTU = this._manageSignalsSDTU_Combo.SelectedItem.ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }

        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            LogicState[] logicSignals = new LogicState[16];
            for (int i = 0; i < this._logicSignalsDataGrid.Rows.Count; i++)
            {
                object o = Enum.Parse(typeof(LogicState), this._logicSignalsDataGrid["_diskretValueCol", i].Value.ToString());
                logicSignals[i] = (LogicState)(o);
            }
            this._device.SetInputLogicSignals(this._logicChannelsCombo.SelectedIndex, logicSignals);
        }
        #endregion

        #region �������� ����������
        void _device_AutomaticsPageLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_AutomaticsPageLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAutomaticsPageLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;
        private RadioButtonSelector _groupSelector;

        private void ValidateAutomatics()
        {
            this.ValidateMaskedBoxes(this._automaticsMaskedBoxes);
            this.ValidateMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint });
        }

        private void PrepareAutomaticsSignals()
        {
            this._automaticCombos = new ComboBox[]
            {
                this.apv_self_off, this.apv_conf, this.apv_blocking,
                this.avr_abrasion, this.avr_blocking, this.avr_reset_blocking,
                this.avr_return, this.avr_start, this.lzsh_conf, this.lzsh_conf1_17
            };
            this._automaticsMaskedBoxes = new MaskedTextBox[]
            {
                this.apv_time_1krat, this.apv_time_2krat, this.apv_time_3krat, this.apv_time_4krat,
                this.apv_time_blocking, this.apv_time_ready, this.avr_disconnection,
                this.avr_time_abrasion, this.avr_time_return,
            };
            this.PrepareMaskedBoxes(this._automaticsMaskedBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint }, typeof(double));
            
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
        }

        private void FillAutomaticCombo()
        {
            this.apv_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_start.Items.AddRange(Strings.Logic.ToArray());
            this.avr_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_reset_blocking.Items.AddRange(Strings.Logic.ToArray());
            this.avr_abrasion.Items.AddRange(Strings.Logic.ToArray());
            this.avr_return.Items.AddRange(Strings.Logic.ToArray());
            this.apv_conf.Items.AddRange(Strings.Crat.ToArray());
            this.lzsh_conf.Items.AddRange(Strings.ModesLight.ToArray());
            this.lzsh_conf1_17.Items.AddRange(Strings.LZS1_17.ToArray());
            this.apv_self_off.Items.AddRange(Strings.YesNo.ToArray());


        }
        private void ReadAutomaticsPage()
        {
            this._exchangeProgressBar.PerformStep();
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.APV_Cnf;
            this.apv_blocking.Text = this._device.APV_Blocking;
            this.apv_self_off.Text = this._device.APV_Start;
            this.avr_start.Text = this._device.AVR_Start;
            this.avr_blocking.Text = this._device.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.AVR_Reset;
            this.avr_abrasion.Text = this._device.AVR_Abrasion;
            this.avr_return.Text = this._device.AVR_Return;
            if (this._devVersion < 1.17)
            {
                this.lzsh_conf.Text = this._device.LZSH_Cnf;
            }
            else
            {
                this.lzsh_conf1_17.Text = this._device.LZSH_Cnf1_17;
            }

            this.apv_time_blocking.Text = this._device.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.AVR_Time_Off.ToString();
            this.lzsh_constraint.Text = this._device.LZSH_Constraint.ToString();
        }

        private void OnAutomaticsPageLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            this.apv_conf.Text = this._device.APV_Cnf;
            this.apv_blocking.Text = this._device.APV_Blocking;
            this.apv_self_off.Text = this._device.APV_Start;
            this.avr_start.Text = this._device.AVR_Start;
            this.avr_blocking.Text = this._device.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.AVR_Reset;
            this.avr_abrasion.Text = this._device.AVR_Abrasion;
            this.avr_return.Text = this._device.AVR_Return;
            if (this._devVersion < 1.17)
            {
                this.lzsh_conf.Text = this._device.LZSH_Cnf;
            }
            else
            {
                this.lzsh_conf1_17.Text = this._device.LZSH_Cnf1_17;
            }

            this.apv_time_blocking.Text = this._device.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.AVR_Time_Off.ToString();
            this.lzsh_constraint.Text = this._device.LZSH_Constraint.ToString();

        }

        private bool WriteAutomaticsPage()
        {
            this._device.APV_Cnf = this.apv_conf.Text;
            this._device.APV_Blocking = this.apv_blocking.Text;
            this._device.APV_Time_Blocking = ulong.Parse(this.apv_time_blocking.Text);
            this._device.APV_Time_Ready = ulong.Parse(this.apv_time_ready.Text);
            this._device.APV_Time_1Krat = ulong.Parse(this.apv_time_1krat.Text);
            this._device.APV_Time_2Krat = ulong.Parse(this.apv_time_2krat.Text);
            this._device.APV_Time_3Krat = ulong.Parse(this.apv_time_3krat.Text);
            this._device.APV_Time_4Krat = ulong.Parse(this.apv_time_4krat.Text);
            this._device.APV_Start = this.apv_self_off.Text;

            this._device.AVR_Supply_Off = this.avr_supply_off.Checked;
            this._device.AVR_Self_Off = this.avr_self_off.Checked;
            this._device.AVR_Switch_Off = this.avr_switch_off.Checked;
            this._device.AVR_Abrasion_Switch = this.avr_abrasion_switch.Checked;
            this._device.AVR_Reset_Switch = this.avr_permit_reset_switch.Checked;
            this._device.AVR_Start = this.avr_start.Text;
            this._device.AVR_Blocking = this.avr_blocking.Text;
            this._device.AVR_Reset = this.avr_reset_blocking.Text;
            this._device.AVR_Abrasion = this.avr_abrasion.Text;
            this._device.AVR_Time_Abrasion = ulong.Parse(this.avr_time_abrasion.Text);
            this._device.AVR_Return = this.avr_return.Text;
            this._device.AVR_Time_Return = ulong.Parse(this.avr_time_return.Text);
            this._device.AVR_Time_Off = ulong.Parse(this.avr_disconnection.Text);

            if (this._devVersion < 1.17)
            {
                this._device.LZSH_Cnf = this.lzsh_conf.Text;
            }
            else
            {
                this._device.LZSH_Cnf1_17 = this.lzsh_conf1_17.Text;
            }
            this._device.LZSH_Constraint = double.Parse(this.lzsh_constraint.Text);
            return true;
        }
        #endregion

        #region ������� ������

        void ChangeTokDefenseCellDisabling1(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                GridManager.ChangeCellDisabling(grid, row, "���", 5, 6);
                GridManager.ChangeCellDisabling(grid, row, false, 11, 12);
            }
        }
        void ChangeTokDefenseCellDisabling3(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                if (0 == row)
                {
                    GridManager.ChangeCellDisabling(grid, row, false, 3, 4);
                    GridManager.ChangeCellDisabling(grid, row, false, 7, 8);
                }

            }
        }

        void ShowTokDefenses(MR700.CTokDefenses tokDefenses)
        {
            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();
            this._tokDefenseIbox.Text = tokDefenses.I.ToString();
            this._tokDefenseInbox.Text = tokDefenses.In.ToString();
            this._tokDefenseI0box.Text = tokDefenses.I0.ToString();
            this._tokDefenseI2box.Text = tokDefenses.I2.ToString();

            for (int i = 0; i < 4; i++)
            {
                if (tokDefenses[i].Feature.ToString(CultureInfo.InvariantCulture) == "���������")
                {
                    if (tokDefenses[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,// - 0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime/10,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }
                else
                {
                    if (tokDefenses[i].WorkConstraint <= 24)
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                    else
                    {
                        this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,// - 0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                        this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid1, i);
                    }
                }

            }
            for (int i = 4; i < 10; i++)
            {
                if (tokDefenses[i].WorkConstraint <= 24)
                {
                    this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }
                else
                {

                    this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].PuskU,
                                                       tokDefenses[i].PuskU_Constraint,
                                                       tokDefenses[i].Direction,
                                                       tokDefenses[i].BlockingExist,
                                                       tokDefenses[i].Parameter,
                                                       tokDefenses[i].WorkConstraint,//-0.01,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].AVR});
                    this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
                }
            }
            const int Ig_index = 10;
            this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[Ig_index].Name,
                                                       tokDefenses[Ig_index].Mode,     
                                                       tokDefenses[Ig_index].BlockingNumber,
                                                       tokDefenses[Ig_index].PuskU,
                                                       tokDefenses[Ig_index].PuskU_Constraint,
                                                       tokDefenses[Ig_index].WorkConstraint,
                                                       tokDefenses[Ig_index].WorkTime,
                                                       tokDefenses[Ig_index].Speedup,
                                                       tokDefenses[Ig_index].SpeedupTime,
                                                       tokDefenses[Ig_index].UROV,
                                                       tokDefenses[Ig_index].APV,
                                                       tokDefenses[Ig_index].AVR                                                    

            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);
            const int I12_index = 11;
            this._tokDefenseGrid4.Rows.Add(new object[]{ tokDefenses[I12_index].Name,
                                                       tokDefenses[I12_index].Mode,     
                                                       tokDefenses[I12_index].BlockingNumber,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].WorkConstraint,
                                                       tokDefenses[I12_index].WorkTime,
                                                       false,
                                                       0.0,
                                                       tokDefenses[I12_index].UROV,
                                                       tokDefenses[I12_index].APV,
                                                       tokDefenses[I12_index].AVR  });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);

            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4UpuskCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4PuskConstraintCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid1.Rows.Count; i++)
            {
                this._tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        }

        void PrepareTokDefenses()
        {
            this._tokDefenseModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefenseParameterCol.Items.AddRange(Strings.TokParameter.ToArray());
            this._tokDefenseBlockExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefenseDirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefenseFeatureCol.Items.AddRange(Strings.FeatureLight.ToArray());
            this._tokDefenseBlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefense3ModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefense3ParameterCol.Items.AddRange(Strings.EngineParameter.ToArray());
            this._tokDefense3BlockingExistCol.Items.AddRange(Strings.Blocking.ToArray());
            this._tokDefense3DirectionCol.Items.AddRange(Strings.BusDirection.ToArray());
            this._tokDefense3FeatureCol.Items.AddRange(Strings.Feature.ToArray());
            this._tokDefense3BlockingNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefense4ModeCol.Items.AddRange(Strings.Modes.ToArray());
            this._tokDefense4BlockNumberCol.Items.AddRange(Strings.Logic.ToArray());

            this._tokDefenseI0box.ValidatingType = this._tokDefenseI2box.ValidatingType =
            this._tokDefenseInbox.ValidatingType = this._tokDefenseIbox.ValidatingType = typeof(ulong);

            this._tokDefenseIbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI2box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI0box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseInbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);

            try
            {
                this.ShowTokDefenses(this._device.TokDefensesMain);
            }
            catch (Exception eee)
            { }


            this._tokDefenseGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid4.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);

            this._tokDefenseGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid4.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
        }

        void _tokDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (this._tokDefenseModeCol == e.Cell.OwningColumn ||
                this._tokDefenseU_PuskCol == e.Cell.OwningColumn ||
                this._tokDefenseSpeedUpCol == e.Cell.OwningColumn ||
                this._tokDefenseDirectionCol == e.Cell.OwningColumn ||

                this._tokDefense3ModeCol == e.Cell.OwningColumn ||
                this._tokDefense3UpuskCol == e.Cell.OwningColumn ||
                this._tokDefense3SpeedupCol == e.Cell.OwningColumn ||
                this._tokDefense3DirectionCol == e.Cell.OwningColumn
                )
            {
                this.ChangeTokDefenseCellDisabling1(sender as DataGridView, e.Cell.RowIndex);
            }
            if (this._tokDefense4ModeCol == e.Cell.OwningColumn ||
                (this._tokDefense4SpeedupCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex) ||
                this._tokDefense4UpuskCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex)
            {
                this.ChangeTokDefenseCellDisabling3(sender as DataGridView, e.Cell.RowIndex);
            }

        }

        void _device_TokDefensesMainLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnTokDefenseLoadOk()
        {
            this._exchangeProgressBar.PerformStep();

            if (this._mainRadioButtonDefend.Checked)
            {
                this.ShowTokDefenses(this._device.TokDefensesMain);
            }
            else
            {
                this.ShowTokDefenses(this._device.TokDefensesReserve);
            }
        }
        void _device_TokDefensesMainLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnTokDefenseLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private bool OnTokDefenseGridModeChanged(DataGridView grid, int row)
        {
            int[] columns = new int[grid.Columns.Count - 2];
            if (grid == this._tokDefenseGrid4 && 1 == row)
            {
                columns = new int[6] { 2, 5, 6, 9, 10, 11 };
            }
            else
            {
                for (int i = 0; i < columns.Length; i++)
                {
                    columns[i] = i + 2;
                }
            }

            return GridManager.ChangeCellDisabling(grid, row, "��������", 1, columns);
        }

        private bool WriteTokDefenseItem(MR700.CTokDefenses tokDefenses, DataGridView grid, int rowIndex, int itemIndex)
        {
            bool ret = true;
            bool enabled = false;
            tokDefenses[itemIndex].Mode = grid[1, rowIndex].Value.ToString();
            enabled = tokDefenses[itemIndex].Mode == "��������";
            //if (enabled)
            //{
                tokDefenses[itemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                tokDefenses[itemIndex].PuskU = (bool)grid[3, rowIndex].Value;
                try
                {
                    double value = double.Parse(grid[4, rowIndex].Value.ToString());
                    if ((value < 0 || value > 256) && !enabled)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.InvariantCulture + "0]", 
                            grid[4, rowIndex], this._defendingTabPage, this._tokDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].PuskU_Constraint = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.InvariantCulture + "0]", 
                        grid[4, rowIndex], this._defendingTabPage, this._tokDefendingTabPage);
                    ret = false;
                }

                if (grid != this._tokDefenseGrid4)
                {
                    tokDefenses[itemIndex].Direction = grid[5, rowIndex].Value.ToString();
                    tokDefenses[itemIndex].BlockingExist = grid[6, rowIndex].Value.ToString();
                    tokDefenses[itemIndex].Parameter = grid[7, rowIndex].Value.ToString();
                }

                int workConstraintIndex = (grid == this._tokDefenseGrid4) ? 5 : 8;
                int workTimeIndex = (grid == this._tokDefenseGrid4) ? 6 : 10;
                int speedupIndex = (grid == this._tokDefenseGrid4) ? 7 : 11;
                int speedupTimeIndex = (grid == this._tokDefenseGrid4) ? 8 : 12;
                int UROVIndex = (grid == this._tokDefenseGrid4) ? 9 : 13;
                int APVIndex = (grid == this._tokDefenseGrid4) ? 10 : 14;
                int AVRIndex = (grid == this._tokDefenseGrid4) ? 11 : 15;

                double limit = 40;
                if (grid == this._tokDefenseGrid1)
                {
                    //limit = 5;
                }
                if (grid == this._tokDefenseGrid4)
                {
                    if (0 == rowIndex)
                    {
                        limit = 5;
                    }
                    else
                    {
                        limit = 100;
                    }
                }
                if (grid == this._tokDefenseGrid3)
                {
                    if (4 == rowIndex || 5 == rowIndex)
                    {
                        limit = 5;
                    }
                }

            try
            {
                double value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString());
                if ((value < 0 || value > limit) && !enabled)
                {
                    string message = $"������� ����� � ��������� [0,0 - {limit},0]";
                    this.ShowToolTip(message, grid[workConstraintIndex, rowIndex], this._defendingTabPage, this._tokDefendingTabPage);
                    ret = false;
                }
                else
                {
                    tokDefenses[itemIndex].WorkConstraint = value;
                }

            }
            catch (Exception)
            {
                string message = $"������� ����� � ��������� [0,0 - {limit},0]";
                this.ShowToolTip(message, grid[workConstraintIndex, rowIndex], this._defendingTabPage, this._tokDefendingTabPage);
                ret = false;
            }
            tokDefenses[itemIndex].Feature = grid[9, rowIndex].Value.ToString();
            try
            {
                ulong value;
                if (grid[workTimeIndex, rowIndex].Value == null) 
                { 
                    value = 0; 
                } 
                else 
                { 
                    value = ulong.Parse(grid[workTimeIndex, rowIndex].Value.ToString()); 
                }
                    

                if ("���������" == grid[9, rowIndex].Value.ToString())
                {
                    if (value > MR700.KZLIMIT && !enabled)
                    {
                        this.ShowToolTip(MR700.KZLIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._defendingTabPage,
                            this._tokDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value * 10;
                    }

                }
                else
                {
                    if (value > MR700.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._defendingTabPage,
                            this._tokDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value;
                    }
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._defendingTabPage,
                    this._tokDefendingTabPage);
                ret = false;
            }
            tokDefenses[itemIndex].Speedup = (bool)grid[speedupIndex, rowIndex].Value;
            try
            {
                ulong value = ulong.Parse(grid[speedupTimeIndex, rowIndex].Value.ToString());
                if (value > MR700.TIMELIMIT && !enabled)
                {
                    this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._defendingTabPage,
                        this._tokDefendingTabPage);
                    ret = false;
                }
                else
                {
                    tokDefenses[itemIndex].SpeedupTime = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._defendingTabPage,
                    this._tokDefendingTabPage);
                ret = false;
            }
            tokDefenses[itemIndex].UROV = (bool)grid[UROVIndex, rowIndex].Value;
            tokDefenses[itemIndex].APV = (bool)grid[APVIndex, rowIndex].Value;
            tokDefenses[itemIndex].AVR = (bool)grid[AVRIndex, rowIndex].Value;
            return ret;
        }

        private bool WriteTokDefenses(MR700.CTokDefenses tokDefenses)
        {
            bool ret = true;
            tokDefenses.I = ushort.Parse(this._tokDefenseIbox.Text);
            tokDefenses.I0 = ushort.Parse(this._tokDefenseI0box.Text);
            tokDefenses.I2 = ushort.Parse(this._tokDefenseI2box.Text);
            tokDefenses.In = ushort.Parse(this._tokDefenseInbox.Text);
            for (int i = 0; i < 12; i++)
            {
                if (i >= 0 && i < 4)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid1, i, i);
                }
                if (i >= 4 && i < 10)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid3, i - 4, i);
                }
                if (i >= 10 && i < 12)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid4, i - 10, i);
                }
            }

            return ret;
        }

        private void ValidateTokDefenses()
        {
            this._tokDefenseIbox.ValidateText();
            this._tokDefenseI2box.ValidateText();
            this._tokDefenseI0box.ValidateText();
            this._tokDefenseInbox.ValidateText();
        }

        #endregion

        #region ������ �� ����������
        void _device_VoltageDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_VoltageDefensesLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnVoltageDefensesLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnVoltageDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainRadioButtonDefend.Checked)
            {
                this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
            }
            else
            {
                this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);
            }

        }

        private void PrepareVoltageDefenses()
        {
            this._voltageDefensesGrid1.Rows.Add(4);
            this._voltageDefensesGrid1[0, 0].Value = "U>";
            this._voltageDefensesGrid1[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 1].Value = "U>>";
            this._voltageDefensesGrid1[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 2].Value = "U<";
            this._voltageDefensesGrid1[0, 2].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid1[0, 3].Value = "U<<";
            this._voltageDefensesGrid1[0, 3].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2.Rows.Add(2);
            this._voltageDefensesGrid2[0, 0].Value = "U2>";
            this._voltageDefensesGrid2[0, 1].Value = "U2>>";
            this._voltageDefensesGrid2[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid2[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3.Rows.Add(2);
            this._voltageDefensesGrid3[0, 0].Value = "U0>";
            this._voltageDefensesGrid3[0, 1].Value = "U0>>";
            this._voltageDefensesGrid3[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesGrid3[0, 1].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

            this._voltageDefensesGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._voltageDefensesGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._voltageDefensesGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);

            this._voltageDefensesModeCol1.Items.AddRange(Strings.Modes.ToArray());
            this._voltageDefensesBlockingNumberCol1.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol1.Items.AddRange(Strings.VoltageParameterU.ToArray());

            this._voltageDefensesModeCol2.Items.AddRange(Strings.Modes.ToArray());
            this._voltageDefensesBlockingNumberCol2.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol2.Items.AddRange(Strings.VoltageParameterU2.ToArray());

            this._voltageDefensesModeCol3.Items.AddRange(Strings.Modes.ToArray());
            this._voltageDefensesBlockingNumberCol3.Items.AddRange(Strings.Logic.ToArray());
            this._voltageDefensesParameterCol3.Items.AddRange(Strings.VoltageParameterU0.ToArray());

            this._voltageDefensesGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);
            this._voltageDefensesGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);
            this._voltageDefensesGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnVoltageDefenseCellStateChanged);

            this.ShowVoltageDefenses(this._device.VoltageDefensesMain);
        }

        void OnVoltageDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._voltageDefensesModeCol1 ||
                e.Cell.OwningColumn == this._voltageDefensesModeCol2 ||
                e.Cell.OwningColumn == this._voltageDefensesModeCol3 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol1 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol2 ||
                e.Cell.OwningColumn == this._voltageDefensesReturnCol3
                )
            {
                DataGridView voltageGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnVoltageDefenseModeChange(voltageGrid, rowIndex);
            }
        }

        private static void OnVoltageDefenseModeChange(DataGridView voltageGrid, int rowIndex)
        {
            int[] columns = new int[voltageGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            if (!GridManager.ChangeCellDisabling(voltageGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(voltageGrid, rowIndex, false, 6, 7, 8, 9);
            }
        }

        private void SetVoltageDefenseGridItem(DataGridView grid, MR700.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            grid[1, rowIndex].Value = voltageDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = voltageDefenses[defenseItemIndex].BlockingNumber;
            grid[3, rowIndex].Value = voltageDefenses[defenseItemIndex].Parameter;
            grid[4, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkConstraint;
            grid[5, rowIndex].Value = voltageDefenses[defenseItemIndex].WorkTime;
            grid[6, rowIndex].Value = voltageDefenses[defenseItemIndex].Return;
            grid[7, rowIndex].Value = voltageDefenses[defenseItemIndex].APV_Return;
            grid[8, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnConstraint;
            grid[9, rowIndex].Value = voltageDefenses[defenseItemIndex].ReturnTime;
            grid[10, rowIndex].Value = voltageDefenses[defenseItemIndex].UROV;
            grid[11, rowIndex].Value = voltageDefenses[defenseItemIndex].APV;
            grid[12, rowIndex].Value = voltageDefenses[defenseItemIndex].AVR;
            grid[13, rowIndex].Value = voltageDefenses[defenseItemIndex].Reset;
        }

        private void ShowVoltageDefenses(MR700.CVoltageDefenses voltageDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid1, voltageDefenses, i, i);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid1, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid2, voltageDefenses, i, i + 4);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid2, i);
            }
            for (int i = 0; i < 2; i++)
            {
                this.SetVoltageDefenseGridItem(this._voltageDefensesGrid3, voltageDefenses, i, i + 6);
                OnVoltageDefenseModeChange(this._voltageDefensesGrid3, i);
            }

        }

        private bool WriteVoltageDefenseGridItem(DataGridView grid, MR700.CVoltageDefenses voltageDefenses, int rowIndex, int defenseItemIndex)
        {
            bool ret = true;
            bool enabled = false;
            voltageDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            enabled = voltageDefenses[defenseItemIndex].Mode == "��������";
            //if (enabled)
            //{
                voltageDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                voltageDefenses[defenseItemIndex].Parameter = grid[3, rowIndex].Value.ToString();
                try
                {
                    double value = double.Parse(grid[4, rowIndex].Value.ToString());
                    if ((value < 0 || value > 256) && !enabled)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]",
                            grid[4, rowIndex], this._defendingTabPage, this._voltageDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].WorkConstraint = double.Parse(grid[4, rowIndex].Value.ToString());
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]", 
                        grid[4, rowIndex], this._defendingTabPage, this._voltageDefendingTabPage);
                    ret = false;
                }
                try
                {
                    ulong value;
                    if (grid[5, rowIndex].Value == null)
                    {
                        value = 0;
                    }
                    else 
                    {
                        value = ulong.Parse(grid[5, rowIndex].Value.ToString());
                    }
                    
                    if (value > MR700.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], this._defendingTabPage,
                            this._voltageDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[5, rowIndex], this._defendingTabPage,
                        this._voltageDefendingTabPage);
                    ret = false;
                }
                voltageDefenses[defenseItemIndex].Return = (bool)grid[6, rowIndex].Value;
                voltageDefenses[defenseItemIndex].APV_Return = (bool)grid[7, rowIndex].Value;

                try
                {
                    double value = double.Parse(grid[8, rowIndex].Value.ToString());
                    if ((value < 0 || value > 256) && !enabled)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]", 
                            grid[8, rowIndex], this._defendingTabPage, this._voltageDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].ReturnConstraint = value;
                    }
                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � ��������� [0 - 256" + CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator + "0]", 
                        grid[8, rowIndex], this._defendingTabPage, this._voltageDefendingTabPage);
                    ret = false;
                }

                try
                {
                    ulong value = ulong.Parse(grid[9, rowIndex].Value.ToString());
                    if (value > MR700.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], this._defendingTabPage,
                            this._voltageDefendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        voltageDefenses[defenseItemIndex].ReturnTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[9, rowIndex], this._defendingTabPage, this._voltageDefendingTabPage);
                    ret = false;
                }


                voltageDefenses[defenseItemIndex].UROV = (bool)grid[10, rowIndex].Value;
                voltageDefenses[defenseItemIndex].APV = (bool)grid[11, rowIndex].Value;
                voltageDefenses[defenseItemIndex].AVR = (bool)grid[12, rowIndex].Value;
                voltageDefenses[defenseItemIndex].Reset = (bool)grid[13, rowIndex].Value;
            //}
            return ret;
        }

        public bool WriteVoltageDefenses(MR700.CVoltageDefenses voltageDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid1, voltageDefenses, i, i);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid2, voltageDefenses, i, i + 4);
            }
            for (int i = 0; i < 2; i++)
            {
                ret &= this.WriteVoltageDefenseGridItem(this._voltageDefensesGrid3, voltageDefenses, i, i + 6);
            }
            return ret;
        }

        #endregion

        #region �������� ����� �� �������

        void _device_FrequenceDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_FrequenceDefensesLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnFrequenceDefensesLoadOk));
            }
            catch (InvalidOperationException)
            { }
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnFrequenceDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainRadioButtonDefend.Checked)
            {
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
            else
            {
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }

        }

        private void PrepareFrequenceDefenses()
        {
            this._frequenceDefensesGrid.Rows.Add(4);

            this._frequenceDefensesMode.Items.AddRange(Strings.Modes.ToArray());
            this._frequenceDefensesBlockingNumber.Items.AddRange(Strings.Logic.ToArray());
            this._frequenceDefensesGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._frequenceDefensesGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this.OnFrequenceDefenseCellStateChanged);
            this._frequenceDefensesGrid.Height = this._frequenceDefensesGrid.ColumnHeadersHeight + (this._frequenceDefensesGrid.Rows.Count + 1) * this._frequenceDefensesGrid.RowTemplate.Height;
            this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
        }

        void OnFrequenceDefenseCellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (e.Cell.OwningColumn == this._frequenceDefensesMode || e.Cell.OwningColumn == this._frequenceDefensesReturn)
            {
                DataGridView frequenceGrid = sender as DataGridView;
                int rowIndex = e.Cell.RowIndex;

                OnFrequenceDefenseModeChanged(frequenceGrid, rowIndex);
            }
        }

        private static void OnFrequenceDefenseModeChanged(DataGridView frequenceGrid, int rowIndex)
        {
            int[] columns = new int[frequenceGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }

            if (!GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, "��������", 1, columns))
            {
                GridManager.ChangeCellDisabling(frequenceGrid, rowIndex, false, 5, 6, 7, 8);
            }
        }

        private void SetFrequenceDefenseGridItem(DataGridView grid, MR700.CFrequenceDefenses frequenceDefenses, int rowIndex, int defenseItemIndex)
        {
            grid[0, rowIndex].Value = frequenceDefenses[defenseItemIndex].Name;
            grid[1, rowIndex].Value = frequenceDefenses[defenseItemIndex].Mode;
            grid[2, rowIndex].Value = frequenceDefenses[defenseItemIndex].BlockingNumber;
            grid[3, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkConstraint;
            grid[4, rowIndex].Value = frequenceDefenses[defenseItemIndex].WorkTime;
            grid[5, rowIndex].Value = frequenceDefenses[defenseItemIndex].Return;
            grid[6, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV_Return;
            grid[7, rowIndex].Value = frequenceDefenses[defenseItemIndex].ReturnTime;
            grid[8, rowIndex].Value = frequenceDefenses[defenseItemIndex].ConstraintAPV;
            grid[9, rowIndex].Value = frequenceDefenses[defenseItemIndex].UROV;
            grid[10, rowIndex].Value = frequenceDefenses[defenseItemIndex].APV;
            grid[11, rowIndex].Value = frequenceDefenses[defenseItemIndex].AVR;
            grid[12, rowIndex].Value = frequenceDefenses[defenseItemIndex].Reset;
        }

        private void ShowFrequenceDefenses(MR700.CFrequenceDefenses frequenceDefenses)
        {
            for (int i = 0; i < 4; i++)
            {
                this.SetFrequenceDefenseGridItem(this._frequenceDefensesGrid, frequenceDefenses, i, i);
                OnFrequenceDefenseModeChanged(this._frequenceDefensesGrid, i);
            }
        }

        private bool WriteFrequenceDefenseGridItem(DataGridView grid, MR700.CFrequenceDefenses frequenceDefenses,
            int rowIndex, int defenseItemIndex)
        {
            bool rowEnabled = false;
            bool ret = true;
            frequenceDefenses[defenseItemIndex].Mode = grid[1, rowIndex].Value.ToString();
            rowEnabled = "��������" == frequenceDefenses[defenseItemIndex].Mode;

            frequenceDefenses[defenseItemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
            try
            {
                double value = double.Parse(grid[3, rowIndex].Value.ToString());
                if ((value < 47 || value > 52) && !rowEnabled)
                {
                    string message = string.Format("������� ����� � ��������� [47{0}0 - 52{1}0]",
                        CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                        CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    this.ShowToolTip(message, grid[3, rowIndex], this._defendingTabPage, this._frequencyDefendingTabPage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].WorkConstraint = value;
                }

            }
            catch (Exception)
            {
                string message = string.Format("������� ����� � ��������� [47{0}0 - 52{1}0]",
                       CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                       CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                this.ShowToolTip(message, grid[3, rowIndex], this._defendingTabPage, this._frequencyDefendingTabPage);
                ret = false;
            }
            try
            {
                ulong value;
                if (grid[4, rowIndex].Value == null)
                {
                    value = 0;
                }
                else
                {
                    value = ulong.Parse(grid[4, rowIndex].Value.ToString());
                }
                if (value > MR700.TIMELIMIT && !rowEnabled)
                {
                    throw new OverflowException(MR700.TIMELIMIT_ERROR_MSG);
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].WorkTime = value;
                }
            }
            catch (Exception)
            {
                this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[4, rowIndex], this._defendingTabPage,
                    this._frequencyDefendingTabPage);
                ret = false;
            }
            frequenceDefenses[defenseItemIndex].Return = (bool) grid[5, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].APV_Return = (bool) grid[6, rowIndex].Value;
            try
            {
                ulong value = ulong.Parse(grid[7, rowIndex].Value.ToString());
                if (value > MR700.TIMELIMIT && !rowEnabled)
                {
                    throw new OverflowException(MR700.TIMELIMIT_ERROR_MSG);
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].ReturnTime = value;
                }
            }
            catch (Exception)
            {
                this.ShowToolTip(MR700.TIMELIMIT_ERROR_MSG, grid[7, rowIndex], this._defendingTabPage,
                    this._frequencyDefendingTabPage);
                ret = false;
            }
            try
            {
                double value = double.Parse(grid[8, rowIndex].Value.ToString());

                if ((value < 47 || value > 52) && !rowEnabled && frequenceDefenses[defenseItemIndex].Return)
                {
                    string message = string.Format("������� ����� � ��������� [47{0}0 - 52{1}0]",
                       CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                       CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                    this.ShowToolTip(message, grid[8, rowIndex], this._defendingTabPage, this._frequencyDefendingTabPage);
                    ret = false;
                }
                else
                {
                    frequenceDefenses[defenseItemIndex].ConstraintAPV = value;
                }
            }
            catch (Exception)
            {
                string message = string.Format("������� ����� � ��������� [47{0}0 - 52{1}0]",
                       CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                       CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                this.ShowToolTip(message, grid[8, rowIndex], this._defendingTabPage,
                    this._frequencyDefendingTabPage);
                ret = false;
            }
            frequenceDefenses[defenseItemIndex].UROV = (bool) grid[9, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].APV = (bool) grid[10, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].AVR = (bool) grid[11, rowIndex].Value;
            frequenceDefenses[defenseItemIndex].Reset = (bool) grid[12, rowIndex].Value;
            return ret;
        }

        public bool WriteFrequenceDefenses(MR700.CFrequenceDefenses frequenceDefenses)
        {
            bool ret = true;
            for (int i = 0; i < 4; i++)
            {
                ret &= this.WriteFrequenceDefenseGridItem(this._frequenceDefensesGrid, frequenceDefenses, i, i);
            }

            return ret;
        }

        #endregion

        private void _oscKonfComb_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._device.OscilloscopeKonfCount = this._oscKonfComb.SelectedIndex;
            switch ( this._oscKonfComb.SelectedIndex)
            {
                case 0:
                    this._oscTimeLabel.Text = "2728 ��";
                    break;
                case 1:
                    this._oscTimeLabel.Text = "1363 ��";
                    break;
                case 2:
                    this._oscTimeLabel.Text = "908 ��";
                    break;
            }
        }

        private void _HUD_Box_TextChanged(object sender, EventArgs e)
        {
            string aa = "";
            try
            {
                Convert.ToInt32(this._HUD_Box.Text[this._HUD_Box.Text.Length - 1]);
                if (this._HUD_Box.Text[this._HUD_Box.Text.Length - 1] == ' ')
                {
                    throw new ArgumentNullException();
                }
                if (this._HUD_Box.Text[this._HUD_Box.Text.Length - 1] == '.')
                {
                    string a = "";
                    for (int i = 0; i < this._HUD_Box.Text.Length - 1; i++)
                    {
                        a += this._HUD_Box.Text[i];
                    }
                    this._HUD_Box.Text = a;
                    this._HUD_Box.AppendText(",");
                }
                if (Convert.ToDouble(this._HUD_Box.Text) > 1)
                {
                    this._HUD_Box.Text = "1";
                }
                if (Convert.ToDouble(this._HUD_Box.Text) < 0)
                {
                    this._HUD_Box.Text = "0";
                }
            }
            catch (Exception sww)
            {
                if ((this._HUD_Box.Text[this._HUD_Box.Text.Length - 1] != '.') || (this._HUD_Box.Text[this._HUD_Box.Text.Length - 1] != ','))
                {
                    aa = "";
                    if (this._HUD_Box.Text.Length != 1)
                    {
                        for (int i = 0; i < this._HUD_Box.Text.Length - 1; i++)
                        {
                            aa += this._HUD_Box.Text[i];
                        }
                    }
                    this._HUD_Box.Clear();
                }
            }
            this._HUD_Box.AppendText(aa);
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            this._saveToXmlButton.Focus(); // �������� ����� ������� ������, ����� ����������� �������� � datagridview
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._processLabel.Text = HtmlExport.Export(Resources.MR700Main, Resources.MR700Res, this._device, "��700", this._device.DeviceVersion);
            }
        }

        private void _mainRadioButtonDefend_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioButtonDefend.Checked)
            {
                this._device.TokDefensesReserve = new MR700.CTokDefenses();
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.ShowTokDefenses(this._device.TokDefensesMain);

                this.WriteVoltageDefenses(this._device.VoltageDefensesReserve);
                this.ShowVoltageDefenses(this._device.VoltageDefensesMain);

                this.WriteFrequenceDefenses(this._device.FrequenceDefensesReserve);
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesMain);
            }
            else
            {
                this._device.TokDefensesMain = new MR700.CTokDefenses();
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.ShowTokDefenses(this._device.TokDefensesReserve);

                this.WriteVoltageDefenses(this._device.VoltageDefensesMain);
                this.ShowVoltageDefenses(this._device.VoltageDefensesReserve);

                this.WriteFrequenceDefenses(this._device.FrequenceDefensesMain);
                this.ShowFrequenceDefenses(this._device.FrequenceDefensesReserve);
            }
        }

        private void avr_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if (cb == null) return;
            int ind = Convert.ToInt32(cb.Tag);
            switch (ind)
            {
                case 1:
                    this._device.AVR_Supply_Off = cb.Checked;
                    break;
                case 2:
                    this._device.AVR_Switch_Off = cb.Checked;
                    break;
                case 3:
                    this._device.AVR_Self_Off = cb.Checked;
                    break;
                case 4:
                    this._device.AVR_Abrasion_Switch = cb.Checked;
                    break;
                case 5:
                    this._device.AVR_Reset_Switch = cb.Checked;
                    break;
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
            }
        }

        private void Mr700ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}