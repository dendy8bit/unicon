using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using Microsoft.SqlServer.Server;

namespace BEMN.MR700.Version1
{
    public partial class MeasuringForm : Form , IFormView
    {
        private MR700 _device;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _automationLeds;
        private MemoryEntity<DateTimeStruct> _dateTime;

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(MR700 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this.Init();
        }

        private void Init()
        {
            this._manageLeds = new LedControl[] 
            {
                this._manageLed1, this._manageLed2, this._manageLed3, this._manageLed4, this._manageLed5, this._manageLed6, this._manageLed7, this._manageLed8
            };

            this._additionalLeds = new LedControl[]
            {
                this._addIndLed1, this._addIndLed2, this._addIndLed3, this._addIndLed4
            };

            this._indicatorLeds = new LedControl[]
            {
                this._indLed1, this._indLed2, this._indLed3, this._indLed4, this._indLed5, this._indLed6, this._indLed7, this._indLed8

            };

            this._inputLeds = new LedControl[] 
            {
                this._inD_led1, this._inD_led2, this._inD_led3, this._inD_led4, this._inD_led5, this._inD_led6, this._inD_led7, this._inD_led8, this._inD_led9,
                this._inD_led10, this._inD_led11, this._inD_led12, this._inD_led13, this._inD_led14, this._inD_led15, this._inD_led16, this._inL_led1, this._inL_led2,
                this._inL_led3, this._inL_led4, this._inL_led5, this._inL_led6, this._inL_led7, this._inL_led8

            };

            this._outputLeds = new LedControl[]
            {
                this._outLed1, this._outLed2, this._outLed3, this._outLed4, this._outLed5, this._outLed6, this._outLed7, this._outLed8

            };

            this._releLeds = new LedControl[]
            {
                this._releLed1, this._releLed2, this._releLed3, this._releLed4, this._releLed5, this._releLed6, this._releLed7, this._releLed8

            };

            this._limitLeds = new LedControl[]
            {
                this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4, this._ImaxLed5, this._ImaxLed6, this._ImaxLed7, this._ImaxLed8, this._I0maxLed1,
                this._I0maxLed2, this._I0maxLed3, this._I0maxLed4, this._I0maxLed5, this._I0maxLed6, this._I0maxLed7, this._I0maxLed8, this._InmaxLed1, this._InmaxLed2,
                this._InmaxLed3, this._InmaxLed4, this._InmaxLed5, this._InmaxLed6, this._InmaxLed7, this._InmaxLed8, this._FmaxLed1, this._FmaxLed2, this._FmaxLed3,
                this._FmaxLed4, this._FmaxLed5, this._FmaxLed6, this._FmaxLed7, this._FmaxLed8, this._UmaxLed1, this._UmaxLed2, this._UmaxLed3, this._UmaxLed4,
                this._UmaxLed5, this._UmaxLed6, this._UmaxLed7, this._UmaxLed8, this._U0maxLed1, this._U0maxLed2, this._U0maxLed3, this._U0maxLed4, this._U0maxLed5,
                this._U0maxLed6, this._U0maxLed7, this._U0maxLed8, this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8

            };

            this._faultStateLeds = new LedControl[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, this._faultStateLed4, this._faultStateLed5, this._faultStateLed6,
                this._faultStateLed7, this._faultStateLed8

            };

            this._faultSignalsLeds = new LedControl[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, this._faultSignalLed4, this._faultSignalLed5, this._faultSignalLed6,
                this._faultSignalLed7, this._faultSignalLed8, this._faultSignalLed9, this._faultSignalLed10, this._faultSignalLed11, this._faultSignalLed12,
                this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15, this._faultSignalLed16

            };

            this._automationLeds = new LedControl[]
            {
                this._autoLed1, this._autoLed2, this._autoLed3, this._autoLed4, this._autoLed5, this._autoLed6, this._autoLed7, this._autoLed8

            };
            
            this._device.DiagnosticLoadOk += HandlerHelper.CreateHandler(this, this.OnAnalogSignalsLoadOk);
            this._device.DiagnosticLoadFail += HandlerHelper.CreateHandler(this, this.OnAnalogSignalsLoadFail);

            this._device.AnalogSignalsLoadOK += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadOk);
            this._device.AnalogSignalsLoadFail += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadFail);
            
            this._dateTime = this._device.DateAndTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);
        }

        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }
        
        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticQuick();
            this._device.MakeDateTimeQuick();
            this._device.MakeAnalogSignalsQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticSlow();
            this._device.MakeDateTimeSlow();
            this._device.MakeAnalogSignalsSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveDiagnostic();
            this._device.RemoveDateTime();
            this._device.RemoveAnalogSignals();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.LoadAnalogSignalsCycle();
                this._device.LoadDiagnosticCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._device.RemoveDiagnostic();
                this._device.RemoveDateTime();
                this._device.RemoveAnalogSignals();
                this._dateTime.RemoveStructQueries();
                this.OnAnalogSignalsLoadFail();
                this.OnDiagnosticLoadFail();
            }
        }

        void OnAnalogSignalsLoadFail()
        {
            this._InBox.Text = string.Empty;
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;
            this._UnBox.Text = string.Empty;
            this._UaBox.Text = string.Empty;
            this._UbBox.Text = string.Empty;
            this._UcBox.Text = string.Empty;
            this._UabBox.Text = string.Empty;
            this._UbcBox.Text = string.Empty;
            this._UcaBox.Text = string.Empty;
            this._U0Box.Text = string.Empty;
            this._U1Box.Text = string.Empty;
            this._U2Box.Text = string.Empty;
            this._F_Box.Text = string.Empty;
        }
        
        private string Znak(string b, int first, int last)
        {
            string s = "";

            if (b[last] != '1')
            {
                if (b[first] == '0')
                {
                    s = "+";
                }
                else
                {
                    s = "-";
                }
            }

            return s;
        }

        private float ConvertVersion(string version)
        {
            string sVersion = "";
            float fVersion;

            for (int i = 0; i < version.Length; i++)
            {
                if (version[i] == '.')
                {
                    sVersion += ",";
                }
                else
                {
                    sVersion += version[i];
                }
            }

            fVersion = Convert.ToSingle(sVersion, CultureInfo.InvariantCulture);

            return fVersion;
        }

        private void OnAnalogSignalsLoadOk()
        {
            float Version = 0;

            try
            {
                Version = this.ConvertVersion(this._device.Info.Version);
            }
            catch (Exception)
            {

            }
            
            string s = "";
            
            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 0, 1);
            }
            this._InBox.Text = String.Format("In = " + s + " {0:F2} �", this._device.In, CultureInfo.InvariantCulture);

            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 2, 3);
            }
            this._IaBox.Text = String.Format("Ia = " + s + " {0:F2} �", this._device.Ia, CultureInfo.InvariantCulture);

            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 4, 5);
            }
            this._IbBox.Text = String.Format("Ib = " + s + " {0:F2} �", this._device.Ib, CultureInfo.InvariantCulture);

            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 6, 7);
            }
            this._IcBox.Text = String.Format("Ic = " + s + " {0:F2} �", this._device.Ic, CultureInfo.InvariantCulture);

            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 8, 9);
            }
            this._I0Box.Text = String.Format("I0 = " + s + " {0:F2} �", this._device.I0, CultureInfo.InvariantCulture);

            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 10, 11);
            }
            this._I1Box.Text = String.Format("I1 = " + s + " {0:F2} �", this._device.I1, CultureInfo.InvariantCulture);

            if (Version >= 1.12)
            {
                s = this.Znak(this._device.BdZnaki, 12, 13);
            }
            this._I2Box.Text = String.Format("I2 = " + s + " {0:F2} �", this._device.I2, CultureInfo.InvariantCulture);

            this._IgBox.Text = String.Format("I� = {0:F2} �", this._device.Ig);
            this._UnBox.Text = String.Format("Un = {0:F2} �", this._device.Un);
            this._UaBox.Text = String.Format("Ua = {0:F2} �", this._device.Ua);
            this._UbBox.Text = String.Format("Ub = {0:F2} �", this._device.Ub);
            this._UcBox.Text = String.Format("Uc = {0:F2} �", this._device.Uc);
            this._UabBox.Text = String.Format("Uab = {0:F2} �", this._device.Uab);
            this._UbcBox.Text = String.Format("Ubc = {0:F2} �", this._device.Ubc);
            this._UcaBox.Text = String.Format("Uca = {0:F2} �", this._device.Uca);
            this._U0Box.Text = String.Format("U0 = {0:F2} �", this._device.U0);
            this._U1Box.Text = String.Format("U1 = {0:F2} �", this._device.U1);
            this._U2Box.Text = String.Format("U2 = {0:F2} �", this._device.U2);
            this._F_Box.Text = String.Format("F = {0:F2} ��", this._device.F);
        }
        
        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._additionalLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._automationLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
        }

        private void OnDiagnosticLoadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._device.ManageSignals);
            LedManager.SetLeds(this._additionalLeds, this._device.AdditionalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._device.Indicators);
            LedManager.SetLeds(this._inputLeds, this._device.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._device.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._device.Rele);
            LedManager.SetLeds(this._limitLeds, this._device.LimitSignals);
            LedManager.SetLeds(this._automationLeds, this._device.Automation);
            LedManager.SetLeds(this._faultSignalsLeds, this._device.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._device.FaultState);
        }
        
        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void ConfirmSDTU(ushort address,string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��700", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber, this._device);
            }
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "�������� ������ �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "�������� ������ ������");
        }

        private void _resetIndicatBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1804, "�������� ���������");
        }

        private void _selectConstraintGroupBut_Click(object sender, EventArgs e)
        {
            string msg;
            bool constraintGroup;

            if (this._manageLed4.State == LedState.NoSignaled)
            {
                constraintGroup = true;
                msg = "����������� �� �������� ������";
            }
            else if (this._manageLed4.State == LedState.Signaled)
            {
                constraintGroup = false;
                msg = "����������� �� ��������� ������";
            }
            else
            {
                return;
            }

            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��700", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SelectConstraintGroup(constraintGroup);
            }
        }

        private void _dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR700); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}