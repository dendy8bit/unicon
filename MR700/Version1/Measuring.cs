using System;
using System.Globalization;
using System.Windows.Forms;
using BEMN.MBServer;

namespace BEMN.MR700.Version1
{
    public class Measuring
    {
        public static double GetI(ushort value, ushort KoeffNPTT, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return (double)((double)b * (double)value / (double)0x10000 * (double)KoeffNPTT);
        }

        public static double GetP(ushort value, ushort KoeffTTTN, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return ((double)b * (double)value / 0x10000);
        }

        public static double GetConstraintOnly(ushort value, ConstraintKoefficient koeff)
        {
            double temp = ((double)value * (int)koeff / 65535) / 100;
            return Math.Round(temp, 2);
            //return Math.Floor(temp + 0.5) / 100;
        }

        public static double GetU(ushort value, double Koeff)
        {
            return (double)((double)value / (double)0x100 * (double)Koeff);
        }

        public static double GetF(ushort value)
        {
            return (double)((double)value / (double)0x100);
        }

        public static ulong GetTime(ushort value)
        {
            return value < 32768 ? (ulong)value * 10 : ((ulong)value - 32768) * 100;
        }

        public static string ConvertVersion(string value)
        {
            string ret = "";
            for (int i = 0; i < value.Length; i++)
            {
                if (value[i] == '.')
                {
                    ret += ',';
                }
                else
                {
                    if (char.IsNumber(value[i]))
                        ret += value[i]; 
                }
            }
            return ret;
        }

        public static bool SetConsGA
        {
            get { return _setConsGA; }
            set { _setConsGA = value; }
        }

        private static bool _setConsGA = false;

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff, string version)
        {
            double ret = 0.0f;
            if (version == "123") 
            {
                if (ConstraintKoefficient.K_Undefine == koeff)
                {
                    ret = GetConstraint(value, ConstraintKoefficient.K_25600,"123");

                    if (Common.HIBYTE(value) >= 0x80)
                    {
                        ret -= 0x80;
                        ret = ret > 1 ? Math.Round(ret * 10, 2) / 10 : Math.Floor(ret * 100 + 0.5) / 100;
                        ret *= 1000;
                    }
                }
                else
                {
                    if (_setConsGA)
                    {
                        double temp = (double)value * (int)koeff / 65535;
                        return Math.Round(temp, 7) / 100;
                    }
                    else
                    {
                        double temp = (double)value * (int)koeff / 65535;
                        ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
                    }
                }
                return ret;
            }

            string vers = "0";
            try
            {
                //vers = ConvertVersion(version);
	            vers = version;
            }
            catch (Exception ll)
            {

            }
            if (vers == "")
            {
                vers = ConvertVersion("1.12");
            }

            if (ConstraintKoefficient.K_Undefine == koeff)
            {
                try
                {
                    if ((Convert.ToDouble(vers, CultureInfo.InvariantCulture) > 1.11) && (version != ""))
                    {
                        ret = GetConstraint(value, ConstraintKoefficient.K_25600, "1.12");
                        if (Common.HIBYTE(value) >= 0x80)
                        {
                            ret -= 0x80;
                            ret = ret > 1 ? Math.Floor(ret * 10 + 0.5) / 10 : Math.Floor(ret * 100 + 0.5) / 100;
                            ret *= 1000;
                        }
                    }
                    else
                    {
                        ret = GetConstraint(value, ConstraintKoefficient.K_25600, "1.11");
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                
            }
            else
            {
                double temp = (double)value * (int)koeff / 65535;
                ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            }
            return ret;
            #region �������
            //double ret = 0.0f;

            //if (ConstraintKoefficient.K_Undefine == koeff)
            //{

            //    if (Convert.ToDouble(version) > 1.11)
            //    {
            //        ret = GetConstraint(value, ConstraintKoefficient.K_25600,"1.11");
            //        if (Common.HIBYTE(value) >= 0x80)
            //            {
            //                ret -= 0x80;
            //                ret = ret > 1 ? Math.Floor(ret * 10 + 0.5) / 10 : Math.Floor(ret * 100 + 0.5) / 100;
            //                ret *= 1000;
            //            }
            //        else
            //        {
            //            double temp = (double)value * (int)koeff / 65535;
            //            ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            //        }
            //    }else
            //    {
            //        //double temp = (double)value * (int)koeff / 65535;
            //        //ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            //        ret = GetConstraint(value, ConstraintKoefficient.K_25600, "1.11");

            //    }

            //}
            //else
            //{
            //    double temp = (double)value * (int)koeff / 65535;
            //    ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            //}
            //return ret;



            //double temp = (double)value * (int)koeff / 65535;
            //return Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            //return Math.Floor(temp + 0.5) / 100;
            #endregion
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff, string version)
        {

            string vers = "0";
            try
            {
                vers = ConvertVersion(version);
            }
            catch (Exception ll)
            {

            }
            if (vers == "")
            {
                vers = ConvertVersion("1.12");
            }

            ushort ret = 0;

            if (ConstraintKoefficient.K_Undefine == koeff)
            {
                try
                {
                    double v = Convert.ToDouble(vers);
                    if (v > 1.11)
                    {
                        if (value >= 128)
                        {
                            ret = SetConstraint(value / 1000, ConstraintKoefficient.K_25600);
                            ret += 0x8000;
                        }
                        else
                        {
                            ret = SetConstraint(value, ConstraintKoefficient.K_25600);
                        }
                    }
                    else
                    {
                        ret = SetConstraint(value, ConstraintKoefficient.K_25600);
                        //ret = (ushort)(value * 65535 * 100 / (int)koeff);
                    }
                }
                catch (Exception _ex) 
                {

                }
            }
            else
            {
                ret = (ushort)(value * 65535 * 100 / (int)koeff);
            }
            return ret;
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            ushort ret = 0;
            if (ConstraintKoefficient.K_Undefine == koeff)
            {
                if (value >= 128)
                {
                    ret = SetConstraint(value / 1000, ConstraintKoefficient.K_25600);
                    ret += 0x8000;
                }
                else
                {
                    ret = SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            else
            {
                ret = (ushort)(value * 65535 * 100 / (int)koeff);
            }
            return ret;
        }

        public static ushort SetTime(ulong value)
        {
            return value < 327680 ? (ushort)(value / 10) : (ushort)(value / 100 + 32768);
        }
    }
}