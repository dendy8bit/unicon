namespace BEMN.MR700.Version1
{
    partial class SystemJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._journalProgress = new System.Windows.Forms.ProgressBar();
            this._deserializeSysJournalBut = new System.Windows.Forms.Button();
            this._serializeSysJournalBut = new System.Windows.Forms.Button();
            this._readSysJournalBut = new System.Windows.Forms.Button();
            this._sysJournalGrid = new System.Windows.Forms.DataGridView();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(111, 343);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(13, 13);
            this._journalCntLabel.TabIndex = 11;
            this._journalCntLabel.Text = "0";
            // 
            // _journalProgress
            // 
            this._journalProgress.Location = new System.Drawing.Point(6, 341);
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(100, 17);
            this._journalProgress.TabIndex = 10;
            // 
            // _deserializeSysJournalBut
            // 
            this._deserializeSysJournalBut.Location = new System.Drawing.Point(236, 309);
            this._deserializeSysJournalBut.Name = "_deserializeSysJournalBut";
            this._deserializeSysJournalBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeSysJournalBut.TabIndex = 9;
            this._deserializeSysJournalBut.Text = "��������� �� �����";
            this._deserializeSysJournalBut.UseVisualStyleBackColor = true;
            this._deserializeSysJournalBut.Click += new System.EventHandler(this._deserializeSysJournalBut_Click);
            // 
            // _serializeSysJournalBut
            // 
            this._serializeSysJournalBut.Location = new System.Drawing.Point(103, 309);
            this._serializeSysJournalBut.Name = "_serializeSysJournalBut";
            this._serializeSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._serializeSysJournalBut.TabIndex = 8;
            this._serializeSysJournalBut.Text = "��������� � ����";
            this._serializeSysJournalBut.UseVisualStyleBackColor = true;
            this._serializeSysJournalBut.Click += new System.EventHandler(this._serializeSysJournalBut_Click);
            // 
            // _readSysJournalBut
            // 
            this._readSysJournalBut.Location = new System.Drawing.Point(5, 309);
            this._readSysJournalBut.Name = "_readSysJournalBut";
            this._readSysJournalBut.Size = new System.Drawing.Size(75, 23);
            this._readSysJournalBut.TabIndex = 7;
            this._readSysJournalBut.Text = "���������";
            this._readSysJournalBut.UseVisualStyleBackColor = true;
            this._readSysJournalBut.Click += new System.EventHandler(this._readSystemJournalBut_Click);
            // 
            // _sysJournalGrid
            // 
            this._sysJournalGrid.AllowUserToAddRows = false;
            this._sysJournalGrid.AllowUserToDeleteRows = false;
            this._sysJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._sysJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._sysJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._sysJournalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._sysJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._sysJournalGrid.Name = "_sysJournalGrid";
            this._sysJournalGrid.RowHeadersVisible = false;
            this._sysJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._sysJournalGrid.Size = new System.Drawing.Size(374, 303);
            this._sysJournalGrid.TabIndex = 6;
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������ ������� ��� ��700";
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��700_�������������";
            this._saveSysJournalDlg.Filter = "��700- ������ ������� | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������� ��� ��700";
            // 
            // _indexCol
            // 
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SystemJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 364);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._journalProgress);
            this.Controls.Add(this._deserializeSysJournalBut);
            this.Controls.Add(this._serializeSysJournalBut);
            this.Controls.Add(this._readSysJournalBut);
            this.Controls.Add(this._sysJournalGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SystemJournalForm";
            this.Text = "SystemJournalForm";
            this.Activated += new System.EventHandler(this.SystemJournalForm_Activated);
            this.Deactivate += new System.EventHandler(this.SystemJournalForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemJournalForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.ProgressBar _journalProgress;
        private System.Windows.Forms.Button _deserializeSysJournalBut;
        private System.Windows.Forms.Button _serializeSysJournalBut;
        private System.Windows.Forms.Button _readSysJournalBut;
        private System.Windows.Forms.DataGridView _sysJournalGrid;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
    }
}