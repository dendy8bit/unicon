using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR700.Version1
{
    public partial class SystemJournalForm : Form , IFormView
    {
        private MR700 _device;

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR700 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.SystemJournalRecordLoadOk += this._device_SystemJournalRecordLoadOk;
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnSysJournalIndexLoadOk), index);
            }
            catch (InvalidOperationException)
            {}
        }

        private void OnSysJournalIndexLoadOk(int i)
        {            
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            this._sysJournalGrid.Rows.Add(new object[] { i + 1, this._device.SystemJournal[i].time, this._device.SystemJournal[i].msg });
            if (i == 127)
            {
                this._readSysJournalBut.Enabled = true;
            }
        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR700); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.js;
            }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readSystemJournalBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readSysJournalBut.Enabled = false;
            this._device.SystemJournal = new MR700.CSystemJournal();
            this._device.RemoveSystemJournal();
            this._journalProgress.Value = 0;
            this._journalProgress.Maximum = MR700.SYSTEMJOURNAL_RECORD_CNT;
            this._sysJournalGrid.Rows.Clear();
            this._device.LoadSystemJournal();
        }
        
        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��700_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(new object[]{this._sysJournalGrid["_indexCol", i].Value,
                                            this._sysJournalGrid["_timeCol", i].Value,
                                            this._sysJournalGrid["_msgCol", i].Value});
            }

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {            
            DataTable table = new DataTable("��700_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            try
            {
                if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
                {
                    this._sysJournalGrid.Rows.Clear();
                    table.ReadXml(this._openSysJounralDlg.FileName);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("���� �������� ���� ���������", "������ ������ �����", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(new object[]{table.Rows[i].ItemArray[0],
                                                      table.Rows[i].ItemArray[1],
                                                      table.Rows[i].ItemArray[2]});
            }
        }

        private void SystemJournalForm_Activated(object sender, EventArgs e)
        {
            this._device.SuspendSystemJournal(false);
        }

        private void SystemJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._device.SuspendSystemJournal(true);
        }

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveSystemJournal();
        }
    }
}