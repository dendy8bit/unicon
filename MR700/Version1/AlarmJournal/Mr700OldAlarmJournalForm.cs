﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR700.Version1.AlarmJournal.Structures;
using BEMN.MR700.Version2.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR700.Version1.AlarmJournal
{
    public partial class Mr700OldAlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР700_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string PARAMETERS_LOADED = "Параметры загружены";
        private const string FAULT_LOAD_PARAMETERS = "Невозможно загрузить параметры";
        private const string RECORDS_IN_JOURNAL_PATTERN = "В журнале {0} сообщений";
        #endregion [Constants]


        #region [Private fields]
        private DataTable _table;
        private readonly MemoryEntity<OldAlarmJournalStruct> _alarmRecord;
        private readonly MemoryEntity<MeasureTransStruct> _measuringChannel;
        private MR700 _device;
        #endregion [Private fields]
        
        #region [Ctor's]
        public Mr700OldAlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr700OldAlarmJournalForm(MR700 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._alarmRecord = device.Mr700DeviceV2Prop.OldJA;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);

            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.Mr700DeviceV2Prop.MeasureTransAj;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);
            
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            this._configProgressBar.Maximum = this._alarmRecord.Slots.Count;
        } 
        #endregion [Ctor's]
        
        #region [Help members]
        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }
        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
            this.Reading = false;
        }
        private void JournalReadFail()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.Reading = false;
        }

        private void ReadJournalOk()
        {
            int i = 1;
            MeasureTransStruct measure = this._measuringChannel.Value;
            double version = Common.VersionConverter(this._device.DeviceVersion);
            foreach (OldAlarmJournalRecordStruct record in this._alarmRecord.Value.Records)
            {
                if (record.IsEmpty)
                {
                    continue;
                }
              
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Stage,
                        record.WorkParametr,
                        record.ValueParametr(measure),
                        record.GroupOfSetpoints(version),
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetI(record.In, measure.Ttnp * 5),
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.Ttnp * 5),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetU(record.Uab,measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.Tnnp),
                        record.Discret
                    );
                i++;
            }
            this._alarmJournalGrid.DataSource = this._table;
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this._alarmJournalGrid.Update();
            this.Reading = false;
        }
   
        private DataTable GetJournalDataTable()
         {
             DataTable table = new DataTable(TABLE_NAME);
             for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
             {
                 table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
             }
             return table;
         }
        #endregion [Help members]

        #region [Event Handlers]
        private void Mr700AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this.Reading = true;
            this._measuringChannel.LoadStruct();
        }


        private bool Reading
        {
            set
            {
                this._readAlarmJournalButton.Enabled = !value;
                this._saveAlarmJournalButton.Enabled = !value;
                this._exportButton.Enabled = !value;
                this._loadAlarmJournalButton.Enabled =! value;
            }
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }
        
        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR700AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
        #endregion [Event Handlers]

        


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR700); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr700OldAlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]
    }
}
