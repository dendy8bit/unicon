<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
   <xsl:choose>
  <!--111111111111111111����� ������!111111111111111111111111111111-->
  <xsl:when test="MR700/DeviceVersion !=' ' ">
    <h3>���������� <xsl:value-of select="MR700/DeviceType"/> ������ �� <xsl:value-of select="MR700/DeviceVersion"/></h3>
    <b>������������ �������� ����</b>
    <table border="1">
      <tr bgcolor="#c1ced5">
         <th>����� ����</th>
         <th>���</th>
         <th>������</th>
         <th>�������, ��</th>
      </tr>
      <xsl:for-each select="MR700/��������_����">
      <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="@�������"/></td>
      </tr>
    </xsl:for-each>
    </table>
<p></p>
    <b>������������ ������������ �����������</b> 
    <table border="1">    
      <tr bgcolor="#c1ced5">
         <th>����� ����������</th>
         <th>���</th>
         <th>������</th>
         <th>����� �����. �� ���. ���.</th>
         <th>����� �����. �� ��</th>
         <th>����� �����. �� ��</th>
      </tr>
     <xsl:for-each select="MR700/��������_����������">
        <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <xsl:element name="td">
        <xsl:attribute name="id">indexing_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���������"/>,"indexing_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
      <xsl:element name="td">
        <xsl:attribute name="id">alarm_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@������"/>,"alarm_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
         <xsl:element name="td">
        <xsl:attribute name="id">sys_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@�������"/>,"sys_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
        </tr>
      </xsl:for-each>
     </table>
<p></p>
    <h3>������������ ������������� ��������������� � ���</h3> 
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>I� ���. �������. ��� ��, �</th>
         <th>���. �������. ��� ����, �</th>
         <th>Imax ����. ���, I�</th>
         <th>����� ���</th>
         <th>X�� �����, ��/��</th>
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/��"/></td>
         <td><xsl:value-of select="MR700/����"/></td>
         <td><xsl:value-of select="MR700/������������_���"/></td>
         <td><xsl:value-of select="MR700/���_���"/></td>
         <td><xsl:value-of select="MR700/���"/></td>
      </tr>
     </table>
    <p></p>
     <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>����������� ��</th>
         <th>������������� ��</th>
         <th>����������� ����</th>
         <th>������������� ����</th>
         <th>��� ��</th>
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/��"/></td>
         <td><xsl:value-of select="MR700/�������������_��"/></td>
         <td><xsl:value-of select="MR700/����"/></td>
         <td><xsl:value-of select="MR700/�������������_����"/></td>
         <td><xsl:value-of select="MR700/���_��"/></td>
      </tr>
     </table>
<p></p>
<h3>������������ ������������</h3> 
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>���������� ������������</th>
        
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/OscilloscopeKonfCount"/></td>
      </tr>
     </table>
<p></p>
<h3>������������ ���� �������������</h3> 
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>�������</th>
        
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/�������_�������������"/></td>
      </tr>
     </table>
<p></p>
<h3>������������ ������� ��������</h3> 
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>���� ��������</th>
         <th>���� ���������</th>
         <th>������� ��������</th>
         <th>������� ���������</th>
         <th>����� ���������</th>
         <th>������� �� ���.�������</th>
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/����_���������"/></td>
         <td><xsl:value-of select="MR700/����_��������"/></td>
         <td><xsl:value-of select="MR700/�������_��������"/></td>
         <td><xsl:value-of select="MR700/�������_���������"/></td>
         <td><xsl:value-of select="MR700/�����_������������"/></td>
         <td><xsl:value-of select="MR700/������������_�������"/></td>
      </tr>
     </table>
<p></p>
<h3>������������ ���������� ����������</h3>
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>�� �����</th>
         <th>�� ������</th>
         <th>�������</th>
         <th>�� ����</th>
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/������_��_�����"/></td>
         <td><xsl:value-of select="MR700/������_��_������"/></td>
         <td><xsl:value-of select="MR700/������_��_��������"/></td>
         <td><xsl:value-of select="MR700/������_��_����"/></td>
      </tr>
     </table>
<p></p>
<h3>������������ ���������� �����������</h3>
    <table border="1">    
      <tr bgcolor="#ced5c1">
         <th>�������</th>
         <th>��������</th>
         <th>�������������</th>
         <th>����-�� ���������</th>
         <th>T����, ��</th>
         <th>I����, I�</th>
         <th>������� ��, ��</th>
         <th>����. ���������, ��</th>

      </tr>
      <tr>
         <td><xsl:value-of select="MR700/�����������_��������"/></td>
         <td><xsl:value-of select="MR700/�����������_���������"/></td>
         <td><xsl:value-of select="MR700/�����������_������"/></td>
         <td><xsl:value-of select="MR700/�����������_����������"/></td>
         <td><xsl:value-of select="MR700/�����������_�����_����"/></td>
         <td><xsl:value-of select="MR700/�����������_���_����"/></td>
         <td><xsl:value-of select="MR700/�����������_�������"/></td>
         <td><xsl:value-of select="MR700/�����������_������������"/></td>

      </tr>
     </table>

<h3>������������ ����������</h3>
<h4>������������ ���</h4>
    <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>������������ ���</th>
         <th>����. ����-��</th>
         <th>� ����-�� ����� ������� ���-�, ��</th>
         <th>T�����, ��</th>
         <th>T1����, ��</th>
         <th>T2����, ��</th>
         <th>T3����, ��</th>
         <th>T4����, ��</th>
         <th>��� �� ����������. ����-�</th>
      </tr>
      <tr>
         <td><xsl:value-of select="MR700/������������_���"/></td>
         <td><xsl:value-of select="MR700/����������_���"/></td>
         <td><xsl:value-of select="MR700/�����_����������_���"/></td>
         <td><xsl:value-of select="MR700/�����_����������_���"/></td>
         <td><xsl:value-of select="MR700/�����_1_�����_���"/></td>
         <td><xsl:value-of select="MR700/�����_2_�����_���"/></td>
         <td><xsl:value-of select="MR700/�����_3_�����_���"/></td>
         <td><xsl:value-of select="MR700/�����_4_�����_���"/></td>
         <td><xsl:value-of select="MR700/������_���_��_��������������"/></td>
      </tr>
     </table>
<p></p>
<h4>������������ ���</h4>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>���� ��� �� ����.�������</th>
         <th>���� ��� �� ����.���. ����-�</th>
         <th>���� ��� �� ����������. ����-�</th>
         <th>���� ��� �� ������</th>
         <th>������ ����-��</th>
         <th>������ ������ ����-��</th>
         <th>������. ������ ����-�� �� ����.���-�</th>
         <th>������ �����</th>
         <th>������ ����������</th>
         <th>�����, ��</th>
         <th>������ ��������</th>
         <th>������, ��</th>
         <th>�����, ��</th>
      </tr>
      <tr>
         <td id="td1"><script>translateBoolean(<xsl:value-of select="MR700/����������_�������_���"/>,"td1");</script></td>
         <td id="td2"><script>translateBoolean(<xsl:value-of select="MR700/�������_����������_�����������_���"/>,"td2");</script></td>
         <td id="td3"><script>translateBoolean(<xsl:value-of select="MR700/��������������_���"/>,"td3");</script></td>
         <td id="td4"><script>translateBoolean(<xsl:value-of select="MR700/������������_������_���"/>,"td4");</script></td>
         <td><xsl:value-of select="MR700/����������_���"/></td>
         <td><xsl:value-of select="MR700/�����_����������_���"/></td>
         <td id="td5"><script>translateBoolean(<xsl:value-of select="MR700/����������_������_���_��_���������_�����������"/>,"td5");</script></td>
         <td><xsl:value-of select="MR700/������_���"/></td>
         <td><xsl:value-of select="MR700/������������_���"/></td>
         <td><xsl:value-of select="MR700/�����_������������_���"/></td>
         <td><xsl:value-of select="MR700/�������_���"/></td>
         <td><xsl:value-of select="MR700/�����_��������_���"/></td>
         <td><xsl:value-of select="MR700/�����_����������_���"/></td>
      </tr>
     </table>
<p></p>
<h4>������������ ���</h4>
     <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>������������ ���</th>
         <th>������� �� ���� ���, I�</th>
      </tr>
      <tr>
         <td><xsl:if test="MR700/DeviceVersion &lt;1.14"><xsl:value-of select="MR700/������������_���"/></xsl:if><xsl:if test="MR700/DeviceVersion &gt;=1.14"><xsl:value-of select="MR700/������������_���114"/></xsl:if></td>
         <td><xsl:value-of select="MR700/�������_���"/></td>
      </tr>
     </table>
<p></p>
    <b>������������ ������� �����</b> 
     <table border="1">    
      <tr bgcolor="#bcbcbc">
         <th>����� ��</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� ����.</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� �� ��������</th>
         <th>���� �����.</th>
         <th>������, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
         <th>�����</th>
      </tr>
      <xsl:for-each select="MR700/�������_������">
       <tr>
          <td><xsl:value-of select="position()"/></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion =1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.12"><xsl:value-of select="@�����_v1.12"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
         <xsl:element name="td">
             <xsl:attribute name="id">vozvratVZ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratVZ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratVZ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratVZ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@�������_�����"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>
 
         <xsl:element name="td">
             <xsl:attribute name="id">urovVZ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"urovVZ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvVZ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvVZ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrVZ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrVZ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">sbrosVZ_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�����"/>,"sbrosVZ_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
<h3>������������ �����</h3>
<h4>������������ ������� �����. �������� �������</h4>
     <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR700/�������_������_��������">
       <tr>
          <td><xsl:if test="position() &lt;=4">I><xsl:value-of select="position()"/></xsl:if><xsl:if test="position() =5">I2></xsl:if><xsl:if test="position() =6">I2>></xsl:if><xsl:if test="position() =7">I0></xsl:if><xsl:if test="position() =8">I0>></xsl:if><xsl:if test="position() =9">In></xsl:if><xsl:if test="position() =10">In>></xsl:if><xsl:if test="position() =11">Ig></xsl:if><xsl:if test="position() =12">I2/I1</xsl:if></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">puskpoU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����_��_U"/>,"puskpoU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@����_��_U_�������"/></td>
         <td><xsl:value-of select="@�����������"/></td>
         <td><xsl:value-of select="@����������_��������"/></td>
         <td><xsl:value-of select="@��������"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@��������������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
         <xsl:element name="td">
             <xsl:attribute name="id">Uskorenie_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenie_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@���������_�����"/></td>



         <xsl:element name="td">
             <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
 <p>
<h4>������������ ����� �� ����������. �������� �������</h4>
<table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>

         <th>����</th>
         <th>���</th>
         <th>���</th>
        <xsl:if test="MR700/DeviceVersion &gt;=1.11">
        <th>����-�� Uabc ���� 5�</th>
        <th>�����</th>        
        </xsl:if>
      </tr>
      <xsl:for-each select="MR700/������_����������_��������">
       <tr>
          <td><xsl:if test="position() =1">U></xsl:if><xsl:if test="position() =2">U>></xsl:if><xsl:if test="position() =3">Umin1</xsl:if><xsl:if test="position() =4">Umin2</xsl:if><xsl:if test="position() =5">U2></xsl:if><xsl:if test="position() =6">U2>></xsl:if><xsl:if test="position() =7">Un></xsl:if><xsl:if test="position() =8">Un>></xsl:if></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@��������"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@��������_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">vozvratU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	 <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���_�������"/>,"APVvozvratU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@�������_�������"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>

         <xsl:element name="td">
             <xsl:attribute name="id">UROVU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:if test="../DeviceVersion &gt;=1.11">
         <td><xsl:value-of select="@����������_v1.11"/></td>
         <xsl:element name="td">
             <xsl:attribute name="id">resetU_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@Reset"/>,"resetU_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
        </xsl:if>
      </tr>
      </xsl:for-each>
      </table>
 </p>
 <p>
<h4>������������ ����� �� �������. �������� �������</h4>
<table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>F����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>

         <th>����</th>
         <th>���</th>
         <th>���</th>
        <xsl:if test="MR700/DeviceVersion &gt;=1.11">
        <th>�����</th>        
        </xsl:if>
      </tr>
      <xsl:for-each select="MR700/������_��_�������_��������">
       <tr>
          <td><xsl:if test="position() =1">F></xsl:if><xsl:if test="position() =2">F>></xsl:if><xsl:if test="position() =3">Fmin1</xsl:if><xsl:if test="position() =4">Fmin2</xsl:if></td>
          <td><xsl:if test="../DeviceVersion =1.10"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion &lt;1.1"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion =1.1"><xsl:value-of select="@�����_v1.1"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">vozvratF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	 <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@������������_���"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>

         <xsl:element name="td">
             <xsl:attribute name="id">UROVF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:if test="../DeviceVersion &gt;=1.11">
         <xsl:element name="td">
             <xsl:attribute name="id">resetF_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@Reset"/>,"resetF_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
        </xsl:if>
      </tr>
      </xsl:for-each>
      </table>
 </p> 
<p>
<h4>������������ ������� �����. ��������� �������</h4>
    <table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>���� �� U</th>
         <th>U��c�, �</th>
         <th>�������.</th>
         <th>��� ������. ���.�������.</th>
         <th>��������</th>
         <th>I����, I�</th>
         <th>���-�� ����.</th>
         <th>T����, ��/����.</th>
         <th>�����.</th>
         <th>T�����, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
      </tr>
      <xsl:for-each select="MR700/�������_������_���������">
       <tr>
          <td><xsl:if test="position() &lt;=4">I><xsl:value-of select="position()"/></xsl:if><xsl:if test="position() =5">I2></xsl:if><xsl:if test="position() =6">I2>></xsl:if><xsl:if test="position() =7">I0></xsl:if><xsl:if test="position() =8">I0>></xsl:if><xsl:if test="position() =9">In></xsl:if><xsl:if test="position() =10">In>></xsl:if><xsl:if test="position() =11">Ig></xsl:if><xsl:if test="position() =12">I2/I1</xsl:if></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">puskpoUrez_1<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����_��_U"/>,"puskpoUrez_1<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@����_��_U_�������"/></td>
         <td><xsl:value-of select="@�����������"/></td>
         <td><xsl:value-of select="@����������_��������"/></td>
         <td><xsl:value-of select="@��������"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@��������������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 
 <xsl:element name="td">
             <xsl:attribute name="id">Uskorenierez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenierez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@���������_�����"/></td>
  <xsl:element name="td">
             <xsl:attribute name="id">urovIrez_1<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"urovIrez_1<xsl:value-of select="position()"/>");</script>
         </xsl:element>


         <xsl:element name="td">
             <xsl:attribute name="id">apvIrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvIrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
           <xsl:element name="td">
             <xsl:attribute name="id">avrIrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrIrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
      </tr>
      </xsl:for-each>
      </table>
 </p>
<h4>������������ ����� �� ����������. ��������� �������</h4>
<table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>

         <th>����</th>
         <th>���</th>
         <th>���</th>
        <xsl:if test="MR700/DeviceVersion &gt;=1.11">
        <th>����-�� Uabc ���� 5�</th>
        <th>�����</th>        
        </xsl:if>
      </tr>
      <xsl:for-each select="MR700/������_����������_���������">
       <tr>
          <td><xsl:if test="position() =1">U></xsl:if><xsl:if test="position() =2">U>></xsl:if><xsl:if test="position() =3">Umin1</xsl:if><xsl:if test="position() =4">Umin2</xsl:if><xsl:if test="position() =5">U2></xsl:if><xsl:if test="position() =6">U2>></xsl:if><xsl:if test="position() =7">Un></xsl:if><xsl:if test="position() =8">Un>></xsl:if></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
          <td><xsl:if test="../DeviceVersion &lt;1.11"><xsl:value-of select="@��������"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@��������_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">vozvratUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	 <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���_�������"/>,"APVvozvratUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@�������_�������"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>

         <xsl:element name="td">
             <xsl:attribute name="id">UROVUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:if test="../DeviceVersion &gt;=1.11">
         <td><xsl:value-of select="@����������_v1.11"/></td>
         <xsl:element name="td">
             <xsl:attribute name="id">resetUrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@Reset"/>,"resetUrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
        </xsl:if>
      </tr>
      </xsl:for-each>
      </table>
<h4>������������ ����� �� �������. ��������� �������</h4>
<table border="1">    
      <tr bgcolor="#d0d0d0">
         <th>C������ ���.������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>F����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>

         <th>����</th>
         <th>���</th>
         <th>���</th>
        <xsl:if test="MR700/DeviceVersion &gt;=1.11">
        <th>�����</th>        
        </xsl:if>
      </tr>
      <xsl:for-each select="MR700/������_��_�������_���������">
       <tr>
          <td><xsl:if test="position() =1">F></xsl:if><xsl:if test="position() =2">F>></xsl:if><xsl:if test="position() =3">Fmin1</xsl:if><xsl:if test="position() =4">Fmin2</xsl:if></td>
          <td><xsl:if test="../DeviceVersion &lt;1.1"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion =1.10"><xsl:value-of select="@�����"/></xsl:if><xsl:if test="../DeviceVersion =1.1"><xsl:value-of select="@�����_v1.1"/></xsl:if><xsl:if test="../DeviceVersion &gt;=1.11"><xsl:value-of select="@�����_v1.11"/></xsl:if></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@������������_�����"/></td>
 	 <xsl:element name="td">
             <xsl:attribute name="id">vozvratFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
 	 <xsl:element name="td">
             <xsl:attribute name="id">APVvozvratFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <td><xsl:value-of select="@������������_���"/></td>
         <td><xsl:value-of select="@�������_�����"/></td>

         <xsl:element name="td">
             <xsl:attribute name="id">UROVFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">apvFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"apvFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:element name="td">
             <xsl:attribute name="id">avrFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@���"/>,"avrFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
         <xsl:if test="../DeviceVersion &gt;=1.11">
         <xsl:element name="td">
             <xsl:attribute name="id">resetFrez_<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="@Reset"/>,"resetFrez_<xsl:value-of select="position()"/>");</script>
         </xsl:element>
        </xsl:if>
      </tr>
      </xsl:for-each>
      </table>
	  </xsl:when>
  <!-- 20 � ������--> 
<xsl:otherwise>
		<h2>���������� <xsl:value-of select="��700/���_����������"/>. ������ �� <xsl:value-of select="��700/������"/>. ����� <xsl:value-of select="��700/�����_����������"/> </h2>

		<!--�������  �������-->
		   <h3><b>������� ������� </b></h3>
   
   <b>��������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>��� ��</th>
         <th>Im, In</th>
         <th>ITT�, A</th>
         <th>ITTn, A</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��700/�������������_�������������/���_��"/></td>
         <td><xsl:value-of select="��700/�������������_�������������/I�"/></td>
         <td><xsl:value-of select="��700/�������������_�������������/������������_��"/></td>
         <td><xsl:value-of select="��700/�������������_�������������/������������_����"/></td>
      </tr>

    </table>
	<p></p>
 <b>��� ��</b>
    <table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>U0</th>
         <th>KTH�</th>
         <th>������.���</th>
         <th>KTHn</th>
		 <th>������.��n</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��700/�������������_�������������/���_Uo"/></td>
         <td><xsl:value-of select="��700/�������������_�������������/��"/></td>
         <td><xsl:value-of select="��700/�������������_�������������/�������������_��"/></td>
		 <td><xsl:value-of select="��700/�������������_�������������/����"/></td>
         <td><xsl:value-of select="��700/�������������_�������������/�������������_����"/></td>
	  </tr>

    </table>
	<p></p>
	<b>����������� ����� �����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>�����</th>
         <th>X �����, ��/��</th>
      </tr>
     
      <tr align="center">
		<td><xsl:for-each select="��700/�������������_�������������/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
         <td><xsl:value-of select="��700/�������������_�������������/Xyd"/></td>
       </tr>
    </table>
	<p></p>
	<b>������� �������</b>
    <table border="1" cellspacing="0">

	
      <tr bgcolor="FFFFCC">
         <th>���� ���������</th>
         <th>���� ��������</th>
		 <th>������� ���������</th>
         <th>������� ��������</th>
         <th>����� ������������</th>
         <th>������ �������</th>		 
      </tr>
     
      <tr align="center">
		<td><xsl:value-of select="��700/�������_�������/����_����"/></td>
        <td><xsl:value-of select="��700/�������_�������/����_���"/></td>
		<td><xsl:value-of select="��700/�������_�������/����_����_���������"/></td>
        <td><xsl:value-of select="��700/�������_�������/����_����_��������"/></td>
		<td><xsl:value-of select="��700/�������_�������/�����_������������"/></td>
        <td><xsl:value-of select="��700/�������_�������/������_�������"/></td>
       </tr>
    </table>
	<p></p>
	<b>������������ �����������</b>
	<table border="1" cellspacing="0">
<td>
<b>������� ����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>�� ������</th>
         <th>�� �����</th>
		 <th>�������</th>
         <th>����</th>
        	 
      </tr>
     
      <tr align="center">
		<td><xsl:value-of select="��700/������������_�����������/����"/></td>
        <td><xsl:value-of select="��700/������������_�����������/����"/></td>
		<td><xsl:value-of select="��700/������������_�����������/�������"/></td>
        <td><xsl:value-of select="��700/������������_�����������/����"/></td>
       </tr>
    </table>
	
	<b>�����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>��������� ���������</th>
         <th>��������� ��������</th>
		 <th>�������������</th>
         <th>����������</th>
		 <th>����� ����, ��</th>
         <th>��� ����, In</th>
		 <th>������� ��, ��</th>
         <th>����. �����, ��</th>
        	 
      </tr>
     
      <tr align="center">
		<td><xsl:value-of select="��700/������������_�����������/���������"/></td>
        <td><xsl:value-of select="��700/������������_�����������/��������"/></td>
		<td><xsl:value-of select="��700/������������_�����������/������"/></td>
        <td><xsl:value-of select="��700/������������_�����������/����������"/></td>
		<td><xsl:value-of select="��700/������������_�����������/t����"/></td>
        <td><xsl:value-of select="��700/������������_�����������/���_����"/></td>
		<td><xsl:value-of select="��700/������������_�����������/�������"/></td>
        <td><xsl:value-of select="��700/������������_�����������/���������"/></td>
       </tr>
    </table>	
	</td>
	</table>
	<p></p>
	<b>���� �������������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
         <th>�������, ��</th>
         <th>����������</th>
		 <th>���</th>
         <th>��</th>
		 <th>�����������</th>
		 <th>����������</th>
		 <th>�������</th>
        	 
      </tr>
     
      <tr align="center">
		<td><xsl:value-of select="��700/����_�������������/@�������_����_�������������"/></td>
			<xsl:for-each select="��700/����_�������������/Signals/�������������">
			 <xsl:if test="position()!= 4 " >
			  <xsl:if test="position()!= 6 " >	
				<td><xsl:value-of select="current()"/></td>
				
			   </xsl:if>
			 </xsl:if>  
			</xsl:for-each>
       </tr>
    </table>
	<p></p>
	<b>������� ���������� �������</b>
	<table border="1" cellspacing="0">
	<td>
	<b>���������� ������� �</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="��700/�������_����������_�������/��">  
     <xsl:if test="position() &lt; 5 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	
		<b>���������� ������� ���</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="��700/�������_����������_�������/��">  
     <xsl:if test="position() &gt; 4 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	</td>
	</table>
	<p></p>
	<b>�����</b>&#32;
			<xsl:for-each select="��700/�����/����">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					<xsl:value-of select="position()"/>&#32;
					</xsl:if>
					
				</xsl:if>
			 </xsl:for-each>
	<p></p>	
    <b>�����������</b>
    <table border="1" cellspacing="0">
	  <tr bgcolor="FFFFCC">
         <th>������������ ������� ���.</th>
         <th>�������� ���.</th>
         <th>����. ���������� ���.</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��700/���/����������_�����������"/></td>
         <td><xsl:value-of select="��700/���/��������"/></td>
         <td><xsl:value-of select="��700/���/����������"/></td>

      </tr>
	</table>
	
<!-- |||||||||||||||||||||||||||�������� �������||||||||||||||||||||||||||||||||||||||||||| -->	
   <h3><b>�������� ������� </b></h3>
   <b>�������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>���</th>
         <th>������</th>
         <th>�����, ��</th>
      </tr>
     
      
		 <xsl:for-each select="��700/����/���_����/����_����">
		 <xsl:if test="position() &lt; 9 ">
				
		 <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="@�����"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
    </table>  
	<p></p>
	
  
	   <b>����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>���</th>
         <th>������</th>
        <th>����� ���</th>
        <th>����� ��</th>
        <th>����� ��</th>
        
      </tr>
		 <xsl:for-each select="��700/����������/���_����������/����_���������">
		 <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:for-each select="@�����_���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
         <td><xsl:for-each select="@�����_��"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>  
         <td><xsl:for-each select="@�����_��"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>  
        </tr>
       
		 </xsl:for-each>
    </table>   
	<p></p>
	
	
    <b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>������������</th>
      </tr>
     
      
		 <xsl:for-each select="��700/���_���/���">
		 <tr>
         <td><xsl:value-of select="position()"/></td>
		 <td>
		 <xsl:for-each select="�������">
         <xsl:value-of select="current()"/>|
         </xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
	 </table>   

 <!-- |||||||||||||||||||||||||||���������� � ����������||||||||||||||||||||||||||||||||||||||||||| -->	
 <h3><b>���������� � ����������</b></h3>
  
   <b>���</b>
		    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC">
         <th>�����</th>
         <th>�������</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��700/���/�����"/></td>
         <td><xsl:value-of select="��700/���/�������_���"/></td>
		 </tr>
    </table> 
		<p></p>
  <b>���</b>
		    <table border="1" cellspacing="0">
			
      <tr bgcolor="9966CC">
         <th>�����</th>
         <th>����������</th>
		 <th>t ����, ��</th>
		 <th>t �����, ��</th>
		 <th>1 ����</th>
		 <th>2 ����</th>
		 <th>3 ����</th>
		 <th>4 ����</th>
		 <th>��������������</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��700/���/�����"/></td>
         <td><xsl:value-of select="��700/���/����_����������_���"/></td>
		 <td><xsl:value-of select="��700/���/�����_����������_���"/></td>
		 <td><xsl:value-of select="��700/���/�����_����������_���"/></td>
		 <td><xsl:value-of select="��700/���/����1"/></td>
		 <td><xsl:value-of select="��700/���/����2"/></td>
		 <td><xsl:value-of select="��700/���/����3"/></td>
		 <td><xsl:value-of select="��700/���/����4"/></td>
		 <td><xsl:for-each select="��700/���/������_���_��_�����������������_����������_�����������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 
		 </tr>
    </table> 	
   	<p></p>
   <b>���</b>
		    <table border="1" cellspacing="0">			
			
      <tr bgcolor="9966CC">
         <th>�� �������</th>
         <th>�� ����������</th>
		 <th>�� ��������.</th>
		 <th>�� ������</th>
		 <th>����. ����</th>
		 <th>����������</th>
		 <th>�����</th>
		 <th>��� ������.</th>
		 <th>t ��, ��</th>
		 <th>�������</th>
		 <th>t ���, ��</th>
		 <th>t ����, ��</th>
		 <th>�����</th>
      </tr>
     
 		  <tr>
		  <td><xsl:for-each select="��700/���/��_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		  <td><xsl:for-each select="��700/���/��_����������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		  <td><xsl:for-each select="��700/���/��_��������������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		  <td><xsl:for-each select="��700/���/��_������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 <td><xsl:value-of select="��700/���/����"/></td>
		 <td><xsl:value-of select="��700/���/����_����������_���"/></td>
		 <td><xsl:value-of select="��700/���/����_�����_����������_���"/></td>
		 <td><xsl:value-of select="��700/���/����_���_������������"/></td>
		 <td><xsl:value-of select="��700/���/�����_���_������������"/></td>
		 <td><xsl:value-of select="��700/���/����_���_�������"/></td>
		 <td><xsl:value-of select="��700/���/�����_���_�������"/></td>
		 <td><xsl:value-of select="��700/���/��������_����������_�������"/></td>
		  <td><xsl:for-each select="��700/���/�����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
    </table>	

	
<!-- |||||||||||||||||||||||||||������||||||||||||||||||||||||||||||||||||||||||| -->  
<h3><b>������� ������</b></h3>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
          <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>������������</th>
		 <th>����� ����., ��</th>
		 <th>�������</th>
		 <th>��� ��</th>
		 <th>���� ��</th>
		 <th>����� ��������, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		 <th>�����</th>
		</tr>
		 <xsl:for-each select="��700/�������/���/DefenseExternalStruct">
		 <tr align="center">
		    <td>��- <xsl:value-of select="position()"/> </td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="����_������������"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����_��"/></center></td>			
			<td><center><xsl:value-of select="�������_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
		 
		</table>
  

 <h2><b>������. �������� ������ �������</b></h2>
<h3><b>���� ��</b></h3>
		<table border="1" cellspacing="0" >
		<tr bgcolor="FFCC66">
         <th>I</th>
         <th>In</th>
		 <th>I0</th>
         <th>I2</th>
		</tr>
     
 		  <tr>
         <td><xsl:value-of select="��700/�������/��������/����/I"/></td>
         <td><xsl:value-of select="��700/�������/��������/����/In"/></td>
		 <td><xsl:value-of select="��700/�������/��������/����/I0"/></td>
		 <td><xsl:value-of select="��700/�������/��������/����/I2"/></td>
		 </tr>
		</table> 
  
<h3><b>������ I</b></h3>
		<table border="1" cellspacing="0">
		<td>
		<b>������ I> ������������� ����</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>���� �� U</th>
		 <th>U����, �</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>��������</th>
		 <th>���. ������������, In</th>
		 <th>��������������</th>
		 <th>t����, ��/����</th>
		 <th>���������</th>
		 <th>t���, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/�������/��������/��������_�������/Rows/CurrentDefenseStruct">
		 <xsl:if test="position() &lt; 5">
		 <tr align="center">
			<td>I&#62;  <xsl:value-of select="position()"/></td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:for-each select="Ustart"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="Uustavka"/></td>
			<td><xsl:value-of select="�����������"/></td>
			<td><xsl:value-of select="�����������_����������"/></td>
			<td><xsl:value-of select="��������_1"/></td>
			<td><xsl:value-of select="�������_������������"/></td>
			<td><xsl:value-of select="��������������"/></td>
			<td><xsl:value-of select="�����_������������"/></td>
			<td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_���������"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
			<p></p>	
				<b>������ I2</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>���� �� U</th>
		 <th>U����, �</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>��������</th>
		 <th>���. ������������, In</th>
		 <th>t����, ��/����</th>
		 <th>���������</th>
		 <th>t���, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/�������/��������/��������_�������/Rows/CurrentDefenseStruct">
		 <xsl:if test="position() &gt; 4">
			<xsl:if test="position() &lt; 7">
		 <tr align="center">
				<xsl:if test="position() = 5">	<td>I2&#62; </td></xsl:if>
				<xsl:if test="position() = 6">	<td>I2&#62;&#62; </td></xsl:if>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:for-each select="Ustart"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="Uustavka"/></td>
			<td><xsl:value-of select="�����������"/></td>
			<td><xsl:value-of select="�����������_����������"/></td>
			<td><xsl:value-of select="��������_2"/></td>
			<td><xsl:value-of select="�������_������������"/></td>
			<td><xsl:value-of select="�����_������������"/></td>
			<td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_���������"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		</table>
			<p></p>	
						<b>������ I0</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>���� �� U</th>
		 <th>U����, �</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>��������</th>
		 <th>���. ������������, In</th>
		 <th>t����, ��/����</th>
		 <th>���������</th>
		 <th>t���, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/�������/��������/��������_�������/Rows/CurrentDefenseStruct">
		 <xsl:if test="position() &gt; 6">
			<xsl:if test="position() &lt; 9">
		 <tr align="center">
				<xsl:if test="position() = 7">	<td>I0&#62; </td></xsl:if>
				<xsl:if test="position() = 8">	<td>I0&#62;&#62; </td></xsl:if>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:for-each select="Ustart"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="Uustavka"/></td>
			<td><xsl:value-of select="�����������"/></td>
			<td><xsl:value-of select="�����������_����������"/></td>
			<td><xsl:value-of select="��������_2"/></td>
			<td><xsl:value-of select="�������_������������"/></td>
			<td><xsl:value-of select="�����_������������"/></td>
			<td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_���������"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		<p></p>
						<b>������ In</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>���� �� U</th>
		 <th>U����, �</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>��������</th>
		 <th>���. ������������, In</th>
		 <th>t����, ��/����</th>
		 <th>���������</th>
		 <th>t���, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/�������/��������/��������_�������/Rows/CurrentDefenseStruct">
		 <xsl:if test="position() &gt; 8">
			<xsl:if test="position() &lt; 11">
		 <tr align="center">
				<xsl:if test="position() = 9">	<td>In&#62; </td></xsl:if>
				<xsl:if test="position() = 10">	<td>In&#62;&#62; </td></xsl:if>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:for-each select="Ustart"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="Uustavka"/></td>
			<td><xsl:value-of select="�����������"/></td>
			<td><xsl:value-of select="�����������_����������"/></td>
			<td><xsl:value-of select="��������_2"/></td>
			<td><xsl:value-of select="�������_������������In"/></td>
			<td><xsl:value-of select="�����_������������"/></td>
			<td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_���������"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		
				<p></p>
						<b>������ I�</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>���� �� U</th>
		 <th>U����, �</th>
		 <th>���. ������������, In</th>
		 <th>t����, ��/����</th>
		 <th>���������</th>
		 <th>t���, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/�������_��������������/��������//Rows/AddCurrentDefenseStruct">
		 <xsl:if test="position() = 1">
		 <tr align="center">
			<td>I�</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:for-each select="Ustart"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="Uustavka"/></td>
			<td><xsl:value-of select="�������"/></td>
			<td><xsl:value-of select="�����_������������"/></td>
			<td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_���������"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		
					<p></p>
						<b>������ I2/I1</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>���� �� U</th>
		 <th>U����, �</th>
		 <th>���. ������������, In</th>
		 <th>t����, ��/����</th>
		 <th>���������</th>
		 <th>t���, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/�������_��������������/��������//Rows/AddCurrentDefenseStruct">
		 <xsl:if test="position() = 2">
		 <tr align="center">
			<td>I2/I1</td>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:for-each select="Ustart"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="Uustavka"/></td>
			<td><xsl:value-of select="�������"/></td>
			<td><xsl:value-of select="�����_������������"/></td>
			<td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_���������"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		</table>
			
		</td>
		</table>

<h3><b>������ U</b></h3>	
	<table border="1" cellspacing="0" >
	<td>
	<b>������ U></b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
	     <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th> 
		</tr>
		
		 <xsl:for-each select="��700/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &lt; 3">
		 <tr align="center">
		    <xsl:if test="position() = 1"><td>U> </td></xsl:if>
			<xsl:if test="position() = 2"><td>U>> </td></xsl:if>
			<td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="��������_1"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
		 
		 <b>������ U&#60;</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
	     <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th>
		<th>���������� U&#60; 5�</th>		
		</tr>
		
		 <xsl:for-each select="��700/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &gt; 2">
		 <xsl:if test="position() &lt; 5">
		 <tr align="center">
		    <xsl:if test="position() = 3"><td>U&#60; </td></xsl:if>
			<xsl:if test="position() = 4"><td>U&#60;&#60; </td></xsl:if>
			<td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="��������_1"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="����������5�"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
		 
	<b>������ U2></b>
		<table border="1" cellspacing="0">

		<tr bgcolor="FFCC66">
	     <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th>
		</tr>
		
		 <xsl:for-each select="��700/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &gt; 4">
		 <xsl:if test="position() &lt; 7">
		 <tr align="center">
		    <xsl:if test="position() = 5"><td>U2> </td></xsl:if>
			<xsl:if test="position() = 6"><td>U2>> </td></xsl:if>
			<td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="��������_2"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
	
	<b>������ U0></b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
	     <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th>
		</tr>
		
		 <xsl:for-each select="��700/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &gt; 6">
		 <xsl:if test="position() &lt; 9">
		 <tr align="center">
		    <xsl:if test="position() = 7"><td>U0> </td></xsl:if>
			<xsl:if test="position() = 8"><td>U0>> </td></xsl:if>
			<td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="��������_3"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
	
	</td>
	</table>

	 <h3><b>������ F</b></h3>
		<table border="1" cellspacing="0">
		<td>
		
		<b>������ F></b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>������� ����.</th>
		 <th>����� ����., ��</th>
		 <th>�������</th>
		 <th>��� ��</th>
		 <th>������� ��</th>
		 <th>����� ��������, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		 <th>�����</th>
		</tr>
     
 			
		 <xsl:for-each select="��700/���������/��������/Rows/FrequencyDefenseStruct">
		<xsl:if test="position() &lt; 3">
		 <tr align="center">
		    <xsl:if test="position() = 1"><td>F> </td></xsl:if>
			<xsl:if test="position() = 2"><td>F>> </td></xsl:if>
			<td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		
		
		<b>������ F&#60;</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>����������</th>
         <th>������� ����.</th>
		 <th>����� ����., ��</th>
		 <th>�������</th>
		 <th>��� ��</th>
		 <th>������� ��</th>
		 <th>����� ��������, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��700/���������/��������/Rows/FrequencyDefenseStruct">
		<xsl:if test="position() &gt; 2">
		 <tr align="center">
		    <xsl:if test="position() = 3"><td>F&#60; </td></xsl:if>
			<xsl:if test="position() = 4"><td>F&#60;&#60; </td></xsl:if>
			<td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		
		</td>
		</table>
 
 

	
</xsl:otherwise>

</xsl:choose>

   </body>
  </html>
</xsl:template>
</xsl:stylesheet>
