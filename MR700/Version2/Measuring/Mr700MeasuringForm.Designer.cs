namespace BEMN.MR700.Version2.Measuring
{
    partial class Mr700MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mr700MeasuringForm));
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct2 = new BEMN.Devices.Structures.DateTimeStruct();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._extDefenseLed8 = new BEMN.Forms.LedControl();
            this._extDefenseLed7 = new BEMN.Forms.LedControl();
            this._extDefenseLed6 = new BEMN.Forms.LedControl();
            this._extDefenseLed5 = new BEMN.Forms.LedControl();
            this._extDefenseLed4 = new BEMN.Forms.LedControl();
            this._extDefenseLed3 = new BEMN.Forms.LedControl();
            this._extDefenseLed2 = new BEMN.Forms.LedControl();
            this._extDefenseLed1 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._autoLed8 = new BEMN.Forms.LedControl();
            this._autoLed7 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this._autoLed6 = new BEMN.Forms.LedControl();
            this.label100 = new System.Windows.Forms.Label();
            this._autoLed5 = new BEMN.Forms.LedControl();
            this._autoLed4 = new BEMN.Forms.LedControl();
            this._autoLed3 = new BEMN.Forms.LedControl();
            this._autoLed2 = new BEMN.Forms.LedControl();
            this._autoLed1 = new BEMN.Forms.LedControl();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._FmaxLed8 = new BEMN.Forms.LedControl();
            this._FmaxLed7 = new BEMN.Forms.LedControl();
            this._FmaxLed6 = new BEMN.Forms.LedControl();
            this._FmaxLed5 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this._FmaxLed4 = new BEMN.Forms.LedControl();
            this._FmaxLed3 = new BEMN.Forms.LedControl();
            this._FmaxLed2 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._FmaxLed1 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._faultSignalLed8 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._faultSignalLed7 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._faultSignalLed6 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._faultSignalLed5 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._faultSignalLed4 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._faultSignalLed3 = new BEMN.Forms.LedControl();
            this.label127 = new System.Windows.Forms.Label();
            this._faultSignalLed2 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this._faultSignalLed1 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._faultStateLed8 = new BEMN.Forms.LedControl();
            this._faultStateLed7 = new BEMN.Forms.LedControl();
            this._faultStateLed6 = new BEMN.Forms.LedControl();
            this._faultStateLed5 = new BEMN.Forms.LedControl();
            this._faultStateLed4 = new BEMN.Forms.LedControl();
            this._faultStateLed3 = new BEMN.Forms.LedControl();
            this._faultStateLed2 = new BEMN.Forms.LedControl();
            this._faultStateLed1 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._UmaxLed8 = new BEMN.Forms.LedControl();
            this._UmaxLed7 = new BEMN.Forms.LedControl();
            this._UmaxLed6 = new BEMN.Forms.LedControl();
            this._UmaxLed5 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._UmaxLed4 = new BEMN.Forms.LedControl();
            this._UmaxLed3 = new BEMN.Forms.LedControl();
            this._UmaxLed2 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._UmaxLed1 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this._images = new System.Windows.Forms.ImageList(this.components);
            this.label68 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ImaxLed8 = new BEMN.Forms.LedControl();
            this._ImaxLed7 = new BEMN.Forms.LedControl();
            this._ImaxLed6 = new BEMN.Forms.LedControl();
            this._ImaxLed5 = new BEMN.Forms.LedControl();
            this._ImaxLed4 = new BEMN.Forms.LedControl();
            this._ImaxLed3 = new BEMN.Forms.LedControl();
            this._ImaxLed2 = new BEMN.Forms.LedControl();
            this._ImaxLed1 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._InmaxLed8 = new BEMN.Forms.LedControl();
            this._InmaxLed7 = new BEMN.Forms.LedControl();
            this._InmaxLed6 = new BEMN.Forms.LedControl();
            this._InmaxLed5 = new BEMN.Forms.LedControl();
            this._InmaxLed4 = new BEMN.Forms.LedControl();
            this._InmaxLed3 = new BEMN.Forms.LedControl();
            this._InmaxLed2 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._InmaxLed1 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._I0maxLed8 = new BEMN.Forms.LedControl();
            this._I0maxLed7 = new BEMN.Forms.LedControl();
            this._I0maxLed6 = new BEMN.Forms.LedControl();
            this._I0maxLed5 = new BEMN.Forms.LedControl();
            this._I0maxLed4 = new BEMN.Forms.LedControl();
            this._I0maxLed3 = new BEMN.Forms.LedControl();
            this._I0maxLed2 = new BEMN.Forms.LedControl();
            this._I0maxLed1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._U0maxLed8 = new BEMN.Forms.LedControl();
            this._U0maxLed7 = new BEMN.Forms.LedControl();
            this._U0maxLed6 = new BEMN.Forms.LedControl();
            this._U0maxLed5 = new BEMN.Forms.LedControl();
            this._U0maxLed4 = new BEMN.Forms.LedControl();
            this._U0maxLed3 = new BEMN.Forms.LedControl();
            this._U0maxLed2 = new BEMN.Forms.LedControl();
            this._U0maxLed1 = new BEMN.Forms.LedControl();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this._I1Box = new System.Windows.Forms.TextBox();
            this._I0Box = new System.Windows.Forms.TextBox();
            this._IcBox = new System.Windows.Forms.TextBox();
            this._IbBox = new System.Windows.Forms.TextBox();
            this._IaBox = new System.Windows.Forms.TextBox();
            this._InBox = new System.Windows.Forms.TextBox();
            this._U2Box = new System.Windows.Forms.TextBox();
            this._U1Box = new System.Windows.Forms.TextBox();
            this._U0Box = new System.Windows.Forms.TextBox();
            this._UcaBox = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this._UbcBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._inL_led8 = new BEMN.Forms.LedControl();
            this._inL_led7 = new BEMN.Forms.LedControl();
            this._inL_led6 = new BEMN.Forms.LedControl();
            this._inL_led5 = new BEMN.Forms.LedControl();
            this._inL_led4 = new BEMN.Forms.LedControl();
            this._inL_led3 = new BEMN.Forms.LedControl();
            this._inL_led2 = new BEMN.Forms.LedControl();
            this._inL_led1 = new BEMN.Forms.LedControl();
            this._inD_led16 = new BEMN.Forms.LedControl();
            this._inD_led15 = new BEMN.Forms.LedControl();
            this._inD_led14 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._inD_led13 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._inD_led12 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this._inD_led11 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._inD_led10 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this._inD_led9 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._inD_led8 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._inD_led7 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this._inD_led6 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this._inD_led5 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._inD_led4 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._inD_led3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._inD_led2 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._inD_led1 = new BEMN.Forms.LedControl();
            this._UabBox = new System.Windows.Forms.TextBox();
            this._UbBox = new System.Windows.Forms.TextBox();
            this._UaBox = new System.Windows.Forms.TextBox();
            this._UnBox = new System.Windows.Forms.TextBox();
            this._F_Box = new System.Windows.Forms.TextBox();
            this._IgBox = new System.Windows.Forms.TextBox();
            this._I2Box = new System.Windows.Forms.TextBox();
            this._UcBox = new System.Windows.Forms.TextBox();
            this._CosBox = new System.Windows.Forms.TextBox();
            this._QBox = new System.Windows.Forms.TextBox();
            this._PBox = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this._addIndLed4 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._addIndLed3 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._addIndLed2 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._addIndLed1 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._diagTab = new System.Windows.Forms.TabControl();
            this._analogPage = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._manageLed9 = new BEMN.Forms.LedControl();
            this.label158 = new System.Windows.Forms.Label();
            this._offSplBtn = new System.Windows.Forms.Button();
            this._onSplBtn = new System.Windows.Forms.Button();
            this._selectConstraintGroupBut = new System.Windows.Forms.Button();
            this._resetIndicatBut = new System.Windows.Forms.Button();
            this.label137 = new System.Windows.Forms.Label();
            this._manageLed8 = new BEMN.Forms.LedControl();
            this._manageLed5 = new BEMN.Forms.LedControl();
            this._manageLed4 = new BEMN.Forms.LedControl();
            this._manageLed3 = new BEMN.Forms.LedControl();
            this._manageLed7 = new BEMN.Forms.LedControl();
            this._manageLed6 = new BEMN.Forms.LedControl();
            this._resetJA_But = new System.Windows.Forms.Button();
            this._resetJS_But = new System.Windows.Forms.Button();
            this._resetFaultBut = new System.Windows.Forms.Button();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this._manageLed2 = new BEMN.Forms.LedControl();
            this._manageLed1 = new BEMN.Forms.LedControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._outLed8 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._outLed7 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._outLed6 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._outLed5 = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._outLed4 = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._outLed3 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._outLed2 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._outLed1 = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._releLed8Label = new System.Windows.Forms.Label();
            this._releLed8 = new BEMN.Forms.LedControl();
            this._releLed7Label = new System.Windows.Forms.Label();
            this._releLed7 = new BEMN.Forms.LedControl();
            this._releLed6Label = new System.Windows.Forms.Label();
            this._releLed6 = new BEMN.Forms.LedControl();
            this._releLed5Label = new System.Windows.Forms.Label();
            this._releLed5 = new BEMN.Forms.LedControl();
            this._releLed4Label = new System.Windows.Forms.Label();
            this._releLed4 = new BEMN.Forms.LedControl();
            this._releLed3Label = new System.Windows.Forms.Label();
            this._releLed3 = new BEMN.Forms.LedControl();
            this._releLed2Label = new System.Windows.Forms.Label();
            this._releLed2 = new BEMN.Forms.LedControl();
            this._releLed1Label = new System.Windows.Forms.Label();
            this._releLed1 = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this._indLed8 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this._indLed7 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._indLed6 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._indLed5 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._indLed4 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._indLed3 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._indLed2 = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._indLed1 = new BEMN.Forms.LedControl();
            this._diskretPage = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label129 = new System.Windows.Forms.Label();
            this._faultSignalLed16 = new BEMN.Forms.LedControl();
            this._faultSignalLed15 = new BEMN.Forms.LedControl();
            this._faultSignalLed14 = new BEMN.Forms.LedControl();
            this._faultSignalLed13 = new BEMN.Forms.LedControl();
            this._faultSignalLed12 = new BEMN.Forms.LedControl();
            this._faultSignalLed11 = new BEMN.Forms.LedControl();
            this._faultSignalLed10 = new BEMN.Forms.LedControl();
            this._faultSignalLed9 = new BEMN.Forms.LedControl();
            this.label139 = new System.Windows.Forms.Label();
            this.label14_releLed9Label0 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._diagTab.SuspendLayout();
            this._analogPage.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._diskretPage.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.SuspendLayout();
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(125, 44);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(36, 13);
            this.label107.TabIndex = 43;
            this.label107.Text = "�� - 6";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(125, 23);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(36, 13);
            this.label108.TabIndex = 41;
            this.label108.Text = "�� - 5";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(27, 87);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(36, 13);
            this.label109.TabIndex = 39;
            this.label109.Text = "�� - 4";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(28, 65);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(36, 13);
            this.label110.TabIndex = 37;
            this.label110.Text = "�� - 3";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(28, 44);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 35;
            this.label111.Text = "�� - 2";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(28, 23);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 33;
            this.label112.Text = "�� - 1";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(125, 87);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(36, 13);
            this.label105.TabIndex = 47;
            this.label105.Text = "�� - 8";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(125, 65);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(36, 13);
            this.label106.TabIndex = 45;
            this.label106.Text = "�� - 7";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label105);
            this.groupBox16.Controls.Add(this._extDefenseLed8);
            this.groupBox16.Controls.Add(this.label106);
            this.groupBox16.Controls.Add(this._extDefenseLed7);
            this.groupBox16.Controls.Add(this.label107);
            this.groupBox16.Controls.Add(this._extDefenseLed6);
            this.groupBox16.Controls.Add(this.label108);
            this.groupBox16.Controls.Add(this._extDefenseLed5);
            this.groupBox16.Controls.Add(this.label109);
            this.groupBox16.Controls.Add(this._extDefenseLed4);
            this.groupBox16.Controls.Add(this.label110);
            this.groupBox16.Controls.Add(this._extDefenseLed3);
            this.groupBox16.Controls.Add(this.label111);
            this.groupBox16.Controls.Add(this._extDefenseLed2);
            this.groupBox16.Controls.Add(this.label112);
            this.groupBox16.Controls.Add(this._extDefenseLed1);
            this.groupBox16.Location = new System.Drawing.Point(6, 247);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(180, 108);
            this.groupBox16.TabIndex = 9;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "������� ������";
            // 
            // _extDefenseLed8
            // 
            this._extDefenseLed8.Location = new System.Drawing.Point(106, 86);
            this._extDefenseLed8.Name = "_extDefenseLed8";
            this._extDefenseLed8.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed8.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed8.TabIndex = 46;
            // 
            // _extDefenseLed7
            // 
            this._extDefenseLed7.Location = new System.Drawing.Point(106, 65);
            this._extDefenseLed7.Name = "_extDefenseLed7";
            this._extDefenseLed7.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed7.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed7.TabIndex = 44;
            // 
            // _extDefenseLed6
            // 
            this._extDefenseLed6.Location = new System.Drawing.Point(106, 44);
            this._extDefenseLed6.Name = "_extDefenseLed6";
            this._extDefenseLed6.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed6.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed6.TabIndex = 42;
            // 
            // _extDefenseLed5
            // 
            this._extDefenseLed5.Location = new System.Drawing.Point(106, 23);
            this._extDefenseLed5.Name = "_extDefenseLed5";
            this._extDefenseLed5.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed5.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed5.TabIndex = 40;
            // 
            // _extDefenseLed4
            // 
            this._extDefenseLed4.Location = new System.Drawing.Point(8, 86);
            this._extDefenseLed4.Name = "_extDefenseLed4";
            this._extDefenseLed4.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed4.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed4.TabIndex = 38;
            // 
            // _extDefenseLed3
            // 
            this._extDefenseLed3.Location = new System.Drawing.Point(8, 65);
            this._extDefenseLed3.Name = "_extDefenseLed3";
            this._extDefenseLed3.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed3.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed3.TabIndex = 36;
            // 
            // _extDefenseLed2
            // 
            this._extDefenseLed2.Location = new System.Drawing.Point(8, 44);
            this._extDefenseLed2.Name = "_extDefenseLed2";
            this._extDefenseLed2.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed2.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed2.TabIndex = 34;
            // 
            // _extDefenseLed1
            // 
            this._extDefenseLed1.Location = new System.Drawing.Point(8, 23);
            this._extDefenseLed1.Name = "_extDefenseLed1";
            this._extDefenseLed1.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed1.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed1.TabIndex = 32;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(21, 37);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(78, 13);
            this.label120.TabIndex = 17;
            this.label120.Text = "�. ����������";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(35, 60);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(44, 13);
            this.label101.TabIndex = 23;
            this.label101.Text = "������";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(35, 45);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(91, 13);
            this.label102.TabIndex = 21;
            this.label102.Text = "��� ����������";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(35, 30);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(123, 13);
            this.label103.TabIndex = 19;
            this.label103.Text = "��� ��������� ������";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(35, 15);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(118, 13);
            this.label104.TabIndex = 17;
            this.label104.Text = "��� �������� ������";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(35, 120);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(63, 13);
            this.label97.TabIndex = 31;
            this.label97.Text = "���������";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(35, 105);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(134, 13);
            this.label98.TabIndex = 29;
            this.label98.Text = "��������� �����������";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label97);
            this.groupBox15.Controls.Add(this._autoLed8);
            this.groupBox15.Controls.Add(this.label98);
            this.groupBox15.Controls.Add(this._autoLed7);
            this.groupBox15.Controls.Add(this.label99);
            this.groupBox15.Controls.Add(this._autoLed6);
            this.groupBox15.Controls.Add(this.label100);
            this.groupBox15.Controls.Add(this._autoLed5);
            this.groupBox15.Controls.Add(this.label101);
            this.groupBox15.Controls.Add(this._autoLed4);
            this.groupBox15.Controls.Add(this.label102);
            this.groupBox15.Controls.Add(this._autoLed3);
            this.groupBox15.Controls.Add(this.label103);
            this.groupBox15.Controls.Add(this._autoLed2);
            this.groupBox15.Controls.Add(this.label104);
            this.groupBox15.Controls.Add(this._autoLed1);
            this.groupBox15.Location = new System.Drawing.Point(6, 109);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(180, 137);
            this.groupBox15.TabIndex = 8;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "����������";
            // 
            // _autoLed8
            // 
            this._autoLed8.Location = new System.Drawing.Point(8, 120);
            this._autoLed8.Name = "_autoLed8";
            this._autoLed8.Size = new System.Drawing.Size(13, 13);
            this._autoLed8.State = BEMN.Forms.LedState.Off;
            this._autoLed8.TabIndex = 30;
            // 
            // _autoLed7
            // 
            this._autoLed7.Location = new System.Drawing.Point(8, 105);
            this._autoLed7.Name = "_autoLed7";
            this._autoLed7.Size = new System.Drawing.Size(13, 13);
            this._autoLed7.State = BEMN.Forms.LedState.Off;
            this._autoLed7.TabIndex = 28;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(35, 90);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(76, 13);
            this.label99.TabIndex = 27;
            this.label99.Text = "������ ����";
            // 
            // _autoLed6
            // 
            this._autoLed6.Location = new System.Drawing.Point(8, 90);
            this._autoLed6.Name = "_autoLed6";
            this._autoLed6.Size = new System.Drawing.Size(13, 13);
            this._autoLed6.State = BEMN.Forms.LedState.Off;
            this._autoLed6.TabIndex = 26;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(35, 75);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(70, 13);
            this.label100.TabIndex = 25;
            this.label100.Text = "������ ���";
            // 
            // _autoLed5
            // 
            this._autoLed5.Location = new System.Drawing.Point(8, 75);
            this._autoLed5.Name = "_autoLed5";
            this._autoLed5.Size = new System.Drawing.Size(13, 13);
            this._autoLed5.State = BEMN.Forms.LedState.Off;
            this._autoLed5.TabIndex = 24;
            // 
            // _autoLed4
            // 
            this._autoLed4.Location = new System.Drawing.Point(8, 60);
            this._autoLed4.Name = "_autoLed4";
            this._autoLed4.Size = new System.Drawing.Size(13, 13);
            this._autoLed4.State = BEMN.Forms.LedState.Off;
            this._autoLed4.TabIndex = 22;
            // 
            // _autoLed3
            // 
            this._autoLed3.Location = new System.Drawing.Point(8, 45);
            this._autoLed3.Name = "_autoLed3";
            this._autoLed3.Size = new System.Drawing.Size(13, 13);
            this._autoLed3.State = BEMN.Forms.LedState.Off;
            this._autoLed3.TabIndex = 20;
            // 
            // _autoLed2
            // 
            this._autoLed2.Location = new System.Drawing.Point(8, 30);
            this._autoLed2.Name = "_autoLed2";
            this._autoLed2.Size = new System.Drawing.Size(13, 13);
            this._autoLed2.State = BEMN.Forms.LedState.Off;
            this._autoLed2.TabIndex = 18;
            // 
            // _autoLed1
            // 
            this._autoLed1.Location = new System.Drawing.Point(8, 15);
            this._autoLed1.Name = "_autoLed1";
            this._autoLed1.Size = new System.Drawing.Size(13, 13);
            this._autoLed1.State = BEMN.Forms.LedState.Off;
            this._autoLed1.TabIndex = 16;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._FmaxLed8);
            this.groupBox13.Controls.Add(this._FmaxLed7);
            this.groupBox13.Controls.Add(this._FmaxLed6);
            this.groupBox13.Controls.Add(this._FmaxLed5);
            this.groupBox13.Controls.Add(this.label85);
            this.groupBox13.Controls.Add(this.label86);
            this.groupBox13.Controls.Add(this.label87);
            this.groupBox13.Controls.Add(this.label88);
            this.groupBox13.Controls.Add(this.label89);
            this.groupBox13.Controls.Add(this._FmaxLed4);
            this.groupBox13.Controls.Add(this._FmaxLed3);
            this.groupBox13.Controls.Add(this._FmaxLed2);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Controls.Add(this._FmaxLed1);
            this.groupBox13.Location = new System.Drawing.Point(480, 6);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(89, 99);
            this.groupBox13.TabIndex = 7;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "F";
            // 
            // _FmaxLed8
            // 
            this._FmaxLed8.Location = new System.Drawing.Point(34, 79);
            this._FmaxLed8.Name = "_FmaxLed8";
            this._FmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed8.State = BEMN.Forms.LedState.Off;
            this._FmaxLed8.TabIndex = 24;
            // 
            // _FmaxLed7
            // 
            this._FmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._FmaxLed7.Name = "_FmaxLed7";
            this._FmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed7.State = BEMN.Forms.LedState.Off;
            this._FmaxLed7.TabIndex = 23;
            // 
            // _FmaxLed6
            // 
            this._FmaxLed6.Location = new System.Drawing.Point(34, 63);
            this._FmaxLed6.Name = "_FmaxLed6";
            this._FmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed6.State = BEMN.Forms.LedState.Off;
            this._FmaxLed6.TabIndex = 22;
            // 
            // _FmaxLed5
            // 
            this._FmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._FmaxLed5.Name = "_FmaxLed5";
            this._FmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed5.State = BEMN.Forms.LedState.Off;
            this._FmaxLed5.TabIndex = 21;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(48, 79);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(25, 13);
            this.label85.TabIndex = 20;
            this.label85.Text = "F<<";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(48, 63);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(19, 13);
            this.label86.TabIndex = 19;
            this.label86.Text = "F<";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(48, 47);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(25, 13);
            this.label87.TabIndex = 18;
            this.label87.Text = "F>>";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(48, 31);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(19, 13);
            this.label88.TabIndex = 17;
            this.label88.Text = "F>";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(26, 14);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(35, 13);
            this.label89.TabIndex = 16;
            this.label89.Text = "����.";
            // 
            // _FmaxLed4
            // 
            this._FmaxLed4.Location = new System.Drawing.Point(34, 47);
            this._FmaxLed4.Name = "_FmaxLed4";
            this._FmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed4.State = BEMN.Forms.LedState.Off;
            this._FmaxLed4.TabIndex = 6;
            // 
            // _FmaxLed3
            // 
            this._FmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._FmaxLed3.Name = "_FmaxLed3";
            this._FmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed3.State = BEMN.Forms.LedState.Off;
            this._FmaxLed3.TabIndex = 4;
            // 
            // _FmaxLed2
            // 
            this._FmaxLed2.Location = new System.Drawing.Point(34, 31);
            this._FmaxLed2.Name = "_FmaxLed2";
            this._FmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed2.State = BEMN.Forms.LedState.Off;
            this._FmaxLed2.TabIndex = 2;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(5, 14);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(23, 13);
            this.label90.TabIndex = 1;
            this.label90.Text = "��";
            // 
            // _FmaxLed1
            // 
            this._FmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._FmaxLed1.Name = "_FmaxLed1";
            this._FmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._FmaxLed1.State = BEMN.Forms.LedState.Off;
            this._FmaxLed1.TabIndex = 0;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(21, 89);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(58, 13);
            this.label119.TabIndex = 19;
            this.label119.Text = "�. ������";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(21, 191);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(83, 13);
            this.label114.TabIndex = 29;
            this.label114.Text = "�. ����������";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(21, 219);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(52, 13);
            this.label121.TabIndex = 31;
            this.label121.Text = "�9 - �16";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label121);
            this.groupBox18.Controls.Add(this._faultSignalLed8);
            this.groupBox18.Controls.Add(this.label122);
            this.groupBox18.Controls.Add(this._faultSignalLed7);
            this.groupBox18.Controls.Add(this.label123);
            this.groupBox18.Controls.Add(this._faultSignalLed6);
            this.groupBox18.Controls.Add(this.label124);
            this.groupBox18.Controls.Add(this._faultSignalLed5);
            this.groupBox18.Controls.Add(this.label125);
            this.groupBox18.Controls.Add(this._faultSignalLed4);
            this.groupBox18.Controls.Add(this.label126);
            this.groupBox18.Controls.Add(this._faultSignalLed3);
            this.groupBox18.Controls.Add(this.label127);
            this.groupBox18.Controls.Add(this._faultSignalLed2);
            this.groupBox18.Controls.Add(this.label128);
            this.groupBox18.Controls.Add(this._faultSignalLed1);
            this.groupBox18.Location = new System.Drawing.Point(322, 112);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(115, 243);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "������� ������������� 1";
            // 
            // _faultSignalLed8
            // 
            this._faultSignalLed8.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed8.Name = "_faultSignalLed8";
            this._faultSignalLed8.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed8.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed8.TabIndex = 30;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(21, 193);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(46, 13);
            this.label122.TabIndex = 29;
            this.label122.Text = "�1 - �8";
            // 
            // _faultSignalLed7
            // 
            this._faultSignalLed7.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed7.Name = "_faultSignalLed7";
            this._faultSignalLed7.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed7.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed7.TabIndex = 28;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(21, 166);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(32, 13);
            this.label123.TabIndex = 27;
            this.label123.Text = "����";
            // 
            // _faultSignalLed6
            // 
            this._faultSignalLed6.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed6.Name = "_faultSignalLed6";
            this._faultSignalLed6.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed6.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed6.TabIndex = 26;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(21, 140);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(56, 13);
            this.label124.TabIndex = 25;
            this.label124.Text = "������� I";
            // 
            // _faultSignalLed5
            // 
            this._faultSignalLed5.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed5.Name = "_faultSignalLed5";
            this._faultSignalLed5.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed5.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed5.TabIndex = 24;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(21, 114);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(61, 13);
            this.label125.TabIndex = 23;
            this.label125.Text = "������� U";
            // 
            // _faultSignalLed4
            // 
            this._faultSignalLed4.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed4.Name = "_faultSignalLed4";
            this._faultSignalLed4.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed4.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed4.TabIndex = 22;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(21, 63);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(70, 13);
            this.label126.TabIndex = 21;
            this.label126.Text = "�.  ���� I2c";
            // 
            // _faultSignalLed3
            // 
            this._faultSignalLed3.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed3.Name = "_faultSignalLed3";
            this._faultSignalLed3.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed3.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed3.TabIndex = 20;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(21, 89);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(88, 13);
            this.label127.TabIndex = 19;
            this.label127.Text = "�. �����������";
            // 
            // _faultSignalLed2
            // 
            this._faultSignalLed2.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed2.Name = "_faultSignalLed2";
            this._faultSignalLed2.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed2.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed2.TabIndex = 18;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(21, 37);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(73, 13);
            this.label128.TabIndex = 17;
            this.label128.Text = "������ ���";
            // 
            // _faultSignalLed1
            // 
            this._faultSignalLed1.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed1.Name = "_faultSignalLed1";
            this._faultSignalLed1.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed1.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed1.TabIndex = 16;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(21, 218);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(91, 13);
            this.label113.TabIndex = 31;
            this.label113.Text = "�.  ���. �������";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(21, 166);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(67, 13);
            this.label115.TabIndex = 27;
            this.label115.Text = "�.���. ����";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(21, 166);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(50, 13);
            this.label131.TabIndex = 27;
            this.label131.Text = "�. �����";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(21, 63);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(84, 13);
            this.label134.TabIndex = 21;
            this.label134.Text = "�. �����. ���";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(21, 89);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(44, 13);
            this.label135.TabIndex = 19;
            this.label135.Text = "������";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(21, 37);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(90, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "������ �������";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(21, 140);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(39, 13);
            this.label132.TabIndex = 25;
            this.label132.Text = "�. ��";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(21, 63);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(44, 13);
            this.label118.TabIndex = 21;
            this.label118.Text = "������";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(21, 140);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(89, 13);
            this.label116.TabIndex = 25;
            this.label116.Text = "� .�����������";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(21, 114);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(39, 13);
            this.label133.TabIndex = 23;
            this.label133.Text = "�. ��";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(21, 114);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(44, 13);
            this.label117.TabIndex = 23;
            this.label117.Text = "������";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label113);
            this.groupBox17.Controls.Add(this._faultStateLed8);
            this.groupBox17.Controls.Add(this.label114);
            this.groupBox17.Controls.Add(this._faultStateLed7);
            this.groupBox17.Controls.Add(this.label115);
            this.groupBox17.Controls.Add(this._faultStateLed6);
            this.groupBox17.Controls.Add(this.label116);
            this.groupBox17.Controls.Add(this._faultStateLed5);
            this.groupBox17.Controls.Add(this.label117);
            this.groupBox17.Controls.Add(this._faultStateLed4);
            this.groupBox17.Controls.Add(this.label118);
            this.groupBox17.Controls.Add(this._faultStateLed3);
            this.groupBox17.Controls.Add(this.label119);
            this.groupBox17.Controls.Add(this._faultStateLed2);
            this.groupBox17.Controls.Add(this.label120);
            this.groupBox17.Controls.Add(this._faultStateLed1);
            this.groupBox17.Location = new System.Drawing.Point(194, 112);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(115, 243);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "��������� �������������";
            // 
            // _faultStateLed8
            // 
            this._faultStateLed8.Location = new System.Drawing.Point(6, 219);
            this._faultStateLed8.Name = "_faultStateLed8";
            this._faultStateLed8.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed8.State = BEMN.Forms.LedState.Off;
            this._faultStateLed8.TabIndex = 30;
            // 
            // _faultStateLed7
            // 
            this._faultStateLed7.Location = new System.Drawing.Point(6, 193);
            this._faultStateLed7.Name = "_faultStateLed7";
            this._faultStateLed7.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed7.State = BEMN.Forms.LedState.Off;
            this._faultStateLed7.TabIndex = 28;
            // 
            // _faultStateLed6
            // 
            this._faultStateLed6.Location = new System.Drawing.Point(6, 167);
            this._faultStateLed6.Name = "_faultStateLed6";
            this._faultStateLed6.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed6.State = BEMN.Forms.LedState.Off;
            this._faultStateLed6.TabIndex = 26;
            // 
            // _faultStateLed5
            // 
            this._faultStateLed5.Location = new System.Drawing.Point(6, 141);
            this._faultStateLed5.Name = "_faultStateLed5";
            this._faultStateLed5.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed5.State = BEMN.Forms.LedState.Off;
            this._faultStateLed5.TabIndex = 24;
            // 
            // _faultStateLed4
            // 
            this._faultStateLed4.Location = new System.Drawing.Point(6, 115);
            this._faultStateLed4.Name = "_faultStateLed4";
            this._faultStateLed4.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed4.State = BEMN.Forms.LedState.Off;
            this._faultStateLed4.TabIndex = 22;
            // 
            // _faultStateLed3
            // 
            this._faultStateLed3.Location = new System.Drawing.Point(6, 89);
            this._faultStateLed3.Name = "_faultStateLed3";
            this._faultStateLed3.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed3.State = BEMN.Forms.LedState.Off;
            this._faultStateLed3.TabIndex = 20;
            // 
            // _faultStateLed2
            // 
            this._faultStateLed2.Location = new System.Drawing.Point(6, 63);
            this._faultStateLed2.Name = "_faultStateLed2";
            this._faultStateLed2.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed2.State = BEMN.Forms.LedState.Off;
            this._faultStateLed2.TabIndex = 18;
            // 
            // _faultStateLed1
            // 
            this._faultStateLed1.Location = new System.Drawing.Point(6, 37);
            this._faultStateLed1.Name = "_faultStateLed1";
            this._faultStateLed1.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed1.State = BEMN.Forms.LedState.Off;
            this._faultStateLed1.TabIndex = 16;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(48, 79);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 13);
            this.label55.TabIndex = 20;
            this.label55.Text = "U<<";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._UmaxLed8);
            this.groupBox10.Controls.Add(this._UmaxLed7);
            this.groupBox10.Controls.Add(this._UmaxLed6);
            this.groupBox10.Controls.Add(this._UmaxLed5);
            this.groupBox10.Controls.Add(this.label55);
            this.groupBox10.Controls.Add(this.label57);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._UmaxLed4);
            this.groupBox10.Controls.Add(this._UmaxLed3);
            this.groupBox10.Controls.Add(this._UmaxLed2);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._UmaxLed1);
            this.groupBox10.Location = new System.Drawing.Point(97, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(89, 99);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "U max";
            // 
            // _UmaxLed8
            // 
            this._UmaxLed8.Location = new System.Drawing.Point(34, 79);
            this._UmaxLed8.Name = "_UmaxLed8";
            this._UmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed8.State = BEMN.Forms.LedState.Off;
            this._UmaxLed8.TabIndex = 24;
            // 
            // _UmaxLed7
            // 
            this._UmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._UmaxLed7.Name = "_UmaxLed7";
            this._UmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed7.State = BEMN.Forms.LedState.Off;
            this._UmaxLed7.TabIndex = 23;
            // 
            // _UmaxLed6
            // 
            this._UmaxLed6.Location = new System.Drawing.Point(34, 63);
            this._UmaxLed6.Name = "_UmaxLed6";
            this._UmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed6.State = BEMN.Forms.LedState.Off;
            this._UmaxLed6.TabIndex = 22;
            // 
            // _UmaxLed5
            // 
            this._UmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._UmaxLed5.Name = "_UmaxLed5";
            this._UmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed5.State = BEMN.Forms.LedState.Off;
            this._UmaxLed5.TabIndex = 21;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(48, 63);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(21, 13);
            this.label57.TabIndex = 19;
            this.label57.Text = "U<";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(48, 47);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(27, 13);
            this.label63.TabIndex = 18;
            this.label63.Text = "U>>";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(48, 31);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(21, 13);
            this.label64.TabIndex = 17;
            this.label64.Text = "U>";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(26, 14);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(35, 13);
            this.label65.TabIndex = 16;
            this.label65.Text = "����.";
            // 
            // _UmaxLed4
            // 
            this._UmaxLed4.Location = new System.Drawing.Point(34, 47);
            this._UmaxLed4.Name = "_UmaxLed4";
            this._UmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed4.State = BEMN.Forms.LedState.Off;
            this._UmaxLed4.TabIndex = 6;
            // 
            // _UmaxLed3
            // 
            this._UmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._UmaxLed3.Name = "_UmaxLed3";
            this._UmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed3.State = BEMN.Forms.LedState.Off;
            this._UmaxLed3.TabIndex = 4;
            // 
            // _UmaxLed2
            // 
            this._UmaxLed2.Location = new System.Drawing.Point(34, 31);
            this._UmaxLed2.Name = "_UmaxLed2";
            this._UmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed2.State = BEMN.Forms.LedState.Off;
            this._UmaxLed2.TabIndex = 2;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(5, 14);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(23, 13);
            this.label66.TabIndex = 1;
            this.label66.Text = "��";
            // 
            // _UmaxLed1
            // 
            this._UmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._UmaxLed1.Name = "_UmaxLed1";
            this._UmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed1.State = BEMN.Forms.LedState.Off;
            this._UmaxLed1.TabIndex = 0;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(50, 79);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(28, 13);
            this.label79.TabIndex = 20;
            this.label79.Text = "I0>>";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(50, 63);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(22, 13);
            this.label80.TabIndex = 19;
            this.label80.Text = "I0>";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(50, 47);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 18;
            this.label81.Text = "I2>>";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(50, 31);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(22, 13);
            this.label82.TabIndex = 17;
            this.label82.Text = "I2>";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(26, 14);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 13);
            this.label83.TabIndex = 16;
            this.label83.Text = "����.";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(5, 14);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(23, 13);
            this.label84.TabIndex = 1;
            this.label84.Text = "��";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(50, 63);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(28, 13);
            this.label72.TabIndex = 19;
            this.label72.Text = "I>>>";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(50, 47);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(22, 13);
            this.label71.TabIndex = 18;
            this.label71.Text = "I>>";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(50, 31);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(16, 13);
            this.label70.TabIndex = 17;
            this.label70.Text = "I>";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(26, 14);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 16;
            this.label69.Text = "����.";
            // 
            // _images
            // 
            this._images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.TransparentColor = System.Drawing.Color.Transparent;
            this._images.Images.SetKeyName(0, "analog.bmp");
            this._images.Images.SetKeyName(1, "diskret.bmp");
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(5, 14);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(23, 13);
            this.label68.TabIndex = 1;
            this.label68.Text = "��";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(50, 79);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(34, 13);
            this.label73.TabIndex = 20;
            this.label73.Text = "I>>>>";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ImaxLed8);
            this.groupBox9.Controls.Add(this._ImaxLed7);
            this.groupBox9.Controls.Add(this._ImaxLed6);
            this.groupBox9.Controls.Add(this._ImaxLed5);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.label72);
            this.groupBox9.Controls.Add(this.label71);
            this.groupBox9.Controls.Add(this.label70);
            this.groupBox9.Controls.Add(this.label69);
            this.groupBox9.Controls.Add(this._ImaxLed4);
            this.groupBox9.Controls.Add(this._ImaxLed3);
            this.groupBox9.Controls.Add(this._ImaxLed2);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this._ImaxLed1);
            this.groupBox9.Location = new System.Drawing.Point(6, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(89, 99);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "I max";
            // 
            // _ImaxLed8
            // 
            this._ImaxLed8.Location = new System.Drawing.Point(35, 79);
            this._ImaxLed8.Name = "_ImaxLed8";
            this._ImaxLed8.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed8.State = BEMN.Forms.LedState.Off;
            this._ImaxLed8.TabIndex = 24;
            // 
            // _ImaxLed7
            // 
            this._ImaxLed7.Location = new System.Drawing.Point(8, 79);
            this._ImaxLed7.Name = "_ImaxLed7";
            this._ImaxLed7.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed7.State = BEMN.Forms.LedState.Off;
            this._ImaxLed7.TabIndex = 23;
            // 
            // _ImaxLed6
            // 
            this._ImaxLed6.Location = new System.Drawing.Point(35, 63);
            this._ImaxLed6.Name = "_ImaxLed6";
            this._ImaxLed6.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed6.State = BEMN.Forms.LedState.Off;
            this._ImaxLed6.TabIndex = 22;
            // 
            // _ImaxLed5
            // 
            this._ImaxLed5.Location = new System.Drawing.Point(8, 63);
            this._ImaxLed5.Name = "_ImaxLed5";
            this._ImaxLed5.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed5.State = BEMN.Forms.LedState.Off;
            this._ImaxLed5.TabIndex = 21;
            // 
            // _ImaxLed4
            // 
            this._ImaxLed4.Location = new System.Drawing.Point(35, 47);
            this._ImaxLed4.Name = "_ImaxLed4";
            this._ImaxLed4.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed4.State = BEMN.Forms.LedState.Off;
            this._ImaxLed4.TabIndex = 6;
            // 
            // _ImaxLed3
            // 
            this._ImaxLed3.Location = new System.Drawing.Point(8, 47);
            this._ImaxLed3.Name = "_ImaxLed3";
            this._ImaxLed3.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed3.State = BEMN.Forms.LedState.Off;
            this._ImaxLed3.TabIndex = 4;
            // 
            // _ImaxLed2
            // 
            this._ImaxLed2.Location = new System.Drawing.Point(35, 31);
            this._ImaxLed2.Name = "_ImaxLed2";
            this._ImaxLed2.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed2.State = BEMN.Forms.LedState.Off;
            this._ImaxLed2.TabIndex = 2;
            // 
            // _ImaxLed1
            // 
            this._ImaxLed1.Location = new System.Drawing.Point(8, 31);
            this._ImaxLed1.Name = "_ImaxLed1";
            this._ImaxLed1.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed1.State = BEMN.Forms.LedState.Off;
            this._ImaxLed1.TabIndex = 0;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(50, 79);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(30, 13);
            this.label91.TabIndex = 20;
            this.label91.Text = "I2/I1";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(50, 63);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(16, 13);
            this.label92.TabIndex = 19;
            this.label92.Text = "Ig";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(50, 47);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(28, 13);
            this.label93.TabIndex = 18;
            this.label93.Text = "In>>";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(50, 31);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 17;
            this.label94.Text = "In>";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(26, 14);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(35, 13);
            this.label95.TabIndex = 16;
            this.label95.Text = "����.";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._InmaxLed8);
            this.groupBox14.Controls.Add(this._InmaxLed7);
            this.groupBox14.Controls.Add(this._InmaxLed6);
            this.groupBox14.Controls.Add(this._InmaxLed5);
            this.groupBox14.Controls.Add(this.label91);
            this.groupBox14.Controls.Add(this.label92);
            this.groupBox14.Controls.Add(this.label93);
            this.groupBox14.Controls.Add(this.label94);
            this.groupBox14.Controls.Add(this.label95);
            this.groupBox14.Controls.Add(this._InmaxLed4);
            this.groupBox14.Controls.Add(this._InmaxLed3);
            this.groupBox14.Controls.Add(this._InmaxLed2);
            this.groupBox14.Controls.Add(this.label96);
            this.groupBox14.Controls.Add(this._InmaxLed1);
            this.groupBox14.Location = new System.Drawing.Point(383, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(89, 99);
            this.groupBox14.TabIndex = 6;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "In Ig I2/I1 ";
            // 
            // _InmaxLed8
            // 
            this._InmaxLed8.Location = new System.Drawing.Point(35, 79);
            this._InmaxLed8.Name = "_InmaxLed8";
            this._InmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed8.State = BEMN.Forms.LedState.Off;
            this._InmaxLed8.TabIndex = 24;
            // 
            // _InmaxLed7
            // 
            this._InmaxLed7.Location = new System.Drawing.Point(8, 79);
            this._InmaxLed7.Name = "_InmaxLed7";
            this._InmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed7.State = BEMN.Forms.LedState.Off;
            this._InmaxLed7.TabIndex = 23;
            // 
            // _InmaxLed6
            // 
            this._InmaxLed6.Location = new System.Drawing.Point(35, 63);
            this._InmaxLed6.Name = "_InmaxLed6";
            this._InmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed6.State = BEMN.Forms.LedState.Off;
            this._InmaxLed6.TabIndex = 22;
            // 
            // _InmaxLed5
            // 
            this._InmaxLed5.Location = new System.Drawing.Point(8, 63);
            this._InmaxLed5.Name = "_InmaxLed5";
            this._InmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed5.State = BEMN.Forms.LedState.Off;
            this._InmaxLed5.TabIndex = 21;
            // 
            // _InmaxLed4
            // 
            this._InmaxLed4.Location = new System.Drawing.Point(35, 47);
            this._InmaxLed4.Name = "_InmaxLed4";
            this._InmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed4.State = BEMN.Forms.LedState.Off;
            this._InmaxLed4.TabIndex = 6;
            // 
            // _InmaxLed3
            // 
            this._InmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._InmaxLed3.Name = "_InmaxLed3";
            this._InmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed3.State = BEMN.Forms.LedState.Off;
            this._InmaxLed3.TabIndex = 4;
            // 
            // _InmaxLed2
            // 
            this._InmaxLed2.Location = new System.Drawing.Point(35, 31);
            this._InmaxLed2.Name = "_InmaxLed2";
            this._InmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed2.State = BEMN.Forms.LedState.Off;
            this._InmaxLed2.TabIndex = 2;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(5, 14);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(23, 13);
            this.label96.TabIndex = 1;
            this.label96.Text = "��";
            // 
            // _InmaxLed1
            // 
            this._InmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._InmaxLed1.Name = "_InmaxLed1";
            this._InmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed1.State = BEMN.Forms.LedState.Off;
            this._InmaxLed1.TabIndex = 0;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(48, 47);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(33, 13);
            this.label75.TabIndex = 18;
            this.label75.Text = "U2>>";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(48, 31);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(27, 13);
            this.label76.TabIndex = 17;
            this.label76.Text = "U2>";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(26, 14);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 16;
            this.label77.Text = "����.";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._I0maxLed8);
            this.groupBox12.Controls.Add(this._I0maxLed7);
            this.groupBox12.Controls.Add(this._I0maxLed6);
            this.groupBox12.Controls.Add(this._I0maxLed5);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.label80);
            this.groupBox12.Controls.Add(this.label81);
            this.groupBox12.Controls.Add(this.label82);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this._I0maxLed4);
            this.groupBox12.Controls.Add(this._I0maxLed3);
            this.groupBox12.Controls.Add(this._I0maxLed2);
            this.groupBox12.Controls.Add(this.label84);
            this.groupBox12.Controls.Add(this._I0maxLed1);
            this.groupBox12.Location = new System.Drawing.Point(194, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(89, 99);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "I0 � I2 max";
            // 
            // _I0maxLed8
            // 
            this._I0maxLed8.Location = new System.Drawing.Point(35, 79);
            this._I0maxLed8.Name = "_I0maxLed8";
            this._I0maxLed8.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed8.State = BEMN.Forms.LedState.Off;
            this._I0maxLed8.TabIndex = 24;
            // 
            // _I0maxLed7
            // 
            this._I0maxLed7.Location = new System.Drawing.Point(8, 79);
            this._I0maxLed7.Name = "_I0maxLed7";
            this._I0maxLed7.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed7.State = BEMN.Forms.LedState.Off;
            this._I0maxLed7.TabIndex = 23;
            // 
            // _I0maxLed6
            // 
            this._I0maxLed6.Location = new System.Drawing.Point(35, 63);
            this._I0maxLed6.Name = "_I0maxLed6";
            this._I0maxLed6.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed6.State = BEMN.Forms.LedState.Off;
            this._I0maxLed6.TabIndex = 22;
            // 
            // _I0maxLed5
            // 
            this._I0maxLed5.Location = new System.Drawing.Point(8, 63);
            this._I0maxLed5.Name = "_I0maxLed5";
            this._I0maxLed5.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed5.State = BEMN.Forms.LedState.Off;
            this._I0maxLed5.TabIndex = 21;
            // 
            // _I0maxLed4
            // 
            this._I0maxLed4.Location = new System.Drawing.Point(35, 47);
            this._I0maxLed4.Name = "_I0maxLed4";
            this._I0maxLed4.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed4.State = BEMN.Forms.LedState.Off;
            this._I0maxLed4.TabIndex = 6;
            // 
            // _I0maxLed3
            // 
            this._I0maxLed3.Location = new System.Drawing.Point(8, 47);
            this._I0maxLed3.Name = "_I0maxLed3";
            this._I0maxLed3.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed3.State = BEMN.Forms.LedState.Off;
            this._I0maxLed3.TabIndex = 4;
            // 
            // _I0maxLed2
            // 
            this._I0maxLed2.Location = new System.Drawing.Point(35, 31);
            this._I0maxLed2.Name = "_I0maxLed2";
            this._I0maxLed2.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed2.State = BEMN.Forms.LedState.Off;
            this._I0maxLed2.TabIndex = 2;
            // 
            // _I0maxLed1
            // 
            this._I0maxLed1.Location = new System.Drawing.Point(8, 31);
            this._I0maxLed1.Name = "_I0maxLed1";
            this._I0maxLed1.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed1.State = BEMN.Forms.LedState.Off;
            this._I0maxLed1.TabIndex = 0;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(5, 14);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(23, 13);
            this.label78.TabIndex = 1;
            this.label78.Text = "��";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(48, 79);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(33, 13);
            this.label67.TabIndex = 20;
            this.label67.Text = "U0>>";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(48, 63);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(27, 13);
            this.label74.TabIndex = 19;
            this.label74.Text = "U0>";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._U0maxLed8);
            this.groupBox11.Controls.Add(this._U0maxLed7);
            this.groupBox11.Controls.Add(this._U0maxLed6);
            this.groupBox11.Controls.Add(this._U0maxLed5);
            this.groupBox11.Controls.Add(this.label67);
            this.groupBox11.Controls.Add(this.label74);
            this.groupBox11.Controls.Add(this.label75);
            this.groupBox11.Controls.Add(this.label76);
            this.groupBox11.Controls.Add(this.label77);
            this.groupBox11.Controls.Add(this._U0maxLed4);
            this.groupBox11.Controls.Add(this._U0maxLed3);
            this.groupBox11.Controls.Add(this._U0maxLed2);
            this.groupBox11.Controls.Add(this.label78);
            this.groupBox11.Controls.Add(this._U0maxLed1);
            this.groupBox11.Location = new System.Drawing.Point(288, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(89, 99);
            this.groupBox11.TabIndex = 5;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "U0 � U2 max";
            // 
            // _U0maxLed8
            // 
            this._U0maxLed8.Location = new System.Drawing.Point(34, 79);
            this._U0maxLed8.Name = "_U0maxLed8";
            this._U0maxLed8.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed8.State = BEMN.Forms.LedState.Off;
            this._U0maxLed8.TabIndex = 24;
            // 
            // _U0maxLed7
            // 
            this._U0maxLed7.Location = new System.Drawing.Point(8, 79);
            this._U0maxLed7.Name = "_U0maxLed7";
            this._U0maxLed7.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed7.State = BEMN.Forms.LedState.Off;
            this._U0maxLed7.TabIndex = 23;
            // 
            // _U0maxLed6
            // 
            this._U0maxLed6.Location = new System.Drawing.Point(34, 63);
            this._U0maxLed6.Name = "_U0maxLed6";
            this._U0maxLed6.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed6.State = BEMN.Forms.LedState.Off;
            this._U0maxLed6.TabIndex = 22;
            // 
            // _U0maxLed5
            // 
            this._U0maxLed5.Location = new System.Drawing.Point(8, 63);
            this._U0maxLed5.Name = "_U0maxLed5";
            this._U0maxLed5.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed5.State = BEMN.Forms.LedState.Off;
            this._U0maxLed5.TabIndex = 21;
            // 
            // _U0maxLed4
            // 
            this._U0maxLed4.Location = new System.Drawing.Point(34, 47);
            this._U0maxLed4.Name = "_U0maxLed4";
            this._U0maxLed4.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed4.State = BEMN.Forms.LedState.Off;
            this._U0maxLed4.TabIndex = 6;
            // 
            // _U0maxLed3
            // 
            this._U0maxLed3.Location = new System.Drawing.Point(8, 47);
            this._U0maxLed3.Name = "_U0maxLed3";
            this._U0maxLed3.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed3.State = BEMN.Forms.LedState.Off;
            this._U0maxLed3.TabIndex = 4;
            // 
            // _U0maxLed2
            // 
            this._U0maxLed2.Location = new System.Drawing.Point(34, 31);
            this._U0maxLed2.Name = "_U0maxLed2";
            this._U0maxLed2.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed2.State = BEMN.Forms.LedState.Off;
            this._U0maxLed2.TabIndex = 2;
            // 
            // _U0maxLed1
            // 
            this._U0maxLed1.Location = new System.Drawing.Point(8, 31);
            this._U0maxLed1.Name = "_U0maxLed1";
            this._U0maxLed1.Size = new System.Drawing.Size(13, 13);
            this._U0maxLed1.State = BEMN.Forms.LedState.Off;
            this._U0maxLed1.TabIndex = 0;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(98, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 79;
            this.label41.Text = "��8";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(98, 120);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 77;
            this.label42.Text = "��7";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(98, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 75;
            this.label43.Text = "��6";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(98, 90);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 73;
            this.label44.Text = "��5";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(98, 75);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 71;
            this.label45.Text = "��4";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(98, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 69;
            this.label46.Text = "��3";
            // 
            // _I1Box
            // 
            this._I1Box.Enabled = false;
            this._I1Box.Location = new System.Drawing.Point(149, 19);
            this._I1Box.Name = "_I1Box";
            this._I1Box.Size = new System.Drawing.Size(68, 20);
            this._I1Box.TabIndex = 24;
            // 
            // _I0Box
            // 
            this._I0Box.Enabled = false;
            this._I0Box.Location = new System.Drawing.Point(149, 72);
            this._I0Box.Name = "_I0Box";
            this._I0Box.Size = new System.Drawing.Size(68, 20);
            this._I0Box.TabIndex = 23;
            // 
            // _IcBox
            // 
            this._IcBox.Enabled = false;
            this._IcBox.Location = new System.Drawing.Point(38, 71);
            this._IcBox.Name = "_IcBox";
            this._IcBox.Size = new System.Drawing.Size(68, 20);
            this._IcBox.TabIndex = 22;
            // 
            // _IbBox
            // 
            this._IbBox.Enabled = false;
            this._IbBox.Location = new System.Drawing.Point(38, 45);
            this._IbBox.Name = "_IbBox";
            this._IbBox.Size = new System.Drawing.Size(68, 20);
            this._IbBox.TabIndex = 21;
            // 
            // _IaBox
            // 
            this._IaBox.Enabled = false;
            this._IaBox.Location = new System.Drawing.Point(38, 19);
            this._IaBox.Name = "_IaBox";
            this._IaBox.Size = new System.Drawing.Size(68, 20);
            this._IaBox.TabIndex = 20;
            // 
            // _InBox
            // 
            this._InBox.Enabled = false;
            this._InBox.Location = new System.Drawing.Point(258, 19);
            this._InBox.Name = "_InBox";
            this._InBox.Size = new System.Drawing.Size(68, 20);
            this._InBox.TabIndex = 19;
            // 
            // _U2Box
            // 
            this._U2Box.Enabled = false;
            this._U2Box.Location = new System.Drawing.Point(258, 55);
            this._U2Box.Name = "_U2Box";
            this._U2Box.Size = new System.Drawing.Size(68, 20);
            this._U2Box.TabIndex = 18;
            // 
            // _U1Box
            // 
            this._U1Box.Enabled = false;
            this._U1Box.Location = new System.Drawing.Point(258, 29);
            this._U1Box.Name = "_U1Box";
            this._U1Box.Size = new System.Drawing.Size(68, 20);
            this._U1Box.TabIndex = 17;
            // 
            // _U0Box
            // 
            this._U0Box.Enabled = false;
            this._U0Box.Location = new System.Drawing.Point(258, 81);
            this._U0Box.Name = "_U0Box";
            this._U0Box.Size = new System.Drawing.Size(68, 20);
            this._U0Box.TabIndex = 16;
            // 
            // _UcaBox
            // 
            this._UcaBox.Enabled = false;
            this._UcaBox.Location = new System.Drawing.Point(149, 81);
            this._UcaBox.Name = "_UcaBox";
            this._UcaBox.Size = new System.Drawing.Size(68, 20);
            this._UcaBox.TabIndex = 15;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(98, 45);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(28, 13);
            this.label47.TabIndex = 67;
            this.label47.Text = "��2";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(98, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(28, 13);
            this.label48.TabIndex = 65;
            this.label48.Text = "��1";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(58, 135);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 63;
            this.label33.Text = "�16";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(58, 120);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 61;
            this.label34.Text = "�15";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(58, 105);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 59;
            this.label35.Text = "�14";
            // 
            // _UbcBox
            // 
            this._UbcBox.Enabled = false;
            this._UbcBox.Location = new System.Drawing.Point(149, 55);
            this._UbcBox.Name = "_UbcBox";
            this._UbcBox.Size = new System.Drawing.Size(68, 20);
            this._UbcBox.TabIndex = 14;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this._inL_led8);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this._inL_led7);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this._inL_led6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this._inL_led5);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this._inL_led4);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this._inL_led3);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this._inL_led2);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this._inL_led1);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this._inD_led16);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this._inD_led15);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this._inD_led14);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this._inD_led13);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this._inD_led12);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this._inD_led11);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this._inD_led10);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this._inD_led9);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this._inD_led8);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this._inD_led7);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this._inD_led6);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this._inD_led5);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this._inD_led4);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this._inD_led3);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this._inD_led2);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this._inD_led1);
            this.groupBox5.Location = new System.Drawing.Point(186, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(126, 155);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������� �������";
            // 
            // _inL_led8
            // 
            this._inL_led8.Location = new System.Drawing.Point(85, 135);
            this._inL_led8.Name = "_inL_led8";
            this._inL_led8.Size = new System.Drawing.Size(13, 13);
            this._inL_led8.State = BEMN.Forms.LedState.Off;
            this._inL_led8.TabIndex = 78;
            // 
            // _inL_led7
            // 
            this._inL_led7.Location = new System.Drawing.Point(85, 120);
            this._inL_led7.Name = "_inL_led7";
            this._inL_led7.Size = new System.Drawing.Size(13, 13);
            this._inL_led7.State = BEMN.Forms.LedState.Off;
            this._inL_led7.TabIndex = 76;
            // 
            // _inL_led6
            // 
            this._inL_led6.Location = new System.Drawing.Point(85, 105);
            this._inL_led6.Name = "_inL_led6";
            this._inL_led6.Size = new System.Drawing.Size(13, 13);
            this._inL_led6.State = BEMN.Forms.LedState.Off;
            this._inL_led6.TabIndex = 74;
            // 
            // _inL_led5
            // 
            this._inL_led5.Location = new System.Drawing.Point(85, 90);
            this._inL_led5.Name = "_inL_led5";
            this._inL_led5.Size = new System.Drawing.Size(13, 13);
            this._inL_led5.State = BEMN.Forms.LedState.Off;
            this._inL_led5.TabIndex = 72;
            // 
            // _inL_led4
            // 
            this._inL_led4.Location = new System.Drawing.Point(85, 75);
            this._inL_led4.Name = "_inL_led4";
            this._inL_led4.Size = new System.Drawing.Size(13, 13);
            this._inL_led4.State = BEMN.Forms.LedState.Off;
            this._inL_led4.TabIndex = 70;
            // 
            // _inL_led3
            // 
            this._inL_led3.Location = new System.Drawing.Point(85, 60);
            this._inL_led3.Name = "_inL_led3";
            this._inL_led3.Size = new System.Drawing.Size(13, 13);
            this._inL_led3.State = BEMN.Forms.LedState.Off;
            this._inL_led3.TabIndex = 68;
            // 
            // _inL_led2
            // 
            this._inL_led2.Location = new System.Drawing.Point(85, 45);
            this._inL_led2.Name = "_inL_led2";
            this._inL_led2.Size = new System.Drawing.Size(13, 13);
            this._inL_led2.State = BEMN.Forms.LedState.Off;
            this._inL_led2.TabIndex = 66;
            // 
            // _inL_led1
            // 
            this._inL_led1.Location = new System.Drawing.Point(85, 30);
            this._inL_led1.Name = "_inL_led1";
            this._inL_led1.Size = new System.Drawing.Size(13, 13);
            this._inL_led1.State = BEMN.Forms.LedState.Off;
            this._inL_led1.TabIndex = 64;
            // 
            // _inD_led16
            // 
            this._inD_led16.Location = new System.Drawing.Point(45, 135);
            this._inD_led16.Name = "_inD_led16";
            this._inD_led16.Size = new System.Drawing.Size(13, 13);
            this._inD_led16.State = BEMN.Forms.LedState.Off;
            this._inD_led16.TabIndex = 62;
            // 
            // _inD_led15
            // 
            this._inD_led15.Location = new System.Drawing.Point(45, 120);
            this._inD_led15.Name = "_inD_led15";
            this._inD_led15.Size = new System.Drawing.Size(13, 13);
            this._inD_led15.State = BEMN.Forms.LedState.Off;
            this._inD_led15.TabIndex = 60;
            // 
            // _inD_led14
            // 
            this._inD_led14.Location = new System.Drawing.Point(45, 105);
            this._inD_led14.Name = "_inD_led14";
            this._inD_led14.Size = new System.Drawing.Size(13, 13);
            this._inD_led14.State = BEMN.Forms.LedState.Off;
            this._inD_led14.TabIndex = 58;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(58, 90);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 57;
            this.label36.Text = "�13";
            // 
            // _inD_led13
            // 
            this._inD_led13.Location = new System.Drawing.Point(45, 90);
            this._inD_led13.Name = "_inD_led13";
            this._inD_led13.Size = new System.Drawing.Size(13, 13);
            this._inD_led13.State = BEMN.Forms.LedState.Off;
            this._inD_led13.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(58, 75);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 55;
            this.label37.Text = "�12";
            // 
            // _inD_led12
            // 
            this._inD_led12.Location = new System.Drawing.Point(45, 75);
            this._inD_led12.Name = "_inD_led12";
            this._inD_led12.Size = new System.Drawing.Size(13, 13);
            this._inD_led12.State = BEMN.Forms.LedState.Off;
            this._inD_led12.TabIndex = 54;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(58, 60);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(28, 13);
            this.label38.TabIndex = 53;
            this.label38.Text = "�11";
            // 
            // _inD_led11
            // 
            this._inD_led11.Location = new System.Drawing.Point(45, 60);
            this._inD_led11.Name = "_inD_led11";
            this._inD_led11.Size = new System.Drawing.Size(13, 13);
            this._inD_led11.State = BEMN.Forms.LedState.Off;
            this._inD_led11.TabIndex = 52;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(58, 45);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 51;
            this.label39.Text = "�10";
            // 
            // _inD_led10
            // 
            this._inD_led10.Location = new System.Drawing.Point(45, 45);
            this._inD_led10.Name = "_inD_led10";
            this._inD_led10.Size = new System.Drawing.Size(13, 13);
            this._inD_led10.State = BEMN.Forms.LedState.Off;
            this._inD_led10.TabIndex = 50;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(58, 30);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(22, 13);
            this.label40.TabIndex = 49;
            this.label40.Text = "�9";
            // 
            // _inD_led9
            // 
            this._inD_led9.Location = new System.Drawing.Point(45, 30);
            this._inD_led9.Name = "_inD_led9";
            this._inD_led9.Size = new System.Drawing.Size(13, 13);
            this._inD_led9.State = BEMN.Forms.LedState.Off;
            this._inD_led9.TabIndex = 48;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(21, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "�8";
            // 
            // _inD_led8
            // 
            this._inD_led8.Location = new System.Drawing.Point(7, 135);
            this._inD_led8.Name = "_inD_led8";
            this._inD_led8.Size = new System.Drawing.Size(13, 13);
            this._inD_led8.State = BEMN.Forms.LedState.Off;
            this._inD_led8.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(21, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 45;
            this.label26.Text = "�7";
            // 
            // _inD_led7
            // 
            this._inD_led7.Location = new System.Drawing.Point(7, 120);
            this._inD_led7.Name = "_inD_led7";
            this._inD_led7.Size = new System.Drawing.Size(13, 13);
            this._inD_led7.State = BEMN.Forms.LedState.Off;
            this._inD_led7.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(21, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "�6";
            // 
            // _inD_led6
            // 
            this._inD_led6.Location = new System.Drawing.Point(7, 105);
            this._inD_led6.Name = "_inD_led6";
            this._inD_led6.Size = new System.Drawing.Size(13, 13);
            this._inD_led6.State = BEMN.Forms.LedState.Off;
            this._inD_led6.TabIndex = 42;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(21, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "�5";
            // 
            // _inD_led5
            // 
            this._inD_led5.Location = new System.Drawing.Point(7, 90);
            this._inD_led5.Name = "_inD_led5";
            this._inD_led5.Size = new System.Drawing.Size(13, 13);
            this._inD_led5.State = BEMN.Forms.LedState.Off;
            this._inD_led5.TabIndex = 40;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(21, 75);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "�4";
            // 
            // _inD_led4
            // 
            this._inD_led4.Location = new System.Drawing.Point(7, 75);
            this._inD_led4.Name = "_inD_led4";
            this._inD_led4.Size = new System.Drawing.Size(13, 13);
            this._inD_led4.State = BEMN.Forms.LedState.Off;
            this._inD_led4.TabIndex = 38;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(21, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 37;
            this.label30.Text = "�3";
            // 
            // _inD_led3
            // 
            this._inD_led3.Location = new System.Drawing.Point(7, 60);
            this._inD_led3.Name = "_inD_led3";
            this._inD_led3.Size = new System.Drawing.Size(13, 13);
            this._inD_led3.State = BEMN.Forms.LedState.Off;
            this._inD_led3.TabIndex = 36;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(21, 45);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "�2";
            // 
            // _inD_led2
            // 
            this._inD_led2.Location = new System.Drawing.Point(7, 45);
            this._inD_led2.Name = "_inD_led2";
            this._inD_led2.Size = new System.Drawing.Size(13, 13);
            this._inD_led2.State = BEMN.Forms.LedState.Off;
            this._inD_led2.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(21, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(22, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "�1";
            // 
            // _inD_led1
            // 
            this._inD_led1.Location = new System.Drawing.Point(7, 30);
            this._inD_led1.Name = "_inD_led1";
            this._inD_led1.Size = new System.Drawing.Size(13, 13);
            this._inD_led1.State = BEMN.Forms.LedState.Off;
            this._inD_led1.TabIndex = 32;
            // 
            // _UabBox
            // 
            this._UabBox.Enabled = false;
            this._UabBox.Location = new System.Drawing.Point(149, 29);
            this._UabBox.Name = "_UabBox";
            this._UabBox.Size = new System.Drawing.Size(68, 20);
            this._UabBox.TabIndex = 13;
            // 
            // _UbBox
            // 
            this._UbBox.Enabled = false;
            this._UbBox.Location = new System.Drawing.Point(38, 55);
            this._UbBox.Name = "_UbBox";
            this._UbBox.Size = new System.Drawing.Size(68, 20);
            this._UbBox.TabIndex = 12;
            // 
            // _UaBox
            // 
            this._UaBox.Enabled = false;
            this._UaBox.Location = new System.Drawing.Point(38, 29);
            this._UaBox.Name = "_UaBox";
            this._UaBox.Size = new System.Drawing.Size(68, 20);
            this._UaBox.TabIndex = 11;
            // 
            // _UnBox
            // 
            this._UnBox.Enabled = false;
            this._UnBox.Location = new System.Drawing.Point(364, 55);
            this._UnBox.Name = "_UnBox";
            this._UnBox.Size = new System.Drawing.Size(68, 20);
            this._UnBox.TabIndex = 10;
            // 
            // _F_Box
            // 
            this._F_Box.Enabled = false;
            this._F_Box.Location = new System.Drawing.Point(364, 29);
            this._F_Box.Name = "_F_Box";
            this._F_Box.Size = new System.Drawing.Size(68, 20);
            this._F_Box.TabIndex = 9;
            // 
            // _IgBox
            // 
            this._IgBox.Enabled = false;
            this._IgBox.Location = new System.Drawing.Point(257, 45);
            this._IgBox.Name = "_IgBox";
            this._IgBox.Size = new System.Drawing.Size(68, 20);
            this._IgBox.TabIndex = 8;
            // 
            // _I2Box
            // 
            this._I2Box.Enabled = false;
            this._I2Box.Location = new System.Drawing.Point(149, 45);
            this._I2Box.Name = "_I2Box";
            this._I2Box.Size = new System.Drawing.Size(68, 20);
            this._I2Box.TabIndex = 7;
            // 
            // _UcBox
            // 
            this._UcBox.Enabled = false;
            this._UcBox.Location = new System.Drawing.Point(38, 81);
            this._UcBox.Name = "_UcBox";
            this._UcBox.Size = new System.Drawing.Size(68, 20);
            this._UcBox.TabIndex = 6;
            // 
            // _CosBox
            // 
            this._CosBox.Enabled = false;
            this._CosBox.Location = new System.Drawing.Point(46, 71);
            this._CosBox.Name = "_CosBox";
            this._CosBox.Size = new System.Drawing.Size(68, 20);
            this._CosBox.TabIndex = 27;
            // 
            // _QBox
            // 
            this._QBox.Enabled = false;
            this._QBox.Location = new System.Drawing.Point(46, 45);
            this._QBox.Name = "_QBox";
            this._QBox.Size = new System.Drawing.Size(68, 20);
            this._QBox.TabIndex = 26;
            // 
            // _PBox
            // 
            this._PBox.Enabled = false;
            this._PBox.Location = new System.Drawing.Point(46, 19);
            this._PBox.Name = "_PBox";
            this._PBox.Size = new System.Drawing.Size(68, 20);
            this._PBox.TabIndex = 25;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(9, 204);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(0, 13);
            this.label53.TabIndex = 66;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(28, 123);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(136, 13);
            this.label51.TabIndex = 65;
            this.label51.Text = "������� ��������������";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(28, 81);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(85, 13);
            this.label58.TabIndex = 63;
            this.label58.Text = "������ �������";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(28, 60);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(86, 13);
            this.label62.TabIndex = 61;
            this.label62.Text = "�������������";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(28, 165);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(99, 13);
            this.label61.TabIndex = 59;
            this.label61.Text = "����� ������ ��";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(28, 145);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(99, 13);
            this.label59.TabIndex = 57;
            this.label59.Text = "����� ������ ��";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Controls.Add(this._addIndLed4);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this._addIndLed3);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this._addIndLed2);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this._addIndLed1);
            this.groupBox7.Location = new System.Drawing.Point(533, 272);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(117, 100);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "���.����������";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(27, 79);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(66, 13);
            this.label49.TabIndex = 31;
            this.label49.Text = "���.������";
            // 
            // _addIndLed4
            // 
            this._addIndLed4.Location = new System.Drawing.Point(8, 58);
            this._addIndLed4.Name = "_addIndLed4";
            this._addIndLed4.Size = new System.Drawing.Size(13, 13);
            this._addIndLed4.State = BEMN.Forms.LedState.Off;
            this._addIndLed4.TabIndex = 30;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(27, 58);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(75, 13);
            this.label52.TabIndex = 25;
            this.label52.Text = "���.�������";
            // 
            // _addIndLed3
            // 
            this._addIndLed3.Location = new System.Drawing.Point(8, 79);
            this._addIndLed3.Name = "_addIndLed3";
            this._addIndLed3.Size = new System.Drawing.Size(13, 13);
            this._addIndLed3.State = BEMN.Forms.LedState.Off;
            this._addIndLed3.TabIndex = 24;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(27, 37);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(73, 13);
            this.label54.TabIndex = 21;
            this.label54.Text = "���.�������";
            // 
            // _addIndLed2
            // 
            this._addIndLed2.Location = new System.Drawing.Point(8, 37);
            this._addIndLed2.Name = "_addIndLed2";
            this._addIndLed2.Size = new System.Drawing.Size(13, 13);
            this._addIndLed2.State = BEMN.Forms.LedState.Off;
            this._addIndLed2.TabIndex = 20;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(27, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(78, 13);
            this.label56.TabIndex = 17;
            this.label56.Text = "���.��������";
            // 
            // _addIndLed1
            // 
            this._addIndLed1.Location = new System.Drawing.Point(8, 16);
            this._addIndLed1.Name = "_addIndLed1";
            this._addIndLed1.Size = new System.Drawing.Size(13, 13);
            this._addIndLed1.State = BEMN.Forms.LedState.Off;
            this._addIndLed1.TabIndex = 16;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(20, 193);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(91, 13);
            this.label130.TabIndex = 29;
            this.label130.Text = "�.������������";
            // 
            // _diagTab
            // 
            this._diagTab.Controls.Add(this._analogPage);
            this._diagTab.Controls.Add(this._diskretPage);
            this._diagTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._diagTab.ImageList = this._images;
            this._diagTab.Location = new System.Drawing.Point(0, 0);
            this._diagTab.Name = "_diagTab";
            this._diagTab.SelectedIndex = 0;
            this._diagTab.Size = new System.Drawing.Size(802, 478);
            this._diagTab.TabIndex = 1;
            // 
            // _analogPage
            // 
            this._analogPage.AllowDrop = true;
            this._analogPage.Controls.Add(this.groupBox21);
            this._analogPage.Controls.Add(this.groupBox20);
            this._analogPage.Controls.Add(this.groupBox1);
            this._analogPage.Controls.Add(this._dateTimeControl);
            this._analogPage.Controls.Add(this.groupBox8);
            this._analogPage.Controls.Add(this.groupBox7);
            this._analogPage.Controls.Add(this.groupBox5);
            this._analogPage.Controls.Add(this.groupBox4);
            this._analogPage.Controls.Add(this.groupBox3);
            this._analogPage.Controls.Add(this.groupBox2);
            this._analogPage.ImageIndex = 0;
            this._analogPage.Location = new System.Drawing.Point(4, 23);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogPage.Size = new System.Drawing.Size(794, 451);
            this._analogPage.TabIndex = 0;
            this._analogPage.Text = "���������� �� ";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._QBox);
            this.groupBox21.Controls.Add(this._CosBox);
            this.groupBox21.Controls.Add(this._PBox);
            this.groupBox21.Controls.Add(this.label155);
            this.groupBox21.Controls.Add(this.label156);
            this.groupBox21.Controls.Add(this.label157);
            this.groupBox21.Location = new System.Drawing.Point(8, 178);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(129, 101);
            this.groupBox21.TabIndex = 47;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "��������";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(10, 74);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(30, 13);
            this.label155.TabIndex = 5;
            this.label155.Text = "cos f";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(10, 48);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(15, 13);
            this.label156.TabIndex = 2;
            this.label156.Text = "Q";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(10, 22);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(14, 13);
            this.label157.TabIndex = 0;
            this.label157.Text = "P";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.label144);
            this.groupBox20.Controls.Add(this.label145);
            this.groupBox20.Controls.Add(this.label146);
            this.groupBox20.Controls.Add(this._F_Box);
            this.groupBox20.Controls.Add(this._U2Box);
            this.groupBox20.Controls.Add(this.label147);
            this.groupBox20.Controls.Add(this._U1Box);
            this.groupBox20.Controls.Add(this.label148);
            this.groupBox20.Controls.Add(this._U0Box);
            this.groupBox20.Controls.Add(this.label149);
            this.groupBox20.Controls.Add(this._UcaBox);
            this.groupBox20.Controls.Add(this.label150);
            this.groupBox20.Controls.Add(this._UbcBox);
            this.groupBox20.Controls.Add(this.label151);
            this.groupBox20.Controls.Add(this._UabBox);
            this.groupBox20.Controls.Add(this.label152);
            this.groupBox20.Controls.Add(this._UbBox);
            this.groupBox20.Controls.Add(this._UcBox);
            this.groupBox20.Controls.Add(this.label153);
            this.groupBox20.Controls.Add(this._UaBox);
            this.groupBox20.Controls.Add(this.label154);
            this.groupBox20.Controls.Add(this._UnBox);
            this.groupBox20.Location = new System.Drawing.Point(8, 285);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(449, 112);
            this.groupBox20.TabIndex = 46;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "���������� � �������";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(116, 84);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(27, 13);
            this.label144.TabIndex = 22;
            this.label144.Text = "Uca";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(116, 58);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(27, 13);
            this.label145.TabIndex = 20;
            this.label145.Text = "Ubc";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(116, 32);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(27, 13);
            this.label146.TabIndex = 18;
            this.label146.Text = "Uab";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(337, 58);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(21, 13);
            this.label147.TabIndex = 16;
            this.label147.Text = "Un";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(337, 32);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(13, 13);
            this.label148.TabIndex = 14;
            this.label148.Text = "F";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(231, 84);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(21, 13);
            this.label149.TabIndex = 12;
            this.label149.Text = "U0";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(231, 58);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(21, 13);
            this.label150.TabIndex = 10;
            this.label150.Text = "U2";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(231, 32);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(21, 13);
            this.label151.TabIndex = 8;
            this.label151.Text = "U1";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(11, 84);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(21, 13);
            this.label152.TabIndex = 6;
            this.label152.Text = "Uc";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(11, 58);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(21, 13);
            this.label153.TabIndex = 4;
            this.label153.Text = "Ub";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(11, 32);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(21, 13);
            this.label154.TabIndex = 2;
            this.label154.Text = "Ua";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this._I1Box);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this._I0Box);
            this.groupBox1.Controls.Add(this.label138);
            this.groupBox1.Controls.Add(this._IcBox);
            this.groupBox1.Controls.Add(this.label140);
            this.groupBox1.Controls.Add(this._IbBox);
            this.groupBox1.Controls.Add(this.label141);
            this.groupBox1.Controls.Add(this._IaBox);
            this.groupBox1.Controls.Add(this.label143);
            this.groupBox1.Controls.Add(this._IgBox);
            this.groupBox1.Controls.Add(this._InBox);
            this.groupBox1.Controls.Add(this._I2Box);
            this.groupBox1.Location = new System.Drawing.Point(143, 178);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 101);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "����";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(127, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "I0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Ic";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(236, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "I�";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(127, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "I2";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(236, 22);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(16, 13);
            this.label138.TabIndex = 0;
            this.label138.Text = "In";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(16, 48);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(16, 13);
            this.label140.TabIndex = 2;
            this.label140.Text = "Ib";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(127, 22);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(16, 13);
            this.label141.TabIndex = 0;
            this.label141.Text = "I1";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(16, 22);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(16, 13);
            this.label143.TabIndex = 0;
            this.label143.Text = "Ia";
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct2;
            this._dateTimeControl.Location = new System.Drawing.Point(318, 6);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 44;
            this._dateTimeControl.TimeChanged += new System.Action(this._dateTimeControl_TimeChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox6);
            this.groupBox8.Controls.Add(this._selectConstraintGroupBut);
            this.groupBox8.Controls.Add(this._resetIndicatBut);
            this.groupBox8.Controls.Add(this.label137);
            this.groupBox8.Controls.Add(this._manageLed8);
            this.groupBox8.Controls.Add(this.label53);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this._manageLed5);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this._manageLed4);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Controls.Add(this._manageLed3);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this._manageLed7);
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Controls.Add(this._manageLed6);
            this.groupBox8.Controls.Add(this._resetJA_But);
            this.groupBox8.Controls.Add(this._resetJS_But);
            this.groupBox8.Controls.Add(this._resetFaultBut);
            this.groupBox8.Controls.Add(this._breakerOffBut);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this._breakerOnBut);
            this.groupBox8.Controls.Add(this.label60);
            this.groupBox8.Controls.Add(this._manageLed2);
            this.groupBox8.Controls.Add(this._manageLed1);
            this.groupBox8.Location = new System.Drawing.Point(533, 8);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(248, 258);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "����������� �������(����)";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._manageLed9);
            this.groupBox6.Controls.Add(this.label158);
            this.groupBox6.Controls.Add(this._offSplBtn);
            this.groupBox6.Controls.Add(this._onSplBtn);
            this.groupBox6.Location = new System.Drawing.Point(6, 184);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(236, 69);
            this.groupBox6.TabIndex = 72;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "�������� ��������������� ������";
            // 
            // _manageLed9
            // 
            this._manageLed9.Location = new System.Drawing.Point(6, 35);
            this._manageLed9.Name = "_manageLed9";
            this._manageLed9.Size = new System.Drawing.Size(13, 13);
            this._manageLed9.State = BEMN.Forms.LedState.Off;
            this._manageLed9.TabIndex = 71;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(22, 35);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(69, 13);
            this.label158.TabIndex = 70;
            this.label158.Text = "������ ���";
            // 
            // _offSplBtn
            // 
            this._offSplBtn.Location = new System.Drawing.Point(161, 41);
            this._offSplBtn.Name = "_offSplBtn";
            this._offSplBtn.Size = new System.Drawing.Size(75, 23);
            this._offSplBtn.TabIndex = 68;
            this._offSplBtn.Text = "���������";
            this._offSplBtn.UseVisualStyleBackColor = true;
            this._offSplBtn.Click += new System.EventHandler(this.OffSplBtnClock);
            // 
            // _onSplBtn
            // 
            this._onSplBtn.Location = new System.Drawing.Point(161, 19);
            this._onSplBtn.Name = "_onSplBtn";
            this._onSplBtn.Size = new System.Drawing.Size(75, 23);
            this._onSplBtn.TabIndex = 69;
            this._onSplBtn.Text = "��������";
            this._onSplBtn.UseVisualStyleBackColor = true;
            this._onSplBtn.Click += new System.EventHandler(this.OnSplBtnClock);
            // 
            // _selectConstraintGroupBut
            // 
            this._selectConstraintGroupBut.Location = new System.Drawing.Point(167, 78);
            this._selectConstraintGroupBut.Name = "_selectConstraintGroupBut";
            this._selectConstraintGroupBut.Size = new System.Drawing.Size(75, 23);
            this._selectConstraintGroupBut.TabIndex = 71;
            this._selectConstraintGroupBut.Text = "������.";
            this._selectConstraintGroupBut.UseVisualStyleBackColor = true;
            this._selectConstraintGroupBut.Click += new System.EventHandler(this._selectConstraintGroupBut_Click);
            // 
            // _resetIndicatBut
            // 
            this._resetIndicatBut.Location = new System.Drawing.Point(167, 58);
            this._resetIndicatBut.Name = "_resetIndicatBut";
            this._resetIndicatBut.Size = new System.Drawing.Size(75, 21);
            this._resetIndicatBut.TabIndex = 69;
            this._resetIndicatBut.Text = "��. ���.";
            this._resetIndicatBut.UseVisualStyleBackColor = true;
            this._resetIndicatBut.Click += new System.EventHandler(this._resetIndicatBut_Click);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(28, 101);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(107, 13);
            this.label137.TabIndex = 68;
            this.label137.Text = "��������� �������";
            this.label137.Visible = false;
            // 
            // _manageLed8
            // 
            this._manageLed8.Location = new System.Drawing.Point(12, 165);
            this._manageLed8.Name = "_manageLed8";
            this._manageLed8.Size = new System.Drawing.Size(13, 13);
            this._manageLed8.State = BEMN.Forms.LedState.Off;
            this._manageLed8.TabIndex = 67;
            // 
            // _manageLed5
            // 
            this._manageLed5.Location = new System.Drawing.Point(12, 102);
            this._manageLed5.Name = "_manageLed5";
            this._manageLed5.Size = new System.Drawing.Size(13, 13);
            this._manageLed5.State = BEMN.Forms.LedState.Off;
            this._manageLed5.TabIndex = 64;
            this._manageLed5.Visible = false;
            // 
            // _manageLed4
            // 
            this._manageLed4.Location = new System.Drawing.Point(12, 81);
            this._manageLed4.Name = "_manageLed4";
            this._manageLed4.Size = new System.Drawing.Size(13, 13);
            this._manageLed4.State = BEMN.Forms.LedState.Off;
            this._manageLed4.TabIndex = 62;
            // 
            // _manageLed3
            // 
            this._manageLed3.Location = new System.Drawing.Point(12, 60);
            this._manageLed3.Name = "_manageLed3";
            this._manageLed3.Size = new System.Drawing.Size(13, 13);
            this._manageLed3.State = BEMN.Forms.LedState.Off;
            this._manageLed3.TabIndex = 60;
            // 
            // _manageLed7
            // 
            this._manageLed7.Location = new System.Drawing.Point(12, 144);
            this._manageLed7.Name = "_manageLed7";
            this._manageLed7.Size = new System.Drawing.Size(13, 13);
            this._manageLed7.State = BEMN.Forms.LedState.Off;
            this._manageLed7.TabIndex = 58;
            // 
            // _manageLed6
            // 
            this._manageLed6.Location = new System.Drawing.Point(12, 123);
            this._manageLed6.Name = "_manageLed6";
            this._manageLed6.Size = new System.Drawing.Size(13, 13);
            this._manageLed6.State = BEMN.Forms.LedState.Off;
            this._manageLed6.TabIndex = 56;
            // 
            // _resetJA_But
            // 
            this._resetJA_But.Location = new System.Drawing.Point(167, 160);
            this._resetJA_But.Name = "_resetJA_But";
            this._resetJA_But.Size = new System.Drawing.Size(75, 23);
            this._resetJA_But.TabIndex = 53;
            this._resetJA_But.Text = "��������";
            this._resetJA_But.UseVisualStyleBackColor = true;
            this._resetJA_But.Click += new System.EventHandler(this._resetJA_But_Click);
            // 
            // _resetJS_But
            // 
            this._resetJS_But.Location = new System.Drawing.Point(167, 138);
            this._resetJS_But.Name = "_resetJS_But";
            this._resetJS_But.Size = new System.Drawing.Size(75, 23);
            this._resetJS_But.TabIndex = 52;
            this._resetJS_But.Text = "��������";
            this._resetJS_But.UseVisualStyleBackColor = true;
            this._resetJS_But.Click += new System.EventHandler(this._resetJS_But_Click);
            // 
            // _resetFaultBut
            // 
            this._resetFaultBut.Location = new System.Drawing.Point(167, 116);
            this._resetFaultBut.Name = "_resetFaultBut";
            this._resetFaultBut.Size = new System.Drawing.Size(75, 23);
            this._resetFaultBut.TabIndex = 51;
            this._resetFaultBut.Text = "��������";
            this._resetFaultBut.UseVisualStyleBackColor = true;
            this._resetFaultBut.Click += new System.EventHandler(this._resetFaultBut_Click);
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(167, 35);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 47;
            this._breakerOffBut.Text = "���������";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(28, 39);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(122, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "����������� �������";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(167, 13);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 45;
            this._breakerOnBut.Text = "��������";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(28, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(127, 13);
            this.label60.TabIndex = 38;
            this.label60.Text = "����������� ��������";
            // 
            // _manageLed2
            // 
            this._manageLed2.Location = new System.Drawing.Point(12, 39);
            this._manageLed2.Name = "_manageLed2";
            this._manageLed2.Size = new System.Drawing.Size(13, 13);
            this._manageLed2.State = BEMN.Forms.LedState.Off;
            this._manageLed2.TabIndex = 32;
            // 
            // _manageLed1
            // 
            this._manageLed1.Location = new System.Drawing.Point(12, 18);
            this._manageLed1.Name = "_manageLed1";
            this._manageLed1.Size = new System.Drawing.Size(13, 13);
            this._manageLed1.State = BEMN.Forms.LedState.Off;
            this._manageLed1.TabIndex = 31;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._outLed8);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._outLed7);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._outLed6);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this._outLed5);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this._outLed4);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this._outLed3);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this._outLed2);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this._outLed1);
            this.groupBox4.Location = new System.Drawing.Point(112, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(68, 155);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "���. �������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 135);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "���8";
            // 
            // _outLed8
            // 
            this._outLed8.Location = new System.Drawing.Point(9, 135);
            this._outLed8.Name = "_outLed8";
            this._outLed8.Size = new System.Drawing.Size(13, 13);
            this._outLed8.State = BEMN.Forms.LedState.Off;
            this._outLed8.TabIndex = 46;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "���7";
            // 
            // _outLed7
            // 
            this._outLed7.Location = new System.Drawing.Point(9, 120);
            this._outLed7.Name = "_outLed7";
            this._outLed7.Size = new System.Drawing.Size(13, 13);
            this._outLed7.State = BEMN.Forms.LedState.Off;
            this._outLed7.TabIndex = 44;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "���6";
            // 
            // _outLed6
            // 
            this._outLed6.Location = new System.Drawing.Point(9, 105);
            this._outLed6.Name = "_outLed6";
            this._outLed6.Size = new System.Drawing.Size(13, 13);
            this._outLed6.State = BEMN.Forms.LedState.Off;
            this._outLed6.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "���5";
            // 
            // _outLed5
            // 
            this._outLed5.Location = new System.Drawing.Point(9, 90);
            this._outLed5.Name = "_outLed5";
            this._outLed5.Size = new System.Drawing.Size(13, 13);
            this._outLed5.State = BEMN.Forms.LedState.Off;
            this._outLed5.TabIndex = 40;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "���4";
            // 
            // _outLed4
            // 
            this._outLed4.Location = new System.Drawing.Point(9, 75);
            this._outLed4.Name = "_outLed4";
            this._outLed4.Size = new System.Drawing.Size(13, 13);
            this._outLed4.State = BEMN.Forms.LedState.Off;
            this._outLed4.TabIndex = 38;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(28, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "���3";
            // 
            // _outLed3
            // 
            this._outLed3.Location = new System.Drawing.Point(9, 60);
            this._outLed3.Name = "_outLed3";
            this._outLed3.Size = new System.Drawing.Size(13, 13);
            this._outLed3.State = BEMN.Forms.LedState.Off;
            this._outLed3.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 35;
            this.label23.Text = "���2";
            // 
            // _outLed2
            // 
            this._outLed2.Location = new System.Drawing.Point(9, 45);
            this._outLed2.Name = "_outLed2";
            this._outLed2.Size = new System.Drawing.Size(13, 13);
            this._outLed2.State = BEMN.Forms.LedState.Off;
            this._outLed2.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "���1";
            // 
            // _outLed1
            // 
            this._outLed1.Location = new System.Drawing.Point(9, 30);
            this._outLed1.Name = "_outLed1";
            this._outLed1.Size = new System.Drawing.Size(13, 13);
            this._outLed1.State = BEMN.Forms.LedState.Off;
            this._outLed1.TabIndex = 32;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._releLed8Label);
            this.groupBox3.Controls.Add(this._releLed8);
            this.groupBox3.Controls.Add(this._releLed7Label);
            this.groupBox3.Controls.Add(this._releLed7);
            this.groupBox3.Controls.Add(this._releLed6Label);
            this.groupBox3.Controls.Add(this._releLed6);
            this.groupBox3.Controls.Add(this._releLed5Label);
            this.groupBox3.Controls.Add(this._releLed5);
            this.groupBox3.Controls.Add(this._releLed4Label);
            this.groupBox3.Controls.Add(this._releLed4);
            this.groupBox3.Controls.Add(this._releLed3Label);
            this.groupBox3.Controls.Add(this._releLed3);
            this.groupBox3.Controls.Add(this._releLed2Label);
            this.groupBox3.Controls.Add(this._releLed2);
            this.groupBox3.Controls.Add(this._releLed1Label);
            this.groupBox3.Controls.Add(this._releLed1);
            this.groupBox3.Location = new System.Drawing.Point(57, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(49, 138);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "����";
            // 
            // _releLed8Label
            // 
            this._releLed8Label.AutoSize = true;
            this._releLed8Label.Location = new System.Drawing.Point(27, 120);
            this._releLed8Label.Name = "_releLed8Label";
            this._releLed8Label.Size = new System.Drawing.Size(13, 13);
            this._releLed8Label.TabIndex = 31;
            this._releLed8Label.Text = "8";
            // 
            // _releLed8
            // 
            this._releLed8.Location = new System.Drawing.Point(8, 120);
            this._releLed8.Name = "_releLed8";
            this._releLed8.Size = new System.Drawing.Size(13, 13);
            this._releLed8.State = BEMN.Forms.LedState.Off;
            this._releLed8.TabIndex = 30;
            // 
            // _releLed7Label
            // 
            this._releLed7Label.AutoSize = true;
            this._releLed7Label.Location = new System.Drawing.Point(27, 105);
            this._releLed7Label.Name = "_releLed7Label";
            this._releLed7Label.Size = new System.Drawing.Size(13, 13);
            this._releLed7Label.TabIndex = 29;
            this._releLed7Label.Text = "7";
            // 
            // _releLed7
            // 
            this._releLed7.Location = new System.Drawing.Point(8, 105);
            this._releLed7.Name = "_releLed7";
            this._releLed7.Size = new System.Drawing.Size(13, 13);
            this._releLed7.State = BEMN.Forms.LedState.Off;
            this._releLed7.TabIndex = 28;
            // 
            // _releLed6Label
            // 
            this._releLed6Label.AutoSize = true;
            this._releLed6Label.Location = new System.Drawing.Point(27, 90);
            this._releLed6Label.Name = "_releLed6Label";
            this._releLed6Label.Size = new System.Drawing.Size(13, 13);
            this._releLed6Label.TabIndex = 27;
            this._releLed6Label.Text = "6";
            // 
            // _releLed6
            // 
            this._releLed6.Location = new System.Drawing.Point(8, 90);
            this._releLed6.Name = "_releLed6";
            this._releLed6.Size = new System.Drawing.Size(13, 13);
            this._releLed6.State = BEMN.Forms.LedState.Off;
            this._releLed6.TabIndex = 26;
            // 
            // _releLed5Label
            // 
            this._releLed5Label.AutoSize = true;
            this._releLed5Label.Location = new System.Drawing.Point(27, 75);
            this._releLed5Label.Name = "_releLed5Label";
            this._releLed5Label.Size = new System.Drawing.Size(13, 13);
            this._releLed5Label.TabIndex = 25;
            this._releLed5Label.Text = "5";
            // 
            // _releLed5
            // 
            this._releLed5.Location = new System.Drawing.Point(8, 75);
            this._releLed5.Name = "_releLed5";
            this._releLed5.Size = new System.Drawing.Size(13, 13);
            this._releLed5.State = BEMN.Forms.LedState.Off;
            this._releLed5.TabIndex = 24;
            // 
            // _releLed4Label
            // 
            this._releLed4Label.AutoSize = true;
            this._releLed4Label.Location = new System.Drawing.Point(27, 60);
            this._releLed4Label.Name = "_releLed4Label";
            this._releLed4Label.Size = new System.Drawing.Size(13, 13);
            this._releLed4Label.TabIndex = 23;
            this._releLed4Label.Text = "4";
            // 
            // _releLed4
            // 
            this._releLed4.Location = new System.Drawing.Point(8, 60);
            this._releLed4.Name = "_releLed4";
            this._releLed4.Size = new System.Drawing.Size(13, 13);
            this._releLed4.State = BEMN.Forms.LedState.Off;
            this._releLed4.TabIndex = 22;
            // 
            // _releLed3Label
            // 
            this._releLed3Label.AutoSize = true;
            this._releLed3Label.Location = new System.Drawing.Point(27, 45);
            this._releLed3Label.Name = "_releLed3Label";
            this._releLed3Label.Size = new System.Drawing.Size(13, 13);
            this._releLed3Label.TabIndex = 21;
            this._releLed3Label.Text = "3";
            // 
            // _releLed3
            // 
            this._releLed3.Location = new System.Drawing.Point(8, 45);
            this._releLed3.Name = "_releLed3";
            this._releLed3.Size = new System.Drawing.Size(13, 13);
            this._releLed3.State = BEMN.Forms.LedState.Off;
            this._releLed3.TabIndex = 20;
            // 
            // _releLed2Label
            // 
            this._releLed2Label.AutoSize = true;
            this._releLed2Label.Location = new System.Drawing.Point(27, 30);
            this._releLed2Label.Name = "_releLed2Label";
            this._releLed2Label.Size = new System.Drawing.Size(13, 13);
            this._releLed2Label.TabIndex = 19;
            this._releLed2Label.Text = "2";
            // 
            // _releLed2
            // 
            this._releLed2.Location = new System.Drawing.Point(8, 30);
            this._releLed2.Name = "_releLed2";
            this._releLed2.Size = new System.Drawing.Size(13, 13);
            this._releLed2.State = BEMN.Forms.LedState.Off;
            this._releLed2.TabIndex = 18;
            // 
            // _releLed1Label
            // 
            this._releLed1Label.AutoSize = true;
            this._releLed1Label.Location = new System.Drawing.Point(27, 15);
            this._releLed1Label.Name = "_releLed1Label";
            this._releLed1Label.Size = new System.Drawing.Size(13, 13);
            this._releLed1Label.TabIndex = 17;
            this._releLed1Label.Text = "1";
            // 
            // _releLed1
            // 
            this._releLed1.Location = new System.Drawing.Point(8, 15);
            this._releLed1.Name = "_releLed1";
            this._releLed1.Size = new System.Drawing.Size(13, 13);
            this._releLed1.State = BEMN.Forms.LedState.Off;
            this._releLed1.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this._indLed8);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._indLed7);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._indLed6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this._indLed5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._indLed4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._indLed3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._indLed2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._indLed1);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(43, 138);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "���.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "8";
            // 
            // _indLed8
            // 
            this._indLed8.Location = new System.Drawing.Point(6, 120);
            this._indLed8.Name = "_indLed8";
            this._indLed8.Size = new System.Drawing.Size(13, 13);
            this._indLed8.State = BEMN.Forms.LedState.Off;
            this._indLed8.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "7";
            // 
            // _indLed7
            // 
            this._indLed7.Location = new System.Drawing.Point(6, 105);
            this._indLed7.Name = "_indLed7";
            this._indLed7.Size = new System.Drawing.Size(13, 13);
            this._indLed7.State = BEMN.Forms.LedState.Off;
            this._indLed7.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "6";
            // 
            // _indLed6
            // 
            this._indLed6.Location = new System.Drawing.Point(6, 90);
            this._indLed6.Name = "_indLed6";
            this._indLed6.Size = new System.Drawing.Size(13, 13);
            this._indLed6.State = BEMN.Forms.LedState.Off;
            this._indLed6.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "5";
            // 
            // _indLed5
            // 
            this._indLed5.Location = new System.Drawing.Point(6, 75);
            this._indLed5.Name = "_indLed5";
            this._indLed5.Size = new System.Drawing.Size(13, 13);
            this._indLed5.State = BEMN.Forms.LedState.Off;
            this._indLed5.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "4";
            // 
            // _indLed4
            // 
            this._indLed4.Location = new System.Drawing.Point(6, 60);
            this._indLed4.Name = "_indLed4";
            this._indLed4.Size = new System.Drawing.Size(13, 13);
            this._indLed4.State = BEMN.Forms.LedState.Off;
            this._indLed4.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "3";
            // 
            // _indLed3
            // 
            this._indLed3.Location = new System.Drawing.Point(6, 45);
            this._indLed3.Name = "_indLed3";
            this._indLed3.Size = new System.Drawing.Size(13, 13);
            this._indLed3.State = BEMN.Forms.LedState.Off;
            this._indLed3.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "2";
            // 
            // _indLed2
            // 
            this._indLed2.Location = new System.Drawing.Point(6, 30);
            this._indLed2.Name = "_indLed2";
            this._indLed2.Size = new System.Drawing.Size(13, 13);
            this._indLed2.State = BEMN.Forms.LedState.Off;
            this._indLed2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "1";
            // 
            // _indLed1
            // 
            this._indLed1.Location = new System.Drawing.Point(6, 15);
            this._indLed1.Name = "_indLed1";
            this._indLed1.Size = new System.Drawing.Size(13, 13);
            this._indLed1.State = BEMN.Forms.LedState.Off;
            this._indLed1.TabIndex = 0;
            // 
            // _diskretPage
            // 
            this._diskretPage.Controls.Add(this.groupBox19);
            this._diskretPage.Controls.Add(this.groupBox18);
            this._diskretPage.Controls.Add(this.groupBox17);
            this._diskretPage.Controls.Add(this.groupBox16);
            this._diskretPage.Controls.Add(this.groupBox15);
            this._diskretPage.Controls.Add(this.groupBox13);
            this._diskretPage.Controls.Add(this.groupBox14);
            this._diskretPage.Controls.Add(this.groupBox11);
            this._diskretPage.Controls.Add(this.groupBox12);
            this._diskretPage.Controls.Add(this.groupBox10);
            this._diskretPage.Controls.Add(this.groupBox9);
            this._diskretPage.ImageIndex = 1;
            this._diskretPage.Location = new System.Drawing.Point(4, 23);
            this._diskretPage.Name = "_diskretPage";
            this._diskretPage.Padding = new System.Windows.Forms.Padding(3);
            this._diskretPage.Size = new System.Drawing.Size(794, 451);
            this._diskretPage.TabIndex = 1;
            this._diskretPage.Text = "���������� ��";
            this._diskretPage.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label129);
            this.groupBox19.Controls.Add(this._faultSignalLed16);
            this.groupBox19.Controls.Add(this.label130);
            this.groupBox19.Controls.Add(this._faultSignalLed15);
            this.groupBox19.Controls.Add(this.label131);
            this.groupBox19.Controls.Add(this._faultSignalLed14);
            this.groupBox19.Controls.Add(this.label132);
            this.groupBox19.Controls.Add(this._faultSignalLed13);
            this.groupBox19.Controls.Add(this.label133);
            this.groupBox19.Controls.Add(this._faultSignalLed12);
            this.groupBox19.Controls.Add(this.label134);
            this.groupBox19.Controls.Add(this._faultSignalLed11);
            this.groupBox19.Controls.Add(this.label135);
            this.groupBox19.Controls.Add(this._faultSignalLed10);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this._faultSignalLed9);
            this.groupBox19.Location = new System.Drawing.Point(454, 112);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(115, 243);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "������� ������������� 2";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(21, 218);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(84, 13);
            this.label129.TabIndex = 31;
            this.label129.Text = "�. ������ ���";
            // 
            // _faultSignalLed16
            // 
            this._faultSignalLed16.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed16.Name = "_faultSignalLed16";
            this._faultSignalLed16.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed16.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed16.TabIndex = 30;
            // 
            // _faultSignalLed15
            // 
            this._faultSignalLed15.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed15.Name = "_faultSignalLed15";
            this._faultSignalLed15.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed15.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed15.TabIndex = 28;
            // 
            // _faultSignalLed14
            // 
            this._faultSignalLed14.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed14.Name = "_faultSignalLed14";
            this._faultSignalLed14.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed14.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed14.TabIndex = 26;
            // 
            // _faultSignalLed13
            // 
            this._faultSignalLed13.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed13.Name = "_faultSignalLed13";
            this._faultSignalLed13.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed13.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed13.TabIndex = 24;
            // 
            // _faultSignalLed12
            // 
            this._faultSignalLed12.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed12.Name = "_faultSignalLed12";
            this._faultSignalLed12.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed12.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed12.TabIndex = 22;
            // 
            // _faultSignalLed11
            // 
            this._faultSignalLed11.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed11.Name = "_faultSignalLed11";
            this._faultSignalLed11.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed11.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed11.TabIndex = 20;
            // 
            // _faultSignalLed10
            // 
            this._faultSignalLed10.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed10.Name = "_faultSignalLed10";
            this._faultSignalLed10.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed10.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed10.TabIndex = 18;
            // 
            // _faultSignalLed9
            // 
            this._faultSignalLed9.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed9.Name = "_faultSignalLed9";
            this._faultSignalLed9.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed9.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed9.TabIndex = 16;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(27, 150);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(19, 13);
            this.label139.TabIndex = 35;
            this.label139.Text = "10";
            // 
            // label14_releLed9Label0
            // 
            this.label14_releLed9Label0.AutoSize = true;
            this.label14_releLed9Label0.Location = new System.Drawing.Point(27, 165);
            this.label14_releLed9Label0.Name = "label14_releLed9Label0";
            this.label14_releLed9Label0.Size = new System.Drawing.Size(19, 13);
            this.label14_releLed9Label0.TabIndex = 37;
            this.label14_releLed9Label0.Text = "11";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(27, 195);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(19, 13);
            this.label142.TabIndex = 41;
            this.label142.Text = "13";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "6";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "8";
            // 
            // Mr700MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 478);
            this.Controls.Add(this._diagTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mr700MeasuringForm";
            this.Text = "TZL_MeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._diagTab.ResumeLayout(false);
            this._analogPage.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._diskretPage.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _extDefenseLed6;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _extDefenseLed5;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _extDefenseLed4;
        private System.Windows.Forms.Label label110;
        private BEMN.Forms.LedControl _extDefenseLed3;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _extDefenseLed2;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _FmaxLed8;
        private BEMN.Forms.LedControl _FmaxLed7;
        private BEMN.Forms.LedControl _FmaxLed6;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _faultStateLed2;
        private BEMN.Forms.LedControl _extDefenseLed8;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.GroupBox groupBox16;
        private BEMN.Forms.LedControl _extDefenseLed7;
        private BEMN.Forms.LedControl _extDefenseLed1;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _faultStateLed1;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _autoLed4;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _autoLed3;
        private System.Windows.Forms.Label label103;
        private BEMN.Forms.LedControl _FmaxLed5;
        private BEMN.Forms.LedControl _autoLed2;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.LedControl _autoLed1;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _autoLed8;
        private System.Windows.Forms.Label label98;
        private BEMN.Forms.LedControl _autoLed7;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label99;
        private BEMN.Forms.LedControl _autoLed6;
        private System.Windows.Forms.Label label100;
        private BEMN.Forms.LedControl _autoLed5;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _FmaxLed4;
        private BEMN.Forms.LedControl _FmaxLed3;
        private BEMN.Forms.LedControl _FmaxLed2;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _FmaxLed1;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _faultStateLed8;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _faultStateLed7;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _faultSignalLed8;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _faultSignalLed7;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _faultSignalLed6;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _faultSignalLed5;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _faultSignalLed4;
        private System.Windows.Forms.Label label126;
        private BEMN.Forms.LedControl _faultSignalLed3;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _faultSignalLed2;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _faultSignalLed1;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label131;
        private BEMN.Forms.LedControl _faultSignalLed12;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _faultSignalLed11;
        private System.Windows.Forms.Label label135;
        private BEMN.Forms.LedControl _faultSignalLed10;
        private System.Windows.Forms.Label label136;
        private BEMN.Forms.LedControl _faultSignalLed9;
        private BEMN.Forms.LedControl _faultSignalLed14;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _faultStateLed4;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _faultSignalLed13;
        private BEMN.Forms.LedControl _faultStateLed6;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label133;
        private BEMN.Forms.LedControl _faultStateLed5;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.GroupBox groupBox17;
        private BEMN.Forms.LedControl _faultStateLed3;
        private BEMN.Forms.LedControl _UmaxLed8;
        private BEMN.Forms.LedControl _UmaxLed7;
        private BEMN.Forms.LedControl _UmaxLed6;
        private BEMN.Forms.LedControl _UmaxLed5;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _UmaxLed4;
        private BEMN.Forms.LedControl _UmaxLed3;
        private BEMN.Forms.LedControl _UmaxLed2;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _UmaxLed1;
        private BEMN.Forms.LedControl _I0maxLed5;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _I0maxLed4;
        private BEMN.Forms.LedControl _I0maxLed3;
        private BEMN.Forms.LedControl _I0maxLed2;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _I0maxLed1;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _ImaxLed4;
        private System.Windows.Forms.ImageList _images;
        private BEMN.Forms.LedControl _ImaxLed3;
        private BEMN.Forms.LedControl _ImaxLed2;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _ImaxLed1;
        private System.Windows.Forms.Label label73;
        private BEMN.Forms.LedControl _ImaxLed8;
        private BEMN.Forms.LedControl _ImaxLed7;
        private BEMN.Forms.LedControl _ImaxLed6;
        private BEMN.Forms.LedControl _ImaxLed5;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _I0maxLed6;
        private BEMN.Forms.LedControl _InmaxLed8;
        private BEMN.Forms.LedControl _InmaxLed7;
        private BEMN.Forms.LedControl _InmaxLed6;
        private BEMN.Forms.LedControl _InmaxLed5;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private BEMN.Forms.LedControl _InmaxLed4;
        private System.Windows.Forms.GroupBox groupBox14;
        private BEMN.Forms.LedControl _InmaxLed3;
        private BEMN.Forms.LedControl _InmaxLed2;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _InmaxLed1;
        private BEMN.Forms.LedControl _I0maxLed8;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _U0maxLed4;
        private BEMN.Forms.LedControl _U0maxLed3;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _I0maxLed7;
        private BEMN.Forms.LedControl _U0maxLed2;
        private System.Windows.Forms.Label label78;
        private BEMN.Forms.LedControl _U0maxLed1;
        private BEMN.Forms.LedControl _U0maxLed8;
        private BEMN.Forms.LedControl _U0maxLed7;
        private BEMN.Forms.LedControl _U0maxLed6;
        private BEMN.Forms.LedControl _U0maxLed5;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _inL_led8;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _inL_led7;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _inL_led6;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _inL_led5;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _inL_led4;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _inL_led3;
        private System.Windows.Forms.TextBox _I1Box;
        private System.Windows.Forms.TextBox _I0Box;
        private System.Windows.Forms.TextBox _IcBox;
        private System.Windows.Forms.TextBox _IbBox;
        private System.Windows.Forms.TextBox _IaBox;
        private System.Windows.Forms.TextBox _InBox;
        private System.Windows.Forms.TextBox _U2Box;
        private System.Windows.Forms.TextBox _U1Box;
        private System.Windows.Forms.TextBox _U0Box;
        private System.Windows.Forms.TextBox _UcaBox;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _inL_led2;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _inL_led1;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _inD_led16;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _inD_led15;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _inD_led14;
        private System.Windows.Forms.TextBox _UbcBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _inD_led13;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _inD_led12;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _inD_led11;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _inD_led10;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _inD_led9;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _inD_led8;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _inD_led7;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _inD_led6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _inD_led5;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _inD_led4;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _inD_led3;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _inD_led2;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _inD_led1;
        private System.Windows.Forms.TextBox _UabBox;
        private System.Windows.Forms.TextBox _UbBox;
        private System.Windows.Forms.TextBox _UaBox;
        private System.Windows.Forms.TextBox _UnBox;
        private System.Windows.Forms.TextBox _F_Box;
        private System.Windows.Forms.TextBox _IgBox;
        private System.Windows.Forms.TextBox _I2Box;
        private System.Windows.Forms.TextBox _UcBox;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _manageLed5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _manageLed4;
        private System.Windows.Forms.Label label62;
        private BEMN.Forms.LedControl _manageLed3;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _manageLed7;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _manageLed6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _addIndLed4;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _addIndLed3;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _addIndLed2;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _addIndLed1;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TabControl _diagTab;
        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _resetIndicatBut;
        private System.Windows.Forms.Label label137;
        private BEMN.Forms.LedControl _manageLed8;
        private System.Windows.Forms.Button _resetJA_But;
        private System.Windows.Forms.Button _resetJS_But;
        private System.Windows.Forms.Button _resetFaultBut;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _manageLed2;
        private BEMN.Forms.LedControl _manageLed1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _outLed8;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _outLed7;
        private System.Windows.Forms.Label label19;
        private BEMN.Forms.LedControl _outLed6;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _outLed5;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _outLed4;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _outLed3;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _outLed2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _outLed1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _releLed8Label;
        private BEMN.Forms.LedControl _releLed8;
        private System.Windows.Forms.Label _releLed7Label;
        private BEMN.Forms.LedControl _releLed7;
        private System.Windows.Forms.Label _releLed6Label;
        private BEMN.Forms.LedControl _releLed6;
        private System.Windows.Forms.Label _releLed5Label;
        private BEMN.Forms.LedControl _releLed5;
        private System.Windows.Forms.Label _releLed4Label;
        private BEMN.Forms.LedControl _releLed4;
        private System.Windows.Forms.Label _releLed3Label;
        private BEMN.Forms.LedControl _releLed3;
        private System.Windows.Forms.Label _releLed2Label;
        private BEMN.Forms.LedControl _releLed2;
        private System.Windows.Forms.Label _releLed1Label;
        private BEMN.Forms.LedControl _releLed1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _indLed8;
        private System.Windows.Forms.Label label6;
        private BEMN.Forms.LedControl _indLed7;
        private System.Windows.Forms.Label label7;
        private BEMN.Forms.LedControl _indLed6;
        private System.Windows.Forms.Label label8;
        private BEMN.Forms.LedControl _indLed5;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _indLed4;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _indLed3;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _indLed2;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _indLed1;
        private System.Windows.Forms.TabPage _diskretPage;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label129;
        private BEMN.Forms.LedControl _faultSignalLed16;
        private BEMN.Forms.LedControl _faultSignalLed15;
        private System.Windows.Forms.Button _selectConstraintGroupBut;
        private System.Windows.Forms.TextBox _CosBox;
        private System.Windows.Forms.TextBox _QBox;
        private System.Windows.Forms.TextBox _PBox;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label14_releLed9Label0;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.GroupBox groupBox6;
        private BEMN.Forms.LedControl _manageLed9;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Button _offSplBtn;
        private System.Windows.Forms.Button _onSplBtn;

    }
}