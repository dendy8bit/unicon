﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR700.Version2.Configuration;

namespace BEMN.MR700.Version2.Measuring.Structures
{
  public  class GroupSetpointBaseStruct:StructBase
  {
    [Layout(0)]  private ushort _group;

      public string GetGroup(double version)
      {
          if (version < 1.1)
          {
              return Validator.Get(_group, StringsConfig.SetpointsNamesV1);
          }
          return Validator.Get(_group, StringsConfig.SetpointsNames);

      }
       public void SetGroup(double version,string value)
      {
           if (version < 1.1)
           {
               _group = Validator.Set(value, StringsConfig.SetpointsNamesV1); 
           }
           else
           {
              _group = Validator.Set(value, StringsConfig.SetpointsNames);  
           }
           
      }
  }
}
