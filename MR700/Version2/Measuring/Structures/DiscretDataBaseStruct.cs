﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR700.Version2.Measuring.Structures
{
    /// <summary>
    /// МР 550 Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct :StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 28;

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = BASE_SIZE)]
        private ushort[] _base; //бд общаяя

        #endregion [Private fields]


      


        #region [Properties]

        private string GetSign(int higt, int low)
        {
            if (Common.GetBit(this._base[17], higt))
            {
                return string.Empty;
            }
            return Common.GetBit(this._base[17], low) ? "-" : "+";
        }

        public string InSign
        {
            get { return GetSign(1, 0); }
        }

        public string IaSign
        {
            get { return GetSign(3, 2); }
        }

        public string IbSign
        {
            get { return GetSign(5, 4); }
        }

        public string IcSign
        {
            get { return GetSign(7, 6); }
        }

        public string I0Sign
        {
            get { return GetSign(9, 8); }
        }

        public string I1Sign
        {
            get { return GetSign(11, 10); }
        }
        public string I2Sign
        {
            get { return GetSign(13, 12); }
        }
        public BitArray ManageSignals
        {
            get
            {
                bool[] manageSign = Common.GetBitsArray(_base, 0, 9);
                manageSign[9] &= !this.FaultSignals[15]; // работа СПЛ
                return new BitArray(manageSign);
            }
        }

        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new byte[] { Common.LOBYTE(_base[2]) });
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }

        public BitArray Indicators
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_base[2]) });
            }
        }

        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[0x9]),
                                                 Common.HIBYTE(_base[0x9]),
                                                 Common.LOBYTE(_base[0xA])});
            }
        }

        public BitArray OutputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_base[0xA]) });
            }
        }

        public BitArray Rele
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[3])});
            }
        }

        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]{/*Common.HIBYTE(_base[0xA]),*/
                                               Common.LOBYTE(_base[0xB]),
                                               Common.HIBYTE(_base[0xB]),
                                               Common.LOBYTE(_base[0xC]),
                                               Common.HIBYTE(_base[0xC]),
                                               Common.LOBYTE(_base[0xD]), 
                                               Common.HIBYTE(_base[0xD]),
                                               Common.LOBYTE(_base[0xE])});
            }
        }

        public BitArray Automation
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_base[8]) });
            }
        }

        public BitArray FaultSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[5]), Common.HIBYTE(_base[5]) });
            }
        }

        public BitArray FaultState
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_base[4]) });
            }
        }

        /*

        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get
            {
                return Common.GetBitsArray(this._base, 144, 151);
            }
        }
        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals
        {
            get
            {
                return Common.GetBitsArray(this._base, 160, 167);
            }
        }
        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 168, 175); }
        }
        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses
        {
            get { return Common.GetBitsArray(this._base, 224, 231); }
        }

        /// <summary>
        /// Защиты I
        /// </summary>
        public bool[] MaximumCurrent
        {
            get { return Common.GetBitsArray(this._base, 176, 199); }
        }
        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic
        {
            get { return Common.GetBitsArray(this._base, 232, 255); }
        } 
        /*  
        /// <summary>
        /// U,F,Q
        /// </summary>
        public bool[] Voltage
        {
            get { return Common.GetBitsArray(this._base, 104, 137); }
        }
     
    /*   
        /// <summary>
        /// УРОВ
        /// </summary>
        public bool[] Urov
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 186, 204); }
        }
        /// <summary>
        /// Состояния
        /// </summary>
        public bool[] State
        {
            get
            {
                List<bool> res = new List<bool>();
                res.AddRange(Common.GetBitsArray(new BitArray(this._base), 191, 195));
                res.AddRange(Common.GetBitsArray(new BitArray(this._base), 259, 260));
                return res.ToArray();
            }
        }
      */ 
   /*     /// <summary>
        /// Реле
        /// </summary>
        public bool[] FaultSignals
        {
            get { return Common.GetBitsArray(this._base, 80, 95); }
        }
        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] Indicators
        {
            get { return Common.GetBitsArray(this._base, 40, 47); }
        } 
        /// <summary>
        /// Автоматика
        /// </summary>
        public bool[] Automatics
        {
            get { return Common.GetBitsArray(this._base, 136, 143); }
        }
        
        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults
        {
            get { return Common.GetBitsArray(this._base, 64, 71); }
        }

        public bool[] Manage
        {
            get
            {
                return new bool[]
                    {
Common.GetBit(this._base[0],0),
Common.GetBit(this._base[0],1),
Common.GetBit(this._base[0],6),
Common.GetBit(this._base[0],7),
Common.GetBit(this._base[0],2),
                    };
            }
        }

        /*  /// <summary>
        /// Контроль
        /// </summary>
        public bool[] ControlSignals
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 254, 257); }
        }
       
        

        /// <summary>
        /// Направления токов
        /// </summary>
        public string[] CurrentsSymbols
        {
            get
            {
              
                    var booleanArray = Common.GetBitsArray(new BitArray(this._param), 0, 13);
                    var result = new string[7];
                    for (int i = 0; i < 14; i += 2)
                    {
                        result[i/2] = this.GetCurrentSymbol(booleanArray[i], booleanArray[i + 1]);
                    }
                    return result;
              

            }
        }

        private string GetCurrentSymbol(bool symbol, bool error)
        {
            return error ? string.Empty : (symbol ? "-" : "+");
        }

        /// <summary>
        /// Неисправность логики
        /// </summary>
        private bool FaultLogic
        {
            get
            {
                return Common.GetBit(this._alarm[1], 7) | //ошибка CRC констант программы логики
                       Common.GetBit(this._alarm[2], 0) | //ошибка CRC разрешения программы логики
                       Common.GetBit(this._alarm[2], 1) | //ошибка CRC программы логики
                       Common.GetBit(this._alarm[2], 2) | //ошибка CRC меню логики
                       Common.GetBit(this._alarm[2], 3); //ошибка в ходе выполнения программы логики
            }
        }

        */
        #endregion [Properties]
      

    }
}
