﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.MR700.Version2.SystemJournal.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SystemJournalStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public SystemJournalRecordStruct[] _records;

        public SystemJournalStruct(bool a)
        {
            this._records = new SystemJournalRecordStruct[128];
        }

        public StructInfo GetStructInfo(int len)
        {
            return new StructInfo { FullSize = 8 * 128, SlotsArray = true };
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            const int slotLenght = 8;
            int arrayLength = 128;
            Device.slot[] slots = new Device.slot[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                slots[i] = new Device.slot(start, (ushort)(start + slotLenght));
                start += slotLenght * 2;
            }
            return slots;
        }

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this._records = new SystemJournalRecordStruct[128];
            for (int i = 0; i < this._records.Length; i++)
            {
                this._records[i] = new SystemJournalRecordStruct();
                byte[] oneStruct = new byte[this._records[i].GetSize()];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this._records[i].InitStruct(oneStruct);
                index += this._records[0].GetSize();
            }
        }

        public ushort[] GetValues()
        {
            throw new NotImplementedException();
        }
    }

}
