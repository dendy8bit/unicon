﻿using System.Collections.Generic;

namespace BEMN.MR700.Version2.AlarmJournal
{
    public static class AjStrings
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Основная",
                        "Резервная"
                    };
            }
        }
        /// <summary>
        /// Сработавшая ступень
        /// </summary>
        public static List<string> Stage
        {
            get
            {
                return new List<string>
                    {
                        "",
                        "I>",
                        "I>>",
                        "I>>>",
                        "I>>>>",
                        "I2>",
                        "I2>>",
                        "I0>",
                        "I0>>",
                        "In>",
                        "In>>",
                        "Iг>",
                        "I2/I1", //12
                        "F>",
                        "F>>",
                        "F<",
                        "F<<",
                        "U>",
                        "U>>",
                        "U<",
                        "U<<",
                        "U2>",
                        "U2>>",
                        "U0>",
                        "U0>>",
                        "ВЗ-1",
                        "ВЗ-2",
                        "ВЗ-3",
                        "ВЗ-4",
                        "ВЗ-5",
                        "ВЗ-6",
                        "ВЗ-7",
                        "ВЗ-8",
                        ""
                    };
            }
        }
        /// <summary>
        /// Сообщение
        /// </summary>
        public static List<string> Message
        {
            get
            {
                return new List<string>
                    {
                        "Журнал пуст",
                        "Сигнализация",
                        "Отключение",
                        "Работа",
                        "Неуспешное АПВ",
                        "Возврат",
                        "Включение",
                        "ОМП"
                    };
            }
        }

        /// <summary>
        /// Параметр срабатывания
        /// </summary>
        public static List<string> Parametr
        {
           get
           {
               return new List<string>
                   {
                       "", // 0
                       "Iг", // 1
                       "In", // 2
                       "Ia", // 3
                       "Ib", // 4
                       "Ic", // 5
                       "I0", // 6
                       "I1", // 7
                       "I2", // 8
                       "Pn", // 9
                       "Pa", // 10
                       "Pb", // 11
                       "Pc", // 12
                       "P0", // 13
                       "P1", // 14
                       "P2", // 15
                       "F", // 16
                       "Un", // 17
                       "Ua ", // 18
                       "Ub ", // 19
                       "Uc ", // 20
                       "U0 ", // 21
                       "U1", // 22
                       "U2", // 23
                       "Uab", // 24
                       "Ubc", // 25
                       "Uca", // 26
                       "Обрыв провода", // 27
                       "Lкз"
                   };
           }
        }

    }
}
