﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR700.Version2.Configuration.Structures.Vls
{
    /// <summary>
    /// конфигурациия выходных логических сигналов
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Конфигурация_ВЛС")]
    public class OutputLogicSignalStruct : StructBase, IXmlSerializable
    {
        #region [Private fields]
        public const int LOGIC_COUNT = 8;

        [XmlArray(ElementName = "Все_ВЛС")] [Layout(0, Count = LOGIC_COUNT)] private OutputLogicStruct[] _logicSignals;

        #endregion [Private fields]
        [BindingProperty(0)]
        [XmlIgnore]
        public OutputLogicStruct this[int index]
        {
            get { return this._logicSignals[index]; }
            set { this._logicSignals[index] = value; }
        }



        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < LOGIC_COUNT; i++)
            {

                writer.WriteStartElement(string.Format("ВЛС"));
                this[i].WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}
