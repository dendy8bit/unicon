﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.MR700.Version2.Configuration.Structures.Vls
{
    /// <summary>
    /// Конфигурациия выходных логических сигналов
    /// </summary>

    [XmlRoot(ElementName = "Конфигурация_одного_ВЛС")]
    public class OutputLogicStruct : StructBase, IBitField, IXmlSerializable
    {
        #region [Constants]
        public const int LOGIC_COUNT = 8; 
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = LOGIC_COUNT)]
        private ushort[] _logicSignals; 
        #endregion [Private fields]


        #region [Properties]
         [XmlIgnore]
        public BitArray Bits
        {
            get
            {
                var mass = Common.TOBYTES(this._logicSignals, false);
                var result = new BitArray(mass);
                return result;
            }

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    int x = i / 16;
                    int y = i % 16;
                    this._logicSignals[x] = Common.SetBit(this._logicSignals[x], y, value[i]);
                }
            }
        }
            [XmlIgnore]
        public string[] BitsXml
        {
            get
            {
                
                List<string> res = new List<string>();
                var bits = this.Bits;
                for (int i = 0; i < bits.Count; i++)
                {
                    if (bits[i])
                    {
                        res.Add(StringsConfig.VlsSignalsV2[i]);  
                    }
                    
                }
                if (res.Count == 0)
                {
                    res.Add("НЕТ");
                }
               
                return res.ToArray();
            }
            set
            {
                
            }
        }

 
        #endregion [Properties]




        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            bool flag = false;
            for (int i = 0; i < StringsConfig.VlsSignalsV2.Count; i++)
            {
                if (this.Bits[i])
                {
                    flag = true;
                    writer.WriteElementString("Выбрано", StringsConfig.VlsSignalsV2[i]);
                }
                
            }
            if (!flag)
            {
                writer.WriteElementString("Выбрано", "НЕТ");
            }
        }
    }
}
