﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR700.Version2.Configuration.Structures.CurrentDefenses
{
    /// <summary>
    /// конфигурациия для одной стороны углов max чуствительности
    /// </summary>
    [XmlRoot(ElementName = "Углы")]
    public class CornerStruct : StructBase 
    {
        #region [Public fields]

        /// <summary>
        /// I
        /// </summary>
        [Layout(0)] private ushort _c;
        /// <summary>
        /// угол для расчета по I0
        /// </summary>
        [Layout(1)]
        private ushort _c0;
  /// <summary>
        /// угол для расчета по I2
        /// </summary>
        [Layout(2)] private ushort _c2;
        /// <summary>
        /// угол для расчета по In
        /// </summary>
       [Layout(3)] private ushort _cn;

      

      

        #endregion [Public fields]

        /// <summary>
        /// I
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "I")]
        public ushort C
        {
            get { return this._c; }
            set { this._c = value; }
        }

        /// <summary>
        /// угол для расчета по I0
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "I0")]
        public ushort C0
        {
            get { return this._c0; }
            set { this._c0 = value; }
        }

        /// <summary>
        /// угол для расчета по In
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "I2")]
        public ushort Cn
        {
            get { return this._cn; }
            set { this._cn = value; }
        }

        /// <summary>
        /// угол для расчета по I2
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "In")]
        public ushort C2
        {
            get { return this._c2; }
            set { this._c2 = value; }
        }
    }
}
