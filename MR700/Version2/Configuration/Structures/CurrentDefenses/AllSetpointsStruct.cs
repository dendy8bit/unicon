﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR700.Version2.Configuration.Structures.CurrentDefenses
{
    public class AllSetpointsStruct : StructBase, ISetpointContainer<SetpointStruct>
    {

         [Layout(0)]
        private CornerStruct _corner1;
        [Layout(1)]
        private AllCurrentDefensesStruct _currentDefense1;
        [Layout(2)]
        private CornerStruct _corner2;
        [Layout(3)]
        private AllCurrentDefensesStruct _currentDefense2;

   [XmlIgnore]
        public SetpointStruct[] Setpoints
        {
            get
            {
                var result = new[]
                    {
                        new SetpointStruct
                            {
                                Corners = _corner1.Clone<CornerStruct>(),
                                CurrentDefenses = _currentDefense1.Clone<AllCurrentDefensesStruct>()

                            },
                        new SetpointStruct
                            {
                                Corners = _corner2.Clone<CornerStruct>(),
                                CurrentDefenses = _currentDefense2.Clone<AllCurrentDefensesStruct>()
                            }
                    };
               
                return result;
            }
            set
            {
                _corner1 = value[0].Corners;
                _currentDefense1 = value[0].CurrentDefenses;
             

                _corner2 = value[1].Corners;
                _currentDefense2 = value[1].CurrentDefenses;

            }
        }

        [XmlElement(ElementName = "Основная")]
        public SetpointStruct Main
        {
            get
            {
                return new SetpointStruct
                    {
                        Corners = _corner1.Clone<CornerStruct>(),
                        CurrentDefenses = _currentDefense1.Clone<AllCurrentDefensesStruct>()

                    };
            }
            set
            {
                _corner1 = value.Corners;
                _currentDefense1 = value.CurrentDefenses;

            }
        }

        [XmlElement(ElementName = "Резервная")]
        public SetpointStruct Reserv
        {
            get
            {
                return new SetpointStruct
                    {
                        Corners = _corner2.Clone<CornerStruct>(),
                        CurrentDefenses = _currentDefense2.Clone<AllCurrentDefensesStruct>()
                    };
            }
            set
            {
                _corner2 = value.Corners;
                _currentDefense2 = value.CurrentDefenses;
            }
        }
    }
}
