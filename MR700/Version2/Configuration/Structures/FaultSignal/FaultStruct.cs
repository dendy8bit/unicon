﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR700.Version2.Configuration.Structures.FaultSignal
{
    public class FaultStruct : StructBase
    {
        [Layout(0)] private FaultSignalStruct _signals;
        [Layout(1)] private ushort _time;
        [Layout(2, Count = 3)] private ushort[] _res;

        [BindingProperty(0)]
        public FaultSignalStruct Signals
        {
            get { return _signals; }
            set { _signals = value; }
        }
        [XmlAttribute(AttributeName = "Импульс_реле_неисправность")]
        [BindingProperty(1)]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(_time) ; }
            set { _time =ValuesConverterCommon.SetWaitTime(value) ; }
        }
    }
}