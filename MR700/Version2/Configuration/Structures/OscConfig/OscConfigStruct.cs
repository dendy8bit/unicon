﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR700.Version2.Configuration.Structures.OscConfig
{
    public class OscConfigStruct : StructBase
    {
        [Layout(0)] private ushort _oscConf;
        
        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return (ushort)(Common.GetBits(this._oscConf, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._oscConf = Common.SetBits(this._oscConf, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string FixationXml
        {
            get { return Validator.Get(this._oscConf, StringsConfig.OscFixation, 7); }
            set { this._oscConf = Validator.Set(value, StringsConfig.OscFixation, this._oscConf, 7); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string SizeXml
        {
            get { return Validator.Get(this._oscConf, StringsConfig.OscSize, 0, 1, 2, 3, 4, 5, 6); }
            set { this._oscConf = Validator.Set(value, StringsConfig.OscSize, this._oscConf, 0, 1, 2, 3, 4, 5, 6); }
        }
    }
}
