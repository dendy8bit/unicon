using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR700.Version2.Configuration.Structures.Relay
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStruct : StructBase, IDgvRowsContainer<ReleOutputStruct>
    {
        public const int RELAY_COUNT = 8;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = 16)]
        private ReleOutputStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]
    
       public ReleOutputStruct[] Rows
        {
            get
            {
                return _relays;
            }
            set
            {
                this._relays = value;
            }
        }
    }
}