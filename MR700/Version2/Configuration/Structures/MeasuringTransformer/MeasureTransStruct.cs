﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR700.Version2.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        #region [Public field]

        [Layout(0)]
        private ushort _ttMode;//res
        [Layout(1)]
        private ushort _tt;
        [Layout(2)]
        private ushort _ttnp;
        [Layout(3)]
        private ushort _maxI;
        [Layout(4)]
        private ushort _startI;//res
        [Layout(5)]
        private ushort _res3;
        [Layout(6)]
        private ushort _res1;
        [Layout(7)]
        private ushort _res2;
        [Layout(8)]
        private ushort _tnMode;
        [Layout(9)]
        private ushort _tn;
        [Layout(10)]
        private ushort _tnFault;
        [Layout(11)]
        private ushort _tnnp;
        [Layout(12)]
        private ushort _tnnpFault;
        [Layout(13)]
        private ushort _opmMode;
        [Layout(14)]
        private ushort _rOpm;





        #endregion [Public field]

        [XmlElement(ElementName = "конфигурация_ТТ")]
         [BindingProperty(0)]
        public ushort Tt
        {
            get { return _tt; }
            set { _tt = value; }
        }
        [XmlElement(ElementName = "конфигурация_ТТНП")]
         [BindingProperty(1)]
        public ushort Ttnp
        {
            get { return _ttnp; }
            set { _ttnp = value; }
        }
         [XmlElement(ElementName = "Iм")]
        [BindingProperty(2)]
        public double MaxI
        {
            get
            {
                return ValuesConverterCommon.GetIn(_maxI);
            }
            set
            {
                _maxI = ValuesConverterCommon.SetIn(value);
            }

        }
        [XmlElement(ElementName = "тип_ТТ")]
          [BindingProperty(3)]
         public string TtMode
        {
            get { return Validator.Get(_ttMode, StringsConfig.TtType); }
            set { _ttMode = Validator.Set(value, StringsConfig.TtType); }
        }
        [XmlElement(ElementName = "тип_Uo")]
        [BindingProperty(4)]
        public string TnMode
        {
            get { return Validator.Get(_tnMode, StringsConfig.TnType); }
            set { _tnMode = Validator.Set(value, StringsConfig.TnType); }
        }
        [XmlElement(ElementName = "ТН")]
        [BindingProperty(5)]
        public double Tn
        {
            get { return ValuesConverterCommon.GetFactor(_tn); }
            set { _tn = ValuesConverterCommon.SetFactor(value); }
        }
        [XmlElement(ElementName = "Неисправность_ТН")]
        [BindingProperty(6)]
        public string TnFault
        {
                        get { return Validator.Get(_tnFault, StringsConfig.LogicSignals); }
            set { _tnFault = Validator.Set(value, StringsConfig.LogicSignals); }
   
        }
        [XmlElement(ElementName = "ТННП")]
        [BindingProperty(7)]
        public double Tnnp
        {
            get { return ValuesConverterCommon.GetFactor(_tnnp); }
            set { _tnnp = ValuesConverterCommon.SetFactor(value); }
        }
        [XmlElement(ElementName = "Неисправность_ТННП")]
        [BindingProperty(8)]
        public string TnnpFault
        {
            get { return Validator.Get(_tnnpFault, StringsConfig.LogicSignals); }
            set { _tnnpFault = Validator.Set(value, StringsConfig.LogicSignals); }
        }
        [XmlElement(ElementName = "ОМП")]
        [BindingProperty(9)]
        public bool Omp
        {
            get { return Common.GetBit(this._opmMode, 0); }
            set { this._opmMode = Common.SetBit(this._opmMode, 0, value); }
        }
        [XmlElement(ElementName = "Xyd")]
        [BindingProperty(10)]
        public double Xyd
        {
            get { return _rOpm/1000.0; }
            set { _rOpm =(ushort) (value*1000); }
        }
    }
}
