﻿namespace BEMN.MR700.OldOsc.ShowOsc
{
    partial class Mr700OldOscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this._discretsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._discrestsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this._discrete5Button = new System.Windows.Forms.Button();
            this._discrete16Button = new System.Windows.Forms.Button();
            this._discrete15Button = new System.Windows.Forms.Button();
            this._discrete14Button = new System.Windows.Forms.Button();
            this._discrete13Button = new System.Windows.Forms.Button();
            this._discrete12Button = new System.Windows.Forms.Button();
            this._discrete11Button = new System.Windows.Forms.Button();
            this._discrete10Button = new System.Windows.Forms.Button();
            this._discrete9Button = new System.Windows.Forms.Button();
            this._discrete8Button = new System.Windows.Forms.Button();
            this._discrete7Button = new System.Windows.Forms.Button();
            this._discrete6Button = new System.Windows.Forms.Button();
            this._discrete2Button = new System.Windows.Forms.Button();
            this._discrete3Button = new System.Windows.Forms.Button();
            this._discrete4Button = new System.Windows.Forms.Button();
            this._discrete1Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._voltageLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._uScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._u3Button = new System.Windows.Forms.Button();
            this._u4Button = new System.Windows.Forms.Button();
            this._u1Button = new System.Windows.Forms.Button();
            this._u2Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._uChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._voltageChartDecreaseButton = new System.Windows.Forms.Button();
            this._voltageChartIncreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._currentsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._iScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this._i3Button = new System.Windows.Forms.Button();
            this._i4Button = new System.Windows.Forms.Button();
            this._i1Button = new System.Windows.Forms.Button();
            this._i2Button = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this._iChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this._currentChartDecreaseButton = new System.Windows.Forms.Button();
            this._currentChartIncreaseButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this._marker1TrackBar = new System.Windows.Forms.TrackBar();
            this._marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._markerScrollPanel = new System.Windows.Forms.Panel();
            this._marker2Box = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._marker2D11 = new System.Windows.Forms.Label();
            this._marker2D10 = new System.Windows.Forms.Label();
            this._marker2D9 = new System.Windows.Forms.Label();
            this._marker2D16 = new System.Windows.Forms.Label();
            this._marker2D15 = new System.Windows.Forms.Label();
            this._marker2D14 = new System.Windows.Forms.Label();
            this._marker2D13 = new System.Windows.Forms.Label();
            this._marker2D12 = new System.Windows.Forms.Label();
            this._marker2D8 = new System.Windows.Forms.Label();
            this._marker2D7 = new System.Windows.Forms.Label();
            this._marker2D6 = new System.Windows.Forms.Label();
            this._marker2D5 = new System.Windows.Forms.Label();
            this._marker2D4 = new System.Windows.Forms.Label();
            this._marker2D3 = new System.Windows.Forms.Label();
            this._marker2D2 = new System.Windows.Forms.Label();
            this._marker2D1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._marker2U4 = new System.Windows.Forms.Label();
            this._marker2U3 = new System.Windows.Forms.Label();
            this._marker2U2 = new System.Windows.Forms.Label();
            this._marker2U1 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._marker2I4 = new System.Windows.Forms.Label();
            this._marker2I3 = new System.Windows.Forms.Label();
            this._marker2I2 = new System.Windows.Forms.Label();
            this._marker2I1 = new System.Windows.Forms.Label();
            this._marker1Box = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._marker1U4 = new System.Windows.Forms.Label();
            this._marker1U3 = new System.Windows.Forms.Label();
            this._marker1U2 = new System.Windows.Forms.Label();
            this._marker1U1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._marker1D11 = new System.Windows.Forms.Label();
            this._marker1D10 = new System.Windows.Forms.Label();
            this._marker1D9 = new System.Windows.Forms.Label();
            this._marker1D16 = new System.Windows.Forms.Label();
            this._marker1D15 = new System.Windows.Forms.Label();
            this._marker1D14 = new System.Windows.Forms.Label();
            this._marker1D13 = new System.Windows.Forms.Label();
            this._marker1D12 = new System.Windows.Forms.Label();
            this._marker1D8 = new System.Windows.Forms.Label();
            this._marker1D7 = new System.Windows.Forms.Label();
            this._marker1D6 = new System.Windows.Forms.Label();
            this._marker1D5 = new System.Windows.Forms.Label();
            this._marker1D4 = new System.Windows.Forms.Label();
            this._marker1D3 = new System.Windows.Forms.Label();
            this._marker1D2 = new System.Windows.Forms.Label();
            this._marker1D1 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._marker1I4 = new System.Windows.Forms.Label();
            this._marker1I3 = new System.Windows.Forms.Label();
            this._marker1I2 = new System.Windows.Forms.Label();
            this._marker1I1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._deltaTimeBox = new System.Windows.Forms.GroupBox();
            this._markerTimeDelta = new System.Windows.Forms.Label();
            this._marker2TimeBox = new System.Windows.Forms.GroupBox();
            this._marker2Time = new System.Windows.Forms.Label();
            this._marker1TimeBox = new System.Windows.Forms.GroupBox();
            this._marker1Time = new System.Windows.Forms.Label();
            this._xDecreaseButton = new System.Windows.Forms.Button();
            this._currentСheckBox = new System.Windows.Forms.CheckBox();
            this._discrestsСheckBox = new System.Windows.Forms.CheckBox();
            this._markCheckBox = new System.Windows.Forms.CheckBox();
            this._markerCheckBox = new System.Windows.Forms.CheckBox();
            this._newMarkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._xIncreaseButton = new System.Windows.Forms.Button();
            this._oscRunСheckBox = new System.Windows.Forms.CheckBox();
            this._voltageСheckBox = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this._discretsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this._voltageLayoutPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this._currentsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._markerScrollPanel.SuspendLayout();
            this._marker2Box.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this._marker1Box.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._deltaTimeBox.SuspendLayout();
            this._marker2TimeBox.SuspendLayout();
            this._marker1TimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1384, 753);
            this.MAINTABLE.TabIndex = 1;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 733);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1084, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1078, 702);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this._discretsLayoutPanel);
            this.panel1.Controls.Add(this._voltageLayoutPanel);
            this.panel1.Controls.Add(this._currentsLayoutPanel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 642);
            this.panel1.TabIndex = 32;
            // 
            // _discretsLayoutPanel
            // 
            this._discretsLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this._discretsLayoutPanel.ColumnCount = 3;
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this._discretsLayoutPanel.Controls.Add(this._discrestsChart, 1, 1);
            this._discretsLayoutPanel.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this._discretsLayoutPanel.Controls.Add(this.label2, 1, 0);
            this._discretsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._discretsLayoutPanel.Location = new System.Drawing.Point(0, 1076);
            this._discretsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._discretsLayoutPanel.Name = "_discretsLayoutPanel";
            this._discretsLayoutPanel.RowCount = 2;
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.Size = new System.Drawing.Size(1061, 383);
            this._discretsLayoutPanel.TabIndex = 40;
            // 
            // _discrestsChart
            // 
            this._discrestsChart.BkGradient = false;
            this._discrestsChart.BkGradientAngle = 90;
            this._discrestsChart.BkGradientColor = System.Drawing.Color.White;
            this._discrestsChart.BkGradientRate = 0.5F;
            this._discrestsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discrestsChart.BkRestrictedToChartPanel = false;
            this._discrestsChart.BkShinePosition = 1F;
            this._discrestsChart.BkTransparency = 0F;
            this._discrestsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discrestsChart.BorderExteriorLength = 0;
            this._discrestsChart.BorderGradientAngle = 225;
            this._discrestsChart.BorderGradientLightPos1 = 1F;
            this._discrestsChart.BorderGradientLightPos2 = -1F;
            this._discrestsChart.BorderGradientRate = 0.5F;
            this._discrestsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discrestsChart.BorderLightIntermediateBrightness = 0F;
            this._discrestsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discrestsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discrestsChart.ChartPanelBkTransparency = 0F;
            this._discrestsChart.ControlShadow = false;
            this._discrestsChart.CoordinateAxesVisible = true;
            this._discrestsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._discrestsChart.CoordinateXOrigin = 0D;
            this._discrestsChart.CoordinateYMax = 100D;
            this._discrestsChart.CoordinateYMin = -100D;
            this._discrestsChart.CoordinateYOrigin = 0D;
            this._discrestsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrestsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discrestsChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._discrestsChart.FooterColor = System.Drawing.Color.Black;
            this._discrestsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discrestsChart.FooterVisible = false;
            this._discrestsChart.GridColor = System.Drawing.Color.MistyRose;
            this._discrestsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discrestsChart.GridVisible = true;
            this._discrestsChart.GridXSubTicker = 0;
            this._discrestsChart.GridXTicker = 10;
            this._discrestsChart.GridYSubTicker = 0;
            this._discrestsChart.GridYTicker = 2;
            this._discrestsChart.HeaderColor = System.Drawing.Color.Black;
            this._discrestsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discrestsChart.HeaderVisible = false;
            this._discrestsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.InnerBorderLength = 0;
            this._discrestsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.LegendBkColor = System.Drawing.Color.White;
            this._discrestsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discrestsChart.LegendVisible = false;
            this._discrestsChart.Location = new System.Drawing.Point(55, 28);
            this._discrestsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discrestsChart.MiddleBorderLength = 0;
            this._discrestsChart.Name = "_discrestsChart";
            this._discrestsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.OuterBorderLength = 0;
            this._discrestsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.Precision = 0;
            this._discrestsChart.RoundRadius = 10;
            this._discrestsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discrestsChart.ShadowDepth = 8;
            this._discrestsChart.ShadowRate = 0.5F;
            this._discrestsChart.Size = new System.Drawing.Size(979, 351);
            this._discrestsChart.TabIndex = 29;
            this._discrestsChart.Text = "daS_Net_XYChart2";
            this._discrestsChart.XMax = 100D;
            this._discrestsChart.XMin = 0D;
            this._discrestsChart.XScaleColor = System.Drawing.Color.White;
            this._discrestsChart.XScaleVisible = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this._discrete5Button, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this._discrete16Button, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this._discrete15Button, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this._discrete14Button, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this._discrete13Button, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this._discrete12Button, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this._discrete11Button, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this._discrete10Button, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this._discrete9Button, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this._discrete8Button, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this._discrete7Button, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this._discrete6Button, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this._discrete2Button, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this._discrete3Button, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this._discrete4Button, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this._discrete1Button, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(4, 28);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 17;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(44, 351);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // _discrete5Button
            // 
            this._discrete5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete5Button.AutoSize = true;
            this._discrete5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete5Button.ForeColor = System.Drawing.Color.Black;
            this._discrete5Button.Location = new System.Drawing.Point(0, 88);
            this._discrete5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete5Button.Name = "_discrete5Button";
            this._discrete5Button.Size = new System.Drawing.Size(41, 22);
            this._discrete5Button.TabIndex = 16;
            this._discrete5Button.Text = "Д5";
            this._discrete5Button.UseVisualStyleBackColor = false;
            // 
            // _discrete16Button
            // 
            this._discrete16Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete16Button.AutoSize = true;
            this._discrete16Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete16Button.ForeColor = System.Drawing.Color.Black;
            this._discrete16Button.Location = new System.Drawing.Point(0, 330);
            this._discrete16Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete16Button.Name = "_discrete16Button";
            this._discrete16Button.Size = new System.Drawing.Size(41, 22);
            this._discrete16Button.TabIndex = 27;
            this._discrete16Button.Text = "Д16";
            this._discrete16Button.UseVisualStyleBackColor = false;
            // 
            // _discrete15Button
            // 
            this._discrete15Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete15Button.AutoSize = true;
            this._discrete15Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete15Button.ForeColor = System.Drawing.Color.Black;
            this._discrete15Button.Location = new System.Drawing.Point(0, 308);
            this._discrete15Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete15Button.Name = "_discrete15Button";
            this._discrete15Button.Size = new System.Drawing.Size(41, 22);
            this._discrete15Button.TabIndex = 26;
            this._discrete15Button.Text = "Д15";
            this._discrete15Button.UseVisualStyleBackColor = false;
            // 
            // _discrete14Button
            // 
            this._discrete14Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete14Button.AutoSize = true;
            this._discrete14Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete14Button.ForeColor = System.Drawing.Color.Black;
            this._discrete14Button.Location = new System.Drawing.Point(0, 286);
            this._discrete14Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete14Button.Name = "_discrete14Button";
            this._discrete14Button.Size = new System.Drawing.Size(41, 22);
            this._discrete14Button.TabIndex = 25;
            this._discrete14Button.Text = "Д14";
            this._discrete14Button.UseVisualStyleBackColor = false;
            // 
            // _discrete13Button
            // 
            this._discrete13Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete13Button.AutoSize = true;
            this._discrete13Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete13Button.ForeColor = System.Drawing.Color.Black;
            this._discrete13Button.Location = new System.Drawing.Point(0, 264);
            this._discrete13Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete13Button.Name = "_discrete13Button";
            this._discrete13Button.Size = new System.Drawing.Size(41, 22);
            this._discrete13Button.TabIndex = 24;
            this._discrete13Button.Text = "Д13";
            this._discrete13Button.UseVisualStyleBackColor = false;
            // 
            // _discrete12Button
            // 
            this._discrete12Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete12Button.AutoSize = true;
            this._discrete12Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete12Button.ForeColor = System.Drawing.Color.Black;
            this._discrete12Button.Location = new System.Drawing.Point(0, 242);
            this._discrete12Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete12Button.Name = "_discrete12Button";
            this._discrete12Button.Size = new System.Drawing.Size(41, 22);
            this._discrete12Button.TabIndex = 23;
            this._discrete12Button.Text = "Д12";
            this._discrete12Button.UseVisualStyleBackColor = false;
            // 
            // _discrete11Button
            // 
            this._discrete11Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete11Button.AutoSize = true;
            this._discrete11Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete11Button.ForeColor = System.Drawing.Color.Black;
            this._discrete11Button.Location = new System.Drawing.Point(0, 220);
            this._discrete11Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete11Button.Name = "_discrete11Button";
            this._discrete11Button.Size = new System.Drawing.Size(41, 22);
            this._discrete11Button.TabIndex = 22;
            this._discrete11Button.Text = "Д11";
            this._discrete11Button.UseVisualStyleBackColor = false;
            // 
            // _discrete10Button
            // 
            this._discrete10Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete10Button.AutoSize = true;
            this._discrete10Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete10Button.ForeColor = System.Drawing.Color.Black;
            this._discrete10Button.Location = new System.Drawing.Point(0, 198);
            this._discrete10Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete10Button.Name = "_discrete10Button";
            this._discrete10Button.Size = new System.Drawing.Size(41, 22);
            this._discrete10Button.TabIndex = 21;
            this._discrete10Button.Text = "Д10";
            this._discrete10Button.UseVisualStyleBackColor = false;
            // 
            // _discrete9Button
            // 
            this._discrete9Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete9Button.AutoSize = true;
            this._discrete9Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete9Button.ForeColor = System.Drawing.Color.Black;
            this._discrete9Button.Location = new System.Drawing.Point(0, 176);
            this._discrete9Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete9Button.Name = "_discrete9Button";
            this._discrete9Button.Size = new System.Drawing.Size(41, 22);
            this._discrete9Button.TabIndex = 20;
            this._discrete9Button.Text = "Д9";
            this._discrete9Button.UseVisualStyleBackColor = false;
            // 
            // _discrete8Button
            // 
            this._discrete8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete8Button.AutoSize = true;
            this._discrete8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete8Button.ForeColor = System.Drawing.Color.Black;
            this._discrete8Button.Location = new System.Drawing.Point(0, 154);
            this._discrete8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete8Button.Name = "_discrete8Button";
            this._discrete8Button.Size = new System.Drawing.Size(41, 22);
            this._discrete8Button.TabIndex = 19;
            this._discrete8Button.Text = "Д8";
            this._discrete8Button.UseVisualStyleBackColor = false;
            // 
            // _discrete7Button
            // 
            this._discrete7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete7Button.AutoSize = true;
            this._discrete7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete7Button.ForeColor = System.Drawing.Color.Black;
            this._discrete7Button.Location = new System.Drawing.Point(0, 132);
            this._discrete7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete7Button.Name = "_discrete7Button";
            this._discrete7Button.Size = new System.Drawing.Size(41, 22);
            this._discrete7Button.TabIndex = 18;
            this._discrete7Button.Text = "Д7";
            this._discrete7Button.UseVisualStyleBackColor = false;
            // 
            // _discrete6Button
            // 
            this._discrete6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete6Button.AutoSize = true;
            this._discrete6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete6Button.ForeColor = System.Drawing.Color.Black;
            this._discrete6Button.Location = new System.Drawing.Point(0, 110);
            this._discrete6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete6Button.Name = "_discrete6Button";
            this._discrete6Button.Size = new System.Drawing.Size(41, 22);
            this._discrete6Button.TabIndex = 17;
            this._discrete6Button.Text = "Д6";
            this._discrete6Button.UseVisualStyleBackColor = false;
            // 
            // _discrete2Button
            // 
            this._discrete2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete2Button.AutoSize = true;
            this._discrete2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete2Button.ForeColor = System.Drawing.Color.Black;
            this._discrete2Button.Location = new System.Drawing.Point(0, 22);
            this._discrete2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete2Button.Name = "_discrete2Button";
            this._discrete2Button.Size = new System.Drawing.Size(41, 22);
            this._discrete2Button.TabIndex = 37;
            this._discrete2Button.Text = "Д2";
            this._discrete2Button.UseVisualStyleBackColor = false;
            // 
            // _discrete3Button
            // 
            this._discrete3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete3Button.AutoSize = true;
            this._discrete3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete3Button.ForeColor = System.Drawing.Color.Black;
            this._discrete3Button.Location = new System.Drawing.Point(0, 44);
            this._discrete3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete3Button.Name = "_discrete3Button";
            this._discrete3Button.Size = new System.Drawing.Size(41, 22);
            this._discrete3Button.TabIndex = 38;
            this._discrete3Button.Text = "Д3";
            this._discrete3Button.UseVisualStyleBackColor = false;
            // 
            // _discrete4Button
            // 
            this._discrete4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete4Button.AutoSize = true;
            this._discrete4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete4Button.ForeColor = System.Drawing.Color.Black;
            this._discrete4Button.Location = new System.Drawing.Point(0, 66);
            this._discrete4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete4Button.Name = "_discrete4Button";
            this._discrete4Button.Size = new System.Drawing.Size(41, 22);
            this._discrete4Button.TabIndex = 39;
            this._discrete4Button.Text = "Д4";
            this._discrete4Button.UseVisualStyleBackColor = false;
            // 
            // _discrete1Button
            // 
            this._discrete1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete1Button.AutoSize = true;
            this._discrete1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete1Button.ForeColor = System.Drawing.Color.Black;
            this._discrete1Button.Location = new System.Drawing.Point(0, 0);
            this._discrete1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete1Button.Name = "_discrete1Button";
            this._discrete1Button.Size = new System.Drawing.Size(41, 22);
            this._discrete1Button.TabIndex = 36;
            this._discrete1Button.Text = "Д1";
            this._discrete1Button.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Дискреты";
            // 
            // _voltageLayoutPanel
            // 
            this._voltageLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this._voltageLayoutPanel.ColumnCount = 3;
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this._voltageLayoutPanel.Controls.Add(this._uScroll, 2, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this._voltageLayoutPanel.Controls.Add(this._uChart, 1, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this._voltageLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._voltageLayoutPanel.Location = new System.Drawing.Point(0, 538);
            this._voltageLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._voltageLayoutPanel.Name = "_voltageLayoutPanel";
            this._voltageLayoutPanel.RowCount = 2;
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.Size = new System.Drawing.Size(1061, 538);
            this._voltageLayoutPanel.TabIndex = 39;
            // 
            // _uScroll
            // 
            this._uScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._uScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._uScroll.LargeChange = 1;
            this._uScroll.Location = new System.Drawing.Point(1041, 37);
            this._uScroll.Minimum = 100;
            this._uScroll.Name = "_uScroll";
            this._uScroll.Size = new System.Drawing.Size(15, 500);
            this._uScroll.TabIndex = 32;
            this._uScroll.Value = 100;
            this._uScroll.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this._voltageLayoutPanel.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 666F));
            this.tableLayoutPanel3.Controls.Add(this._u3Button, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this._u4Button, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this._u1Button, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this._u2Button, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // _u3Button
            // 
            this._u3Button.BackColor = System.Drawing.Color.Red;
            this._u3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u3Button.Location = new System.Drawing.Point(213, 3);
            this._u3Button.Name = "_u3Button";
            this._u3Button.Size = new System.Drawing.Size(49, 23);
            this._u3Button.TabIndex = 13;
            this._u3Button.Text = "Uc";
            this._u3Button.UseVisualStyleBackColor = false;
            // 
            // _u4Button
            // 
            this._u4Button.BackColor = System.Drawing.Color.Indigo;
            this._u4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u4Button.Location = new System.Drawing.Point(268, 3);
            this._u4Button.Name = "_u4Button";
            this._u4Button.Size = new System.Drawing.Size(49, 23);
            this._u4Button.TabIndex = 11;
            this._u4Button.Text = "Un";
            this._u4Button.UseVisualStyleBackColor = false;
            // 
            // _u1Button
            // 
            this._u1Button.BackColor = System.Drawing.Color.Yellow;
            this._u1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u1Button.Location = new System.Drawing.Point(103, 3);
            this._u1Button.Name = "_u1Button";
            this._u1Button.Size = new System.Drawing.Size(49, 23);
            this._u1Button.TabIndex = 10;
            this._u1Button.Text = "Ua";
            this._u1Button.UseVisualStyleBackColor = false;
            // 
            // _u2Button
            // 
            this._u2Button.BackColor = System.Drawing.Color.Green;
            this._u2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u2Button.Location = new System.Drawing.Point(158, 3);
            this._u2Button.Name = "_u2Button";
            this._u2Button.Size = new System.Drawing.Size(49, 23);
            this._u2Button.TabIndex = 9;
            this._u2Button.Text = "Ub";
            this._u2Button.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Напряжения";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _uChart
            // 
            this._uChart.BkGradient = false;
            this._uChart.BkGradientAngle = 90;
            this._uChart.BkGradientColor = System.Drawing.Color.White;
            this._uChart.BkGradientRate = 0.5F;
            this._uChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._uChart.BkRestrictedToChartPanel = false;
            this._uChart.BkShinePosition = 1F;
            this._uChart.BkTransparency = 0F;
            this._uChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._uChart.BorderExteriorLength = 0;
            this._uChart.BorderGradientAngle = 225;
            this._uChart.BorderGradientLightPos1 = 1F;
            this._uChart.BorderGradientLightPos2 = -1F;
            this._uChart.BorderGradientRate = 0.5F;
            this._uChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._uChart.BorderLightIntermediateBrightness = 0F;
            this._uChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._uChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._uChart.ChartPanelBkTransparency = 0F;
            this._uChart.ControlShadow = false;
            this._uChart.CoordinateAxesVisible = true;
            this._uChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._uChart.CoordinateXOrigin = 0D;
            this._uChart.CoordinateYMax = 100D;
            this._uChart.CoordinateYMin = -100D;
            this._uChart.CoordinateYOrigin = 0D;
            this._uChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._uChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._uChart.FooterColor = System.Drawing.Color.Black;
            this._uChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._uChart.FooterVisible = false;
            this._uChart.GridColor = System.Drawing.Color.MistyRose;
            this._uChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._uChart.GridVisible = true;
            this._uChart.GridXSubTicker = 0;
            this._uChart.GridXTicker = 10;
            this._uChart.GridYSubTicker = 0;
            this._uChart.GridYTicker = 10;
            this._uChart.HeaderColor = System.Drawing.Color.Black;
            this._uChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._uChart.HeaderVisible = false;
            this._uChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.InnerBorderLength = 0;
            this._uChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._uChart.LegendBkColor = System.Drawing.Color.White;
            this._uChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._uChart.LegendVisible = false;
            this._uChart.Location = new System.Drawing.Point(55, 40);
            this._uChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._uChart.MiddleBorderLength = 0;
            this._uChart.Name = "_uChart";
            this._uChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.OuterBorderLength = 0;
            this._uChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._uChart.Precision = 0;
            this._uChart.RoundRadius = 10;
            this._uChart.ShadowColor = System.Drawing.Color.DimGray;
            this._uChart.ShadowDepth = 8;
            this._uChart.ShadowRate = 0.5F;
            this._uChart.Size = new System.Drawing.Size(979, 494);
            this._uChart.TabIndex = 33;
            this._uChart.Text = "daS_Net_XYChart1";
            this._uChart.XMax = 100D;
            this._uChart.XMin = 0D;
            this._uChart.XScaleColor = System.Drawing.Color.Black;
            this._uChart.XScaleVisible = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this._voltageChartDecreaseButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._voltageChartIncreaseButton, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(4, 40);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(44, 494);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // _voltageChartDecreaseButton
            // 
            this._voltageChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartDecreaseButton.Enabled = false;
            this._voltageChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartDecreaseButton.Location = new System.Drawing.Point(3, 250);
            this._voltageChartDecreaseButton.Name = "_voltageChartDecreaseButton";
            this._voltageChartDecreaseButton.Size = new System.Drawing.Size(38, 24);
            this._voltageChartDecreaseButton.TabIndex = 1;
            this._voltageChartDecreaseButton.Text = "-";
            this._voltageChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _voltageChartIncreaseButton
            // 
            this._voltageChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartIncreaseButton.Location = new System.Drawing.Point(3, 220);
            this._voltageChartIncreaseButton.Name = "_voltageChartIncreaseButton";
            this._voltageChartIncreaseButton.Size = new System.Drawing.Size(38, 24);
            this._voltageChartIncreaseButton.TabIndex = 0;
            this._voltageChartIncreaseButton.Text = "+";
            this._voltageChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(14, 202);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // _currentsLayoutPanel
            // 
            this._currentsLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this._currentsLayoutPanel.ColumnCount = 3;
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this._currentsLayoutPanel.Controls.Add(this._iScroll, 2, 1);
            this._currentsLayoutPanel.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this._currentsLayoutPanel.Controls.Add(this._iChart, 1, 1);
            this._currentsLayoutPanel.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this._currentsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._currentsLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._currentsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._currentsLayoutPanel.Name = "_currentsLayoutPanel";
            this._currentsLayoutPanel.RowCount = 2;
            this._currentsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._currentsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._currentsLayoutPanel.Size = new System.Drawing.Size(1061, 538);
            this._currentsLayoutPanel.TabIndex = 41;
            // 
            // _iScroll
            // 
            this._iScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._iScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._iScroll.LargeChange = 1;
            this._iScroll.Location = new System.Drawing.Point(1041, 37);
            this._iScroll.Minimum = 100;
            this._iScroll.Name = "_iScroll";
            this._iScroll.Size = new System.Drawing.Size(15, 500);
            this._iScroll.TabIndex = 32;
            this._iScroll.Value = 100;
            this._iScroll.Visible = false;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this._currentsLayoutPanel.SetColumnSpan(this.tableLayoutPanel7, 3);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 666F));
            this.tableLayoutPanel7.Controls.Add(this._i3Button, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this._i4Button, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this._i1Button, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this._i2Button, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel7.TabIndex = 23;
            // 
            // _i3Button
            // 
            this._i3Button.BackColor = System.Drawing.Color.Red;
            this._i3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i3Button.Location = new System.Drawing.Point(213, 3);
            this._i3Button.Name = "_i3Button";
            this._i3Button.Size = new System.Drawing.Size(49, 23);
            this._i3Button.TabIndex = 13;
            this._i3Button.Text = "Ic";
            this._i3Button.UseVisualStyleBackColor = false;
            // 
            // _i4Button
            // 
            this._i4Button.BackColor = System.Drawing.Color.Indigo;
            this._i4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i4Button.Location = new System.Drawing.Point(268, 3);
            this._i4Button.Name = "_i4Button";
            this._i4Button.Size = new System.Drawing.Size(49, 23);
            this._i4Button.TabIndex = 11;
            this._i4Button.Text = "In";
            this._i4Button.UseVisualStyleBackColor = false;
            // 
            // _i1Button
            // 
            this._i1Button.BackColor = System.Drawing.Color.Yellow;
            this._i1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i1Button.Location = new System.Drawing.Point(103, 3);
            this._i1Button.Name = "_i1Button";
            this._i1Button.Size = new System.Drawing.Size(49, 23);
            this._i1Button.TabIndex = 10;
            this._i1Button.Text = "Ia";
            this._i1Button.UseVisualStyleBackColor = false;
            // 
            // _i2Button
            // 
            this._i2Button.BackColor = System.Drawing.Color.Green;
            this._i2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i2Button.Location = new System.Drawing.Point(158, 3);
            this._i2Button.Name = "_i2Button";
            this._i2Button.Size = new System.Drawing.Size(49, 23);
            this._i2Button.TabIndex = 9;
            this._i2Button.Text = "Ib";
            this._i2Button.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Токи";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _iChart
            // 
            this._iChart.BkGradient = false;
            this._iChart.BkGradientAngle = 90;
            this._iChart.BkGradientColor = System.Drawing.Color.White;
            this._iChart.BkGradientRate = 0.5F;
            this._iChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._iChart.BkRestrictedToChartPanel = false;
            this._iChart.BkShinePosition = 1F;
            this._iChart.BkTransparency = 0F;
            this._iChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._iChart.BorderExteriorLength = 0;
            this._iChart.BorderGradientAngle = 225;
            this._iChart.BorderGradientLightPos1 = 1F;
            this._iChart.BorderGradientLightPos2 = -1F;
            this._iChart.BorderGradientRate = 0.5F;
            this._iChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._iChart.BorderLightIntermediateBrightness = 0F;
            this._iChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._iChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._iChart.ChartPanelBkTransparency = 0F;
            this._iChart.ControlShadow = false;
            this._iChart.CoordinateAxesVisible = true;
            this._iChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._iChart.CoordinateXOrigin = 0D;
            this._iChart.CoordinateYMax = 100D;
            this._iChart.CoordinateYMin = -100D;
            this._iChart.CoordinateYOrigin = 0D;
            this._iChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._iChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._iChart.FooterColor = System.Drawing.Color.Black;
            this._iChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._iChart.FooterVisible = false;
            this._iChart.GridColor = System.Drawing.Color.MistyRose;
            this._iChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._iChart.GridVisible = true;
            this._iChart.GridXSubTicker = 0;
            this._iChart.GridXTicker = 10;
            this._iChart.GridYSubTicker = 0;
            this._iChart.GridYTicker = 10;
            this._iChart.HeaderColor = System.Drawing.Color.Black;
            this._iChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._iChart.HeaderVisible = false;
            this._iChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._iChart.InnerBorderLength = 0;
            this._iChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._iChart.LegendBkColor = System.Drawing.Color.White;
            this._iChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._iChart.LegendVisible = false;
            this._iChart.Location = new System.Drawing.Point(55, 40);
            this._iChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._iChart.MiddleBorderLength = 0;
            this._iChart.Name = "_iChart";
            this._iChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._iChart.OuterBorderLength = 0;
            this._iChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._iChart.Precision = 0;
            this._iChart.RoundRadius = 10;
            this._iChart.ShadowColor = System.Drawing.Color.DimGray;
            this._iChart.ShadowDepth = 8;
            this._iChart.ShadowRate = 0.5F;
            this._iChart.Size = new System.Drawing.Size(979, 494);
            this._iChart.TabIndex = 33;
            this._iChart.Text = "daS_Net_XYChart1";
            this._iChart.XMax = 100D;
            this._iChart.XMin = 0D;
            this._iChart.XScaleColor = System.Drawing.Color.Black;
            this._iChart.XScaleVisible = true;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel9.Controls.Add(this._currentChartDecreaseButton, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this._currentChartIncreaseButton, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(4, 40);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 4;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(44, 494);
            this.tableLayoutPanel9.TabIndex = 31;
            // 
            // _currentChartDecreaseButton
            // 
            this._currentChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentChartDecreaseButton.Enabled = false;
            this._currentChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentChartDecreaseButton.Location = new System.Drawing.Point(3, 250);
            this._currentChartDecreaseButton.Name = "_currentChartDecreaseButton";
            this._currentChartDecreaseButton.Size = new System.Drawing.Size(39, 24);
            this._currentChartDecreaseButton.TabIndex = 1;
            this._currentChartDecreaseButton.Text = "-";
            this._currentChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentChartIncreaseButton
            // 
            this._currentChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentChartIncreaseButton.Location = new System.Drawing.Point(3, 220);
            this._currentChartIncreaseButton.Name = "_currentChartIncreaseButton";
            this._currentChartIncreaseButton.Size = new System.Drawing.Size(39, 24);
            this._currentChartIncreaseButton.TabIndex = 0;
            this._currentChartIncreaseButton.Text = "+";
            this._currentChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.Location = new System.Drawing.Point(15, 202);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "Y";
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 3;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.MarkersTable.Controls.Add(this._marker1TrackBar, 1, 0);
            this.MarkersTable.Controls.Add(this._marker2TrackBar, 1, 1);
            this.MarkersTable.Controls.Add(this.label3, 0, 0);
            this.MarkersTable.Controls.Add(this.label4, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 2;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.Size = new System.Drawing.Size(1078, 60);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // _marker1TrackBar
            // 
            this._marker1TrackBar.BackColor = System.Drawing.Color.LightSkyBlue;
            this._marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker1TrackBar.Location = new System.Drawing.Point(42, 4);
            this._marker1TrackBar.Name = "_marker1TrackBar";
            this._marker1TrackBar.Size = new System.Drawing.Size(1004, 24);
            this._marker1TrackBar.TabIndex = 2;
            this._marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker1TrackBar.Scroll += new System.EventHandler(this._marker1TrackBar_Scroll);
            // 
            // _marker2TrackBar
            // 
            this._marker2TrackBar.BackColor = System.Drawing.Color.Violet;
            this._marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker2TrackBar.LargeChange = 0;
            this._marker2TrackBar.Location = new System.Drawing.Point(42, 35);
            this._marker2TrackBar.Maximum = 3400;
            this._marker2TrackBar.Name = "_marker2TrackBar";
            this._marker2TrackBar.Size = new System.Drawing.Size(1004, 24);
            this._marker2TrackBar.TabIndex = 3;
            this._marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker2TrackBar.Scroll += new System.EventHandler(this._marker2TrackBar_Scroll);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 30);
            this.label3.TabIndex = 4;
            this.label3.Text = "М 1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 30);
            this.label4.TabIndex = 5;
            this.label4.Text = "М 2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1087, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(294, 722);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this._markerScrollPanel);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(293, 720);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мгновенные значения";
            // 
            // _markerScrollPanel
            // 
            this._markerScrollPanel.AutoScroll = true;
            this._markerScrollPanel.Controls.Add(this._marker2Box);
            this._markerScrollPanel.Controls.Add(this._marker1Box);
            this._markerScrollPanel.Controls.Add(this.groupBox4);
            this._markerScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._markerScrollPanel.Location = new System.Drawing.Point(0, 13);
            this._markerScrollPanel.Name = "_markerScrollPanel";
            this._markerScrollPanel.Size = new System.Drawing.Size(293, 707);
            this._markerScrollPanel.TabIndex = 3;
            // 
            // _marker2Box
            // 
            this._marker2Box.Controls.Add(this.groupBox5);
            this._marker2Box.Controls.Add(this.groupBox3);
            this._marker2Box.Controls.Add(this.groupBox12);
            this._marker2Box.Location = new System.Drawing.Point(142, 3);
            this._marker2Box.Name = "_marker2Box";
            this._marker2Box.Size = new System.Drawing.Size(133, 559);
            this._marker2Box.TabIndex = 3;
            this._marker2Box.TabStop = false;
            this._marker2Box.Text = "М 2";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._marker2D11);
            this.groupBox5.Controls.Add(this._marker2D10);
            this.groupBox5.Controls.Add(this._marker2D9);
            this.groupBox5.Controls.Add(this._marker2D16);
            this.groupBox5.Controls.Add(this._marker2D15);
            this.groupBox5.Controls.Add(this._marker2D14);
            this.groupBox5.Controls.Add(this._marker2D13);
            this.groupBox5.Controls.Add(this._marker2D12);
            this.groupBox5.Controls.Add(this._marker2D8);
            this.groupBox5.Controls.Add(this._marker2D7);
            this.groupBox5.Controls.Add(this._marker2D6);
            this.groupBox5.Controls.Add(this._marker2D5);
            this.groupBox5.Controls.Add(this._marker2D4);
            this.groupBox5.Controls.Add(this._marker2D3);
            this.groupBox5.Controls.Add(this._marker2D2);
            this.groupBox5.Controls.Add(this._marker2D1);
            this.groupBox5.Location = new System.Drawing.Point(5, 214);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(119, 336);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Дискреты";
            // 
            // _marker2D11
            // 
            this._marker2D11.AutoSize = true;
            this._marker2D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D11.Location = new System.Drawing.Point(6, 216);
            this._marker2D11.Name = "_marker2D11";
            this._marker2D11.Size = new System.Drawing.Size(40, 13);
            this._marker2D11.TabIndex = 34;
            this._marker2D11.Text = "Д11 = ";
            // 
            // _marker2D10
            // 
            this._marker2D10.AutoSize = true;
            this._marker2D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D10.Location = new System.Drawing.Point(6, 196);
            this._marker2D10.Name = "_marker2D10";
            this._marker2D10.Size = new System.Drawing.Size(40, 13);
            this._marker2D10.TabIndex = 33;
            this._marker2D10.Text = "Д10 = ";
            // 
            // _marker2D9
            // 
            this._marker2D9.AutoSize = true;
            this._marker2D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D9.Location = new System.Drawing.Point(6, 176);
            this._marker2D9.Name = "_marker2D9";
            this._marker2D9.Size = new System.Drawing.Size(34, 13);
            this._marker2D9.TabIndex = 32;
            this._marker2D9.Text = "Д9 = ";
            // 
            // _marker2D16
            // 
            this._marker2D16.AutoSize = true;
            this._marker2D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D16.Location = new System.Drawing.Point(5, 316);
            this._marker2D16.Name = "_marker2D16";
            this._marker2D16.Size = new System.Drawing.Size(40, 13);
            this._marker2D16.TabIndex = 28;
            this._marker2D16.Text = "Д16 = ";
            // 
            // _marker2D15
            // 
            this._marker2D15.AutoSize = true;
            this._marker2D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D15.Location = new System.Drawing.Point(5, 296);
            this._marker2D15.Name = "_marker2D15";
            this._marker2D15.Size = new System.Drawing.Size(40, 13);
            this._marker2D15.TabIndex = 27;
            this._marker2D15.Text = "Д15 = ";
            // 
            // _marker2D14
            // 
            this._marker2D14.AutoSize = true;
            this._marker2D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D14.Location = new System.Drawing.Point(5, 276);
            this._marker2D14.Name = "_marker2D14";
            this._marker2D14.Size = new System.Drawing.Size(40, 13);
            this._marker2D14.TabIndex = 26;
            this._marker2D14.Text = "Д14 = ";
            // 
            // _marker2D13
            // 
            this._marker2D13.AutoSize = true;
            this._marker2D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D13.Location = new System.Drawing.Point(5, 256);
            this._marker2D13.Name = "_marker2D13";
            this._marker2D13.Size = new System.Drawing.Size(40, 13);
            this._marker2D13.TabIndex = 25;
            this._marker2D13.Text = "Д13 = ";
            // 
            // _marker2D12
            // 
            this._marker2D12.AutoSize = true;
            this._marker2D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D12.Location = new System.Drawing.Point(5, 236);
            this._marker2D12.Name = "_marker2D12";
            this._marker2D12.Size = new System.Drawing.Size(40, 13);
            this._marker2D12.TabIndex = 24;
            this._marker2D12.Text = "Д12 = ";
            // 
            // _marker2D8
            // 
            this._marker2D8.AutoSize = true;
            this._marker2D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D8.Location = new System.Drawing.Point(6, 156);
            this._marker2D8.Name = "_marker2D8";
            this._marker2D8.Size = new System.Drawing.Size(34, 13);
            this._marker2D8.TabIndex = 23;
            this._marker2D8.Text = "Д8 = ";
            // 
            // _marker2D7
            // 
            this._marker2D7.AutoSize = true;
            this._marker2D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D7.Location = new System.Drawing.Point(6, 136);
            this._marker2D7.Name = "_marker2D7";
            this._marker2D7.Size = new System.Drawing.Size(34, 13);
            this._marker2D7.TabIndex = 22;
            this._marker2D7.Text = "Д7 = ";
            // 
            // _marker2D6
            // 
            this._marker2D6.AutoSize = true;
            this._marker2D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D6.Location = new System.Drawing.Point(6, 116);
            this._marker2D6.Name = "_marker2D6";
            this._marker2D6.Size = new System.Drawing.Size(34, 13);
            this._marker2D6.TabIndex = 21;
            this._marker2D6.Text = "Д6 = ";
            // 
            // _marker2D5
            // 
            this._marker2D5.AutoSize = true;
            this._marker2D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D5.Location = new System.Drawing.Point(6, 96);
            this._marker2D5.Name = "_marker2D5";
            this._marker2D5.Size = new System.Drawing.Size(34, 13);
            this._marker2D5.TabIndex = 20;
            this._marker2D5.Text = "Д5 = ";
            // 
            // _marker2D4
            // 
            this._marker2D4.AutoSize = true;
            this._marker2D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D4.Location = new System.Drawing.Point(6, 76);
            this._marker2D4.Name = "_marker2D4";
            this._marker2D4.Size = new System.Drawing.Size(34, 13);
            this._marker2D4.TabIndex = 19;
            this._marker2D4.Text = "Д4 = ";
            // 
            // _marker2D3
            // 
            this._marker2D3.AutoSize = true;
            this._marker2D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D3.Location = new System.Drawing.Point(6, 56);
            this._marker2D3.Name = "_marker2D3";
            this._marker2D3.Size = new System.Drawing.Size(34, 13);
            this._marker2D3.TabIndex = 18;
            this._marker2D3.Text = "Д3 = ";
            // 
            // _marker2D2
            // 
            this._marker2D2.AutoSize = true;
            this._marker2D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D2.Location = new System.Drawing.Point(6, 36);
            this._marker2D2.Name = "_marker2D2";
            this._marker2D2.Size = new System.Drawing.Size(34, 13);
            this._marker2D2.TabIndex = 17;
            this._marker2D2.Text = "Д2 = ";
            // 
            // _marker2D1
            // 
            this._marker2D1.AutoSize = true;
            this._marker2D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D1.Location = new System.Drawing.Point(6, 16);
            this._marker2D1.Name = "_marker2D1";
            this._marker2D1.Size = new System.Drawing.Size(34, 13);
            this._marker2D1.TabIndex = 16;
            this._marker2D1.Text = "Д1 = ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._marker2U4);
            this.groupBox3.Controls.Add(this._marker2U3);
            this.groupBox3.Controls.Add(this._marker2U2);
            this.groupBox3.Controls.Add(this._marker2U1);
            this.groupBox3.Location = new System.Drawing.Point(5, 113);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(119, 95);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Напряжения";
            // 
            // _marker2U4
            // 
            this._marker2U4.AutoSize = true;
            this._marker2U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U4.Location = new System.Drawing.Point(6, 78);
            this._marker2U4.Name = "_marker2U4";
            this._marker2U4.Size = new System.Drawing.Size(28, 13);
            this._marker2U4.TabIndex = 3;
            this._marker2U4.Text = "In = ";
            // 
            // _marker2U3
            // 
            this._marker2U3.AutoSize = true;
            this._marker2U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U3.Location = new System.Drawing.Point(6, 58);
            this._marker2U3.Name = "_marker2U3";
            this._marker2U3.Size = new System.Drawing.Size(28, 13);
            this._marker2U3.TabIndex = 2;
            this._marker2U3.Text = "Ic = ";
            // 
            // _marker2U2
            // 
            this._marker2U2.AutoSize = true;
            this._marker2U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U2.Location = new System.Drawing.Point(6, 38);
            this._marker2U2.Name = "_marker2U2";
            this._marker2U2.Size = new System.Drawing.Size(28, 13);
            this._marker2U2.TabIndex = 1;
            this._marker2U2.Text = "Ib = ";
            // 
            // _marker2U1
            // 
            this._marker2U1.AutoSize = true;
            this._marker2U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U1.Location = new System.Drawing.Point(6, 18);
            this._marker2U1.Name = "_marker2U1";
            this._marker2U1.Size = new System.Drawing.Size(28, 13);
            this._marker2U1.TabIndex = 0;
            this._marker2U1.Text = "Ia = ";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._marker2I4);
            this.groupBox12.Controls.Add(this._marker2I3);
            this.groupBox12.Controls.Add(this._marker2I2);
            this.groupBox12.Controls.Add(this._marker2I1);
            this.groupBox12.Location = new System.Drawing.Point(5, 12);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(119, 95);
            this.groupBox12.TabIndex = 40;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Токи";
            // 
            // _marker2I4
            // 
            this._marker2I4.AutoSize = true;
            this._marker2I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I4.Location = new System.Drawing.Point(6, 78);
            this._marker2I4.Name = "_marker2I4";
            this._marker2I4.Size = new System.Drawing.Size(25, 13);
            this._marker2I4.TabIndex = 3;
            this._marker2I4.Text = "In =";
            // 
            // _marker2I3
            // 
            this._marker2I3.AutoSize = true;
            this._marker2I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I3.Location = new System.Drawing.Point(6, 58);
            this._marker2I3.Name = "_marker2I3";
            this._marker2I3.Size = new System.Drawing.Size(25, 13);
            this._marker2I3.TabIndex = 2;
            this._marker2I3.Text = "Ic =";
            // 
            // _marker2I2
            // 
            this._marker2I2.AutoSize = true;
            this._marker2I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I2.Location = new System.Drawing.Point(6, 38);
            this._marker2I2.Name = "_marker2I2";
            this._marker2I2.Size = new System.Drawing.Size(25, 13);
            this._marker2I2.TabIndex = 1;
            this._marker2I2.Text = "Ib =";
            // 
            // _marker2I1
            // 
            this._marker2I1.AutoSize = true;
            this._marker2I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I1.Location = new System.Drawing.Point(6, 18);
            this._marker2I1.Name = "_marker2I1";
            this._marker2I1.Size = new System.Drawing.Size(25, 13);
            this._marker2I1.TabIndex = 0;
            this._marker2I1.Text = "Ia =";
            // 
            // _marker1Box
            // 
            this._marker1Box.Controls.Add(this.groupBox2);
            this._marker1Box.Controls.Add(this.groupBox11);
            this._marker1Box.Controls.Add(this.groupBox8);
            this._marker1Box.Location = new System.Drawing.Point(3, 3);
            this._marker1Box.Name = "_marker1Box";
            this._marker1Box.Size = new System.Drawing.Size(133, 559);
            this._marker1Box.TabIndex = 0;
            this._marker1Box.TabStop = false;
            this._marker1Box.Text = "М 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._marker1U4);
            this.groupBox2.Controls.Add(this._marker1U3);
            this.groupBox2.Controls.Add(this._marker1U2);
            this.groupBox2.Controls.Add(this._marker1U1);
            this.groupBox2.Location = new System.Drawing.Point(5, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(119, 95);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Напряжения";
            // 
            // _marker1U4
            // 
            this._marker1U4.AutoSize = true;
            this._marker1U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U4.Location = new System.Drawing.Point(6, 78);
            this._marker1U4.Name = "_marker1U4";
            this._marker1U4.Size = new System.Drawing.Size(28, 13);
            this._marker1U4.TabIndex = 3;
            this._marker1U4.Text = "In = ";
            // 
            // _marker1U3
            // 
            this._marker1U3.AutoSize = true;
            this._marker1U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U3.Location = new System.Drawing.Point(6, 58);
            this._marker1U3.Name = "_marker1U3";
            this._marker1U3.Size = new System.Drawing.Size(28, 13);
            this._marker1U3.TabIndex = 2;
            this._marker1U3.Text = "Ic = ";
            // 
            // _marker1U2
            // 
            this._marker1U2.AutoSize = true;
            this._marker1U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U2.Location = new System.Drawing.Point(6, 38);
            this._marker1U2.Name = "_marker1U2";
            this._marker1U2.Size = new System.Drawing.Size(28, 13);
            this._marker1U2.TabIndex = 1;
            this._marker1U2.Text = "Ib = ";
            // 
            // _marker1U1
            // 
            this._marker1U1.AutoSize = true;
            this._marker1U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U1.Location = new System.Drawing.Point(6, 18);
            this._marker1U1.Name = "_marker1U1";
            this._marker1U1.Size = new System.Drawing.Size(28, 13);
            this._marker1U1.TabIndex = 0;
            this._marker1U1.Text = "Ia = ";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._marker1D11);
            this.groupBox11.Controls.Add(this._marker1D10);
            this.groupBox11.Controls.Add(this._marker1D9);
            this.groupBox11.Controls.Add(this._marker1D16);
            this.groupBox11.Controls.Add(this._marker1D15);
            this.groupBox11.Controls.Add(this._marker1D14);
            this.groupBox11.Controls.Add(this._marker1D13);
            this.groupBox11.Controls.Add(this._marker1D12);
            this.groupBox11.Controls.Add(this._marker1D8);
            this.groupBox11.Controls.Add(this._marker1D7);
            this.groupBox11.Controls.Add(this._marker1D6);
            this.groupBox11.Controls.Add(this._marker1D5);
            this.groupBox11.Controls.Add(this._marker1D4);
            this.groupBox11.Controls.Add(this._marker1D3);
            this.groupBox11.Controls.Add(this._marker1D2);
            this.groupBox11.Controls.Add(this._marker1D1);
            this.groupBox11.Location = new System.Drawing.Point(5, 214);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(119, 336);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дискреты";
            // 
            // _marker1D11
            // 
            this._marker1D11.AutoSize = true;
            this._marker1D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D11.Location = new System.Drawing.Point(6, 216);
            this._marker1D11.Name = "_marker1D11";
            this._marker1D11.Size = new System.Drawing.Size(40, 13);
            this._marker1D11.TabIndex = 34;
            this._marker1D11.Text = "Д11 = ";
            // 
            // _marker1D10
            // 
            this._marker1D10.AutoSize = true;
            this._marker1D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D10.Location = new System.Drawing.Point(6, 196);
            this._marker1D10.Name = "_marker1D10";
            this._marker1D10.Size = new System.Drawing.Size(40, 13);
            this._marker1D10.TabIndex = 33;
            this._marker1D10.Text = "Д10 = ";
            // 
            // _marker1D9
            // 
            this._marker1D9.AutoSize = true;
            this._marker1D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D9.Location = new System.Drawing.Point(6, 176);
            this._marker1D9.Name = "_marker1D9";
            this._marker1D9.Size = new System.Drawing.Size(34, 13);
            this._marker1D9.TabIndex = 32;
            this._marker1D9.Text = "Д9 = ";
            // 
            // _marker1D16
            // 
            this._marker1D16.AutoSize = true;
            this._marker1D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D16.Location = new System.Drawing.Point(5, 316);
            this._marker1D16.Name = "_marker1D16";
            this._marker1D16.Size = new System.Drawing.Size(40, 13);
            this._marker1D16.TabIndex = 28;
            this._marker1D16.Text = "Д16 = ";
            // 
            // _marker1D15
            // 
            this._marker1D15.AutoSize = true;
            this._marker1D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D15.Location = new System.Drawing.Point(5, 296);
            this._marker1D15.Name = "_marker1D15";
            this._marker1D15.Size = new System.Drawing.Size(40, 13);
            this._marker1D15.TabIndex = 27;
            this._marker1D15.Text = "Д15 = ";
            // 
            // _marker1D14
            // 
            this._marker1D14.AutoSize = true;
            this._marker1D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D14.Location = new System.Drawing.Point(5, 276);
            this._marker1D14.Name = "_marker1D14";
            this._marker1D14.Size = new System.Drawing.Size(40, 13);
            this._marker1D14.TabIndex = 26;
            this._marker1D14.Text = "Д14 = ";
            // 
            // _marker1D13
            // 
            this._marker1D13.AutoSize = true;
            this._marker1D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D13.Location = new System.Drawing.Point(5, 256);
            this._marker1D13.Name = "_marker1D13";
            this._marker1D13.Size = new System.Drawing.Size(40, 13);
            this._marker1D13.TabIndex = 25;
            this._marker1D13.Text = "Д13 = ";
            // 
            // _marker1D12
            // 
            this._marker1D12.AutoSize = true;
            this._marker1D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D12.Location = new System.Drawing.Point(5, 236);
            this._marker1D12.Name = "_marker1D12";
            this._marker1D12.Size = new System.Drawing.Size(40, 13);
            this._marker1D12.TabIndex = 24;
            this._marker1D12.Text = "Д12 = ";
            // 
            // _marker1D8
            // 
            this._marker1D8.AutoSize = true;
            this._marker1D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D8.Location = new System.Drawing.Point(6, 156);
            this._marker1D8.Name = "_marker1D8";
            this._marker1D8.Size = new System.Drawing.Size(34, 13);
            this._marker1D8.TabIndex = 23;
            this._marker1D8.Text = "Д8 = ";
            // 
            // _marker1D7
            // 
            this._marker1D7.AutoSize = true;
            this._marker1D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D7.Location = new System.Drawing.Point(6, 136);
            this._marker1D7.Name = "_marker1D7";
            this._marker1D7.Size = new System.Drawing.Size(34, 13);
            this._marker1D7.TabIndex = 22;
            this._marker1D7.Text = "Д7 = ";
            // 
            // _marker1D6
            // 
            this._marker1D6.AutoSize = true;
            this._marker1D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D6.Location = new System.Drawing.Point(6, 116);
            this._marker1D6.Name = "_marker1D6";
            this._marker1D6.Size = new System.Drawing.Size(34, 13);
            this._marker1D6.TabIndex = 21;
            this._marker1D6.Text = "Д6 = ";
            // 
            // _marker1D5
            // 
            this._marker1D5.AutoSize = true;
            this._marker1D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D5.Location = new System.Drawing.Point(6, 96);
            this._marker1D5.Name = "_marker1D5";
            this._marker1D5.Size = new System.Drawing.Size(34, 13);
            this._marker1D5.TabIndex = 20;
            this._marker1D5.Text = "Д5 = ";
            // 
            // _marker1D4
            // 
            this._marker1D4.AutoSize = true;
            this._marker1D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D4.Location = new System.Drawing.Point(6, 76);
            this._marker1D4.Name = "_marker1D4";
            this._marker1D4.Size = new System.Drawing.Size(34, 13);
            this._marker1D4.TabIndex = 19;
            this._marker1D4.Text = "Д4 = ";
            // 
            // _marker1D3
            // 
            this._marker1D3.AutoSize = true;
            this._marker1D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D3.Location = new System.Drawing.Point(6, 56);
            this._marker1D3.Name = "_marker1D3";
            this._marker1D3.Size = new System.Drawing.Size(34, 13);
            this._marker1D3.TabIndex = 18;
            this._marker1D3.Text = "Д3 = ";
            // 
            // _marker1D2
            // 
            this._marker1D2.AutoSize = true;
            this._marker1D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D2.Location = new System.Drawing.Point(6, 36);
            this._marker1D2.Name = "_marker1D2";
            this._marker1D2.Size = new System.Drawing.Size(34, 13);
            this._marker1D2.TabIndex = 17;
            this._marker1D2.Text = "Д2 = ";
            // 
            // _marker1D1
            // 
            this._marker1D1.AutoSize = true;
            this._marker1D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D1.Location = new System.Drawing.Point(6, 16);
            this._marker1D1.Name = "_marker1D1";
            this._marker1D1.Size = new System.Drawing.Size(34, 13);
            this._marker1D1.TabIndex = 16;
            this._marker1D1.Text = "Д1 = ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._marker1I4);
            this.groupBox8.Controls.Add(this._marker1I3);
            this.groupBox8.Controls.Add(this._marker1I2);
            this.groupBox8.Controls.Add(this._marker1I1);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(119, 95);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Токи";
            // 
            // _marker1I4
            // 
            this._marker1I4.AutoSize = true;
            this._marker1I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I4.Location = new System.Drawing.Point(6, 78);
            this._marker1I4.Name = "_marker1I4";
            this._marker1I4.Size = new System.Drawing.Size(28, 13);
            this._marker1I4.TabIndex = 3;
            this._marker1I4.Text = "In = ";
            // 
            // _marker1I3
            // 
            this._marker1I3.AutoSize = true;
            this._marker1I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I3.Location = new System.Drawing.Point(6, 58);
            this._marker1I3.Name = "_marker1I3";
            this._marker1I3.Size = new System.Drawing.Size(28, 13);
            this._marker1I3.TabIndex = 2;
            this._marker1I3.Text = "Ic = ";
            // 
            // _marker1I2
            // 
            this._marker1I2.AutoSize = true;
            this._marker1I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I2.Location = new System.Drawing.Point(6, 38);
            this._marker1I2.Name = "_marker1I2";
            this._marker1I2.Size = new System.Drawing.Size(28, 13);
            this._marker1I2.TabIndex = 1;
            this._marker1I2.Text = "Ib = ";
            // 
            // _marker1I1
            // 
            this._marker1I1.AutoSize = true;
            this._marker1I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I1.Location = new System.Drawing.Point(6, 18);
            this._marker1I1.Name = "_marker1I1";
            this._marker1I1.Size = new System.Drawing.Size(28, 13);
            this._marker1I1.TabIndex = 0;
            this._marker1I1.Text = "Ia = ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._deltaTimeBox);
            this.groupBox4.Controls.Add(this._marker2TimeBox);
            this.groupBox4.Controls.Add(this._marker1TimeBox);
            this.groupBox4.Location = new System.Drawing.Point(3, 616);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(831, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Время";
            // 
            // _deltaTimeBox
            // 
            this._deltaTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._deltaTimeBox.Controls.Add(this._markerTimeDelta);
            this._deltaTimeBox.Location = new System.Drawing.Point(6, 46);
            this._deltaTimeBox.Name = "_deltaTimeBox";
            this._deltaTimeBox.Size = new System.Drawing.Size(266, 33);
            this._deltaTimeBox.TabIndex = 2;
            this._deltaTimeBox.TabStop = false;
            this._deltaTimeBox.Text = "Дельта";
            // 
            // _markerTimeDelta
            // 
            this._markerTimeDelta.AutoSize = true;
            this._markerTimeDelta.Location = new System.Drawing.Point(118, 16);
            this._markerTimeDelta.Name = "_markerTimeDelta";
            this._markerTimeDelta.Size = new System.Drawing.Size(30, 13);
            this._markerTimeDelta.TabIndex = 2;
            this._markerTimeDelta.Text = "0 мс";
            // 
            // _marker2TimeBox
            // 
            this._marker2TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker2TimeBox.Controls.Add(this._marker2Time);
            this._marker2TimeBox.Location = new System.Drawing.Point(144, 13);
            this._marker2TimeBox.Name = "_marker2TimeBox";
            this._marker2TimeBox.Size = new System.Drawing.Size(119, 33);
            this._marker2TimeBox.TabIndex = 1;
            this._marker2TimeBox.TabStop = false;
            this._marker2TimeBox.Text = "М 2";
            // 
            // _marker2Time
            // 
            this._marker2Time.AutoSize = true;
            this._marker2Time.Location = new System.Drawing.Point(41, 16);
            this._marker2Time.Name = "_marker2Time";
            this._marker2Time.Size = new System.Drawing.Size(30, 13);
            this._marker2Time.TabIndex = 1;
            this._marker2Time.Text = "0 мс";
            // 
            // _marker1TimeBox
            // 
            this._marker1TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker1TimeBox.Controls.Add(this._marker1Time);
            this._marker1TimeBox.Location = new System.Drawing.Point(6, 13);
            this._marker1TimeBox.Name = "_marker1TimeBox";
            this._marker1TimeBox.Size = new System.Drawing.Size(118, 33);
            this._marker1TimeBox.TabIndex = 0;
            this._marker1TimeBox.TabStop = false;
            this._marker1TimeBox.Text = "М 1";
            // 
            // _marker1Time
            // 
            this._marker1Time.AutoSize = true;
            this._marker1Time.Location = new System.Drawing.Point(39, 16);
            this._marker1Time.Name = "_marker1Time";
            this._marker1Time.Size = new System.Drawing.Size(30, 13);
            this._marker1Time.TabIndex = 0;
            this._marker1Time.Text = "0 мс";
            // 
            // _xDecreaseButton
            // 
            this._xDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xDecreaseButton.Location = new System.Drawing.Point(148, 3);
            this._xDecreaseButton.Name = "_xDecreaseButton";
            this._xDecreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xDecreaseButton.TabIndex = 3;
            this._xDecreaseButton.Text = "X -";
            this._xDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentСheckBox
            // 
            this._currentСheckBox.AutoSize = true;
            this._currentСheckBox.BackColor = System.Drawing.Color.Silver;
            this._currentСheckBox.Checked = true;
            this._currentСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._currentСheckBox.Location = new System.Drawing.Point(195, 3);
            this._currentСheckBox.Name = "_currentСheckBox";
            this._currentСheckBox.Size = new System.Drawing.Size(51, 17);
            this._currentСheckBox.TabIndex = 4;
            this._currentСheckBox.Text = "Токи";
            this._currentСheckBox.UseVisualStyleBackColor = false;
            this._currentСheckBox.CheckedChanged += new System.EventHandler(this._currentСonnectionsСheckBox_CheckedChanged);
            // 
            // _discrestsСheckBox
            // 
            this._discrestsСheckBox.AutoSize = true;
            this._discrestsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._discrestsСheckBox.Checked = true;
            this._discrestsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._discrestsСheckBox.Location = new System.Drawing.Point(361, 3);
            this._discrestsСheckBox.Name = "_discrestsСheckBox";
            this._discrestsСheckBox.Size = new System.Drawing.Size(78, 17);
            this._discrestsСheckBox.TabIndex = 5;
            this._discrestsСheckBox.Text = "Дискреты";
            this._discrestsСheckBox.UseVisualStyleBackColor = false;
            this._discrestsСheckBox.CheckedChanged += new System.EventHandler(this._discrestsСheckBox_CheckedChanged);
            // 
            // _markCheckBox
            // 
            this._markCheckBox.AutoSize = true;
            this._markCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markCheckBox.Location = new System.Drawing.Point(691, 3);
            this._markCheckBox.Name = "_markCheckBox";
            this._markCheckBox.Size = new System.Drawing.Size(58, 17);
            this._markCheckBox.TabIndex = 7;
            this._markCheckBox.Text = "Метки";
            this._markCheckBox.UseVisualStyleBackColor = false;
            // 
            // _markerCheckBox
            // 
            this._markerCheckBox.AutoSize = true;
            this._markerCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markerCheckBox.Location = new System.Drawing.Point(761, 3);
            this._markerCheckBox.Name = "_markerCheckBox";
            this._markerCheckBox.Size = new System.Drawing.Size(73, 17);
            this._markerCheckBox.TabIndex = 8;
            this._markerCheckBox.Text = "Маркеры";
            this._markerCheckBox.UseVisualStyleBackColor = false;
            this._markerCheckBox.CheckedChanged += new System.EventHandler(this._markerCheckBox_CheckedChanged);
            // 
            // _xIncreaseButton
            // 
            this._xIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xIncreaseButton.Location = new System.Drawing.Point(108, 3);
            this._xIncreaseButton.Name = "_xIncreaseButton";
            this._xIncreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xIncreaseButton.TabIndex = 2;
            this._xIncreaseButton.Text = "X +";
            this._xIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // _oscRunСheckBox
            // 
            this._oscRunСheckBox.AutoSize = true;
            this._oscRunСheckBox.BackColor = System.Drawing.Color.Silver;
            this._oscRunСheckBox.Location = new System.Drawing.Point(521, 3);
            this._oscRunСheckBox.Name = "_oscRunСheckBox";
            this._oscRunСheckBox.Size = new System.Drawing.Size(127, 17);
            this._oscRunСheckBox.TabIndex = 9;
            this._oscRunСheckBox.Text = "Пуск осциллографа";
            this._oscRunСheckBox.UseVisualStyleBackColor = false;
            this._oscRunСheckBox.CheckedChanged += new System.EventHandler(this._oscRunСheckBox_CheckedChanged);
            // 
            // _voltageСheckBox
            // 
            this._voltageСheckBox.AutoSize = true;
            this._voltageСheckBox.BackColor = System.Drawing.Color.Silver;
            this._voltageСheckBox.Checked = true;
            this._voltageСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._voltageСheckBox.Location = new System.Drawing.Point(256, 3);
            this._voltageСheckBox.Name = "_voltageСheckBox";
            this._voltageСheckBox.Size = new System.Drawing.Size(90, 17);
            this._voltageСheckBox.TabIndex = 10;
            this._voltageСheckBox.Text = "Напряжения";
            this._voltageСheckBox.UseVisualStyleBackColor = false;
            this._voltageСheckBox.CheckedChanged += new System.EventHandler(this._voltageСheckBox_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1384, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Mr700OldOscilloscopeResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 753);
            this.Controls.Add(this._voltageСheckBox);
            this.Controls.Add(this._oscRunСheckBox);
            this.Controls.Add(this._markerCheckBox);
            this.Controls.Add(this._markCheckBox);
            this.Controls.Add(this._discrestsСheckBox);
            this.Controls.Add(this._currentСheckBox);
            this.Controls.Add(this._xDecreaseButton);
            this.Controls.Add(this._xIncreaseButton);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "Mr700OldOscilloscopeResultForm";
            this.Text = "Mr901OscilloscopeResultForm";
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this._discretsLayoutPanel.ResumeLayout(false);
            this._discretsLayoutPanel.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this._voltageLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this._currentsLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._markerScrollPanel.ResumeLayout(false);
            this._marker2Box.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this._marker1Box.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this._deltaTimeBox.ResumeLayout(false);
            this._deltaTimeBox.PerformLayout();
            this._marker2TimeBox.ResumeLayout(false);
            this._marker2TimeBox.PerformLayout();
            this._marker1TimeBox.ResumeLayout(false);
            this._marker1TimeBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.TrackBar _marker1TrackBar;
        private System.Windows.Forms.TrackBar _marker2TrackBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox _deltaTimeBox;
        private System.Windows.Forms.GroupBox _marker2TimeBox;
        private System.Windows.Forms.GroupBox _marker1TimeBox;
        private System.Windows.Forms.GroupBox _marker1Box;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _xDecreaseButton;
        private System.Windows.Forms.CheckBox _currentСheckBox;
        private System.Windows.Forms.CheckBox _discrestsСheckBox;
        private System.Windows.Forms.CheckBox _markCheckBox;
        private System.Windows.Forms.CheckBox _markerCheckBox;
        private System.Windows.Forms.ToolTip _newMarkToolTip;
        private System.Windows.Forms.Label _marker1I4;
        private System.Windows.Forms.Label _marker1I3;
        private System.Windows.Forms.Label _marker1I2;
        private System.Windows.Forms.Label _marker1I1;
        private System.Windows.Forms.Label _marker1D11;
        private System.Windows.Forms.Label _marker1D10;
        private System.Windows.Forms.Label _marker1D9;
        private System.Windows.Forms.Label _marker1D16;
        private System.Windows.Forms.Label _marker1D15;
        private System.Windows.Forms.Label _marker1D14;
        private System.Windows.Forms.Label _marker1D13;
        private System.Windows.Forms.Label _marker1D12;
        private System.Windows.Forms.Label _marker1D8;
        private System.Windows.Forms.Label _marker1D7;
        private System.Windows.Forms.Label _marker1D6;
        private System.Windows.Forms.Label _marker1D5;
        private System.Windows.Forms.Label _marker1D4;
        private System.Windows.Forms.Label _marker1D3;
        private System.Windows.Forms.Label _marker1D2;
        private System.Windows.Forms.Label _marker1D1;
        private System.Windows.Forms.Panel _markerScrollPanel;
        private System.Windows.Forms.GroupBox _marker2Box;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label _marker2I4;
        private System.Windows.Forms.Label _marker2I3;
        private System.Windows.Forms.Label _marker2I2;
        private System.Windows.Forms.Label _marker2I1;
        private System.Windows.Forms.Button _xIncreaseButton;
        private System.Windows.Forms.Label _markerTimeDelta;
        private System.Windows.Forms.Label _marker2Time;
        private System.Windows.Forms.Label _marker1Time;
        private System.Windows.Forms.CheckBox _oscRunСheckBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label _marker2D11;
        private System.Windows.Forms.Label _marker2D10;
        private System.Windows.Forms.Label _marker2D9;
        private System.Windows.Forms.Label _marker2D16;
        private System.Windows.Forms.Label _marker2D15;
        private System.Windows.Forms.Label _marker2D14;
        private System.Windows.Forms.Label _marker2D13;
        private System.Windows.Forms.Label _marker2D12;
        private System.Windows.Forms.Label _marker2D8;
        private System.Windows.Forms.Label _marker2D7;
        private System.Windows.Forms.Label _marker2D6;
        private System.Windows.Forms.Label _marker2D5;
        private System.Windows.Forms.Label _marker2D4;
        private System.Windows.Forms.Label _marker2D3;
        private System.Windows.Forms.Label _marker2D2;
        private System.Windows.Forms.Label _marker2D1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _marker2U4;
        private System.Windows.Forms.Label _marker2U3;
        private System.Windows.Forms.Label _marker2U2;
        private System.Windows.Forms.Label _marker2U1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label _marker1U4;
        private System.Windows.Forms.Label _marker1U3;
        private System.Windows.Forms.Label _marker1U2;
        private System.Windows.Forms.Label _marker1U1;
        private System.Windows.Forms.CheckBox _voltageСheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel _discretsLayoutPanel;
        private BEMN_XY_Chart.DAS_Net_XYChart _discrestsChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button _discrete5Button;
        private System.Windows.Forms.Button _discrete16Button;
        private System.Windows.Forms.Button _discrete15Button;
        private System.Windows.Forms.Button _discrete14Button;
        private System.Windows.Forms.Button _discrete13Button;
        private System.Windows.Forms.Button _discrete12Button;
        private System.Windows.Forms.Button _discrete11Button;
        private System.Windows.Forms.Button _discrete10Button;
        private System.Windows.Forms.Button _discrete9Button;
        private System.Windows.Forms.Button _discrete8Button;
        private System.Windows.Forms.Button _discrete7Button;
        private System.Windows.Forms.Button _discrete6Button;
        private System.Windows.Forms.Button _discrete2Button;
        private System.Windows.Forms.Button _discrete3Button;
        private System.Windows.Forms.Button _discrete4Button;
        private System.Windows.Forms.Button _discrete1Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel _voltageLayoutPanel;
        private System.Windows.Forms.VScrollBar _uScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button _u3Button;
        private System.Windows.Forms.Button _u4Button;
        private System.Windows.Forms.Button _u1Button;
        private System.Windows.Forms.Button _u2Button;
        private System.Windows.Forms.Label label5;
        private BEMN_XY_Chart.DAS_Net_XYChart _uChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _voltageChartDecreaseButton;
        private System.Windows.Forms.Button _voltageChartIncreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel _currentsLayoutPanel;
        private System.Windows.Forms.VScrollBar _iScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button _i3Button;
        private System.Windows.Forms.Button _i4Button;
        private System.Windows.Forms.Button _i1Button;
        private System.Windows.Forms.Button _i2Button;
        private System.Windows.Forms.Label label7;
        private BEMN_XY_Chart.DAS_Net_XYChart _iChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button _currentChartDecreaseButton;
        private System.Windows.Forms.Button _currentChartIncreaseButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MenuStrip menuStrip1;



    }
}