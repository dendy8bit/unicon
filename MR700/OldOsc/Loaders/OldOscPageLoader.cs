﻿using System;
using BEMN.Devices;
using BEMN.MR700.OldOsc.Structures;
using Devices.MemoryEntityClasses;

namespace BEMN.MR700.OldOsc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OldOscPageLoader
    {
        #region [Private fields]

        private MemoryEntity<ArrayStruct> _oscArray;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;


        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана один слот
        /// </summary>
        public event Action SlotReadSuccessful;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;

        private readonly Device _device;
        private int _oscCount;
        #endregion [Events]





        #region [Ctor's]
        public OldOscPageLoader(Device device)
        {
            this._device = device;

        } 
        #endregion [Ctor's]


        #region [Public members]

        public int SlotCount 
        {
            get { return this._oscArray.Slots.Count; }
        }

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        public void StartRead(OscJournalStruct journalStruct, int oscCount)
        {
            this._oscCount = oscCount;
            ArrayStruct.FullSize = journalStruct.Len;
            this._journalStruct = journalStruct;
            this._oscArray = new MemoryEntity<ArrayStruct>("Отсчёты", this._device.MB, 0x4000) { DeviceNum = this._device.DeviceNumber };
            this._oscArray.AllReadOk += o =>
                {
                    this._oscArray.RemoveStrucktQuerys();
                    OscReadComplite();
                };
            this._oscArray.AllReadFail += o => this._oscArray.RemoveStrucktQuerys();
            this._oscArray.ReadOk += OnRaiseSlotReadSuccessful;
           
            this._oscArray.LoadStruckt();
        }

        private void OnRaiseSlotReadSuccessful(object sender)
        {
            if (this.SlotReadSuccessful != null)
            {
                this.SlotReadSuccessful.Invoke();
            }
        }
        #endregion [Public members]


        #region [Event Raise Members]

        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }

        #endregion [Event Raise Members]


        #region [Help members]


        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            var p = this._journalStruct.Len*2;//р=р´×2 байт
            var a = this._journalStruct.After*2;     //а - начало аварии (смещение в байтах) = а´×2
            var k = this._journalStruct.AfterFault + this._oscCount-1 ; //где  k = k´ + 1 (для одной осциллограммы и для одной перезаписываемой осциллограммы);
                                        //k = k´ + 2 (для двух перезаписываемых осциллограмм).     
            var b = a + (k*18 + p%18);    //b - начало осциллограммы = а+(k×18+p%18),

            if (b>=p)//При этом: если b>=p, то b=b-p.
            {
                b -= p;
            }

            var start = b/2; //начало в словах
            var fault = a/2; //начало аварии в словах

            //Результирующий массив
            var readMassiv = this._oscArray.Value.Data;
            var invertedMass = new ushort[this._journalStruct.Len - (p%18)/2];


            Array.ConstrainedCopy(readMassiv, start, invertedMass, 0, readMassiv.Length - start);
            Array.ConstrainedCopy(readMassiv, 0, invertedMass, readMassiv.Length - start, start - this._oscCount - (p % 18) / 2);
            this.ResultArray = invertedMass;

            this.OnRaiseOscReadSuccessful();
        }

        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
