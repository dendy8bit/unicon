﻿using System;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;

namespace BEMN.MR700.OldOsc.Loaders
{
    /// <summary>
    /// Загружает структуру уставок осц. (ДЛЯ СТАРЫХ ОСЦ.!)
    /// </summary>
    public class OldOscOptionsLoader
    {
        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _oscConfig;
        private readonly MemoryEntity<OneWordStruct> _oscCount; 
        #endregion [Private fields]



        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail; 
        #endregion [Events]

        public int AllOscCount
        {
            get { return this._oscConfig.Value.Word == 2 ? 2 : 1; }
        }

        /// <summary>
        /// Количество осц.
        /// </summary>
        public int OscCount
        {
			//get
			//{
			//    var config = this._oscConfig.Value.Word;
			//    var count = this._oscCount.Value.Word;

			//    switch (config)
			//    {
			//        case 0:
			//        {
			//            if (count == 0 || count == 1)
			//            {
			//                return count;
			//            }
			//            else
			//            {
			//                return 0;
			//            }
			//        }

			//        case 1:
			//        {
			//           if (count == 1 || count == 2)
			//            {
			//                return 1;
			//            }
			//           else
			//           {
			//               return 0;
			//           }
			//        }

			//        case 2:
			//        {

			//            return count;
			//        }
			//    }
			//    return 0;
			//}
			get
			{
				var config = this._oscConfig.Value.Word;
				var count = this._oscCount.Value.Word;

				if (count == 0 | count == 1)
				{
					return count;
				}

				if (config == 1)
				{
					return 1;
				}
				return 2;
			}
		}


        #region [Ctor's]
        public OldOscOptionsLoader(Device device)
        {
            this._oscConfig = new MemoryEntity<OneWordStruct>("Конфигурация осциллографа", device, 0x1274);
            this._oscCount = new MemoryEntity<OneWordStruct>("Число осциллограмм", device, 0x3F00);

            this._oscConfig.AllReadOk += o => this._oscCount.LoadStruct();
            this._oscConfig.AllReadFail += OscopeAllReadFail;

            this._oscCount.AllReadOk +=  OscopeAllReadOk;
            this._oscCount.AllReadFail += OscopeAllReadFail;


        }
        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        void OscopeAllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        void OscopeAllReadOk(object sender)
        {
            if (this.LoadOk != null)
            {
                this.LoadOk.Invoke();
            }
        }  
        #endregion [Memory Entity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запускает загрузку конф. осц.
        /// </summary>
        public void StartRead()
        {
           this._oscConfig.LoadStruct();
        } 
        #endregion [Public members]
    }
}
