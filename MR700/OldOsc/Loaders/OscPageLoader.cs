﻿using System;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using BEMN.MR700.OldOsc.Structures;

namespace BEMN.MR700.OldOsc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OldOscPageLoader
    {
        #region [Private fields]
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private MemoryEntity<ArrayStruct> _oscArray;
        /// <summary>
        /// Структура записи страницы
        /// </summary>
        private MemoryEntity<OneWordStruct> _page;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;

        private readonly Device _device;
        private int _oscCount;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана один слот
        /// </summary>
        public event Action SlotReadSuccessful;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Осцилограмма не загружена
        /// </summary>
        public event Action OscReadFail;

        
        #endregion [Events]
        

        #region [Ctor's]
        public OldOscPageLoader(Device device)
        {
            this._device = device;
            this._page = new MemoryEntity<OneWordStruct>("Установка номера осцилограммы", device, 0x3F02);
            this._page.AllWriteOk += o => this._oscArray.LoadStruct();
            this._page.AllWriteFail += o => this.OnRaiseOscReadFail();
        } 
        #endregion [Ctor's]


        #region [Public members]

        public int SlotCount 
        {
            get { return this._oscArray.Slots.Count; }
        }

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        /// <param name="oscCount"></param>
        /// <param name="oscPage"></param>
        public void StartRead(OscJournalStruct journalStruct, int oscCount, int oscPage)
        {
            this._oscCount = oscCount;
            this._journalStruct = journalStruct;
            ArrayStruct.FullSize = journalStruct.Len;
            this._oscArray = new MemoryEntity<ArrayStruct>("Отсчёты", this._device, 0x4000);
            this._oscArray.AllReadOk += o =>
            {
                this._oscArray.RemoveStructQueries();
                OscReadComplite();
            };
            this._oscArray.AllReadFail += o =>
            {
                this._oscArray.RemoveStructQueries();
                this.OnRaiseOscReadFail();
            };
            this._oscArray.ReadOk += OnRaiseSlotReadSuccessful;   
            
            this._page.Value = new OneWordStruct((ushort)(oscPage+1));
            this._page.SaveStruct6();
        }


        #endregion [Public members]


        #region [Event Raise Members]
        private void OnRaiseSlotReadSuccessful(object sender)
        {
            if (this.SlotReadSuccessful != null)
            {
                this.SlotReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }

        /// <summary>
        /// Вызывает событие "Осцилограмма не загружена"
        /// </summary>
        private void OnRaiseOscReadFail()
        {
            if (this.OscReadFail != null)
            {
                this.OscReadFail.Invoke();
            }
        }

        #endregion [Event Raise Members]


        #region [Help members]
        
        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            int p = this._journalStruct.Len*2;//р=р´×2 байт
            int a = this._journalStruct.After*2;     //а - начало аварии (смещение в байтах) = а´×2
            int k = this._journalStruct.AfterFault + this._oscCount ; //где  k = k´ + 1 (для одной осциллограммы и для одной перезаписываемой осциллограммы);
                                        //k = k´ + 2 (для двух перезаписываемых осциллограмм).     
            int b = a + (k*18 + p%18);    //b - начало осциллограммы = а+(k×18+p%18),

            if (b>=p)//При этом: если b>=p, то b=b-p.
            {
                b -= p;
            }
            //Результирующий массив
            byte[] readMassiv = Common.TOBYTES(this._oscArray.Value.Data, false);
            byte[] invertedMass = new byte[p];

            Array.ConstrainedCopy(readMassiv, b, invertedMass, 0, p-b);
            Array.ConstrainedCopy(readMassiv, 0, invertedMass, p-b, b);
            this.ResultArray = Common.TOWORDS(invertedMass, false);

            this.OnRaiseOscReadSuccessful();
        }

        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
