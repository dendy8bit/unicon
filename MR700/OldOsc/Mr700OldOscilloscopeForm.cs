using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.MR700.OldOsc.HelpClasses;
using BEMN.MR700.OldOsc.Loaders;
using BEMN.MR700.OldOsc.ShowOsc;
using BEMN.MR700.OldOsc.Structures;
using System.IO;
using System.Reflection;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR700.OldOsc
{
    public partial class Mr700OldOscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string OSC_JOURNAL_IS_EMPTY = "������ ������������ ����";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string READING_OSC = "���� ������ �������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string FAIL_LOAD_OSC = "���������� ��������� ������������";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OldOscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OldOscJournalLoader _oldOscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;

        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;

        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        private readonly OldOscOptionsLoader _oscopeOptionsLoader;
        private readonly MR700 _device;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr700OldOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public Mr700OldOscilloscopeForm(MR700 device)
        {
            this.InitializeComponent();
            this._device = device;
            //��������� �������
            this._oldOscJournalLoader = new OldOscJournalLoader(device);
            this._oldOscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oldOscJournalLoader.AllReadOk += HandlerHelper.CreateActionHandler(this, this.AllJournalReadOk);
            this._oldOscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //������������ �����������
            this._oscopeOptionsLoader = new OldOscOptionsLoader(device);
            this._oscopeOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, () =>
            {
                if (this._oscopeOptionsLoader.OscCount == 0)
                {
                    this._statusLabel.Text = OSC_JOURNAL_IS_EMPTY;
                    this._oscJournalReadButton.Enabled = true;
                }
                else
                {
                    this._oldOscJournalLoader.StartReadJournal(this._oscopeOptionsLoader.OscCount);
                }
            });
            this._oscopeOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //��������� ������� �����
            this._currentOptionsLoader = new CurrentOptionsLoader(device);
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscopeOptionsLoader.StartRead);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //��������� ������������
            this._pageLoader = new OldOscPageLoader(this._device);
            this._pageLoader.SlotReadSuccessful += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadFail += HandlerHelper.CreateActionHandler(this, this.OscReadFail);

            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]    


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR700); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr700OldOscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }
        
        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            this._oscilloscopeCountCb.Items.Add(this._oldOscJournalLoader.RecordNumber);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oldOscJournalLoader.RecordNumber);
            this._table.Rows.Add
                (
                    this._oldOscJournalLoader.RecordStruct.GetNumber,
                    this._oldOscJournalLoader.RecordStruct.GetDate,
                    this._oldOscJournalLoader.RecordStruct.GetTime,
                    this._oldOscJournalLoader.RecordStruct.SizeTime,
                    this._oldOscJournalLoader.RecordStruct.FaultTime,
                    this._currentOptionsLoader.MeasureStruct.Tt,
                    this._currentOptionsLoader.MeasureStruct.Ttnp,
                    this._currentOptionsLoader.MeasureStruct.Tn,
                    this._currentOptionsLoader.MeasureStruct.Tnnp
                );
            this._oscJournalDataGrid.Refresh();
        }

        private void AllJournalReadOk()
        {
            this.EnableButtons = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct, this._currentOptionsLoader.MeasureStruct);

            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscReadButton.Enabled = true;

            this.EnableButtons = true;
        }
        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadFail()
        {
            this._statusLabel.Text = FAIL_LOAD_OSC;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this.EnableButtons = true;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��700_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[1800], new OscJournalStruct(), new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��700 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                var resForm = new Mr700OldOscilloscopeResultForm(this.CountingList);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oldOscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._currentOptionsLoader.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oldOscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscopeOptionsLoader.AllOscCount, selectedOsc);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.SlotCount;
            this._statusLabel.Text = READING_OSC;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = FAIL_LOAD_OSC;
            }
        }

        #endregion [Event Handlers]
    }
}