using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.MBServer;
using System.Linq;
using System.Text;
using BEMN.MR700.OldOsc.Structures;

namespace BEMN.MR700.OldOsc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 9;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 16;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 4;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        public const int VOLTAGES_COUNT = 4;

        private const string SAVE_OSC_FAIL = "������ ���������� �����";

        public static readonly string[] INames = new[] { "Ia", "Ib", "Ic", "In" };
        public static readonly string[] UNames = new[] { "Ua", "Ub", "Uc", "Un" };
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ ������� 1-16
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private readonly short[][] _baseCurrents;

        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [����]
        
        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// ������������ ����������
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }  
        #endregion [����������]
        
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        private string _dateTime;

        private OscJournalStruct _oscJournalStruct;
        private MeasureTransStruct _measureTrans;
        #endregion [Private fields]

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        public string DateAndTime
        {
            get { return this._dateTime; }
        }
        
        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, MeasureTransStruct measure)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._measureTrans = measure;
            this._dateTime = this._oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.FaultTime;
            //���������� ����, ����������� � ������� ������ ������������ ������, ��� ����� �������������
            pageValue = pageValue.Skip(this._oscJournalStruct.SizeCountdown).Take(this._oscJournalStruct.SizeTime *this._oscJournalStruct.SizeCountdown).ToArray();
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = this._oscJournalStruct.SizeTime;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            this._discrets  = this.DiscretArrayInit(this._countingArray[8]);

            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();
                double factor = (i != 3 ? 40 *this._measureTrans.Tt : 5 *this._measureTrans.Ttnp) / 32768.0 * Math.Sqrt(2);
                this._currents[i] = this._baseCurrents[i].Select(a => a * factor).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + CURRENTS_COUNT].Select(a => (short)(a - 32768)).ToArray();
                double factor = (i != 3 ? this._measureTrans.Tn : this._measureTrans.Tnnp)*256*Math.Sqrt(2)/32768.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => factor * a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param pinName="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    throw new ArgumentException();
                }

                string hdrPath = Path.ChangeExtension(filePath, "hdr");


                using (StreamWriter hdrFile = new StreamWriter(hdrPath))
                {
                    hdrFile.WriteLine("�� 700 {0} {1}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime);
                    hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.SizeTime);
                    hdrFile.WriteLine("Alarm = {0}", this._alarm);
                    hdrFile.WriteLine(1251);
                }


                string cgfPath = Path.ChangeExtension(filePath, "cfg");

                using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
                {
                    cgfFile.WriteLine("MP700,1");
                    cgfFile.WriteLine("24,8A,16D");
                    int index = 1;
                    for (int i = 0; i < this.Currents.Length; i++)
                    {
                        NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                        double factorA = (i != 3 ? 40 * this._measureTrans.Tt : 5 * this._measureTrans.Ttnp) / 32768.0 * Math.Sqrt(2);
                        cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, INames[i], factorA.ToString(format));
                        index++;
                    }

                    for (int i = 0; i < this.Voltages.Length; i++)
                    {
                        NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                        double factor = (i != 3 ? this._measureTrans.Tn : this._measureTrans.Tnnp) * 256 * Math.Sqrt(2) / 32768.0;
                        cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, UNames[i],factor.ToString(format));
                        index++;
                    }

                    for (int i = 0; i < this.Discrets.Length; i++)
                    {
                        cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                        index++;
                    }


                    cgfFile.WriteLine("50");
                    cgfFile.WriteLine("1");
                    cgfFile.WriteLine("1000,{0}", this._count);

                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                    cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                    cgfFile.WriteLine("ASCII");
                }

                string datPath = Path.ChangeExtension(filePath, "dat");
                using (StreamWriter datFile = new StreamWriter(datPath))
                {
                    for (int i = 0; i < this._count; i++)
                    {
                        datFile.Write("{0:D6},{1:D6}", i, i*1000);
                        foreach (short[] current in this._baseCurrents)
                        {
                            datFile.Write(",{0}", current[i]);
                        }
                        foreach (short[] voltage in this._baseVoltages)
                        {
                            datFile.Write(",{0}", voltage[i]);
                        }

                        foreach (ushort[] discret in this.Discrets)
                        {
                            datFile.Write(",{0}", discret[i]);
                        }
                        datFile.WriteLine();
                    }
                }
            }
            catch
            {
                MessageBox.Show(SAVE_OSC_FAIL);
            }

        }

        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[,] factors = new double[VOLTAGES_COUNT + CURRENTS_COUNT,2];

            Regex factorRegex = new Regex(@"\d+\,[IU]\w+\,\,\,[AV]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2, 0] = double.Parse(res, format);
                res = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                factors[i - 2, 1] = double.Parse(res, format);
            }

            int counts = int.Parse(cfgStrings[28].Replace("1000,", string.Empty));

            CountingList result = new CountingList(counts) {_alarm = alarm};
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            string dataPattern =
                @"^\d+\,\d+,(?<I1>\-?\d+),(?<I2>\-?\d+),(?<I3>\-?\d+),(?<I4>\-?\d+),(?<U1>\-?\d+),(?<U2>\-?\d+),(?<U3>\-?\d+),(?<U4>\-?\d+),(?<D1>\d+),(?<D2>\d+),(?<D3>\d+),(?<D4>\d+),(?<D5>\d+),(?<D6>\d+),(?<D7>\d+),(?<D8>\d+),(?<D9>\d+),(?<D10>\d+),(?<D11>\d+),(?<D12>\d+),(?<D13>\d+),(?<D14>\d+),(?<D15>\d+),(?<D16>\d+)";
            Regex dataRegex = new Regex(dataPattern);



            double[][] currents = new double[CURRENTS_COUNT][];
            double[][] voltages = new double[VOLTAGES_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];

            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }



            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["I" + (j + 1)].Value) * factors[j, 0] + factors[j, 1];
                }

                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    voltages[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["U" + (j + 1)].Value) * factors[j + CURRENTS_COUNT, 0] + factors[j + CURRENTS_COUNT, 1];
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }

  
            }
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());                
            }

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, voltages[i].Max());
                result._minU = Math.Min(result.MinU, voltages[i].Min());
            }

            result.Currents = currents;
            result.FilePath = filePath;
            result.IsLoad = true;
            result.Discrets = discrets;
            result.Voltages = voltages;


            result._dateTime = timeString;

            return result;


        }
        private CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];

            this._count = count;
        }

    }
}
