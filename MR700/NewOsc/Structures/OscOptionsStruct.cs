﻿namespace BEMN.MR700.NewOsc.Structures
{
    /// <summary>
    /// Параматры осцилографа
    /// </summary>
    public class OscOptionsStruct
    {
        #region [Properties]
        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        public ushort FullOscSizeInPages
        {
            get { return 52; }
        }

        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize
        {
            get { return 1024; }
        }

        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        public int LoadedFullOscSizeInWords
        {
            get { return 5916 * 9; } //размер 1 неперезаписываемой осц * размер отсчета
        }

        #endregion [Properties]
    }
}
