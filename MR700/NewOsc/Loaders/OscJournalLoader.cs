﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR700.NewOsc.Structures;

namespace BEMN.MR700.NewOsc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
    //    private readonly MemoryEntity<OscOptionsStruct> _oscOptions;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Успешно прочитан журнал осциллографа
        /// </summary>
        public event Action AllReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]


        #region [Ctor's]

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        public OscJournalLoader(Device device)
        {
            this._oscRecords = new List<OscJournalStruct>();
            //Сброс журнала на нулевую запись
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Обновление журнала осциллографа", device, 0x800); 
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);
            //Записи журнала
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", device, 0x800);
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);
        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return _recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return _oscRecords; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }
        
        private void WriteRecordNumber()
        {
            this._refreshOscJournal.Value.Word = (ushort)this._recordNumber;
            this._refreshOscJournal.SaveStruct6();
        }

        private void StartReadOscJournal()
        {
            this._oscJournal.LoadStruct();
        }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStruct.RecordIndex = this._recordNumber;

                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStruct>());
                this._recordNumber = this._recordNumber + 1;
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
                this.WriteRecordNumber();
            }
            else
            {
                if(this.AllReadRecordOk != null)
                    this.AllReadRecordOk.Invoke();
            }
        }
        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            this.WriteRecordNumber();
        } 
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }

        public void Close()
        {
            this._oscJournal.RemoveStructQueries();
            this._refreshOscJournal.RemoveStructQueries();
        }
    }
}
