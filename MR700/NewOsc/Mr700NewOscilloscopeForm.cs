using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.MR700.NewOsc.HelpClasses;
using BEMN.MR700.NewOsc.Loaders;
using BEMN.MR700.NewOsc.ShowOsc;
using BEMN.MR700.NewOsc.Structures;
using System.IO;
using System.Reflection;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR700.NewOsc
{
    public partial class Mr700NewOscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string OSC_JOURNAL_IS_EMPTY = "������ ������������ ����";
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private MR700 _device;
        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr700NewOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public Mr700NewOscilloscopeForm(MR700 device)
        {
            this.InitializeComponent();
            this._device = device;
            //��������� �������
            this._oscJournalLoader = new OscJournalLoader(device);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.AllReadRecordOk += HandlerHelper.CreateActionHandler(this, this.AllJournalReadOk);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //��������� �������
            this._pageLoader = new OscPageLoader(device);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
            //��������� ������� �����
            this._currentOptionsLoader = new CurrentOptionsLoader(device);
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscJournalLoader.StartReadJournal);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);

            this._table = this.GetJournalDataTable();
        }

        #endregion [Ctor's]

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
            this.CanSelectOsc = true;
            this.EnableButtons = true;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR700); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr700NewOscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }


        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            this._oscilloscopeCountCb.Items.Add(this._oscJournalLoader.RecordNumber);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oscJournalLoader.RecordNumber);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        private void AllJournalReadOk()
        {
            if (this._oscJournalLoader.OscRecords.Count == 0)
            {
                this._statusLabel.Text = OSC_JOURNAL_IS_EMPTY;
            }
            else
            {
                this._statusLabel.Text = "������ ��������";
            }

            this.EnableButtons = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct, this._currentOptionsLoader.MeasureStruct);
            }
            catch (Exception e)
            {
            }
            this.EnableButtons = true;
            this._oscReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��700_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[12000], new OscJournalStruct(), new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��700 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                        Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                        fileName);
            }
            else
            {
                Mr700OscilloscopeResultForm resForm = new Mr700OscilloscopeResultForm(this.CountingList);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                this.EnableButtons = false;
                this._oscReadButton.Enabled = false;
                this._oscSaveButton.Enabled = false;
                this._oscShowButton.Enabled = false;
                this._oscilloscopeCountCb.Items.Clear();
                this._oscilloscopeCountCb.SelectedIndex = -1;
                this._table.Clear();
                this._oscJournalLoader.Reset();
                this._oscJournalDataGrid.Refresh();
                this.CanSelectOsc = false;
                this._currentOptionsLoader.StartRead();
                this._statusLabel.Text = "������ ������� ������������";
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                this.EnableButtons = true;
            }
            
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            
            this._statusLabel.Text = "������ �������������";
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName/*,this._oscilloscopeSettings.Value*/);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void Mr700NewOscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*this._pageLoader.Close();
            this._oscJournalLoader.Close();
            this._currentOptionsLoader.Close(); 
                this._oscilloscopeSettings.RemoveStructQuerys();*/
        }
        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }

    }
}