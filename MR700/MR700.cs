using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Drawing.Design;
using System.Globalization;
using BEMN.MBServer;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using BEMN.MBServer.Queries;
using BEMN.MR700.BSBGL;
using BEMN.MR700.NewOsc;
using BEMN.MR700.OldOsc;
using BEMN.MR700.Version2.AlarmJournal.Structures;
using BEMN.MR700.Version2.Configuration;
using BEMN.MR700.Version2.Configuration.Structures;
using BEMN.MR700.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR700.Version2.Measuring;
using BEMN.MR700.Version2.Measuring.Structures;
using BEMN.MR700.Version2.SystemJournal;
using BEMN.MR700.Version2.SystemJournal.Structures;
using BEMN.MR700.Version1;
using BEMN.MR700.Version1.AlarmJournal;
using BEMN.MR700.Version1.AlarmJournal.Structures;
using BEMN.MR700.Version2.AlarmJournal;
using DescriptionAttribute = System.ComponentModel.DescriptionAttribute;
using BEMN.Framework;

namespace BEMN.MR700
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OscJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] datatime;
        public int ready;
        public int point;
        public int begin;
        public int len;
        public int after;
        public int alm;
        public int rez;
    }

    public class MR700 : Device, IDeviceView, IDeviceVersion
    {
        public const ulong TIMELIMIT = 3000000;
        public const ulong KZLIMIT = 4000;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]";
        public const string KZLIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 4000]";
        private Mr700DeviceV2 _mr700DeviceV2;

        public class Mr700DeviceV2
        {
            private readonly Device _device;
            private MemoryEntity<DateTimeStruct> _dateTime;
            private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
            private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
            private MemoryEntity<MeasureTransStruct> _measureTrans;
            private MemoryEntity<ConfigurationStructV2> _configurationV1;
            private MemoryEntity<SystemJournalStruct> _systemJournal;
            private MemoryEntity<OldAlarmJournalStruct> _oldAlarmJournal;

            public MemoryEntity<OldAlarmJournalStruct> OldJA
            {
                get { return this._oldAlarmJournal; }
            }
 
            public Mr700DeviceV2(Device device)
            {
                _device = device;
                this._configurationV1 = new MemoryEntity<ConfigurationStructV2>("������������V1", device, 0x1000);
                this._systemJournal = new MemoryEntity<SystemJournalStruct>("��", device, 0x2000);
                this._alarmRecord = new MemoryEntity<AlarmJournalStruct>("��", device, 0x2800);
                this._measureTransAj = new MemoryEntity<MeasureTransStruct>("��������� ��������� ��", device, 0x1000);
                this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", device, 0x200);
                this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("���������� ��", device,0x1800);
                this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("���������� ��", device,0x1900);
                this._measureTrans = new MemoryEntity<MeasureTransStruct>("��������� ���������", device, 0x1000);
                this._oldAlarmJournal = new MemoryEntity<OldAlarmJournalStruct>("������ ��", device, 0x2800);
                this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", device, 0xB000);
                this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", device, 0x0001);
                this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", device,0xC000);
                this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", device, 0xA000);
                this._stopSpl = new MemoryEntity<OneWordStruct>("StopSpl", _device, 0x1808);
                this._startSpl = new MemoryEntity<OneWordStruct>("StartSpl", _device, 0x1809);
                this._stateSpl = new MemoryEntity<OneWordStruct>("StatetSpl", _device, 0x1805);
            }
            
            public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
            {
                get { return _discretDataBase; }
            }

            public MemoryEntity<DateTimeStruct> DateAndTime
            {
                get { return _dateTime; }
            }

            public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
            {
                get { return _analogDataBase; }
            }

            public MemoryEntity<MeasureTransStruct> MeasureTrans
            {
                get { return _measureTrans; }
            }

            private MemoryEntity<MeasureTransStruct> _measureTransAj;

            public MemoryEntity<MeasureTransStruct> MeasureTransAj
            {
                get { return _measureTransAj; }
            }
            
            private MemoryEntity<AlarmJournalStruct> _alarmRecord;

            public MemoryEntity<AlarmJournalStruct> AlarmRecord
            {
                get { return _alarmRecord; }
            }
            
            public MemoryEntity<SystemJournalStruct> SystemJournal
            {
                get { return this._systemJournal; }
            }

            public MemoryEntity<ConfigurationStructV2> ConfigurationV1
            {
                get { return _configurationV1; }
                set { _configurationV1 = value; }
            }

            #region Programming

            private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
            private MemoryEntity<StartStruct> _programStartStruct;
            private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
            private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
            private MemoryEntity<OneWordStruct> _stopSpl;
            private MemoryEntity<OneWordStruct> _startSpl;
            private MemoryEntity<OneWordStruct> _stateSpl;
 
            public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
            {
                get { return _programStorageStruct; }
            }

            public MemoryEntity<StartStruct> ProgramStartStruct
            {
                get { return _programStartStruct; }
            }

            public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
            {
                get { return _programSignalsStruct; }
            }

            public MemoryEntity<SourceProgramStruct> SourceProgramStruct
            {
                get { return _sourceProgramStruct; }
            }

            public MemoryEntity<OneWordStruct> StopSpl
            {
                get { return _stopSpl; }
            }
            public MemoryEntity<OneWordStruct> StartSpl
            {
                get { return _startSpl; }
            }
            public MemoryEntity<OneWordStruct> StateSpl
            {
                get { return _stateSpl; }
            }

            #endregion

            public void WriteConfiguration()
            {
                _device.SetBit(_device.DeviceNumber, 0, true, "��700 ������ �������������", this);
            }

            public void ReadConfiguration()
            {
                _device.SetBit(_device.DeviceNumber, 0, false, "��700 ������ ����������", this);
            }
        }


        #region ��������� ������

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;
        [Browsable(false)]
        [XmlIgnore]
        public CSystemJournal SystemJournal
        {
            get { return _systemJournalRecords; }
            set { _systemJournalRecords = value; }
        }

        public struct SystemRecord
        {
            public string msg;
            public string time;

        };

        public class CSystemJournal : ICollection
        {

            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

            public int Count
            {
                get
                {
                    return _messages.Count;
                }
            }

            public SystemRecord this[int i]
            {
                get
                {
                    return (SystemRecord)(_messages[i]);
                }

            }

            public bool IsMessageEmpty(int i)
            {
                return "�����" == _messages[i].msg;
            }

            public bool AddMessage(ushort[] value)
            {
                bool ret;
                //Common.SwapArrayItems(ref value);
                SystemRecord msg = CreateMessage(value);
                if ("�����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;

            }

            private SystemRecord CreateMessage(ushort[] value)
            {
                SystemRecord msg = new SystemRecord();

                switch (value[0])
                {
                    case 0:
                        msg.msg = "�����";
                        break;
                    case 1:
                    case 2:
                        msg.msg = "������ �������� ������";
                        break;
                    case 3:
                        msg.msg = "������������� ��. ����";
                        break;
                    case 4:
                        msg.msg = "��. ���� ��������";
                        break;
                    case 5:
                        msg.msg = "����������� ���� �����";
                        break;
                    case 6:
                        msg.msg = "����������� � �����";
                        break;
                    case 7:
                        msg.msg = "��� 2 ���������� (����)";
                        break;
                    case 8:
                        msg.msg = "��� 2 �������� (����)";
                        break;
                    case 9:
                        msg.msg = "��� 1 ���������� (����)";
                        break;
                    case 10:
                        msg.msg = "��� 1 �������� (����)";
                        break;
                    case 11:
                        msg.msg = "��� ����������";
                        break;
                    case 12:
                        msg.msg = "��� ��������";
                        break;
                    case 13:
                        msg.msg = "���1 ����������";
                        break;
                    case 14:
                        msg.msg = "���1 ��������";
                        break;
                    case 15:
                        msg.msg = "���2 ����������";
                        break;
                    case 16:
                        msg.msg = "���2 ��������";
                        break;
                    case 17:
                        msg.msg = "������ ����������� ����� �������";
                        break;
                    case 18:
                    case 19:
                        msg.msg = "������ ����������� ����� ������";
                        break;
                    case 20:
                        msg.msg = "������ ������� �������";
                        break;
                    case 21:
                        msg.msg = "������ ������� ������";
                        break;
                    case 22:
                        msg.msg = "��������� �����";
                        break;
                    case 25:
                        msg.msg = "���� � ������� ��������";
                        break;
                    case 26:
                        msg.msg = "������ �������";
                        break;
                    case 27:
                        msg.msg = "����� ������� �������";
                        break;
                    case 28:
                        msg.msg = "����� ������� ������";
                        break;
                    case 29:
                        msg.msg = "����� ������� �����������";
                        break;
                    case 32:
                        msg.msg = "���� � ������� ��������";
                        break;
                    case 33:
                        msg.msg = "������ ��������� ����������";
                        break;
                    case 34:
                        msg.msg = "������� ����������";
                        break;
                    case 35:
                        msg.msg = "���������� ���������";
                        break;
                    case 36:
                        msg.msg = "���������� ��������";
                        break;
                    case 38:
                        msg.msg = "���� ����� ������������ ";
                        break;
                    case 39:
                        msg.msg = "���� � ����� ������������ ";
                        break;
                    case 46:
                        msg.msg = "�������� ���  Iabc";
                        break;
                    case 47:
                        msg.msg = "������ ���  Iabc";
                        break;
                    case 48:
                        msg.msg = "�����������  Iabc";
                        break;
                    case 49:
                        msg.msg = "���������  Iabc";
                        break;
                    case 50:
                        msg.msg = "�� ����. �������������";
                        break;
                    case 51:
                        msg.msg = "�� ��������";
                        break;
                    case 56:
                        msg.msg = "U��� < 5�";
                        break;
                    case 57:
                        msg.msg = "U��� > 5�";
                        break;
                    case 58:
                        msg.msg = "���� ����. �������������";
                        break;
                    case 59:
                        msg.msg = "���� ��������";
                        break;
                    case 60:
                        msg.msg = "������� ��� ���������";
                        break;
                    case 61:
                        msg.msg = "������� � �����";
                        break;
                    case 62:
                        msg.msg = "����������� ��������";
                        break;
                    case 63:
                        msg.msg = "����������� �������";
                        break;
                    case 64:
                        msg.msg = "���������� �����������";
                        break;
                    case 65:
                        msg.msg = "����� �����������";
                        break;
                    case 66:
                        msg.msg = "������������� �����������";
                        break;
                    case 67:
                        msg.msg = "����.������. �����������";
                        break;
                    case 68:
                        msg.msg = "������.�����. �����������";
                        break;
                    case 69:
                        msg.msg = "������ ����";
                        break;
                    case 71:
                        msg.msg = "������ ���������";
                        break;
                    case 72:
                        msg.msg = "��� �����������";
                        break;
                    case 73:
                        msg.msg = "��� ��.����������";
                        break;
                    case 74:
                        msg.msg = "������ ��� 1 ����";
                        break;
                    case 75:
                        msg.msg = "������ ��� 2 ����";
                        break;
                    case 76:
                        msg.msg = "������ ��� 3 ����";
                        break;
                    case 77:
                        msg.msg = "������ ��� 4 ����";
                        break;
                    case 78:
                        msg.msg = "��� ��������";
                        break;
                    case 85:
                        msg.msg = "A�� ����������";
                        break;
                    case 86:
                        msg.msg = "��� ����. ����������";
                        break;
                    case 87:
                        msg.msg = "��� ����������";
                        break;
                    case 88:
                        msg.msg = "��� ���������";
                        break;
                    case 89:
                        msg.msg = "��� ��������";
                        break;
                    case 90:
                        msg.msg = "��� ���. ������";
                        break;
                    case 91:
                        msg.msg = "��� ����. ������";
                        break;
                    case 92:
                        msg.msg = "��� ������ �� ������";
                        break;
                    case 93:
                        msg.msg = "��� ������ ������� ����.";
                        break;
                    case 94:
                        msg.msg = "��� ������ �� �������";
                        break;
                    case 95:
                        msg.msg = "��� ������ ��������.";
                        break;
                    case 96:
                        msg.msg = "������ ���������";
                        break;
                    case 97:
                        msg.msg = "������ ��������";
                        break;
                    case 98:
                        msg.msg = "���� ���������";
                        break;
                    case 99:
                        msg.msg = "���� ��������";
                        break;
                    case 100:
                        msg.msg = "������� ���������";
                        break;
                    case 101:
                        msg.msg = "������� ��������";
                        break;
                    case 102:
                        msg.msg = "���� ���������";
                        break;
                    case 103:
                        msg.msg = "���� ��������";
                        break;
                    case 104:
                        msg.msg = "�������� �������";
                        break;
                    case 105:
                        msg.msg = "��������� �������";
                        break;
                    case 106:
                        msg.msg = "����.������. �������";
                        break;
                    case 108:
                        msg.msg = "����-�������� �������";
                        break;
                    case 109:
                        msg.msg = "����-��������� �������";
                        break;
                    case 110:
                        msg.msg = "����-�������� �������";
                        break;
                    case 111:
                        msg.msg = "����-��������� �������";
                        break;
                    case 112:
                        msg.msg = "��� �������";
                        break;
                    case 113:
                        msg.msg = "��� ������� F>";
                        break;
                    case 114:
                        msg.msg = "��� ������� F>>";
                        break;
                    case 115:
                        msg.msg = "��� ������� F<";
                        break;
                    case 116:
                        msg.msg = "��� ������� F<<";
                        break;
                    case 117:
                        msg.msg = "��� ������� U>";
                        break;
                    case 118:
                        msg.msg = "��� ������� U>>";
                        break;
                    case 119:
                    case 120:
                        msg.msg = "��� ������� U<<";
                        break;
                    case 121:
                        msg.msg = "��� ������� U2>";
                        break;
                    case 122:
                        msg.msg = "��� ������� U2>>";
                        break;
                    case 123:
                        msg.msg = "��� ������� Uo>";
                        break;
                    case 124:
                        msg.msg = "��� ������� Uo>";
                        break;
                    case 125:
                        msg.msg = "��� ������� ��-1";
                        break;
                    case 126:
                        msg.msg = "��� ������� ��-2";
                        break;
                    case 127:
                        msg.msg = "��� ������� ��-3";
                        break;
                    case 128:
                        msg.msg = "��� ������� ��-4";
                        break;
                    case 129:
                        msg.msg = "��� ������� ��-5";
                        break;
                    case 130:
                        msg.msg = "��� ������� ��-6";
                        break;
                    case 131:
                        msg.msg = "��� ������� ��-7";
                        break;
                    case 132:
                        msg.msg = "��� ������� ��-8";
                        break;
                    case 133:
                        msg.msg = "U<10B ������� ������������";
                        break;
                    case 134:
                        msg.msg = "U>10B ������� ����������";
                        break;
                    case 135:
                        msg.msg = "��� ���� ����������";
                        break;
                    case 136:
                        msg.msg = "��� ���� ����������";
                        break;
                    case 137:
                        msg.msg = "��������� D1";
                        break;
                    case 138:
                        msg.msg = "���������� D1";
                        break;
                    case 139:
                        msg.msg = "��������� D2";
                        break;
                    case 140:
                        msg.msg = "���������� D2";
                        break;
                    case 141:
                        msg.msg = "��������� D3";
                        break;
                    case 142:
                        msg.msg = "���������� D3";
                        break;
                    case 143:
                        msg.msg = "��������� D4";
                        break;
                    case 144:
                        msg.msg = "���������� D4";
                        break;
                    case 145:
                        msg.msg = "��������� D5";
                        break;
                    case 146:
                        msg.msg = "���������� D5";
                        break;
                    case 147:
                        msg.msg = "��������� D6";
                        break;
                    case 148:
                        msg.msg = "���������� D6";
                        break;
                    case 149:
                        msg.msg = "��������� D7";
                        break;
                    case 150:
                        msg.msg = "���������� D7";
                        break;
                    case 151:
                        msg.msg = "��������� D8";
                        break;
                    case 152:
                        msg.msg = "���������� D8";
                        break;
                    case 153:
                        msg.msg = "��������� D9";
                        break;
                    case 154:
                        msg.msg = "���������� D9";
                        break;
                    case 155:
                        msg.msg = "��������� D10";
                        break;
                    case 156:
                        msg.msg = "���������� D10";
                        break;
                    case 157:
                        msg.msg = "��������� D11";
                        break;
                    case 158:
                        msg.msg = "���������� D11";
                        break;
                    case 159:
                        msg.msg = "��������� D12";
                        break;
                    case 160:
                        msg.msg = "���������� D12";
                        break;
                    case 161:
                        msg.msg = "��������� D13";
                        break;
                    case 162:
                        msg.msg = "���������� D13";
                        break;
                    case 163:
                        msg.msg = "��������� D14";
                        break;
                    case 164:
                        msg.msg = "���������� D14";
                        break;
                    case 165:
                        msg.msg = "��������� D15";
                        break;
                    case 166:
                        msg.msg = "���������� D15";
                        break;
                    case 167:
                        msg.msg = "��������� D16";
                        break;
                    case 168:
                        msg.msg = "���������� D16";
                        break;
                    case 169:
                        msg.msg = "��������� I>";
                        break;
                    case 170:
                        msg.msg = "���������� I>";
                        break;
                    case 171:
                        msg.msg = "��������� I>>";
                        break;
                    case 172:
                        msg.msg = "���������� I>>";
                        break;
                    case 173:
                        msg.msg = "��������� I>>>";
                        break;
                    case 174:
                        msg.msg = "���������� I>>>";
                        break;
                    case 175:
                        msg.msg = "��������� I>>>>";
                        break;
                    case 176:
                        msg.msg = "���������� I>>>>";
                        break;
                    case 177:
                        msg.msg = "��������� I2>";
                        break;
                    case 178:
                        msg.msg = "���������� I2>";
                        break;
                    case 179:
                        msg.msg = "��������� I2>>";
                        break;
                    case 180:
                        msg.msg = "���������� I2>>";
                        break;
                    case 181:
                        msg.msg = "��������� I0>";
                        break;
                    case 182:
                        msg.msg = "���������� I0>";
                        break;
                    case 183:
                        msg.msg = "��������� I0>>";
                        break;
                    case 184:
                        msg.msg = "���������� I0>>";
                        break;
                    case 185:
                        msg.msg = "��������� In>";
                        break;
                    case 186:
                        msg.msg = "���������� In>";
                        break;
                    case 187:
                        msg.msg = "��������� In>>";
                        break;
                    case 188:
                        msg.msg = "���������� In>>";
                        break;
                    case 189:
                        msg.msg = "��������� I�";
                        break;
                    case 190:
                        msg.msg = "���������� I�";
                        break;
                    case 191:
                        msg.msg = "��������� I2/I1";
                        break;
                    case 192:
                        msg.msg = "���������� I2/I1";
                        break;
                    case 193:
                        msg.msg = "��������� F>";
                        break;
                    case 194:
                        msg.msg = "���������� F>";
                        break;
                    case 195:
                        msg.msg = "��������� F>>";
                        break;
                    case 196:
                        msg.msg = "���������� F>>";
                        break;
                    case 197:
                        msg.msg = "��������� F<";
                        break;
                    case 198:
                        msg.msg = "���������� F<";
                        break;
                    case 199:
                        msg.msg = "��������� F<<";
                        break;
                    case 200:
                        msg.msg = "���������� F<<";
                        break;
                    case 201:
                        msg.msg = "��������� U>";
                        break;
                    case 202:
                        msg.msg = "���������� U>";
                        break;
                    case 203:
                        msg.msg = "��������� U>>";
                        break;
                    case 204:
                        msg.msg = "���������� U>>";
                        break;
                    case 205:
                        msg.msg = "��������� U<";
                        break;
                    case 206:
                        msg.msg = "���������� U<";
                        break;
                    case 207:
                        msg.msg = "��������� U<<";
                        break;
                    case 208:
                        msg.msg = "���������� U<<";
                        break;
                    case 209:
                        msg.msg = "��������� U2>";
                        break;
                    case 210:
                        msg.msg = "���������� U2>";
                        break;
                    case 211:
                        msg.msg = "��������� U2>>";
                        break;
                    case 212:
                        msg.msg = "���������� U2>>";
                        break;
                    case 213:
                        msg.msg = "��������� U0>";
                        break;
                    case 214:
                        msg.msg = "���������� U0>";
                        break;
                    case 215:
                        msg.msg = "��������� U0>>";
                        break;
                    case 216:
                        msg.msg = "���������� U0>>";
                        break;

                }
                msg.time = value[3].ToString("d2", CultureInfo.InvariantCulture) + "-" + value[2].ToString("d2", CultureInfo.InvariantCulture) + "-" + value[1].ToString("d2", CultureInfo.InvariantCulture) + " " +
                           value[4].ToString("d2", CultureInfo.InvariantCulture) + ":" + value[5].ToString("d2", CultureInfo.InvariantCulture) + ":" + value[6].ToString("d2", CultureInfo.InvariantCulture) + ":" + value[7].ToString("d2", CultureInfo.InvariantCulture);

                return msg;
            }


            #region ICollection Members

            public void CopyTo(Array array, int index)
            {

            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return _messages; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _messages.GetEnumerator();
            }

            #endregion
        }
        #endregion

        #region ������ ������
        public const int ALARMJOURNAL_RECORD_CNT = 32;
        [Browsable(false)]
        public CAlarmJournal AlarmJournal
        {
            get { return _alarmJournalRecords; }
            set { _alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ia;
            public string Ib;
            public string Ic;
            public string I0;
            public string I1;
            public string I2;
            public string In;
            public string Ig;
            public string F;
            public string Uab;
            public string Ubc;
            public string Uca;
            public string U0;
            public string U1;
            public string U2;
            public string Un;
            public string inSignals1;
            public string inSignals2;
        };

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            MR700 _device;

            public CAlarmJournal()
            {

            }

            public CAlarmJournal(MR700 device)
            {
                _device = device;
                _msgDictionary = new Dictionary<ushort, string>(6);
                _codeDictionary = new Dictionary<byte, string>(31);
                _typeDictionary = new Dictionary<byte, string>(26);

                _msgDictionary.Add(0, "������ ����");
                _msgDictionary.Add(1, "������������");
                _msgDictionary.Add(2, "����������");
                _msgDictionary.Add(3, "������");
                _msgDictionary.Add(4, "���������� ���");
                _msgDictionary.Add(5, "�������");
                _msgDictionary.Add(6, "���������");
                _msgDictionary.Add(7, "���");

                _codeDictionary.Add(0, "");
                _codeDictionary.Add(1, "I>");
                _codeDictionary.Add(2, "I>>");
                _codeDictionary.Add(3, "I>>>");
                _codeDictionary.Add(4, "I>>>>");
                _codeDictionary.Add(5, "I2>");
                _codeDictionary.Add(6, "I2>>");
                _codeDictionary.Add(7, "I0>");
                _codeDictionary.Add(8, "I0>>");
                _codeDictionary.Add(9, "In>");
                _codeDictionary.Add(10, "In>>");
                _codeDictionary.Add(11, "Ig>");
                _codeDictionary.Add(12, "I2/I1");
                _codeDictionary.Add(13, "F>");
                _codeDictionary.Add(14, "F>>");
                _codeDictionary.Add(15, "F<");
                _codeDictionary.Add(16, "F<<");
                _codeDictionary.Add(17, "U>");
                _codeDictionary.Add(18, "U>>");
                _codeDictionary.Add(19, "U<");
                _codeDictionary.Add(20, "U<<");
                _codeDictionary.Add(21, "U2>");
                _codeDictionary.Add(22, "U2>>");
                _codeDictionary.Add(23, "U0>");
                _codeDictionary.Add(24, "U0>>");
                _codeDictionary.Add(25, "��-1");
                _codeDictionary.Add(26, "��-2");
                _codeDictionary.Add(27, "��-3");
                _codeDictionary.Add(28, "��-4");
                _codeDictionary.Add(29, "��-5");
                _codeDictionary.Add(30, "��-6");
                _codeDictionary.Add(31, "��-7");
                _codeDictionary.Add(32, "��-8");
                _codeDictionary.Add(33, "���");

                _typeDictionary.Add(0, "");
                _typeDictionary.Add(1, "Ig");
                _typeDictionary.Add(2, "In");
                _typeDictionary.Add(3, "Ia");
                _typeDictionary.Add(4, "Ib");
                _typeDictionary.Add(5, "Ic");
                _typeDictionary.Add(6, "I0");
                _typeDictionary.Add(7, "I1");
                _typeDictionary.Add(8, "I2");
                _typeDictionary.Add(9, "Pn");
                _typeDictionary.Add(10, "Pa");
                _typeDictionary.Add(11, "Pb");
                _typeDictionary.Add(12, "Pc");
                _typeDictionary.Add(13, "P0");
                _typeDictionary.Add(14, "P1");
                _typeDictionary.Add(15, "P2");
                _typeDictionary.Add(16, "F");
                _typeDictionary.Add(17, "Un");
                _typeDictionary.Add(18, "Ua");
                _typeDictionary.Add(19, "Ub");
                _typeDictionary.Add(20, "Uc");
                _typeDictionary.Add(21, "Uab");
                _typeDictionary.Add(22, "Ubc");
                _typeDictionary.Add(23, "Uca");
                _typeDictionary.Add(24, "U0");
                _typeDictionary.Add(25, "U1");
                _typeDictionary.Add(26, "U2");
                _typeDictionary.Add(27, "����� �������");
                _typeDictionary.Add(28, "Q");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);
            
            public string GetTypeValue(ushort damageWord, byte typeByte, string code)
            {
                bool OB = false;
                double damageValue = 0;
                switch (code)
                {
                    case "Ia":
                    case "Ib":
                    case "Ic":
                    case "I0":
                    case "I1":
                    case "I2": damageValue = Measuring.GetI(damageWord, _device.TT, true);
                        break;
                    case "Pn": damageValue = Measuring.GetP(damageWord, (ushort)(_device.TTNP * _device.TNNP), false);
                        break;
                    case "Pa":
                    case "Pb":
                    case "Pc":
                    case "P0":
                    case "P1":
                    case "P2": damageValue = Measuring.GetP(damageWord, (ushort)(_device.TT * _device.TN), true);
                        break;
                    case "In":
                    case "Ig": damageValue = Measuring.GetI(damageWord, _device.TTNP, false);
                        break;
                    case "I2/I1": damageValue = Measuring.GetU(damageWord, 65536);
                        break;
                    case "F":
                    case "Q": damageValue = Measuring.GetConstraintOnly(damageWord, ConstraintKoefficient.K_25600);
                        break;
                    case "Un":
                    case "Ua":
                    case "Ub":
                    case "Uc":
                    case "U0":
                    case "U1":
                    case "U2":
                    case "Uab":
                    case "Ubc":
                    case "Uca": damageValue = Math.Round(Measuring.GetU(damageWord, _device.TN), 1);
                        break;
                    case "����� �������": OB = true;
                        break;
                    default: break;

                }

                try
                {
                    if (OB)
                    {
                        return _typeDictionary[typeByte];
                    }
                    else
                    {
                        return _typeDictionary[typeByte] + string.Format(" = {0:F2}", damageValue);
                    }
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }

            }

            private AlarmRecord CreateMessage(byte[] buffer)
            {
                AlarmRecord rec = new AlarmRecord();
                rec.msg = _msgDictionary[buffer[0]];
                rec.time = buffer[6].ToString("d2", CultureInfo.InvariantCulture) + "-" + buffer[4].ToString("d2", CultureInfo.InvariantCulture) + "-" + buffer[2].ToString("d2", CultureInfo.InvariantCulture) + " " +
                           buffer[8].ToString("d2", CultureInfo.InvariantCulture) + ":" + buffer[10].ToString("d2", CultureInfo.InvariantCulture) + ":" + buffer[12].ToString("d2", CultureInfo.InvariantCulture) + ":" + buffer[14].ToString("d2", CultureInfo.InvariantCulture);
                if (buffer[19] > 28)
                {
                    buffer[19] = 27;
                }
                string group = (0 == (buffer[16] & 0x80)) ? "��������" : "������.";
                string phase = "";
                phase += (0 == (buffer[18] & 0x08)) ? " " : "_";
                phase += (0 == (buffer[18] & 0x01)) ? " " : "A";
                phase += (0 == (buffer[18] & 0x02)) ? " " : "B";
                phase += (0 == (buffer[18] & 0x04)) ? " " : "C";

                byte codeByte = (byte)(buffer[16] & 0x7F);
                if (buffer[0] == 0)
                {
                    rec.code = "";
                }
                else
                {
                    if (codeByte == 33)
                    {
                        rec.code = _codeDictionary[codeByte];
                    }
                    else
                    {
                        rec.code = _codeDictionary[codeByte] + " " + group + " " + phase;
                    }
                }
                if (buffer[19] == 0)
                {
                    rec.type_value = "";
                }
                else
                {
                    try
                    {
                        rec.type_value = GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19], _typeDictionary[buffer[19]]);
                    }
                    catch (Exception mm)
                    {
                        rec.type_value = "";
                    }
                }
                
                double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), _device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), _device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), _device.TT, true);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), _device.TT, true);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), _device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), _device.TT, true);
                double In = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), _device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), _device.TTNP, false);
                double F = Measuring.GetF(Common.TOWORD(buffer[39], buffer[38]));
                double Uab = Measuring.GetU(Common.TOWORD(buffer[41], buffer[40]), _device.TN);
                double Ubc = Measuring.GetU(Common.TOWORD(buffer[43], buffer[42]), _device.TN);
                double Uca = Measuring.GetU(Common.TOWORD(buffer[45], buffer[44]), _device.TN);
                double U0 = Measuring.GetU(Common.TOWORD(buffer[47], buffer[46]), _device.TN);
                double U1 = Measuring.GetU(Common.TOWORD(buffer[49], buffer[48]), _device.TN);
                double U2 = Measuring.GetU(Common.TOWORD(buffer[51], buffer[50]), _device.TN);
                double Un = Measuring.GetU(Common.TOWORD(buffer[53], buffer[52]), _device.TNNP);

                rec.Ia = string.Format("{0:F2}", Ia);
                rec.Ib = string.Format("{0:F2}", Ib);
                rec.Ic = string.Format("{0:F2}", Ic);
                rec.I0 = string.Format("{0:F2}", I0);
                rec.I1 = string.Format("{0:F2}", I1);
                rec.I2 = string.Format("{0:F2}", I2);
                rec.In = string.Format("{0:F2}", In);
                rec.Ig = string.Format("{0:F2}", Ig);
                rec.F = string.Format("{0:F2}", F);
                rec.Uab = string.Format("{0:F2}", Uab);
                rec.Ubc = string.Format("{0:F2}", Ubc);
                rec.Uca = string.Format("{0:F2}", Uca);
                rec.U0 = string.Format("{0:F2}", U0);
                rec.U1 = string.Format("{0:F2}", U1);
                rec.U2 = string.Format("{0:F2}", U2);
                rec.Un = string.Format("{0:F2}", Un);
                byte[] b1 = new byte[] { buffer[54] };
                byte[] b2 = new byte[] { buffer[55] };
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }
        #endregion

        #region ����
        private slot _diagnostic = new slot(0x1800, 0x1812);
        private slot _inputSignals = new slot(0x1000, 0x103C);
        private slot _outputSignals = new slot(0x1200, 0x1270);
        private slot _externalDefenses = new slot(0x1050, 0x1080);
        private slot _countOsc = new slot(0x3F00, 0x3F03);
        private slot _numOsc = new slot(0x3F02, 0x3F03);
        private slot _bdZnakov = new slot(0x1800, 0x181C);


        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];
        private bool _stopAlarmJournal = false;
        private bool _stopSystemJournal = false;
        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private CAlarmJournal _alarmJournalRecords;
        private StringCollection _outputSignalsList = new StringCollection();
        private StringCollection _releList = new StringCollection();
        private COutputRele _outputRele = new COutputRele();

        private slot _konfCount = new slot(0x1274, 0x1275);
        private slot _oscCrush = new slot(0x3800, 0x3801);

        #endregion

        #region �������
        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;
        public event Handler KonfCountLoadOK;
        public event Handler KonfCountLoadFail;
        public event Handler KonfCountSaveOK;
        public event Handler KonfCountSaveFail;
        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler TokDefensesLoadOK;
        public event Handler TokDefensesLoadFail;
        public event Handler TokDefensesSaveOK;
        public event Handler TokDefensesSaveFail;

        public event Handler AnalogSignalsLoadOK;
        public event Handler AnalogSignalsLoadFail;
        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;
        #endregion

        #region ������������ �������������
        public MR700() 
        { 
            Init();

        }

        public MR700(Modbus mb)
        {
            Init();
            MB = mb;
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            _mr700DeviceV2 = new Mr700DeviceV2(this);
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += new BEMN.MBServer.CompleteExchangeHandler(mb_CompleteExchange);
                }
            }
        }


        private void Init()
        {
            HaveVersion = true;
            _alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2000;
            ushort size = 0x8;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                _systemJournal[i] = new slot((ushort)start, (ushort)(start + size));
                start += 0x10;
            }
            start = 0x2800;
            size = 0x1C;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                _alarmJournal[i] = new slot(start, (ushort)(start + size));
                start += 0x40;
            }
            _oscilloscope = new Oscilloscope(this);
            _oscilloscope.Initialize();
        }

        #endregion

        #region �������� ����
        public class COutputRele : ICollection
        {
            public const int LENGTH = 16;
            public const int COUNT = 8;

            public COutputRele()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _releList.Count;
                }
            }

            public void SetBuffer(ushort[] values)
            {
                _releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", (object)LENGTH, "Output rele length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _releList.Add(new OutputReleItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i * 2] = _releList[i].Value[0];
                    ret[i * 2 + 1] = _releList[i].Value[1];
                }
                return ret;
            }

            public OutputReleItem this[int i]
            {
                get
                {
                    return _releList[i];
                }
                set
                {
                    _releList[i] = value;
                }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                _releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _releList.GetEnumerator();
            }

            #endregion
        };

        public class OutputReleItem
        {

            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ����";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Value
            {
                get
                {
                    return new ushort[] { _hiWord, _loWord };
                }
                set
                {
                    _hiWord = value[0];
                    _loWord = value[1];
                }
            }

            public OutputReleItem() { }

            public OutputReleItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get
                {
                    return 0 != Common.HIBYTE(_hiWord);
                }
                set
                {

                    if (value)
                    {
                        _hiWord = Common.TOWORD(0xff, Common.LOBYTE(_hiWord));
                    }
                    else
                    {
                        _hiWord = Common.TOWORD(0x0, Common.LOBYTE(_hiWord));
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BlinkerTypeConverter))]
            public string Type
            {
                get
                {
                    string ret = "";
                    ret = IsBlinker ? "�������" : "�����������";
                    return ret;
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte)Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Description("������������ �������� ����")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(_loWord); }
                set { _loWord = Measuring.SetTime(value); }
            }

            [XmlIgnore]
            [Browsable(false)]
            public ushort ImpulseUshort
            {
                get { return _loWord; }
                set { _loWord = value; }
            }

        };



        [DisplayName("�������� ����")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("�������� �������")]
        [XmlElement("��������_����")]
        public COutputRele OutputRele
        {
            get
            {
                return _outputRele;
            }
        }

        #endregion

        #region �������� ����������
        public class COutputIndicator : ICollection
        {
            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                _indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _indList.GetEnumerator();
            }

            #endregion

            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _indList.Count;
                }
            }


            public const int LENGTH = 16;
            public const int COUNT = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] values)
            {
                _indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values", (object)LENGTH, "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _indList.Add(new OutputIndicatorItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i * 2] = _indList[i].Value[0];
                    ret[i * 2 + 1] = _indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get
                {
                    return _indList[i];
                }
                set
                {
                    _indList[i] = value;
                }
            }

        };

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ���������";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Value
            {
                get
                {
                    return new ushort[] { _hiWord, _loWord };
                }
            }

            public OutputIndicatorItem() { }

            public OutputIndicatorItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }
            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get
                {
                    return 0 != Common.HIBYTE(_hiWord);
                }
                set
                {

                    if (value)
                    {
                        _hiWord = Common.TOWORD(0xff, Common.LOBYTE(_hiWord));
                    }
                    else
                    {
                        _hiWord = Common.TOWORD(0x0, Common.LOBYTE(_hiWord));
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BlinkerTypeConverter))]
            public string Type
            {
                get
                {
                    string ret = "";
                    ret = IsBlinker ? "�������" : "�����������";
                    return ret;
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte)Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("���������")]
            [DisplayName("������ ���������")]
            [Description("������ '����� ���������'")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Indication
            {
                get
                {
                    return 0 != (_loWord & 0x1);
                }
                set
                {
                    _loWord = value ? (ushort)(_loWord | 0x1) : (ushort)(_loWord & ~0x1);
                }
            }
            [XmlAttribute("������")]
            [DisplayName("�. ������")]
            [Description("������ '����� ������� ������'")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Alarm
            {
                get
                {
                    return 0 != (_loWord & 0x2);
                }
                set
                {
                    _loWord = value ? (ushort)(_loWord | 0x2) : (ushort)(_loWord & ~0x2);
                }
            }
            [XmlAttribute("�������")]
            [DisplayName("�. �������")]
            [Description("������ '����� ������� �������'")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool System
            {
                get
                {
                    return 0 != (_loWord & 0x4);
                }
                set
                {
                    _loWord = value ? (ushort)(_loWord | 0x4) : (ushort)(_loWord & ~0x4);
                }
            }
        };

        private COutputIndicator _outputIndicator = new COutputIndicator();

        [XmlElement("��������_����������")]
        [DisplayName("�������� ����������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("�������� �������")]
        public COutputIndicator OutputIndicator
        {
            get
            {
                return _outputIndicator;
            }
        }

        #endregion

        #region ���������� �������

        private slot _analog = new slot(0x1900, 0x1918);

        [System.ComponentModel.Category("���������� �������")]
        public double In
        {
            get
            {
                return Measuring.GetI(_analog.Value[0], TTNP, false);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ia
        {
            get
            {
                return Measuring.GetI(_analog.Value[1], TT, true);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ib
        {
            get
            {
                return Measuring.GetI(_analog.Value[2], TT, true);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ic
        {
            get
            {
                return Measuring.GetI(_analog.Value[3], TT, true);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double I0
        {
            get
            {
                return Measuring.GetI(_analog.Value[4], TT, true);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double I1
        {
            get
            {
                return Measuring.GetI(_analog.Value[5], TT, true);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double I2
        {
            get
            {
                return Measuring.GetI(_analog.Value[6], TT, true);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ig
        {
            get
            {
                return Measuring.GetI(_analog.Value[7], TTNP, false);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Un
        {
            get
            {
                return Measuring.GetU(_analog.Value[8], TNNP);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ua
        {
            get
            {
                return Measuring.GetU(_analog.Value[9], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ub
        {
            get
            {
                return Measuring.GetU(_analog.Value[10], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Uc
        {
            get
            {
                return Measuring.GetU(_analog.Value[11], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Uab
        {
            get
            {
                return Measuring.GetU(_analog.Value[12], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Ubc
        {
            get
            {
                return Measuring.GetU(_analog.Value[13], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Uca
        {
            get
            {
                return Measuring.GetU(_analog.Value[14], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double U0
        {
            get
            {
                return Measuring.GetU(_analog.Value[15], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double U1
        {
            get
            {
                return Measuring.GetU(_analog.Value[16], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double U2
        {
            get
            {
                return Measuring.GetU(_analog.Value[17], TN);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double F
        {
            get
            {
                return Measuring.GetF(_analog.Value[18]);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public double Q
        {
            get
            {
                return Measuring.GetConstraint(_analog.Value[19], ConstraintKoefficient.K_25600, DeviceVersion);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public int Npusk
        {
            get
            {
                return (int)Common.HIBYTE(_analog.Value[20] & 255);
            }
        }
        [System.ComponentModel.Category("���������� �������")]
        public int Ngor
        {
            get
            {
                return (int)Common.HIBYTE(_analog.Value[21] & 255);
            }
        }

        #endregion

        #region ���������� �������

        public string LoadZnaki(ushort[] masInputSignals)
        {
            byte[] buffer1 = Common.TOBYTES(masInputSignals, false);
            byte[] b1 = new byte[] { buffer1[33] };
            byte[] b2 = new byte[] { buffer1[34] };
            string r1 = Common.BitsToString(new BitArray(b1));
            string r2 = Common.BitsToString(new BitArray(b2));
            string res = r1 + r2;
            return res;
        }

        public string BdZnaki
        {
            get
            {
                return LoadZnaki(_diagnostic.Value);
            }
        }



        [XmlIgnore]
        [DisplayName("����������� �������")]
        [Description("����������� ������� ����")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray ManageSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0]), Common.HIBYTE(_diagnostic.Value[0]) });
            }
        }
        [XmlIgnore]
        [DisplayName("�������������� �������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[2]) });
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }
        [XmlIgnore]
        [DisplayName("����������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray Indicators
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[2]) });
            }
        }
        [XmlIgnore]
        [DisplayName("������� �������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0x9]),
                                                 Common.HIBYTE(_diagnostic.Value[0x9]),
                                                 Common.LOBYTE(_diagnostic.Value[0xA])});
            }
        }
        [XmlIgnore]
        [DisplayName("����")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray Rele
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[3]) });
            }
        }
        [XmlIgnore]
        [DisplayName("�������� �������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray OutputSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0xA]) });
            }
        }
        [XmlIgnore]
        [DisplayName("������� ��������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]{Common.HIBYTE(_diagnostic.Value[0xA]),
                                               Common.LOBYTE(_diagnostic.Value[0xB]),
                                               Common.HIBYTE(_diagnostic.Value[0xB]),
                                               Common.LOBYTE(_diagnostic.Value[0xC]),
                                               Common.HIBYTE(_diagnostic.Value[0xC]),
                                               Common.LOBYTE(_diagnostic.Value[0xD]), 
                                               Common.HIBYTE(_diagnostic.Value[0xD])});
            }
        }
        [XmlIgnore]
        [DisplayName("��������� �������������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray FaultState
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[4]) });
            }
        }
        [XmlIgnore]
        [DisplayName("������� �������������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray FaultSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[5]), Common.HIBYTE(_diagnostic.Value[5]) });
            }
        }
        [XmlIgnore]
        [DisplayName("����������")]
        [System.ComponentModel.Category("���������� �������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray Automation
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[8]) });
            }
        }
        #endregion

        #region ��������� ���� � ����������
        [XmlElement("��")]
        [DisplayName("��")]
        [Description("��������� ��� ��������������")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        public ushort TT
        {
            get
            {
                return _inputSignals.Value[1];
            }
            set
            {
                _inputSignals.Value[1] = value;
            }
        }
        [XmlElement("����")]
        [DisplayName("����")]
        [Description("��������� ��� �������������� ������� ������������������")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        public ushort TTNP
        {
            get
            {
                return _inputSignals.Value[2];
            }
            set
            {
                _inputSignals.Value[2] = value;
            }
        }
        [XmlElement("���_��")]
        [DisplayName("��� ��")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        [TypeConverter(typeof(TN_TypeConverter))]
        public string TN_Type
        {
            get
            {
                return Strings.TN_Type[_inputSignals.Value[8]];
            }
            set
            {
                _inputSignals.Value[8] = (ushort)Strings.TN_Type.IndexOf(value);
            }
        }
        [XmlElement("��")]
        [DisplayName("��")]
        [Description("����������� ��")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        public double TN
        {
            get { return Measuring.GetConstraint(_inputSignals.Value[9], ConstraintKoefficient.K_Undefine, DeviceVersion); }
            set { _inputSignals.Value[9] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine, DeviceVersion); }
        }
        [XmlElement("�������������_��")]
        [DisplayName("������������� ��")]
        [Description("������� ������������� ��")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string TN_Dispepair
        {
            get
            {
                try
                {
                    return Strings.Logic[_inputSignals.Value[10]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return Strings.Logic[Strings.Logic.Count - 1];
                }

            }
            set
            {
                _inputSignals.Value[10] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        #region
        [XmlElement("���_���")]
        [DisplayName("��� ���")]
        [System.ComponentModel.Category("����������� ����� �����������")]
        [TypeConverter(typeof(TN_TypeConverter))]
        #endregion
        #region
        public string OMP_Type
        {
            get
            {
                return Strings.ModesLight[_inputSignals.Value[13]];
            }
            set
            {
                _inputSignals.Value[13] = (ushort)Strings.ModesLight.IndexOf(value);
            }
        }
        #endregion

        #region
        [XmlElement("���")]
        [DisplayName("���")]
        [System.ComponentModel.Category("����������� ����� �����������")]
        #endregion
        #region
        public double HUD
        {
            get
            {
                return _inputSignals.Value[14];
            }
            set
            {
                _inputSignals.Value[14] = (ushort)(value * 1000);
            }
        }
        #endregion

        [XmlElement("�������������_����")]
        [DisplayName("������������� ����")]
        [Description("������� ������������� ����")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string TNNP_Dispepair
        {
            get
            {
                try
                {
                    return Strings.Logic[_inputSignals.Value[12]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return Strings.Logic[Strings.Logic.Count - 1];
                }

            }
            set
            {
                _inputSignals.Value[12] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("����")]
        [DisplayName("����")]
        [Description("����������� ����")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        public double TNNP
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[0xB], ConstraintKoefficient.K_Undefine, DeviceVersion);
            }
            set
            {
                _inputSignals.Value[0xB] = Measuring.SetConstraint(value, ConstraintKoefficient.K_Undefine);
            }
        }
        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Description("������������ ��� ��������")]
        [System.ComponentModel.Category("��������� ���� � ����������")]
        public double MaxTok
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[3], ConstraintKoefficient.K_4001, DeviceVersion);
            }
            set
            {
                _inputSignals.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
            }
        }
        #endregion

        #region ������� �������

        public virtual double POscilloscopeCrush
        {
            set
            {
                _oscCrush.Value[0] = (ushort)value;
            }
        }

        public virtual double OscilloscopeKonfCount
        {
            get
            {
                return _konfCount.Value[0];
            }
            set
            {
                _konfCount.Value[0] = (ushort)value;
            }
        }

        [XmlIgnore]
        [DisplayName("������� �������������")]
        [Description("������� �������������")]
        [System.ComponentModel.Category("������� �������")]
        [Editor(typeof(BitsEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray DispepairSignal
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_inputSignals.Value[0x1D]) });
            }
            set
            {
                _inputSignals.Value[0x1D] = Common.BitsToUshort(value);
            }
        }

        [XmlElement("�������_�������������")]
        [DisplayName("������� �������������")]
        [System.ComponentModel.Category("������� �������")]
        [Editor(typeof(BitsEditor), typeof(UITypeEditor))]
        public ulong DispepairImpulse
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[0x1E]);
            }
            set
            {
                _inputSignals.Value[0x1E] = Measuring.SetTime(value);
            }
        }



        [XmlElement("����_��������")]
        [DisplayName("���� ��������")]
        [Description("����� ����� ����� ��������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string KeyOn
        {
            get
            {
                ushort index = _inputSignals.Value[0x10];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x10] = (ushort)(Strings.Logic.IndexOf(value));
            }

        }
        [XmlElement("����_���������")]
        [DisplayName("���� ���������")]
        [Description("����� ����� ����� ���������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string KeyOff
        {
            get
            {
                ushort index = _inputSignals.Value[0x11];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x11] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("�������_��������")]
        [DisplayName("������� ��������")]
        [Description("����� ����� ������� ��������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string ExternalOn
        {
            get
            {
                ushort index = _inputSignals.Value[0x12];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x12] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("�������_���������")]
        [DisplayName("������� ���������")]
        [Description("����� ����� ������� ���������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string ExternalOff
        {
            get
            {
                ushort index = _inputSignals.Value[0x13];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x13] = (ushort)(Strings.Logic.IndexOf(value));
            }

        }
        [XmlElement("������������_�������")]
        [DisplayName("������������ �������")]
        [Description("������� ������ ������ �������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string ConstraintGroup
        {
            get
            {
                ushort index = _inputSignals.Value[0x15];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x15] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("����_������������")]
        [DisplayName("���� ������������")]
        [Description("������� ������ ���� ������������")]
        [System.ComponentModel.Category("������� �������")]
        public string OscilloscopeStart
        {
            get
            {
                ushort index = _inputSignals.Value[0x16];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x16] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }

        [XmlElement("��������_���������")]
        [DisplayName("�������� ���������")]
        [Description("�������� ���������")]
        [System.ComponentModel.Category("������� �������")]
        public string KontrDiskr
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x17], 0) ? Strings.ModesLight[1] : Strings.ModesLight[0];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.ModesLight[0]) ? false : true;
                _inputSignals.Value[0x17] = Common.SetBit(_inputSignals.Value[0x17], 0, bit);
            }
        }

        [XmlElement("��������_��")]
        [DisplayName("�������� ��")]
        [Description("�������� ��")]
        [System.ComponentModel.Category("������� �������")]
        public string KontrIO
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x17], 1) ? Strings.ModesLight[1] : Strings.ModesLight[0];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.ModesLight[0]) ? false : true;
                _inputSignals.Value[0x17] = Common.SetBit(_inputSignals.Value[0x17], 1, bit);
            }
        }

        [XmlElement("�����_������������")]
        [DisplayName("����� ������������")]
        [Description("������� ������ ����� ������������")]
        [System.ComponentModel.Category("������� �������")]
        public string SignalizationReset
        {
            get
            {
                ushort index = _inputSignals.Value[0x14];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[0x14] = (ushort)(Strings.Logic.IndexOf(value));
            }
        }

        #endregion

        #region ������� ����������

        [XmlElement("������_��_������")]
        [DisplayName("������ �� ������")]
        [System.ComponentModel.Category("������� �������")]
        [Description("����������� ������ �� ������")]
        [TypeConverter(typeof(ForbiddenTypeConverter))]
        public string ManageSignalButton
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 0) ? Strings.Forbidden[0] : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 0, bit);
            }
        }
        [XmlElement("������_��_�����")]
        [DisplayName("������ �� �����")]
        [System.ComponentModel.Category("������� �������")]
        [Description("����������� ������ �� �����")]
        [TypeConverter(typeof(ControlTypeConverter))]
        public string ManageSignalKey
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 1) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 1, bit);
            }
        }
        [XmlElement("������_��_��������")]
        [DisplayName("������ �� ��������")]
        [System.ComponentModel.Category("������� �������")]
        [Description("����������� ������ �� �������� ����������")]
        [TypeConverter(typeof(ControlTypeConverter))]
        public string ManageSignalExternal
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 2) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 2, bit);
            }
        }
        [XmlElement("������_��_����")]
        [DisplayName("������ �� ����")]
        [System.ComponentModel.Category("������� �������")]
        [Description("����������� ������ �� ����")]
        [TypeConverter(typeof(ForbiddenTypeConverter))]
        public string ManageSignalSDTU
        {
            get
            {

                string ret = Common.GetBit(_inputSignals.Value[0x3B], 3) ? Strings.Forbidden[0] : Strings.Forbidden[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? true : false;
                _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 3, bit);
            }
        }

        #endregion

        #region �����������
        [XmlElement("�����������_���������")]
        [DisplayName("����������� - ����������")]
        [Description("����������� - ��������� ����������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherOff
        {

            get
            {
                ushort index = _inputSignals.Value[50];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[50] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("�����������_��������")]
        [DisplayName("����������� - ���������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherOn
        {
            get
            {
                ushort index = _inputSignals.Value[51];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[51] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("�����������_������")]
        [DisplayName("����������� - �������������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherError
        {
            get
            {
                ushort index = _inputSignals.Value[52];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[52] = (ushort)(Strings.Logic.IndexOf(value));
            }


        }
        [XmlElement("�����������_����������")]
        [DisplayName("����������� - ����������")]
        [System.ComponentModel.Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string SwitcherBlock
        {
            get
            {
                ushort index = _inputSignals.Value[53];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[(int)index];
            }
            set
            {
                _inputSignals.Value[53] = (ushort)(Strings.Logic.IndexOf(value));
            }

        }
        [XmlElement("�����������_�����_����")]
        [DisplayName("����������� - ����� ����")]
        [System.ComponentModel.Category("������� �������")]
        public ulong SwitcherTimeUROV
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[54]);
            }
            set
            {
                _inputSignals.Value[54] = Measuring.SetTime(value);
            }
        }
        [XmlElement("�����������_���_����")]
        [DisplayName("����������� - ��� ����")]
        [System.ComponentModel.Category("������� �������")]
        public double SwitcherTokUROV
        {
            get
            {
                return Measuring.GetConstraint(_inputSignals.Value[55], ConstraintKoefficient.K_4001, DeviceVersion);
            }
            set
            {
                _inputSignals.Value[55] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
            }

        }
        [XmlElement("�����������_�������")]
        [DisplayName("����������� - �������")]
        [System.ComponentModel.Category("������� �������")]
        public ulong SwitcherImpulse
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[56]);
            }
            set
            {
                _inputSignals.Value[56] = Measuring.SetTime(value);
            }

        }
        [XmlElement("�����������_������������")]
        [DisplayName("����������� - ������������")]
        [Description("����������� -   ������������ ��������� ������")]
        [System.ComponentModel.Category("������� �������")]
        public ulong SwitcherDuration
        {
            get
            {
                return Measuring.GetTime(_inputSignals.Value[57]);
            }
            set
            {
                _inputSignals.Value[57] = Measuring.SetTime(value);
            }

        }

        #endregion

        #region �������� ���������� �������

        //[TypeConverter(typeof(RussianCollectionEditor))]
        //[Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        //public List<BitArray> OutputLogicSignals
        //{
        //    get
        //    {
        //       List<BitArray> _outputLogicSignals = new List<BitArray>(8);
        //        for (int i = 0; i < 8; i++)
        //        {
        //            _outputLogicSignals.Add(GetOutputLogicSignals(i));
        //        }
        //        return _outputLogicSignals;
        //    }
        //    set
        //    {
        //        for (int i = 0; i < 8; i++)
        //        {
        //            SetOutputLogicSignals(i, value[i]);
        //        }
        //    }
        //}

        private const int OUTPUTLOGIC_BITS_CNT = 96;
        public BitArray GetOutputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }

            ushort[] logicBuffer = new ushort[8];
            Array.ConstrainedCopy(_outputSignals.Value, channel * 8, logicBuffer, 0, 8);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(OUTPUTLOGIC_BITS_CNT);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;

        }

        public void SetOutputLogicSignals(int channel, BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (104 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i * 16 + j < 104)
                    {
                        if (values[i * 16 + j])
                        {
                            logicBuffer[i] += (ushort)Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, _outputSignals.Value, channel * 8, 8);

        }
        #endregion

        #region ������� ���������� �������

        public LogicState[] GetInputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[16];
            SetLogicStates(_inputSignals.Value[0x22 + channel * 2], true, ref ret);
            SetLogicStates(_inputSignals.Value[0x22 + channel * 2 + 1], false, ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < logicSignals.Length; i++)
            {
                if (i < 8)
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        firstWord += (ushort)Math.Pow(2, i);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        firstWord += (ushort)Math.Pow(2, i);
                        firstWord += (ushort)Math.Pow(2, i + 8);
                    }
                }
                else
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        secondWord += (ushort)Math.Pow(2, i - 8);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        secondWord += (ushort)Math.Pow(2, i - 8);
                        secondWord += (ushort)Math.Pow(2, i);
                    }
                }
            }
            _inputSignals.Value[0x22 + channel * 2] = firstWord;
            _inputSignals.Value[0x22 + channel * 2 + 1] = secondWord;
        }

        void SetLogicStates(ushort value, bool firstWord, ref  LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = firstWord ? 0 : 8;
            int end = firstWord ? 8 : 16;

            for (int i = start; i < end; i++)
            {
                int pow = firstWord ? i : i - 8;
                logicArray[i] = (byte)(Math.Pow(2, pow)) != ((lowByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.��;
                //logicArray[i] = (byte)(Math.Pow(2, pow)) != ((hiByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.������;
                logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.��) ? LogicState.������ : logicArray[i];
            }
        }
        private bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte)(Math.Pow(2, pow)) == ((hiByte) & (byte)(Math.Pow(2, pow)));
        }
        #endregion

        #region �������� ����������
        //���� ����������
        private slot _automaticsPage = new slot(0x103c, 0x1050);
        private BitArray _bits = new BitArray(8);
        //������� �������� ����������
        public event Handler AutomaticsPageLoadOK;
        public event Handler AutomaticsPageLoadFail;
        public event Handler AutomaticsPageSaveOK;
        public event Handler AutomaticsPageSaveFail;

        public void LoadAutomaticsPage()
        {
            LoadSlot(DeviceNumber, _automaticsPage, "LoadAutomaticsPage" + DeviceNumber, this);
        }

        public void SaveAutomaticsPage()
        {
            SaveSlot(DeviceNumber, _automaticsPage, "SaveAutomaticsPage" + DeviceNumber, this);
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(ModeTypeConverter))]
        public string APV_Cnf
        {
            get
            {
                byte index = (byte)(Common.LOBYTE(_automaticsPage.Value[0]));
                if (index >= Strings.Crat.Count)
                {
                    index = (byte)(Strings.Crat.Count - 1);
                }
                return Strings.Crat[index];
            }
            set { _automaticsPage.Value[0] = Common.TOWORD(Common.HIBYTE(_automaticsPage.Value[0]), (byte)Strings.Crat.IndexOf(value)); }
        }

        [XmlElement("����������_���")]
        [DisplayName("���������� ���")]
        [Description("����� ����� ���������� ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string APV_Blocking
        {
            get
            {
                byte index = (byte)(Common.LOBYTE(_automaticsPage.Value[1]));
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[1] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("����� ���")]
        [Description("����� ���������� ���")]
        [System.ComponentModel.Category("����������")]
        public ulong APV_Time_Blocking
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[2]);
            }
            set
            {
                _automaticsPage.Value[2] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("���������� ���")]
        [Description("����� ���������� ���")]
        [System.ComponentModel.Category("����������")]
        public ulong APV_Time_Ready
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[3]);
            }
            set
            {
                _automaticsPage.Value[3] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�����_1_�����_���")]
        [DisplayName("���� 1 ���")]
        [Description("����� 1����� ���")]
        [System.ComponentModel.Category("����������")]

        public ulong APV_Time_1Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[4]);
            }
            set
            {
                _automaticsPage.Value[4] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�����_2_�����_���")]
        [DisplayName("���� 2 ���")]
        [Description("����� 2����� ���")]
        [System.ComponentModel.Category("����������")]

        public ulong APV_Time_2Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[5]);
            }
            set
            {
                _automaticsPage.Value[5] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�����_3_�����_���")]
        [DisplayName("���� 3 ���")]
        [Description("����� 3����� ���")]
        [System.ComponentModel.Category("����������")]

        public ulong APV_Time_3Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[6]);
            }
            set
            {
                _automaticsPage.Value[6] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�����_4_�����_���")]
        [DisplayName("���� 4 ���")]
        [Description("����� 4����� ���")]
        [System.ComponentModel.Category("����������")]

        public ulong APV_Time_4Krat
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[7]);
            }
            set
            {
                _automaticsPage.Value[7] = Measuring.SetTime(value);
            }
        }

        [XmlElement("������_���_��_��������������")]
        [DisplayName("�������������� ���")]
        [Description("������ ��� �� ��������������")]
        [TypeConverter(typeof(YesNoTypeConverter))]
        [System.ComponentModel.Category("����������")]

        public string APV_Start
        {
            get
            {
                byte index = (byte)(Common.HIBYTE(_automaticsPage.Value[0]));
                if (index >= Strings.YesNo.Count)
                {
                    index = (byte)(Strings.YesNo.Count - 1);
                }
                return Strings.YesNo[index];

            }
            set { _automaticsPage.Value[0] = Common.TOWORD((byte)Strings.YesNo.IndexOf(value), Common.LOBYTE(_automaticsPage.Value[0])); }
        }

        [XmlElement("����������_�������_���")]
        [DisplayName("������� ���")]
        [Description("���������� ������� ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("����������")]

        public bool AVR_Supply_Off
        {
            get
            {
                return AVR_Add_Signals[0];
            }
            set
            {
                SetCurrentBit(8, 0, value);
            }
        }

        private void SetCurrentBit(byte numWord, byte numBit, bool val)
        {
            if (val)
            {
                if (((_automaticsPage.Value[numWord] >> numBit) & 1) == 0)
                {
                    _automaticsPage.Value[numWord] += (ushort)Math.Pow(2, numBit);
                }

            }
            else
            {
                if (((_automaticsPage.Value[numWord] >> numBit) & 1) == 1)
                {
                    _automaticsPage.Value[numWord] -= (ushort)Math.Pow(2, numBit);
                }
            }
            _bits[numBit] = val;
        }

        [XmlElement("�������_����������_�����������_���")]
        [DisplayName("���������� ���")]
        [Description("������� ���������� ����������� ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("����������")]
        public bool AVR_Switch_Off
        {
            get
            {
                return AVR_Add_Signals[2];
            }
            set
            {
                SetCurrentBit(8, 2, value);
            }

        }

        [XmlElement("��������������_���")]
        [DisplayName("�������������� ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("����������")]
        public bool AVR_Self_Off
        {
            get
            {
                return AVR_Add_Signals[1];
            }
            set
            {
                SetCurrentBit(8, 1, value);
            }
        }

        [XmlElement("������������_������_���")]
        [DisplayName("������ ���")]
        [Description("������������ ������ ���")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("����������")]
        public bool AVR_Abrasion_Switch
        {
            get
            {
                return AVR_Add_Signals[3];
            }
            set
            {
                SetCurrentBit(8, 3, value);
            }
        }

        [XmlElement("����������_������_���_��_���������_�����������")]
        [DisplayName("����� ���")]
        [Description("���������� ������ ��� �� ��������� �����������")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [System.ComponentModel.Category("����������")]

        public bool AVR_Reset_Switch
        {
            get
            {
                return AVR_Add_Signals[7];
            }
            set
            {
                SetCurrentBit(8, 7, value);
            }
        }

        [XmlIgnore]
        [DisplayName("������� ���")]
        [Editor(typeof(BitsEditor), typeof(UITypeEditor))]
        [System.ComponentModel.Category("����������")]
        private BitArray AVR_Add_Signals
        {
            get
            {
                return _bits;
            }
            set
            {
                _bits = value;
            }

        }

        [XmlElement("����������_���")]
        [DisplayName("���������� ���")]
        [Description("����� ����� ���������� ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Blocking
        {
            get
            {
                ushort index = _automaticsPage.Value[9];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[9] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("����� ���")]
        [Description("����� ���������� ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Reset
        {
            get
            {
                ushort index = _automaticsPage.Value[10];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[10] = (ushort)Strings.Logic.IndexOf(value);
            }

        }

        [XmlElement("������_���")]
        [DisplayName("������ ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Start
        {
            get
            {
                ushort index = _automaticsPage.Value[11];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[11] = (ushort)Strings.Logic.IndexOf(value);
            }

        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Abrasion
        {
            get
            {
                ushort index = _automaticsPage.Value[12];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[12] = (ushort)Strings.Logic.IndexOf(value);
            }

        }

        [XmlElement("�����_������������_���")]
        [DisplayName("����� c����������� ���")]
        [System.ComponentModel.Category("����������")]
        public ulong AVR_Time_Abrasion
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[13]);
            }
            set
            {
                _automaticsPage.Value[13] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�������_���")]
        [DisplayName("������� ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string AVR_Return
        {
            get
            {
                ushort index = _automaticsPage.Value[14];
                if (index >= Strings.Logic.Count)
                {
                    index = (byte)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _automaticsPage.Value[14] = (ushort)Strings.Logic.IndexOf(value);
            }
        }

        [XmlElement("�����_��������_���")]
        [DisplayName("����� �������� ���")]
        [System.ComponentModel.Category("����������")]
        public ulong AVR_Time_Return
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[15]);
            }
            set
            {
                _automaticsPage.Value[15] = Measuring.SetTime(value);
            }
        }

        [XmlElement("�����_����������_���")]
        [DisplayName("����� ���������� ���")]
        [System.ComponentModel.Category("����������")]
        public ulong AVR_Time_Off
        {
            get
            {
                return Measuring.GetTime(_automaticsPage.Value[16]);
            }
            set
            {
                _automaticsPage.Value[16] = Measuring.SetTime(value);
            }
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(ModeLightTypeConverter))]
        public string LZSH_Cnf
        {
            get
            {
                ushort index = _automaticsPage.Value[18];
                if (index >= Strings.ModesLight.Count)
                {
                    index = (byte)(Strings.ModesLight.Count - 1);
                }
                return Strings.ModesLight[index];
            }
            set
            {
                _automaticsPage.Value[18] = (ushort)Strings.ModesLight.IndexOf(value);
            }
        }

        [XmlElement("������������_���1_17")]
        [DisplayName("������������ ���1_17")]
        [System.ComponentModel.Category("����������")]
        [TypeConverter(typeof(ModeLightTypeConverter))]
        public string LZSH_Cnf1_17
        {
            get
            {
                ushort index = _automaticsPage.Value[18];
                if (index >= Strings.LZS1_17.Count)
                {
                    index = (byte)(Strings.LZS1_17.Count - 1);
                }
                return Strings.LZS1_17[index];
            }
            set
            {
                _automaticsPage.Value[18] = (ushort)Strings.LZS1_17.IndexOf(value);
            }
        }

        [XmlElement("�������_���")]
        [DisplayName("������� ���")]
        [System.ComponentModel.Category("����������")]
        public double LZSH_Constraint
        {
            get
            {
                return Measuring.GetConstraint(_automaticsPage.Value[19], ConstraintKoefficient.K_4001, DeviceVersion);
            }
            set
            {
                _automaticsPage.Value[19] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
            }
        }

        #endregion

        #region ������� ������

        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 48;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(ExternalDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", (object)LENGTH, "External defense length must be 48");
                }
                for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                    _defenseList.Add(new ExternalDefenseItem(temp));
                }
            }
            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * ExternalDefenseItem.LENGTH, ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ExternalDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }
        };

        public class ExternalDefenseItem
        {
            public const int LENGTH = 6;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem() { }
            public ExternalDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }
            public override string ToString()
            {
                return "������� ������";
            }
            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value", (object)LENGTH, "External defense item length must be 6");
                }
                _values = buffer;
            }
            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2);
                    if (index >= Strings.ModesExternal.Count)
                    {
                        index = (byte)(Strings.ModesExternal.Count - 1);
                    }
                    return Strings.ModesExternal[index];
                }
                set { _values[0] = Common.SetBits(_values[0], (ushort)Strings.ModesExternal.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Reset
            {
                get
                {
                    return Common.GetBit(_values[0], 13);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 13, value);
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return 0 != (_values[0] & 32);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 32) : (ushort)(_values[0] & ~32);
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return 0 != (_values[0] & 64);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 64) : (ushort)(_values[0] & ~64);
                }
            }
            [XmlAttribute("����")]
            [DisplayName("����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return 0 != (_values[0] & 128);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 128) : (ushort)(_values[0] & ~128);
                }
            }

            [XmlAttribute("�������_���")]
            [DisplayName("��� ��������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV_Return
            {
                get
                {
                    return 0 != (_values[0] & 0x8000);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 0x8000) : (ushort)(_values[0] & ~0x8000);
                }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Return
            {
                get
                {
                    return 0 != (_values[0] & 0x4000);
                }
                set
                {
                    _values[0] = value ? (ushort)(_values[0] |= 0x4000) : (ushort)(_values[0] & ~0x4000);
                }
            }


            [XmlAttribute("����������_�����")]
            [DisplayName("����� ����������")]
            [Description("����� ����� ���������� ��")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ExternalDefensesTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = _values[1];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort)(Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set
                {
                    _values[1] = (ushort)Strings.ExternalDefense.IndexOf(value);
                }
            }
            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("����� ����� ������������ ��")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ExternalDefensesTypeConverter))]
            public string WorkingNumber
            {
                get
                {
                    ushort value = _values[2];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort)(Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set
                {
                    _values[2] = (ushort)Strings.ExternalDefense.IndexOf(value);
                }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������ ��")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong WorkingTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }
            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("����� ����� �������� ��")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ExternalDefensesTypeConverter))]
            public string ReturnNumber
            {
                get
                {
                    ushort value = _values[4];
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort)(Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set
                {
                    _values[4] = (ushort)Strings.ExternalDefense.IndexOf(value);
                }
            }
            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("�������� ������� �������� ��")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong ReturnTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }
        }

        private CExternalDefenses _cexternalDefenses = new CExternalDefenses();

        [XmlElement("�������_������")]
        [DisplayName("������� ������")]
        [Description("������� ������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������� ������")]
        public CExternalDefenses ExternalDefenses
        {
            get { return _cexternalDefenses; }
            set { _cexternalDefenses = value; }
        }


        #endregion

        #region ������� ������

        private slot _tokDefensesMain = new slot(0x1080, 0x10C0);
        private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
        private slot _tokDefenses2Main = new slot(0x1100, 0x1120);
        private slot _tokDefenses2Reserve = new slot(0x1120, 0x1140);

        public void LoadTokDefenses()
        {
            LoadSlot(DeviceNumber, _tokDefenses2Reserve, "LoadTokDefensesReserve2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _tokDefensesReserve, "LoadTokDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _tokDefenses2Main, "LoadTokDefensesMain2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _tokDefensesMain, "LoadTokDefensesMain" + DeviceNumber, this);
        }

        public void SaveTokDefenses()
        {
            Array.ConstrainedCopy(TokDefensesMain.ToUshort1(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort2(), 0, _tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort1(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort2(), 0, _tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);

            SaveSlot(DeviceNumber, _tokDefenses2Main, "SaveTokDefensesMain2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _tokDefenses2Reserve, "SaveTokDefensesReserve2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _tokDefensesReserve, "SaveTokDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _tokDefensesMain, "SaveTokDefensesMain" + DeviceNumber, this);
        }

        private CTokDefenses _ctokDefensesMain = new CTokDefenses();
        private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

        [XmlElement("�������_������_��������")]
        [DisplayName("�������� ������")]
        [Description("������� ������ - �������� ������ �������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������� ������")]
        public CTokDefenses TokDefensesMain
        {
            get
            {
                return _ctokDefensesMain;
            }
            set
            {
                _ctokDefensesMain = value;
            }
        }
        [XmlElement("�������_������_���������")]
        [DisplayName("��������� ������")]
        [Description("������� ������ - ��������� ������ �������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������� ������")]
        public CTokDefenses TokDefensesReserve
        {
            get
            {
                return _ctokDefensesReserve;
            }
            set
            {
                _ctokDefensesReserve = value;
            }
        }


        public class CTokDefenses : ICollection
        {
            public const int LENGTH1 = 64;
            public const int COUNT1 = 10;
            public const int LENGTH2 = 16;
            public const int COUNT2 = 2;
            private ushort[] _buffer;


            #region ICollection Members

            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _items.Count;
                }
            }

            public void Add(TokDefenseItem item)
            {
                _items.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _items.GetEnumerator();
            }

            #endregion

            public ushort I
            {
                get { return _buffer[0]; }
                set { _buffer[0] = value; }
            }
            public ushort I0
            {
                get { return _buffer[1]; }
                set { _buffer[1] = value; }
            }
            public ushort In
            {
                get { return _buffer[2]; }
                set { _buffer[2] = value; }
            }
            public ushort I2
            {
                get { return _buffer[3]; }
                set { _buffer[3] = value; }
            }

            private List<TokDefenseItem> _items = new List<TokDefenseItem>(12);

            public CTokDefenses()
            {
                SetBuffer(new ushort[160]);
            }
            public TokDefenseItem this[int i]
            {
                get
                {
                    return _items[i];
                }
                set
                {
                    _items[i] = value;
                }
            }

            public void SetBuffer(ushort[] buffer)
            {
                _buffer = buffer;
                _items.Clear();
                for (int i = 0; i < TokDefenseItem.LENGTH * 12; i += TokDefenseItem.LENGTH)
                {
                    if (i == TokDefenseItem.LENGTH * 11)
                    {
                        i += 2;
                    }
                    ushort[] item = new ushort[TokDefenseItem.LENGTH];
                    Array.ConstrainedCopy(_buffer, i + 4, item, 0, TokDefenseItem.LENGTH);
                    _items.Add(new TokDefenseItem(item));
                }

                for (int i = 0; i < 12; i++)
                {
                    _items[i].IsIncrease = i < 4 ? true : false;
                    _items[i].IsKoeff500 = i < 8 ? false : true;
                    _items[i].IsI12 = false;

                }

                _items[11].IsI12 = true;

                _items[0].Name = "I>";
                _items[1].Name = "I>>";
                _items[2].Name = "I>>>";
                _items[3].Name = "I>>>>";
                _items[4].Name = "I2>";
                _items[5].Name = "I2>>";
                _items[6].Name = "I0>";
                _items[7].Name = "I0>>";
                _items[8].Name = "In>";
                _items[9].Name = "In>>";
                _items[10].Name = "Ig";
                _items[11].Name = "I2/I1";
            }

            public ushort[] ToUshort1()
            {
                ushort[] buffer = new ushort[LENGTH1];
                buffer[0] = I;
                buffer[1] = I0;
                buffer[2] = In;
                buffer[3] = I2;

                for (int i = 0; i < COUNT1; i++)
                {
                    Array.ConstrainedCopy(_items[i].Values, 0, buffer, 4 + i * TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                }
                return buffer;
            }
            public ushort[] ToUshort2()
            {
                ushort[] buffer = new ushort[LENGTH2];

                Array.ConstrainedCopy(_items[10].Values, 0, buffer, 0, TokDefenseItem.LENGTH);
                Array.ConstrainedCopy(_items[11].Values, 0, buffer, 2 + TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                return buffer;
            }
        };

        public class TokDefenseItem
        {
            public const int LENGTH = 6;
            private bool _isKoeff500;

            [XmlIgnore]
            [Browsable(false)]
            public bool IsKoeff500
            {
                get { return _isKoeff500; }
                set { _isKoeff500 = value; }
            }

            private bool _isIncrease;
            private string _name;
            private ushort[] _values = new ushort[LENGTH];

            public TokDefenseItem() { }
            public TokDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            private bool _isI12 = false;

            public bool IsI12
            {
                get { return _isI12; }
                set { _isI12 = value; }
            }


            [XmlIgnore]
            [Browsable(false)]
            public bool IsIncrease
            {
                get { return _isIncrease; }
                set { _isIncrease = value; }
            }
            [XmlIgnore]
            [Browsable(false)]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Tok defense increase1 item value", (object)LENGTH, "Tok defense increase1 item length must be 6");
                }
                _values = buffer;
            }
            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte)(Common.LOBYTE(_values[0]) & 0x07);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }
            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("����")]
            [DisplayName("����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("����������_��������")]
            [DisplayName("���������� ���")]
            [Description("��� ����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BlockingTypeConverter))]
            public string BlockingExist
            {
                get
                {
                    string ret = "";
                    ret = Common.GetBit(_values[0], 9) ? Strings.Blocking[1] : Strings.Blocking[0];
                    return ret;
                }
                set
                {
                    bool b = true;
                    if (Strings.Blocking[0] == value)
                    {
                        b = false;
                    }
                    _values[0] = Common.SetBit(_values[0], 9, b);
                }
            }
            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = _values[1];
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort)(Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set
                {
                    int f = Strings.Logic.IndexOf(value);
                    _values[1] = (ushort)f;
                }
            }
            [XmlAttribute("����_��_U")]
            [DisplayName("���� �� U")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool PuskU
            {
                get
                {
                    return Common.GetBit(_values[0], 14);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 14, value);
                }
            }
            [XmlAttribute("���������")]
            [DisplayName("���������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Speedup
            {
                get
                {
                    return Common.GetBit(_values[0], 15);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 15, value);
                }
            }
            [XmlAttribute("�����������")]
            [DisplayName("�����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BusDirectionTypeConverter))]
            public string Direction
            {
                get
                {
                    ushort temp = Common.GetBits(_values[0], 10, 11);
                    byte value = (byte)(Common.HIBYTE(temp) >> 2);
                    value = value >= Strings.BusDirection.Count ? (byte)(Strings.BusDirection.Count - 1) : value;
                    return Strings.BusDirection[value];
                }
                set
                {
                    ushort index = (ushort)Strings.BusDirection.IndexOf(value);
                    _values[0] = Common.SetBits(_values[0], index, 10, 11);
                }
            }
            [XmlAttribute("��������")]
            [Browsable(false)]
            public string Parameter
            {
                get
                {
                    string ret = "";
                    if (IsIncrease)
                    {
                        ret = Common.GetBit(_values[0], 8) ? Strings.TokParameter[1] : Strings.TokParameter[0];
                    }
                    else
                    {
                        ret = Common.GetBit(_values[0], 8) ? Strings.EngineParameter[1] : Strings.EngineParameter[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (IsIncrease)
                    {
                        if (Strings.TokParameter[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.EngineParameter[0] == value)
                        {
                            b = false;
                        }
                    }

                    _values[0] = Common.SetBit(_values[0], 8, b);
                }
            }
            [XmlAttribute("���������")]
            [Browsable(false)]
            public string Engine
            {
                get
                {
                    string ret = "";
                    ret = Common.GetBit(_values[0], 15) ? Strings.EngineMode[1] : Strings.EngineMode[0];
                    return ret;
                }
                set
                {
                    bool b = true;
                    if (Strings.EngineMode[0] == value)
                    {
                        b = false;
                    }
                    _values[0] = Common.SetBit(_values[0], 15, b);
                }
            }
            [XmlAttribute("��������������")]
            [DisplayName("��������������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(FeatureTypeConverter))]
            public string Feature
            {
                get
                {
                    string ret;
                    if (IsIncrease)
                    {
                        ret = Common.GetBit(_values[0], 12) ? Strings.FeatureLight[1] : Strings.Feature[0];
                    }
                    else
                    {
                        ret = Common.GetBit(_values[0], 12) ? Strings.FeatureLight[0] : Strings.Feature[0];
                    }

                    return ret;
                }
                set
                {
                    bool b = true;
                    if (IsIncrease)
                    {
                        if (Strings.FeatureLight[0] == value)
                        {
                            b = false;
                        }
                    }
                    else
                    {
                        if (Strings.Feature[0] == value)
                        {
                            b = false;
                        }
                    }

                    _values[0] = Common.SetBit(_values[0], 12, b);
                }
            }
            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4001;
                    if (IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    ret = Measuring.GetConstraint(_values[2], koeff, "1.11");
                    return ret;
                }
                set
                {
                    ConstraintKoefficient koeff = ConstraintKoefficient.K_4001;
                    if (IsKoeff500)
                    {
                        koeff = ConstraintKoefficient.K_500;
                    }
                    if (IsI12)
                    {
                        koeff = ConstraintKoefficient.K_10000;
                    }
                    _values[2] = Measuring.SetConstraint(value, koeff);
                }
            }
            [XmlAttribute("������������_�����")]
            [DisplayName("������������ �����")]
            [Description("�������� ������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong WorkTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }
            [XmlAttribute("����_��_U_�������")]
            [DisplayName("���� �� U �������")]
            [Description("������� ����� �� U")]
            [System.ComponentModel.Category("��������� ���")]
            public double PuskU_Constraint
            {
                get
                {
                    return Measuring.GetConstraint(_values[4], ConstraintKoefficient.K_25600, "1.11");
                }
                set
                {
                    _values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("���������_�����")]
            [DisplayName("��������� �����")]
            [Description("�������� ������� ���������")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong SpeedupTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }

        };

        #endregion

        #region ������ �� ����������

        private slot _voltageDefensesMain = new slot(0x1180, 0x11C0);
        private slot _voltageDefensesReserve = new slot(0x11C0, 0x1200);

        public void LoadVoltageDefenses()
        {
            LoadSlot(DeviceNumber, _voltageDefensesReserve, "LoadVoltageDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _voltageDefensesMain, "LoadVoltageDefensesMain" + DeviceNumber, this);
        }

        public void SaveVoltageDefenses()
        {
            Array.ConstrainedCopy(VoltageDefensesMain.ToUshort(), 0, _voltageDefensesMain.Value, 0, CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(VoltageDefensesReserve.ToUshort(), 0, _voltageDefensesReserve.Value, 0, CVoltageDefenses.LENGTH);
            SaveSlot(DeviceNumber, _voltageDefensesReserve, "SaveVoltageDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _voltageDefensesMain, "SaveVoltageDefensesMain" + DeviceNumber, this);
        }

        public event Handler VoltageDefensesLoadOk;
        public event Handler VoltageDefensesLoadFail;
        public event Handler VoltageDefensesSaveOk;
        public event Handler VoltageDefensesSaveFail;

        private CVoltageDefenses _cvoltageDefensesMain = new CVoltageDefenses();
        private CVoltageDefenses _cvoltageDefensesReserve = new CVoltageDefenses();

        [XmlElement("������_����������_��������")]
        [DisplayName("�������� ������")]
        [Description("������ �� ���������� - �������� ������ �������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������ �� ����������")]
        public CVoltageDefenses VoltageDefensesMain
        {
            get
            {
                return _cvoltageDefensesMain;
            }
        }
        [XmlElement("������_����������_���������")]
        [DisplayName("��������� ������")]
        [Description("������ �� ���������� - ��������� ������ �������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������ �� ����������")]
        public CVoltageDefenses VoltageDefensesReserve
        {
            get
            {
                return _cvoltageDefensesReserve;
            }
        }

        public enum VoltageDefenseType { U, U2, U0 };

        public class CVoltageDefenses : ICollection
        {
            public const int LENGTH = 64;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(VoltageDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            private List<VoltageDefenseItem> _defenseList = new List<VoltageDefenseItem>(COUNT);
            public CVoltageDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ �� ����������", (object)LENGTH, "����� ��� ����� �� ���������� ������ ���� " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += VoltageDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[VoltageDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, VoltageDefenseItem.LENGTH);
                    _defenseList.Add(new VoltageDefenseItem(temp));
                }
                _defenseList[4].DefenseType = _defenseList[5].DefenseType = VoltageDefenseType.U2;
                _defenseList[6].DefenseType = _defenseList[7].DefenseType = VoltageDefenseType.U0;

                _defenseList[0].Name = "U>";
                _defenseList[1].Name = "U>>";
                _defenseList[2].Name = "U<";
                _defenseList[3].Name = "U<<";
                _defenseList[4].Name = "U2>";
                _defenseList[5].Name = "U2>>";
                _defenseList[6].Name = "U0>";
                _defenseList[7].Name = "U0>>";
            }

            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * VoltageDefenseItem.LENGTH, VoltageDefenseItem.LENGTH);
                }
                return buffer;
            }
            public VoltageDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }
        };

        public class VoltageDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];
            public override string ToString()
            {
                return "������ �� ����������";
            }

            public VoltageDefenseItem() { }
            public VoltageDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }
            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� ����������", (object)LENGTH, "����� ����� ������ ���� ������" + LENGTH);
                }
                _values = buffer;
            }
            private VoltageDefenseType _type = VoltageDefenseType.U;
            [XmlAttribute("���")]
            [Browsable(false)]
            public VoltageDefenseType DefenseType
            {
                get
                {
                    return _type;
                }
                set
                {
                    _type = value;
                }
            }
            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Reset
            {
                get
                {
                    return Common.GetBit(_values[0], 13);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 13, value);
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("����")]
            [DisplayName("����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("���_�������")]
            [DisplayName("��� ��������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV_Return
            {
                get
                {
                    return Common.GetBit(_values[0], 15);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 15, value);
                }
            }
            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Return
            {
                get
                {
                    return Common.GetBit(_values[0], 14);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 14, value);
                }
            }
            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [System.ComponentModel.Category("��������� ���")]
            public string Parameter
            {
                get


                {
                    string ret = "";
                    int index = (Common.GetBits(_values[0], 8, 9, 10, 11) >> 8);
                    switch (_type)
                    {
                        case VoltageDefenseType.U:
                            if (index >= Strings.VoltageParameterU.Count)
                            {
                                index = Strings.VoltageParameterU.Count - 1;
                            }
                            ret = Strings.VoltageParameterU[index];
                            break;
                        case VoltageDefenseType.U0:
                            if (index >= Strings.VoltageParameterU0.Count)
                            {
                                index = Strings.VoltageParameterU0.Count - 1;
                            }
                            ret = Strings.VoltageParameterU0[index];
                            break;
                        case VoltageDefenseType.U2:
                            if (index >= Strings.VoltageParameterU2.Count)
                            {
                                index = Strings.VoltageParameterU2.Count - 1;
                            }
                            ret = Strings.VoltageParameterU2[index];
                            break;
                        default:
                            break;
                    }
                    return ret;
                }
                set
                {
                    switch (_type)
                    {
                        case VoltageDefenseType.U:
                            _values[0] = Common.SetBits(_values[0], (ushort)Strings.VoltageParameterU.IndexOf(value), 8, 9, 10, 11);
                            break;
                        case VoltageDefenseType.U2:
                            _values[0] = Common.SetBits(_values[0], (ushort)Strings.VoltageParameterU2.IndexOf(value), 8, 9, 10, 11);
                            break;
                        case VoltageDefenseType.U0:
                            _values[0] = Common.SetBits(_values[0], (ushort)Strings.VoltageParameterU0.IndexOf(value), 8, 9, 10, 11);
                            break;
                        default:
                            break;
                    }

                }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort)(_values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort)(Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set
                {
                    _values[1] = (ushort)Strings.Logic.IndexOf(value);
                }
            }
            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[2], ConstraintKoefficient.K_25600, "1.11");
                    return ret;
                }
                set
                {
                    _values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("������������_�����")]
            [DisplayName("������������ �����")]
            [Description("�������� ������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong WorkTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }
            [XmlAttribute("�������_�������")]
            [DisplayName("������� �������")]
            [System.ComponentModel.Category("��������� ���")]
            public double ReturnConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[4], ConstraintKoefficient.K_25600, "1.11");
                    return ret;
                }
                set
                {
                    _values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("�������_�����")]
            [DisplayName("������� �����")]
            [Description("�������� ������� ��������")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong ReturnTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }
            private string _name;
            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }
        }
        #endregion

        #region �������� ����� �� �������

        private slot _frequenceDefensesMain = new slot(0x1140, 0x1160);
        private slot _frequenceDefensesReserve = new slot(0x1160, 0x1180);

        public void LoadFrequenceDefenses()
        {
            LoadSlot(DeviceNumber, _frequenceDefensesReserve, "LoadFrequenceDefensesReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _frequenceDefensesMain, "LoadFrequenceDefensesMain" + DeviceNumber, this);
        }

        public void SaveFrequenceDefenses()
        {
            Array.ConstrainedCopy(FrequenceDefensesMain.ToUshort(), 0, _frequenceDefensesMain.Value, 0, CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesReserve.ToUshort(), 0, _frequenceDefensesReserve.Value, 0, CFrequenceDefenses.LENGTH);
            SaveSlot(DeviceNumber, _frequenceDefensesReserve, "SaveFrequenceDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _frequenceDefensesMain, "SaveFrequenceDefensesMain" + DeviceNumber, this);
        }

        public event Handler FrequenceDefensesLoadOk;
        public event Handler FrequenceDefensesLoadFail;
        public event Handler FrequenceDefensesSaveOk;
        public event Handler FrequenceDefensesSaveFail;

        private CFrequenceDefenses _cFrequenceDefensesMain = new CFrequenceDefenses();
        private CFrequenceDefenses _cFrequenceDefensesReserve = new CFrequenceDefenses();

        [XmlElement("������_��_�������_��������")]
        [DisplayName("�������� ������")]
        [Description("������ �� ������� - �������� ������ �������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������ �� �������")]
        public CFrequenceDefenses FrequenceDefensesMain
        {
            get
            {
                return _cFrequenceDefensesMain;
            }
        }
        [XmlElement("������_��_�������_���������")]
        [DisplayName("��������� ������")]
        [Description("������ �� ������� - ��������� ������ �������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������ �� �������")]
        public CFrequenceDefenses FrequenceDefensesReserve
        {
            get
            {
                return _cFrequenceDefensesReserve;
            }
        }

        public class CFrequenceDefenses : ICollection
        {
            public const int LENGTH = 0x20;
            public const int COUNT = 4;


            #region ICollection Members

            public void Add(FrequenceDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            [XmlIgnore]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            [XmlIgnore]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            private List<FrequenceDefenseItem> _defenseList = new List<FrequenceDefenseItem>(COUNT);

            public CFrequenceDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ �� �������", (object)LENGTH, "����� ��� ����� �� ������� ������ ���� " + LENGTH);
                }
                for (int i = 0; i < LENGTH; i += FrequenceDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[FrequenceDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, FrequenceDefenseItem.LENGTH);
                    _defenseList.Add(new FrequenceDefenseItem(temp));
                }
                _defenseList[0].Name = "F>";
                _defenseList[1].Name = "F>>";
                _defenseList[2].Name = "F<";
                _defenseList[3].Name = "F<<";


            }
            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * FrequenceDefenseItem.LENGTH, FrequenceDefenseItem.LENGTH);
                }
                return buffer;
            }
            public FrequenceDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }

        };

        public class FrequenceDefenseItem
        {
            public const int LENGTH = 8;
            private ushort[] _values = new ushort[LENGTH];

            private string _name;
            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }

            public FrequenceDefenseItem() { }
            public FrequenceDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            public override string ToString()
            {
                return "������ �� �������";
            }
            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� �������", (object)LENGTH, "����� ������� ������ ���� ������" + LENGTH);
                }
                _values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [System.ComponentModel.Category("��������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Reset
            {
                get
                {
                    return Common.GetBit(_values[0], 13);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 13, value);
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("����")]
            [DisplayName("����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("�������_���")]
            [DisplayName("��� ��������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV_Return
            {
                get
                {
                    return Common.GetBit(_values[0], 15);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 15, value);
                }
            }
            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool Return
            {
                get
                {
                    return Common.GetBit(_values[0], 14);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 14, value);
                }
            }
            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort)(_values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort)(Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set
                {
                    _values[1] = (ushort)Strings.Logic.IndexOf(value);
                }
            }
            [XmlAttribute("������������_�������")]
            [DisplayName("������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[2], ConstraintKoefficient.K_25600, "1.11");
                    return ret;
                }
                set
                {
                    _values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong WorkTime
            {
                get
                {
                    return Measuring.GetTime(_values[3]);
                }
                set
                {
                    _values[3] = Measuring.SetTime(value);
                }
            }

            [XmlAttribute("������������_���")]
            [DisplayName("������� ���")]
            [Description("������� ������������ ���")]
            [System.ComponentModel.Category("��������� ���")]
            public double ConstraintAPV
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[4], ConstraintKoefficient.K_25600, "1.11");
                    return ret;
                }
                set
                {
                    _values[4] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
            [XmlAttribute("�������_�����")]
            [DisplayName("����� ��������")]
            [Description("�������� ������� ��������")]
            [System.ComponentModel.Category("��������� ���")]
            public ulong ReturnTime
            {
                get
                {
                    return Measuring.GetTime(_values[5]);
                }
                set
                {
                    _values[5] = Measuring.SetTime(value);
                }
            }
        }

        #endregion

        #region ������ ���������

        private slot _engineDefenses = new slot(0x1120, 0x1133);

        public void LoadEngineDefenses()
        {
            LoadSlot(DeviceNumber, _engineDefenses, "LoadEngineDefenses" + DeviceNumber, this);
        }

        public void SaveEngineDefenses()
        {
            Array.ConstrainedCopy(EngineDefenses.ToUshort(), 0, _engineDefenses.Value, 0, CEngingeDefenses.LENGTH);
            SaveSlot(DeviceNumber, _engineDefenses, "SaveEngineDefenses" + DeviceNumber, this);
        }

        public event Handler EngineDefensesLoadOk;
        public event Handler EngineDefensesLoadFail;
        public event Handler EngineDefensesSaveOk;
        public event Handler EngineDefensesSaveFail;


        private CEngingeDefenses _cengineDefenses = new CEngingeDefenses();

        [XmlElement("������_���������")]
        [DisplayName("������ ���������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [System.ComponentModel.Category("������ ���������")]
        public CEngingeDefenses EngineDefenses
        {
            get
            {
                return _cengineDefenses;
            }
        }

        [XmlElement("�����_�������_���������")]
        [DisplayName("����� �������")]
        [Description("����� ������� ���������")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort EngineHeatingTime
        {
            get { return _engineDefenses.Value[0]; }
            set { _engineDefenses.Value[0] = value; }
        }
        [XmlElement("�����_����������_���������")]
        [DisplayName("����� ����������")]
        [Description("����� ���������� ���������")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort EngineCoolingTime
        {
            get { return _engineDefenses.Value[1]; }
            set { _engineDefenses.Value[1] = value; }
        }
        [XmlElement("���_���������")]
        [DisplayName("��� ���������")]
        [Description("��� ���������,In")]
        [System.ComponentModel.Category("������ ���������")]
        public double EngineIn
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[2], ConstraintKoefficient.K_4000, "1.11"); }
            set { _engineDefenses.Value[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }
        [XmlElement("�������_������������_���������")]
        [DisplayName("������� ������������")]
        [Description("������� ������������,I�")]
        [System.ComponentModel.Category("������ ���������")]
        public double EngineWorkConstraint
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[3], ConstraintKoefficient.K_4000, "1.11"); }
            set { _engineDefenses.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }
        [XmlElement("�����_�����_���������")]
        [DisplayName("����� �����")]
        [System.ComponentModel.Category("������ ���������")]
        public ulong EnginePuskTime
        {
            get { return Measuring.GetTime(_engineDefenses.Value[4]); }
            set { _engineDefenses.Value[4] = Measuring.SetTime(value); }
        }
        [XmlElement("���_���������_���������")]
        [DisplayName("��������� ���������")]
        [Description("��������� ���������,%")]
        [System.ComponentModel.Category("������ ���������")]
        public double EngineHeatPuskConstraint
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[5], ConstraintKoefficient.K_25600, "1.11"); }
            set { _engineDefenses.Value[5] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }
        [XmlElement("����_Q_�����")]
        [DisplayName("���� Q �����")]
        [TypeConverter(typeof(LogicTypeConverter))]
        [System.ComponentModel.Category("������ ���������")]
        public string EngineResetQ
        {
            get
            {
                ushort index = (ushort)(_engineDefenses.Value[6] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _engineDefenses.Value[6] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("����_N_����")]
        [DisplayName("���� N ����")]
        [TypeConverter(typeof(LogicTypeConverter))]
        [System.ComponentModel.Category("������ ���������")]
        public string EnginePuskN
        {
            get
            {
                ushort index = (ushort)(_engineDefenses.Value[7] & 0xFF);
                if (index >= Strings.Logic.Count)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set
            {
                _engineDefenses.Value[7] = (ushort)Strings.Logic.IndexOf(value);
            }
        }
        [XmlElement("�����_Q")]
        [DisplayName("����� Q")]
        [TypeConverter(typeof(ModeLightTypeConverter))]
        [System.ComponentModel.Category("������ ���������")]
        public string QMode
        {
            get
            {
                string ret = "";
                bool bit = Common.GetBit(_engineDefenses.Value[12], 0);
                ret = bit ? "�������" : "��������";
                return ret;
            }
            set
            {
                int index = Strings.ModesLight.IndexOf(value);
                _engineDefenses.Value[12] = (0 == index) ? Common.SetBit(_engineDefenses.Value[12], 0, false) : Common.SetBit(_engineDefenses.Value[12], 0, true);
            }
        }
        [XmlElement("�������_Q")]
        [DisplayName("������� Q")]
        [System.ComponentModel.Category("������ ���������")]
        public double QConstraint
        {
            get { return Measuring.GetConstraint(_engineDefenses.Value[13], ConstraintKoefficient.K_25600, "1.11"); }
            set { _engineDefenses.Value[13] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600); }
        }
        [XmlElement("�����_Q")]
        [DisplayName("����� Q")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort QTime
        {
            get { return _engineDefenses.Value[14]; }
            set { _engineDefenses.Value[14] = value; }
        }
        [XmlElement("N_����")]
        [DisplayName("����� ������")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort BlockingPuskCount
        {
            get { return _engineDefenses.Value[16]; }
            set { _engineDefenses.Value[16] = value; }
        }
        [XmlElement("N_���")]
        [DisplayName("����� ���.������")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort BlockingHeatCount
        {
            get { return _engineDefenses.Value[15]; }
            set { _engineDefenses.Value[15] = value; }
        }
        [XmlElement("T_����")]
        [DisplayName("������������ ����������")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort BlockingDuration
        {
            get { return _engineDefenses.Value[17]; }
            set { _engineDefenses.Value[17] = value; }
        }
        [XmlElement("T_����")]
        [DisplayName("����� ����������")]
        [System.ComponentModel.Category("������ ���������")]
        public ushort BlockingTime
        {
            get { return _engineDefenses.Value[18]; }
            set { _engineDefenses.Value[18] = value; }
        }

        public class CEngingeDefenses : ICollection
        {
            public const int LENGTH = 19;
            public const int COUNT = 2;

            private ushort[] _buffer = new ushort[LENGTH];
            private List<EngineDefenseItem> _defenseList = new List<EngineDefenseItem>(COUNT);

            public CEngingeDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }
            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("������ ���������", (object)LENGTH, "����� ��� ����� ��������� ������ ���� " + LENGTH);
                }
                _buffer = buffer;
                for (int i = 8; i < 12; i += EngineDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[EngineDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, EngineDefenseItem.LENGTH);
                    _defenseList.Add(new EngineDefenseItem(temp));
                }
                _defenseList[0].Name = "Q>";
                _defenseList[1].Name = "Q>>";
            }
            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return _defenseList.Count;
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];
                Array.ConstrainedCopy(_buffer, 0, buffer, 0, LENGTH);

                for (int i = 0; i < CEngingeDefenses.COUNT; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, 8 + EngineDefenseItem.LENGTH * i, EngineDefenseItem.LENGTH);
                }
                return buffer;
            }

            public EngineDefenseItem this[int i]
            {
                get
                {
                    return _defenseList[i];
                }
                set
                {
                    _defenseList[i] = value;
                }
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {

            }

            public void Add(EngineDefenseItem item)
            {
                _defenseList.Add(item);
            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion
        };

        public class EngineDefenseItem
        {
            public const int LENGTH = 2;
            private ushort[] _values = new ushort[LENGTH];

            public override string ToString()
            {
                return "������ ���������";
            }
            public EngineDefenseItem() { }
            public EngineDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            private string _name;
            [Browsable(false)]
            [XmlIgnore]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }


            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [System.ComponentModel.Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public ushort[] Values
            {
                get
                {
                    return _values;
                }
                set
                {
                    SetItem(value);
                }
            }
            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("����� ������ �� ����������", (object)LENGTH, "����� ����� ������ ���� ������" + LENGTH);
                }
                _values = buffer;
            }
            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    ushort index = Common.GetBits(_values[0], 0, 1, 2);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    _values[0] = Common.SetBits(_values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2);
                }
            }
            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool AVR
            {
                get
                {
                    return Common.GetBit(_values[0], 5);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 5, value);
                }
            }
            [XmlAttribute("���")]
            [DisplayName("���")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit(_values[0], 6);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 6, value);
                }
            }
            [XmlAttribute("����")]
            [DisplayName("����")]
            [System.ComponentModel.Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit(_values[0], 7);
                }
                set
                {
                    _values[0] = Common.SetBit(_values[0], 7, value);
                }
            }
            [XmlAttribute("�������")]
            [DisplayName("������� ������������")]
            [System.ComponentModel.Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    double ret = 0.0;
                    ret = Measuring.GetConstraint(_values[1], ConstraintKoefficient.K_25600, "1.11");
                    return ret;
                }
                set
                {
                    _values[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
                }
            }
        }

        #endregion

        #region �������������
        private Oscilloscope _oscilloscope;

        public Oscilloscope Oscilloscope
        {
            get { return _oscilloscope; }
        }

        //#region ��������
        //[XmlElement("CountOsc")]
        //[DisplayName("CountOsc")]
        //[Description("����� ������������")]
        //[System.ComponentModel.Category("����� ������������")]
        //#endregion
        //public ushort CountOsc
        //{
        //    get
        //    {
        //        return _countOsc.Value[0];
        //    }
        //}

        //#region ��������
        //[XmlElement("NumOsc")]
        //[DisplayName("NumOsc")]
        //[Description("����� �������� �������������")]
        //[System.ComponentModel.Category("����� �������� �������������")]
        //#endregion
        //public ushort NumOsc
        //{
        //    get
        //    {
        //        return _numOsc.Value[0];
        //    }
        //    set
        //    {
        //        _numOsc.Value[0] = value;
        //    }
        //}

        //public void LoadCount()
        //{
        //    LoadSlot(DeviceNumber, _countOsc, "LoadCount" + DeviceNumber);
        //}

        //public void SaveNum()
        //{
        //    NumOsc = 1;
        //    SaveSlot6(DeviceNumber, _numOsc, "SaveNum" + DeviceNumber);
        //}

        #endregion

        #region ���� - �����
        private slot _datetime = new slot(0x200, 0x207);
        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, _datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10, this);
        }
        [Browsable(false)]
        public byte[] DateTime
        {
            get
            {
                return Common.TOBYTES(_datetime.Value, true);
            }
            set
            {
                _datetime.Value = Common.TOWORDS(value, true);
            }
        }
        #endregion

        #region ������� ��������/����������

        public void LoadDiagnostic()
        {
            LoadBitSlot(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, this);
        }

        public void LoadDiagnosticCycle()
        {
            LoadSlotCycle(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(1000), 10, this);
        }
        public void LoadAnalogSignals()
        {
            LoadSlot(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, this);
        }

        public void LoadInputSignals()
        {
            LoadSlot(DeviceNumber, _inputSignals, "LoadInputSignals" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _konfCount, "�" + DeviceNumber + " ��������� ������������", this);
        }

        public void LoadOutputSignals()
        {
            LoadSlot(DeviceNumber, _outputSignals, "LoadOutputSignals" + DeviceNumber, this);
        }

        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, _externalDefenses, "LoadExternalDefenses" + DeviceNumber, this);
        }

        public void SaveExternalDefenses()
        {
            if (CExternalDefenses.COUNT == ExternalDefenses.Count)
            {
                Array.ConstrainedCopy(ExternalDefenses.ToUshort(), 0, _externalDefenses.Value, 0, CExternalDefenses.LENGTH);
            }
            SaveSlot(DeviceNumber, _externalDefenses, "SaveExternalDefenses" + DeviceNumber, this);
        }


        public void SaveOutputSignals()
        {
            if (COutputRele.COUNT == OutputRele.Count)
            {
                Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, 0x40, COutputRele.LENGTH);
            }
            if (COutputIndicator.COUNT == OutputIndicator.Count)
            {
                Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, 0x60, COutputIndicator.LENGTH);
            }
            SaveSlot(DeviceNumber, _outputSignals, "SaveOutputSignals" + DeviceNumber, this);
        }

        public void SaveInputSignals()
        {
            SaveSlot(DeviceNumber, _inputSignals, "SaveInputSignals" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _konfCount, "SaveKonfCount" + DeviceNumber, this);
        }

        public void CrushOsc()
        {
            SaveSlot6(DeviceNumber, _oscCrush, "����� ������������� - ������������ ���", this);
        }

        public void RemoveDiagnostic()
        {
            MB.RemoveQuery("LoadDiagnostic" + DeviceNumber);
        }

        public void RemoveAnalogSignals()
        {
            MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void RemoveDateTime()
        {
            MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        public void SuspendSystemJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopAlarmJournal = true;

        }

        public void RemoveSystemJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopSystemJournal = true;
        }

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, _datetime, "SaveDateTime" + DeviceNumber, this);
        }

        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, _systemJournal[0], "LoadSJRecord0_" + DeviceNumber, this);
            _stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            if (!_inputSignals.Loaded)
            {
                LoadInputSignals();
            }
            _stopAlarmJournal = false;
            LoadSlot(DeviceNumber, _alarmJournal[0], "LoadALRecord0_" + DeviceNumber, this);
        }
        #endregion

        #region ������������
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ��700 ���������", binFileName);
            }

            try
            {
                DeserializeSlot(doc, "/��700_�������/������", _infoSlot);
                DeserializeSlot(doc, "/��700_�������/����_�����", _datetime);
                DeserializeSlot(doc, "/��700_�������/�������_�������", _inputSignals);
                DeserializeSlot(doc, "/��700_�������/��������_�������", _outputSignals);
                DeserializeSlot(doc, "/��700_�������/�����������", _diagnostic);
                DeserializeSlot(doc, "/��700_�������/������_���������", _engineDefenses);
                DeserializeSlot(doc, "/��700_�������/������_�������", _externalDefenses);
                DeserializeSlot(doc, "/��700_�������/������_�������_��������", _tokDefensesMain);
                DeserializeSlot(doc, "/��700_�������/������_�������_���������", _tokDefensesReserve);
                DeserializeSlot(doc, "/��700_�������/������_�������2_��������", _tokDefenses2Main);
                DeserializeSlot(doc, "/��700_�������/������_�������2_���������", _tokDefenses2Reserve);
                DeserializeSlot(doc, "/��700_�������/������_����������_��������", _voltageDefensesMain);
                DeserializeSlot(doc, "/��700_�������/������_����������_���������", _voltageDefensesReserve);
                DeserializeSlot(doc, "/��700_�������/����������", _automaticsPage);
                DeserializeBit(doc, "/��700_�������/����������_����������_�������", out _bits);
                DeserializeSlot(doc, "/��700_�������/������_�������_��������", _frequenceDefensesMain);
                DeserializeSlot(doc, "/��700_�������/������_�������_���������", _frequenceDefensesReserve);
                DeserializeSlot(doc, "/��700_�������/������������_������������", _konfCount);

            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��700 ���������", binFileName);
            }


            ExternalDefenses.SetBuffer(_externalDefenses.Value);
            ushort[] buffer = new ushort[_tokDefensesReserve.Size + _tokDefenses2Reserve.Size];
            Array.ConstrainedCopy(_tokDefensesReserve.Value, 0, buffer, 0, _tokDefensesReserve.Size);
            Array.ConstrainedCopy(_tokDefenses2Reserve.Value, 0, buffer, _tokDefensesReserve.Size, _tokDefenses2Reserve.Size / 2);
            TokDefensesReserve.SetBuffer(buffer);
            buffer = new ushort[_tokDefensesMain.Size + _tokDefenses2Main.Size];
            Array.ConstrainedCopy(_tokDefensesMain.Value, 0, buffer, 0, _tokDefensesMain.Size);
            Array.ConstrainedCopy(_tokDefenses2Main.Value, 0, buffer, _tokDefensesMain.Size, _tokDefenses2Main.Size / 2);
            TokDefensesMain.SetBuffer(buffer);
            VoltageDefensesMain.SetBuffer(_voltageDefensesMain.Value);
            VoltageDefensesReserve.SetBuffer(_voltageDefensesReserve.Value);
            EngineDefenses.SetBuffer(_engineDefenses.Value);
            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
            OutputRele.SetBuffer(releBuffer);
            ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
            OutputIndicator.SetBuffer(outputIndicator);
            FrequenceDefensesMain.SetBuffer(_frequenceDefensesMain.Value);
            FrequenceDefensesReserve.SetBuffer(_frequenceDefensesReserve.Value);
        }

        void DeserializeBit(XmlDocument doc, string nodePath, out BitArray Bits)
        {
            // !!!!�������� ��� ����� ������� ��������!!!!
            try
            {
                Bits = Common.StringToBits(doc.SelectSingleNode(nodePath).InnerText);
            }
            catch (Exception)
            {
                Bits = new BitArray(8);
            }
        }

        void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void Serialize(string binFileName)
        {
           /* string xmlFileName = System.IO.Path.ChangeExtension(binFileName, ".xml");

            Type d = this.GetType();
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(this.GetType());
            System.IO.TextWriter writer = new System.IO.StreamWriter(xmlFileName, false, Encoding.UTF8);
            ser.Serialize(writer, this);
            writer.Close();*/

            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("��700_�������"));

            Array.ConstrainedCopy(EngineDefenses.ToUshort(), 0, _engineDefenses.Value, 0, CEngingeDefenses.LENGTH);
            Array.ConstrainedCopy(ExternalDefenses.ToUshort(), 0, _externalDefenses.Value, 0, CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesMain.ToUshort(), 0, _frequenceDefensesMain.Value, 0, CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(FrequenceDefensesReserve.ToUshort(), 0, _frequenceDefensesReserve.Value, 0, CFrequenceDefenses.LENGTH);
            Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, 0x40, COutputRele.LENGTH);
            Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, 0x60, COutputIndicator.LENGTH);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort1(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort2(), 0, _tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort1(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort2(), 0, _tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);
            Array.ConstrainedCopy(VoltageDefensesMain.ToUshort(), 0, _voltageDefensesMain.Value, 0, CVoltageDefenses.LENGTH);
            Array.ConstrainedCopy(VoltageDefensesReserve.ToUshort(), 0, _voltageDefensesReserve.Value, 0, CVoltageDefenses.LENGTH);

            SerializeSlot(doc, "������", _infoSlot);
            SerializeSlot(doc, "����_�����", _datetime);
            SerializeSlot(doc, "�������_�������", _inputSignals);
            SerializeSlot(doc, "��������_�������", _outputSignals);
            SerializeSlot(doc, "�����������", _diagnostic);
            SerializeSlot(doc, "������_���������", _engineDefenses);
            SerializeSlot(doc, "������_�������", _externalDefenses);
            SerializeSlot(doc, "������_�������_��������", _tokDefensesMain);
            SerializeSlot(doc, "������_�������_���������", _tokDefensesReserve);
            SerializeSlot(doc, "������_�������2_��������", _tokDefenses2Main);
            SerializeSlot(doc, "������_�������2_���������", _tokDefenses2Reserve);
            SerializeSlot(doc, "������_����������_��������", _voltageDefensesMain);
            SerializeSlot(doc, "������_����������_���������", _voltageDefensesReserve);
            SerializeSlot(doc, "����������", _automaticsPage);
            SerializeBits(doc, "����������_����������_�������", _bits);
            SerializeSlot(doc, "������_�������_��������", _frequenceDefensesMain);
            SerializeSlot(doc, "������_�������_���������", _frequenceDefensesReserve);
            SerializeSlot(doc, "������������_������������", _konfCount);

            doc.Save(binFileName);
        }
        void SerializeBits(XmlDocument doc, string nodeName, BitArray Bits)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Common.BitsToString(Bits);
            doc.DocumentElement.AppendChild(element);
        }

        void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MR700); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr700; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��700"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        public void ConfirmConstraint()
        {
            this.SetBit(DeviceNumber, 0, true, "��700 ������ �������������",this);
        }

        private slot _constraintSelector = new slot(0x400, 0x401);

        public void SelectConstraintGroup(bool mainGroup)
        {
            _constraintSelector.Value[0] = mainGroup ? (ushort)0 : (ushort)1;
            SaveSlot(DeviceNumber, _constraintSelector, "��700 �" + DeviceNumber + " ������������ ������ �������", this);
        }


        private new void mb_CompleteExchange(object sender, Query query)
        {

            double vers = 0;
            try
            {
                vers = Common.VersionConverter(Info.Version);
            }
            catch { }

            if (vers < 2.0)
            {
                if (this._oscilloscope == null) // ��� �������� ������� �� ������ ����� ������������� �������� ������ (��������� ������� � ������������ ��� ����������
                    // � ����������� �������������� � �����.
                {
                    this.MB.CompleteExchange -= this.mb_CompleteExchange;
                    return;
                }

                _oscilloscope.OnMbCompleteExchange(sender, query);

                #region ������ �� �������

                if ("LoadFrequenceDefensesReserve" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _frequenceDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                        FrequenceDefensesReserve.SetBuffer(_frequenceDefensesReserve.Value);
                    }
                }
                if ("LoadFrequenceDefensesMain" + DeviceNumber == query.name)
                {
                    _frequenceDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    FrequenceDefensesMain.SetBuffer(_frequenceDefensesMain.Value);
                    if (0 == query.fail)
                    {
                        if (null != FrequenceDefensesLoadOk)
                        {
                            FrequenceDefensesLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != FrequenceDefensesLoadFail)
                        {
                            FrequenceDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveFrequenceDefensesMain" + DeviceNumber == query.name)
                {
                    Raise(query, FrequenceDefensesSaveOk, FrequenceDefensesSaveFail);
                }

                #endregion

                #region ������ ����������

                if ("LoadVoltageDefensesReserve" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _voltageDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                        VoltageDefensesReserve.SetBuffer(_voltageDefensesReserve.Value);
                    }
                }
                if ("LoadVoltageDefensesMain" + DeviceNumber == query.name)
                {
                    _voltageDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    VoltageDefensesMain.SetBuffer(_voltageDefensesMain.Value);
                    if (0 == query.fail)
                    {
                        if (null != VoltageDefensesLoadOk)
                        {
                            VoltageDefensesLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != VoltageDefensesLoadFail)
                        {
                            VoltageDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveVoltageDefensesMain" + DeviceNumber == query.name)
                {
                    Raise(query, VoltageDefensesSaveOk, VoltageDefensesSaveFail);
                }

                #endregion

                #region ������� ������

                if ("LoadTokDefensesMain2" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _tokDefenses2Main.Value = Common.TOWORDS(query.readBuffer, true);
                    }
                }
                if ("LoadTokDefensesReserve2" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _tokDefenses2Reserve.Value = Common.TOWORDS(query.readBuffer, true);
                    }
                }
                if ("LoadTokDefensesReserve" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        ushort[] buffer = new ushort[_tokDefensesReserve.Size + _tokDefenses2Reserve.Size];
                        _tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                        Array.ConstrainedCopy(_tokDefensesReserve.Value, 0, buffer, 0, _tokDefensesReserve.Size);
                        Array.ConstrainedCopy(_tokDefenses2Reserve.Value, 0, buffer, _tokDefensesReserve.Size,
                                              _tokDefenses2Reserve.Size);
                        _ctokDefensesReserve.SetBuffer(buffer);
                    }
                }
                if ("LoadTokDefensesMain" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        ushort[] buffer = new ushort[_tokDefensesMain.Size + _tokDefenses2Main.Size];
                        _tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                        Array.ConstrainedCopy(_tokDefensesMain.Value, 0, buffer, 0, _tokDefensesMain.Size);
                        Array.ConstrainedCopy(_tokDefenses2Main.Value, 0, buffer, _tokDefensesMain.Size,
                                              _tokDefenses2Main.Size);
                        _ctokDefensesMain.SetBuffer(buffer);
                        if (null != TokDefensesLoadOK)
                        {
                            TokDefensesLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != TokDefensesLoadFail)
                        {
                            TokDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveTokDefensesMain" + DeviceNumber == query.name)
                {
                    Raise(query, TokDefensesSaveOK, TokDefensesSaveFail);
                }

                #endregion

                #region ������ �������

                if (IsIndexQuery(query.name, "LoadSJRecord"))
                {
                    int index = GetIndex(query.name, "LoadSJRecord");
                    if (0 == query.fail)
                    {
                        if (false == _systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                        {
                            if (null != SystemJournalLoadOk)
                            {
                                SystemJournalLoadOk(this);
                            }
                        }
                        else
                        {
                            if (null != SystemJournalRecordLoadOk)
                            {
                                SystemJournalRecordLoadOk(this, index);
                            }
                            index += 1;
                            if (index < SYSTEMJOURNAL_RECORD_CNT && !_stopSystemJournal)
                            {
                                LoadSlot(DeviceNumber, _systemJournal[index],
                                         "LoadSJRecord" + index + "_" + DeviceNumber, this);
                            }
                            else
                            {
                                if (null != SystemJournalLoadOk)
                                {
                                    SystemJournalLoadOk(this);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (null != SystemJournalRecordLoadFail)
                        {
                            SystemJournalRecordLoadFail(this, index);
                        }
                    }
                }

                #endregion

                #region �������� �������

                if ("LoadOutputSignals" + DeviceNumber == query.name)
                {
                    _outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                    ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                    Array.ConstrainedCopy(_outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                    _outputRele.SetBuffer(releBuffer);

                    ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                    Array.ConstrainedCopy(_outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                    _outputIndicator.SetBuffer(outputIndicator);

                    if (0 == query.fail)
                    {
                        if (null != OutputSignalsLoadOK)
                        {
                            OutputSignalsLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != OutputSignalsLoadFail)
                        {
                            OutputSignalsLoadFail(this);
                        }
                    }
                }
                if ("SaveOutputSignals" + DeviceNumber == query.name)
                {
                    Raise(query, OutputSignalsSaveOK, OutputSignalsSaveFail);
                }

                #endregion

                #region ������� ������

                if ("LoadExternalDefenses" + DeviceNumber == query.name)
                {
                    _externalDefenses.Value = Common.TOWORDS(query.readBuffer, true);
                    ExternalDefenses.SetBuffer(_externalDefenses.Value);
                    if (0 == query.fail)
                    {
                        if (null != ExternalDefensesLoadOK)
                        {
                            ExternalDefensesLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != ExternalDefensesLoadFail)
                        {
                            ExternalDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveExternalDefenses" + DeviceNumber == query.name)
                {
                    Raise(query, ExternalDefensesSaveOK, ExternalDefensesSaveFail);
                }

                #endregion

                #region ������� �������

                if ("LoadInputSignals" + DeviceNumber == query.name)
                {
                    Raise(query, InputSignalsLoadOK, InputSignalsLoadFail, ref _inputSignals);
                }
                if ("SaveInputSignals" + DeviceNumber == query.name)
                {
                    Raise(query, InputSignalsSaveOK, InputSignalsSaveFail);
                }
                if (query.name == "�" + DeviceNumber + " ��������� ������������")
                {
                    Raise(query, KonfCountLoadOK, KonfCountLoadFail, ref _konfCount);
                }
                if ("SaveKonfCount" + DeviceNumber == query.name)
                {
                    Raise(query, KonfCountSaveOK, KonfCountSaveFail);
                }

                #endregion

                #region ��� �������� ����������

                if ("LoadAutomaticsPage" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _automaticsPage.Value = Common.TOWORDS(query.readBuffer, true);
                        AVR_Add_Signals = new BitArray(new byte[] {Common.LOBYTE(_automaticsPage.Value[8])});
                        if (null != AutomaticsPageLoadOK)
                        {
                            AutomaticsPageLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != AutomaticsPageLoadFail)
                        {
                            AutomaticsPageLoadFail(this);
                        }
                    }
                }
                if ("SaveAutomaticsPage" + DeviceNumber == query.name)
                {
                    Raise(query, AutomaticsPageSaveOK, AutomaticsPageSaveFail);
                }

                #endregion

                if ("LoadDiagnostic" + DeviceNumber == query.name)
                {
                    Raise(query, DiagnosticLoadOk, DiagnosticLoadFail, ref _diagnostic);
                }
                if ("LoadDateTime" + DeviceNumber == query.name)
                {
                    Raise(query, DateTimeLoadOk, DateTimeLoadFail, ref _datetime);
                }


                if ("LoadAnalogSignals" + DeviceNumber == query.name)
                {
                    if (!_inputSignals.Loaded)
                    {
                        LoadInputSignals();
                    }
                    else
                    {
                        Raise(query, AnalogSignalsLoadOK, AnalogSignalsLoadFail, ref _analog);
                    }
                }
            }
            else
            {
            }

            base.mb_CompleteExchange(sender, query);
        }

        public Type[] Forms
        {
            get
            {
                var ver = Common.VersionConverter(this.DeviceVersion);
                if (ver < 2.0)
                {
                    return new[]
                        {
                            typeof (Mr700OldAlarmJournalForm),
                            typeof (MeasuringForm),
                            typeof (SystemJournalForm),
                            typeof (ConfigurationForm),
                            typeof (Mr700OldOscilloscopeForm)
                        };
                }
                else
                {
                    return new[]
                        {
                            typeof (BSBGLEF),                       
                            typeof (Mr700NewOscilloscopeForm),
                            typeof (Mr700ConfigurationFormV2),
                            typeof (Mr700SystemJournalForm),
                            typeof (Mr700AlarmJournalForm),
                            typeof (Mr700MeasuringForm)
                        };
                }
            }
        }

        public List<string> Versions
        {
            get
            {

                return new List<string>
                    {
                        "1.00",
                        "1.10",
                        "1.11",
                        "1.12",
                        "1.13",
                        "1.14",
                        "1.15",
                        "1.16",
                        "1.17",
                        "1.18",
                        "2.00",
                        "2.01",
                        "2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "2.07"

                    };

            }
        }
        
        public Mr700DeviceV2 Mr700DeviceV2Prop
        {
            get { return _mr700DeviceV2; }
        }
        private MemoryEntity<DateTimeStruct> _dateTime;
        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return _dateTime; }
        }
    }

}
