﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR801DVG.AlarmJournal;
using BEMN.MR801DVG.AlarmJournal.Structures;
using BEMN.MR801DVG.BSBGL;
using BEMN.MR801DVG.Configuration;
using BEMN.MR801DVG.Configuration.Structures;
using BEMN.MR801DVG.Configuration.Structures.Engine;
using BEMN.MR801DVG.Configuration.Structures.MeasureTransformer;
using BEMN.MR801DVG.Measuring;
using BEMN.MR801DVG.Measuring.Structures;
using BEMN.MR801DVG.Oscilloscope;
using BEMN.MR801DVG.Properties;
using BEMN.MR801DVG.SystemJournal;
using BEMN.MR801DVG.SystemJournal.Structures;

namespace BEMN.MR801DVG
{
    public class MR801dvg : Device, IDeviceView, IDeviceVersion
    {
        public MR801dvg()
        {
            HaveVersion = true;
        }

        public MR801dvg(Modbus mb)
        {
            HaveVersion = true;
            MB = mb;
            if (MB != null)
            {
                MB.CompleteExchange += this.MbCompleteExchange;
            }
            this.InitAddr();
        }

        private void MbCompleteExchange(object device, Query query)
        {
            if (query.name == "ConfirmConfig" + DeviceNumber)
            {
                if (query.error)
                {
                    MessageBox.Show("Не удалось записать бит подтверждения изменения конфигурации. Конфигурация в устройстве не изменена",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void InitAddr()
        {
            this.Configurationtruct = new MemoryEntity<ConfigurationStruct>("Чтение конфигурации", this, 0x1000);
            this.MeasuringTrans = new MemoryEntity<MeasureTransStruct>("Измерительный трансформатор(измерения)", this, 0x103E);
            this.EngineParameters = new MemoryEntity<EngineParametersStruct>("Параметры двигателя", this, 0x1022);
            this.AnalogData = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая база данных", this, 0x0E00);
            this.DiscretData = new MemoryEntity<DiscretDataBaseStruct>("Дискретная база данных", this, 0x0D00);
            this.DateTime = new MemoryEntity<DateTimeStruct>("Дата/время", this, 0x200);

            this.MeasureAJ = new MemoryEntity<MeasureTransStruct>("Измерительный трансформатор(ЖА)", this, 0x103E);
            this.AlarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", this, 0x700);
            this.SetAjPage = new MemoryEntity<OneWordStruct>("Номер страницы ЖА",  this, 0x700);

            this.SetSjPage = new MemoryEntity<OneWordStruct>("Страница ЖС", this, 0x600);
            this.SystemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы", this, 0x600);

            this.SourceProgram = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
            this.ProgramStart = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this.ProgramSignals = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
            this.ProgramPage = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);

            this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);
            this.StateSpl = new MemoryEntity<OneWordStruct>("Состояние ошибок логики", this, 0x0D13);
        }

        public MemoryEntity<ConfigurationStruct> Configurationtruct { get; private set; }
        public MemoryEntity<MeasureTransStruct> MeasuringTrans { get; private set; }
        public MemoryEntity<EngineParametersStruct> EngineParameters { get; private set; }
        public MemoryEntity<AnalogDataBaseStruct> AnalogData { get; private set; }
        public MemoryEntity<DateTimeStruct> DateTime { get; private set; }
        public MemoryEntity<DiscretDataBaseStruct> DiscretData { get; private set; }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal { get; private set; }
        public MemoryEntity<MeasureTransStruct> MeasureAJ { get; private set; }
        public MemoryEntity<OneWordStruct> SetAjPage { get; private set; } 

        public MemoryEntity<ProgramPageStruct> ProgramPage { get; private set; }
        public MemoryEntity<SourceProgramStruct> SourceProgram { get; private set; }
        public MemoryEntity<StartStruct> ProgramStart { get; private set; }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignals { get; private set; }
        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StateSpl { get; private set; }

        public MemoryEntity<OneWordStruct> SetSjPage { get; private set; } 
        public MemoryEntity<SystemJournalStruct> SystemJournal { get; private set; }

        #region IDeviceVersion
        public Type[] Forms
        {
            get
            {
                return new[]
                {
                    typeof (OscilloscopeForm),
                    typeof (ConfigurationForm),
                    typeof (MeasuringForm),
                    typeof (AlarmJournalForm),
                    typeof (SystemJournalForm),
                    typeof (BSBGLEF)
                };
            }
        }

        public List<string> Versions => new List<string>
        {
            "1.00",
            "1.01"
        };

        #endregion

        #region IDeviceView
        public Type ClassType
        {
            get { return typeof(MR801dvg); }
        }
        public bool ForceShow { get { return false; } }
        public Image NodeImage { get { return Framework.Properties.Resources.mr801; } }
        public string NodeName { get { return "МР801двг"; } }
        public INodeView[] ChildNodes { get { return new INodeView[] { }; } }
        public bool Deletable { get { return true; } }
        #endregion
    }
}
