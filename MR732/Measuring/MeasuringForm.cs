﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MR801DVG.Measuring.Structures;

namespace BEMN.MR801DVG.Measuring
{
    public partial class MeasuringForm : Form, IFormView
    {
        private const string MEASURE_TRANS_READ_FAIL = "Невозможно загрузить конфигурацию осциллографа";
        private const string ENGINE_PARAMETERS_READ_FAIL = "Невозможно загрузить параметры двигателя";

        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности";
        private const string RESET_INDICATION = "Сбросить блинкеры";
        private const string MAIN_GROUP_SETPOINTS = "Переключить на основную группу уставок";
        private const string RESERVE_GROUP_SETPOINTS = "Переключить на резервную группу уставок";
        private const string RESET_HOT_STATE = "Сбросить состояние тепловой модели";
        private const string RESET_COUNT_START = "Сбросить число пусков";
        private const string RESET_TT = "Сбросить неисправности цепей ТТ";
        private const string RUN_OSC = "Запустить осциллограф";
        private const string START_LOGIC = "Запуск СПЛ";
        private const string STOP_LOGIC = "Останов СПЛ";

        private MR801dvg _device;
        private readonly AveragerTime<AnalogDataBaseStruct> _averagerTime;

        private string[] _signs;
        private LedControl[] _diskretLeds;
        private LedControl[] _inputSignalsLeds;
        private LedControl[] _outputSignalsLeds;
        private LedControl[] _idiffSignalsLeds;
        private LedControl[] _iSignalsLeds;
        private LedControl[] _u_f_SignalsLeds;
        private LedControl[] _qEXTSignalsLeds;
        private LedControl[] _ssl;
        private LedControl[] _commonSignals;
        private LedControl[] _releIndicatorSignalsLeds;
        private LedControl[] _disrepairsSignalsLeds;
        private LedControl[] _controlSignalsLeds;

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(MR801dvg device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            
            this._device.DateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._device.AnalogData.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._device.AnalogData.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._device.MeasuringTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._device.MeasuringTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadFail);

            this._device.EngineParameters.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,this.EngineParametersReadOk);
            this._device.EngineParameters.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,this.EngineParametersReadFail);

            this._device.Configurationtruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.AnalogData.RemoveStructQueries();
                this._device.EngineParameters.LoadStruct();
            });

            this._device.DiscretData.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnBitBDLoadOK);
            this._device.DiscretData.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnBitBDLoadFail);

            this._averagerTime = new AveragerTime<AnalogDataBaseStruct>(500);
            this._averagerTime.Tick += this.AveragerTimeTick;
            this.InitLeds();
        }

        private void InitLeds()
        {
            this._diskretLeds = new[]
            {
                this._D1, this._D2, this._D3, this._D4, this._D5, this._D6, this._D7, this._D8,
                this._D9, this._D10, this._D11, this._D12, this._D13, this._D14, this._D15, this._D16,
                this._D17, this._D18, this._D19, this._D20, this._D21, this._D22, this._D23, this._D24
            };

            this._inputSignalsLeds = new[]
            {
                this._LS1, this._LS2, this._LS3, this._LS4, this._LS5, this._LS6, this._LS7, this._LS8,
                this._LS9, this._LS10, this._LS11, this._LS12, this._LS13, this._LS14, this._LS15, this._LS16
            };
            this._outputSignalsLeds = new[]
            {
                this._VLS1, this._VLS2, this._VLS3, this._VLS4, this._VLS5, this._VLS6, this._VLS7, this._VLS8,
                this._VLS9, this._VLS10, this._VLS11, this._VLS12, this._VLS13, this._VLS14, this._VLS15, this._VLS16
            };

            this._idiffSignalsLeds = new[]
            {
                this._IdMax2Mgn, this._IdMax2IO, this._IdMax2, this._IdMax1IO, this._IdMax1, this._p1IO, this._p1, this._p2IO, this._p2
            };

            this._iSignalsLeds = new[]
            {
                this._I1IO, this._I1, this._I2IO, this._I2, this._I3IO, this._I3, this._I4IO, this._I4, this._I5IO,
                this._I5, this._I6IO, this._I6, this._I7IO, this._I7, this._I8IO, this._I8, this._I01IO, this._I01,
                this._I02IO, this._I02, this._I03IO, this._I03, this._In1IO, this._In1, this._In2IO, this._In2,
                this._In3IO, this._In3
            };

            this._u_f_SignalsLeds = new[]
            {
                this._Umax1IO, this._Umax1, this._Umax2IO, this._Umax2, this._Umax3IO, this._Umax3, this._Umax4IO, this._Umax4,
                this._Umin1IO, this._Umin1, this._Umin2IO, this._Umin2, this._Umin3IO, this._Umin3, this._Umin4IO, this._Umin4,
                this._Fmax1IO, this._Fmax1, this._Fmax2IO, this._Fmax2, this._Fmax3IO, this._Fmax3, this._Fmax4IO, this._Fmax4,
                this._Fmin1IO, this._Fmin1, this._Fmin2IO, this._Fmin2, this._Fmin3IO, this._Fmin3, this._Fmin4IO, this._Fmin4
            };

            this._qEXTSignalsLeds = new[]
            {
                this._Q1, this._Q2, this._blockQ, this._blockN,this._startEngine,
                this._EXT1, this._EXT2, this._EXT3, this._EXT4, this._EXT5, this._EXT6, this._EXT7, this._EXT8,
                this._EXT9, this._EXT10, this._EXT11, this._EXT12, this._EXT13, this._EXT14, this._EXT15, this._EXT16
            };

            this._ssl = new[]
            {
                this.SFL1, this.SFL2, this.SFL3, this.SFL4, this.SFL5, this.SFL6, this.SFL7, this.SFL8,
                this.SFL9, this.SFL10, this.SFL11, this.SFL12, this.SFL13, this.SFL14, this.SFL15, this.SFL16,
                this.SFL17, this.SFL18, this.SFL19, this.SFL20, this.SFL21, this.SFL22, this.SFL23, this.SFL24,
                this.SFL25, this.SFL26, this.SFL27, this.SFL28, this.SFL29, this.SFL30, this.SFL31, this.SFL32
            };

            this._commonSignals = new[]
            {
                this._disrepair, this._groopMain, this._groupRez, this._faultTnInd1, this._alarm, this._offSwitch, this._onSwitch,
                this._avrOn, this._avrOff, this._avrBlock, this._lzsh, this._urov, this._apv, this._acceleration,
                this._signaling
            };

            this._releIndicatorSignalsLeds = new[]
            {
                this._Rele1, this._Rele2, this._Rele3, this._Rele4, this._Rele5,
                this._Rele6, this._Rele7, this._Rele8, this._Rele9, this._Rele10, this._Rele11, this._Rele12,
                this._Rele13, this._Rele14, this._Rele15, this._Rele16, this._Rele17, this._Rele18, this._Ind1,
                this._Ind2, this._Ind3, this._Ind4, this._Ind5, this._Ind6, this._Ind7, this._Ind8, this._Ind9,
                this._Ind10, this._Ind11, this._Ind12
            };

            this._controlSignalsLeds = new[]
            {
                this.BDC_MESS_SYS, this.BDC_MESS_ALARM, this.BDC_MESS_OSC, this.BDC_MESS_DISPSYS, new LedControl(), this.SWITCH_OFF, this.SWITCH_ON
            };

            this._disrepairsSignalsLeds = new[]
            {
                this._Hard, this._Soft, this._Meas, this._Switch1, this._faultTnInd, this._Contr, this._Mod1, this._Mod2, this._Mod3, this._Mod4, this._Mod5,
                this._Ust, this._Groupust, this._Passwust, this._Jsys, this._Jalm, this._Osc
            };
        }

        private void OnBitBDLoadFail()
        {
            LedManager.TurnOffLeds(this._diskretLeds);
            LedManager.TurnOffLeds(this._inputSignalsLeds);
            LedManager.TurnOffLeds(this._outputSignalsLeds);
            LedManager.TurnOffLeds(this._idiffSignalsLeds);
            LedManager.TurnOffLeds(this._iSignalsLeds);
            LedManager.TurnOffLeds(this._u_f_SignalsLeds);
            LedManager.TurnOffLeds(this._qEXTSignalsLeds);
            LedManager.TurnOffLeds(this._ssl);
            LedManager.TurnOffLeds(this._commonSignals);
            LedManager.TurnOffLeds(this._releIndicatorSignalsLeds);
            LedManager.TurnOffLeds(this._controlSignalsLeds);
            LedManager.TurnOffLeds(this._disrepairsSignalsLeds);
            this._groupMain_dublicate.State = this._groopMain.State;
            this._groupReserve_dublicate.State = this._groupRez.State;
            this._logicState.State = LedState.Off;
        }

        private void OnBitBDLoadOK()
        {
            LedManager.SetLeds(this._diskretLeds, this._device.DiscretData.Value.DiscretInputs);
            LedManager.SetLeds(this._inputSignalsLeds, this._device.DiscretData.Value.InputsLogicSignals);
            LedManager.SetLeds(this._outputSignalsLeds, this._device.DiscretData.Value.OutputLogicSignals);
            LedManager.SetLeds(this._idiffSignalsLeds, this._device.DiscretData.Value.CurrentDiff);
            LedManager.SetLeds(this._iSignalsLeds, this._device.DiscretData.Value.Current);
            LedManager.SetLeds(this._u_f_SignalsLeds, this._device.DiscretData.Value.VoltageFrequency);
            LedManager.SetLeds(this._qEXTSignalsLeds, this._device.DiscretData.Value.QandExternalDefenses);
            LedManager.SetLeds(this._ssl, this._device.DiscretData.Value.FreeLogic);
            LedManager.SetLeds(this._commonSignals, this._device.DiscretData.Value.CommonSignals);
            LedManager.SetLeds(this._releIndicatorSignalsLeds, this._device.DiscretData.Value.RelaysIndicators);
            LedManager.SetLeds(this._controlSignalsLeds, this._device.DiscretData.Value.State);
            LedManager.SetLeds(this._disrepairsSignalsLeds, this._device.DiscretData.Value.Faults);

            this._groupMain_dublicate.State = this._groopMain.State;
            this._groupReserve_dublicate.State = this._groupRez.State;

            bool enableLogic = this._device.DiscretData.Value.State[7] && !this._device.DiscretData.Value.FaultLogic;
            this._logicState.State = enableLogic ? LedState.NoSignaled : LedState.Signaled;
        }

        private void EngineParametersReadFail()
        {
            MessageBox.Show(ENGINE_PARAMETERS_READ_FAIL);
        }

        private void EngineParametersReadOk()
        {
            this._device.MeasuringTrans.LoadStruct();
        }

        private void MeasureTransReadFail()
        {
            MessageBox.Show(MEASURE_TRANS_READ_FAIL);
        }

        private void MeasureTransReadOk()
        {
            this._device.AnalogData.LoadStructCycle();
        }

        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._device.AnalogData.Value);
        }

        private void AnalogBdReadFail()
        {
            string errValue = "0";
            this._Ua.Text = errValue;
            this._Ub.Text = errValue;
            this._Uc.Text = errValue;
            this._Uab.Text = errValue;
            this._Ubc.Text = errValue;
            this._Uca.Text = errValue;
            this._Un.Text = errValue;
            this._U0.Text = errValue;
            this._U2.Text = errValue;
            this._Ida.Text = errValue;
            this._Idb.Text = errValue;
            this._Idc.Text = errValue;

            this._Is1a.Text = errValue;
            this._Is1b.Text = errValue;
            this._Is1c.Text = errValue;
            this._Is1n.Text = errValue;
            this._Is10.Text = errValue;
            this._Is12.Text = errValue;
            this._Is11.Text = errValue;

            this._Is2a.Text = errValue;
            this._Is2b.Text = errValue;
            this._Is2c.Text = errValue;
            this._Is2n.Text = errValue;
            this._Is20.Text = errValue;
            this._Is22.Text = errValue;
            this._Is21.Text = errValue;

            this._percentIdaFromIbas.Text = errValue;
            this._percentIdbFromIbas.Text = errValue;
            this._percentIdcFromIbas.Text = errValue;
            this._percentIbaFromIbas.Text = errValue;
            this._percentIbbFromIbas.Text = errValue;
            this._percentIbcFromIbas.Text = errValue;

            this._pTextBox.Text = errValue;
            this._qTextBox.Text = errValue;
            this._cosfTextBox.Text = errValue;

            this._frequency.Text = errValue;
            this._qpTextBox.Text = errValue;
            this._nStartTextBox.Text = errValue;
            this._nHotTextBox.Text = errValue;
        }

        private void AveragerTimeTick()
        {
            try
            {
                this._signs = this._signs ?? new[] {"!", "!", "!", "!", "!", "!", "!"};
                this.GetAnalogValue();
            }
            catch
            {
                // ignored
            }
        }

        private void GetAnalogValue()
        {
            this._Ua.Text = this._device.AnalogData.Value.GetUa(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Ub.Text = this._device.AnalogData.Value.GetUb(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Uc.Text = this._device.AnalogData.Value.GetUc(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Uab.Text = this._device.AnalogData.Value.GetUab(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Ubc.Text = this._device.AnalogData.Value.GetUbc(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Uca.Text = this._device.AnalogData.Value.GetUca(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Un.Text = this._device.AnalogData.Value.GetUn(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._U0.Text = this._device.AnalogData.Value.GetU0(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._U2.Text = this._device.AnalogData.Value.GetU2(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);

            this._Ida.Text = this._device.AnalogData.Value.GetIda(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Idb.Text = this._device.AnalogData.Value.GetIdb(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Idc.Text = this._device.AnalogData.Value.GetIdc(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Iba.Text = this._device.AnalogData.Value.GetIba(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Ibb.Text = this._device.AnalogData.Value.GetIbb(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Ibc.Text = this._device.AnalogData.Value.GetIbc(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);

            this._Is1a.Text = this._device.AnalogData.Value.GetIs1a(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is1b.Text = this._device.AnalogData.Value.GetIs1b(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is1c.Text = this._device.AnalogData.Value.GetIs1c(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is1n.Text = this._device.AnalogData.Value.GetInS1(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is10.Text = this._device.AnalogData.Value.GetI0S1(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is12.Text =  this._device.AnalogData.Value.GetI2S1(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is11.Text = this._device.AnalogData.Value.GetI1S1(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);

            this._Is2a.Text = this._device.AnalogData.Value.GetIs2a(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is2b.Text = this._device.AnalogData.Value.GetIs2b(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is2c.Text = this._device.AnalogData.Value.GetIs2c(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is2n.Text = this._device.AnalogData.Value.GetInS2(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is20.Text = this._device.AnalogData.Value.GetI0S2(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is22.Text = this._device.AnalogData.Value.GetI2S2(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._Is21.Text = this._device.AnalogData.Value.GetI1S2(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);

            this._percentIdaFromIbas.Text = this._device.AnalogData.Value.GetPercentIda(this._averagerTime.ValueList, this._device.EngineParameters.Value.Idv);
            this._percentIdbFromIbas.Text = this._device.AnalogData.Value.GetPercentIdb(this._averagerTime.ValueList, this._device.EngineParameters.Value.Idv);
            this._percentIdcFromIbas.Text = this._device.AnalogData.Value.GetPercentIdc(this._averagerTime.ValueList, this._device.EngineParameters.Value.Idv);
            this._percentIbaFromIbas.Text = this._device.AnalogData.Value.GetPercentIba(this._averagerTime.ValueList, this._device.EngineParameters.Value.Idv);
            this._percentIbbFromIbas.Text = this._device.AnalogData.Value.GetPercentIbb(this._averagerTime.ValueList, this._device.EngineParameters.Value.Idv);
            this._percentIbcFromIbas.Text = this._device.AnalogData.Value.GetPercentIbc(this._averagerTime.ValueList, this._device.EngineParameters.Value.Idv);

            this._pTextBox.Text = this._device.AnalogData.Value.GetP(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._qTextBox.Text = this._device.AnalogData.Value.GetQ(this._averagerTime.ValueList, this._device.MeasuringTrans.Value);
            this._cosfTextBox.Text = this._device.AnalogData.Value.GetCosF(this._averagerTime.ValueList);

            this._frequency.Text = this._device.AnalogData.Value.GetF(this._averagerTime.ValueList);
            this._qpTextBox.Text = this._device.AnalogData.Value.GetQt(this._averagerTime.ValueList);
            this._nStartTextBox.Text = this._device.AnalogData.Value.GetCountStarts;
            this._nHotTextBox.Text = this._device.AnalogData.Value.GetCountHotStarts;
        }

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._device.DateTime.Value;
        }

        private void dateTimeControl_TimeChanged()
        {
            this._device.DateTime.Value = this._dateTimeControl.DateTime;
            this._device.DateTime.SaveStruct();
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.EngineParameters.LoadStruct();
                this._device.DiscretData.LoadStructCycle();
                this._device.DateTime.LoadStructCycle();
            }
            else
            {
                this._device.DiscretData.RemoveStructQueries();
                this._device.DateTime.RemoveStructQueries();
                this._device.AnalogData.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.OnBitBDLoadFail();
            }
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.DiscretData.RemoveStructQueries();
            this._device.DateTime.RemoveStructQueries();
            this._device.AnalogData.RemoveStructQueries();
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D03, RESET_OSC);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D04, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D05, RESET_INDICATION);
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D09, MAIN_GROUP_SETPOINTS);
        }

        private void _reserveGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0A, RESERVE_GROUP_SETPOINTS);
        }

        private void _resetTermStateButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D11, RESET_HOT_STATE);
        }
        
        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0C, "Включить выключатель");
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0B, "Отключить выключатель");
        }

        private void startLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(@"Запустить свободно программируемую логику в устройстве?", @"Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.DiscretData.SetBitByAdress(0x0D10, START_LOGIC);
        }

        private void stopLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(@"Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства",
                @"Останов СПЛ", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.DiscretData.SetBitByAdress(0x0D0F, STOP_LOGIC);
        }

        private void _resetCountPusk_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D12, RESET_COUNT_START);
        }

        private void _resetFaultTnBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D13, RESET_TT);
        }

        private void _runOscBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D14, RUN_OSC);
        }

        private void ConfirmCommand(ushort addres, string message)
        {
            if (DialogResult.Yes ==
                MessageBox.Show($"{message}?", "Подтверждение команды", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                this._device.DiscretData.SetBitByAdress(addres, message);
            }
        }

        
        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(MR801dvg); }
        }

        public bool Multishow { get { return false; } }

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion [INodeView Members]
    }
}
