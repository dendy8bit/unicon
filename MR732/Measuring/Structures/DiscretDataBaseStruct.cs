﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Measuring.Structures
{
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Fields]
        [Layout(0, Count = 16)] private ushort[] _base;
        [Layout(1, Count = 16)] private ushort[] _dispairs;
        [Layout(2, Count = 16)] private ushort[] _params;
        #endregion [Fields]

        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get { return Common.GetBitsArray(this._base, 0, 23); }
        }

        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 24, 39); }
        }

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 40, 55); }
        }

        /// <summary>
        /// Защиты I дифф., P
        /// </summary>
        public bool[] CurrentDiff
        {
            get { return Common.GetBitsArray(this._base, 56, 64); }
        }

        /// <summary>
        /// Защиты I, I*
        /// </summary>
        public bool[] Current
        {
            get { return Common.GetBitsArray(this._base, 67, 94); }
        }

        /// <summary>
        /// U,F,Q
        /// </summary>
        public bool[] VoltageFrequency
        {
            get { return Common.GetBitsArray(this._base, 95, 126); }
        }

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] QandExternalDefenses
        {
            get { return Common.GetBitsArray(this._base, 127, 147); }
        }

        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic
        {
            get { return Common.GetBitsArray(this._base, 148, 179); }
        }

        /// <summary>
        /// Смешанные сигналы
        /// </summary>
        public bool[] CommonSignals
        {
            get { return Common.GetBitsArray(this._base, 180, 194); }
        }

        /// <summary>
        /// Реле и индикаторы
        /// </summary>
        public bool[] RelaysIndicators
        {
            get { return Common.GetBitsArray(this._base, 195, 224); }
        }

        /// <summary>
        /// Состояния
        /// </summary>
        public bool[] State
        {
            get { return Common.GetBitsArray(this._base, 227, 235); }
        }
        
        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults
        {
            get
            {
                bool[] ret = 
                {
                    Common.GetBit(this._dispairs[0], 0), //аппаратная
                    Common.GetBit(this._dispairs[0], 1), //программная
                    Common.GetBit(this._dispairs[0], 2), //измерений
                    Common.GetBit(this._dispairs[0], 3), //выключателя
                    //Common.GetBit(this._dispairs[0], 4), // RESERVE
                    Common.GetBit(this._dispairs[0], 5), //Неисправность ТТ
                    Common.GetBit(this._dispairs[0], 6), //цепей управления
                    Common.GetBit(this._dispairs[0], 7), //модуль 1
                    Common.GetBit(this._dispairs[0], 8), //модуль 2
                    Common.GetBit(this._dispairs[0], 9), //модуль 3
                    Common.GetBit(this._dispairs[0], 10), //модуль 4
                    Common.GetBit(this._dispairs[0], 11), //модуль 5
                    Common.GetBit(this._dispairs[0], 12), //уставок
                    Common.GetBit(this._dispairs[0], 13), //группы уставок
                    Common.GetBit(this._dispairs[0], 14), //пароля уставок
                    Common.GetBit(this._dispairs[0], 15), //журнала системы
                    Common.GetBit(this._dispairs[1], 0), //журнала аварий
                    Common.GetBit(this._dispairs[1], 1) //осциллографа
                };
                return ret;
            }
        }

        /// <summary>
        /// Направления токов
        /// </summary>
        public string[] CurrentsSymbols
        {
            get
            {
                bool[] booleanArray = Common.GetBitsArray(this._params, 0, 24);
                string[] result = new string[12];
                for (int i = 0; i < 24; i += 2)
                {
                    result[i / 2] = this.GetCurrentSymbol(booleanArray[i], booleanArray[i + 1]);
                }
                return result;
            }
        }

        private string GetCurrentSymbol(bool symbol, bool error)
        {
            return error ? string.Empty : (symbol ? "-" : "+");
        }

        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool FaultLogic
        {
            get
            {
                return Common.GetBit(this._dispairs[2], 5) | //ошибка CRC констант программы логики
                       Common.GetBit(this._dispairs[2], 6) | //ошибка CRC разрешения программы логики
                       Common.GetBit(this._dispairs[2], 7) | //ошибка CRC программы логики
                       Common.GetBit(this._dispairs[2], 8) | //ошибка CRC меню логики
                       Common.GetBit(this._dispairs[2], 9); //ошибка в ходе выполнения программы логики
            }
        }
        #endregion [Properties]
    }
}
