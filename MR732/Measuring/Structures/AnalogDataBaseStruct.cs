﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR801DVG.Configuration.Structures.MeasureTransformer;

namespace BEMN.MR801DVG.Measuring.Structures
{
    public class AnalogDataBaseStruct: StructBase
    {
        //дифф. ток основная гармоника
        [Layout(0)] private ushort _Ida;      // ток A
        [Layout(1)] private ushort _Idb;      // ток B
        [Layout(2)] private ushort _Idc;      // ток C
        //дифф. ток вторая гармоника
        [Layout(3)] private ushort _Id2a;     // ток A
        [Layout(4)] private ushort _Id2b;     // ток B
        [Layout(5)] private ushort _Id2c;     // ток C
        //дифф. ток пятая гармоника
        [Layout(6)] private ushort _Id5a;     // ток A
        [Layout(7)] private ushort _Id5b;     // ток B
        [Layout(8)] private ushort _Id5c;     // ток C
        //тормозной ток
        [Layout(9)] private ushort _Iba;      // ток A
        [Layout(10)] private ushort _Ibb;      // ток B
        [Layout(11)] private ushort _Ibc;		// ток C
        //ток стороны 1
        [Layout(12)] private ushort _Is1n;     // ток N
        [Layout(13)] private ushort _Is1a;     // ток A
        [Layout(14)] private ushort _Is1b;     // ток B
        [Layout(15)] private ushort _Is1c;     // ток C
        [Layout(16)] private ushort _Is10;     // ток 0
        [Layout(17)] private ushort _Is11;     // ток 1
        [Layout(18)] private ushort _Is12;     // ток 2
        //ток стороны 2
        [Layout(19)] private ushort _Is2n;     // ток N
        [Layout(20)] private ushort _Is2a;     // ток A
        [Layout(21)] private ushort _Is2b;     // ток B
        [Layout(22)] private ushort _Is2c;     // ток C
        [Layout(23)] private ushort _Is20;     // ток 0
        [Layout(24)] private ushort _Is21;     // ток 1
        [Layout(25)] private ushort _Is22;     // ток 2
        //канал U
        [Layout(26)] private ushort _Un;       // напряжение 0
        [Layout(27)] private ushort _Ua;       // напряжение A
        [Layout(28)] private ushort _Ub;       // напряжение B
        [Layout(29)] private ushort _Uc;       // напряжение C
        //напряжения расчетные
        [Layout(30)] private ushort _Uab;
        [Layout(31)] private ushort _Ubc;
        [Layout(32)] private ushort _Uca;
        [Layout(33)] private ushort _U0;
        [Layout(34)] private ushort _U2;
        //канал L1
        [Layout(35)] private ushort _I0;       // ток 0
        [Layout(36)] private ushort _Ia;       // ток A
        [Layout(37)] private ushort _Ib;       // ток B
        [Layout(38)] private ushort _Ic;       // ток C
        //канал L2
        [Layout(39)] private ushort _I20;      // ток 0
        [Layout(40)] private ushort _I2a;      // ток A
        [Layout(41)] private ushort _I2b;      // ток B
        [Layout(42)] private ushort _I2c;      // ток C
        //канал F
        [Layout(43)] private ushort _F;        // частота
        //свободная логика(SIGNAL FREE LOGIC)
        [Layout(44)] private ushort _SFL1;     //
        [Layout(45)] private ushort _SFL2;     //
        [Layout(46)] private ushort _SFL3;     //
        [Layout(47)] private ushort _SFL4;     //
        [Layout(48)] private ushort _SFL5;     //
        [Layout(49)] private ushort _SFL6;     //
        [Layout(50)] private ushort _SFL7;     //
        [Layout(51)] private ushort _SFL8;     //
        //тепловая модель
        [Layout(52)] private ushort _QT;       //состояние тепловой модели
        [Layout(53)] private ushort _QTNUM;    //число пусков
        [Layout(54)] private ushort _QTNUMHOT; //число горячих пусков

        [Layout(55)] private ushort _fi; // угол сдвига фаз
        [Layout(56)] private int _p; //дб выровнено на 2 слова	
        [Layout(57)] private int _q; //дб выровнено на 2 слова
        

        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            double sum = list.Aggregate(0, (current, analog) => current + func.Invoke(analog));
            return (ushort)(sum / count);
        }

        private int GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, int> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            double sum = list.Aggregate(0, (current, analog) => current + func.Invoke(analog));
            return (int)(sum / count);
        }

        //ДИФФ. ТОК ОСН. ГАРМОНИКИ
        public string GetIda(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Ida);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        public string GetIdb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Idb);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        public string GetIdc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Idc);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        //ТОРМОЗНОЙ ТОК
        public string GetIba(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Iba);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        public string GetIbb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Ibb);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        public string GetIbc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Ibc);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        //СТОРОНА 1
        public string GetIs1a(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is1a);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        public string GetIs1b(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is1b);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }
        public string GetIs1c(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is1c);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }

        public string GetInS1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is1n);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittx * 40);
        }

        public string GetI0S1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is10);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }

        public string GetI2S1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is12);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }

        public string GetI1S1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is11);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL1.Ittl * 40);
        }

        //СТОРОНА 2
        public string GetIs2a(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is2a);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittl * 40);
        }
        public string GetIs2b(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is2b);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittl * 40);
        }
        public string GetIs2c(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is2c);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittl * 40);
        }

        public string GetInS2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is2n);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittx * 40);
        }

        public string GetI0S2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is20);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittl * 40);
        }

        public string GetI2S2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is22);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittl * 40);
        }

        public string GetI1S2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Is21);
            return ValuesConverterCommon.Analog.GetI(value, measure.ChannelL2.Ittl * 40);
        }

        // ПРОЦЕНТЫ ДИФ ТОКА И ТОРМОЗНОГО
        private string ShowPercent(ushort value, double Idv)
        {
            double percent = value * 4000.0 / 65536.0/ Idv;
            string param = "%Iдв";
            return Math.Round(percent, 2) + " " + param;
        }

        public string GetPercentIda(List<AnalogDataBaseStruct> list, double Idv)
        {
            ushort val = this.GetMean(list, o => o._Ida);
            return this.ShowPercent(val, Idv);
        }

        public string GetPercentIdb(List<AnalogDataBaseStruct> list, double Idv)
        {
            ushort val = this.GetMean(list, o => o._Idb);
            return this.ShowPercent(val, Idv);
        }

        public string GetPercentIdc(List<AnalogDataBaseStruct> list, double Idv)
        {
            ushort val = this.GetMean(list, o => o._Idc);
            return this.ShowPercent(val, Idv);
        }

        public string GetPercentIba(List<AnalogDataBaseStruct> list, double Idv)
        {
            ushort val = this.GetMean(list, o => o._Iba);
            return this.ShowPercent(val, Idv);
        }

        public string GetPercentIbb(List<AnalogDataBaseStruct> list, double Idv)
        {
            ushort val = this.GetMean(list, o => o._Ibb);
            return this.ShowPercent(val, Idv);
        }

        public string GetPercentIbc(List<AnalogDataBaseStruct> list, double Idv)
        {
            ushort val = this.GetMean(list, o => o._Ibc);
            return this.ShowPercent(val, Idv);
        }

        //КАНАЛ НАПРЯЖЕНИЯ
        public string GetUa(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Ua);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Ub);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Uc);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUab(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Uab);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUbc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Ubc);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUca(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Uca);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetU2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._U2);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetU0(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._U0);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUn(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._Un);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
        }

        public string GetF(List<AnalogDataBaseStruct> list)
        {
            ushort value = this.GetMean(list, o => o._F);
            return ValuesConverterCommon.Analog.GetF(value);
        }

        public string GetQt(List<AnalogDataBaseStruct> list)
        {
            ushort value = this.GetMean(list, o => o._QT);
            return ValuesConverterCommon.Analog.GetQ(value);
        }

        public string GetCountStarts => this._QTNUM.ToString("D");
        public string GetCountHotStarts => this._QTNUMHOT.ToString("D");

        public string GetCosF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._fi);
            return ValuesConverterCommon.Analog.GetCosF(value);
        }

        public string GetQ(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            double value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQ((int)value, measure.ChannelL1.Ittl * measure.ChannelU.KthlValue);
        }

        public string GetP(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            double value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetP(value, measure.ChannelL1.Ittl * measure.ChannelU.KthlValue);
        }

    }
}
