﻿using System;
using System.Collections.Generic;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR801DVG.Oscilloscope.Structures
{
    public class OscJournalStruct : StructBase
    {
        #region [Constants]
        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}.{3:d2}";
        private const string NUMBER_PATTERN = "{0}";
        /// <summary>
        /// Размер одной страницы в словах
        /// </summary>
        private const int OSCLEN = 1024;
        /// <summary>
        /// Размер осциллографа
        /// </summary>
        public const int OscSize = 1744890; 

        #endregion [Constants]

        public static int RecordIndex;

        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _reserv;
        /// <summary>
        /// "READY" если 0 - осциллограмма готова 
        /// </summary>
        [Layout(8)] private int _ready;
        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        [Layout(9)] private int _point;
        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        [Layout(10)] private int _begin;
        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        [Layout(11)] private int _len;
        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        [Layout(12)] private int _after;
        /// <summary>
        /// "ALM" Номер (последней) сработавшей защиты 
        /// </summary>
        [Layout(13)] private ushort _numberDefence;
        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        [Layout(14)] private ushort _sizeReference;

        #endregion [Private fields]

        
        #region [Properties]

        public string Stage
        {
            get
            {
                return this.StageList.Count > this._numberDefence
                    ? this.StageList[this._numberDefence]
                    : this._numberDefence.ToString();
            }
        }

        private List<string> StageList
        {
            get
            {
                return new List<string>
                {
                    "Iд>> мгн.",
                    "Iд>>",
                    "Iд>",
                    "I> 1",
                    "I> 2",
                    "I> 3",
                    "I> 4",
                    "I> 5",
                    "I> 6",
                    "I< ",
                    "I2/I1",
                    "I*> 1",
                    "I*> 2",
                    "I*> 3",
                    "I*> 4",
                    "I*> 5",
                    "I*> 6",
                    "U> 1",
                    "U> 2",
                    "U> 3",
                    "U> 4",
                    "U< 1",
                    "U< 2",
                    "U< 3",
                    "U< 4",
                    "F> 1",
                    "F> 2",
                    "F> 3",
                    "F> 4",
                    "F< 1",
                    "F< 2",
                    "F< 3",
                    "F< 4",
                    "Q> 1",
                    "Q> 2",
                    "Блок. по Q",
                    "Блок. по N",
                    "ВНЕШ. 1",
                    "ВНЕШ. 2",
                    "ВНЕШ. 3",
                    "ВНЕШ. 4",
                    "ВНЕШ. 5",
                    "ВНЕШ. 6",
                    "ВНЕШ. 7",
                    "ВНЕШ. 8",
                    "ВНЕШ. 9",
                    "ВНЕШ. 10",
                    "ВНЕШ. 11",
                    "ВНЕШ. 12",
                    "ВНЕШ. 13",
                    "ВНЕШ. 14",
                    "ВНЕШ. 15",
                    "ВНЕШ. 16",
                    "P> 1",
                    "P> 2",
                    "ЖА СПЛ",
                    "",
                    "",
                    "",
                    "ПУСК ОСЦИЛЛОГРАФА ОТ ДИСКР. СИГНАЛА",
                    "МЕНЮ: ПУСК ОСЦИЛЛОГРАФА",
                    "СДТУ: ПУСК ОСЦИЛЛОГРАФА"
                };
            }
        } 
        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                    {
                        this.GetNumber, //Номер
                        this.GetDate,
                        this.GetTime, //Время
                        this.Stage,
                        this.Len, //Размер
                        this._ready, //Готовность
                        this.Point, //Адрес начала
                        this.GetEnd, //Адрес конца
                        this.Begin, //Смещение
                        
                        this.After, //№ сработавшей защиты
                        this.SizeReference
                    };
            }
        }
        //пустая запись или осциллограмма не готова, тоже признак конца журнала
        public bool IsEmpty
        {
            get
            {
                return this.SizeReference == 0 | this._ready != 0;
            }
        }
        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return (ushort) (this.Point/OSCLEN); }
        }

        #endregion [Properties]


        #region [Private Properties]

        /// <summary>
        /// Номер соощения
        /// </summary>
        private string GetNumber
        {
            get
            {
                string result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        TIME_PATTERN,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                return string.Format
                    (
                        DATE_PATTERN,
                        this._date,
                        this._month,
                        this._year
                    );
            }
        }

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                DateTime a = new DateTime(this._year, this._month, this._date, this._hour, this._minute, this._second, this._millisecond * 10);
                DateTime result = a.AddMilliseconds(alarm);
                return $"{result.Month:D2}/{result.Day:D2}/{result.Year:D2},{result.Hour:D2}:{result.Minute:D2}:{result.Second:D2}.{result.Millisecond:D3}";

            }
            catch (Exception)
            {

                return this.GetFormattedDateTime;
            }

        }

        public string GetFormattedDateTime =>
            $"{this._month:D2}/{this._date:D2}/{this._year:D2},{this._hour:D2}:{this._minute:D2}:{this._second:D2}.{this._millisecond*10:D3}";

        
        /// <summary>
        /// Адрес конца
        /// </summary>
        private string GetEnd
        {
            get
            {
                int num = OscSize;
                int num2 = this.Point + (this.Len*this.SizeReference);
                return num2 > num ? string.Format("{0} [{1}]", num2, num2 - num) : num2.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        public int Len
        {
            get { return this._len; }
        }

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        public int After
        {
            get { return this._after; }
        }

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        public int Begin
        {
            get { return this._begin; }
        }

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        public int Point
        {
            get { return this._point; }
        }

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeReference
        {
            get { return this._sizeReference; }
        }

        #endregion [Private Properties]

    }
}
