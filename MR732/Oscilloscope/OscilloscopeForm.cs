using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MR801DVG.Configuration.Structures.MeasureTransformer;
using BEMN.MR801DVG.Configuration.Structures.Oscope;
using BEMN.MR801DVG.Oscilloscope.HelpClasses;
using BEMN.MR801DVG.Oscilloscope.Loaders;
using BEMN.MR801DVG.Oscilloscope.Structures;
using BEMN.MR801DVG.Properties;

namespace BEMN.MR801DVG.Oscilloscope
{
    public partial class OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";

        #endregion [Constants]
        
        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
       
        
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        private MemoryEntity<OscOptionsStruct> _oscopeOptions;
        private MemoryEntity<OscopeConfigStruct> _oscopeConfigChannels;
        /// <summary>
        /// �������� �����
        /// </summary>
        private MemoryEntity<MeasureTransStruct> _measureTransformer;
        private MR801dvg _device;
        #endregion [Private fields]
        
        #region [Ctor's]
        public OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeForm(MR801dvg device)
        {
            this.InitializeComponent();
            this._device = device;
            MemoryEntity<SetOscStartPageStruct> setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("����� �������� ������������", this._device, 0x900);
            MemoryEntity<OscPage> oscPage = new MemoryEntity<OscPage>("C������� ������������", this._device, 0x900);
            //��������� �������
            this._pageLoader = new OscPageLoader(setOscStartPage, oscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);

            MemoryEntity<OneWordStruct> setOscJournalNumber = new MemoryEntity<OneWordStruct>("����� ������ ��", this._device, 0x800);
            MemoryEntity<OscJournalStruct> oscJournal = new MemoryEntity<OscJournalStruct>("������ ��", this._device, 0x800);
            //��������� �������
            this._oscJournalLoader = new OscJournalLoader(oscJournal, setOscJournalNumber);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoader.AllReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            //������������ �����������
            this._oscopeOptions = new MemoryEntity<OscOptionsStruct>("������������ ��������� ������������", this._device, 0x05A0);
            this._oscopeOptions.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            this._oscopeOptions.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            //��������� ������� �����
            this._measureTransformer = new MemoryEntity<MeasureTransStruct>("������������� ����� �������������", this._device, 0x103E);
            this._measureTransformer.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscopeOptions.LoadStruct);
            this._measureTransformer.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            //������������ ������� ������������
            this._oscopeConfigChannels = new MemoryEntity<OscopeConfigStruct>("������������ ������� ������������", this._device, 0x102E);
            this._oscopeConfigChannels.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._measureTransformer.LoadStruct);
            this._oscopeConfigChannels.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]
        
        #region [Help Classes Events Handlers]

        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
            this.EnableButtons = true;
            this._oscJournalReadButton.Enabled = true;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.RecordNumber == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }

            this.EnableButtons = true;
        }

        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            int number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct, 
                this._measureTransformer.Value, this._oscopeConfigChannels.Value.ChannelsWords);
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this.EnableButtons = true;
            this._oscShowButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]
        
        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]
        
        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("��732_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]
        
        #region [Event Handlers]
        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[12000], new OscJournalStruct(), new MeasureTransStruct(), new ushort[24] );
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this.CountingList.IsLoad)
                {
                    fileName = this.CountingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��801��� v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                MessageBox.Show("");
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscopeConfigChannels.LoadStruct();
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscopeOptions.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        { 
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR801dvg); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]
    }
}