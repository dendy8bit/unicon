<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
  <script type="text/javascript">
    function StringToFloat(value){
    var res = parseFloat(value)
    return res
    }
  </script>
</head>
    <body>
      <h2>
        ���������� <xsl:value-of select="MR801DVG/���_����������"/>. ������ �� <xsl:value-of select="MR801DVG/������"/>. ����� <xsl:value-of select="MR801DVG/�����_����������"/>
      </h2>

      <h3>
        <b>��������� ��������������� ���������������</b>
      </h3>
      <b>������� 1</b>
      <table border="1">
        <tr bgcolor="FFFFCC">
          <th>����������� ��������� ��� �� L1, �</th>
          <th>���������� ����������� L1</th>
          <th>����������� ��������� ��� �� X1, A</th>
          <th>���������� ����������� X1</th>
          <th>��������� I0 L1</th>
          <th>��� ��������������</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L1/������������_��"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L1/����������_L"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L1/������������_����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L1/����������_X"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L1/���������_I0"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L1/���_��"/>
          </td>
        </tr>
      </table>

      <b>������� 2</b>
      <table border="1">
        <tr bgcolor="FFFFCC">
          <th>����������� ��������� ��� �� L2, �</th>
          <th>���������� ����������� L2</th>
          <th>����������� ��������� ��� �� X2, A</th>
          <th>���������� ����������� X2</th>
          <th>��������� I0 L2</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L2/������������_��"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L2/����������_L"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L2/������������_����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L2/����������_X"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_L2/���������_I0"/>
          </td>
        </tr>
      </table>

      <b>����������</b>
      <table border="1">
        <tr bgcolor="FFFFCC">
          <th>U0</th>
          <th>KTH�</th>
          <th>������.���</th>
          <th>KTHn</th>
          <th>������.��n</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_U/���_Uo"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_U/KTHL"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_U/�������������_L"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_U/KTHX"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/�������������_�������������/�����_U/�������������_X"/>
          </td>
        </tr>
      </table>

      <b>�������� ����� ��</b>
      <table border="1">
        <tr bgcolor="FFFFCC">
          <th>I�min, I��</th>
          <th>T��, ��</th>
          <th>�����</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/��������_��/I�min"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/��������_��/�����_����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/��������_��/�����"/>
          </td>
        </tr>
      </table>

      <!-- |||||||||||||||||||||||||||��������� ���������||||||||||||||||||||||||||||||||||||||||||| -->
      <h3>
        <b>��������� ���������</b>
      </h3>
      <table border="1">
        <tr bgcolor="CCCCCC">
          <th>�����, � </th>
          <th>����, �</th>
          <th>I��, In</th>
          <th>I����, In</th>
          <th>� �����, ��</th>
          <th>� ����, ��</th>
          <th>Q���, %</th>
          <th>���� Q�����</th>
          <th>���� N�����</th>
        </tr>
        <tr align ="center">
          <td>
            <xsl:value-of select="MR801DVG/���������/T���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/T���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/I��"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/I����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/T����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/T����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/Q"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/Q_�����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���������/N_�����"/>
          </td>
        </tr>
      </table>
      <p></p>

      <b>���������� ������</b>
      <table border="1">
        <tr>
          <th bgcolor="CCCCCC">P</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/�������_���������/Power"/>
            <xsl:value-of select="MR801DVG/�������_���������/Pwatt"/>
          </td>
        </tr>
        <tr>
          <th bgcolor="CCCCCC">cosf</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/�������_���������/Cos"/>
          </td>
        </tr>
        <tr>
          <th bgcolor="CCCCCC">���, %</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/�������_���������/Kpd"/>
          </td>
        </tr>
        <tr>
          <th bgcolor="CCCCCC">Sn</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/EngineSn"/>
          </td>
        </tr>
      </table>
      <p></p>
      <!--|||||||||||||||||||||||||������� �������|||||||||||||||||||||||||||||||||||||||||-->
      <h3>
        <b>������� �������</b>
      </h3>
      <b>���������� ������� �</b>
      <table border="1">
        <tr bgcolor="CCFF99">
          <th>����� ��</th>
          <th>������������</th>
        </tr>
        <xsl:for-each select="MR801DVG/��/��">
          <xsl:if test="position() &lt; 9 ">
            <tr align="center">
              <td>
                <xsl:value-of  select="position()"/>
              </td>
              <td>
                <xsl:for-each select="�������">
                  <xsl:if test="current() !='���'">
                    <xsl:if test="current() ='��'">
                      �<xsl:value-of select="position()"/>
                    </xsl:if>
                    <xsl:if test="current() ='������'">
                      ^�<xsl:value-of select="position()"/>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <b>���������� ������� ���</b>
      <table border="1">
        <tr bgcolor="CCFF99">
          <th>����� ��</th>
          <th>������������</th>
        </tr>
        <xsl:for-each select="MR801DVG/��/��">
          <xsl:if test="position() &gt; 8 ">
            <tr align="center">
              <td>
                <xsl:value-of  select="position()"/>
              </td>
              <td>
                <xsl:for-each select="�������">
                  <xsl:if test="current() !='���'">
                    <xsl:if test="current() ='��'">
                      �<xsl:value-of select="position()"/>
                    </xsl:if>
                    <xsl:if test="current() ='������'">
                      ^�<xsl:value-of select="position()"/>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>
      <br />

      <table border="1">
        <tr>
          <th bgcolor="CCCCCC">��������� ������ �������</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/�������_�������/����_���������_��_�������"/>
          </td>
        </tr>
        <tr>
          <th bgcolor="CCCCCC">����� ���������</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/�������_�������/����_������_�����������"/>
          </td>
        </tr>
        <tr>
          <th bgcolor="CCCCCC">����� ������������� ��</th>
          <td align="center">
            <xsl:value-of select="MR801DVG/��������_��/�����_�������������_��"/>
          </td>
        </tr>
      </table>
      <p></p>

      <!-- |||||||||||||||||||||||||||�������� �������||||||||||||||||||||||||||||||||||||||||||| -->
      <h3>
        <b>�������� ������� </b>
      </h3>
      <b>�������� ����</b>
      <table border="1">
        <tr bgcolor="CCFFCC">
          <th>�����</th>
          <th>���</th>
          <th>������</th>
          <th>�����, ��</th>
        </tr>


        <xsl:for-each select="MR801DVG/���������_����������/����/���_����/����_����">
          <tr align="center">
            <td>
              <xsl:value-of select="position()"/>
            </td>
            <td>
              <xsl:value-of select="@���"/>
            </td>
            <td>
              <xsl:value-of select="@������"/>
            </td>
            <td>
              <xsl:value-of select="@�����"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>
      <p></p>

      <b>����������</b>
      <table border="1">
        <tr bgcolor="CCFFCC">
          <th>�����</th>
          <th>���</th>
          <th>������</th>
          <th>����</th>
        </tr>


        <xsl:for-each select="MR801DVG/���������_����������/����������/���_����������/����_���������">
          <tr align="center">
            <td>
              <xsl:value-of select="position()"/>
            </td>
            <td>
              <xsl:value-of select="@���"/>
            </td>
            <td>
              <xsl:value-of select="@������"/>
            </td>
            <td>
              <xsl:value-of select="����_����������/@����"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>

      <p></p>

      <b>���� �������������</b>
      <table border="1">
        <tr bgcolor="CCFFCC">
          <th>����������</th>
          <th>�����������</th>
          <th>���������</th>
          <th>�����������</th>
          <th>������������� ����� ��</th>
          <th>�������, ��</th>
        </tr>

        <tr align="center">
          <td>
            <xsl:variable  name = "v1" select = "MR801DVG/���������_����������/����_�������������/@�������������_1" />
            <xsl:if test="$v1 = 'false' " >��� </xsl:if>
            <xsl:if test="$v1 = 'true' " > ���� </xsl:if>
          </td>

          <td>
            <xsl:variable  name = "v2" select= "MR801DVG/���������_����������/����_�������������/@�������������_2" />
            <xsl:if test="$v2 = 'false' " >��� </xsl:if>
            <xsl:if test="$v2 = 'true' " > ���� </xsl:if>
          </td>

          <td>
            <xsl:variable  name = "v3" select= "MR801DVG/���������_����������/����_�������������/@�������������_3" />
            <xsl:if test="$v3 = 'false' " >��� </xsl:if>
            <xsl:if test="$v3 = 'true' " > ���� </xsl:if>
          </td>

          <td>
            <xsl:variable  name = "v4" select= "MR801DVG/���������_����������/����_�������������/@�������������_4" />
            <xsl:if test="$v4 = 'false' " >��� </xsl:if>
            <xsl:if test="$v4 = 'true' " > ���� </xsl:if>
          </td>

          <td>
            <xsl:variable  name = "v5" select= "MR801DVG/���������_����������/����_�������������/@�������������_5" />
            <xsl:if test="$v5 = 'false' " >��� </xsl:if>
            <xsl:if test="$v5 = 'true' " > ���� </xsl:if>
          </td>

          <td>
            <xsl:value-of  select= "MR801DVG/���������_����������/����_�������������/@�������_����_�������������" />
          </td>
        </tr>
      </table>

      <p></p>

      <b>���</b>
      <table border="1">
        <tr bgcolor="CCFFCC">
          <th>����� ���</th>
          <th>������������</th>
        </tr>
        <xsl:for-each select="MR801DVG/���/���">
          <tr align="center">
            <td>
              <xsl:value-of select="position()"/>
            </td>
            <td>
              <xsl:for-each select="�������">
                <xsl:value-of select="current()"/>|
              </xsl:for-each>
            </td>
          </tr>
        </xsl:for-each>
      </table>

      <!-- |||||||||||||||||||||||||||���������� � ����������||||||||||||||||||||||||||||||||||||||||||| -->
      <h3>
        <b>���������� � ����������</b>
      </h3>
      <b>����������� - <xsl:for-each select="MR801DVG/������������_�����������/�����������">
          <xsl:if test="current() = 'false'"> ���</xsl:if>
          <xsl:if test="current() = 'true'"> ��</xsl:if>
        </xsl:for-each>
      </b>
      <table border="1">
        <tr bgcolor="9966CC">
          <th>���������</th>
          <th>��������</th>
          <th>�������������</th>
          <th>����-��</th>
          <th>t ����, ��</th>
          <th>I ����, In</th>
          <th>�������, ��</th>
          <th>t �����, ��</th>
          <th>�������� �����</th>
          <th>����. ����</th>
        </tr>

        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/���������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/��������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/t����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/���_����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/�������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/���������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/��������_�����_���������_����������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����������_����"/>
          </td>
        </tr>
      </table>
      <p></p>

      <b>����������</b>
      <table border="1">
        <tr bgcolor="9966CC">
          <th>���� ��������</th>
          <th>���� ���������</th>
          <th>������� ��������</th>
          <th>������� ���������</th>
          <th>����</th>
          <th>����</th>
          <th>�������</th>
          <th>����</th>
          <th>��������</th>
        </tr>

        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����_���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����_����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����_����_��������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����_����_���������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/�������"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/������������_�����������/��������"/>
          </td>
        </tr>
      </table>
      <p></p>

      <b>���</b>
      <table border="1">
        <tr bgcolor="9966CC">
          <th>�����</th>
          <th>����������</th>
          <th>t ����, ��</th>
          <th>t �����, ��</th>
          <th>1 ����</th>
          <th>2 ����</th>
          <th>��������������</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/���/�����"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���/����_����������_���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���/�����_����������_���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���/�����_����������_���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���/����1"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���/����2"/>
          </td>
          <td>
            <xsl:for-each select="MR801DVG/���/��������������">
              <xsl:if test="current() = 'true'">����</xsl:if>
              <xsl:if test="current() = 'false'">���</xsl:if>
            </xsl:for-each>
          </td>
          
        </tr>
      </table>
      <p></p>

      <b>���</b>
      <table border="1">
        <tr bgcolor="9966CC">
          <th>�� �������</th>
          <th>�� ����������</th>
          <th>�� ��������.</th>
          <th>�� ������</th>
          <th>����. ����</th>
          <th>����������</th>
          <th>�����</th>
          <th>��� ������.</th>
          <th>t ��, ��</th>
          <th>�������</th>
          <th>t ���, ��</th>
          <th>t ����, ��</th>
          <th>�����</th>
        </tr>
        <xsl:for-each select="MR801DVG/���">
          <tr align="center">
            <xsl:for-each select="��_�������">
              <td>
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
              </td>
            </xsl:for-each>
          <td>
            <xsl:for-each select="��_����������">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="��_��������������">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="��_������">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:value-of select="����"/>
          </td>
          <td>
            <xsl:value-of select="����������"/>
          </td>
          <td>
            <xsl:value-of select="����_������"/>
          </td>
          <td>
            <xsl:value-of select="���_������������"/>
          </td>
          <td>
            <xsl:value-of select="���"/>
          </td>
          <td>
            <xsl:value-of select="���_�������"/>
          </td>
          <td>
            <xsl:value-of select="������"/>
          </td>
          <td>
            <xsl:value-of select="�����_�������"/>
          </td>
          <td>
            <xsl:value-of select="�����"/>
          </td>
        </tr>
        </xsl:for-each>
      </table>
      <p></p>
      <b>���</b>
      <table border="1">
        <tr bgcolor="9966CC">
          <th>�����</th>
          <th>�������, l�</th>
        </tr>

        <tr align="center">
          <td>
            <xsl:value-of select="MR801DVG/���/������������_���"/>
          </td>
          <td>
            <xsl:value-of select="MR801DVG/���/�������_���"/>
          </td>
        </tr>
      </table>

      
      <!-- |||||||||||||||||||||||||||������||||||||||||||||||||||||||||||||||||||||||| -->
      <h2>
        <b>������. �������� ������ �������</b>
      </h2>
      <h3>
        <b>���� ��</b>
      </h3>
      <b>������� 1</b>
      <table border="1">
        <tr align="center">
          <th bgcolor="FFCC66">I</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/I_L1"/>
          </td>
        </tr>
       <tr align="center">
          <th bgcolor="FFCC66">In</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/In_L1"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="FFCC66">I0</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/I0_L1"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="FFCC66">I2</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/I2_L1"/>
          </td>
        </tr>
      </table>
      <p></p>
      <b>������� 2</b>
      <table border="1">
        <tr align="center">
          <th bgcolor="FFCC66">I</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/I_L2"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="FFCC66">In</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/In_L2"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="FFCC66">I0</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/I0_L2"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="FFCC66">I2</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/����/I2_L2"/>
          </td>
        </tr>
      </table>

      <h3>����. ������</h3>

      <b>���. ������� ������</b>

      <table border="1">
        <tr align="center">
          <th bgcolor="EAC634">�����</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/�����"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">����������</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/����������"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">l�&gt;, l��</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/I�"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">t��, ��</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/���"/>
          </td>
        </tr>
      </table>
      <p></p>

      <b>���������� l2/l1 - <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/I2_x002F_I1">
          <xsl:if test="current() = 'false'">���</xsl:if>
          <xsl:if test="current() = 'true'">����</xsl:if>
        </xsl:for-each>
        </b>

      <table border="1">
        <tr align="center">
          <th bgcolor="EAC634">������. ����</th>
          <td>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/������_����_I2_x002F_I1">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">l2/l1(�� ���), %</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/�������_I2_x002F_I1"/>
          </td>
        </tr>
      </table>
      <p></p>

      <b>���������� l5/l1 - <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/I5_x002F_I1">
          <xsl:if test="current() = 'false'">���</xsl:if>
          <xsl:if test="current() = 'true'">����</xsl:if>
        </xsl:for-each>
      </b>

      <table border="1">
        <tr align="center">
          <th bgcolor="EAC634">������. ����</th>
          <td>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/������_����_I5_x002F_I1">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">l5/l1(�� ���������������), %</th>
          <td>
            <xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/�������_I5_x002F_I1"/>
          </td>
        </tr>
      </table>
      <p></p>

      <b>�������������� ����������</b>
      <table border="1">
        <tr bgcolor="EAC634" align="center">
          <th>l�1 - ������, l��</th>
          <th>f1 - ���� �������, '</th>
          <th>l�2 - ������, l��</th>
          <th>f2 - ���� �������, '</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������">
          <tr align="center">
            <td>
              <xsl:value-of select="I�1"/>
            </td>
            <td>
              <xsl:value-of select="K1"/>
            </td>
            <td>
              <xsl:value-of select="I�2"/>
            </td>
            <td>
              <xsl:value-of select="K2"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>

      <p></p>

      <table border="1">
        <tr align="center">
          <th bgcolor="EAC634">�����������</th>
          <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/���_������/���"/></td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">����</th>
          <td>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/����">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">���</th>
          <td>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/���">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="EAC634">���</th>
          <td>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������/���">
              <xsl:if test="current() = 'false'">���</xsl:if>
              <xsl:if test="current() = 'true'">��</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
      </table>

      <h3>����. �������</h3>

      <b>���. ������� ������� ��� ����������</b>

      <table border="1">
        <tr align="center" bgcolor="FFCC66">
          <th>�����</th>
          <th>����������</th>
          <th>������� l�&gt;&gt;, l��</th>
          <th>�������� ������� T�&gt;&gt;, ��</th>
          <th>������� �� ���������� ���������</th>
          <th>�����������</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
        </tr>
        <tr align="center">
          <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/���_������_�������">
            <td><xsl:value-of select="�����"/></td>
            <td><xsl:value-of select="����������"/></td>
            <td><xsl:value-of select="I�"/></td>
            <td><xsl:value-of select="���"/></td>
            <td>
              <xsl:for-each select="I�_���">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
              </xsl:for-each>
            </td>
            <td><xsl:value-of select="���"/></td>
              <td>
                <xsl:for-each select="����">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
                </xsl:for-each>
              </td>
              <td>
              <xsl:for-each select="���">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
              </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="���">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
                </xsl:for-each>
              </td>
           
          </xsl:for-each>
        </tr>
      </table>
      <p></p>

      <h3>������ P</h3>
      <table border="1">
        <tr align="center" bgcolor="FFCC66">
          <th>�������</th>
          <th>�����</th>
          <th>S��, S�</th>
          <th>���, ����.</th>
          <th>tcp, ��</th>
          <th>�������</th>
          <th>S��, S�</th>
          <th>t��, ��</th>
          <th>I��, l�</th>
          <th>����������</th>
          <th>�����������</th>
          <th>��������</th>
          <th>����</th>
          <th>���</th>
          <th>�����</th>
          <th>���</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_��������/���_������_��������">
        <tr align="center">
            <td>
              P<xsl:value-of select="position()"/>
            </td>
            <td>
              <xsl:value-of select="�����"/>
            </td>
            <td>
              <xsl:value-of select="�������_������������"/>
            </td>
            <td>
              <xsl:value-of select="����_������������"/>
            </td>
            <td>
              <xsl:value-of select="���"/>
            </td>
            <td>
              <xsl:for-each select="�������">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:value-of select="�������_��������"/>
            </td>
            <td>
              <xsl:value-of select="���"/>
            </td>
            <td>
              <xsl:value-of select="I��"/>
            </td>
            <td>
              <xsl:value-of select="����������"/>
            </td>
            <td>
              <xsl:value-of select="���"/>
            </td>
          <td>
              <xsl:for-each select="���_�������">
              <xsl:if test="current() = 'true'">����</xsl:if>
              <xsl:if test="current() = 'false'">���</xsl:if>
              </xsl:for-each>
            </td>
          <td>
                <xsl:for-each select="����">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
                </xsl:for-each>
              </td>
          <td>
            <xsl:for-each select="���">
                <xsl:if test="current() = 'true'">����</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
            </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="�����_�������">
                  <xsl:if test="current() = 'true'">����</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
               </xsl:for-each>
              </td>
              <td>
                 <xsl:for-each select="���">
                  <xsl:if test="current() = 'true'">����</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
                 </xsl:for-each>
              </td>
            
        </tr>
        </xsl:for-each>
      </table>

      <h3>
        <b>������ I</b>
      </h3>
          <b>������ I> ������������� ����</b>
          <table border="1">
            <tr bgcolor="FFCC66" align="center">
              <th>�������</th>
              <th>�����</th>
              <th>I��, In</th>
              <th>�������</th>
              <th>���� �� U</th>
              <th>U����, �</th>
              <th>�����������</th>
              <th>������.����</th>
              <th>������</th>
              <th>������-��</th>
              <th>t��, ��</th>
              <th>k �����. �-��</th>
              <th>����������</th>
              <th>�����������</th>
              <th>�����.</th>
              <th>��, ��</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
            </tr>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/�������/���_���/MtzMainStruct">
              <xsl:if test="position() &lt; 5">
                <tr align="center">
                  <td>
                    I&#62;<xsl:value-of select="position()"/>
                  </td>
                  <td>
                    <xsl:value-of select="�����"/>
                  </td>
                  <td>
                    <xsl:value-of select="�������"/>
                  </td>
                  <td>
                    <xsl:value-of select="�������"/>
                  </td>
                  <xsl:for-each select="U����">
                  <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	��	</xsl:if>
                  </td>
                  </xsl:for-each>
                  <td>
                    <xsl:value-of select="U����_�������"/>
                  </td>
                  <td>
                    <xsl:value-of select="�����������"/>
                  </td>
                  <td>
                    <xsl:value-of select="������_����"/>
                  </td>
                  <td>
                    <xsl:value-of select="������"/>
                  </td>
                  <td>
                    <xsl:value-of select="��������������"/>
                  </td>
                  <td>
                    <xsl:value-of select="t��"/>
                  </td>
                  <td>
                    <xsl:value-of select="K"/>
                  </td>
                  <td>
                    <xsl:value-of select="����������"/>
                  </td>
                  <td>
                    <xsl:value-of select="���"/>
                  </td>
                  <xsl:for-each select="���������">
                    <td>
                      <xsl:if test="current() = 'true'">����</xsl:if>
                      <xsl:if test="current() = 'false'">���</xsl:if>
                    </td>
                  </xsl:for-each>
                  <td>
                    <xsl:value-of select="t�"/>
                  </td>
                  <xsl:for-each select="����">
                    <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </td>
                  </xsl:for-each>
                  <xsl:for-each select="���">
                    <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </td>
                  </xsl:for-each>
                  <xsl:for-each select="���">
                    <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </td>
                  </xsl:for-each>
                </tr>
              </xsl:if>
            </xsl:for-each>
          </table>
          <p></p>
              <table border="1">
                <tr bgcolor="FFCC66" align="center">
                  <th>�������</th>
                  <th>�����</th>
                  <th>I��, In</th>
                  <th>�������</th>
                  <th>���� �� U</th>
                  <th>U����, �</th>
                  <th>�����������</th>
                  <th>������.����</th>
                  <th>������</th>
                  <th>������-��</th>
                  <th>t��, ��</th>
                  <th>k �����. �-��</th>
                  <th>����������</th>
                  <th>�����������</th>
                  <th>�����.</th>
                  <th>��, ��</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/�������/���_���/MtzMainStruct">
                  <xsl:choose>
                    <xsl:when test="position() = 5">
                      <tr align="center">
                      <td>
                        I&#62;5
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����_Ip"/>
                      </td>
                      <xsl:for-each select="U����">
                        <td>
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	��	</xsl:if>
                        </td>
                      </xsl:for-each>
                      <td>
                        <xsl:value-of select="U����_�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="������_����"/>
                      </td>
                      <td>
                        <xsl:value-of select="������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������������"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="K"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <xsl:for-each select="���������">
                        <td>
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </td>
                      </xsl:for-each>
                      <td>
                        <xsl:value-of select="t�"/>
                      </td>
                      <xsl:for-each select="����">
                        <td>
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </td>
                      </xsl:for-each>
                      <xsl:for-each select="���">
                        <td>
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </td>
                      </xsl:for-each>
                      <xsl:for-each select="���">
                        <td>
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </td>
                      </xsl:for-each>
                    </tr>
                    </xsl:when>
                    <xsl:when test="position() = 6">
                      <tr align="center">
                        <td>
                          I&#62;6
                        </td>
                        <td>
                          <xsl:value-of select="�����"/>
                        </td>
                        <td>
                          <xsl:value-of select="�������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�����_Ip"/>
                        </td>
                        <xsl:for-each select="U����">
                          <td>
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	��	</xsl:if>
                          </td>
                        </xsl:for-each>
                        <td>
                          <xsl:value-of select="U����_�������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�����������"/>
                        </td>
                        <td>
                          <xsl:value-of select="������_����"/>
                        </td>
                        <td>
                          <xsl:value-of select="������"/>
                        </td>
                        <td>
                          <xsl:value-of select="��������������"/>
                        </td>
                        <td>
                          <xsl:value-of select="t��"/>
                        </td>
                        <td>
                          <xsl:value-of select="K"/>
                        </td>
                        <td>
                          <xsl:value-of select="����������"/>
                        </td>
                        <td>
                          <xsl:value-of select="���"/>
                        </td>
                        <xsl:for-each select="���������">
                          <td>
                            <xsl:if test="current() = 'true'">����</xsl:if>
                            <xsl:if test="current() = 'false'">���</xsl:if>
                          </td>
                        </xsl:for-each>
                        <td>
                          <xsl:value-of select="t�"/>
                        </td>
                        <xsl:for-each select="����">
                          <td>
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </td>
                        </xsl:for-each>
                        <xsl:for-each select="���">
                          <td>
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </td>
                        </xsl:for-each>
                        <xsl:for-each select="���">
                          <td>
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </td>
                        </xsl:for-each>
                      </tr>
                    </xsl:when>
                  </xsl:choose>
                </xsl:for-each>
              </table>
              <p></p>

          <table border="1">
            <tr bgcolor="FFCC66">
              <th>�������</th>
              <th>�����</th>
              <th>I��, In</th>
              <th>������</th>
              <th>t��, ��</th>
              <th>����������</th>
              <th>�����������</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
            </tr>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/�������/���_���/MtzMainStruct">
              <xsl:if test="position() = 7">
                <tr align="center">
                  <td>
                      I&#60;
                  </td>
                  <td>
                    <center>
                      <xsl:value-of select="�����"/>
                    </center>
                  </td>
                  <td>
                    <center>
                      <xsl:value-of select="�������"/>
                    </center>
                  </td>
                  <td>
                    <center>
                      <xsl:value-of select="������"/>
                    </center>
                  </td>
                  <td>
                    <center>
                      <xsl:value-of select="t��"/>
                    </center>
                  </td>
                  <td>
                    <center>
                      <xsl:value-of select="����������"/>
                    </center>
                  </td>
                  <td>
                    <center>
                      <xsl:value-of select="���"/>
                    </center>
                  </td>
                  <xsl:for-each select="����">
                    <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </td>
                  </xsl:for-each>
                  <xsl:for-each select="���">
                    <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </td>
                  </xsl:for-each>
                  <xsl:for-each select="���">
                    <td>
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </td>
                  </xsl:for-each>
                </tr>
              </xsl:if>
            </xsl:for-each>
          </table>
      <p></p>

      <table border="1">
        <tr align="center" bgcolor="FFCC66">
          <th>�������</th>
          <th>�����</th>
          <th>����������</th>
          <th>l2/l1, %</th>
          <th>t��, ��</th>
          <th>�����������</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_I21/���_I2_x002F_I1/MtzI21">
          <tr align="center">
          <td>
            l2/l1
          </td>
          <td>
            <xsl:value-of select="�����"/>
          </td>
          <td>
            <xsl:value-of select="����������"/>
          </td>
          <td>
            <xsl:value-of select="�������"/>
          </td>
          <td>
            <xsl:value-of select="t��"/>
          </td>
          <td>
            <xsl:value-of select="���"/>
          </td>
          <xsl:for-each select="����">
          <td>
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
          </td>
          </xsl:for-each>
          <xsl:for-each select="���">
            <td>
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </td>
          </xsl:for-each>
          <xsl:for-each select="���">
            <td>
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </td>
          </xsl:for-each>
          </tr>
        </xsl:for-each>
      </table>

      <h3>
        <b>������ I*</b>
      </h3>
      <table border="1">
        <tr bgcolor="FFCC66">
          <th>�������</th>
          <th>�����</th>
          <th>I��, In</th>
          <th>�������</th>
          <th>���� �� U</th>
          <th>U����, �</th>
          <th>�����������</th>
          <th>������.����</th>
          <th>I*</th>
          <th>������-��</th>
          <th>t��, ��</th>
          <th>k �����. �-��</th>
          <th>����������</th>
          <th>�����������</th>
          <th>���������</th>
          <th>��, ��</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/�������_���/���_���_I_x002A_/MtzStarStruct">
          <tr align="center">
            <td>
              <center>
                I*&#62;  <xsl:value-of select="position()"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="�����"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="�������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="�������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="U����_�������"/>
              </center>
            </td>
            <td>
              <xsl:for-each select="����_��_U">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">����</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <center>
                <xsl:value-of select="�����������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="������_����"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="I_x002A_"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="��������������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="t��"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="K"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="����������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="���"/>
              </center>
            </td>
            <td>
              <xsl:for-each select="���������">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">����</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <center>
                <xsl:value-of select="t�"/>
              </center>
            </td>
            <td>
              <xsl:for-each select="����">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">����</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="���">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">����</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="���">
                <xsl:if test="current() ='false'">���</xsl:if>
                <xsl:if test="current() ='true'">����</xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </xsl:for-each>
      </table>
      
            <h3> ������ U&gt;</h3>
            <table border="1">
              <tr align="center" bgcolor="FFCC66">
                <th>�������</th>
                <th>�����</th>
                <th>���</th>
                <th>U��, �</th>
                <th>t��, ��</th>
                <th>t��, ��</th>
                <th>�������</th>
                <th>U��, �</th>
                <th>���������� U&lt;5�</th>
                <th>����������</th>
                <th>�����������</th>
                <th>��� ����.</th>
                <th>����</th>
                <th>���</th>
                <th>���</th>
                <th>�����</th>
              </tr>
              <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_U/���_������_U/DefenseUStruct">
                <xsl:if test="position() &lt; 5">
                <tr align="center">
                <td>
                  U&gt;<xsl:value-of select="position()"/>
                </td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="���_Umax"/></td>
                <td><xsl:value-of select="�������"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td>
                  <xsl:for-each select="�������">
                  <xsl:if test="current() = 'true'">����</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td><xsl:value-of select="U��"/></td>
                <td>
                  <xsl:for-each select="����������_U_5V">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td><xsl:value-of select="����������"/></td>
                <td><xsl:value-of select="���"/></td>
                <td>
                  <xsl:for-each select="���_�������">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="����">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="�����_�������">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
            <p></p>
            <h3>���. U&lt;</h3>
             <table border="1">
              <tr align="center" bgcolor="FFCC66">
                <th>�������</th>
                <th>�����</th>
                <th>���</th>
                <th>U��, �</th>
                <th>t��, ��</th>
                <th>t��, ��</th>
                <th>�������</th>
                <th>U��, �</th>
                <th>���������� U&lt;5�</th>
                <th>����������</th>
                <th>�����������</th>
                <th>��� ����.</th>
                <th>����</th>
                <th>���</th>
                <th>���</th>
                <th>�����</th>
              </tr>
              <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_U/���_������_U/DefenseUStruct">
                <xsl:if test="position() &gt; 4">
                <tr align="center">
                <td>
                  U&lt;<xsl:value-of select="position()-4"/>
                </td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="���_Umax"/></td>
                <td><xsl:value-of select="�������"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td>
                  <xsl:for-each select="�������">
                  <xsl:if test="current() = 'true'">����</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td><xsl:value-of select="U��"/></td>
                <td>
                  <xsl:for-each select="����������_U_5V">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td><xsl:value-of select="����������"/></td>
                <td><xsl:value-of select="���"/></td>
                <td>
                  <xsl:for-each select="���_�������">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="����">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="�����_�������">
                    <xsl:if test="current() = 'true'">����</xsl:if>
                    <xsl:if test="current() = 'false'">���</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
                </xsl:if>
              </xsl:for-each>
            </table>

      <h3>
        <b>������ F></b>
      </h3>
      <table border="1">
        <tr bgcolor="FFCC66">
          <th>�������</th>
          <th>�����</th>
          <th>F��, ��</th>
          <th>t��, ��</th>
          <th>t��, ��</th>
          <th>�������</th>
          <th>F��, ��</th>
          <th>����������</th>
          <th>�����������</th>
          <th>��� ����.</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
          <th>�����</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_F/���_������_F/DefenseFStruct">
          <xsl:if test="position() &lt; 5">
            <tr align="center">
              <td>
                <center>
                  F>  <xsl:value-of select="position()"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="�����"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="F��"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="t��"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="t��"/>
                </center>
              </td>
              <td>
                <xsl:for-each select="F��_�������">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              <td>
                <center>
                  <xsl:value-of select="F��"/>
                </center>
              </td>
              </td>
              <td>
                <center>
                  <xsl:value-of select="����������"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="���"/>
                </center>
              </td>
              <td>
                <xsl:for-each select="���_�������">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="����">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="���">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="���">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="�����_�������">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <h3>
        <b>������ F&#60;</b>
      </h3>
     <table border="1">
        <tr bgcolor="FFCC66">
          <th>�������</th>
          <th>�����</th>
          <th>F��, ��</th>
          <th>t��, ��</th>
          <th>t��, ��</th>
          <th>�������</th>
          <th>F��, ��</th>
          <th>����������</th>
          <th>�����������</th>
          <th>��� ����.</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
          <th>�����</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_F/���_������_F/DefenseFStruct">
          <xsl:if test="position() &gt; 4">
            <tr align="center">
              <td>
                <center>
                  F&lt;  <xsl:value-of select="position()-4"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="�����"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="F��"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="t��"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="t��"/>
                </center>
              </td>
              <td>
                <xsl:for-each select="F��_�������">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              <td>
                <center>
                  <xsl:value-of select="F��"/>
                </center>
              </td>
              </td>
              <td>
                <center>
                  <xsl:value-of select="����������"/>
                </center>
              </td>
              <td>
                <center>
                  <xsl:value-of select="���"/>
                </center>
              </td>
              <td>
                <xsl:for-each select="���_�������">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="����">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="���">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="���">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="�����_�������">
                  <xsl:if test="current() ='false'">	���	</xsl:if>
                  <xsl:if test="current() ='true'">	����	</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <h3>���������� ������ � ���. Q</h3>
      <p></p>
          <b>���������� �� Q</b>
          <table border="1">
            <tr align="center">
              <th bgcolor="FFCC66">�����</th>
              <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/�����������/�����"/></td>
            </tr>
            <tr align="center">
              <th bgcolor="FFCC66">Q, %</th>
              <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/�����������/�������_������������"/></td>
            </tr>
            <tr align="center">
              <th bgcolor="FFCC66">t, �</th>
              <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/�����������/�����_������������"/></td>
            </tr>
          </table>

          <p></p>
          <b>���������� �� N</b>
          <table border="1">
            <tr align="center">
              <th bgcolor="FFCC66">N����</th>
              <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/�����������/�����_��������_������"/></td>
            </tr>
            <tr align="center">
              <th bgcolor="FFCC66">N���</th>
              <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/�����������/�����_�������_������"/></td>
            </tr>
            <tr align="center">
              <th bgcolor="FFCC66">t, �</th>
              <td><xsl:value-of select="MR801DVG/�������_�����/��������_������_�������/�����������/�����_����������"/></td>
            </tr>
          </table>

          <h3>
            <b>������ Q></b>
          </h3>

          <table border="1">
            <tr bgcolor="FFCC66">
              <th>�������</th>
              <th>�����</th>
              <th>Q, %</th>
              <th>����������</th>
              <th>�����������</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
            </tr>
            <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/������_Q/���_������_Q/DefenseQStruct">
              <tr>
                <td>
                  <center>
                    Q&#62;  <xsl:value-of select="position()"/>
                  </center>
                </td>
                <td>
                  <center>
                    <xsl:value-of select="�����"/>
                  </center>
                </td>
                <td>
                  <center>
                    <xsl:value-of select="�������"/>
                  </center>
                </td>
                <td>
                  <center>
                    <xsl:value-of select="����������"/>
                  </center>
                </td>
                <td>
                  <center>
                    <xsl:value-of select="���"/>
                  </center>
                </td>
                <td>
                  <xsl:for-each select="����">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:for-each>
          </table>

      <h3>
        <b>�������</b>
      </h3>

      <table border="1">
        <tr bgcolor="FFCC66">
          <th>�������</th>
          <th>�����</th>
          <th>������ ������������</th>
          <th>t��, ��</th>
          <th>t��, ��</th>
          <th>�������</th>
          <th>������ ��������</th>
          <th>������ ����������</th>
          <th>�����������</th>
          <th>��� ����.</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
          <th>����� �������</th>
        </tr>
        <xsl:for-each select="MR801DVG/�������_�����/��������_������_�������/�������/���_������_�������/ExternalDefenseStruct">
          <tr align="center">
            <td>
              <center>
                �3 <xsl:value-of select="position()"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="�����"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="����_���_�������_�����"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="t��"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="t��"/>
              </center>
            </td>
            <td>
              <xsl:for-each select="�������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <center>
                <xsl:value-of select="��������������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="����������"/>
              </center>
            </td>
            <td>
              <center>
                <xsl:value-of select="���"/>
              </center>
            </td>
            <td>
              <xsl:for-each select="���_�������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="����">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="���">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="���">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="�����_�������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </xsl:for-each>
      </table>

      <!-- |||||||||||||||||||||||||||�����������||||||||||||||||||||||||||||||||||||||||||| -->
      <h3>
        <b>�������������</b>
      </h3>
      <table border="1">
        <tr align="left">
          <th bgcolor="FFCC66">����� � ������������ ���.</th>
          <td align="center"><xsl:value-of select="MR801DVG/������������_������������/����������_�����������"/> | <xsl:value-of select="MR801DVG/OscCount"/> ��</td>
        </tr>
        <tr align="left">
          <th bgcolor="FFCC66">�������� ������������</th>
          <td align="center"><xsl:value-of select="MR801DVG/������������_������������/��������"/></td>
        </tr>
        <tr align="left">
          <th bgcolor="FFCC66">������������ ����������</th>
          <td align="center"><xsl:value-of select="MR801DVG/������������_������������/����������"/></td>
        </tr>
        <tr align="left">
          <th bgcolor="FFCC66">���� ����� ������������</th>
          <td align="center"><xsl:value-of select="MR801DVG/������������_������������/����_�����_���"/></td>
        </tr>
      </table>

      <p></p>

      <b>������</b>
      <table border="1">
        <tr bgcolor="FFCC66">
          <th>�����</th>
          <th>������</th>
        </tr>
        <xsl:for-each select="MR801DVG/������������_������������/���_������/���_������/OscopeChannelStruct">
        <tr align="center">
          <td>����� <xsl:value-of select="position()"/></td>
          <td> <xsl:value-of select="@�����"/></td>
        </tr>
        </xsl:for-each>
      </table>
    </body>
  </html>
</xsl:template>
</xsl:stylesheet>
