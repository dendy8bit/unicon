﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR801DVG.Configuration.Structures;
using BEMN.MR801DVG.Configuration.Structures.APV;
using BEMN.MR801DVG.Configuration.Structures.AVR;
using BEMN.MR801DVG.Configuration.Structures.ControlTT;
using BEMN.MR801DVG.Configuration.Structures.Defenses;
using BEMN.MR801DVG.Configuration.Structures.Defenses.Corners;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseF;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseP;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseQ;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseU;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DifferencialCutoutDefense;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DifferencialDefense;
using BEMN.MR801DVG.Configuration.Structures.Defenses.External;
using BEMN.MR801DVG.Configuration.Structures.Defenses.MtzMain;
using BEMN.MR801DVG.Configuration.Structures.Defenses.MtzStar;
using BEMN.MR801DVG.Configuration.Structures.Defenses.Termblock;
using BEMN.MR801DVG.Configuration.Structures.Engine;
using BEMN.MR801DVG.Configuration.Structures.InputLogicSignals;
using BEMN.MR801DVG.Configuration.Structures.InputSignals;
using BEMN.MR801DVG.Configuration.Structures.LZSH;
using BEMN.MR801DVG.Configuration.Structures.MeasureTransformer;
using BEMN.MR801DVG.Configuration.Structures.Oscope;
using BEMN.MR801DVG.Configuration.Structures.OutputLogicSignals;
using BEMN.MR801DVG.Configuration.Structures.Passport;
using BEMN.MR801DVG.Configuration.Structures.RelayInd;
using BEMN.MR801DVG.Configuration.Structures.Switch;

namespace BEMN.MR801DVG.Configuration
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private const string CONFIG_READ_OK = "Конфигурация прочитана успешно";
        private const string CONFIG_READ_FAIL = "Невозможно загрузить конфигурацию";
        private const string CONFIG_IS_READING = "Идет чтение конфигурации из устройства";
        private const string BASE_CONFIG_PATH = "\\MR801DVG\\MR801DVG_BaseConfig_v{0}.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";
        private const string XML_HEAD = "MR801dvg_SET_POINTS";
        private const string WRITING_CONFIG = "Идет запись конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string SETPOINT_FILE_NAME = "МР801двг_Уставки_версия_{0}.xml";

        private readonly MR801dvg _device;
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private ConfigurationStruct _currentSetpointsStruct;

        private StructUnion<ConfigurationStruct> _configurationValidator;

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(MR801dvg device)
        {
            this.InitializeComponent();
            this._device = device;
            this._configuration = device.Configurationtruct;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadFail);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, this.ConfigurationWriteFail);

            this._progressBar.Maximum = this._configuration.Slots.Count;
            this.Init();
        }


        private void Init()
        {
            this._currentSetpointsStruct = new ConfigurationStruct();

            NewStructValidator<SwitchStruct> switchStructValidator = new NewStructValidator<SwitchStruct>(
                this._toolTip,
                new ControlInfoCheck(this._switch),
                new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                new ControlInfoText(this._switchTUrov, RulesContainer.TimeRule),
                new ControlInfoText(this._switchIUrov, RulesContainer.Ustavka40),
                new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed),
                new ControlInfoCombo(this._switchBinding, StringsConfig.Binding),
                new ControlInfoCombo(this._sdtuBlock, StringsConfig.SwitchSignals)
                );

            NewStructValidator<ApvStruct> apvValidator = new NewStructValidator<ApvStruct>(
                this._toolTip,
                new ControlInfoCombo(this._apvModes, StringsConfig.ApvModes),
                new ControlInfoCombo(this._apvBlocking, StringsConfig.SwitchSignals),
                new ControlInfoText(this._apvTBlock, RulesContainer.TimeRule),
                new ControlInfoText(this._apvTReady, RulesContainer.TimeRule),
                new ControlInfoText(this._apv1Krat, RulesContainer.TimeRule),
                new ControlInfoText(this._apv2Krat, RulesContainer.TimeRule),
                new ControlInfoCheck(this._autoSwitch)
            );

            NewStructValidator<AvrStruct> avrStructValidator = new NewStructValidator<AvrStruct>(
                this._toolTip,
                new ControlInfoCheck(this._avrSignal),
                new ControlInfoCheck(this._avrSelfOff),
                new ControlInfoCheck(this._avrSwitchOff),
                new ControlInfoCheck(this._avrDef),
                new ControlInfoCombo(this._avrBlocking, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrBlockClear, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrSIGNOn, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrResolve, StringsConfig.SwitchSignals),
                new ControlInfoText(this._avrTSr, RulesContainer.TimeRule),
                new ControlInfoCombo(this._avrBack, StringsConfig.SwitchSignals),
                new ControlInfoText(this._avrTBack, RulesContainer.TimeRule),
                new ControlInfoText(this._avrTOff, RulesContainer.TimeRule),
                new ControlInfoCombo(this._avrClear, StringsConfig.ForbiddenAllowed));

            NewStructValidator<LzshStruct> lzshStructValidator = new NewStructValidator<LzshStruct>(
                this._toolTip,
                new ControlInfoCombo(this._lzhModes, StringsConfig.LzshModes),
                new ControlInfoText(this._lzhVal, RulesContainer.Ustavka40));

            NewStructValidator<EngineParametersStruct> engineValidator = new NewStructValidator<EngineParametersStruct>
                (
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineIdv, RulesContainer.Ustavka40),
                new ControlInfoText(this._iStartBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._tStartBox, new CustomIntRule(0, 3276700)),
                new ControlInfoText(this._qBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._engineQresetGr1, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._engineNreset, StringsConfig.SwitchSignals),
                new ControlInfoText(this._tCount, RulesContainer.UshortTo65534)
                );

            NewStructValidator<InputSignalsStruct> inputSignalStructValidator = new NewStructValidator<InputSignalsStruct>(
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals));

            NewDgwValidatior<OscopeAllChannelsStruct, OscopeChannelStruct> oscopeAllChannelValidatior =
                new NewDgwValidatior<OscopeAllChannelsStruct, OscopeChannelStruct>(
                    this._oscChannelsDgv,
                    OscopeAllChannelsStruct.CHANNEL_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.OscChannelsNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.RelaySignals));

            NewStructValidator<OscopeConfigStruct> oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>(
                this._toolTip,
                new ControlInfoText(this._oscopePercent, RulesContainer.UshortTo100),
                new ControlInfoCombo(this._oscopeFixationComboBox, StringsConfig.OscFixation),
                new ControlInfoCombo(this._oscopeCountComboBox, StringsConfig.OscMode),
                new ControlInfoValidator(oscopeAllChannelValidatior),
                new ControlInfoCombo(this._inpOsc, StringsConfig.RelaySignals));

            NewStructValidator<KanalTransStruct> channelL1 = new NewStructValidator<KanalTransStruct>(
                this._toolTip,
                new ControlInfoText(this._TT_L1_TextBox, RulesContainer.UshortRule),
                new ControlInfoText(this._TT_X1_TextBox, RulesContainer.UshortRule),
                new ControlInfoCombo(this._polarityL1, StringsConfig.Polarity),
                new ControlInfoCombo(this._polarityX1, StringsConfig.Polarity),
                new ControlInfoCombo(this._i0CorrectionL1, StringsConfig.BeNo),
                new ControlInfoCombo(this._typeTT1, StringsConfig.TransformatorType));

            NewStructValidator<KanalTransStruct> channelL2 = new NewStructValidator<KanalTransStruct>(
                this._toolTip,
                new ControlInfoText(this._TT_L2_TextBox, RulesContainer.UshortRule),
                new ControlInfoText(this._TT_X2_TextBox, RulesContainer.UshortRule),
                new ControlInfoCombo(this._polarityL2, StringsConfig.Polarity),
                new ControlInfoCombo(this._polarityX2, StringsConfig.Polarity),
                new ControlInfoCombo(this._i0CorrectionL2, StringsConfig.BeNo));

            NewStructValidator<KanalUTransStruct> channelU = new NewStructValidator<KanalUTransStruct>(
                this._toolTip,
                new ControlInfoCombo(this._typeTn, StringsConfig.UoType),
                new ControlInfoText(this._TH_L_TextBox, RulesContainer.Ustavka128),
                new ControlInfoText(this._TH_X_TextBox, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._TH_LKoeff_ComboBox, StringsConfig.KthKoefs),
                new ControlInfoCombo(this._TH_XKoeff_ComboBox, StringsConfig.KthKoefs),
                new ControlInfoCombo(this._faultTnl, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._faultTnx, StringsConfig.SwitchSignals));

            StructUnion<MeasureTransStruct> measuringTrans = new StructUnion<MeasureTransStruct>(
                channelL1, channelL2, channelU);

            // ЛС
            DataGridView[] lsBoxes =
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12,
                this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16
            };
            NewDgwValidatior<InputLogicSignalsStruct>[] inputLogicValidator = new NewDgwValidatior<InputLogicSignalsStruct>[AllInputLogicSignalsStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicSignalsStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicSignalsStruct>
                    (
                    lsBoxes[i],
                    InputLogicSignalsStruct.DISCRETS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.LsState)
                    );
            }
            StructUnion<AllInputLogicSignalsStruct> inputLogicUnion = new StructUnion<AllInputLogicSignalsStruct>(inputLogicValidator);

            // ВЛС
            CheckedListBox[] vlsBoxes =
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8,
                this.VLScheckedListBox9, this.VLScheckedListBox10, this.VLScheckedListBox11, this.VLScheckedListBox12,
                this.VLScheckedListBox13, this.VLScheckedListBox14, this.VLScheckedListBox15, this.VLScheckedListBox16
            };

            NewCheckedListBoxValidator<OutputLogicSignalsStruct>[] vlsValidator = new NewCheckedListBoxValidator<OutputLogicSignalsStruct>[AllOutputLogicSignalsStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalsStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicSignalsStruct>(vlsBoxes[i], StringsConfig.VlsSignals);
            }
            StructUnion<AllOutputLogicSignalsStruct> vlsUnion = new StructUnion<AllOutputLogicSignalsStruct>(vlsValidator);

            #region Уставки защит
            NewStructValidator<CornersStruct> cornersValid = new NewStructValidator<CornersStruct>(
                this._toolTip,
                new ControlInfoText(this._cIL1, RulesContainer.UshortTo360),
                new ControlInfoText(this._cInL1, RulesContainer.UshortTo360),
                new ControlInfoText(this._cI0L1, RulesContainer.UshortTo360),
                new ControlInfoText(this._cI2L1, RulesContainer.UshortTo360),
                new ControlInfoText(this._cIL2, RulesContainer.UshortTo360),
                new ControlInfoText(this._cInL2, RulesContainer.UshortTo360),
                new ControlInfoText(this._cI0L2, RulesContainer.UshortTo360),
                new ControlInfoText(this._cI2L2, RulesContainer.UshortTo360));

            NewStructValidator<DifferencialDefenseStruct> difValid = new NewStructValidator<DifferencialDefenseStruct>(
                this._toolTip,
                new ControlInfoCombo(this._modeDTZComboBox, StringsConfig.DefenseMode),
                new ControlInfoCombo(this._blockingDTZComboBox, StringsConfig.SwitchSignals),
                new ControlInfoText(this._constraintDTZTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._timeEnduranceDTZTextBox, RulesContainer.TimeRule),
                new ControlInfoCheck(this._modeI2I1),
                new ControlInfoCheck(this._crossBlockI2I1),
                new ControlInfoText(this._I2I1TextBox, RulesContainer.UshortTo100),
                new ControlInfoCheck(this._modeI5I1),
                new ControlInfoCheck(this._crossBlockI5I1),
                new ControlInfoText(this._I5I1TextBox, RulesContainer.UshortTo100),
                new ControlInfoText(this._Ib1BeginTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._K1AngleOfSlopeTextBox, new CustomUshortRule(0, 89)),
                new ControlInfoText(this._Ib2BeginTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._K2TangensTextBox, new CustomUshortRule(0, 89)),
                new ControlInfoCombo(this._oscDTZComboBox, StringsConfig.OscDefenceMode),
                new ControlInfoCheck(this._diffUROV),
                new ControlInfoCheck(this._DiffAPV),
                new ControlInfoCheck(this._diffAVR)
                );

            NewStructValidator<DifferencialCutoutDefenseStruct> diffCutoutValidator = new NewStructValidator<DifferencialCutoutDefenseStruct>(
                this._toolTip,
                new ControlInfoCombo(this._modeDTOBTComboBox, StringsConfig.DefenseMode),
                new ControlInfoCombo(this._blockingDTOBTComboBox, StringsConfig.SwitchSignals),
                new ControlInfoText(this._constraintDTOBTTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._timeEnduranceDTOBTTextBox, RulesContainer.TimeRule),
                new ControlInfoCheck(this._diffCutoutMgn),
                new ControlInfoCombo(this._diffCutoutOsc, StringsConfig.OscDefenceMode),
                new ControlInfoCheck(this._diffCutoutUROV), 
                new ControlInfoCheck(this._diffCutoutAPV),
                new ControlInfoCheck(this._diffCutoutAVR));

            NewDgwValidatior<AllDefensePStruct, DefensePStruct> defPDgwValidatior = new NewDgwValidatior<AllDefensePStruct, DefensePStruct>(
                this._defPdgv, 2, this._toolTip,
                new ColumnInfoCombo(StringsConfig.DefPNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoText(new CustomDoubleRule(-2.5, 2.5, 2)),
                new ColumnInfoText(new CustomUshortRule(0, 359)),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoText(new CustomDoubleRule(-2.5, 2.5, 2)),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv (
                        this._defPdgv,
                        new TurnOffRule(1, StringsConfig.DefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(5, false, false, 6, 7)
                        )
                }
            };

            NewDgwValidatior<AllMtzMainStruct, MtzMainStruct> mtzMainValidator = new NewDgwValidatior
                <AllMtzMainStruct, MtzMainStruct>(
                new[] {this._defensesI14DataGrid, this._defensesI56DataGrid, this._defenseI7DataGrid},
                new[] {4, 2, 1},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.DefINames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.Binding, ColumnsType.COMBO, true, false, true),
                new ColumnInfoCombo(StringsConfig.I56Modes, ColumnsType.COMBO, false, true, false),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCombo(StringsConfig.Direction),
                new ColumnInfoCombo(StringsConfig.Undir), 
                new ColumnInfoCombo(StringsConfig.Logic, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.LogicI7, ColumnsType.COMBO, false, false, true),
                new ColumnInfoCombo(StringsConfig.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._defensesI14DataGrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
                        ),
                    new TurnOffDgv
                        (
                        this._defensesI56DataGrid,  new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
                        ),
                    new TurnOffDgv
                        (
                        this._defenseI7DataGrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
                        )
                }
            };

            NewDgwValidatior<AllMtzI21, MtzI21> i21Valid = new NewDgwValidatior<AllMtzI21, MtzI21>(this._defenseI2I1DataGrid, 1, this._toolTip,
                new ColumnInfoCombo(new List<string> {"I2/I1"}, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.UshortTo100),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck());

            NewDgwValidatior<AllMtzStarStruct, MtzStarStruct> mtzStarValidator = new NewDgwValidatior <AllMtzStarStruct, MtzStarStruct>
                (this._difensesIstarDataGrid, AllMtzStarStruct.COUNT, this._toolTip,
                new ColumnInfoCombo(StringsConfig.DefIStarNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.Binding),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCombo(StringsConfig.Direction),
                new ColumnInfoCombo(StringsConfig.Undir),
                new ColumnInfoCombo(StringsConfig.Istar),
                new ColumnInfoCombo(StringsConfig.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (this._difensesIstarDataGrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18))
                }
            };

            NewDgwValidatior<AllDefensesUStruct, DefenseUStruct> defUValid = new NewDgwValidatior<AllDefensesUStruct, DefenseUStruct>(
                new[] {this._defensesUBDataGrid, this._defensesUMDataGrid}, new[] {4, 4}, this._toolTip,
                new ColumnInfoCombo(StringsConfig.DefUNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoCombo(StringsConfig.UmaxDefenseMode, ColumnsType.COMBO, true, false),
                new ColumnInfoCombo(StringsConfig.UminDefenseMode, ColumnsType.COMBO, false, true),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode), 
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (this._defensesUBDataGrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)),
                    new TurnOffDgv
                        (this._defensesUMDataGrid,  new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
                }
            };

            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> defFValid = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (new[] {this._difensesFBDataGrid, this._difensesFMDataGrid}, new[] {4, 4}, this._toolTip,
                new ColumnInfoCombo(StringsConfig.DefFNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (this._difensesFBDataGrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)),
                    new TurnOffDgv
                        (this._difensesFMDataGrid,  new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))
                }
            };

            NewDgwValidatior<AllDefenseQStruct, DefenseQStruct> defQValid = new NewDgwValidatior
                <AllDefenseQStruct, DefenseQStruct>(
                this._defenseQgrid, 2, this._toolTip,
                new ColumnInfoCombo(StringsConfig.DefQNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._defenseQgrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,2, 3, 4, 5, 6, 7))
                }
            };

            NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct> externalDefValid = 
                new NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct>(
                this._externalDifensesDataGrid, AllExternalDefenseStruct.COUNT, this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseMode),
                new ColumnInfoCombo(StringsConfig.ExternalDefSignals),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefSignals),
                new ColumnInfoCombo(StringsConfig.ExternalDefSignals),
                new ColumnInfoCombo(StringsConfig.OscDefenceMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (this._externalDifensesDataGrid, new TurnOffRule(1, StringsConfig.DefenseMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))
                }
            };

            NewStructValidator<TermblockStruct> termBlockValidator = new NewStructValidator<TermblockStruct>(
                this._toolTip,
                new ControlInfoCombo(this._blockQmode, StringsConfig.OffOn),
                new ControlInfoText(this._blockQust, RulesContainer.Ustavka256),
                new ControlInfoText(this._blockQt, RulesContainer.UshortRule),
                new ControlInfoText(this._blockNpusk, RulesContainer.UshortTo10),
                new ControlInfoText(this._blockNhot, RulesContainer.UshortTo10),
                new ControlInfoText(this._blockNt, RulesContainer.UshortRule));

            StructUnion<DefensesSetpointsStruct> setpointUnion = new StructUnion<DefensesSetpointsStruct>(
                cornersValid, 
                difValid, 
                diffCutoutValidator, 
                defPDgwValidatior, 
                mtzMainValidator, 
                i21Valid, 
                mtzStarValidator, 
                defUValid, 
                defFValid, 
                defQValid, 
                termBlockValidator, 
                externalDefValid);
            
            RadioButtonSelector selector = new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton, this._copySetpointsBtn);
            SetpointsValidator<GroupSetpointStruct, DefensesSetpointsStruct> setpointValidator = 
                new SetpointsValidator<GroupSetpointStruct, DefensesSetpointsStruct>(selector, setpointUnion);
            #endregion

            NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> releyValidator = 
                new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (this._outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule));

            NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>  indicatorValidator = 
                new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoColor());

            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
                (this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule));

            StructUnion<AutomaticsParametersStruct> automaticsParametersUnion =
                new StructUnion<AutomaticsParametersStruct>(releyValidator, indicatorValidator, faultValidator);

            NewStructValidator<ControlTTStruct> controlTtValidator = new NewStructValidator<ControlTTStruct>(
                this._toolTip,
                new ControlInfoText(this._controlTtIdmin, RulesContainer.Ustavka40),
                new ControlInfoText(this._controlTtTsr, RulesContainer.TimeRule),
                new ControlInfoCombo(this._controlTtNeisp, StringsConfig.ControlTtNeipr),
                new ControlInfoCombo(this._inpResTT, StringsConfig.SwitchSignals));

            NewStructValidator<PassportStruct> passportValidator = new NewStructValidator<PassportStruct>(
                this._toolTip,
                new ControlInfoText(this._passportP, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._passportWatt, StringsConfig.Pwatt),
                new ControlInfoText(this._passportCos, new CustomDoubleRule(0, 0.99, 2)),
                new ControlInfoText(this._passportKpd, RulesContainer.UshortTo100));

            this._configurationValidator = new StructUnion<ConfigurationStruct>(
                switchStructValidator,
                apvValidator,
                avrStructValidator,
                lzshStructValidator,
                engineValidator,
                inputSignalStructValidator,
                oscopeConfigValidator,
                measuringTrans,
                inputLogicUnion,
                vlsUnion,
                setpointValidator,
                automaticsParametersUnion,
                controlTtValidator,
                passportValidator);
        }

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this._currentSetpointsStruct = this._configuration.Value;
            this.ShowConfiguration();
            this._statusLabel.Text = CONFIG_READ_OK;
            MessageBox.Show(CONFIG_READ_OK);
        }

        private void ConfigurationReadFail()
        {
            this._statusLabel.Text = CONFIG_READ_FAIL;
            MessageBox.Show(CONFIG_READ_FAIL);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0x0D00, true, "ConfirmConfig" + this._device.DeviceNumber, this._device);
        }

        private void ConfigurationWriteFail()
        {
            this._configuration.RemoveStructQueries();
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            this._configurationValidator.Set(this._currentSetpointsStruct);
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.StartReadConfiguration();
        }

        private void StartReadConfiguration()
        {
            this._progressBar.Value = 0;
            this._statusLabel.Text = CONFIG_IS_READING;
            this._configuration.LoadStruct();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfigInDevice();
        }

        private void WriteConfigInDevice()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР801двг №{0}?", this._device.DeviceNumber),"Запись", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes) return;
            this._progressBar.Value = 0;
            this._statusLabel.Text = "Проверка уставок";
            if (!this.WriteConfigFromForm()) return;
            this._statusLabel.Text = WRITING_CONFIG;
            this._configuration.SaveStruct();
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfigFromForm()
        {
            this.IsProcess = true;
            string message;
            this._statusLabel.Text = "Проверка уставок";
            if (this._configurationValidator.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationValidator.Get();
                this._configuration.Value = this._currentSetpointsStruct;
                this.IsProcess = false;
                return true;
            }
            MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
            this.IsProcess = false;
            return false;
        }

        private void _oscopeCountComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._oscCount.Text = StringsConfig.OscTimes.Length > this._oscopeCountComboBox.SelectedIndex
                ? StringsConfig.OscTimes[this._oscopeCountComboBox.SelectedIndex]
                : "XXXXX";
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartReadConfiguration();
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._configuration?.RemoveStructQueries();
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(true);
        }

        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {
                XmlDocument doc = new XmlDocument();
                if (this._device.DeviceVersion == "1.00")
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                         string.Format(BASE_CONFIG_PATH, this._device.DeviceVersion));
                }
                else
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(BASE_CONFIG_PATH, "1.01"));
                }
                

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                        "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        this._configurationValidator.Reset();
                }
                else
                {
                    this._configurationValidator.Reset();
                }
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            if (this.WriteConfigFromForm())
            {
                this._statusLabel.Text = "Идет запись конфигурации в файл";
                this._saveConfigurationDlg.FileName = string.Format(SETPOINT_FILE_NAME, this._device.DeviceVersion);
                if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog())
                {
                    this._statusLabel.Text = string.Empty;
                }
                else
                {
                    try
                    {
                        this.Serialize(this._saveConfigurationDlg.FileName);
                        this._statusLabel.Text = string.Format("Файл {0} успешно сохранён",
                            this._saveConfigurationDlg.FileName);
                    }
                    catch
                    {
                        MessageBox.Show(FILE_SAVE_FAIL);
                    }
                }
                this.IsProcess = false;
            }
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("MR801DVG"));
            ushort[] values = this._currentSetpointsStruct.GetValues();
            XmlElement element = doc.CreateElement(XML_HEAD);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
            if (doc.DocumentElement == null)
            {
                throw new NullReferenceException();
            }
            doc.DocumentElement.AppendChild(element);
            doc.Save(binFileName);
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch (Exception)
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfigInDevice();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfigInDevice();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoints(false);
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void _passportWatt_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._passport_TextChanged(sender, e);
        }

        private void _passport_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string var = this._passportWatt.SelectedItem.ToString();

                double p = double.Parse(this._passportP.Text);
                double cosf = double.Parse(this._passportCos.Text);
                double kpd = double.Parse(this._passportKpd.Text);

                double sn = Math.Abs(kpd) > 0 && Math.Abs(cosf) > 0 ? p*100/(cosf*kpd) : 0;
                
                this._sn.Text = $"{sn:F2} {var}";
            }
            catch
            {
                this._sn.Text = "0";
            }
        }

        private void _switch_CheckedChanged(object sender, EventArgs e)
        {
            this.switchGroup.Enabled = this.controlGroup.Enabled = this._switch.Checked;
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfigFromForm())
                {
                    this._currentSetpointsStruct.DeviceVersion = this._device.DeviceVersion;
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._currentSetpointsStruct.EngineSn = this._sn.Text;
                    this._currentSetpointsStruct.OscCount = this._oscCount.Text;

                    this._statusLabel.Text = HtmlExport.Export(Properties.Resources.MR801dvgMain,
                        Properties.Resources.MR801dvgRes, this._currentSetpointsStruct,
                        this._currentSetpointsStruct.DeviceType, this._currentSetpointsStruct.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
                this.IsProcess = false;
            }
        }

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(MR801dvg); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }


        #endregion [IFormView Members]
    }
}
