﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.AVR
{
    public class AvrStruct : StructBase
    {
        [Layout(0)] private ushort _config;     //конфигурация
        [Layout(1)] private ushort _block;      //вход блокировки АВР
        [Layout(2)] private ushort _clear;      //вход сброс блокировки АВР
        [Layout(3)] private ushort _start;      //вход сигнала запуск АВР
        [Layout(4)] private ushort _on;         //вход АВР срабатывания
        [Layout(5)] private ushort _timeOn;     //время АВР срабатывания
        [Layout(6)] private ushort _off;        //вход АВР возврат
        [Layout(7)] private ushort _timeOff;    //время АВР возврат
        [Layout(8)] private ushort _timeOtkl;   //задержка отключения резерва
        [Layout(9)] private ushort _rez;

        [BindingProperty(0)]
        [XmlElement(ElementName = "От_сигнала")]
        public bool Signal
        {
            get { return Common.GetBit(this._config, 0); }
            set { this._config = Common.SetBit(this._config, 0, value); }
        }
        
        [BindingProperty(1)]
        [XmlElement(ElementName = "По_самоотключению")]
        public bool SwitchOffSelf
        {
            get { return Common.GetBit(this._config, 1); }
            set { this._config = Common.SetBit(this._config, 1, value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "По_отключению")]
        public bool SwitchOff
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "От_защиты")]
        public bool Defence
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Вход_сброса")]
        public string ResetInput
        {
            get { return Validator.Get(this._clear, StringsConfig.SwitchSignals); }
            set { this._clear = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Пуск")]
        public string Start
        {
            get { return Validator.Get(this._start, StringsConfig.SwitchSignals); }
            set { this._start = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "АВР_срабатывание")]
        public string AvrOn
        {
            get { return Validator.Get(this._on, StringsConfig.SwitchSignals); }
            set { this._on = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Тср")]
        public int Ton
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOn); }
            set { this._timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "АВР_возврат")]
        public string AvrOff
        {
            get { return Validator.Get(this._off, StringsConfig.SwitchSignals); }
            set { this._off = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Твозвр")]
        public int Toff
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOff); }
            set { this._timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Тоткл_резерва")]
        public int Trez
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOtkl); }
            set { this._timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Сброс")]
        public string Reset
        {
            get { return Validator.Get(this._config, StringsConfig.ForbiddenAllowed, 7); }
            set { this._config = Validator.Set(value, StringsConfig.ForbiddenAllowed, this._config, 7); }
        }
    }
}
