﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.InputSignals
{
    public class InputSignalsStruct : StructBase
    {
        [Layout(0)] private ushort _groupUst;       // вход переключения на аварийную гр. уставок
        [Layout(1)] private ushort _clrIndicators;  // вход сброса индикации

        [BindingProperty(0)]
        [XmlElement(ElementName = "Вход_аварийная_гр_уставок")]
        public string GroupInput
        {
            get { return Validator.Get(this._groupUst, StringsConfig.SwitchSignals); }
            set { this._groupUst = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        
        [BindingProperty(1)]
        [XmlElement(ElementName = "Вход_сброса_индикаторов")]

        public string ClearIndicators
        {
            get { return Validator.Get(this._clrIndicators, StringsConfig.SwitchSignals); }
            set { this._clrIndicators = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
