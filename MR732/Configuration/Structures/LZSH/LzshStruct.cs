﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.LZSH
{
    public class LzshStruct : StructBase
    {
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _val;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_ЛЗШ")]
        public string Config
        {
            get { return Validator.Get(this._config, StringsConfig.LzshModes); }
            set { this._config = Validator.Set(value, StringsConfig.LzshModes); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка_ЛЗШ")]
        public double Value
        {
            get { return ValuesConverterCommon.GetUstavka40(this._val); }
            set { this._val = ValuesConverterCommon.SetUstavka40(value); }
        }
    }
}
