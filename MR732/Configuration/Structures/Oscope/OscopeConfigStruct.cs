﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.Oscope
{
    public class OscopeConfigStruct : StructBase
    {
        [Layout(0)] private ushort _config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort _size; //размер осциллограмы
        [Layout(2)] private ushort _percent; //процент от размера осциллограммы
        [Layout(3)] private OscopeAllChannelsStruct _allChannels;
        [Layout(4)] private ushort _inpOsc;
        [Layout(5, Count = 4)] private ushort[] _reserve;

        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return this._percent; }
            set { this._percent = value; }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string Fixation
        {
            get { return Validator.Get(this._config, StringsConfig.OscFixation, 0); }
            set { this._config = Validator.Set(value, StringsConfig.OscFixation, this._config, 0); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string Size
        {
            get { return Validator.Get((ushort)(this._size-1), StringsConfig.OscMode); }
            set { this._size = (ushort)(Validator.Set(value, StringsConfig.OscMode)+1); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Все_каналы")]
        public OscopeAllChannelsStruct AllChannels
        {
            get { return this._allChannels; }
            set { this._allChannels = value; }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Вход_пуска_осц")]
        public string InpOsc
        {
            get { return Validator.Get(this._inpOsc, StringsConfig.RelaySignals); }
            set { this._inpOsc = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        [XmlIgnore]
        public ushort[] ChannelsWords => this._allChannels.ChannelsWords;
    }
}
