﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.Oscope
{
    public class OscopeChannelStruct : StructBase
    {
        [Layout(0)] private ushort _channel;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string Channel
        {
            get { return Validator.Get(this._channel, StringsConfig.RelaySignals); }
            set { this._channel = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        [XmlIgnore]
        public ushort ChannelWord
        {
            get { return this._channel; }
        }
    }
}
