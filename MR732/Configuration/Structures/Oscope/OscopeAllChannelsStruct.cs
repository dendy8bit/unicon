﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Oscope
{
    public class OscopeAllChannelsStruct : StructBase, IDgvRowsContainer<OscopeChannelStruct>
    {
        public const int CHANNEL_COUNT = 8;

        [Layout(0, Count = CHANNEL_COUNT)] private OscopeChannelStruct[] _channels;
        
        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public OscopeChannelStruct[] Rows
        {
            get { return this._channels; }
            set { this._channels = value; }
        }

        [XmlIgnore]
        public ushort[] ChannelsWords => this._channels.Select(c => c.ChannelWord).ToArray();
    }
}
