﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.Corners
{
    public class CornersStruct : StructBase
    {
         #region [Fields]
        
        [Layout(0)] private ushort _cL1;
        [Layout(1)] private ushort _cnL1;
        [Layout(2)] private ushort _c0L1;
        [Layout(3)] private ushort _c2L1;
        [Layout(4)] private ushort _cL2;
        [Layout(5)] private ushort _cnL2;
        [Layout(6)] private ushort _c0L2;
        [Layout(7)] private ushort _c2L2;
        #endregion [Public fields]

        /// <summary>
        /// угол I для стороны 1
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "I_L1")]
        public ushort CL1
        {
            get { return this._cL1; }
            set { this._cL1 = value; }
        }

        /// <summary>
        /// угол для расчета по In для стороны 1
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "In_L1")]
        public ushort CnL1
        {
            get { return this._cnL1; }
            set { this._cnL1 = value; }
        }

        /// <summary>
        /// угол для расчета по I0 для стороны 1
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "I0_L1")]
        public ushort C0L1
        {
            get { return this._c0L1; }
            set { this._c0L1 = value; }
        }

        /// <summary>
        /// угол для расчета по I2 для стороны 1
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "I2_L1")]
        public ushort C2L1
        {
            get { return this._c2L1; }
            set { this._c2L1 = value; }
        }

        /// <summary>
        /// I для стороны 2
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "I_L2")]
        public ushort CL2
        {
            get { return this._cL2; }
            set { this._cL2 = value; }
        }

        /// <summary>
        /// угол для расчета по In для стороны 2
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "In_L2")]
        public ushort CnL2
        {
            get { return this._cnL2; }
            set { this._cnL2 = value; }
        }

        /// <summary>
        /// угол для расчета по I0 для стороны 2
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "I0_L2")]
        public ushort C0L2
        {
            get { return this._c0L2; }
            set { this._c0L2 = value; }
        }

        /// <summary>
        /// угол для расчета по I2 для стороны 2
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "I2_L2")]
        public ushort C2L2
        {
            get { return this._c2L2; }
            set { this._c2L2 = value; }
        }
    }
}
