﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.DifferencialCutoutDefense
{
    public class DifferencialCutoutDefenseStruct : StructBase
    {
        [Layout(0)] private ushort _config;                 //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;                //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;                  //вход блокировки
        [Layout(3)] private ushort _ust;                    //уставка срабатывания
        [Layout(4)] private ushort _time;                   //время срабатывания
        [Layout(5)] private ushort _rez;					//резерв

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseMode, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseMode, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Iд")]
        public double Id
        {
            get { return ValuesConverterCommon.GetUstavka40(this._ust); }
            set { this._ust = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Тср")]
        public int Tsr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Iд_мгн")]
        public bool Idmgn
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config1, StringsConfig.OscDefenceMode, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscDefenceMode, this._config1, 4, 5); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config1, 0); }
            set { this._config1 = Common.SetBit(this._config1, 0, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config1, 1); }
            set { this._config1 = Common.SetBit(this._config1, 1, value); }
        }
    }
}
