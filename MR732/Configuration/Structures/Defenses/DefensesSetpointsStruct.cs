﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR801DVG.Configuration.Structures.Defenses.Corners;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseF;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseP;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseQ;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseU;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DifferencialCutoutDefense;
using BEMN.MR801DVG.Configuration.Structures.Defenses.DifferencialDefense;
using BEMN.MR801DVG.Configuration.Structures.Defenses.External;
using BEMN.MR801DVG.Configuration.Structures.Defenses.MtzMain;
using BEMN.MR801DVG.Configuration.Structures.Defenses.MtzStar;
using BEMN.MR801DVG.Configuration.Structures.Defenses.Termblock;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses
{
    /// <summary>
    /// список защит
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Все_защиты")]
    public class DefensesSetpointsStruct : StructBase
    {
        [Layout(0)] private CornersStruct _corner;
        [Layout(1)] private DifferencialDefenseStruct _differencialDefense;     
        [Layout(2)] private DifferencialCutoutDefenseStruct _differencialCutoutDefense;
        [Layout(3)] private AllDefensePStruct _allDefenseP;
        [Layout(4, Count = 6)] private ushort[] _reserve;
        [Layout(5)] private AllMtzMainStruct _allMtzMain;
        [Layout(6)] private AllMtzI21 _mtzI21;
        [Layout(7)] private AllMtzStarStruct _allMtzStar;
        [Layout(8)] private AllDefensesUStruct _allDefensesU;
        [Layout(9)] private AllDefenseFStruct _allDefensesF;
        [Layout(10)] private AllDefenseQStruct _allDefenseQ;
        [Layout(11)] private TermblockStruct _termblock;
        [Layout(12)] private AllExternalDefenseStruct _externalDefenses;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public CornersStruct Corner
        {
            get { return this._corner; }
            set { this._corner = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Диф_защиты")]
        public DifferencialDefenseStruct Defense
        {
            get { return this._differencialDefense; }
            set { this._differencialDefense = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Диф_защиты_отсечки")]
        public DifferencialCutoutDefenseStruct CutoutDefense
        {
            get { return this._differencialCutoutDefense; }
            set { this._differencialCutoutDefense = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Защиты_мощности")]
        public AllDefensePStruct AllDefenseP
        {
            get { return this._allDefenseP; }
            set { this._allDefenseP = value; }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Токовые")]
        public AllMtzMainStruct AllMtzMain
        {
            get { return this._allMtzMain; }
            set { this._allMtzMain = value; }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "Защита_I21")]
        public AllMtzI21 I21
        {
            get { return this._mtzI21; }
            set { this._mtzI21 = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "Токовые_нул")]
        public AllMtzStarStruct AllMtzStar
        {
            get { return this._allMtzStar; }
            set { this._allMtzStar = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Защиты_U")]
        public AllDefensesUStruct AllDefensesU
        {
            get { return this._allDefensesU; }
            set { this._allDefensesU = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Защиты_F")]
        public AllDefenseFStruct AllDefensesF
        {
            get { return this._allDefensesF; }
            set { this._allDefensesF = value; }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Защиты_Q")]
        public AllDefenseQStruct AllDefensesQ
        {
            get { return this._allDefenseQ; }
            set { this._allDefenseQ = value; }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Термозащита")]
        public TermblockStruct Termblock
        {
            get { return this._termblock; }
            set { this._termblock = value; }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefenseStruct ExternalDefenses
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
