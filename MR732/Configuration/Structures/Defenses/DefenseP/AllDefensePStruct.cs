﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseP
{
    public class AllDefensePStruct : StructBase, IDgvRowsContainer<DefensePStruct>
    {
        public const int COUNT = 2;
        [Layout(0, Count = COUNT)] DefensePStruct[] _differencialDefenses;

        [XmlElement(ElementName = "Все_защиты_мощности")]
        public DefensePStruct[] Rows
        {
            get { return this._differencialDefenses; }
            set { this._differencialDefenses = value; }
        }
    }
}
