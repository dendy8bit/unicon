﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseP
{
    public class DefensePStruct : StructBase
    {
        [Layout(0)] private ushort _config;             // конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;            // конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;              // вход блокировки
        [Layout(3)] private ushort _ust1;               // уставка срабатывания
        [Layout(4)] private ushort _time1;              // выдержка времени
        [Layout(5)] private ushort _corner1;            // угол срабатывания
        [Layout(6)] private ushort _ust2;               // уставка возврата
        [Layout(7)] private ushort _time2;              // время возврата
        [Layout(8)] private ushort _ustI;               // Iср в Iном
        [Layout(9)] private ushort _tu;                 // время ускорения (резерв)
        [Layout(10)] private ushort _usk;				// вход ускорения (резерв)
        [Layout(11)] private ushort _reserve;		    // резерв	

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseMode, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseMode, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public double Ssr
        {
            get { return Math.Round(2.5*(short)this._ust1 / 32767, 2); }
            set { this._ust1 = (ushort)(value/2.5*32767); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Угол_срабатывания")]
        public ushort Corner1
        {
            get { return this._corner1; }
            set { this._corner1 = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Тср")]
        public int Tsr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time1); }
            set { this._time1 = ValuesConverterCommon.SetWaitTime(value); }
        }
        
        [BindingProperty(4)]
        [XmlElement(ElementName = "Возврат")]
        public bool Vozvrat
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Уставка_возврата")]
        public double Svz
        {
            get { return Math.Round(2.5 * (short)this._ust2 / 32767, 2); }
            set { this._ust2 = (ushort)(value / 2.5 * 32767); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Твз")]
        public int Tvz
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time2); }
            set { this._time2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Iср")]
        public double Isr
        {
            get { return ValuesConverterCommon.GetUstavka40(this._ustI); }
            set { this._ustI = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config1, StringsConfig.OscDefenceMode, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscDefenceMode, this._config1, 4, 5); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Апв_возврат")]
        public bool ApvBack
        {
            get { return Common.GetBit(this._config1, 2); }
            set { this._config1 = Common.SetBit(this._config1, 2, value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config1, 0); }
            set { this._config1 = Common.SetBit(this._config1, 0, value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config1, 1); }
            set { this._config1 = Common.SetBit(this._config1, 1, value); }
        }
    }
}
