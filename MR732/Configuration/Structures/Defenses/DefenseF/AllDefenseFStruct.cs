﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseF
{
    public class AllDefenseFStruct : StructBase, IDgvRowsContainer<DefenseFStruct>
    {
        [Layout(0, Count = 8)]
        private DefenseFStruct[] _f;

        [XmlArray(ElementName = "Все_защиты_F")]
        public DefenseFStruct[] Rows
        {
            get { return this._f; }
            set { this._f = value; }
        }
    }
}
