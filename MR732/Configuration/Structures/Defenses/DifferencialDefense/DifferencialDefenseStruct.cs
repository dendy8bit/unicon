﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.DifferencialDefense
{
    public class DifferencialDefenseStruct : StructBase
    {
        [Layout(0)] private ushort config;              // конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort config1;             // конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort block;               // вход блокировки
        [Layout(3)] private ushort ust;                 // уставка срабатывания
        [Layout(4)] private ushort time;                // выдержка времени
        //характеристика торможения_
        [Layout(5)] private ushort ib1;                 // начало  1
        [Layout(6)] private ushort k1;                  // тангенс 1
        [Layout(7)] private ushort ib2;                 // начало  2
        [Layout(8)] private ushort k2;                  // тангенс 2
        //торможение
        [Layout(9)] private ushort I21;                 // %
        [Layout(10)] private ushort I51;                 // %
        [Layout(11)] private ushort _rez;				// резерв	


        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this.config, StringsConfig.DefenseMode, 0,1); }
            set { this.config = Validator.Set(value, StringsConfig.DefenseMode, this.config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this.block, StringsConfig.SwitchSignals); }
            set { this.block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Iд")]
        public double Id
        {
            get { return ValuesConverterCommon.GetUstavka40(this.ust); }
            set { this.ust = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Тср")]
        public int Tsr
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "I2/I1")]
        public bool I2I1
        {
            get { return Common.GetBit(this.config, 10); }
            set { this.config = Common.SetBit(this.config, 10, value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Перекр_блок_I2/I1")]
        public bool CrossBlockI2I1
        {
            get { return Common.GetBit(this.config, 7); }
            set { this.config = Common.SetBit(this.config, 7, value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Уставка_I2/I1")]
        public ushort UstavkaI2I1
        {
            get { return this.I21; }
            set { this.I21 = value; }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "I5/I1")]
        public bool I5I1
        {
            get { return Common.GetBit(this.config, 9); }
            set { this.config = Common.SetBit(this.config, 9, value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Перекр_блок_I5/I1")]
        public bool CrossBlockI5I1
        {
            get { return Common.GetBit(this.config, 6); }
            set { this.config = Common.SetBit(this.config, 6, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Уставка_I5/I1")]
        public ushort UstavkaI5I1
        {
            get { return this.I51; }
            set { this.I51 = value; }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Iб1")]
        public double Ib1
        {
            get { return ValuesConverterCommon.GetUstavka40(this.ib1); }
            set { this.ib1 = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "K1")]
        public ushort K1
        {
            get { return this.k1; }
            set { this.k1 = value; }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Iб2")]
        public double Ib2
        {
            get { return ValuesConverterCommon.GetUstavka40(this.ib2); }
            set { this.ib2 = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "K2")]
        public ushort K2
        {
            get { return this.k2; }
            set { this.k2 = value; }
        }

        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this.config1, StringsConfig.OscDefenceMode, 4, 5); }
            set { this.config1 = Validator.Set(value, StringsConfig.OscDefenceMode, this.config1, 4, 5); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this.config, 2); }
            set { this.config = Common.SetBit(this.config, 2, value); }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this.config1, 0); }
            set { this.config1 = Common.SetBit(this.config1, 0, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(17)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this.config1, 1); }
            set { this.config1 = Common.SetBit(this.config1, 1, value); }
        }
    }
}
