﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.MtzMain
{
    public class AllMtzI21 : StructBase, IDgvRowsContainer<MtzI21>
    {
        [Layout(0)] private MtzI21 _mtzmain;

        [XmlArray(ElementName = "Все_I2/I1")]
        public MtzI21[] Rows
        {
            get { return new [] { this._mtzmain}; }
            set { this._mtzmain = value[0]; }
        }
    }
}
