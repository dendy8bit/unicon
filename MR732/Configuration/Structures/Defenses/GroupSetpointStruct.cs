﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses
{
    public class GroupSetpointStruct : StructBase, ISetpointContainer<DefensesSetpointsStruct>
    {
        #region [Public fields]
        [Layout(0)] private DefensesSetpointsStruct _mainSetpoints;
        [Layout(1)] private DefensesSetpointsStruct _reserveSetpoints;
        #endregion [Public fields]
        [XmlIgnore]
        public DefensesSetpointsStruct[] Setpoints
        {
            get
            {
                return new[]
                {
                    this._mainSetpoints.Clone<DefensesSetpointsStruct>(),
                    this._reserveSetpoints.Clone<DefensesSetpointsStruct>()
                };
            }
            set
            {
                this._mainSetpoints = value[0].Clone<DefensesSetpointsStruct>();
                this._reserveSetpoints = value[1].Clone<DefensesSetpointsStruct>();
            }
        }
        /// <summary>
        /// Основная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Основная_группа_уставок")]
        public DefensesSetpointsStruct MainSetpoints
        {
            get { return this._mainSetpoints; }
            set { _mainSetpoints = value; }
        }

        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Резервная_группа_уставок")]
        public DefensesSetpointsStruct ReserveSetpoints
        {
            get { return this._reserveSetpoints; }
            set { _reserveSetpoints = value; }
        }
        /*
        public const int GROUP_COUNT = 2;
        [Layout(0, Count = GROUP_COUNT)] private DefensesSetpointsStruct[] _groupSetpoints;

        [XmlElement(ElementName = "Все_уставки_защит")]
        public DefensesSetpointsStruct[] Setpoints
        {
            get
            {
                DefensesSetpointsStruct[] res = new DefensesSetpointsStruct[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<DefensesSetpointsStruct>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
        */
    }
}
