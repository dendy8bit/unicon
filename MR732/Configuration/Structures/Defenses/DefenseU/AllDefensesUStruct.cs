﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.DefenseU
{
    public class AllDefensesUStruct : StructBase, IDgvRowsContainer<DefenseUStruct>
    {
        [Layout(0, Count = 8)] private DefenseUStruct[] _u; //мтз U

        [XmlArray(ElementName = "Все_защиты_U")]
        public DefenseUStruct[] Rows
        {
            get { return this._u; }
            set { this._u = value; }
        }
    }
}
