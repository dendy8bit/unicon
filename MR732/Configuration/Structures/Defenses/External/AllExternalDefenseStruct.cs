﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.External
{
    public class AllExternalDefenseStruct: StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        public const int COUNT = 16;
        [Layout(0, Count = COUNT)] private ExternalDefenseStruct[] _ext; 

        [XmlArray(ElementName = "Все_защиты_внешние")]
        public ExternalDefenseStruct[] Rows
        {
            get { return this._ext; }
            set { this._ext = value; }
        }
    }
}
