﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.MtzStar
{
    public class AllMtzStarStruct : StructBase, IDgvRowsContainer<MtzStarStruct>
    {
        public const int COUNT = 6;
        [Layout(1, Count = COUNT)] private MtzStarStruct[] _mtzmain;

        [XmlArray(ElementName = "Все_МТЗ_I*")]
        public MtzStarStruct[] Rows
        {
            get { return this._mtzmain; }
            set { this._mtzmain = value; }
        }
    }
}
