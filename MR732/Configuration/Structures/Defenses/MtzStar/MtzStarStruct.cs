﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.Defenses.MtzStar
{
    public class MtzStarStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1; //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block; //вход блокировки
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _k; //коэфиц. зависимой хар-ки
        [Layout(6)] private ushort _u; //уставка пуска по напряжению
        [Layout(7)] private ushort _tu; //время ускорения_
        [Layout(8)] private ushort _i21; // уставка в %
        [Layout(9)] private ushort _usk; //резерв 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseMode, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseMode, this._config, 0, 1); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Сторона")]
        public string Side
        {
            get { return Validator.Get(this._config, StringsConfig.Binding, 4); }
            set { this._config = Validator.Set(value, StringsConfig.Binding, this._config, 4); }
        }

        /// <summary>
        /// Uпуск (есть/нет)
        /// </summary>
        [XmlElement(ElementName = "Пуск_по_U")]
        [BindingProperty(3)]
        public bool UStartBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Uпуск_уставка")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// Направление
        /// </summary>
        [XmlElement(ElementName = "Направление")]
        [BindingProperty(5)]
        public string Direction
        {
            get { return Validator.Get(this._config, StringsConfig.Direction, 6, 7); }
            set { this._config = Validator.Set(value, StringsConfig.Direction, this._config, 6, 7); }
        }

        /// <summary>
        /// Недост. напр(I*)
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Недост_напр")]
        public string Undir
        {
            get { return Validator.Get(this._config, StringsConfig.Undir, 8); }
            set { this._config = Validator.Set(value, StringsConfig.Undir, this._config, 8); }
        }

        /// <summary>
        /// I*
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "I*")]
        public string Istar
        {
            get { return Validator.Get(this._config, StringsConfig.Istar, 14, 15); }
            set { this._config = Validator.Set(value, StringsConfig.Istar, this._config, 14, 15); }
        }

        /// <summary>
        /// Характеристика
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Характеристика")]
        public string Characteristic
        {
            get { return Validator.Get(this._config, StringsConfig.Characteristic, 12); }
            set { this._config = Validator.Set(value, StringsConfig.Characteristic, this._config, 12); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// K
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "K")]
        public ushort K
        {
            get { return this._k; }
            set { this._k = value; }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config1, StringsConfig.OscDefenceMode, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscDefenceMode, this._config1, 4, 5); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Ускорение")]
        public bool Uskor
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "tу")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config1, 0); }
            set { this._config1 = Common.SetBit(this._config1, 0, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(17)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config1, 1); }
            set { this._config1 = Common.SetBit(this._config1, 1, value); }
        }
        #endregion [Properties]
    }
}
