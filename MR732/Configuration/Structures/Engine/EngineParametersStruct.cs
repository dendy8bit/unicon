﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.Engine
{
    public class EngineParametersStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _timeHot;   //постоянная времени нагрева
        [Layout(2)] private ushort _timeCold;  //постоянная времени охлаждения
        [Layout(3)] private ushort _iDv;       //Iдв
        [Layout(4)] private ushort _iStart;
        [Layout(5)] private ushort _tStart;
        [Layout(6)] private ushort _q;
        [Layout(7)] private ushort _resetQ;     //вход Q сброса 
        [Layout(8)] private ushort _resetN;     //вход N сброса
        [Layout(9)] private ushort _tDlit;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// постоянная времени нагрева
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Tгор")]
        public ushort TimeHot
        {
            get { return this._timeHot; }
            set { this._timeHot = value; }
        }
        /// <summary>
        /// постоянная времени охлаждения
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Tхол")]
        public ushort TimeCold
        {
            get { return this._timeCold; }
            set { this._timeCold = value; }
        }
        /// <summary>
        /// Ток двигателя
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Iдв")]
        public double Idv
        {
            get { return ValuesConverterCommon.GetIn(this._iDv); }
            set { this._iDv = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Ток пуска
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Iпуск")]
        public double Istart
        {
            get { return ValuesConverterCommon.GetIn(this._iStart); }
            set { this._iStart = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Время пуска двигателя
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Tпуск")]
        public int TimeStart
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tStart); }
            set { this._tStart = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Уставка Q
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Q")]
        public double Q
        {
            get { return ValuesConverterCommon.GetUstavka256(this._q); }
            set { this._q = ValuesConverterCommon.SetUstavka256(value); }
        }
        /// <summary>
        /// Q сброс
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Q_сброс")]
        public string Qreset
        {
            get { return Validator.Get(this._resetQ, StringsConfig.SwitchSignals); }
            set { this._resetQ = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        /// <summary>
        /// N сброс
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "N_сброс")]
        public string Nreset
        {
            get { return Validator.Get(this._resetN, StringsConfig.SwitchSignals); }
            set { this._resetN = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        /// <summary>
        /// время длительности
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Tдлит")]
        public ushort Time
        {
            get { return this._tDlit; }
            set { this._tDlit = value; }
        }
        #endregion
    }
}
