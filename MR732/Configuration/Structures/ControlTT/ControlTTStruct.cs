﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.ControlTT
{
    public class ControlTTStruct : StructBase
    {
        [Layout(0)] private ushort _config;                //  выведена, неисправность, неисправность + блокировка
        [Layout(1)] private ushort _ust;                   //  уставка минимального дифференциального тока
        [Layout(2)] private ushort _time;                  //  выдержка времени
        [Layout(3)] private ushort _inpRes;                //  вход внешнего входа сброса неисправности

        [BindingProperty(0)]
        [XmlElement(ElementName = "Iдmin")]
        public double Idmin
        {
            get { return ValuesConverterCommon.GetUstavka40(this._ust); }
            set { this._ust = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Время_сраб")]
        public int Tsr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Режим")]
        public string Neipr
        {
            get { return Validator.Get(this._config, StringsConfig.ControlTtNeipr, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.ControlTtNeipr, this._config, 0, 1); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Сброс_неисправности_ТТ")]
        public string Reset
        {
            get { return Validator.Get(this._inpRes, StringsConfig.SwitchSignals); }
            set { this._inpRes = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
