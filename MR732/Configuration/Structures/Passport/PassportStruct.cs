﻿using System;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.Passport
{
    public class PassportStruct : StructBase
    {
        [Layout(0)] private ushort _pow;
        [Layout(1)] private ushort _cos;
        [Layout(2)] private ushort _kpd;
        [Layout(3)] private ushort _res;

        [BindingProperty(0)]
        public double Power
        {
            get
            {
                ushort val = Common.GetBits(this._pow, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                return Math.Round(128.0*val/32767, 2);
            }
            set
            {
                ushort val = (ushort)(value/128*32767);
                this._pow = Common.SetBits(this._pow, val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        [BindingProperty(1)]
        public string Pwatt
        {
            get { return Validator.Get(this._pow, StringsConfig.Pwatt, 15); }
            set { this._pow = Validator.Set(value, StringsConfig.Pwatt, this._pow, 15); }
        }

        [BindingProperty(2)]
        public double Cos
        {
            get {
                if (this._cos >= 10) {
                    return Convert.ToDouble($"0.{this._cos}", CultureInfo.InvariantCulture);
                }
                else
                {
                    return Convert.ToDouble($"0.0{this._cos}", CultureInfo.InvariantCulture);
                }
            }  
            set
            {
                string cos = value.ToString("F2");
                this._cos = cos.Contains(".") || cos.Contains(",") ? ushort.Parse(cos.Split('.', ',')[1]) : (ushort)0;    
            }
        }

        [BindingProperty(3)]
        public ushort Kpd
        {
            get { return this._kpd; }
            set { this._kpd = value; }
        }
    }
}
