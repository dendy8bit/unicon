﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.MeasureTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора U
    /// </summary>
    public class KanalUTransStruct : StructBase
    {
        #region [Private fields]


        [Layout(0)] private ushort _kttl; //коэффициент
        [Layout(1)] private ushort _kttx; //номинальный первичный ток нулевой последовательности
        [Layout(3)] private ushort _polarityL; //вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX; //вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //для трансформатора напряжения тип ТН
        [Layout(6)] private ushort _imax; //есть или не компенсация токов нулевой последовательности 0-нет 1-есть

        #endregion [Private fields]

        #region [U (ТН)]

        /// <summary>
        /// тип_Uo
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "тип_Uo")]
        public string UtypeXml
        {
            get { return Validator.Get(this._binding, StringsConfig.UoType); }
            set { this._binding = Validator.Set(value, StringsConfig.UoType); }
        }

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(1)]
        [XmlIgnore]
        public double Kthl
        {
            get { return ValuesConverterCommon.GetKth(this._kttl); }
            set { this._kttl = ValuesConverterCommon.SetKth(value); }
        }

        /// <summary>
        /// KTHX
        /// </summary>
        [BindingProperty(2)]
        [XmlIgnore]
        public double Kthx
        {
            get { return ValuesConverterCommon.GetKth(this._kttx); }
            set { this._kttx = ValuesConverterCommon.SetKth(value); }
        }

        /// <summary>
        /// KTHL коэффициент
        /// </summary>
        [BindingProperty(3)]
        [XmlIgnore]
        public string Lkoef
        {
            get { return Validator.Get(this._kttl, StringsConfig.KthKoefs, 15); }
            set { this._kttl = Validator.Set(value, StringsConfig.KthKoefs, this._kttl, 15); }
        }

        /// <summary>
        /// KTHX коэффициент
        /// </summary>
        [BindingProperty(4)]
        [XmlIgnore]
        public string Xkoef
        {
            get { return Validator.Get(this._kttx, StringsConfig.KthKoefs, 15); }
            set { this._kttx = Validator.Set(value, StringsConfig.KthKoefs, this._kttx, 15); }
        }

        /// <summary>
        /// Неисправность L
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Неисправность_L")]
        public string FaultL
        {
            get { return Validator.Get(this._polarityL, StringsConfig.SwitchSignals); }
            set { this._polarityL = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Неисправность X
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Неисправность_X")]
        public string FaultX
        {
            get { return Validator.Get(this._polarityX, StringsConfig.SwitchSignals); }
            set { this._polarityX = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        [XmlElement(ElementName = "KTHL")]
        public double KthlValue
        {
            get
            {
                double value = ValuesConverterCommon.GetKth(this._kttl);
                return Common.GetBit(this._kttl, 15)
                    ? value*1000
                    : value;
            }
            set { }
        }
        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        [XmlElement(ElementName = "KTHX")]
        public double KthxValue
        {
            get
            {
                double value = ValuesConverterCommon.GetKth(this._kttx);
                return Common.GetBit(this._kttx, 15)
                    ? value*1000
                    : value;
            }
            set { }
        }
        #endregion [U (ТН)] 
    }
}
