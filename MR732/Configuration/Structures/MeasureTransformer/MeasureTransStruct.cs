﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR801DVG.Configuration.Structures.MeasureTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        #region [Public field]

        [Layout(0)] private KanalTransStruct _l1; //канал I cторона 1 
        [Layout(1)] private KanalTransStruct _l2; //канал I cторона 2
        [Layout(2)] private KanalUTransStruct _u; //канал U защиты 

        #endregion [Public field]
        /// <summary>
        /// Канал L1
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Канал_L1")]
        public KanalTransStruct ChannelL1
        {
            get { return this._l1; }
            set { this._l1 = value; }
        }
        /// <summary>
        /// Канал L2
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Канал_L2")]
        public KanalTransStruct ChannelL2
        {
            get { return this._l2; }
            set { this._l2 = value; }
        }

        /// <summary>
        /// Канал U
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Канал_U")]
        public KanalUTransStruct ChannelU
        {
            get { return this._u; }
            set { this._u = value; }
        }
    }
}
