﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR732.Configuration.Structures.MeasureTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора I
    /// </summary>
    public class KanalTransStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _kttl; //номинальный первичный ток
        [Layout(1)] private ushort _kttx; //номинальный первичный ток нулевой последовательности
        [Layout(3)] private ushort _polarityL; //полярность подключения(Polarity of connection), вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX; //полярность подключения(Polarity of connection) нулевой последовательности, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //привязка (Binding)(для трансформатора напряжения тип ТН)
        [Layout(6)] private ushort _imax; //есть или не компенсация токов нулевой последовательности 0-нет 1-есть

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// тип ТТ
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "тип_ТТ")]
        public string ItypeXml
        {
            get { return Validator.Get(this._binding, StringsConfig.TtType); }
            set { this._binding = Validator.Set(value, StringsConfig.TtType); }
        }

        /// <summary>
        /// Iм
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Iм")]
        public double Im
        {
            get { return ValuesConverterCommon.GetIn(this._imax); }
            set { this._imax = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public double Ittl
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._kttl);
                return Common.GetBit(this._kttl, 15)
                    ? value * 1000
                    : value;
            }
            
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        public double Ittx
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._kttx);
                return Common.GetBit(this._kttx, 15)
                    ? value * 1000
                    : value;
            }
            
        }

        #endregion [Properties]
    }
}
