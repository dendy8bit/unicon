﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR801DVG.Configuration.Structures.MeasureTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора I
    /// </summary>
    public class KanalTransStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _ittl; //номинальный первичный ток
        [Layout(1)] private ushort _ittx; //номинальный первичный ток нулевой последовательности
        [Layout(3)] private ushort _polarityL; //полярность подключения(Polarity of connection), вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX; //полярность подключения(Polarity of connection) нулевой последовательности, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //привязка (Binding)
        [Layout(6)] private ushort _i0Correction; //есть или нет компенсации токов нулевой последовательности (0-нет 1-есть)

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public ushort Ittl
        {
            get { return this._ittl; }
            set { this._ittl = value; }
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        public ushort Ittx
        {
            get { return this._ittx; }
            set { this._ittx = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Полярность_L")]
        public string PolarityL
        {
            get { return Validator.Get(this._polarityL, StringsConfig.Polarity); }
            set { this._polarityL = Validator.Set(value, StringsConfig.Polarity); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Полярность_X")]
        public string PolarityX
        {
            get { return Validator.Get(this._polarityX, StringsConfig.Polarity); }
            set { this._polarityX = Validator.Set(value, StringsConfig.Polarity); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Коррекция_I0")]
        public string Correction
        {
            get { return Validator.Get(this._i0Correction, StringsConfig.BeNo); }
            set { this._i0Correction = Validator.Set(value, StringsConfig.BeNo); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Тип_ТТ")]
        public string TypeTT
        {
            get { return Validator.Get(this._binding, StringsConfig.TransformatorType, 0); }
            set { this._binding = Validator.Set(value, StringsConfig.TransformatorType, this._binding, 0); }
        }

        #endregion [Properties]
    }
}
