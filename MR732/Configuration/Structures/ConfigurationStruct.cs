﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR801DVG.Configuration.Structures.APV;
using BEMN.MR801DVG.Configuration.Structures.AVR;
using BEMN.MR801DVG.Configuration.Structures.ControlTT;
using BEMN.MR801DVG.Configuration.Structures.Defenses;
using BEMN.MR801DVG.Configuration.Structures.Engine;
using BEMN.MR801DVG.Configuration.Structures.InputLogicSignals;
using BEMN.MR801DVG.Configuration.Structures.InputSignals;
using BEMN.MR801DVG.Configuration.Structures.LZSH;
using BEMN.MR801DVG.Configuration.Structures.MeasureTransformer;
using BEMN.MR801DVG.Configuration.Structures.Oscope;
using BEMN.MR801DVG.Configuration.Structures.OutputLogicSignals;
using BEMN.MR801DVG.Configuration.Structures.Passport;
using BEMN.MR801DVG.Configuration.Structures.RelayInd;
using BEMN.MR801DVG.Configuration.Structures.Switch;

namespace BEMN.MR801DVG.Configuration.Structures
{
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "MR801DVG")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType
        {
            get { return "МР801двг"; }
            set { }
        }

        [XmlElement(ElementName = "EngineSn")] 
        public string EngineSn { get; set; }
        [XmlElement(ElementName = "OscCount")]
        public string OscCount { get; set; }
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [Layout(0)] private SwitchStruct _sw;
        [Layout(1)] private ApvStruct _apv;
        [Layout(2)] private AvrStruct _avr;
        [Layout(3)] private LzshStruct _lzsh;
        [Layout(4)] private EngineParametersStruct _engine;
        [Layout(5)] private InputSignalsStruct _inputSignals;
        [Layout(6)] private OscopeConfigStruct _oscopeConfig;
        [Layout(7)] private MeasureTransStruct _measureTrans;
        [Layout(8)] private AllInputLogicSignalsStruct _allInputLogicSignals;
        [Layout(9)] private AllOutputLogicSignalsStruct _allOutputLogicSignals;
        [Layout(10)] private GroupSetpointStruct _setpoint;
        [Layout(11)] private AutomaticsParametersStruct _automatics;
        [Layout(12, Count = 116, Ignore = true)] private ushort[] _congigSystem;
        [Layout(13)] private ControlTTStruct _controlTT;
        [Layout(14)] private PassportStruct _passport;

        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(0)]
        public SwitchStruct Switch
        {
            get { return this._sw; }
            set { this._sw = value; }
        }

        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "АПВ")]
        [BindingProperty(1)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }

        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "АВР")]
        [BindingProperty(2)]
        public AvrStruct Avr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }

        /// <summary>
        /// ЛЗШ
        /// </summary>
        [XmlElement(ElementName = "ЛЗШ")]
        [BindingProperty(3)]
        public LzshStruct Lzsh
        {
            get { return this._lzsh; }
            set { this._lzsh = value; }
        }
        
        /// <summary>
        /// Параметры двигателя
        /// </summary>
        [XmlElement(ElementName = "Двигатель")]
        [BindingProperty(4)]
        public EngineParametersStruct Engine
        {
            get { return this._engine; }
            set { this._engine = value; }
        }

        /// <summary>
        /// Входные сигналы
        /// </summary>
        [XmlElement(ElementName = "Входные_сигналы")]
        [BindingProperty(5)]
        public InputSignalsStruct InputSignals
        {
            get { return this._inputSignals; }
            set { this._inputSignals = value; }
        }
        /// <summary>
        /// Конфигурация осциллографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осциллографа")]
        [BindingProperty(6)]
        public OscopeConfigStruct OscpConfig
        {
            get { return this._oscopeConfig; }
            set { this._oscopeConfig = value; }
        }

        /// <summary>
        /// Конфигурация осциллографа
        /// </summary>
        [XmlElement(ElementName = "Измерительный_трансформатор")]
        [BindingProperty(7)]
        public MeasureTransStruct MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }

        /// <summary>
        /// Входные логические сигналы
        /// </summary>
        [XmlElement(ElementName = "ЛС")]
        [BindingProperty(8)]
        public AllInputLogicSignalsStruct AllInputLogic
        {
            get { return this._allInputLogicSignals; }
            set { this._allInputLogicSignals = value; }
        }
        
        /// <summary>
        /// Выходные логические сигналы
        /// </summary>
        [XmlElement(ElementName = "ВЛС")]
        [BindingProperty(9)]
        public AllOutputLogicSignalsStruct AllOutputLogic
        {
            get { return this._allOutputLogicSignals; }
            set { this._allOutputLogicSignals = value; }
        }

        [XmlElement(ElementName = "Уставки_защит")]
        [BindingProperty(10)]
        public GroupSetpointStruct Setpoint
        {
            get { return this._setpoint; }
            set { this._setpoint = value; }
        }

        [XmlElement(ElementName = "Параметры_автоматики")]
        [BindingProperty(11)]
        public AutomaticsParametersStruct Automatics
        {
            get { return this._automatics; }
            set { this._automatics = value; }
        }

        [XmlElement(ElementName = "Контроль_ТТ")]
        [BindingProperty(12)]
        public ControlTTStruct ControlTT
        {
            get { return this._controlTT; }
            set { this._controlTT = value; }
        }
        
        [XmlElement(ElementName = "Паспорт_двигателя")]
        [BindingProperty(13)]
        public PassportStruct Passport
        {
            get { return this._passport; }
            set { this._passport = value; }
        }
    }
}
