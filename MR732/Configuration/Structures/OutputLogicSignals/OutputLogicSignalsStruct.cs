﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.MR801DVG.Configuration.Structures.OutputLogicSignals
{
    [XmlRoot(ElementName = "Конфигурация_одного_ВЛС")]
    public class OutputLogicSignalsStruct : StructBase, IBitField, IXmlSerializable
    {
        #region [Constants]
        public const int LOGIC_COUNT = 16;
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = LOGIC_COUNT)] private ushort[] _logicSignals;
        #endregion [Private fields]


        #region [Properties]
        [XmlIgnore]
        public BitArray Bits
        {
            get
            {
                var mass = Common.TOBYTES(this._logicSignals, false);
                var result = new BitArray(mass);
                return result;
            }

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    int x = i / 16;
                    int y = i % 16;
                    this._logicSignals[x] = Common.SetBit(this._logicSignals[x], y, value[i]);
                }
            }
        }
        [XmlIgnore]
        public string[] BitsXml
        {
            get
            {

                List<string> res = new List<string>();
                var bits = this.Bits;
                for (int i = 0; i < bits.Count; i++)
                {
                    if (bits[i])
                    {
                        res.Add(StringsConfig.VlsSignals[i]);
                    }

                }
                if (res.Count == 0)
                {
                    res.Add("НЕТ");
                }

                return res.ToArray();
            }
            set
            {

            }
        }
        #endregion [Properties]

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            bool flag = false;
            for (int i = 0; i < StringsConfig.VlsSignals.Count; i++)
            {
                if (this.Bits[i])
                {
                    flag = true;
                    writer.WriteElementString("Выбрано", StringsConfig.VlsSignals[i]);
                }

            }
            if (!flag)
            {
                writer.WriteElementString("Выбрано", "НЕТ");
            }
        }
    }
}
