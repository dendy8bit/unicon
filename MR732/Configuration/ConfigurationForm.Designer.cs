﻿namespace BEMN.MR801DVG.Configuration
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle118 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle119 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle120 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle121 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle122 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle123 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle124 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle125 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle126 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle127 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle128 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle129 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle130 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle131 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle132 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle133 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle134 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle135 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle136 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle137 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle138 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle139 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle140 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle141 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle142 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle143 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle144 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle145 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle146 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle147 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle148 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle149 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle150 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label129 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._diffCutoutOsc = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this._diffCutoutUROV = new System.Windows.Forms.CheckBox();
            this._diffCutoutAVR = new System.Windows.Forms.CheckBox();
            this._diffCutoutAPV = new System.Windows.Forms.CheckBox();
            this._diffCutoutMgn = new System.Windows.Forms.CheckBox();
            this._timeEnduranceDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this._constraintDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this._blockingDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._modeDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.label56 = new System.Windows.Forms.Label();
            this._K2TangensTextBox = new System.Windows.Forms.MaskedTextBox();
            this._Ib2BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this._modeDTZComboBox = new System.Windows.Forms.ComboBox();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this._difensesIstarDataGrid = new System.Windows.Forms.DataGridView();
            this._i0StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0ModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0IColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0InColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0UsYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0UstartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0DirColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0UndirColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0I0Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0CharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0TColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0kColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0OscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0TyBoolColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0TyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0UROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0APVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0AVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label59 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.VLScheckedListBox12 = new System.Windows.Forms.CheckedListBox();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this._defenseI2I1DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn37 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn43 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn44 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._defenseI7DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn29 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn30 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn31 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn32 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn33 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn34 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn35 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn36 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._defensesI56DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn21 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn22 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn23 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn24 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn25 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn26 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn27 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn28 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._defensesI14DataGrid = new System.Windows.Forms.DataGridView();
            this._iStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iIColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iInColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUstartYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iUStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUnDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iLogicColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iCharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iTColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iKColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iOscModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iTyBoolColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iTyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iUROVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iAPVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iAVRModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._allDefensesPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._copySetpointsBtn = new System.Windows.Forms.Button();
            this._mainRadioButton = new System.Windows.Forms.RadioButton();
            this._reserveRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._difensesTC = new System.Windows.Forms.TabControl();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._cIL2 = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._cInL2 = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._cI0L2 = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._cI2L2 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._cI2L1 = new System.Windows.Forms.MaskedTextBox();
            this._cI0L1 = new System.Windows.Forms.MaskedTextBox();
            this._cInL1 = new System.Windows.Forms.MaskedTextBox();
            this._cIL1 = new System.Windows.Forms.MaskedTextBox();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._diffAVR = new System.Windows.Forms.CheckBox();
            this._DiffAPV = new System.Windows.Forms.CheckBox();
            this._diffUROV = new System.Windows.Forms.CheckBox();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._crossBlockI5I1 = new System.Windows.Forms.CheckBox();
            this._modeI5I1 = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this._I5I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label75 = new System.Windows.Forms.Label();
            this._oscDTZComboBox = new System.Windows.Forms.ComboBox();
            this._blockingDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this._timeEnduranceDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._crossBlockI2I1 = new System.Windows.Forms.CheckBox();
            this._modeI2I1 = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this._I2I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label73 = new System.Windows.Forms.Label();
            this._constraintDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._K1AngleOfSlopeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._Ib1BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this._defPdgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn52 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn24 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn25 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn26 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._defensesUMDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn38 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn39 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn40 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn41 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn20 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this._defensesUBDataGrid = new System.Windows.Forms.DataGridView();
            this._uBStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBBlockingUMColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage26 = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._difensesFMDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn42 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn21 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn45 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn46 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn50 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn47 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn48 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn49 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn51 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this._difensesFBDataGrid = new System.Windows.Forms.DataGridView();
            this._fBStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fBUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fBOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fBAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage28 = new System.Windows.Forms.TabPage();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._blockNpusk = new System.Windows.Forms.MaskedTextBox();
            this._blockNt = new System.Windows.Forms.MaskedTextBox();
            this._blockNhot = new System.Windows.Forms.MaskedTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this._blockQt = new System.Windows.Forms.MaskedTextBox();
            this._blockQust = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._blockQmode = new System.Windows.Forms.ComboBox();
            this._defenseQgrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn20 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._externalDifensesDataGrid = new System.Windows.Forms.DataGridView();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox11 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox13 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox14 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox15 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox16 = new System.Windows.Forms.CheckedListBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndColorCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox9 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this._oscPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label133 = new System.Windows.Forms.Label();
            this._oscopePercent = new System.Windows.Forms.MaskedTextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this._oscChannelsDgv = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._oscopeCountComboBox = new System.Windows.Forms.ComboBox();
            this.label136 = new System.Windows.Forms.Label();
            this._oscopeLenghtLabel = new System.Windows.Forms.Label();
            this._oscCount = new System.Windows.Forms.TextBox();
            this._inpOsc = new System.Windows.Forms.ComboBox();
            this._oscopeFixationComboBox = new System.Windows.Forms.ComboBox();
            this.label135 = new System.Windows.Forms.Label();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this.panel1 = new System.Windows.Forms.Panel();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox10 = new System.Windows.Forms.CheckedListBox();
            this._inputSygnalsPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._inpResTT = new System.Windows.Forms.ComboBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._grUstComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals9 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals10 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this._inputSignals13 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this._inputSignals14 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this._inputSignals15 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this._inputSignals16 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._inputSignals5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this._inputSignals6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this._inputSignals7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this._inputSignals8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._measureTransPage = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._controlTtNeisp = new System.Windows.Forms.ComboBox();
            this._controlTtTsr = new System.Windows.Forms.MaskedTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this._controlTtIdmin = new System.Windows.Forms.MaskedTextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this._TH_XKoeff_ComboBox = new System.Windows.Forms.ComboBox();
            this._faultTnx = new System.Windows.Forms.ComboBox();
            this._TH_LKoeff_ComboBox = new System.Windows.Forms.ComboBox();
            this.label80 = new System.Windows.Forms.Label();
            this._faultTnl = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._TH_L_TextBox = new System.Windows.Forms.MaskedTextBox();
            this._TH_X_TextBox = new System.Windows.Forms.MaskedTextBox();
            this._typeTn = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this._i0CorrectionL2 = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this._TT_X2_TextBox = new System.Windows.Forms.MaskedTextBox();
            this._TT_L2_TextBox = new System.Windows.Forms.MaskedTextBox();
            this._polarityX2 = new System.Windows.Forms.ComboBox();
            this._polarityL2 = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._typeTT1 = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this._i0CorrectionL1 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this._TT_X1_TextBox = new System.Windows.Forms.MaskedTextBox();
            this._TT_L1_TextBox = new System.Windows.Forms.MaskedTextBox();
            this._polarityX1 = new System.Windows.Forms.ComboBox();
            this._polarityL1 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this._powTransPage = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._sn = new System.Windows.Forms.MaskedTextBox();
            this.label39 = new System.Windows.Forms.Label();
            this._passportKpd = new System.Windows.Forms.MaskedTextBox();
            this.label66 = new System.Windows.Forms.Label();
            this._passportCos = new System.Windows.Forms.MaskedTextBox();
            this._passportWatt = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this._passportP = new System.Windows.Forms.MaskedTextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._engineNreset = new System.Windows.Forms.ComboBox();
            this._engineQresetGr1 = new System.Windows.Forms.ComboBox();
            this._qBox = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._tCount = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._tStartBox = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._iStartBox = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._engineIdv = new System.Windows.Forms.MaskedTextBox();
            this.label152 = new System.Windows.Forms.Label();
            this._engineCoolingTimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._engineHeatingTimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this._fault5CheckBox = new System.Windows.Forms.CheckBox();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._fault4Label = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this._fault3Label = new System.Windows.Forms.Label();
            this._fault2Label = new System.Windows.Forms.Label();
            this._fault1Label = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this._automatic = new System.Windows.Forms.TabPage();
            this._switch = new System.Windows.Forms.CheckBox();
            this.switchGroup = new System.Windows.Forms.GroupBox();
            this._sdtuBlock = new System.Windows.Forms.ComboBox();
            this._switchKontCep = new System.Windows.Forms.ComboBox();
            this._switchTUskor = new System.Windows.Forms.MaskedTextBox();
            this._switchImp = new System.Windows.Forms.MaskedTextBox();
            this._switchIUrov = new System.Windows.Forms.MaskedTextBox();
            this._switchTUrov = new System.Windows.Forms.MaskedTextBox();
            this._switchBlock = new System.Windows.Forms.ComboBox();
            this._switchError = new System.Windows.Forms.ComboBox();
            this._switchOn = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this._switchOff = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage30 = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this._autoSwitch = new System.Windows.Forms.CheckBox();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this._apv2Krat = new System.Windows.Forms.MaskedTextBox();
            this._apv1Krat = new System.Windows.Forms.MaskedTextBox();
            this._apvTReady = new System.Windows.Forms.MaskedTextBox();
            this._apvTBlock = new System.Windows.Forms.MaskedTextBox();
            this._apvBlocking = new System.Windows.Forms.ComboBox();
            this.label95 = new System.Windows.Forms.Label();
            this._apvModes = new System.Windows.Forms.ComboBox();
            this.label94 = new System.Windows.Forms.Label();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this._avrDef = new System.Windows.Forms.CheckBox();
            this._avrSelfOff = new System.Windows.Forms.CheckBox();
            this._avrSwitchOff = new System.Windows.Forms.CheckBox();
            this._avrSignal = new System.Windows.Forms.CheckBox();
            this.label125 = new System.Windows.Forms.Label();
            this._avrClear = new System.Windows.Forms.ComboBox();
            this.label124 = new System.Windows.Forms.Label();
            this._avrTOff = new System.Windows.Forms.MaskedTextBox();
            this.label123 = new System.Windows.Forms.Label();
            this._avrTBack = new System.Windows.Forms.MaskedTextBox();
            this.label122 = new System.Windows.Forms.Label();
            this._avrBack = new System.Windows.Forms.ComboBox();
            this.label121 = new System.Windows.Forms.Label();
            this._avrTSr = new System.Windows.Forms.MaskedTextBox();
            this.label120 = new System.Windows.Forms.Label();
            this._avrResolve = new System.Windows.Forms.ComboBox();
            this.label119 = new System.Windows.Forms.Label();
            this._avrBlockClear = new System.Windows.Forms.ComboBox();
            this.label118 = new System.Windows.Forms.Label();
            this._avrBlocking = new System.Windows.Forms.ComboBox();
            this.label117 = new System.Windows.Forms.Label();
            this._avrSIGNOn = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label127 = new System.Windows.Forms.Label();
            this._lzhVal = new System.Windows.Forms.MaskedTextBox();
            this._lzhModes = new System.Windows.Forms.ComboBox();
            this.label126 = new System.Windows.Forms.Label();
            this.controlGroup = new System.Windows.Forms.GroupBox();
            this._switchBinding = new System.Windows.Forms.ComboBox();
            this._switchSDTU = new System.Windows.Forms.ComboBox();
            this._switchVnesh = new System.Windows.Forms.ComboBox();
            this._switchKey = new System.Windows.Forms.ComboBox();
            this._switchButtons = new System.Windows.Forms.ComboBox();
            this._switchVneshOff = new System.Windows.Forms.ComboBox();
            this._switchVneshOn = new System.Windows.Forms.ComboBox();
            this._switchKeyOff = new System.Windows.Forms.ComboBox();
            this._switchKeyOn = new System.Windows.Forms.ComboBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this.ButtontoolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox6.SuspendLayout();
            this.tabPage19.SuspendLayout();
            this.tabPage22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesIstarDataGrid)).BeginInit();
            this.tabPage21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defenseI2I1DataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._defenseI7DataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._defensesI56DataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._defensesI14DataGrid)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._allDefensesPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this._difensesTC.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage18.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defPdgv)).BeginInit();
            this.tabPage23.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defensesUMDataGrid)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defensesUBDataGrid)).BeginInit();
            this.tabPage26.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).BeginInit();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).BeginInit();
            this.tabPage28.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defenseQgrid)).BeginInit();
            this.tabPage25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).BeginInit();
            this.VLS1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.VLS2.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this._oscPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsDgv)).BeginInit();
            this._statusStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.VLS10.SuspendLayout();
            this._inputSygnalsPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).BeginInit();
            this._measureTransPage.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._configurationTabControl.SuspendLayout();
            this._powTransPage.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this._automatic.SuspendLayout();
            this.switchGroup.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage30.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.tabPage31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.tabPage32.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.controlGroup.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(6, 184);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(28, 13);
            this.label129.TabIndex = 19;
            this.label129.Text = "АВР";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(6, 164);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(29, 13);
            this.label128.TabIndex = 17;
            this.label128.Text = "АПВ";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._diffCutoutOsc);
            this.groupBox6.Controls.Add(this.label53);
            this.groupBox6.Controls.Add(this._diffCutoutUROV);
            this.groupBox6.Controls.Add(this._diffCutoutAVR);
            this.groupBox6.Controls.Add(this._diffCutoutAPV);
            this.groupBox6.Controls.Add(this._diffCutoutMgn);
            this.groupBox6.Controls.Add(this.label129);
            this.groupBox6.Controls.Add(this.label128);
            this.groupBox6.Controls.Add(this._timeEnduranceDTOBTTextBox);
            this.groupBox6.Controls.Add(this._constraintDTOBTTextBox);
            this.groupBox6.Controls.Add(this.label51);
            this.groupBox6.Controls.Add(this._blockingDTOBTComboBox);
            this.groupBox6.Controls.Add(this._modeDTOBTComboBox);
            this.groupBox6.Controls.Add(this.label52);
            this.groupBox6.Controls.Add(this.label50);
            this.groupBox6.Controls.Add(this.label49);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(319, 216);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Диф. токовая отсечка без торможения";
            // 
            // _diffCutoutOsc
            // 
            this._diffCutoutOsc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._diffCutoutOsc.FormattingEnabled = true;
            this._diffCutoutOsc.Location = new System.Drawing.Point(196, 120);
            this._diffCutoutOsc.Name = "_diffCutoutOsc";
            this._diffCutoutOsc.Size = new System.Drawing.Size(90, 21);
            this._diffCutoutOsc.TabIndex = 22;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 123);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(76, 13);
            this.label53.TabIndex = 21;
            this.label53.Text = "Осциллограф";
            // 
            // _diffCutoutUROV
            // 
            this._diffCutoutUROV.AutoSize = true;
            this._diffCutoutUROV.Location = new System.Drawing.Point(196, 147);
            this._diffCutoutUROV.Name = "_diffCutoutUROV";
            this._diffCutoutUROV.Size = new System.Drawing.Size(15, 14);
            this._diffCutoutUROV.TabIndex = 20;
            this._diffCutoutUROV.UseVisualStyleBackColor = true;
            // 
            // _diffCutoutAVR
            // 
            this._diffCutoutAVR.AutoSize = true;
            this._diffCutoutAVR.Location = new System.Drawing.Point(196, 187);
            this._diffCutoutAVR.Name = "_diffCutoutAVR";
            this._diffCutoutAVR.Size = new System.Drawing.Size(15, 14);
            this._diffCutoutAVR.TabIndex = 20;
            this._diffCutoutAVR.UseVisualStyleBackColor = true;
            // 
            // _diffCutoutAPV
            // 
            this._diffCutoutAPV.AutoSize = true;
            this._diffCutoutAPV.Location = new System.Drawing.Point(196, 167);
            this._diffCutoutAPV.Name = "_diffCutoutAPV";
            this._diffCutoutAPV.Size = new System.Drawing.Size(15, 14);
            this._diffCutoutAPV.TabIndex = 20;
            this._diffCutoutAPV.UseVisualStyleBackColor = true;
            // 
            // _diffCutoutMgn
            // 
            this._diffCutoutMgn.AutoSize = true;
            this._diffCutoutMgn.Location = new System.Drawing.Point(196, 100);
            this._diffCutoutMgn.Name = "_diffCutoutMgn";
            this._diffCutoutMgn.Size = new System.Drawing.Size(15, 14);
            this._diffCutoutMgn.TabIndex = 20;
            this._diffCutoutMgn.UseVisualStyleBackColor = true;
            // 
            // _timeEnduranceDTOBTTextBox
            // 
            this._timeEnduranceDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTOBTTextBox.Location = new System.Drawing.Point(196, 74);
            this._timeEnduranceDTOBTTextBox.Name = "_timeEnduranceDTOBTTextBox";
            this._timeEnduranceDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTOBTTextBox.TabIndex = 16;
            this._timeEnduranceDTOBTTextBox.Tag = "3276700";
            this._timeEnduranceDTOBTTextBox.Text = "0";
            this._timeEnduranceDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _constraintDTOBTTextBox
            // 
            this._constraintDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTOBTTextBox.Location = new System.Drawing.Point(196, 55);
            this._constraintDTOBTTextBox.Name = "_constraintDTOBTTextBox";
            this._constraintDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTOBTTextBox.TabIndex = 15;
            this._constraintDTOBTTextBox.Tag = "40";
            this._constraintDTOBTTextBox.Text = "0";
            this._constraintDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 38);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(68, 13);
            this.label51.TabIndex = 4;
            this.label51.Text = "Блокировка";
            // 
            // _blockingDTOBTComboBox
            // 
            this._blockingDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTOBTComboBox.FormattingEnabled = true;
            this._blockingDTOBTComboBox.Location = new System.Drawing.Point(196, 35);
            this._blockingDTOBTComboBox.Name = "_blockingDTOBTComboBox";
            this._blockingDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._blockingDTOBTComboBox.TabIndex = 11;
            // 
            // _modeDTOBTComboBox
            // 
            this._modeDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTOBTComboBox.FormattingEnabled = true;
            this._modeDTOBTComboBox.Location = new System.Drawing.Point(196, 15);
            this._modeDTOBTComboBox.Name = "_modeDTOBTComboBox";
            this._modeDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTOBTComboBox.TabIndex = 7;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 144);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(37, 13);
            this.label52.TabIndex = 5;
            this.label52.Text = "УРОВ";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 77);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(155, 13);
            this.label50.TabIndex = 3;
            this.label50.Text = "Выдержка времени Tд>>, мс";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 58);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(95, 13);
            this.label49.TabIndex = 2;
            this.label49.Text = "Уставка Iд>>, Iдв";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 101);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(189, 13);
            this.label48.TabIndex = 1;
            this.label48.Text = "Ступень по мгновенным значениям";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 18);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(42, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "Режим";
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.groupBox6);
            this.tabPage19.Location = new System.Drawing.Point(4, 25);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Size = new System.Drawing.Size(858, 462);
            this.tabPage19.TabIndex = 2;
            this.tabPage19.Text = "Диф. отсечка";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(12, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(42, 13);
            this.label56.TabIndex = 8;
            this.label56.Text = "Режим";
            // 
            // _K2TangensTextBox
            // 
            this._K2TangensTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K2TangensTextBox.Location = new System.Drawing.Point(141, 78);
            this._K2TangensTextBox.Name = "_K2TangensTextBox";
            this._K2TangensTextBox.Size = new System.Drawing.Size(90, 20);
            this._K2TangensTextBox.TabIndex = 18;
            this._K2TangensTextBox.Tag = "89";
            this._K2TangensTextBox.Text = "0";
            this._K2TangensTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Ib2BeginTextBox
            // 
            this._Ib2BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib2BeginTextBox.Location = new System.Drawing.Point(141, 59);
            this._Ib2BeginTextBox.Name = "_Ib2BeginTextBox";
            this._Ib2BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib2BeginTextBox.TabIndex = 17;
            this._Ib2BeginTextBox.Tag = "40";
            this._Ib2BeginTextBox.Text = "0";
            this._Ib2BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _modeDTZComboBox
            // 
            this._modeDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTZComboBox.FormattingEnabled = true;
            this._modeDTZComboBox.Location = new System.Drawing.Point(148, 13);
            this._modeDTZComboBox.Name = "_modeDTZComboBox";
            this._modeDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTZComboBox.TabIndex = 9;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this._difensesIstarDataGrid);
            this.tabPage22.Location = new System.Drawing.Point(4, 25);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Size = new System.Drawing.Size(858, 462);
            this.tabPage22.TabIndex = 5;
            this.tabPage22.Text = "Защ. I*";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // _difensesIstarDataGrid
            // 
            this._difensesIstarDataGrid.AllowUserToAddRows = false;
            this._difensesIstarDataGrid.AllowUserToDeleteRows = false;
            this._difensesIstarDataGrid.AllowUserToResizeColumns = false;
            this._difensesIstarDataGrid.AllowUserToResizeRows = false;
            this._difensesIstarDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesIstarDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesIstarDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._i0StageColumn,
            this._i0ModesColumn,
            this._i0IColumn,
            this._i0InColumn,
            this._i0UsYNColumn,
            this._i0UstartColumn,
            this._i0DirColumn,
            this._i0UndirColumn,
            this._i0I0Column,
            this._i0CharColumn,
            this._i0TColumn,
            this._i0kColumn,
            this._i0BlockingColumn,
            this._i0OscColumn,
            this._i0TyBoolColumn,
            this._i0TyColumn,
            this._i0UROVColumn,
            this._i0APVColumn,
            this._i0AVRColumn});
            this._difensesIstarDataGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._difensesIstarDataGrid.Location = new System.Drawing.Point(0, 0);
            this._difensesIstarDataGrid.MultiSelect = false;
            this._difensesIstarDataGrid.Name = "_difensesIstarDataGrid";
            this._difensesIstarDataGrid.RowHeadersVisible = false;
            this._difensesIstarDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesIstarDataGrid.RowTemplate.Height = 24;
            this._difensesIstarDataGrid.ShowCellErrors = false;
            this._difensesIstarDataGrid.ShowRowErrors = false;
            this._difensesIstarDataGrid.Size = new System.Drawing.Size(858, 187);
            this._difensesIstarDataGrid.TabIndex = 4;
            // 
            // _i0StageColumn
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this._i0StageColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this._i0StageColumn.Frozen = true;
            this._i0StageColumn.HeaderText = "Ступень";
            this._i0StageColumn.Name = "_i0StageColumn";
            this._i0StageColumn.ReadOnly = true;
            this._i0StageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0StageColumn.Width = 60;
            // 
            // _i0ModesColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0ModesColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this._i0ModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._i0ModesColumn.HeaderText = "Режим";
            this._i0ModesColumn.Name = "_i0ModesColumn";
            this._i0ModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0ModesColumn.Width = 80;
            // 
            // _i0IColumn
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0IColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this._i0IColumn.HeaderText = "Iср, Iн";
            this._i0IColumn.Name = "_i0IColumn";
            this._i0IColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0IColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0IColumn.Width = 55;
            // 
            // _i0InColumn
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0InColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this._i0InColumn.HeaderText = "Сторона";
            this._i0InColumn.Name = "_i0InColumn";
            this._i0InColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0InColumn.Width = 65;
            // 
            // _i0UsYNColumn
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.NullValue = false;
            this._i0UsYNColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this._i0UsYNColumn.HeaderText = "Пуск по U";
            this._i0UsYNColumn.Name = "_i0UsYNColumn";
            this._i0UsYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0UsYNColumn.Width = 65;
            // 
            // _i0UstartColumn
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0UstartColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this._i0UstartColumn.HeaderText = "Uпуск, В";
            this._i0UstartColumn.Name = "_i0UstartColumn";
            this._i0UstartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0UstartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0UstartColumn.Width = 60;
            // 
            // _i0DirColumn
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0DirColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this._i0DirColumn.HeaderText = "Направление";
            this._i0DirColumn.Name = "_i0DirColumn";
            this._i0DirColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0DirColumn.Width = 85;
            // 
            // _i0UndirColumn
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0UndirColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this._i0UndirColumn.HeaderText = "Недост.напр.";
            this._i0UndirColumn.Name = "_i0UndirColumn";
            this._i0UndirColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0UndirColumn.Width = 80;
            // 
            // _i0I0Column
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0I0Column.DefaultCellStyle = dataGridViewCellStyle9;
            this._i0I0Column.HeaderText = "I*";
            this._i0I0Column.Name = "_i0I0Column";
            this._i0I0Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0I0Column.Width = 40;
            // 
            // _i0CharColumn
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0CharColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this._i0CharColumn.HeaderText = "Характ-ка";
            this._i0CharColumn.Name = "_i0CharColumn";
            this._i0CharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0CharColumn.Width = 90;
            // 
            // _i0TColumn
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0TColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this._i0TColumn.HeaderText = "tср, мс";
            this._i0TColumn.Name = "_i0TColumn";
            this._i0TColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0TColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0TColumn.Width = 65;
            // 
            // _i0kColumn
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0kColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this._i0kColumn.HeaderText = "k завис. хар-ки";
            this._i0kColumn.Name = "_i0kColumn";
            this._i0kColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0kColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _i0BlockingColumn
            // 
            this._i0BlockingColumn.HeaderText = "Блокировка";
            this._i0BlockingColumn.Name = "_i0BlockingColumn";
            this._i0BlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0BlockingColumn.Width = 90;
            // 
            // _i0OscColumn
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0OscColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this._i0OscColumn.HeaderText = "Осциллограф";
            this._i0OscColumn.Name = "_i0OscColumn";
            this._i0OscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0OscColumn.Width = 80;
            // 
            // _i0TyBoolColumn
            // 
            this._i0TyBoolColumn.HeaderText = "Ускорение";
            this._i0TyBoolColumn.Name = "_i0TyBoolColumn";
            this._i0TyBoolColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0TyBoolColumn.Width = 69;
            // 
            // _i0TyColumn
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0TyColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this._i0TyColumn.HeaderText = "ty, мс";
            this._i0TyColumn.Name = "_i0TyColumn";
            this._i0TyColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0TyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0TyColumn.Width = 55;
            // 
            // _i0UROVColumn
            // 
            this._i0UROVColumn.HeaderText = "УРОВ";
            this._i0UROVColumn.Name = "_i0UROVColumn";
            this._i0UROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0UROVColumn.Width = 60;
            // 
            // _i0APVColumn
            // 
            this._i0APVColumn.HeaderText = "АПВ";
            this._i0APVColumn.Name = "_i0APVColumn";
            this._i0APVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0APVColumn.Width = 40;
            // 
            // _i0AVRColumn
            // 
            this._i0AVRColumn.HeaderText = "АВР";
            this._i0AVRColumn.Name = "_i0AVRColumn";
            this._i0AVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._i0AVRColumn.Width = 40;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(12, 76);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(42, 13);
            this.label59.TabIndex = 17;
            this.label59.Text = "tср, мс";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(12, 57);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(43, 13);
            this.label61.TabIndex = 16;
            this.label61.Text = "Iд>, Iдв";
            // 
            // VLScheckedListBox12
            // 
            this.VLScheckedListBox12.CheckOnClick = true;
            this.VLScheckedListBox12.FormattingEnabled = true;
            this.VLScheckedListBox12.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox12.Name = "VLScheckedListBox12";
            this.VLScheckedListBox12.ScrollAlwaysVisible = true;
            this.VLScheckedListBox12.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox12.TabIndex = 6;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this._defenseI2I1DataGrid);
            this.tabPage21.Controls.Add(this._defenseI7DataGrid);
            this.tabPage21.Controls.Add(this._defensesI56DataGrid);
            this.tabPage21.Controls.Add(this._defensesI14DataGrid);
            this.tabPage21.Location = new System.Drawing.Point(4, 25);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Size = new System.Drawing.Size(858, 462);
            this.tabPage21.TabIndex = 4;
            this.tabPage21.Text = "Защ. I";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // _defenseI2I1DataGrid
            // 
            this._defenseI2I1DataGrid.AllowUserToAddRows = false;
            this._defenseI2I1DataGrid.AllowUserToDeleteRows = false;
            this._defenseI2I1DataGrid.AllowUserToResizeColumns = false;
            this._defenseI2I1DataGrid.AllowUserToResizeRows = false;
            this._defenseI2I1DataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._defenseI2I1DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this._defenseI2I1DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defenseI2I1DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewComboBoxColumn37,
            this.dataGridViewComboBoxColumn43,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewComboBoxColumn44,
            this.dataGridViewCheckBoxColumn13,
            this.dataGridViewCheckBoxColumn14,
            this.dataGridViewCheckBoxColumn15});
            this._defenseI2I1DataGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._defenseI2I1DataGrid.Location = new System.Drawing.Point(0, 289);
            this._defenseI2I1DataGrid.MultiSelect = false;
            this._defenseI2I1DataGrid.Name = "_defenseI2I1DataGrid";
            this._defenseI2I1DataGrid.RowHeadersVisible = false;
            this._defenseI2I1DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defenseI2I1DataGrid.RowTemplate.Height = 24;
            this._defenseI2I1DataGrid.ShowCellErrors = false;
            this._defenseI2I1DataGrid.ShowRowErrors = false;
            this._defenseI2I1DataGrid.Size = new System.Drawing.Size(858, 46);
            this._defenseI2I1DataGrid.TabIndex = 8;
            // 
            // dataGridViewTextBoxColumn29
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn29.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn29.Frozen = true;
            this.dataGridViewTextBoxColumn29.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn29.Width = 55;
            // 
            // dataGridViewComboBoxColumn37
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn37.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewComboBoxColumn37.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn37.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn37.Name = "dataGridViewComboBoxColumn37";
            this.dataGridViewComboBoxColumn37.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn37.Width = 80;
            // 
            // dataGridViewComboBoxColumn43
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn43.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewComboBoxColumn43.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn43.Name = "dataGridViewComboBoxColumn43";
            this.dataGridViewComboBoxColumn43.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn43.Width = 90;
            // 
            // dataGridViewTextBoxColumn30
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn30.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn30.HeaderText = "I2/I1, %";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn30.Width = 50;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn32.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn32.Width = 65;
            // 
            // dataGridViewComboBoxColumn44
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn44.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewComboBoxColumn44.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn44.Name = "dataGridViewComboBoxColumn44";
            this.dataGridViewComboBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn44.Width = 110;
            // 
            // dataGridViewCheckBoxColumn13
            // 
            this.dataGridViewCheckBoxColumn13.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn13.Name = "dataGridViewCheckBoxColumn13";
            this.dataGridViewCheckBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn13.Width = 50;
            // 
            // dataGridViewCheckBoxColumn14
            // 
            this.dataGridViewCheckBoxColumn14.HeaderText = "АПВ";
            this.dataGridViewCheckBoxColumn14.Name = "dataGridViewCheckBoxColumn14";
            this.dataGridViewCheckBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn14.Width = 40;
            // 
            // dataGridViewCheckBoxColumn15
            // 
            this.dataGridViewCheckBoxColumn15.HeaderText = "АВР";
            this.dataGridViewCheckBoxColumn15.Name = "dataGridViewCheckBoxColumn15";
            this.dataGridViewCheckBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn15.Width = 40;
            // 
            // _defenseI7DataGrid
            // 
            this._defenseI7DataGrid.AllowUserToAddRows = false;
            this._defenseI7DataGrid.AllowUserToDeleteRows = false;
            this._defenseI7DataGrid.AllowUserToResizeColumns = false;
            this._defenseI7DataGrid.AllowUserToResizeRows = false;
            this._defenseI7DataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._defenseI7DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this._defenseI7DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defenseI7DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewComboBoxColumn29,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewComboBoxColumn30,
            this.Column6,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewComboBoxColumn31,
            this.dataGridViewComboBoxColumn32,
            this.Column16,
            this.dataGridViewComboBoxColumn33,
            this.dataGridViewComboBoxColumn34,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewComboBoxColumn35,
            this.dataGridViewComboBoxColumn36,
            this.dataGridViewCheckBoxColumn7,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewCheckBoxColumn10});
            this._defenseI7DataGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._defenseI7DataGrid.Location = new System.Drawing.Point(0, 224);
            this._defenseI7DataGrid.MultiSelect = false;
            this._defenseI7DataGrid.Name = "_defenseI7DataGrid";
            this._defenseI7DataGrid.RowHeadersVisible = false;
            this._defenseI7DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defenseI7DataGrid.RowTemplate.Height = 24;
            this._defenseI7DataGrid.ShowCellErrors = false;
            this._defenseI7DataGrid.ShowRowErrors = false;
            this._defenseI7DataGrid.Size = new System.Drawing.Size(858, 65);
            this._defenseI7DataGrid.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn23.Frozen = true;
            this.dataGridViewTextBoxColumn23.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 55;
            // 
            // dataGridViewComboBoxColumn29
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn29.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewComboBoxColumn29.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn29.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn29.Name = "dataGridViewComboBoxColumn29";
            this.dataGridViewComboBoxColumn29.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn29.Width = 80;
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn24.HeaderText = "Iср, Iн";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Width = 50;
            // 
            // dataGridViewComboBoxColumn30
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn30.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewComboBoxColumn30.HeaderText = "Сторона";
            this.dataGridViewComboBoxColumn30.Name = "dataGridViewComboBoxColumn30";
            this.dataGridViewComboBoxColumn30.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn30.Width = 55;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.Visible = false;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.NullValue = false;
            this.dataGridViewCheckBoxColumn6.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewCheckBoxColumn6.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn6.Visible = false;
            this.dataGridViewCheckBoxColumn6.Width = 65;
            // 
            // dataGridViewTextBoxColumn25
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn25.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn25.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.ToolTipText = "123123123";
            this.dataGridViewTextBoxColumn25.Visible = false;
            this.dataGridViewTextBoxColumn25.Width = 65;
            // 
            // dataGridViewComboBoxColumn31
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn31.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewComboBoxColumn31.HeaderText = "Направление";
            this.dataGridViewComboBoxColumn31.Name = "dataGridViewComboBoxColumn31";
            this.dataGridViewComboBoxColumn31.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn31.Visible = false;
            this.dataGridViewComboBoxColumn31.Width = 85;
            // 
            // dataGridViewComboBoxColumn32
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn32.DefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridViewComboBoxColumn32.HeaderText = "Недост.напр";
            this.dataGridViewComboBoxColumn32.Name = "dataGridViewComboBoxColumn32";
            this.dataGridViewComboBoxColumn32.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn32.Visible = false;
            this.dataGridViewComboBoxColumn32.Width = 80;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Column16";
            this.Column16.Name = "Column16";
            this.Column16.Visible = false;
            // 
            // dataGridViewComboBoxColumn33
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn33.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewComboBoxColumn33.HeaderText = "Логика";
            this.dataGridViewComboBoxColumn33.Name = "dataGridViewComboBoxColumn33";
            this.dataGridViewComboBoxColumn33.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn33.Width = 90;
            // 
            // dataGridViewComboBoxColumn34
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn34.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewComboBoxColumn34.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn34.Name = "dataGridViewComboBoxColumn34";
            this.dataGridViewComboBoxColumn34.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn34.Visible = false;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn26.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn26.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn26.Width = 50;
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewTextBoxColumn27.HeaderText = "k завис. хар-ки";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Visible = false;
            // 
            // dataGridViewComboBoxColumn35
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn35.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewComboBoxColumn35.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn35.Name = "dataGridViewComboBoxColumn35";
            this.dataGridViewComboBoxColumn35.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn35.Width = 90;
            // 
            // dataGridViewComboBoxColumn36
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn36.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewComboBoxColumn36.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn36.Name = "dataGridViewComboBoxColumn36";
            this.dataGridViewComboBoxColumn36.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn36.Width = 110;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "Ускор.";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn7.Visible = false;
            this.dataGridViewCheckBoxColumn7.Width = 55;
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn28.DefaultCellStyle = dataGridViewCellStyle37;
            this.dataGridViewTextBoxColumn28.HeaderText = "ty, мс";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn28.Visible = false;
            this.dataGridViewTextBoxColumn28.Width = 50;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn8.Width = 50;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "АПВ";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn9.Width = 40;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "АВР";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn10.Width = 40;
            // 
            // _defensesI56DataGrid
            // 
            this._defensesI56DataGrid.AllowUserToAddRows = false;
            this._defensesI56DataGrid.AllowUserToDeleteRows = false;
            this._defensesI56DataGrid.AllowUserToResizeColumns = false;
            this._defensesI56DataGrid.AllowUserToResizeRows = false;
            this._defensesI56DataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._defensesI56DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this._defensesI56DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defensesI56DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewComboBoxColumn21,
            this.dataGridViewTextBoxColumn18,
            this.Column3,
            this.dataGridViewComboBoxColumn22,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewComboBoxColumn23,
            this.dataGridViewComboBoxColumn24,
            this.dataGridViewComboBoxColumn25,
            this.Column15,
            this.dataGridViewComboBoxColumn26,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewComboBoxColumn27,
            this.dataGridViewComboBoxColumn28,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn5});
            this._defensesI56DataGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._defensesI56DataGrid.Location = new System.Drawing.Point(0, 136);
            this._defensesI56DataGrid.MultiSelect = false;
            this._defensesI56DataGrid.Name = "_defensesI56DataGrid";
            this._defensesI56DataGrid.RowHeadersVisible = false;
            this._defensesI56DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defensesI56DataGrid.RowTemplate.Height = 24;
            this._defensesI56DataGrid.ShowCellErrors = false;
            this._defensesI56DataGrid.ShowRowErrors = false;
            this._defensesI56DataGrid.Size = new System.Drawing.Size(858, 88);
            this._defensesI56DataGrid.TabIndex = 6;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn17.Frozen = true;
            this.dataGridViewTextBoxColumn17.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 55;
            // 
            // dataGridViewComboBoxColumn21
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn21.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridViewComboBoxColumn21.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn21.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn21.Name = "dataGridViewComboBoxColumn21";
            this.dataGridViewComboBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn21.Width = 80;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewTextBoxColumn18.HeaderText = "Iср, Iн";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 50;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // dataGridViewComboBoxColumn22
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn22.DefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridViewComboBoxColumn22.HeaderText = "Условие";
            this.dataGridViewComboBoxColumn22.Name = "dataGridViewComboBoxColumn22";
            this.dataGridViewComboBoxColumn22.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle43.NullValue = false;
            this.dataGridViewCheckBoxColumn1.DefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn1.Width = 65;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn19.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.ToolTipText = "123123123";
            this.dataGridViewTextBoxColumn19.Width = 65;
            // 
            // dataGridViewComboBoxColumn23
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn23.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewComboBoxColumn23.HeaderText = "Направление";
            this.dataGridViewComboBoxColumn23.Name = "dataGridViewComboBoxColumn23";
            this.dataGridViewComboBoxColumn23.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn23.Width = 85;
            // 
            // dataGridViewComboBoxColumn24
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn24.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewComboBoxColumn24.HeaderText = "Недост.напр";
            this.dataGridViewComboBoxColumn24.Name = "dataGridViewComboBoxColumn24";
            this.dataGridViewComboBoxColumn24.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn24.Width = 80;
            // 
            // dataGridViewComboBoxColumn25
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn25.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewComboBoxColumn25.HeaderText = "Логика";
            this.dataGridViewComboBoxColumn25.Name = "dataGridViewComboBoxColumn25";
            this.dataGridViewComboBoxColumn25.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn25.Width = 90;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Column15";
            this.Column15.Name = "Column15";
            this.Column15.Visible = false;
            // 
            // dataGridViewComboBoxColumn26
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn26.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewComboBoxColumn26.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn26.Name = "dataGridViewComboBoxColumn26";
            this.dataGridViewComboBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn20.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 50;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn21.HeaderText = "k завис. хар-ки";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewComboBoxColumn27
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn27.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewComboBoxColumn27.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn27.Name = "dataGridViewComboBoxColumn27";
            this.dataGridViewComboBoxColumn27.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn27.Width = 90;
            // 
            // dataGridViewComboBoxColumn28
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn28.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewComboBoxColumn28.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn28.Name = "dataGridViewComboBoxColumn28";
            this.dataGridViewComboBoxColumn28.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn28.Width = 110;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "Ускор.";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn2.Width = 55;
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn22.HeaderText = "ty, мс";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 50;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn3.Width = 50;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "АПВ";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn4.Width = 40;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "АВР";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn5.Width = 40;
            // 
            // _defensesI14DataGrid
            // 
            this._defensesI14DataGrid.AllowUserToAddRows = false;
            this._defensesI14DataGrid.AllowUserToDeleteRows = false;
            this._defensesI14DataGrid.AllowUserToResizeColumns = false;
            this._defensesI14DataGrid.AllowUserToResizeRows = false;
            this._defensesI14DataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._defensesI14DataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle54;
            this._defensesI14DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defensesI14DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._iStageColumn,
            this._iModesColumn,
            this._iIColumn,
            this._iInColumn,
            this.Column4,
            this._iUstartYNColumn,
            this._iUStartColumn,
            this._iDirectColumn,
            this._iUnDirectColumn,
            this._iLogicColumn,
            this.Column14,
            this._iCharColumn,
            this._iTColumn,
            this._iKColumn,
            this._iBlockingColumn,
            this._iOscModeColumn,
            this._iTyBoolColumn,
            this._iTyColumn,
            this._iUROVModeColumn,
            this._iAPVModeColumn,
            this._iAVRModeColumn});
            this._defensesI14DataGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._defensesI14DataGrid.Location = new System.Drawing.Point(0, 0);
            this._defensesI14DataGrid.MultiSelect = false;
            this._defensesI14DataGrid.Name = "_defensesI14DataGrid";
            this._defensesI14DataGrid.RowHeadersVisible = false;
            this._defensesI14DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defensesI14DataGrid.RowTemplate.Height = 24;
            this._defensesI14DataGrid.ShowCellErrors = false;
            this._defensesI14DataGrid.ShowRowErrors = false;
            this._defensesI14DataGrid.Size = new System.Drawing.Size(858, 136);
            this._defensesI14DataGrid.TabIndex = 5;
            // 
            // _iStageColumn
            // 
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.White;
            this._iStageColumn.DefaultCellStyle = dataGridViewCellStyle55;
            this._iStageColumn.Frozen = true;
            this._iStageColumn.HeaderText = "Ступень";
            this._iStageColumn.Name = "_iStageColumn";
            this._iStageColumn.ReadOnly = true;
            this._iStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iStageColumn.Width = 55;
            // 
            // _iModesColumn
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iModesColumn.DefaultCellStyle = dataGridViewCellStyle56;
            this._iModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._iModesColumn.HeaderText = "Режим";
            this._iModesColumn.Name = "_iModesColumn";
            this._iModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iModesColumn.Width = 80;
            // 
            // _iIColumn
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iIColumn.DefaultCellStyle = dataGridViewCellStyle57;
            this._iIColumn.HeaderText = "Iср, Iн";
            this._iIColumn.Name = "_iIColumn";
            this._iIColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iIColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iIColumn.Width = 50;
            // 
            // _iInColumn
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iInColumn.DefaultCellStyle = dataGridViewCellStyle58;
            this._iInColumn.HeaderText = "Сторона";
            this._iInColumn.Name = "_iInColumn";
            this._iInColumn.Width = 55;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // _iUstartYNColumn
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle59.NullValue = false;
            this._iUstartYNColumn.DefaultCellStyle = dataGridViewCellStyle59;
            this._iUstartYNColumn.HeaderText = "Пуск по U";
            this._iUstartYNColumn.Name = "_iUstartYNColumn";
            this._iUstartYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUstartYNColumn.Width = 65;
            // 
            // _iUStartColumn
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iUStartColumn.DefaultCellStyle = dataGridViewCellStyle60;
            this._iUStartColumn.HeaderText = "Uпуск, В";
            this._iUStartColumn.MaxInputLength = 6;
            this._iUStartColumn.Name = "_iUStartColumn";
            this._iUStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iUStartColumn.ToolTipText = "123123123";
            this._iUStartColumn.Width = 65;
            // 
            // _iDirectColumn
            // 
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iDirectColumn.DefaultCellStyle = dataGridViewCellStyle61;
            this._iDirectColumn.HeaderText = "Направление";
            this._iDirectColumn.Name = "_iDirectColumn";
            this._iDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iDirectColumn.Width = 85;
            // 
            // _iUnDirectColumn
            // 
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iUnDirectColumn.DefaultCellStyle = dataGridViewCellStyle62;
            this._iUnDirectColumn.HeaderText = "Недост.напр";
            this._iUnDirectColumn.Name = "_iUnDirectColumn";
            this._iUnDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUnDirectColumn.Width = 80;
            // 
            // _iLogicColumn
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iLogicColumn.DefaultCellStyle = dataGridViewCellStyle63;
            this._iLogicColumn.HeaderText = "Логика";
            this._iLogicColumn.Name = "_iLogicColumn";
            this._iLogicColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iLogicColumn.Width = 90;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Column14";
            this.Column14.Name = "Column14";
            this.Column14.Visible = false;
            // 
            // _iCharColumn
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iCharColumn.DefaultCellStyle = dataGridViewCellStyle64;
            this._iCharColumn.HeaderText = "Характ-ка";
            this._iCharColumn.Name = "_iCharColumn";
            this._iCharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _iTColumn
            // 
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iTColumn.DefaultCellStyle = dataGridViewCellStyle65;
            this._iTColumn.HeaderText = "tср, мс";
            this._iTColumn.Name = "_iTColumn";
            this._iTColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iTColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iTColumn.Width = 50;
            // 
            // _iKColumn
            // 
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iKColumn.DefaultCellStyle = dataGridViewCellStyle66;
            this._iKColumn.HeaderText = "k завис. хар-ки";
            this._iKColumn.Name = "_iKColumn";
            this._iKColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _iBlockingColumn
            // 
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iBlockingColumn.DefaultCellStyle = dataGridViewCellStyle67;
            this._iBlockingColumn.HeaderText = "Блокировка";
            this._iBlockingColumn.Name = "_iBlockingColumn";
            this._iBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iBlockingColumn.Width = 90;
            // 
            // _iOscModeColumn
            // 
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iOscModeColumn.DefaultCellStyle = dataGridViewCellStyle68;
            this._iOscModeColumn.HeaderText = "Осциллограф";
            this._iOscModeColumn.Name = "_iOscModeColumn";
            this._iOscModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iOscModeColumn.Width = 110;
            // 
            // _iTyBoolColumn
            // 
            this._iTyBoolColumn.HeaderText = "Ускор.";
            this._iTyBoolColumn.Name = "_iTyBoolColumn";
            this._iTyBoolColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iTyBoolColumn.Width = 55;
            // 
            // _iTyColumn
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iTyColumn.DefaultCellStyle = dataGridViewCellStyle69;
            this._iTyColumn.HeaderText = "ty, мс";
            this._iTyColumn.Name = "_iTyColumn";
            this._iTyColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iTyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iTyColumn.Width = 50;
            // 
            // _iUROVModeColumn
            // 
            this._iUROVModeColumn.HeaderText = "УРОВ";
            this._iUROVModeColumn.Name = "_iUROVModeColumn";
            this._iUROVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iUROVModeColumn.Width = 50;
            // 
            // _iAPVModeColumn
            // 
            this._iAPVModeColumn.HeaderText = "АПВ";
            this._iAPVModeColumn.Name = "_iAPVModeColumn";
            this._iAPVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iAPVModeColumn.Width = 40;
            // 
            // _iAVRModeColumn
            // 
            this._iAVRModeColumn.HeaderText = "АВР";
            this._iAVRModeColumn.Name = "_iAVRModeColumn";
            this._iAVRModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._iAVRModeColumn.Width = 40;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._outputReleGrid);
            this.groupBox12.Location = new System.Drawing.Point(8, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(393, 189);
            this.groupBox12.TabIndex = 3;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputReleGrid.Location = new System.Drawing.Point(3, 16);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle70;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(387, 170);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releWaitCol.Width = 75;
            // 
            // _allDefensesPage
            // 
            this._allDefensesPage.Controls.Add(this.groupBox5);
            this._allDefensesPage.Controls.Add(this.groupBox10);
            this._allDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._allDefensesPage.Name = "_allDefensesPage";
            this._allDefensesPage.Size = new System.Drawing.Size(884, 553);
            this._allDefensesPage.TabIndex = 4;
            this._allDefensesPage.Text = "Защиты";
            this._allDefensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._copySetpointsBtn);
            this.groupBox5.Controls.Add(this._mainRadioButton);
            this.groupBox5.Controls.Add(this._reserveRadioButton);
            this.groupBox5.Location = new System.Drawing.Point(8, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(360, 35);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Группа уставок";
            // 
            // _copySetpointsBtn
            // 
            this._copySetpointsBtn.Location = new System.Drawing.Point(202, 8);
            this._copySetpointsBtn.Name = "_copySetpointsBtn";
            this._copySetpointsBtn.Size = new System.Drawing.Size(142, 23);
            this._copySetpointsBtn.TabIndex = 8;
            this._copySetpointsBtn.Text = "Основные -> Резервные";
            this._copySetpointsBtn.UseVisualStyleBackColor = true;
            // 
            // _mainRadioButton
            // 
            this._mainRadioButton.AutoSize = true;
            this._mainRadioButton.Checked = true;
            this._mainRadioButton.Location = new System.Drawing.Point(8, 11);
            this._mainRadioButton.Name = "_mainRadioButton";
            this._mainRadioButton.Size = new System.Drawing.Size(75, 17);
            this._mainRadioButton.TabIndex = 6;
            this._mainRadioButton.TabStop = true;
            this._mainRadioButton.Text = "Основная";
            this._mainRadioButton.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioButton
            // 
            this._reserveRadioButton.AutoSize = true;
            this._reserveRadioButton.Location = new System.Drawing.Point(98, 11);
            this._reserveRadioButton.Name = "_reserveRadioButton";
            this._reserveRadioButton.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioButton.TabIndex = 7;
            this._reserveRadioButton.Text = "Резервная";
            this._reserveRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this._difensesTC);
            this.groupBox10.Location = new System.Drawing.Point(9, 40);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(872, 510);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Защиты";
            // 
            // _difensesTC
            // 
            this._difensesTC.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._difensesTC.Controls.Add(this.tabPage17);
            this._difensesTC.Controls.Add(this.tabPage18);
            this._difensesTC.Controls.Add(this.tabPage19);
            this._difensesTC.Controls.Add(this.tabPage20);
            this._difensesTC.Controls.Add(this.tabPage21);
            this._difensesTC.Controls.Add(this.tabPage22);
            this._difensesTC.Controls.Add(this.tabPage23);
            this._difensesTC.Controls.Add(this.tabPage26);
            this._difensesTC.Controls.Add(this.tabPage28);
            this._difensesTC.Controls.Add(this.tabPage25);
            this._difensesTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesTC.Location = new System.Drawing.Point(3, 16);
            this._difensesTC.Name = "_difensesTC";
            this._difensesTC.SelectedIndex = 0;
            this._difensesTC.Size = new System.Drawing.Size(866, 491);
            this._difensesTC.TabIndex = 0;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.groupBox16);
            this.tabPage17.Location = new System.Drawing.Point(4, 25);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(858, 462);
            this.tabPage17.TabIndex = 0;
            this.tabPage17.Text = "Параметры сети";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.groupBox37);
            this.groupBox16.Controls.Add(this.groupBox4);
            this.groupBox16.Location = new System.Drawing.Point(6, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(188, 149);
            this.groupBox16.TabIndex = 2;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Угол МЧ";
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this.label17);
            this.groupBox37.Controls.Add(this._cIL2);
            this.groupBox37.Controls.Add(this.label16);
            this.groupBox37.Controls.Add(this._cInL2);
            this.groupBox37.Controls.Add(this.label15);
            this.groupBox37.Controls.Add(this._cI0L2);
            this.groupBox37.Controls.Add(this.label14);
            this.groupBox37.Controls.Add(this._cI2L2);
            this.groupBox37.Location = new System.Drawing.Point(97, 19);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(85, 125);
            this.groupBox37.TabIndex = 3;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Сторона 2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 100);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "I2";
            // 
            // _cIL2
            // 
            this._cIL2.Location = new System.Drawing.Point(28, 19);
            this._cIL2.Name = "_cIL2";
            this._cIL2.Size = new System.Drawing.Size(44, 20);
            this._cIL2.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "I0";
            // 
            // _cInL2
            // 
            this._cInL2.Location = new System.Drawing.Point(28, 45);
            this._cInL2.Name = "_cInL2";
            this._cInL2.Size = new System.Drawing.Size(44, 20);
            this._cInL2.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "In";
            // 
            // _cI0L2
            // 
            this._cI0L2.Location = new System.Drawing.Point(28, 71);
            this._cI0L2.Name = "_cI0L2";
            this._cI0L2.Size = new System.Drawing.Size(44, 20);
            this._cI0L2.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "I";
            // 
            // _cI2L2
            // 
            this._cI2L2.Location = new System.Drawing.Point(28, 97);
            this._cI2L2.Name = "_cI2L2";
            this._cI2L2.Size = new System.Drawing.Size(44, 20);
            this._cI2L2.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._cI2L1);
            this.groupBox4.Controls.Add(this._cI0L1);
            this.groupBox4.Controls.Add(this._cInL1);
            this.groupBox4.Controls.Add(this._cIL1);
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(85, 125);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Сторона 1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "I2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "I0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "In";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "I";
            // 
            // _cI2L1
            // 
            this._cI2L1.Location = new System.Drawing.Point(28, 97);
            this._cI2L1.Name = "_cI2L1";
            this._cI2L1.Size = new System.Drawing.Size(44, 20);
            this._cI2L1.TabIndex = 0;
            // 
            // _cI0L1
            // 
            this._cI0L1.Location = new System.Drawing.Point(28, 71);
            this._cI0L1.Name = "_cI0L1";
            this._cI0L1.Size = new System.Drawing.Size(44, 20);
            this._cI0L1.TabIndex = 0;
            // 
            // _cInL1
            // 
            this._cInL1.Location = new System.Drawing.Point(28, 45);
            this._cInL1.Name = "_cInL1";
            this._cInL1.Size = new System.Drawing.Size(44, 20);
            this._cInL1.TabIndex = 0;
            // 
            // _cIL1
            // 
            this._cIL1.Location = new System.Drawing.Point(28, 19);
            this._cIL1.Name = "_cIL1";
            this._cIL1.Size = new System.Drawing.Size(44, 20);
            this._cIL1.TabIndex = 0;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.groupBox7);
            this.tabPage18.Location = new System.Drawing.Point(4, 25);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(858, 462);
            this.tabPage18.TabIndex = 1;
            this.tabPage18.Text = "Дифф. защита";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._diffAVR);
            this.groupBox7.Controls.Add(this._DiffAPV);
            this.groupBox7.Controls.Add(this._diffUROV);
            this.groupBox7.Controls.Add(this.label131);
            this.groupBox7.Controls.Add(this.label130);
            this.groupBox7.Controls.Add(this.groupBox25);
            this.groupBox7.Controls.Add(this._oscDTZComboBox);
            this.groupBox7.Controls.Add(this._blockingDTZComboBox);
            this.groupBox7.Controls.Add(this.label76);
            this.groupBox7.Controls.Add(this.label77);
            this.groupBox7.Controls.Add(this.label78);
            this.groupBox7.Controls.Add(this._timeEnduranceDTZTextBox);
            this.groupBox7.Controls.Add(this.groupBox9);
            this.groupBox7.Controls.Add(this._constraintDTZTextBox);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Controls.Add(this.label59);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Controls.Add(this._modeDTZComboBox);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(276, 456);
            this.groupBox7.TabIndex = 13;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Диф. токовая защита";
            // 
            // _diffAVR
            // 
            this._diffAVR.AutoSize = true;
            this._diffAVR.Location = new System.Drawing.Point(147, 432);
            this._diffAVR.Name = "_diffAVR";
            this._diffAVR.Size = new System.Drawing.Size(15, 14);
            this._diffAVR.TabIndex = 37;
            this._diffAVR.UseVisualStyleBackColor = true;
            // 
            // _DiffAPV
            // 
            this._DiffAPV.AutoSize = true;
            this._DiffAPV.Location = new System.Drawing.Point(147, 412);
            this._DiffAPV.Name = "_DiffAPV";
            this._DiffAPV.Size = new System.Drawing.Size(15, 14);
            this._DiffAPV.TabIndex = 37;
            this._DiffAPV.UseVisualStyleBackColor = true;
            // 
            // _diffUROV
            // 
            this._diffUROV.AutoSize = true;
            this._diffUROV.Location = new System.Drawing.Point(147, 392);
            this._diffUROV.Name = "_diffUROV";
            this._diffUROV.Size = new System.Drawing.Size(15, 14);
            this._diffUROV.TabIndex = 37;
            this._diffUROV.UseVisualStyleBackColor = true;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(13, 432);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(28, 13);
            this.label131.TabIndex = 45;
            this.label131.Text = "АВР";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(12, 412);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(29, 13);
            this.label130.TabIndex = 43;
            this.label130.Text = "АПВ";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._crossBlockI5I1);
            this.groupBox25.Controls.Add(this._modeI5I1);
            this.groupBox25.Controls.Add(this.label24);
            this.groupBox25.Controls.Add(this._I5I1TextBox);
            this.groupBox25.Controls.Add(this.label75);
            this.groupBox25.Location = new System.Drawing.Point(6, 178);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(261, 68);
            this.groupBox25.TabIndex = 41;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Блокировка I5/I1";
            // 
            // _crossBlockI5I1
            // 
            this._crossBlockI5I1.AutoSize = true;
            this._crossBlockI5I1.Location = new System.Drawing.Point(170, 19);
            this._crossBlockI5I1.Name = "_crossBlockI5I1";
            this._crossBlockI5I1.Size = new System.Drawing.Size(15, 14);
            this._crossBlockI5I1.TabIndex = 37;
            this._crossBlockI5I1.UseVisualStyleBackColor = true;
            // 
            // _modeI5I1
            // 
            this._modeI5I1.AutoSize = true;
            this._modeI5I1.Location = new System.Drawing.Point(102, 1);
            this._modeI5I1.Name = "_modeI5I1";
            this._modeI5I1.Size = new System.Drawing.Size(15, 14);
            this._modeI5I1.TabIndex = 37;
            this._modeI5I1.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 13);
            this.label24.TabIndex = 41;
            this.label24.Text = "Перекр. блок.";
            // 
            // _I5I1TextBox
            // 
            this._I5I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I5I1TextBox.Location = new System.Drawing.Point(170, 39);
            this._I5I1TextBox.Name = "_I5I1TextBox";
            this._I5I1TextBox.Size = new System.Drawing.Size(61, 20);
            this._I5I1TextBox.TabIndex = 37;
            this._I5I1TextBox.Tag = "100";
            this._I5I1TextBox.Text = "0";
            this._I5I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(6, 41);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(158, 13);
            this.label75.TabIndex = 38;
            this.label75.Text = "I5/I1 (от перевозбуждения), %";
            // 
            // _oscDTZComboBox
            // 
            this._oscDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTZComboBox.FormattingEnabled = true;
            this._oscDTZComboBox.Location = new System.Drawing.Point(148, 365);
            this._oscDTZComboBox.Name = "_oscDTZComboBox";
            this._oscDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTZComboBox.TabIndex = 29;
            // 
            // _blockingDTZComboBox
            // 
            this._blockingDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTZComboBox.FormattingEnabled = true;
            this._blockingDTZComboBox.Location = new System.Drawing.Point(148, 33);
            this._blockingDTZComboBox.Name = "_blockingDTZComboBox";
            this._blockingDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._blockingDTZComboBox.TabIndex = 27;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(12, 368);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(76, 13);
            this.label76.TabIndex = 26;
            this.label76.Text = "Осциллограф";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(12, 392);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(37, 13);
            this.label77.TabIndex = 25;
            this.label77.Text = "УРОВ";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(12, 36);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(68, 13);
            this.label78.TabIndex = 24;
            this.label78.Text = "Блокировка";
            // 
            // _timeEnduranceDTZTextBox
            // 
            this._timeEnduranceDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTZTextBox.Location = new System.Drawing.Point(148, 72);
            this._timeEnduranceDTZTextBox.Name = "_timeEnduranceDTZTextBox";
            this._timeEnduranceDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTZTextBox.TabIndex = 14;
            this._timeEnduranceDTZTextBox.Tag = "3276700";
            this._timeEnduranceDTZTextBox.Text = "0";
            this._timeEnduranceDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._crossBlockI2I1);
            this.groupBox9.Controls.Add(this._modeI2I1);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this._I2I1TextBox);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Location = new System.Drawing.Point(6, 104);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(261, 68);
            this.groupBox9.TabIndex = 23;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Блокировка I2/I1";
            // 
            // _crossBlockI2I1
            // 
            this._crossBlockI2I1.AutoSize = true;
            this._crossBlockI2I1.Location = new System.Drawing.Point(170, 19);
            this._crossBlockI2I1.Name = "_crossBlockI2I1";
            this._crossBlockI2I1.Size = new System.Drawing.Size(15, 14);
            this._crossBlockI2I1.TabIndex = 37;
            this._crossBlockI2I1.UseVisualStyleBackColor = true;
            // 
            // _modeI2I1
            // 
            this._modeI2I1.AutoSize = true;
            this._modeI2I1.Location = new System.Drawing.Point(102, 0);
            this._modeI2I1.Name = "_modeI2I1";
            this._modeI2I1.Size = new System.Drawing.Size(15, 14);
            this._modeI2I1.TabIndex = 37;
            this._modeI2I1.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 30;
            this.label23.Text = "Перекр. блок";
            // 
            // _I2I1TextBox
            // 
            this._I2I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2I1TextBox.Location = new System.Drawing.Point(170, 39);
            this._I2I1TextBox.Name = "_I2I1TextBox";
            this._I2I1TextBox.Size = new System.Drawing.Size(62, 20);
            this._I2I1TextBox.TabIndex = 19;
            this._I2I1TextBox.Tag = "100";
            this._I2I1TextBox.Text = "0";
            this._I2I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 41);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(89, 13);
            this.label73.TabIndex = 22;
            this.label73.Text = "I2/I1 (от БТН), %";
            // 
            // _constraintDTZTextBox
            // 
            this._constraintDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTZTextBox.Location = new System.Drawing.Point(148, 53);
            this._constraintDTZTextBox.Name = "_constraintDTZTextBox";
            this._constraintDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTZTextBox.TabIndex = 13;
            this._constraintDTZTextBox.Tag = "40";
            this._constraintDTZTextBox.Text = "0";
            this._constraintDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._K2TangensTextBox);
            this.groupBox8.Controls.Add(this._Ib2BeginTextBox);
            this.groupBox8.Controls.Add(this._K1AngleOfSlopeTextBox);
            this.groupBox8.Controls.Add(this._Ib1BeginTextBox);
            this.groupBox8.Controls.Add(this.label65);
            this.groupBox8.Controls.Add(this.label64);
            this.groupBox8.Controls.Add(this.label63);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Location = new System.Drawing.Point(6, 252);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(261, 107);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Характеристика торможения";
            // 
            // _K1AngleOfSlopeTextBox
            // 
            this._K1AngleOfSlopeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K1AngleOfSlopeTextBox.Location = new System.Drawing.Point(141, 40);
            this._K1AngleOfSlopeTextBox.Name = "_K1AngleOfSlopeTextBox";
            this._K1AngleOfSlopeTextBox.Size = new System.Drawing.Size(90, 20);
            this._K1AngleOfSlopeTextBox.TabIndex = 16;
            this._K1AngleOfSlopeTextBox.Tag = "89";
            this._K1AngleOfSlopeTextBox.Text = "0";
            this._K1AngleOfSlopeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Ib1BeginTextBox
            // 
            this._Ib1BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib1BeginTextBox.Location = new System.Drawing.Point(141, 21);
            this._Ib1BeginTextBox.Name = "_Ib1BeginTextBox";
            this._Ib1BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib1BeginTextBox.TabIndex = 15;
            this._Ib1BeginTextBox.Tag = "40";
            this._Ib1BeginTextBox.Text = "0";
            this._Ib1BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 81);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(100, 13);
            this.label65.TabIndex = 5;
            this.label65.Text = "f2 - угол наклона, \'";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 62);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(87, 13);
            this.label64.TabIndex = 4;
            this.label64.Text = "Iб2 - начало, Iдв";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 43);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(100, 13);
            this.label63.TabIndex = 3;
            this.label63.Text = "f1 - угол наклона, \'";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 24);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(87, 13);
            this.label62.TabIndex = 2;
            this.label62.Text = "Iб1 - начало, Iдв";
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this._defPdgv);
            this.tabPage20.Location = new System.Drawing.Point(4, 25);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Size = new System.Drawing.Size(858, 462);
            this.tabPage20.TabIndex = 12;
            this.tabPage20.Text = "Защ. P";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // _defPdgv
            // 
            this._defPdgv.AllowUserToAddRows = false;
            this._defPdgv.AllowUserToDeleteRows = false;
            this._defPdgv.AllowUserToResizeColumns = false;
            this._defPdgv.AllowUserToResizeRows = false;
            this._defPdgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle71.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle71.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle71.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle71.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle71.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._defPdgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle71;
            this._defPdgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defPdgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewComboBoxColumn52,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.Column13,
            this.Column7,
            this.dataGridViewTextBoxColumn48,
            this.Column8,
            this.Column12,
            this.Column9,
            this.Column10,
            this.dataGridViewCheckBoxColumn24,
            this.dataGridViewCheckBoxColumn25,
            this.dataGridViewCheckBoxColumn26,
            this.Column11});
            this._defPdgv.Dock = System.Windows.Forms.DockStyle.Top;
            this._defPdgv.Location = new System.Drawing.Point(0, 0);
            this._defPdgv.MultiSelect = false;
            this._defPdgv.Name = "_defPdgv";
            this._defPdgv.RowHeadersVisible = false;
            this._defPdgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defPdgv.RowTemplate.Height = 24;
            this._defPdgv.ShowCellErrors = false;
            this._defPdgv.ShowRowErrors = false;
            this._defPdgv.Size = new System.Drawing.Size(858, 97);
            this._defPdgv.TabIndex = 6;
            // 
            // dataGridViewTextBoxColumn43
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle72.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle72.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle72.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle72.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn43.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewTextBoxColumn43.Frozen = true;
            this.dataGridViewTextBoxColumn43.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn43.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn43.Width = 55;
            // 
            // dataGridViewComboBoxColumn52
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn52.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewComboBoxColumn52.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn52.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn52.Name = "dataGridViewComboBoxColumn52";
            this.dataGridViewComboBoxColumn52.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn52.Width = 80;
            // 
            // dataGridViewTextBoxColumn44
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn44.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewTextBoxColumn44.HeaderText = "Sср, Sн";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn44.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn44.Width = 60;
            // 
            // dataGridViewTextBoxColumn45
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn45.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewTextBoxColumn45.HeaderText = "Уср, град.";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn45.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn45.Width = 65;
            // 
            // dataGridViewTextBoxColumn46
            // 
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn46.DefaultCellStyle = dataGridViewCellStyle76;
            this.dataGridViewTextBoxColumn46.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn46.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn46.Width = 50;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Возврат";
            this.Column13.Name = "Column13";
            this.Column13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column13.Width = 60;
            // 
            // Column7
            // 
            dataGridViewCellStyle149.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle149;
            this.Column7.HeaderText = "Sвз, Sн";
            this.Column7.Name = "Column7";
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 70;
            // 
            // dataGridViewTextBoxColumn48
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn48.DefaultCellStyle = dataGridViewCellStyle77;
            this.dataGridViewTextBoxColumn48.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn48.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn48.Width = 60;
            // 
            // Column8
            // 
            dataGridViewCellStyle150.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle150;
            this.Column8.HeaderText = "Iср, Iн";
            this.Column8.Name = "Column8";
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 55;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Блокировка";
            this.Column12.Name = "Column12";
            this.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column12.Width = 90;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Осциллограф";
            this.Column9.Name = "Column9";
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "АПВвозвр";
            this.Column10.Name = "Column10";
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column10.Width = 60;
            // 
            // dataGridViewCheckBoxColumn24
            // 
            this.dataGridViewCheckBoxColumn24.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn24.Name = "dataGridViewCheckBoxColumn24";
            this.dataGridViewCheckBoxColumn24.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn24.Width = 50;
            // 
            // dataGridViewCheckBoxColumn25
            // 
            this.dataGridViewCheckBoxColumn25.HeaderText = "АПВ";
            this.dataGridViewCheckBoxColumn25.Name = "dataGridViewCheckBoxColumn25";
            this.dataGridViewCheckBoxColumn25.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn25.Width = 50;
            // 
            // dataGridViewCheckBoxColumn26
            // 
            this.dataGridViewCheckBoxColumn26.HeaderText = "Сброс";
            this.dataGridViewCheckBoxColumn26.Name = "dataGridViewCheckBoxColumn26";
            this.dataGridViewCheckBoxColumn26.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn26.Width = 45;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "АВР";
            this.Column11.Name = "Column11";
            this.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column11.Width = 50;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.groupBox23);
            this.tabPage23.Controls.Add(this.groupBox22);
            this.tabPage23.Location = new System.Drawing.Point(4, 25);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Size = new System.Drawing.Size(858, 462);
            this.tabPage23.TabIndex = 6;
            this.tabPage23.Text = "Защ. U";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this._defensesUMDataGrid);
            this.groupBox23.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox23.Location = new System.Drawing.Point(0, 167);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(858, 166);
            this.groupBox23.TabIndex = 5;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Защиты U<";
            // 
            // _defensesUMDataGrid
            // 
            this._defensesUMDataGrid.AllowUserToAddRows = false;
            this._defensesUMDataGrid.AllowUserToDeleteRows = false;
            this._defensesUMDataGrid.AllowUserToResizeColumns = false;
            this._defensesUMDataGrid.AllowUserToResizeRows = false;
            this._defensesUMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._defensesUMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defensesUMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewComboBoxColumn38,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewComboBoxColumn39,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewCheckBoxColumn12,
            this.dataGridViewComboBoxColumn40,
            this.dataGridViewComboBoxColumn41,
            this.dataGridViewCheckBoxColumn16,
            this.dataGridViewCheckBoxColumn17,
            this.dataGridViewCheckBoxColumn18,
            this.dataGridViewCheckBoxColumn19,
            this.dataGridViewCheckBoxColumn20});
            this._defensesUMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._defensesUMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._defensesUMDataGrid.MultiSelect = false;
            this._defensesUMDataGrid.Name = "_defensesUMDataGrid";
            this._defensesUMDataGrid.RowHeadersVisible = false;
            this._defensesUMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defensesUMDataGrid.RowTemplate.Height = 24;
            this._defensesUMDataGrid.ShowCellErrors = false;
            this._defensesUMDataGrid.ShowRowErrors = false;
            this._defensesUMDataGrid.Size = new System.Drawing.Size(852, 147);
            this._defensesUMDataGrid.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle78.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle78.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle78;
            this.dataGridViewTextBoxColumn31.Frozen = true;
            this.dataGridViewTextBoxColumn31.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn31.Width = 80;
            // 
            // dataGridViewComboBoxColumn38
            // 
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn38.DefaultCellStyle = dataGridViewCellStyle79;
            this.dataGridViewComboBoxColumn38.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn38.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn38.Name = "dataGridViewComboBoxColumn38";
            this.dataGridViewComboBoxColumn38.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.HeaderText = "Column5";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Visible = false;
            // 
            // dataGridViewComboBoxColumn39
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn39.DefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridViewComboBoxColumn39.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn39.Name = "dataGridViewComboBoxColumn39";
            this.dataGridViewComboBoxColumn39.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn39.Width = 90;
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle81;
            this.dataGridViewTextBoxColumn34.HeaderText = "Uср, В";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn34.Width = 60;
            // 
            // dataGridViewTextBoxColumn35
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn35.DefaultCellStyle = dataGridViewCellStyle82;
            this.dataGridViewTextBoxColumn35.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn35.Width = 70;
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn36.DefaultCellStyle = dataGridViewCellStyle83;
            this.dataGridViewTextBoxColumn36.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn36.Width = 70;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle84.NullValue = false;
            this.dataGridViewCheckBoxColumn11.DefaultCellStyle = dataGridViewCellStyle84;
            this.dataGridViewCheckBoxColumn11.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn11.Width = 70;
            // 
            // dataGridViewTextBoxColumn37
            // 
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle85;
            this.dataGridViewTextBoxColumn37.HeaderText = "Uвз, В";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn37.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn37.Width = 75;
            // 
            // dataGridViewCheckBoxColumn12
            // 
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle86.NullValue = false;
            this.dataGridViewCheckBoxColumn12.DefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridViewCheckBoxColumn12.HeaderText = "Блокировка U<5В";
            this.dataGridViewCheckBoxColumn12.Name = "dataGridViewCheckBoxColumn12";
            this.dataGridViewCheckBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn12.Width = 110;
            // 
            // dataGridViewComboBoxColumn40
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn40.DefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridViewComboBoxColumn40.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn40.Name = "dataGridViewComboBoxColumn40";
            this.dataGridViewComboBoxColumn40.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn40.Width = 90;
            // 
            // dataGridViewComboBoxColumn41
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn41.DefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridViewComboBoxColumn41.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn41.Name = "dataGridViewComboBoxColumn41";
            this.dataGridViewComboBoxColumn41.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn41.Width = 110;
            // 
            // dataGridViewCheckBoxColumn16
            // 
            this.dataGridViewCheckBoxColumn16.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn16.Name = "dataGridViewCheckBoxColumn16";
            this.dataGridViewCheckBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn16.Width = 80;
            // 
            // dataGridViewCheckBoxColumn17
            // 
            this.dataGridViewCheckBoxColumn17.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn17.Name = "dataGridViewCheckBoxColumn17";
            this.dataGridViewCheckBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn17.Width = 60;
            // 
            // dataGridViewCheckBoxColumn18
            // 
            this.dataGridViewCheckBoxColumn18.HeaderText = "АПВ";
            this.dataGridViewCheckBoxColumn18.Name = "dataGridViewCheckBoxColumn18";
            this.dataGridViewCheckBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn18.Width = 40;
            // 
            // dataGridViewCheckBoxColumn19
            // 
            this.dataGridViewCheckBoxColumn19.HeaderText = "АВР";
            this.dataGridViewCheckBoxColumn19.Name = "dataGridViewCheckBoxColumn19";
            this.dataGridViewCheckBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn19.Width = 40;
            // 
            // dataGridViewCheckBoxColumn20
            // 
            this.dataGridViewCheckBoxColumn20.HeaderText = "Сброс";
            this.dataGridViewCheckBoxColumn20.Name = "dataGridViewCheckBoxColumn20";
            this.dataGridViewCheckBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn20.Width = 60;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this._defensesUBDataGrid);
            this.groupBox22.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox22.Location = new System.Drawing.Point(0, 0);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(858, 167);
            this.groupBox22.TabIndex = 4;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Защиты U>";
            // 
            // _defensesUBDataGrid
            // 
            this._defensesUBDataGrid.AllowUserToAddRows = false;
            this._defensesUBDataGrid.AllowUserToDeleteRows = false;
            this._defensesUBDataGrid.AllowUserToResizeColumns = false;
            this._defensesUBDataGrid.AllowUserToResizeRows = false;
            this._defensesUBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._defensesUBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defensesUBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._uBStageColumn,
            this._uBModesColumn,
            this._uBTypeColumn,
            this.Column5,
            this._uBUsrColumn,
            this._uBTsrColumn,
            this._uBTvzColumn,
            this._uBUvzYNColumn,
            this._uBUvzColumn,
            this._uBBlockingUMColumn,
            this._uBBlockingColumn,
            this._uBOscColumn,
            this._uBAPVRetColumn,
            this._uBUROVColumn,
            this._uBAPVColumn,
            this._uBAVRColumn,
            this._uBSbrosColumn});
            this._defensesUBDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._defensesUBDataGrid.Location = new System.Drawing.Point(3, 16);
            this._defensesUBDataGrid.MultiSelect = false;
            this._defensesUBDataGrid.Name = "_defensesUBDataGrid";
            this._defensesUBDataGrid.RowHeadersVisible = false;
            this._defensesUBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defensesUBDataGrid.RowTemplate.Height = 24;
            this._defensesUBDataGrid.ShowCellErrors = false;
            this._defensesUBDataGrid.ShowRowErrors = false;
            this._defensesUBDataGrid.Size = new System.Drawing.Size(852, 148);
            this._defensesUBDataGrid.TabIndex = 3;
            // 
            // _uBStageColumn
            // 
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle89.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle89.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle89.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle89.SelectionForeColor = System.Drawing.Color.White;
            this._uBStageColumn.DefaultCellStyle = dataGridViewCellStyle89;
            this._uBStageColumn.Frozen = true;
            this._uBStageColumn.HeaderText = "Ступень";
            this._uBStageColumn.Name = "_uBStageColumn";
            this._uBStageColumn.ReadOnly = true;
            this._uBStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBStageColumn.Width = 80;
            // 
            // _uBModesColumn
            // 
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBModesColumn.DefaultCellStyle = dataGridViewCellStyle90;
            this._uBModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._uBModesColumn.HeaderText = "Режим";
            this._uBModesColumn.Name = "_uBModesColumn";
            this._uBModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _uBTypeColumn
            // 
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTypeColumn.DefaultCellStyle = dataGridViewCellStyle91;
            this._uBTypeColumn.HeaderText = "Тип";
            this._uBTypeColumn.Name = "_uBTypeColumn";
            this._uBTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBTypeColumn.Width = 90;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            // 
            // _uBUsrColumn
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBUsrColumn.DefaultCellStyle = dataGridViewCellStyle92;
            this._uBUsrColumn.HeaderText = "Uср, В";
            this._uBUsrColumn.Name = "_uBUsrColumn";
            this._uBUsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBUsrColumn.Width = 60;
            // 
            // _uBTsrColumn
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTsrColumn.DefaultCellStyle = dataGridViewCellStyle93;
            this._uBTsrColumn.HeaderText = "tср, мс";
            this._uBTsrColumn.Name = "_uBTsrColumn";
            this._uBTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBTsrColumn.Width = 70;
            // 
            // _uBTvzColumn
            // 
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTvzColumn.DefaultCellStyle = dataGridViewCellStyle94;
            this._uBTvzColumn.HeaderText = "tвз, мс";
            this._uBTvzColumn.Name = "_uBTvzColumn";
            this._uBTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBTvzColumn.Width = 70;
            // 
            // _uBUvzYNColumn
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle95.NullValue = false;
            this._uBUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle95;
            this._uBUvzYNColumn.HeaderText = "Возврат";
            this._uBUvzYNColumn.Name = "_uBUvzYNColumn";
            this._uBUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUvzYNColumn.Width = 70;
            // 
            // _uBUvzColumn
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBUvzColumn.DefaultCellStyle = dataGridViewCellStyle96;
            this._uBUvzColumn.HeaderText = "Uвз, В";
            this._uBUvzColumn.Name = "_uBUvzColumn";
            this._uBUvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBUvzColumn.Width = 75;
            // 
            // _uBBlockingUMColumn
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle97.NullValue = false;
            this._uBBlockingUMColumn.DefaultCellStyle = dataGridViewCellStyle97;
            this._uBBlockingUMColumn.HeaderText = "Блокировка U<5В";
            this._uBBlockingUMColumn.Name = "_uBBlockingUMColumn";
            this._uBBlockingUMColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBBlockingUMColumn.Width = 110;
            // 
            // _uBBlockingColumn
            // 
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBBlockingColumn.DefaultCellStyle = dataGridViewCellStyle98;
            this._uBBlockingColumn.HeaderText = "Блокировка";
            this._uBBlockingColumn.Name = "_uBBlockingColumn";
            this._uBBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBBlockingColumn.Width = 90;
            // 
            // _uBOscColumn
            // 
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBOscColumn.DefaultCellStyle = dataGridViewCellStyle99;
            this._uBOscColumn.HeaderText = "Осциллограф";
            this._uBOscColumn.Name = "_uBOscColumn";
            this._uBOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBOscColumn.Width = 110;
            // 
            // _uBAPVRetColumn
            // 
            this._uBAPVRetColumn.HeaderText = "АПВ возвр.";
            this._uBAPVRetColumn.Name = "_uBAPVRetColumn";
            this._uBAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBAPVRetColumn.Width = 80;
            // 
            // _uBUROVColumn
            // 
            this._uBUROVColumn.HeaderText = "УРОВ";
            this._uBUROVColumn.Name = "_uBUROVColumn";
            this._uBUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUROVColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._uBUROVColumn.Width = 60;
            // 
            // _uBAPVColumn
            // 
            this._uBAPVColumn.HeaderText = "АПВ";
            this._uBAPVColumn.Name = "_uBAPVColumn";
            this._uBAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBAPVColumn.Width = 40;
            // 
            // _uBAVRColumn
            // 
            this._uBAVRColumn.HeaderText = "АВР";
            this._uBAVRColumn.Name = "_uBAVRColumn";
            this._uBAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBAVRColumn.Width = 40;
            // 
            // _uBSbrosColumn
            // 
            this._uBSbrosColumn.HeaderText = "Сброс";
            this._uBSbrosColumn.Name = "_uBSbrosColumn";
            this._uBSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBSbrosColumn.Width = 60;
            // 
            // tabPage26
            // 
            this.tabPage26.Controls.Add(this.groupBox19);
            this.tabPage26.Controls.Add(this.groupBox27);
            this.tabPage26.Location = new System.Drawing.Point(4, 25);
            this.tabPage26.Name = "tabPage26";
            this.tabPage26.Size = new System.Drawing.Size(858, 462);
            this.tabPage26.TabIndex = 9;
            this.tabPage26.Text = "Защ. F";
            this.tabPage26.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._difensesFMDataGrid);
            this.groupBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox19.Location = new System.Drawing.Point(0, 167);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(858, 167);
            this.groupBox19.TabIndex = 6;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Защиты F>";
            // 
            // _difensesFMDataGrid
            // 
            this._difensesFMDataGrid.AllowUserToAddRows = false;
            this._difensesFMDataGrid.AllowUserToDeleteRows = false;
            this._difensesFMDataGrid.AllowUserToResizeColumns = false;
            this._difensesFMDataGrid.AllowUserToResizeRows = false;
            this._difensesFMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewComboBoxColumn42,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewCheckBoxColumn21,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewComboBoxColumn45,
            this.dataGridViewComboBoxColumn46,
            this.dataGridViewComboBoxColumn50,
            this.dataGridViewComboBoxColumn47,
            this.dataGridViewComboBoxColumn48,
            this.dataGridViewComboBoxColumn49,
            this.dataGridViewComboBoxColumn51});
            this._difensesFMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesFMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesFMDataGrid.MultiSelect = false;
            this._difensesFMDataGrid.Name = "_difensesFMDataGrid";
            this._difensesFMDataGrid.RowHeadersVisible = false;
            this._difensesFMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFMDataGrid.RowTemplate.Height = 24;
            this._difensesFMDataGrid.ShowCellErrors = false;
            this._difensesFMDataGrid.ShowRowErrors = false;
            this._difensesFMDataGrid.Size = new System.Drawing.Size(852, 148);
            this._difensesFMDataGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn38
            // 
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn38.DefaultCellStyle = dataGridViewCellStyle100;
            this.dataGridViewTextBoxColumn38.Frozen = true;
            this.dataGridViewTextBoxColumn38.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn38.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn38.Width = 80;
            // 
            // dataGridViewComboBoxColumn42
            // 
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn42.DefaultCellStyle = dataGridViewCellStyle101;
            this.dataGridViewComboBoxColumn42.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn42.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn42.Name = "dataGridViewComboBoxColumn42";
            this.dataGridViewComboBoxColumn42.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn42.Width = 80;
            // 
            // dataGridViewTextBoxColumn39
            // 
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridViewTextBoxColumn39.HeaderText = "Fср, Гц";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn39.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn39.Width = 70;
            // 
            // dataGridViewTextBoxColumn40
            // 
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn40.DefaultCellStyle = dataGridViewCellStyle103;
            this.dataGridViewTextBoxColumn40.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn40.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn40.Width = 70;
            // 
            // dataGridViewTextBoxColumn41
            // 
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridViewTextBoxColumn41.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn41.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn41.Width = 70;
            // 
            // dataGridViewCheckBoxColumn21
            // 
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle105.NullValue = false;
            this.dataGridViewCheckBoxColumn21.DefaultCellStyle = dataGridViewCellStyle105;
            this.dataGridViewCheckBoxColumn21.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn21.Name = "dataGridViewCheckBoxColumn21";
            this.dataGridViewCheckBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn21.Width = 80;
            // 
            // dataGridViewTextBoxColumn42
            // 
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle106;
            this.dataGridViewTextBoxColumn42.HeaderText = "Fвз, Гц";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn42.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn42.Width = 75;
            // 
            // dataGridViewComboBoxColumn45
            // 
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn45.DefaultCellStyle = dataGridViewCellStyle107;
            this.dataGridViewComboBoxColumn45.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn45.Name = "dataGridViewComboBoxColumn45";
            this.dataGridViewComboBoxColumn45.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn45.Width = 90;
            // 
            // dataGridViewComboBoxColumn46
            // 
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn46.DefaultCellStyle = dataGridViewCellStyle108;
            this.dataGridViewComboBoxColumn46.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn46.Name = "dataGridViewComboBoxColumn46";
            this.dataGridViewComboBoxColumn46.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn46.Width = 90;
            // 
            // dataGridViewComboBoxColumn50
            // 
            this.dataGridViewComboBoxColumn50.HeaderText = "АПВ возвр.";
            this.dataGridViewComboBoxColumn50.Name = "dataGridViewComboBoxColumn50";
            this.dataGridViewComboBoxColumn50.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewComboBoxColumn47
            // 
            this.dataGridViewComboBoxColumn47.HeaderText = "УРОВ";
            this.dataGridViewComboBoxColumn47.Name = "dataGridViewComboBoxColumn47";
            this.dataGridViewComboBoxColumn47.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn47.Width = 60;
            // 
            // dataGridViewComboBoxColumn48
            // 
            this.dataGridViewComboBoxColumn48.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn48.Name = "dataGridViewComboBoxColumn48";
            this.dataGridViewComboBoxColumn48.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn48.Width = 40;
            // 
            // dataGridViewComboBoxColumn49
            // 
            this.dataGridViewComboBoxColumn49.HeaderText = "АВР";
            this.dataGridViewComboBoxColumn49.Name = "dataGridViewComboBoxColumn49";
            this.dataGridViewComboBoxColumn49.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn49.Width = 40;
            // 
            // dataGridViewComboBoxColumn51
            // 
            this.dataGridViewComboBoxColumn51.HeaderText = "Сброс";
            this.dataGridViewComboBoxColumn51.Name = "dataGridViewComboBoxColumn51";
            this.dataGridViewComboBoxColumn51.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn51.Width = 60;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this._difensesFBDataGrid);
            this.groupBox27.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox27.Location = new System.Drawing.Point(0, 0);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(858, 167);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Защиты F>";
            // 
            // _difensesFBDataGrid
            // 
            this._difensesFBDataGrid.AllowUserToAddRows = false;
            this._difensesFBDataGrid.AllowUserToDeleteRows = false;
            this._difensesFBDataGrid.AllowUserToResizeColumns = false;
            this._difensesFBDataGrid.AllowUserToResizeRows = false;
            this._difensesFBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._fBStageColumn,
            this._fBModesColumn,
            this._fBUsrColumn,
            this._fBTsrColumn,
            this._fBTvzColumn,
            this._fBUvzYNColumn,
            this._fBUvzColumn,
            this._fBBlockingColumn,
            this._fBOscColumn,
            this._fBAPVRetColumn,
            this._fBUROVColumn,
            this._fBAPVColumn,
            this._fBAVRColumn,
            this._fBSbrosColumn});
            this._difensesFBDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesFBDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesFBDataGrid.MultiSelect = false;
            this._difensesFBDataGrid.Name = "_difensesFBDataGrid";
            this._difensesFBDataGrid.RowHeadersVisible = false;
            this._difensesFBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFBDataGrid.RowTemplate.Height = 24;
            this._difensesFBDataGrid.ShowCellErrors = false;
            this._difensesFBDataGrid.ShowRowErrors = false;
            this._difensesFBDataGrid.Size = new System.Drawing.Size(852, 148);
            this._difensesFBDataGrid.TabIndex = 3;
            // 
            // _fBStageColumn
            // 
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle109.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle109.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle109.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle109.SelectionForeColor = System.Drawing.Color.White;
            this._fBStageColumn.DefaultCellStyle = dataGridViewCellStyle109;
            this._fBStageColumn.Frozen = true;
            this._fBStageColumn.HeaderText = "Ступень";
            this._fBStageColumn.Name = "_fBStageColumn";
            this._fBStageColumn.ReadOnly = true;
            this._fBStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBStageColumn.Width = 80;
            // 
            // _fBModesColumn
            // 
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBModesColumn.DefaultCellStyle = dataGridViewCellStyle110;
            this._fBModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._fBModesColumn.HeaderText = "Режим";
            this._fBModesColumn.Name = "_fBModesColumn";
            this._fBModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBModesColumn.Width = 80;
            // 
            // _fBUsrColumn
            // 
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBUsrColumn.DefaultCellStyle = dataGridViewCellStyle111;
            this._fBUsrColumn.HeaderText = "Fср, Гц";
            this._fBUsrColumn.Name = "_fBUsrColumn";
            this._fBUsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBUsrColumn.Width = 70;
            // 
            // _fBTsrColumn
            // 
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBTsrColumn.DefaultCellStyle = dataGridViewCellStyle112;
            this._fBTsrColumn.HeaderText = "tср, мс";
            this._fBTsrColumn.Name = "_fBTsrColumn";
            this._fBTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBTsrColumn.Width = 70;
            // 
            // _fBTvzColumn
            // 
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBTvzColumn.DefaultCellStyle = dataGridViewCellStyle113;
            this._fBTvzColumn.HeaderText = "tвз, мс";
            this._fBTvzColumn.Name = "_fBTvzColumn";
            this._fBTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBTvzColumn.Width = 70;
            // 
            // _fBUvzYNColumn
            // 
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle114.NullValue = false;
            this._fBUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle114;
            this._fBUvzYNColumn.HeaderText = "Возврат";
            this._fBUvzYNColumn.Name = "_fBUvzYNColumn";
            this._fBUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBUvzYNColumn.Width = 80;
            // 
            // _fBUvzColumn
            // 
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBUvzColumn.DefaultCellStyle = dataGridViewCellStyle115;
            this._fBUvzColumn.HeaderText = "Fвз, Гц";
            this._fBUvzColumn.Name = "_fBUvzColumn";
            this._fBUvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBUvzColumn.Width = 75;
            // 
            // _fBBlockingColumn
            // 
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBBlockingColumn.DefaultCellStyle = dataGridViewCellStyle116;
            this._fBBlockingColumn.HeaderText = "Блокировка";
            this._fBBlockingColumn.Name = "_fBBlockingColumn";
            this._fBBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBBlockingColumn.Width = 90;
            // 
            // _fBOscColumn
            // 
            dataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBOscColumn.DefaultCellStyle = dataGridViewCellStyle117;
            this._fBOscColumn.HeaderText = "Осциллограф";
            this._fBOscColumn.Name = "_fBOscColumn";
            this._fBOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBOscColumn.Width = 90;
            // 
            // _fBAPVRetColumn
            // 
            this._fBAPVRetColumn.HeaderText = "АПВ возвр.";
            this._fBAPVRetColumn.Name = "_fBAPVRetColumn";
            this._fBAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _fBUROVColumn
            // 
            this._fBUROVColumn.HeaderText = "УРОВ";
            this._fBUROVColumn.Name = "_fBUROVColumn";
            this._fBUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBUROVColumn.Width = 60;
            // 
            // _fBAPVColumn
            // 
            this._fBAPVColumn.HeaderText = "АПВ";
            this._fBAPVColumn.Name = "_fBAPVColumn";
            this._fBAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBAPVColumn.Width = 40;
            // 
            // _fBAVRColumn
            // 
            this._fBAVRColumn.HeaderText = "АВР";
            this._fBAVRColumn.Name = "_fBAVRColumn";
            this._fBAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBAVRColumn.Width = 40;
            // 
            // _fBSbrosColumn
            // 
            this._fBSbrosColumn.HeaderText = "Сброс";
            this._fBSbrosColumn.Name = "_fBSbrosColumn";
            this._fBSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._fBSbrosColumn.Width = 60;
            // 
            // tabPage28
            // 
            this.tabPage28.Controls.Add(this.groupBox39);
            this.tabPage28.Controls.Add(this.groupBox38);
            this.tabPage28.Controls.Add(this._defenseQgrid);
            this.tabPage28.Location = new System.Drawing.Point(4, 25);
            this.tabPage28.Name = "tabPage28";
            this.tabPage28.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage28.Size = new System.Drawing.Size(858, 462);
            this.tabPage28.TabIndex = 11;
            this.tabPage28.Text = "Блок-ки пусков и защ.Q";
            this.tabPage28.UseVisualStyleBackColor = true;
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._blockNpusk);
            this.groupBox39.Controls.Add(this._blockNt);
            this.groupBox39.Controls.Add(this._blockNhot);
            this.groupBox39.Controls.Add(this.label25);
            this.groupBox39.Controls.Add(this.label36);
            this.groupBox39.Controls.Add(this.label37);
            this.groupBox39.Location = new System.Drawing.Point(182, 6);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(170, 87);
            this.groupBox39.TabIndex = 13;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Блокировка по N";
            // 
            // _blockNpusk
            // 
            this._blockNpusk.Location = new System.Drawing.Point(54, 20);
            this._blockNpusk.Name = "_blockNpusk";
            this._blockNpusk.Size = new System.Drawing.Size(100, 20);
            this._blockNpusk.TabIndex = 2;
            // 
            // _blockNt
            // 
            this._blockNt.Location = new System.Drawing.Point(54, 58);
            this._blockNt.Name = "_blockNt";
            this._blockNt.Size = new System.Drawing.Size(100, 20);
            this._blockNt.TabIndex = 2;
            // 
            // _blockNhot
            // 
            this._blockNhot.Location = new System.Drawing.Point(54, 39);
            this._blockNhot.Name = "_blockNhot";
            this._blockNhot.Size = new System.Drawing.Size(100, 20);
            this._blockNhot.TabIndex = 2;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 61);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "t, c";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 42);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(32, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Nгор";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 23);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(38, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Nпуск";
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this._blockQt);
            this.groupBox38.Controls.Add(this._blockQust);
            this.groupBox38.Controls.Add(this.label20);
            this.groupBox38.Controls.Add(this.label19);
            this.groupBox38.Controls.Add(this.label18);
            this.groupBox38.Controls.Add(this._blockQmode);
            this.groupBox38.Location = new System.Drawing.Point(6, 6);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(170, 87);
            this.groupBox38.TabIndex = 13;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Блокировка по Q";
            // 
            // _blockQt
            // 
            this._blockQt.Location = new System.Drawing.Point(54, 58);
            this._blockQt.Name = "_blockQt";
            this._blockQt.Size = new System.Drawing.Size(100, 20);
            this._blockQt.TabIndex = 2;
            // 
            // _blockQust
            // 
            this._blockQust.Location = new System.Drawing.Point(54, 39);
            this._blockQust.Name = "_blockQust";
            this._blockQust.Size = new System.Drawing.Size(100, 20);
            this._blockQust.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "t, c";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Q, %";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Режим";
            // 
            // _blockQmode
            // 
            this._blockQmode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockQmode.FormattingEnabled = true;
            this._blockQmode.Location = new System.Drawing.Point(54, 19);
            this._blockQmode.Name = "_blockQmode";
            this._blockQmode.Size = new System.Drawing.Size(100, 21);
            this._blockQmode.TabIndex = 0;
            // 
            // _defenseQgrid
            // 
            this._defenseQgrid.AllowUserToAddRows = false;
            this._defenseQgrid.AllowUserToDeleteRows = false;
            this._defenseQgrid.AllowUserToResizeColumns = false;
            this._defenseQgrid.AllowUserToResizeRows = false;
            this._defenseQgrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._defenseQgrid.BackgroundColor = System.Drawing.Color.White;
            this._defenseQgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._defenseQgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewComboBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn16,
            this.dataGridViewComboBoxColumn17,
            this.dataGridViewComboBoxColumn18,
            this.dataGridViewComboBoxColumn19,
            this.dataGridViewComboBoxColumn20});
            this._defenseQgrid.Location = new System.Drawing.Point(6, 99);
            this._defenseQgrid.MultiSelect = false;
            this._defenseQgrid.Name = "_defenseQgrid";
            this._defenseQgrid.RowHeadersVisible = false;
            this._defenseQgrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._defenseQgrid.RowTemplate.Height = 24;
            this._defenseQgrid.ShowCellErrors = false;
            this._defenseQgrid.ShowRowErrors = false;
            this._defenseQgrid.Size = new System.Drawing.Size(528, 78);
            this._defenseQgrid.TabIndex = 12;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle118.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle118.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle118.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle118.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle118.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle118;
            this.dataGridViewTextBoxColumn15.Frozen = true;
            this.dataGridViewTextBoxColumn15.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 80;
            // 
            // dataGridViewComboBoxColumn15
            // 
            dataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn15.DefaultCellStyle = dataGridViewCellStyle119;
            this.dataGridViewComboBoxColumn15.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn15.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            this.dataGridViewComboBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn15.Width = 80;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle120;
            this.dataGridViewTextBoxColumn16.HeaderText = "Q, %";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 70;
            // 
            // dataGridViewComboBoxColumn16
            // 
            dataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn16.DefaultCellStyle = dataGridViewCellStyle121;
            this.dataGridViewComboBoxColumn16.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            this.dataGridViewComboBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn16.Width = 90;
            // 
            // dataGridViewComboBoxColumn17
            // 
            dataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn17.DefaultCellStyle = dataGridViewCellStyle122;
            this.dataGridViewComboBoxColumn17.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            this.dataGridViewComboBoxColumn17.Width = 90;
            // 
            // dataGridViewComboBoxColumn18
            // 
            this.dataGridViewComboBoxColumn18.HeaderText = "УРОВ";
            this.dataGridViewComboBoxColumn18.Name = "dataGridViewComboBoxColumn18";
            this.dataGridViewComboBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn18.Width = 45;
            // 
            // dataGridViewComboBoxColumn19
            // 
            this.dataGridViewComboBoxColumn19.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn19.Name = "dataGridViewComboBoxColumn19";
            this.dataGridViewComboBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn19.Width = 35;
            // 
            // dataGridViewComboBoxColumn20
            // 
            this.dataGridViewComboBoxColumn20.HeaderText = "АВР";
            this.dataGridViewComboBoxColumn20.Name = "dataGridViewComboBoxColumn20";
            this.dataGridViewComboBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn20.Width = 35;
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.groupBox24);
            this.tabPage25.Location = new System.Drawing.Point(4, 25);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Size = new System.Drawing.Size(858, 462);
            this.tabPage25.TabIndex = 8;
            this.tabPage25.Text = "Внешние";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox24.Controls.Add(this._externalDifensesDataGrid);
            this.groupBox24.Location = new System.Drawing.Point(3, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(852, 450);
            this.groupBox24.TabIndex = 4;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Внешние защиты";
            // 
            // _externalDifensesDataGrid
            // 
            this._externalDifensesDataGrid.AllowUserToAddRows = false;
            this._externalDifensesDataGrid.AllowUserToDeleteRows = false;
            this._externalDifensesDataGrid.AllowUserToResizeColumns = false;
            this._externalDifensesDataGrid.AllowUserToResizeRows = false;
            this._externalDifensesDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._externalDifensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDifensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._externalDifStageColumn,
            this._externalDifModesColumn,
            this._externalDifSrabColumn,
            this._externalDifTsrColumn,
            this._externalDifTvzColumn,
            this._externalDifVozvrYNColumn,
            this._externalDifVozvrColumn,
            this._externalDifBlockingColumn,
            this._externalDifOscColumn,
            this._externalDifAPVRetColumn,
            this._externalDifUROVColumn,
            this._externalDifAPVColumn,
            this._externalDifAVRColumn,
            this._externalDifSbrosColumn});
            this._externalDifensesDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDifensesDataGrid.Location = new System.Drawing.Point(3, 16);
            this._externalDifensesDataGrid.MultiSelect = false;
            this._externalDifensesDataGrid.Name = "_externalDifensesDataGrid";
            this._externalDifensesDataGrid.RowHeadersVisible = false;
            this._externalDifensesDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDifensesDataGrid.RowTemplate.Height = 24;
            this._externalDifensesDataGrid.ShowCellErrors = false;
            this._externalDifensesDataGrid.ShowRowErrors = false;
            this._externalDifensesDataGrid.Size = new System.Drawing.Size(846, 431);
            this._externalDifensesDataGrid.TabIndex = 3;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle123.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle123.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle123.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle123.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle123.SelectionForeColor = System.Drawing.Color.White;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle123;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "Ступень";
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.ReadOnly = true;
            this._externalDifStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle124.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle124;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Режим";
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifModesColumn.Width = 80;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle125.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle125;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle126.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle126;
            this._externalDifTsrColumn.HeaderText = "tср, мс";
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 70;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle127.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle127;
            this._externalDifTvzColumn.HeaderText = "tвз, мс";
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 70;
            // 
            // _externalDifVozvrYNColumn
            // 
            dataGridViewCellStyle128.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle128.NullValue = false;
            this._externalDifVozvrYNColumn.DefaultCellStyle = dataGridViewCellStyle128;
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrYNColumn.Width = 80;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle129.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle129;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврата";
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrColumn.Width = 110;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle130.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle130;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifBlockingColumn.Width = 130;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle131.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle131;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifOscColumn.Width = 80;
            // 
            // _externalDifAPVRetColumn
            // 
            this._externalDifAPVRetColumn.HeaderText = "АПВ возвр.";
            this._externalDifAPVRetColumn.Name = "_externalDifAPVRetColumn";
            this._externalDifAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _externalDifUROVColumn
            // 
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifUROVColumn.Width = 60;
            // 
            // _externalDifAPVColumn
            // 
            this._externalDifAPVColumn.HeaderText = "АПВ";
            this._externalDifAPVColumn.Name = "_externalDifAPVColumn";
            this._externalDifAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVColumn.Width = 40;
            // 
            // _externalDifAVRColumn
            // 
            this._externalDifAVRColumn.HeaderText = "АВР";
            this._externalDifAVRColumn.Name = "_externalDifAVRColumn";
            this._externalDifAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAVRColumn.Width = 40;
            // 
            // _externalDifSbrosColumn
            // 
            this._externalDifSbrosColumn.HeaderText = "Сброс";
            this._externalDifSbrosColumn.Name = "_externalDifSbrosColumn";
            this._externalDifSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSbrosColumn.Width = 60;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(388, 474);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox1.TabIndex = 6;
            // 
            // VLScheckedListBox11
            // 
            this.VLScheckedListBox11.CheckOnClick = true;
            this.VLScheckedListBox11.FormattingEnabled = true;
            this.VLScheckedListBox11.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox11.Name = "VLScheckedListBox11";
            this.VLScheckedListBox11.ScrollAlwaysVisible = true;
            this.VLScheckedListBox11.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox11.TabIndex = 6;
            // 
            // VLScheckedListBox13
            // 
            this.VLScheckedListBox13.CheckOnClick = true;
            this.VLScheckedListBox13.FormattingEnabled = true;
            this.VLScheckedListBox13.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox13.Name = "VLScheckedListBox13";
            this.VLScheckedListBox13.ScrollAlwaysVisible = true;
            this.VLScheckedListBox13.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox13.TabIndex = 6;
            // 
            // VLScheckedListBox14
            // 
            this.VLScheckedListBox14.CheckOnClick = true;
            this.VLScheckedListBox14.FormattingEnabled = true;
            this.VLScheckedListBox14.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox14.Name = "VLScheckedListBox14";
            this.VLScheckedListBox14.ScrollAlwaysVisible = true;
            this.VLScheckedListBox14.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox14.TabIndex = 6;
            // 
            // VLScheckedListBox15
            // 
            this.VLScheckedListBox15.CheckOnClick = true;
            this.VLScheckedListBox15.FormattingEnabled = true;
            this.VLScheckedListBox15.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox15.Name = "VLScheckedListBox15";
            this.VLScheckedListBox15.ScrollAlwaysVisible = true;
            this.VLScheckedListBox15.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox15.TabIndex = 6;
            // 
            // VLScheckedListBox16
            // 
            this.VLScheckedListBox16.CheckOnClick = true;
            this.VLScheckedListBox16.FormattingEnabled = true;
            this.VLScheckedListBox16.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox16.Name = "VLScheckedListBox16";
            this.VLScheckedListBox16.ScrollAlwaysVisible = true;
            this.VLScheckedListBox16.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox16.TabIndex = 6;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox11.Location = new System.Drawing.Point(8, 198);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(393, 190);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndColorCol});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(9, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle132.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle132;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(376, 167);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndColorCol
            // 
            this._outIndColorCol.HeaderText = "Цвет";
            this._outIndColorCol.Name = "_outIndColorCol";
            this._outIndColorCol.ReadOnly = true;
            this._outIndColorCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndColorCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndColorCol.Width = 70;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(388, 474);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox2.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(388, 474);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox4.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(388, 474);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox7.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(388, 474);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox3.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(388, 474);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox8.TabIndex = 6;
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLScheckedListBox9);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(388, 474);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox9
            // 
            this.VLScheckedListBox9.CheckOnClick = true;
            this.VLScheckedListBox9.FormattingEnabled = true;
            this.VLScheckedListBox9.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox9.Name = "VLScheckedListBox9";
            this.VLScheckedListBox9.ScrollAlwaysVisible = true;
            this.VLScheckedListBox9.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox9.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(388, 474);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox5.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(388, 474);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox6.TabIndex = 6;
            // 
            // _oscPage
            // 
            this._oscPage.Controls.Add(this.groupBox2);
            this._oscPage.Location = new System.Drawing.Point(4, 22);
            this._oscPage.Name = "_oscPage";
            this._oscPage.Size = new System.Drawing.Size(884, 553);
            this._oscPage.TabIndex = 8;
            this._oscPage.Text = "Осциллограф";
            this._oscPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label133);
            this.groupBox2.Controls.Add(this._oscopePercent);
            this.groupBox2.Controls.Add(this.label58);
            this.groupBox2.Controls.Add(this.label134);
            this.groupBox2.Controls.Add(this.groupBox34);
            this.groupBox2.Controls.Add(this._oscopeCountComboBox);
            this.groupBox2.Controls.Add(this.label136);
            this.groupBox2.Controls.Add(this._oscopeLenghtLabel);
            this.groupBox2.Controls.Add(this._oscCount);
            this.groupBox2.Controls.Add(this._inpOsc);
            this.groupBox2.Controls.Add(this._oscopeFixationComboBox);
            this.groupBox2.Controls.Add(this.label135);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(335, 363);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Конфигурация осциллографа";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(6, 22);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(199, 13);
            this.label133.TabIndex = 10;
            this.label133.Text = "Число и длительность осциллограмм";
            // 
            // _oscopePercent
            // 
            this._oscopePercent.Location = new System.Drawing.Point(211, 73);
            this._oscopePercent.Name = "_oscopePercent";
            this._oscopePercent.Size = new System.Drawing.Size(86, 20);
            this._oscopePercent.TabIndex = 19;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 102);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(139, 13);
            this.label58.TabIndex = 11;
            this.label58.Text = "Вход пуска осциллографа";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(6, 49);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(138, 13);
            this.label134.TabIndex = 11;
            this.label134.Text = "Фиксация осциллограмм";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this._oscChannelsDgv);
            this.groupBox34.Location = new System.Drawing.Point(9, 137);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(273, 220);
            this.groupBox34.TabIndex = 18;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannelsDgv
            // 
            this._oscChannelsDgv.AllowUserToAddRows = false;
            this._oscChannelsDgv.AllowUserToDeleteRows = false;
            this._oscChannelsDgv.AllowUserToResizeColumns = false;
            this._oscChannelsDgv.AllowUserToResizeRows = false;
            this._oscChannelsDgv.BackgroundColor = System.Drawing.Color.White;
            this._oscChannelsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannelsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this._oscChannelsDgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this._oscChannelsDgv.Location = new System.Drawing.Point(3, 16);
            this._oscChannelsDgv.Name = "_oscChannelsDgv";
            this._oscChannelsDgv.RowHeadersVisible = false;
            this._oscChannelsDgv.Size = new System.Drawing.Size(267, 201);
            this._oscChannelsDgv.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Канал";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 60;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Сигнал";
            this.Column2.Name = "Column2";
            // 
            // _oscopeCountComboBox
            // 
            this._oscopeCountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscopeCountComboBox.FormattingEnabled = true;
            this._oscopeCountComboBox.Location = new System.Drawing.Point(211, 19);
            this._oscopeCountComboBox.Name = "_oscopeCountComboBox";
            this._oscopeCountComboBox.Size = new System.Drawing.Size(36, 21);
            this._oscopeCountComboBox.TabIndex = 12;
            this._oscopeCountComboBox.SelectedIndexChanged += new System.EventHandler(this._oscopeCountComboBox_SelectedIndexChanged);
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(303, 76);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(15, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "%";
            // 
            // _oscopeLenghtLabel
            // 
            this._oscopeLenghtLabel.AutoSize = true;
            this._oscopeLenghtLabel.Location = new System.Drawing.Point(303, 22);
            this._oscopeLenghtLabel.Name = "_oscopeLenghtLabel";
            this._oscopeLenghtLabel.Size = new System.Drawing.Size(21, 13);
            this._oscopeLenghtLabel.TabIndex = 13;
            this._oscopeLenghtLabel.Text = "мс";
            // 
            // _oscCount
            // 
            this._oscCount.Location = new System.Drawing.Point(251, 19);
            this._oscCount.MaxLength = 3;
            this._oscCount.Name = "_oscCount";
            this._oscCount.ReadOnly = true;
            this._oscCount.Size = new System.Drawing.Size(46, 20);
            this._oscCount.TabIndex = 16;
            this._oscCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inpOsc
            // 
            this._inpOsc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpOsc.FormattingEnabled = true;
            this._inpOsc.Location = new System.Drawing.Point(192, 99);
            this._inpOsc.Name = "_inpOsc";
            this._inpOsc.Size = new System.Drawing.Size(105, 21);
            this._inpOsc.TabIndex = 14;
            // 
            // _oscopeFixationComboBox
            // 
            this._oscopeFixationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscopeFixationComboBox.FormattingEnabled = true;
            this._oscopeFixationComboBox.Location = new System.Drawing.Point(211, 46);
            this._oscopeFixationComboBox.Name = "_oscopeFixationComboBox";
            this._oscopeFixationComboBox.Size = new System.Drawing.Size(86, 21);
            this._oscopeFixationComboBox.TabIndex = 14;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(6, 76);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(143, 13);
            this.label135.TabIndex = 15;
            this.label135.Text = "Длительность предзаписи";
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(4, 5);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 26;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.ButtontoolTip.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (Ctrl+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(588, 5);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(108, 23);
            this._saveConfigBut.TabIndex = 29;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.ButtontoolTip.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(155, 5);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(134, 23);
            this._writeConfigBut.TabIndex = 27;
            this._writeConfigBut.Text = "Записать в устройство";
            this.ButtontoolTip.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (Ctrl+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(463, 5);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(119, 23);
            this._loadConfigBut.TabIndex = 28;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.ButtontoolTip.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(321, 5);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(136, 23);
            this._resetSetpointsButton.TabIndex = 28;
            this._resetSetpointsButton.Text = "Загрузить баз. уставки";
            this.ButtontoolTip.SetToolTip(this._resetSetpointsButton, "Загрузить базовые уставки");
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this.resetBtn_Click);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 55;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(702, 5);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(112, 23);
            this._saveToXmlButton.TabIndex = 35;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this.ButtontoolTip.SetToolTip(this._saveToXmlButton, "Сохранить конфигурацию в HTML файл");
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 30);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(892, 22);
            this._statusStrip.TabIndex = 26;
            this._statusStrip.Text = "statusStrip1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._saveToXmlButton);
            this.panel1.Controls.Add(this._writeConfigBut);
            this.panel1.Controls.Add(this._statusStrip);
            this.panel1.Controls.Add(this._readConfigBut);
            this.panel1.Controls.Add(this._saveConfigBut);
            this.panel1.Controls.Add(this._resetSetpointsButton);
            this.panel1.Controls.Add(this._loadConfigBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 584);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 52);
            this.panel1.TabIndex = 31;
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLScheckedListBox10);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(388, 474);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox10
            // 
            this.VLScheckedListBox10.CheckOnClick = true;
            this.VLScheckedListBox10.FormattingEnabled = true;
            this.VLScheckedListBox10.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox10.Name = "VLScheckedListBox10";
            this.VLScheckedListBox10.ScrollAlwaysVisible = true;
            this.VLScheckedListBox10.Size = new System.Drawing.Size(184, 454);
            this.VLScheckedListBox10.TabIndex = 6;
            // 
            // _inputSygnalsPage
            // 
            this._inputSygnalsPage.Controls.Add(this.groupBox28);
            this._inputSygnalsPage.Controls.Add(this.groupBox18);
            this._inputSygnalsPage.Controls.Add(this.groupBox15);
            this._inputSygnalsPage.Controls.Add(this.groupBox17);
            this._inputSygnalsPage.Controls.Add(this.groupBox14);
            this._inputSygnalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSygnalsPage.Name = "_inputSygnalsPage";
            this._inputSygnalsPage.Size = new System.Drawing.Size(884, 553);
            this._inputSygnalsPage.TabIndex = 7;
            this._inputSygnalsPage.Text = "Входные сигналы";
            this._inputSygnalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this._inpResTT);
            this.groupBox28.Location = new System.Drawing.Point(406, 117);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(179, 52);
            this.groupBox28.TabIndex = 4;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Сброс неисправности ТТ";
            // 
            // _inpResTT
            // 
            this._inpResTT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpResTT.FormattingEnabled = true;
            this._inpResTT.Location = new System.Drawing.Point(6, 19);
            this._inpResTT.Name = "_inpResTT";
            this._inpResTT.Size = new System.Drawing.Size(167, 21);
            this._inpResTT.TabIndex = 0;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(406, 59);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(179, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс блинкеров";
            // 
            // _indComboBox
            // 
            this._indComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(6, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._grUstComboBox);
            this.groupBox15.Location = new System.Drawing.Point(406, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(179, 52);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Аварийная группа уставок";
            // 
            // _grUstComboBox
            // 
            this._grUstComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._grUstComboBox.FormattingEnabled = true;
            this._grUstComboBox.Location = new System.Drawing.Point(6, 19);
            this._grUstComboBox.Name = "_grUstComboBox";
            this._grUstComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUstComboBox.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tabControl2);
            this.groupBox17.Location = new System.Drawing.Point(207, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(193, 547);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Controls.Add(this.tabPage16);
            this.tabControl2.Location = new System.Drawing.Point(6, 18);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(181, 528);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals9);
            this.tabPage9.Location = new System.Drawing.Point(4, 49);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(173, 475);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "ЛС9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals9
            // 
            this._inputSignals9.AllowUserToAddRows = false;
            this._inputSignals9.AllowUserToDeleteRows = false;
            this._inputSignals9.AllowUserToResizeColumns = false;
            this._inputSignals9.AllowUserToResizeRows = false;
            this._inputSignals9.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals9.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals9.Location = new System.Drawing.Point(3, 3);
            this._inputSignals9.MultiSelect = false;
            this._inputSignals9.Name = "_inputSignals9";
            this._inputSignals9.RowHeadersVisible = false;
            this._inputSignals9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle133.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals9.RowsDefaultCellStyle = dataGridViewCellStyle133;
            this._inputSignals9.RowTemplate.Height = 20;
            this._inputSignals9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals9.ShowCellErrors = false;
            this._inputSignals9.ShowCellToolTips = false;
            this._inputSignals9.ShowEditingIcon = false;
            this._inputSignals9.ShowRowErrors = false;
            this._inputSignals9.Size = new System.Drawing.Size(167, 469);
            this._inputSignals9.TabIndex = 2;
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "№";
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "Значение";
            this._signalValueColILI.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals10);
            this.tabPage10.Location = new System.Drawing.Point(4, 49);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(173, 475);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "ЛС10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals10
            // 
            this._inputSignals10.AllowUserToAddRows = false;
            this._inputSignals10.AllowUserToDeleteRows = false;
            this._inputSignals10.AllowUserToResizeColumns = false;
            this._inputSignals10.AllowUserToResizeRows = false;
            this._inputSignals10.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals10.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals10.Location = new System.Drawing.Point(3, 3);
            this._inputSignals10.MultiSelect = false;
            this._inputSignals10.Name = "_inputSignals10";
            this._inputSignals10.RowHeadersVisible = false;
            this._inputSignals10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle134.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals10.RowsDefaultCellStyle = dataGridViewCellStyle134;
            this._inputSignals10.RowTemplate.Height = 20;
            this._inputSignals10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals10.ShowCellErrors = false;
            this._inputSignals10.ShowCellToolTips = false;
            this._inputSignals10.ShowEditingIcon = false;
            this._inputSignals10.ShowRowErrors = false;
            this._inputSignals10.Size = new System.Drawing.Size(167, 469);
            this._inputSignals10.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "№";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals11);
            this.tabPage11.Location = new System.Drawing.Point(4, 49);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(173, 475);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "ЛС11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals11
            // 
            this._inputSignals11.AllowUserToAddRows = false;
            this._inputSignals11.AllowUserToDeleteRows = false;
            this._inputSignals11.AllowUserToResizeColumns = false;
            this._inputSignals11.AllowUserToResizeRows = false;
            this._inputSignals11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals11.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals11.Location = new System.Drawing.Point(3, 3);
            this._inputSignals11.MultiSelect = false;
            this._inputSignals11.Name = "_inputSignals11";
            this._inputSignals11.RowHeadersVisible = false;
            this._inputSignals11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle135.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals11.RowsDefaultCellStyle = dataGridViewCellStyle135;
            this._inputSignals11.RowTemplate.Height = 20;
            this._inputSignals11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals11.ShowCellErrors = false;
            this._inputSignals11.ShowCellToolTips = false;
            this._inputSignals11.ShowEditingIcon = false;
            this._inputSignals11.ShowRowErrors = false;
            this._inputSignals11.Size = new System.Drawing.Size(167, 469);
            this._inputSignals11.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "№";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals12);
            this.tabPage12.Location = new System.Drawing.Point(4, 49);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(173, 475);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "ЛС12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals12
            // 
            this._inputSignals12.AllowUserToAddRows = false;
            this._inputSignals12.AllowUserToDeleteRows = false;
            this._inputSignals12.AllowUserToResizeColumns = false;
            this._inputSignals12.AllowUserToResizeRows = false;
            this._inputSignals12.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals12.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals12.Location = new System.Drawing.Point(3, 3);
            this._inputSignals12.MultiSelect = false;
            this._inputSignals12.Name = "_inputSignals12";
            this._inputSignals12.RowHeadersVisible = false;
            this._inputSignals12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle136.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals12.RowsDefaultCellStyle = dataGridViewCellStyle136;
            this._inputSignals12.RowTemplate.Height = 20;
            this._inputSignals12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals12.ShowCellErrors = false;
            this._inputSignals12.ShowCellToolTips = false;
            this._inputSignals12.ShowEditingIcon = false;
            this._inputSignals12.ShowRowErrors = false;
            this._inputSignals12.Size = new System.Drawing.Size(167, 469);
            this._inputSignals12.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "№";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this._inputSignals13);
            this.tabPage13.Location = new System.Drawing.Point(4, 49);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(173, 475);
            this.tabPage13.TabIndex = 4;
            this.tabPage13.Text = "ЛС13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // _inputSignals13
            // 
            this._inputSignals13.AllowUserToAddRows = false;
            this._inputSignals13.AllowUserToDeleteRows = false;
            this._inputSignals13.AllowUserToResizeColumns = false;
            this._inputSignals13.AllowUserToResizeRows = false;
            this._inputSignals13.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals13.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals13.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewComboBoxColumn11});
            this._inputSignals13.Location = new System.Drawing.Point(3, 3);
            this._inputSignals13.MultiSelect = false;
            this._inputSignals13.Name = "_inputSignals13";
            this._inputSignals13.RowHeadersVisible = false;
            this._inputSignals13.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle137.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals13.RowsDefaultCellStyle = dataGridViewCellStyle137;
            this._inputSignals13.RowTemplate.Height = 20;
            this._inputSignals13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals13.ShowCellErrors = false;
            this._inputSignals13.ShowCellToolTips = false;
            this._inputSignals13.ShowEditingIcon = false;
            this._inputSignals13.ShowRowErrors = false;
            this._inputSignals13.Size = new System.Drawing.Size(167, 469);
            this._inputSignals13.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "№";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 24;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn11.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this._inputSignals14);
            this.tabPage14.Location = new System.Drawing.Point(4, 49);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(173, 475);
            this.tabPage14.TabIndex = 5;
            this.tabPage14.Text = "ЛС14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // _inputSignals14
            // 
            this._inputSignals14.AllowUserToAddRows = false;
            this._inputSignals14.AllowUserToDeleteRows = false;
            this._inputSignals14.AllowUserToResizeColumns = false;
            this._inputSignals14.AllowUserToResizeRows = false;
            this._inputSignals14.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals14.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals14.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewComboBoxColumn12});
            this._inputSignals14.Location = new System.Drawing.Point(3, 3);
            this._inputSignals14.MultiSelect = false;
            this._inputSignals14.Name = "_inputSignals14";
            this._inputSignals14.RowHeadersVisible = false;
            this._inputSignals14.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle138.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals14.RowsDefaultCellStyle = dataGridViewCellStyle138;
            this._inputSignals14.RowTemplate.Height = 20;
            this._inputSignals14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals14.ShowCellErrors = false;
            this._inputSignals14.ShowCellToolTips = false;
            this._inputSignals14.ShowEditingIcon = false;
            this._inputSignals14.ShowRowErrors = false;
            this._inputSignals14.Size = new System.Drawing.Size(167, 469);
            this._inputSignals14.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "№";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 24;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn12.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this._inputSignals15);
            this.tabPage15.Location = new System.Drawing.Point(4, 49);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(173, 475);
            this.tabPage15.TabIndex = 6;
            this.tabPage15.Text = "ЛС15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // _inputSignals15
            // 
            this._inputSignals15.AllowUserToAddRows = false;
            this._inputSignals15.AllowUserToDeleteRows = false;
            this._inputSignals15.AllowUserToResizeColumns = false;
            this._inputSignals15.AllowUserToResizeRows = false;
            this._inputSignals15.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals15.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals15.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals15.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewComboBoxColumn13});
            this._inputSignals15.Location = new System.Drawing.Point(3, 3);
            this._inputSignals15.MultiSelect = false;
            this._inputSignals15.Name = "_inputSignals15";
            this._inputSignals15.RowHeadersVisible = false;
            this._inputSignals15.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle139.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals15.RowsDefaultCellStyle = dataGridViewCellStyle139;
            this._inputSignals15.RowTemplate.Height = 20;
            this._inputSignals15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals15.ShowCellErrors = false;
            this._inputSignals15.ShowCellToolTips = false;
            this._inputSignals15.ShowEditingIcon = false;
            this._inputSignals15.ShowRowErrors = false;
            this._inputSignals15.Size = new System.Drawing.Size(167, 469);
            this._inputSignals15.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "№";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 24;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn13.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn13.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn13.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this._inputSignals16);
            this.tabPage16.Location = new System.Drawing.Point(4, 49);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(173, 475);
            this.tabPage16.TabIndex = 7;
            this.tabPage16.Text = "ЛС16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // _inputSignals16
            // 
            this._inputSignals16.AllowUserToAddRows = false;
            this._inputSignals16.AllowUserToDeleteRows = false;
            this._inputSignals16.AllowUserToResizeColumns = false;
            this._inputSignals16.AllowUserToResizeRows = false;
            this._inputSignals16.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals16.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals16.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn14});
            this._inputSignals16.Location = new System.Drawing.Point(3, 3);
            this._inputSignals16.MultiSelect = false;
            this._inputSignals16.Name = "_inputSignals16";
            this._inputSignals16.RowHeadersVisible = false;
            this._inputSignals16.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle140.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals16.RowsDefaultCellStyle = dataGridViewCellStyle140;
            this._inputSignals16.RowTemplate.Height = 20;
            this._inputSignals16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals16.ShowCellErrors = false;
            this._inputSignals16.ShowCellToolTips = false;
            this._inputSignals16.ShowEditingIcon = false;
            this._inputSignals16.ShowRowErrors = false;
            this._inputSignals16.Size = new System.Drawing.Size(167, 469);
            this._inputSignals16.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "№";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 24;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn14.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn14.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn14.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tabControl1);
            this.groupBox14.Location = new System.Drawing.Point(8, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(193, 547);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Логические сигналы И";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(181, 528);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 49);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(173, 475);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ЛС1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(3, 3);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle141.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle141;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(167, 469);
            this._inputSignals1.TabIndex = 2;
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "№";
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "Значение";
            this._signalValueCol.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 49);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(173, 475);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ЛС2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(3, 3);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle142.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle142;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(167, 469);
            this._inputSignals2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 49);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(173, 475);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ЛС3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Location = new System.Drawing.Point(3, 3);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle143.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle143;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(167, 469);
            this._inputSignals3.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 49);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(173, 475);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ЛС4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Location = new System.Drawing.Point(3, 3);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle144.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle144;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(167, 469);
            this._inputSignals4.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "№";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._inputSignals5);
            this.tabPage5.Location = new System.Drawing.Point(4, 49);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(173, 475);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ЛС5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _inputSignals5
            // 
            this._inputSignals5.AllowUserToAddRows = false;
            this._inputSignals5.AllowUserToDeleteRows = false;
            this._inputSignals5.AllowUserToResizeColumns = false;
            this._inputSignals5.AllowUserToResizeRows = false;
            this._inputSignals5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals5.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn4});
            this._inputSignals5.Location = new System.Drawing.Point(3, 3);
            this._inputSignals5.MultiSelect = false;
            this._inputSignals5.Name = "_inputSignals5";
            this._inputSignals5.RowHeadersVisible = false;
            this._inputSignals5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle145.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals5.RowsDefaultCellStyle = dataGridViewCellStyle145;
            this._inputSignals5.RowTemplate.Height = 20;
            this._inputSignals5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals5.ShowCellErrors = false;
            this._inputSignals5.ShowCellToolTips = false;
            this._inputSignals5.ShowEditingIcon = false;
            this._inputSignals5.ShowRowErrors = false;
            this._inputSignals5.Size = new System.Drawing.Size(167, 469);
            this._inputSignals5.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "№";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this._inputSignals6);
            this.tabPage6.Location = new System.Drawing.Point(4, 49);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(173, 475);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "ЛС6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // _inputSignals6
            // 
            this._inputSignals6.AllowUserToAddRows = false;
            this._inputSignals6.AllowUserToDeleteRows = false;
            this._inputSignals6.AllowUserToResizeColumns = false;
            this._inputSignals6.AllowUserToResizeRows = false;
            this._inputSignals6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals6.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn5});
            this._inputSignals6.Location = new System.Drawing.Point(3, 3);
            this._inputSignals6.MultiSelect = false;
            this._inputSignals6.Name = "_inputSignals6";
            this._inputSignals6.RowHeadersVisible = false;
            this._inputSignals6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle146.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals6.RowsDefaultCellStyle = dataGridViewCellStyle146;
            this._inputSignals6.RowTemplate.Height = 20;
            this._inputSignals6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals6.ShowCellErrors = false;
            this._inputSignals6.ShowCellToolTips = false;
            this._inputSignals6.ShowEditingIcon = false;
            this._inputSignals6.ShowRowErrors = false;
            this._inputSignals6.Size = new System.Drawing.Size(167, 469);
            this._inputSignals6.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "№";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 24;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this._inputSignals7);
            this.tabPage7.Location = new System.Drawing.Point(4, 49);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(173, 475);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "ЛС7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // _inputSignals7
            // 
            this._inputSignals7.AllowUserToAddRows = false;
            this._inputSignals7.AllowUserToDeleteRows = false;
            this._inputSignals7.AllowUserToResizeColumns = false;
            this._inputSignals7.AllowUserToResizeRows = false;
            this._inputSignals7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals7.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn6});
            this._inputSignals7.Location = new System.Drawing.Point(3, 3);
            this._inputSignals7.MultiSelect = false;
            this._inputSignals7.Name = "_inputSignals7";
            this._inputSignals7.RowHeadersVisible = false;
            this._inputSignals7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle147.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals7.RowsDefaultCellStyle = dataGridViewCellStyle147;
            this._inputSignals7.RowTemplate.Height = 20;
            this._inputSignals7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals7.ShowCellErrors = false;
            this._inputSignals7.ShowCellToolTips = false;
            this._inputSignals7.ShowEditingIcon = false;
            this._inputSignals7.ShowRowErrors = false;
            this._inputSignals7.Size = new System.Drawing.Size(167, 469);
            this._inputSignals7.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "№";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 24;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this._inputSignals8);
            this.tabPage8.Location = new System.Drawing.Point(4, 49);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(173, 475);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "ЛС8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // _inputSignals8
            // 
            this._inputSignals8.AllowUserToAddRows = false;
            this._inputSignals8.AllowUserToDeleteRows = false;
            this._inputSignals8.AllowUserToResizeColumns = false;
            this._inputSignals8.AllowUserToResizeRows = false;
            this._inputSignals8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals8.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn7});
            this._inputSignals8.Location = new System.Drawing.Point(3, 3);
            this._inputSignals8.MultiSelect = false;
            this._inputSignals8.Name = "_inputSignals8";
            this._inputSignals8.RowHeadersVisible = false;
            this._inputSignals8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle148.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals8.RowsDefaultCellStyle = dataGridViewCellStyle148;
            this._inputSignals8.RowTemplate.Height = 20;
            this._inputSignals8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals8.ShowCellErrors = false;
            this._inputSignals8.ShowCellToolTips = false;
            this._inputSignals8.ShowEditingIcon = false;
            this._inputSignals8.ShowRowErrors = false;
            this._inputSignals8.Size = new System.Drawing.Size(167, 469);
            this._inputSignals8.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "№";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 24;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(222, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(222, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // _measureTransPage
            // 
            this._measureTransPage.Controls.Add(this.groupBox21);
            this._measureTransPage.Controls.Add(this.groupBox36);
            this._measureTransPage.Controls.Add(this.groupBox35);
            this._measureTransPage.Controls.Add(this.groupBox3);
            this._measureTransPage.Location = new System.Drawing.Point(4, 22);
            this._measureTransPage.Name = "_measureTransPage";
            this._measureTransPage.Size = new System.Drawing.Size(884, 553);
            this._measureTransPage.TabIndex = 2;
            this._measureTransPage.Text = "Данные изм. трансформаторов";
            this._measureTransPage.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._controlTtNeisp);
            this.groupBox21.Controls.Add(this._controlTtTsr);
            this.groupBox21.Controls.Add(this.label55);
            this.groupBox21.Controls.Add(this.label54);
            this.groupBox21.Controls.Add(this._controlTtIdmin);
            this.groupBox21.Controls.Add(this.label40);
            this.groupBox21.Location = new System.Drawing.Point(357, 3);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(167, 104);
            this.groupBox21.TabIndex = 65;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Контроль цепей ТТ";
            // 
            // _controlTtNeisp
            // 
            this._controlTtNeisp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._controlTtNeisp.FormattingEnabled = true;
            this._controlTtNeisp.Location = new System.Drawing.Point(63, 71);
            this._controlTtNeisp.Name = "_controlTtNeisp";
            this._controlTtNeisp.Size = new System.Drawing.Size(96, 21);
            this._controlTtNeisp.TabIndex = 2;
            // 
            // _controlTtTsr
            // 
            this._controlTtTsr.Location = new System.Drawing.Point(63, 45);
            this._controlTtTsr.Name = "_controlTtTsr";
            this._controlTtTsr.Size = new System.Drawing.Size(67, 20);
            this._controlTtTsr.TabIndex = 1;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 74);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(42, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Режим";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 48);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(46, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Tср, мс";
            // 
            // _controlTtIdmin
            // 
            this._controlTtIdmin.Location = new System.Drawing.Point(63, 19);
            this._controlTtIdmin.Name = "_controlTtIdmin";
            this._controlTtIdmin.Size = new System.Drawing.Size(67, 20);
            this._controlTtIdmin.TabIndex = 1;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Iдmin, Iдв";
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this._TH_XKoeff_ComboBox);
            this.groupBox36.Controls.Add(this._faultTnx);
            this.groupBox36.Controls.Add(this._TH_LKoeff_ComboBox);
            this.groupBox36.Controls.Add(this.label80);
            this.groupBox36.Controls.Add(this._faultTnl);
            this.groupBox36.Controls.Add(this.label79);
            this.groupBox36.Controls.Add(this.label21);
            this.groupBox36.Controls.Add(this.label22);
            this.groupBox36.Controls.Add(this._TH_L_TextBox);
            this.groupBox36.Controls.Add(this._TH_X_TextBox);
            this.groupBox36.Controls.Add(this._typeTn);
            this.groupBox36.Controls.Add(this.label43);
            this.groupBox36.Controls.Add(this.label42);
            this.groupBox36.Controls.Add(this.label41);
            this.groupBox36.Location = new System.Drawing.Point(8, 288);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(633, 88);
            this.groupBox36.TabIndex = 64;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Трансформатор напряжения";
            // 
            // _TH_XKoeff_ComboBox
            // 
            this._TH_XKoeff_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TH_XKoeff_ComboBox.FormattingEnabled = true;
            this._TH_XKoeff_ComboBox.Items.AddRange(new object[] {
            "1",
            "1000"});
            this._TH_XKoeff_ComboBox.Location = new System.Drawing.Point(337, 37);
            this._TH_XKoeff_ComboBox.Name = "_TH_XKoeff_ComboBox";
            this._TH_XKoeff_ComboBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._TH_XKoeff_ComboBox.Size = new System.Drawing.Size(66, 21);
            this._TH_XKoeff_ComboBox.TabIndex = 63;
            // 
            // _faultTnx
            // 
            this._faultTnx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._faultTnx.FormattingEnabled = true;
            this._faultTnx.Location = new System.Drawing.Point(510, 37);
            this._faultTnx.Name = "_faultTnx";
            this._faultTnx.Size = new System.Drawing.Size(116, 21);
            this._faultTnx.TabIndex = 68;
            // 
            // _TH_LKoeff_ComboBox
            // 
            this._TH_LKoeff_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TH_LKoeff_ComboBox.FormattingEnabled = true;
            this._TH_LKoeff_ComboBox.Items.AddRange(new object[] {
            "1",
            "1000"});
            this._TH_LKoeff_ComboBox.Location = new System.Drawing.Point(337, 17);
            this._TH_LKoeff_ComboBox.Name = "_TH_LKoeff_ComboBox";
            this._TH_LKoeff_ComboBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this._TH_LKoeff_ComboBox.Size = new System.Drawing.Size(66, 21);
            this._TH_LKoeff_ComboBox.TabIndex = 61;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(409, 41);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(96, 13);
            this.label80.TabIndex = 67;
            this.label80.Text = "Неисправность X";
            // 
            // _faultTnl
            // 
            this._faultTnl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._faultTnl.FormattingEnabled = true;
            this._faultTnl.Location = new System.Drawing.Point(510, 17);
            this._faultTnl.Name = "_faultTnl";
            this._faultTnl.Size = new System.Drawing.Size(116, 21);
            this._faultTnl.TabIndex = 66;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(409, 20);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(95, 13);
            this.label79.TabIndex = 65;
            this.label79.Text = "Неисправность L";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(320, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 62;
            this.label21.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(320, 41);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 13);
            this.label22.TabIndex = 64;
            this.label22.Text = "*";
            // 
            // _TH_L_TextBox
            // 
            this._TH_L_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TH_L_TextBox.Location = new System.Drawing.Point(203, 19);
            this._TH_L_TextBox.Name = "_TH_L_TextBox";
            this._TH_L_TextBox.Size = new System.Drawing.Size(116, 20);
            this._TH_L_TextBox.TabIndex = 55;
            this._TH_L_TextBox.Tag = "128";
            this._TH_L_TextBox.Text = "0";
            this._TH_L_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TH_X_TextBox
            // 
            this._TH_X_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TH_X_TextBox.Location = new System.Drawing.Point(203, 38);
            this._TH_X_TextBox.Name = "_TH_X_TextBox";
            this._TH_X_TextBox.Size = new System.Drawing.Size(116, 20);
            this._TH_X_TextBox.TabIndex = 56;
            this._TH_X_TextBox.Tag = "128";
            this._TH_X_TextBox.Text = "0";
            this._TH_X_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _typeTn
            // 
            this._typeTn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._typeTn.FormattingEnabled = true;
            this._typeTn.Location = new System.Drawing.Point(203, 57);
            this._typeTn.Name = "_typeTn";
            this._typeTn.Size = new System.Drawing.Size(116, 21);
            this._typeTn.TabIndex = 60;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 60);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(30, 13);
            this.label43.TabIndex = 59;
            this.label43.Text = "Uo =";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 41);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(189, 13);
            this.label42.TabIndex = 58;
            this.label42.Text = "Коэффициент трансформации TH X";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(188, 13);
            this.label41.TabIndex = 57;
            this.label41.Text = "Коэффициент трансформации TH L";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this._i0CorrectionL2);
            this.groupBox35.Controls.Add(this.label32);
            this.groupBox35.Controls.Add(this.label60);
            this.groupBox35.Controls.Add(this.label46);
            this.groupBox35.Controls.Add(this._TT_X2_TextBox);
            this.groupBox35.Controls.Add(this._TT_L2_TextBox);
            this.groupBox35.Controls.Add(this._polarityX2);
            this.groupBox35.Controls.Add(this._polarityL2);
            this.groupBox35.Controls.Add(this.label35);
            this.groupBox35.Controls.Add(this.label34);
            this.groupBox35.Controls.Add(this.label33);
            this.groupBox35.Controls.Add(this.label31);
            this.groupBox35.Location = new System.Drawing.Point(8, 156);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(343, 126);
            this.groupBox35.TabIndex = 63;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Сторона 2";
            // 
            // _i0CorrectionL2
            // 
            this._i0CorrectionL2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._i0CorrectionL2.FormattingEnabled = true;
            this._i0CorrectionL2.Location = new System.Drawing.Point(203, 97);
            this._i0CorrectionL2.Name = "_i0CorrectionL2";
            this._i0CorrectionL2.Size = new System.Drawing.Size(116, 21);
            this._i0CorrectionL2.TabIndex = 70;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 99);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(89, 13);
            this.label32.TabIndex = 69;
            this.label32.Text = "Коррекция I0 L2";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.ForeColor = System.Drawing.Color.Red;
            this.label60.Location = new System.Drawing.Point(320, 60);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(20, 13);
            this.label60.TabIndex = 68;
            this.label60.Text = "[А]";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(320, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(20, 13);
            this.label46.TabIndex = 67;
            this.label46.Text = "[А]";
            // 
            // _TT_X2_TextBox
            // 
            this._TT_X2_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TT_X2_TextBox.Location = new System.Drawing.Point(203, 58);
            this._TT_X2_TextBox.Name = "_TT_X2_TextBox";
            this._TT_X2_TextBox.Size = new System.Drawing.Size(116, 20);
            this._TT_X2_TextBox.TabIndex = 60;
            this._TT_X2_TextBox.Tag = "65535";
            this._TT_X2_TextBox.Text = "0";
            this._TT_X2_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TT_L2_TextBox
            // 
            this._TT_L2_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TT_L2_TextBox.Location = new System.Drawing.Point(203, 19);
            this._TT_L2_TextBox.Name = "_TT_L2_TextBox";
            this._TT_L2_TextBox.Size = new System.Drawing.Size(116, 20);
            this._TT_L2_TextBox.TabIndex = 59;
            this._TT_L2_TextBox.Tag = "65535";
            this._TT_L2_TextBox.Text = "0";
            this._TT_L2_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _polarityX2
            // 
            this._polarityX2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarityX2.FormattingEnabled = true;
            this._polarityX2.Location = new System.Drawing.Point(203, 77);
            this._polarityX2.Name = "_polarityX2";
            this._polarityX2.Size = new System.Drawing.Size(116, 21);
            this._polarityX2.TabIndex = 66;
            // 
            // _polarityL2
            // 
            this._polarityL2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarityL2.FormattingEnabled = true;
            this._polarityL2.Location = new System.Drawing.Point(203, 38);
            this._polarityL2.Name = "_polarityL2";
            this._polarityL2.Size = new System.Drawing.Size(116, 21);
            this._polarityL2.TabIndex = 65;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(154, 13);
            this.label35.TabIndex = 64;
            this.label35.Text = "Полярность подключения X2";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 60);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(190, 13);
            this.label34.TabIndex = 63;
            this.label34.Text = "Номинальный первичный ток TT X2";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 40);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(153, 13);
            this.label33.TabIndex = 62;
            this.label33.Text = "Полярность подключения L2";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(189, 13);
            this.label31.TabIndex = 61;
            this.label31.Text = "Номинальный первичный ток TT L2";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._typeTT1);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this._i0CorrectionL1);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this._TT_X1_TextBox);
            this.groupBox3.Controls.Add(this._TT_L1_TextBox);
            this.groupBox3.Controls.Add(this._polarityX1);
            this.groupBox3.Controls.Add(this._polarityL1);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(343, 147);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сторона 1";
            // 
            // _typeTT1
            // 
            this._typeTT1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._typeTT1.FormattingEnabled = true;
            this._typeTT1.Location = new System.Drawing.Point(203, 117);
            this._typeTT1.Name = "_typeTT1";
            this._typeTT1.Size = new System.Drawing.Size(116, 21);
            this._typeTT1.TabIndex = 64;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 120);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(115, 13);
            this.label38.TabIndex = 62;
            this.label38.Text = "Тип трансформатора";
            // 
            // _i0CorrectionL1
            // 
            this._i0CorrectionL1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._i0CorrectionL1.FormattingEnabled = true;
            this._i0CorrectionL1.Location = new System.Drawing.Point(203, 97);
            this._i0CorrectionL1.Name = "_i0CorrectionL1";
            this._i0CorrectionL1.Size = new System.Drawing.Size(116, 21);
            this._i0CorrectionL1.TabIndex = 68;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 100);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(89, 13);
            this.label27.TabIndex = 67;
            this.label27.Text = "Коррекция I0 L1";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(320, 60);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(20, 13);
            this.label45.TabIndex = 66;
            this.label45.Text = "[А]";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(320, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(20, 13);
            this.label44.TabIndex = 65;
            this.label44.Text = "[А]";
            // 
            // _TT_X1_TextBox
            // 
            this._TT_X1_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TT_X1_TextBox.Location = new System.Drawing.Point(203, 58);
            this._TT_X1_TextBox.Name = "_TT_X1_TextBox";
            this._TT_X1_TextBox.Size = new System.Drawing.Size(116, 20);
            this._TT_X1_TextBox.TabIndex = 59;
            this._TT_X1_TextBox.Tag = "65535";
            this._TT_X1_TextBox.Text = "0";
            this._TT_X1_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TT_L1_TextBox
            // 
            this._TT_L1_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TT_L1_TextBox.Location = new System.Drawing.Point(203, 19);
            this._TT_L1_TextBox.Name = "_TT_L1_TextBox";
            this._TT_L1_TextBox.Size = new System.Drawing.Size(116, 20);
            this._TT_L1_TextBox.TabIndex = 58;
            this._TT_L1_TextBox.Tag = "65535";
            this._TT_L1_TextBox.Text = "0";
            this._TT_L1_TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _polarityX1
            // 
            this._polarityX1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarityX1.FormattingEnabled = true;
            this._polarityX1.Location = new System.Drawing.Point(203, 77);
            this._polarityX1.Name = "_polarityX1";
            this._polarityX1.Size = new System.Drawing.Size(116, 21);
            this._polarityX1.TabIndex = 64;
            // 
            // _polarityL1
            // 
            this._polarityL1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._polarityL1.FormattingEnabled = true;
            this._polarityL1.Location = new System.Drawing.Point(203, 38);
            this._polarityL1.Name = "_polarityL1";
            this._polarityL1.Size = new System.Drawing.Size(116, 21);
            this._polarityL1.TabIndex = 63;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 80);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(154, 13);
            this.label30.TabIndex = 62;
            this.label30.Text = "Полярность подключения X1";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 60);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(190, 13);
            this.label29.TabIndex = 61;
            this.label29.Text = "Номинальный первичный ток TT X1";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(153, 13);
            this.label28.TabIndex = 60;
            this.label28.Text = "Полярность подключения L1";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(189, 13);
            this.label26.TabIndex = 57;
            this.label26.Text = "Номинальный первичный ток TT L1";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(222, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР801двг";
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.Controls.Add(this._measureTransPage);
            this._configurationTabControl.Controls.Add(this._powTransPage);
            this._configurationTabControl.Controls.Add(this._inputSygnalsPage);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._automatic);
            this._configurationTabControl.Controls.Add(this._allDefensesPage);
            this._configurationTabControl.Controls.Add(this._oscPage);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(892, 579);
            this._configurationTabControl.TabIndex = 30;
            // 
            // _powTransPage
            // 
            this._powTransPage.Controls.Add(this.groupBox20);
            this._powTransPage.Controls.Add(this.groupBox1);
            this._powTransPage.Location = new System.Drawing.Point(4, 22);
            this._powTransPage.Name = "_powTransPage";
            this._powTransPage.Padding = new System.Windows.Forms.Padding(3);
            this._powTransPage.Size = new System.Drawing.Size(884, 553);
            this._powTransPage.TabIndex = 0;
            this._powTransPage.Text = "Парам. двигателя";
            this._powTransPage.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._sn);
            this.groupBox20.Controls.Add(this.label39);
            this.groupBox20.Controls.Add(this._passportKpd);
            this.groupBox20.Controls.Add(this.label66);
            this.groupBox20.Controls.Add(this._passportCos);
            this.groupBox20.Controls.Add(this._passportWatt);
            this.groupBox20.Controls.Add(this.label67);
            this.groupBox20.Controls.Add(this._passportP);
            this.groupBox20.Controls.Add(this.label68);
            this.groupBox20.Location = new System.Drawing.Point(226, 6);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(241, 111);
            this.groupBox20.TabIndex = 12;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Паспортные данные";
            // 
            // _sn
            // 
            this._sn.Location = new System.Drawing.Point(147, 83);
            this._sn.Name = "_sn";
            this._sn.ReadOnly = true;
            this._sn.Size = new System.Drawing.Size(88, 20);
            this._sn.TabIndex = 24;
            this._sn.Tag = "65000";
            this._sn.Text = "0";
            this._sn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 86);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(135, 13);
            this.label39.TabIndex = 18;
            this.label39.Text = "Sн = Pн*100/(cosf*КПД) =";
            // 
            // _passportKpd
            // 
            this._passportKpd.Location = new System.Drawing.Point(57, 57);
            this._passportKpd.Name = "_passportKpd";
            this._passportKpd.Size = new System.Drawing.Size(69, 20);
            this._passportKpd.TabIndex = 24;
            this._passportKpd.Tag = "65000";
            this._passportKpd.Text = "0";
            this._passportKpd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._passportKpd.TextChanged += new System.EventHandler(this._passport_TextChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 60);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(45, 13);
            this.label66.TabIndex = 18;
            this.label66.Text = "КПД, %";
            // 
            // _passportCos
            // 
            this._passportCos.Location = new System.Drawing.Point(57, 38);
            this._passportCos.Name = "_passportCos";
            this._passportCos.Size = new System.Drawing.Size(69, 20);
            this._passportCos.TabIndex = 23;
            this._passportCos.Tag = "65000";
            this._passportCos.Text = "0";
            this._passportCos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._passportCos.TextChanged += new System.EventHandler(this._passport_TextChanged);
            // 
            // _passportWatt
            // 
            this._passportWatt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._passportWatt.FormattingEnabled = true;
            this._passportWatt.Location = new System.Drawing.Point(127, 19);
            this._passportWatt.Name = "_passportWatt";
            this._passportWatt.Size = new System.Drawing.Size(45, 21);
            this._passportWatt.TabIndex = 29;
            this._passportWatt.SelectedIndexChanged += new System.EventHandler(this._passportWatt_SelectedIndexChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 41);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(27, 13);
            this.label67.TabIndex = 17;
            this.label67.Text = "cosf";
            // 
            // _passportP
            // 
            this._passportP.Location = new System.Drawing.Point(57, 19);
            this._passportP.Name = "_passportP";
            this._passportP.Size = new System.Drawing.Size(69, 20);
            this._passportP.TabIndex = 16;
            this._passportP.Tag = "65000";
            this._passportP.Text = "0";
            this._passportP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._passportP.TextChanged += new System.EventHandler(this._passport_TextChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 22);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(14, 13);
            this.label68.TabIndex = 15;
            this.label68.Text = "P";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._engineNreset);
            this.groupBox1.Controls.Add(this._engineQresetGr1);
            this.groupBox1.Controls.Add(this._qBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._tCount);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._tStartBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._iStartBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this._engineIdv);
            this.groupBox1.Controls.Add(this.label152);
            this.groupBox1.Controls.Add(this._engineCoolingTimeGr1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this._engineHeatingTimeGr1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 204);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры двигателя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Вход N сброс";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Вход Q сброс";
            // 
            // _engineNreset
            // 
            this._engineNreset.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._engineNreset.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._engineNreset.FormattingEnabled = true;
            this._engineNreset.Location = new System.Drawing.Point(108, 172);
            this._engineNreset.Name = "_engineNreset";
            this._engineNreset.Size = new System.Drawing.Size(90, 21);
            this._engineNreset.TabIndex = 30;
            // 
            // _engineQresetGr1
            // 
            this._engineQresetGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._engineQresetGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._engineQresetGr1.FormattingEnabled = true;
            this._engineQresetGr1.Location = new System.Drawing.Point(108, 152);
            this._engineQresetGr1.Name = "_engineQresetGr1";
            this._engineQresetGr1.Size = new System.Drawing.Size(90, 21);
            this._engineQresetGr1.TabIndex = 29;
            // 
            // _qBox
            // 
            this._qBox.Location = new System.Drawing.Point(129, 133);
            this._qBox.Name = "_qBox";
            this._qBox.Size = new System.Drawing.Size(69, 20);
            this._qBox.TabIndex = 27;
            this._qBox.Tag = "65000";
            this._qBox.Text = "0";
            this._qBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Qгор, %";
            // 
            // _tCount
            // 
            this._tCount.Location = new System.Drawing.Point(129, 114);
            this._tCount.Name = "_tCount";
            this._tCount.Size = new System.Drawing.Size(69, 20);
            this._tCount.TabIndex = 26;
            this._tCount.Tag = "65000";
            this._tCount.Text = "0";
            this._tCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Tдлит, с";
            // 
            // _tStartBox
            // 
            this._tStartBox.Location = new System.Drawing.Point(129, 95);
            this._tStartBox.Name = "_tStartBox";
            this._tStartBox.Size = new System.Drawing.Size(69, 20);
            this._tStartBox.TabIndex = 25;
            this._tStartBox.Tag = "65000";
            this._tStartBox.Text = "0";
            this._tStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Tпуск, мс";
            // 
            // _iStartBox
            // 
            this._iStartBox.Location = new System.Drawing.Point(129, 76);
            this._iStartBox.Name = "_iStartBox";
            this._iStartBox.Size = new System.Drawing.Size(69, 20);
            this._iStartBox.TabIndex = 28;
            this._iStartBox.Tag = "65000";
            this._iStartBox.Text = "0";
            this._iStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Iпуск, Iн";
            // 
            // _engineIdv
            // 
            this._engineIdv.Location = new System.Drawing.Point(129, 57);
            this._engineIdv.Name = "_engineIdv";
            this._engineIdv.Size = new System.Drawing.Size(69, 20);
            this._engineIdv.TabIndex = 24;
            this._engineIdv.Tag = "65000";
            this._engineIdv.Text = "0";
            this._engineIdv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(6, 60);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(37, 13);
            this.label152.TabIndex = 18;
            this.label152.Text = "Iдв, Iн";
            // 
            // _engineCoolingTimeGr1
            // 
            this._engineCoolingTimeGr1.Location = new System.Drawing.Point(129, 38);
            this._engineCoolingTimeGr1.Name = "_engineCoolingTimeGr1";
            this._engineCoolingTimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineCoolingTimeGr1.TabIndex = 23;
            this._engineCoolingTimeGr1.Tag = "65000";
            this._engineCoolingTimeGr1.Text = "0";
            this._engineCoolingTimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Т охлаждения, с";
            // 
            // _engineHeatingTimeGr1
            // 
            this._engineHeatingTimeGr1.Location = new System.Drawing.Point(129, 19);
            this._engineHeatingTimeGr1.Name = "_engineHeatingTimeGr1";
            this._engineHeatingTimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineHeatingTimeGr1.TabIndex = 16;
            this._engineHeatingTimeGr1.Tag = "65000";
            this._engineHeatingTimeGr1.Text = "0";
            this._engineHeatingTimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "T нагрева,c";
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox26);
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Controls.Add(this.groupBox11);
            this._outputSignalsPage.Controls.Add(this.groupBox12);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Size = new System.Drawing.Size(884, 553);
            this._outputSignalsPage.TabIndex = 6;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._fault2CheckBox);
            this.groupBox26.Controls.Add(this._fault5CheckBox);
            this.groupBox26.Controls.Add(this._fault4CheckBox);
            this.groupBox26.Controls.Add(this._fault3CheckBox);
            this.groupBox26.Controls.Add(this._fault1CheckBox);
            this.groupBox26.Controls.Add(this.label57);
            this.groupBox26.Controls.Add(this.label4);
            this.groupBox26.Controls.Add(this._fault4Label);
            this.groupBox26.Controls.Add(this._impTB);
            this.groupBox26.Controls.Add(this._fault3Label);
            this.groupBox26.Controls.Add(this._fault2Label);
            this.groupBox26.Controls.Add(this._fault1Label);
            this.groupBox26.Location = new System.Drawing.Point(8, 394);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(393, 150);
            this.groupBox26.TabIndex = 6;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Реле неисправность";
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(198, 39);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 12;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault5CheckBox
            // 
            this._fault5CheckBox.AutoSize = true;
            this._fault5CheckBox.Location = new System.Drawing.Point(198, 84);
            this._fault5CheckBox.Name = "_fault5CheckBox";
            this._fault5CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault5CheckBox.TabIndex = 12;
            this._fault5CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(198, 69);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 12;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(198, 54);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 12;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(198, 24);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 12;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(22, 85);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(148, 13);
            this.label57.TabIndex = 9;
            this.label57.Text = "5. Неисправность цепей ТТ";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Твозвр, мс";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _fault4Label
            // 
            this._fault4Label.AutoSize = true;
            this._fault4Label.Location = new System.Drawing.Point(22, 70);
            this._fault4Label.Name = "_fault4Label";
            this._fault4Label.Size = new System.Drawing.Size(169, 13);
            this._fault4Label.TabIndex = 9;
            this._fault4Label.Text = "4. Неисправность выключателя";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(198, 115);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(121, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Tag = "3276700 ";
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _fault3Label
            // 
            this._fault3Label.AutoSize = true;
            this._fault3Label.Location = new System.Drawing.Point(22, 55);
            this._fault3Label.Name = "_fault3Label";
            this._fault3Label.Size = new System.Drawing.Size(157, 13);
            this._fault3Label.TabIndex = 2;
            this._fault3Label.Text = "3. Неисправность измерений";
            // 
            // _fault2Label
            // 
            this._fault2Label.AutoSize = true;
            this._fault2Label.Location = new System.Drawing.Point(22, 40);
            this._fault2Label.Name = "_fault2Label";
            this._fault2Label.Size = new System.Drawing.Size(170, 13);
            this._fault2Label.TabIndex = 1;
            this._fault2Label.Text = "2. Программная неисправность";
            // 
            // _fault1Label
            // 
            this._fault1Label.AutoSize = true;
            this._fault1Label.Location = new System.Drawing.Point(22, 25);
            this._fault1Label.Name = "_fault1Label";
            this._fault1Label.Size = new System.Drawing.Size(159, 13);
            this._fault1Label.TabIndex = 0;
            this._fault1Label.Text = "1. Аппаратная неисправность";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.VLSTabControl);
            this.groupBox13.Location = new System.Drawing.Point(407, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(402, 546);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "ВЛС";
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Controls.Add(this.VLS9);
            this.VLSTabControl.Controls.Add(this.VLS10);
            this.VLSTabControl.Controls.Add(this.VLS11);
            this.VLSTabControl.Controls.Add(this.VLS12);
            this.VLSTabControl.Controls.Add(this.VLS13);
            this.VLSTabControl.Controls.Add(this.VLS14);
            this.VLSTabControl.Controls.Add(this.VLS15);
            this.VLSTabControl.Controls.Add(this.VLS16);
            this.VLSTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VLSTabControl.Location = new System.Drawing.Point(3, 16);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(396, 527);
            this.VLSTabControl.TabIndex = 0;
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLScheckedListBox11);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(388, 474);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLScheckedListBox12);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(388, 474);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLScheckedListBox13);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(388, 474);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLScheckedListBox14);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(388, 474);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLScheckedListBox15);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(388, 474);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLScheckedListBox16);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(388, 474);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // _automatic
            // 
            this._automatic.Controls.Add(this._switch);
            this._automatic.Controls.Add(this.switchGroup);
            this._automatic.Controls.Add(this.tabControl4);
            this._automatic.Controls.Add(this.controlGroup);
            this._automatic.Location = new System.Drawing.Point(4, 22);
            this._automatic.Name = "_automatic";
            this._automatic.Padding = new System.Windows.Forms.Padding(3);
            this._automatic.Size = new System.Drawing.Size(884, 553);
            this._automatic.TabIndex = 9;
            this._automatic.Text = "Автоматика и управление";
            this._automatic.UseVisualStyleBackColor = true;
            // 
            // _switch
            // 
            this._switch.AutoSize = true;
            this._switch.Location = new System.Drawing.Point(91, 7);
            this._switch.Name = "_switch";
            this._switch.Size = new System.Drawing.Size(15, 14);
            this._switch.TabIndex = 27;
            this._switch.UseVisualStyleBackColor = true;
            this._switch.CheckedChanged += new System.EventHandler(this._switch_CheckedChanged);
            // 
            // switchGroup
            // 
            this.switchGroup.Controls.Add(this._sdtuBlock);
            this.switchGroup.Controls.Add(this._switchKontCep);
            this.switchGroup.Controls.Add(this._switchTUskor);
            this.switchGroup.Controls.Add(this._switchImp);
            this.switchGroup.Controls.Add(this._switchIUrov);
            this.switchGroup.Controls.Add(this._switchTUrov);
            this.switchGroup.Controls.Add(this._switchBlock);
            this.switchGroup.Controls.Add(this._switchError);
            this.switchGroup.Controls.Add(this._switchOn);
            this.switchGroup.Controls.Add(this.label69);
            this.switchGroup.Controls.Add(this._switchOff);
            this.switchGroup.Controls.Add(this.label97);
            this.switchGroup.Controls.Add(this.label98);
            this.switchGroup.Controls.Add(this.label99);
            this.switchGroup.Controls.Add(this.label91);
            this.switchGroup.Controls.Add(this.label92);
            this.switchGroup.Controls.Add(this.label93);
            this.switchGroup.Controls.Add(this.label90);
            this.switchGroup.Controls.Add(this.label89);
            this.switchGroup.Controls.Add(this.label88);
            this.switchGroup.Enabled = false;
            this.switchGroup.Location = new System.Drawing.Point(8, 6);
            this.switchGroup.Name = "switchGroup";
            this.switchGroup.Size = new System.Drawing.Size(208, 233);
            this.switchGroup.TabIndex = 14;
            this.switchGroup.TabStop = false;
            this.switchGroup.Text = "Выключатель";
            // 
            // _sdtuBlock
            // 
            this._sdtuBlock.FormattingEnabled = true;
            this._sdtuBlock.Location = new System.Drawing.Point(97, 196);
            this._sdtuBlock.Name = "_sdtuBlock";
            this._sdtuBlock.Size = new System.Drawing.Size(105, 21);
            this._sdtuBlock.TabIndex = 26;
            // 
            // _switchKontCep
            // 
            this._switchKontCep.FormattingEnabled = true;
            this._switchKontCep.Location = new System.Drawing.Point(97, 176);
            this._switchKontCep.Name = "_switchKontCep";
            this._switchKontCep.Size = new System.Drawing.Size(105, 21);
            this._switchKontCep.TabIndex = 26;
            // 
            // _switchTUskor
            // 
            this._switchTUskor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUskor.Location = new System.Drawing.Point(97, 157);
            this._switchTUskor.Name = "_switchTUskor";
            this._switchTUskor.Size = new System.Drawing.Size(105, 20);
            this._switchTUskor.TabIndex = 25;
            this._switchTUskor.Tag = "3276700";
            this._switchTUskor.Text = "0";
            this._switchTUskor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchImp
            // 
            this._switchImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchImp.Location = new System.Drawing.Point(97, 138);
            this._switchImp.Name = "_switchImp";
            this._switchImp.Size = new System.Drawing.Size(105, 20);
            this._switchImp.TabIndex = 24;
            this._switchImp.Tag = "3276700";
            this._switchImp.Text = "0";
            this._switchImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchIUrov
            // 
            this._switchIUrov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchIUrov.Location = new System.Drawing.Point(97, 119);
            this._switchIUrov.Name = "_switchIUrov";
            this._switchIUrov.Size = new System.Drawing.Size(105, 20);
            this._switchIUrov.TabIndex = 23;
            this._switchIUrov.Tag = "40";
            this._switchIUrov.Text = "0";
            this._switchIUrov.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchTUrov
            // 
            this._switchTUrov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUrov.Location = new System.Drawing.Point(97, 100);
            this._switchTUrov.Name = "_switchTUrov";
            this._switchTUrov.Size = new System.Drawing.Size(105, 20);
            this._switchTUrov.TabIndex = 22;
            this._switchTUrov.Tag = "3276700";
            this._switchTUrov.Text = "0";
            this._switchTUrov.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchBlock
            // 
            this._switchBlock.FormattingEnabled = true;
            this._switchBlock.Location = new System.Drawing.Point(97, 81);
            this._switchBlock.Name = "_switchBlock";
            this._switchBlock.Size = new System.Drawing.Size(105, 21);
            this._switchBlock.TabIndex = 21;
            // 
            // _switchError
            // 
            this._switchError.FormattingEnabled = true;
            this._switchError.Location = new System.Drawing.Point(97, 61);
            this._switchError.Name = "_switchError";
            this._switchError.Size = new System.Drawing.Size(105, 21);
            this._switchError.TabIndex = 20;
            // 
            // _switchOn
            // 
            this._switchOn.FormattingEnabled = true;
            this._switchOn.Location = new System.Drawing.Point(97, 41);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(105, 21);
            this._switchOn.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 197);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(69, 13);
            this.label69.TabIndex = 17;
            this.label69.Text = "Блок. СДТУ";
            // 
            // _switchOff
            // 
            this._switchOff.FormattingEnabled = true;
            this._switchOff.Location = new System.Drawing.Point(97, 21);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(105, 21);
            this._switchOff.TabIndex = 18;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 177);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(58, 13);
            this.label97.TabIndex = 17;
            this.label97.Text = "Конт. цеп.";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(6, 157);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(59, 13);
            this.label98.TabIndex = 16;
            this.label98.Text = "tускор, мс";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 138);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(72, 13);
            this.label99.TabIndex = 15;
            this.label99.Text = "Импульс, мс";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(6, 120);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(48, 13);
            this.label91.TabIndex = 14;
            this.label91.Text = "Iуров, Iн";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(6, 100);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(53, 13);
            this.label92.TabIndex = 13;
            this.label92.Text = "tуров, мс";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 82);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(50, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Блок-ка.";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 62);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(48, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Неиспр.";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 42);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(42, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Включ.";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 24);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Отключ.";
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage30);
            this.tabControl4.Controls.Add(this.tabPage31);
            this.tabControl4.Controls.Add(this.tabPage32);
            this.tabControl4.Location = new System.Drawing.Point(8, 245);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(419, 196);
            this.tabControl4.TabIndex = 16;
            // 
            // tabPage30
            // 
            this.tabPage30.Controls.Add(this.groupBox29);
            this.tabPage30.Location = new System.Drawing.Point(4, 22);
            this.tabPage30.Name = "tabPage30";
            this.tabPage30.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage30.Size = new System.Drawing.Size(411, 170);
            this.tabPage30.TabIndex = 0;
            this.tabPage30.Text = "АПВ";
            this.tabPage30.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this._autoSwitch);
            this.groupBox29.Controls.Add(this.label111);
            this.groupBox29.Controls.Add(this.label110);
            this.groupBox29.Controls.Add(this.label109);
            this.groupBox29.Controls.Add(this.label96);
            this.groupBox29.Controls.Add(this._apv2Krat);
            this.groupBox29.Controls.Add(this._apv1Krat);
            this.groupBox29.Controls.Add(this._apvTReady);
            this.groupBox29.Controls.Add(this._apvTBlock);
            this.groupBox29.Controls.Add(this._apvBlocking);
            this.groupBox29.Controls.Add(this.label95);
            this.groupBox29.Controls.Add(this._apvModes);
            this.groupBox29.Controls.Add(this.label94);
            this.groupBox29.Location = new System.Drawing.Point(3, 3);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(405, 160);
            this.groupBox29.TabIndex = 0;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "АПВ";
            // 
            // _autoSwitch
            // 
            this._autoSwitch.AutoSize = true;
            this._autoSwitch.Location = new System.Drawing.Point(9, 141);
            this._autoSwitch.Name = "_autoSwitch";
            this._autoSwitch.Size = new System.Drawing.Size(113, 17);
            this._autoSwitch.TabIndex = 14;
            this._autoSwitch.Text = "Самоотключение";
            this._autoSwitch.UseVisualStyleBackColor = true;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 119);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(57, 13);
            this.label111.TabIndex = 12;
            this.label111.Text = "2Крат, мс";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 100);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(57, 13);
            this.label110.TabIndex = 11;
            this.label110.Text = "1Крат, мс";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 81);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(58, 13);
            this.label109.TabIndex = 10;
            this.label109.Text = "tготов, мс";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 62);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(54, 13);
            this.label96.TabIndex = 9;
            this.label96.Text = "tблок, мс";
            // 
            // _apv2Krat
            // 
            this._apv2Krat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apv2Krat.Location = new System.Drawing.Point(95, 116);
            this._apv2Krat.Name = "_apv2Krat";
            this._apv2Krat.Size = new System.Drawing.Size(121, 20);
            this._apv2Krat.TabIndex = 7;
            this._apv2Krat.Tag = "3276700";
            this._apv2Krat.Text = "0";
            this._apv2Krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apv1Krat
            // 
            this._apv1Krat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apv1Krat.Location = new System.Drawing.Point(95, 97);
            this._apv1Krat.Name = "_apv1Krat";
            this._apv1Krat.Size = new System.Drawing.Size(121, 20);
            this._apv1Krat.TabIndex = 6;
            this._apv1Krat.Tag = "3276700";
            this._apv1Krat.Text = "0";
            this._apv1Krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTReady
            // 
            this._apvTReady.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTReady.Location = new System.Drawing.Point(95, 78);
            this._apvTReady.Name = "_apvTReady";
            this._apvTReady.Size = new System.Drawing.Size(121, 20);
            this._apvTReady.TabIndex = 5;
            this._apvTReady.Tag = "3276700";
            this._apvTReady.Text = "0";
            this._apvTReady.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTBlock
            // 
            this._apvTBlock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTBlock.Location = new System.Drawing.Point(95, 59);
            this._apvTBlock.Name = "_apvTBlock";
            this._apvTBlock.Size = new System.Drawing.Size(121, 20);
            this._apvTBlock.TabIndex = 4;
            this._apvTBlock.Tag = "3276700";
            this._apvTBlock.Text = "0";
            this._apvTBlock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvBlocking
            // 
            this._apvBlocking.FormattingEnabled = true;
            this._apvBlocking.Location = new System.Drawing.Point(95, 39);
            this._apvBlocking.Name = "_apvBlocking";
            this._apvBlocking.Size = new System.Drawing.Size(121, 21);
            this._apvBlocking.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(6, 42);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(68, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Блокировка";
            // 
            // _apvModes
            // 
            this._apvModes.FormattingEnabled = true;
            this._apvModes.Location = new System.Drawing.Point(95, 19);
            this._apvModes.Name = "_apvModes";
            this._apvModes.Size = new System.Drawing.Size(121, 21);
            this._apvModes.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(6, 22);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(42, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "Режим";
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this.groupBox30);
            this.tabPage31.Location = new System.Drawing.Point(4, 22);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage31.Size = new System.Drawing.Size(411, 170);
            this.tabPage31.TabIndex = 1;
            this.tabPage31.Text = "АВР";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this._avrDef);
            this.groupBox30.Controls.Add(this._avrSelfOff);
            this.groupBox30.Controls.Add(this._avrSwitchOff);
            this.groupBox30.Controls.Add(this._avrSignal);
            this.groupBox30.Controls.Add(this.label125);
            this.groupBox30.Controls.Add(this._avrClear);
            this.groupBox30.Controls.Add(this.label124);
            this.groupBox30.Controls.Add(this._avrTOff);
            this.groupBox30.Controls.Add(this.label123);
            this.groupBox30.Controls.Add(this._avrTBack);
            this.groupBox30.Controls.Add(this.label122);
            this.groupBox30.Controls.Add(this._avrBack);
            this.groupBox30.Controls.Add(this.label121);
            this.groupBox30.Controls.Add(this._avrTSr);
            this.groupBox30.Controls.Add(this.label120);
            this.groupBox30.Controls.Add(this._avrResolve);
            this.groupBox30.Controls.Add(this.label119);
            this.groupBox30.Controls.Add(this._avrBlockClear);
            this.groupBox30.Controls.Add(this.label118);
            this.groupBox30.Controls.Add(this._avrBlocking);
            this.groupBox30.Controls.Add(this.label117);
            this.groupBox30.Controls.Add(this._avrSIGNOn);
            this.groupBox30.Controls.Add(this.label116);
            this.groupBox30.Controls.Add(this.label115);
            this.groupBox30.Controls.Add(this.label114);
            this.groupBox30.Controls.Add(this.label113);
            this.groupBox30.Location = new System.Drawing.Point(3, 3);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(405, 161);
            this.groupBox30.TabIndex = 0;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "АВР";
            // 
            // _avrDef
            // 
            this._avrDef.AutoSize = true;
            this._avrDef.Location = new System.Drawing.Point(88, 80);
            this._avrDef.Name = "_avrDef";
            this._avrDef.Size = new System.Drawing.Size(15, 14);
            this._avrDef.TabIndex = 26;
            this._avrDef.UseVisualStyleBackColor = true;
            // 
            // _avrSelfOff
            // 
            this._avrSelfOff.AutoSize = true;
            this._avrSelfOff.Location = new System.Drawing.Point(88, 60);
            this._avrSelfOff.Name = "_avrSelfOff";
            this._avrSelfOff.Size = new System.Drawing.Size(15, 14);
            this._avrSelfOff.TabIndex = 26;
            this._avrSelfOff.UseVisualStyleBackColor = true;
            // 
            // _avrSwitchOff
            // 
            this._avrSwitchOff.AutoSize = true;
            this._avrSwitchOff.Location = new System.Drawing.Point(88, 40);
            this._avrSwitchOff.Name = "_avrSwitchOff";
            this._avrSwitchOff.Size = new System.Drawing.Size(15, 14);
            this._avrSwitchOff.TabIndex = 26;
            this._avrSwitchOff.UseVisualStyleBackColor = true;
            // 
            // _avrSignal
            // 
            this._avrSignal.AutoSize = true;
            this._avrSignal.Location = new System.Drawing.Point(88, 20);
            this._avrSignal.Name = "_avrSignal";
            this._avrSignal.Size = new System.Drawing.Size(15, 14);
            this._avrSignal.TabIndex = 26;
            this._avrSignal.UseVisualStyleBackColor = true;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(202, 116);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(38, 13);
            this.label125.TabIndex = 25;
            this.label125.Text = "Сброс";
            // 
            // _avrClear
            // 
            this._avrClear.FormattingEnabled = true;
            this._avrClear.Location = new System.Drawing.Point(279, 113);
            this._avrClear.Name = "_avrClear";
            this._avrClear.Size = new System.Drawing.Size(108, 21);
            this._avrClear.TabIndex = 24;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(202, 97);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(53, 13);
            this.label124.TabIndex = 23;
            this.label124.Text = "tоткл, мс";
            // 
            // _avrTOff
            // 
            this._avrTOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTOff.Location = new System.Drawing.Point(279, 94);
            this._avrTOff.Name = "_avrTOff";
            this._avrTOff.Size = new System.Drawing.Size(108, 20);
            this._avrTOff.TabIndex = 22;
            this._avrTOff.Tag = "3276700";
            this._avrTOff.Text = "0";
            this._avrTOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(202, 78);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(48, 13);
            this.label123.TabIndex = 21;
            this.label123.Text = "tвоз, мс";
            // 
            // _avrTBack
            // 
            this._avrTBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTBack.Location = new System.Drawing.Point(279, 75);
            this._avrTBack.Name = "_avrTBack";
            this._avrTBack.Size = new System.Drawing.Size(108, 20);
            this._avrTBack.TabIndex = 20;
            this._avrTBack.Tag = "3276700";
            this._avrTBack.Text = "0";
            this._avrTBack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(202, 59);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(49, 13);
            this.label122.TabIndex = 19;
            this.label122.Text = "Возврат";
            // 
            // _avrBack
            // 
            this._avrBack.FormattingEnabled = true;
            this._avrBack.Location = new System.Drawing.Point(279, 56);
            this._avrBack.Name = "_avrBack";
            this._avrBack.Size = new System.Drawing.Size(108, 21);
            this._avrBack.TabIndex = 18;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(202, 39);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(42, 13);
            this.label121.TabIndex = 17;
            this.label121.Text = "tср, мс";
            // 
            // _avrTSr
            // 
            this._avrTSr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTSr.Location = new System.Drawing.Point(279, 37);
            this._avrTSr.Name = "_avrTSr";
            this._avrTSr.Size = new System.Drawing.Size(108, 20);
            this._avrTSr.TabIndex = 16;
            this._avrTSr.Tag = "3276700";
            this._avrTSr.Text = "0";
            this._avrTSr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(202, 21);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(72, 13);
            this.label120.TabIndex = 15;
            this.label120.Text = "АВР разреш.";
            // 
            // _avrResolve
            // 
            this._avrResolve.FormattingEnabled = true;
            this._avrResolve.Location = new System.Drawing.Point(279, 17);
            this._avrResolve.Name = "_avrResolve";
            this._avrResolve.Size = new System.Drawing.Size(108, 21);
            this._avrResolve.TabIndex = 14;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(6, 141);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(38, 13);
            this.label119.TabIndex = 13;
            this.label119.Text = "Сброс";
            // 
            // _avrBlockClear
            // 
            this._avrBlockClear.FormattingEnabled = true;
            this._avrBlockClear.Location = new System.Drawing.Point(88, 138);
            this._avrBlockClear.Name = "_avrBlockClear";
            this._avrBlockClear.Size = new System.Drawing.Size(108, 21);
            this._avrBlockClear.TabIndex = 12;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(6, 121);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(47, 13);
            this.label118.TabIndex = 11;
            this.label118.Text = "Блок-ка";
            // 
            // _avrBlocking
            // 
            this._avrBlocking.FormattingEnabled = true;
            this._avrBlocking.Location = new System.Drawing.Point(88, 118);
            this._avrBlocking.Name = "_avrBlocking";
            this._avrBlocking.Size = new System.Drawing.Size(108, 21);
            this._avrBlocking.TabIndex = 10;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(6, 101);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(62, 13);
            this.label117.TabIndex = 9;
            this.label117.Text = "СИГН пуск";
            // 
            // _avrSIGNOn
            // 
            this._avrSIGNOn.FormattingEnabled = true;
            this._avrSIGNOn.Location = new System.Drawing.Point(88, 98);
            this._avrSIGNOn.Name = "_avrSIGNOn";
            this._avrSIGNOn.Size = new System.Drawing.Size(108, 21);
            this._avrSIGNOn.TabIndex = 8;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 81);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(62, 13);
            this.label116.TabIndex = 7;
            this.label116.Text = "По защите";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(6, 61);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(76, 13);
            this.label115.TabIndex = 5;
            this.label115.Text = "По самооткл.";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(6, 41);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(63, 13);
            this.label114.TabIndex = 3;
            this.label114.Text = "По отключ.";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(6, 21);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(64, 13);
            this.label113.TabIndex = 1;
            this.label113.Text = "От сигнала";
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this.groupBox31);
            this.tabPage32.Location = new System.Drawing.Point(4, 22);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Size = new System.Drawing.Size(411, 170);
            this.tabPage32.TabIndex = 2;
            this.tabPage32.Text = "ЛЗШ";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label127);
            this.groupBox31.Controls.Add(this._lzhVal);
            this.groupBox31.Controls.Add(this._lzhModes);
            this.groupBox31.Controls.Add(this.label126);
            this.groupBox31.Location = new System.Drawing.Point(4, 3);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(404, 161);
            this.groupBox31.TabIndex = 1;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "ЛЗШ";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 42);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(65, 13);
            this.label127.TabIndex = 11;
            this.label127.Text = "Уставка, Iн";
            // 
            // _lzhVal
            // 
            this._lzhVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._lzhVal.Location = new System.Drawing.Point(95, 39);
            this._lzhVal.Name = "_lzhVal";
            this._lzhVal.Size = new System.Drawing.Size(121, 20);
            this._lzhVal.TabIndex = 10;
            this._lzhVal.Tag = "40";
            this._lzhVal.Text = "0";
            this._lzhVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _lzhModes
            // 
            this._lzhModes.FormattingEnabled = true;
            this._lzhModes.Location = new System.Drawing.Point(95, 19);
            this._lzhModes.Name = "_lzhModes";
            this._lzhModes.Size = new System.Drawing.Size(121, 21);
            this._lzhModes.TabIndex = 3;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 22);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(42, 13);
            this.label126.TabIndex = 2;
            this.label126.Text = "Режим";
            // 
            // controlGroup
            // 
            this.controlGroup.Controls.Add(this._switchBinding);
            this.controlGroup.Controls.Add(this._switchSDTU);
            this.controlGroup.Controls.Add(this._switchVnesh);
            this.controlGroup.Controls.Add(this._switchKey);
            this.controlGroup.Controls.Add(this._switchButtons);
            this.controlGroup.Controls.Add(this._switchVneshOff);
            this.controlGroup.Controls.Add(this._switchVneshOn);
            this.controlGroup.Controls.Add(this._switchKeyOff);
            this.controlGroup.Controls.Add(this._switchKeyOn);
            this.controlGroup.Controls.Add(this.label100);
            this.controlGroup.Controls.Add(this.label101);
            this.controlGroup.Controls.Add(this.label102);
            this.controlGroup.Controls.Add(this.label103);
            this.controlGroup.Controls.Add(this.label104);
            this.controlGroup.Controls.Add(this.label105);
            this.controlGroup.Controls.Add(this.label106);
            this.controlGroup.Controls.Add(this.label107);
            this.controlGroup.Controls.Add(this.label108);
            this.controlGroup.Enabled = false;
            this.controlGroup.Location = new System.Drawing.Point(222, 6);
            this.controlGroup.Name = "controlGroup";
            this.controlGroup.Size = new System.Drawing.Size(200, 233);
            this.controlGroup.TabIndex = 15;
            this.controlGroup.TabStop = false;
            this.controlGroup.Text = "Управление";
            // 
            // _switchBinding
            // 
            this._switchBinding.FormattingEnabled = true;
            this._switchBinding.Location = new System.Drawing.Point(89, 180);
            this._switchBinding.Name = "_switchBinding";
            this._switchBinding.Size = new System.Drawing.Size(105, 21);
            this._switchBinding.TabIndex = 35;
            // 
            // _switchSDTU
            // 
            this._switchSDTU.FormattingEnabled = true;
            this._switchSDTU.Location = new System.Drawing.Point(89, 160);
            this._switchSDTU.Name = "_switchSDTU";
            this._switchSDTU.Size = new System.Drawing.Size(105, 21);
            this._switchSDTU.TabIndex = 34;
            // 
            // _switchVnesh
            // 
            this._switchVnesh.FormattingEnabled = true;
            this._switchVnesh.Location = new System.Drawing.Point(89, 140);
            this._switchVnesh.Name = "_switchVnesh";
            this._switchVnesh.Size = new System.Drawing.Size(105, 21);
            this._switchVnesh.TabIndex = 33;
            // 
            // _switchKey
            // 
            this._switchKey.FormattingEnabled = true;
            this._switchKey.Location = new System.Drawing.Point(89, 121);
            this._switchKey.Name = "_switchKey";
            this._switchKey.Size = new System.Drawing.Size(105, 21);
            this._switchKey.TabIndex = 32;
            // 
            // _switchButtons
            // 
            this._switchButtons.FormattingEnabled = true;
            this._switchButtons.Location = new System.Drawing.Point(89, 101);
            this._switchButtons.Name = "_switchButtons";
            this._switchButtons.Size = new System.Drawing.Size(105, 21);
            this._switchButtons.TabIndex = 31;
            // 
            // _switchVneshOff
            // 
            this._switchVneshOff.FormattingEnabled = true;
            this._switchVneshOff.Location = new System.Drawing.Point(89, 81);
            this._switchVneshOff.Name = "_switchVneshOff";
            this._switchVneshOff.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOff.TabIndex = 30;
            // 
            // _switchVneshOn
            // 
            this._switchVneshOn.FormattingEnabled = true;
            this._switchVneshOn.Location = new System.Drawing.Point(89, 61);
            this._switchVneshOn.Name = "_switchVneshOn";
            this._switchVneshOn.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOn.TabIndex = 29;
            // 
            // _switchKeyOff
            // 
            this._switchKeyOff.FormattingEnabled = true;
            this._switchKeyOff.Location = new System.Drawing.Point(89, 41);
            this._switchKeyOff.Name = "_switchKeyOff";
            this._switchKeyOff.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOff.TabIndex = 28;
            // 
            // _switchKeyOn
            // 
            this._switchKeyOn.FormattingEnabled = true;
            this._switchKeyOn.Location = new System.Drawing.Point(89, 21);
            this._switchKeyOn.Name = "_switchKeyOn";
            this._switchKeyOn.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOn.TabIndex = 27;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(6, 183);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(57, 13);
            this.label100.TabIndex = 26;
            this.label100.Text = "Привязка";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 163);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(38, 13);
            this.label101.TabIndex = 25;
            this.label101.Text = "СДТУ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 143);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "Внешнее";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 124);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(33, 13);
            this.label103.TabIndex = 23;
            this.label103.Text = "Ключ";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 104);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(36, 13);
            this.label104.TabIndex = 22;
            this.label104.Text = "Меню";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 84);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(66, 13);
            this.label105.TabIndex = 21;
            this.label105.Text = "Внеш. откл.";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 64);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(61, 13);
            this.label106.TabIndex = 20;
            this.label106.Text = "Внеш. вкл.";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 44);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(62, 13);
            this.label107.TabIndex = 19;
            this.label107.Text = "Ключ откл.";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 24);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(57, 13);
            this.label108.TabIndex = 18;
            this.label108.Text = "Ключ вкл.";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(223, 136);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.clearSetpointsItem.Text = "Загрузить базовые уставки";
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР801";
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 636);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConfigurationForm_KeyUp);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage19.ResumeLayout(false);
            this.tabPage22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesIstarDataGrid)).EndInit();
            this.tabPage21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._defenseI2I1DataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._defenseI7DataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._defensesI56DataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._defensesI14DataGrid)).EndInit();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._allDefensesPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this._difensesTC.ResumeLayout(false);
            this.tabPage17.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage18.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._defPdgv)).EndInit();
            this.tabPage23.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._defensesUMDataGrid)).EndInit();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._defensesUBDataGrid)).EndInit();
            this.tabPage26.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).EndInit();
            this.groupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).EndInit();
            this.tabPage28.ResumeLayout(false);
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defenseQgrid)).EndInit();
            this.tabPage25.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).EndInit();
            this.VLS1.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.VLS2.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this._oscPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsDgv)).EndInit();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.VLS10.ResumeLayout(false);
            this._inputSygnalsPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).EndInit();
            this._measureTransPage.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._configurationTabControl.ResumeLayout(false);
            this._powTransPage.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this._automatic.ResumeLayout(false);
            this._automatic.PerformLayout();
            this.switchGroup.ResumeLayout(false);
            this.switchGroup.PerformLayout();
            this.tabControl4.ResumeLayout(false);
            this.tabPage30.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.tabPage31.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.tabPage32.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.controlGroup.ResumeLayout(false);
            this.controlGroup.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTOBTTextBox;
        private System.Windows.Forms.MaskedTextBox _constraintDTOBTTextBox;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox _blockingDTOBTComboBox;
        private System.Windows.Forms.ComboBox _modeDTOBTComboBox;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox _K2TangensTextBox;
        private System.Windows.Forms.MaskedTextBox _Ib2BeginTextBox;
        private System.Windows.Forms.ComboBox _modeDTZComboBox;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox12;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.TabPage _allDefensesPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton _mainRadioButton;
        private System.Windows.Forms.RadioButton _reserveRadioButton;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TabControl _difensesTC;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox _I5I1TextBox;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ComboBox _oscDTZComboBox;
        private System.Windows.Forms.ComboBox _blockingDTZComboBox;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTZTextBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _I2I1TextBox;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.MaskedTextBox _constraintDTZTextBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.MaskedTextBox _K1AngleOfSlopeTextBox;
        private System.Windows.Forms.MaskedTextBox _Ib1BeginTextBox;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TabPage tabPage26;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.DataGridView _difensesFBDataGrid;
        private System.Windows.Forms.TabPage tabPage25;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.DataGridView _externalDifensesDataGrid;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox11;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox13;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox14;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox15;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox16;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox9;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        private System.Windows.Forms.TabPage _oscPage;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox10;
        private System.Windows.Forms.TabPage _inputSygnalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _grUstComboBox;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView _inputSignals9;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView _inputSignals10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView _inputSignals11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView _inputSignals12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.DataGridView _inputSignals13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.DataGridView _inputSignals14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.DataGridView _inputSignals15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.DataGridView _inputSignals16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView _inputSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView _inputSignals2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView _inputSignals3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView _inputSignals4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView _inputSignals5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView _inputSignals6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView _inputSignals7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.DataGridView _inputSignals8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.TabPage _measureTransPage;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.TabPage _powTransPage;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label _fault4Label;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.Label _fault3Label;
        private System.Windows.Forms.Label _fault2Label;
        private System.Windows.Forms.Label _fault1Label;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.MaskedTextBox _oscopePercent;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.DataGridView _oscChannelsDgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column2;
        private System.Windows.Forms.ComboBox _oscopeCountComboBox;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label _oscopeLenghtLabel;
        private System.Windows.Forms.TextBox _oscCount;
        private System.Windows.Forms.ComboBox _oscopeFixationComboBox;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.ComboBox _TH_XKoeff_ComboBox;
        private System.Windows.Forms.ComboBox _faultTnx;
        private System.Windows.Forms.ComboBox _TH_LKoeff_ComboBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.ComboBox _faultTnl;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _TH_L_TextBox;
        private System.Windows.Forms.MaskedTextBox _TH_X_TextBox;
        private System.Windows.Forms.ComboBox _typeTn;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.ComboBox _i0CorrectionL2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.MaskedTextBox _TT_X2_TextBox;
        private System.Windows.Forms.MaskedTextBox _TT_L2_TextBox;
        private System.Windows.Forms.ComboBox _polarityX2;
        private System.Windows.Forms.ComboBox _polarityL2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _i0CorrectionL1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.MaskedTextBox _TT_X1_TextBox;
        private System.Windows.Forms.MaskedTextBox _TT_L1_TextBox;
        private System.Windows.Forms.ComboBox _polarityX1;
        private System.Windows.Forms.ComboBox _polarityL1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox _cI2L1;
        private System.Windows.Forms.MaskedTextBox _cI0L1;
        private System.Windows.Forms.MaskedTextBox _cInL1;
        private System.Windows.Forms.MaskedTextBox _cIL1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _cIL2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox _cInL2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox _cI0L2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox _cI2L2;
        private System.Windows.Forms.CheckBox _crossBlockI5I1;
        private System.Windows.Forms.CheckBox _modeI5I1;
        private System.Windows.Forms.CheckBox _crossBlockI2I1;
        private System.Windows.Forms.CheckBox _modeI2I1;
        private System.Windows.Forms.TabPage tabPage28;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.MaskedTextBox _blockQust;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox _blockQmode;
        private System.Windows.Forms.DataGridView _defenseQgrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _engineNreset;
        private System.Windows.Forms.ComboBox _engineQresetGr1;
        private System.Windows.Forms.MaskedTextBox _qBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _tCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox _tStartBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _iStartBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _engineIdv;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.MaskedTextBox _engineCoolingTimeGr1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox _engineHeatingTimeGr1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.MaskedTextBox _blockNpusk;
        private System.Windows.Forms.MaskedTextBox _blockNt;
        private System.Windows.Forms.MaskedTextBox _blockNhot;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.MaskedTextBox _blockQt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox _diffAVR;
        private System.Windows.Forms.CheckBox _DiffAPV;
        private System.Windows.Forms.CheckBox _diffUROV;
        private System.Windows.Forms.ComboBox _diffCutoutOsc;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.CheckBox _diffCutoutUROV;
        private System.Windows.Forms.CheckBox _diffCutoutAVR;
        private System.Windows.Forms.CheckBox _diffCutoutAPV;
        private System.Windows.Forms.CheckBox _diffCutoutMgn;
        private System.Windows.Forms.DataGridView _defenseI2I1DataGrid;
        private System.Windows.Forms.DataGridView _defenseI7DataGrid;
        private System.Windows.Forms.DataGridView _defensesI56DataGrid;
        private System.Windows.Forms.DataGridView _defensesI14DataGrid;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.DataGridView _defensesUMDataGrid;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView _defensesUBDataGrid;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView _difensesFMDataGrid;
        private System.Windows.Forms.Button _copySetpointsBtn;
        private System.Windows.Forms.DataGridView _difensesIstarDataGrid;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.DataGridView _defPdgv;
        private System.Windows.Forms.ComboBox _typeTT1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.MaskedTextBox _passportKpd;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.MaskedTextBox _passportCos;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.MaskedTextBox _passportP;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.ComboBox _passportWatt;
        private System.Windows.Forms.TabPage _automatic;
        private System.Windows.Forms.GroupBox switchGroup;
        private System.Windows.Forms.ComboBox _switchKontCep;
        private System.Windows.Forms.MaskedTextBox _switchTUskor;
        private System.Windows.Forms.MaskedTextBox _switchImp;
        private System.Windows.Forms.MaskedTextBox _switchIUrov;
        private System.Windows.Forms.MaskedTextBox _switchTUrov;
        private System.Windows.Forms.ComboBox _switchBlock;
        private System.Windows.Forms.ComboBox _switchError;
        private System.Windows.Forms.ComboBox _switchOn;
        private System.Windows.Forms.ComboBox _switchOff;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage30;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.CheckBox _autoSwitch;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.MaskedTextBox _apv2Krat;
        private System.Windows.Forms.MaskedTextBox _apv1Krat;
        private System.Windows.Forms.MaskedTextBox _apvTReady;
        private System.Windows.Forms.MaskedTextBox _apvTBlock;
        private System.Windows.Forms.ComboBox _apvBlocking;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.ComboBox _apvModes;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.CheckBox _avrDef;
        private System.Windows.Forms.CheckBox _avrSelfOff;
        private System.Windows.Forms.CheckBox _avrSwitchOff;
        private System.Windows.Forms.CheckBox _avrSignal;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.ComboBox _avrClear;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.MaskedTextBox _avrTOff;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.MaskedTextBox _avrTBack;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.ComboBox _avrBack;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.MaskedTextBox _avrTSr;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.ComboBox _avrResolve;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.ComboBox _avrBlockClear;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.ComboBox _avrBlocking;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.ComboBox _avrSIGNOn;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.MaskedTextBox _lzhVal;
        private System.Windows.Forms.ComboBox _lzhModes;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox controlGroup;
        private System.Windows.Forms.ComboBox _switchBinding;
        private System.Windows.Forms.ComboBox _switchSDTU;
        private System.Windows.Forms.ComboBox _switchVnesh;
        private System.Windows.Forms.ComboBox _switchKey;
        private System.Windows.Forms.ComboBox _switchButtons;
        private System.Windows.Forms.ComboBox _switchVneshOff;
        private System.Windows.Forms.ComboBox _switchVneshOn;
        private System.Windows.Forms.ComboBox _switchKeyOff;
        private System.Windows.Forms.ComboBox _switchKeyOn;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.CheckBox _switch;
        private System.Windows.Forms.ToolTip ButtontoolTip;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column12;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn24;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn25;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn26;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndColorCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i0StageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0ModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i0IColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0InColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i0UsYNColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i0UstartColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0DirColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0UndirColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0I0Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0CharColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i0TColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i0kColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0BlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _i0OscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i0TyBoolColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i0TyColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i0UROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i0APVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _i0AVRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn37;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn44;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn40;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn41;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn16;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBTvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBUvzYNColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBBlockingUMColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBSbrosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn45;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn46;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn50;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn47;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn48;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn49;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fBModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBTvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBUvzYNColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBUvzColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fBBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fBOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBSbrosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifSbrosColumn;
        private System.Windows.Forms.MaskedTextBox _sn;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.ComboBox _controlTtNeisp;
        private System.Windows.Forms.MaskedTextBox _controlTtTsr;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox _controlTtIdmin;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.ComboBox _inpResTT;
        private System.Windows.Forms.CheckBox _fault5CheckBox;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox _inpOsc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn30;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn32;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn33;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn35;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn36;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn22;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn28;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iIColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iInColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iUstartYNColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iUStartColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iDirectColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iUnDirectColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iLogicColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column14;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iCharColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iTColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iKColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iOscModeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iTyBoolColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iTyColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iUROVModeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iAPVModeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _iAVRModeColumn;
        private System.Windows.Forms.ComboBox _sdtuBlock;
        private System.Windows.Forms.Label label69;
    }
}