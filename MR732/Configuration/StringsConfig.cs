﻿using System.Collections.Generic;
using BEMN.MR801DVG.Configuration.Structures.InputLogicSignals;
using BEMN.MR801DVG.Configuration.Structures.Oscope;
using BEMN.MR801DVG.Configuration.Structures.RelayInd;

namespace BEMN.MR801DVG.Configuration
{
    public static class StringsConfig
    {
        public static List<string> ControlTtReset => new List<string>
        {
            "Ручной","Автомат."
        };
        public static List<string> ControlTtNeipr =>new List<string>
        {
            "Выведен",
            "Неисправн.",
            "Блок+неисп"
        };

        public static List<string> Pwatt => new List<string>
        {
            "кВт","МВт"
        };

        public static List<string> TransformatorType => new List<string>
        {
            "Ia, Ib, Ic",
            "Ia, Ic"
        };

        public static string[] OscTimes => new[]
        {
            "62317",
            "41545",
            "31158",
            "24927",
            "20772",
            "17805",
            "15579",
            "13848",
            "12463",
            "11330",
            "10386",
            "9587",
            "8902",
            "8309",
            "7789",
            "7331",
            "6924",
            "6559",
            "6231",
            "5935",
            "5665",
            "5418",
            "5193",
            "4985",
            "4793",
            "4616",
            "4451",
            "4297",
            "4154",
            "4020",
            "3894",
            "3776",
            "3665",
            "3561",
            "3462",
            "3368",
            "3279",
            "3195",
            "3115",
            "3039"
        };

        /// <summary>
        /// Названия индикаторов
        /// </summary>
        public static List<string> IndNames
        {
            get
            {
                List<string> indNames = new List<string>();
                for (int i = 0; i < AllIndicatorsStruct.INDICATORS_COUNT; i++)
                {
                    indNames.Add((i + 1).ToString());
                }
                return indNames;
            }
        }

        /// <summary>
        /// Названия реле
        /// </summary>
        public static List<string> RelayNames
        {
            get
            {
                List<string> relayNames = new List<string>();
                for (int i = 1; i <= AllReleOutputStruct.RELAY_COUNT; i++)
                {
                    relayNames.Add(i.ToString());
                }
                return relayNames;
            }
        }
        /// <summary>
        /// Тип реле и индикаторов
        /// </summary>
        public static List<string> ReleyType => new List<string>
        {
            "Повторитель",
            "Блинкер"
        };

        public static List<string> ExternalStages => new List<string>
        {
            "ВЗ 1",
            "ВЗ 2",
            "ВЗ 3",
            "ВЗ 4",
            "ВЗ 5",
            "ВЗ 6",
            "ВЗ 7",
            "ВЗ 8",
            "ВЗ 9",
            "ВЗ 10",
            "ВЗ 11",
            "ВЗ 12",
            "ВЗ 13",
            "ВЗ 14",
            "ВЗ 15",
            "ВЗ 16"
        };

        public static List<string> DefQNames => new List<string>
        {
            "Q>","Q>>"
        }; 

        public static List<string> DefFNames => new List<string>
        {
            "F>1","F>2","F>3","F>4","F<1","F<2","F<3","F<4"
        };

        public static List<string> DefUNames => new List<string>
        {
            "U>1","U>2","U>3","U>4","U<1","U<2","U<3","U<4"
        }; 

        public static List<string> DefINames => new List<string>
        {
            "I>1","I>2","I>3","I>4","I>5","I>6","I<"
        };

        public static List<string> DefIStarNames => new List<string>
        {
            "I*>1","I*>2","I*>3","I*>4","I*>5","I*>6"
        };

        public static List<string> DefPNames => new List<string>
        {
            "P1","P2"
        };

        /// <summary>
        /// тип для защит Umin
        /// </summary>
        public static List<string> UminDefenseMode
        {
            get
            {
                return new List<string>
                {
                    "Одна фаза",
                    "Все фазы",
                    "Одно линейное",
                    "Все линейные",
                    "Un"
                };
            }
        }
        /// <summary>
        /// тип для защит Umax
        /// </summary>
        public static List<string> UmaxDefenseMode
        {
            get
            {
                return new List<string>
                {
                    "Одна фаза",
                    "Все фазы",
                    "Одно линейное",
                    "Все линейные",
                    "Un",
                    "U0",
                    "U2"
                };
            }
        }
        /// <summary>
        /// I*
        /// </summary>
        public static List<string> Istar
        {
            get
            {
                return new List<string>
                {
                    "In",
                    "I0",
                    "I2"
                };
            }
        }
        /// <summary>
        /// Режимы для I>5, I>6
        /// </summary>
        public static List<string> I56Modes => new List<string>
        {
            "Реж1 (Работа)",
            "Реж2 (Пуск)"
        };

        /// <summary>
        /// Характеристика
        /// </summary>
        public static List<string> Characteristic => new List<string>
        {
            "Независимая",
            "Зависимая"
        };

        /// <summary>
        /// Логика
        /// </summary>
        public static List<string> Logic => new List<string>
        {
            "Одна фаза",
            "Все фазы"
        };

        /// <summary>
        /// Логика
        /// </summary>
        public static List<string> LogicI7 => new List<string>
        {
            "Одна фаза",
            "Две фазы",
            "Три фазы"
        };

        /// <summary>
        /// Направление
        /// </summary>
        public static List<string> Direction => new List<string>
        {
            "Нет",
            "От шин",
            "К шинам"
        };

        /// <summary>
        /// Недост. напр(I*)
        /// </summary>
        public static List<string> Undir => new List<string>
        {
            "Ненапр.",
            "Блок."
        };
        
        public static List<string> VlsSignals => new List<string>
        {
            "Д1 ",
            "Д2 ",
            "Д3 ",
            "Д4 ",
            "Д5 ",
            "Д6 ",
            "Д7 ",
            "Д8 ",
            "Д9 ",
            "Д10 ",
            "Д11 ",
            "Д12 ",
            "Д13",
            "Д14 ",
            "Д15 ",
            "Д16 ",
            "Д17 ",
            "Д18 ",
            "Д19 ",
            "Д20 ",
            "Д21 ",
            "Д22 ",
            "Д23 ",
            "Д24 ",
            "ЛС1 ",
            "ЛС2 ",
            "ЛС3 ",
            "ЛС4 ",
            "ЛС5 ",
            "ЛС6 ",
            "ЛС7 ",
            "ЛС8 ",
            "ЛС9 ",
            "ЛС10 ",
            "ЛС11 ",
            "ЛС12 ",
            "ЛС13",
            "ЛС14 ",
            "ЛС15 ",
            "ЛС16 ",
            "Iд>> мгн",
            "Iд>> ИО",
            "Iд>>",
            "Iд> ИО",
            "Iд>",
            "P>1 ИО",
            "P>1",
            "P>2 ИО",
            "P>2",
            "РЕЗЕРВ1",
            "РЕЗЕРВ2",
            "I> 1 ИО",
            "I> 1",
            "I> 2 ИО",
            "I> 2",
            "I> 3 ИО",
            "I> 3",
            "I> 4 ИО",
            "I> 4",
            "I> 5 ИО",
            "I> 5",
            "I> 6 ИО",
            "I> 6",
            "I< 7 ИО",
            "I< 7",
            "I2/I1 ИО",
            "I2/I1",
            "I*> 1 ИО",
            "I*> 1",
            "I*> 2 ИО",
            "I*> 2",
            "I*> 3 ИО",
            "I*> 3",
            "I*> 4 ИО",
            "I*> 4",
            "I*> 5 ИО",
            "I*> 5",
            "I*> 6 ИО",
            "I*> 6",
            "U> 1 ИО",
            "U> 1",
            "U> 2 ИО",
            "U> 2",
            "U> 3 ИО",
            "U> 3",
            "U> 4 ИО",
            "U> 4 ",
            "U< 1 ИО",
            "U< 1",
            "U< 2 ИО",
            "U< 2",
            "U< 3 ИО",
            "U< 3",
            "U< 4 ИО",
            "U< 4",
            "F> 1 ИО",
            "F> 1",
            "F> 2 ИО",
            "F> 2",
            "F> 3 ИО",
            "F> 3",
            "F> 4 ИО",
            "F> 4 ",
            "F< 1 ИО",
            "F< 1",
            "F< 2 ИО",
            "F< 2",
            "F< 3 ИО",
            "F< 3",
            "F< 4 ИО",
            "F< 4",
            "Q>",
            "Q>>",
            "Блк. по Q",
            "Блк. по N",
            "Пуск",
            "ВНЕШ. 1",
            "ВНЕШ. 2",
            "ВНЕШ. 3",
            "ВНЕШ. 4",
            "ВНЕШ. 5",
            "ВНЕШ. 6",
            "ВНЕШ. 7",
            "ВНЕШ. 8",
            "ВНЕШ. 9",
            "ВНЕШ. 10",
            "ВНЕШ. 11",
            "ВНЕШ. 12",
            "ВНЕШ. 13",
            "ВНЕШ. 14",
            "ВНЕШ. 15",
            "ВНЕШ. 16",
            "ССЛ1",
            "ССЛ2",
            "ССЛ3",
            "ССЛ4",
            "ССЛ5",
            "ССЛ6",
            "ССЛ7",
            "ССЛ8",
            "ССЛ9",
            "ССЛ10",
            "ССЛ11",
            "ССЛ12",
            "ССЛ13",
            "ССЛ14",
            "ССЛ15",
            "ССЛ16",
            "ССЛ17",
            "ССЛ18",
            "ССЛ19",
            "ССЛ20",
            "ССЛ21",
            "ССЛ22",
            "ССЛ23",
            "ССЛ24",
            "ССЛ25",
            "ССЛ26",
            "ССЛ27",
            "ССЛ28",
            "ССЛ29",
            "ССЛ30",
            "ССЛ31",
            "ССЛ32",
            "Неиспр.",
            "Гр. Осн.",
            "Гр. Рез",
            "Неиспр. ТТ",
            "Авар. откл.",
            "Откл. выкл.",
            "Вкл. выкл.",
            "АВР Вкл.",
            "АВР Откл.",
            "АВР Блок.",
            "Работа ЛЗШ",
            "Работа УРОВ",
            "Вкл. по АПВ",
            "Ускорение",
            "Сигнализация"
        };

        /// <summary>
        /// значения дискрет ЛС
        /// </summary>
        public static List<string> LsState => new List<string>
        {
            "Нет",
            "Да",
            "Инверс"
        };

        /// <summary>
        /// Сигналы ЛС
        /// </summary>
        public static List<string> LsSignals
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= InputLogicSignalsStruct.DISCRETS_COUNT; i++)
                {
                    ret.Add("Д" + i);
                }
                return ret;
            }
        }

        public static List<string> Polarity => new List<string> {"Положительная", "Отрицательная"};

        public static List<string> KthKoefs => new List<string> {"1", "1000"};

        public static List<string> OscMode
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= 40; i++)
                {
                    ret.Add(i.ToString());
                }
                return ret;
            }
        }

        public static List<string> OscChannelsNames
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= OscopeAllChannelsStruct.CHANNEL_COUNT; i++)
                {
                    ret.Add($"Канал {i}");
                }
                return ret;
            }
        } 

        /// <summary>
        /// Фиксация осц.
        /// </summary>
        public static List<string> OscFixation => new List<string>
        {
            "Первой",
            "Посл."
        };

        public static List<string> LzshModes => new List<string>
        {
            "Выведено",
            "Схема 1",
            "Схема 2"
        };

        public static List<string> Binding => new List<string>
        {
            "S1",
            "S2"
        };

        /// <summary>
        /// Тип ТН (U0)
        /// </summary>
        public static List<string> UoType => new List<string>
        {
            "Un",
            "U0"
        };

        public static List<string> ExternalDefSignals => new List<string>
        {
            "Нет",
            "Д1 ",
            "Д1 Инв.",
            "Д2 ",
            "Д2 Инв.",
            "Д3 ",
            "Д3 Инв.",
            "Д4 ",
            "Д4 Инв.",
            "Д5 ",
            "Д5 Инв.",
            "Д6 ",
            "Д6 Инв.",
            "Д7 ",
            "Д7 Инв.",
            "Д8 ",
            "Д8 Инв.",
            "Д9 ",
            "Д9 Инв.",
            "Д10 ",
            "Д10 Инв.",
            "Д11 ",
            "Д11 Инв.",
            "Д12 ",
            "Д12 Инв.",
            "Д13 ",
            "Д13 Инв.",
            "Д14 ",
            "Д14 Инв.",
            "Д15 ",
            "Д15 Инв.",
            "Д16 ",
            "Д16 Инв.",
            "Д17 ",
            "Д17 Инв.",
            "Д18 ",
            "Д18 Инв.",
            "Д19 ",
            "Д19 Инв.",
            "Д20 ",
            "Д20 Инв.",
            "Д21 ",
            "Д21 Инв.",
            "Д22 ",
            "Д22 Инв.",
            "Д23 ",
            "Д23 Инв.",
            "Д24 ",
            "Д24 Инв.",
            "ЛС1 ",
            "ЛС1 Инв.",
            "ЛС2 ",
            "ЛС2 Инв.",
            "ЛС3 ",
            "ЛС3 Инв.",
            "ЛС4 ",
            "ЛС4 Инв.",
            "ЛС5 ",
            "ЛС5 Инв.",
            "ЛС6 ",
            "ЛС6 Инв.",
            "ЛС7 ",
            "ЛС7 Инв.",
            "ЛС8 ",
            "ЛС8 Инв.",
            "ЛС9 ",
            "ЛС9 Инв.",
            "ЛС10 ",
            "ЛС10 Инв.",
            "ЛС11 ",
            "ЛС11 Инв.",
            "ЛС12 ",
            "ЛС12 Инв.",
            "ЛС13",
            "ЛС13 Инв.",
            "ЛС14 ",
            "ЛС14 Инв.",
            "ЛС15 ",
            "ЛС15 Инв.",
            "ЛС16 ",
            "ЛС16 Инв.",
            "ВЛС1 ",
            "ВЛС1 Инв.",
            "ВЛС2 ",
            "ВЛС2 Инв.",
            "ВЛС3 ",
            "ВЛС3 Инв.",
            "ВЛС4 ",
            "ВЛС4 Инв.",
            "ВЛС5 ",
            "ВЛС5 Инв.",
            "ВЛС6 ",
            "ВЛС6 Инв.",
            "ВЛС7 ",
            "ВЛС7 Инв.",
            "ВЛС8 ",
            "ВЛС8 Инв.",
            "ВЛС9 ",
            "ВЛС9 Инв.",
            "ВЛС10 ",
            "ВЛС10 Инв.",
            "ВЛС11 ",
            "ВЛС11 Инв.",
            "ВЛС12 ",
            "ВЛС12 Инв.",
            "ВЛС13",
            "ВЛС13 Инв.",
            "ВЛС14 ",
            "ВЛС14 Инв.",
            "ВЛС15 ",
            "ВЛС15 Инв.",
            "ВЛС16 ",
            "ВЛС16 Инв.",
            "Iд>> мгн",
            "Iд>> мгн Инв.",
            "Iд>> ИО",
            "Iд>> ИО Инв.",
            "Iд>>",
            "Iд>> Инв.",
            "Iд> ИО",
            "Iд> ИО Инв.",
            "Iд>",
            "Iд> Инв.",
            "P>1 ИО",
            "P>1 ИО Инв.",
            "P>1",
            "P>1 Инв.",
            "P>2 ИО",
            "P>2 ИО Инв.",
            "P>2",
            "P>2 Инв.",
            "РЕЗЕРВ1",
            "РЕЗЕРВ1 Инв.",
            "РЕЗЕРВ2",
            "РЕЗЕРВ2 Инв.",
            "I> 1 ИО",
            "I> 1 ИО Инв.",
            "I> 1",
            "I> 1 Инв.",
            "I> 2 ИО",
            "I> 2 ИО Инв.",
            "I> 2",
            "I> 2 Инв.",
            "I> 3 ИО",
            "I> 3 ИО Инв.",
            "I> 3",
            "I> 3 Инв.",
            "I> 4 ИО",
            "I> 4 ИО Инв.",
            "I> 4",
            "I> 4 Инв.",
            "I> 5 ИО",
            "I> 5 ИО Инв.",
            "I> 5",
            "I> 5 Инв.",
            "I> 6 ИО",
            "I> 6 ИО Инв.",
            "I> 6",
            "I> 6 Инв.",
            "I< 7 ИО",
            "I< 7 ИО Инв.",
            "I< 7",
            "I< 7 Инв.",
            "I2/I1 ИО",
            "I2/I1 ИО Инв.",
            "I2/I1",
            "I2/I1 Инв.",
            "I*> 1 ИО",
            "I*> 1 ИО Инв.",
            "I*> 1",
            "I*> 1 Инв.",
            "I*> 2 ИО",
            "I*> 2 ИО Инв.",
            "I*> 2",
            "I*> 2 Инв.",
            "I*> 3 ИО",
            "I*> 3 ИО Инв.",
            "I*> 3",
            "I*> 3 Инв.",
            "I*> 4 ИО",
            "I*> 4 ИО Инв.",
            "I*> 4",
            "I*> 4 Инв.",
            "I*> 5 ИО",
            "I*> 5 ИО Инв.",
            "I*> 5",
            "I*> 5 Инв.",
            "I*> 6 ИО",
            "I*> 6 ИО Инв.",
            "I*> 6",
            "I*> 6 Инв.",
            "U> 1 ИО",
            "U> 1 ИО Инв.",
            "U> 1",
            "U> 1 Инв.",
            "U> 2 ИО",
            "U> 2 ИО Инв.",
            "U> 2",
            "U> 2 Инв.",
            "U> 3 ИО",
            "U> 3 ИО Инв.",
            "U> 3",
            "U> 3 Инв.",
            "U> 4 ИО",
            "U> 4 ИО Инв.",
            "U> 4 ",
            "U> 4 Инв.",
            "U< 1 ИО",
            "U< 1 ИО Инв.",
            "U< 1",
            "U< 1 Инв.",
            "U< 2 ИО",
            "U< 2 ИО Инв.",
            "U< 2",
            "U< 2 Инв.",
            "U< 3 ИО",
            "U< 3 ИО Инв.",
            "U< 3",
            "U< 3 Инв.",
            "U< 4 ИО",
            "U< 4 ИО Инв.",
            "U< 4",
            "U< 4 Инв.",
            "F> 1 ИО",
            "F> 1 ИО Инв.",
            "F> 1",
            "F> 1 Инв.",
            "F> 2 ИО",
            "F> 2 ИО Инв.",
            "F> 2",
            "F> 2 Инв.",
            "F> 3 ИО",
            "F> 3 ИО Инв.",
            "F> 3",
            "F> 3 Инв.",
            "F> 4 ИО",
            "F> 4 ИО Инв.",
            "F> 4 ",
            "F> 4 Инв.",
            "F< 1 ИО",
            "F< 1 ИО Инв.",
            "F< 1",
            "F< 1 Инв.",
            "F< 2 ИО",
            "F< 2 ИО Инв.",
            "F< 2",
            "F< 2 Инв.",
            "F< 3 ИО",
            "F< 3 ИО Инв.",
            "F< 3",
            "F< 3 Инв.",
            "F< 4 ИО",
            "F< 4 ИО Инв.",
            "F< 4",
            "F< 4 Инв.",
            "Q>",
            "Q> Инв.",
            "Q>>",
            "Q>> Инв.",
            "Блк. по Q",
            "Блк. по Q Инв.",
            "Блк. по N",
            "Блк. по N Инв.",
            "ПУСК",
            "РАБОТА"
        }; 

        public static List<string> RelaySignals => new List<string>
        {
            "Нет",
            "Д1 ",
            "Д1 Инв.",
            "Д2 ",
            "Д2 Инв.",
            "Д3 ",
            "Д3 Инв.",
            "Д4 ",
            "Д4 Инв.",
            "Д5 ",
            "Д5 Инв.",
            "Д6 ",
            "Д6 Инв.",
            "Д7 ",
            "Д7 Инв.",
            "Д8 ",
            "Д8 Инв.",
            "Д9 ",
            "Д9 Инв.",
            "Д10 ",
            "Д10 Инв.",
            "Д11 ",
            "Д11 Инв.",
            "Д12 ",
            "Д12 Инв.",
            "Д13 ",
            "Д13 Инв.",
            "Д14 ",
            "Д14 Инв.",
            "Д15 ",
            "Д15 Инв.",
            "Д16 ",
            "Д16 Инв.",
            "Д17 ",
            "Д17 Инв.",
            "Д18 ",
            "Д18 Инв.",
            "Д19 ",
            "Д19 Инв.",
            "Д20 ",
            "Д20 Инв.",
            "Д21 ",
            "Д21 Инв.",
            "Д22 ",
            "Д22 Инв.",
            "Д23 ",
            "Д23 Инв.",
            "Д24 ",
            "Д24 Инв.",
            "ЛС1 ",
            "ЛС1 Инв.",
            "ЛС2 ",
            "ЛС2 Инв.",
            "ЛС3 ",
            "ЛС3 Инв.",
            "ЛС4 ",
            "ЛС4 Инв.",
            "ЛС5 ",
            "ЛС5 Инв.",
            "ЛС6 ",
            "ЛС6 Инв.",
            "ЛС7 ",
            "ЛС7 Инв.",
            "ЛС8 ",
            "ЛС8 Инв.",
            "ЛС9 ",
            "ЛС9 Инв.",
            "ЛС10 ",
            "ЛС10 Инв.",
            "ЛС11 ",
            "ЛС11 Инв.",
            "ЛС12 ",
            "ЛС12 Инв.",
            "ЛС13",
            "ЛС13 Инв.",
            "ЛС14 ",
            "ЛС14 Инв.",
            "ЛС15 ",
            "ЛС15 Инв.",
            "ЛС16 ",
            "ЛС16 Инв.",
            "ВЛС1 ",
            "ВЛС1 Инв.",
            "ВЛС2 ",
            "ВЛС2 Инв.",
            "ВЛС3 ",
            "ВЛС3 Инв.",
            "ВЛС4 ",
            "ВЛС4 Инв.",
            "ВЛС5 ",
            "ВЛС5 Инв.",
            "ВЛС6 ",
            "ВЛС6 Инв.",
            "ВЛС7 ",
            "ВЛС7 Инв.",
            "ВЛС8 ",
            "ВЛС8 Инв.",
            "ВЛС9 ",
            "ВЛС9 Инв.",
            "ВЛС10 ",
            "ВЛС10 Инв.",
            "ВЛС11 ",
            "ВЛС11 Инв.",
            "ВЛС12 ",
            "ВЛС12 Инв.",
            "ВЛС13",
            "ВЛС13 Инв.",
            "ВЛС14 ",
            "ВЛС14 Инв.",
            "ВЛС15 ",
            "ВЛС15 Инв.",
            "ВЛС16 ",
            "ВЛС16 Инв.",
            "Iд>> мгн",
            "Iд>> мгн Инв.",
            "Iд>> ИО",
            "Iд>> ИО Инв.",
            "Iд>>",
            "Iд>> Инв.",
            "Iд> ИО",
            "Iд> ИО Инв.",
            "Iд>",
            "Iд> Инв.",
            "P>1 ИО",
            "P>1 ИО Инв.",
            "P>1",
            "P>1 Инв.",
            "P>2 ИО",
            "P>2 ИО Инв.",
            "P>2",
            "P>2 Инв.",
            "РЕЗЕРВ1",
            "РЕЗЕРВ1 Инв.",
            "РЕЗЕРВ2",
            "РЕЗЕРВ2 Инв.",
            "I> 1 ИО",
            "I> 1 ИО Инв.",
            "I> 1",
            "I> 1 Инв.",
            "I> 2 ИО",
            "I> 2 ИО Инв.",
            "I> 2",
            "I> 2 Инв.",
            "I> 3 ИО",
            "I> 3 ИО Инв.",
            "I> 3",
            "I> 3 Инв.",
            "I> 4 ИО",
            "I> 4 ИО Инв.",
            "I> 4",
            "I> 4 Инв.",
            "I> 5 ИО",
            "I> 5 ИО Инв.",
            "I> 5",
            "I> 5 Инв.",
            "I> 6 ИО",
            "I> 6 ИО Инв.",
            "I> 6",
            "I> 6 Инв.",
            "I< 7 ИО",
            "I< 7 ИО Инв.",
            "I< 7",
            "I< 7 Инв.",
            "I2/I1 ИО",
            "I2/I1 ИО Инв.",
            "I2/I1",
            "I2/I1 Инв.",
            "I*> 1 ИО",
            "I*> 1 ИО Инв.",
            "I*> 1",
            "I*> 1 Инв.",
            "I*> 2 ИО",
            "I*> 2 ИО Инв.",
            "I*> 2",
            "I*> 2 Инв.",
            "I*> 3 ИО",
            "I*> 3 ИО Инв.",
            "I*> 3",
            "I*> 3 Инв.",
            "I*> 4 ИО",
            "I*> 4 ИО Инв.",
            "I*> 4",
            "I*> 4 Инв.",
            "I*> 5 ИО",
            "I*> 5 ИО Инв.",
            "I*> 5",
            "I*> 5 Инв.",
            "I*> 6 ИО",
            "I*> 6 ИО Инв.",
            "I*> 6",
            "I*> 6 Инв.",
            "U> 1 ИО",
            "U> 1 ИО Инв.",
            "U> 1",
            "U> 1 Инв.",
            "U> 2 ИО",
            "U> 2 ИО Инв.",
            "U> 2",
            "U> 2 Инв.",
            "U> 3 ИО",
            "U> 3 ИО Инв.",
            "U> 3",
            "U> 3 Инв.",
            "U> 4 ИО",
            "U> 4 ИО Инв.",
            "U> 4 ",
            "U> 4 Инв.",
            "U< 1 ИО",
            "U< 1 ИО Инв.",
            "U< 1",
            "U< 1 Инв.",
            "U< 2 ИО",
            "U< 2 ИО Инв.",
            "U< 2",
            "U< 2 Инв.",
            "U< 3 ИО",
            "U< 3 ИО Инв.",
            "U< 3",
            "U< 3 Инв.",
            "U< 4 ИО",
            "U< 4 ИО Инв.",
            "U< 4",
            "U< 4 Инв.",
            "F> 1 ИО",
            "F> 1 ИО Инв.",
            "F> 1",
            "F> 1 Инв.",
            "F> 2 ИО",
            "F> 2 ИО Инв.",
            "F> 2",
            "F> 2 Инв.",
            "F> 3 ИО",
            "F> 3 ИО Инв.",
            "F> 3",
            "F> 3 Инв.",
            "F> 4 ИО",
            "F> 4 ИО Инв.",
            "F> 4 ",
            "F> 4 Инв.",
            "F< 1 ИО",
            "F< 1 ИО Инв.",
            "F< 1",
            "F< 1 Инв.",
            "F< 2 ИО",
            "F< 2 ИО Инв.",
            "F< 2",
            "F< 2 Инв.",
            "F< 3 ИО",
            "F< 3 ИО Инв.",
            "F< 3",
            "F< 3 Инв.",
            "F< 4 ИО",
            "F< 4 ИО Инв.",
            "F< 4",
            "F< 4 Инв.",
            "Q>",
            "Q> Инв.",
            "Q>>",
            "Q>> Инв.",
            "Блк. по Q",
            "Блк. по Q Инв.",
            "Блк. по N",
            "Блк. по N Инв.",
            "ПУСК",
            "РАБОТА",
            "ВНЕШ. 1",
            "ВНЕШ. 1 Инв.",
            "ВНЕШ. 2",
            "ВНЕШ. 2 Инв.",
            "ВНЕШ. 3",
            "ВНЕШ. 3 Инв.",
            "ВНЕШ. 4",
            "ВНЕШ. 4 Инв.",
            "ВНЕШ. 5",
            "ВНЕШ. 5 Инв.",
            "ВНЕШ. 6",
            "ВНЕШ. 6 Инв.",
            "ВНЕШ. 7",
            "ВНЕШ. 7 Инв.",
            "ВНЕШ. 8",
            "ВНЕШ. 8 Инв.",
            "ВНЕШ. 9",
            "ВНЕШ. 9 Инв.",
            "ВНЕШ. 10",
            "ВНЕШ. 10 Инв.",
            "ВНЕШ. 11",
            "ВНЕШ. 11 Инв.",
            "ВНЕШ. 12",
            "ВНЕШ. 12 Инв.",
            "ВНЕШ. 13",
            "ВНЕШ. 13 Инв.",
            "ВНЕШ. 14",
            "ВНЕШ. 14 Инв.",
            "ВНЕШ. 15",
            "ВНЕШ. 15 Инв.",
            "ВНЕШ. 16",
            "ВНЕШ. 16 Инв.",
            "ССЛ1",
            "ССЛ1 Инв.", //210
            "ССЛ2",
            "ССЛ2 Инв.",
            "ССЛ3",
            "ССЛ3 Инв.",
            "ССЛ4",
            "ССЛ4 Инв.",
            "ССЛ5",
            "ССЛ5 Инв.",
            "ССЛ6",
            "ССЛ6 Инв.", //220
            "ССЛ7",
            "ССЛ7 Инв.",
            "ССЛ8",
            "ССЛ8 Инв.",
            "ССЛ9",
            "ССЛ9 Инв.",
            "ССЛ10",
            "ССЛ10 Инв.",
            "ССЛ11",
            "ССЛ11 Инв.", //230
            "ССЛ12",
            "ССЛ12 Инв.",
            "ССЛ13",
            "ССЛ13 Инв.",
            "ССЛ14",
            "ССЛ14 Инв.",
            "ССЛ15",
            "ССЛ15 Инв.",
            "ССЛ16",
            "ССЛ16 Инв.", //240
            "ССЛ17",
            "ССЛ17 Инв.",
            "ССЛ18",
            "ССЛ18 Инв.",
            "ССЛ19",
            "ССЛ19 Инв.",
            "ССЛ20",
            "ССЛ20 Инв.",
            "ССЛ21",
            "ССЛ21 Инв.", //250
            "ССЛ22",
            "ССЛ22 Инв.",
            "ССЛ23",
            "ССЛ23 Инв.",
            "ССЛ24",
            "ССЛ24 Инв.",
            "ССЛ25",
            "ССЛ25 Инв.",
            "ССЛ26",
            "ССЛ26 Инв.",
            "ССЛ27",
            "ССЛ27 Инв.", //120
            "ССЛ28",
            "ССЛ28 Инв.",
            "ССЛ29",
            "ССЛ29 Инв.",
            "ССЛ30",
            "ССЛ30 Инв.",
            "ССЛ31",
            "ССЛ31 Инв.",
            "ССЛ32",
            "ССЛ32 Инв.",
            "Неиспр.",
            "Неиспр. Инв.",
            "Гр. Осн.",
            "Гр. Осн. Инв.",
            "Гр. Рез",
            "Гр. Рез Инв.",
            "Неиспр. ТТ",
            "Неиспр. ТТ Инв.",
            "Авар. Откл.",
            "Авар. Откл. Инв.",
            "Откл. Выкл.",
            "Откл. Выкл. Инв.",
            "Вкл. Выкл.",
            "Вкл. Выкл. Инв.",
            "АВР Вкл.",
            "АВР Вкл. Инв.",
            "АВР Откл.",
            "АВР Откл. Инв.",
            "АВР Блок.",
            "АВР Блок. Инв.",
            "Работа ЛЗШ",
            "Работа ЛЗШ Инв.",
            "Работа УРОВ",
            "Работа УРОВ Инв.",
            "Вкл. по АПВ ",
            "Вкл. по АПВ Инв.",
            "Ускорение",
            "Ускорение Инв.",
            "Сигнализация",
            "Сигнализация Инв."
        };

        /// <summary>
        /// Выведено/Введено
        /// </summary>
        public static List<string> OffOn => new List<string>
        {
            "Выведено",
            "Введено"
        };

        /// <summary>
        /// Выведено/Введено
        /// </summary>
        public static List<string> DefenseMode => new List<string>
        {
            "Выведено",
            "Введено",
            "Сигнализация",
            "Отключение"
        };

        public static List<string> OscDefenceMode => new List<string>
        {
            "Выведено",
            "Пуск по ИО",
            "Пуск по защите"
        };

        /// <summary>
        /// разрешено Контроль
        /// </summary>
        public static List<string> ControlForbidden => new List<string>
        {
            "Контроль",
            "Разрешено"
        };


        public static List<string> ImpDlit => new List<string>
        {
            "Импульсная",
            "Длительная"
        };

        /// <summary>
        /// запрещено разрешено
        /// </summary>
        public static List<string> ForbiddenAllowed => new List<string>
        {
            "Запрещено",
            "Разрешено"
        };


        /// <summary>
        /// Режим АПВ
        /// </summary>
        public static List<string> ApvModes => new List<string>
        {
            "Нет",
            "1КРАТ",
            "2КРАТ"
        };

        public static List<string> DisableType => new List<string>
        {
            "tзапрета по фронту",
            "с tзапрета по возврату"
        };

        /// <summary>
        /// Нет/Есть
        /// </summary>
        public static List<string> BeNo => new List<string>
        {
            "Нет",
            "Есть"
        };

        /// <summary>
        /// Сигналы выключателя
        /// </summary>
        public static List<string> SwitchSignals => new List<string>
        {
            "НЕТ",
            "Д1 ",
            "Д1 Инв.",
            "Д2 ",
            "Д2 Инв.",
            "Д3 ",
            "Д3 Инв.",
            "Д4 ",
            "Д4 Инв.",
            "Д5 ",
            "Д5 Инв.",
            "Д6 ",
            "Д6 Инв.",
            "Д7 ",
            "Д7 Инв.",
            "Д8 ",
            "Д8 Инв.",
            "Д9 ",
            "Д9 Инв.",
            "Д10 ",
            "Д10 Инв.",
            "Д11 ",
            "Д11 Инв.",
            "Д12 ",
            "Д12 Инв.",
            "Д13 ",
            "Д13 Инв.",
            "Д14 ",
            "Д14 Инв.",
            "Д15 ",
            "Д15 Инв.",
            "Д16 ",
            "Д16 Инв.",
            "Д17 ",
            "Д17 Инв.",
            "Д18 ",
            "Д18 Инв.",
            "Д19 ",
            "Д19 Инв.",
            "Д20 ",
            "Д20 Инв.",
            "Д21 ",
            "Д21 Инв.",
            "Д22 ",
            "Д22 Инв.",
            "Д23 ",
            "Д23 Инв.",
            "Д24 ",
            "Д24 Инв.",
            "ЛС1",
            "ЛС1 Инв.",
            "ЛС2",
            "ЛС2 Инв.",
            "ЛС3",
            "ЛС3 Инв.",
            "ЛС4",
            "ЛС4 Инв.",
            "ЛС5",
            "ЛС5 Инв.",
            "ЛС6",
            "ЛС6 Инв.",
            "ЛС7",
            "ЛС7 Инв.",
            "ЛС8",
            "ЛС8 Инв.",
            "ЛС9",
            "ЛС9 Инв.",
            "ЛС10",
            "ЛС10 Инв.",
            "ЛС11",
            "ЛС11 Инв.",
            "ЛС12",
            "ЛС12 Инв.",
            "ЛС13",
            "ЛС13 Инв.",
            "ЛС14",
            "ЛС14 Инв.",
            "ЛС15",
            "ЛС15 Инв.",
            "ЛС16",
            "ЛС16 Инв.",
            "ВЛС1",
            "ВЛС1 Инв.",
            "ВЛС2",
            "ВЛС2 Инв.",
            "ВЛС3",
            "ВЛС3 Инв.",
            "ВЛС4",
            "ВЛС4 Инв.",
            "ВЛС5",
            "ВЛС5 Инв.",
            "ВЛС6",
            "ВЛС6 Инв.",
            "ВЛС7",
            "ВЛС7 Инв.",
            "ВЛС8",
            "ВЛС8 Инв.",
            "ВЛС9",
            "ВЛС9 Инв.",
            "ВЛС10",
            "ВЛС10 Инв.",
            "ВЛС11",
            "ВЛС11 Инв.",
            "ВЛС12",
            "ВЛС12 Инв.",
            "ВЛС13",
            "ВЛС13 Инв.",
            "ВЛС14",
            "ВЛС14 Инв.",
            "ВЛС15",
            "ВЛС15 Инв.",
            "ВЛС16",
            "ВЛС16 Инв."
        };
    }
}
