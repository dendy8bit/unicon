﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MR801DVG.AlarmJournal.Structures;
using BEMN.MR801DVG.Configuration.Structures.MeasureTransformer;
using BEMN.MR801DVG.Properties;

namespace BEMN.MR801DVG.AlarmJournal
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР801двг_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        #endregion [Constants]
        
        #region [Private fields]

        MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        MemoryEntity<MeasureTransStruct> _measure;
        MemoryEntity<OneWordStruct> _setPage;  

        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private MR801dvg _device;
        #endregion [Private fields]


        #region [Ctor's]
        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR801dvg device)
        {
            this.InitializeComponent();
            this._device = device;
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            
            this._measure = device.MeasureAJ;
            this._measure.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.CurrenOptionLoaded);
            this._measure.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._alarmJournal = device.AlarmJournal;
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecordMemoryOk);
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._failCounter < 80)
                {
                    this._failCounter++;
                }
                else
                {
                    this._alarmJournal.RemoveStructQueries();
                    this._failCounter = 0;
                    this.ButtonsEnabled = true;
                }
            });

            this._setPage = device.SetAjPage;
            this._setPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._alarmJournal.LoadStruct);
            this._setPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
        } 

        #endregion [Ctor's]


        #region [Help members]

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }
        
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }
        
        private void CurrenOptionLoaded()
        {
            this._recordNumber = this._failCounter = 0;
            this._table.Clear();
            this.SavePageNumber();
        }

        private void SavePageNumber()
        {
            this._setPage.Value.Word = (ushort)this._recordNumber;
            this._setPage.SaveStruct6();
        }

        private void ReadRecordMemoryOk()
        {
            if (!this._alarmJournal.Value.IsEmpty)
            {
                this.ReadRecord();
                this.SavePageNumber();
            }
            else
            {
                if (this._table.Rows.Count == 0)
                {
                    this._statusLabel.Text = JOURNAL_IS_EMPTY;
                }
                this.ButtonsEnabled = true;
            }
        }

        private void ReadRecord()
        {
            try
            {
                this._recordNumber++;
                this._failCounter = this._recordNumber;

                AlarmJournalRecordStruct record = this._alarmJournal.Value;
                MeasureTransStruct measure = this._measure.Value;
                string parameter = this.GetParameter(record);
                string parametrValue = this.GetParametrValue(record, measure);
                string triggeredDefense = record.TriggeredDefense == AjStrings.TriggeredDefense.Count - 1
                    ? AjStrings.TriggeredDefense[record.TriggeredDefense] + record.ValueOfTriggeredParametr1
                    : AjStrings.TriggeredDefense[record.TriggeredDefense];
                this._table.Rows.Add
                    (
                        this._recordNumber,
                        record.GetTime,
                        AjStrings.Message[record.Message],
                        triggeredDefense,
                        parameter,
                        parametrValue,
                        AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        ValuesConverterCommon.Analog.GetI(record.Ida, measure.ChannelL1.Ittl*40),
                        ValuesConverterCommon.Analog.GetI(record.Idb, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Idc, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ita, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Itb, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Itc, measure.ChannelL1.Ittl * 40),

                        ValuesConverterCommon.Analog.GetI(record.Is1a, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is1b, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is1c, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is1n, measure.ChannelL1.Ittx * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is10, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is12, measure.ChannelL1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is11, measure.ChannelL1.Ittl * 40),

                        ValuesConverterCommon.Analog.GetI(record.Is2a, measure.ChannelL2.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is2b, measure.ChannelL2.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is2c, measure.ChannelL2.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is2n, measure.ChannelL2.Ittx * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is20, measure.ChannelL2.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is22, measure.ChannelL2.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Is21, measure.ChannelL2.Ittl * 40),

                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetQ(record.Q),
                        record.D1To8,
                        record.D9To16,
                        record.D17To24
                    );
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                    (
                        this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
                    );
            }
        }
        
        private string GetParameter(AlarmJournalRecordStruct record)
        {
            return record.TriggeredDefense > 34 && record.TriggeredDefense < 53 || record.TriggeredDefense == 55
                ? string.Empty 
                : AjStrings.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalRecordStruct record, MeasureTransStruct measure)
        {
            int numParameter = record.NumberOfTriggeredParametr;
            ushort value1 = record.ValueOfTriggeredParametr1;
            ushort value2 = record.ValueOfTriggeredParametr2;

            if (numParameter >= 0 && numParameter<9 || numParameter >=13 && numParameter<=15)
            {
                return ValuesConverterCommon.Analog.GetI(value1, measure.ChannelL1.Ittl * 40);
            }
            if (numParameter >= 9 && numParameter<12 || numParameter >=17 && numParameter<=19)
            {
                return ValuesConverterCommon.Analog.GetI(value1, measure.ChannelL2.Ittl * 40);
            }
            if (numParameter == 12)
            {
                return ValuesConverterCommon.Analog.GetI(value1, measure.ChannelL1.Ittx * 40);
            }
            if (numParameter == 16)
            {
                return ValuesConverterCommon.Analog.GetI(value1, measure.ChannelL1.Ittx * 40);
            }
            if (numParameter >= 20 && numParameter < 26|| numParameter >= 27 && numParameter < 29)
            {
                return ValuesConverterCommon.Analog.GetU(value1, measure.ChannelU.KthlValue);
            }
            if (numParameter == 26)
            {
                return ValuesConverterCommon.Analog.GetU(value1, measure.ChannelU.KthxValue);
            }
            if (numParameter == 29)
            {
                return ValuesConverterCommon.Analog.GetF(value1);
            }
            if (numParameter == 33)
            {
                return ValuesConverterCommon.Analog.GetQ(value1);
            }
            if (numParameter == 34)
            {
                return ValuesConverterCommon.Analog.GetFullPower((short)value1, (short)value2, measure.ChannelU.KthlValue, measure.ChannelL1.Ittl);
            }
            if (numParameter >= 30 && numParameter <= 32)
            {
                return string.Empty;
            }
            return value1.ToString();
        }
        
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].HeaderText);
            }
            return table;
        }

        private void StartReadOption()
        {
            this._recordNumber = this._failCounter = 0;
            this._statusLabel.Text = READ_AJ;
            this._measure.LoadStruct();
        }
        #endregion [Help members]

        #region [Event Handlers]

        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            if(!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.StartReadOption();
        }
        
        private void _readAlarmJournalButtonClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.StartReadOption();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            this._table.Clear();
            //if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            //{
            //    byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
            //    AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
            //    int size = journal.GetSize();
            //    List<byte> journalBytes = new List<byte>();
            //    journalBytes.AddRange(Common.SwapArrayItems(file));
            //    int countRecord = journalBytes.Count / size;
            //    int j = 0;
            //    for (int i = 0; j < countRecord-2; i = i + size)
            //    {
            //        journal.InitStruct(journalBytes.GetRange(i,size).ToArray());
            //        this._alarmJournal.Value = journal;
            //        this.ReadRecord();
            //        j++;
            //    }
            //}
            //else
            //{
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            //}
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR801dvgAJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
        
        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._alarmJournal.RemoveStructQueries();
        }

        #endregion [Event Handlers]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR801dvg); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]
    }
}
