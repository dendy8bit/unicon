﻿namespace BEMN.MR801DVG.AlarmJournal
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Itc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UnCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._3U0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._FCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._QCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать журнал";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this._readAlarmJournalButtonClick);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(536, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(402, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(128, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР 761";
            this._openAlarmJournalDialog.Filter = "МР761 Журнал аварий(*.xml)|*.xml|МР761 Журнал аварий(*.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР 761";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР801двг";
            this._saveAlarmJournalDialog.Filter = "ЖА МР801двг|*.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР801двг";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 17);
            this._statusLabel.Text = ".";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(659, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(117, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР801двг";
            this._saveJournalHtmlDialog.Filter = "Журнал аварий МР801двг|*.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР801двг";
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._groupCol,
            this._IdaCol,
            this._IdbCol,
            this._IdcCol,
            this._ItaCol,
            this._ItbCol,
            this._Itc,
            this._Is1a,
            this._Is1b,
            this._Is1c,
            this._Is1n,
            this._Is10,
            this._Is12,
            this._Is11,
            this._Is2a,
            this._Is2b,
            this._Is2c,
            this._Is2n,
            this._Is20,
            this._Is22,
            this._Is21,
            this._UaCol,
            this._UbCol,
            this._UcCol,
            this._UnCol,
            this._UabCol,
            this._UbcCol,
            this._UcaCol,
            this._3U0Col,
            this._U2Col,
            this._FCol,
            this._QCol,
            this._D0Col,
            this._D1Col,
            this._D2Col});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 528);
            this._alarmJournalGrid.TabIndex = 24;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "№";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Дата/Время";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "Сообщение";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle6;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "Сработавшая защита";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._msgCol.HeaderText = "Сработавшая защита";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 140;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "Параметр срабатывания";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "Значение параметра срабатывания";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._groupCol.DataPropertyName = "Группа уставок";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle10;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IdaCol
            // 
            this._IdaCol.DataPropertyName = "Ida";
            this._IdaCol.HeaderText = "Ida";
            this._IdaCol.Name = "_IdaCol";
            this._IdaCol.ReadOnly = true;
            this._IdaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdaCol.Width = 28;
            // 
            // _IdbCol
            // 
            this._IdbCol.DataPropertyName = "Idb";
            this._IdbCol.HeaderText = "Idb";
            this._IdbCol.Name = "_IdbCol";
            this._IdbCol.ReadOnly = true;
            this._IdbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdbCol.Width = 28;
            // 
            // _IdcCol
            // 
            this._IdcCol.DataPropertyName = "Idc";
            this._IdcCol.HeaderText = "Idc";
            this._IdcCol.Name = "_IdcCol";
            this._IdcCol.ReadOnly = true;
            this._IdcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdcCol.Width = 28;
            // 
            // _ItaCol
            // 
            this._ItaCol.DataPropertyName = "Ita";
            this._ItaCol.HeaderText = "Ita";
            this._ItaCol.Name = "_ItaCol";
            this._ItaCol.ReadOnly = true;
            this._ItaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItaCol.Width = 25;
            // 
            // _ItbCol
            // 
            this._ItbCol.DataPropertyName = "Itb";
            this._ItbCol.HeaderText = "Itb";
            this._ItbCol.Name = "_ItbCol";
            this._ItbCol.ReadOnly = true;
            this._ItbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItbCol.Width = 25;
            // 
            // _Itc
            // 
            this._Itc.DataPropertyName = "Itc";
            this._Itc.HeaderText = "Itc";
            this._Itc.Name = "_Itc";
            this._Itc.ReadOnly = true;
            this._Itc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Itc.Width = 25;
            // 
            // _Is1a
            // 
            this._Is1a.DataPropertyName = "Ia s1";
            this._Is1a.HeaderText = "Ia s1";
            this._Is1a.Name = "_Is1a";
            this._Is1a.ReadOnly = true;
            this._Is1a.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1a.Width = 32;
            // 
            // _Is1b
            // 
            this._Is1b.DataPropertyName = "Ib s1";
            this._Is1b.HeaderText = "Ib s1";
            this._Is1b.Name = "_Is1b";
            this._Is1b.ReadOnly = true;
            this._Is1b.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1b.Width = 32;
            // 
            // _Is1c
            // 
            this._Is1c.DataPropertyName = "Ic s1";
            this._Is1c.HeaderText = "Ic s1";
            this._Is1c.Name = "_Is1c";
            this._Is1c.ReadOnly = true;
            this._Is1c.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1c.Width = 32;
            // 
            // _Is1n
            // 
            this._Is1n.DataPropertyName = "In s1";
            this._Is1n.HeaderText = "In s1";
            this._Is1n.Name = "_Is1n";
            this._Is1n.ReadOnly = true;
            this._Is1n.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1n.Width = 32;
            // 
            // _Is10
            // 
            this._Is10.DataPropertyName = "I0 s1";
            this._Is10.HeaderText = "I0 s1";
            this._Is10.Name = "_Is10";
            this._Is10.ReadOnly = true;
            this._Is10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is10.Width = 32;
            // 
            // _Is12
            // 
            this._Is12.DataPropertyName = "I2 s1";
            this._Is12.HeaderText = "I2 s1";
            this._Is12.Name = "_Is12";
            this._Is12.ReadOnly = true;
            this._Is12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is12.Width = 32;
            // 
            // _Is11
            // 
            this._Is11.DataPropertyName = "I1 s1";
            this._Is11.HeaderText = "I1 s1";
            this._Is11.Name = "_Is11";
            this._Is11.ReadOnly = true;
            this._Is11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is11.Width = 32;
            // 
            // _Is2a
            // 
            this._Is2a.DataPropertyName = "Ia s2";
            this._Is2a.HeaderText = "Ia s2";
            this._Is2a.Name = "_Is2a";
            this._Is2a.ReadOnly = true;
            this._Is2a.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2a.Width = 32;
            // 
            // _Is2b
            // 
            this._Is2b.DataPropertyName = "Ib s2";
            this._Is2b.HeaderText = "Ib s2";
            this._Is2b.Name = "_Is2b";
            this._Is2b.ReadOnly = true;
            this._Is2b.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2b.Width = 32;
            // 
            // _Is2c
            // 
            this._Is2c.DataPropertyName = "Ic s2";
            this._Is2c.HeaderText = "Ic s2";
            this._Is2c.Name = "_Is2c";
            this._Is2c.ReadOnly = true;
            this._Is2c.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2c.Width = 32;
            // 
            // _Is2n
            // 
            this._Is2n.DataPropertyName = "In s2";
            this._Is2n.HeaderText = "In s2";
            this._Is2n.Name = "_Is2n";
            this._Is2n.ReadOnly = true;
            this._Is2n.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2n.Width = 32;
            // 
            // _Is20
            // 
            this._Is20.DataPropertyName = "I0 s2";
            this._Is20.HeaderText = "I0 s2";
            this._Is20.Name = "_Is20";
            this._Is20.ReadOnly = true;
            this._Is20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is20.Width = 32;
            // 
            // _Is22
            // 
            this._Is22.DataPropertyName = "I2 s2";
            this._Is22.HeaderText = "I2 s2";
            this._Is22.Name = "_Is22";
            this._Is22.ReadOnly = true;
            this._Is22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is22.Width = 32;
            // 
            // _Is21
            // 
            this._Is21.DataPropertyName = "I1 s2";
            this._Is21.HeaderText = "I1 s2";
            this._Is21.Name = "_Is21";
            this._Is21.ReadOnly = true;
            this._Is21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is21.Width = 32;
            // 
            // _UaCol
            // 
            this._UaCol.DataPropertyName = "Ua";
            this._UaCol.HeaderText = "Ua";
            this._UaCol.Name = "_UaCol";
            this._UaCol.ReadOnly = true;
            this._UaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UaCol.Width = 27;
            // 
            // _UbCol
            // 
            this._UbCol.DataPropertyName = "Ub";
            this._UbCol.HeaderText = "Ub";
            this._UbCol.Name = "_UbCol";
            this._UbCol.ReadOnly = true;
            this._UbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbCol.Width = 27;
            // 
            // _UcCol
            // 
            this._UcCol.DataPropertyName = "Uc";
            this._UcCol.HeaderText = "Uc";
            this._UcCol.Name = "_UcCol";
            this._UcCol.ReadOnly = true;
            this._UcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcCol.Width = 27;
            // 
            // _UnCol
            // 
            this._UnCol.DataPropertyName = "Un";
            this._UnCol.HeaderText = "Un";
            this._UnCol.Name = "_UnCol";
            this._UnCol.ReadOnly = true;
            this._UnCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UnCol.Width = 27;
            // 
            // _UabCol
            // 
            this._UabCol.DataPropertyName = "Uab";
            this._UabCol.HeaderText = "Uab";
            this._UabCol.Name = "_UabCol";
            this._UabCol.ReadOnly = true;
            this._UabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UabCol.Width = 33;
            // 
            // _UbcCol
            // 
            this._UbcCol.DataPropertyName = "Ubc";
            this._UbcCol.HeaderText = "Ubc";
            this._UbcCol.Name = "_UbcCol";
            this._UbcCol.ReadOnly = true;
            this._UbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbcCol.Width = 33;
            // 
            // _UcaCol
            // 
            this._UcaCol.DataPropertyName = "Uca";
            this._UcaCol.HeaderText = "Uca";
            this._UcaCol.Name = "_UcaCol";
            this._UcaCol.ReadOnly = true;
            this._UcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcaCol.Width = 33;
            // 
            // _3U0Col
            // 
            this._3U0Col.DataPropertyName = "U0";
            this._3U0Col.HeaderText = "U0";
            this._3U0Col.Name = "_3U0Col";
            this._3U0Col.ReadOnly = true;
            this._3U0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._3U0Col.Width = 27;
            // 
            // _U2Col
            // 
            this._U2Col.DataPropertyName = "U2";
            this._U2Col.HeaderText = "U2";
            this._U2Col.Name = "_U2Col";
            this._U2Col.ReadOnly = true;
            this._U2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U2Col.Width = 27;
            // 
            // _FCol
            // 
            this._FCol.DataPropertyName = "F";
            this._FCol.HeaderText = "F";
            this._FCol.Name = "_FCol";
            this._FCol.ReadOnly = true;
            this._FCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._FCol.Width = 19;
            // 
            // _QCol
            // 
            this._QCol.DataPropertyName = "Q";
            this._QCol.HeaderText = "Q";
            this._QCol.Name = "_QCol";
            this._QCol.ReadOnly = true;
            this._QCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._QCol.Width = 21;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "Д[1-8]";
            this._D0Col.HeaderText = "Д[1-8]";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 43;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "Д[9-16]";
            this._D1Col.HeaderText = "Д[9-16]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 49;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "Д[17-24]";
            this._D2Col.HeaderText = "Д[17-24]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 55;
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 593);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Itc;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1a;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1b;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1c;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1n;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is10;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is12;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is11;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2a;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2b;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2c;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2n;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is20;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is22;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is21;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _3U0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _FCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _QCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
    }
}