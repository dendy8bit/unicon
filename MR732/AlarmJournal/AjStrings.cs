﻿using System.Collections.Generic;

namespace BEMN.MR801DVG.AlarmJournal
{
    public static class AjStrings
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup => new List<string>
        {
            "Основная",
            "Резервная"
        };

        /// <summary>
        /// Сработавшая защита
        /// </summary>
        public static List<string> TriggeredDefense => new List<string>
        {
            "Iд>> мгн.",
            "Iд>>",
            "Iд>",
            "I> 1",
            "I> 2",
            "I> 3",
            "I> 4",
            "I> 5",
            "I> 6",
            "I< ",
            "I2/I1",
            "I*> 1",
            "I*> 2",
            "I*> 3",
            "I*> 4",
            "I*> 5",
            "I*> 6",
            "U> 1",
            "U> 2",
            "U> 3",
            "U> 4",
            "U< 1",
            "U< 2",
            "U< 3",
            "U< 4",
            "F> 1",
            "F> 2",
            "F> 3",
            "F> 4",
            "F< 1",
            "F< 2",
            "F< 3",
            "F< 4",
            "Q>",
            "Q>>",
            "Блок. по Q",
            "Блок. по N",
            "ВНЕШ. 1",
            "ВНЕШ. 2",
            "ВНЕШ. 3",
            "ВНЕШ. 4",
            "ВНЕШ. 5",
            "ВНЕШ. 6",
            "ВНЕШ. 7",
            "ВНЕШ. 8",
            "ВНЕШ. 9",
            "ВНЕШ. 10",
            "ВНЕШ. 11",
            "ВНЕШ. 12",
            "ВНЕШ. 13",
            "ВНЕШ. 14",
            "ВНЕШ. 15",
            "ВНЕШ. 16",
            "P1",                            
            "P2",
            "ЖА СПЛ"
        };

        /// <summary>
        /// Сообщение
        /// </summary>
        public static List<string> Message => new List<string>
        {
            "ОШИБКА СООБЩЕНИЯ",
            "СИГНАЛ-ЦИЯ",
            "РАБОТА",
            "ОТКЛЮЧЕНИЕ",
            "НЕУСПЕШНОЕ АПВ",
            "АВАРИЯ",
            "ЛОГИКА",
            "СООБЩЕНИЕ "
        };

        /// <summary>
        /// Параметр срабатывания
        /// </summary>
        public static List<string> Parametr => new List<string>
        {
            "Ida",  //0
            "Idb",  //1
            "Idc",  //2
            "Ita",  //3
            "Itb",  //4
            "Itc",  //5

            "Ia s1", //6
            "Ib s1", //7
            "Ic s1", //8

            "Ia s2", //9
            "Ib s2", //10
            "Ic s2", //11

            "In s1", //12
            "I0 s1", //13
            "I2 s1", //14
            "I1 s1", //15

            "In s2", //16
            "I0 s2", //17
            "I2 s2", //18
            "I1 s2", //19

            "Ua",   //20
            "Ub",   //21
            "Uc",   //22
            "Uab",  //23
            "Ubc",  //24
            "Uca",  //25
            "Un ",  //26
            "U0 ",  //27
            "U2 ",  //28
            "F",    //29
            "Д1",   //30
            "Д2",   //31
            "СПЛ",  //32
            "Q",    //33
            "P, Q"     //34
        };
    }
}
