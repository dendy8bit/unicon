﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR801DVG.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _numOfDefAndNumOfTrigParam;
        [Layout(9)] private ushort _groupOfSetpointsAndTypeDmg;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _valueOfTriggeredParametr1;
        [Layout(12)] private ushort _ida;
        [Layout(13)] private ushort _idb;
        [Layout(14)] private ushort _idc;
        [Layout(15)] private ushort _ita;
        [Layout(16)] private ushort _itb;
        [Layout(17)] private ushort _itc;

        [Layout(18)] private ushort _is1a;
        [Layout(19)] private ushort _is1b;
        [Layout(20)] private ushort _is1c;

        [Layout(21)] private ushort _is2a;
        [Layout(22)] private ushort _is2b;
        [Layout(23)] private ushort _is2c;

        [Layout(24)] private ushort _is1n;
        [Layout(25)] private ushort _is10;
        [Layout(26)] private ushort _is12;
        [Layout(27)] private ushort _is11;
        
        [Layout(28)] private ushort _is2n;
        [Layout(29)] private ushort _is20;
        [Layout(30)] private ushort _is22;
        [Layout(31)] private ushort _is21;

        [Layout(32)] private ushort _ua;
        [Layout(33)] private ushort _ub;
        [Layout(34)] private ushort _uc;
        [Layout(35)] private ushort _uab;
        [Layout(36)] private ushort _ubc;
        [Layout(37)] private ushort _uca;
        [Layout(38)] private ushort _un;
        [Layout(39)] private ushort _u0;
        [Layout(40)] private ushort _u2;
        
        [Layout(41)] private ushort _f;
        [Layout(42)] private ushort _d1;
        [Layout(43)] private ushort _d2;
        [Layout(44)] private ushort _spl;
        [Layout(45)] private ushort _q;
        
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                int sum = this._year + this._month + this._date + this._hour + this._minute + this._second + this._millisecond*10 + this._message;
                return sum == 0;
            }
        }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond*10
                    );
            }
        }

        public ushort Message
        {
            get { return this._message; }
        }

        public int TriggeredDefense
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public int NumberOfTriggeredParametr
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 8, 9, 10, 11, 12, 13, 14, 15)>>8; }
        }

        public ushort ValueOfTriggeredParametr1
        {
            get { return this._valueOfTriggeredParametr; }
        }

        public ushort ValueOfTriggeredParametr2
        {
            get { return this._valueOfTriggeredParametr1; }
        }

        public int GroupOfSetpoints
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 0, 1, 2, 3, 4, 5, 6, 7); }
        }
        
        public int TypeOfDmg
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 8,9,10,11)>>8; }
        }

        public ushort Ida
        {
            get { return this._ida; }
        }

        public ushort Idb
        {
            get { return this._idb; }
        }

        public ushort Idc
        {
            get { return this._idc; }
        }

        public ushort Ita
        {
            get { return this._ita; }
        }

        public ushort Itb
        {
            get { return this._itb; }
        }

        public ushort Itc
        {
            get { return this._itc; }
        }

        public ushort Is1a
        {
            get { return this._is1a; }
        }
        public ushort Is1b
        {
            get { return this._is1b; }
        }
        public ushort Is1c
        {
            get { return this._is1c; }
        }
        public ushort Is1n
        {
            get { return this._is1n; }
        }
        public ushort Is10
        {
            get { return this._is10; }
        }
        public ushort Is12
        {
            get { return this._is12; }
        }
        public ushort Is11
        {
            get { return this._is11; }
        }

        public ushort Is2a
        {
            get { return this._is2a; }
        }
        public ushort Is2b
        {
            get { return this._is2b; }
        }
        public ushort Is2c
        {
            get { return this._is2c; }
        }
        public ushort Is2n
        {
            get { return this._is2n; }
        }
        public ushort Is20
        {
            get { return this._is20; }
        }
        public ushort Is22
        {
            get { return this._is22; }
        }
        public ushort Is21
        {
            get { return this._is21; }
        }

        public ushort Ua
        {
            get { return this._ua; }
        }

        public ushort Ub
        {
            get { return this._ub; }
        }

        public ushort Uc
        {
            get { return this._uc; }
        }

        public ushort Uab
        {
            get { return this._uab; }
        }

        public ushort Ubc
        {
            get { return this._ubc; }
        }

        public ushort Uca
        {
            get { return this._uca; }
        }

        public ushort Un
        {
            get { return this._un; }
        }

        public ushort U0
        {
            get { return this._u0; }
        }

        public ushort U2
        {
            get { return this._u2; }
        }
        
        public ushort F
        {
            get { return this._f; }
        }

        public ushort D1
        {
            get { return this._d1; }
        }

        public ushort D2
        {
            get { return this._d2; }
        }
        
        public string D1To8
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D1), true); }
        }

        public string D9To16
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D1), true); }
        }

        public string D17To24
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D2), true); }
        }

        public ushort Q
        {
            get { return this._q; }
        }

        #endregion [Properties]

    }
}
