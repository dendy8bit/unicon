﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MR801DVG.Properties;

namespace BEMN.MR801DVG.SystemJournal
{
    public partial class SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР801двг_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";

        #endregion [Constants]

        #region [Private fields]
        private DataTable _dataTable;
        private int _recordNumber;
        private MR801dvg _device;
        #endregion [Private fields]

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR801dvg device)
        {
            this.InitializeComponent();
            this._device = device;

            this._device.SetSjPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.SystemJournal.LoadStruct);
            this._device.SetSjPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._device.SystemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
        }
        
        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
        }
        #endregion [Properties]

        public void EnableButton(bool value)
        {
            this._exportButton.Enabled = value;
            this._saveJournalButton.Enabled = value;
            this._readJournalButton.Enabled = value;
            this._loadJournalButton.Enabled = value;
        }


        #region [Help members]
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_FAIL;
        }

        private void ReadRecord()
        {
            if (!this._device.SystemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._device.SystemJournal.Value.GetRecordTime,
                    this._device.SystemJournal.Value.GetRecordMessage
                );
                this.SaveJsNumber();
            }
            else
            {
                this.EnableButton(true);
            }
        }

        private void GetJournalDataTable()
        {
            this._dataTable = new DataTable(TABLE_NAME_SYS);
            this._dataTable.Columns.Add(NUMBER_SYS);
            this._dataTable.Columns.Add(TIME_SYS);
            this._dataTable.Columns.Add(MESSAGE_SYS);
            
            this._systemJournalGrid.DataSource = this._dataTable;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this.EnableButton(false);
            this._recordNumber = 0;
            this.SaveJsNumber();
        }

        private void SaveJsNumber()
        {
            this._device.SetSjPage.Value.Word = (ushort)this._recordNumber;
            this._device.SetSjPage.SaveStruct();
        }

        #endregion [Help members]


        #region [Events Handlers]
        private void Mr761SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.GetJournalDataTable();
            this.StartRead();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR801dvgSJ);
               this._statusLabel.Text = JOURNAL_SAVED;
            }
        }



        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR801dvg); }
        }
        
        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]
    }
}
