﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR801DVG.SystemJournal.Structures
{
  
    public class SystemJournalStruct : StructBase
    {

        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2}.{6:d2}";
        private const string MESSAGE_WITH_CODE_PATTERN = "{0} ({1})";
        private const string MESSAGE_SPL_PATTERN = "СООБЩЕНИЕ ЖС СПЛ N{0}";

        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _moduleErrorCode;
        [Layout(8)] private ushort _message;

        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                int sum = this._year +
                          this._month +
                          this._date +
                          this._hour +
                          this._minute +
                          this._second +
                          this._millisecond +
                          this._message;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetRecordTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );

            }
        }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string GetRecordMessage
        {
            get
            {
                if ((this._message == 7) || (this._message == 9) || (this._message == 11)
                    || (this._message == 13) || (this._message == 15))
                {
                    return string.Format(MESSAGE_WITH_CODE_PATTERN, StringsSj.Message[this._message], this._moduleErrorCode);
                }
                if (this._message < StringsSj.Message.Count)
                {
                    return StringsSj.Message[this._message];
                }
                if (this._message >= 500)
                {
                    return string.Format(MESSAGE_SPL_PATTERN, this._message - 499);
                }
                return string.Empty;
            }
        }
        #endregion [Properties]
    }
}
