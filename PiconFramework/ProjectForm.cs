using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;


namespace BEMN.Framework
{ 
    /// <summary>
	/// Summary description for DeviceNetForm.
	/// </summary>
	public class ProjectForm : Form
	{
        #region Classes

        #endregion Classes

        #region Field Controls

        private TreeView projectTree;
		private ContextMenuStrip _projectMenu;
        private ToolStripMenuItem menuAddDevice;
		private ImageList _projectImages;
        private System.ComponentModel.IContainer components;
        private ToolStripMenuItem _prjPropMenu;

        #endregion Field Controls

        #region Field Images

        Icon _newProjectIcon = new Icon(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("BEMN.Framework.Resources.new_project.ico"));
        Icon _defaultIcon = new Icon(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("BEMN.Framework.Resources.default.ico"));

        #endregion Field Images

        #region Construct/Destruct
        public ProjectForm()
		{
            this.InitializeComponent(); // ������������ �������� �����������, ����� �� ����� ����� Designer.cs
			TreeManager.Tree = this.projectTree; // ��������� � �������� Tree ������ TreeManager ������ ������ Treeview ����� ProjectForm
            var menuManager = new MenuManager();
            menuManager.AddDeviceItems(this.menuAddDevice, DllLoader.DeviceTypes); // ��� ����������� ������ ���� "�������� ����������" ������� ���������� ������ �� ����� ������������.
            this._projectImages.Images.Add(this._newProjectIcon);
            NodeManager.Images = new ImageIndexHash(this._projectImages);                       
		}
             
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (this.components != null)
                {
                    this.components.Dispose();
                }
			}
			base.Dispose( disposing );
		}

        #endregion Construct/Destruct 

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.projectTree = new TreeView();
            this._projectImages = new ImageList(this.components);
            this._projectMenu = new ContextMenuStrip(this.components);
            this.menuAddDevice = new ToolStripMenuItem();
            this._prjPropMenu = new ToolStripMenuItem();
            this._projectMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // projectTree
            // 
            this.projectTree.BorderStyle = BorderStyle.FixedSingle;
            this.projectTree.Dock = DockStyle.Fill;
            this.projectTree.HotTracking = true;
            this.projectTree.ImageIndex = 0;
            this.projectTree.ImageList = this._projectImages;
            this.projectTree.LabelEdit = true;
            this.projectTree.Location = new Point(0, 0);
            this.projectTree.Name = "projectTree";
            this.projectTree.SelectedImageIndex = 0;
            this.projectTree.ShowNodeToolTips = true;
            this.projectTree.Size = new Size(280, 349);
            this.projectTree.TabIndex = 1;
            this.projectTree.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.projectTree_NodeMouseClick);
            this.projectTree.BeforeLabelEdit += new NodeLabelEditEventHandler(this.projectTree_BeforeLabelEdit);
            this.projectTree.KeyDown += new KeyEventHandler(this.projectTree_KeyDown);
            // 
            // _projectImages
            // 
            this._projectImages.ColorDepth = ColorDepth.Depth32Bit;
            this._projectImages.ImageSize = new Size(16, 16);
            this._projectImages.TransparentColor = Color.Transparent;
            // 
            // _projectMenu
            // 
            this._projectMenu.Items.AddRange(new ToolStripItem[] {
            this.menuAddDevice,
            this._prjPropMenu});
            this._projectMenu.Name = "_projectMenu";
            this._projectMenu.Size = new Size(197, 48);
            // 
            // menuAddDevice
            // 
            this.menuAddDevice.Name = "menuAddDevice";
            this.menuAddDevice.Size = new Size(196, 22);
            this.menuAddDevice.Text = "�������� ����������";
            // 
            // _prjPropMenu
            // 
            this._prjPropMenu.Name = "_prjPropMenu";
            this._prjPropMenu.Size = new Size(196, 22);
            this._prjPropMenu.Text = "��������";
            // 
            // ProjectForm
            // 
            this.AutoScaleBaseSize = new Size(5, 13);
            this.ClientSize = new Size(280, 349);
            this.ControlBox = false;
            this.Controls.Add(this.projectTree);
            this.MaximizeBox = false;
            this.Name = "ProjectForm";
            this.Text = "����������";
            this._projectMenu.ResumeLayout(false);
            this.ResumeLayout(false);
		}
        #endregion Windows Form Designer generated code

        #region Methods Actions

        public bool SaveProject(string fname)
		{
            return ProjectLoader.SaveProject(fname, this.projectTree, this._projectImages);
		}

        public void LoadProject(string fname)
		{
            ProjectLoader.LoadProject(fname, this.projectTree, this._projectImages);
		}

        #endregion Methods Actions

        #region Methods UIControlHandlers

        private void projectTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            this.projectTree.SelectedNode = e.Node;
            
            if (e.Button == MouseButtons.Left)
            {
                if (NodeManager.IsPageNode(e.Node))
                {
                    Device device = TreeManager.FindDevice(e.Node.Parent);

                    if (null != device)
                    {
                        Type formType = (Type) e.Node.Tag;
                        FormManager.Activate(device, formType); 
                    }
                }
            }

            if (e.Button == MouseButtons.Right)
            {
                var menu = new ContextMenuStrip();

                if (NodeManager.IsProjectNode(e.Node))
                {
                    this._projectMenu.Show(this.projectTree, new Point(e.X, e.Y));
                    return;
                }

                if (NodeManager.IsDeviceNode(e.Node))
                {
                    menu = MenuManager.CreateDeviceNodeMenu(e); // �������� ������������ ���� ����������
                }

                if (e.Node.Tag is INodeView && (e.Node.Tag as INodeView).Deletable)
                {
                    MenuManager.AddDeleteItem(menu); // ���������� ������ ���� "�������"
                }

                if (0 != menu.Items.Count)
                {
                    menu.Show(this.projectTree, e.Location); // ����������� ������������ ���� 
                }
            }
        }

        private void projectTree_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (false == NodeManager.IsProjectNode(e.Node))
            {
                e.CancelEdit = true;
            }
            
        }
                
        private void projectTree_KeyDown(object sender, KeyEventArgs e)
        {
            var node = this.projectTree.SelectedNode;

            if (e.KeyCode == Keys.Delete && this.projectTree.Nodes.Count != 0 && NodeManager.IsDeletable(node))
            {
                TreeManager.OnRemoveNode(node);
            }
        }

        #endregion Methods UIControlHandlers       
    }
}