using System;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.Framework.BusinessLogic
{
	/// <summary>
	/// Summary description for FormManager.
	/// </summary>
	public class FormManager
	{
	    private static Form parentForm;
        public static Form[] MdiChildren => parentForm.MdiChildren;

        public static void Close(Device device)
		{
			foreach(Form f in parentForm.MdiChildren)
			{
				if (device ==  f.Tag)
				{                    
					f.Close();
				}
			}
		}

        public static void CloseAll()
		{
          	foreach(Form f in parentForm.MdiChildren)
			{
				f.Close();
			}
            
		}

        public static void MinimizeAll()
        {
            foreach (Form f in parentForm.MdiChildren)
            {
                f.WindowState = FormWindowState.Minimized;
            }
        }

        public static void ShowAll()
        {
            foreach (Form f in parentForm.MdiChildren)
            {
                f.WindowState = FormWindowState.Normal;
            }
        }

        public static void UpdateFormTitle(Device device)
        {
            Type[] formTypes = DllLoader.GetFormTypes(device.GetType());
            for (int i = 0; i < formTypes.Length; i++)
            {
                Form form = GetForm(device, formTypes[i]);
                if (null != form)
                {
                    form.Text = CreateDeviceFormTitle(device, form);    
                }
                
            }
        }

        public static string CreateDeviceFormTitle(Device device, Form form)
        {
            if (device.IsConnect && device.DeviceDlgInfo.IsConnectionMode)
            {
                if (device.DeviceDlgInfo.ModbusType == ModbusType.ModbusTcp)
                {
                    return (device as IDeviceView)?.NodeName + " �" + device.DeviceNumber + " - TCP-����������� " + (form as IFormView)?.NodeName;
                }
                string portString = 0 == device.MB.Port.PortNum ? "�����������" : device.MB.Port.PortNum.ToString();
                return (device as IDeviceView)?.NodeName + " �" + device.DeviceNumber + " - ���� " + portString+ ". "+ (form as IFormView)?.NodeName;
            }
            return $"{(device as IDeviceView)?.NodeName} �������-�����. {(form as IFormView)?.NodeName}";
        }

        public static void Activate(Device device, Type formType)
		{
            if (device.DeviceVersion == string.Empty && device.HaveVersion) return;
            Form deviceForm = GetForm(device, formType);
            TreeManager.IsTreeChanged = true;
            try
            {
                if (deviceForm == null || ((IFormView) deviceForm).Multishow)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    var prepareForm = Activator.CreateInstance(formType, device);
                    deviceForm = prepareForm as Form;
                    deviceForm.Text = CreateDeviceFormTitle(device, deviceForm);
                    deviceForm.Icon = Icon.FromHandle(new Bitmap(((IFormView) deviceForm).NodeImage).GetHicon());
                    deviceForm.MdiParent = parentForm;
                    deviceForm.Tag = device;
                    Cursor.Current = Cursors.Default;
                    deviceForm.Show();
                    deviceForm.Activate();
                }
                else
                {
                    deviceForm.Activate();
                }
            }
            catch (Exception e)
            {
                deviceForm?.Close();
            }
		}
        
        /// <summary>
        /// ��������� ����� �� ������ � ��������� ����������
        /// </summary>
        public static void Add(Device device, Type formType, Point location, Size size)
        {
            try
            {
                if (device.HaveVersion && device is IDeviceVersion)
                {
                    Type[] typesForm = ((IDeviceVersion) device).Forms;
                    if (!typesForm.Contains(formType))
                    {
                        throw new Exception("�� ������� ����������� ����� � ����������");
                    }
                }
                Form deviceForm = Activator.CreateInstance(formType, device) as Form;
                deviceForm.Text = CreateDeviceFormTitle(device, deviceForm);
                deviceForm.Icon = Icon.FromHandle(new Bitmap(((IFormView) deviceForm).NodeImage).GetHicon());
                deviceForm.Tag = device;
                deviceForm.Size = size;
                deviceForm.MdiParent = parentForm;
                deviceForm.Show();
                deviceForm.Location = location;
            }
            catch
            {
                
            }
        }

        public static Device Copy(Device device, Type formType)
        { 
            Form deviceForm = new Form();
            Modbus mb = new Modbus(device.MB.Port);
            Device curDevice = Activator.CreateInstance(device.GetType(), mb) as Device;
            curDevice.DeviceNumber = device.DeviceNumber;
            try
            {
                deviceForm = Activator.CreateInstance(formType, curDevice) as Form;
            }
            catch (Exception)
            {
            }
            deviceForm.Text = CreateDeviceFormTitle(curDevice, deviceForm);
            deviceForm.Icon = Icon.FromHandle(new Bitmap(((IFormView) deviceForm).NodeImage).GetHicon());
            deviceForm.MdiParent = parentForm;
            deviceForm.Tag = device;
            deviceForm.Show();
            deviceForm.Activate();
            
            return curDevice;
          }

        public static Form GetForm(Type formType)
        {
            Form ret = null;

            foreach (Form f in parentForm.MdiChildren)
            {
                if (formType == f.GetType())
                {
                   ret = f;
                }
            }
            return ret;
        }

        public static Form GetForm(Device device, Type formType) 
		{
			Form ret = null;

			foreach(Form f in parentForm.MdiChildren)
			{
				if (formType == f.GetType())
				{
					if (device == f.Tag)
					{
						ret = f;
					}
				}
			}
			return ret;
		}
                                
        public static Form ParentForm
        {
            set => parentForm = value;
            get => parentForm;
        }
	}
}
