using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.Framework.BusinessLogic
{
    public class TreeManager
    {
        public static event ActiveNodeChangedHandler ActiveNodeChanged;

        private static TreeView _tree;
        private static TreeNode _activeNode;
        private static DeviceDlgInfo _deviceDlgInfo;
        private static List<Device> _devices = new List<Device>();
        private static object _synsObj = new object();
        private static bool flag = true;


        public static List<Device> Device => _devices;
        public static TreeView Tree
        {
            get { return _tree; }
            set { _tree = value; }
        }
        public static TreeNode SelectedNode => _tree.SelectedNode;
        public static bool IsTreeChanged { get; set; } = false;
        
        /// <summary>
        /// ��������� ����� �� ���������� ����� ��� ����������
        /// ��� �������� � ������� ����� 
        /// </summary>
        public static bool ShowFrom { get; set; }


        public static TreeNode ActiveNode
        {
            get => _activeNode;
            set
            {
                if (null != _activeNode)
                {
                    _activeNode.NodeFont = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular);
                    _activeNode.Text = _activeNode.Text;
                }
                _activeNode = value;
                if (null != _activeNode)
                {
                    _activeNode.NodeFont = new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold);
                    _activeNode.Text = _activeNode.Text;
                    _activeNode = value;
                }

                IsTreeChanged = true;

                ActiveNodeChangedEventArgs e = new ActiveNodeChangedEventArgs(_activeNode);
                ActiveNodeChanged?.Invoke(e);
            }
        }
        
        public static void OnRemoveNode(TreeNode node)
        {
            try
            {
                if (NodeManager.IsDeviceNode(node))
                {
                    if (DialogResult.OK ==
                        MessageBox.Show("������� ���������� " + (node.Tag as INodeView)?.NodeName + "?", "������", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                    {
                        RemoveDeviceNode(node, true);
                    }
                }
                else
                {
                    node.Parent.Nodes.Remove(node);
                    IsTreeChanged = true;
                }
            }
            catch
            {
                
            }
        }
        public static void ReplaceText(TreeNode node, Device device)
        {
            if (node == null) return;

            string message;
            if (device.IsConnect && device.DeviceDlgInfo.IsConnectionMode)
            {
                // ��� ����� RTU 
                if (device.MB.ModbusType == ModbusType.ModbusRtu)
                {
                    if (device.Info.Version.IndexOf("���", StringComparison.Ordinal) != -1)
                    {
                        message = device.IsConnect ? device.Info.Version : $"��� ����� � {device.Info.Version}";
                    }
                    else
                    {
                        message = device.IsConnect ? device.Info.Type : "��� ����� � �����������";
                        if (message == "RZT110N") // ��������� �������� ��� ���
                        {
                            message = "��T-110";
                        }
                        if (device.HaveVersion) message += " ver." + device.DeviceVersion;
                        if (((INodeView) device).NodeName.Contains("��-����"))
                        {
                            message = string.Empty;
                        }
                    }
                }
                // ��� ����� TCP 
                else
                {
                    if (device.Info.Version.IndexOf("���", StringComparison.Ordinal) != -1)
                    {
                        message = device.IsConnect ? $"{device.Info.Version} (TCP)" : $"��� ����� � {device.Info.Version}";
                    }
                    else
                    {
                        message = device.IsConnect ? device.Info.Type : "��� ����� � �����������";
                        if (message == "RZT110N") // ��������� �������� ��� ���
                        {
                            message = "��T-110";
                        }
                        if (device.HaveVersion) message += " ver." + device.DeviceVersion + " (TCP)";
                        if (((INodeView)device).NodeName.Contains("��-����"))
                        {
                            message = string.Empty;
                        }
                    }
                }               
            }
            else
            {
                message = "�������-�����";
                if (device.HaveVersion)
                {
                    if (device.ToString() == "BEMN.MRUNIVERSAL.MRUniversal")
                    {
                        message = $"{device.DeviceType} �������-����� ver.{device.Info.Version}";
                    }
                    else
                    {
                        message += $" ver.{device.Info.Version}";
                    }                      
                }
            }

            ReplaceText(node, string.IsNullOrEmpty(device.Caption)
                    ? $"�{device.DeviceNumber} {((INodeView) node.Tag).NodeName} {(string.IsNullOrEmpty(message) ? string.Empty : $"({message})")}"
                    : string.Format("�{0} {1} [{3}] ({2})", device.DeviceNumber, ((INodeView) node.Tag).NodeName, message, device.Caption));
        }
        public static void ReplaceText(TreeNode node, string text)
        {
            Tree.Invoke(new ReplaceTextHandler(ReplaceTextImpl), node, text);
        }
        private static void ReplaceTextImpl(TreeNode node, string text)
        {
            node.Text = text;
        }
        public static void AddProjectNode()
        {
            _tree.Nodes.Add(NodeManager.CreateProjectNode());
            IsTreeChanged = true;
        }
        public static void AddDeviceNode(Device device, TreeNode parentNode, bool bSaveChanges)
        {
            if (0 == _tree.Nodes.Count)
            {
                AddProjectNode(); // �������� ���� "����� ������"
            }
            
            // ������� ���� ���������� 
            TreeNode deviceNode = NodeManager.CreateDeviceNode(device);

            if (device.IsConnect && device.DeviceDlgInfo.IsConnectionMode && device.HaveVersion)
            {
                deviceNode.Text += "(�����������...)";
            }
            
            try
            {
                try
                {
                    // �������� ���� ���� ��� �������� ����������-���������� ���������� IDeviceVersion
                    IDeviceVersion devVers = device as IDeviceVersion;
                    Type[] deviceForms;

                    // ���� ���������
                    if (devVers != null)
                    {
                        deviceForms = devVers.Forms;
                    }
                    // ���� ���
                    else
                    {
                        deviceForms = DllLoader.GetFormTypes(device.GetType()); // �������� ���� ���� �� ���� 
                    }

                    TreeNode[] formNodes = NodeManager.CreateFormNodes(deviceForms).ToArray();

                    string nodeName = string.Empty;

                    INodeView nodeView = deviceNode.Tag as INodeView;

                    if (nodeView != null)
                    {
                        nodeName = nodeView.NodeName; // �������� ���� ���������� � ������
                    }

                    if (DllLoader.GetDeviceNames().Contains(nodeName))
                    {
                        deviceNode.Nodes.AddRange(formNodes); // ��������� � ���� ���������� ���� ��� ����
                    }
                }

                catch (Exception e)
                {
                    MessageBox.Show("�� ����� ����������� ����������� ����! \n�� �������� ��� ���� ���������!.", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (null == parentNode)
                {
                    parentNode = _tree.TopNode;
                }

                parentNode.Nodes.Add(deviceNode); // ��������� � ���� "����� ������" ���� ����������

                device.DeviceNumberChanged += DeviceManager.OnDeviceNumberChanged;
                device.DeviceConnectStateChanged += DeviceConnectStateChanged;

                if (device.HaveVersion)
                {
                    ShowFrom = !device.DeviceType.ToUpper().StartsWith("MR"); // ���� ���������� - ��, �� �� ���������� �����

                    device._versionOk += OnDeviceVersionLoadOk;
                    device._versionFail += OnDeviceVersionLoadFail;
                    if (device.MB.Port == null || device.MB.Port.PortNum == 0 && !device.MB.NetworkEnabled || !device.IsConnect)
                    {
                        OnDeviceVersionLoadFail(device);
                    }
                    else
                    {
                        device.LoadVersion(device);
                    }
                }
                else
                {
                    ReplaceText(GetDeviceNode(device), device);
                }

                if (device is ICustomizeNode)
                {
                    ((ICustomizeNode) device).CustomNodeTextChanged += TreeManager_CustomNodeTextChanged;
                }
                _devices.Add(device);
                IsTreeChanged = true;
                Framework.MainWindow.InvokeAction(Tree.ExpandAll);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        
        /// <summary>
        /// �������� ���� ����������
        /// </summary>
        public static TreeNode GetDeviceNode(Device device)
        {
            return (TreeNode)Tree.Invoke(new GetNodeHandler(GetNode), device, null);
        }

        /// <summary>
        /// ������������ ��������� ��������� ����������� ����������
        /// </summary>
        public static void DeviceConnectStateChanged(Device device)
        {
            TreeNode deviceNode = GetDeviceNode(device);
            if (!device.IsConnect)
            {
                device.MB.Dispose();
            }
            
            TreeManager.ReplaceText(deviceNode, device);
        }

        public static double ConvertVersionFromString(string[] version)
        {
            if (version != null)
            {
                try
                {
                    string versionInString = string.Concat(version[0].Replace("T",string.Empty), CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, version[1].Replace("T", string.Empty)).Replace("�� ", string.Empty);
                    return Convert.ToDouble(versionInString);
                }
                catch
                {
                    return 0;
                }

            }
            return 0;
        }

        static void TreeManager_CustomNodeTextChanged(object sender, CustomNodeTextEventArgs e)
        {
            TreeNode node = TreeManager.GetNode(sender, TreeManager.Tree.TopNode);
            if (null != node)
            {
                node.Text = e.Text;
            }
        }

        public static void OnDeviceVersionLoadFail(object sender)
        {
            lock (_synsObj)
            {
                Device device = (Device) sender;

                if (device.DeviceVersion == null)
                {
                    device.DeviceVersion = string.Empty;
                }

                if (Tree.IsHandleCreated && !Tree.IsDisposed)
                {
                    TreeNode deviceNode = GetDeviceNode(device);

                    try
                    {
                        IDeviceVersion devVer = device as IDeviceVersion;
                        Type[] deviceForms;

                        if (devVer != null)
                        {
                            if (ShowFrom)
                            {
                                if (device.DeviceType.StartsWith("MR"))
                                {
                                    ChoiseDeviceNameAndType choise = new ChoiseDeviceNameAndType(device.GetType(), device.DeviceType);
                                    choise.ShowDialog();
                                    device.DeviceVersion = choise.DeviceVersion;
                                    device.DevicePlant = choise.DeviceConfig;
                                }
                                else
                                {
                                    ChooseVersion cv = new ChooseVersion(devVer.Versions, ((INodeView) device).NodeName);
                                    if (cv.ShowDialog() == DialogResult.OK)
                                    {
                                        device.DeviceVersion = cv.Vers;
                                    }
                                }
                            }

                            deviceForms = devVer.Forms;
                        }
                        else
                        {
                            deviceForms = DllLoader.GetFormTypes(device.GetType()); // �������� ���� ���� �� ���� ����������
                        }

                        
                        TreeNode[] formNodes = NodeManager.CreateFormNodes(deviceForms).ToArray();
                        deviceNode = (TreeNode)Tree.Invoke(new ChangeNodeHandler(ChangeNode), sender, null, formNodes);
                    }
                    catch
                    {
                        // ignored
                    }
                    if (deviceNode?.Tag is INodeView)
                    {
                        if (device.DeviceVersion == string.Empty)
                        {
                            device.DeviceVersion = "1.0";
                        }
                        ReplaceText(deviceNode, device);
                    }
                }
            }
        }

        public static void OnDeviceVersionLoadOk(object sender)
        {
            lock (_synsObj)
            {
                Device device = (Device) sender;
                string startVersion = device.Info.Version;
                device.InitVersion();
                TreeNode deviceNode = (TreeNode) Tree.Invoke(new GetNodeHandler(GetNode), sender, null);

                //todo ����� �������!!!
                string nodeNameKl = (deviceNode.Tag as INodeView).NodeName;
                if(nodeNameKl == "��27")
                {
                    device.DeviceVersion = "1.00";
                    device.Info.Type = "��27";
                }
                
                if (!string.IsNullOrEmpty(device.DeviceVersion) & !string.IsNullOrEmpty(startVersion) & (startVersion != device.DeviceVersion))
                {
                    MessageBox.Show($"��� ���������� {device.DeviceNumber} ������ �� ���������. ����� ��������� ������ �� ����������");
                }
                try
                {
                    IDeviceVersion devVer = device as IDeviceVersion;
                    Type[] deviceForms;
                    if (devVer != null)
                    {
                        if (device.DeviceVersion == string.Empty)
                        {
                            ChooseVersion cv = new ChooseVersion(devVer.Versions, ((INodeView) device).NodeName);
                            cv.StartPosition = FormStartPosition.CenterParent;
                            DialogResult dr = cv.ShowDialog();
                            if (dr == DialogResult.OK)
                            {
                                device.DeviceVersion = cv.Vers;
                            }
                        }
                        
                        deviceForms = devVer.Forms;
                    }
                    else
                    {
                        deviceForms = DllLoader.GetFormTypes(device.GetType()); // �������� ���� ���� �� ���� ����������
                    }

                    TreeNode[] formNodes = NodeManager.CreateFormNodes(deviceForms).ToArray();
                    deviceNode = (TreeNode)Tree.Invoke(new ChangeNodeHandler(ChangeNode), sender, null, formNodes);

                }
                catch
                {
                    device.DeviceVersion = string.Empty;
                }
                try
                {
                    string nodeName = (deviceNode.Tag as INodeView)?.NodeName;
                    List<string> devicesNames = DllLoader.GetDeviceNames();
                    string message = $"������������ ���������� �{device.DeviceNumber} ";

                    if (devicesNames.Contains(nodeName) && device.Info.Type != string.Empty)
                    {
                        ReplaceText(deviceNode, device);
                        if (device.Info.Type == "RZT110N")
                        {
                            message += $"��T-110 �� �{device.Info.Version}";
                        }
                        else if (device.Info.Type == "MR801")
                        {
                            message += $"��801 �.{device.Info.Version}";
                        }
                        Framework.MainWindow.InvokeAction(
                            () => MessageBox.Show(Framework.MainWindow, message, "����������"));
                    }
                    if(!devicesNames.Contains(nodeName))
                    {
                        deviceNode.Remove();
                        MessageBox.Show($"� ����� �{device.PortNum} ���������� ������ ����������.", $"�������� ���������� {nodeName}.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch {Debug.WriteLine("��� ������");}
            }
        }
        
        public static void RemoveDeviceNode(TreeNode node,bool saveChanges)
        {
            if (NodeManager.IsDeviceNode(node)) // ��� ���� ���������� ? 
            {
                Device device = node.Tag as Device;
                FormManager.Close(device); // �� - ������� ���� ����������
                if (device.MB.Logging || !Logger.LogIsEmpty)
                {
                    Logger.SaveLog(); // ��������� ���, ���� �� �����
                }

                CloseModbus(device);
            }

            for (int i = 0; i < node.Nodes.Count; i++)
            {
                RemoveDeviceNode(node.Nodes[i], false);
                i--;
            }

            if (node == ActiveNode)
            {
                ActiveNode = null;
            }

            node.Remove();
            IsTreeChanged = true;
        }

        public static void FreePort(TreeNode node)
        {
            if (NodeManager.IsDeviceNode(node)) // ��� ���� ���������� ? 
            {
                Device device = node.Tag as Device;
                FormManager.Close(device); // �� - ������� ���� ����������
                if (device.MB.Logging || !Logger.LogIsEmpty)
                {
                    Logger.SaveLog(); // ��������� ���, ���� �� �����
                }

                CloseModbus(device);
            }
        }

        public static void FreePortEmul(TreeNode node)
        {
            if (NodeManager.IsDeviceNode(node)) // ��� ���� ���������� ? 
            {
                Device device = node.Tag as Device;
                //FormManager.Close(device); // �� - ������� ���� ����������
                if (device.MB.Logging || !Logger.LogIsEmpty)
                {
                    Logger.SaveLog(); //��������� ���, ���� �� �����
                }

                CloseModbus(device);
            }
        }

        private static void CloseModbus(Device device)
        {
            if (device.MB.Port == null || !_devices.Any(d => d.PortNum == device.PortNum && d != device))
            {
                device.MB.Dispose(); // ��������� ����, ���� ������ �������� �� ���� �� ����������
            }
        }
              
        public static void RemoveNode(TreeNode node, TreeNode parentNode)
        {
            for (int i = 0; i < parentNode.Nodes.Count; i++)
            {
                if (node == parentNode.Nodes[i])
                {
                    parentNode.Nodes.Remove(node);
                    IsTreeChanged = true;
                }
                else
                {                    
                    RemoveNode(node, parentNode.Nodes[i]);
                }
            }
        }

        public static void RemoveProjectNode()
        {
            if (null != _tree.TopNode)
            {
                _tree.Nodes.Clear();
            }
        }
               
        public static Device FindDevice(TreeNode node)
        {
            if (null == node)
            {
                return null;
            }
            if (node.Tag is Device)
            {
                return node.Tag as Device;
            }
            return FindDevice(node.Parent);
        }

        public static TreeNode GetNode(object Tag, TreeNode node)
        {
            try
            {
                TreeNode ret = null;
                if (null == node)
                {
                    node = Tree.Nodes[0];
                }
                if (node.Tag == Tag)
                {
                    ret = node;
                }
                else
                {
                    for (int i = 0; i < node.Nodes.Count; i++)
                    {
                        ret = GetNode(Tag, node.Nodes[i]);
                        if (null != ret)
                        {
                            return ret;
                        }
                    }
                }
                return ret;
            }
            catch
            {
                Debug.WriteLine("��� ������");
                return null;
            }
        }

        public static TreeNode ChangeNode(object Tag, TreeNode node, TreeNode[] formNodes)
        {
            TreeNode ret = null;
            if (null == node)
            {
                node = Tree.Nodes[0];
            }
            if (node.Tag == Tag)
            {
                ret = node;
                ret.Nodes.Clear();
                ret.Nodes.AddRange(formNodes);
            }
            else
            {
                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    ret = ChangeNode(Tag, node.Nodes[i], formNodes);
                    if (null != ret)
                    {
                        return ret;
                    }
                }
            }
            return ret;
        }

        public static TreeNode GetNode(INodeView view, TreeNode parentNode)
        {
            TreeNode ret = null;

            if (NodeManager.IsNodesEqual(view, parentNode))
            {
                ret = parentNode;
            }
            else
            {
                for (int i = 0; i < parentNode.Nodes.Count; i++)
                {
                    ret = GetNode(view, parentNode.Nodes[i]);
                    if (null != ret)
                    {
                        return ret;
                    }
                    
                }  
            }
            return ret;
        }
    }
}