using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace BEMN.Framework.BusinessLogic
{
    public class ImageIndexHash : Hashtable
    {
        private ImageList _images;
                
        public ImageIndexHash(ImageList images)
        {
            _images = images;
        }

        public class NullImageException : NullReferenceException
        {
            public NullImageException(string msg)
                : base(msg)
            { }
        };


        public void AddImage(Image image, Type type)
        {
            if (null == image)
            {
                throw new NullImageException("����������� �� ����� ���� ���������");
            }
            if (false == ContainsKey(type))
            {
                _images.Images.Add(image);
                Add(type, _images.Images.Count - 1);
            }
        }
        
        public void AddImages(List<Image> images, Type[] types)
        {
            for (int i = 0; i < images.Count; i++)
            {
                AddImage(images[i], types[i]);
            }
        }
        
        public ImageList List
        {
            get
            {
                return _images;
            }
        }
        public int[] this[Type[] types]
        {
            get
            {
                ArrayList inds = new ArrayList();

                for (int i = 0; i < types.Length; i++)
                {
                    inds.Add(this[types[i]]);
                }
                int[] ret = new int[inds.Count];
                inds.CopyTo(ret, 0);
                return ret;
            }
        }
    }
}