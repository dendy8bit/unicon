using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms.DeviceConnectionForm;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.Framework.BusinessLogic
{
    public class DeviceManager
    {
        private static string _ipAddress = "";
        private static int _portNumber = 502;
        private static int _timeOut = 3000;
        private static bool _isGetInfoConnectionTcp = false;

        #region Field Modbus
        public static Device ActiveDevice
        {
            get
            {
                Device ret = null;
                if (null != TreeManager.ActiveNode)
                {
                    if (TreeManager.ActiveNode.Tag is Device)
                    {
                        ret = TreeManager.ActiveNode.Tag as Device;
                    }
                }
                return ret;
            }
        }
             
        public static byte[] PortNumbers => _portNumbers;

        private static byte[] _portNumbers;
        
        /// <summary>
        /// ��������� ���������� �� �����.
        /// </summary>
        public static bool ModbusInit()
        {
            bool ret = false;
            try
            {
                _portNumbers = Modbus.SerialServer.ExistPorts as byte[];
                ret = true;
            }
            catch (System.Runtime.InteropServices.COMException) // HRESULT
            {
                //if (DialogResult.OK == MessageBox.Show("COM ����� ������ ��� �����������, ����� � ����������� �� ����� ��������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning))
                //{
                    ret = false;
                    _portNumbers = new byte[0];
                    return ret;
                //}
            }
            catch
            {
                MessageBox.Show("SerialServer �� ����������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ret = false;
            }
            return ret;
        }

        #endregion

        public static void OnDeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
        {
            TreeNode node = TreeManager.Tree.Invoke(new GetNodeHandler(TreeManager.GetNode), sender, TreeManager.Tree.TopNode) as TreeNode;
            if (null == node) return;
            node.Text = NodeManager.DeviceNodeText(sender as Device);
            FormManager.UpdateFormTitle(sender as Device);
        }


        public static Device CreateDevice(Type deviceType)
        {
            DeviceDlgInfo info = ShowDeviceNumberDlg(deviceType);
            _ipAddress = info.Ip;
            _portNumber = info.TcpPort;
            _timeOut = info.ReceiveTimeout;
            SetOldValue(info);
            //GetOldValue(info);
            return info.IsOkPressed ? CreateDevice(deviceType, info) : null;
        }


        // ����� ���������� ������ CreateDevice, ������ ��� ������ ����������� ���� ����� DeviceConnectionForm
        public static Device CreateDeviceMR(Type deviceType, DeviceDlgInfo deviceDlgInfo, Modbus mb)
        {
            DeviceDlgInfo info = deviceDlgInfo;
            _ipAddress = info.Ip;
            _portNumber = info.TcpPort;
            _timeOut = info.ReceiveTimeout;
            SetOldValue(info);

            return CreateDevice(deviceType, deviceDlgInfo, mb);
        }

        
        public static Device CreateDevice(Type deviceType, DeviceDlgInfo deviceDlgInfo, Modbus modbus)
        {
            Cursor.Current = Cursors.WaitCursor;
            Device device = (Device)Activator.CreateInstance(deviceType, modbus);
            Cursor.Current = Cursors.Default;

            device.DeviceNumber = deviceDlgInfo.DeviceNumber;
            device.DeviceDlgInfo = deviceDlgInfo;
            device.SetStartConnectionState(deviceDlgInfo.IsConnectionMode);

            return device;
        }

        
        public static Device CreateDevice(Type deviceType, DeviceDlgInfo deviceInfo)
        {
            Device device = null;

            try
            {
                //������� Modbus -->
                Modbus modbus = new Modbus();
                if (deviceInfo.IsConnectionMode)
                {
                    if (deviceInfo.ModbusType == ModbusType.ModbusRtu)
                    {
                        modbus.SetNewPort(Modbus.SerialServer.GetNamedPort(deviceInfo.PortNumber, (uint) Modbus.ProcessID));
                    }
                    else
                    {
                        TcpConnectionForm connectionForm = new TcpConnectionForm();
                        connectionForm.SocketConnection(deviceInfo.Ip, deviceInfo.TcpPort, deviceInfo.ReceiveTimeout);
                        while (!connectionForm.Complite)
                        {
                            Application.DoEvents();
                        }
                        if (connectionForm.Socket == null)
                        {
                            return null;
                        }
                        
                        modbus.SetNetworkConnection(connectionForm.Socket, deviceInfo.ModbusType);
                    }
                }

                //<--
                Cursor.Current = Cursors.WaitCursor;
                device = (Device) Activator.CreateInstance(deviceType, modbus);
                Cursor.Current = Cursors.Default;
                //<--

                device.DeviceNumber = deviceInfo.DeviceNumber;
                device.DeviceDlgInfo = deviceInfo;
                device.SetStartConnectionState(deviceInfo.IsConnectionMode);
            }
            catch (MissingMethodException)
            {
                MessageBox.Show("���������� �� ����� ���� ���������,��� ��� � ��� ���������� ��� ������ �������.", "������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return device;
        }

        private static DeviceDlgInfo ShowDeviceNumberDlg(Type deviceType)
        {
            DeviceDlgInfo info = new DeviceDlgInfo();
            List<byte> notShown = new List<byte>(255);
            byte parentPortNumber = 1;
            ModbusInit();
            string nodeName = string.Empty;

            try
            {
                object dev = Activator.CreateInstance(deviceType);
                nodeName = (dev as INodeView)?.NodeName;
            }
            catch (Exception)
            {
            }
            
            info.Ip = _ipAddress;
            info.TcpPort = _portNumber;
            info.ReceiveTimeout = _timeOut;

            if (!_isGetInfoConnectionTcp)
            {
                GetOldValue(info);
            }

            DeviceConnectionForm dlgDevice = new DeviceConnectionForm(nodeName, PortNumbers, notShown, true, parentPortNumber);
            
            info = dlgDevice.ShowDialogRes(info);
            _isGetInfoConnectionTcp = true;
            return info;
        }

        /// <summary>
        /// ������� ��������� ������ ��� ����������� ������ ���������� �� ����������, ����� �� ����� ����������
        /// </summary>
        public static Device CreateDevice(DeviceDlgInfo deviceDlgInfo)
        {
            Device device = new Device(CreateModbus(deviceDlgInfo))
            {
                DeviceDlgInfo = deviceDlgInfo,
                DeviceNumber = deviceDlgInfo.DeviceNumber
            };

            device.SetStartConnectionState(deviceDlgInfo.IsConnectionMode);
            return device;
        }

        /// <summary>
        /// ������� Modbus
        /// </summary>
        public static Modbus CreateModbus(DeviceDlgInfo deviceDlgInfo)
        {
            Modbus modbus = new Modbus();

            // ���� ��� ������ ����� "�����������" ��� ���� ��������� �����
            if (deviceDlgInfo.IsConnectionMode)
            {
                if (deviceDlgInfo.ModbusType == ModbusType.ModbusRtu)
                {
                    modbus.SetNewPort(Modbus.SerialServer.GetNamedPort(deviceDlgInfo.PortNumber, (uint) Modbus.ProcessID));
                }
                else
                {
                    TcpConnectionForm connectionForm = new TcpConnectionForm();

                    // ������ SocketConnection ��������� ����������� �����, ��� �������� ������������ � ����������
                    connectionForm.SocketConnection(deviceDlgInfo.Ip, deviceDlgInfo.TcpPort, deviceDlgInfo.ReceiveTimeout);

                    // ���� �� ����������� � ����������, ��������� �������� ������������ ������
                    while (!connectionForm.Complite)
                    {
                        Application.DoEvents();
                    }
                    if (connectionForm.Socket == null)
                    {
                        return null;
                    }

                    modbus.SetNetworkConnection(connectionForm.Socket, deviceDlgInfo.ModbusType);
                }
            }
            return modbus;
        }
        
        public static DeviceDlgInfo ShowDeviceNumberDlg()
        {
            DeviceDlgInfo info = new DeviceDlgInfo();
            List<byte> notShown = new List<byte>(255);
            byte parentPortNumber = 1;
            ModbusInit();

            info.Ip = _ipAddress;
            info.TcpPort = _portNumber;
            info.ReceiveTimeout = _timeOut;

            if (!_isGetInfoConnectionTcp)
            {
                GetOldValue(info);
            }

            DeviceConnectionForm dlgDevice = new DeviceConnectionForm(PortNumbers, notShown, true, parentPortNumber);

            info = dlgDevice.ShowDialogRes(info);
            _isGetInfoConnectionTcp = true;

            if (info.IsOkPressed)
            {
                _ipAddress = info.Ip;
                _portNumber = info.TcpPort;
                _timeOut = info.ReceiveTimeout;
                SetOldValue(info);
                return info;
            }
            
            return null;
        }

        private static void SetOldValue(DeviceDlgInfo info)
        {
            Properties.Settings.Default.IpAddres = info.Ip;
            Properties.Settings.Default.TcpPort = info.TcpPort;
            Properties.Settings.Default.ReceiveTimeout = info.ReceiveTimeout;
            Properties.Settings.Default.Save();
        }

        private static void GetOldValue(DeviceDlgInfo info)
        {
            info.Ip = Properties.Settings.Default.IpAddres;
            info.TcpPort = Properties.Settings.Default.TcpPort;
            info.ReceiveTimeout = Properties.Settings.Default.ReceiveTimeout;
        }

        public static Device[] GetDevices()
        {
            return SearchDevices(TreeManager.Tree.TopNode);
        }

        public static Device[] SearchDevices(TreeNode root)
        {
            ArrayList ret = new ArrayList();

            if (null == TreeManager.Tree.TopNode)
            {
                return new Device[]{};
            }

            if (root == null)
            {
                root = TreeManager.Tree.TopNode;
            }

            foreach (TreeNode chnode in root.Nodes)
            {
                if (chnode.Tag is Device)
                {
                    ret.Add(chnode.Tag as Device);
                }
                if (null != chnode.Nodes)
                {
                    ret.AddRange(SearchDevices(chnode));
                }
            }

            return (Device[])ret.ToArray(typeof(Device));
        }
    }
}