using System;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer.Exceptions;

namespace BEMN.Framework.BusinessLogic
{
    public class ProjectLoader
    {
        private static XmlTextWriter _writer;
        private static StreamReader _reader;
        private static StringCollection _exceptions = new StringCollection();
        
        /// <summary>
        /// ������� XMLElement �� ������ TreeNode, ��� ������������ ���������� 
        /// </summary>
        /// <param name="doc"> XML ��������	</param>
        /// <param name="node"> ���� ������ </param>
        /// <param name="imageList"></param>
        /// <returns> XML ������� </returns>
        private static XmlElement CreateXmlTree(XmlDocument doc, TreeNode node, ImageList imageList)
        {
            XmlElement ret = doc.CreateElement("TreeNode");
            XmlElement text = doc.CreateElement("Text");
            text.InnerText = node.Text;
            XmlElement image = doc.CreateElement("Image");
            image.InnerText = ImageHelper.ToString(imageList.Images[node.ImageIndex]);

            XmlElement tag = doc.CreateElement("Tag");

            // ���� ���� - ����� ������
            if (NodeManager.IsProjectNode(node))
            {
                XmlElement prj = doc.CreateElement("Project");
                prj.InnerText = node.Tag.ToString();
                tag.AppendChild(prj);
            }
            // ���� ���� - ���������� (�� � ��.)
            if (node.Tag is INodeSerializable)
            {
                tag.AppendChild((node.Tag as INodeSerializable).ToXml(doc));
            }

            // ���� ���� - ����� ���������� (��, ��, ��������� � �.�.)
            if (NodeManager.IsPageNode(node))
            {                
                tag.AppendChild(CreateXmlFromForm(doc, node.Tag as Type, TreeManager.FindDevice(node)));
            }

            ret.AppendChild(text);
            ret.AppendChild(tag);
            ret.AppendChild(image);

            if (null != TreeManager.ActiveNode && node == TreeManager.ActiveNode)
            {
                XmlAttribute isActiveAttr = doc.CreateAttribute("IsActive");
                isActiveAttr.Value = "true";
                ret.Attributes.Append(isActiveAttr);
            }

            foreach (TreeNode childNode in node.Nodes)
            {
                ret.AppendChild(CreateXmlTree(doc, childNode, imageList));
            }

            return ret;
        }
        
        private static XmlElement CreateXmlFromForm(XmlDocument doc, Type formType, Device device)
        {
            XmlElement form = doc.CreateElement("Form");
            XmlAttribute type = doc.CreateAttribute("Type");
            type.InnerText = formType.ToString();
            Form f = null == device ? FormManager.GetForm(formType) : FormManager.GetForm(device, formType);
            if (null != f)
            {
                XmlElement location = doc.CreateElement("Location");
                XmlElement size = doc.CreateElement("Size");
                XmlElement l_x = doc.CreateElement("X");
                XmlElement l_y = doc.CreateElement("Y");
                XmlElement width = doc.CreateElement("Width");
                XmlElement height = doc.CreateElement("Height");
                l_x.InnerText = f.Location.X.ToString();
                l_y.InnerText = f.Location.Y.ToString();
                width.InnerText = f.Width.ToString();
                height.InnerText = f.Height.ToString();
                location.AppendChild(l_x);
                location.AppendChild(l_y);
                size.AppendChild(width);
                size.AppendChild(height);
                form.AppendChild(location);
                form.AppendChild(size);
            }

            form.Attributes.Append(type);

            return form;
        }

        public static void SaveSettings(Framework frm)
        {
            string pathSettings = Application.StartupPath + "\\UniconSettings.ini";
            UniconSettings settings = new UniconSettings();
            settings.Settings.OkExchange = frm.ShowOk;
            settings.Settings.FailExchange = frm.ShowFail;
            settings.Settings.Autosave = frm.Autosave;
            settings.Settings.AutoloadSetpoints = frm.AutoLoadSetpoints;
            settings.Settings.UpdateWeb = frm.UpdateWeb;
            settings.Settings.LastVersion = frm.LastVersionTime;
            settings.Settings.WriteForSignatures = Framework.IsWriteSignatures;
            using (_writer = new XmlTextWriter(pathSettings, Encoding.Unicode))
            {
                _writer.Formatting = Formatting.Indented;
                XmlSerializer serializer = new XmlSerializer(typeof (UniconSettings));
                serializer.Serialize(_writer, settings);
            }
        }


        public static void LoadSettings(Framework frm)
        {
            string pathSettings = Application.StartupPath + "\\UniconSettings.ini";
            if (File.Exists(pathSettings)) // ��������� ���������� �� ���� �������� ������� UniconSettings.ini" �� ���������� ����
            {
                using (_reader = new StreamReader(pathSettings))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof (UniconSettings));
                    UniconSettings settings = (UniconSettings) serializer.Deserialize(_reader);
                    frm.ShowOk = settings.Settings.OkExchange;
                    frm.ShowFail = settings.Settings.FailExchange;
                    frm.Autosave = settings.Settings.Autosave;
                    frm.AutoLoadSetpoints = settings.Settings.AutoloadSetpoints;
                    frm.UpdateWeb = settings.Settings.UpdateWeb;
                    frm.LastVersionTime = settings.Settings.LastVersion;
                    Framework.IsWriteSignatures = settings.Settings.WriteForSignatures;
                }
            }
        }

        public static void Close()
        {
            _writer?.Close();
            _reader?.Close();
        }
        
        public static bool SaveProject(string fname, TreeView tree, ImageList images)
        {
            string directory = Path.GetDirectoryName(fname);
            if (string.IsNullOrEmpty(directory)) return false;
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            if (0 != tree.Nodes.Count && null != tree.Nodes[0])
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.AppendChild(xmlDoc.CreateElement("Unicon"));
                XmlElement prjElement = xmlDoc.CreateElement("Project");
                xmlDoc.DocumentElement.AppendChild(prjElement);
                prjElement.AppendChild(CreateXmlTree(xmlDoc, tree.Nodes[0], images));
                
                XmlAttribute versionAttr = xmlDoc.CreateAttribute("Version");
                versionAttr.Value = GetCurrentAssemblyVersion();
                xmlDoc.DocumentElement.Attributes.Append(versionAttr);
                
                xmlDoc.Save(fname);
                return true;
            }
            return false;
        }

        public static string GetCurrentAssemblyVersion()
        {
            string system = System.Text.RegularExpressions.Regex.Replace(Assembly.GetExecutingAssembly().CodeBase, "mscorlib.dll", "System.dll");
            system = System.Text.RegularExpressions.Regex.Replace(system, "file:///", "");
            AssemblyName name = AssemblyName.GetAssemblyName(system);
            return name.Version.ToString(4);
        }

        public static void LoadProject(string fname, TreeView tree, ImageList images)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fname);

            string fileVersion = "0.0.1.0";
            if (null != xmlDoc.DocumentElement.Attributes["Version"])
            {
                fileVersion = xmlDoc.DocumentElement.Attributes["Version"].Value;
            } 
            string assemblyVersion = GetCurrentAssemblyVersion();
            Version version= new Version(assemblyVersion);
            if (version < new Version(2,3) )
            {
                MessageBox.Show("������ ����� " + fname + " - " + fileVersion + 
                                ", �� ��������� � ������� ��������� - " + assemblyVersion + 
                                ". ���� �� ����� ���� ��������.","��������",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            else
            {
                tree.Nodes.Clear();
                XmlNode treeNode = xmlDoc.SelectSingleNode("/Unicon/Project/TreeNode");
                tree.Nodes.Add(CreateTreeNode(treeNode, images));
                tree.ExpandAll();
                                
                string errMsg = "";
                foreach (string msg in _exceptions)
                {
                    errMsg += msg + "\n";
                }
                if (0 != _exceptions.Count)
                {
                    MessageBox.Show(errMsg);
                }
                TreeManager.IsTreeChanged = false;
            }
        }

        private static object CreateTag(XmlElement node)
        {
            object ret = null;
            if (0 != node.ChildNodes.Count)
            {
                switch (node.ChildNodes[0].Name)
                {
                    case "Project":
                        ret = node.ChildNodes[0].InnerText;
                        break;
                    case "Device":
                        ret = CreateDevice(node);
                        break;
                    case "Form":
                        XmlAttribute formAttr = node.ChildNodes[0].Attributes["Type"];
                        if (null != formAttr)
                        {
                            ret = DllLoader.GetType(formAttr.Value);
                        }
                        break;
                    default:
                        ret = Activator.CreateInstance(DllLoader.GetType(node.ChildNodes[0].Attributes["Type"].Value));
                        if (ret is INodeSerializable)
                        {
                            (ret as INodeSerializable).FromXml((XmlElement)node.ChildNodes[0]);
                        }
                        break;
                }
            }

            return ret;
        }

        private static Device CreateDevice(XmlElement node)
        {
            // ������� ��������� ����������
            Device bufferDevice = Activator.CreateInstance(DllLoader.GetType(node.ChildNodes[0].Attributes["Type"].InnerText)) as Device;
            try
            {
                // ������� ������, ������, ���������� �����, ����� ����������, deviceDlgInfo
                bufferDevice.FromXml(node.ChildNodes[0] as XmlElement);

            }
            
            catch (InvalidPortException e)
            {
                if (false == _exceptions.Contains(e.Message))
                {
                    _exceptions.Add(e.Message);
                }
            }

            // ������� ��������� ���������� c ��������
            Device device = Activator.CreateInstance(bufferDevice.GetType(), bufferDevice.MB) as Device;

            if (device != null)
            {
                device.DeviceDlgInfo = bufferDevice.DeviceDlgInfo;
                device.DeviceNumber = bufferDevice.DeviceNumber;
                device.DeviceVersion = bufferDevice.DeviceVersion;
                device.DevicePlant = bufferDevice.DevicePlant;
                device.Caption = bufferDevice.Caption;
                ///todo ���� ������� �������
                //if (device is IDeviceVersion)
                //{
                //    var forms = ((IDeviceVersion)device).Forms;
                //}
                device.DeviceConnectStateChanged += TreeManager.DeviceConnectStateChanged;
                device.DeviceNumberChanged += DeviceManager.OnDeviceNumberChanged;
                if (device.HaveVersion)
                {
                    TreeManager.ShowFrom = false;
                    device._versionOk += TreeManager.OnDeviceVersionLoadOk;
                    device._versionFail += TreeManager.OnDeviceVersionLoadFail;
                    // ������ ���������� ������ ����������� �����, ����� ��� �� �������� ����� �������� �������
                    if (device.IsConnect&&string.IsNullOrEmpty(device.DeviceVersion))
                        device.LoadVersion(device);
                }

                Framework.IsProjectOpening = false;
            }
            
            return device;
        }
        
        private static TreeNode CreateTreeNode(XmlNode rootNode, ImageList imageList)
        {
            TreeNode ret = new TreeNode();

            foreach (XmlElement node in rootNode.ChildNodes)
            {                
                switch (node.Name)
                {
                    case "TreeNode":
                        TreeNode curNode = CreateTreeNode(node, imageList);
                        if (NodeManager.IsPageNode(curNode)) // ���� ��� ����� 
                        {
                            // ������� ����������
                            XmlNode x = node.SelectSingleNode("Tag/Form/Location/X");  
                            XmlNode y = node.SelectSingleNode("Tag/Form/Location/Y");
                            XmlNode width = node.SelectSingleNode("Tag/Form/Size/Width");
                            XmlNode height = node.SelectSingleNode("Tag/Form/Size/Height");
                            if (ret.Tag is Device && null != x && null != y && null != width && null != height) // ���� ���������� �������� �� 0
                            {
                                FormManager.Add((Device) ret.Tag, (Type) curNode.Tag,
                                    new Point(int.Parse(x.InnerText), int.Parse(y.InnerText)),
                                    new Size(int.Parse(width.InnerText), int.Parse(height.InnerText))); // ��������� ����� � �������� �������������
                            }
                        }

                        ret.Nodes.Add(curNode);

                        if (null != node.Attributes["IsActive"] && "true" == node.Attributes["IsActive"].Value)
                        {
                            TreeManager.ActiveNode = curNode;
                        }

                        break;
                    case "Text":
                        ret.Text = node.InnerText;
                        break;
                    case "Image":
                        imageList.Images.Add(ImageHelper.FromString(node.InnerText));
                        ret.ImageIndex = ret.SelectedImageIndex = imageList.Images.Count - 1;
                        break;
                    case "Tag":
                        ret.Tag = CreateTag(node);
                        break;
                }
            }
                                                
            return ret;
        }
    }
}