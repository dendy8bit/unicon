using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.DeviceConnectionForm;
using BEMN.Interfaces;
using BEMN.MBServer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BEMN.Framework.BusinessLogic
{
    public class DeviceAddEventArgs : EventArgs
    {
        public string DeviceName { get; set; }

        public bool Success { get; set; }

        public Device AddedDevice { get; set; }


        public DeviceAddEventArgs(Device device, string name, bool success)
        {
            this.AddedDevice = device;
            this.DeviceName = name;
            this.Success = success;
        }
    }

    /// <summary>
    /// ��������� ���������� ���� ������������ � ����������
    /// </summary>
    public class MenuManager
    {
        public static event DeviceAddHandler DeviceAddBegin;
        public static event DeviceAddHandler DeviceAddEnd;

        private bool loadVersion; // ������������ ��� ����������� ������ ������ (������ 1 ���), ���� �������� � ����� this.OnDeviceVersionLoadFail
        public static bool NeedRead; 

        public static void RemoveDeviceItems(ContextMenuStrip curMenu, List<Type> deviceTypes)
        {
            foreach (var curType in deviceTypes)
            {
                string name;
                Image image;
                try
                {
                    name = DllLoader.DeviceNameHash[curType];
                    image = DllLoader.DeviceImageHash[curType];
                }
                catch (KeyNotFoundException)
                {
                    continue;
                }

                var item = new ToolStripMenuItem(name, image);

                if (false == IsMenuAdded(curMenu.Items, item))
                {
                    curMenu.Items.Remove(item);
                }
            }
        }

        public void AddDeviceItems(ToolStripDropDownButton button, List<Type> deviceTypes)
        {
            this.GetDeviseItems(button, deviceTypes, this.OnAddDeviceItemClick);
        }
        
        public void AddDeviceItems(ContextMenuStrip curMenu, List<Type> deviceTypes)
        {
            foreach (var curType in deviceTypes)
            {
                string name;
                Image image;
                try
                {
                    name = DllLoader.DeviceNameHash[curType];
                    image = DllLoader.DeviceImageHash[curType];
                }
                catch (KeyNotFoundException)
                {
                    continue;
                }
                var item = new ToolStripMenuItem(name, image, this.OnAddDeviceItemClick);
                item.Tag = curType; // �������� Tag �������� ��� ����������
                if (false == IsMenuAdded(curMenu.Items, item))
                {
                    curMenu.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// ��������� ���������� �� ����� ���� � ���������.
        /// </summary>
        private static bool IsMenuAdded(ToolStripItemCollection menuColl, ToolStripMenuItem item)
        {
            var ret = false;
            foreach (ToolStripMenuItem current in menuColl)
            {
                if (current.Text == item.Text)
                {
                    ret = true;
                }
            }
            return ret;
        }

        /// <summary>
        /// ��������� ���������� ����� ���� "�������� ����������" �������.
        /// </summary>
        /// <param name="rootItem">����� ���� "�������� ����������".</param>
        /// <param name="deviceTypes">������ ���������, ��������������� ������ "�������� ����������".</param>
        /// <param name="OnMenuClick">������� �� ������� ��� �� "�������� ����������".</param>
        private void GetDeviseItems(ToolStripDropDownItem rootItem, List<Type> deviceTypes, EventHandler OnMenuClick)
        {
            /// <summary>
            /// ������ �� ���, ��27, ������ ���������, ��303.
            /// </summary>
            var TSList = new List<ToolStripMenuItem>();

            /// <summary>
            /// ������ ���� �� � ���.
            /// </summary>
            var DefensesList = new List<ToolStripMenuItem>();

            /// <summary>
            /// ������ ���100 � ���.
            /// </summary>
            var TcsList = new List<ToolStripMenuItem>();

            /// <summary>
            /// ������ ���� ����.
            /// </summary>
            var TokDefList = new List<ToolStripMenuItem>();

            /// <summary>
            /// ������ ���� ��� � ���-24.
            /// </summary>
            var ArcDefensesList = new List<ToolStripMenuItem>();

            /// <summary>
            /// ������ ���� ���.
            /// </summary>
            var ControlersList = new List<ToolStripMenuItem>();

            ToolStripMenuItem MPSet = null;  // ����� ���� "��-����"
            var itemMR = new ToolStripMenuItem("����������������� ���� ��", Properties.Resources.mrBig, OnMenuClick); // ������ -> "����������������� ����"
            var itemRzt = new ToolStripMenuItem("���-110", Properties.Resources.rzt110, OnMenuClick); // ������ -> "���� �����������������"
            var itemMtz = new ToolStripMenuItem("���-610�.3�(��300)", Properties.Resources.mr301, OnMenuClick); // ������ -> "���-610�.3�(��300)"

            foreach (var curType in deviceTypes)
            {
                if (curType.Name == "RztDevice")
                {
                    itemRzt.Tag = curType;
                }

                if (curType.Name == "MR300")
                {
                    itemMtz.Tag = curType;
                }

                string name;
                Image image;
                try
                {
                    name = DllLoader.DeviceNameHash[curType]; // ������� ��� ����������
                    image = DllLoader.DeviceImageHash[curType]; // ������� �������� ����������
                }
                catch (KeyNotFoundException)
                {
                    continue;
                }

                var item = new ToolStripMenuItem(name, image, OnMenuClick);

                item.Tag = curType; // � �������� Tag ������� ������� ����� ����������
                if (IsMenuAdded(rootItem.DropDownItems, item)) continue; // ��������� ���������� �� ����� ���� � ���������. ���� ��, ���������� ����������

                if (name == "��-����")
                {
                    MPSet = item;
                }
                else if (name.StartsWith("���") || name.StartsWith("���"))
                {
                    TcsList.Add(item);
                }
                else if (name.StartsWith("��") || name.StartsWith("���"))
                {
                    DefensesList.Add(item);
                }
                else if (name.StartsWith("���"))
                {
                    ControlersList.Add(item);
                }
                else if (name.StartsWith("����"))
                {
                    TokDefList.Add(item);
                }
                else if (name.StartsWith("���") | name == "���-24")
                {
                    ArcDefensesList.Add(item);
                }
                else
                {
                    TSList.Add(item);
                }
            }


            var Defenses = DefensesList.ToArray();
            Array.Sort(Defenses, new DefCompare()); // ��������� ������ ����� �������� ����� ��������������� ������ 

            //Hide MRUniversal
            //Defenses[1].Visible = false;

            // ���������� ���������� ����� � ������� ��������, ������� ������, ����� �����
            try
            {
                var mr801new = Defenses[19];
                var mr801old = Defenses[20];

                var mr901new = Defenses[23];
                var mr901old = Defenses[24];

                var mr902new = Defenses[25];
                var mr902old = Defenses[26];

                Defenses[19] = mr801old;
                Defenses[20] = mr801new;

                Defenses[23] = mr901old;
                Defenses[24] = mr901new;

                Defenses[25] = mr902old;
                Defenses[26] = mr902new;

                var temp = TokDefList[1]; // ��� ���� (��� ���������� � ������������ �������)
                TokDefList[1] = TokDefList[3];
                TokDefList[3] = TokDefList[2];
                TokDefList[2] = temp;
                TokDefList.Reverse();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            var Controlers = ControlersList.ToArray();
            Array.Sort(Controlers, new DefCompare()); // ��������� ������ ��� (Controlers) � ������� �����������
            if (MPSet != null)
                rootItem.DropDownItems.Add(MPSet); // "�������� ����������" -> ��������� ����� ���� ��-����
            var DefMenuItem = new ToolStripMenuItem("������", null, itemMR, itemRzt, itemMtz); // ��������� � ����� ���� "������" ����� ���� �����������������
            DefMenuItem.DropDownItems.Add(new ToolStripMenuItem("������� ������", null, ArcDefensesList.ToArray())); // "������" -> ��������� ����� ���� "�������" � ���������� ������� ArcDefensesList 
            rootItem.DropDownItems.Add(DefMenuItem); // "�������� ����������" -> ��������� ����� ���� "������" � ���������� ������� DefMenuItem
            rootItem.DropDownItems.Add(new ToolStripMenuItem("���", null, Controlers)); // "�������� ����������" -> ��������� ����� ���� "���" � ���������� ������� Controlers
            rootItem.DropDownItems.Add(new ToolStripMenuItem("����", null, TokDefList.ToArray())); // "�������� ����������" -> ��������� ����� ���� "����" � ���������� ������� TokDefList
            rootItem.DropDownItems.Add(new ToolStripMenuItem("���", null, TcsList.ToArray())); // "�������� ����������" -> ��������� ����� ���� "���" � ���������� ������� ���100 � ���
            rootItem.DropDownItems.AddRange(TSList.ToArray());  // "�������� ����������" -> ��������� ������ TSList
        }

        /// <summary>
        /// ��� ����������� ������ ���� "�������� ����������" ������� ���������� ������ �� ����� ������������.
        /// </summary>
        public void AddDeviceItems(ToolStripMenuItem rootItem, List<Type> deviceTypes)
        {
            try
            {
                rootItem.DropDownItems.Clear();
                this.GetDeviseItems(rootItem, deviceTypes, this.OnAddDeviceItemClick);
            }
            catch (TargetInvocationException e)
            {
                MessageBox.Show(e.InnerException.Message + ". ����������� ������ ���������� ��������� ����������.");
            }
        }

        /// <summary>
        /// �� ������� �� ����� ���� ����������. ������ "���"
        /// </summary>
        public void OnAddDeviceItemClick(object sender, EventArgs e)
        {
            var menuStrip = (ToolStripMenuItem) sender; // ����� ����. ������: "����������������� ���� ��"
            
            if (menuStrip.Text == "����������������� ���� ��")
            {
                // ��� ���� ��
                this.MrClick();
            }
            else
            {
                Device device;

                DeviceAddBegin?.Invoke(new DeviceAddEventArgs(null, menuStrip.Text, true)); // ������� ������ ���������� ���������� � ������
                var parentNode = TreeManager.Tree.Nodes.Count == 0 ? null : TreeManager.Tree.Nodes[0];
                var deviceType = (Type) menuStrip.Tag;

                try
                {
                    device = DeviceManager.CreateDevice(deviceType);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DeviceAddEnd?.Invoke(new DeviceAddEventArgs(null, menuStrip.Text, false)); // ������� ���������� ���������� ���������� � ������
                    return;
                }
                if (null != device)
                {
                    TreeManager.AddDeviceNode(device, parentNode, true);
                    Framework.Log.AddDevice(device);

                    DeviceAddEnd?.Invoke(new DeviceAddEventArgs(device, menuStrip.Text, true)); // ������� ��������� ���������� ���������� � ������
                }
            }
        }

        /// <summary>
        /// ����� ���������� �� ������� ��� �� "����������������� ����"
        /// </summary>
        private void MrClick()
        {
            this.loadVersion = false; // ��������� �������� �� ���������

            DeviceDlgInfo deviceDlgInfo = DeviceManager.ShowDeviceNumberDlg(); // �������� ���������� � ����� ����������� (DeviceConnectionForm)
            if (deviceDlgInfo == null) return; // ���� ������ ������ ������ ������� �� ������

            Device device = DeviceManager.CreateDevice(deviceDlgInfo);         // ������� ������ ��� ������ ������
            device._versionTaskFail += this.OnDeviceVersionLoadFail;           // ���� �� ��������� ��������� 20 ����, ��������� _versionTaskFail � ����������� 10 ����

            // ������ this._device._infoSlot (20 ����, ���� �� ������ - 10 ����)
            device._infoSlot = new Device.slot(0x500, 0x520);

            // ���� ��� ������ ������� ����� ��� ��� ��������� ������
            if (!deviceDlgInfo.IsConnectionMode)
            {
                this.OfflineMode(deviceDlgInfo);
                return;
            }
            
            // ������ ���� � ������� (����������� �� ����������)
            var task = Task.Run(() => device.LoadVersionTask(device));
            Task.WaitAll(task);         

            // ���� �� ������� ��������� ���������� �� ���������� - ������� �����
            if (!device.IsConnect)
            {
                MessageBox.Show("��������� ������������ � ����������!");
                this.OfflineMode(deviceDlgInfo);
                return;
            }
            
            this.GetDeviceVersionAndType(device); // �������������� ������ ������������ ������� �� this._device._infoSlot.Value, �� ������ ����� �������� ��� ������           
        }

        /// <summary>
        /// ����� ���������� ��� ��������� ������ ������ (���������� �� ����������).
        /// </summary>
        private void OnDeviceVersionLoadFail(object sender)
        {
            if (!this.loadVersion)
            {
                this.loadVersion = true; // ��� ������ �� ������, ���� ������� � ���� �����
                var device = (Device) sender;
                device._infoSlot = new Device.slot(0x500, 0x510);
                device.MB.RemoveQuery("version on task" + device.DeviceNumber);
                device.LoadVersionTask(device);
            }
        }

        /// <summary>
        /// ����� �������� ��� ���������� (������) �� �������� �������. � ����� ����� � ������ ����������� � ������� ���������� ������������������ ������ ����.�������. ������ � �������
        /// </summary>
        private void GetDeviceType(Device device, List<Type> deviceTypes)
        {
            Type deviceType = null;
            var menuManager = new MenuManager();

            // ������� deviceType
            switch (device.DeviceType.ToUpper())
            {
                case "MR100":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr100Device"));
                    break;
                case "MR300":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR300"));
                    break;
                case "MR301":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR301"));
                    break;
                case "MR5":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR5Device"));
                    break;
                case "MR500":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR500"));
                    break;
                case "MR550":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr550Device"));
                    break;
                case "MR600":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR600"));
                    break;
                case "MR700":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR700"));
                    break;
                case "MR730":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR730"));
                    break;
                case "MR731":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr731Device"));
                    break;
                case "MR741":
                    try
                    {
                        deviceType = Common.VersionConverter(device.DeviceVersion) >= 3.09 ? deviceTypes.Find(x => x.Name.Contains("MRUniversal")) : deviceTypes.Find(x => x.Name.Contains("TZL"));
                    }
                    catch
                    {
                        deviceType = deviceTypes.Find(x => x.Name.Contains("TZL"));
                    }
                    break;
                case "MR750":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr750Device"));
                    break;
                case "MR761":
                    try
                    {
                        deviceType = Common.VersionConverter(device.DeviceVersion) >= 3.09 ? deviceTypes.Find(x => x.Name.Contains("MRUniversal")) : deviceTypes.Find(x => x.Name.Contains("MR761"));
                    }
                    catch
                    {
                        deviceType = deviceTypes.Find(x => x.Name.Contains("MR761"));
                    }
                    break;
                case "MR761OBR":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr761ObrDevice"));
                    break;
                case "MR762":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr762Device"));
                    break;
                case "MR763":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr763Device"));
                    break;
                case "MR771":
                    deviceType = Common.VersionConverter(device.DeviceVersion) >= 1.14 ? deviceTypes.Find(x => x.Name.Contains("MRUniversal")) : deviceTypes.Find(x => x.Name.Contains("Mr771"));
                    break;
                case "MR801":
                    deviceType = Common.VersionConverter(device.DeviceVersion) >= 3.00 ? deviceTypes.Find(x => x.Name.Contains("Mr7")) : deviceTypes.Find(x => x.Name.Contains("UDZT"));
                    break;                 
                case "MR801DVG":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("MR801dvg"));
                    break;
                case "MR851":
                    deviceType = deviceTypes.Find(x => x.Name.Contains("Mr851Device"));
                    break;
                case "MR901":
                    try
                    {
                        deviceType = Common.VersionConverter(device.DeviceVersion) >= 3.00 ? deviceTypes.Find(x => x.Name.Contains("MR901New")) : deviceTypes.Find(x => x.Name.Contains("Mr901Device"));
                    }
                    catch
                    {
                        deviceType = deviceTypes.Find(x => x.Name.Contains("Mr901Device"));
                    }
                    break;
                case "MR902":
                    try
                    {
                        deviceType = Common.VersionConverter(device.DeviceVersion) >= 3.00 ? deviceTypes.Find(x => x.Name.Contains("Mr902New")) : deviceTypes.Find(x => x.Name.Contains("Mr902"));
                    }
                    catch
                    {
                        deviceType = deviceTypes.Find(x => x.Name.Contains("Mr902"));
                    }
                    break;
                default:
                    MessageBox.Show("��� ���������� �� ���������.");
                    return;
            }

            // ����� ���������� device (��� � �����) � ��������� ��� � ������
            menuManager.AddDeviceInProject(deviceType, device);
        }

        // ����� offlinemode
        private void GetDeviceVersionAndType(Device device)
        {
            device?.InitVersion(); // ��������� DeviceType, DevicePlant � DeviceVersion
            this.GetDeviceType(device, DllLoader.DeviceTypes);
        }

        private void OfflineMode(DeviceDlgInfo deviceDlgInfo)
        {
            ChoiseDeviceNameAndType offlineForm = new ChoiseDeviceNameAndType();    // ������� ������ �����
            offlineForm.ShowDialog();                                               // �������� ������ � �����

            Device device = DeviceManager.CreateDevice(deviceDlgInfo);
            
            device.DeviceNumber = deviceDlgInfo.DeviceNumber;   
            device.Info.Type = offlineForm.DeviceName;
            device.Info.Version = offlineForm.DeviceVersion;
            device.Info.Plant = offlineForm.DeviceConfig;

            device.DeviceDlgInfo.IsConnectionMode = false;

            this.GetDeviceType(device, DllLoader.DeviceTypes); 
        }

        /// <summary>
        /// ��� ���������� ���� ������ OnAddDeviceItemClick, ����� else
        /// </summary>
        public void AddDeviceInProject(Type deviceType, Device oldDevice)
        {
            Device device;

            DeviceAddBegin?.Invoke(new DeviceAddEventArgs(null, oldDevice.DeviceType, true)); // ������� ������ ���������� ���������� � ������
            var parentNode = TreeManager.Tree.Nodes.Count == 0 ? null : TreeManager.Tree.Nodes[0];
            
            try
            {
                device = DeviceManager.CreateDeviceMR(deviceType, oldDevice.DeviceDlgInfo, oldDevice.MB);
                device.Info = oldDevice.Info;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DeviceAddEnd?.Invoke(new DeviceAddEventArgs(null, oldDevice.DeviceType, false)); // ������� ���������� ���������� ���������� � ������
                return;
            }

            // ������� ������
            TreeManager.AddDeviceNode(device, parentNode, true);
            Framework.Log.AddDevice(device);

            DeviceAddEnd?.Invoke(new DeviceAddEventArgs(device, oldDevice.DeviceType, true)); // ������� ��������� ���������� ���������� � ������
            

            // ��������� (������� �������)
            // �� ��� �������� ������������������� �������� MB, DeviceDlgInfo, DeviceInfo ��� �������� � oldDevice, �������� �������� �� ����� ���������� �������
            //try
            //{
            //    // ������� ���������� ������
            //    Device device = DeviceManager.SetDeviceInfo(deviceType, oldDevice.DeviceDlgInfo, oldDevice.Info, oldDevice.MB);

            //    // ������ ������
            //    var parentNode = TreeManager.Tree.Nodes.Count == 0 ? null : TreeManager.Tree.Nodes[0];
            //    TreeManager.AddDeviceNode(device, parentNode, true);

            //    Framework.Log.AddDevice(device);
            //    DeviceAddEnd?.Invoke(new DeviceAddEventArgs(device, device.DeviceType, true)); // ������� ��������� ���������� ���������� � ������  
            //}
            //catch (MissingMethodException)
            //{
            //    MessageBox.Show("���������� �� ����� ���� ���������.", "������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}  
        }


        public static ContextMenuStrip AddDeleteItem(ContextMenuStrip menu)
        {
            if (menu.Items.Count > 0)
            {
                menu.Items.Add(new ToolStripSeparator());
            }
            menu.Items.Add("�������", Properties.Resources.delete, OnDeleteItemClick);
            return menu;
        }

        private static void OnDeleteItemClick(object sender, EventArgs e)
        {
            TreeManager.OnRemoveNode(TreeManager.Tree.SelectedNode);
        }

        /// <summary>
        /// �������� ������������ ���� ����������
        /// </summary>
        public static ContextMenuStrip CreateDeviceNodeMenu(TreeNodeMouseClickEventArgs e)
        {
            var menu = new ContextMenuStrip();

            if (NodeManager.IsDeviceNode(e.Node))
            {
                var currentDevice = (Device)e.Node.Tag;
                menu.Items.Add("������������", new Bitmap(10, 10),
                   (sender, args) =>
                   Framework.MainWindow.InvokeAction(
                       () =>
                       {
                           DeviceManager.ModbusInit();
                           var notShown = new List<byte>(255);
                           byte parentPortNumber = 1;

                           var nodeName = string.Empty;
                           try
                           {
                               nodeName = (currentDevice as INodeView)?.NodeName;
                           }
                           catch (Exception)
                           { }
                           var dlgDevice = new DeviceConnectionForm(nodeName, DeviceManager.PortNumbers, notShown, true, parentPortNumber);
                           var info = dlgDevice.ShowDialog(currentDevice.DeviceDlgInfo);
                           if (!info.IsOkPressed) return;
                           currentDevice.DeviceDlgInfo = info;
                           if (!info.IsConnectionMode)
                           {
                               currentDevice.MB.Dispose();
                               TreeManager.ShowFrom = true;
                               TreeManager.OnDeviceVersionLoadFail(currentDevice);
                               return;
                           }
                           try
                           {
                               currentDevice.MB.Dispose();

                               // RTU
                               if (info.ModbusType == ModbusType.ModbusRtu)
                               {
                                   var port = Modbus.SerialServer.GetNamedPort(info.PortNumber, (uint)Modbus.ProcessID);
                                   currentDevice.MB.Port = port;
                                   currentDevice.MB.SetModbusType(ModbusType.ModbusRtu);
                               }

                               // TCP
                               else if (info.ModbusType == ModbusType.ModbusTcp)
                               {
                                   var connectionForm = new TcpConnectionForm();
                                   connectionForm.SocketConnection(info.Ip, info.TcpPort, info.ReceiveTimeout);

                                   while (!connectionForm.Complite)
                                   {
                                       Application.DoEvents();
                                   }

                                   if (connectionForm.Socket == null)
                                   {
                                       return;
                                   }

                                   currentDevice.MB.SetModbusType(ModbusType.ModbusTcp);
                                   currentDevice.MB.SetNetworkConnection(connectionForm.Socket, info.ModbusType);
                               }

                               // RTU-TCP
                               else
                               {
                                   var connectionForm = new TcpConnectionForm();
                                   connectionForm.SocketConnection(info.Ip, info.TcpPort, info.ReceiveTimeout);

                                   while (!connectionForm.Complite)
                                   {
                                       Application.DoEvents();
                                   }

                                   if (connectionForm.Socket == null)
                                   {
                                       return;
                                   }

                                   currentDevice.MB.SetModbusType(ModbusType.ModbusOnTcp);
                                   currentDevice.MB.SetNetworkConnection(connectionForm.Socket, info.ModbusType);
                               }

                               currentDevice.DeviceNumber = info.DeviceNumber;
                               currentDevice.LoadVersion(currentDevice);                                
                           }
                           catch { }
                       }));

                if (currentDevice.MB.Port.Opened)
                {
                    menu.Items.Add("���������� ����", new Bitmap(10, 10),
                        (sender, args) =>
                            Framework.MainWindow.InvokeAction(
                                () =>
                                {
                                    var node = TreeManager.GetDeviceNode(currentDevice);
                                    TreeManager.FreePort(node);
                                    currentDevice.MB.Dispose();
                                    FormManager.Close(currentDevice);
                                }));
                }

                // ��������� ����� ���� "���������� ���������� �� ����������", ���� ����������� ������� ����
                try
                {
                    if (currentDevice.IsConnect &&
                        currentDevice.DeviceType == "MR761" ||
                        currentDevice.DeviceType == "MR762" ||
                        currentDevice.DeviceType == "MR763" ||
                        currentDevice.DeviceType == "MR771" ||
                       (currentDevice.DeviceType == "MR801" && Common.VersionConverter(currentDevice.DeviceVersion) >= 3.00) ||
                       (currentDevice.DeviceType == "MR901" && Common.VersionConverter(currentDevice.DeviceVersion) >= 3.00) ||
                       (currentDevice.DeviceType == "MR902" && Common.VersionConverter(currentDevice.DeviceVersion) >= 3.00))
                    {
                        menu.Items.Add("���������� ���������� �� ����������", new Bitmap(10, 10),
                            (sender, args) =>
                                Framework.MainWindow.InvokeAction(
                                    () =>
                                    {
                                        var aboutDevice = new AboutDevice(currentDevice);
                                        aboutDevice.ShowDialog();
                                    }));
                    }
                }
                catch (Exception ex)
                {
                }

                if (currentDevice.MB.NetworkEnabled)
                {
                    menu.Items.Add("�����������", new Bitmap(10, 10),
                        (sender, args) =>
                            Framework.MainWindow.InvokeAction(
                                () =>
                                {
                                    currentDevice.MB.Dispose();
                                    FormManager.Close(currentDevice);
                                }));
                }
                menu.Items.Add("������� ������� ����������", new Bitmap(10, 10),
                               (sender, args) =>
                               Framework.MainWindow.InvokeAction(
                                   () =>
                                       {
                                           var captionForm = new ChangeDeviceCaptionForm(currentDevice.Caption);
                                           if (captionForm.ShowDialog(Framework.MainWindow) == DialogResult.OK)
                                           {
                                               currentDevice.Caption = captionForm.Caption;
                                               var node = TreeManager.GetDeviceNode(currentDevice);
                                               TreeManager.ReplaceText(node, currentDevice);
                                           }
                                       }));
                var text = currentDevice.MB.Logging ? "��������� ������������" : "�������� ������������";
                var menuItem = new ToolStripMenuItem(text, null,
                    (sender, args) => Framework.MainWindow.InvokeAction(
                        () =>
                        {
                            currentDevice.MB.Logging = !currentDevice.MB.Logging;
                            if (currentDevice.MB.Logging == false)
                            {
                                Logger.SaveLog();
                            }
                        }));
                menuItem.Checked = currentDevice.MB.Logging;
                menu.Items.Add(menuItem);
            }
            return menu;
        }

        /// <summary>
        /// ����� ����� ��� ���������� ��������� � ������� �����������. 
        /// </summary>
        private class DefCompare : System.Collections.IComparer
        {
            public int Compare(object x, object y)
            {
                if (x.ToString().StartsWith("���") && x.ToString() == y.ToString())
                    return 0;
                if (x.ToString().StartsWith("���") && x.ToString() != y.ToString())
                    return -1;
                if (y.ToString().StartsWith("���") && x.ToString() != y.ToString())
                    return 1;
                return string.CompareOrdinal(x.ToString(), y.ToString());
            }
        }
    }
}
