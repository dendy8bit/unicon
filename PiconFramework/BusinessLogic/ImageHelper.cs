using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;

namespace BEMN.Framework.BusinessLogic
{
    /// <summary>
    /// �����, ���������� ������ ��� �������� �������� � ��������������� ������� � ������� (��� xml)
    /// </summary>
    public class ImageHelper
    {
        public static Image FromString(string imgString)
        {
            byte[] bytes = Convert.FromBase64String(imgString);
            Image ret = Bitmap.FromStream(new MemoryStream(bytes));
            return ret;
        }
        public static string ToString(Image img)
        {
            SerializationInfo si = new SerializationInfo(img.GetType(), new FormatterConverter());
            ((ISerializable)img).GetObjectData(si, new StreamingContext());
            byte[] bytes = (byte[])si.GetValue("Data", typeof(byte[]));
            string ret = Convert.ToBase64String(bytes);
            return ret;
        }
    }
}