using System;
using System.Windows.Forms;

namespace BEMN.Framework.BusinessLogic
{
    public class ActiveNodeChangedEventArgs : EventArgs
    {
        private TreeNode _activeNode;

        public ActiveNodeChangedEventArgs(TreeNode activeNode)
        {
            _activeNode = activeNode;
        }

        public TreeNode ActiveNode
        {
            get
            {
                return _activeNode;
            }
        }
    }
}