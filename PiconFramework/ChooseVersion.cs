﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BEMN.Framework
{
    public partial class ChooseVersion : Form
    {
        private string _vers = string.Empty;
        private string _type = string.Empty;

        public string Vers
        {
            get { return _vers; }
        } 

        public ChooseVersion(string type)
        {
            _type = type;
            InitializeComponent();
        }

        private List<string> _versions;
        private string _deviceName; 
        public ChooseVersion(List<string> versions, string deviceName )
        {
            InitializeComponent();
            this._versions = versions;
            this._deviceName = deviceName;
        }

        public new DialogResult ShowDialog()
        {
          return  Framework.MainWindow.InvokeFunc(() => this.ShowDialog(Framework.MainWindow));
        }

        private void ChooseVersion_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_type))
            {
                this.Text = this._deviceName;
                VersionCB.DataSource = this._versions;
                VersionCB.SelectedIndex = VersionCB.Items.Count - 1;
                return;
            }

            switch (_type) 
            {
                //case "BEMN.TEZ.Tez":
                //    {
                //        this.Text = "TEZ";
                //        VersionCB.Items.AddRange(TEZ.ToArray());
                //        break;
                //    }
                    
            }
            if (VersionCB.Items.Count != 0)
            {
                //VersionCB.SelectedIndex = 0;
                VersionCB.SelectedIndex = VersionCB.Items.Count - 1;
            }
            
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            _vers = VersionCB.SelectedItem.ToString();
        }
    }
}
