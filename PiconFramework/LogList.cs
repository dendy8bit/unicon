using System;
using System.Windows.Forms;
using System.Drawing;
using BEMN.MBServer;
using BEMN.Devices;
using System.Collections.Generic;
using BEMN.MBServer.Queries;

namespace BEMN.Framework
{
    internal delegate void InsertMsgHandler(string msg, int pos);
    internal delegate void AddMsgHandler(string msg);

	public class LogList : ListBox
	{
		private ContextMenuStrip logMenu;
	    readonly List<Device> _devices = new List<Device>();
		private Device  _device;

        private ulong _all;
        private ulong _fail;
        private ulong _ok;

	    public delegate void ShowStatistic();
        public event ShowStatistic ShowNewStatistic;

	    public bool ShowOk { get; set; } = true;
        public bool ShowFail { get; set; } = true;

	    public ulong Ok => this._ok;
        public ulong Fail => this._fail;
        public ulong All => this._all;

        public float OkPercent => _all == 0 ? 0 : (float)_ok / (float)_all * 100.0f;
        public float FailPercent => (float)_fail / (float)_all * 100.0f;

	
		public void AddDevice(Device device)
		{
			_device = device;
            SetModbusHandlers(device);
            if (!_devices.Contains(_device))
            {
                _devices.Add(device);
            }
		}
		
		public LogList()
		{
			InitializeComponent();
			this.MouseDown += this.LogList_MouseDown;
		}
		
		private string MsgTime() => " " + DateTime.Now + " ";

		private void SetModbusHandlers(Device device)
		{
			if (null != device.MB)
			{ 
                device.MB.OkExchange += new OkExchangeHandler(mb_OkExchange);
                device.MB.FailExchange += new FailExchangeHandler(mb_FailExchange);
			}
		}

        void mb_OkExchange(object sender, Query query)
        {
            _all++;
            _ok++;
            try
            {
                BeginInvoke(new InsertMsgHandler(InsertMsg), "������: ��� - " + this._all + ", ���������� - " + this._ok + ", �������� - " + this._fail + " ", 0);
            }
            catch (InvalidOperationException)
            { }
        }
		

		public void InsertMsg(string msg,int pos)
		{
            if (this.Created)
            {
                msg += MsgTime();

                if (pos == Items.Count)
                {
                    Items.Add(msg);
                }
                try
                {
                    Items[pos] = msg;
                }
                catch (ArgumentOutOfRangeException)
                {
                    AddMsg(msg);
                }
            }
		}

		public void AddMsg(string msg)
		{
            if (this.Created)
            {
                msg += MsgTime();

                if (Items.Count > 1)
                {
                    object[] buffer = new object[Items.Count];
                    Items.CopyTo(buffer, 0);
                    Items.Clear();

                    Items.Add(buffer[0]);
                    Items.Add(msg);
                    for (int i = 1; i < buffer.Length; i++)
                    {
                        Items.Add(buffer[i]);
                    }
                }
                else
                {
                    Items.Add(msg);
                }
            }
		}
	
		private void mb_FailExchange(object sender, Query query, string err)
		{
            _all++;
            _fail++;
            if (ShowFail && err != "skip")
            {
                try
                {
                    BeginInvoke(new AddMsgHandler(AddMsg), "O����� ������� " + query.name + ". ������ " + err + " ");
                }
                catch (InvalidOperationException)
                { }                
            }
            try
            {
                BeginInvoke(new InsertMsgHandler(InsertMsg), "������: ��� - " + _all + ", ���������� - " + _ok +", �������� - " + _fail + " ", 0);
            }
            catch (InvalidOperationException)
            { }
		}
	
	
		private void LogList_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				logMenu.Show(this, new Point(e.X,e.Y));
			}
		}

		private void InitializeComponent()
		{
			ToolStripMenuItem clearMenuItem = new ToolStripMenuItem("�������� ���",null, this.ClearLogHandler);
            ToolStripMenuItem clearSttistic = new ToolStripMenuItem("�������� ����������", null, this.ClearStatisticHandler);
			this.logMenu = new ContextMenuStrip();
			logMenu.Items.Add(clearMenuItem);
		    logMenu.Items.Add(clearSttistic);
		}

        private void ClearStatisticHandler(object sender, EventArgs e)
        {
            _device.MB.OkExchange -= new OkExchangeHandler(mb_OkExchange);
            _device.MB.FailExchange -= new FailExchangeHandler(mb_FailExchange);
            
            Items.Clear();
            _ok = 0;
            _fail = 0;
            _all = 0;

            ShowNewStatistic?.Invoke();

            _device.MB.OkExchange += new OkExchangeHandler(mb_OkExchange);
            _device.MB.FailExchange += new FailExchangeHandler(mb_FailExchange);
        }

		private void ClearLogHandler(object sender,EventArgs e)
		{
		    _device.MB.OkExchange -= new OkExchangeHandler(mb_OkExchange);
            _device.MB.FailExchange -= new FailExchangeHandler(mb_FailExchange);
            
            Items.Clear();
		
            _device.MB.OkExchange +=new OkExchangeHandler(mb_OkExchange);
			_device.MB.FailExchange += new FailExchangeHandler(mb_FailExchange);
		}
	}
}
