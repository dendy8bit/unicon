using System;
using System.Windows.Forms;
using System.Drawing;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.Utils;

namespace BEMN.Framework.BusinessLogic
{
	/// <summary>
	/// Summary description for FormManager.
	/// </summary>
	public class FormManager
	{  

        public static Form[] MdiChildren
        {
            get
            {                
                return parentForm.MdiChildren;
            }
        }

        private static Form parentForm;
		
		public static void Close(Device device)
		{
			foreach(Form f in parentForm.MdiChildren)
			{
				if (device ==  f.Tag)
				{                    
					f.Close();
				}
			}
		}

        public static void CloseAll()
		{
          	foreach(Form f in parentForm.MdiChildren)
			{
				f.Close();
			}
            
		}

        public static void MinimizeAll()
        {
            foreach (Form f in parentForm.MdiChildren)
            {
                f.WindowState = FormWindowState.Minimized;
            }
        }

        public static void ShowAll()
        {
            foreach (Form f in parentForm.MdiChildren)
            {
                f.WindowState = FormWindowState.Normal;
            }
        }

        public static void UpdateFormTitle(Device device)
        {
            Type[] formTypes = DllLoader.GetFormTypes(device.GetType());
            for (int i = 0; i < formTypes.Length; i++)
            {
                Form form = GetForm(device, formTypes[i]);
                if (null != form)
                {
                    form.Text = CreateDeviceFormTitle(device, form);    
                }
                
            }
        }

        public static string CreateDeviceFormTitle(Device device, Form form)
        {
            string portString  = 0 == device.MB.Port.PortNum ? "�����������" : device.MB.Port.PortNum.ToString();
            return (device as IDeviceView).NodeName + " �" + device.DeviceNumber + " - ���� " + portString+ ". "+ (form as IFormView).NodeName;
        }

        public static Form Activate(Device device, System.Type formType)
		{
            Form deviceForm = null;

            if (device.DeviceVersion == string.Empty && device.HaveVersion) return deviceForm;
            deviceForm = GetForm(device, formType);
            TreeManager.IsTreeChanged = true;
            if (null == deviceForm || (deviceForm as IFormView).Multishow)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    deviceForm = Activator.CreateInstance(formType, new object[] { device }) as Form;
                    deviceForm.Text = CreateDeviceFormTitle(device, deviceForm);
                    deviceForm.Icon = Icon.FromHandle(new Bitmap((deviceForm as IFormView).NodeImage).GetHicon());
                    deviceForm.MdiParent = parentForm;
                    deviceForm.Tag = device;
                    Cursor.Current = Cursors.Default;
                    deviceForm.Show();
                }
                catch (Exception e)
                {
                    if (deviceForm != null)
                    {
                        deviceForm.Close();
                    }
                }
            }
                
            try
            {
                if (deviceForm != null)
                {
                    deviceForm.Activate();
                }
            }
            catch { }
            return deviceForm;
		}
        
        public static void Add(Device device, Type formType, Point location, Size size)
        {
            try
            {
                Form deviceForm = null;
                deviceForm = Activator.CreateInstance(formType, new object[] {device}) as Form;
                deviceForm.Text = CreateDeviceFormTitle(device, deviceForm);
                deviceForm.Icon = Icon.FromHandle(new Bitmap((deviceForm as IFormView).NodeImage).GetHicon());
                deviceForm.Tag = device;
                deviceForm.Size = size;
                deviceForm.MdiParent = parentForm;
                deviceForm.Show();
                deviceForm.Location = location;
            }
            catch
            {
                
            }
        }

        public static Device Copy(Device device, System.Type formType)
        { 
            Form deviceForm = new Form();
            MBServer.Modbus mb = new MBServer.Modbus(device.MB.Port);
            Device curDevice = Activator.CreateInstance(device.GetType(), new object[] {mb}) as Device;
            curDevice.DeviceNumber = device.DeviceNumber;
            try
            {
                deviceForm = Activator.CreateInstance(formType, new object[] {curDevice}) as Form;
            }
            catch (Exception)
            {
            }
            deviceForm.Text = CreateDeviceFormTitle(curDevice, deviceForm);
            deviceForm.Icon = Icon.FromHandle(new Bitmap((deviceForm as IFormView).NodeImage).GetHicon());
            deviceForm.MdiParent = parentForm;
            deviceForm.Tag = device;
            deviceForm.Show();
            deviceForm.Activate();
            
            return curDevice;
          }

        public static Form GetForm(Type formType)
        {
            Form ret = null;

            foreach (Form f in parentForm.MdiChildren)
            {
                if (formType == f.GetType())
                {
                   ret = f;
                }
            }
            return ret;
        }

        public static  Form GetForm(Device device, System.Type formType) 
		{
			Form ret = null;

			foreach(Form f in parentForm.MdiChildren)
			{
				if (formType == f.GetType())
				{
					if (device == f.Tag)
					{
						ret = f;
					}
				}
			}
			return ret;
		}
                                
        public static Form ParentForm
        {
            set
            {
                parentForm = value;
            }
            get
            {
                return parentForm;
            }
        }
	}
}
