﻿using System;
using System.Windows.Forms;

namespace BEMN.Framework
{
    public partial class ChangeDeviceCaptionForm : Form
    {
        public ChangeDeviceCaptionForm()
        {
            InitializeComponent();
        }

        public string Caption { get; private set; }

        public ChangeDeviceCaptionForm(string caption)
        {
            InitializeComponent();
            if (caption == null)
            {
                caption = string.Empty;
            }
            this._captionTextBox.Text = caption;
        }

        private void _okButton_Click(object sender, EventArgs e)
        {
            this.Caption = this._captionTextBox.Text;
            DialogResult = DialogResult.OK;
        }

        private void _cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
