﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace BEMN.Framework
{
    public partial class About : Form
    {
        
        public About()
        {
            InitializeComponent();
    
            this.StartPosition = FormStartPosition.CenterScreen;
          /*  about_box.Lines = new[]
                {
                    "Имя программы: УниКон",
                    string.Format("Версия: {0}", Assembly.GetExecutingAssembly().GetName().Version),
                    "Дата: " + DateTime.Now,
                    "Производитель: ОАО «Белэлектромонтажналадка»",
                    "220101, г. Минск, ул. Плеханова, 105А",
                    "тел./факс СКБ              248 91 92, 248 88 57",
                    "тел./факс Приемная    249 99 05, 249 43 19",
                    "сайт: bemn.by",
                    "e-mail: skb@bemn.by"
                };*/


            this._aboutLabel.Text = "Имя программы: УниКон" + Environment.NewLine +
                          string.Format("Версия: {0}", Assembly.GetExecutingAssembly().GetName().Version) +
                          Environment.NewLine +
                          "Дата: " + RetrieveLinkerTimestamp() /*DateTime.Now*/ + Environment.NewLine +
                          "Производитель: ОАО «Белэлектромонтажналадка»" + Environment.NewLine +
                          "220101, г. Минск, ул. Плеханова, 105А" + Environment.NewLine +
                          "СКБ: тел/факс +375 (17) 368-88-57, телефон +375 (17) 368-81-92 " + Environment.NewLine +
                          "Приемная: факс +375 (17) 367-43-19, телефон +375 (17) 368-09-05" + Environment.NewLine +
                          "сайт: bemn.by" + Environment.NewLine +
                          "e-mail: upr@bemn.by";


            


        }
        public static DateTime RetrieveLinkerTimestamp()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];
            System.IO.Stream s = null;

            try
            {
                s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                s.Read(b, 0, 2048);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }

            int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddSeconds(secondsSince1970);
            dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
            return dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("www.bemn.by");
            }
            catch (Exception)
            {

            }
        }

    }
      
}