using System;
using System.Drawing;
using System.Windows.Forms;

namespace BEMN.Framework
{
    public partial class WaitingForm : Form
    {        
        public WaitingForm()
        {
            InitializeComponent();
        }
        private void WaitingForm_Paint(object sender, PaintEventArgs e)
        {

        }
	
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WaitingForm));
            Graphics gfx = e.Graphics;
        }  
 
        /// <summary>
        /// label msg �� ����� WaitingForm
        /// </summary>
        public string Message
        {
            get { return msg.Text; }
            set {
                    msg.Text = value;
                    Refresh(); 
                }
        }

        public ProgressBar Progress
        {
            get
            {
                return progressBar;
            }
        }

        private void WaitingForm_Load(object sender, EventArgs e)
        {

        }
    }
}