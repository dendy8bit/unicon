using System;
using System.Windows.Forms;
using System.Drawing;

using BEMN.Devices;
using BEMN.Interfaces;
using Crownwood.Magic.Docking;

namespace BEMN.Framework.BusinessLogic
{
    public class PropertyManager
    {
        DockingManager _styleManager;
        Content _currentContent;
        PropertyGrid _grid;
        Object _showObject;

        public PropertyGrid Grid
        {
            get
            {
                return _grid;
            }
        }

        public int panelHeight = 600;

        public PropertyManager(DockingManager styleManager)
        {
            Image image = Properties.Resources.properties;
            _styleManager = styleManager;
            _currentContent = _styleManager.Contents.Add(CreateDevicePanel(), "C�������");
            _styleManager.AutoResize = true;
            _currentContent.ImageList = new ImageList();
            _currentContent.ImageList.Images.Add(image, Color.White);
            _currentContent.ImageIndex = 0;
            _currentContent.CloseButton = _currentContent.HideButton = _currentContent.CaptionBar = true;
            _currentContent.AutoHideSize = _currentContent.DisplaySize = new Size(280, panelHeight);


            _styleManager.AddContentWithState(_currentContent, State.DockRight);
            _styleManager.HideContent(_currentContent);


        }

        public void ShowObject(object showObject)
        {
            _showObject = showObject;
            if (showObject is IDeviceLog)
            {
                (showObject as IDeviceLog).PropertiesLoadOk += new LoadHandler(PropertyManager_LoadPropertiesOk);
                (showObject as IDeviceLog).PropertiesLoadFail += new LoadHandler(PropertyManager_LoadPropertiesOk);
                (showObject as IDeviceLog).LoadProperties();
            }
            else
            {
                ShowObject();
            }

        }

        void PropertyManager_LoadPropertiesOk(object sender)
        {
            _grid.Invoke(new OnDeviceEventHandler(ShowObject));
        }

        private void ShowObject()
        {
            _currentContent = _styleManager.Contents["C�������"];
            _currentContent.AutoHideSize = new Size(280, panelHeight);
            _currentContent.DisplaySize = new Size(280, panelHeight);
            _grid.SelectedObject = _showObject;
            _styleManager.ShowContent(_currentContent);
        }

        private Panel CreateDevicePanel()
        {
            Panel panel = new Panel();
            _grid = new PropertyGrid();
            _grid.Size = panel.Size = new Size(275, (int)(panelHeight * 1.5 - 20));
            _grid.Dock = DockStyle.Bottom;

            GroupBox group = new GroupBox();
            group.Width = _grid.Width;
            group.Height = 40;
            group.Dock = DockStyle.Top;
            group.Location = new Point(0, 0);
            Button readButton = new Button();
            Button writeButton = new Button();
            writeButton.Location = new Point(100, 10);
            readButton.Location = new Point(10, 10);
            writeButton.Size = readButton.Size = new Size(80, 25);
            readButton.Text = "���������";
            writeButton.Text = "��������";
            readButton.Click += new EventHandler(readButton_Click);
            writeButton.Click += new EventHandler(writeButton_Click);
            group.Controls.Add(readButton);
            group.Controls.Add(writeButton);
            _grid.Location = new Point(0, group.Height + 10);

            panel.Controls.Add(group);
            panel.Controls.Add(_grid);
            return panel;

        }

        void writeButton_Click(object sender, EventArgs e)
        {
            if (null != _showObject && _showObject is IDeviceLog)
            {
                (_showObject as IDeviceLog).SaveProperties();
            }
        }

        void readButton_Click(object sender, EventArgs e)
        {
            if (null != _showObject && _showObject is IDeviceLog)
            {
                (_showObject as IDeviceLog).LoadProperties();
            }
        }







    }
}
