﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Crownwood.Magic.Docking;

namespace BEMN.Framework
{
    public partial class Framework : Form
    {
        #region UI Fields

        private IContainer components;
        private ProjectForm projectForm;

        private static LogList logList;
        private ImageList barImages;
        private ContextMenuStrip _addDeviceContextMenu;
        private OpenFileDialog openProjectDialog;
        private SaveFileDialog saveProjectFileDialog;
        private DockingManager _styleManager;
      
        private ToolStripMenuItem toolStripMenuItem1;
        private StatusStrip _statusPanel;
        private MenuStrip _mainMenu;
        private ToolStripMenuItem _fileMenu;
        private ToolStripMenuItem _newFileItem;
        private ToolStripMenuItem _openFileItem;
        private ToolStripMenuItem _saveFileItem;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem _addDeviceItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem _exitItem;
        private ToolStripMenuItem _wndMenuItem;
        private ToolStripMenuItem _cascadMenuItem;
        private ToolStripMenuItem _verticalMenuItem;
        private ToolStripMenuItem _horizontMenuItem;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem _showAllMenuItem;
        private ToolStripMenuItem _hideAllMenuItem;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem _closeDocMenuItem;
        private ToolStripMenuItem _minimizeDocMenuItem;
        private ToolStripMenuItem _showDocMenuItem;
        private ToolStripMenuItem _settingsMenuItem;
        private ToolStripMenuItem _msgFilterMenuItem;
        private ToolStripMenuItem _normalExchangeMenuItem;
        private ToolStripMenuItem _errorExchangeMenuItem;
        private ToolStripMenuItem _referenceMenuItem;
        private ToolStripMenuItem _aboutMenuItem;
        private ToolStrip _mainToolStrip;
        private ToolStripButton _openToolStripButton;
        private ToolStripButton _saveToolStripButton;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripDropDownButton _addDeviceToolStripButton;
        private ToolStripMenuItem _newProjectItem;
        private ToolStripButton _saveAllToolStripButton;
        private ToolStripProgressBar _statusProgress;
        private ToolStripStatusLabel _statusLabel;
        private string _currentProject;
        private ToolStripPanel BottomToolStripPanel;
        private ToolStripPanel TopToolStripPanel;
        private ToolStripPanel RightToolStripPanel;
        private ToolStripPanel LeftToolStripPanel;
        private ToolStripContentPanel ContentPanel;
        private ToolStripMenuItem _autosaveMenuItem;

        #endregion

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Framework));
            this.barImages = new System.Windows.Forms.ImageList(this.components);
            this._addDeviceContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openProjectDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveProjectFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this._statusPanel = new System.Windows.Forms.StatusStrip();
            this._statusProgress = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._mainMenu = new System.Windows.Forms.MenuStrip();
            this._fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._newFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._newProjectItem = new System.Windows.Forms.ToolStripMenuItem();
            this._openFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._saveFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this._addDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._exitItem = new System.Windows.Forms.ToolStripMenuItem();
            this._wndMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._cascadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._verticalMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._horizontMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._showAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._hideAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this._closeDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._minimizeDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._showDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._settingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._msgFilterMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._normalExchangeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._errorExchangeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._readSignaturesSignalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._autoLoadSetpointsMI = new System.Windows.Forms.ToolStripMenuItem();
            this._autosaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this._portsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._referenceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._mainToolStrip = new System.Windows.Forms.ToolStrip();
            this._newFileStrpiBut = new System.Windows.Forms.ToolStripDropDownButton();
            this._newPorjectStripBut = new System.Windows.Forms.ToolStripMenuItem();
            this._newSourceStripBut = new System.Windows.Forms.ToolStripMenuItem();
            this._openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._saveAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this._toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this._addDeviceToolStripButton = new System.Windows.Forms.ToolStripDropDownButton();
            this._statusPanel.SuspendLayout();
            this._mainMenu.SuspendLayout();
            this._mainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // barImages
            // 
            this.barImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("barImages.ImageStream")));
            this.barImages.TransparentColor = System.Drawing.Color.Transparent;
            this.barImages.Images.SetKeyName(0, "");
            this.barImages.Images.SetKeyName(1, "");
            this.barImages.Images.SetKeyName(2, "");
            this.barImages.Images.SetKeyName(3, "");
            this.barImages.Images.SetKeyName(4, "");
            // 
            // _addDeviceContextMenu
            // 
            this._addDeviceContextMenu.Name = "menuContextAddDevice";
            this._addDeviceContextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // openProjectDialog
            // 
            this.openProjectDialog.CheckPathExists = false;
            this.openProjectDialog.DefaultExt = "uprj";
            this.openProjectDialog.Filter = "Уникон-проект файлы| *.uprj";
            this.openProjectDialog.RestoreDirectory = true;
            this.openProjectDialog.Title = "Открыть файл проекта";
            // 
            // saveProjectFileDialog
            // 
            this.saveProjectFileDialog.CheckPathExists = false;
            this.saveProjectFileDialog.CreatePrompt = true;
            this.saveProjectFileDialog.DefaultExt = "uprj";
            this.saveProjectFileDialog.Filter = "Уникон-проект файлы| *.uprj";
            this.saveProjectFileDialog.RestoreDirectory = true;
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(150, 175);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(78, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // _statusPanel
            // 
            this._statusPanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusProgress,
            this._statusLabel});
            this._statusPanel.Location = new System.Drawing.Point(0, 644);
            this._statusPanel.Name = "_statusPanel";
            this._statusPanel.Size = new System.Drawing.Size(948, 22);
            this._statusPanel.TabIndex = 9;
            this._statusPanel.Text = "statusStrip1";
            // 
            // _statusProgress
            // 
            this._statusProgress.Name = "_statusProgress";
            this._statusProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _mainMenu
            // 
            this._mainMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this._mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._fileMenu,
            this._wndMenuItem,
            this._settingsMenuItem,
            this._referenceMenuItem});
            this._mainMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this._mainMenu.Location = new System.Drawing.Point(0, 0);
            this._mainMenu.MdiWindowListItem = this._wndMenuItem;
            this._mainMenu.Name = "_mainMenu";
            this._mainMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._mainMenu.Size = new System.Drawing.Size(948, 23);
            this._mainMenu.TabIndex = 0;
            // 
            // _fileMenu
            // 
            this._fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._newFileItem,
            this._openFileItem,
            this._saveFileItem,
            this.toolStripSeparator2,
            this._addDeviceItem,
            this.toolStripSeparator1,
            this._exitItem});
            this._fileMenu.Name = "_fileMenu";
            this._fileMenu.Size = new System.Drawing.Size(48, 19);
            this._fileMenu.Text = "&Файл";
            // 
            // _newFileItem
            // 
            this._newFileItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._newProjectItem});
            this._newFileItem.Image = ((System.Drawing.Image)(resources.GetObject("_newFileItem.Image")));
            this._newFileItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._newFileItem.Name = "_newFileItem";
            this._newFileItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._newFileItem.Size = new System.Drawing.Size(191, 22);
            this._newFileItem.Text = "&Новый ";
            // 
            // _newProjectItem
            // 
            this._newProjectItem.Image = global::BEMN.Framework.Properties.Resources.newproject;
            this._newProjectItem.Name = "_newProjectItem";
            this._newProjectItem.Size = new System.Drawing.Size(114, 22);
            this._newProjectItem.Text = "Проект";
            this._newProjectItem.Click += new System.EventHandler(this._newFileItem_Click);
            // 
            // _openFileItem
            // 
            this._openFileItem.Image = ((System.Drawing.Image)(resources.GetObject("_openFileItem.Image")));
            this._openFileItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._openFileItem.Name = "_openFileItem";
            this._openFileItem.Size = new System.Drawing.Size(191, 22);
            this._openFileItem.Text = "&Открыть проект";
            this._openFileItem.Click += new System.EventHandler(this._openFileItem_Click);
            // 
            // _saveFileItem
            // 
            this._saveFileItem.Image = ((System.Drawing.Image)(resources.GetObject("_saveFileItem.Image")));
            this._saveFileItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._saveFileItem.Name = "_saveFileItem";
            this._saveFileItem.Size = new System.Drawing.Size(191, 22);
            this._saveFileItem.Text = "&Сохранить";
            this._saveFileItem.Click += new System.EventHandler(this._saveItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(188, 6);
            // 
            // _addDeviceItem
            // 
            this._addDeviceItem.Image = ((System.Drawing.Image)(resources.GetObject("_addDeviceItem.Image")));
            this._addDeviceItem.Name = "_addDeviceItem";
            this._addDeviceItem.Size = new System.Drawing.Size(191, 22);
            this._addDeviceItem.Text = "Добавить устройство";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(188, 6);
            // 
            // _exitItem
            // 
            this._exitItem.Name = "_exitItem";
            this._exitItem.Size = new System.Drawing.Size(191, 22);
            this._exitItem.Text = "Вы&ход";
            this._exitItem.Click += new System.EventHandler(this._exitMenuItem_Click);
            // 
            // _wndMenuItem
            // 
            this._wndMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cascadMenuItem,
            this._verticalMenuItem,
            this._horizontMenuItem,
            this.toolStripSeparator3,
            this._showAllMenuItem,
            this._hideAllMenuItem,
            this.toolStripSeparator4,
            this._closeDocMenuItem,
            this._minimizeDocMenuItem,
            this._showDocMenuItem});
            this._wndMenuItem.Name = "_wndMenuItem";
            this._wndMenuItem.Size = new System.Drawing.Size(47, 19);
            this._wndMenuItem.Text = "Окна";
            // 
            // _cascadMenuItem
            // 
            this._cascadMenuItem.Name = "_cascadMenuItem";
            this._cascadMenuItem.Size = new System.Drawing.Size(189, 22);
            this._cascadMenuItem.Text = "Каскадом";
            this._cascadMenuItem.Click += new System.EventHandler(this._cascadMenuItem_Click);
            // 
            // _verticalMenuItem
            // 
            this._verticalMenuItem.Name = "_verticalMenuItem";
            this._verticalMenuItem.Size = new System.Drawing.Size(189, 22);
            this._verticalMenuItem.Text = "Вертикально";
            this._verticalMenuItem.Click += new System.EventHandler(this._verticalMenuItem_Click);
            // 
            // _horizontMenuItem
            // 
            this._horizontMenuItem.Name = "_horizontMenuItem";
            this._horizontMenuItem.Size = new System.Drawing.Size(189, 22);
            this._horizontMenuItem.Text = "Горизонтально";
            this._horizontMenuItem.Click += new System.EventHandler(this._horizontMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(186, 6);
            // 
            // _showAllMenuItem
            // 
            this._showAllMenuItem.Name = "_showAllMenuItem";
            this._showAllMenuItem.Size = new System.Drawing.Size(189, 22);
            this._showAllMenuItem.Text = "Показать панели";
            this._showAllMenuItem.Click += new System.EventHandler(this._showAllMenuItem_Click);
            // 
            // _hideAllMenuItem
            // 
            this._hideAllMenuItem.Name = "_hideAllMenuItem";
            this._hideAllMenuItem.Size = new System.Drawing.Size(189, 22);
            this._hideAllMenuItem.Text = "Спрятать панели";
            this._hideAllMenuItem.Click += new System.EventHandler(this._hideAllMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(186, 6);
            // 
            // _closeDocMenuItem
            // 
            this._closeDocMenuItem.Name = "_closeDocMenuItem";
            this._closeDocMenuItem.Size = new System.Drawing.Size(189, 22);
            this._closeDocMenuItem.Text = "Закрыть документы";
            this._closeDocMenuItem.Click += new System.EventHandler(this._closeDocMenuItem_Click);
            // 
            // _minimizeDocMenuItem
            // 
            this._minimizeDocMenuItem.Name = "_minimizeDocMenuItem";
            this._minimizeDocMenuItem.Size = new System.Drawing.Size(189, 22);
            this._minimizeDocMenuItem.Text = "Свернуть документы";
            this._minimizeDocMenuItem.Click += new System.EventHandler(this._minimizeDocMenuItem_Click);
            // 
            // _showDocMenuItem
            // 
            this._showDocMenuItem.Name = "_showDocMenuItem";
            this._showDocMenuItem.Size = new System.Drawing.Size(189, 22);
            this._showDocMenuItem.Text = "Показать документы";
            this._showDocMenuItem.Click += new System.EventHandler(this._showDocMenuItem_Click);
            // 
            // _settingsMenuItem
            // 
            this._settingsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msgFilterMenuItem,
            this._readSignaturesSignalsToolStripMenuItem,
            this._autoLoadSetpointsMI,
            this._autosaveMenuItem,
            this._toolStripMenuItem2,
            this._portsToolStripMenuItem});
            this._settingsMenuItem.Name = "_settingsMenuItem";
            this._settingsMenuItem.Size = new System.Drawing.Size(79, 19);
            this._settingsMenuItem.Text = "Настройки";
            // 
            // _msgFilterMenuItem
            // 
            this._msgFilterMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._normalExchangeMenuItem,
            this._errorExchangeMenuItem});
            this._msgFilterMenuItem.Name = "_msgFilterMenuItem";
            this._msgFilterMenuItem.Size = new System.Drawing.Size(350, 22);
            this._msgFilterMenuItem.Text = "Фильтр сообщений";
            // 
            // _normalExchangeMenuItem
            // 
            this._normalExchangeMenuItem.Checked = true;
            this._normalExchangeMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._normalExchangeMenuItem.Name = "_normalExchangeMenuItem";
            this._normalExchangeMenuItem.Size = new System.Drawing.Size(195, 22);
            this._normalExchangeMenuItem.Text = "Нормальные обмены";
            this._normalExchangeMenuItem.Click += new System.EventHandler(this._normalExchangeMenuItem_Click);
            // 
            // _errorExchangeMenuItem
            // 
            this._errorExchangeMenuItem.Checked = true;
            this._errorExchangeMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._errorExchangeMenuItem.Name = "_errorExchangeMenuItem";
            this._errorExchangeMenuItem.Size = new System.Drawing.Size(195, 22);
            this._errorExchangeMenuItem.Text = "Ошибочные обмены";
            this._errorExchangeMenuItem.Click += new System.EventHandler(this._errorExchangeMenuItem_Click);
            // 
            // _readSignaturesSignalsToolStripMenuItem
            // 
            this._readSignaturesSignalsToolStripMenuItem.Checked = true;
            this._readSignaturesSignalsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._readSignaturesSignalsToolStripMenuItem.Name = "_readSignaturesSignalsToolStripMenuItem";
            this._readSignaturesSignalsToolStripMenuItem.Size = new System.Drawing.Size(350, 22);
            this._readSignaturesSignalsToolStripMenuItem.Text = "Записывать подписи сигналов";
            this._readSignaturesSignalsToolStripMenuItem.Click += new System.EventHandler(this._readForReadSignaturesSignalsToolStripMenuItem_Click);
            // 
            // _autoLoadSetpointsMI
            // 
            this._autoLoadSetpointsMI.Checked = true;
            this._autoLoadSetpointsMI.CheckState = System.Windows.Forms.CheckState.Checked;
            this._autoLoadSetpointsMI.Name = "_autoLoadSetpointsMI";
            this._autoLoadSetpointsMI.Size = new System.Drawing.Size(350, 22);
            this._autoLoadSetpointsMI.Text = "Загружать уставки автоматически";
            this._autoLoadSetpointsMI.Click += new System.EventHandler(this._autoLoadSetpointsMI_Click);
            // 
            // _autosaveMenuItem
            // 
            this._autosaveMenuItem.Checked = true;
            this._autosaveMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._autosaveMenuItem.Name = "_autosaveMenuItem";
            this._autosaveMenuItem.Size = new System.Drawing.Size(350, 22);
            this._autosaveMenuItem.Text = "Предлагать сохранение проекта перед закрытием";
            this._autosaveMenuItem.Click += new System.EventHandler(this._autosaveMenuItem_Click);
            // 
            // _toolStripMenuItem2
            // 
            this._toolStripMenuItem2.Name = "_toolStripMenuItem2";
            this._toolStripMenuItem2.Size = new System.Drawing.Size(347, 6);
            // 
            // _portsToolStripMenuItem
            // 
            this._portsToolStripMenuItem.Name = "_portsToolStripMenuItem";
            this._portsToolStripMenuItem.Size = new System.Drawing.Size(350, 22);
            this._portsToolStripMenuItem.Text = "Порты";
            this._portsToolStripMenuItem.Click += new System.EventHandler(this.OnPortsToolStripButtonClick);
            // 
            // _referenceMenuItem
            // 
            this._referenceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._aboutMenuItem});
            this._referenceMenuItem.Name = "_referenceMenuItem";
            this._referenceMenuItem.Size = new System.Drawing.Size(65, 19);
            this._referenceMenuItem.Text = "&Справка";
            // 
            // _aboutMenuItem
            // 
            this._aboutMenuItem.Name = "_aboutMenuItem";
            this._aboutMenuItem.ShortcutKeyDisplayString = "F1";
            this._aboutMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._aboutMenuItem.Size = new System.Drawing.Size(177, 22);
            this._aboutMenuItem.Text = "&О программе...";
            this._aboutMenuItem.Click += new System.EventHandler(this._aboutMenuItem_Click);
            // 
            // _mainToolStrip
            // 
            this._mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._newFileStrpiBut,
            this._openToolStripButton,
            this._saveToolStripButton,
            this._saveAllToolStripButton,
            this.toolStripSeparator6,
            this.toolStripButton1,
            this._toolStripSeparator10,
            this._addDeviceToolStripButton});
            this._mainToolStrip.Location = new System.Drawing.Point(0, 23);
            this._mainToolStrip.Name = "_mainToolStrip";
            this._mainToolStrip.Size = new System.Drawing.Size(948, 25);
            this._mainToolStrip.TabIndex = 7;
            this._mainToolStrip.Text = "toolStrip1";
            // 
            // _newFileStrpiBut
            // 
            this._newFileStrpiBut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._newFileStrpiBut.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._newPorjectStripBut,
            this._newSourceStripBut});
            this._newFileStrpiBut.Image = ((System.Drawing.Image)(resources.GetObject("_newFileStrpiBut.Image")));
            this._newFileStrpiBut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._newFileStrpiBut.Name = "_newFileStrpiBut";
            this._newFileStrpiBut.Size = new System.Drawing.Size(29, 22);
            this._newFileStrpiBut.Text = "&Новый...";
            // 
            // _newPorjectStripBut
            // 
            this._newPorjectStripBut.Image = global::BEMN.Framework.Properties.Resources.newproject;
            this._newPorjectStripBut.Name = "_newPorjectStripBut";
            this._newPorjectStripBut.Size = new System.Drawing.Size(139, 22);
            this._newPorjectStripBut.Text = "Проект ";
            this._newPorjectStripBut.Click += new System.EventHandler(this._newFileItem_Click);
            // 
            // _newSourceStripBut
            // 
            this._newSourceStripBut.Image = global::BEMN.Framework.Properties.Resources.newsource;
            this._newSourceStripBut.Name = "_newSourceStripBut";
            this._newSourceStripBut.Size = new System.Drawing.Size(139, 22);
            this._newSourceStripBut.Text = "Программа";
            // 
            // _openToolStripButton
            // 
            this._openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_openToolStripButton.Image")));
            this._openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._openToolStripButton.Name = "_openToolStripButton";
            this._openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._openToolStripButton.Text = "&Открыть проект";
            this._openToolStripButton.Click += new System.EventHandler(this._openFileItem_Click);
            // 
            // _saveToolStripButton
            // 
            this._saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_saveToolStripButton.Image")));
            this._saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._saveToolStripButton.Name = "_saveToolStripButton";
            this._saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._saveToolStripButton.Text = "&Сохранить проект";
            this._saveToolStripButton.Click += new System.EventHandler(this._saveItem_Click);
            // 
            // _saveAllToolStripButton
            // 
            this._saveAllToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._saveAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_saveAllToolStripButton.Image")));
            this._saveAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._saveAllToolStripButton.Name = "_saveAllToolStripButton";
            this._saveAllToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._saveAllToolStripButton.Text = "Сохранить все";
            this._saveAllToolStripButton.Click += new System.EventHandler(this._saveAllToolStripButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Синхронизация времени";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // _toolStripSeparator10
            // 
            this._toolStripSeparator10.Name = "_toolStripSeparator10";
            this._toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // _addDeviceToolStripButton
            // 
            this._addDeviceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._addDeviceToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_addDeviceToolStripButton.Image")));
            this._addDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._addDeviceToolStripButton.Name = "_addDeviceToolStripButton";
            this._addDeviceToolStripButton.Size = new System.Drawing.Size(29, 22);
            this._addDeviceToolStripButton.Text = "Добавить устройство";
            // 
            // Framework
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(948, 666);
            this.Controls.Add(this._statusPanel);
            this.Controls.Add(this._mainToolStrip);
            this.Controls.Add(this._mainMenu);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this._mainMenu;
            this.Name = "Framework";
            this.Text = "УниКон";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Framework_FormClosing);
            this.Load += new System.EventHandler(this.Framework_Load);
            this.Click += new System.EventHandler(this._openFileItem_Click);
            this._statusPanel.ResumeLayout(false);
            this._statusPanel.PerformLayout();
            this._mainMenu.ResumeLayout(false);
            this._mainMenu.PerformLayout();
            this._mainToolStrip.ResumeLayout(false);
            this._mainToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ToolStripButton toolStripButton1;
        private ToolStripMenuItem _autoLoadSetpointsMI;
        private ToolStripMenuItem _readSignaturesSignalsToolStripMenuItem;
    }
}
