using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.Xml;

using System.Windows.Forms;

using BEMN.Devices;
using BEMN.Modules;
using BEMN.Interfaces;

namespace BEMN.Framework.BusinessLogic
{
	/// <summary>
	/// 
	/// </summary>
	public  class DllLoader
	{
		private static ArrayList _deviceTypes = new ArrayList(); 
		private static ArrayList _formTypes   = new ArrayList();
		private static ArrayList _moduleTypes = new ArrayList();
        private static List<Type> _nodeTypes = new List<Type>();
        private static List<Image> _nodeImages = new List<Image>();

        //private static Dictionary<Type, Image> _nodes = new Dictionary<Type,Image>();

        
        public static  List<Type> NodeTypes
        {
            get
            {
                return _nodeTypes;
            }
        }
        public static Type[] DeviceTypes
		{
			get
			{
				Type[] ret = new Type[_deviceTypes.Count];
				 _deviceTypes.ToArray(typeof(Type)).CopyTo(ret,0);
				
				return ret;
			}
		}
        public static Type[] FormTypes
		{
			get
			{
				Type[] ret = new Type[_formTypes.Count];
				_formTypes.ToArray(typeof(Type)).CopyTo(ret,0);
				
				return ret;
			}
		}

      
        public static Image[] FormImages
		{
			get
			{
				Image[] ret = new Image[FormTypes.Length];

				for (int i = 0; i < FormTypes.Length;i++ )
				{
                   ret[i] = (Activator.CreateInstance(FormTypes[i]) as IFormView).NodeImage;
				}
						
				return ret;
			}
		}

        public static Image[] NodeImages
        {
            get
            {
                Image[] ret = new Image[NodeTypes.Count];

                for (int i = 0; i < NodeTypes.Count; i++)
                {
                    ret[i] = (Activator.CreateInstance(NodeTypes[i]) as INodeView).NodeImage;
                }

                return ret;
            }
        }

        public static  Image[] ModuleImages
        {
            get
            {
                Image[] ret = new Image[ModuleTypes.Length];

                for (int i = 0; i < ModuleTypes.Length; i++)
                {
                    ret[i] = (Activator.CreateInstance(ModuleTypes[i]) as IDeviceView).NodeImage;
                }

                return ret;
            }
        }

		public static  string[] FormNames
		{
			get
			{
				string[] ret = new string[FormTypes.Length];

				for (int i = 0; i < FormTypes.Length;i++ )
				{
					ret[i] = (Activator.CreateInstance(FormTypes[i]) as IFormView).NodeName;
				}
						
				return ret;
			}
		}
        public static string[] DeviceNames
		{
			get
			{
				string[] ret = new string[DeviceTypes.Length];

				for (int i = 0; i < DeviceTypes.Length;i++ )
				{
					ret[i] = (Activator.CreateInstance(DeviceTypes[i]) as IDeviceView).NodeName;
				}
						
				return ret;
			}
		}

        public static Image[] DeviceImages
		{
			get
			{
				Image[] ret = new Image[DeviceTypes.Length];

				for (int i = 0; i < DeviceTypes.Length;i++ )
				{
					ret[i] = (Activator.CreateInstance(DeviceTypes[i]) as IDeviceView).NodeImage;
				}
						
				return ret;
			}
		}

        public static Type[] ModuleTypes
		{
			get
			{
				Type[] ret = new Type[_moduleTypes.Count];
				_moduleTypes.ToArray(typeof(Type)).CopyTo(ret,0);
				
				return ret;
			}
		}

		public static Type  GetType(string typeStr)
		{
			Type ret = null;

			foreach(string dllName in DllNames)
			{
				Assembly curAssmbl = Assembly.LoadFrom(dllName);
				Type[] types = curAssmbl.GetTypes();
				foreach(Type curType in types)
				{
					if (typeStr == curType.ToString())
					{
						ret = curType;
					}		
					
				}
			}
			return ret;
		}



		protected static  string[] DllNames
		{
			get
			{					
				return System.IO.Directory.GetFiles("Devices","*.dll");
			}
			
		}

		 static DllLoader()
		{
			 //System.IO.Directory.SetCurrentDirectory(ConfigLoader.InitialDirectory);
			LoadDlls();
			 
			
		}

		

		public static bool IsInheritedFrom(Type childType,Type baseType)
		{
			bool ret = false;
			
			if (childType == typeof(object) || null == childType.BaseType)
			{
				ret = false;
			}
            else if (childType.BaseType.ToString() == baseType.ToString())
			{
				ret = true;
			}
			else
			{
				ret = IsInheritedFrom(childType.BaseType,baseType);
			 }

			return ret;
           			
		}

		public static bool IsCorrectDeviceType(Type curType)
		{
			bool  ret = false;

			if (IsInheritedFrom(curType,typeof(Device)) && !IsInheritedFrom(curType,typeof(BEMN.Modules.Module)))
			{
				if(null !=  curType.GetInterface("BEMN.Interfaces.INodeView") )
				{
					ret = true;
				}
			}
			return ret;
		}
		public static bool IsCorrectFormType(Type curType)
		{
			bool  ret = false;

			if (IsInheritedFrom(curType,typeof(System.Windows.Forms.Form)))
			{
				if(null !=  curType.GetInterface("BEMN.Interfaces.IFormView"))
				{
					ret = true;
				}
			}
			return ret;
		}

        public static bool IsPiconType(Type curType)
        {
            bool ret = false;

            if (IsInheritedFrom(curType, typeof(Devices.Device)))
            {
                if (null != curType.GetInterface("BEMN.Interfaces.IPicon"))
                {
                    ret = true;
                }
            }
            return ret;
        }

        public static bool IsNodeType(Type curType)
        {
            bool ret = false;
                     
            if (null != curType.GetInterface("BEMN.Interfaces.INodeView"))
            {
                ret = true;
            }
       
            return ret;
        }

        public static bool IsConnectModuleType(Type curType)
        {
            bool ret = false;

            if (IsInheritedFrom(curType, typeof(Modules.Module)))
            {
                if (null != curType.GetInterface("BEMN.Interfaces.IConnectModule"))
                {
                    ret = true;
                }
            }
            return ret;
        }

        public static bool IsCorrectModuleType(Type curType)
        {
            bool ret = false;

            if (IsInheritedFrom(curType, typeof(Modules.Module)))
            {
                if (null != curType.GetInterface("BEMN.Interfaces.IDeviceView"))
                {
                    ret = true;
                }
            }
            return ret;
        }

        public static Type GetTypeFromName(string NodeName)
		{
			Type ret = null;
            
			foreach(Type curType in NodeTypes)
			{				
				INodeView nodeView = (Activator.CreateInstance(curType) as INodeView);
                if (NodeName == nodeView.NodeName)
				{
					ret = curType;
				}
			}
                       

			return ret;
		}


        public static Type[] GetFormTypes(Type deviceType)
		{
			ArrayList formTypes = new ArrayList();
			foreach(Type curType in FormTypes)
			{
				IFormView formView = (Activator.CreateInstance(curType) as IFormView);
				if(deviceType == formView.FormDevice)
				{
					formTypes.Add(curType);
				}
			}

			Type[] ret = new Type[formTypes.Count];
			formTypes.ToArray(typeof(Type)).CopyTo(ret,0);
			return  ret;
			
		}

		
		public static void LoadDlls()
		{
			string[] files = DllNames;
            _deviceTypes.Clear();
            _formTypes.Clear();
            _moduleTypes.Clear();

			foreach(string dllName in files)
			{
				Assembly curAssmbl = Assembly.LoadFrom(dllName);
				Type[] types = curAssmbl.GetTypes();
				foreach(Type curType in types)
				{
					if (IsCorrectDeviceType(curType))
					{
						_deviceTypes.Add(curType);
					}
					if (IsCorrectFormType(curType))
					{
						_formTypes.Add(curType);
					}
					if (IsCorrectModuleType(curType))
					{
						_moduleTypes.Add(curType);
					}
                    if (IsNodeType(curType))
                    {
                        _nodeTypes.Add(curType);
                    }
					
				}
			}
		}
	}
}
