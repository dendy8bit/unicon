﻿using System;
using System.Xml.Serialization;

namespace BEMN.Framework
{
    [Serializable]
    [XmlRoot("Unicon")]
    public class UniconSettings
    {
        public UniconSettings()
        {
            this.Settings = new CommonSettings();
            this.OscSettings = new OscilloscopeSettings();
        }

        public CommonSettings Settings { get; set; }
        public OscilloscopeSettings OscSettings { get; set; }
    }

    [Serializable]
    public class CommonSettings
    {
        public bool OkExchange { get; set; }
        public bool FailExchange { get; set; }
        public bool Autosave { get; set; }
        public bool AutoloadSetpoints { get; set; }
        public bool UpdateWeb { get; set; }
        public DateTime LastVersion { get; set; }
        public bool WriteForSignatures { get; set; }
    }

    [Serializable]
    public class OscilloscopeSettings
    {
        public bool VectorWindow { get; set; }
        public bool PieWindow { get; set; }
        public bool FrequencyWindow { get; set; }
    }
}
