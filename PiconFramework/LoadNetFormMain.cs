﻿using System;
using System.Windows.Forms;

namespace BEMN.Framework
{
    public partial class LoadNetFormMain : Form
    {
        public LoadNetFormMain()
        {
            InitializeComponent();          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoadNetForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        public bool OscFlag
        {
            get
            {
                return this.checkBox1.Checked;
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://go.microsoft.com/fwlink/?LinkID=186913");
        }
    }
}
