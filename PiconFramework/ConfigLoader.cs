using System;
using System.Xml;

namespace BEMN.Framework
{
	/// <summary>
	/// 
	/// </summary>
	public class ConfigLoader
	{
		public static readonly string CONFIG_FILE = "Framework.config";
		private static XmlDocument doc;
	
		static ConfigLoader()
		{
			Reload();	
          
		}

		public static void  SetCurrentDir(string current_dir)
		{
			XmlNode curdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/CurrentDirectory");
			curdir_node.InnerText = current_dir;
			doc.Save(CONFIG_FILE);
		}

		public static void Reload()
		{
			string cfgFile = CONFIG_FILE;
			doc = new XmlDocument();
			try
			{
				doc.Load(cfgFile);
			}
			catch
			{
				System.Windows.Forms.MessageBox.Show("�� ���� ��������� ���� ������������ " + cfgFile);
			}
		}


		public static string DevicesDir
		{
			get
			{
				XmlNode devdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/DevicesDirectory");
				return devdir_node.InnerText;
			}
		}

		public static string TempDir
		{
			get
			{
                XmlNode devdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/TempDirectory");
				return devdir_node.InnerText;
			}
		}

		public static string InitialDirectory
		{
			get
			{
                XmlNode devdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/CurrentDirectory");
				return devdir_node.InnerText;
			}
		}


		public static string JournalsDir
		{
			get
			{
                XmlNode devdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/JournalsDirectory");
				return devdir_node.InnerText;
			}
		}

		public static string UtilsDir
		{
			get
			{
                XmlNode devdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/UtilsDirectory");
				return devdir_node.InnerText;
			}
		}

		public static string ProjectsDir
		{
			get
			{
                XmlNode devdir_node = doc.SelectSingleNode("/FrameworkConfig/Enviroment/ProjectsDirectory");
				return devdir_node.InnerText;
			}
		}

	}
}

