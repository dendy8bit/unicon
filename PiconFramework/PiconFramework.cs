using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.BusinessLogic;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using Crownwood.Magic.Menus;
using Crownwood.Magic.Win32;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.Framework
{
    public partial class Framework
    {
        private bool _autosave = true;
        private int _serialBarId;
        private ToolStripDropDownButton _newFileStrpiBut;
        private ToolStripMenuItem _newPorjectStripBut;
        private ToolStripMenuItem _newSourceStripBut;
        private ToolStripSeparator _toolStripMenuItem2;
        private ToolStripMenuItem _portsToolStripMenuItem;
        private ToolStripSeparator _toolStripSeparator10;
        public static bool IsProjectOpening { get; set; }

        public static Framework MainWindow { get; private set; }

        public bool UpdateWeb { get; set; }

        public DateTime LastVersionTime { get; set; }

        public static bool IsWriteSignatures { get; set; } = true;

        public bool Autosave
        {
            get { return this._autosave; }
            set 
            {
                this._autosave = value;
                this._autosaveMenuItem.Checked = value;
            }
        }

        public bool AutoLoadSetpoints
        {
            get { return this._autoLoadSetpointsMI.Checked; }
            set
            {
                Device.SetAutiloadSetpoint(value);
                this._autoLoadSetpointsMI.Checked = value;
            }
        }

        public bool ShowOk
        {
            get { return Log.ShowOk; }
            set { Log.ShowOk = this._normalExchangeMenuItem.Checked = value; }
        }

        public bool ShowFail
        {
            get { return Log.ShowFail; }
            set { Log.ShowFail = this._errorExchangeMenuItem.Checked = value; }
        }

        #region Properties

        public string CurrentProject
        {
            get { return this._currentProject; }
            set
            {
                this._currentProject = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), value);
                Text = Path.GetFileNameWithoutExtension(this.CurrentProject) + " - ������";
            }
        }

        public static LogList Log
        {
            get { return logList; }
        }

        #endregion

        #region Constants

        private const string AUTOSAVE_FILE = "Projects\\autosave.uprj";

        #endregion
        
        #region Methods CreateForm

        private void CreateExchangePanel()
        {
            logList = new LogList {ScrollAlwaysVisible = true, Dock = DockStyle.Fill};
            logList.ShowNewStatistic += this.ShowStatistic;
            Panel panel = new Panel {Size = new Size(Width, 100), Dock = DockStyle.Bottom};
            panel.Controls.Add(logList);
            Content c = this._styleManager.Contents.Add(panel, "C����");
            c.CloseButton = c.HideButton = c.CaptionBar = true;
            c.ImageList = new ImageList();
            c.ImageList.Images.Add(Resources.serial_ok.ToBitmap());
            c.ImageIndex = 0;
            c.Docked = false;
            c.DisplaySize = panel.Size;

            this._styleManager.AddContentWithState(c, State.DockBottom);
        }

        private void CreateDeviceNetForm()
        {
            this.projectForm = new ProjectForm
            {
                MdiParent = this,
                Anchor = AnchorStyles.Left,
                Dock = DockStyle.Left
            };

            Content c = this._styleManager.Contents.Add(this.projectForm, "����������");
            c.CaptionBar = c.HideButton = c.Docked = true;
            c.CloseButton = false;
            c.DisplaySize = new Size(285, this.projectForm.Height);
            this._styleManager.AddContentWithState(c, State.DockLeft); 
        }

        #endregion	Methods CreateForm

        #region Methods Actions
        private static void NewProject()
        {
            if (0 != TreeManager.Tree.Nodes.Count)
            {
                if (DialogResult.OK ==
                    MessageBox.Show("������������� ��������� ������� ����� �������. ���������� ?", "������",
                        MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
                {
                    FormManager.CloseAll();
                    TreeManager.AddProjectNode();
                }
            }
            else
            {
                FormManager.CloseAll();
                TreeManager.AddProjectNode();
            }
        }

        private bool SaveProject()
        {
            bool ret = false;
            string dir = Path.GetDirectoryName(Application.ExecutablePath) + "\\Projects";
            if (Directory.Exists(dir))
            {
                this.saveProjectFileDialog.InitialDirectory = dir;
            }
            else
            {
                Directory.CreateDirectory(dir);
                this.saveProjectFileDialog.InitialDirectory = dir;
            }
            if (DialogResult.OK == this.saveProjectFileDialog.ShowDialog())
            {
                ret = this.projectForm.SaveProject(this.saveProjectFileDialog.FileName);
                this.CurrentProject = this.saveProjectFileDialog.FileName;
            }
            return ret;
        }

        private void OpenProject()
        {
            IsProjectOpening = true;
            this.openProjectDialog.InitialDirectory = "Devices//Projects";
            this.openProjectDialog.ShowDialog();
            if (0 != this.openProjectDialog.FileName.Length)
            {
                this.OpenProject(this.openProjectDialog.FileName);
            }
        }

        private void OpenProject(string fname)
        {
            FormManager.CloseAll();

            this.projectForm.LoadProject(fname);

            foreach (Device device in DeviceManager.SearchDevices(null))
            {
                logList.AddDevice(device);

                if (null != device.MB)
                {
                    device.MB.OkExchange += this.MB_OkExchange;
                    device.MB.FailExchange += this.MB_FailExchange;
                    device.DeviceNumberChanged += this.AddedDevice_DeviceNumberChanged;
                    device.PortNumberChanged += this.AddedDevice_PortNumberChanged;
                }
            }
            this.CurrentProject = fname;
        }

        #endregion

        #region Methods UIControl Handlers
        private void _autoLoadSetpointsMI_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            item.Checked = !item.Checked;
            Device.SetAutiloadSetpoint(item.Checked);
        }

        private static void _styleManager_ContextMenu(PopupMenu pm, CancelEventArgs cea)
        {
            pm.MenuCommands["Show All"].Text = "�������� ���";
            pm.MenuCommands["Hide All"].Text = "�������� ���";
        }

        private void Framework_FormClosing(object sender, FormClosingEventArgs e)
        {
            ProjectLoader.SaveSettings(this);
            this._statusLabel.Text = "������ " + this.CurrentProject + " �����������";
            if (this.Autosave)
            {
                List<string> prjFileList = new List<string>(1);
                DialogResult saveResult = DialogResult.No;
                if (TreeManager.IsTreeChanged)
                {
                    prjFileList.Add(this.CurrentProject);
                    SaveSourceDialog dlg = new SaveSourceDialog();
                    dlg.StartPosition = FormStartPosition.CenterScreen;
                    dlg.FilesList = prjFileList;
                    saveResult = dlg.ShowDialog();
                }

                if (DialogResult.Cancel == saveResult)
                {
                    e.Cancel = true;
                    return;
                }

                if (DialogResult.Yes == saveResult)
                {
                    this._statusLabel.Image = Resources.save;
                    this._statusLabel.Text = "���������� ������� " + this.CurrentProject;
                    this.projectForm.SaveProject(this.CurrentProject);
                }
            }
            ProjectLoader.Close();
            TreeManager.RemoveProjectNode();
            Modbus.ClearTcp();
        }


        private void _newFileItem_Click(object sender, EventArgs e)
        {
            NewProject();
        }

        private void _openFileItem_Click(object sender, EventArgs e)
        {
            this.OpenProject();
        }
        
        private void _exitMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _aboutMenuItem_Click(object sender, EventArgs e)
        {
            About about_form = new About();
            about_form.ShowDialog();
        }


        private void InitManagers()
        {
            NodeManager.Images.Clear();
            NodeManager.Images.AddImages(DllLoader.GetNodeImages(), DllLoader.NodeTypes.ToArray()); // ��������� ���������� � ��������������� �� �������� � ���-�������
            this._addDeviceContextMenu.Items.Clear();
            this._addDeviceItem.DropDownItems.Clear();
            MenuManager menuManager = new MenuManager();

            menuManager.AddDeviceItems(this._addDeviceToolStripButton, DllLoader.DeviceTypes); // ��� �� ��������, ����������� ��� ������� ���� "���������" ���������� ������� (�������� ����������, ��������, �������� �� ������� ���)
            menuManager.AddDeviceItems(this._addDeviceContextMenu, DllLoader.DeviceTypes);     // ���->"�������� ���������" ����������
            menuManager.AddDeviceItems(this._addDeviceItem, DllLoader.DeviceTypes);            // ����->"�������� ����������" ����������
            
            MenuManager.DeviceAddBegin += this.MenuManager_DeviceAddBegin;
            MenuManager.DeviceAddEnd += this.MenuManager_DeviceAddEnd;
        }

        private void MenuManager_DeviceAddEnd(DeviceAddEventArgs e)
        {
            if (e.Success)
            {
                this._statusLabel.Text = e.DeviceName + " ������� ��������  � ������";
                this._statusProgress.Value = 100;
                e.AddedDevice.DeviceNumberChanged += this.AddedDevice_DeviceNumberChanged;
                e.AddedDevice.PortNumberChanged += this.AddedDevice_PortNumberChanged;
                e.AddedDevice.MB.FailExchange += this.MB_FailExchange;
                e.AddedDevice.MB.OkExchange += this.MB_OkExchange;
            }
            else
            {
                this._statusLabel.Text = e.DeviceName + " �� �������� � ������";
                this._statusProgress.Value = 100;
            }
        }

        private void AddedDevice_PortNumberChanged(object sender, PortNumberChangedEventArgs e)
        {
            this._statusLabel.Text = ((IDeviceView) sender).NodeName + " �" + e.NewNumber + ": ���������� ���� " + e.OldNumber + " -> ����� ���� " + e.NewNumber;

        }

        private void AddedDevice_DeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
        {
            this._statusLabel.Text = ((IDeviceView) sender).NodeName + " �" + newNumber + ": ���������� ����� " + oldNumber + " -> ����� ����� " + newNumber;
        }

        private void MB_OkExchange(object sender, Query query)
        {
            if (!Disposing && !IsDisposed)
            {
                BeginInvoke(new OnDeviceEventHandler(this.OnModbusOK));
            }
        }

        private void MB_FailExchange(object sender, Query query, string err)
        {
            if (!Disposing && !IsDisposed)
            {
                BeginInvoke(new OnDeviceEventHandler(this.OnModbusFail));
            }
        }

        private void OnModbusOK()
        {
            this._statusLabel.Text = "���� �����. ����� " + Log.All + ",����. " + Log.Ok + ",����. " + Log.Fail + ",���������� " + Log.OkPercent + "%";
            this._statusLabel.Image = Resources.serial_ok.ToBitmap();
        }

        private void OnModbusFail()
        {
            this._statusLabel.Text = "��� �����. ����� " + Log.All + ",����. " + Log.Ok + ",����. " + Log.Fail + ",���������� " + Log.OkPercent + "%";
            this._statusLabel.Image = Resources.serial_fail.ToBitmap();
        }

        private void ShowStatistic()
        {
            this._statusLabel.Text = "����� " + Log.All + ",����. " + Log.Ok + ",����. " + Log.Fail + ",���������� " + Log.OkPercent + "%";
            this._statusLabel.Image = null;
        }

        private void MenuManager_DeviceAddBegin(DeviceAddEventArgs e)
        {
            this._statusLabel.Text = "���������� " + e.AddedDevice;
            this._statusProgress.Value = 0;
        }
        
        private void _cascadMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void _verticalMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void _horizontMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void _showAllMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this._styleManager.ShowAllContents();
            }
            catch (NullReferenceException)
            {
            }
        }

        private void _hideAllMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this._styleManager.HideAllContents();
            }
            catch (NullReferenceException)
            {
            }
        }

        private void _saveItem_Click(object sender, EventArgs e)
        {
            this._statusLabel.Image = Resources.save;
            this._statusLabel.Text = "���������� �������...";
            this._statusProgress.Value = 0;
            if (this.SaveProject())
            {
                this._statusProgress.Value = 100;
                this._statusLabel.Text = "������ ��������";
            }
            else
            {
                this._statusProgress.Value = 0;
                this._statusLabel.Text = "���������� ������� ��������";
            }
        }


        private void _closeDocMenuItem_Click(object sender, EventArgs e)
        {
            FormManager.CloseAll();
        }

        private void _minimizeDocMenuItem_Click(object sender, EventArgs e)
        {
            FormManager.MinimizeAll();
        }

        private void _showDocMenuItem_Click(object sender, EventArgs e)
        {
            FormManager.ShowAll();
        }
        
        private void _saveAllToolStripButton_Click(object sender, EventArgs e)
        {
            this._statusLabel.Image = Resources.save_all;
            this._statusLabel.Text = "���������� �������";
            this._statusProgress.Value = 0;
            if (this.projectForm.SaveProject(this.CurrentProject))
            {
                this._statusProgress.Value = 50;
                this._statusProgress.Value = 100;
                this._statusLabel.Text = "������ ��������";
            }
            else
            {
                this._statusProgress.Value = 100;
                this._statusLabel.Text = "���������� ��������� ������";
            }
        }

        private void _normalExchangeMenuItem_Click(object sender, EventArgs e)
        {
            this._normalExchangeMenuItem.Checked = !this._normalExchangeMenuItem.Checked;
            this.ShowOk = this._normalExchangeMenuItem.Checked;
        }

        private void _errorExchangeMenuItem_Click(object sender, EventArgs e)
        {
            this._errorExchangeMenuItem.Checked = !this._errorExchangeMenuItem.Checked;
            this.ShowFail = this._errorExchangeMenuItem.Checked;
        }

        #endregion	Methods UIControl Handlers

        #region Construct/Destruct

        [DllImport("kernel32.dll")]
        public static extern bool SetProcessWorkingSetSize(IntPtr handle,
            int minimumWorkingSetSize, int maximumWorkingSetSize);

        public Framework()
        {
            this.UpdateWeb = true;
            this.InitializeComponent();
            Application.ApplicationExit += ApplicationOnApplicationExit;
        }

        private void ApplicationOnApplicationExit(object sender, EventArgs eventArgs)
        {
            try
            {
                Modbus.SerialServer.FireExit();
                Process p = Process.GetProcessById(this._serialBarId);
                p.Kill();

                Process[] processes = Process.GetProcessesByName("SerialServer");
                foreach (Process process in processes)
                {
                    process.Kill();
                }
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
            }
        }

        private static void OnInvalidCache(WaitingForm waitForm)
        {
            waitForm.Progress.Maximum += 2*waitForm.Progress.Step;
            waitForm.Message = "��� ���������. �������� ������ �� ���������";
            DllLoader.GetDataFromDll();
            waitForm.Progress.Increment(20);

            waitForm.Message = "����������� ������";
            DllLoader.SaveCache();
            waitForm.Progress.Increment(20);
        }

        /// <summary>
        /// ����������� �������� ������, ���� ���� ����� ������
        /// </summary>
        private void UpdateFunction()
        {
            try
            {
                WebClient wc = new WebClient();
                string htmlContent = wc.DownloadString("http://bemn.by/download/servisnoe-programmnoe-obespechenie/");
                
                Encoding utf8 = Encoding.GetEncoding("Windows-1251");
                Encoding win1251 = Encoding.GetEncoding("UTF-8");
                byte[] utf8Bytes = win1251.GetBytes(htmlContent);
                byte[] win1251Bytes = Encoding.Convert(win1251, utf8, utf8Bytes);
                htmlContent = win1251.GetString(win1251Bytes);

                MatchCollection AnchorTags = Regex.Matches(htmlContent.ToLower(), @"(<a href=.*?\>.*?</a>)", RegexOptions.Singleline);

                /*�������� ������ ��� ��������:
                    <a href="/filedownload.php?file=4986">
	                <span style="width:46px;height:46px;display:block;background:url(/images/file-zip.png) no-repeat;float:left; margin:-11px 20px 0 0;"></span>
	                ������ (15.07.2019). ��������� ���������������� ��������� ��, ���, ���, ���.zip(9076 kb)
	                </a>
                */

                foreach (Match AnchorTag in AnchorTags)
                {
                    try
                    {
                        string value = AnchorTag.Groups[1].Value;
                        string result = string.Empty;
                        string finalValue = string.Empty;
                        string linkCache = string.Empty;

                        int i = 0;

                        if (value.Contains("������"))
                        {
                            int j = 0;

                            for (; j < value.Length; j++)
                            {
                                if(value[j] == '/') break;;
                            }

                            linkCache = new string(value.Skip(j).ToArray()); // ������� ��� ������� �� ������� "/"

                            string link = linkCache.Split('"')[0]; // ������� ��� ������� ����� ������ ������� ������� (")

                            for (; i < value.Length; i++) // ���� ������ ����� ������������ �� "�"
                            {
                                if (value[i] == '�') break;
                            }
                            
                            result = new string(value.Skip(i).ToArray()); // ��� ��� �� ����� �� "�" ���������
                            
                            i = 0;

                            for (; i < result.Length; i++)
                            {
                                if (result[i] == '(') // ���� ������ "("
                                {
                                    finalValue = result;
                                    break;
                                }
                            }
                            
                            string newResultValue = finalValue.Split(')')[0]; // ������� �� ����� ������ ")"

                            i = 0;

                            for (; i < newResultValue.Length; i++) // ���� ������ "("
                            {
                                if (newResultValue[i] == '(') break;
                            }

                            result = new string(newResultValue.Skip(i + 1).ToArray()); // ������� �� �� ������ "(" � "(" ����, ������ �������� ������ ����. ������: 15.01.2020

                            DateTime server = DateTime.Parse(result);
                            if ((server > this.LastVersionTime) || this.UpdateWeb)
                            {
                                this.LastVersionTime = server;
                            }
                            else
                            {
                                return;
                            }
                            if (server > About.RetrieveLinkerTimestamp())
                            {
                                UpdateMessageForm messageForm = new UpdateMessageForm();
                                messageForm.DontShow = !this.UpdateWeb;
                                messageForm.StartPosition = FormStartPosition.CenterParent;
                                DialogResult resultDlg = messageForm.ShowDialog(this);
                                this.UpdateWeb = !messageForm.DontShow;

                                if (resultDlg == DialogResult.Yes)
                                {
                                    Process.Start("http://bemn.by" + link);
                                }
                            }

                        }

                    }

                    catch (Exception e)
                    {

                    }

                    //string pattern = @"href=\""/(.*?)\"".*������ (\d\d\.\d\d\.\d\d\d\d).*";

                    //Match HrefAttribute = Regex.Match(value, pattern, RegexOptions.Singleline);

                    //if (HrefAttribute.Success)
                    //{
                    //    DateTime server = DateTime.Parse(HrefAttribute.Groups[2].Value);
                    //    if ((server > this.LastVersionTime) || this.UpdateWeb)
                    //    {
                    //        this.LastVersionTime = server;
                    //    }
                    //    else
                    //    {
                    //        return;
                    //    }
                    //    if (server > About.RetrieveLinkerTimestamp())
                    //    {
                    //        UpdateMessageForm messageForm = new UpdateMessageForm();
                    //        messageForm.DontShow = !this.UpdateWeb;
                    //        messageForm.StartPosition = FormStartPosition.CenterParent;
                    //        DialogResult result = messageForm.ShowDialog(this);
                    //        this.UpdateWeb = !messageForm.DontShow;

                    //        if (result == DialogResult.Yes)
                    //        {
                    //            Process.Start("http://bemn.by/" + HrefAttribute.Groups[1].Value);
                    //        }
                    //    }
                    //}
                }
            }
            catch{}
        }

        private void Framework_Load(object sender, EventArgs e)
        {
            this.Check4DotNet(); // �������� ������ .Net Framework, ������������� �� ����������.
            WaitingForm waitForm = new WaitingForm(); 
            waitForm.Show(); // ����������� ����� �������� �������.
            waitForm.Message = "����������� ����� ���������";
            DllLoader.ParseTypes(); // ������� *.dll ��������� � �������. ���������� ����� (�������) ������ DllLoader.cs
            waitForm.Progress.Increment(20);

            try
            {
                if (DllLoader.IsDllListChanged()) // ��� �������� ���������, �������� �� ���������
                {
                    waitForm.Progress.Maximum += waitForm.Progress.Step;
                    waitForm.Message = "���������� ���� ��������. �������� ������ �� ���������";
                    DllLoader.GetDataFromDll(); // ����������� �� ������ ���� (� ����������� ��������)
                    waitForm.Progress.Increment(20);

                    waitForm.Message = "����������� ������";
                    DllLoader.SaveCache();
                    waitForm.Progress.Increment(20);
                }
                else // ��� ���������� ���������, �������� �� ����� cache.xml
                {
                    waitForm.Message = "�������� ������ �� ����";
                    DllLoader.LoadCache();
                    waitForm.Progress.Increment(20);
                }
            }

            catch (XmlException)
            {
                OnInvalidCache(waitForm);
            }
            catch (ArgumentOutOfRangeException)
            {
                OnInvalidCache(waitForm);
            }
            catch (FormatException)
            {
                OnInvalidCache(waitForm);
            }
            
            waitForm.Message = "��������� ������� �����";
            //������������ ������ ���
            SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle,-1, -1);

            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU", false);
            FormManager.ParentForm = this;
            this._styleManager = new DockingManager(this, VisualStyle.IDE);
            this._styleManager.ContextMenu += _styleManager_ContextMenu;

            waitForm.Message = "���������������� �����";
            waitForm.Progress.Increment(20);
            DeviceManager.ModbusInit(); // ��������� ���������� �� ����� �����, ���������� �� SerialServer
            waitForm.Message = "��������� ������ ������";
            waitForm.Progress.Increment(20);
            this.CreateDeviceNetForm(); // �������� ����� ������� ����� �� ����� PiconFramework
            waitForm.Message = "������������ ������-������";
            waitForm.Progress.Increment(20);
            this.InitManagers(); // ���������� ������� ������� MenuManager, NodeManager �� ������� ������ DllLoader 
            waitForm.Message = "��������� ������ �������";
            waitForm.Progress.Increment(20);
            this.CreateExchangePanel(); // �������� ����� �������� ������ � ����� �� ����� PiconFramework

            waitForm.Message = "����������� ������";
            waitForm.Progress.Increment(20);
            ProjectLoader.LoadSettings(this); // �������� �������� �������, ���� ���������� ���� UniconSetting.ini
                                              // �������� ��������� ��������� ��� ���������� ���������� ������

            this.UpdateFunction(); // ����������� �������� ������, ���� ���� ����� ������ 
         
            if (File.Exists(AUTOSAVE_FILE)) // ���� ���������� ���� autosave.uprj
            {
                this.OpenProject(AUTOSAVE_FILE); // ��������� ���
                this._statusLabel.Text = "������ ������";
            }
            this.CurrentProject = AUTOSAVE_FILE;

            if (this.RunSerialBar())
            {
                waitForm.Close();
            }
            else
            {
                MessageBox.Show(
                    "������ ������� ���������� ��� ���� SerialBar.exe �� ������. ��� ���������� ������ ����������� ���������",
                    "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                waitForm.Close();
                Close();
            }
        }
        
        /// <summary>
        /// ��������� ������� �� ������� � �������� PID'��
        /// </summary>
        /// <param name="id">PID</param>
        /// <returns>������� �� �������</returns>
        private static bool IsProcessRunned(Int32 id)
        {
            bool ret;
            try
            {
                ret = id != 0 && Process.GetProcessById(id).HasExited;
            }
            catch (ArgumentException)
            {
                ret = false;
            }
            catch (Win32Exception)
            {
               ret = false;
            }
            return ret;
        }

        /// <summary>
        /// ������ serialBar.exe
        /// </summary>
        /// <returns>������� �� ������� serialBar.exe</returns>
        private bool RunSerialBar()
        {
            if (IsProcessRunned(this._serialBarId))
                return true;
            string processName = "SerialBar";
            string serialBarPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "serialBar.exe");
            if (File.Exists(serialBarPath))
            {
                Process[] p = Process.GetProcessesByName(processName);
                foreach (Process proces in p)
                {
                    proces.Kill();
                }
                Process pr = Process.Start(serialBarPath); // ������ SerialBar.exe �� ���������� ����
                if (pr == null)
                    return false;
                this._serialBarId = pr.Id;
                if (!pr.HasExited)
                    pr.WaitForInputIdle(1000);
                return true;
            }
            return false;
        }

        protected override void Dispose(bool disposing)
        {
           if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public T InvokeFunc<T>(Func<T> method)
        {
            try
            {
                if (IsHandleCreated)
                {
                    return (T) Invoke(method);
                }
                return default(T);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public void InvokeAction(Action method)
        {
            try
            {
                if (IsHandleCreated)
                {
                    Invoke(method);
                }
            }
            catch (Exception)
            {
            }
        }

        [STAThread]
        private static void Main()
        {
            try
            {
                if (ExistUnicon())
                {
                    Application.EnableVisualStyles();
                    Application.DoEvents();
                    MainWindow = new Framework();
                    Application.Run(MainWindow);
                }
                else
                {
                    MessageBox.Show("���������� \"������\" ��� ��������.", "������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("����������� ������. ���������� ����� �������.", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.LogException(e);
                MainWindow.Close();
            }
        }

        /// <summary>
        /// ��������� ������� ��� ������� ������.
        /// ���������� true - ���� 1 ���, false - ���� �����.
        /// </summary>
        private static bool ExistUnicon()
        {
            string processName = "UniCon";
            Process[] p = Process.GetProcessesByName(processName);
            return p.Length < 2;
        }

        #endregion

        private void _autosaveMenuItem_Click(object sender, EventArgs e)
        {
            this.Autosave = this._autosaveMenuItem.Checked = !this._autosaveMenuItem.Checked;
        }
        
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(HandleRef hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        private static void ShowSerialBarThr()
        {
            object wrapper = new object();
            HandleRef hSerialBar = new HandleRef(wrapper, FindWindow("Afx:00400000:0", "SERIALBAR"));
            SendMessage(hSerialBar, (uint) (Msgs.WM_USER + 0x101), new IntPtr(0), new IntPtr(0));
        }

        private void OnPortsToolStripButtonClick(object sender, EventArgs e)
        {
            if (this.RunSerialBar())
            {
                Thread thr = new Thread(ShowSerialBarThr);
                thr.Start();
            }
        }

        private void _readForReadSignaturesSignalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IsWriteSignatures = _readSignaturesSignalsToolStripMenuItem.Checked = !_readSignaturesSignalsToolStripMenuItem.Checked;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            TimeSynchronizationForm _timeSynchronization = new TimeSynchronizationForm();
            _timeSynchronization.MdiParent = this;
            _timeSynchronization.Show();
        }


        private void Check4DotNet()
        {
            if (Validator.GetVersionFromRegistry() || Settings.Default.OscFlag) return;
            
            LoadNetFormMain loadNetForm = new LoadNetFormMain();
            loadNetForm.ShowDialog(this);
            Settings.Default.OscFlag = loadNetForm.OscFlag;
            Settings.Default.Save();
        }
    }
}