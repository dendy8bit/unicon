using System.Windows.Forms;
using System.Drawing;
using BEMN.Devices;

namespace BEMN.Framework.BusinessLogic
{
    public delegate void ReplaceTextHandler(TreeNode node,string newText);
    public delegate void ReplaceImageHandler(TreeNode node, Image newImage,Image oldImage);
    public delegate TreeNode GetNodeHandler(object Tag, TreeNode node);
    public delegate TreeNode ChangeNodeHandler(object Tag, TreeNode node, TreeNode[] formNodes);
    public delegate void ActiveNodeChangedHandler(ActiveNodeChangedEventArgs e);
    public delegate void DeviceAddHandler(DeviceAddEventArgs e);
}
