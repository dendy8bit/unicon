using System;
using System.Windows.Forms;
using System.Collections.Generic;

using BEMN.Devices;
using BEMN.Interfaces;

using BEMN.Utils;

namespace BEMN.Framework.BusinessLogic
{
	/// <summary>
	/// 
	/// </summary>
	public class NodeManager
	{
        private static ImageIndexHash _images;

        public static ImageIndexHash Images
        {
            get
            {
                return _images;
            }
            set
            {
                _images = value;
            }
        }

        public static  bool IsProjectNode(TreeNode node)
		{
			return null != node.Tag && "������" == node.Tag.ToString();
		}
        
	    public static bool IsPageNode(TreeNode node)
		{
			return node.Tag is Type;
		}

		public static bool IsDeviceNode(TreeNode node)
		{
		    return null != node && null != node.Tag && node.Tag is Device;			
		}

        public static bool IsDeletable(TreeNode node)
        {
           return null != node && null != node.Tag && node.Tag is INodeView && (node.Tag as INodeView).Deletable;
        }
        
		public static TreeNode CreateNode(string text,int imageIndex,object tag)
		{
			TreeNode node = new TreeNode(text);
			node.ImageIndex = imageIndex;
			node.SelectedImageIndex = imageIndex;
			node.Tag = tag;
            AddChildNodes(node);
          
			return node;
		}

        public static bool IsNodesEqual(INodeView view,TreeNode node)
        {
            return null != view && null != node.Tag as INodeView && view.NodeName == node.Text && view.ClassType == (node.Tag as INodeView).ClassType;
        }

        public static void AddChildNodes(TreeNode node)
        {
            if (null != node.Tag)
            {
                if (node.Tag is INodeView || node.Tag.GetType() is INodeView)
                {
                    INodeView view = node.Tag as INodeView;
                    if (null != view.ChildNodes)
                    {                        
                        foreach (INodeView chView in view.ChildNodes)
                        {
                            try
                            {
                            object tag = Activator.CreateInstance(chView.ClassType);
                                                                              
                            if (DllLoader.IsCorrectFormType(tag.GetType()))
                            {
                                tag = tag.GetType();
                            }
                            
                            node.Nodes.Add(CreateNode(chView.NodeName, (int)Images[chView.ClassType], tag));
                            }
                            catch (Exception)
                            {

                                MessageBox.Show("Error. AddChildNodes.");
                            }

                        }                
                    }
                    
                }    
            }
            
        }

		public static TreeNode CreateProjectNode()
		{
			return NodeManager.CreateNode("����� ������",0,"������");
		}

		public static TreeNode CreateDeviceNode(Device device)
		{            
			return  CreateNode(DeviceNodeText(device),(int)Images[device.GetType()],device);
		}
                
		public static List<TreeNode> CreateFormNodes(Type[] formTypes)
		{
            int[] images = Images[formTypes];
            List<TreeNode> ret = new List<TreeNode>(formTypes.Length);
            int j = 0;
			for (int i = 0; i < formTypes.Length; i++)
			{                
               TreeNode node = CreateFormNode(formTypes[i], images[i]);
               if (!DllLoader.FormForceShowHash[formTypes[i]])
               {
                   ret.Add(node);
               }

			}
            Comparison<TreeNode> comparer = new Comparison<TreeNode>(NodeComparer);
            ret.Sort(comparer);
			return ret;
		}

	    public static int NodeComparer(TreeNode n1, TreeNode n2)
	    {
	        return string.Compare(n1.Text, n2.Text);
	    }

	    public static TreeNode CreateFormNode(Type formType, int imageInd)
        {
            return CreateNode(DllLoader.FormNameHash[formType], imageInd, formType);
        }
        
		public static string DeviceNodeText(Device device)
		{
            string ret = "�" + device.DeviceNumber + " " + (device as IDeviceView).NodeName;
            return ret;
		}
        	
	}
}
