﻿using System;
using System.Windows.Forms;

namespace BEMN.Framework
{
    public partial class UpdateMessageForm : Form
    {
        public UpdateMessageForm()
        {
            InitializeComponent();
        }

        public bool DontShow { get; set; }


        private void button2_Click(object sender, EventArgs e)
        {
            DontShow = checkBox1.Checked;
            this.DialogResult = DialogResult.Yes;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DontShow = checkBox1.Checked;
            this.DialogResult = DialogResult.No;
        }

        private void UpdateMessageForm_Shown(object sender, EventArgs e)
        {
            checkBox1.Checked = DontShow;
        }
    }
}
