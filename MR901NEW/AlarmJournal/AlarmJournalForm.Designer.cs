﻿namespace BEMN.MR901NEW.AlarmJournal
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iSH1TormColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iSH2TormColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iPOTormColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I6cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I9Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I10Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I11Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I12Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I13Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I14Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I15Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I16Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I17Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I18Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I19Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I20Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I21Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I22Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I23Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I24Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ubCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ucCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._unCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ubcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ucaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._3u0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать журнал";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this._readAlarmJournalButtonClick);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(536, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(402, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(128, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._openAlarmJournalDialog.Filter = "МР901 Журнал аварий(*.xml)|*.xml|МР801 Журнал аварий(*.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР901";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР901) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 17);
            this._statusLabel.Text = ".";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(659, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(117, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР901";
            this._saveJournalHtmlDialog.Filter = "Журнал аварий МР901 | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._groupCol,
            this._IdaCol,
            this._iSH1TormColumn,
            this._IdbCol,
            this._iSH2TormColumn,
            this._IdcCol,
            this._iPOTormColumn,
            this._I1Col,
            this._I2Col,
            this._I3Col,
            this._i4Col,
            this._I5Col,
            this._I6cCol,
            this._I7Col,
            this._I8Col,
            this._I9Col,
            this._I10Col,
            this._I11Col,
            this._I12Col,
            this._I13Col,
            this._I14Col,
            this._I15Col,
            this._I16Col,
            this._I17Col,
            this._I18Col,
            this._I19Col,
            this._I20Col,
            this._I21Col,
            this._I22Col,
            this._I23Col,
            this._I24Col,
            this._uaCol,
            this._ubCol,
            this._ucCol,
            this._unCol,
            this._uabCol,
            this._ubcCol,
            this._ucaCol,
            this._3u0Col,
            this._u2Col,
            this._d1Col,
            this._d2Col,
            this._d3Col,
            this._d4Col,
            this._d5Col,
            this._d6Col,
            this._d7Col,
            this._d8Col});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle55.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle55;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 528);
            this._alarmJournalGrid.TabIndex = 24;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "№";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Дата/Время";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._timeCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._msg1Col.DataPropertyName = "Сообщение";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle3;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msg1Col.Width = 71;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._msgCol.DataPropertyName = "Сработавшая защита";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._msgCol.HeaderText = "Сработавшая защита";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 110;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._codeCol.DataPropertyName = "Параметр срабатывания";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 126;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "Значение параметра срабатывания";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._groupCol.DataPropertyName = "Группа уставок";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._groupCol.Width = 82;
            // 
            // _IdaCol
            // 
            this._IdaCol.DataPropertyName = "Iдиф. СШ1";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IdaCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._IdaCol.HeaderText = "Iдиф. СШ1";
            this._IdaCol.Name = "_IdaCol";
            this._IdaCol.ReadOnly = true;
            this._IdaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdaCol.Width = 58;
            // 
            // _iSH1TormColumn
            // 
            this._iSH1TormColumn.DataPropertyName = "Iторм. СШ1";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._iSH1TormColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this._iSH1TormColumn.HeaderText = "Iторм. СШ1";
            this._iSH1TormColumn.Name = "_iSH1TormColumn";
            this._iSH1TormColumn.ReadOnly = true;
            this._iSH1TormColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iSH1TormColumn.Width = 62;
            // 
            // _IdbCol
            // 
            this._IdbCol.DataPropertyName = "Iдиф. СШ2";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IdbCol.DefaultCellStyle = dataGridViewCellStyle10;
            this._IdbCol.HeaderText = "Iдиф. СШ2";
            this._IdbCol.Name = "_IdbCol";
            this._IdbCol.ReadOnly = true;
            this._IdbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdbCol.Width = 58;
            // 
            // _iSH2TormColumn
            // 
            this._iSH2TormColumn.DataPropertyName = "Iторм. СШ2";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._iSH2TormColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this._iSH2TormColumn.HeaderText = "Iторм. СШ2";
            this._iSH2TormColumn.Name = "_iSH2TormColumn";
            this._iSH2TormColumn.ReadOnly = true;
            this._iSH2TormColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iSH2TormColumn.Width = 62;
            // 
            // _IdcCol
            // 
            this._IdcCol.DataPropertyName = "Iдиф. ПО";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IdcCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._IdcCol.HeaderText = "Iдиф. ПО";
            this._IdcCol.Name = "_IdcCol";
            this._IdcCol.ReadOnly = true;
            this._IdcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdcCol.Width = 52;
            // 
            // _iPOTormColumn
            // 
            this._iPOTormColumn.DataPropertyName = "Iторм. ПО";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._iPOTormColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this._iPOTormColumn.HeaderText = "Iторм. ПО";
            this._iPOTormColumn.Name = "_iPOTormColumn";
            this._iPOTormColumn.ReadOnly = true;
            this._iPOTormColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iPOTormColumn.Width = 57;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "I1";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I1Col.DefaultCellStyle = dataGridViewCellStyle14;
            this._I1Col.HeaderText = "I1";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 22;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "I2";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I2Col.DefaultCellStyle = dataGridViewCellStyle15;
            this._I2Col.HeaderText = "I2";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 22;
            // 
            // _I3Col
            // 
            this._I3Col.DataPropertyName = "I3";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I3Col.DefaultCellStyle = dataGridViewCellStyle16;
            this._I3Col.HeaderText = "I3";
            this._I3Col.Name = "_I3Col";
            this._I3Col.ReadOnly = true;
            this._I3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I3Col.Width = 22;
            // 
            // _i4Col
            // 
            this._i4Col.DataPropertyName = "I4";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._i4Col.DefaultCellStyle = dataGridViewCellStyle17;
            this._i4Col.HeaderText = "I4";
            this._i4Col.Name = "_i4Col";
            this._i4Col.ReadOnly = true;
            this._i4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i4Col.Width = 22;
            // 
            // _I5Col
            // 
            this._I5Col.DataPropertyName = "I5";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I5Col.DefaultCellStyle = dataGridViewCellStyle18;
            this._I5Col.HeaderText = "I5";
            this._I5Col.Name = "_I5Col";
            this._I5Col.ReadOnly = true;
            this._I5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I5Col.Width = 22;
            // 
            // _I6cCol
            // 
            this._I6cCol.DataPropertyName = "I6";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I6cCol.DefaultCellStyle = dataGridViewCellStyle19;
            this._I6cCol.HeaderText = "I6";
            this._I6cCol.Name = "_I6cCol";
            this._I6cCol.ReadOnly = true;
            this._I6cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I6cCol.Width = 22;
            // 
            // _I7Col
            // 
            this._I7Col.DataPropertyName = "I7";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I7Col.DefaultCellStyle = dataGridViewCellStyle20;
            this._I7Col.HeaderText = "I7";
            this._I7Col.Name = "_I7Col";
            this._I7Col.ReadOnly = true;
            this._I7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I7Col.Width = 22;
            // 
            // _I8Col
            // 
            this._I8Col.DataPropertyName = "I8";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I8Col.DefaultCellStyle = dataGridViewCellStyle21;
            this._I8Col.HeaderText = "I8";
            this._I8Col.Name = "_I8Col";
            this._I8Col.ReadOnly = true;
            this._I8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I8Col.Width = 22;
            // 
            // _I9Col
            // 
            this._I9Col.DataPropertyName = "I9";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I9Col.DefaultCellStyle = dataGridViewCellStyle22;
            this._I9Col.HeaderText = "I9";
            this._I9Col.Name = "_I9Col";
            this._I9Col.ReadOnly = true;
            this._I9Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I9Col.Width = 22;
            // 
            // _I10Col
            // 
            this._I10Col.DataPropertyName = "I10";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I10Col.DefaultCellStyle = dataGridViewCellStyle23;
            this._I10Col.HeaderText = "I10";
            this._I10Col.Name = "_I10Col";
            this._I10Col.ReadOnly = true;
            this._I10Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I10Col.Width = 28;
            // 
            // _I11Col
            // 
            this._I11Col.DataPropertyName = "I11";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I11Col.DefaultCellStyle = dataGridViewCellStyle24;
            this._I11Col.HeaderText = "I11";
            this._I11Col.Name = "_I11Col";
            this._I11Col.ReadOnly = true;
            this._I11Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I11Col.Width = 28;
            // 
            // _I12Col
            // 
            this._I12Col.DataPropertyName = "I12";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I12Col.DefaultCellStyle = dataGridViewCellStyle25;
            this._I12Col.HeaderText = "I12";
            this._I12Col.Name = "_I12Col";
            this._I12Col.ReadOnly = true;
            this._I12Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I12Col.Width = 28;
            // 
            // _I13Col
            // 
            this._I13Col.DataPropertyName = "I13";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I13Col.DefaultCellStyle = dataGridViewCellStyle26;
            this._I13Col.HeaderText = "I13";
            this._I13Col.Name = "_I13Col";
            this._I13Col.ReadOnly = true;
            this._I13Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I13Col.Width = 28;
            // 
            // _I14Col
            // 
            this._I14Col.DataPropertyName = "I14";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I14Col.DefaultCellStyle = dataGridViewCellStyle27;
            this._I14Col.HeaderText = "I14";
            this._I14Col.Name = "_I14Col";
            this._I14Col.ReadOnly = true;
            this._I14Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I14Col.Width = 28;
            // 
            // _I15Col
            // 
            this._I15Col.DataPropertyName = "I15";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I15Col.DefaultCellStyle = dataGridViewCellStyle28;
            this._I15Col.HeaderText = "I15";
            this._I15Col.Name = "_I15Col";
            this._I15Col.ReadOnly = true;
            this._I15Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I15Col.Width = 28;
            // 
            // _I16Col
            // 
            this._I16Col.DataPropertyName = "I16";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I16Col.DefaultCellStyle = dataGridViewCellStyle29;
            this._I16Col.HeaderText = "I16";
            this._I16Col.Name = "_I16Col";
            this._I16Col.ReadOnly = true;
            this._I16Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I16Col.Width = 28;
            // 
            // _I17Col
            // 
            this._I17Col.DataPropertyName = "I17";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I17Col.DefaultCellStyle = dataGridViewCellStyle30;
            this._I17Col.HeaderText = "I17";
            this._I17Col.Name = "_I17Col";
            this._I17Col.ReadOnly = true;
            this._I17Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I17Col.Width = 28;
            // 
            // _I18Col
            // 
            this._I18Col.DataPropertyName = "I18";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I18Col.DefaultCellStyle = dataGridViewCellStyle31;
            this._I18Col.HeaderText = "I18";
            this._I18Col.Name = "_I18Col";
            this._I18Col.ReadOnly = true;
            this._I18Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I18Col.Width = 28;
            // 
            // _I19Col
            // 
            this._I19Col.DataPropertyName = "I19";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I19Col.DefaultCellStyle = dataGridViewCellStyle32;
            this._I19Col.HeaderText = "I19";
            this._I19Col.Name = "_I19Col";
            this._I19Col.ReadOnly = true;
            this._I19Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I19Col.Width = 28;
            // 
            // _I20Col
            // 
            this._I20Col.DataPropertyName = "I20";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I20Col.DefaultCellStyle = dataGridViewCellStyle33;
            this._I20Col.HeaderText = "I20";
            this._I20Col.Name = "_I20Col";
            this._I20Col.ReadOnly = true;
            this._I20Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I20Col.Width = 28;
            // 
            // _I21Col
            // 
            this._I21Col.DataPropertyName = "I21";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I21Col.DefaultCellStyle = dataGridViewCellStyle34;
            this._I21Col.HeaderText = "I21";
            this._I21Col.Name = "_I21Col";
            this._I21Col.ReadOnly = true;
            this._I21Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I21Col.Width = 28;
            // 
            // _I22Col
            // 
            this._I22Col.DataPropertyName = "I22";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I22Col.DefaultCellStyle = dataGridViewCellStyle35;
            this._I22Col.HeaderText = "I22";
            this._I22Col.Name = "_I22Col";
            this._I22Col.ReadOnly = true;
            this._I22Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I22Col.Width = 28;
            // 
            // _I23Col
            // 
            this._I23Col.DataPropertyName = "I23";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I23Col.DefaultCellStyle = dataGridViewCellStyle36;
            this._I23Col.HeaderText = "I23";
            this._I23Col.Name = "_I23Col";
            this._I23Col.ReadOnly = true;
            this._I23Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I23Col.Width = 28;
            // 
            // _I24Col
            // 
            this._I24Col.DataPropertyName = "I24";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._I24Col.DefaultCellStyle = dataGridViewCellStyle37;
            this._I24Col.HeaderText = "I24";
            this._I24Col.Name = "_I24Col";
            this._I24Col.ReadOnly = true;
            this._I24Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I24Col.Width = 28;
            // 
            // _uaCol
            // 
            this._uaCol.DataPropertyName = "Ua";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._uaCol.DefaultCellStyle = dataGridViewCellStyle38;
            this._uaCol.HeaderText = "Ua";
            this._uaCol.Name = "_uaCol";
            this._uaCol.ReadOnly = true;
            this._uaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uaCol.Width = 27;
            // 
            // _ubCol
            // 
            this._ubCol.DataPropertyName = "Ub";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ubCol.DefaultCellStyle = dataGridViewCellStyle39;
            this._ubCol.HeaderText = "Ub";
            this._ubCol.Name = "_ubCol";
            this._ubCol.ReadOnly = true;
            this._ubCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ubCol.Width = 27;
            // 
            // _ucCol
            // 
            this._ucCol.DataPropertyName = "Uc";
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ucCol.DefaultCellStyle = dataGridViewCellStyle40;
            this._ucCol.HeaderText = "Uc";
            this._ucCol.Name = "_ucCol";
            this._ucCol.ReadOnly = true;
            this._ucCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ucCol.Width = 27;
            // 
            // _unCol
            // 
            this._unCol.DataPropertyName = "Un";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._unCol.DefaultCellStyle = dataGridViewCellStyle41;
            this._unCol.HeaderText = "Un";
            this._unCol.Name = "_unCol";
            this._unCol.ReadOnly = true;
            this._unCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._unCol.Width = 27;
            // 
            // _uabCol
            // 
            this._uabCol.DataPropertyName = "Uab";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._uabCol.DefaultCellStyle = dataGridViewCellStyle42;
            this._uabCol.HeaderText = "Uab";
            this._uabCol.Name = "_uabCol";
            this._uabCol.ReadOnly = true;
            this._uabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uabCol.Width = 33;
            // 
            // _ubcCol
            // 
            this._ubcCol.DataPropertyName = "Ubc";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ubcCol.DefaultCellStyle = dataGridViewCellStyle43;
            this._ubcCol.HeaderText = "Ubc";
            this._ubcCol.Name = "_ubcCol";
            this._ubcCol.ReadOnly = true;
            this._ubcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ubcCol.Width = 33;
            // 
            // _ucaCol
            // 
            this._ucaCol.DataPropertyName = "Uca";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ucaCol.DefaultCellStyle = dataGridViewCellStyle44;
            this._ucaCol.HeaderText = "Uca";
            this._ucaCol.Name = "_ucaCol";
            this._ucaCol.ReadOnly = true;
            this._ucaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ucaCol.Width = 33;
            // 
            // _3u0Col
            // 
            this._3u0Col.DataPropertyName = "3U0";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._3u0Col.DefaultCellStyle = dataGridViewCellStyle45;
            this._3u0Col.HeaderText = "3U0";
            this._3u0Col.Name = "_3u0Col";
            this._3u0Col.ReadOnly = true;
            this._3u0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._3u0Col.Width = 33;
            // 
            // _u2Col
            // 
            this._u2Col.DataPropertyName = "U2";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._u2Col.DefaultCellStyle = dataGridViewCellStyle46;
            this._u2Col.HeaderText = "U2";
            this._u2Col.Name = "_u2Col";
            this._u2Col.ReadOnly = true;
            this._u2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._u2Col.Width = 27;
            // 
            // _d1Col
            // 
            this._d1Col.DataPropertyName = "Д1-Д8";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d1Col.DefaultCellStyle = dataGridViewCellStyle47;
            this._d1Col.HeaderText = "Д1-Д8";
            this._d1Col.Name = "_d1Col";
            this._d1Col.ReadOnly = true;
            this._d1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d1Col.Width = 46;
            // 
            // _d2Col
            // 
            this._d2Col.DataPropertyName = "Д9-Д16";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d2Col.DefaultCellStyle = dataGridViewCellStyle48;
            this._d2Col.HeaderText = "Д9-Д16";
            this._d2Col.Name = "_d2Col";
            this._d2Col.ReadOnly = true;
            this._d2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d2Col.Width = 52;
            // 
            // _d3Col
            // 
            this._d3Col.DataPropertyName = "Д17-Д24";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d3Col.DefaultCellStyle = dataGridViewCellStyle49;
            this._d3Col.HeaderText = "Д17-Д24";
            this._d3Col.Name = "_d3Col";
            this._d3Col.ReadOnly = true;
            this._d3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d3Col.Width = 58;
            // 
            // _d4Col
            // 
            this._d4Col.DataPropertyName = "Д25-Д32";
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d4Col.DefaultCellStyle = dataGridViewCellStyle50;
            this._d4Col.HeaderText = "Д25-Д32";
            this._d4Col.Name = "_d4Col";
            this._d4Col.ReadOnly = true;
            this._d4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d4Col.Width = 58;
            // 
            // _d5Col
            // 
            this._d5Col.DataPropertyName = "Д33-Д40";
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d5Col.DefaultCellStyle = dataGridViewCellStyle51;
            this._d5Col.HeaderText = "Д33-Д40";
            this._d5Col.Name = "_d5Col";
            this._d5Col.ReadOnly = true;
            this._d5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d5Col.Width = 58;
            // 
            // _d6Col
            // 
            this._d6Col.DataPropertyName = "Д41-Д48";
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d6Col.DefaultCellStyle = dataGridViewCellStyle52;
            this._d6Col.HeaderText = "Д41-Д48";
            this._d6Col.Name = "_d6Col";
            this._d6Col.ReadOnly = true;
            this._d6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d6Col.Width = 58;
            // 
            // _d7Col
            // 
            this._d7Col.DataPropertyName = "Д49-Д56";
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d7Col.DefaultCellStyle = dataGridViewCellStyle53;
            this._d7Col.HeaderText = "Д49-Д56";
            this._d7Col.Name = "_d7Col";
            this._d7Col.ReadOnly = true;
            this._d7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d7Col.Width = 58;
            // 
            // _d8Col
            // 
            this._d8Col.DataPropertyName = "Д57-Д64";
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d8Col.DefaultCellStyle = dataGridViewCellStyle54;
            this._d8Col.HeaderText = "Д57-Д64";
            this._d8Col.Name = "_d8Col";
            this._d8Col.ReadOnly = true;
            this._d8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d8Col.Width = 58;
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 593);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iSH1TormColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iSH2TormColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _iPOTormColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _i4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I6cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I9Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I10Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I12Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I13Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I14Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I15Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I16Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I17Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I18Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I19Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I20Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I21Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I22Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I23Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I24Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ubCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ucCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _unCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ubcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ucaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _3u0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d8Col;
    }
}