﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901NEW.AlarmJournal.Structures;
using BEMN.MR901NEW.Configuration.Structures.Connections;
using BEMN.MR901NEW.Osc.Loaders;
using BEMN.MR901NEW.Properties;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR901NEW.AlarmJournal
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР901_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string DEVICE_NAME = "MR901";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private const string FAIL_READ_SIGN_RS =
            "Чтение подписей сигналов ЖА СПЛ по RS-485 невозмоэжно. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        #endregion [Constants]

        #region [Private fields]
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordUStruct> _alarmJournalU;
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;

        private readonly MemoryEntity<ConnectionsAndTransformer> _currentConnectionsMemory;
        private ConnectionsAndTransformer _currentConnectionsAndVoltage;

        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private MR901New _device;
        private List<string> _messages;
        private FileDriver _fileDriver;
        private List<ushort> _factors;
        private ushort _factorsDiffAndTorm;
        #endregion [Private fields]


        #region [Ctor's]
        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR901New device)
        {
            this.InitializeComponent();
            this._device = device;

            AllConnectionStruct.SetDeviceConnectionsType(Strings.DeviceType);

            this._currentConnectionsMemory = new MemoryEntity<ConnectionsAndTransformer>("Присоединения (ЖА)", device, 0x106C);
            this._currentConnectionsMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._currentConnectionsAndVoltage = this._currentConnectionsMemory.Value;
                this.CurrenOptionLoaded();
            });
            this._currentConnectionsMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._currentOptionsLoader = device.CurrentOptionsLoaderAj;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, ()=>
            {
                if (this._device.MB.BaudeRate < 921600)
                {
                    _currentConnectionsMemory.LoadStruct(new TimeSpan(100));
                }
                else
                {
                    _currentConnectionsMemory.LoadStruct();
                }
            });
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
            
            //Новая МР801, кака и ИР771, может читать за раз пачку сообщений, а не по одной штуке. читаемый размер не должен превышать 1024 слова.
            //Поэтому в лоадере идет чтение стректуры с пачкой до 14 сообщений(стерктура All. потому что в одном сообщении 72 слова. 72*14 = 1008 слов - влазим в размер.

            if (_device.AllAlarmJournal != null || _device.AllAlarmUJournal != null)
            {
                switch (Strings.DeviceType)
                {
                    case "T20N4D40R35":
                    case "T20N4D32R43":
                        this._journalLoader = new AlarmJournalLoader(device.AllAlarmUJournal, device.SetPageJa);
                        this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
                        this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
                        break;
                    default:
                        this._journalLoader = new AlarmJournalLoader(device.AllAlarmJournal, device.SetPageJa);
                        this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
                        this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
                        break;
                }
                
            }
            else
            {
                switch (Strings.DeviceType)
                {
                    case "T20N4D40R35":
                    case "T20N4D32R43":
                        this._alarmJournalU = device.AlarmJournalU;
                        this._alarmJournalU.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecords);
                        this._alarmJournalU.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                        {
                            if (this._failCounter < 80)
                            {
                                this._failCounter++;
                            }
                            else
                            {
                                this._alarmJournal.RemoveStructQueries();
                                this._failCounter = 0;
                                this.ButtonsEnabled = true;
                            }
                        });
                        this._setPageAlarmJournal = device.SetPageJa;
                        this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                        {
                            this._alarmJournalU.LoadStruct(new TimeSpan(100));
                        });
                        this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
                        break;
                    default:
                        this._alarmJournal = device.AlarmJournal;
                        this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecords);
                        this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                        {
                            if (this._failCounter < 80)
                            {
                                this._failCounter++;
                            }
                            else
                            {
                                this._alarmJournal.RemoveStructQueries();
                                this._failCounter = 0;
                                this.ButtonsEnabled = true;
                            }
                        });
                        this._setPageAlarmJournal = device.SetPageJa;
                        this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                        {
                            this._alarmJournal.LoadStruct(new TimeSpan(100));
                        });
                        this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
                        break;
                }
               
            }


            this._fileDriver = new FileDriver(device, this);
        }
        #endregion [Ctor's]


        #region [Help members]

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals)serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._messages = PropFormsStrings.GetNewAlarmList();
                }
            }
            catch
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }
            this.StartReadOption();
        }

        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void CurrenOptionLoaded()
        {
            this._table.Clear();
            this._recordNumber = this._failCounter = 0;
            if (this._device.AllAlarmJournal != null || this._device.AllAlarmUJournal != null)
            {
                this._journalLoader.Clear();
                this._journalLoader.StartRead();
            }
            else
            {
                this.SavePageNumber();
            }

        }

        private void ReadRecords()
        {
            try
            {
                if (this._alarmJournal != null)
                {
                    if (!this._alarmJournal.Value.IsEmpty)
                    {
                        this._recordNumber++;
                        this._failCounter = this._recordNumber;

                        AlarmJournalRecordStruct record = this._alarmJournal.Value;
                        AllConnectionStruct measure = this._currentOptionsLoader[record.GroupOfSetpoint];

                        _factors = measure.AllItt.ToList();
                        _factorsDiffAndTorm = measure.IttJoin;

                        string triggeredDef = this.GetTriggeredDefence(record);
                        string parameter = this.GetTriggeredParam(record);
                        string parametrValue = this.GetParametrValue(record, _factors);

                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            Strings.AlarmJournalMessage[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            Strings.GroupsNames[record.GroupOfSetpoint],
                            GetCurrents(record.IdSH1, _factors, 3),
                            GetCurrents(record.ItSH1, _factors, 3),
                            GetCurrents(record.IdSH2, _factors, 3),
                            GetCurrents(record.ItSH2, _factors, 3),
                            GetCurrents(record.IdPO, _factors, 3),
                            GetCurrents(record.ItPO, _factors, 3),
                            GetCurrents(record.I1, _factors, 3),
                            GetCurrents(record.I2, _factors, 3),
                            GetCurrents(record.I3, _factors, 3),
                            GetCurrents(record.I4, _factors, 3),
                            GetCurrents(record.I5, _factors, 3),
                            GetCurrents(record.I6, _factors, 3),
                            GetCurrents(record.I7, _factors, 3),
                            GetCurrents(record.I8, _factors, 3),
                            GetCurrents(record.I9, _factors, 3),
                            GetCurrents(record.I10, _factors, 3),
                            GetCurrents(record.I11, _factors, 3),
                            GetCurrents(record.I12, _factors, 3),
                            GetCurrents(record.I13, _factors, 3),
                            GetCurrents(record.I14, _factors, 3),
                            GetCurrents(record.I15, _factors, 3),
                            GetCurrents(record.I16, _factors, 3),
                            GetCurrents(record.I17, _factors, 3),
                            GetCurrents(record.I18, _factors, 3),
                            GetCurrents(record.I19, _factors, 3),
                            GetCurrents(record.I20, _factors, 3),
                            GetCurrents(record.I21, _factors, 3),
                            GetCurrents(record.I22, _factors, 3),
                            GetCurrents(record.I23, _factors, 3),
                            GetCurrents(record.I24, _factors, 3),

                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56,
                            record.D57To64
                        );

                        this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                        this.statusStrip1.Update();
                        this.SavePageNumber();
                    }
                    else
                    {
                        if (this._table.Rows.Count == 0)
                        {
                            this._statusLabel.Text = JOURNAL_IS_EMPTY;
                        }
                        this.ButtonsEnabled = true;
                    }
                }
                else
                {
                    if (!this._alarmJournalU.Value.IsEmpty)
                    {
                        this._recordNumber++;
                        this._failCounter = this._recordNumber;

                        AlarmJournalRecordUStruct record = this._alarmJournalU.Value;
                        AllConnectionStruct measures = this._currentOptionsLoader[record.GroupOfSetpoint];
                        AllConnectionStruct measure = _currentConnectionsAndVoltage.Connections;

                        _factors = measures.AllItt.Skip(1).ToList();
                        _factorsDiffAndTorm = measures.IttJoin;

                        string triggeredDef = this.GetTriggeredDefence(record);
                        string parameter = this.GetTriggeredParam(record);
                        string parametrValue = this.GetParametrValue(record, _factors, _currentConnectionsAndVoltage);

                        this._table.Rows.Add
                        (
                             this._recordNumber,
                            record.GetTime,
                            Strings.AlarmJournalMessage[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            Strings.GroupsNames[record.GroupOfSetpoint],
                            GetCurrents(record.IdSH1, _factors, 3),
                            GetCurrents(record.ItSH1, _factors, 3),
                            GetCurrents(record.IdSH2, _factors, 3),
                            GetCurrents(record.ItSH2, _factors, 3),
                            GetCurrents(record.IdPO, _factors, 3),
                            GetCurrents(record.ItPO, _factors, 3),
                            GetCurrents(record.I1, _factors, 3),
                            GetCurrents(record.I2, _factors, 3),
                            GetCurrents(record.I3, _factors, 3),
                            GetCurrents(record.I4, _factors, 3),
                            GetCurrents(record.I5, _factors, 3),
                            GetCurrents(record.I6, _factors, 3),
                            GetCurrents(record.I7, _factors, 3),
                            GetCurrents(record.I8, _factors, 3),
                            GetCurrents(record.I9, _factors, 3),
                            GetCurrents(record.I10, _factors, 3),
                            GetCurrents(record.I11, _factors, 3),
                            GetCurrents(record.I12, _factors, 3),
                            GetCurrents(record.I13, _factors, 3),
                            GetCurrents(record.I14, _factors, 3),
                            GetCurrents(record.I15, _factors, 3),
                            GetCurrents(record.I16, _factors, 3),
                            GetCurrents(record.I17, _factors, 3),
                            GetCurrents(record.I18, _factors, 3),
                            GetCurrents(record.I19, _factors, 3),
                            GetCurrents(record.I20, _factors, 3),

                            0,
                            0,
                            0,
                            0,

                            GetVoltage(record.Ua, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.Ub, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.Uc, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.Un, _currentConnectionsAndVoltage.TN.KthlValue),

                            GetVoltage(record.Uab, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.Ubc, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.Uca, _currentConnectionsAndVoltage.TN.KthlValue),

                            //GetVoltage(record.U1, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.U30, _currentConnectionsAndVoltage.TN.KthlValue),
                            GetVoltage(record.U2, _currentConnectionsAndVoltage.TN.KthlValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56,
                            record.D57To64
                        );

                        this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                        this.statusStrip1.Update();
                        this.SavePageNumber();
                    }
                    else
                    {
                        if (this._table.Rows.Count == 0)
                        {
                            this._statusLabel.Text = JOURNAL_IS_EMPTY;
                        }
                        this.ButtonsEnabled = true;
                    }
                }
                
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                (
                    this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", ""
                );
            }
        }

        private void ReadAllRecords()
        {
            if (this._journalLoader.JournalRecords?.Count == 0 || this._journalLoader.JournalRecordsU?.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }

            this._recordNumber = 1;

            switch (Strings.DeviceType)
            {
                case "T20N4D40R35":
                case "T20N4D32R43":
                    foreach (AlarmJournalRecordUStruct record in this._journalLoader.JournalRecordsU)
                    {
                        try
                        {
                            AllConnectionStruct measures = this._currentOptionsLoader[record.GroupOfSetpoint];
                            AllConnectionStruct measure = _currentConnectionsAndVoltage.Connections;

                            _factors = measures.AllItt.Skip(1).ToList();
                            _factorsDiffAndTorm = measures.IttJoin;

                            string triggeredDef = this.GetTriggeredDefence(record);
                            string parameter = this.GetTriggeredParam(record);
                            string parametrValue = this.GetParametrValue(record, _factors, _currentConnectionsAndVoltage);

                            // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],
                                GetCurrents(record.IdSH1, _factors, 3),
                                GetCurrents(record.ItSH1, _factors, 3),
                                GetCurrents(record.IdSH2, _factors, 3),
                                GetCurrents(record.ItSH2, _factors, 3),
                                GetCurrents(record.IdPO, _factors, 3),
                                GetCurrents(record.ItPO, _factors, 3),
                                GetCurrents(record.I1, _factors, 3),
                                GetCurrents(record.I2, _factors, 3),
                                GetCurrents(record.I3, _factors, 3),
                                GetCurrents(record.I4, _factors, 3),
                                GetCurrents(record.I5, _factors, 3),
                                GetCurrents(record.I6, _factors, 3),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 3),
                                GetCurrents(record.I11, _factors, 3),
                                GetCurrents(record.I12, _factors, 3),
                                GetCurrents(record.I13, _factors, 3),
                                GetCurrents(record.I14, _factors, 3),
                                GetCurrents(record.I15, _factors, 3),
                                GetCurrents(record.I16, _factors, 3),
                                GetCurrents(record.I17, _factors, 3),
                                GetCurrents(record.I18, _factors, 3),
                                GetCurrents(record.I19, _factors, 3),
                                GetCurrents(record.I20, _factors, 3),

                                0, 
                                0, 
                                0, 
                                0,

                                GetVoltage(record.Ua, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Ub, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Uc, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Un, _currentConnectionsAndVoltage.TN.KthlValue),

                                GetVoltage(record.Uab, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Ubc, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Uca, _currentConnectionsAndVoltage.TN.KthlValue),

                                //GetVoltage(record.U1, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.U30, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.U2, _currentConnectionsAndVoltage.TN.KthlValue),

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40,
                                record.D41To48,
                                record.D49To56,
                                record.D57To64
                            );

                        }
                        catch (Exception e)
                        {
                            this._table.Rows.Add
                            (
                                this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", ""
                            );
                        }
                        this._recordNumber++;
                    }
                    break;
                default:
                    foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
                    {
                        try
                        {
                            AllConnectionStruct measure = this._currentOptionsLoader[record.GroupOfSetpoint];

                            _factors = measure.AllItt;

                            //for (int i = 0; i < _factors.Count; i++)
                            //{
                            //    _factors[i] = (ushort)(_factors[i] * 40);
                            //}

                            string triggeredDef = this.GetTriggeredDefence(record);
                            string parameter = this.GetTriggeredParam(record);
                            string parametrValue = this.GetParametrValue(record, _factors);

                            // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],
                                GetCurrents(record.IdSH1, _factors, 3),
                                GetCurrents(record.ItSH1, _factors, 3),
                                GetCurrents(record.IdSH2, _factors, 3),
                                GetCurrents(record.ItSH2, _factors, 3),
                                GetCurrents(record.IdPO, _factors, 3),
                                GetCurrents(record.ItPO, _factors, 3),
                                GetCurrents(record.I1, _factors, 3),
                                GetCurrents(record.I2, _factors, 3),
                                GetCurrents(record.I3, _factors, 3),
                                GetCurrents(record.I4, _factors, 3),
                                GetCurrents(record.I5, _factors, 3),
                                GetCurrents(record.I6, _factors, 3),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 3),
                                GetCurrents(record.I11, _factors, 3),
                                GetCurrents(record.I12, _factors, 3),
                                GetCurrents(record.I13, _factors, 3),
                                GetCurrents(record.I14, _factors, 3),
                                GetCurrents(record.I15, _factors, 3),
                                GetCurrents(record.I16, _factors, 3),
                                GetCurrents(record.I17, _factors, 3),
                                GetCurrents(record.I18, _factors, 3),
                                GetCurrents(record.I19, _factors, 3),
                                GetCurrents(record.I20, _factors, 3),
                                GetCurrents(record.I21, _factors, 3),
                                GetCurrents(record.I22, _factors, 3),
                                GetCurrents(record.I23, _factors, 3),
                                GetCurrents(record.I24, _factors, 3),

                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40,
                                record.D41To48,
                                record.D49To56,
                                record.D57To64
                            );

                        }
                        catch (Exception e)
                        {
                            this._table.Rows.Add
                            (
                                this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", ""
                            );
                        }
                        this._recordNumber++;
                    }
                    break;
            }

           
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber - 1);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredParam(AlarmJournalRecordStruct record)
        {
            return record.NumberOfTriggeredParametr == 48 ? this._messages[record.ValueOfTriggeredParametr] : Strings.AlarmJournalTrigged[record.NumberOfTriggeredParametr];
        }

        private string GetTriggeredParam(AlarmJournalRecordUStruct record)
        {
            return record.NumberOfTriggeredParametr == 48 ? this._messages[record.ValueOfTriggeredParametr] : Strings.AlarmJournalTrigged[record.NumberOfTriggeredParametr];
        }

        private string GetVoltage(ushort value, double koef)
        {
            return ValuesConverterCommon.Analog.GetU(value, koef);
        }

        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private void ReadRecordFromBinFile()
        {
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }

            this._recordNumber = 1;

            switch (Strings.DeviceType)
            {
                case "T20N4D40R35":
                case "T20N4D32R43":
                    foreach (AlarmJournalRecordUStruct record in this._journalLoader.JournalRecordsU)
                    {
                        try
                        {
                            AllConnectionStruct measures = this._currentOptionsLoader[record.GroupOfSetpoint];
                            AllConnectionStruct measure = _currentConnectionsAndVoltage.Connections;

                            _factors = measures.AllItt.Skip(1).ToList();
                            _factorsDiffAndTorm = measures.IttJoin;

                            string triggeredDef = this.GetTriggeredDefence(record);
                            string parameter = this.GetTriggeredParam(record);
                            string parametrValue = this.GetParametrValue(record, _factors, _currentConnectionsAndVoltage);

                            // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],
                                GetCurrents(record.IdSH1, _factors, 3),
                                GetCurrents(record.ItSH1, _factors, 3),
                                GetCurrents(record.IdSH2, _factors, 3),
                                GetCurrents(record.ItSH2, _factors, 3),
                                GetCurrents(record.IdPO, _factors, 3),
                                GetCurrents(record.ItPO, _factors, 3),
                                GetCurrents(record.I1, _factors, 3),
                                GetCurrents(record.I2, _factors, 3),
                                GetCurrents(record.I3, _factors, 3),
                                GetCurrents(record.I4, _factors, 3),
                                GetCurrents(record.I5, _factors, 3),
                                GetCurrents(record.I6, _factors, 3),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 3),
                                GetCurrents(record.I11, _factors, 3),
                                GetCurrents(record.I12, _factors, 3),
                                GetCurrents(record.I13, _factors, 3),
                                GetCurrents(record.I14, _factors, 3),
                                GetCurrents(record.I15, _factors, 3),
                                GetCurrents(record.I16, _factors, 3),
                                GetCurrents(record.I17, _factors, 3),
                                GetCurrents(record.I18, _factors, 3),
                                GetCurrents(record.I19, _factors, 3),
                                GetCurrents(record.I20, _factors, 3),

                                0,
                                0,
                                0,
                                0,

                                GetVoltage(record.Ua, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Ub, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Uc, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Uab, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Ubc, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Uca, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.Un, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.U30, _currentConnectionsAndVoltage.TN.KthlValue),
                                GetVoltage(record.U2, _currentConnectionsAndVoltage.TN.KthlValue),

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40,
                                record.D41To48,
                                record.D49To56,
                                record.D57To64
                            );

                        }
                        catch (Exception e)
                        {
                            this._table.Rows.Add
                            (
                                this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", ""
                            );
                        }
                        this._recordNumber++;
                    }
                    break;
                default:
                    foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
                    {
                        try
                        {
                            AllConnectionStruct measure = this._currentOptionsLoader[record.GroupOfSetpoint];

                            _factors = measure.AllItt;

                            for (int i = 0; i < _factors.Count; i++)
                            {
                                _factors[i] = (ushort)(_factors[i] * 40);
                            }

                            string triggeredDef = this.GetTriggeredDefence(record);
                            string parameter = this.GetTriggeredParam(record);
                            string parametrValue = this.GetParametrValue(record, _factors);

                            // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                            this._table.Rows.Add
                            (
                                this._recordNumber,
                                record.GetTime,
                                Strings.AlarmJournalMessage[record.Message],
                                triggeredDef,
                                parameter,
                                parametrValue,
                                Strings.GroupsNames[record.GroupOfSetpoint],
                                GetCurrents(record.IdSH1, _factors, 3),
                                GetCurrents(record.ItSH1, _factors, 3),
                                GetCurrents(record.IdSH2, _factors, 3),
                                GetCurrents(record.ItSH2, _factors, 3),
                                GetCurrents(record.IdPO, _factors, 3),
                                GetCurrents(record.ItPO, _factors, 3),
                                GetCurrents(record.I1, _factors, 3),
                                GetCurrents(record.I2, _factors, 3),
                                GetCurrents(record.I3, _factors, 3),
                                GetCurrents(record.I4, _factors, 3),
                                GetCurrents(record.I5, _factors, 3),
                                GetCurrents(record.I6, _factors, 3),
                                GetCurrents(record.I7, _factors, 3),
                                GetCurrents(record.I8, _factors, 3),
                                GetCurrents(record.I9, _factors, 3),
                                GetCurrents(record.I10, _factors, 3),
                                GetCurrents(record.I11, _factors, 3),
                                GetCurrents(record.I12, _factors, 3),
                                GetCurrents(record.I13, _factors, 3),
                                GetCurrents(record.I14, _factors, 3),
                                GetCurrents(record.I15, _factors, 3),
                                GetCurrents(record.I16, _factors, 3),
                                GetCurrents(record.I17, _factors, 3),
                                GetCurrents(record.I18, _factors, 3),
                                GetCurrents(record.I19, _factors, 3),
                                GetCurrents(record.I20, _factors, 3),
                                GetCurrents(record.I21, _factors, 3),
                                GetCurrents(record.I22, _factors, 3),
                                GetCurrents(record.I23, _factors, 3),
                                GetCurrents(record.I24, _factors, 3),

                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,

                                record.D1To8,
                                record.D9To16,
                                record.D17To24,
                                record.D25To32,
                                record.D33To40,
                                record.D41To48,
                                record.D49To56,
                                record.D57To64
                            );

                        }
                        catch (Exception e)
                        {
                            this._table.Rows.Add
                            (
                                this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                                "", "", "", "", "", "", "", "", "", "", ""
                            );
                        }
                        this._recordNumber++;
                    }
                    break;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredDefence(AlarmJournalRecordStruct record)
        {
            return record.GetTriggedOption;
        }

        private string GetTriggeredDefence(AlarmJournalRecordUStruct record)
        {
            return record.GetTriggedOption;
        }

        private string GetParametrValue(AlarmJournalRecordStruct record, List<ushort> factors)
        {
            string value = record.GetValueTriggedOption(factors);
            return value;
        }

        private string GetParametrValue(AlarmJournalRecordUStruct record, List<ushort> factors, ConnectionsAndTransformer cnt)
        {
            string value = record.GetValueTriggedOption(factors, cnt);
            return value;
        }

        private string GetCurrents(ushort value, List<ushort> factors, int index)
        {
            return ValuesConverterCommon.Analog.GetI901(value, factors[index]);
        }

        private string GetCurrents(ushort value, ushort factors)
        {
            return ValuesConverterCommon.Analog.GetI901(value, factors);
        }

        private DataTable GetJournalDataTable()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                _device.Info.DeviceConfiguration = _device.DevicePlant;
            }

            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].HeaderText);
            }

            switch (_device.Info.DeviceConfiguration)
            {
                case "T16N0D64R43":
                    this._alarmJournalGrid.Columns["_I17Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I18Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I19Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I20Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I21Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I22Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I23Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I24Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_uaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_unCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_uabCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubcCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_3u0Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_u2Col"].Visible = false;
                    break;
                case "T24N0D40R35":
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_uaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_unCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_uabCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubcCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_3u0Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_u2Col"].Visible = false;
                    break;
                case "T24N0D24R51":
                    this._alarmJournalGrid.Columns["_d4Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_uaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_unCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_uabCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubcCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_3u0Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_u2Col"].Visible = false;
                    break;
                case "T24N0D32R43":
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_uaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_unCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_uabCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubcCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_3u0Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_u2Col"].Visible = false;
                    break;
                case "T20N4D40R35":
                    this._alarmJournalGrid.Columns["_I21Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I22Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I23Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I24Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    break;
                case "T20N4D32R43":
                    this._alarmJournalGrid.Columns["_I21Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I22Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I23Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I24Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    break;
                case "T16N0D24R19":
                    this._alarmJournalGrid.Columns["_I17Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I18Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I19Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I20Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I21Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I22Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I23Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_I24Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d4Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d5Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d6Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d7Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_d8Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_uaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_unCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_uabCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ubcCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_ucaCol"].Visible = false;
                    this._alarmJournalGrid.Columns["_3u0Col"].Visible = false;
                    this._alarmJournalGrid.Columns["_u2Col"].Visible = false;
                    break;
            }
            
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;

            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                switch (_device.ConnectionPort)
                {
                    case 2:
                        MessageBox.Show(FAIL_READ_SIGN_RS, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this._messages = PropFormsStrings.GetNewAlarmList();
                        this.StartReadOption();
                        break;
                    default:
                        this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                        break;
                }
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void _readAlarmJournalButtonClick(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                switch (_device.ConnectionPort)
                {
                    case 2:
                        MessageBox.Show(FAIL_READ_SIGN_RS, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this._messages = PropFormsStrings.GetNewAlarmList();
                        this.StartReadOption();
                        break;
                    default:
                        this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                        break;
                }
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void StartReadOption()
        {
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            int j = 0;
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int countRecord = journalBytes.Count / size;
                for (int i = 0; j < countRecord - 2; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                    this.ReadRecordFromBinFile();
                    j++;
                }
            }
            else
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = "Журнал загружен. " + string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            this._saveAlarmJournalDialog.FileName = $"Журнал аварий МР801 {this._device.DevicePlant} v{this._device.DeviceVersion.Replace(".", "_")}";

            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            this._saveJournalHtmlDialog.FileName = $"Журнал аварий МР801 {this._device.DevicePlant} v{this._device.DeviceVersion.Replace(".", "_")}";

            if (this._saveJournalHtmlDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveJournalHtmlDialog.FileName, "МР901. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }

            //if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            //{
            //    HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR7AJ);
            //    this._statusLabel.Text = JOURNAL_SAVED;
            //}
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosedEventArgs e)
        {
            if (this._alarmJournal != null)
            {
                this._alarmJournal.RemoveStructQueries();
            }
            if (this._journalLoader != null)
            {
                this._journalLoader.ClearEvents();
            }
        }
        #endregion [Event Handlers]
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR901New); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]
        
    }

}
