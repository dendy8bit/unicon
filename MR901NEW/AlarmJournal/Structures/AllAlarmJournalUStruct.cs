﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR901NEW.AlarmJournal.Structures
{
    public class AllAlarmJournalUStruct : StructBase
    {
        [Layout(0, Count = 15)] AlarmJournalRecordUStruct[] _alarmJournalRecords;

        public List<AlarmJournalRecordUStruct> AllJournalRecords
        {
            get { return new List<AlarmJournalRecordUStruct>(this._alarmJournalRecords.Where(j=>!j.IsEmpty));}
        }
    }
}
