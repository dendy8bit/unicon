﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR901NEW.Configuration.Structures.Connections;

namespace BEMN.MR901NEW.AlarmJournal.Structures
{
    public class AlarmJournalRecordUStruct: StructBase
    {
        #region [Constants]
        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        private const string SPL_PATTERN = "СПЛ {0}"; 
        #endregion [Constants]


        #region [Static fields]
        /// <summary>
        /// Номер сообщения
        /// </summary>
        public static int Number = 0; 
        #endregion [Static fields]


        #region [Private fields]
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _stage;
        [Layout(9)] private ushort _numberOfTriggeredParametr;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _groupOfSetpoints;

        [Layout(12)] private ushort _ida1;
        [Layout(13)] private ushort _idb1;
        [Layout(14)] private ushort _idc1;

        [Layout(15)] private ushort _ida2;
        [Layout(16)] private ushort _idb2;
        [Layout(17)] private ushort _idc2;

        [Layout(18)] private ushort _ida3;
        [Layout(19)] private ushort _idb3;
        [Layout(20)] private ushort _idc3;

        [Layout(21)] private ushort _ita1;
        [Layout(22)] private ushort _itb1;
        [Layout(23)] private ushort _itc1;

        [Layout(24)] private ushort _ita2;
        [Layout(25)] private ushort _itb2;
        [Layout(26)] private ushort _itc2;

        [Layout(27)] private ushort _ita3;
        [Layout(28)] private ushort _itb3;
        [Layout(29)] private ushort _itc3;

        [Layout(30)] private ushort _i1;
        [Layout(31)] private ushort _i2;
        [Layout(32)] private ushort _i3;
        [Layout(33)] private ushort _i4;
        [Layout(34)] private ushort _i5;
        [Layout(35)] private ushort _i6;
        [Layout(36)] private ushort _i7;
        [Layout(37)] private ushort _i8;
        [Layout(38)] private ushort _i9;
        [Layout(39)] private ushort _i10;
        [Layout(40)] private ushort _i11;
        [Layout(41)] private ushort _i12;
        [Layout(42)] private ushort _i13;
        [Layout(43)] private ushort _i14;
        [Layout(44)] private ushort _i15;
        [Layout(45)] private ushort _i16;
        [Layout(46)] private ushort _i17;
        [Layout(47)] private ushort _i18;
        [Layout(48)] private ushort _i19;
        [Layout(49)] private ushort _i20;
        [Layout(50)] private ushort _ua;
        [Layout(51)] private ushort _ub;
        [Layout(52)] private ushort _uc;
        [Layout(53)] private ushort _un;
        [Layout(54)] private ushort _d1;
        [Layout(55)] private ushort _d2;
        [Layout(56)] private ushort _d3;
        [Layout(57)] private ushort _d4;
        [Layout(58)] private ushort _d5;
        [Layout(59)] private ushort _d6;
        [Layout(60)] private ushort _spl;
        [Layout(61)] private ushort _uab;
        [Layout(62)] private ushort _ubc;
        [Layout(63)] private ushort _uca;
        [Layout(64)] private ushort _3u0;
        [Layout(65)] private ushort _u2;
        [Layout(66)] private ushort _u1Reserve;
        [Layout(67)] private ushort _reserve;
        #endregion [Private fields]


        public ushort Year => this._year;
        public ushort Month => this._month;
        public ushort Date => this._date;
        public ushort Hour => this._hour;
        public ushort Minute => this._minute;
        public ushort Second => this._second;
        public ushort Millisecond => this._millisecond;
        public ushort Message => this._message;


        public ushort IdSH1 => _ida1;
        public ushort IdSH2 => _ida2;
        public ushort IdPO => _ida3;
        public ushort ItSH1 => _ita1;
        public ushort ItSH2 => _ita2;
        public ushort ItPO => _ita3;

        public ushort I1 => _i1;
        public ushort I2 => _i2;
        public ushort I3 => _i3;
        public ushort I4 => _i4;
        public ushort I5 => _i5;
        public ushort I6 => _i6;
        public ushort I7 => _i7;
        public ushort I8 => _i8;
        public ushort I9 => _i9;
        public ushort I10 => _i10;
        public ushort I11 => _i11;
        public ushort I12 => _i12;
        public ushort I13 => _i13;
        public ushort I14 => _i14;
        public ushort I15 => _i15;
        public ushort I16 => _i16;
        public ushort I17 => _i17;
        public ushort I18 => _i18;
        public ushort I19 => _i19;
        public ushort I20 => _i20;
        public ushort Ua => _ua;
        public ushort Ub => _ub;
        public ushort Uc => _uc;
        public ushort Un => _un;
        public ushort Uab => _uab;
        public ushort Ubc => _ubc;
        public ushort Uca => _uca;
        public ushort U30 => _3u0;
        public ushort U2 => _u2;
        public ushort U1 => _u1Reserve;

        public ushort D1 => this._d1;
        public ushort D2 => this._d2;
        public ushort D3 => this._d3;
        public ushort D4 => this._d4;
        public ushort D5 => this._d5;
        public ushort D6 => this._d6;


        public string D1To8 => Common.ByteToMask(Common.LOBYTE(this.D1), true);

        public string D9To16 => Common.ByteToMask(Common.HIBYTE(this.D1), true);

        public string D17To24 => Common.ByteToMask(Common.LOBYTE(this.D2), true);

        public string D25To32 => Common.ByteToMask(Common.HIBYTE(this.D2), true);

        public string D33To40 => Common.ByteToMask(Common.LOBYTE(this.D3), true);

        public string D41To48 => Common.ByteToMask(Common.HIBYTE(this.D3), true);

        public string D49To56 => Common.ByteToMask(Common.LOBYTE(this.D4), true);
        public string D57To64 => Common.ByteToMask(Common.HIBYTE(this.D4), true);

        /// <summary>
        /// Сработанная ступень
        /// </summary>
        public string GetTriggedOption => Strings.AlarmJournalStage[this._stage];

        public string GetTriggParam => Strings.AlarmJournalTrigged[_numberOfTriggeredParametr];

        public ushort GroupOfSetpoint => _groupOfSetpoints;

        public ushort NumberOfTriggeredParametr => _numberOfTriggeredParametr;

        public ushort ValueOfTriggeredParametr => _valueOfTriggeredParametr;

        /// <summary>
        /// //Значение сработанного параметра
        /// </summary>
        public string GetValueTriggedOption(List<ushort> factors, ConnectionsAndTransformer cnt)
        {
            if (this._numberOfTriggeredParametr < 18)
                return ValuesConverterCommon.Analog.GetI901(this._valueOfTriggeredParametr, factors[3]);

            if (this._numberOfTriggeredParametr < 38)
            {
                int parametr = this._numberOfTriggeredParametr - 18;
                return ValuesConverterCommon.Analog.GetI901(this._valueOfTriggeredParametr, factors[3]);
            }

            if (this._numberOfTriggeredParametr >= 38 && this._numberOfTriggeredParametr < 41)
            {
                return ValuesConverterCommon.Analog.GetU(this._valueOfTriggeredParametr, cnt.TN.KthlValue);
            }
            if (this._numberOfTriggeredParametr == 41)
            {
                return ValuesConverterCommon.Analog.GetU(this._valueOfTriggeredParametr, cnt.TN.KthxValue);
            }

            if (this._numberOfTriggeredParametr < 44)
                return Strings.Otkl[this._valueOfTriggeredParametr];
            if (this._numberOfTriggeredParametr == 44)
                return string.Format(SPL_PATTERN, this._valueOfTriggeredParametr);
            // Uab, Ubc..
            if (this._numberOfTriggeredParametr > 45 && this._numberOfTriggeredParametr < 48)
            {
                return ValuesConverterCommon.Analog.GetU(this._valueOfTriggeredParametr, cnt.TN.KthlValue);
            }
            if (this._numberOfTriggeredParametr == 48)
            {
                return string.Format("ЖА СПЛ {0}", this._valueOfTriggeredParametr);
            }
            return string.Empty;
        }


        //для того, чтобы вычитывать одним запросом всю запись, потому что в устройстве есть автоинкремент
        //public override object GetSlots(ushort start, bool slotArray, int slotLen)
        //{
        //    slotLen = GetSize() / 2;
        //    return base.GetSlots(start, slotArray, slotLen);
        //}

        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year + this.Month + this.Date + this.Hour + this.Minute + this.Second + this.Millisecond
                          + this._message + this._valueOfTriggeredParametr;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime => string.Format
        (
            DATE_TIME_PATTERN,
            this.Date,
            this.Month,
            this.Year,
            this.Hour,
            this.Minute,
            this.Second,
            this.Millisecond
        );
    }
}
