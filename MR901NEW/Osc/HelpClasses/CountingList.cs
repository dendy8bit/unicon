using BEMN.MBServer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BEMN.MR901NEW.Configuration.Structures.Connections;
using BEMN.MR901NEW.Configuration.Structures.Ls;
using BEMN.MR901NEW.Configuration.Structures.Oscope;
using BEMN.MR901NEW.Osc.Structures;

namespace BEMN.MR901NEW.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        private const ushort CURRENTS_MAX = 24;
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        private int _countingSize;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private int _discretsCount;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private int _channelsCount;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        private int _currentsCount;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        private int _voltagesCount;
        /// <summary>
        /// ��1 ��2 �� (����. � ����. ����)
        /// </summary>
        private int _currentsVirtCount;

        private List<string> _iNames;
        private string[] _uNames;

        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;

        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private short[][] _baseCurrents;

        /// <summary>
        /// ������������ �������� ����� ���� � ����
        /// </summary>
        private ushort[][] _baseCurrentsDiff;

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double _minI;

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double _maxI;

        /// <summary>
        /// ����������� ��� ���� � ����
        /// </summary>
        public double _minIDiff;

        /// <summary>
        /// ������������ ��� ���� � ����
        /// </summary>
        public double _maxIDiff;

        /// <summary>
        /// ������������
        /// </summary>
        public double[][] _currents;

        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;

        private OscJournalStruct _oscJournalStruct;

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] _channels;

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] _discrets;

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int _count;

        private ChannelWithBase[] _channelsWithBase;

        private string _hdrString;

        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private List<double> _currentsKoefs;

        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private List<double> _voltageKoefs;

        private MR901New _device;

        private List<bool> _diffAndTormCurrent;

        #region [Ctor's]

        public CountingList(MR901New device, ushort[] pageValue, OscJournalStruct oscJournalStruct, ConnectionsAndTransformer cnt, OscopeConfigStruct oscConfig)
        {
            this._device = device;

            OscopeAllChannelsStruct.SetDeviceChannelsType(Strings.DeviceType);

            _diffAndTormCurrent = new List<bool>();
            _diffAndTormCurrent.Add(oscConfig.SH1);
            _diffAndTormCurrent.Add(oscConfig.SH2);
            _diffAndTormCurrent.Add(oscConfig.PO);

            this._channelsWithBase = this._device.AllChannels.Value.Rows;
            this._oscJournalStruct = oscJournalStruct;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;

            this.GetCountingSettings(this._device.OscOptions.Value);

            this._countingArray = new ushort[this._countingSize][];
            //����� ���������� ��������
            this._count = pageValue.Length / this._countingSize;
            //������������� �������
            for (int i = 0; i < this._countingSize; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == this._countingSize)
                {
                    m++;
                    n = 0;
                }
            }
            //����
            this.SetCountingCurrents(cnt);

            //����������
            if (this._voltagesCount != 0)
            {
                this.SetCountingVoltage(cnt.TN);
            }

            //��������
            this._discrets = this.SetDiscrets(this._discretsCount, out int increment, out bool b);//dicrets.ToArray();

            //������
            this._channels = this.SetChannels(this._channelsCount, increment, b);//channels.ToArray();

            this._hdrString = $"��901 v{this._device.DeviceVersion} {this._device.DevicePlant} {oscJournalStruct.GetDate} {oscJournalStruct.GetTime} ������� - {oscJournalStruct.Stage}";
        }

        private void GetCountingSettings(OscSettintsStruct settings)
        {
            this._countingSize = settings.SizeCounting;
            this._currentsCount = settings.CurrentsCount;
            this._voltagesCount = settings.VoltagesCount;
            this._discretsCount = settings.DiscretsCount;
            this._channelsCount = settings.ChannelsCount;
            this._currentsVirtCount = settings.CurrentsVirtCount;
        }

        private ushort[][] SetDiscrets(int discrCount, out int increment, out bool b)
        {
            List<ushort[]> discrets = new List<ushort[]>();
            increment = 0;
            b = false;
            int discrWords = discrCount / 16;
            if (discrWords % 16 != 0 && this._discretsCount != 32 && this._discretsCount != 64)
            {
                b = true;
                discrWords++;
            }
            for (int i = 0; i < discrWords; i++)
            {
                if (b && i == discrWords - 1)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[this._currentsCount +_currentsVirtCount + this._voltagesCount + increment]);
                    discrets.AddRange(d.Take(8));
                }
                else
                {
                    discrets.AddRange(this.DiscretArrayInit(this._countingArray[this._currentsCount + _currentsVirtCount + this._voltagesCount + increment++]));
                }
            }
            return discrets.ToArray();
        }
        

        private ushort[][] SetChannels(int channelsCount, int increment, bool b)
        {
            List<ushort[]> channels = new List<ushort[]>();
            int channelsWords = channelsCount / 16;
            bool bb;
            if (b)
            {
                bb = (channelsCount - 8) % 16 != 0;
                channelsWords++;
            }
            else
            {
                bb = channelsCount % 16 != 0;
            }
            if (bb)
            {
                channelsWords++;
            }
            for (int i = 0; i < channelsWords; i++)
            {
                if (b && i == 0)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[this._currentsCount + _currentsVirtCount + this._voltagesCount + increment++]);
                    channels.AddRange(d.Skip(8));
                }
                else if (bb && i < channelsWords - 1)
                {
                    ushort[][] d = this.DiscretArrayInit(this._countingArray[this._currentsCount + _currentsVirtCount + this._voltagesCount + increment]);
                    channels.AddRange(d.Take(8));
                }
                else
                {
                    channels.AddRange(this.DiscretArrayInit(this._countingArray[this._currentsCount + _currentsVirtCount + this._voltagesCount + increment++]));
                }
            }

            return channels.ToArray();
        }

        private void SetCountingCurrents(ConnectionsAndTransformer cnt)
        {
            AllConnectionStruct.SetDeviceConnectionsType(_device.Info.Plant);

            List<ushort> factorsBase = cnt.Connections.AllItt.Skip(1).ToList();

            int maxValue = cnt.Connections.IttJoin;

            this._currentsKoefs = new List<double>();
            this._currents = new double[this._currentsCount + _currentsVirtCount][];

            this._baseCurrentsDiff = new ushort[_currentsVirtCount][];
            this._baseCurrents = new short[this._currentsCount][];

            this._maxI = double.MinValue;
            this._minI = double.MaxValue;

            bool isVirtual = _currentsVirtCount != 0;

            for (int i = 0; i < this._currentsCount + _currentsVirtCount; i++)
            {
                if (isVirtual)
                {
                    for (int j = 0; j < _currentsVirtCount; j++)
                    {
                        this._baseCurrentsDiff[j] = this._countingArray[j].ToArray();
                        this._currentsKoefs.Add(Math.Sqrt(2) * maxValue * 40 / 32767);
                        this._currents[j] = this._baseCurrentsDiff[j].Select(a => this._currentsKoefs[j] * a).ToArray();
                        this._maxI = Math.Max(this._maxI, this._currents[j].Max());
                        this._minI = Math.Min(this._minI, this._currents[j].Min());
                        i++;
                    }

                    isVirtual = false;
                }
                this._baseCurrents[i - _currentsVirtCount] = this._countingArray[i].Select(a => (short)a).ToArray();
                this._currentsKoefs.Add(Math.Sqrt(2) * factorsBase[i - _currentsVirtCount] * 40 / 32767);
                this._currents[i] = this._baseCurrents[i - _currentsVirtCount].Select(a => this._currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this._maxI, this._currents[i/* - _currentsVirtCount*/].Max());
                this._minI = Math.Min(this._minI, this._currents[i/* - _currentsVirtCount*/].Min());
            }
        }

        private void SetCountingVoltage(ParametersNTStruct measure)
        {
            this._voltages = new double[this._voltagesCount][];
            this._baseVoltages = new short[this._voltagesCount][];
            this._voltageKoefs = new List<double>();
            this._maxU = double.MinValue;
            this._minU = double.MaxValue;
            for (int i = 0; i < this._voltagesCount; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + this._currentsCount + _currentsVirtCount].Select(a => (short)a).ToArray();

                double koef = i == this._voltagesCount - 1 ? measure.KthxValue : measure.KthlValue;

                this._voltageKoefs.Add(koef * 2 * Math.Sqrt(2) / 256.0);
                this._voltages[i] = this._baseVoltages[i].Select(a => this._voltageKoefs[i] * a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            switch (_device.Info.Plant)
            {
                case "T16N0D64R43":
                case "T16N0D24R19":
                    this._currentsCount = 16;
                    break;
                case "T24N0D40R35":
                case "T24N0D24R51":
                case "T24N0D32R43":
                    this._currentsCount = 24;
                    break;
                case "T20N4D40R35":
                case "T20N4D32R43":
                    this._voltagesCount = 4;
                    this._currentsCount = 20;
                    this._uNames = new[] { "Ua", "Ub", "Uc", "Un" };
                    break;
                default:
                    this._currentsCount = 16;
                    break;
            }
        }

        public CountingList(int count)
        {
            this._discrets = new ushort[this._discretsCount][];
            this._channels = new ushort[this._channelsCount][];
            this._currents = new double[this._currentsCount][];
            this._baseCurrents = new short[this._currentsCount][];

            this._voltages = new double[this._voltagesCount][];
            this._baseVoltages = new short[this._voltagesCount][];

            this._count = count;
        }
        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        #endregion [Help members]

        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this._hdrString);
                for (int i = 0; i < this._channelsWithBase.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}, {2}", i + 1, this._channelsWithBase[i].Channel, this._channelsWithBase[i].Base);
                }
                hdrFile.WriteLine($"T{this._currents.Length}N{_voltagesCount}");
                hdrFile.WriteLine(1251);
            }

            NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP901,1,1991");
                cgfFile.WriteLine($"{this._currents.Length + _voltagesCount + this._discretsCount + this._channelsCount},{this._currents.Length + _voltagesCount}A,{this._discretsCount + this._channelsCount}D");

                int index = 1;

                /*
                for (int i = 0; i < _currentsVirtCount / 2; i++)
                {
                    cgfFile.WriteLine("{0},Id{1},,,A,{2},0,0,-32768,32767", index, i + 1, this._currentsKoefs[i].ToString(format));
                    cgfFile.WriteLine("{0},It{1},,,A,{2},0,0,-32768,32767", index, i + 1, this._currentsKoefs[i].ToString(format));
                    index++;
                }
                */
                
                //�������� �� ������� ��1 ��2 �� �����
                if (_diffAndTormCurrent[0] && _diffAndTormCurrent[1] && _diffAndTormCurrent[2])
                {
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));

                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));

                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[2].ToString(format));
                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[2].ToString(format));
                }
                else if (_diffAndTormCurrent[0] && _diffAndTormCurrent[1])
                {
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));

                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                }
                else if (_diffAndTormCurrent[0] && _diffAndTormCurrent[2])
                {
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));

                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                }
                else if (_diffAndTormCurrent[1] && _diffAndTormCurrent[2])
                {
                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));

                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[1].ToString(format));
                }
                else if (_diffAndTormCurrent[0])
                {
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���1,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                }
                else if (_diffAndTormCurrent[1])
                {
                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���2,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                }
                else if (_diffAndTormCurrent[2])
                {
                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                    cgfFile.WriteLine("{0},I���,,,A,{1},0,0,-32768,32767,1,1,P", index, this._currentsKoefs[0].ToString(format));
                }

                if (_currentsVirtCount != 0)
                {
                    for (int i = _currentsVirtCount; i < this._currentsCount + _currentsVirtCount; i++)
                    {
                        cgfFile.WriteLine("{0},I{1},,,A,{2},0,0,-32768,32767,1,1,P", index, i - _currentsVirtCount + 1, this._currentsKoefs[i].ToString(format));
                        index++;
                    }
                }
                else
                {
                    for (int i = 0; i < this._currentsCount; i++)
                    {
                        cgfFile.WriteLine("{0},I{1},,,A,{2},0,0,-32768,32767,1,1,P", index, i + 1, this._currentsKoefs[i].ToString(format));
                        index++;
                    }
                }
                

                for (int i = 0; i < this._voltagesCount; i++)
                {
                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, this._uNames[i], this._voltageKoefs[i].ToString(format));
                    index++;
                }
                for (int i = 0; i < this._discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                for (int i = 0; i < this._channels.Length; i++)
                {
                    try
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, this._channelsWithBase[i].ChannelStr);
                        index++;
                    }
                    catch (Exception)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, "Error");
                        index++;
                    }
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (ushort[] current in this._baseCurrentsDiff)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    for (int j = 0; j < this._voltagesCount; j++)
                    {
                        datFile.Write(",{0}", this._baseVoltages[j][i]);
                    }
                    
                    foreach (ushort[] discret in this._discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this._channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }
        public CountingList Load(string filePath, MR901New device)
        {
            this._device = device;

            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];

            OscopeAllChannelsStruct.SetDeviceChannelsType(Strings.DeviceType);
            this._channelsCount = this._device.AllChannels.Value.Rows.Length;

            InputLogicStruct.SetDeviceDiscretsType(Strings.DeviceType);
            _discretsCount = InputLogicStruct.DiscrestCount;

            ChannelWithBase[] channelWithBase = new ChannelWithBase[this._channelsCount];
            for (int i = 0; i < this._channelsCount; i++)
            {
                string[] channelAndBase =
                    hdrStrings[1 + i].Split(new[] { '=', ' ' }, StringSplitOptions.RemoveEmptyEntries)[1]
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                channelWithBase[i] = new ChannelWithBase
                {
                    Channel = Convert.ToUInt16(channelAndBase[0]),
                    Base = channelAndBase.Length > 1 ? Convert.ToByte(channelAndBase[1]) : (byte)0
                };
            }

            string[] analogChannels = hdrStrings[hdrStrings.Length - 2].Split(new[] { 'T', 'N' }, StringSplitOptions.RemoveEmptyEntries);
            int currentsCount = int.Parse(analogChannels[0]);
            int voltagesCount = int.Parse(analogChannels[1]);

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[] factors = new double[currentsCount + voltagesCount];
            for (int i = 2; i < 2 + currentsCount + voltagesCount; i++)
            {
                string res = cfgStrings[i].Split(',')[5];
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2] = double.Parse(res, format);
            }
            
            int indexCounts = 2 + currentsCount + voltagesCount + _discretsCount + this._channelsCount + 2;
            int counts = int.Parse(cfgStrings[indexCounts].Replace("1000,", string.Empty));
            int alarm = 0;
            try
            {
                string startTime = cfgStrings[indexCounts + 1].Split(',')[1];
                string runTime = cfgStrings[indexCounts + 2].Split(',')[1];

                TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
                alarm = (int)a.TotalMilliseconds;
            }
            catch (Exception)
            {
                // ignored
            }
            CountingList result = new CountingList(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            double[][] currents = new double[currentsCount][];
            double[][] voltages = new double[voltagesCount][];
            ushort[][] discrets = new ushort[this._discretsCount][];
            ushort[][] channels = new ushort[this._channelsCount][];

            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < currentsCount; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) * factors[j];
                }

                for (int j = 0; j < voltagesCount; j++)
                {
                    voltages[j][i] = Convert.ToDouble(means[j + 2 + currentsCount], CultureInfo.InvariantCulture) * factors[j + currentsCount];
                }

                for (int j = 0; j < this._discretsCount; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2 + currentsCount + voltagesCount]);
                }

                for (int j = 0; j < this._channelsCount; j++)
                {
                    channels[j][i] = Convert.ToUInt16(means[j + 2 + currentsCount + voltagesCount + this._discretsCount]);
                }
            }

            result._maxI = double.MinValue;
            result._minI = double.MaxValue;
            for (int i = 0; i < currentsCount; i++)
            {
                result._maxI = Math.Max(result._maxI, currents[i].Max());
                result._minI = Math.Min(result._minI, currents[i].Min());
            }

            result._maxU = double.MinValue;
            result._minU = double.MaxValue;
            for (int i = 0; i < voltagesCount; i++)
            {
                result._maxU = Math.Max(result._maxU, voltages[i].Max());
                result._minU = Math.Min(result._minU, voltages[i].Min());
            }

            result._currents = currents;
            result._channels = channels;
            result._discrets = discrets;
            result._voltages = voltages;
            result._channelsWithBase = channelWithBase;
            result._hdrString = hdrString;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
