﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR901NEW.AlarmJournal;
using BEMN.MR901NEW.AlarmJournal.Structures;
using BEMN.MR901NEW.BSBGL;
using BEMN.MR901NEW.Configuration;
using BEMN.MR901NEW.Configuration.Structures;
using BEMN.MR901NEW.Configuration.Structures.Defenses;
using BEMN.MR901NEW.Configuration.Structures.Oscope;
using BEMN.MR901NEW.Emulation;
using BEMN.MR901NEW.Emulation.Structures;
using BEMN.MR901NEW.FileSharingService;
using BEMN.MR901NEW.Measuring;
using BEMN.MR901NEW.Osc;
using BEMN.MR901NEW.Osc.Loaders;
using BEMN.MR901NEW.Osc.Structures;
using BEMN.MR901NEW.SystemJournal;
using BEMN.MR901NEW.SystemJournal.Structures;

namespace BEMN.MR901NEW
{
    public class MR901New : Device, IDeviceView, IDeviceVersion
    {
        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";

        public const int BAUDE_RATE_MAX = 921600;
        public const ushort MEASURE_START_ADDRESS = 0x14FE;
        public const ushort GROUP_SETPOINT_SIZE = 0x53A;

        private ushort _groupSetPointSize;
        private MemoryEntity<OneWordStruct> _groupUstavki;
        private MemoryEntity<OscJournalStruct> _oscJournal;

        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;

        private MemoryEntity<FullSystemJournalStruct1024> _full1024;
        private MemoryEntity<FullSystemJournalStruct64> _full64;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<OneWordStruct> _saveSysJournalIndex;

        #region Programming

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<LogicProgramSignals> _programSignalsStruct;
        private MemoryEntity<SomeStruct> _stateSpl;
        public MemoryEntity<OneWordStruct> ProgramPageStruct { get; private set; }

        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }

        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<LogicProgramSignals> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }

        public MemoryEntity<SomeStruct> StateSpl
        {
            get { return this._stateSpl; }
        }
        #endregion

        public MR901New()
        {
            HaveVersion = true;
        }
        public MR901New(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
        }
        
        private int _connectionPort;

        public int ConnectionPort
        {
            get { return _connectionPort; }
            set { _connectionPort = value; }
        }

        public void InitMemoryEntity()
        {
            int slotLen = this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64;

            this.Configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000, slotLen);

            this.SetOscStartPage = new MemoryEntity<OneWordStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this.OscOptions = new MemoryEntity<OscSettintsStruct>("Параметры осциллографа", this, 0x05A0);

            this.AllChannels = new MemoryEntity<OscopeAllChannelsStruct>("Все каналы осциллографа", this, 0x110A, slotLen);
            this.CurrentOptionsLoader = new CurrentOptionsLoader(this);
            this.CurrentOptionsLoaderAj = new CurrentOptionsLoader(this);
            this.OscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900, slotLen);
            this.RefreshOscJournal = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);

            if (this.MB.BaudeRate == BAUDE_RATE_MAX)
            {
                this.AllAlarmJournal = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
                this.AllAlarmUJournal = new MemoryEntity<AllAlarmJournalUStruct>("Журнал аварий", this, 0x0700, slotLen);
                this.AllOscJournal = new MemoryEntity<AllOscJournalStruct>("Журнал осциллографа (весь)", this, 0x0800, slotLen);
                this._full1024 = new MemoryEntity<FullSystemJournalStruct1024>("Запись журнала системы 1024", this, 0x0600, slotLen);
                this._full64 = null;
            }
            else
            {
                this._full1024 = null;
                this._full64 = new MemoryEntity<FullSystemJournalStruct64>("Запись журнала системы 64", this, 0x0600);
            }

            this.AlarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Запись журнала аварий", this, 0x0700, 68);
            this.AlarmJournalU = new MemoryEntity<AlarmJournalRecordUStruct>("Запись журнала аварий", this, 0x0700, 68);
            this.SetPageJa = new MemoryEntity<OneWordStruct>("Страница ЖА", this, 0x0700);

            this._saveSysJournalIndex = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, 0x0600);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Запись журнала системы", this, 0x0600);

            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x0800);
            
            this._groupUstavki = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);

            this._dateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800, slotLen);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись нулевых аналоговых сигналов ", this, 0x5800, slotLen);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение времени и статуса эмуляции", this, 0x586E, slotLen);

            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300, slotLen);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals_", this, 0x4100, slotLen);
            this.ProgramPageStruct = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000, slotLen);

            this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D08);
            this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D09);

            this._stateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики", this, 0x0D17);
            ushort[] values = new ushort[4];
            this._stateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
            this._stateSpl.Values = values;
        }

        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournal { get; private set; }
        public MemoryEntity<AllAlarmJournalUStruct> AllAlarmUJournal { get; private set; }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal { get; private set; }
        public MemoryEntity<AlarmJournalRecordUStruct> AlarmJournalU { get; private set; }

        public MemoryEntity<OneWordStruct> SetPageJa { get; private set; }

        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }
        public MemoryEntity<FullSystemJournalStruct1024> Full1SysJournal024 => this._full1024;
        public MemoryEntity<FullSystemJournalStruct64> FullSysJournal64 => this._full64;
        public MemoryEntity<SystemJournalStruct> SystemJournal => this._systemJournal;
        public MemoryEntity<OneWordStruct> SaveSysJournalIndex => this._saveSysJournalIndex;
        public MemoryEntity<WriteStructEmul> WriteStructEmulation => this._writeSructEmulation;
        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull => this._writeSructEmulationNull;
        public MemoryEntity<ReadStructEmul> ReadStructEmulation => this._readSructEmulation;
        public MemoryEntity<DateTimeStruct> DateTime => this._dateTime;
        public MemoryEntity<OscJournalStruct> OscJournal => this._oscJournal;

        public MemoryEntity<ConfigurationStruct> Configuration { get; private set; }
        public CurrentOptionsLoader CurrentOptionsLoaderAj { get; set; }
        public CurrentOptionsLoader CurrentOptionsLoader { get; private set; }
        public MemoryEntity<OneWordStruct> RefreshOscJournal { get; private set; }
        public MemoryEntity<OscPage> OscPage { get; private set; }

        public MemoryEntity<AllOscJournalStruct> AllOscJournal
        {
            get;
            private set;
        }
        public MemoryEntity<OneWordStruct> SetOscStartPage { get; private set; }
        public MemoryEntity<OscSettintsStruct> OscOptions { get; private set; }
        public MemoryEntity<OscopeAllChannelsStruct> AllChannels { get; private set; }
        public MemoryEntity<OneWordStruct> GroupUstavki
        {
            get { return this._groupUstavki; }
        }

        public int CurrentsCount
        {
            get
            {
                string count = GetCountFromString("T");

                return int.Parse(count);
            }
        }

        public int VoltagesCount
        {
            get
            {
                string count = GetCountFromString("N");

                return int.Parse(count);
            }
        }

        public int DiskretsCount
        {
            get
            {
                string count = GetCountFromString("D");

                return int.Parse(count);
            }
        }

        private string GetCountFromString(string chanelName)
        {
            string conf = Info.DeviceConfiguration;
            string count = string.Empty;
            int index = conf.IndexOf(chanelName, StringComparison.InvariantCulture);
            while (true)
            {
                char d = conf[++index];
                if (char.IsDigit(d))
                {
                    count += d;
                }
                else
                {
                    break;
                }
            }

            return count;
        }

        public override void LoadVersion(object deviceObj)
        {
            _infoSlot = new slot(0x500, 0x500 + 0x20);
            base.LoadVersion(deviceObj);
        }

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return MEASURE_START_ADDRESS;
            return (ushort)(MEASURE_START_ADDRESS + this._groupSetPointSize * group);
        }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                if (mb != null)
                {
                    mb.CompleteExchange -= this.CompleteExchange;
                }
                mb = value;
                mb.CompleteExchange += this.CompleteExchange;
            }
        }

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "Сохранить конфигурацию")
            {
                MessageBox.Show(query.error ? ERROR_SAVE_CONFIG : WRITE_OK);
            }

            if (query.name == "LoadPort" + DeviceNumber)
            {
                Raise(query, null, null, ref _port);

                GetConnectionPort(_port.Value);
            }

            mb_CompleteExchange(sender, query);
        }

        private void GetConnectionPort(ushort[] buffer)
        {
            byte[] byteBuf = Common.TOBYTES(buffer, true);
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            char[] charBuf = new char[byteBuf.Length];
            try
            {
                int byteUsed, charUsed;
                bool complete;
                dec.Convert(byteBuf, 0, byteBuf.Length, charBuf, 0, byteBuf.Length, true, out byteUsed, out charUsed,
                    out complete);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }

            string[] param;
            try
            {
                param = new string(charBuf, 0, 5).Split(new[] { ' ', '\0' }, StringSplitOptions.RemoveEmptyEntries);
                int portNumber = Convert.ToInt32(new String(param[0].Where(Char.IsDigit).ToArray()));
                ConnectionPort = portNumber;
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        private slot _port = new slot(0x05D0, 0x05E0);

        private void LoadPort()
        {
            LoadSlot(DeviceNumber, _port, "LoadPort" + DeviceNumber, this);
        }

        #region IDeviceVersion

        public const string T16N0D64R43 = "T16N0D64R43";
        public const string T24N0D40R35 = "T24N0D40R35";
        public const string T24N0D24R51 = "T24N0D24R51";
        public const string T24N0D32R43 = "T24N0D32R43";
        public const string T20N4D40R35 = "T20N4D40R35";
        public const string T20N4D32R43 = "T20N4D32R43";
        public const string T16N0D24R19 = "T16N0D24R19";

        private static string[] _deviceApparatConfig =
        {
            T16N0D64R43,
            T24N0D40R35,
            T24N0D24R51,
            T24N0D32R43,
            T20N4D40R35,
            T20N4D32R43,
            T16N0D24R19
        };

        private static readonly string[] _deviceOutString = { T16N0D64R43, T24N0D40R35, T24N0D24R51, T24N0D32R43, T20N4D40R35, T20N4D32R43, T16N0D24R19 };
        
        public Type[] Forms
        {
            get
            {
                Strings.CurrentVersion = Common.VersionConverter(DeviceVersion);

                this.InitMemoryEntity();

                _groupSetPointSize = new GroupSetpoint().GetStructInfo(MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64)
                    .FullSize;

                if ((this.DeviceDlgInfo.IsConnectionMode || this.IsConnect)) LoadPort();

                //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                //{
                //    ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                //    choice.ShowDialog();
                //    DevicePlant = choice.DeviceType;
                //}
                
                Strings.DeviceType = DevicePlant;

                return new[]
                {
                    typeof (ConfigurationForm),
                    typeof (Mr901OscilloscopeForm),
                    typeof (EmulationForm),
                    typeof (BSBGLEF),
                    typeof (SystemJournalForm),
                    typeof (AlarmJournalForm),
                    typeof (MeasuringForm),
                    typeof (FileSharingForm)
                };

            }
        }

        public List<string> Versions => new List<string>
        {
            "3.00"
        };

        #endregion

       
        public Type ClassType => typeof(MR901New);
        
        public bool ForceShow => false;

        public Image NodeImage => Framework.Properties.Resources.mrBig;

        public string NodeName => "МР901 (в.3.xx)";
      
        public INodeView[] ChildNodes => new INodeView[] { };
       
        public bool Deletable => true;
    }
}
