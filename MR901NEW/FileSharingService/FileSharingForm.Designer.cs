﻿namespace BEMN.MR901NEW.FileSharingService
{
    partial class FileSharingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._pathToFileGroupBox = new System.Windows.Forms.GroupBox();
            this._passwordMTB = new System.Windows.Forms.MaskedTextBox();
            this._changePasswordBTN = new System.Windows.Forms.Button();
            this.pathInPCTextBox = new System.Windows.Forms.TextBox();
            this.writeToDeviceBtn = new System.Windows.Forms.Button();
            this.readFromDeviceBtn = new System.Windows.Forms.Button();
            this._filesOperationsGroupBox = new System.Windows.Forms.GroupBox();
            this._infoCommandsButton = new System.Windows.Forms.Button();
            this._changePasswordButton = new System.Windows.Forms.Button();
            this._deleteButton = new System.Windows.Forms.Button();
            this._sendCommandButton = new System.Windows.Forms.Button();
            this.filesInDeviceTreeView = new System.Windows.Forms.TreeView();
            this._pathTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.readFileNamesInDevice = new System.Windows.Forms.Button();
            this._backgroundImagePanel = new System.Windows.Forms.PictureBox();
            this._aboutFunctionLabel = new System.Windows.Forms.Label();
            this._passTextBox = new System.Windows.Forms.MaskedTextBox();
            this._passButton = new System.Windows.Forms.Button();
            this._pathToFileGroupBox.SuspendLayout();
            this._filesOperationsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._backgroundImagePanel)).BeginInit();
            this.SuspendLayout();
            // 
            // _pathToFileGroupBox
            // 
            this._pathToFileGroupBox.Controls.Add(this._passwordMTB);
            this._pathToFileGroupBox.Controls.Add(this._changePasswordBTN);
            this._pathToFileGroupBox.Controls.Add(this.pathInPCTextBox);
            this._pathToFileGroupBox.Controls.Add(this.writeToDeviceBtn);
            this._pathToFileGroupBox.Controls.Add(this.readFromDeviceBtn);
            this._pathToFileGroupBox.Location = new System.Drawing.Point(12, 12);
            this._pathToFileGroupBox.Name = "_pathToFileGroupBox";
            this._pathToFileGroupBox.Size = new System.Drawing.Size(448, 81);
            this._pathToFileGroupBox.TabIndex = 1;
            this._pathToFileGroupBox.TabStop = false;
            this._pathToFileGroupBox.Text = "Путь к файлу на компьютере";
            // 
            // _passwordMTB
            // 
            this._passwordMTB.Location = new System.Drawing.Point(6, 50);
            this._passwordMTB.Name = "_passwordMTB";
            this._passwordMTB.Size = new System.Drawing.Size(100, 20);
            this._passwordMTB.TabIndex = 5;
            // 
            // _changePasswordBTN
            // 
            this._changePasswordBTN.Location = new System.Drawing.Point(116, 49);
            this._changePasswordBTN.Name = "_changePasswordBTN";
            this._changePasswordBTN.Size = new System.Drawing.Size(110, 23);
            this._changePasswordBTN.TabIndex = 4;
            this._changePasswordBTN.Text = "Сменить пароль";
            this._changePasswordBTN.UseVisualStyleBackColor = true;
            this._changePasswordBTN.Click += new System.EventHandler(this._changePasswordBTN_Click);
            // 
            // pathInPCTextBox
            // 
            this.pathInPCTextBox.Enabled = false;
            this.pathInPCTextBox.Location = new System.Drawing.Point(6, 19);
            this.pathInPCTextBox.Name = "pathInPCTextBox";
            this.pathInPCTextBox.Size = new System.Drawing.Size(322, 20);
            this.pathInPCTextBox.TabIndex = 1;
            // 
            // writeToDeviceBtn
            // 
            this.writeToDeviceBtn.Enabled = false;
            this.writeToDeviceBtn.Location = new System.Drawing.Point(334, 48);
            this.writeToDeviceBtn.Name = "writeToDeviceBtn";
            this.writeToDeviceBtn.Size = new System.Drawing.Size(108, 23);
            this.writeToDeviceBtn.TabIndex = 0;
            this.writeToDeviceBtn.Text = "Записать";
            this.writeToDeviceBtn.UseVisualStyleBackColor = true;
            this.writeToDeviceBtn.Click += new System.EventHandler(this.writeToDeviceBtn_Click);
            // 
            // readFromDeviceBtn
            // 
            this.readFromDeviceBtn.Location = new System.Drawing.Point(334, 18);
            this.readFromDeviceBtn.Name = "readFromDeviceBtn";
            this.readFromDeviceBtn.Size = new System.Drawing.Size(108, 23);
            this.readFromDeviceBtn.TabIndex = 0;
            this.readFromDeviceBtn.Text = "Открыть";
            this.readFromDeviceBtn.UseVisualStyleBackColor = true;
            this.readFromDeviceBtn.Click += new System.EventHandler(this.readFromDeviceBtn_Click);
            // 
            // _filesOperationsGroupBox
            // 
            this._filesOperationsGroupBox.Controls.Add(this._infoCommandsButton);
            this._filesOperationsGroupBox.Controls.Add(this._changePasswordButton);
            this._filesOperationsGroupBox.Controls.Add(this._deleteButton);
            this._filesOperationsGroupBox.Controls.Add(this._sendCommandButton);
            this._filesOperationsGroupBox.Controls.Add(this.filesInDeviceTreeView);
            this._filesOperationsGroupBox.Controls.Add(this._pathTextBox);
            this._filesOperationsGroupBox.Controls.Add(this.label1);
            this._filesOperationsGroupBox.Controls.Add(this.readFileNamesInDevice);
            this._filesOperationsGroupBox.Location = new System.Drawing.Point(12, 99);
            this._filesOperationsGroupBox.Name = "_filesOperationsGroupBox";
            this._filesOperationsGroupBox.Size = new System.Drawing.Size(448, 237);
            this._filesOperationsGroupBox.TabIndex = 2;
            this._filesOperationsGroupBox.TabStop = false;
            this._filesOperationsGroupBox.Text = "Файлы в устройстве";
            // 
            // _infoCommandsButton
            // 
            this._infoCommandsButton.Location = new System.Drawing.Point(292, 72);
            this._infoCommandsButton.Name = "_infoCommandsButton";
            this._infoCommandsButton.Size = new System.Drawing.Size(150, 23);
            this._infoCommandsButton.TabIndex = 11;
            this._infoCommandsButton.Text = "Информация о командах";
            this._infoCommandsButton.UseVisualStyleBackColor = true;
            this._infoCommandsButton.Click += new System.EventHandler(this._infoCommandsButton_Click);
            // 
            // _changePasswordButton
            // 
            this._changePasswordButton.Location = new System.Drawing.Point(292, 138);
            this._changePasswordButton.Name = "_changePasswordButton";
            this._changePasswordButton.Size = new System.Drawing.Size(150, 23);
            this._changePasswordButton.TabIndex = 10;
            this._changePasswordButton.Text = "Сменить пароль";
            this._changePasswordButton.UseVisualStyleBackColor = true;
            this._changePasswordButton.Click += new System.EventHandler(this._changePasswordButton_Click);
            // 
            // _deleteButton
            // 
            this._deleteButton.Enabled = false;
            this._deleteButton.Location = new System.Drawing.Point(292, 167);
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(150, 23);
            this._deleteButton.TabIndex = 9;
            this._deleteButton.Text = "Удалить выбранный файл";
            this._deleteButton.UseVisualStyleBackColor = true;
            // 
            // _sendCommandButton
            // 
            this._sendCommandButton.Location = new System.Drawing.Point(292, 43);
            this._sendCommandButton.Name = "_sendCommandButton";
            this._sendCommandButton.Size = new System.Drawing.Size(150, 23);
            this._sendCommandButton.TabIndex = 8;
            this._sendCommandButton.Text = "Выполнить";
            this._sendCommandButton.UseVisualStyleBackColor = true;
            this._sendCommandButton.Click += new System.EventHandler(this._sendCommandButton_Click);
            // 
            // filesInDeviceTreeView
            // 
            this.filesInDeviceTreeView.Location = new System.Drawing.Point(6, 19);
            this.filesInDeviceTreeView.Name = "filesInDeviceTreeView";
            this.filesInDeviceTreeView.Size = new System.Drawing.Size(220, 212);
            this.filesInDeviceTreeView.TabIndex = 7;
            // 
            // _pathTextBox
            // 
            this._pathTextBox.Location = new System.Drawing.Point(292, 19);
            this._pathTextBox.Name = "_pathTextBox";
            this._pathTextBox.Size = new System.Drawing.Size(150, 20);
            this._pathTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(232, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Команда";
            // 
            // readFileNamesInDevice
            // 
            this.readFileNamesInDevice.Location = new System.Drawing.Point(292, 196);
            this.readFileNamesInDevice.Name = "readFileNamesInDevice";
            this.readFileNamesInDevice.Size = new System.Drawing.Size(150, 35);
            this.readFileNamesInDevice.TabIndex = 3;
            this.readFileNamesInDevice.Text = "Получить файлы на устройстве";
            this.readFileNamesInDevice.UseVisualStyleBackColor = true;
            this.readFileNamesInDevice.Click += new System.EventHandler(this.readFileNamesInDevice_Click);
            // 
            // _backgroundImagePanel
            // 
            this._backgroundImagePanel.BackColor = System.Drawing.SystemColors.Control;
            this._backgroundImagePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._backgroundImagePanel.Location = new System.Drawing.Point(2, 99);
            this._backgroundImagePanel.Name = "_backgroundImagePanel";
            this._backgroundImagePanel.Size = new System.Drawing.Size(470, 237);
            this._backgroundImagePanel.TabIndex = 11;
            this._backgroundImagePanel.TabStop = false;
            // 
            // _aboutFunctionLabel
            // 
            this._aboutFunctionLabel.AutoSize = true;
            this._aboutFunctionLabel.Location = new System.Drawing.Point(53, 194);
            this._aboutFunctionLabel.Name = "_aboutFunctionLabel";
            this._aboutFunctionLabel.Size = new System.Drawing.Size(150, 26);
            this._aboutFunctionLabel.TabIndex = 17;
            this._aboutFunctionLabel.Text = "Пароль для разблокировки \r\n  дополнительных функций";
            // 
            // _passTextBox
            // 
            this._passTextBox.Location = new System.Drawing.Point(214, 197);
            this._passTextBox.Name = "_passTextBox";
            this._passTextBox.Size = new System.Drawing.Size(113, 20);
            this._passTextBox.TabIndex = 16;
            this._passTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _passButton
            // 
            this._passButton.Location = new System.Drawing.Point(344, 196);
            this._passButton.Name = "_passButton";
            this._passButton.Size = new System.Drawing.Size(75, 23);
            this._passButton.TabIndex = 15;
            this._passButton.Text = "Принять";
            this._passButton.UseVisualStyleBackColor = true;
            this._passButton.Click += new System.EventHandler(this._passButton_Click);
            // 
            // FileSharingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 348);
            this.Controls.Add(this._aboutFunctionLabel);
            this.Controls.Add(this._passTextBox);
            this.Controls.Add(this._passButton);
            this.Controls.Add(this._backgroundImagePanel);
            this.Controls.Add(this._pathToFileGroupBox);
            this.Controls.Add(this._filesOperationsGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileSharingForm";
            this.Text = "FileSharingForm";
            this.Load += new System.EventHandler(this.FileSharingForm_Load);
            this._pathToFileGroupBox.ResumeLayout(false);
            this._pathToFileGroupBox.PerformLayout();
            this._filesOperationsGroupBox.ResumeLayout(false);
            this._filesOperationsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._backgroundImagePanel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox _pathToFileGroupBox;
        private System.Windows.Forms.TextBox pathInPCTextBox;
        private System.Windows.Forms.Button writeToDeviceBtn;
        private System.Windows.Forms.GroupBox _filesOperationsGroupBox;
        private System.Windows.Forms.TreeView filesInDeviceTreeView;
        private System.Windows.Forms.TextBox _pathTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button readFileNamesInDevice;
        private System.Windows.Forms.Button readFromDeviceBtn;
        private System.Windows.Forms.Button _sendCommandButton;
        private System.Windows.Forms.Button _changePasswordButton;
        private System.Windows.Forms.Button _deleteButton;
        private System.Windows.Forms.PictureBox _backgroundImagePanel;
        private System.Windows.Forms.Button _infoCommandsButton;
        private System.Windows.Forms.Label _aboutFunctionLabel;
        private System.Windows.Forms.MaskedTextBox _passTextBox;
        private System.Windows.Forms.Button _passButton;
        private System.Windows.Forms.MaskedTextBox _passwordMTB;
        private System.Windows.Forms.Button _changePasswordBTN;
    }
}