<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <body>

        <xsl:variable name="conf" select="��901/������"></xsl:variable>
          <h3>
            ���������� <xsl:value-of select="��901/���_����������"/>. ������ �� <xsl:value-of select="��901/������"/>. ����� <xsl:value-of select="��901/�����_����������"/>. ������������ <xsl:value-of select="��901/������"/>
          </h3>
          <p></p>
          <b>�������������</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="32CD32">
              <th>�������</th>
              <th>I��, �</th>
              <th>������.</th>
              <th>�����.</th>
              <th>��������</th>
              <th>����</th>
              <th>���������</th>
              <th>�������� ���������, ��</th>
            </tr>
            <xsl:for-each select="��901/�������������/�������������/Rows/ConnectionStruct">
              <xsl:choose>
                <xsl:when test="$conf = 'A2' = 'A3' = 'A4'">
                  <tr align="center">
                    <td>
                      ������������� <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="I��"/>
                    </td>
                    <td>
                      <xsl:value-of select="����������"/>
                    </td>
                    <td>
                      <xsl:value-of select="���������"/>
                    </td>
                    <td>
                      <xsl:value-of select="��������"/>
                    </td>
                    <td>
                      <xsl:value-of select="����"/>
                    </td>
                    <td>
                      <xsl:for-each select="���������">
                        <xsl:if test="current() ='false'">
                          ���
                        </xsl:if>
                        <xsl:if test="current() ='true'">
                          ��
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                    <td>
                      <xsl:value-of select="��������_���������"/>
                    </td>
                  </tr>
                </xsl:when>
                <xsl:when test="$conf = 'A5'">
                  <xsl:if test="position() &lt; 21">
                    <tr align="center">
                      <td>
                        ������������� <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="I��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="��������_���������"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:if test="position() &lt; 17">
                    <tr align="center">
                      <td>
                        ������������� <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="I��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="��������_���������"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </table>
          <p></p>

          <xsl:choose>
            <xsl:when test="$conf = 'A5'">
              <h3>��������� TH</h3>

              <b>������������ �������������� ����������</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFFFCC">
                  <th>����</th>
                  <th>���n</th>
                  <th>���� ����. ������. ���</th>
                  <th>���� ����. ������. ��n</th>
                </tr>
                <tr align="center">
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/KTHL"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/KTHX"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/���������"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/��������n"/>
                  </td>
                </tr>
              </table>

              <table border="1" cellspacing="0">
                <tr bgcolor="FFFFCC">
                  <th>�������� 3U0</th>
                  <th>d3U0, �</th>
                  <th>Td3U0, ��</th>
                  <th>����������</th>
                </tr>
                <tr align="center">
                  <td>
                    <xsl:for-each select="��901/�������������/���������_��/��������_3U0">
                      <xsl:if test="current() ='false'">
                        ���
                      </xsl:if>
                      <xsl:if test="current() ='true'">
                        ��
                      </xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/d3U0"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/Td3U0"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/d3U0����������"/>
                  </td>
                </tr>
              </table>

              <table border="1" cellspacing="0">
                <tr bgcolor="FFFFCC">
                  <th>�������� �����������</th>
                  <th>Umin/Umsx, %</th>
                  <th>T��.��, ��</th>
                  <th>����������</th>
                </tr>
                <tr align="center">
                  <td>
                    <xsl:for-each select="��901/�������������/���������_��/��������_����������">
                      <xsl:if test="current() ='false'">
                        ���
                      </xsl:if>
                      <xsl:if test="current() ='true'">
                        ��
                      </xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/Uns"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/Tuns"/>
                  </td>
                  <td>
                    <xsl:value-of select="��901/�������������/���������_��/Uns����������"/>
                  </td>
                </tr>
              </table>
            </xsl:when>
          </xsl:choose>
          <p></p>
          <h3>������� �������</h3>

          <b>������� ���������� �������</b>
          <table border="1" cellspacing="0">
            <td>
              <b>���������� ������� �</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="FFFFCC">
                  <th>����� ��</th>
                  <th>������������</th>
                </tr>

                <xsl:for-each select="��901/������������_�������_��������/��">
                  <xsl:if test="position() &lt; 9 ">
                    <tr>
                      <td>
                        <xsl:value-of  select="position()"/>
                      </td>

                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() !='���'">
                            <xsl:if test="current() ='��'">
                              �<xsl:value-of select="position()"/>
                            </xsl:if>
                            <xsl:if test="current() ='������'">
                              ^�<xsl:value-of select="position()"/>
                            </xsl:if>
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>

              <b>���������� ������� ���</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="FFFFCC">
                  <th>����� ��</th>
                  <th>������������</th>
                </tr>

                <xsl:for-each select="��901/������������_�������_��������/��">
                  <xsl:if test="position() &gt; 8 ">
                    <tr>
                      <td>
                        <xsl:value-of  select="position()"/>
                      </td>

                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() !='���'">
                            <xsl:if test="current() ='��'">
                              �<xsl:value-of select="position()"/>
                            </xsl:if>
                            <xsl:if test="current() ='������'">
                              ^�<xsl:value-of select="position()"/>
                            </xsl:if>
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
            </td>
          </table>
          <p></p>

          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="FFFFCC">��������� ������ ������� 1</th>
              <td>
                <xsl:value-of select="��901/�������_�������/����_���������_������_�������_1"/>
              </td>
            </tr>
          </table>
          <p></p>

        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="FFFFCC">��������� ������ ������� 2</th>
            <td>
              <xsl:value-of select="��901/�������_�������/����_���������_������_�������_2"/>
            </td>
          </tr>
        </table>
        <p></p>

          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="FFFFCC">����� ���������</th>
              <td>
                <xsl:value-of select="��901/�������_�������/����_�����_����."/>
              </td>
            </tr>
          </table>
          <p></p>

          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="FFFFCC">����� ������������� ��</th>
              <td>
                <xsl:value-of select="��901/����_��/�����_�������������_��"/>
              </td>
            </tr>
          </table>
          <p></p>

        <b>�����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FF8C00">
            <th>�����</th>
            <th>���</th>
            <th>��������</th>
          </tr>
          <xsl:for-each select="��901/�����������/���_������������/AntiBounce">

            <xsl:choose>

              <xsl:when test="$conf = 'A1'">
                <xsl:if test="position() &lt; 65">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:when test="$conf = 'A2' = 'A5'">
                <xsl:if test="position() &lt; 41 ">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:when test="$conf = 'A6'">
                <xsl:if test="position() &lt; 33 ">
                  <tr>
                    <!--align ="center">-->
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>

              <xsl:otherwise>
                <xsl:if test="position() &lt; 25 ">
                  <tr align ="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:otherwise>

            </xsl:choose>

          </xsl:for-each>
        </table>

        <h3>�������� ����� TT</h3>
        <table border="1" cellspacing="0">
            <tr bgcolor="FF69B4">
              <th>�������� ����� ��</th>
              <th>l�min, l�</th>
              <th>T��, ��</th>
              <th>�����</th>
              <th>�����</th>
            </tr>
            <xsl:for-each select="��901/����_��/����_��_���/TtStructChannels/ControlTtStruct">
              <tr align="center">
                <td>
                  <xsl:if test="position() =1">��1</xsl:if>
                  <xsl:if test="position() =2">��2</xsl:if>
                  <xsl:if test="position() =3">��</xsl:if>
                </td>
                <td>
                  <xsl:value-of select="I�min"/>
                </td>
                <td>
                  <xsl:value-of select="T��"/>
                </td>
                <td>
                  <xsl:value-of select="�������������"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
              </tr>
            </xsl:for-each>
        </table>
        <p></p>

        <h2>�������</h2>

        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="c1ced5">���� �������� �������</th>
            <th>
              <xsl:value-of select="��901/�������_�����/InputAdd"/>
            </th>
          </tr>
        </table>
        <p></p>

        <h3>������.</h3>
        <xsl:for-each select="��901/������/������">
          <xsl:variable name="index" select="position()"/>
          <h2 style="color:#FF0000FF"> ������ ������� <xsl:value-of select="position()"/></h2>
          <table border="1" cellspacing="0">
            <td>
              <b>���������������� ������ �� ����������� ���������</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="B0C4DE">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���� �� l�3 ��</th>
                  <th>l�>, l�</th>
                  <th>l�>>, l�</th>
                  <th>t��, ��</th>
                  <th>l�, l�</th>
                  <th>f</th>
                  <th>����.������.2</th>
                  <th>l2�, %</th>
                  <th>����.������.5</th>
                  <th>l5�, %</th>
                  <th>�����.�����.</th>
                  <th>������������</th>
                  <th>l�*,l�</th>
                  <th>t��, ��</th>
                  <th>���� �������.</th>
                  <th>���.</th>
                  <th>����</th>
                </tr>

                <xsl:for-each select="���/Rows/DifferentialCurrentStruct">
                  <xsl:if test="position() &lt; 4 ">
                    <tr align="center">
                      <td>
                        <xsl:if test="position() =1">l�1 ��1</xsl:if>
                        <xsl:if test="position() =2">l�2 ��2</xsl:if>
                        <xsl:if test="position() =3">l�3 ��</xsl:if>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������2"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="f"/>
                      </td>
                      <td>
                        <xsl:for-each select="����2">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="����2�������"/>
                      </td>
                      <td>
                        <xsl:for-each select="����5">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="����5�������"/>
                      </td>
                      <td>
                        <xsl:for-each select="�����">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="�����������">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="T��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>


              <b>���������������� ������ �� ���������� ���������</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="B0C4DE">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���� �� l�3 ��</th>
                  <th>l�>, l�</th>
                  <th>l�>>, l�</th>
                  <!--<th>t��, ��</th>-->
                  <th>l�, l�</th>
                  <th>f</th>
                  <!--<th>����.������.2</th>
                      <th>l2�, %</th>
                      <th>����.������.5</th>
                      <th>l5�, %</th>
                      <th>�����.�����.</th>-->
                  <th>������������</th>
                  <th>l�*,l�</th>
                  <th>t��, ��</th>
                  <th>���� �������.</th>
                  <th>���.</th>
                  <th>����</th>
                </tr>

                <xsl:for-each select="���/Rows/DifferentialCurrentStruct">
                  <xsl:if test="position() &gt; 3 ">
                    <tr align="center">
                      <td>
                        <xsl:if test="position() =4">l�1� ��1</xsl:if>
                        <xsl:if test="position() =5">l�2� ��2</xsl:if>
                        <xsl:if test="position() =6">l�3� ��</xsl:if>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������2"/>
                      </td>
                      <!--<td>
                        <xsl:value-of select="t��"/>
                      </td>-->
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="f"/>
                      </td>
                      <!--<td>
                            <xsl:for-each select="����2">
                              <xsl:if test="current() ='false'">
                                ���
                              </xsl:if>
                              <xsl:if test="current() ='true'">
                                ��
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:value-of select="����2�������"/>
                          </td>
                          <td>
                            <xsl:for-each select="����5">
                              <xsl:if test="current() ='false'">
                                ���
                              </xsl:if>
                              <xsl:if test="current() ='true'">
                                ��
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:value-of select="����5�������"/>
                          </td>
                          <td>
                            <xsl:for-each select="�����">
                              <xsl:if test="current() ='false'">
                                ���
                              </xsl:if>
                              <xsl:if test="current() ='true'">
                                ��
                              </xsl:if>
                            </xsl:for-each>
                          </td>-->
                      <td>
                        <xsl:for-each select="�����������">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="T��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">
                            ���
                          </xsl:if>
                          <xsl:if test="current() ='true'">
                            ��
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
            </td>
          </table>
          <p></p>
          
          
          <b>������ I></b>
          <table border="1" cellspacing="0">
            <tr bgcolor="B0C4DE">
              <th>�������</th>
              <th>�����</th>
              <th>����������</th>
              <th>���������</th>
              <th>l��, l�</th>
              <th>��������������</th>
              <th>t, ��</th>
              <th>k �����. ���-��</th>
              <th>�����������</th>
              <th>����</th>
            </tr>
            <xsl:for-each select="���/Rows/MtzStruct">
              <tr align="center">
                <td>
                  ������� l> <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���������"/>
                </td>
                <td>
                  <xsl:value-of select="�������"/>
                </td>
                <td>
                  <xsl:value-of select="��������������"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="�"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:for-each select="����">
                    <xsl:if test="current() ='false'">
                      ���
                    </xsl:if>
                    <xsl:if test="current() ='true'">
                      ��
                    </xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:for-each>
          </table>
          <p></p>

          <!-- IF CONF A5 => INPUT DEF U -->
          <xsl:choose>
            <xsl:when test="$conf = 'A5'">
              <h3>������ U</h3>
              <b>������ U> </b>
              <table border="1" cellspacing="0">
                <tr bgcolor="B0C4DE">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>����������</th>
                  <th>����. � 3U0</th>
                  <th>����. � ��</th>
                  <th>���</th>
                  <th>Ucp, �</th>
                  <th>t��, ��</th>
                  <th>�������</th>
                  <th>U��, �</th>
                  <th>t��, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                </tr>
                <xsl:for-each select="����������/Rows/VoltageDefenseStruct">
                  <xsl:if test="position() = 1">
                    <tr align="center">
                      <td>U> 1</td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="_x0033_U0">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="���UB"/>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                  <xsl:if test="position() = 2">
                    <tr align="center">
                      <td>U> 2</td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="_x0033_U0">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="���UB"/>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <b>������ U&lt; </b>
              <table border="1" cellspacing="0">
                <tr bgcolor="B0C4DE">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>����������</th>
                  <th>����. U&lt;5�</th>
                  <th>����.�.3U0</th>
                  <th>����.�.��</th>
                  <th>���</th>
                  <th>Ucp, �</th>
                  <th>t��, ��</th>
                  <th>�������</th>
                  <th>U��, �</th>
                  <th>t��, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                </tr>
                <xsl:for-each select="����������/Rows/VoltageDefenseStruct">
                  <xsl:if test="position() = 3">
                    <tr align="center">
                      <td>U&lt; 1</td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="U5�">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="_x0033_U0">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="���UM"/>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                  <xsl:if test="position() = 4">
                    <tr align="center">
                      <td>U&lt; 2</td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:for-each select="U5�">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="_x0033_U0">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="��">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="���UM"/>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="U��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() = 'true'">����</xsl:if>
                          <xsl:if test="current() = 'false'">���</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
            </xsl:when>
          </xsl:choose>

          <p></p>

          <b>�������</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="B0C4DE">
              <th>�������</th>
              <th>�����</th>
              <th>����������</th>
              <th>����������</th>
              <th>����.</th>
              <th>t��, ��</th>
              <th>t��, ��</th>
              <th>����.</th>
              <th>����/���</th>
              <th>�����������</th>
              <th>����</th>
            </tr>
            <xsl:for-each select="�������/Rows/ExternalDefenseStruct">
              <xsl:choose>
                <xsl:when test="$conf = 'A2' = 'A3' = 'A4'">
                  <tr align="center">
                    <td>
                      B3 - <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="�����"/>
                    </td>
                    <td>
                      <xsl:value-of select="����"/>
                    </td>
                    <td>
                      <xsl:value-of select="����������"/>
                    </td>
                    <td>
                      <xsl:value-of select="����"/>
                    </td>
                    <td>
                      <xsl:value-of select="t��"/>
                    </td>
                    <td>
                      <xsl:value-of select="t��"/>
                    </td>
                    <td>
                      <xsl:value-of select="����_�������"/>
                    </td>
                    <td>
                      <xsl:value-of select="�������"/>
                    </td>
                    <td>
                      <xsl:value-of select="���"/>
                    </td>
                    <td>
                      <xsl:value-of select="����"/>
                    </td>
                  </tr>
                </xsl:when>
                <xsl:when test="$conf = 'A5'">
                  <xsl:if test="position() &lt; 21">
                    <tr align="center">
                      <td>
                        B3 - <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:if test="position() &lt; 17">
                    <tr align="center">
                      <td>
                        B3 - <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:value-of select="����"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </table>
          <p></p>
          </xsl:for-each>

        <h3>����</h3>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">���. ����.</th>
            <td>
              <xsl:value-of select="��901/����/�����"/>
            </td>
          </tr>
        </table>
        <p></p>

        <b>���� ��</b>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">���� ���� �� �� ��.</th>
            <td>
              <xsl:value-of select="��901/����/�������������"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">�� ����</th>
            <td>
              <xsl:value-of select="��901/����/��_����"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����1, �� (�� ����)
            </th>
            <td>
              <xsl:value-of select="��901/����/�1"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              ���� 2
            </th>
            <td>
              <xsl:value-of select="��901/����/����_2"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����2, �� (�� ����.)
            </th>
            <td>
              <xsl:value-of select="��901/����/�2"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              ���� 3
            </th>
            <td>
              <xsl:value-of select="��901/����/����_3"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����3, �� (���. �������.)
            </th>
            <td>
              <xsl:value-of select="��901/����/�3"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="B0C4DG">
              t����, ��
            </th>
            <td>
              <xsl:value-of select="��901/����/T_����������"/>
            </td>
          </tr>
        </table>

        <b>������ �����</b>

        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">
              ��1
            </th>
            <td>
              <xsl:value-of select="��901/����/��1_������_�����"/>
            </td>
          </tr>
        </table>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">
              ��2
            </th>
            <td>
              <xsl:value-of select="��901/����/��2_������_�����"/>
            </td>
          </tr>
        </table>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">
              ��
            </th>
            <td>
              <xsl:value-of select="��901/����/��_������_�����"/>
            </td>
          </tr>
        </table>

        <b>����. ����</b>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">
              ��1
            </th>
            <td>
              <xsl:value-of select="��901/����/��1_����_����"/>
            </td>
          </tr>
        </table>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">
              ��2
            </th>
            <td>
              <xsl:value-of select="��901/����/��2_����_����"/>
            </td>
          </tr>
        </table>
        <table border="1" cellspacing="0">
          <tr>
            <th bgcolor="B0C4DG">
              ��
            </th>
            <td>
              <xsl:value-of select="��901/����/��_����_����"/>
            </td>
          </tr>
        </table>
        <p></p>

          <b>���� �������������</b>
          <table border="1" cellspacing="0">
            <tr>
              <th bgcolor="B0C400">
                ����
              </th>
              <td>
                <xsl:value-of select="��901/����/����_����_����"/>
              </td>
            </tr>
            <tr>
              <th bgcolor="B0C400">
                �� ����
              </th>
              <td>
                <xsl:value-of select="��901/����/��_����_����_����"/>
              </td>
            </tr>
            <table border="1" cellspacing="0">
              <tr bgcolor="4682B4">
                <th>�������</th>
                <th>��. �����</th>
                <th>l����, l�</th>
                <th>t ����1 ��., �� (�� ����)</th>
                <th>t ����2 ��., �� (�� ����. ���.)</th>
              </tr>
              <xsl:for-each select="��901/����_�������������/Rows/UrovConnectionStruct">
                <xsl:choose>
                  <xsl:when test="$conf = 'A2' = 'A3' = 'A4'">
                    <tr align="center">
                      <td>
                        ������������� <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="��_�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="I_����"/>
                      </td>
                      <td>
                        <xsl:value-of select="T1"/>
                      </td>
                      <td>
                        <xsl:value-of select="T2"/>
                      </td>
                    </tr>
                  </xsl:when>
                  <xsl:when test="$conf = 'A5'">
                    <xsl:if test="position() &lt; 21">
                      <tr align="center">
                        <td>
                          ������������� <xsl:value-of select="position()"/>
                        </td>
                        <td>
                          <xsl:value-of select="��_�����"/>
                        </td>
                        <td>
                          <xsl:value-of select="I_����"/>
                        </td>
                        <td>
                          <xsl:value-of select="T1"/>
                        </td>
                        <td>
                          <xsl:value-of select="T2"/>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:if test="position() &lt; 17">
                      <tr align="center">
                        <td>
                          ������������� <xsl:value-of select="position()"/>
                        </td>
                        <td>
                          <xsl:value-of select="��_�����"/>
                        </td>
                        <td>
                          <xsl:value-of select="I_����"/>
                        </td>
                        <td>
                          <xsl:value-of select="T1"/>
                        </td>
                        <td>
                          <xsl:value-of select="T2"/>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </table>
          </table>
          <p></p>

        <h3>���� � ����������</h3>

        <b>�������� ����</b>
        <table border="1" cellspacing="0">

          <tr bgcolor="4682B4">
            <th>�����</th>
            <th>���</th>
            <th>������</th>
            <th>������, ��</th>
          </tr>

          <xsl:for-each select="��901/����/���_����/����_����">
            <xsl:choose>
              <xsl:when test="$conf = 'A1' = 'A2' = 'A3'">
                <tr align="center">
                  <xsl:if test="position() &lt; 43">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select= "@���" />
                    </td>
                    <td>
                      <xsl:value-of select= "@������" />
                    </td>
                    <td>
                      <xsl:value-of select= "@�����" />
                    </td>
                  </xsl:if>
                </tr>
              </xsl:when>
              <xsl:when test="$conf = 'A4' = 'A5'">
                <tr align="center">
                  <xsl:if test="position() &lt; 35">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select= "@���" />
                    </td>
                    <td>
                      <xsl:value-of select= "@������" />
                    </td>
                    <td>
                      <xsl:value-of select= "@�����" />
                    </td>
                  </xsl:if>
                </tr>
              </xsl:when>
              <xsl:when test="$conf = 'A6'">
                <tr align="center">
                  <xsl:if test="position() &lt; 51">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select= "@���" />
                    </td>
                    <td>
                      <xsl:value-of select= "@������" />
                    </td>
                    <td>
                      <xsl:value-of select= "@�����" />
                    </td>
                  </xsl:if>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr align="center">
                  <xsl:if test="position() &lt; 19">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select= "@���" />
                    </td>
                    <td>
                      <xsl:value-of select= "@������" />
                    </td>
                    <td>
                      <xsl:value-of select= "@�����" />
                    </td>
                  </xsl:if>
                </tr>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </table>
        <p></p>

        <b>����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>�����</th>
            <th>���</th>
            <th>������ "�������"</th>
            <th>������ "�������"</th>
            <th>����� ������</th>
          </tr>

          <xsl:for-each select="��901/����������/���_����������/����_���������">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select= "@���" />
              </td>
              <td>
                <xsl:value-of select= "@������_�������" />
              </td>
              <td>
                <xsl:value-of select= "@������_�������" />
              </td>
              <td>
                <xsl:value-of select= "@�����_������" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <b>���� �������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="4682B4">
            <th>���������� �������������</th>
            <th>����������� �������������</th>
            <th>������������� ����� ��</th>
            <th>����� ����������� (����)</th>
            <th>������</th>
            <th>������������� ����� TH</th>
            <th>�������, ��</th>
          </tr>

          <xsl:for-each select="��901/����_�������������">
            <tr align="center">
              <td>
                <xsl:for-each select="@�������������_1">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="@�������������_2">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="@�������������_3">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="@�������������_4">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="@�������������_5">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="@�������������_6">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select= "@�������_����_�������������" />
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellspacing="0" cellpadding="4">
          <tr>����� �����������</tr>
          <xsl:for-each select="��901/�������_�����">
            <tr align="left">
              <th bgcolor="90EE90">1. �� ����� � ������ �������</th>
              <td>
              <xsl:for-each select="@�����_��_�����_�_������_�������">
                <xsl:if test="current() = 'true'">��</xsl:if>
                <xsl:if test="current() = 'false'">���</xsl:if>
              </xsl:for-each>
              </td>
            </tr>
            <tr align="left">
              <th bgcolor="90EE90">2. �� ����� � ������ ������</th>
              <td>
                <xsl:for-each select="@�����_��_�����_�_������_������">
                  <xsl:if test="current() = 'true'">��</xsl:if>
                  <xsl:if test="current() = 'false'">���</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������������ ����������</tr>
          <th bgcolor="90EE90">���. I�2��2</th>
          <td>
            <xsl:value-of select="��901/������������_Ig/IndConf"/>
          </td>
        </table>
        <p></p>

        <h2>���������� ��������</h2>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>����������� ����</tr>
          <tr bgcolor="FF8C00">
            <th>�</th>
            <th>���</th>
            <th>������</th>
            <th>T�����., ��</th>
          </tr>
          <xsl:for-each select="��901/����/���_����/����_����">
            <xsl:choose>
              <xsl:when test="$conf = 'A1' = 'A2' = 'A3'">
                <xsl:if test="position() &gt; 42">
                  <tr align="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@������"/>
                    </td>
                    <td>
                      <xsl:value-of select="@�����"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>
              <xsl:when test="$conf = 'A4' = 'A5'">
                <xsl:if test="position() &gt; 34">
                  <tr align="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@������"/>
                    </td>
                    <td>
                      <xsl:value-of select="@�����"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>
              <xsl:when test="$conf = 'A6'">
                <xsl:if test="position() &gt; 50">
                  <tr align="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@������"/>
                    </td>
                    <td>
                      <xsl:value-of select="@�����"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="position() &gt; 18">
                  <tr align="center">
                    <td>
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���"/>
                    </td>
                    <td>
                      <xsl:value-of select="@������"/>
                    </td>
                    <td>
                      <xsl:value-of select="@�����"/>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>��������������� RS-��������</tr>
          <tr bgcolor="FF8C00">
            <th>�</th>
            <th>���</th>
            <th>���� R</th>
            <th>���� S</th>
          </tr>
          <xsl:for-each select="��901/RS_��������/���_rs_��������/RsTriggersStruct">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select="@���"/>
              </td>
              <td>
                <xsl:value-of select="@����_R"/>
              </td>
              <td>
                <xsl:value-of select="@����_S"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>
        
        <h3>���</h3>
          <table border="1" cellspacing="0">
            <tr bgcolor="4682B4">
              <th>�����</th>
              <th>������������</th>
            </tr>
            <xsl:for-each select="��901/���_���/���">
              <tr>
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:value-of select="current()"/>|
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:for-each>
          </table>
          <p></p>

        <xsl:choose>
          <xsl:when test="$conf = 'A5'">
            <h3>��������� TH</h3>

            <b>������������ �������������� ����������</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>����</th>
                <th>���n</th>
                <th>���� ����. ������. ���</th>
                <th>���� ����. ������. ��n</th>
              </tr>
              <tr align="center">
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/KTHL"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/KTHX"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/���������"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/��������n"/>
                </td>
              </tr>
            </table>

            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>�������� 3U0</th>
                <th>d3U0, �</th>
                <th>Td3U0, ��</th>
                <th>����������</th>
              </tr>
              <tr align="center">
                <td>
                  <xsl:for-each select="��901/�������������/���������_��/��������_3U0">
                    <xsl:if test="current() ='false'">
                      ���
                    </xsl:if>
                    <xsl:if test="current() ='true'">
                      ��
                    </xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/d3U0"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/Td3U0"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/d3U0����������"/>
                </td>
              </tr>
            </table>

            <table border="1" cellspacing="0">
              <tr bgcolor="FFFFCC">
                <th>�������� �����������</th>
                <th>Umin/Umsx, %</th>
                <th>T��.��, ��</th>
                <th>����������</th>
              </tr>
              <tr align="center">
                <td>
                  <xsl:for-each select="��901/�������������/���������_��/��������_����������">
                    <xsl:if test="current() ='false'">
                      ���
                    </xsl:if>
                    <xsl:if test="current() ='true'">
                      ��
                    </xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/Uns"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/Tuns"/>
                </td>
                <td>
                  <xsl:value-of select="��901/�������������/���������_��/Uns����������"/>
                </td>
              </tr>
            </table>
          </xsl:when>
        </xsl:choose>

        <h3>�����������</h3>
        <b>��������� ������������</b>
        <table border="1" cellspacing="0">
          <td>
            <table border="1" cellspacing="0">
              <tr bgcolor="98FB98">
                <th>�����������</th>
                <th>����. ����������, %</th>
                <th>������. ��</th>
                <th>���� �����</th>
                <th>��1</th>
                <th>��2</th>
                <th>��</th>
              </tr>
              <tr align="center">
                <td>
                  <xsl:value-of select="��901/������������_�����������/������������_���/����������_�����������"/> - <xsl:value-of select="��901/����������_���_��"/>
                </td>
                <td>
                  <xsl:value-of select="��901/������������_�����������/������������_���/����������"/>
                </td>
                <td>
                  <xsl:value-of select="��901/������������_�����������/������������_���/��������"/>
                </td>
                <td>
                  <xsl:value-of select="��901/������������_�����������/����_�������_������������/@�����"/>
                </td>
                <td>
                 <xsl:for-each select="��901/������������_�����������/������������_���/��1">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                 </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��901/������������_�����������/������������_���/��2">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="��901/������������_�����������/������������_���/��">
                  <xsl:if test="current() ='false'">
                    ���
                  </xsl:if>
                  <xsl:if test="current() ='true'">
                    ��
                  </xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </table>

              <table border="1" cellpadding="4" cellspacing="0">
                <tr>��������������� ���������� ������</tr>
                <tr bgcolor="FF69B4">
                  <th>�����</th>
                  <th>����</th>
                  <th>�����</th>
                </tr>
                <xsl:for-each select="��901/������������_�����������/������������_�������/���_������/ChannelWithBase">
                  <xsl:choose>
                    <xsl:when test="$conf = 'A1'">
                      <xsl:if test="position() &lt; 65">
                        <tr align="center">
                          <td>
                            <xsl:value-of select="position()"/>
                          </td>
                          <td>
                            <xsl:value-of select="@����"/>
                          </td>
                          <td>
                            <xsl:value-of select="@�����"/>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test="$conf = 'A3' = 'A7'">
                      <xsl:if test="position() &lt; 89">
                        <tr align="center">
                          <td>
                            <xsl:value-of select="position()"/>
                          </td>
                          <td>
                            <xsl:value-of select="@����"/>
                          </td>
                          <td>
                            <xsl:value-of select="@�����"/>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:when>
                    <xsl:when test="$conf = 'A2' = 'A5'">
                      <xsl:if test="position() &lt; 89">
                        <tr align="center">
                          <td>
                            <xsl:value-of select="position()"/>
                          </td>
                          <td>
                            <xsl:value-of select="@����"/>
                          </td>
                          <td>
                            <xsl:value-of select="@�����"/>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:if test="position() &lt; 81">
                        <tr align="center">
                          <td>
                            <xsl:value-of select="position()"/>
                          </td>
                          <td>
                            <xsl:value-of select="@����"/>
                          </td>
                          <td>
                            <xsl:value-of select="@�����"/>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </table>

              <p></p>
            </td>
          </table>

          <b>������������ Ethernet</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="B0C4DE">
              <th>IP-�����</th>
            </tr>
            <xsl:for-each select="��901/IP">
              <tr align="center">
                <td>
                  <xsl:value-of select="IpHi2"/>.
                  <xsl:value-of select="IpHi1"/>.
                  <xsl:value-of select="IpLo2"/>.
                  <xsl:value-of select="IpLo1"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        <p></p>

        <b>��������������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="B0C4DE">
            <th>IP-�����</th>
          </tr>
          <xsl:for-each select="��901/IP">
            <tr align="center">
              <td>
                <xsl:value-of select="��������������"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>


      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
