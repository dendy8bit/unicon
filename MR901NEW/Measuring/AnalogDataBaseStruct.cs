﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR901NEW.Measuring
{
    /// <summary>
    /// МР 901 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private int _i1; // ток 1
        [Layout(1)] private int _i2; // ток 2
        [Layout(2)] private int _i3; // ток 3
        [Layout(3)] private int _i4; // ток 4
        [Layout(4)] private int _i5; // ток 5
        [Layout(5)] private int _i6; // ток 6
        [Layout(6)] private int _i7; // ток 7
        [Layout(7)] private int _i8; // ток 8
        [Layout(8)] private int _i9; // ток 9
        [Layout(9)] private int _i10; // ток 10
        [Layout(10)] private int _i11; // ток 11
        [Layout(11)] private int _i12; // ток 12
        [Layout(12)] private int _i13; // ток 13
        [Layout(13)] private int _i14; // ток 14
        [Layout(14)] private int _i15; // ток 15
        [Layout(15)] private int _i16; // ток 16
        [Layout(16)] private int _i17; // ток 9
        [Layout(17)] private int _i18; // ток 10
        [Layout(18)] private int _i19; // ток 11
        [Layout(19)] private int _i20; // ток 12
        [Layout(20)] private int _i21; // ток 13
        [Layout(21)] private int _i22; // ток 14
        [Layout(22)] private int _i23; // ток 15
        [Layout(23)] private int _i24; // ток 16
        //ток
        [Layout(24)] private int _ida1; // ток СШ1
        [Layout(25)] private int _ida2; // ток СШ2
        [Layout(26)] private int _ida3; // ток ПО
        //ток
        [Layout(27)] private int _iba1; // ток СШ1
        [Layout(28)] private int _iba2; // ток СШ2
        [Layout(29)] private int _iba3; // ток ПО
        //ток
        [Layout(30)] private int _idb1; // ток СШ1
        [Layout(31)] private int _idb2; // ток СШ2
        [Layout(32)] private int _idb3; // ток ПО
        //ток
        [Layout(33)] private int _ibb1; // ток СШ1
        [Layout(34)] private int _ibb2; // ток СШ2
        [Layout(35)] private int _ibb3; // ток ПО
        //ток
        [Layout(36)] private int _ibc1; // ток СШ1
        [Layout(37)] private int _ibc2; // ток СШ2
        [Layout(38)] private int _ibc3; // ток ПО
        //res
        [Layout(39)] private int _res1; // ток СШ1
        [Layout(40)] private int _res2; // ток СШ2
        [Layout(41)] private int _res3; // ток ПО
        //напряжения
        [Layout(42)] private int _ua;
        [Layout(43)] private int _ub;
        [Layout(44)] private int _uc;
        [Layout(45)] private int _un;
        [Layout(46)] private int _uab;
        [Layout(47)] private int _ubc;
        [Layout(48)] private int _uca;
        [Layout(49)] private int _u0;
        [Layout(50)] private int _u1;
        [Layout(51)] private int _u2;
        [Layout(52)] private int _u30;
        [Layout(53)] private int _res4;

        [Layout(54)] private int a1; // 
        [Layout(55)] private int a2; // 
        [Layout(56)] private int a3; // 
        //ток
        [Layout(57)] private int a4; // 
        [Layout(58)] private int a5; // 
        [Layout(59)] private int a6; // 
        //ток
        [Layout(60)] private int a7; // 
        [Layout(61)] private int a8; // 
        [Layout(62)] private int a9; // 
        //ток
        [Layout(63)] private int a10; // 
        [Layout(64)] private int a11; // 
        [Layout(65)] private int a12; // 
        //ток
        [Layout(66)] private int a13; // 
        [Layout(67)] private int a14; // 
        [Layout(68)] private int a15; // 
        //res
        [Layout(69)] private int a16; // 
        [Layout(70)] private int a17; // 
        [Layout(71)] private int a18; //
        #endregion [Private fields]
        
        #region [Properties]
        public string GetI1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[1]);
        }

        public string GetI2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[2]);
        }

        public string GetI3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i3);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[3]);
        }

        public string GetI4(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i4);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[4]);
        }

        public string GetI5(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i5);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[5]);
        }

        public string GetI6(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i6);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[6]);
        }

        public string GetI7(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i7);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[7]);
        }

        public string GetI8(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i8);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[8]);
        }

        public string GetI9(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i9);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[9]);
        }

        public string GetI10(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i10);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[10]);
        }

        public string GetI11(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i11);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[11]);
        }

        public string GetI12(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i12);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[12]);
        }

        public string GetI13(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i13);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[13]);
        }

        public string GetI14(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i14);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[14]);
        }

        public string GetI15(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i15);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[15]);
        }

        public string GetI16(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i16);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[16]);
        }

        public string GetI17(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i17);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[17]);
        }

        public string GetI18(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i18);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[18]);
        }

        public string GetI19(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i19);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[19]);
        }

        public string GetI20(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i20);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[20]);
        }

        public string GetI21(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i21);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[21]);
        }

        public string GetI22(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i22);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[22]);
        }

        public string GetI23(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i23);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[23]);
        }

        public string GetI24(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._i24);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[24]);
        }

        public string GetIda1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._ida1);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[0]);
        }

        public string GetIda2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._ida2);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[0]);
        }

        public string GetIda3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._ida3);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[0]);
        }

        public string GetIba1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._iba1);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[0]);
        }

        public string GetIba2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._iba2);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[0]);
        }

        public string GetIba3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            int value = this.GetMean(list, o => o._iba3);
            return ValuesConverterCommon.Analog.GetIBig(value, factors[0]);
        }

        private int GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, int> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (int)(sum / (double)count);
        }

        public string GetUa(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetUb(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetUc(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetUab(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetUbc(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetUca(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetU1(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetU2(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string Get3U0(List<AnalogDataBaseStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._u30);
            return ValuesConverterCommon.Analog.GetUBig(value, kthl);
        }

        public string GetUn(List<AnalogDataBaseStruct> list, double kthx)
        {
            var value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetUBig(value, kthx);
        }
        #endregion [Properties]
    }
}
