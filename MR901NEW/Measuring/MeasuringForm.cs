﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901New.Measuring;
using BEMN.MR901NEW.Configuration.Structures.Connections;
using BEMN.MR901NEW.Properties;

namespace BEMN.MR901NEW.Measuring
{
    public partial class MeasuringForm : Form, IFormView
    {
        #region [Constants]
        private const string RESET_INDICATION = "Сброс индикации";
        private const string RESET_FAULT_SYSTEM_JOURNAL = "Сбросить наличие неисправности по ЖС";
        private const string RESET_OSC_JOURNAL = "Сбросить журнал осциллографа";
        private const string RESET_ALARM_JOURNAL = "Сбросить журнал аварий";
        private const string RESET_SYSTEM_JOURNAL = "Сбросить журнал системы";
        private const string SWITCH_GROUP_USTAVKI = "Переключить на группу уставок №{0}?";
        private const string RESET_TT = "Сбросить неисправности ТТ";
        private const string START_OSC = "Запустить осциллограф";
        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseBigStruct> _discretDataBase;
        private readonly MemoryEntity<ConnectionsAndTransformer> _connections;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly AveragerTime<AnalogDataBaseStruct> _averagerTime;
        private readonly MemoryEntity<OneWordStruct> _groupUstavki;
        private readonly MR901New _device;
        private int[] _factors;
        private string[] _bindings;
        private double _kthl;
        private double _kthx;

        private ushort? _numGroup;
        private int ind;
        /// <summary>
        /// Аналоговые токи
        /// </summary>
        private TextBox[] _analogCurrentBoxes;

        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Дествующий ток
        /// </summary>
        private LedControl[] _operatingCurrent;

        /// <summary>
        /// Максимальный ток
        /// </summary>
        private LedControl[] _maximumCurrent;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;
        /// <summary>
        /// Защиты U
        /// </summary>
        private LedControl[] _voltageDiscrets;

        private List<bool> _voltageBoolList; 
        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// УРОВ
        /// </summary>
        private LedControl[] _urov;

        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _state;
        private LedControl[] _groupState;
        
        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private Diod[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faults;

        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;

        private LedControl[] _neisprTNLeds;

        /// <summary>
        /// Энергонезависимые RS-Тригеры
        /// </summary>
        private LedControl[] _rsTriggers;

        #endregion [Private fields]


        #region [Ctor's]

        public MeasuringForm()
        {
            this.InitializeComponent();
        }
        
        public MeasuringForm(MR901New device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;

            AllConnectionStruct.SetDeviceConnectionsType(Strings.DeviceType);

            this._groupUstavki = device.GroupUstavki;
            _groupUstavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {

            });
            this._groupUstavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Группа уставок успешно изменена", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this._groupUstavki.LoadStruct();
            });
            this._groupUstavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно изменить группу уставок", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));

            this._averagerTime = new AveragerTime<AnalogDataBaseStruct>(this, 500);
            this._averagerTime.Tick += this.AveragerTimeTick;

            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", device, 0x0E00);
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._discretDataBase = new MemoryEntity<DiscretDataBaseBigStruct>("Дискретная БД", device, 0x0D00);
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);

            this._connections = new MemoryEntity<ConnectionsAndTransformer>("Присоединения", device, 0x106C);
            this._connections.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
                this._factors = this._connections.Value.Connections.IttJoinMeasuring.Select(val => val*40).ToArray();

                if (voltageGroup.Visible)
                {
                    this._kthl = this._connections.Value.TN.KthlValue;
                    this._kthx = this._connections.Value.TN.KthxValue;
                }
                this._analogDataBase.LoadStructCycle();
            });

            this._dateTime = new MemoryEntity<DateTimeStruct>("Дата/время", device, 0x200);
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);
            
            device.Configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._analogDataBase.RemoveStructQueries();
                this._connections.LoadStruct();
            });

            this.Init();

            _commandComboBox.SelectedIndex = 0;
        }

        private void Init()
        {
            this._rsTriggers = new[]
            {
                this._rst1, this._rst2, this._rst3, this._rst4, this._rst5, this._rst6, this._rst7, this._rst8,
                this._rst9, this._rst10, this._rst11, this._rst12, this._rst13, this._rst14, this._rst15, this._rst16
            };

            this._freeLogic = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32,
                this._ssl33, this._ssl34, this._ssl35, this._ssl36, this._ssl37, this._ssl38, this._ssl39, this._ssl40,
                this._ssl41, this._ssl42, this._ssl43, this._ssl44, this._ssl45, this._ssl46, this._ssl47, this._ssl48
            };

            this._maximumCurrent = new[]
            {
                this._i1Io, this._i1, this._i2Io, this._i2, this._i3Io, this._i3, this._i4Io, this._i4, this._i5Io,
                this._i5,
                this._i6Io, this._i6, this._i7Io, this._i7, this._i8Io, this._i8, this._i9Io, this._i9, this._i10Io,
                this._i10,
                this._i11Io, this._i11, this._i12Io, this._i12, this._i13Io, this._i13, this._i14Io, this._i14,
                this._i15Io,
                this._i15, this._i16Io, this._i16, this._i17Io, this._i17, this._i18Io, this._i18, this._i19Io,
                this._i19,
                this._i20Io, this._i20, this._i21Io, this._i21, this._i22Io, this._i22, this._i23Io, this._i23,
                this._i24Io,
                this._i24, this._i25Io, this._i25, this._i26Io, this._i26, this._i27Io, this._i27, this._i28Io,
                this._i28,
                this._i29Io, this._i29, this._i30Io, this._i30, this._i31Io, this._i31, this._i32Io, this._i32
            };

            this._operatingCurrent = new[]
            {
                this._difDefenceInstantaneousChtoSh1, this._difDefenceInstantaneousSrabSh1,
                this._difDefenceInstantaneousChtoSh2,
                this._difDefenceInstantaneousSrabSh2, this._difDefenceInstantaneousChtoPo,
                this._difDefenceInstantaneousSrabPo,
                this._difDefenceActualIoSh1, this._difDefenceActualChtoSh1, this._difDefenceActualSrabSh1,
                this._difDefenceActualIoSh2, this._difDefenceActualChtoSh2, this._difDefenceActualSrabSh2,
                this._difDefenceActualIoPo, this._difDefenceActualChtoPo, this._difDefenceActualSrabPo
            };
            this._urov = new[]
            {
                this._urovSH1, this._urovSH2, this._urovPO,
                this._urovPr1, this._urovPr2, this._urovPr3, this._urovPr4, this._urovPr5, this._urovPr6,
                this._urovPr7, this._urovPr8, this._urovPr9, this._urovPr10, this._urovPr11, this._urovPr12,
                this._urovPr13, this._urovPr14, this._urovPr15, this._urovPr16, this._urovPr17, this._urovPr18,
                this._urovPr19,this._urovPr20,this._urovPr21,this._urovPr22,this._urovPr23,this._urovPr24
            };

            this._state = new[]
            {
                this._fault,this._mainGroupOfSetpoints,this._reservedGroupOfSetpoints,this._alarm,this._signalization
            };

            this._groupState = new[]
            {
                this._mainGroup, this._reservedGroup
            };

            this._indicators = new[]
            {
                this.diod1, this.diod2, this.diod3, this.diod4, this.diod5, this.diod6, this.diod7,
                this.diod8, this.diod9, this.diod10, this.diod11, this.diod12
            };

            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16,
                this._vz17, this._vz18, this._vz19, this._vz20, this._vz21, this._vz22, this._vz23, this._vz24
            };

            this._voltageDiscrets = new[]
            {
                this._u1bIO, this._u1b, this._u2bIO, this._u2b, this._u1lIO, this._u1l, this._u2lIO, this._u2l
            };

            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };
            this._analogCurrentBoxes = new[]
            {
                this._i1TextBox, this._i2TextBox, this._i3TextBox, this._i4TextBox, this._i5TextBox, this._i6TextBox,
                this._i7TextBox, this._i8TextBox, this._i9TextBox, this._i10TextBox, this._i11TextBox, this._i12TextBox,
                this._i13TextBox, this._i14TextBox, this._i15TextBox, this._i16TextBox, this._i17TextBox, this._i18TextBox,
                this._i19TextBox, this._i20TextBox, this._i21TextBox,this._i22TextBox, this._i23TextBox, this._i24TextBox,

                this._id1TextBox,this._id2TextBox,this._id3TextBox, this._it1TextBox, this._it2TextBox, this._it3TextBox
            };

            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal, this._availabilityFaultSystemJournal
            };

            this._faults = new[]
            {
                this._faultHardware, this._faultSoftware, this._faultMeasuring, this._faultUrov,
                this._faultModule1, this._faultModule2, this._faultModule3, this._faultModule4, this._faultModule5,
                this._faultModule6,this._faultSetpoints, this._faultGroupsOfSetpoints, this._faultSystemJournal,
                this._faultAlarmJournal, this._faultOsc, this._faultLogic, this._faultTt1, this._faultTt2, this._faultTt3
            };

            this.CreateDeviceType();
        }

        private void CreateDeviceType()
        {
            Label[] currentsLabel =
            {
                this._i17Label, this._i18Label, this._i19Label, this._i20Label, this._i21Label, this._i22Label, this._i23Label, this._i24Label
            };

            TextBox[] currents =
            {
                this._i17TextBox, this._i18TextBox, this._i19TextBox, this._i20TextBox, this._i21TextBox,
                this._i22TextBox, this._i23TextBox, this._i24TextBox
            };

            Label[] urovLabels =
            {
                this._urovPrLabel17, this._urovPrLabel18, this._urovPrLabel19, this._urovPrLabel20, this._urovPrLabel21,
                this._urovPrLabel22, this._urovPrLabel23, this._urovPrLabel24
            };

            LedControl[] urov =
            {
                this._urovPr17, this._urovPr18, this._urovPr19, this._urovPr20, this._urovPr21, this._urovPr22,
                this._urovPr23, this._urovPr24
            };

            switch (this._device.DevicePlant)
            {
                case "T16N0D64R43":
                    foreach (TextBox box in currents)
                    {
                        box.Visible = false;
                    }
                    foreach (Label label in currentsLabel)
                    {
                        label.Visible = false;
                    }
                    foreach (Label urovLabel in urovLabels)
                    {
                        urovLabel.Visible = false;
                    }
                    foreach (LedControl control in urov)
                    {
                        control.Visible = false;
                    }
                    this.connectionCurrentsGB.Size = new Size(230, 239);
                    this.CreateModules(this.relayGroupBox, new [] { 16, 16, 10}, "Р", out this._relays, new[] { 16, 16, 6 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16, 16, 16 }, "Д", out this._discretInputs, new[] { 0 });
                    break;
                case "T24N0D40R35":
                    this.CreateModules(this.relayGroupBox, new[] { 16, 16, 2 }, "Р", out this._relays, new[] { 16, 16, 14 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16, 8 }, "Д", out this._discretInputs, new[] { 0 });
                    break;
                case "T24N0D24R51":
                    this.CreateModules(this.relayGroupBox, new[] { 16, 16, 16, 2 }, "Р", out this._relays, new[] { 16, 14 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 8 }, "Д", out this._discretInputs, new[] { 0 });
                    break;
                case "T24N0D32R43":
                    this.CreateModules(this.relayGroupBox, new[] { 16, 16, 10 }, "Р", out this._relays, new[] { 16, 16, 6 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16 }, "Д", out this._discretInputs, new[] { 0 });
                    break;
                case "T20N4D40R35":
                    this.CreateModules(this.relayGroupBox, new[] { 16, 16, 2 }, "Р", out this._relays, new[] { 16, 16, 14 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16, 8 }, "Д", out this._discretInputs, new[] { 0 });
                    this.voltageGroup.Visible = true;
                    this.voltageDiscretsGroup.Visible = true;
                    this._voltageBoolList = new List<bool>();
                    for (int i = 4; i < currents.Length; i++)
                    {
                        currents[i].Visible = false;
                        currentsLabel[i].Visible = false;
                    }

                    this._vz24label.Visible = this._vz23label.Visible = this._vz22label.Visible = this._vz21label.Visible = false;
                    this._vz21.Visible = this._vz22.Visible = this._vz23.Visible = this._vz24.Visible = false;

                    this._urovPr21.Visible = this._urovPr22.Visible = this._urovPr23.Visible = this._urovPr24.Visible = false;
                    this._urovPrLabel21.Visible = this._urovPrLabel22.Visible = this._urovPrLabel23.Visible = this._urovPrLabel24.Visible = false;

                    this._neisprTNGroup.Visible = true;
                    this._neisprTN.Visible = true;
                    this._neisprTNLabel.Visible = true;
                    this._neisprTNLeds = new[]
                    {
                        this._neisprTN, this._neisprTN3U0, this._neisprTNns, this._neisprTNUabc, new LedControl(),  this._neisprTNUn
                    };

                    break;
                case "T20N4D32R43":
                    this.CreateModules(this.relayGroupBox, new[] { 16, 16, 10 }, "Р", out this._relays, new[] { 16, 16, 6 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16 }, "Д", out this._discretInputs, new[] { 0 });
                    this.voltageGroup.Visible = true;
                    this.voltageDiscretsGroup.Visible = true;
                    this._voltageBoolList = new List<bool>();
                    for (int i = 4; i < currents.Length; i++)
                    {
                        currents[i].Visible = false;
                        currentsLabel[i].Visible = false;
                    }

                    this._vz24label.Visible = this._vz23label.Visible = this._vz22label.Visible = this._vz21label.Visible = false;
                    this._vz21.Visible = this._vz22.Visible = this._vz23.Visible = this._vz24.Visible = false;

                    this._urovPr21.Visible = this._urovPr22.Visible = this._urovPr23.Visible = this._urovPr24.Visible = false;
                    this._urovPrLabel21.Visible = this._urovPrLabel22.Visible = this._urovPrLabel23.Visible = this._urovPrLabel24.Visible = false;

                    this._neisprTNGroup.Visible = true;
                    this._neisprTN.Visible = true;
                    this._neisprTNLabel.Visible = true;
                    this._neisprTNLeds = new[]
                    {
                        this._neisprTN, this._neisprTN3U0, this._neisprTNns, this._neisprTNUabc, new LedControl(),  this._neisprTNUn
                    };

                    break;
                case "T16N0D24R19":
                    foreach (TextBox box in currents)
                    {
                        box.Visible = false;
                    }
                    foreach (Label label in currentsLabel)
                    {
                        label.Visible = false;
                    }
                    foreach (Label urovLabel in urovLabels)
                    {
                        urovLabel.Visible = false;
                    }
                    foreach (LedControl control in urov)
                    {
                        control.Visible = false;
                    }
                    this.connectionCurrentsGB.Size = new Size(230, 239);
                    this.CreateModules(this.relayGroupBox, new[] { 16, 2 }, "Р", out this._relays, new[] { 16, 16, 16, 14 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 8 }, "Д", out this._discretInputs, new[] { 0 });

                    // В модификации "T16N0D24R19" нет модуля 6, скрываем его на форме и поднимаем остальные компоненты повыше, чтобы красивее смотрелись (10 строк кода)
                    this.label41.Visible = false;
                    this._faultModule6.Visible = false;

                    // label
                    this.label193.Location = new Point(121, 114);
                    this.label192.Location = new Point(121, 133);
                    this.label200.Location = new Point(121, 152);
                    this._neisprTNLabel.Location = new Point(121, 171);

                    // led
                    this._faultOsc.Location = new Point(106, 114);
                    this._faultAlarmJournal.Location = new Point(106, 133);
                    this._faultSystemJournal.Location = new Point(106, 152);
                    this._neisprTN.Location = new Point(106, 171);

                    break;
                default:
                    foreach (TextBox box in currents)
                    {
                        box.Visible = false;
                    }
                    foreach (Label label in currentsLabel)
                    {
                        label.Visible = false;
                    }
                    foreach (Label urovLabel in urovLabels)
                    {
                        urovLabel.Visible = false;
                    }
                    foreach (LedControl control in urov)
                    {
                        control.Visible = false;
                    }
                    this.connectionCurrentsGB.Size = new Size(230, 239);
                    this.CreateModules(this.relayGroupBox, new[] { 16, 2 }, "Р", out this._relays, new []{ 16, 16, 16, 14 }, true);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 8 }, "Д", out this._discretInputs, new []{0});
                    break;
            }
        }
        
        private void CreateModules(GroupBox mainContainer, int[] counts, string character, out LedControl[] mass, int[] countVirtRele, bool virtRele = false)
        {
            List<LedControl> list = new List<LedControl>();
            int counter = 1;
            for (int module = 0; module < counts.Length; module++)
            {
                Panel moduleGroup = new Panel
                {
                    Size = new Size(48, 262),
                    Location = new Point(6 + 54 * module, 11),
                    Text = string.Empty
                };
                for (int relay = 0; relay < counts[module]; relay++)
                {
                    LedControl relayLed = new LedControl { Location = new Point(6, 15 + 15 * relay) };
                    list.Add(relayLed);
                    Label label = new Label { Location = new Point(25, 15 + 15 * relay), Text = character + counter++, AutoSize = true};
                    moduleGroup.Controls.Add(relayLed);
                    moduleGroup.Controls.Add(label);
                }
                mainContainer.Controls.Add(moduleGroup);
            }

            if (virtRele)
            {
                for (int module = 0; module < countVirtRele.Length; module++)
                {
                    Panel moduleGroup = new Panel
                    {
                        Size = new Size(48, 262),
                        Location = new Point(6 + 54 * module, 11),
                        Text = string.Empty
                    };
                    for (int relay = 0; relay < countVirtRele[module]; relay++)
                    {
                        LedControl relayLed = new LedControl { Location = new Point(6, 15 + 15 * relay) };
                        list.Add(relayLed);
                        Label label = new Label { Location = new Point(25, 15 + 15 * relay), Text = character + counter++, AutoSize = true };
                        moduleGroup.Controls.Add(relayLed);
                        moduleGroup.Controls.Add(label);
                    }
                    _virtualReleGroupBox.Controls.Add(moduleGroup);
                }
            }

            mass = list.ToArray();
        }

        private void AveragerTimeTick()
        {
            this._id1TextBox.Text = this._analogDataBase.Value.GetIda1(this._averagerTime.ValueList, this._factors);
            this._it1TextBox.Text = this._analogDataBase.Value.GetIba1(this._averagerTime.ValueList, this._factors);
            this._id2TextBox.Text = this._analogDataBase.Value.GetIda2(this._averagerTime.ValueList, this._factors);
            this._it2TextBox.Text = this._analogDataBase.Value.GetIba2(this._averagerTime.ValueList, this._factors);
            this._id3TextBox.Text = this._analogDataBase.Value.GetIda3(this._averagerTime.ValueList, this._factors);
            this._it3TextBox.Text = this._analogDataBase.Value.GetIba3(this._averagerTime.ValueList, this._factors);
            this._i1TextBox.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._factors);
            this._i2TextBox.Text = this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._factors);
            this._i3TextBox.Text = this._analogDataBase.Value.GetI3(this._averagerTime.ValueList, this._factors);
            this._i4TextBox.Text = this._analogDataBase.Value.GetI4(this._averagerTime.ValueList, this._factors);
            this._i5TextBox.Text = this._analogDataBase.Value.GetI5(this._averagerTime.ValueList, this._factors);
            this._i6TextBox.Text = this._analogDataBase.Value.GetI6(this._averagerTime.ValueList, this._factors);
            this._i7TextBox.Text = this._analogDataBase.Value.GetI7(this._averagerTime.ValueList, this._factors);
            this._i8TextBox.Text = this._analogDataBase.Value.GetI8(this._averagerTime.ValueList, this._factors);
            this._i9TextBox.Text = this._analogDataBase.Value.GetI9(this._averagerTime.ValueList, this._factors);
            this._i10TextBox.Text = this._analogDataBase.Value.GetI10(this._averagerTime.ValueList, this._factors);
            this._i11TextBox.Text = this._analogDataBase.Value.GetI11(this._averagerTime.ValueList, this._factors);
            this._i12TextBox.Text = this._analogDataBase.Value.GetI12(this._averagerTime.ValueList, this._factors);
            this._i13TextBox.Text = this._analogDataBase.Value.GetI13(this._averagerTime.ValueList, this._factors);
            this._i14TextBox.Text = this._analogDataBase.Value.GetI14(this._averagerTime.ValueList, this._factors);
            this._i15TextBox.Text = this._analogDataBase.Value.GetI15(this._averagerTime.ValueList, this._factors);
            this._i16TextBox.Text = this._analogDataBase.Value.GetI16(this._averagerTime.ValueList, this._factors);
            if (this._i17TextBox.Visible)
            {
                this._i17TextBox.Text = this._analogDataBase.Value.GetI17(this._averagerTime.ValueList, this._factors);
                this._i18TextBox.Text = this._analogDataBase.Value.GetI18(this._averagerTime.ValueList, this._factors);
                this._i19TextBox.Text = this._analogDataBase.Value.GetI19(this._averagerTime.ValueList, this._factors);
                this._i20TextBox.Text = this._analogDataBase.Value.GetI20(this._averagerTime.ValueList, this._factors);
                if (this._i21TextBox.Visible) // конфигурация А5-А7
                {
                    this._i21TextBox.Text = this._analogDataBase.Value.GetI21(this._averagerTime.ValueList, this._factors);
                    this._i22TextBox.Text = this._analogDataBase.Value.GetI22(this._averagerTime.ValueList, this._factors);
                    this._i23TextBox.Text = this._analogDataBase.Value.GetI23(this._averagerTime.ValueList, this._factors);
                    this._i24TextBox.Text = this._analogDataBase.Value.GetI24(this._averagerTime.ValueList, this._factors);
                }
            }
            if (this.voltageGroup.Visible)
            {
                this._ua.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, this._kthl);
                this._ub.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, this._kthl);
                this._uc.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, this._kthl);
                this._un.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, this._kthx);
                this._uab.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._kthl);
                this._ubc.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._kthl);
                this._uca.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._kthl);
                this._u1.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._kthl);
                this._u2.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._kthl);
                this._u30.Text = this._analogDataBase.Value.Get3U0(this._averagerTime.ValueList, this._kthl);
            }
        }

        #endregion [Ctor's]


        #region [Help members]

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);
            LedManager.TurnOffLeds(this._operatingCurrent);
            LedManager.TurnOffLeds(this._maximumCurrent);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._urov);
            LedManager.TurnOffLeds(this._state);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._rsTriggers);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._faults);
            LedManager.TurnOffLeds(this._voltageDiscrets);
            if (this._neisprTNLeds != null) LedManager.TurnOffLeds(_neisprTNLeds);
            this._logicState.State = LedState.Off;
            this._mainGroup.State = LedState.Off;
            this._reservedGroup.State = LedState.Off;
            if (this._neisprTNLeds != null) LedManager.TurnOffLeds(_neisprTNLeds);
            foreach (var indicator in this._indicators)
            {
                indicator.TurnOff();
            }
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, this._discretDataBase.Value.Discrets);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, this._discretDataBase.Value.LogicSignals);
            //Выходные ЛС
            LedManager.SetLeds(this._outputLogicSignals, this._discretDataBase.Value.ExtLogicSignals);
            //Дествующий ток
            LedManager.SetLeds(this._operatingCurrent, this._discretDataBase.Value.DiffCurrents);
            //Максимальный ток
            LedManager.SetLeds(this._maximumCurrent, this._discretDataBase.Value.Currents);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, this._discretDataBase.Value.ExternalDefenses);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, this._discretDataBase.Value.Ssl);
            //УРОВ
            LedManager.SetLeds(this._urov, this._discretDataBase.Value.Urov);
            //Состояния
            LedManager.SetLeds(this._state, this._discretDataBase.Value.State);
            LedManager.SetLeds(_groupState, this._discretDataBase.Value.GroupState);
            //Реле
            LedManager.SetLeds(this._relays, this._discretDataBase.Value.Relays);
            //RS-Тригеры 
            LedManager.SetLeds(this._rsTriggers, this._discretDataBase.Value.RSTriggers);
            //Индикаторы
            for (int i = 0; i < this._indicators.Length; i++)
            {
                this._indicators[i].SetState(this._discretDataBase.Value.Indicators[i]);
            }
            //Контроль
            LedManager.SetLeds(this._controlSignals, this._discretDataBase.Value.Control);
            bool res = this._discretDataBase.Value.Control[7] && !this._discretDataBase.Value.FaultLogic;
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;
            //Неисправности
            LedManager.SetLeds(this._faults, this._discretDataBase.Value.Faults);
            if (this._voltageBoolList != null)
            {
                this._voltageBoolList.Clear();
                this._voltageBoolList.AddRange(this._discretDataBase.Value.ExternalDefenses.Skip(20));
                this._voltageBoolList.AddRange(this._discretDataBase.Value.Urov.Skip(23));
                LedManager.SetLeds(this._voltageDiscrets, this._voltageBoolList.ToArray());
            }
            if (this._neisprTNGroup.Visible)
            {
                LedManager.SetLeds(this._neisprTNLeds, this._discretDataBase.Value.NeisprTN);
            }
        }

        /// <summary>
        /// Ошибка чтения аналоговой базы данных
        /// </summary>
        private void AnalogBdReadFail()
        {
            foreach (TextBox t in this._analogCurrentBoxes)
            {
                t.Text = string.Empty;
            }
        }

        /// <summary>
        /// Прочитана аналоговая база данных
        /// </summary>
        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }
        
        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                if (this._device.MB.BaudeRate < 921600)
                {
                    this._connections.LoadStruct(new TimeSpan(100));
                    this._groupUstavki.LoadStructCycle(new TimeSpan(100));
                    this._discretDataBase.LoadStructCycle(new TimeSpan(100));
                    this._dateTime.LoadStructCycle(new TimeSpan(100));
                }
                else
                {
                    this._connections.LoadStruct();
                    this._groupUstavki.LoadStructCycle();
                    this._discretDataBase.LoadStructCycle();
                    this._dateTime.LoadStructCycle();
                }
            }
            else
            {
                this._analogDataBase.RemoveStructQueries();
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        #endregion [Help members]


        #region [Events Handlers]
        private void Mr901MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void Mr901MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._analogDataBase.RemoveStructQueries();
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ind = 0;
            if (ind == this._groupUstavki.Value.Word) return;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _reserveGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ind = 1;
            if (ind == this._groupUstavki.Value.Word) return;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D01, RESET_SYSTEM_JOURNAL);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D02, RESET_ALARM_JOURNAL);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D03, RESET_OSC_JOURNAL);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D04, RESET_FAULT_SYSTEM_JOURNAL);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D05, RESET_INDICATION);
        }

        private void ResetTtClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0A, RESET_TT);
        }
        
        private void StartOscClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0B, START_OSC);
        }
        
        private void dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }


        private void startLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D09, true, "Запуск СПЛ", this._device);
        }

        private void stopLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D08, true, "Останов СПЛ", this._device);
        }

        private void _commandBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            int i = _commandComboBox.SelectedIndex;
            this.ConfirmCommand((ushort)(0x0D20 + i), "Выполнить команду " + (i + 1));
        }

        private void ConfirmCommand(ushort address, string command)
        {
            DialogResult res = MessageBox.Show(command + "?", "Подтверждение комманды", MessageBoxButtons.YesNo);
            if (res == DialogResult.No) return;
            this._discretDataBase.SetBitByAdress(address, command);
        }

        #endregion [Events Handlers]

        #region [IFormView Members]

        public Type FormDevice => typeof(MR901New);

        public Type ClassType => typeof(MeasuringForm);

        public bool ForceShow => false;

        public Image NodeImage => Resources.measuring.ToBitmap();

        public string NodeName => "Измерения";

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        public bool Multishow { get; private set; }


        #endregion [INodeView Members]
    }
}
