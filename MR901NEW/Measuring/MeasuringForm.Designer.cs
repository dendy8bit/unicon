﻿namespace BEMN.MR901NEW.Measuring
{
    partial class MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this.stopReading = new System.Windows.Forms.Button();
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._analogTabPage = new System.Windows.Forms.TabPage();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.voltageGroup = new System.Windows.Forms.GroupBox();
            this._u30 = new System.Windows.Forms.TextBox();
            this._uca = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this._u2 = new System.Windows.Forms.TextBox();
            this._ubc = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this._u1 = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this._uab = new System.Windows.Forms.TextBox();
            this.label138 = new System.Windows.Forms.Label();
            this._un = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this._uc = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this._ub = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this._ua = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.connectionCurrentsGB = new System.Windows.Forms.GroupBox();
            this._i24TextBox = new System.Windows.Forms.TextBox();
            this._i16TextBox = new System.Windows.Forms.TextBox();
            this._i24Label = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._i23TextBox = new System.Windows.Forms.TextBox();
            this._i15TextBox = new System.Windows.Forms.TextBox();
            this._i23Label = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this._i22TextBox = new System.Windows.Forms.TextBox();
            this._i14TextBox = new System.Windows.Forms.TextBox();
            this._i22Label = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this._i21TextBox = new System.Windows.Forms.TextBox();
            this._i13TextBox = new System.Windows.Forms.TextBox();
            this._i21Label = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this._i20TextBox = new System.Windows.Forms.TextBox();
            this._i12TextBox = new System.Windows.Forms.TextBox();
            this._i20Label = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._i19TextBox = new System.Windows.Forms.TextBox();
            this._i11TextBox = new System.Windows.Forms.TextBox();
            this._i19Label = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._i18TextBox = new System.Windows.Forms.TextBox();
            this._i10TextBox = new System.Windows.Forms.TextBox();
            this._i18Label = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._i17TextBox = new System.Windows.Forms.TextBox();
            this._i17Label = new System.Windows.Forms.Label();
            this._i9TextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._i8TextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._i7TextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._i6TextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this._i5TextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._i4TextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._i3TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._i2TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._i1TextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._it3TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._id3TextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._it2TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._id2TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._it1TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._id1TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._virtualReleGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.diod12 = new BEMN.Forms.Diod();
            this.diod11 = new BEMN.Forms.Diod();
            this.diod10 = new BEMN.Forms.Diod();
            this.diod9 = new BEMN.Forms.Diod();
            this.diod8 = new BEMN.Forms.Diod();
            this.diod7 = new BEMN.Forms.Diod();
            this.diod6 = new BEMN.Forms.Diod();
            this.diod5 = new BEMN.Forms.Diod();
            this.diod4 = new BEMN.Forms.Diod();
            this.diod3 = new BEMN.Forms.Diod();
            this.diod2 = new BEMN.Forms.Diod();
            this.diod1 = new BEMN.Forms.Diod();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this._rsTriggersGB = new System.Windows.Forms.GroupBox();
            this._rst8 = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._rst7 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this._rst1 = new BEMN.Forms.LedControl();
            this._rst6 = new BEMN.Forms.LedControl();
            this.label127 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this._rst5 = new BEMN.Forms.LedControl();
            this._rst2 = new BEMN.Forms.LedControl();
            this.label131 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this._rst4 = new BEMN.Forms.LedControl();
            this._rst16 = new BEMN.Forms.LedControl();
            this._rst3 = new BEMN.Forms.LedControl();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this._rst15 = new BEMN.Forms.LedControl();
            this._rst9 = new BEMN.Forms.LedControl();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this._rst14 = new BEMN.Forms.LedControl();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this._rst10 = new BEMN.Forms.LedControl();
            this._rst13 = new BEMN.Forms.LedControl();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this._rst11 = new BEMN.Forms.LedControl();
            this._rst12 = new BEMN.Forms.LedControl();
            this.label171 = new System.Windows.Forms.Label();
            this.relayGroupBox = new System.Windows.Forms.GroupBox();
            this.discretsGroupBox = new System.Windows.Forms.GroupBox();
            this._discretTabPage = new System.Windows.Forms.TabPage();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label223 = new System.Windows.Forms.Label();
            this._difDefenceActualIoPo = new BEMN.Forms.LedControl();
            this._difDefenceActualIoSh2 = new BEMN.Forms.LedControl();
            this._difDefenceActualIoSh1 = new BEMN.Forms.LedControl();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this._difDefenceActualChtoPo = new BEMN.Forms.LedControl();
            this._difDefenceActualChtoSh2 = new BEMN.Forms.LedControl();
            this._difDefenceActualChtoSh1 = new BEMN.Forms.LedControl();
            this._difDefenceActualSrabPo = new BEMN.Forms.LedControl();
            this._difDefenceActualSrabSh2 = new BEMN.Forms.LedControl();
            this._difDefenceActualSrabSh1 = new BEMN.Forms.LedControl();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label215 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this._difDefenceInstantaneousChtoPo = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousChtoSh2 = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousChtoSh1 = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousSrabPo = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousSrabSh2 = new BEMN.Forms.LedControl();
            this._difDefenceInstantaneousSrabSh1 = new BEMN.Forms.LedControl();
            this.label212 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._signalization = new BEMN.Forms.LedControl();
            this._reservedGroupOfSetpoints = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this._alarm = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._mainGroupOfSetpoints = new BEMN.Forms.LedControl();
            this.label209 = new System.Windows.Forms.Label();
            this._fault = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this.voltageDiscretsGroup = new System.Windows.Forms.GroupBox();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this._u2lIO = new BEMN.Forms.LedControl();
            this._u1lIO = new BEMN.Forms.LedControl();
            this._u2l = new BEMN.Forms.LedControl();
            this.label274 = new System.Windows.Forms.Label();
            this._u2bIO = new BEMN.Forms.LedControl();
            this._u1l = new BEMN.Forms.LedControl();
            this.label275 = new System.Windows.Forms.Label();
            this._u1bIO = new BEMN.Forms.LedControl();
            this._u2b = new BEMN.Forms.LedControl();
            this.label276 = new System.Windows.Forms.Label();
            this._u1b = new BEMN.Forms.LedControl();
            this.label277 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label191 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this._i32Io = new BEMN.Forms.LedControl();
            this.label221 = new System.Windows.Forms.Label();
            this._i31Io = new BEMN.Forms.LedControl();
            this.label222 = new System.Windows.Forms.Label();
            this._i30Io = new BEMN.Forms.LedControl();
            this.label229 = new System.Windows.Forms.Label();
            this._i29Io = new BEMN.Forms.LedControl();
            this.label230 = new System.Windows.Forms.Label();
            this._i28Io = new BEMN.Forms.LedControl();
            this.label270 = new System.Windows.Forms.Label();
            this._i27Io = new BEMN.Forms.LedControl();
            this.label269 = new System.Windows.Forms.Label();
            this._i26Io = new BEMN.Forms.LedControl();
            this._i32 = new BEMN.Forms.LedControl();
            this._i25Io = new BEMN.Forms.LedControl();
            this.label95 = new System.Windows.Forms.Label();
            this._i31 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._i30 = new BEMN.Forms.LedControl();
            this.label97 = new System.Windows.Forms.Label();
            this._i29 = new BEMN.Forms.LedControl();
            this.label98 = new System.Windows.Forms.Label();
            this._i28 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this._i27 = new BEMN.Forms.LedControl();
            this.label100 = new System.Windows.Forms.Label();
            this._i24Io = new BEMN.Forms.LedControl();
            this._i26 = new BEMN.Forms.LedControl();
            this.label101 = new System.Windows.Forms.Label();
            this._i23Io = new BEMN.Forms.LedControl();
            this._i25 = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._i22Io = new BEMN.Forms.LedControl();
            this._i24 = new BEMN.Forms.LedControl();
            this.label103 = new System.Windows.Forms.Label();
            this._i21Io = new BEMN.Forms.LedControl();
            this._i23 = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this._i20Io = new BEMN.Forms.LedControl();
            this._i22 = new BEMN.Forms.LedControl();
            this.label105 = new System.Windows.Forms.Label();
            this._i19Io = new BEMN.Forms.LedControl();
            this._i21 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._i18Io = new BEMN.Forms.LedControl();
            this._i20 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._i17Io = new BEMN.Forms.LedControl();
            this._i19 = new BEMN.Forms.LedControl();
            this.label108 = new System.Windows.Forms.Label();
            this._i18 = new BEMN.Forms.LedControl();
            this.label109 = new System.Windows.Forms.Label();
            this._i16Io = new BEMN.Forms.LedControl();
            this._i17 = new BEMN.Forms.LedControl();
            this.label110 = new System.Windows.Forms.Label();
            this._i15Io = new BEMN.Forms.LedControl();
            this._i16 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._i14Io = new BEMN.Forms.LedControl();
            this._i15 = new BEMN.Forms.LedControl();
            this.label80 = new System.Windows.Forms.Label();
            this._i13Io = new BEMN.Forms.LedControl();
            this._i14 = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._i12Io = new BEMN.Forms.LedControl();
            this._i13 = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._i11Io = new BEMN.Forms.LedControl();
            this._i12 = new BEMN.Forms.LedControl();
            this.label83 = new System.Windows.Forms.Label();
            this._i10Io = new BEMN.Forms.LedControl();
            this._i11 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._i9Io = new BEMN.Forms.LedControl();
            this._i10 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._i8Io = new BEMN.Forms.LedControl();
            this._i9 = new BEMN.Forms.LedControl();
            this.label86 = new System.Windows.Forms.Label();
            this._i7Io = new BEMN.Forms.LedControl();
            this._i8 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._i6Io = new BEMN.Forms.LedControl();
            this._i7 = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._i5Io = new BEMN.Forms.LedControl();
            this._i6 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._i4Io = new BEMN.Forms.LedControl();
            this._i5 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._i3Io = new BEMN.Forms.LedControl();
            this._i4 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._i2Io = new BEMN.Forms.LedControl();
            this._i3 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._i1Io = new BEMN.Forms.LedControl();
            this._i2 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._i1 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl48 = new BEMN.Forms.LedControl();
            this._ssl40 = new BEMN.Forms.LedControl();
            this._ssl32 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl47 = new BEMN.Forms.LedControl();
            this._ssl39 = new BEMN.Forms.LedControl();
            this._ssl31 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl46 = new BEMN.Forms.LedControl();
            this._ssl38 = new BEMN.Forms.LedControl();
            this._ssl30 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl45 = new BEMN.Forms.LedControl();
            this._ssl37 = new BEMN.Forms.LedControl();
            this._ssl29 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl44 = new BEMN.Forms.LedControl();
            this._ssl36 = new BEMN.Forms.LedControl();
            this._ssl28 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl43 = new BEMN.Forms.LedControl();
            this._ssl35 = new BEMN.Forms.LedControl();
            this._ssl27 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl42 = new BEMN.Forms.LedControl();
            this._ssl34 = new BEMN.Forms.LedControl();
            this._ssl26 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl41 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._ssl33 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz24 = new BEMN.Forms.LedControl();
            this._vz16 = new BEMN.Forms.LedControl();
            this._vz24label = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this._vz23 = new BEMN.Forms.LedControl();
            this._vz15 = new BEMN.Forms.LedControl();
            this._vz23label = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this._vz22 = new BEMN.Forms.LedControl();
            this._vz14 = new BEMN.Forms.LedControl();
            this._vz22label = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this._vz21 = new BEMN.Forms.LedControl();
            this._vz13 = new BEMN.Forms.LedControl();
            this._vz21label = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this._vz20 = new BEMN.Forms.LedControl();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label293 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this._vz19 = new BEMN.Forms.LedControl();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label292 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this._vz18 = new BEMN.Forms.LedControl();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label291 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this._vz17 = new BEMN.Forms.LedControl();
            this.label290 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._faultMeasuring = new BEMN.Forms.LedControl();
            this.label204 = new System.Windows.Forms.Label();
            this._faultUrov = new BEMN.Forms.LedControl();
            this.label207 = new System.Windows.Forms.Label();
            this._neisprTN = new BEMN.Forms.LedControl();
            this._neisprTNLabel = new System.Windows.Forms.Label();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule6 = new BEMN.Forms.LedControl();
            this.label41 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this.label194 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this.label195 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this.label196 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this._faultTt2 = new BEMN.Forms.LedControl();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.faultTt2Label = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this._faultTt1 = new BEMN.Forms.LedControl();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this.faultTt1Label = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this._faultTt3 = new BEMN.Forms.LedControl();
            this._faultLogic = new BEMN.Forms.LedControl();
            this.faultTt3Label = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this._neisprTNGroup = new System.Windows.Forms.GroupBox();
            this._neisprTNUabc = new BEMN.Forms.LedControl();
            this.label45 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this._neisprTNns = new BEMN.Forms.LedControl();
            this.label43 = new System.Windows.Forms.Label();
            this._neisprTN3U0 = new BEMN.Forms.LedControl();
            this._neisprTN3U0Label = new System.Windows.Forms.Label();
            this._neisprTNUn = new BEMN.Forms.LedControl();
            this.urovGroupBox = new System.Windows.Forms.GroupBox();
            this._urovPO = new BEMN.Forms.LedControl();
            this.label158 = new System.Windows.Forms.Label();
            this._urovSH2 = new BEMN.Forms.LedControl();
            this.label159 = new System.Windows.Forms.Label();
            this._urovSH1 = new BEMN.Forms.LedControl();
            this.label160 = new System.Windows.Forms.Label();
            this._urovPr24 = new BEMN.Forms.LedControl();
            this._urovPr16 = new BEMN.Forms.LedControl();
            this._urovPrLabel24 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this._urovPr23 = new BEMN.Forms.LedControl();
            this._urovPr15 = new BEMN.Forms.LedControl();
            this._urovPrLabel23 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this._urovPr22 = new BEMN.Forms.LedControl();
            this._urovPr14 = new BEMN.Forms.LedControl();
            this._urovPrLabel22 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this._urovPr21 = new BEMN.Forms.LedControl();
            this._urovPr13 = new BEMN.Forms.LedControl();
            this._urovPrLabel21 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this._urovPr20 = new BEMN.Forms.LedControl();
            this._urovPr12 = new BEMN.Forms.LedControl();
            this._urovPrLabel20 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this._urovPr19 = new BEMN.Forms.LedControl();
            this._urovPr11 = new BEMN.Forms.LedControl();
            this._urovPrLabel19 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this._urovPr18 = new BEMN.Forms.LedControl();
            this._urovPr10 = new BEMN.Forms.LedControl();
            this._urovPrLabel18 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this._urovPr17 = new BEMN.Forms.LedControl();
            this._urovPrLabel17 = new System.Windows.Forms.Label();
            this._urovPr9 = new BEMN.Forms.LedControl();
            this.label157 = new System.Windows.Forms.Label();
            this._urovPr8 = new BEMN.Forms.LedControl();
            this.label135 = new System.Windows.Forms.Label();
            this._urovPr7 = new BEMN.Forms.LedControl();
            this.label143 = new System.Windows.Forms.Label();
            this._urovPr6 = new BEMN.Forms.LedControl();
            this.label144 = new System.Windows.Forms.Label();
            this._urovPr5 = new BEMN.Forms.LedControl();
            this.label145 = new System.Windows.Forms.Label();
            this._urovPr4 = new BEMN.Forms.LedControl();
            this.label146 = new System.Windows.Forms.Label();
            this._urovPr3 = new BEMN.Forms.LedControl();
            this.label147 = new System.Windows.Forms.Label();
            this._urovPr2 = new BEMN.Forms.LedControl();
            this.label148 = new System.Windows.Forms.Label();
            this._urovPr1 = new BEMN.Forms.LedControl();
            this.label149 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._reserveGroupButton = new System.Windows.Forms.Button();
            this._mainGroupButton = new System.Windows.Forms.Button();
            this.label490 = new System.Windows.Forms.Label();
            this.label491 = new System.Windows.Forms.Label();
            this._reservedGroup = new BEMN.Forms.LedControl();
            this._mainGroup = new BEMN.Forms.LedControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._commandBtn = new System.Windows.Forms.Button();
            this._commandComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this.startOsc = new System.Windows.Forms.Button();
            this._resetTt = new System.Windows.Forms.Button();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this._dataBaseTabControl.SuspendLayout();
            this._analogTabPage.SuspendLayout();
            this.voltageGroup.SuspendLayout();
            this.connectionCurrentsGB.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._rsTriggersGB.SuspendLayout();
            this._discretTabPage.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.voltageDiscretsGroup.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this._neisprTNGroup.SuspendLayout();
            this.urovGroupBox.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.SuspendLayout();
            // 
            // stopReading
            // 
            this.stopReading.Location = new System.Drawing.Point(6, 45);
            this.stopReading.Name = "stopReading";
            this.stopReading.Size = new System.Drawing.Size(133, 23);
            this.stopReading.TabIndex = 7;
            this.stopReading.Text = "Остановить чтение";
            this.stopReading.UseVisualStyleBackColor = true;
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(this._analogTabPage);
            this._dataBaseTabControl.Controls.Add(this.tabPage1);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(795, 698);
            this._dataBaseTabControl.TabIndex = 0;
            // 
            // _analogTabPage
            // 
            this._analogTabPage.Controls.Add(this._dateTimeControl);
            this._analogTabPage.Controls.Add(this.voltageGroup);
            this._analogTabPage.Controls.Add(this.connectionCurrentsGB);
            this._analogTabPage.Controls.Add(this.groupBox1);
            this._analogTabPage.Location = new System.Drawing.Point(4, 22);
            this._analogTabPage.Name = "_analogTabPage";
            this._analogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogTabPage.Size = new System.Drawing.Size(787, 672);
            this._analogTabPage.TabIndex = 0;
            this._analogTabPage.Text = "Аналоговая БД";
            this._analogTabPage.UseVisualStyleBackColor = true;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(367, 6);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 43;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // voltageGroup
            // 
            this.voltageGroup.Controls.Add(this._u30);
            this.voltageGroup.Controls.Add(this._uca);
            this.voltageGroup.Controls.Add(this.label132);
            this.voltageGroup.Controls.Add(this.label133);
            this.voltageGroup.Controls.Add(this._u2);
            this.voltageGroup.Controls.Add(this._ubc);
            this.voltageGroup.Controls.Add(this.label134);
            this.voltageGroup.Controls.Add(this.label136);
            this.voltageGroup.Controls.Add(this._u1);
            this.voltageGroup.Controls.Add(this.label137);
            this.voltageGroup.Controls.Add(this._uab);
            this.voltageGroup.Controls.Add(this.label138);
            this.voltageGroup.Controls.Add(this._un);
            this.voltageGroup.Controls.Add(this.label161);
            this.voltageGroup.Controls.Add(this._uc);
            this.voltageGroup.Controls.Add(this.label162);
            this.voltageGroup.Controls.Add(this._ub);
            this.voltageGroup.Controls.Add(this.label163);
            this.voltageGroup.Controls.Add(this._ua);
            this.voltageGroup.Controls.Add(this.label164);
            this.voltageGroup.Location = new System.Drawing.Point(6, 380);
            this.voltageGroup.Name = "voltageGroup";
            this.voltageGroup.Size = new System.Drawing.Size(345, 138);
            this.voltageGroup.TabIndex = 1;
            this.voltageGroup.TabStop = false;
            this.voltageGroup.Text = "Напряжения";
            this.voltageGroup.Visible = false;
            // 
            // _u30
            // 
            this._u30.Location = new System.Drawing.Point(270, 81);
            this._u30.Name = "_u30";
            this._u30.ReadOnly = true;
            this._u30.Size = new System.Drawing.Size(61, 20);
            this._u30.TabIndex = 23;
            // 
            // _uca
            // 
            this._uca.Location = new System.Drawing.Point(149, 81);
            this._uca.Name = "_uca";
            this._uca.ReadOnly = true;
            this._uca.Size = new System.Drawing.Size(61, 20);
            this._uca.TabIndex = 23;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(242, 84);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(27, 13);
            this.label132.TabIndex = 22;
            this.label132.Text = "3U0";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(121, 84);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(27, 13);
            this.label133.TabIndex = 22;
            this.label133.Text = "Uca";
            // 
            // _u2
            // 
            this._u2.Location = new System.Drawing.Point(270, 55);
            this._u2.Name = "_u2";
            this._u2.ReadOnly = true;
            this._u2.Size = new System.Drawing.Size(61, 20);
            this._u2.TabIndex = 21;
            // 
            // _ubc
            // 
            this._ubc.Location = new System.Drawing.Point(149, 55);
            this._ubc.Name = "_ubc";
            this._ubc.ReadOnly = true;
            this._ubc.Size = new System.Drawing.Size(61, 20);
            this._ubc.TabIndex = 21;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(242, 58);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(21, 13);
            this.label134.TabIndex = 20;
            this.label134.Text = "U2";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(121, 58);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(27, 13);
            this.label136.TabIndex = 20;
            this.label136.Text = "Ubc";
            // 
            // _u1
            // 
            this._u1.Location = new System.Drawing.Point(270, 29);
            this._u1.Name = "_u1";
            this._u1.ReadOnly = true;
            this._u1.Size = new System.Drawing.Size(61, 20);
            this._u1.TabIndex = 19;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(242, 32);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(21, 13);
            this.label137.TabIndex = 18;
            this.label137.Text = "U1";
            // 
            // _uab
            // 
            this._uab.Location = new System.Drawing.Point(149, 29);
            this._uab.Name = "_uab";
            this._uab.ReadOnly = true;
            this._uab.Size = new System.Drawing.Size(61, 20);
            this._uab.TabIndex = 19;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(121, 32);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(27, 13);
            this.label138.TabIndex = 18;
            this.label138.Text = "Uab";
            // 
            // _un
            // 
            this._un.Location = new System.Drawing.Point(34, 107);
            this._un.Name = "_un";
            this._un.ReadOnly = true;
            this._un.Size = new System.Drawing.Size(61, 20);
            this._un.TabIndex = 9;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(6, 110);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(21, 13);
            this.label161.TabIndex = 8;
            this.label161.Text = "Un";
            // 
            // _uc
            // 
            this._uc.Location = new System.Drawing.Point(34, 81);
            this._uc.Name = "_uc";
            this._uc.ReadOnly = true;
            this._uc.Size = new System.Drawing.Size(61, 20);
            this._uc.TabIndex = 7;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(6, 84);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(21, 13);
            this.label162.TabIndex = 6;
            this.label162.Text = "Uc";
            // 
            // _ub
            // 
            this._ub.Location = new System.Drawing.Point(34, 55);
            this._ub.Name = "_ub";
            this._ub.ReadOnly = true;
            this._ub.Size = new System.Drawing.Size(61, 20);
            this._ub.TabIndex = 5;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(6, 58);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(21, 13);
            this.label163.TabIndex = 4;
            this.label163.Text = "Ub";
            // 
            // _ua
            // 
            this._ua.Location = new System.Drawing.Point(34, 29);
            this._ua.Name = "_ua";
            this._ua.ReadOnly = true;
            this._ua.Size = new System.Drawing.Size(61, 20);
            this._ua.TabIndex = 3;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(6, 32);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(21, 13);
            this.label164.TabIndex = 2;
            this.label164.Text = "Ua";
            // 
            // connectionCurrentsGB
            // 
            this.connectionCurrentsGB.Controls.Add(this._i24TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i16TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i24Label);
            this.connectionCurrentsGB.Controls.Add(this.label22);
            this.connectionCurrentsGB.Controls.Add(this._i23TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i15TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i23Label);
            this.connectionCurrentsGB.Controls.Add(this.label21);
            this.connectionCurrentsGB.Controls.Add(this._i22TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i14TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i22Label);
            this.connectionCurrentsGB.Controls.Add(this.label20);
            this.connectionCurrentsGB.Controls.Add(this._i21TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i13TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i21Label);
            this.connectionCurrentsGB.Controls.Add(this.label19);
            this.connectionCurrentsGB.Controls.Add(this._i20TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i12TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i20Label);
            this.connectionCurrentsGB.Controls.Add(this.label18);
            this.connectionCurrentsGB.Controls.Add(this._i19TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i11TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i19Label);
            this.connectionCurrentsGB.Controls.Add(this.label17);
            this.connectionCurrentsGB.Controls.Add(this._i18TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i10TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i18Label);
            this.connectionCurrentsGB.Controls.Add(this.label16);
            this.connectionCurrentsGB.Controls.Add(this._i17TextBox);
            this.connectionCurrentsGB.Controls.Add(this._i17Label);
            this.connectionCurrentsGB.Controls.Add(this._i9TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label15);
            this.connectionCurrentsGB.Controls.Add(this._i8TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label14);
            this.connectionCurrentsGB.Controls.Add(this._i7TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label13);
            this.connectionCurrentsGB.Controls.Add(this._i6TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label12);
            this.connectionCurrentsGB.Controls.Add(this._i5TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label11);
            this.connectionCurrentsGB.Controls.Add(this._i4TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label10);
            this.connectionCurrentsGB.Controls.Add(this._i3TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label9);
            this.connectionCurrentsGB.Controls.Add(this._i2TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label8);
            this.connectionCurrentsGB.Controls.Add(this._i1TextBox);
            this.connectionCurrentsGB.Controls.Add(this.label7);
            this.connectionCurrentsGB.Location = new System.Drawing.Point(6, 135);
            this.connectionCurrentsGB.Name = "connectionCurrentsGB";
            this.connectionCurrentsGB.Size = new System.Drawing.Size(345, 239);
            this.connectionCurrentsGB.TabIndex = 1;
            this.connectionCurrentsGB.TabStop = false;
            this.connectionCurrentsGB.Text = "Токи присоединений";
            // 
            // _i24TextBox
            // 
            this._i24TextBox.Location = new System.Drawing.Point(270, 211);
            this._i24TextBox.Name = "_i24TextBox";
            this._i24TextBox.ReadOnly = true;
            this._i24TextBox.Size = new System.Drawing.Size(61, 20);
            this._i24TextBox.TabIndex = 33;
            // 
            // _i16TextBox
            // 
            this._i16TextBox.Location = new System.Drawing.Point(149, 211);
            this._i16TextBox.Name = "_i16TextBox";
            this._i16TextBox.ReadOnly = true;
            this._i16TextBox.Size = new System.Drawing.Size(61, 20);
            this._i16TextBox.TabIndex = 33;
            // 
            // _i24Label
            // 
            this._i24Label.AutoSize = true;
            this._i24Label.Location = new System.Drawing.Point(242, 214);
            this._i24Label.Name = "_i24Label";
            this._i24Label.Size = new System.Drawing.Size(22, 13);
            this._i24Label.TabIndex = 32;
            this._i24Label.Text = "I24";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(121, 214);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(22, 13);
            this.label22.TabIndex = 32;
            this.label22.Text = "I16";
            // 
            // _i23TextBox
            // 
            this._i23TextBox.Location = new System.Drawing.Point(270, 185);
            this._i23TextBox.Name = "_i23TextBox";
            this._i23TextBox.ReadOnly = true;
            this._i23TextBox.Size = new System.Drawing.Size(61, 20);
            this._i23TextBox.TabIndex = 31;
            // 
            // _i15TextBox
            // 
            this._i15TextBox.Location = new System.Drawing.Point(149, 185);
            this._i15TextBox.Name = "_i15TextBox";
            this._i15TextBox.ReadOnly = true;
            this._i15TextBox.Size = new System.Drawing.Size(61, 20);
            this._i15TextBox.TabIndex = 31;
            // 
            // _i23Label
            // 
            this._i23Label.AutoSize = true;
            this._i23Label.Location = new System.Drawing.Point(242, 188);
            this._i23Label.Name = "_i23Label";
            this._i23Label.Size = new System.Drawing.Size(22, 13);
            this._i23Label.TabIndex = 30;
            this._i23Label.Text = "I23";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(121, 188);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(22, 13);
            this.label21.TabIndex = 30;
            this.label21.Text = "I15";
            // 
            // _i22TextBox
            // 
            this._i22TextBox.Location = new System.Drawing.Point(270, 159);
            this._i22TextBox.Name = "_i22TextBox";
            this._i22TextBox.ReadOnly = true;
            this._i22TextBox.Size = new System.Drawing.Size(61, 20);
            this._i22TextBox.TabIndex = 29;
            // 
            // _i14TextBox
            // 
            this._i14TextBox.Location = new System.Drawing.Point(149, 159);
            this._i14TextBox.Name = "_i14TextBox";
            this._i14TextBox.ReadOnly = true;
            this._i14TextBox.Size = new System.Drawing.Size(61, 20);
            this._i14TextBox.TabIndex = 29;
            // 
            // _i22Label
            // 
            this._i22Label.AutoSize = true;
            this._i22Label.Location = new System.Drawing.Point(242, 162);
            this._i22Label.Name = "_i22Label";
            this._i22Label.Size = new System.Drawing.Size(22, 13);
            this._i22Label.TabIndex = 28;
            this._i22Label.Text = "I22";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(121, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "I14";
            // 
            // _i21TextBox
            // 
            this._i21TextBox.Location = new System.Drawing.Point(270, 133);
            this._i21TextBox.Name = "_i21TextBox";
            this._i21TextBox.ReadOnly = true;
            this._i21TextBox.Size = new System.Drawing.Size(61, 20);
            this._i21TextBox.TabIndex = 27;
            // 
            // _i13TextBox
            // 
            this._i13TextBox.Location = new System.Drawing.Point(149, 133);
            this._i13TextBox.Name = "_i13TextBox";
            this._i13TextBox.ReadOnly = true;
            this._i13TextBox.Size = new System.Drawing.Size(61, 20);
            this._i13TextBox.TabIndex = 27;
            // 
            // _i21Label
            // 
            this._i21Label.AutoSize = true;
            this._i21Label.Location = new System.Drawing.Point(242, 136);
            this._i21Label.Name = "_i21Label";
            this._i21Label.Size = new System.Drawing.Size(22, 13);
            this._i21Label.TabIndex = 26;
            this._i21Label.Text = "I21";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(121, 136);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "I13";
            // 
            // _i20TextBox
            // 
            this._i20TextBox.Location = new System.Drawing.Point(270, 107);
            this._i20TextBox.Name = "_i20TextBox";
            this._i20TextBox.ReadOnly = true;
            this._i20TextBox.Size = new System.Drawing.Size(61, 20);
            this._i20TextBox.TabIndex = 25;
            // 
            // _i12TextBox
            // 
            this._i12TextBox.Location = new System.Drawing.Point(149, 107);
            this._i12TextBox.Name = "_i12TextBox";
            this._i12TextBox.ReadOnly = true;
            this._i12TextBox.Size = new System.Drawing.Size(61, 20);
            this._i12TextBox.TabIndex = 25;
            // 
            // _i20Label
            // 
            this._i20Label.AutoSize = true;
            this._i20Label.Location = new System.Drawing.Point(242, 110);
            this._i20Label.Name = "_i20Label";
            this._i20Label.Size = new System.Drawing.Size(22, 13);
            this._i20Label.TabIndex = 24;
            this._i20Label.Text = "I20";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(121, 110);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "I12";
            // 
            // _i19TextBox
            // 
            this._i19TextBox.Location = new System.Drawing.Point(270, 81);
            this._i19TextBox.Name = "_i19TextBox";
            this._i19TextBox.ReadOnly = true;
            this._i19TextBox.Size = new System.Drawing.Size(61, 20);
            this._i19TextBox.TabIndex = 23;
            // 
            // _i11TextBox
            // 
            this._i11TextBox.Location = new System.Drawing.Point(149, 81);
            this._i11TextBox.Name = "_i11TextBox";
            this._i11TextBox.ReadOnly = true;
            this._i11TextBox.Size = new System.Drawing.Size(61, 20);
            this._i11TextBox.TabIndex = 23;
            // 
            // _i19Label
            // 
            this._i19Label.AutoSize = true;
            this._i19Label.Location = new System.Drawing.Point(242, 84);
            this._i19Label.Name = "_i19Label";
            this._i19Label.Size = new System.Drawing.Size(22, 13);
            this._i19Label.TabIndex = 22;
            this._i19Label.Text = "I19";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(121, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "I11";
            // 
            // _i18TextBox
            // 
            this._i18TextBox.Location = new System.Drawing.Point(270, 55);
            this._i18TextBox.Name = "_i18TextBox";
            this._i18TextBox.ReadOnly = true;
            this._i18TextBox.Size = new System.Drawing.Size(61, 20);
            this._i18TextBox.TabIndex = 21;
            // 
            // _i10TextBox
            // 
            this._i10TextBox.Location = new System.Drawing.Point(149, 55);
            this._i10TextBox.Name = "_i10TextBox";
            this._i10TextBox.ReadOnly = true;
            this._i10TextBox.Size = new System.Drawing.Size(61, 20);
            this._i10TextBox.TabIndex = 21;
            // 
            // _i18Label
            // 
            this._i18Label.AutoSize = true;
            this._i18Label.Location = new System.Drawing.Point(242, 58);
            this._i18Label.Name = "_i18Label";
            this._i18Label.Size = new System.Drawing.Size(22, 13);
            this._i18Label.TabIndex = 20;
            this._i18Label.Text = "I18";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(121, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "I10";
            // 
            // _i17TextBox
            // 
            this._i17TextBox.Location = new System.Drawing.Point(270, 29);
            this._i17TextBox.Name = "_i17TextBox";
            this._i17TextBox.ReadOnly = true;
            this._i17TextBox.Size = new System.Drawing.Size(61, 20);
            this._i17TextBox.TabIndex = 19;
            // 
            // _i17Label
            // 
            this._i17Label.AutoSize = true;
            this._i17Label.Location = new System.Drawing.Point(242, 32);
            this._i17Label.Name = "_i17Label";
            this._i17Label.Size = new System.Drawing.Size(22, 13);
            this._i17Label.TabIndex = 18;
            this._i17Label.Text = "I17";
            // 
            // _i9TextBox
            // 
            this._i9TextBox.Location = new System.Drawing.Point(149, 29);
            this._i9TextBox.Name = "_i9TextBox";
            this._i9TextBox.ReadOnly = true;
            this._i9TextBox.Size = new System.Drawing.Size(61, 20);
            this._i9TextBox.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(121, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "I9";
            // 
            // _i8TextBox
            // 
            this._i8TextBox.Location = new System.Drawing.Point(34, 211);
            this._i8TextBox.Name = "_i8TextBox";
            this._i8TextBox.ReadOnly = true;
            this._i8TextBox.Size = new System.Drawing.Size(61, 20);
            this._i8TextBox.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 214);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "I8";
            // 
            // _i7TextBox
            // 
            this._i7TextBox.Location = new System.Drawing.Point(34, 185);
            this._i7TextBox.Name = "_i7TextBox";
            this._i7TextBox.ReadOnly = true;
            this._i7TextBox.Size = new System.Drawing.Size(61, 20);
            this._i7TextBox.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "I7";
            // 
            // _i6TextBox
            // 
            this._i6TextBox.Location = new System.Drawing.Point(34, 159);
            this._i6TextBox.Name = "_i6TextBox";
            this._i6TextBox.ReadOnly = true;
            this._i6TextBox.Size = new System.Drawing.Size(61, 20);
            this._i6TextBox.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "I6";
            // 
            // _i5TextBox
            // 
            this._i5TextBox.Location = new System.Drawing.Point(34, 133);
            this._i5TextBox.Name = "_i5TextBox";
            this._i5TextBox.ReadOnly = true;
            this._i5TextBox.Size = new System.Drawing.Size(61, 20);
            this._i5TextBox.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 136);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "I5";
            // 
            // _i4TextBox
            // 
            this._i4TextBox.Location = new System.Drawing.Point(34, 107);
            this._i4TextBox.Name = "_i4TextBox";
            this._i4TextBox.ReadOnly = true;
            this._i4TextBox.Size = new System.Drawing.Size(61, 20);
            this._i4TextBox.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "I4";
            // 
            // _i3TextBox
            // 
            this._i3TextBox.Location = new System.Drawing.Point(34, 81);
            this._i3TextBox.Name = "_i3TextBox";
            this._i3TextBox.ReadOnly = true;
            this._i3TextBox.Size = new System.Drawing.Size(61, 20);
            this._i3TextBox.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "I3";
            // 
            // _i2TextBox
            // 
            this._i2TextBox.Location = new System.Drawing.Point(34, 55);
            this._i2TextBox.Name = "_i2TextBox";
            this._i2TextBox.ReadOnly = true;
            this._i2TextBox.Size = new System.Drawing.Size(61, 20);
            this._i2TextBox.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "I2";
            // 
            // _i1TextBox
            // 
            this._i1TextBox.Location = new System.Drawing.Point(34, 29);
            this._i1TextBox.Name = "_i1TextBox";
            this._i1TextBox.ReadOnly = true;
            this._i1TextBox.Size = new System.Drawing.Size(61, 20);
            this._i1TextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "I1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(6, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 119);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дифференциальные и тормозные токи";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._it3TextBox);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._id3TextBox);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(236, 27);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(109, 84);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ПО";
            // 
            // _it3TextBox
            // 
            this._it3TextBox.Location = new System.Drawing.Point(34, 49);
            this._it3TextBox.Name = "_it3TextBox";
            this._it3TextBox.ReadOnly = true;
            this._it3TextBox.Size = new System.Drawing.Size(61, 20);
            this._it3TextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Iт3";
            // 
            // _id3TextBox
            // 
            this._id3TextBox.Location = new System.Drawing.Point(34, 23);
            this._id3TextBox.Name = "_id3TextBox";
            this._id3TextBox.ReadOnly = true;
            this._id3TextBox.Size = new System.Drawing.Size(61, 20);
            this._id3TextBox.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Iд3";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._it2TextBox);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this._id2TextBox);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(121, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(109, 84);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "СШ2";
            // 
            // _it2TextBox
            // 
            this._it2TextBox.Location = new System.Drawing.Point(34, 49);
            this._it2TextBox.Name = "_it2TextBox";
            this._it2TextBox.ReadOnly = true;
            this._it2TextBox.Size = new System.Drawing.Size(61, 20);
            this._it2TextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Iт2";
            // 
            // _id2TextBox
            // 
            this._id2TextBox.Location = new System.Drawing.Point(34, 23);
            this._id2TextBox.Name = "_id2TextBox";
            this._id2TextBox.ReadOnly = true;
            this._id2TextBox.Size = new System.Drawing.Size(61, 20);
            this._id2TextBox.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Iд2";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._it1TextBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._id1TextBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(6, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(109, 84);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "СШ1";
            // 
            // _it1TextBox
            // 
            this._it1TextBox.Location = new System.Drawing.Point(34, 49);
            this._it1TextBox.Name = "_it1TextBox";
            this._it1TextBox.ReadOnly = true;
            this._it1TextBox.Size = new System.Drawing.Size(61, 20);
            this._it1TextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Iт1";
            // 
            // _id1TextBox
            // 
            this._id1TextBox.Location = new System.Drawing.Point(34, 23);
            this._id1TextBox.Name = "_id1TextBox";
            this._id1TextBox.ReadOnly = true;
            this._id1TextBox.Size = new System.Drawing.Size(61, 20);
            this._id1TextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Iд1";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._virtualReleGroupBox);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this._rsTriggersGB);
            this.tabPage1.Controls.Add(this.relayGroupBox);
            this.tabPage1.Controls.Add(this.discretsGroupBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(787, 672);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Дискретные входы, индикаторы и реле";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _virtualReleGroupBox
            // 
            this._virtualReleGroupBox.Location = new System.Drawing.Point(288, 3);
            this._virtualReleGroupBox.Name = "_virtualReleGroupBox";
            this._virtualReleGroupBox.Size = new System.Drawing.Size(223, 285);
            this._virtualReleGroupBox.TabIndex = 49;
            this._virtualReleGroupBox.TabStop = false;
            this._virtualReleGroupBox.Text = "Виртуальные реле";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.diod12);
            this.groupBox7.Controls.Add(this.diod11);
            this.groupBox7.Controls.Add(this.diod10);
            this.groupBox7.Controls.Add(this.diod9);
            this.groupBox7.Controls.Add(this.diod8);
            this.groupBox7.Controls.Add(this.diod7);
            this.groupBox7.Controls.Add(this.diod6);
            this.groupBox7.Controls.Add(this.diod5);
            this.groupBox7.Controls.Add(this.diod4);
            this.groupBox7.Controls.Add(this.diod3);
            this.groupBox7.Controls.Add(this.diod2);
            this.groupBox7.Controls.Add(this.diod1);
            this.groupBox7.Controls.Add(this.label172);
            this.groupBox7.Controls.Add(this.label173);
            this.groupBox7.Controls.Add(this.label174);
            this.groupBox7.Controls.Add(this.label175);
            this.groupBox7.Controls.Add(this.label176);
            this.groupBox7.Controls.Add(this.label177);
            this.groupBox7.Controls.Add(this.label178);
            this.groupBox7.Controls.Add(this.label224);
            this.groupBox7.Controls.Add(this.label228);
            this.groupBox7.Controls.Add(this.label232);
            this.groupBox7.Controls.Add(this.label233);
            this.groupBox7.Controls.Add(this.label234);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(51, 259);
            this.groupBox7.TabIndex = 58;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Инд.";
            // 
            // diod12
            // 
            this.diod12.BackColor = System.Drawing.Color.Transparent;
            this.diod12.Location = new System.Drawing.Point(6, 235);
            this.diod12.Name = "diod12";
            this.diod12.Size = new System.Drawing.Size(14, 14);
            this.diod12.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod12.TabIndex = 30;
            // 
            // diod11
            // 
            this.diod11.BackColor = System.Drawing.Color.Transparent;
            this.diod11.Location = new System.Drawing.Point(6, 215);
            this.diod11.Name = "diod11";
            this.diod11.Size = new System.Drawing.Size(14, 14);
            this.diod11.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod11.TabIndex = 31;
            // 
            // diod10
            // 
            this.diod10.BackColor = System.Drawing.Color.Transparent;
            this.diod10.Location = new System.Drawing.Point(6, 195);
            this.diod10.Name = "diod10";
            this.diod10.Size = new System.Drawing.Size(14, 14);
            this.diod10.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod10.TabIndex = 32;
            // 
            // diod9
            // 
            this.diod9.BackColor = System.Drawing.Color.Transparent;
            this.diod9.Location = new System.Drawing.Point(6, 175);
            this.diod9.Name = "diod9";
            this.diod9.Size = new System.Drawing.Size(14, 14);
            this.diod9.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod9.TabIndex = 33;
            // 
            // diod8
            // 
            this.diod8.BackColor = System.Drawing.Color.Transparent;
            this.diod8.Location = new System.Drawing.Point(6, 155);
            this.diod8.Name = "diod8";
            this.diod8.Size = new System.Drawing.Size(14, 14);
            this.diod8.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod8.TabIndex = 34;
            // 
            // diod7
            // 
            this.diod7.BackColor = System.Drawing.Color.Transparent;
            this.diod7.Location = new System.Drawing.Point(6, 135);
            this.diod7.Name = "diod7";
            this.diod7.Size = new System.Drawing.Size(14, 14);
            this.diod7.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod7.TabIndex = 35;
            // 
            // diod6
            // 
            this.diod6.BackColor = System.Drawing.Color.Transparent;
            this.diod6.Location = new System.Drawing.Point(6, 115);
            this.diod6.Name = "diod6";
            this.diod6.Size = new System.Drawing.Size(14, 14);
            this.diod6.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod6.TabIndex = 36;
            // 
            // diod5
            // 
            this.diod5.BackColor = System.Drawing.Color.Transparent;
            this.diod5.Location = new System.Drawing.Point(6, 95);
            this.diod5.Name = "diod5";
            this.diod5.Size = new System.Drawing.Size(14, 14);
            this.diod5.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod5.TabIndex = 37;
            // 
            // diod4
            // 
            this.diod4.BackColor = System.Drawing.Color.Transparent;
            this.diod4.Location = new System.Drawing.Point(6, 75);
            this.diod4.Name = "diod4";
            this.diod4.Size = new System.Drawing.Size(14, 14);
            this.diod4.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod4.TabIndex = 38;
            // 
            // diod3
            // 
            this.diod3.BackColor = System.Drawing.Color.Transparent;
            this.diod3.Location = new System.Drawing.Point(6, 55);
            this.diod3.Name = "diod3";
            this.diod3.Size = new System.Drawing.Size(14, 14);
            this.diod3.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod3.TabIndex = 39;
            // 
            // diod2
            // 
            this.diod2.BackColor = System.Drawing.Color.Transparent;
            this.diod2.Location = new System.Drawing.Point(6, 35);
            this.diod2.Name = "diod2";
            this.diod2.Size = new System.Drawing.Size(14, 14);
            this.diod2.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod2.TabIndex = 40;
            // 
            // diod1
            // 
            this.diod1.BackColor = System.Drawing.Color.Transparent;
            this.diod1.Location = new System.Drawing.Point(6, 15);
            this.diod1.Name = "diod1";
            this.diod1.Size = new System.Drawing.Size(14, 14);
            this.diod1.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod1.TabIndex = 41;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(25, 216);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(19, 13);
            this.label172.TabIndex = 28;
            this.label172.Text = "11";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(25, 116);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(13, 13);
            this.label173.TabIndex = 26;
            this.label173.Text = "6";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(25, 196);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(19, 13);
            this.label174.TabIndex = 24;
            this.label174.Text = "10";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(25, 176);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(13, 13);
            this.label175.TabIndex = 22;
            this.label175.Text = "9";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(25, 156);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(13, 13);
            this.label176.TabIndex = 20;
            this.label176.Text = "8";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(25, 136);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(13, 13);
            this.label177.TabIndex = 18;
            this.label177.Text = "7";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(25, 236);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 16;
            this.label178.Text = "12";
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(25, 96);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(13, 13);
            this.label224.TabIndex = 8;
            this.label224.Text = "5";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(25, 76);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(13, 13);
            this.label228.TabIndex = 6;
            this.label228.Text = "4";
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(25, 56);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(13, 13);
            this.label232.TabIndex = 4;
            this.label232.Text = "3";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(25, 36);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(13, 13);
            this.label233.TabIndex = 2;
            this.label233.Text = "2";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(25, 16);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(13, 13);
            this.label234.TabIndex = 0;
            this.label234.Text = "1";
            // 
            // _rsTriggersGB
            // 
            this._rsTriggersGB.Controls.Add(this._rst8);
            this._rsTriggersGB.Controls.Add(this.label44);
            this._rsTriggersGB.Controls.Add(this._rst7);
            this._rsTriggersGB.Controls.Add(this.label46);
            this._rsTriggersGB.Controls.Add(this._rst1);
            this._rsTriggersGB.Controls.Add(this._rst6);
            this._rsTriggersGB.Controls.Add(this.label127);
            this._rsTriggersGB.Controls.Add(this.label130);
            this._rsTriggersGB.Controls.Add(this.label140);
            this._rsTriggersGB.Controls.Add(this._rst5);
            this._rsTriggersGB.Controls.Add(this._rst2);
            this._rsTriggersGB.Controls.Add(this.label131);
            this._rsTriggersGB.Controls.Add(this.label139);
            this._rsTriggersGB.Controls.Add(this._rst4);
            this._rsTriggersGB.Controls.Add(this._rst16);
            this._rsTriggersGB.Controls.Add(this._rst3);
            this._rsTriggersGB.Controls.Add(this.label141);
            this._rsTriggersGB.Controls.Add(this.label142);
            this._rsTriggersGB.Controls.Add(this._rst15);
            this._rsTriggersGB.Controls.Add(this._rst9);
            this._rsTriggersGB.Controls.Add(this.label165);
            this._rsTriggersGB.Controls.Add(this.label166);
            this._rsTriggersGB.Controls.Add(this._rst14);
            this._rsTriggersGB.Controls.Add(this.label167);
            this._rsTriggersGB.Controls.Add(this.label168);
            this._rsTriggersGB.Controls.Add(this._rst10);
            this._rsTriggersGB.Controls.Add(this._rst13);
            this._rsTriggersGB.Controls.Add(this.label169);
            this._rsTriggersGB.Controls.Add(this.label170);
            this._rsTriggersGB.Controls.Add(this._rst11);
            this._rsTriggersGB.Controls.Add(this._rst12);
            this._rsTriggersGB.Controls.Add(this.label171);
            this._rsTriggersGB.Location = new System.Drawing.Point(517, 3);
            this._rsTriggersGB.Name = "_rsTriggersGB";
            this._rsTriggersGB.Size = new System.Drawing.Size(131, 187);
            this._rsTriggersGB.TabIndex = 57;
            this._rsTriggersGB.TabStop = false;
            this._rsTriggersGB.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rst8
            // 
            this._rst8.Location = new System.Drawing.Point(6, 164);
            this._rst8.Name = "_rst8";
            this._rst8.Size = new System.Drawing.Size(13, 13);
            this._rst8.State = BEMN.Forms.LedState.Off;
            this._rst8.TabIndex = 15;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(21, 164);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(35, 13);
            this.label44.TabIndex = 14;
            this.label44.Text = "RST8";
            // 
            // _rst7
            // 
            this._rst7.Location = new System.Drawing.Point(6, 145);
            this._rst7.Name = "_rst7";
            this._rst7.Size = new System.Drawing.Size(13, 13);
            this._rst7.State = BEMN.Forms.LedState.Off;
            this._rst7.TabIndex = 13;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(21, 145);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(35, 13);
            this.label46.TabIndex = 12;
            this.label46.Text = "RST7";
            // 
            // _rst1
            // 
            this._rst1.Location = new System.Drawing.Point(6, 31);
            this._rst1.Name = "_rst1";
            this._rst1.Size = new System.Drawing.Size(13, 13);
            this._rst1.State = BEMN.Forms.LedState.Off;
            this._rst1.TabIndex = 1;
            // 
            // _rst6
            // 
            this._rst6.Location = new System.Drawing.Point(6, 126);
            this._rst6.Name = "_rst6";
            this._rst6.Size = new System.Drawing.Size(13, 13);
            this._rst6.State = BEMN.Forms.LedState.Off;
            this._rst6.TabIndex = 11;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(21, 31);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(35, 13);
            this.label127.TabIndex = 0;
            this.label127.Text = "RST1";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(21, 126);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(35, 13);
            this.label130.TabIndex = 10;
            this.label130.Text = "RST6";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(21, 50);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(35, 13);
            this.label140.TabIndex = 2;
            this.label140.Text = "RST2";
            // 
            // _rst5
            // 
            this._rst5.Location = new System.Drawing.Point(6, 107);
            this._rst5.Name = "_rst5";
            this._rst5.Size = new System.Drawing.Size(13, 13);
            this._rst5.State = BEMN.Forms.LedState.Off;
            this._rst5.TabIndex = 9;
            // 
            // _rst2
            // 
            this._rst2.Location = new System.Drawing.Point(6, 50);
            this._rst2.Name = "_rst2";
            this._rst2.Size = new System.Drawing.Size(13, 13);
            this._rst2.State = BEMN.Forms.LedState.Off;
            this._rst2.TabIndex = 3;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(21, 108);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(35, 13);
            this.label131.TabIndex = 8;
            this.label131.Text = "RST5";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(21, 69);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(35, 13);
            this.label139.TabIndex = 4;
            this.label139.Text = "RST3";
            // 
            // _rst4
            // 
            this._rst4.Location = new System.Drawing.Point(6, 88);
            this._rst4.Name = "_rst4";
            this._rst4.Size = new System.Drawing.Size(13, 13);
            this._rst4.State = BEMN.Forms.LedState.Off;
            this._rst4.TabIndex = 7;
            // 
            // _rst16
            // 
            this._rst16.Location = new System.Drawing.Point(62, 164);
            this._rst16.Name = "_rst16";
            this._rst16.Size = new System.Drawing.Size(13, 13);
            this._rst16.State = BEMN.Forms.LedState.Off;
            this._rst16.TabIndex = 15;
            // 
            // _rst3
            // 
            this._rst3.Location = new System.Drawing.Point(6, 69);
            this._rst3.Name = "_rst3";
            this._rst3.Size = new System.Drawing.Size(13, 13);
            this._rst3.State = BEMN.Forms.LedState.Off;
            this._rst3.TabIndex = 5;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(78, 164);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(41, 13);
            this.label141.TabIndex = 14;
            this.label141.Text = "RST16";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(21, 87);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(35, 13);
            this.label142.TabIndex = 6;
            this.label142.Text = "RST4";
            // 
            // _rst15
            // 
            this._rst15.Location = new System.Drawing.Point(62, 145);
            this._rst15.Name = "_rst15";
            this._rst15.Size = new System.Drawing.Size(13, 13);
            this._rst15.State = BEMN.Forms.LedState.Off;
            this._rst15.TabIndex = 13;
            // 
            // _rst9
            // 
            this._rst9.Location = new System.Drawing.Point(62, 31);
            this._rst9.Name = "_rst9";
            this._rst9.Size = new System.Drawing.Size(13, 13);
            this._rst9.State = BEMN.Forms.LedState.Off;
            this._rst9.TabIndex = 1;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(78, 145);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(41, 13);
            this.label165.TabIndex = 12;
            this.label165.Text = "RST15";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(78, 31);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(35, 13);
            this.label166.TabIndex = 0;
            this.label166.Text = "RST9";
            // 
            // _rst14
            // 
            this._rst14.Location = new System.Drawing.Point(62, 126);
            this._rst14.Name = "_rst14";
            this._rst14.Size = new System.Drawing.Size(13, 13);
            this._rst14.State = BEMN.Forms.LedState.Off;
            this._rst14.TabIndex = 11;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(78, 50);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(41, 13);
            this.label167.TabIndex = 2;
            this.label167.Text = "RST10";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(78, 126);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(41, 13);
            this.label168.TabIndex = 10;
            this.label168.Text = "RST14";
            // 
            // _rst10
            // 
            this._rst10.Location = new System.Drawing.Point(62, 50);
            this._rst10.Name = "_rst10";
            this._rst10.Size = new System.Drawing.Size(13, 13);
            this._rst10.State = BEMN.Forms.LedState.Off;
            this._rst10.TabIndex = 3;
            // 
            // _rst13
            // 
            this._rst13.Location = new System.Drawing.Point(62, 107);
            this._rst13.Name = "_rst13";
            this._rst13.Size = new System.Drawing.Size(13, 13);
            this._rst13.State = BEMN.Forms.LedState.Off;
            this._rst13.TabIndex = 9;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(78, 69);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(41, 13);
            this.label169.TabIndex = 4;
            this.label169.Text = "RST11";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(78, 107);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(41, 13);
            this.label170.TabIndex = 8;
            this.label170.Text = "RST13";
            // 
            // _rst11
            // 
            this._rst11.Location = new System.Drawing.Point(62, 69);
            this._rst11.Name = "_rst11";
            this._rst11.Size = new System.Drawing.Size(13, 13);
            this._rst11.State = BEMN.Forms.LedState.Off;
            this._rst11.TabIndex = 5;
            // 
            // _rst12
            // 
            this._rst12.Location = new System.Drawing.Point(62, 88);
            this._rst12.Name = "_rst12";
            this._rst12.Size = new System.Drawing.Size(13, 13);
            this._rst12.State = BEMN.Forms.LedState.Off;
            this._rst12.TabIndex = 7;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(78, 88);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(41, 13);
            this.label171.TabIndex = 6;
            this.label171.Text = "RST12";
            // 
            // relayGroupBox
            // 
            this.relayGroupBox.Location = new System.Drawing.Point(57, 3);
            this.relayGroupBox.Name = "relayGroupBox";
            this.relayGroupBox.Size = new System.Drawing.Size(225, 285);
            this.relayGroupBox.TabIndex = 48;
            this.relayGroupBox.TabStop = false;
            this.relayGroupBox.Text = "Реле";
            // 
            // discretsGroupBox
            // 
            this.discretsGroupBox.Location = new System.Drawing.Point(57, 294);
            this.discretsGroupBox.Name = "discretsGroupBox";
            this.discretsGroupBox.Size = new System.Drawing.Size(225, 285);
            this.discretsGroupBox.TabIndex = 47;
            this.discretsGroupBox.TabStop = false;
            this.discretsGroupBox.Text = "Дискретные входы";
            // 
            // _discretTabPage
            // 
            this._discretTabPage.Controls.Add(this.groupBox22);
            this._discretTabPage.Controls.Add(this.groupBox20);
            this._discretTabPage.Controls.Add(this.voltageDiscretsGroup);
            this._discretTabPage.Controls.Add(this.groupBox11);
            this._discretTabPage.Controls.Add(this.groupBox21);
            this._discretTabPage.Controls.Add(this.groupBox12);
            this._discretTabPage.Controls.Add(this.groupBox19);
            this._discretTabPage.Controls.Add(this._neisprTNGroup);
            this._discretTabPage.Controls.Add(this.urovGroupBox);
            this._discretTabPage.Controls.Add(this.groupBox10);
            this._discretTabPage.Controls.Add(this.groupBox9);
            this._discretTabPage.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage.Name = "_discretTabPage";
            this._discretTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage.Size = new System.Drawing.Size(787, 672);
            this._discretTabPage.TabIndex = 1;
            this._discretTabPage.Text = "Дискретная БД";
            this._discretTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox24);
            this.groupBox22.Controls.Add(this.groupBox23);
            this.groupBox22.Location = new System.Drawing.Point(6, 203);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(147, 226);
            this.groupBox22.TabIndex = 16;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Диф. защиты";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.label223);
            this.groupBox24.Controls.Add(this._difDefenceActualIoPo);
            this.groupBox24.Controls.Add(this._difDefenceActualIoSh2);
            this.groupBox24.Controls.Add(this._difDefenceActualIoSh1);
            this.groupBox24.Controls.Add(this.label216);
            this.groupBox24.Controls.Add(this.label217);
            this.groupBox24.Controls.Add(this.label218);
            this.groupBox24.Controls.Add(this._difDefenceActualChtoPo);
            this.groupBox24.Controls.Add(this._difDefenceActualChtoSh2);
            this.groupBox24.Controls.Add(this._difDefenceActualChtoSh1);
            this.groupBox24.Controls.Add(this._difDefenceActualSrabPo);
            this.groupBox24.Controls.Add(this._difDefenceActualSrabSh2);
            this.groupBox24.Controls.Add(this._difDefenceActualSrabSh1);
            this.groupBox24.Controls.Add(this.label219);
            this.groupBox24.Controls.Add(this.label220);
            this.groupBox24.Location = new System.Drawing.Point(6, 123);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(136, 98);
            this.groupBox24.TabIndex = 1;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "По действ. значениям";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label223.Location = new System.Drawing.Point(3, 15);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(23, 13);
            this.label223.TabIndex = 74;
            this.label223.Text = "ИО";
            // 
            // _difDefenceActualIoPo
            // 
            this._difDefenceActualIoPo.Location = new System.Drawing.Point(6, 72);
            this._difDefenceActualIoPo.Name = "_difDefenceActualIoPo";
            this._difDefenceActualIoPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualIoPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualIoPo.TabIndex = 73;
            // 
            // _difDefenceActualIoSh2
            // 
            this._difDefenceActualIoSh2.Location = new System.Drawing.Point(6, 53);
            this._difDefenceActualIoSh2.Name = "_difDefenceActualIoSh2";
            this._difDefenceActualIoSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualIoSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualIoSh2.TabIndex = 72;
            // 
            // _difDefenceActualIoSh1
            // 
            this._difDefenceActualIoSh1.Location = new System.Drawing.Point(6, 34);
            this._difDefenceActualIoSh1.Name = "_difDefenceActualIoSh1";
            this._difDefenceActualIoSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualIoSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualIoSh1.TabIndex = 71;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(84, 72);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(41, 13);
            this.label216.TabIndex = 62;
            this.label216.Text = "Iд3 ПО";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(84, 53);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(47, 13);
            this.label217.TabIndex = 61;
            this.label217.Text = "Iд2 СШ2";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(84, 34);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(47, 13);
            this.label218.TabIndex = 60;
            this.label218.Text = "Iд1 СШ1";
            // 
            // _difDefenceActualChtoPo
            // 
            this._difDefenceActualChtoPo.Location = new System.Drawing.Point(34, 72);
            this._difDefenceActualChtoPo.Name = "_difDefenceActualChtoPo";
            this._difDefenceActualChtoPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualChtoPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualChtoPo.TabIndex = 58;
            // 
            // _difDefenceActualChtoSh2
            // 
            this._difDefenceActualChtoSh2.Location = new System.Drawing.Point(34, 53);
            this._difDefenceActualChtoSh2.Name = "_difDefenceActualChtoSh2";
            this._difDefenceActualChtoSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualChtoSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualChtoSh2.TabIndex = 56;
            // 
            // _difDefenceActualChtoSh1
            // 
            this._difDefenceActualChtoSh1.Location = new System.Drawing.Point(34, 34);
            this._difDefenceActualChtoSh1.Name = "_difDefenceActualChtoSh1";
            this._difDefenceActualChtoSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualChtoSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualChtoSh1.TabIndex = 54;
            // 
            // _difDefenceActualSrabPo
            // 
            this._difDefenceActualSrabPo.Location = new System.Drawing.Point(65, 72);
            this._difDefenceActualSrabPo.Name = "_difDefenceActualSrabPo";
            this._difDefenceActualSrabPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualSrabPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualSrabPo.TabIndex = 59;
            // 
            // _difDefenceActualSrabSh2
            // 
            this._difDefenceActualSrabSh2.Location = new System.Drawing.Point(65, 53);
            this._difDefenceActualSrabSh2.Name = "_difDefenceActualSrabSh2";
            this._difDefenceActualSrabSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualSrabSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualSrabSh2.TabIndex = 57;
            // 
            // _difDefenceActualSrabSh1
            // 
            this._difDefenceActualSrabSh1.Location = new System.Drawing.Point(65, 34);
            this._difDefenceActualSrabSh1.Name = "_difDefenceActualSrabSh1";
            this._difDefenceActualSrabSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceActualSrabSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceActualSrabSh1.TabIndex = 55;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(61, 15);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(32, 13);
            this.label219.TabIndex = 1;
            this.label219.Text = "Сраб";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(28, 15);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(30, 13);
            this.label220.TabIndex = 0;
            this.label220.Text = "ЧТО";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.label215);
            this.groupBox23.Controls.Add(this.label214);
            this.groupBox23.Controls.Add(this.label213);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousChtoPo);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousChtoSh2);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousChtoSh1);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousSrabPo);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousSrabSh2);
            this.groupBox23.Controls.Add(this._difDefenceInstantaneousSrabSh1);
            this.groupBox23.Controls.Add(this.label212);
            this.groupBox23.Controls.Add(this.label211);
            this.groupBox23.Location = new System.Drawing.Point(6, 19);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(136, 98);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "По мгн. значениям";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(65, 73);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(49, 13);
            this.label215.TabIndex = 62;
            this.label215.Text = "Iд3м ПО";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(65, 54);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(55, 13);
            this.label214.TabIndex = 61;
            this.label214.Text = "Iд2м СШ2";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(65, 35);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(55, 13);
            this.label213.TabIndex = 60;
            this.label213.Text = "Iд1м СШ1";
            // 
            // _difDefenceInstantaneousChtoPo
            // 
            this._difDefenceInstantaneousChtoPo.Location = new System.Drawing.Point(14, 73);
            this._difDefenceInstantaneousChtoPo.Name = "_difDefenceInstantaneousChtoPo";
            this._difDefenceInstantaneousChtoPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousChtoPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousChtoPo.TabIndex = 58;
            // 
            // _difDefenceInstantaneousChtoSh2
            // 
            this._difDefenceInstantaneousChtoSh2.Location = new System.Drawing.Point(14, 54);
            this._difDefenceInstantaneousChtoSh2.Name = "_difDefenceInstantaneousChtoSh2";
            this._difDefenceInstantaneousChtoSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousChtoSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousChtoSh2.TabIndex = 56;
            // 
            // _difDefenceInstantaneousChtoSh1
            // 
            this._difDefenceInstantaneousChtoSh1.Location = new System.Drawing.Point(14, 35);
            this._difDefenceInstantaneousChtoSh1.Name = "_difDefenceInstantaneousChtoSh1";
            this._difDefenceInstantaneousChtoSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousChtoSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousChtoSh1.TabIndex = 54;
            // 
            // _difDefenceInstantaneousSrabPo
            // 
            this._difDefenceInstantaneousSrabPo.Location = new System.Drawing.Point(46, 73);
            this._difDefenceInstantaneousSrabPo.Name = "_difDefenceInstantaneousSrabPo";
            this._difDefenceInstantaneousSrabPo.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousSrabPo.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousSrabPo.TabIndex = 59;
            // 
            // _difDefenceInstantaneousSrabSh2
            // 
            this._difDefenceInstantaneousSrabSh2.Location = new System.Drawing.Point(46, 54);
            this._difDefenceInstantaneousSrabSh2.Name = "_difDefenceInstantaneousSrabSh2";
            this._difDefenceInstantaneousSrabSh2.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousSrabSh2.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousSrabSh2.TabIndex = 57;
            // 
            // _difDefenceInstantaneousSrabSh1
            // 
            this._difDefenceInstantaneousSrabSh1.Location = new System.Drawing.Point(46, 35);
            this._difDefenceInstantaneousSrabSh1.Name = "_difDefenceInstantaneousSrabSh1";
            this._difDefenceInstantaneousSrabSh1.Size = new System.Drawing.Size(13, 13);
            this._difDefenceInstantaneousSrabSh1.State = BEMN.Forms.LedState.Off;
            this._difDefenceInstantaneousSrabSh1.TabIndex = 55;
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(38, 16);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(35, 13);
            this.label212.TabIndex = 1;
            this.label212.Text = "Сраб.";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(6, 16);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(30, 13);
            this.label211.TabIndex = 0;
            this.label211.Text = "ЧТО";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._signalization);
            this.groupBox20.Controls.Add(this._reservedGroupOfSetpoints);
            this.groupBox20.Controls.Add(this.label24);
            this.groupBox20.Controls.Add(this.label208);
            this.groupBox20.Controls.Add(this._alarm);
            this.groupBox20.Controls.Add(this.label23);
            this.groupBox20.Controls.Add(this._mainGroupOfSetpoints);
            this.groupBox20.Controls.Add(this.label209);
            this.groupBox20.Controls.Add(this._fault);
            this.groupBox20.Controls.Add(this.label210);
            this.groupBox20.Location = new System.Drawing.Point(6, 435);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(128, 119);
            this.groupBox20.TabIndex = 15;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Состояние";
            // 
            // _signalization
            // 
            this._signalization.Location = new System.Drawing.Point(6, 97);
            this._signalization.Name = "_signalization";
            this._signalization.Size = new System.Drawing.Size(13, 13);
            this._signalization.State = BEMN.Forms.LedState.Off;
            this._signalization.TabIndex = 11;
            // 
            // _reservedGroupOfSetpoints
            // 
            this._reservedGroupOfSetpoints.Location = new System.Drawing.Point(6, 59);
            this._reservedGroupOfSetpoints.Name = "_reservedGroupOfSetpoints";
            this._reservedGroupOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._reservedGroupOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._reservedGroupOfSetpoints.TabIndex = 11;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 97);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Сигнализация";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(25, 59);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(68, 13);
            this.label208.TabIndex = 10;
            this.label208.Text = "Рез. гр. уст.";
            // 
            // _alarm
            // 
            this._alarm.Location = new System.Drawing.Point(6, 78);
            this._alarm.Name = "_alarm";
            this._alarm.Size = new System.Drawing.Size(13, 13);
            this._alarm.State = BEMN.Forms.LedState.Off;
            this._alarm.TabIndex = 9;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 78);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "Авария";
            // 
            // _mainGroupOfSetpoints
            // 
            this._mainGroupOfSetpoints.Location = new System.Drawing.Point(6, 40);
            this._mainGroupOfSetpoints.Name = "_mainGroupOfSetpoints";
            this._mainGroupOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._mainGroupOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._mainGroupOfSetpoints.TabIndex = 9;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(25, 40);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(69, 13);
            this.label209.TabIndex = 8;
            this.label209.Text = "Осн. гр. уст.";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 21);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 7;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 21);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 6;
            this.label210.Text = "Неисправность";
            // 
            // voltageDiscretsGroup
            // 
            this.voltageDiscretsGroup.Controls.Add(this.label128);
            this.voltageDiscretsGroup.Controls.Add(this.label129);
            this.voltageDiscretsGroup.Controls.Add(this._u2lIO);
            this.voltageDiscretsGroup.Controls.Add(this._u1lIO);
            this.voltageDiscretsGroup.Controls.Add(this._u2l);
            this.voltageDiscretsGroup.Controls.Add(this.label274);
            this.voltageDiscretsGroup.Controls.Add(this._u2bIO);
            this.voltageDiscretsGroup.Controls.Add(this._u1l);
            this.voltageDiscretsGroup.Controls.Add(this.label275);
            this.voltageDiscretsGroup.Controls.Add(this._u1bIO);
            this.voltageDiscretsGroup.Controls.Add(this._u2b);
            this.voltageDiscretsGroup.Controls.Add(this.label276);
            this.voltageDiscretsGroup.Controls.Add(this._u1b);
            this.voltageDiscretsGroup.Controls.Add(this.label277);
            this.voltageDiscretsGroup.Location = new System.Drawing.Point(678, 6);
            this.voltageDiscretsGroup.Name = "voltageDiscretsGroup";
            this.voltageDiscretsGroup.Size = new System.Drawing.Size(84, 111);
            this.voltageDiscretsGroup.TabIndex = 4;
            this.voltageDiscretsGroup.TabStop = false;
            this.voltageDiscretsGroup.Text = "Защиты U";
            this.voltageDiscretsGroup.Visible = false;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.ForeColor = System.Drawing.Color.Blue;
            this.label128.Location = new System.Drawing.Point(25, 16);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(32, 13);
            this.label128.TabIndex = 65;
            this.label128.Text = "Сраб";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.ForeColor = System.Drawing.Color.Red;
            this.label129.Location = new System.Drawing.Point(6, 16);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(23, 13);
            this.label129.TabIndex = 64;
            this.label129.Text = "ИО";
            // 
            // _u2lIO
            // 
            this._u2lIO.Location = new System.Drawing.Point(9, 92);
            this._u2lIO.Name = "_u2lIO";
            this._u2lIO.Size = new System.Drawing.Size(13, 13);
            this._u2lIO.State = BEMN.Forms.LedState.Off;
            this._u2lIO.TabIndex = 7;
            // 
            // _u1lIO
            // 
            this._u1lIO.Location = new System.Drawing.Point(9, 73);
            this._u1lIO.Name = "_u1lIO";
            this._u1lIO.Size = new System.Drawing.Size(13, 13);
            this._u1lIO.State = BEMN.Forms.LedState.Off;
            this._u1lIO.TabIndex = 5;
            // 
            // _u2l
            // 
            this._u2l.Location = new System.Drawing.Point(28, 92);
            this._u2l.Name = "_u2l";
            this._u2l.Size = new System.Drawing.Size(13, 13);
            this._u2l.State = BEMN.Forms.LedState.Off;
            this._u2l.TabIndex = 7;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Location = new System.Drawing.Point(48, 92);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(27, 13);
            this.label274.TabIndex = 6;
            this.label274.Text = "U<2";
            // 
            // _u2bIO
            // 
            this._u2bIO.Location = new System.Drawing.Point(9, 54);
            this._u2bIO.Name = "_u2bIO";
            this._u2bIO.Size = new System.Drawing.Size(13, 13);
            this._u2bIO.State = BEMN.Forms.LedState.Off;
            this._u2bIO.TabIndex = 3;
            // 
            // _u1l
            // 
            this._u1l.Location = new System.Drawing.Point(28, 73);
            this._u1l.Name = "_u1l";
            this._u1l.Size = new System.Drawing.Size(13, 13);
            this._u1l.State = BEMN.Forms.LedState.Off;
            this._u1l.TabIndex = 5;
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Location = new System.Drawing.Point(47, 73);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(27, 13);
            this.label275.TabIndex = 4;
            this.label275.Text = "U<1";
            // 
            // _u1bIO
            // 
            this._u1bIO.Location = new System.Drawing.Point(9, 35);
            this._u1bIO.Name = "_u1bIO";
            this._u1bIO.Size = new System.Drawing.Size(13, 13);
            this._u1bIO.State = BEMN.Forms.LedState.Off;
            this._u1bIO.TabIndex = 1;
            // 
            // _u2b
            // 
            this._u2b.Location = new System.Drawing.Point(28, 54);
            this._u2b.Name = "_u2b";
            this._u2b.Size = new System.Drawing.Size(13, 13);
            this._u2b.State = BEMN.Forms.LedState.Off;
            this._u2b.TabIndex = 3;
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(47, 54);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(27, 13);
            this.label276.TabIndex = 2;
            this.label276.Text = "U>2";
            // 
            // _u1b
            // 
            this._u1b.Location = new System.Drawing.Point(28, 35);
            this._u1b.Name = "_u1b";
            this._u1b.Size = new System.Drawing.Size(13, 13);
            this._u1b.State = BEMN.Forms.LedState.Off;
            this._u1b.TabIndex = 1;
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Location = new System.Drawing.Point(47, 35);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(27, 13);
            this.label277.TabIndex = 0;
            this.label277.Text = "U>1";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label191);
            this.groupBox11.Controls.Add(this.label199);
            this.groupBox11.Controls.Add(this._i32Io);
            this.groupBox11.Controls.Add(this.label221);
            this.groupBox11.Controls.Add(this._i31Io);
            this.groupBox11.Controls.Add(this.label222);
            this.groupBox11.Controls.Add(this._i30Io);
            this.groupBox11.Controls.Add(this.label229);
            this.groupBox11.Controls.Add(this._i29Io);
            this.groupBox11.Controls.Add(this.label230);
            this.groupBox11.Controls.Add(this._i28Io);
            this.groupBox11.Controls.Add(this.label270);
            this.groupBox11.Controls.Add(this._i27Io);
            this.groupBox11.Controls.Add(this.label269);
            this.groupBox11.Controls.Add(this._i26Io);
            this.groupBox11.Controls.Add(this._i32);
            this.groupBox11.Controls.Add(this._i25Io);
            this.groupBox11.Controls.Add(this.label95);
            this.groupBox11.Controls.Add(this._i31);
            this.groupBox11.Controls.Add(this.label96);
            this.groupBox11.Controls.Add(this._i30);
            this.groupBox11.Controls.Add(this.label97);
            this.groupBox11.Controls.Add(this._i29);
            this.groupBox11.Controls.Add(this.label98);
            this.groupBox11.Controls.Add(this._i28);
            this.groupBox11.Controls.Add(this.label99);
            this.groupBox11.Controls.Add(this._i27);
            this.groupBox11.Controls.Add(this.label100);
            this.groupBox11.Controls.Add(this._i24Io);
            this.groupBox11.Controls.Add(this._i26);
            this.groupBox11.Controls.Add(this.label101);
            this.groupBox11.Controls.Add(this._i23Io);
            this.groupBox11.Controls.Add(this._i25);
            this.groupBox11.Controls.Add(this.label102);
            this.groupBox11.Controls.Add(this._i22Io);
            this.groupBox11.Controls.Add(this._i24);
            this.groupBox11.Controls.Add(this.label103);
            this.groupBox11.Controls.Add(this._i21Io);
            this.groupBox11.Controls.Add(this._i23);
            this.groupBox11.Controls.Add(this.label104);
            this.groupBox11.Controls.Add(this._i20Io);
            this.groupBox11.Controls.Add(this._i22);
            this.groupBox11.Controls.Add(this.label105);
            this.groupBox11.Controls.Add(this._i19Io);
            this.groupBox11.Controls.Add(this._i21);
            this.groupBox11.Controls.Add(this.label106);
            this.groupBox11.Controls.Add(this._i18Io);
            this.groupBox11.Controls.Add(this._i20);
            this.groupBox11.Controls.Add(this.label107);
            this.groupBox11.Controls.Add(this._i17Io);
            this.groupBox11.Controls.Add(this._i19);
            this.groupBox11.Controls.Add(this.label108);
            this.groupBox11.Controls.Add(this._i18);
            this.groupBox11.Controls.Add(this.label109);
            this.groupBox11.Controls.Add(this._i16Io);
            this.groupBox11.Controls.Add(this._i17);
            this.groupBox11.Controls.Add(this.label110);
            this.groupBox11.Controls.Add(this._i15Io);
            this.groupBox11.Controls.Add(this._i16);
            this.groupBox11.Controls.Add(this.label79);
            this.groupBox11.Controls.Add(this._i14Io);
            this.groupBox11.Controls.Add(this._i15);
            this.groupBox11.Controls.Add(this.label80);
            this.groupBox11.Controls.Add(this._i13Io);
            this.groupBox11.Controls.Add(this._i14);
            this.groupBox11.Controls.Add(this.label81);
            this.groupBox11.Controls.Add(this._i12Io);
            this.groupBox11.Controls.Add(this._i13);
            this.groupBox11.Controls.Add(this.label82);
            this.groupBox11.Controls.Add(this._i11Io);
            this.groupBox11.Controls.Add(this._i12);
            this.groupBox11.Controls.Add(this.label83);
            this.groupBox11.Controls.Add(this._i10Io);
            this.groupBox11.Controls.Add(this._i11);
            this.groupBox11.Controls.Add(this.label84);
            this.groupBox11.Controls.Add(this._i9Io);
            this.groupBox11.Controls.Add(this._i10);
            this.groupBox11.Controls.Add(this.label85);
            this.groupBox11.Controls.Add(this._i8Io);
            this.groupBox11.Controls.Add(this._i9);
            this.groupBox11.Controls.Add(this.label86);
            this.groupBox11.Controls.Add(this._i7Io);
            this.groupBox11.Controls.Add(this._i8);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._i6Io);
            this.groupBox11.Controls.Add(this._i7);
            this.groupBox11.Controls.Add(this.label88);
            this.groupBox11.Controls.Add(this._i5Io);
            this.groupBox11.Controls.Add(this._i6);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this._i4Io);
            this.groupBox11.Controls.Add(this._i5);
            this.groupBox11.Controls.Add(this.label90);
            this.groupBox11.Controls.Add(this._i3Io);
            this.groupBox11.Controls.Add(this._i4);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this._i2Io);
            this.groupBox11.Controls.Add(this._i3);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this._i1Io);
            this.groupBox11.Controls.Add(this._i2);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this._i1);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Location = new System.Drawing.Point(159, 203);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(312, 193);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Защиты I>";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.ForeColor = System.Drawing.Color.Blue;
            this.label191.Location = new System.Drawing.Point(248, 16);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(32, 13);
            this.label191.TabIndex = 71;
            this.label191.Text = "Сраб";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.ForeColor = System.Drawing.Color.Red;
            this.label199.Location = new System.Drawing.Point(229, 16);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(23, 13);
            this.label199.TabIndex = 70;
            this.label199.Text = "ИО";
            // 
            // _i32Io
            // 
            this._i32Io.Location = new System.Drawing.Point(232, 168);
            this._i32Io.Name = "_i32Io";
            this._i32Io.Size = new System.Drawing.Size(13, 13);
            this._i32Io.State = BEMN.Forms.LedState.Off;
            this._i32Io.TabIndex = 63;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.ForeColor = System.Drawing.Color.Blue;
            this.label221.Location = new System.Drawing.Point(172, 16);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(32, 13);
            this.label221.TabIndex = 69;
            this.label221.Text = "Сраб";
            // 
            // _i31Io
            // 
            this._i31Io.Location = new System.Drawing.Point(232, 149);
            this._i31Io.Name = "_i31Io";
            this._i31Io.Size = new System.Drawing.Size(13, 13);
            this._i31Io.State = BEMN.Forms.LedState.Off;
            this._i31Io.TabIndex = 61;
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.ForeColor = System.Drawing.Color.Red;
            this.label222.Location = new System.Drawing.Point(153, 16);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(23, 13);
            this.label222.TabIndex = 68;
            this.label222.Text = "ИО";
            // 
            // _i30Io
            // 
            this._i30Io.Location = new System.Drawing.Point(232, 130);
            this._i30Io.Name = "_i30Io";
            this._i30Io.Size = new System.Drawing.Size(13, 13);
            this._i30Io.State = BEMN.Forms.LedState.Off;
            this._i30Io.TabIndex = 59;
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.ForeColor = System.Drawing.Color.Blue;
            this.label229.Location = new System.Drawing.Point(95, 16);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(32, 13);
            this.label229.TabIndex = 67;
            this.label229.Text = "Сраб";
            // 
            // _i29Io
            // 
            this._i29Io.Location = new System.Drawing.Point(232, 111);
            this._i29Io.Name = "_i29Io";
            this._i29Io.Size = new System.Drawing.Size(13, 13);
            this._i29Io.State = BEMN.Forms.LedState.Off;
            this._i29Io.TabIndex = 57;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.ForeColor = System.Drawing.Color.Red;
            this.label230.Location = new System.Drawing.Point(76, 16);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(23, 13);
            this.label230.TabIndex = 66;
            this.label230.Text = "ИО";
            // 
            // _i28Io
            // 
            this._i28Io.Location = new System.Drawing.Point(232, 92);
            this._i28Io.Name = "_i28Io";
            this._i28Io.Size = new System.Drawing.Size(13, 13);
            this._i28Io.State = BEMN.Forms.LedState.Off;
            this._i28Io.TabIndex = 55;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.ForeColor = System.Drawing.Color.Blue;
            this.label270.Location = new System.Drawing.Point(25, 16);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(32, 13);
            this.label270.TabIndex = 65;
            this.label270.Text = "Сраб";
            // 
            // _i27Io
            // 
            this._i27Io.Location = new System.Drawing.Point(232, 73);
            this._i27Io.Name = "_i27Io";
            this._i27Io.Size = new System.Drawing.Size(13, 13);
            this._i27Io.State = BEMN.Forms.LedState.Off;
            this._i27Io.TabIndex = 53;
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.ForeColor = System.Drawing.Color.Red;
            this.label269.Location = new System.Drawing.Point(6, 16);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(23, 13);
            this.label269.TabIndex = 64;
            this.label269.Text = "ИО";
            // 
            // _i26Io
            // 
            this._i26Io.Location = new System.Drawing.Point(232, 54);
            this._i26Io.Name = "_i26Io";
            this._i26Io.Size = new System.Drawing.Size(13, 13);
            this._i26Io.State = BEMN.Forms.LedState.Off;
            this._i26Io.TabIndex = 51;
            // 
            // _i32
            // 
            this._i32.Location = new System.Drawing.Point(251, 168);
            this._i32.Name = "_i32";
            this._i32.Size = new System.Drawing.Size(13, 13);
            this._i32.State = BEMN.Forms.LedState.Off;
            this._i32.TabIndex = 63;
            // 
            // _i25Io
            // 
            this._i25Io.Location = new System.Drawing.Point(232, 35);
            this._i25Io.Name = "_i25Io";
            this._i25Io.Size = new System.Drawing.Size(13, 13);
            this._i25Io.State = BEMN.Forms.LedState.Off;
            this._i25Io.TabIndex = 49;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(270, 168);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(28, 13);
            this.label95.TabIndex = 62;
            this.label95.Text = "I>32";
            // 
            // _i31
            // 
            this._i31.Location = new System.Drawing.Point(251, 149);
            this._i31.Name = "_i31";
            this._i31.Size = new System.Drawing.Size(13, 13);
            this._i31.State = BEMN.Forms.LedState.Off;
            this._i31.TabIndex = 61;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(270, 149);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(28, 13);
            this.label96.TabIndex = 60;
            this.label96.Text = "I>31";
            // 
            // _i30
            // 
            this._i30.Location = new System.Drawing.Point(251, 130);
            this._i30.Name = "_i30";
            this._i30.Size = new System.Drawing.Size(13, 13);
            this._i30.State = BEMN.Forms.LedState.Off;
            this._i30.TabIndex = 59;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(270, 130);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(28, 13);
            this.label97.TabIndex = 58;
            this.label97.Text = "I>30";
            // 
            // _i29
            // 
            this._i29.Location = new System.Drawing.Point(251, 111);
            this._i29.Name = "_i29";
            this._i29.Size = new System.Drawing.Size(13, 13);
            this._i29.State = BEMN.Forms.LedState.Off;
            this._i29.TabIndex = 57;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(270, 111);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(28, 13);
            this.label98.TabIndex = 56;
            this.label98.Text = "I>29";
            // 
            // _i28
            // 
            this._i28.Location = new System.Drawing.Point(251, 92);
            this._i28.Name = "_i28";
            this._i28.Size = new System.Drawing.Size(13, 13);
            this._i28.State = BEMN.Forms.LedState.Off;
            this._i28.TabIndex = 55;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(270, 92);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(28, 13);
            this.label99.TabIndex = 54;
            this.label99.Text = "I>28";
            // 
            // _i27
            // 
            this._i27.Location = new System.Drawing.Point(251, 73);
            this._i27.Name = "_i27";
            this._i27.Size = new System.Drawing.Size(13, 13);
            this._i27.State = BEMN.Forms.LedState.Off;
            this._i27.TabIndex = 53;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(270, 73);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(28, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "I>27";
            // 
            // _i24Io
            // 
            this._i24Io.Location = new System.Drawing.Point(156, 168);
            this._i24Io.Name = "_i24Io";
            this._i24Io.Size = new System.Drawing.Size(13, 13);
            this._i24Io.State = BEMN.Forms.LedState.Off;
            this._i24Io.TabIndex = 47;
            // 
            // _i26
            // 
            this._i26.Location = new System.Drawing.Point(251, 54);
            this._i26.Name = "_i26";
            this._i26.Size = new System.Drawing.Size(13, 13);
            this._i26.State = BEMN.Forms.LedState.Off;
            this._i26.TabIndex = 51;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(270, 54);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(28, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "I>26";
            // 
            // _i23Io
            // 
            this._i23Io.Location = new System.Drawing.Point(156, 149);
            this._i23Io.Name = "_i23Io";
            this._i23Io.Size = new System.Drawing.Size(13, 13);
            this._i23Io.State = BEMN.Forms.LedState.Off;
            this._i23Io.TabIndex = 45;
            // 
            // _i25
            // 
            this._i25.Location = new System.Drawing.Point(251, 35);
            this._i25.Name = "_i25";
            this._i25.Size = new System.Drawing.Size(13, 13);
            this._i25.State = BEMN.Forms.LedState.Off;
            this._i25.TabIndex = 49;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(270, 35);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(28, 13);
            this.label102.TabIndex = 48;
            this.label102.Text = "I>25";
            // 
            // _i22Io
            // 
            this._i22Io.Location = new System.Drawing.Point(156, 130);
            this._i22Io.Name = "_i22Io";
            this._i22Io.Size = new System.Drawing.Size(13, 13);
            this._i22Io.State = BEMN.Forms.LedState.Off;
            this._i22Io.TabIndex = 43;
            // 
            // _i24
            // 
            this._i24.Location = new System.Drawing.Point(175, 168);
            this._i24.Name = "_i24";
            this._i24.Size = new System.Drawing.Size(13, 13);
            this._i24.State = BEMN.Forms.LedState.Off;
            this._i24.TabIndex = 47;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(194, 168);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(28, 13);
            this.label103.TabIndex = 46;
            this.label103.Text = "I>24";
            // 
            // _i21Io
            // 
            this._i21Io.Location = new System.Drawing.Point(156, 111);
            this._i21Io.Name = "_i21Io";
            this._i21Io.Size = new System.Drawing.Size(13, 13);
            this._i21Io.State = BEMN.Forms.LedState.Off;
            this._i21Io.TabIndex = 41;
            // 
            // _i23
            // 
            this._i23.Location = new System.Drawing.Point(175, 149);
            this._i23.Name = "_i23";
            this._i23.Size = new System.Drawing.Size(13, 13);
            this._i23.State = BEMN.Forms.LedState.Off;
            this._i23.TabIndex = 45;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(194, 149);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(28, 13);
            this.label104.TabIndex = 44;
            this.label104.Text = "I>23";
            // 
            // _i20Io
            // 
            this._i20Io.Location = new System.Drawing.Point(156, 92);
            this._i20Io.Name = "_i20Io";
            this._i20Io.Size = new System.Drawing.Size(13, 13);
            this._i20Io.State = BEMN.Forms.LedState.Off;
            this._i20Io.TabIndex = 39;
            // 
            // _i22
            // 
            this._i22.Location = new System.Drawing.Point(175, 130);
            this._i22.Name = "_i22";
            this._i22.Size = new System.Drawing.Size(13, 13);
            this._i22.State = BEMN.Forms.LedState.Off;
            this._i22.TabIndex = 43;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(194, 130);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(28, 13);
            this.label105.TabIndex = 42;
            this.label105.Text = "I>22";
            // 
            // _i19Io
            // 
            this._i19Io.Location = new System.Drawing.Point(156, 73);
            this._i19Io.Name = "_i19Io";
            this._i19Io.Size = new System.Drawing.Size(13, 13);
            this._i19Io.State = BEMN.Forms.LedState.Off;
            this._i19Io.TabIndex = 37;
            // 
            // _i21
            // 
            this._i21.Location = new System.Drawing.Point(175, 111);
            this._i21.Name = "_i21";
            this._i21.Size = new System.Drawing.Size(13, 13);
            this._i21.State = BEMN.Forms.LedState.Off;
            this._i21.TabIndex = 41;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(194, 111);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(28, 13);
            this.label106.TabIndex = 40;
            this.label106.Text = "I>21";
            // 
            // _i18Io
            // 
            this._i18Io.Location = new System.Drawing.Point(156, 54);
            this._i18Io.Name = "_i18Io";
            this._i18Io.Size = new System.Drawing.Size(13, 13);
            this._i18Io.State = BEMN.Forms.LedState.Off;
            this._i18Io.TabIndex = 35;
            // 
            // _i20
            // 
            this._i20.Location = new System.Drawing.Point(175, 92);
            this._i20.Name = "_i20";
            this._i20.Size = new System.Drawing.Size(13, 13);
            this._i20.State = BEMN.Forms.LedState.Off;
            this._i20.TabIndex = 39;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(195, 92);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(28, 13);
            this.label107.TabIndex = 38;
            this.label107.Text = "I>20";
            // 
            // _i17Io
            // 
            this._i17Io.Location = new System.Drawing.Point(156, 35);
            this._i17Io.Name = "_i17Io";
            this._i17Io.Size = new System.Drawing.Size(13, 13);
            this._i17Io.State = BEMN.Forms.LedState.Off;
            this._i17Io.TabIndex = 33;
            // 
            // _i19
            // 
            this._i19.Location = new System.Drawing.Point(175, 73);
            this._i19.Name = "_i19";
            this._i19.Size = new System.Drawing.Size(13, 13);
            this._i19.State = BEMN.Forms.LedState.Off;
            this._i19.TabIndex = 37;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(194, 73);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(28, 13);
            this.label108.TabIndex = 36;
            this.label108.Text = "I>19";
            // 
            // _i18
            // 
            this._i18.Location = new System.Drawing.Point(175, 54);
            this._i18.Name = "_i18";
            this._i18.Size = new System.Drawing.Size(13, 13);
            this._i18.State = BEMN.Forms.LedState.Off;
            this._i18.TabIndex = 35;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(194, 54);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(28, 13);
            this.label109.TabIndex = 34;
            this.label109.Text = "I>18";
            // 
            // _i16Io
            // 
            this._i16Io.Location = new System.Drawing.Point(79, 168);
            this._i16Io.Name = "_i16Io";
            this._i16Io.Size = new System.Drawing.Size(13, 13);
            this._i16Io.State = BEMN.Forms.LedState.Off;
            this._i16Io.TabIndex = 31;
            // 
            // _i17
            // 
            this._i17.Location = new System.Drawing.Point(175, 35);
            this._i17.Name = "_i17";
            this._i17.Size = new System.Drawing.Size(13, 13);
            this._i17.State = BEMN.Forms.LedState.Off;
            this._i17.TabIndex = 33;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(194, 35);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(28, 13);
            this.label110.TabIndex = 32;
            this.label110.Text = "I>17";
            // 
            // _i15Io
            // 
            this._i15Io.Location = new System.Drawing.Point(79, 149);
            this._i15Io.Name = "_i15Io";
            this._i15Io.Size = new System.Drawing.Size(13, 13);
            this._i15Io.State = BEMN.Forms.LedState.Off;
            this._i15Io.TabIndex = 29;
            // 
            // _i16
            // 
            this._i16.Location = new System.Drawing.Point(98, 168);
            this._i16.Name = "_i16";
            this._i16.Size = new System.Drawing.Size(13, 13);
            this._i16.State = BEMN.Forms.LedState.Off;
            this._i16.TabIndex = 31;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(117, 168);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(28, 13);
            this.label79.TabIndex = 30;
            this.label79.Text = "I>16";
            // 
            // _i14Io
            // 
            this._i14Io.Location = new System.Drawing.Point(79, 130);
            this._i14Io.Name = "_i14Io";
            this._i14Io.Size = new System.Drawing.Size(13, 13);
            this._i14Io.State = BEMN.Forms.LedState.Off;
            this._i14Io.TabIndex = 27;
            // 
            // _i15
            // 
            this._i15.Location = new System.Drawing.Point(98, 149);
            this._i15.Name = "_i15";
            this._i15.Size = new System.Drawing.Size(13, 13);
            this._i15.State = BEMN.Forms.LedState.Off;
            this._i15.TabIndex = 29;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(117, 149);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(28, 13);
            this.label80.TabIndex = 28;
            this.label80.Text = "I>15";
            // 
            // _i13Io
            // 
            this._i13Io.Location = new System.Drawing.Point(79, 111);
            this._i13Io.Name = "_i13Io";
            this._i13Io.Size = new System.Drawing.Size(13, 13);
            this._i13Io.State = BEMN.Forms.LedState.Off;
            this._i13Io.TabIndex = 25;
            // 
            // _i14
            // 
            this._i14.Location = new System.Drawing.Point(98, 130);
            this._i14.Name = "_i14";
            this._i14.Size = new System.Drawing.Size(13, 13);
            this._i14.State = BEMN.Forms.LedState.Off;
            this._i14.TabIndex = 27;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(117, 130);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 26;
            this.label81.Text = "I>14";
            // 
            // _i12Io
            // 
            this._i12Io.Location = new System.Drawing.Point(79, 92);
            this._i12Io.Name = "_i12Io";
            this._i12Io.Size = new System.Drawing.Size(13, 13);
            this._i12Io.State = BEMN.Forms.LedState.Off;
            this._i12Io.TabIndex = 23;
            // 
            // _i13
            // 
            this._i13.Location = new System.Drawing.Point(98, 111);
            this._i13.Name = "_i13";
            this._i13.Size = new System.Drawing.Size(13, 13);
            this._i13.State = BEMN.Forms.LedState.Off;
            this._i13.TabIndex = 25;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(117, 111);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(28, 13);
            this.label82.TabIndex = 24;
            this.label82.Text = "I>13";
            // 
            // _i11Io
            // 
            this._i11Io.Location = new System.Drawing.Point(79, 73);
            this._i11Io.Name = "_i11Io";
            this._i11Io.Size = new System.Drawing.Size(13, 13);
            this._i11Io.State = BEMN.Forms.LedState.Off;
            this._i11Io.TabIndex = 21;
            // 
            // _i12
            // 
            this._i12.Location = new System.Drawing.Point(98, 92);
            this._i12.Name = "_i12";
            this._i12.Size = new System.Drawing.Size(13, 13);
            this._i12.State = BEMN.Forms.LedState.Off;
            this._i12.TabIndex = 23;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(117, 92);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 13);
            this.label83.TabIndex = 22;
            this.label83.Text = "I>12";
            // 
            // _i10Io
            // 
            this._i10Io.Location = new System.Drawing.Point(79, 54);
            this._i10Io.Name = "_i10Io";
            this._i10Io.Size = new System.Drawing.Size(13, 13);
            this._i10Io.State = BEMN.Forms.LedState.Off;
            this._i10Io.TabIndex = 19;
            // 
            // _i11
            // 
            this._i11.Location = new System.Drawing.Point(98, 73);
            this._i11.Name = "_i11";
            this._i11.Size = new System.Drawing.Size(13, 13);
            this._i11.State = BEMN.Forms.LedState.Off;
            this._i11.TabIndex = 21;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(117, 73);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(28, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "I>11";
            // 
            // _i9Io
            // 
            this._i9Io.Location = new System.Drawing.Point(79, 35);
            this._i9Io.Name = "_i9Io";
            this._i9Io.Size = new System.Drawing.Size(13, 13);
            this._i9Io.State = BEMN.Forms.LedState.Off;
            this._i9Io.TabIndex = 17;
            // 
            // _i10
            // 
            this._i10.Location = new System.Drawing.Point(98, 54);
            this._i10.Name = "_i10";
            this._i10.Size = new System.Drawing.Size(13, 13);
            this._i10.State = BEMN.Forms.LedState.Off;
            this._i10.TabIndex = 19;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(117, 54);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 18;
            this.label85.Text = "I>10";
            // 
            // _i8Io
            // 
            this._i8Io.Location = new System.Drawing.Point(9, 168);
            this._i8Io.Name = "_i8Io";
            this._i8Io.Size = new System.Drawing.Size(13, 13);
            this._i8Io.State = BEMN.Forms.LedState.Off;
            this._i8Io.TabIndex = 15;
            // 
            // _i9
            // 
            this._i9.Location = new System.Drawing.Point(98, 35);
            this._i9.Name = "_i9";
            this._i9.Size = new System.Drawing.Size(13, 13);
            this._i9.State = BEMN.Forms.LedState.Off;
            this._i9.TabIndex = 17;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(117, 35);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(22, 13);
            this.label86.TabIndex = 16;
            this.label86.Text = "I>9";
            // 
            // _i7Io
            // 
            this._i7Io.Location = new System.Drawing.Point(9, 149);
            this._i7Io.Name = "_i7Io";
            this._i7Io.Size = new System.Drawing.Size(13, 13);
            this._i7Io.State = BEMN.Forms.LedState.Off;
            this._i7Io.TabIndex = 13;
            // 
            // _i8
            // 
            this._i8.Location = new System.Drawing.Point(28, 168);
            this._i8.Name = "_i8";
            this._i8.Size = new System.Drawing.Size(13, 13);
            this._i8.State = BEMN.Forms.LedState.Off;
            this._i8.TabIndex = 15;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(47, 168);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(22, 13);
            this.label87.TabIndex = 14;
            this.label87.Text = "I>8";
            // 
            // _i6Io
            // 
            this._i6Io.Location = new System.Drawing.Point(9, 130);
            this._i6Io.Name = "_i6Io";
            this._i6Io.Size = new System.Drawing.Size(13, 13);
            this._i6Io.State = BEMN.Forms.LedState.Off;
            this._i6Io.TabIndex = 11;
            // 
            // _i7
            // 
            this._i7.Location = new System.Drawing.Point(28, 149);
            this._i7.Name = "_i7";
            this._i7.Size = new System.Drawing.Size(13, 13);
            this._i7.State = BEMN.Forms.LedState.Off;
            this._i7.TabIndex = 13;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(47, 149);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(22, 13);
            this.label88.TabIndex = 12;
            this.label88.Text = "I>7";
            // 
            // _i5Io
            // 
            this._i5Io.Location = new System.Drawing.Point(9, 111);
            this._i5Io.Name = "_i5Io";
            this._i5Io.Size = new System.Drawing.Size(13, 13);
            this._i5Io.State = BEMN.Forms.LedState.Off;
            this._i5Io.TabIndex = 9;
            // 
            // _i6
            // 
            this._i6.Location = new System.Drawing.Point(28, 130);
            this._i6.Name = "_i6";
            this._i6.Size = new System.Drawing.Size(13, 13);
            this._i6.State = BEMN.Forms.LedState.Off;
            this._i6.TabIndex = 11;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(47, 130);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(22, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "I>6";
            // 
            // _i4Io
            // 
            this._i4Io.Location = new System.Drawing.Point(9, 92);
            this._i4Io.Name = "_i4Io";
            this._i4Io.Size = new System.Drawing.Size(13, 13);
            this._i4Io.State = BEMN.Forms.LedState.Off;
            this._i4Io.TabIndex = 7;
            // 
            // _i5
            // 
            this._i5.Location = new System.Drawing.Point(28, 111);
            this._i5.Name = "_i5";
            this._i5.Size = new System.Drawing.Size(13, 13);
            this._i5.State = BEMN.Forms.LedState.Off;
            this._i5.TabIndex = 9;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(47, 111);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "I>5";
            // 
            // _i3Io
            // 
            this._i3Io.Location = new System.Drawing.Point(9, 73);
            this._i3Io.Name = "_i3Io";
            this._i3Io.Size = new System.Drawing.Size(13, 13);
            this._i3Io.State = BEMN.Forms.LedState.Off;
            this._i3Io.TabIndex = 5;
            // 
            // _i4
            // 
            this._i4.Location = new System.Drawing.Point(28, 92);
            this._i4.Name = "_i4";
            this._i4.Size = new System.Drawing.Size(13, 13);
            this._i4.State = BEMN.Forms.LedState.Off;
            this._i4.TabIndex = 7;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(48, 92);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(22, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "I>4";
            // 
            // _i2Io
            // 
            this._i2Io.Location = new System.Drawing.Point(9, 54);
            this._i2Io.Name = "_i2Io";
            this._i2Io.Size = new System.Drawing.Size(13, 13);
            this._i2Io.State = BEMN.Forms.LedState.Off;
            this._i2Io.TabIndex = 3;
            // 
            // _i3
            // 
            this._i3.Location = new System.Drawing.Point(28, 73);
            this._i3.Name = "_i3";
            this._i3.Size = new System.Drawing.Size(13, 13);
            this._i3.State = BEMN.Forms.LedState.Off;
            this._i3.TabIndex = 5;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(47, 73);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(22, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "I>3";
            // 
            // _i1Io
            // 
            this._i1Io.Location = new System.Drawing.Point(9, 35);
            this._i1Io.Name = "_i1Io";
            this._i1Io.Size = new System.Drawing.Size(13, 13);
            this._i1Io.State = BEMN.Forms.LedState.Off;
            this._i1Io.TabIndex = 1;
            // 
            // _i2
            // 
            this._i2.Location = new System.Drawing.Point(28, 54);
            this._i2.Name = "_i2";
            this._i2.Size = new System.Drawing.Size(13, 13);
            this._i2.State = BEMN.Forms.LedState.Off;
            this._i2.TabIndex = 3;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(47, 54);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(22, 13);
            this.label93.TabIndex = 2;
            this.label93.Text = "I>2";
            // 
            // _i1
            // 
            this._i1.Location = new System.Drawing.Point(28, 35);
            this._i1.Name = "_i1";
            this._i1.Size = new System.Drawing.Size(13, 13);
            this._i1.State = BEMN.Forms.LedState.Off;
            this._i1.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(47, 35);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "I>1";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl48);
            this.groupBox21.Controls.Add(this._ssl40);
            this.groupBox21.Controls.Add(this._ssl32);
            this.groupBox21.Controls.Add(this.label40);
            this.groupBox21.Controls.Add(this.label32);
            this.groupBox21.Controls.Add(this.label237);
            this.groupBox21.Controls.Add(this._ssl47);
            this.groupBox21.Controls.Add(this._ssl39);
            this.groupBox21.Controls.Add(this._ssl31);
            this.groupBox21.Controls.Add(this.label39);
            this.groupBox21.Controls.Add(this.label31);
            this.groupBox21.Controls.Add(this.label238);
            this.groupBox21.Controls.Add(this._ssl46);
            this.groupBox21.Controls.Add(this._ssl38);
            this.groupBox21.Controls.Add(this._ssl30);
            this.groupBox21.Controls.Add(this.label38);
            this.groupBox21.Controls.Add(this.label30);
            this.groupBox21.Controls.Add(this.label239);
            this.groupBox21.Controls.Add(this._ssl45);
            this.groupBox21.Controls.Add(this._ssl37);
            this.groupBox21.Controls.Add(this._ssl29);
            this.groupBox21.Controls.Add(this.label37);
            this.groupBox21.Controls.Add(this.label29);
            this.groupBox21.Controls.Add(this.label240);
            this.groupBox21.Controls.Add(this._ssl44);
            this.groupBox21.Controls.Add(this._ssl36);
            this.groupBox21.Controls.Add(this._ssl28);
            this.groupBox21.Controls.Add(this.label36);
            this.groupBox21.Controls.Add(this.label28);
            this.groupBox21.Controls.Add(this.label241);
            this.groupBox21.Controls.Add(this._ssl43);
            this.groupBox21.Controls.Add(this._ssl35);
            this.groupBox21.Controls.Add(this._ssl27);
            this.groupBox21.Controls.Add(this.label35);
            this.groupBox21.Controls.Add(this.label27);
            this.groupBox21.Controls.Add(this.label242);
            this.groupBox21.Controls.Add(this._ssl42);
            this.groupBox21.Controls.Add(this._ssl34);
            this.groupBox21.Controls.Add(this._ssl26);
            this.groupBox21.Controls.Add(this.label34);
            this.groupBox21.Controls.Add(this.label26);
            this.groupBox21.Controls.Add(this.label243);
            this.groupBox21.Controls.Add(this._ssl41);
            this.groupBox21.Controls.Add(this.label33);
            this.groupBox21.Controls.Add(this._ssl33);
            this.groupBox21.Controls.Add(this.label25);
            this.groupBox21.Controls.Add(this._ssl25);
            this.groupBox21.Controls.Add(this.label244);
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(300, 6);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(372, 191);
            this.groupBox21.TabIndex = 14;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl48
            // 
            this._ssl48.Location = new System.Drawing.Point(307, 166);
            this._ssl48.Name = "_ssl48";
            this._ssl48.Size = new System.Drawing.Size(13, 13);
            this._ssl48.State = BEMN.Forms.LedState.Off;
            this._ssl48.TabIndex = 63;
            // 
            // _ssl40
            // 
            this._ssl40.Location = new System.Drawing.Point(247, 166);
            this._ssl40.Name = "_ssl40";
            this._ssl40.Size = new System.Drawing.Size(13, 13);
            this._ssl40.State = BEMN.Forms.LedState.Off;
            this._ssl40.TabIndex = 63;
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(186, 166);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(323, 166);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 13);
            this.label40.TabIndex = 62;
            this.label40.Text = "ССЛ48";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(263, 166);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 13);
            this.label32.TabIndex = 62;
            this.label32.Text = "ССЛ40";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(202, 166);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl47
            // 
            this._ssl47.Location = new System.Drawing.Point(307, 147);
            this._ssl47.Name = "_ssl47";
            this._ssl47.Size = new System.Drawing.Size(13, 13);
            this._ssl47.State = BEMN.Forms.LedState.Off;
            this._ssl47.TabIndex = 61;
            // 
            // _ssl39
            // 
            this._ssl39.Location = new System.Drawing.Point(247, 147);
            this._ssl39.Name = "_ssl39";
            this._ssl39.Size = new System.Drawing.Size(13, 13);
            this._ssl39.State = BEMN.Forms.LedState.Off;
            this._ssl39.TabIndex = 61;
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(186, 147);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(323, 147);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 13);
            this.label39.TabIndex = 60;
            this.label39.Text = "ССЛ47";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(263, 147);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 13);
            this.label31.TabIndex = 60;
            this.label31.Text = "ССЛ39";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(202, 147);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl46
            // 
            this._ssl46.Location = new System.Drawing.Point(307, 128);
            this._ssl46.Name = "_ssl46";
            this._ssl46.Size = new System.Drawing.Size(13, 13);
            this._ssl46.State = BEMN.Forms.LedState.Off;
            this._ssl46.TabIndex = 59;
            // 
            // _ssl38
            // 
            this._ssl38.Location = new System.Drawing.Point(247, 128);
            this._ssl38.Name = "_ssl38";
            this._ssl38.Size = new System.Drawing.Size(13, 13);
            this._ssl38.State = BEMN.Forms.LedState.Off;
            this._ssl38.TabIndex = 59;
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(186, 128);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(323, 128);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 13);
            this.label38.TabIndex = 58;
            this.label38.Text = "ССЛ46";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(263, 128);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 58;
            this.label30.Text = "ССЛ38";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(202, 128);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl45
            // 
            this._ssl45.Location = new System.Drawing.Point(307, 109);
            this._ssl45.Name = "_ssl45";
            this._ssl45.Size = new System.Drawing.Size(13, 13);
            this._ssl45.State = BEMN.Forms.LedState.Off;
            this._ssl45.TabIndex = 57;
            // 
            // _ssl37
            // 
            this._ssl37.Location = new System.Drawing.Point(247, 109);
            this._ssl37.Name = "_ssl37";
            this._ssl37.Size = new System.Drawing.Size(13, 13);
            this._ssl37.State = BEMN.Forms.LedState.Off;
            this._ssl37.TabIndex = 57;
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(186, 109);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(323, 109);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 13);
            this.label37.TabIndex = 56;
            this.label37.Text = "ССЛ45";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(263, 109);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 13);
            this.label29.TabIndex = 56;
            this.label29.Text = "ССЛ37";
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(202, 109);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl44
            // 
            this._ssl44.Location = new System.Drawing.Point(307, 90);
            this._ssl44.Name = "_ssl44";
            this._ssl44.Size = new System.Drawing.Size(13, 13);
            this._ssl44.State = BEMN.Forms.LedState.Off;
            this._ssl44.TabIndex = 55;
            // 
            // _ssl36
            // 
            this._ssl36.Location = new System.Drawing.Point(247, 90);
            this._ssl36.Name = "_ssl36";
            this._ssl36.Size = new System.Drawing.Size(13, 13);
            this._ssl36.State = BEMN.Forms.LedState.Off;
            this._ssl36.TabIndex = 55;
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(186, 90);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(323, 90);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 54;
            this.label36.Text = "ССЛ44";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(263, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 13);
            this.label28.TabIndex = 54;
            this.label28.Text = "ССЛ36";
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(202, 90);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl43
            // 
            this._ssl43.Location = new System.Drawing.Point(307, 71);
            this._ssl43.Name = "_ssl43";
            this._ssl43.Size = new System.Drawing.Size(13, 13);
            this._ssl43.State = BEMN.Forms.LedState.Off;
            this._ssl43.TabIndex = 53;
            // 
            // _ssl35
            // 
            this._ssl35.Location = new System.Drawing.Point(247, 71);
            this._ssl35.Name = "_ssl35";
            this._ssl35.Size = new System.Drawing.Size(13, 13);
            this._ssl35.State = BEMN.Forms.LedState.Off;
            this._ssl35.TabIndex = 53;
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(186, 71);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(323, 71);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 13);
            this.label35.TabIndex = 52;
            this.label35.Text = "ССЛ43";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(263, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 52;
            this.label27.Text = "ССЛ35";
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(202, 71);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl42
            // 
            this._ssl42.Location = new System.Drawing.Point(307, 52);
            this._ssl42.Name = "_ssl42";
            this._ssl42.Size = new System.Drawing.Size(13, 13);
            this._ssl42.State = BEMN.Forms.LedState.Off;
            this._ssl42.TabIndex = 51;
            // 
            // _ssl34
            // 
            this._ssl34.Location = new System.Drawing.Point(247, 52);
            this._ssl34.Name = "_ssl34";
            this._ssl34.Size = new System.Drawing.Size(13, 13);
            this._ssl34.State = BEMN.Forms.LedState.Off;
            this._ssl34.TabIndex = 51;
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(186, 52);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(323, 52);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 13);
            this.label34.TabIndex = 50;
            this.label34.Text = "ССЛ42";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(263, 52);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 13);
            this.label26.TabIndex = 50;
            this.label26.Text = "ССЛ34";
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(202, 52);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl41
            // 
            this._ssl41.Location = new System.Drawing.Point(307, 33);
            this._ssl41.Name = "_ssl41";
            this._ssl41.Size = new System.Drawing.Size(13, 13);
            this._ssl41.State = BEMN.Forms.LedState.Off;
            this._ssl41.TabIndex = 49;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(323, 33);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 48;
            this.label33.Text = "ССЛ41";
            // 
            // _ssl33
            // 
            this._ssl33.Location = new System.Drawing.Point(247, 33);
            this._ssl33.Name = "_ssl33";
            this._ssl33.Size = new System.Drawing.Size(13, 13);
            this._ssl33.State = BEMN.Forms.LedState.Off;
            this._ssl33.TabIndex = 49;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(263, 33);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 48;
            this.label25.Text = "ССЛ33";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(186, 33);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(202, 33);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(126, 166);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(142, 166);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(126, 147);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(142, 147);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(126, 128);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(142, 128);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(126, 109);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(142, 109);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(126, 90);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(143, 90);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(126, 71);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(142, 71);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(126, 52);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(142, 52);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(126, 33);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(142, 33);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(63, 166);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(82, 166);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(63, 147);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(79, 147);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(63, 128);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(79, 128);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(63, 109);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(79, 109);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(63, 90);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(79, 90);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(63, 71);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(79, 71);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(63, 52);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(79, 52);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(63, 33);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(79, 33);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 166);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(22, 166);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 147);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(22, 147);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 128);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(22, 128);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 109);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(22, 109);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 90);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(23, 90);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 71);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(22, 71);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 52);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(22, 52);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 33);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(22, 33);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz24);
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this._vz24label);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz23);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this._vz23label);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz22);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this._vz22label);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz21);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this._vz21label);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz20);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label293);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz19);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label292);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz18);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label291);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz17);
            this.groupBox12.Controls.Add(this.label290);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(477, 203);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(195, 192);
            this.groupBox12.TabIndex = 5;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz24
            // 
            this._vz24.Location = new System.Drawing.Point(132, 168);
            this._vz24.Name = "_vz24";
            this._vz24.Size = new System.Drawing.Size(13, 13);
            this._vz24.State = BEMN.Forms.LedState.Off;
            this._vz24.TabIndex = 31;
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(71, 168);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // _vz24label
            // 
            this._vz24label.AutoSize = true;
            this._vz24label.Location = new System.Drawing.Point(151, 168);
            this._vz24label.Name = "_vz24label";
            this._vz24label.Size = new System.Drawing.Size(36, 13);
            this._vz24label.TabIndex = 30;
            this._vz24label.Text = "ВЗ-24";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(90, 168);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВЗ-16";
            // 
            // _vz23
            // 
            this._vz23.Location = new System.Drawing.Point(132, 149);
            this._vz23.Name = "_vz23";
            this._vz23.Size = new System.Drawing.Size(13, 13);
            this._vz23.State = BEMN.Forms.LedState.Off;
            this._vz23.TabIndex = 29;
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(71, 149);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // _vz23label
            // 
            this._vz23label.AutoSize = true;
            this._vz23label.Location = new System.Drawing.Point(151, 149);
            this._vz23label.Name = "_vz23label";
            this._vz23label.Size = new System.Drawing.Size(36, 13);
            this._vz23label.TabIndex = 28;
            this._vz23label.Text = "ВЗ-23";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(90, 149);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВЗ-15";
            // 
            // _vz22
            // 
            this._vz22.Location = new System.Drawing.Point(132, 130);
            this._vz22.Name = "_vz22";
            this._vz22.Size = new System.Drawing.Size(13, 13);
            this._vz22.State = BEMN.Forms.LedState.Off;
            this._vz22.TabIndex = 27;
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(71, 130);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // _vz22label
            // 
            this._vz22label.AutoSize = true;
            this._vz22label.Location = new System.Drawing.Point(151, 130);
            this._vz22label.Name = "_vz22label";
            this._vz22label.Size = new System.Drawing.Size(36, 13);
            this._vz22label.TabIndex = 26;
            this._vz22label.Text = "ВЗ-22";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(90, 130);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(36, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВЗ-14";
            // 
            // _vz21
            // 
            this._vz21.Location = new System.Drawing.Point(132, 111);
            this._vz21.Name = "_vz21";
            this._vz21.Size = new System.Drawing.Size(13, 13);
            this._vz21.State = BEMN.Forms.LedState.Off;
            this._vz21.TabIndex = 25;
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(71, 111);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // _vz21label
            // 
            this._vz21label.AutoSize = true;
            this._vz21label.Location = new System.Drawing.Point(151, 111);
            this._vz21label.Name = "_vz21label";
            this._vz21label.Size = new System.Drawing.Size(36, 13);
            this._vz21label.TabIndex = 24;
            this._vz21label.Text = "ВЗ-21";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(90, 111);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(36, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВЗ-13";
            // 
            // _vz20
            // 
            this._vz20.Location = new System.Drawing.Point(132, 92);
            this._vz20.Name = "_vz20";
            this._vz20.Size = new System.Drawing.Size(13, 13);
            this._vz20.State = BEMN.Forms.LedState.Off;
            this._vz20.TabIndex = 23;
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(71, 92);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(151, 92);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(36, 13);
            this.label293.TabIndex = 22;
            this.label293.Text = "ВЗ-20";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(90, 92);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(36, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВЗ-12";
            // 
            // _vz19
            // 
            this._vz19.Location = new System.Drawing.Point(132, 73);
            this._vz19.Name = "_vz19";
            this._vz19.Size = new System.Drawing.Size(13, 13);
            this._vz19.State = BEMN.Forms.LedState.Off;
            this._vz19.TabIndex = 21;
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(71, 73);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(151, 73);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(36, 13);
            this.label292.TabIndex = 20;
            this.label292.Text = "ВЗ-19";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(90, 73);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(36, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВЗ-11";
            // 
            // _vz18
            // 
            this._vz18.Location = new System.Drawing.Point(132, 54);
            this._vz18.Name = "_vz18";
            this._vz18.Size = new System.Drawing.Size(13, 13);
            this._vz18.State = BEMN.Forms.LedState.Off;
            this._vz18.TabIndex = 19;
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(71, 54);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(151, 54);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(36, 13);
            this.label291.TabIndex = 18;
            this.label291.Text = "ВЗ-18";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(90, 54);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(36, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВЗ-10";
            // 
            // _vz17
            // 
            this._vz17.Location = new System.Drawing.Point(132, 35);
            this._vz17.Name = "_vz17";
            this._vz17.Size = new System.Drawing.Size(13, 13);
            this._vz17.State = BEMN.Forms.LedState.Off;
            this._vz17.TabIndex = 17;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Location = new System.Drawing.Point(151, 35);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(36, 13);
            this.label290.TabIndex = 16;
            this.label290.Text = "ВЗ-17";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(71, 35);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(90, 35);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(30, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВЗ-9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 168);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 168);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(30, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВЗ-8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 149);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 149);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(30, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВЗ-7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 130);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 130);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(30, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВЗ-6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 111);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 111);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(30, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВЗ-5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 92);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 92);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(30, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВЗ-4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 73);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 73);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(30, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВЗ-3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 54);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 54);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(30, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВЗ-2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 35);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 35);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(30, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВЗ-1";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._faultMeasuring);
            this.groupBox19.Controls.Add(this.label204);
            this.groupBox19.Controls.Add(this._faultUrov);
            this.groupBox19.Controls.Add(this.label207);
            this.groupBox19.Controls.Add(this._neisprTN);
            this.groupBox19.Controls.Add(this._neisprTNLabel);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule6);
            this.groupBox19.Controls.Add(this.label41);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this._faultTt2);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.faultTt2Label);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultTt1);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this.faultTt1Label);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this._faultTt3);
            this.groupBox19.Controls.Add(this._faultLogic);
            this.groupBox19.Controls.Add(this.faultTt3Label);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Location = new System.Drawing.Point(159, 402);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(217, 212);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // _faultMeasuring
            // 
            this._faultMeasuring.Location = new System.Drawing.Point(6, 133);
            this._faultMeasuring.Name = "_faultMeasuring";
            this._faultMeasuring.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuring.State = BEMN.Forms.LedState.Off;
            this._faultMeasuring.TabIndex = 33;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(22, 133);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(57, 13);
            this.label204.TabIndex = 32;
            this.label204.Text = "ТТ общая";
            // 
            // _faultUrov
            // 
            this._faultUrov.Location = new System.Drawing.Point(6, 57);
            this._faultUrov.Name = "_faultUrov";
            this._faultUrov.Size = new System.Drawing.Size(13, 13);
            this._faultUrov.State = BEMN.Forms.LedState.Off;
            this._faultUrov.TabIndex = 31;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(22, 57);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(37, 13);
            this.label207.TabIndex = 30;
            this.label207.Text = "УРОВ";
            // 
            // _neisprTN
            // 
            this._neisprTN.Location = new System.Drawing.Point(106, 190);
            this._neisprTN.Name = "_neisprTN";
            this._neisprTN.Size = new System.Drawing.Size(13, 13);
            this._neisprTN.State = BEMN.Forms.LedState.Off;
            this._neisprTN.TabIndex = 33;
            this._neisprTN.Visible = false;
            // 
            // _neisprTNLabel
            // 
            this._neisprTNLabel.AutoSize = true;
            this._neisprTNLabel.Location = new System.Drawing.Point(121, 190);
            this._neisprTNLabel.Name = "_neisprTNLabel";
            this._neisprTNLabel.Size = new System.Drawing.Size(66, 13);
            this._neisprTNLabel.TabIndex = 32;
            this._neisprTNLabel.Text = "Неиспр. ТН";
            this._neisprTNLabel.Visible = false;
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(106, 152);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(121, 152);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(71, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "Журнала ав.";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(106, 133);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(121, 133);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(48, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осцилл.";
            // 
            // _faultModule6
            // 
            this._faultModule6.Location = new System.Drawing.Point(106, 114);
            this._faultModule6.Name = "_faultModule6";
            this._faultModule6.Size = new System.Drawing.Size(13, 13);
            this._faultModule6.State = BEMN.Forms.LedState.Off;
            this._faultModule6.TabIndex = 25;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(121, 114);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(54, 13);
            this.label41.TabIndex = 24;
            this.label41.Text = "Модуля 6";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(106, 95);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(121, 95);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(106, 76);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(121, 76);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(106, 57);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(121, 57);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(106, 38);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(121, 38);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(106, 19);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(121, 19);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(106, 171);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(121, 171);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(82, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "Журнала сист.";
            // 
            // _faultTt2
            // 
            this._faultTt2.Location = new System.Drawing.Point(6, 171);
            this._faultTt2.Name = "_faultTt2";
            this._faultTt2.Size = new System.Drawing.Size(13, 13);
            this._faultTt2.State = BEMN.Forms.LedState.Off;
            this._faultTt2.TabIndex = 11;
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(6, 95);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // faultTt2Label
            // 
            this.faultTt2Label.AutoSize = true;
            this.faultTt2Label.Location = new System.Drawing.Point(22, 171);
            this.faultTt2Label.Name = "faultTt2Label";
            this.faultTt2Label.Size = new System.Drawing.Size(46, 13);
            this.faultTt2Label.TabIndex = 10;
            this.faultTt2Label.Text = "ТТ СШ2";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(22, 95);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultTt1
            // 
            this._faultTt1.Location = new System.Drawing.Point(6, 152);
            this._faultTt1.Name = "_faultTt1";
            this._faultTt1.Size = new System.Drawing.Size(13, 13);
            this._faultTt1.State = BEMN.Forms.LedState.Off;
            this._faultTt1.TabIndex = 9;
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(6, 76);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // faultTt1Label
            // 
            this.faultTt1Label.AutoSize = true;
            this.faultTt1Label.Location = new System.Drawing.Point(22, 152);
            this.faultTt1Label.Name = "faultTt1Label";
            this.faultTt1Label.Size = new System.Drawing.Size(46, 13);
            this.faultTt1Label.TabIndex = 8;
            this.faultTt1Label.Text = "ТТ СШ1";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(22, 76);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // _faultTt3
            // 
            this._faultTt3.Location = new System.Drawing.Point(6, 190);
            this._faultTt3.Name = "_faultTt3";
            this._faultTt3.Size = new System.Drawing.Size(13, 13);
            this._faultTt3.State = BEMN.Forms.LedState.Off;
            this._faultTt3.TabIndex = 7;
            // 
            // _faultLogic
            // 
            this._faultLogic.Location = new System.Drawing.Point(6, 114);
            this._faultLogic.Name = "_faultLogic";
            this._faultLogic.Size = new System.Drawing.Size(13, 13);
            this._faultLogic.State = BEMN.Forms.LedState.Off;
            this._faultLogic.TabIndex = 7;
            // 
            // faultTt3Label
            // 
            this.faultTt3Label.AutoSize = true;
            this.faultTt3Label.Location = new System.Drawing.Point(22, 190);
            this.faultTt3Label.Name = "faultTt3Label";
            this.faultTt3Label.Size = new System.Drawing.Size(40, 13);
            this.faultTt3Label.TabIndex = 6;
            this.faultTt3Label.Text = "ТТ ПО";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(22, 114);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(44, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Логики";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(22, 38);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 19);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(22, 19);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // _neisprTNGroup
            // 
            this._neisprTNGroup.Controls.Add(this._neisprTNUabc);
            this._neisprTNGroup.Controls.Add(this.label45);
            this._neisprTNGroup.Controls.Add(this.label42);
            this._neisprTNGroup.Controls.Add(this._neisprTNns);
            this._neisprTNGroup.Controls.Add(this.label43);
            this._neisprTNGroup.Controls.Add(this._neisprTN3U0);
            this._neisprTNGroup.Controls.Add(this._neisprTN3U0Label);
            this._neisprTNGroup.Controls.Add(this._neisprTNUn);
            this._neisprTNGroup.Location = new System.Drawing.Point(626, 402);
            this._neisprTNGroup.Name = "_neisprTNGroup";
            this._neisprTNGroup.Size = new System.Drawing.Size(136, 105);
            this._neisprTNGroup.TabIndex = 7;
            this._neisprTNGroup.TabStop = false;
            this._neisprTNGroup.Text = "Неисправности ТН";
            this._neisprTNGroup.Visible = false;
            // 
            // _neisprTNUabc
            // 
            this._neisprTNUabc.Location = new System.Drawing.Point(6, 57);
            this._neisprTNUabc.Name = "_neisprTNUabc";
            this._neisprTNUabc.Size = new System.Drawing.Size(13, 13);
            this._neisprTNUabc.State = BEMN.Forms.LedState.Off;
            this._neisprTNUabc.TabIndex = 37;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(22, 76);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(103, 13);
            this.label45.TabIndex = 36;
            this.label45.Text = "Внеш. неиспр. ТНn";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(22, 57);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(97, 13);
            this.label42.TabIndex = 36;
            this.label42.Text = "Внеш. неиспр. ТН";
            // 
            // _neisprTNns
            // 
            this._neisprTNns.Location = new System.Drawing.Point(6, 38);
            this._neisprTNns.Name = "_neisprTNns";
            this._neisprTNns.Size = new System.Drawing.Size(13, 13);
            this._neisprTNns.State = BEMN.Forms.LedState.Off;
            this._neisprTNns.TabIndex = 35;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(22, 38);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(111, 13);
            this.label43.TabIndex = 34;
            this.label43.Text = "По контролю несим.";
            // 
            // _neisprTN3U0
            // 
            this._neisprTN3U0.Location = new System.Drawing.Point(6, 19);
            this._neisprTN3U0.Name = "_neisprTN3U0";
            this._neisprTN3U0.Size = new System.Drawing.Size(13, 13);
            this._neisprTN3U0.State = BEMN.Forms.LedState.Off;
            this._neisprTN3U0.TabIndex = 33;
            // 
            // _neisprTN3U0Label
            // 
            this._neisprTN3U0Label.AutoSize = true;
            this._neisprTN3U0Label.Location = new System.Drawing.Point(22, 19);
            this._neisprTN3U0Label.Name = "_neisprTN3U0Label";
            this._neisprTN3U0Label.Size = new System.Drawing.Size(96, 13);
            this._neisprTN3U0Label.TabIndex = 32;
            this._neisprTN3U0Label.Text = "По контролю 3U0";
            // 
            // _neisprTNUn
            // 
            this._neisprTNUn.Location = new System.Drawing.Point(6, 76);
            this._neisprTNUn.Name = "_neisprTNUn";
            this._neisprTNUn.Size = new System.Drawing.Size(13, 13);
            this._neisprTNUn.State = BEMN.Forms.LedState.Off;
            this._neisprTNUn.TabIndex = 3;
            // 
            // urovGroupBox
            // 
            this.urovGroupBox.Controls.Add(this._urovPO);
            this.urovGroupBox.Controls.Add(this.label158);
            this.urovGroupBox.Controls.Add(this._urovSH2);
            this.urovGroupBox.Controls.Add(this.label159);
            this.urovGroupBox.Controls.Add(this._urovSH1);
            this.urovGroupBox.Controls.Add(this.label160);
            this.urovGroupBox.Controls.Add(this._urovPr24);
            this.urovGroupBox.Controls.Add(this._urovPr16);
            this.urovGroupBox.Controls.Add(this._urovPrLabel24);
            this.urovGroupBox.Controls.Add(this.label150);
            this.urovGroupBox.Controls.Add(this._urovPr23);
            this.urovGroupBox.Controls.Add(this._urovPr15);
            this.urovGroupBox.Controls.Add(this._urovPrLabel23);
            this.urovGroupBox.Controls.Add(this.label151);
            this.urovGroupBox.Controls.Add(this._urovPr22);
            this.urovGroupBox.Controls.Add(this._urovPr14);
            this.urovGroupBox.Controls.Add(this._urovPrLabel22);
            this.urovGroupBox.Controls.Add(this.label152);
            this.urovGroupBox.Controls.Add(this._urovPr21);
            this.urovGroupBox.Controls.Add(this._urovPr13);
            this.urovGroupBox.Controls.Add(this._urovPrLabel21);
            this.urovGroupBox.Controls.Add(this.label153);
            this.urovGroupBox.Controls.Add(this._urovPr20);
            this.urovGroupBox.Controls.Add(this._urovPr12);
            this.urovGroupBox.Controls.Add(this._urovPrLabel20);
            this.urovGroupBox.Controls.Add(this.label154);
            this.urovGroupBox.Controls.Add(this._urovPr19);
            this.urovGroupBox.Controls.Add(this._urovPr11);
            this.urovGroupBox.Controls.Add(this._urovPrLabel19);
            this.urovGroupBox.Controls.Add(this.label155);
            this.urovGroupBox.Controls.Add(this._urovPr18);
            this.urovGroupBox.Controls.Add(this._urovPr10);
            this.urovGroupBox.Controls.Add(this._urovPrLabel18);
            this.urovGroupBox.Controls.Add(this.label156);
            this.urovGroupBox.Controls.Add(this._urovPr17);
            this.urovGroupBox.Controls.Add(this._urovPrLabel17);
            this.urovGroupBox.Controls.Add(this._urovPr9);
            this.urovGroupBox.Controls.Add(this.label157);
            this.urovGroupBox.Controls.Add(this._urovPr8);
            this.urovGroupBox.Controls.Add(this.label135);
            this.urovGroupBox.Controls.Add(this._urovPr7);
            this.urovGroupBox.Controls.Add(this.label143);
            this.urovGroupBox.Controls.Add(this._urovPr6);
            this.urovGroupBox.Controls.Add(this.label144);
            this.urovGroupBox.Controls.Add(this._urovPr5);
            this.urovGroupBox.Controls.Add(this.label145);
            this.urovGroupBox.Controls.Add(this._urovPr4);
            this.urovGroupBox.Controls.Add(this.label146);
            this.urovGroupBox.Controls.Add(this._urovPr3);
            this.urovGroupBox.Controls.Add(this.label147);
            this.urovGroupBox.Controls.Add(this._urovPr2);
            this.urovGroupBox.Controls.Add(this.label148);
            this.urovGroupBox.Controls.Add(this._urovPr1);
            this.urovGroupBox.Controls.Add(this.label149);
            this.urovGroupBox.Location = new System.Drawing.Point(382, 402);
            this.urovGroupBox.Name = "urovGroupBox";
            this.urovGroupBox.Size = new System.Drawing.Size(238, 173);
            this.urovGroupBox.TabIndex = 7;
            this.urovGroupBox.TabStop = false;
            this.urovGroupBox.Text = "УРОВ";
            // 
            // _urovPO
            // 
            this._urovPO.Location = new System.Drawing.Point(6, 57);
            this._urovPO.Name = "_urovPO";
            this._urovPO.Size = new System.Drawing.Size(13, 13);
            this._urovPO.State = BEMN.Forms.LedState.Off;
            this._urovPO.TabIndex = 37;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(25, 57);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(23, 13);
            this.label158.TabIndex = 36;
            this.label158.Text = "ПО";
            // 
            // _urovSH2
            // 
            this._urovSH2.Location = new System.Drawing.Point(6, 38);
            this._urovSH2.Name = "_urovSH2";
            this._urovSH2.Size = new System.Drawing.Size(13, 13);
            this._urovSH2.State = BEMN.Forms.LedState.Off;
            this._urovSH2.TabIndex = 35;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(25, 38);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(29, 13);
            this.label159.TabIndex = 34;
            this.label159.Text = "СШ2";
            // 
            // _urovSH1
            // 
            this._urovSH1.Location = new System.Drawing.Point(6, 19);
            this._urovSH1.Name = "_urovSH1";
            this._urovSH1.Size = new System.Drawing.Size(13, 13);
            this._urovSH1.State = BEMN.Forms.LedState.Off;
            this._urovSH1.TabIndex = 33;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(25, 19);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(29, 13);
            this.label160.TabIndex = 32;
            this.label160.Text = "СШ1";
            // 
            // _urovPr24
            // 
            this._urovPr24.Location = new System.Drawing.Point(176, 152);
            this._urovPr24.Name = "_urovPr24";
            this._urovPr24.Size = new System.Drawing.Size(13, 13);
            this._urovPr24.State = BEMN.Forms.LedState.Off;
            this._urovPr24.TabIndex = 31;
            // 
            // _urovPr16
            // 
            this._urovPr16.Location = new System.Drawing.Point(115, 152);
            this._urovPr16.Name = "_urovPr16";
            this._urovPr16.Size = new System.Drawing.Size(13, 13);
            this._urovPr16.State = BEMN.Forms.LedState.Off;
            this._urovPr16.TabIndex = 31;
            // 
            // _urovPrLabel24
            // 
            this._urovPrLabel24.AutoSize = true;
            this._urovPrLabel24.Location = new System.Drawing.Point(195, 152);
            this._urovPrLabel24.Name = "_urovPrLabel24";
            this._urovPrLabel24.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel24.TabIndex = 30;
            this._urovPrLabel24.Text = "Пр.24";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(134, 152);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(36, 13);
            this.label150.TabIndex = 30;
            this.label150.Text = "Пр.16";
            // 
            // _urovPr23
            // 
            this._urovPr23.Location = new System.Drawing.Point(176, 133);
            this._urovPr23.Name = "_urovPr23";
            this._urovPr23.Size = new System.Drawing.Size(13, 13);
            this._urovPr23.State = BEMN.Forms.LedState.Off;
            this._urovPr23.TabIndex = 29;
            // 
            // _urovPr15
            // 
            this._urovPr15.Location = new System.Drawing.Point(115, 133);
            this._urovPr15.Name = "_urovPr15";
            this._urovPr15.Size = new System.Drawing.Size(13, 13);
            this._urovPr15.State = BEMN.Forms.LedState.Off;
            this._urovPr15.TabIndex = 29;
            // 
            // _urovPrLabel23
            // 
            this._urovPrLabel23.AutoSize = true;
            this._urovPrLabel23.Location = new System.Drawing.Point(195, 133);
            this._urovPrLabel23.Name = "_urovPrLabel23";
            this._urovPrLabel23.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel23.TabIndex = 28;
            this._urovPrLabel23.Text = "Пр.23";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(134, 133);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(36, 13);
            this.label151.TabIndex = 28;
            this.label151.Text = "Пр.15";
            // 
            // _urovPr22
            // 
            this._urovPr22.Location = new System.Drawing.Point(176, 114);
            this._urovPr22.Name = "_urovPr22";
            this._urovPr22.Size = new System.Drawing.Size(13, 13);
            this._urovPr22.State = BEMN.Forms.LedState.Off;
            this._urovPr22.TabIndex = 27;
            // 
            // _urovPr14
            // 
            this._urovPr14.Location = new System.Drawing.Point(115, 114);
            this._urovPr14.Name = "_urovPr14";
            this._urovPr14.Size = new System.Drawing.Size(13, 13);
            this._urovPr14.State = BEMN.Forms.LedState.Off;
            this._urovPr14.TabIndex = 27;
            // 
            // _urovPrLabel22
            // 
            this._urovPrLabel22.AutoSize = true;
            this._urovPrLabel22.Location = new System.Drawing.Point(195, 114);
            this._urovPrLabel22.Name = "_urovPrLabel22";
            this._urovPrLabel22.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel22.TabIndex = 26;
            this._urovPrLabel22.Text = "Пр.22";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(134, 114);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(36, 13);
            this.label152.TabIndex = 26;
            this.label152.Text = "Пр.14";
            // 
            // _urovPr21
            // 
            this._urovPr21.Location = new System.Drawing.Point(176, 95);
            this._urovPr21.Name = "_urovPr21";
            this._urovPr21.Size = new System.Drawing.Size(13, 13);
            this._urovPr21.State = BEMN.Forms.LedState.Off;
            this._urovPr21.TabIndex = 25;
            // 
            // _urovPr13
            // 
            this._urovPr13.Location = new System.Drawing.Point(115, 95);
            this._urovPr13.Name = "_urovPr13";
            this._urovPr13.Size = new System.Drawing.Size(13, 13);
            this._urovPr13.State = BEMN.Forms.LedState.Off;
            this._urovPr13.TabIndex = 25;
            // 
            // _urovPrLabel21
            // 
            this._urovPrLabel21.AutoSize = true;
            this._urovPrLabel21.Location = new System.Drawing.Point(195, 95);
            this._urovPrLabel21.Name = "_urovPrLabel21";
            this._urovPrLabel21.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel21.TabIndex = 24;
            this._urovPrLabel21.Text = "Пр.21";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(134, 95);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(36, 13);
            this.label153.TabIndex = 24;
            this.label153.Text = "Пр.13";
            // 
            // _urovPr20
            // 
            this._urovPr20.Location = new System.Drawing.Point(176, 76);
            this._urovPr20.Name = "_urovPr20";
            this._urovPr20.Size = new System.Drawing.Size(13, 13);
            this._urovPr20.State = BEMN.Forms.LedState.Off;
            this._urovPr20.TabIndex = 23;
            // 
            // _urovPr12
            // 
            this._urovPr12.Location = new System.Drawing.Point(115, 76);
            this._urovPr12.Name = "_urovPr12";
            this._urovPr12.Size = new System.Drawing.Size(13, 13);
            this._urovPr12.State = BEMN.Forms.LedState.Off;
            this._urovPr12.TabIndex = 23;
            // 
            // _urovPrLabel20
            // 
            this._urovPrLabel20.AutoSize = true;
            this._urovPrLabel20.Location = new System.Drawing.Point(195, 76);
            this._urovPrLabel20.Name = "_urovPrLabel20";
            this._urovPrLabel20.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel20.TabIndex = 22;
            this._urovPrLabel20.Text = "Пр.20";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(134, 76);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(36, 13);
            this.label154.TabIndex = 22;
            this.label154.Text = "Пр.12";
            // 
            // _urovPr19
            // 
            this._urovPr19.Location = new System.Drawing.Point(176, 57);
            this._urovPr19.Name = "_urovPr19";
            this._urovPr19.Size = new System.Drawing.Size(13, 13);
            this._urovPr19.State = BEMN.Forms.LedState.Off;
            this._urovPr19.TabIndex = 21;
            // 
            // _urovPr11
            // 
            this._urovPr11.Location = new System.Drawing.Point(115, 57);
            this._urovPr11.Name = "_urovPr11";
            this._urovPr11.Size = new System.Drawing.Size(13, 13);
            this._urovPr11.State = BEMN.Forms.LedState.Off;
            this._urovPr11.TabIndex = 21;
            // 
            // _urovPrLabel19
            // 
            this._urovPrLabel19.AutoSize = true;
            this._urovPrLabel19.Location = new System.Drawing.Point(195, 57);
            this._urovPrLabel19.Name = "_urovPrLabel19";
            this._urovPrLabel19.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel19.TabIndex = 20;
            this._urovPrLabel19.Text = "Пр.19";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(134, 57);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(36, 13);
            this.label155.TabIndex = 20;
            this.label155.Text = "Пр.11";
            // 
            // _urovPr18
            // 
            this._urovPr18.Location = new System.Drawing.Point(176, 38);
            this._urovPr18.Name = "_urovPr18";
            this._urovPr18.Size = new System.Drawing.Size(13, 13);
            this._urovPr18.State = BEMN.Forms.LedState.Off;
            this._urovPr18.TabIndex = 19;
            // 
            // _urovPr10
            // 
            this._urovPr10.Location = new System.Drawing.Point(115, 38);
            this._urovPr10.Name = "_urovPr10";
            this._urovPr10.Size = new System.Drawing.Size(13, 13);
            this._urovPr10.State = BEMN.Forms.LedState.Off;
            this._urovPr10.TabIndex = 19;
            // 
            // _urovPrLabel18
            // 
            this._urovPrLabel18.AutoSize = true;
            this._urovPrLabel18.Location = new System.Drawing.Point(195, 38);
            this._urovPrLabel18.Name = "_urovPrLabel18";
            this._urovPrLabel18.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel18.TabIndex = 18;
            this._urovPrLabel18.Text = "Пр.18";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(134, 38);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(36, 13);
            this.label156.TabIndex = 18;
            this.label156.Text = "Пр.10";
            // 
            // _urovPr17
            // 
            this._urovPr17.Location = new System.Drawing.Point(176, 19);
            this._urovPr17.Name = "_urovPr17";
            this._urovPr17.Size = new System.Drawing.Size(13, 13);
            this._urovPr17.State = BEMN.Forms.LedState.Off;
            this._urovPr17.TabIndex = 17;
            // 
            // _urovPrLabel17
            // 
            this._urovPrLabel17.AutoSize = true;
            this._urovPrLabel17.Location = new System.Drawing.Point(195, 19);
            this._urovPrLabel17.Name = "_urovPrLabel17";
            this._urovPrLabel17.Size = new System.Drawing.Size(36, 13);
            this._urovPrLabel17.TabIndex = 16;
            this._urovPrLabel17.Text = "Пр.17";
            // 
            // _urovPr9
            // 
            this._urovPr9.Location = new System.Drawing.Point(115, 19);
            this._urovPr9.Name = "_urovPr9";
            this._urovPr9.Size = new System.Drawing.Size(13, 13);
            this._urovPr9.State = BEMN.Forms.LedState.Off;
            this._urovPr9.TabIndex = 17;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(134, 19);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(30, 13);
            this.label157.TabIndex = 16;
            this.label157.Text = "Пр.9";
            // 
            // _urovPr8
            // 
            this._urovPr8.Location = new System.Drawing.Point(60, 152);
            this._urovPr8.Name = "_urovPr8";
            this._urovPr8.Size = new System.Drawing.Size(13, 13);
            this._urovPr8.State = BEMN.Forms.LedState.Off;
            this._urovPr8.TabIndex = 15;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(79, 152);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(30, 13);
            this.label135.TabIndex = 14;
            this.label135.Text = "Пр.8";
            // 
            // _urovPr7
            // 
            this._urovPr7.Location = new System.Drawing.Point(60, 133);
            this._urovPr7.Name = "_urovPr7";
            this._urovPr7.Size = new System.Drawing.Size(13, 13);
            this._urovPr7.State = BEMN.Forms.LedState.Off;
            this._urovPr7.TabIndex = 13;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(79, 133);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(30, 13);
            this.label143.TabIndex = 12;
            this.label143.Text = "Пр.7";
            // 
            // _urovPr6
            // 
            this._urovPr6.Location = new System.Drawing.Point(60, 114);
            this._urovPr6.Name = "_urovPr6";
            this._urovPr6.Size = new System.Drawing.Size(13, 13);
            this._urovPr6.State = BEMN.Forms.LedState.Off;
            this._urovPr6.TabIndex = 11;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(79, 114);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(30, 13);
            this.label144.TabIndex = 10;
            this.label144.Text = "Пр.6";
            // 
            // _urovPr5
            // 
            this._urovPr5.Location = new System.Drawing.Point(60, 95);
            this._urovPr5.Name = "_urovPr5";
            this._urovPr5.Size = new System.Drawing.Size(13, 13);
            this._urovPr5.State = BEMN.Forms.LedState.Off;
            this._urovPr5.TabIndex = 9;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(79, 95);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(30, 13);
            this.label145.TabIndex = 8;
            this.label145.Text = "Пр.5";
            // 
            // _urovPr4
            // 
            this._urovPr4.Location = new System.Drawing.Point(60, 76);
            this._urovPr4.Name = "_urovPr4";
            this._urovPr4.Size = new System.Drawing.Size(13, 13);
            this._urovPr4.State = BEMN.Forms.LedState.Off;
            this._urovPr4.TabIndex = 7;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(79, 76);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(30, 13);
            this.label146.TabIndex = 6;
            this.label146.Text = "Пр.4";
            // 
            // _urovPr3
            // 
            this._urovPr3.Location = new System.Drawing.Point(60, 57);
            this._urovPr3.Name = "_urovPr3";
            this._urovPr3.Size = new System.Drawing.Size(13, 13);
            this._urovPr3.State = BEMN.Forms.LedState.Off;
            this._urovPr3.TabIndex = 5;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(79, 57);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(30, 13);
            this.label147.TabIndex = 4;
            this.label147.Text = "Пр.3";
            // 
            // _urovPr2
            // 
            this._urovPr2.Location = new System.Drawing.Point(60, 38);
            this._urovPr2.Name = "_urovPr2";
            this._urovPr2.Size = new System.Drawing.Size(13, 13);
            this._urovPr2.State = BEMN.Forms.LedState.Off;
            this._urovPr2.TabIndex = 3;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(79, 38);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(30, 13);
            this.label148.TabIndex = 2;
            this.label148.Text = "Пр.2";
            // 
            // _urovPr1
            // 
            this._urovPr1.Location = new System.Drawing.Point(60, 19);
            this._urovPr1.Name = "_urovPr1";
            this._urovPr1.Size = new System.Drawing.Size(13, 13);
            this._urovPr1.State = BEMN.Forms.LedState.Off;
            this._urovPr1.TabIndex = 1;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(79, 19);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(30, 13);
            this.label149.TabIndex = 0;
            this.label149.Text = "Пр.1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(6, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 166);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 166);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 147);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 147);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 128);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 109);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 109);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 90);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 90);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 71);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 71);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 52);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 33);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 33);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(153, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 191);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 166);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 166);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 147);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 147);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 128);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 128);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 109);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 109);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 90);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 90);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 71);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 52);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 52);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 33);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 166);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 166);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 147);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 147);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 128);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 109);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 90);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 90);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 71);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 71);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 52);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 33);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 33);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox6);
            this._controlSignalsTabPage.Controls.Add(this.groupBox5);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(787, 672);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._reserveGroupButton);
            this.groupBox6.Controls.Add(this._mainGroupButton);
            this.groupBox6.Controls.Add(this.label490);
            this.groupBox6.Controls.Add(this.label491);
            this.groupBox6.Controls.Add(this._reservedGroup);
            this.groupBox6.Controls.Add(this._mainGroup);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox6.Location = new System.Drawing.Point(328, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(193, 83);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Группа уставок";
            // 
            // _reserveGroupButton
            // 
            this._reserveGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._reserveGroupButton.Location = new System.Drawing.Point(101, 56);
            this._reserveGroupButton.Name = "_reserveGroupButton";
            this._reserveGroupButton.Size = new System.Drawing.Size(86, 23);
            this._reserveGroupButton.TabIndex = 30;
            this._reserveGroupButton.Text = "Переключить";
            this._reserveGroupButton.UseVisualStyleBackColor = true;
            this._reserveGroupButton.Click += new System.EventHandler(this._reserveGroupButton_Click);
            // 
            // _mainGroupButton
            // 
            this._mainGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mainGroupButton.Location = new System.Drawing.Point(6, 56);
            this._mainGroupButton.Name = "_mainGroupButton";
            this._mainGroupButton.Size = new System.Drawing.Size(86, 23);
            this._mainGroupButton.TabIndex = 29;
            this._mainGroupButton.Text = "Переключить";
            this._mainGroupButton.UseVisualStyleBackColor = true;
            this._mainGroupButton.Click += new System.EventHandler(this._mainGroupButton_Click);
            // 
            // label490
            // 
            this.label490.AutoSize = true;
            this.label490.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label490.Location = new System.Drawing.Point(118, 39);
            this.label490.Name = "label490";
            this.label490.Size = new System.Drawing.Size(51, 13);
            this.label490.TabIndex = 28;
            this.label490.Text = "Группа 2";
            // 
            // label491
            // 
            this.label491.AutoSize = true;
            this.label491.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label491.Location = new System.Drawing.Point(22, 39);
            this.label491.Name = "label491";
            this.label491.Size = new System.Drawing.Size(51, 13);
            this.label491.TabIndex = 27;
            this.label491.Text = "Группа 1";
            // 
            // _reservedGroup
            // 
            this._reservedGroup.Location = new System.Drawing.Point(136, 19);
            this._reservedGroup.Name = "_reservedGroup";
            this._reservedGroup.Size = new System.Drawing.Size(13, 13);
            this._reservedGroup.State = BEMN.Forms.LedState.Off;
            this._reservedGroup.TabIndex = 24;
            // 
            // _mainGroup
            // 
            this._mainGroup.Location = new System.Drawing.Point(41, 19);
            this._mainGroup.Name = "_mainGroup";
            this._mainGroup.Size = new System.Drawing.Size(14, 13);
            this._mainGroup.State = BEMN.Forms.LedState.Off;
            this._mainGroup.TabIndex = 23;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._commandBtn);
            this.groupBox5.Controls.Add(this._commandComboBox);
            this.groupBox5.Location = new System.Drawing.Point(328, 88);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(193, 57);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Команды";
            // 
            // _commandBtn
            // 
            this._commandBtn.Location = new System.Drawing.Point(112, 17);
            this._commandBtn.Name = "_commandBtn";
            this._commandBtn.Size = new System.Drawing.Size(75, 23);
            this._commandBtn.TabIndex = 1;
            this._commandBtn.Text = "Выполнить";
            this._commandBtn.UseVisualStyleBackColor = true;
            this._commandBtn.Click += new System.EventHandler(this._commandBtn_Click);
            // 
            // _commandComboBox
            // 
            this._commandComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._commandComboBox.FormattingEnabled = true;
            this._commandComboBox.Items.AddRange(new object[] {
            "Команда 1",
            "Команда 2",
            "Команда 3",
            "Команда 4",
            "Команда 5",
            "Команда 6",
            "Команда 7",
            "Команда 8",
            "Команда 9",
            "Команда 10",
            "Команда 11",
            "Команда 12",
            "Команда 13",
            "Команда 14",
            "Команда 15",
            "Команда 16",
            "Команда 17",
            "Команда 18",
            "Команда 19",
            "Команда 20",
            "Команда 21",
            "Команда 22",
            "Команда 23",
            "Команда 24"});
            this._commandComboBox.Location = new System.Drawing.Point(7, 18);
            this._commandComboBox.Name = "_commandComboBox";
            this._commandComboBox.Size = new System.Drawing.Size(99, 21);
            this._commandComboBox.TabIndex = 0;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this.startOsc);
            this.groupBox27.Controls.Add(this._resetTt);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 257);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(6, 112);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 55;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Свободно программируемая логика";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.stopLogic_Click);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.startLogic_Click);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "Состояние задачи логики";
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 88);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 65);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 42);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 19);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // startOsc
            // 
            this.startOsc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startOsc.Location = new System.Drawing.Point(6, 228);
            this.startOsc.Name = "startOsc";
            this.startOsc.Size = new System.Drawing.Size(304, 23);
            this.startOsc.TabIndex = 9;
            this.startOsc.Text = "Пуск осциллографа";
            this.startOsc.UseVisualStyleBackColor = true;
            this.startOsc.Click += new System.EventHandler(this.StartOscClick);
            // 
            // _resetTt
            // 
            this._resetTt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetTt.Location = new System.Drawing.Point(6, 205);
            this._resetTt.Name = "_resetTt";
            this._resetTt.Size = new System.Drawing.Size(304, 23);
            this._resetTt.TabIndex = 9;
            this._resetTt.Text = "Сброс неисправности ТТ";
            this._resetTt.UseVisualStyleBackColor = true;
            this._resetTt.Click += new System.EventHandler(this.ResetTtClick);
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 182);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс индикации";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 37);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 14);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 88);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(119, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Новая неисправность";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 65);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(123, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая осциллограмма";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 42);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 19);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 698);
            this.Controls.Add(this._dataBaseTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MeasuringForm";
            this.Text = "Mr901Measuring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr901MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.Mr901MeasuringForm_Load);
            this._dataBaseTabControl.ResumeLayout(false);
            this._analogTabPage.ResumeLayout(false);
            this.voltageGroup.ResumeLayout(false);
            this.voltageGroup.PerformLayout();
            this.connectionCurrentsGB.ResumeLayout(false);
            this.connectionCurrentsGB.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._rsTriggersGB.ResumeLayout(false);
            this._rsTriggersGB.PerformLayout();
            this._discretTabPage.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.voltageDiscretsGroup.ResumeLayout(false);
            this.voltageDiscretsGroup.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._neisprTNGroup.ResumeLayout(false);
            this._neisprTNGroup.PerformLayout();
            this.urovGroupBox.ResumeLayout(false);
            this.urovGroupBox.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button stopReading;
        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private System.Windows.Forms.TabPage _analogTabPage;
        private System.Windows.Forms.TabPage _discretTabPage;
        private System.Windows.Forms.GroupBox connectionCurrentsGB;
        private System.Windows.Forms.TextBox _i16TextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _i15TextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox _i14TextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox _i13TextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _i12TextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox _i11TextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _i10TextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox _i9TextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _i8TextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _i7TextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _i6TextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _i5TextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox _i4TextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _i3TextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox _i2TextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _i1TextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox _it3TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _id3TextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox _it2TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _id2TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _it1TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _id1TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private BEMN.Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private BEMN.Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private BEMN.Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private BEMN.Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private BEMN.Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private BEMN.Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private BEMN.Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private BEMN.Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private BEMN.Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private BEMN.Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private BEMN.Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private BEMN.Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private BEMN.Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private BEMN.Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox11;
        private BEMN.Forms.LedControl _i32;
        private System.Windows.Forms.Label label95;
        private BEMN.Forms.LedControl _i31;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _i30;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _i29;
        private System.Windows.Forms.Label label98;
        private BEMN.Forms.LedControl _i28;
        private System.Windows.Forms.Label label99;
        private BEMN.Forms.LedControl _i27;
        private System.Windows.Forms.Label label100;
        private BEMN.Forms.LedControl _i26;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _i25;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _i24;
        private System.Windows.Forms.Label label103;
        private BEMN.Forms.LedControl _i23;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.LedControl _i22;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _i21;
        private System.Windows.Forms.Label label106;
        private BEMN.Forms.LedControl _i20;
        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _i19;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _i18;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _i17;
        private System.Windows.Forms.Label label110;
        private BEMN.Forms.LedControl _i16;
        private System.Windows.Forms.Label label79;
        private BEMN.Forms.LedControl _i15;
        private System.Windows.Forms.Label label80;
        private BEMN.Forms.LedControl _i14;
        private System.Windows.Forms.Label label81;
        private BEMN.Forms.LedControl _i13;
        private System.Windows.Forms.Label label82;
        private BEMN.Forms.LedControl _i12;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _i11;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _i10;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _i9;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _i8;
        private System.Windows.Forms.Label label87;
        private BEMN.Forms.LedControl _i7;
        private System.Windows.Forms.Label label88;
        private BEMN.Forms.LedControl _i6;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _i5;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _i4;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _i3;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _i2;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _i1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private BEMN.Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox urovGroupBox;
        private BEMN.Forms.LedControl _urovPO;
        private System.Windows.Forms.Label label158;
        private BEMN.Forms.LedControl _urovSH2;
        private System.Windows.Forms.Label label159;
        private BEMN.Forms.LedControl _urovSH1;
        private System.Windows.Forms.Label label160;
        private BEMN.Forms.LedControl _urovPr16;
        private System.Windows.Forms.Label label150;
        private BEMN.Forms.LedControl _urovPr15;
        private System.Windows.Forms.Label label151;
        private BEMN.Forms.LedControl _urovPr14;
        private System.Windows.Forms.Label label152;
        private BEMN.Forms.LedControl _urovPr13;
        private System.Windows.Forms.Label label153;
        private BEMN.Forms.LedControl _urovPr12;
        private System.Windows.Forms.Label label154;
        private BEMN.Forms.LedControl _urovPr11;
        private System.Windows.Forms.Label label155;
        private BEMN.Forms.LedControl _urovPr10;
        private System.Windows.Forms.Label label156;
        private BEMN.Forms.LedControl _urovPr9;
        private System.Windows.Forms.Label label157;
        private BEMN.Forms.LedControl _urovPr8;
        private System.Windows.Forms.Label label135;
        private BEMN.Forms.LedControl _urovPr7;
        private System.Windows.Forms.Label label143;
        private BEMN.Forms.LedControl _urovPr6;
        private System.Windows.Forms.Label label144;
        private BEMN.Forms.LedControl _urovPr5;
        private System.Windows.Forms.Label label145;
        private BEMN.Forms.LedControl _urovPr4;
        private System.Windows.Forms.Label label146;
        private BEMN.Forms.LedControl _urovPr3;
        private System.Windows.Forms.Label label147;
        private BEMN.Forms.LedControl _urovPr2;
        private System.Windows.Forms.Label label148;
        private BEMN.Forms.LedControl _urovPr1;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.GroupBox groupBox19;
        private BEMN.Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private BEMN.Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private BEMN.Forms.LedControl _faultModule5;
        private System.Windows.Forms.Label label194;
        private BEMN.Forms.LedControl _faultModule4;
        private System.Windows.Forms.Label label195;
        private BEMN.Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private BEMN.Forms.LedControl _faultModule2;
        private System.Windows.Forms.Label label197;
        private BEMN.Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private BEMN.Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private BEMN.Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private BEMN.Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private BEMN.Forms.LedControl _faultLogic;
        private System.Windows.Forms.Label label203;
        private BEMN.Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private BEMN.Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private BEMN.Forms.LedControl _i32Io;
        private BEMN.Forms.LedControl _i31Io;
        private BEMN.Forms.LedControl _i30Io;
        private BEMN.Forms.LedControl _i29Io;
        private BEMN.Forms.LedControl _i28Io;
        private BEMN.Forms.LedControl _i27Io;
        private BEMN.Forms.LedControl _i26Io;
        private BEMN.Forms.LedControl _i25Io;
        private BEMN.Forms.LedControl _i24Io;
        private BEMN.Forms.LedControl _i23Io;
        private BEMN.Forms.LedControl _i22Io;
        private BEMN.Forms.LedControl _i21Io;
        private BEMN.Forms.LedControl _i20Io;
        private BEMN.Forms.LedControl _i19Io;
        private BEMN.Forms.LedControl _i18Io;
        private BEMN.Forms.LedControl _i17Io;
        private BEMN.Forms.LedControl _i16Io;
        private BEMN.Forms.LedControl _i15Io;
        private BEMN.Forms.LedControl _i14Io;
        private BEMN.Forms.LedControl _i13Io;
        private BEMN.Forms.LedControl _i12Io;
        private BEMN.Forms.LedControl _i11Io;
        private BEMN.Forms.LedControl _i10Io;
        private BEMN.Forms.LedControl _i9Io;
        private BEMN.Forms.LedControl _i8Io;
        private BEMN.Forms.LedControl _i7Io;
        private BEMN.Forms.LedControl _i6Io;
        private BEMN.Forms.LedControl _i5Io;
        private BEMN.Forms.LedControl _i4Io;
        private BEMN.Forms.LedControl _i3Io;
        private BEMN.Forms.LedControl _i2Io;
        private BEMN.Forms.LedControl _i1Io;
        private System.Windows.Forms.GroupBox groupBox21;
        private BEMN.Forms.LedControl _ssl32;
        private System.Windows.Forms.Label label237;
        private BEMN.Forms.LedControl _ssl31;
        private System.Windows.Forms.Label label238;
        private BEMN.Forms.LedControl _ssl30;
        private System.Windows.Forms.Label label239;
        private BEMN.Forms.LedControl _ssl29;
        private System.Windows.Forms.Label label240;
        private BEMN.Forms.LedControl _ssl28;
        private System.Windows.Forms.Label label241;
        private BEMN.Forms.LedControl _ssl27;
        private System.Windows.Forms.Label label242;
        private BEMN.Forms.LedControl _ssl26;
        private System.Windows.Forms.Label label243;
        private BEMN.Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private BEMN.Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private BEMN.Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private BEMN.Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private BEMN.Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private BEMN.Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private BEMN.Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private BEMN.Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private BEMN.Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private BEMN.Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private BEMN.Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private BEMN.Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private BEMN.Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private BEMN.Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private BEMN.Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private BEMN.Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private BEMN.Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private BEMN.Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private BEMN.Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private BEMN.Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private BEMN.Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private BEMN.Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private BEMN.Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private BEMN.Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private BEMN.Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label269;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label230;
        private BEMN.Forms.LedControl _faultUrov;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.GroupBox groupBox20;
        private BEMN.Forms.LedControl _reservedGroupOfSetpoints;
        private System.Windows.Forms.Label label208;
        private BEMN.Forms.LedControl _mainGroupOfSetpoints;
        private System.Windows.Forms.Label label209;
        private BEMN.Forms.LedControl _fault;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label label223;
        private BEMN.Forms.LedControl _difDefenceActualIoPo;
        private BEMN.Forms.LedControl _difDefenceActualIoSh2;
        private BEMN.Forms.LedControl _difDefenceActualIoSh1;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label label218;
        private BEMN.Forms.LedControl _difDefenceActualChtoPo;
        private BEMN.Forms.LedControl _difDefenceActualChtoSh2;
        private BEMN.Forms.LedControl _difDefenceActualChtoSh1;
        private BEMN.Forms.LedControl _difDefenceActualSrabPo;
        private BEMN.Forms.LedControl _difDefenceActualSrabSh2;
        private BEMN.Forms.LedControl _difDefenceActualSrabSh1;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private BEMN.Forms.LedControl _difDefenceInstantaneousChtoPo;
        private BEMN.Forms.LedControl _difDefenceInstantaneousChtoSh2;
        private BEMN.Forms.LedControl _difDefenceInstantaneousChtoSh1;
        private BEMN.Forms.LedControl _difDefenceInstantaneousSrabPo;
        private BEMN.Forms.LedControl _difDefenceInstantaneousSrabSh2;
        private BEMN.Forms.LedControl _difDefenceInstantaneousSrabSh1;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox27;
        private BEMN.Forms.LedControl _availabilityFaultSystemJournal;
        private BEMN.Forms.LedControl _newRecordOscJournal;
        private BEMN.Forms.LedControl _newRecordAlarmJournal;
        private BEMN.Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.Button _resetTt;
        private System.Windows.Forms.Button startOsc;
        private BEMN.Forms.LedControl _faultTt2;
        private System.Windows.Forms.Label faultTt2Label;
        private BEMN.Forms.LedControl _faultTt1;
        private System.Windows.Forms.Label faultTt1Label;
        private BEMN.Forms.LedControl _faultTt3;
        private System.Windows.Forms.Label faultTt3Label;
        private BEMN.Forms.LedControl _faultMeasuring;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.GroupBox groupBox40;
        private BEMN.Forms.LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.TextBox _i24TextBox;
        private System.Windows.Forms.Label _i24Label;
        private System.Windows.Forms.TextBox _i23TextBox;
        private System.Windows.Forms.Label _i23Label;
        private System.Windows.Forms.TextBox _i22TextBox;
        private System.Windows.Forms.Label _i22Label;
        private System.Windows.Forms.TextBox _i21TextBox;
        private System.Windows.Forms.Label _i21Label;
        private System.Windows.Forms.TextBox _i20TextBox;
        private System.Windows.Forms.Label _i20Label;
        private System.Windows.Forms.TextBox _i19TextBox;
        private System.Windows.Forms.Label _i19Label;
        private System.Windows.Forms.TextBox _i18TextBox;
        private System.Windows.Forms.Label _i18Label;
        private System.Windows.Forms.TextBox _i17TextBox;
        private System.Windows.Forms.Label _i17Label;
        private Forms.LedControl _vz24;
        private System.Windows.Forms.Label _vz24label;
        private Forms.LedControl _vz23;
        private System.Windows.Forms.Label _vz23label;
        private Forms.LedControl _vz22;
        private System.Windows.Forms.Label _vz22label;
        private Forms.LedControl _vz21;
        private System.Windows.Forms.Label _vz21label;
        private Forms.LedControl _vz20;
        private System.Windows.Forms.Label label293;
        private Forms.LedControl _vz19;
        private System.Windows.Forms.Label label292;
        private Forms.LedControl _vz18;
        private System.Windows.Forms.Label label291;
        private Forms.LedControl _vz17;
        private System.Windows.Forms.Label label290;
        private Forms.LedControl _urovPr24;
        private System.Windows.Forms.Label _urovPrLabel24;
        private Forms.LedControl _urovPr23;
        private System.Windows.Forms.Label _urovPrLabel23;
        private Forms.LedControl _urovPr22;
        private System.Windows.Forms.Label _urovPrLabel22;
        private Forms.LedControl _urovPr21;
        private System.Windows.Forms.Label _urovPrLabel21;
        private Forms.LedControl _urovPr20;
        private System.Windows.Forms.Label _urovPrLabel20;
        private Forms.LedControl _urovPr19;
        private System.Windows.Forms.Label _urovPrLabel19;
        private Forms.LedControl _urovPr18;
        private System.Windows.Forms.Label _urovPrLabel18;
        private Forms.LedControl _urovPr17;
        private System.Windows.Forms.Label _urovPrLabel17;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox relayGroupBox;
        private System.Windows.Forms.GroupBox discretsGroupBox;
        private Forms.LedControl _signalization;
        private System.Windows.Forms.Label label24;
        private Forms.LedControl _alarm;
        private System.Windows.Forms.Label label23;
        private Forms.LedControl _ssl48;
        private Forms.LedControl _ssl40;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label32;
        private Forms.LedControl _ssl47;
        private Forms.LedControl _ssl39;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label31;
        private Forms.LedControl _ssl46;
        private Forms.LedControl _ssl38;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label30;
        private Forms.LedControl _ssl45;
        private Forms.LedControl _ssl37;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label29;
        private Forms.LedControl _ssl44;
        private Forms.LedControl _ssl36;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label28;
        private Forms.LedControl _ssl43;
        private Forms.LedControl _ssl35;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label27;
        private Forms.LedControl _ssl42;
        private Forms.LedControl _ssl34;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label26;
        private Forms.LedControl _ssl41;
        private System.Windows.Forms.Label label33;
        private Forms.LedControl _ssl33;
        private System.Windows.Forms.Label label25;
        private Forms.LedControl _faultModule6;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox voltageGroup;
        private System.Windows.Forms.TextBox _u30;
        private System.Windows.Forms.TextBox _uca;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.TextBox _u2;
        private System.Windows.Forms.TextBox _ubc;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox _u1;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox _uab;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox _un;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox _uc;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.TextBox _ub;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.TextBox _ua;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.GroupBox voltageDiscretsGroup;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private Forms.LedControl _u2lIO;
        private Forms.LedControl _u1lIO;
        private Forms.LedControl _u2l;
        private System.Windows.Forms.Label label274;
        private Forms.LedControl _u2bIO;
        private Forms.LedControl _u1l;
        private System.Windows.Forms.Label label275;
        private Forms.LedControl _u1bIO;
        private Forms.LedControl _u2b;
        private System.Windows.Forms.Label label276;
        private Forms.LedControl _u1b;
        private System.Windows.Forms.Label label277;
        private Forms.LedControl _neisprTN;
        private System.Windows.Forms.Label _neisprTNLabel;
        private System.Windows.Forms.GroupBox _neisprTNGroup;
        private Forms.LedControl _neisprTNUabc;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label42;
        private Forms.LedControl _neisprTNns;
        private System.Windows.Forms.Label label43;
        private Forms.LedControl _neisprTN3U0;
        private System.Windows.Forms.Label _neisprTN3U0Label;
        private Forms.LedControl _neisprTNUn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _commandBtn;
        private System.Windows.Forms.ComboBox _commandComboBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button _reserveGroupButton;
        private System.Windows.Forms.Button _mainGroupButton;
        private System.Windows.Forms.Label label490;
        private System.Windows.Forms.Label label491;
        private Forms.LedControl _reservedGroup;
        private Forms.LedControl _mainGroup;
        private System.Windows.Forms.GroupBox _rsTriggersGB;
        private Forms.LedControl _rst8;
        private System.Windows.Forms.Label label44;
        private Forms.LedControl _rst7;
        private System.Windows.Forms.Label label46;
        private Forms.LedControl _rst1;
        private Forms.LedControl _rst6;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label140;
        private Forms.LedControl _rst5;
        private Forms.LedControl _rst2;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label139;
        private Forms.LedControl _rst4;
        private Forms.LedControl _rst16;
        private Forms.LedControl _rst3;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private Forms.LedControl _rst15;
        private Forms.LedControl _rst9;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private Forms.LedControl _rst14;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private Forms.LedControl _rst10;
        private Forms.LedControl _rst13;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private Forms.LedControl _rst11;
        private Forms.LedControl _rst12;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.GroupBox groupBox7;
        private Forms.Diod diod12;
        private Forms.Diod diod11;
        private Forms.Diod diod10;
        private Forms.Diod diod9;
        private Forms.Diod diod8;
        private Forms.Diod diod7;
        private Forms.Diod diod6;
        private Forms.Diod diod5;
        private Forms.Diod diod4;
        private Forms.Diod diod3;
        private Forms.Diod diod2;
        private Forms.Diod diod1;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.GroupBox _virtualReleGroupBox;
    }
}