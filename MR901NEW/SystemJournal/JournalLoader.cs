﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR901NEW.SystemJournal.Structures;

namespace BEMN.MR901NEW.SystemJournal
{
    class JournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<FullSystemJournalStruct1024> _full1024;
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<FullSystemJournalStruct64> _full64;
        /// <summary>
        /// Структура записи номер записи журнала
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        /// <summary>
        /// Список структур "Запись журнала системы"
        /// </summary>
        private readonly List<SystemJournalStruct> _journalRecords;
        /// <summary>
        /// Текущий номер записи журнала
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="full1024">Объект записи журнала при размере записи в 1024 слова</param>
        /// /// <param name="full64">Объект записи журнала при размере записи в 64 слова</param>
        /// <param name="saveIndex">Объект сохранения номера записи журнала</param>
        public JournalLoader(MemoryEntity<FullSystemJournalStruct1024> full1024,MemoryEntity<FullSystemJournalStruct64> full64, MemoryEntity<OneWordStruct> saveIndex)
        {
            this._journalRecords = new List<SystemJournalStruct>();
            //Записи журнала
            if (full1024 != null)
            {
                this._full1024 = full1024;
                this._full1024.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
                this._full1024.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);
            }
            if (full64 != null)
            {
                this._full64 = full64;
                this._full64.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
                this._full64.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);
            }
            //запись номера ЖО
            this._saveIndex = saveIndex;
            this._saveIndex.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.StartReadJournalRecords);
            this._saveIndex.AllWriteFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);
        }
        #endregion [Ctor's]


        #region [Properties]
        public List<string> MessagesList { get; set; }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<SystemJournalStruct> JournalRecords
        {
            get { return _journalRecords; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        private void StartReadJournalRecords()
        {
            if (this._full1024 != null)
            {
                this._full1024.LoadStruct();
            }
            else if (this._full64 != null)
            {
                this._full64.LoadStruct();
            }
        }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (this._full1024 != null)
            {
                this._recordNumber += this._full1024.Value.Records.Length;
                foreach (SystemJournalStruct record in this._full1024.Value.Records)
                {
                    record.MessagesList = this.MessagesList;
                }
                List<SystemJournalStruct> currentRecords = new List<SystemJournalStruct>(this._full1024.Value.Records);
                this._journalRecords.AddRange(currentRecords);
                if (this._journalRecords.Count != 0 && currentRecords.Count != 0)
                {
                    this.SaveIdex();
                }
                else
                {
                    if (this.AllJournalReadOk == null) return;
                    this.AllJournalReadOk.Invoke();
                }
                return;
            }
            if (this._full64 != null)
            {
                this._recordNumber += this._full64.Value.Records.Length;
                foreach (SystemJournalStruct record in this._full64.Value.Records)
                {
                    record.MessagesList = this.MessagesList;
                }
                this._journalRecords.AddRange(this._full64.Value.Records);
                if (this._journalRecords.Count == 0 || this._journalRecords.Any(j => j.IsEmpty))
                {
                    this.SaveIdex();
                }
                else
                {
                    if (this.AllJournalReadOk == null) return;
                    this.AllJournalReadOk.Invoke();
                }
            }
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartRead()
        {
            this._recordNumber = 0;
            this._journalRecords.Clear();
            this.SaveIdex();
        }

        public void ClearEvents()
        {
            this._full1024.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._full1024.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            this._full64.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._full64.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            this._saveIndex.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this.StartReadJournalRecords);
            this._saveIndex.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        #endregion [Public members]
        

        private void SaveIdex()
        {
            this._saveIndex.Value.Word =  (ushort)this._recordNumber;
            this._saveIndex.SaveStruct6();
        }
    }
}
