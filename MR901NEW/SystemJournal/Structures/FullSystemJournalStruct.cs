﻿using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR901NEW.SystemJournal.Structures
{
    public class FullSystemJournalStruct1024 : StructBase
    {
        [Layout(0, Count = 113)] private SystemJournalStruct[] _records;

        public SystemJournalStruct[] Records
        {
            get { return this._records.Where(r => !r.IsEmpty).ToArray(); }
        }
    }

    public class FullSystemJournalStruct64 : StructBase
    {
        [Layout(0, Count = 7)] private SystemJournalStruct[] _records;
        public SystemJournalStruct[] Records
        {
            get { return this._records.Where(r => !r.IsEmpty).ToArray(); }
        }
    }
}
