﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR901NEW.Emulation.Structures
{
    [Serializable]
    public class InputAnalogDataI : StructBase
    {
        private InputAnalogData _data = new InputAnalogData();

        public void SetData(InputAnalogData data)
        {
            _data = data;
        }

        public InputAnalogData GetData()
        {
            return _data;
        }

        /// <summary>
        /// Амлитуда
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Амлитуда")]
        public double Amplituda
        {
            get { return ValuesConverterCommon.GetAplituda(this._data.Amplituda); }
            set { this._data.Amplituda = ValuesConverterCommon.SetAplituda(value); }
        }

        /// <summary>
        /// Фаза
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фаза")]
        public double Faza
        {
            get { return _data.Faza; }
            set { _data.Faza = value; }
        }

        /// <summary>
        /// Частота
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Частота")]
        public double F
        {
            get { return _data.F; }
            set { _data.F = value; }
        }
    }
}
