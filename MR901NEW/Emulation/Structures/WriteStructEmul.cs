﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR901NEW.Emulation.Structures
{
    [Serializable]
    public class WriteStructEmul : StructBase
    {
        #region [Private Fiels]
        
        public const int COUNT = 24;

        [Layout(0)] private TimeAndSignalStruct _timeAndSignal;
        [Layout(1, Count = COUNT)] private InputAnalogData[] _inputAnalog; // входные аналогивые данные
        [Layout(2)] private DiscretsSignals _diskretInput; // дискретные данные (64 сигнала)

        #endregion


        public TimeAndSignalStruct TimeSignal
        {
            get { return this._timeAndSignal; }
            set { this._timeAndSignal = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        //[XmlArray("Аналоговые_сигналы")]
        //[XmlArrayItem(Type = typeof(InputAnalogData))]
        [XmlElement(ElementName = "Аналоговые_сигналы")]
        public InputAnalogData[] AllAnalogData
        {
            get { return this._inputAnalog; }
            set { this._inputAnalog = value; }
        }
        
        /// <summary>
        /// Список дискретных синалов
        /// </summary>
        [XmlElement(ElementName = "Список дискретных синалов")]
        public DiscretsSignals DiscretInputs
        {
            get { return this._diskretInput; }
            set { this._diskretInput = value; }
        }
    }
}


