﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901NEW.Emulation.Structures;
using BEMN.MR901NEW.Properties;

namespace BEMN.MR901NEW.Emulation
{
    public partial class EmulationForm : Form, IFormView
    {
        private const string READ_FAIL = "Не удалось прочитать эмуляцию.";
        private const string WRITE_FAIL = "Не удалось записать эмуляцию.";
        private const string INVALID_PORT = "Порт недоступен.";
        
        private int _heightGpUTime;
        private int _heightGpAnalog;
        private int _heightGpDisTime;
        private int _heightGpItime;

        #region [Private fields]

        private readonly MR901New _device;

        private bool IsStart;
        private bool IsSelect;
        private bool IsWrite;
        private bool IsChange;
        private bool IsKvit;
        private bool IsExit;
        private bool IsBlock;
        private double _incriment;

        private Size resolution = Screen.PrimaryScreen.Bounds.Size;
        private AnalogDataSet m_dataSet;
        private List<AnalogCtrI> _currentsData;
        private List<AnalogCtrU> _voltagesData;

        private List<Button> _listButtons;
        private MaskedTextBox _currentAdduction;
        private MemoryEntity<WriteStructEmul> _memoryEntityStructEmulation;
        private MemoryEntity<WriteStructEmul> _memoryEntityNullStructEmulation;
        private MemoryEntity<ReadStructEmul> _readMemoryEntity;
        
        private WriteStructEmul _currentWriteStruct;
        private WriteStructEmul _tempWriteStruct;

        private NewStructValidator<TimeAndSignalStruct> _validatorTimeAndSignal;
        private NewStructValidator<DiscretsSignals> _discrets;
        
        private List<CheckBox> _listStartControls;
        private List<CheckBox> _listDiscretsControls;
        private List<MaskedTextBox> _listAllControls;
        private List<MaskedTextBox> _listAllCornerControls;
        #endregion

        public EmulationForm()
        {
            this.InitializeComponent();
        }

        public EmulationForm(MR901New device)
        {
            try
            {
                this.InitializeComponent();
                this._device = device;

                Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);

                this._memoryEntityStructEmulation = this._device.WriteStructEmulation;
                this._memoryEntityNullStructEmulation = this._device.WriteStructEmulationNull;

                this._readMemoryEntity = this._device.ReadStructEmulation;
                this._readMemoryEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadOk);
                this._readMemoryEntity.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._status.Text = "";
                    this.EmulationReadFail();
                });

                this._memoryEntityStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationReadOk);
                this._memoryEntityStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationWriteOk);
                this._memoryEntityStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationWriteFail();
                });
                this._memoryEntityStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationReadFail();
                });

                this._memoryEntityNullStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.NullEmulationReadOk);
                this._memoryEntityNullStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.EmulationWriteOk);
                this._memoryEntityNullStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityNullStructEmulation.RemoveStructQueries();
                    this.EmulationWriteFail();
                    this._kvit.Enabled = true;
                });
                this._memoryEntityNullStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._memoryEntityStructEmulation.RemoveStructQueries();
                    this.EmulationReadFail();
                    this._kvit.Enabled = true;
                });
                
                this._currentWriteStruct = new WriteStructEmul();

                if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
                {
                    _device.Info.DeviceConfiguration = _device.DevicePlant;
                }

                this.Init();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            
        }

        private void Init()
        {
            this.kvitTooltip.SetToolTip(this._kvit, "Применяется для сброса углов при изменении частоты");

            this._listDiscretsControls = new List<CheckBox>();

            foreach (var control in this._listDiscretsControls)
            {
                control.CheckedChanged += this._discret_CheckedChanged;
            }

            this._listAllCornerControls = new List<MaskedTextBox>();
            this._listAllControls = new List<MaskedTextBox>();
            this._listButtons = new List<Button>();
            
            int countI = _device.CurrentsCount, countD = _device.DiskretsCount, countU = _device.VoltagesCount;

            this.m_dataSet = new AnalogDataSet(countI, countU);

            this.InitAnalogControls(countI, countU);
            this.InitAnalogCheckBoxes(countI, countU, countD);
            this.InitCheckBoxDiskrets(countD);

            foreach (CheckBox discretsControl in _listDiscretsControls)
            {
                discretsControl.CheckedChanged += _discret_CheckedChanged;
            }

            foreach (var control in this._listAllControls)
            {
                control.MouseDown += new MouseEventHandler(this.MouseRightClick);
            }

            foreach (var control in this._listStartControls)
            {
                control.Enabled = false;
            }

            this._startTime.Enabled = false;

            ControlInfoCheck[] cntrDiskrValids = new ControlInfoCheck[_listDiscretsControls.Count];
            for (int i = 0; i < _listDiscretsControls.Count; i++)
            {
                cntrDiskrValids[i] = new ControlInfoCheck(_listDiscretsControls[i]);
            }

            this._discrets = new NewStructValidator<DiscretsSignals>(this.toolTip1, cntrDiskrValids);

            this._validatorTimeAndSignal = new NewStructValidator<TimeAndSignalStruct>
            (
                this.toolTip1,
                new ControlInfoCheck(this._startTime),
                new ControlInfoCombo(this._signal, Strings.RelaySignals)
            );

            this.InitButton();
            this.InitStartControls();

            this.GotFocusInControl();
            //this.FromXml();

        }

        private void GotFocusInControl()
        {
            foreach (var control in this._listAllControls)
            {
                control.GotFocus += this.OnGotFocused;
                control.LostFocus += this.LostFocus;
                control.KeyDown += this.ControlKeyDown;
            }
        }

        private void LostFocus(object sender, EventArgs e)
        {
            if (!this._writeEmulButton.Checked || !(sender is MaskedTextBox)) return;

            this._currentAdduction = (MaskedTextBox)sender;

            if (this.IsChange)
            {
                var isstart = this._currentAdduction.Tag as CheckBox;
                this.IsChange = false;
                this.GetAnalogData();
                this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                this._currentWriteStruct.DiscretInputs = _discrets.Get();
                if (this.IsSelect)
                {
                    if (isstart.Checked || this.IsStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }
                }
            }

            this._currentWriteStruct.TimeSignal.IsRestart = false;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._currentAdduction.TextChanged -= this.OnTextChanged;
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            this.IsChange = true;
        }

        private void ControlKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MaskedTextBox mt = sender as MaskedTextBox;
                if (mt.Text == String.Empty)
                {
                    mt.Text = "0";
                }
                int index = this._listAllControls.IndexOf(mt) + 1;
                if (index == this._listAllControls.Count)
                {
                    index = 0;
                }
                if (!this._listAllControls[index].Visible)
                {
                    while (!this._listAllControls[index].Visible)
                    {
                        index++;
                    }
                }


                this._listAllControls[index].Focus();
            }
        }

        private void OnGotFocused(object sender, EventArgs e)
        {
            MaskedTextBox text = sender as MaskedTextBox;
            text?.SelectAll();

            if (this._writeEmulButton.Checked)
            {
                this._currentAdduction = sender as MaskedTextBox;
                this._currentAdduction.TextChanged += this.OnTextChanged;
            }
        }

        /// <summary>
        /// Связывает кнопки с maskedtextbox
        /// </summary>
        private void InitButton()
        {
            int index = 0;
            for (int i = 0; i < this._listButtons.Count;)
            {
                this._listButtons[i].Tag = this._listAllControls[index];
                this._listButtons[i + 1].Tag = this._listAllControls[index];
                index++;
                i = i + 2;
            }

        }

        /// <summary>
        /// Связывает checkbox с m maskedtextbox
        /// </summary>
        private void InitStartControls()
        {
            int count = 0;
            for (int i = 0; i < this._listAllControls.Count; i++)
            {

                this._listAllControls[i].Tag = this._listStartControls[i];
                count = i;
            }
            count++;
            for (int i = 0; i < this._listDiscretsControls.Count; i++)
            {
                this._listDiscretsControls[i].Tag = this._listStartControls[count];
                count++;
            }
        }


        private void InitCheckBoxDiskrets(int countD)
        {
            switch (countD)
            {
                case 64:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 21)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 22 & i < 44)
                        {
                            c1.Location = new Point(50, 3 + 16 * (i - 22));
                        }

                        if (i >= 44)
                        {
                            if (i > 63) c1.Text = "K" + (i - 63);
                            c1.Location = new Point(97, 3 + 16 * (i - 44));
                        }
                        _listDiscretsControls.Add(c1);
                        panelDiscrets.Controls.Add(c1);
                    }
                    for (int i = 0; i < _currentsData.Count; i++)
                    {
                        if (i > 11)
                        {
                            groupBoxInputDiskrets.Size = new Size(152, 268 + 27 * (i - 11));
                        }
                    }
                    break;
                case 40:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 40)
                        {
                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }

                        if (i >= 40)
                        {
                            if (i > 39) c1.Text = "K" + (i - 39);

                            c1.Location = new Point(97, 3 + 16 * (i - 40));
                        }

                        _listDiscretsControls.Add(c1);
                        panelDiscrets.Controls.Add(c1);
                    }
                    break;
                case 32:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 35)
                        {
                            if (i > 31) c1.Text = "K" + (i - 31);

                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }
                        _listDiscretsControls.Add(c1);
                        panelDiscrets.Controls.Add(c1);
                    }
                    break;
                case 24:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 27)
                        {
                            if (i > 23) c1.Text = "K" + (i - 23);

                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }
                        _listDiscretsControls.Add(c1);
                        panelDiscrets.Controls.Add(c1);
                    }
                    break;
                default:
                    for (int i = 0; i < countD; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 40)
                        {
                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }

                        if (i >= 40)
                        {
                            if (i > 55)
                            {
                                c1.Text = "K" + (i - 55);
                            }
                            c1.Location = new Point(97, 3 + 16 * (i - 40));
                        }
                        _listDiscretsControls.Add(c1);
                        panelDiscrets.Controls.Add(c1);
                    }
                    break;
            }
        }

        private void InitAnalogCheckBoxes(int countI, int countU, int countD)
        {
            this._listStartControls = new List<CheckBox>();

            for (int i = 0; i < countI; i++)
            {
                CheckBox c1 = new CheckBox();
                panelICheck.Controls.Add(c1);
                c1.Text = "I" + (i + 1);
                c1.AutoSize = true;
                c1.Tag = 0;
                c1.Location = new Point(11, 0 + 16 * i);
                _listStartControls.Add(c1);

                CheckBox c2 = new CheckBox();
                panelICheck.Controls.Add(c2);
                c2.Text = "fi" + (i + 1);
                c2.AutoSize = true;
                c2.Tag = 1;
                c2.Location = new Point(52, 0 + 16 * i);
                _listStartControls.Add(c2);

                CheckBox c3 = new CheckBox();
                c3.Text = "Fi" + (i + 1);
                c3.AutoSize = true;
                c3.Tag = 2;
                c3.Location = new Point(93, 0 + 16 * i);
                _listStartControls.Add(c3);
                panelICheck.Controls.Add(c3);

            }

            for (int i = 0; i < countU; i++)
            {
                CheckBox c1 = new CheckBox();
                panelUCheck.Controls.Add(c1);
                c1.Text = "U" + (i + 1);
                c1.AutoSize = true;
                c1.Tag = 0;
                c1.Location = new Point(11, 0 + 16 * i);
                _listStartControls.Add(c1);

                CheckBox c2 = new CheckBox();
                panelUCheck.Controls.Add(c2);
                c2.Text = "fi" + (i + 1);
                c2.AutoSize = true;
                c2.Tag = 1;
                c2.Location = new Point(52, 0 + 16 * i);
                _listStartControls.Add(c2);

                CheckBox c3 = new CheckBox();
                panelUCheck.Controls.Add(c3);
                c3.Text = "Fi" + (i + 1);
                c3.AutoSize = true;
                c3.Tag = 2;
                c3.Location = new Point(93, 0 + 16 * i);
                _listStartControls.Add(c3);
            }

            List<CheckBox> listStartControls = new List<CheckBox>();

            switch (countD)
            {
                case 64:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 21)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 22 & i < 44)
                        {
                            c1.Location = new Point(50, 3 + 16 * (i - 22));
                        }

                        if (i >= 44)
                        {
                            if (i > 63)
                            {
                                c1.Text = "K" + (i - 63);
                            }
                            c1.Location = new Point(97, 3 + 16 * (i - 44));
                        }

                        listStartControls.Add(c1);
                        panelDiskretsTime.Controls.Add(c1);
                    }

                    groupBoxDiskrets.Size = new Size(149, 380);
                    break;
                case 40:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 40)
                        {
                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }

                        if (i >= 40)
                        {
                            if (i > 39) c1.Text = "K" + (i - 39);
                            c1.Location = new Point(97, 3 + 16 * (i - 40));
                        }

                        listStartControls.Add(c1);
                        panelDiskretsTime.Controls.Add(c1);
                    }
                    break;
                case 32:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 35)
                        {
                            if (i > 31) c1.Text = "K" + (i - 31);

                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }
                        listStartControls.Add(c1);
                        panelDiskretsTime.Controls.Add(c1);
                    }
                    break;
                case 24:
                    for (int i = 0; i < countD + 2; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 27)
                        {
                            if (i > 23) c1.Text = "K" + (i - 23);

                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }
                        listStartControls.Add(c1);
                        panelDiskretsTime.Controls.Add(c1);
                    }
                    break;
                default:
                    for (int i = 0; i < countD; i++)
                    {
                        CheckBox c1 = new CheckBox();
                        c1.Text = "Д" + (i + 1);
                        c1.AutoSize = true;
                        c1.Tag = 0;
                        if (i <= 19)
                        {
                            c1.Location = new Point(3, 3 + 16 * i);
                        }

                        if (i >= 20 & i < 40)
                        {
                            c1.Location = new Point(50, 3 + 16 * (i - 20));
                        }

                        if (i >= 40)
                        {
                            if (i > 55)
                            {
                                c1.Text = "K" + (i - 55);
                            }
                            c1.Location = new Point(97, 3 + 16 * (i - 40));
                        }

                        listStartControls.Add(c1);
                        panelDiskretsTime.Controls.Add(c1);
                    }
                    break;
            }

            _listStartControls.AddRange(listStartControls);

            SetSizeAndLocationTime();
        }

        private void InitAnalogControls(int countI, int countU)
        {
            this._currentsData = new List<AnalogCtrI>();
            for (int i = 0; i < countI; i++)
            {
                AnalogCtrI ctrl = new AnalogCtrI("I" + (i + 1), "fi" + (i + 1), "Fi" + (i + 1));

                _listAllControls.AddRange(ctrl.TextBoxes);
                foreach (MaskedTextBox textBox in ctrl.TextBoxes)
                {
                    textBox.LostFocus += LostFocus;
                }

                for (int j = 0; j < ctrl.UpButtons.Length; j++)
                {
                    ctrl.UpButtons[j].Click += Up_Click;
                    _listButtons.Add(ctrl.UpButtons[j]);
                    ctrl.DownButtons[j].Click += Down_Click;
                    _listButtons.Add(ctrl.DownButtons[j]);
                }
                _currentsData.Add(ctrl);
            }

            this._voltagesData = new List<AnalogCtrU>();
            for (int i = 0; i < countU; i++)
            {
                AnalogCtrU ctrlU = new AnalogCtrU("U" + (i + 1), "fu" + (i + 1), "Fu" + (i + 1));

                _listAllControls.AddRange(ctrlU.TextBoxes);

                foreach (MaskedTextBox textBox in ctrlU.TextBoxes)
                {
                    textBox.LostFocus += LostFocus;
                }

                for (int j = 0; j < ctrlU.UpButtons.Length; j++)
                {
                    ctrlU.UpButtons[j].Click += Up_Click;
                    _listButtons.Add(ctrlU.UpButtons[j]);
                    ctrlU.DownButtons[j].Click += Down_Click;
                    _listButtons.Add(ctrlU.DownButtons[j]);
                }
                _voltagesData.Add(ctrlU);
            }

            this.ChangeLocationFormAndControls();

           
        }

        private void SetSizeAndLocationTime()
        {
            for (int i = 0; i < _currentsData.Count; i++)
            {
                if (_currentsData.Count > 4)
                {
                    this.groupBoxItime.Size = new Size(147, 89 + 15 * (i - 2));
                    this.groupBoxUtime.Location = new Point(6, 190 + 15 * (i - 2));
                    //this.groupBoxDiskrets.Location = new Point(6, 189 + 15 * (i - 2));
                }
            }

            this._heightGpItime = groupBoxItime.Size.Height;
            this._heightGpUTime = groupBoxUtime.Size.Height;
            this._heightGpDisTime = groupBoxDiskrets.Location.Y;

            //for (int i = 0; i < _voltagesData.Count; i++)
            //{
            //    if (_voltagesData.Count > 4)
            //    {
            //        groupBoxUtime.Size = new Size(162, 104 * (i - 4));
            //        this.groupBoxDiskrets.Location = new Point(5, this._heightGpDisTime + 14 * (i - 4));
            //    }
            //}
        }

        private void ChangeLocationFormAndControls()
        {
            for (int i = 0; i < _currentsData.Count; i++)
            {
                if (_currentsData.Count > 4)
                {
                    groupBoxI.Size = new Size(367, 160 + 26 * (i - 4));
                    //groupBoxU.Location = new Point(6, 185 + 26 * (i - 4));
                    this.Size = new Size(1112, 515 + 13 * i);
                }
                
                _currentsData[i].Location = new Point(3, 3 + 26 * i);

                panelI.Controls.Add(_currentsData[i]);
            }

            for (int i = 0; i < _voltagesData.Count; i++)
            {
                _voltagesData[i].Location = new Point(3, 3 + 26 * i);

                panelU.Controls.Add(_voltagesData[i]);
            }

            if (_currentsData.Count == 24 && resolution.Height < 800) groupBoxInputSignal.Size = new Size(751, 659);

            if (_voltagesData.Count == 0)
            {
                groupBoxU.Visible = false;
                groupBoxUtime.Visible = false;

                groupBoxInputDiskrets.Location = new Point(376, 15);
            }
        }

        private void Up_Click(object sender, EventArgs e)
        {

            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            provider.NumberGroupSeparator = ".";
            Button but = sender as Button;
            MaskedTextBox mtx = but.Tag as MaskedTextBox;
            if (mtx.Text == "")
            {
                mtx.Text = "0";
            }
            if (mtx.Name.Contains("faza"))
            {
                if (mtx.Enabled == false)
                {
                    this._incriment = 0;
                }
                else
                {
                    this._incriment = 1;
                }
                //if (Convert.ToDouble(mtx.Text) <= 0)
                //{
                //    mtx.Text = (Convert.ToDouble(mtx.Text) + 360).ToString();
                //}
                if (Convert.ToDouble(mtx.Text) >= 360)
                {
                    mtx.Text = (Convert.ToDouble(mtx.Text) - 360).ToString();
                }
            }
            else
            {
                this._incriment = (double)this._step.Value;
            }

            double arg = Convert.ToDouble(mtx.Text, provider);
            mtx.Text = (arg + this._incriment).ToString();

            if (mtx.Name.Contains("I") && !mtx.Name.Contains("faza"))
            {
                if (Convert.ToDouble(mtx.Text) > 200)
                {
                    mtx.Text = 200.ToString();
                }
            }
            if (mtx.Name.Contains("U") && !mtx.Name.Contains("faza"))
            {
                if (Convert.ToDouble(mtx.Text) > 256)
                {
                    mtx.Text = 256.ToString();
                }
            }
            if (mtx.Name.Contains("F"))
            {
                if (Convert.ToDouble(mtx.Text) > 500)
                {
                    mtx.Text = 500.ToString();
                }
            }
            if (this.IsWrite)
            {
                GetAnalogData();
                _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                _currentWriteStruct.DiscretInputs = _discrets.Get();
                var cheak = mtx.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (cheak.Checked || this.IsStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }

                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }

        private void Down_Click(object sender, EventArgs e)
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            provider.NumberGroupSeparator = ".";
            Button but = sender as Button;
            MaskedTextBox mtx = but.Tag as MaskedTextBox;
            if (mtx.Text == "")
            {
                mtx.Text = "0";
            }
            if (mtx.Name.Contains("faza"))
            {
                this._incriment = !mtx.Enabled ? 0 : 1;
            }
            else
            {
                this._incriment = (double)this._step.Value;
            }

            double arg = Convert.ToDouble(mtx.Text, provider);
            mtx.Text = (arg - this._incriment).ToString();

            this.ValidationControl(mtx);

            if (this.IsWrite)
            {
                GetAnalogData();
                _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                _currentWriteStruct.DiscretInputs = _discrets.Get();
                var cheak = mtx.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (cheak.Checked || this.IsStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }


                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }

        private void ValidationControl(MaskedTextBox mtx)
        {
            if ((mtx.Name.Contains("I") && !mtx.Name.Contains("faza")) || (mtx.Name.Contains("U") && !mtx.Name.Contains("faza")) || (mtx.Name.Contains("F")))
            {
                if (Convert.ToDouble(mtx.Text) < 0)
                {
                    mtx.Text = 0.ToString();
                }
            }
            if (mtx.Name.Contains("faza"))
            {
                if (Convert.ToDouble(mtx.Text) < 0)
                {
                    mtx.Text = (Convert.ToDouble(mtx.Text) + 360).ToString();
                }
            }
        }

        private void GetAnalogData()
        {
            for (int i = 0; i < m_dataSet.DataI.Length; i++)
            {
                m_dataSet.DataI[i] = _currentsData[i].GetDataI();
            }

            for (int i = 0; i < m_dataSet.DataU.Length; i++)
            {
                m_dataSet.DataU[i] = _voltagesData[i].GetDataU();
            }

            this._currentWriteStruct.AllAnalogData = m_dataSet.GetData();
        }



        private void _discret_CheckedChanged(object sender, EventArgs e)
        {
            this.IsBlock = true;
            if (this.IsWrite)
            {
                GetAnalogData();
                _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                _currentWriteStruct.DiscretInputs = _discrets.Get();

                var cheak = sender as CheckBox;
                var isstart = cheak.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (isstart.Checked || this.IsStart)
                    {
                        _currentWriteStruct.TimeSignal.StatusTime = true;
                    }

                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }

        private void _kvit_Click(object sender, EventArgs e)
        {
            this._kvit.Enabled = false;
            this._memoryEntityNullStructEmulation.LoadStruct();
        }

        /// <summary>
        /// Метод выполняет функции обратоного вращения и углы баланса (isDirect=false - обратное вращение)
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="isDirect"></param>
        private void BalanceCorner(MaskedTextBox tb, bool isDirect)
        {
            int index = _listAllControls.IndexOf(tb);
            int currentCornel = Convert.ToInt32(tb.Text);
            int cornerValue240;
            int cornerValue120;

            if (isDirect)
            {
                for (int i = index; i < _listAllControls.Count; i++)
                {
                    if (i == index + 3)
                    {
                        int cacheCornerValue240 = 0;

                        cacheCornerValue240 = currentCornel + 240;
                        if (cacheCornerValue240 > 359)
                        {
                            cornerValue240 = cacheCornerValue240 - 360;
                            this._listAllControls[i].Text = Convert.ToString(cornerValue240);
                        }
                        else
                        {
                            this._listAllControls[i].Text = Convert.ToString(cacheCornerValue240);
                        }

                    }

                    if (i == index + 6)
                    {
                        int cacheCornerValue120 = 0;

                        cacheCornerValue120 = currentCornel + 120;

                        if (cacheCornerValue120 > 359)
                        {
                            cornerValue120 = cacheCornerValue120 - 360;
                            this._listAllControls[i].Text = Convert.ToString(cornerValue120);
                        }
                        else
                        {
                            this._listAllControls[i].Text = Convert.ToString(cacheCornerValue120);
                        }

                    }
                }
            }

            else
            {
                for (int i = index; i < _listAllControls.Count; i++)
                {
                    if (i == index + 3)
                    {
                        int cacheCornerValue240 = 0;

                        cacheCornerValue240 = currentCornel + 120;
                        if (cacheCornerValue240 > 359)
                        {
                            cornerValue240 = cacheCornerValue240 - 360;
                            this._listAllControls[i].Text = Convert.ToString(cornerValue240);
                        }
                        else
                        {
                            this._listAllControls[i].Text = Convert.ToString(cacheCornerValue240);
                        }

                    }

                    if (i == index + 6)
                    {
                        int cacheCornerValue120 = 0;

                        cacheCornerValue120 = currentCornel + 240;

                        if (cacheCornerValue120 > 359)
                        {
                            cornerValue120 = cacheCornerValue120 - 360;
                            this._listAllControls[i].Text = Convert.ToString(cornerValue120);
                        }
                        else
                        {
                            this._listAllControls[i].Text = Convert.ToString(cacheCornerValue120);
                        }

                    }
                }
            }
        }

        private void BalanceAmplitudes(MaskedTextBox tb)
        {
            int index = _listAllControls.IndexOf(tb);
            string val = tb.Text;

            if (tb.Name.Contains("IMTB") || tb.Name.Contains("UMTB"))
            {
                for (int i = index; i < _listAllControls.Count; i++)
                {
                    if (i == index + 3)
                    {
                        this._listAllControls[i].Text = val;
                    }

                    if (i == index + 6)
                    {
                        this._listAllControls[i].Text = val;
                    }
                }
            }

            //if (tb.Name.Contains("J"))
            //{
            //    foreach (AnalogCtrI ctrI in _currentsData)
            //    {
            //        ctrI.TextBoxes[0].Text = val;
            //    }
            //}

            //if (tb.Name.Contains("L"))
            //{
            //    foreach (AnalogCtrU ctrU in _voltagesData)
            //    {
            //        ctrU.TextBoxes[0].Text = val;
            //    }
            //}

            if (tb.Name.Contains("A"))
            {
                foreach (AnalogCtrI ctrI in _currentsData)
                {
                    ctrI.TextBoxes[2].Text = val;
                }
            }

            if (tb.Name.Contains("R"))
            {
                foreach (AnalogCtrU ctrU in _voltagesData)
                {
                    ctrU.TextBoxes[2].Text = val;
                }
            }

        }

        private void NominalValue(MaskedTextBox tb)
        {
            if (tb.Name[0] == 'I')
            {
                tb.Text = "5";
            }
            if (tb.Name[0] == 'U')
            {
                double nom = 57.73;
                tb.Text = nom.ToString(CultureInfo.CurrentCulture);
            }
        }

        private void MouseRightClick(object sender, MouseEventArgs e)
        {
            if (!(sender is MaskedTextBox tb))
            {
                return;
            }
            if (e.Button == MouseButtons.Left)
            {
                tb.SelectAll();
            }
            if (e.Button == MouseButtons.Right)
            {
                int index = _listAllControls.IndexOf(tb);

                ContextMenu cm = new ContextMenu();

                cm.MenuItems.Add("Номинальное значение", (o, ea) =>
                {
                    this.NominalValue(tb);
                    if (this._writeEmulButton.Checked)
                    {
                        this.WriteEmulation();
                    }
                });
                cm.MenuItems.Add("Ноль", (o, ea) =>
                {
                    tb.Text = "0";
                    if (this._writeEmulButton.Checked)
                    {
                        this.WriteEmulation();
                    }
                });

                switch (Strings.DeviceType)
                {
                    case "T24N0D40R35":
                    case "T24N0D24R51":
                    case "T24N0D32R43":
                        if (index != 66 && index != 67 && index != 69 && index != 70)
                        {
                            cm.MenuItems.Add("Уравнять амплитуды 3-х каналов", (o, ea) =>
                            {
                                this.BalanceAmplitudes(tb);
                                if (this._writeEmulButton.Checked)
                                {
                                    this.WriteEmulation();
                                }
                            });
                        }
                        break;
                    case "T20N4D40R35":
                    case "T20N4D32R43":
                        if (index != 54 && index != 55 && index != 57 && index != 58 &&
                            index != 66 && index != 67 && index != 69 && index != 70)
                        {
                            cm.MenuItems.Add("Уравнять амплитуды 3-х каналов", (o, ea) =>
                            {
                                this.BalanceAmplitudes(tb);
                                if (this._writeEmulButton.Checked)
                                {
                                    this.WriteEmulation();
                                }
                            });
                        }
                        break;
                    default:
                        if (index != 42 && index != 43 && index != 45 && index != 46)
                        {
                            cm.MenuItems.Add("Уравнять амплитуды 3-х каналов", (o, ea) =>
                            {
                                this.BalanceAmplitudes(tb);
                                if (this._writeEmulButton.Checked)
                                {
                                    this.WriteEmulation();
                                }
                            });
                        }
                        break;
                }

                if (tb.Name.Contains("faza"))
                {
                    cm.MenuItems.Clear();
                    cm.MenuItems.Add("Ноль", (o, ea) =>
                    {
                        tb.Text = "0";
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });

                    switch (Strings.DeviceType)
                    {
                        case "T24N0D40R35":
                        case "T24N0D24R51":
                        case "T24N0D32R43":
                            if (index != 66 && index != 67 && index != 69 && index != 70)
                            {
                                cm.MenuItems.Add("Углы баланса", (o, ea) =>
                                {
                                    this.BalanceCorner(tb, true);
                                    if (this._writeEmulButton.Checked)
                                    {
                                        this.WriteEmulation();
                                    }
                                });
                                cm.MenuItems.Add("Обратное вращение", (o, ea) =>
                                {
                                    this.BalanceCorner(tb, false);
                                    if (this._writeEmulButton.Checked)
                                    {
                                        this.WriteEmulation();
                                    }
                                });
                            }
                            break;
                        case "T20N4D40R35":
                        case "T20N4D32R43":
                            if (index != 54 && index != 55 && index != 57 && index != 58 &&
                                index != 66 && index != 67 && index != 69 && index != 70)
                            {
                                cm.MenuItems.Add("Углы баланса", (o, ea) =>
                                {
                                    this.BalanceCorner(tb, true);
                                    if (this._writeEmulButton.Checked)
                                    {
                                        this.WriteEmulation();
                                    }
                                });
                                cm.MenuItems.Add("Обратное вращение", (o, ea) =>
                                {
                                    this.BalanceCorner(tb, false);
                                    if (this._writeEmulButton.Checked)
                                    {
                                        this.WriteEmulation();
                                    }
                                });
                            }
                            break;
                        default:
                            if (index != 42 && index != 43 && index != 45 && index != 46)
                            {
                                cm.MenuItems.Add("Углы баланса", (o, ea) =>
                                {
                                    this.BalanceCorner(tb, true);
                                    if (this._writeEmulButton.Checked)
                                    {
                                        this.WriteEmulation();
                                    }
                                });
                                cm.MenuItems.Add("Обратное вращение", (o, ea) =>
                                {
                                    this.BalanceCorner(tb, false);
                                    if (this._writeEmulButton.Checked)
                                    {
                                        this.WriteEmulation();
                                    }
                                });
                            }
                            break;
                    }
                }

                if (tb.Name.Contains("F"))
                {
                    cm.MenuItems.Clear();
                    cm.MenuItems.Add("Номинальная частота", (o, ea) =>
                    {
                        tb.Text = "50";
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });
                    cm.MenuItems.Add("Уравнять частоты", (o, ea) =>
                    {
                        this.BalanceAmplitudes(tb);
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });

                }
                tb.ContextMenu = cm;
            }
        }
        private void NullEmulationReadOk()
        {
            try
            {
                this.IsKvit = true;

                this._tempWriteStruct = this._memoryEntityNullStructEmulation.Value;

                WriteStructEmul nul = new WriteStructEmul();
                nul.TimeSignal = new TimeAndSignalStruct();
                nul.TimeSignal.IsRestart = true;
                nul.AllAnalogData = new InputAnalogData[WriteStructEmul.COUNT];

                for (int i = 0; i < nul.AllAnalogData.Length; i++)
                {
                    nul.AllAnalogData[i] = new InputAnalogData();
                }
                nul.DiscretInputs = new DiscretsSignals();

                
                this._memoryEntityNullStructEmulation.Value = nul;
                this._memoryEntityNullStructEmulation.SaveStruct();
            }
            catch (Exception)
            {
                this.IsKvit = false;
            }
        }

        private void WriteEmulation()
        {
            if (this.IsKvit)
            {
                this.IsKvit = false;
                this._currentWriteStruct = this._tempWriteStruct;
            }
            else
            {
                this.GetAnalogData();
                this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                this._currentWriteStruct.DiscretInputs = _discrets.Get();
                //this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
            }

            if (this.IsSelect)
            {
                if (this.IsStart)
                {
                    this._currentWriteStruct.TimeSignal.StatusTime = true;
                }

            }
            this._currentWriteStruct.TimeSignal.IsRestart = false;
            
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct(new TimeSpan(50));
            
        }
        
        private void EmulationReadOk()
        {
            this._currentWriteStruct = this._memoryEntityStructEmulation.Value;
            this._validatorTimeAndSignal.Set(_currentWriteStruct.TimeSignal);
            this._discrets.Set(_currentWriteStruct.DiscretInputs);
            this.ShowAnalogData();
        }

        /// <summary>
        /// Сохранение эмуляции в файл 
        /// </summary>
        private void ToXML()
        {
            DateTime now = DateTime.Now;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML Files (*.xml)|*.xml";
            saveFileDialog.FileName = $"Эмуляция от {now.Day}_{now.Month}_{now.Year}";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    GetAnalogData();
                    _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                    _currentWriteStruct.DiscretInputs = _discrets.Get();

                    var serializer = new XmlSerializer(typeof(WriteStructEmul));

                    using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                    {
                        this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                        serializer.Serialize(writer, _currentWriteStruct);
                    }

                    MessageBox.Show("Файл успешно сохранен!", "Сохранение в файл", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка сохранения!");
                }

            }
        }

        /// <summary>
        /// Загрузка эмуляции из файла
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        private void FromXml(string path)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(WriteStructEmul));

                StreamReader reader = new StreamReader(path);
                WriteStructEmul readValuesFromFiles = (WriteStructEmul)serializer.Deserialize(reader);
                reader.Close();

                this._memoryEntityStructEmulation.Value = readValuesFromFiles;
                this._currentWriteStruct = this._memoryEntityStructEmulation.Value;

                this._validatorTimeAndSignal.Set(readValuesFromFiles.TimeSignal);
                this._discrets.Set(readValuesFromFiles.DiscretInputs);
                this.ShowAnalogData();

                MessageBox.Show("Загрузка данных из файла прошла успешно!", "Загрузка из файла", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Ошибка загрузки файла!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ShowAnalogData()
        {
            m_dataSet.SetData(_currentWriteStruct.AllAnalogData);

            for (int i = 0; i < m_dataSet.DataI.Length; i++)
            {
                _currentsData[i].SetDataI(m_dataSet.DataI[i]);
            }

            for (int i = 0; i < m_dataSet.DataU.Length; i++)
            {
                _voltagesData[i].SetDataU(m_dataSet.DataU[i]);
            }
        }

        private void EmulationWriteOk()
        {
            if (IsBlock = true)
            {

            }
            if (this.IsKvit)
            {
                this._kvit.Enabled = true;
                this.WriteEmulation();
            }
            else
            {
                var write = this._memoryEntityStructEmulation.Value;
                write.TimeSignal.WriteTimeOk= true;
                this._memoryEntityStructEmulation.Value = write;
                this._memoryEntityStructEmulation.LoadStruct();
            }
        }

        /// <summary>
        /// Ошибка записи эмуляции
        /// </summary>
        private void EmulationWriteFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = "Нет связи с устройством";
            MessageBox.Show(WRITE_FAIL);

        }

        private void EmulationReadFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = READ_FAIL+"\nНет связи с устройством.";
        }

        private void ReadOk()
        {
            this._timeSignal.Text = this._readMemoryEntity.Value.TimeSignal;
            this._time.Text = this._readMemoryEntity.Value.Time;
            this._status.Text = this._readMemoryEntity.Value.Status.ToString();
        }

        private void readEmulation_Click(object sender, EventArgs e)
        {
            this._memoryEntityStructEmulation.LoadStruct();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this._incriment = (double)this._step.Value;
        }
        
        private void EmulationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
            this._currentWriteStruct.TimeSignal.StopTime = true;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._readMemoryEntity.RemoveStructQueries();
            
            this._readMemoryEntity.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.ReadOk);
        }

        private void EmulationForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readMemoryEntity.LoadStructCycle();
        }

        private void _writeEmulButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._writeEmulButton.Checked)
            {
                ReadStructEmul.IsStatus = true;
                this._writeEmulButton.BackColor = Color.Black;
                this._writeEmulButton.ForeColor = Color.White;
                if (this.IsSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                    }
                    this._startTime.Enabled = true;
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = false;
                }
                if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

                this.IsWrite = true;
                this.WriteEmulation();
            }
            else
            {
                ReadStructEmul.IsStatus = false;
                this._writeEmulButton.BackColor = Color.Empty;
                this._writeEmulButton.ForeColor = Color.Black;
                this.IsWrite = false;
                if (this.IsSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = true;
                }
            }

        }
        
        private void _status_TextChanged(object sender, EventArgs e)
        {
            switch (this._status.Text)
            {
                case "1":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 без блокировки выходных сигналов запущена";
                        //ReadStructEmul.isStatus = true;
                        break;
                    }
                case "2":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 с блокировкой выходных сигналов запущена";
                        //ReadStructEmul.isStatus = true;
                        break;
                    }
                case "3":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 с блокировкой упр. выходных сигналов запущена";
                        //ReadStructEmul.isStatus = false;
                        break;
                    }
                case "4":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 2 без блокировки выходных сигналов запущена";
                        //ReadStructEmul.isStatus = false;
                        break;
                    }
                case "0":
                {
                    this._statusLedControl.State = LedState.NoSignaled;
                    this._labelStatus.Text = "Эмуляция остановлена";
                    //ReadStructEmul.isStatus = false;
                    break;
                }

            }
        }
            
        private void _startTime_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = sender as CheckBox;
            if (ch.Checked)
            {
                this.IsStart = true;
            }
            else
            {
                this.IsStart = false;
            }
        }

        private void _exitEmulation_CheckedChanged(object sender, EventArgs e)
        {
            if (this._exitEmulation.Checked)
            {
                this.IsExit = true;
            }
            else
            {
                this.IsExit = false;
            }
        }

        private void _time_TextChanged(object sender, EventArgs e)
        {
            if (this.IsExit)
            {
                if (this._time.Text.Contains("0:05"))
                {
                    this.WriteEmulation();
                }
            }
        }

        private void EmulationForm_Activated(object sender, EventArgs e)
        {
            Strings.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }

        #region [IFormView Members]

        public Type ClassType
        {
            get { return typeof(EmulationForm); }

        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.emulation.ToBitmap(); }

        }

        public string NodeName
        {
            get { return "Эмуляция"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public Type FormDevice
        {
            get { return typeof(MR901New); }
        }

        public bool Multishow { get; private set; }

        #endregion

        private bool GetProcess()
        {
            try
            {
                foreach (var process in Process.GetProcesses())
                {
                    if (process.ProcessName == "shlpicr")
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        private void _signal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._signal.SelectedIndex == 0)
            {
                GetAnalogData();
                _currentWriteStruct.TimeSignal = _validatorTimeAndSignal.Get();
                _currentWriteStruct.DiscretInputs = _discrets.Get();
                _currentWriteStruct.TimeSignal.StopTime = true;
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();

                this.IsSelect = false;
                this._startTime.Enabled = false;
                foreach (var control in this._listStartControls)
                {
                    control.Enabled = false;
                }
            }
            else
            {
                if (!this._writeEmulButton.Checked)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                        this._startTime.Enabled = true;
                    }
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                        this._startTime.Enabled = true;
                    }
                }

                this.IsSelect = true;
            }
        }



        private void saveToXmlButton_Click(object sender, EventArgs e)
        {
            ToXML();
        }

        private void _fromXmlBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML Files (*.xml)|*.xml";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FromXml(openFileDialog.FileName);
            }
        }
    }
}
