﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses.U
{
    public class VoltageDefenseStruct : StructBase
    {
        [Layout(0)] public ushort config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] public ushort config1;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] public ushort block; //вход блокировки
        [Layout(3)] public ushort ust; //уставка срабатывания_
        [Layout(4)] public ushort time; //время срабатывания_
        [Layout(5)] public ushort u; //уставка возврата
        [Layout(6)] public ushort tu; //время возврата
        [Layout(7)] public ushort res; //резерв

        #region Внешние защиты

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string MODE
        {
            get { return Validator.Get(this.config, Strings.Mode, 0, 1); }
            set { this.config = Validator.Set(value, Strings.Mode, this.config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Откл")]
        public string OTKL
        {
            get { return Validator.Get(this.config, Strings.Otkl, 4, 5, 6, 7, 8); }
            set { this.config = Validator.Set(value, Strings.Otkl, this.config, 4, 5, 6, 7, 8); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Блокировка")]
        public string BLOCK
        {
            get { return Validator.Get(this.block, Strings.ExtDefSignals); }
            set { this.block = Validator.Set(value, Strings.ExtDefSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "U5в")]
        public bool U5
        {
            get { return Common.GetBit(this.config, 9); }
            set { this.config = Common.SetBit(this.config, 9, value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "3U0")]
        public bool Block3U0
        {
            get { return Common.GetBit(this.config1, 0); }
            set { this.config1 = Common.SetBit(this.config1, 0, value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "НС")]
        public bool BlockNS
        {
            get { return Common.GetBit(this.config1, 1); }
            set { this.config1 = Common.SetBit(this.config1, 1, value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "ТипUB")]
        public string TypeUB
        {
            get { return Validator.Get(this.config, Strings.UBtypes, 10, 11, 12); }
            set { this.config = Validator.Set(value, Strings.UBtypes, this.config, 10, 11, 12); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "ТипUM")]
        public string TypeUM
        {
            get { return Validator.Get(this.config, Strings.UMtypes, 10, 11, 12); }
            set { this.config = Validator.Set(value, Strings.UMtypes, this.config, 10, 11, 12); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Uср")]
        public double SRAB
        {
            get { return ValuesConverterCommon.GetUstavka256(this.ust); }
            set { this.ust = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "tср")]
        public int TSR
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }

        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Возврат")]
        public bool VOZVR
        {
            get { return Common.GetBit(this.config, 3); }
            set { this.config = Common.SetBit(this.config, 3, value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Uвз")]
        public double Uvz
        {
            get { return ValuesConverterCommon.GetUstavka256(this.u); }
            set { this.u = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "tвз")]
        public int TVZ
        {
            get { return ValuesConverterCommon.GetWaitTime(this.tu); }
            set { this.tu = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this.config, Strings.ModesLightOsc, 14, 15); }
            set { this.config = Validator.Set(value, Strings.ModesLightOsc, this.config, 14, 15); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this.config, 2); }
            set { this.config = Common.SetBit(this.config, 2, value); }
        }

        #endregion 
    }
}