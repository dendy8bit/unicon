﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses.U
{
    public class AllVoltageDefenseStruct : StructBase, IDgvRowsContainer<VoltageDefenseStruct>
    {
        public const int COUNT = 4;

        [Layout(0, Count = COUNT)] private VoltageDefenseStruct[] _voltageDefenses;

        public VoltageDefenseStruct[] Rows
        {
            get { return this._voltageDefenses; }
            set { this._voltageDefenses = value; }
        }
    }
}
