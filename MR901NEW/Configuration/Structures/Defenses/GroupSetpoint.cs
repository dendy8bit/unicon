﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR901NEW.Configuration.Structures.Defenses.Differential;
using BEMN.MR901NEW.Configuration.Structures.Defenses.External;
using BEMN.MR901NEW.Configuration.Structures.Defenses.Mtz;
using BEMN.MR901NEW.Configuration.Structures.Defenses.U;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses
{
    public class GroupSetpoint : StructBase
    {
        [Layout(0)] private AllDifferentialCurrentStruct _allDifferentialCurrent;
        [Layout(1)] private AllMtzStruct _allMtz;
        [Layout(2)] private AllExternalDefenseStruct _allExternalDefense;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Диф")]
        public AllDifferentialCurrentStruct AllDifferentialCurrent
        {
            get { return this._allDifferentialCurrent; }
            set { this._allDifferentialCurrent = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "МТЗ")]
        public AllMtzStruct AllMtz
        {
            get { return this._allMtz; }
            set { this._allMtz = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefenseStruct AllExternalDefense
        {
            get { return this._allExternalDefense; }
            set { this._allExternalDefense = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Напряжения")]
        public AllVoltageDefenseStruct AllVoltageDefense
        {
            get
            {
                if (AllExternalDefenseStruct.CurrentCount > 20) return null;

                AllVoltageDefenseStruct ret = new AllVoltageDefenseStruct();
                List<ushort> values = new List<ushort>();
                for (int i = 0; i < AllVoltageDefenseStruct.COUNT; i++)
                {
                    values.AddRange(this._allExternalDefense.Rows[AllExternalDefenseStruct.CurrentCount + i].GetValues());
                }
                ret.InitStruct(Common.TOBYTES(values, false));
                return ret;
            }
            set
            {
                for (int i = 0; i < AllVoltageDefenseStruct.COUNT; i++)
                {
                    byte[] values = Common.TOBYTES(value.Rows[i].GetValues(), false);
                    this._allExternalDefense.Rows[AllExternalDefenseStruct.CurrentCount + i].InitStruct(values);
                }
            }
        }
    }
}
