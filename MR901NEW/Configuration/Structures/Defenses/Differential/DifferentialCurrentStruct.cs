﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses.Differential
{
    public class DifferentialCurrentStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block; //вход блокировки
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        //характеристика торможения_
        [Layout(5)] private ushort _ib1; // начало  1
        [Layout(6)] private ushort _k1; // тангенс 1
        [Layout(7)] private ushort _och; //уставка очувствления
        [Layout(8)] private ushort TOch; //время очувствления
        [Layout(9)] private ushort _inputOch; //вход очувствления
        [Layout(10)] private ushort _i21; //уставка по току 2-ой гармонике
        [Layout(11)] private ushort _ido;
        [Layout(12)] private ushort _i51;
        [Layout(13)] private ushort _iOch;

        #region Диф защиты действующие

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string DIFD_MODE
        {
            get { return Validator.Get(this._config, Strings.Mode, 0, 1); }
            set { this._config = Validator.Set(value, Strings.Mode, this._config, 0, 1); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string DIFD_BLOCK
        {
            get { return Validator.Get(this._block, Strings.InputSignals); }
            set { this._block = Validator.Set(value, Strings.InputSignals); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "ПО")]
        public bool DefenceActualRunForPo
        {
            get { return Common.GetBit(this._config, 11); }
            set { this._config = Common.SetBit(this._config, 11, value); }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка")]
        public double DIFD_ICP
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Уставка2")]
        public double DIFD_IDO
        {
            get { return ValuesConverterCommon.GetIn(this._ido); }
            set { this._ido = ValuesConverterCommon.SetIn(value); }

        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "tср")]
        public int DIFD_TCP
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }

        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "Iб")]
        public double DIFD_IB
        {
            get { return ValuesConverterCommon.GetIn(this._ib1); }
            set { this._ib1 = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "f")]
        public ushort DIFD_F
        {
            get { return this._k1; }
            set { this._k1 = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Блок2")]
        public bool DIFD_BLOCK_G
        {
            get { return Common.GetBit(this._config, 4); }
            set { this._config = Common.SetBit(this._config, 4, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Блок2Уставка")]
        public ushort DIFD_I2G
        {
            get { return this._i21; }
            set { this._i21 = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Блок5")]
        public bool DIFD_BLOCK_5G
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }

        }
        [BindingProperty(11)]
        [XmlElement(ElementName = "Блок5Уставка")]
        public ushort DIFD_I5G
        {
            get { return this._i51; }
            set { this._i51 = value; }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "Насыщ")]
        public bool DIFD_BLOCK_OPR_NAS
        {
            get { return Common.GetBit(this._config, 8); }
            set { this._config = Common.SetBit(this._config, 8, value); }
        }
        [BindingProperty(13)]
        [XmlElement(ElementName = "Очувстление")]
        public bool DIFD_OCH
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }
        [BindingProperty(14)]
        [XmlElement(ElementName = "Iд")]
        public double DIFD_I_OCH
        {
            get { return ValuesConverterCommon.GetIn(this._och); }
            set { this._och = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Iб*")]
        public double DIFD_I_OCH_STAR
        {
            get { return ValuesConverterCommon.GetIn(this._iOch); }
            set { this._iOch = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "Tоч")]
        public int DIFD_T_OCH
        {
            get { return ValuesConverterCommon.GetWaitTime(this.TOch); }
            set { this.TOch = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "ВХОД_оч")]
        public string DIFD_ENTER_OCH
        {
            get { return Validator.Get(this._inputOch, Strings.InputSignals); }
            set { this._inputOch = Validator.Set(value, Strings.InputSignals); }
        }

        [BindingProperty(18)]
        [XmlElement(ElementName = "Осц")]
        public string DIFD_OSC
        {
            get { return Validator.Get(this._config, Strings.ModesLightOsc, 14, 15); }
            set { this._config = Validator.Set(value, Strings.ModesLightOsc, this._config, 14, 15); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "Уров")]
        public bool DIFD_UROV
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        #endregion
    }
}