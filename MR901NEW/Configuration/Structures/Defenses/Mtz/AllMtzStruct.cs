﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses.Mtz
{
  public  class AllMtzStruct:StructBase, IDgvRowsContainer<MtzStruct>
  {
      public const int COUNT = 32;
      #region [Private fields]

      [Layout(0, Count = COUNT)] private MtzStruct[] _mtz;

      #endregion [Private fields]

      public MtzStruct[] Rows
      {
          get { return this._mtz; }
          set { this._mtz = value; }
      }
    }
}
