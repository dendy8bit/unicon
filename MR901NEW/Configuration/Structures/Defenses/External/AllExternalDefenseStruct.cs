﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses.External
{
    public class AllExternalDefenseStruct : StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        private const int COUNT = 24;

        private static int _currentCount;
        public static int CurrentCount => _currentCount;

        [Layout(0, Count = COUNT)] private ExternalDefenseStruct[] _externalDefenses;

        public ExternalDefenseStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }

        public static void SetDeviceExternalDefType(string type)
        {
            switch (type)
            {
                case "T16N0D24R19":
                case "T16N0D64R43":
                case "T24N0D40R35":
                case "T24N0D24R51":
                case "T24N0D32R43":
                    _currentCount = 24;
                    break;
                case "T20N4D40R35":
                case "T20N4D32R43":
                    _currentCount = 20;
                    break;
                default:
                    _currentCount = 16;
                    break;
            }
        }
    }
}
