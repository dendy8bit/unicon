﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR901NEW.Configuration.Structures.Defenses
{
    /// <summary>
    /// защиты по двум группам уставок
    /// </summary>
    [Serializable]
    public class AllDefensesSetpointsStruct : StructBase, ISetpointContainer<GroupSetpoint>
    {
        private const int GROUP_COUNT = 2;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpoint[] _groupSetpoints;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint[] Setpoints
        {
            get
            {
                var res = new GroupSetpoint[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpoint>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
