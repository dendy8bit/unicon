﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901NEW.Configuration.Structures.Urov
{
    public class UrovStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort config;      //конфигурация
        [Layout(1)] private ushort time1;       //время срабатывания
        [Layout(2)] private ushort time2;       //время срабатывания
        [Layout(3)] private ushort time3;       //время срабатывания
        [Layout(4)] private ushort urovSH1;
        [Layout(5)] private ushort urovSH2;
        [Layout(6)] private ushort urovSH12;
        [Layout(7)] private ushort blockSH1;      //блокировка УРОВ
        [Layout(8)] private ushort blockSH2;      //блокировка УРОВ
        [Layout(9)] private ushort blockSH12;      //блокировка УРОВ
        [Layout(10)] private ushort block;      //блокировка УРОВ присоед.
        [Layout(11)] private ushort timeOtkl;
        #endregion [Private fields]

        #region ДЗШ

        [XmlElement(ElementName = "Контр")]
        [BindingProperty(0)]
        public string DzhControl
        {
            get { return Validator.Get(this.config, Strings.KONTR, 0); }
            set { this.config = Validator.Set(value, Strings.KONTR, this.config, 0); }
        }

        [XmlElement(ElementName = "На_себя")]
        [BindingProperty(1)]
        public string DzhSelf
        {
            get { return Validator.Get(this.config, Strings.Forbidden, 1); }
            set { this.config = Validator.Set(value, Strings.Forbidden, this.config, 1); }
        }
        
        [XmlElement(ElementName = "Т1")]
        [BindingProperty(2)]
        public int DzhT1
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time1); }
            set { this.time1 = ValuesConverterCommon.SetWaitTime(value); }

        }

        [XmlElement(ElementName = "Уров_2")]
        [BindingProperty(3)]
        public string DzhUrov2
        {
            get { return Validator.Get(this.config, Strings.Forbidden, 2); }
            set { this.config = Validator.Set(value, Strings.Forbidden, this.config, 2); }
        }

        [XmlElement(ElementName = "Т2")]
        [BindingProperty(4)]
        public int DzhT2
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time2); }
            set { this.time2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [XmlElement(ElementName = "Уров_3")]
        [BindingProperty(5)]
        public string DzhUrov3
        {
            get { return Validator.Get(this.config, Strings.Forbidden, 3); }
            set { this.config = Validator.Set(value, Strings.Forbidden, this.config, 3); }
        }

        [XmlElement(ElementName = "Т3")]
        [BindingProperty(6)]
        public int DzhT3
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time3); }
            set { this.time3 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [XmlElement(ElementName = "СШ1_сигнал_пуска")]
        [BindingProperty(7)]
        public string DzhSh1
        {
            get { return Validator.Get(this.urovSH1, Strings.InputSignals); }
            set { this.urovSH1 = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "СШ2_сигнал_пуска")]
        [BindingProperty(8)]
        public string DzhSh2
        {
            get { return Validator.Get(this.urovSH2, Strings.InputSignals); }
            set { this.urovSH2 = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "ПО_сигнал_пуска")]
        [BindingProperty(9)]
        public string DzhPo
        {
            get { return Validator.Get(this.urovSH12, Strings.InputSignals); }
            set { this.urovSH12 = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "СШ1_блок_уров")]
        [BindingProperty(10)]
        public string DzhShBlock1
        {
            get { return Validator.Get(this.blockSH1, Strings.InputSignals); }
            set { this.blockSH1 = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "СШ2_блок_уров")]
        [BindingProperty(11)]
        public string DzhShBlock2
        {
            get { return Validator.Get(this.blockSH2, Strings.InputSignals); }
            set { this.blockSH2 = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "ПО_блок_уров")]
        [BindingProperty(12)]
        public string DzhPoBlock
        {
            get { return Validator.Get(this.blockSH12, Strings.InputSignals); }
            set { this.blockSH12 = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "На_себя_уров_прис")]
        [BindingProperty(13)]
        public string DzhUrovSelf
        {
            get { return Validator.Get(this.config, Strings.Forbidden, 5); }
            set { this.config = Validator.Set(value, Strings.Forbidden, this.config, 5); }
        }

        [XmlElement(ElementName = "Блок_уров_прис")]
        [BindingProperty(14)]
        public string DzhUrovBlock
        {
            get { return Validator.Get(this.block, Strings.InputSignals); }
            set { this.block = Validator.Set(value, Strings.InputSignals); }
        }

        [XmlElement(ElementName = "Присоединение")]
        [BindingProperty(15)]
        public string DzhConnection
        {
            get { return Validator.Get(this.config, Strings.Forbidden, 4); }
            set { this.config = Validator.Set(value, Strings.Forbidden, this.config, 4); }
        }

        [XmlElement(ElementName = "T_отключения")]
        [BindingProperty(16)]
        public int TOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(this.timeOtkl); }
            set { this.timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }
        #endregion
    }
}