﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901NEW.Configuration.Structures.Urov
{
    public class UrovConnectionStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _ust;  //ток уров
        [Layout(1)] private ushort _time; //время уров
        [Layout(2)] private ushort _time2;
        [Layout(3)] private ushort _pusk;

        #endregion [Private fields]

        #region Уров Присоединения

        [XmlElement(ElementName = "Вх_пуска")]
        [BindingProperty(0)]
        public string UrovPusk
        {
            get { return Validator.Get(this._pusk, Strings.RelaySignals); }
            set { this._pusk = Validator.Set(value, Strings.RelaySignals); }
        }

        [XmlElement(ElementName = "I_уров")]
        [BindingProperty(1)]
        public double UrovJoinI
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        [XmlElement(ElementName = "T1")]
        [BindingProperty(2)]
        public int UrovJoinT1
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        [XmlElement(ElementName = "T2")]
        [BindingProperty(3)]
        public int UrovJoinT2
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time2); }
            set { this._time2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        

        #endregion
    }
}