﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901NEW.Configuration.Structures.Urov
{

    public class AllUrovConnectionStruct : StructBase, IDgvRowsContainer<UrovConnectionStruct>
    {
        private const int COUNT_MEMORY = 24;
        public static int CurrentUrovConnectionCount { get; private set; }

        [Layout(0, Count = COUNT_MEMORY)] private UrovConnectionStruct[] ust;
        
        public UrovConnectionStruct[] Rows
        {
            get { return this.ust; }
            set { this.ust = value; }
        }

        public static void SetDeviceConnectionsType(string type)
        {
            switch (type)
            {
                case "T16N0D64R43":
                case "T16N0D24R19":
                    CurrentUrovConnectionCount = 16;
                    break;
                case "T24N0D40R35":
                case "T24N0D24R51":
                case "T24N0D32R43":
                    CurrentUrovConnectionCount = 24;
                    break;
                case "T20N4D40R35":
                case "T20N4D32R43":
                    CurrentUrovConnectionCount = 20;
                    break;
                default:
                    CurrentUrovConnectionCount = 16;
                    break;
            }
        }
    }
}