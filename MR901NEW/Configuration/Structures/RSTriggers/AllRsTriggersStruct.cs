﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901NEW.Configuration.Structures.RSTriggers
{
    public class AllRsTriggersStruct : StructBase, IDgvRowsContainer<RsTriggersStruct>
    {
        public const int RSTRIGGERS_COUNT_CONST = 16;

        [Layout(0, Count = RSTRIGGERS_COUNT_CONST)] private RsTriggersStruct[] _rsTriggers;

        [XmlArray(ElementName = "Все_rs_триггеры")]

        public RsTriggersStruct[] Rows
        {
            get { return this._rsTriggers; }
            set { this._rsTriggers = value; }
        }
    }
}
