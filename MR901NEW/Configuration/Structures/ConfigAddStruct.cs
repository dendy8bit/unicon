﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901NEW.Configuration.Structures
{
    public class ConfigAddStruct : StructBase
    {
        [Layout(0)] private ushort _inp;
        [Layout(1)] private ushort _general;

        [BindingProperty(0)]
        public string InputAdd
        {
            get { return Validator.Get(this._inp, Strings.InpOporSignals); }
            set { this._inp = Validator.Set(value, Strings.InpOporSignals); }
        }

        /// <summary>
        /// Сброс индикаторов по входу в журнал системы
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сброс_по_входу_в_журнал_системы")]
        public bool ResetSystem
        {
            get { return Common.GetBit(this._general, 0); }
            set { this._general = Common.SetBit(this._general, 0, value); }
        }

        /// <summary>
        /// Сброс индикаторов по входу в журнал аварий
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Сброс_по_входу_в_журнал_аварий")]
        public bool ResetAlarm
        {
            get { return Common.GetBit(this._general, 1); }
            set { this._general = Common.SetBit(this._general, 1, value); }
        }
    }
}
