﻿using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901NEW.Configuration.Structures.ConfigSystem
{
    public class ConfigIPAddress : StructBase
    {
        [Layout(0)] private ushort _ipLo;
        [Layout(1)] private ushort _ipHi;
        [Layout(2)] private ushort _sntpip_lo;
        [Layout(3)] private ushort _sntpip_hi;
        [Layout(4)] private ushort _config;
        [Layout(5)] private ushort _mac_lo;
        [Layout(6)] private ushort _mac_md;
        [Layout(7)] private ushort _mac_hi;
        [Layout(8)] private ushort _period;
        [Layout(9)] private ushort _timezone;

        [BindingProperty(0)]
        public ushort IpLo1
        {
            get { return Common.GetBits(this._ipLo, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._ipLo = Common.SetBits(this._ipLo, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }
        [BindingProperty(1)]
        public ushort IpLo2
        {
            get { return (ushort)(Common.GetBits(this._ipLo, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._ipLo = Common.SetBits(this._ipLo, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        [BindingProperty(2)]
        public ushort IpHi1
        {
            get { return Common.GetBits(this._ipHi, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._ipHi = Common.SetBits(this._ipHi, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        [BindingProperty(3)]
        public ushort IpHi2
        {
            get { return (ushort)(Common.GetBits(this._ipHi, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._ipHi = Common.SetBits(this._ipHi, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Резервирование")]
        public string Reserve
        {
            get { return Validator.Get(this._config, Strings.Reserve, 2, 3); }
            set { this._config = Validator.Set(value, Strings.Reserve, this._config, 2, 3); }
        }

    }
}
