using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901NEW.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleStruct : StructBase, IDgvRowsContainer<ReleOutputStruct>
    {
        public const int RELAY_MEMORY_COUNT = 80;
        private static int _currentCount;
        public static int CurrentCount => _currentCount;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_MEMORY_COUNT)]
        private ReleOutputStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]

        public ReleOutputStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }

        public static void SetDeviceRelaysType(string type)
        {
            switch (type)
            {
                case "T16N0D64R43":
                case "T24N0D32R43":
                case "T20N4D32R43":
                    _currentCount = 42;
                    break;
                case "T24N0D40R35":
                case "T20N4D40R35":
                    _currentCount = 34;
                    break;
                case "T24N0D24R51":
                    _currentCount = 50;
                    break;
                default:
                    _currentCount = 18;
                    break;
            }
        }
    }
}