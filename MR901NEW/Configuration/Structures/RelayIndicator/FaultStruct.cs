using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR901NEW.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// ���� �������������
    /// </summary>
    [XmlRoot(ElementName = "����_�������������")]
    public class FaultStruct : StructBase
    {
        /// <summary>
        /// ���� �������������
        /// </summary>
        [Layout(0)] private ushort _disrepair;
        /// <summary>
        /// ������� ���� �������������
        /// </summary>
        [Layout(1)] private ushort _disrepairImp;

        #region [Properties]

        /// <summary>
        /// ������������� 1
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "�������������_1")]
        public bool Fault1
        {
            get { return Common.GetBit(this._disrepair, 0); }
            set { this._disrepair = Common.SetBit(this._disrepair, 0, value); }
        }

        /// <summary>
        /// ������������� 2
        /// </summary>
        
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "�������������_2")]
        public bool Fault2
        {
            get { return Common.GetBit(this._disrepair, 1); }
            set { this._disrepair = Common.SetBit(this._disrepair, 1, value); }
        }

        /// <summary>
        /// ������������� 3
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "�������������_3")]
        public bool Fault3
        {
            get { return Common.GetBit(this._disrepair, 2); }
            set { this._disrepair = Common.SetBit(this._disrepair, 2, value); }
        }

        /// <summary>
        /// ������������� 4
        /// </summary>
        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "�������������_4")]
        public bool Fault4
        {
            get { return Common.GetBit(this._disrepair, 3); }
            set { this._disrepair = Common.SetBit(this._disrepair, 3, value); }
        }

        /// <summary>
        /// ������������� 5
        /// </summary>
        [BindingProperty(4)]
        [XmlAttribute(AttributeName = "�������������_5")]
        public bool Fault5
        {
            get { return Common.GetBit(this._disrepair, 4); }
            set { this._disrepair = Common.SetBit(this._disrepair, 4, value); }
        }

        /// <summary>
        /// ������������� 6
        /// </summary>
        [BindingProperty(5)]
        [XmlAttribute(AttributeName = "�������������_6")]
        public bool Fault6
        {
            get { return Common.GetBit(this._disrepair, 5); }
            set { this._disrepair = Common.SetBit(this._disrepair, 5, value); }
        }

        /// <summary>
        /// ������� ���� �������������
        /// </summary>
        [XmlAttribute(AttributeName = "�������_����_�������������")]
        [BindingProperty(6)]
        public int FaultTime
        {
            get { return ValuesConverterCommon.GetWaitTime(this._disrepairImp); }
            set { this._disrepairImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion [Properties]
    }
}