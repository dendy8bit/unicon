﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901NEW.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// параметры индикаторов
    /// </summary>
    [XmlType(TypeName = "Один_индикатор")]
    public class IndicatorsStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _signal1;
        [Layout(2)] private ushort _type;
        [Layout(3)] private ushort _reserve;
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string Type
        {
            get { return Validator.Get(this._type, Strings.ReleyType, 0); }
            set { this._type = Validator.Set(value, Strings.ReleyType, this._type, 0); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "База1")]
        public string Base1
        {
            get { return Validator.Get(this._type, Strings.OscBases, 12, 13); }
            set { this._type = Validator.Set(value, Strings.OscBases, this._type, 12, 13); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Сигнал_зеленый")]
        public string SignalGreen
        {
            get
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base1)];
                return Validator.Get(this._signal, list);
            }
            set
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base1)];
                this._signal = Validator.Set(value, list);
            }
        }

        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "База2")]
        public string Base2
        {
            get { return Validator.Get(this._type, Strings.OscBases, 14, 15); }
            set { this._type = Validator.Set(value, Strings.OscBases, this._type, 14, 15); }
        }

        [BindingProperty(4)]
        [XmlAttribute(AttributeName = "Сигнал_красный")]
        public string SignalRed
        {
            get
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base2)];
                return Validator.Get(this._signal1, list);
            }
            set
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[CurrentBase(Base2)];
                this._signal1 = Validator.Set(value, list);
            }
        }

        /// <summary>
        /// Режим работы
        /// </summary>
        [BindingProperty(5)]
        [XmlAttribute(AttributeName = "Режим_работы")]
        public string Mode
        {
            get { return Validator.Get(this._type, Strings.ModeRele, 8, 9); }
            set { this._type = Validator.Set(value, Strings.ModeRele, this._type, 8, 9); }
        }

        private int CurrentBase(string baseString)
        {
            int indexBase = Convert.ToInt32(Regex.Replace(baseString, @"[^\d]+", ""));
            return indexBase - 1;
        }

        #endregion [Properties]
    }
}
