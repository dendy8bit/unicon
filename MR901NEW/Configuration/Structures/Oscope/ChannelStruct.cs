﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using System.Xml.Serialization;

namespace BEMN.MR901NEW.Configuration.Structures.Oscope
{
    [XmlType(TypeName = "Один_канал")]
    public class ChannelStruct : StructBase
    {
        [Layout(0)] private ushort _channel;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get { return Validator.Get(this._channel, Strings.RelaySignals); }
            set { this._channel = Validator.Set(value, Strings.RelaySignals); }
        }
    }
}
