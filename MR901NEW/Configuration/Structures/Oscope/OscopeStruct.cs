﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR901NEW.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private OscopeConfigStruct _oscopeConfig;
        [Layout(1)] private ChannelStruct _test;
        [Layout(2)] private OscopeAllChannelsStruct _oscopeAllChannels;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStruct OscopeConfig
        {
            get { return this._oscopeConfig; }
            set { this._oscopeConfig = value; }
        }
        /// <summary>
        /// вход запуска осциллографа
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "вход_запуска_осциллографа")]
        public ChannelStruct StartOscChannel
        {
            get { return _test; }
            set { this._test = value; }
        }

        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStruct OscopeAllChannels
        {
            get { return this._oscopeAllChannels; }
            set { this._oscopeAllChannels = value; }
        }

        

        #endregion [Properties]
    }
}
