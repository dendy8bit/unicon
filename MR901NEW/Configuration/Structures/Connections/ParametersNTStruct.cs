﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901NEW.Configuration.Structures.Connections
{
    public class ParametersNTStruct: StructBase
    {
        [Layout(0)] private ushort _uttl;
        [Layout(1)] private ushort _uttx;
        [Layout(2)] private ushort _polL;
        [Layout(3)] private ushort _polX;
        [Layout(4)] private ushort _deltaU;
        [Layout(5)] private ushort _timeDeltaU;
        [Layout(6)] private ushort _blockDeltaU;
        [Layout(7)] private ushort _divU;
        [Layout(8)] private ushort _timeDivU;
        [Layout(9)] private ushort _blockDivU;
        [Layout(10)] private ushort _conf;
        [Layout(11)] private ushort rez;

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(0)]
        [XmlIgnore]
        public double Kthl
        {
            get { return ValuesConverterCommon.GetKth(this._uttl); }
            set { this._uttl = ValuesConverterCommon.SetKth(value); }
        }

        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        [XmlIgnore]
        public double KthlValue
        {
            get
            {
                double ktn = Common.SetBit(this._uttl, 15, false);
                return Common.GetBit(this._uttl, 15)
                    ? ktn * 1000 / 256
                    : ktn / 256;
            }
        }
        /// <summary>
        /// KTHL Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTHL")]
        public double KthlXml
        {
            get
            {
                double ktn = ValuesConverterCommon.GetKth(this._uttl);
                return Common.GetBit(this._uttl, 15)
                    ? ktn * 1000
                    : ktn;
            }
            set { }
        }
        /// <summary>
        /// KTHX
        /// </summary>
        [BindingProperty(1)]
        [XmlIgnore]
        public double Kthx
        {
            get { return ValuesConverterCommon.GetKth(this._uttx); }
            set { this._uttx = ValuesConverterCommon.SetKth(value); }
        }

        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        [XmlIgnore]
        public double KthxValue
        {
            get
            {
                double ktx = Common.SetBit(this._uttx, 15, false);
                return Common.GetBit(this._uttx, 15)
                    ? ktx * 1000 / 256
                    : ktx / 256;
            }
        }
        /// <summary>
        /// KTHX Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTHX")]
        public double KthxXml
        {
            get
            {
                double ktx = ValuesConverterCommon.GetKth(this._uttx);
                return Common.GetBit(this._uttx, 15)
                    ? ktx * 1000
                    : ktx;
            }
            set { }
        }

        /// <summary>
        /// KTHL коэффициент
        /// </summary>
        [BindingProperty(2)]
        [XmlIgnore]
        public string Lkoef
        {
            get { return Validator.Get(this._uttl, Strings.KthKoefs, 15); }
            set { this._uttl = Validator.Set(value, Strings.KthKoefs, this._uttl, 15); }
        }

        /// <summary>
        /// KTHX коэффициент
        /// </summary>
        [BindingProperty(3)]
        [XmlIgnore]
        public string Xkoef
        {
            get { return Validator.Get(this._uttx, Strings.KthKoefs, 15); }
            set { this._uttx = Validator.Set(value, Strings.KthKoefs, this._uttx, 15); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "НеиспрТНф")]
        public string NeisprTn
        {
            get { return Validator.Get(this._polL, Strings.InputSignals); }
            set { this._polL = Validator.Set(value, Strings.InputSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "НеиспрТНn")]
        public string NeisprTnx
        {
            get { return Validator.Get(this._polX, Strings.InputSignals); }
            set { this._polX = Validator.Set(value, Strings.InputSignals); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Контроль_3U0")]
        public bool d3U0Enable
        {
            get { return Common.GetBit(this._conf, 0); }
            set { this._conf = Common.SetBit(this._conf, 0, value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "d3U0")]
        public double d3U0
        {
            get { return ValuesConverterCommon.GetUstavka256(this._deltaU); }
            set { this._deltaU = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Td3U0")]
        public int d3U0Time
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeDeltaU); }
            set { this._timeDeltaU = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "d3U0Блокировка")]
        public string d3U0Block
        {
            get { return Validator.Get(this._blockDeltaU, Strings.ExtDefSignals); }
            set { this._blockDeltaU = Validator.Set(value, Strings.ExtDefSignals); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Контроль_несиметрии")]
        public bool UnsEnable
        {
            get { return Common.GetBit(this._conf, 1); }
            set { this._conf = Common.SetBit(this._conf, 1, value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Uns")]
        public ushort Uns
        {
            get { return this._divU; }
            set { this._divU = value; }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Tuns")]
        public int UnsTime
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeDivU); }
            set { this._timeDivU = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "UnsБлокировка")]
        public string UnsBlock
        {
            get { return Validator.Get(this._blockDivU, Strings.ExtDefSignals); }
            set { this._blockDivU = Validator.Set(value, Strings.ExtDefSignals); }
        }

        public void SetKthl(ushort val)
        {
            this._uttl = val;
        }

        public void SetKthx(ushort val)
        {
            this._uttx = val;
        }
    }
}
