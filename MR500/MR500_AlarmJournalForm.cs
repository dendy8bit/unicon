using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MR500.Properties;

namespace BEMN.MR500
{    
    public partial class AlarmJournalForm : Form , IFormView
    {
        private MR500 _device;
        private DataTable _table;


        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._journalProgress.Maximum = MR500.ALARMJOURNAL_RECORD_CNT;
            this._device.AlarmJournalLoadOk += HandlerHelper.CreateHandler(this, this.OnJournalReadOk);
            this._device.AlarmJournalRecordLoadOk += new IndexHandler(this._device_AlarmJournalRecordLoadOk);
            this._device.AlarmJournalRecordLoadFail += new IndexHandler(this._device_AlarmJournalRecordLoadFail);
        }

        private void OnJournalReadOk()
        {
            this._journalProgress.Value = this._journalProgress.Maximum;
            if (this._device.AlarmJournal.Count == 0)
            {
                this._journalCntLabel.Text = "������ ����";
            }
        }

        void _device_AlarmJournalRecordLoadFail(object sender, int index)
        {
            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this._readBut.Enabled = true;
        }

        void _device_AlarmJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnAlarmJournalRecordLoadOk), index );
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnAlarmJournalRecordLoadOk(int i)
        {
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            this._journalGrid.Rows.Add(i + 1,
                this._device.AlarmJournal[i].time,
                this._device.AlarmJournal[i].msg,
                this._device.AlarmJournal[i].code,
                this._device.AlarmJournal[i].type_value,
                this._device.AlarmJournal[i].Ia,
                this._device.AlarmJournal[i].Ib,
                this._device.AlarmJournal[i].Ic,
                this._device.AlarmJournal[i].Io,
                this._device.AlarmJournal[i].Ig,
                this._device.AlarmJournal[i].I0,
                this._device.AlarmJournal[i].I1,
                this._device.AlarmJournal[i].I2,
                this._device.AlarmJournal[i].inSignals1,
                this._device.AlarmJournal[i].inSignals2);
            if (i == 31)
            {
                this._readBut.Enabled = true;
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return (Image)BEMN.MR500.Properties.Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readBut_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._readBut.Enabled = false;
                this._device.AlarmJournal = new MR500.CAlarmJournal(this._device);
                this._device.RemoveAlarmJournal();
                this._journalProgress.Value = 0;
                this._journalGrid.Rows.Clear();
                this._device.LoadAlarmJournal();
            }
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           this._device.RemoveAlarmJournal();
        }

        private void AlarmJournalForm_Activated(object sender, EventArgs e)
        {
            this._device.SuspendAlarmJournal(false);
        }

        private void AlarmJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._device.SuspendAlarmJournal(true);
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            this.FillTable();
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                _table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void FillTable()
        {
            this._table = CreateAlarmJournalTable();
            List<object> row = new List<object>();

            for (int i = 0; i < this._journalGrid.Rows.Count; i++)
            {
                row.Clear();
                for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                {
                    row.Add(this._journalGrid[j, i].Value);
                }
                _table.Rows.Add(row.ToArray());
            }
        }

        private static DataTable CreateAlarmJournalTable()
        {
            DataTable table = new DataTable("��500_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("���_�����������");
            table.Columns.Add("���_�����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("Io");
            table.Columns.Add("Ig");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("��.�������_1-8");
            table.Columns.Add("��.�������_9-16");
            return table;
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = CreateAlarmJournalTable();
          
            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._journalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            List<object> row = new List<object>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row.Clear();
                row.AddRange(table.Rows[i].ItemArray);
                this._journalGrid.Rows.Add(row.ToArray());

            }
        }

        private void _saveToHtml_Click(object sender, EventArgs e)
        {

            this._saveSysJouranlHtmlDlg.FileName = string.Format("������ ������ �� 500 v{0}", _device.DeviceVersion);
            try
            {
                this.FillTable();
                if (DialogResult.OK == _saveSysJouranlHtmlDlg.ShowDialog())
                {
                    HtmlExport.Export(_table, this._saveSysJouranlHtmlDlg.FileName, Resources.Mr500Aj);
                    _journalCntLabel.Text = "������ ��������!";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, exception.Message, "������ ����������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _journalCntLabel.Text = "������ �� ��������!";
                throw;
            }

        }
    }
}