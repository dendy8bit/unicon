using System;
using System.Runtime.InteropServices;

namespace BEMN.MR500
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OscJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] datatime;
        public int ready;
        public int point;
        public int begin;
        public int len;
        public int after;
        public Int32 alm;
        public Int32 rez;
    }
}