namespace BEMN.MR500
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetPointStruct = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._speedupTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._speedupOnCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this._speedupNumberCombo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._applyLogicSignalsBut = new System.Windows.Forms.Button();
            this._logicChannelsCombo = new System.Windows.Forms.ComboBox();
            this._logicSignalsDataGrid = new System.Windows.Forms.DataGridView();
            this._diskretChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._diskretValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._constraintGroupCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._indicationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._dispepairReleDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._outputLogicCheckList = new System.Windows.Forms.CheckedListBox();
            this._outputLogicAcceptBut = new System.Windows.Forms.Button();
            this._outputLogicCombo = new System.Windows.Forms.ComboBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._defendingTabPage = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._newTabControl = new System.Windows.Forms.TabControl();
            this._externalTabPage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefEntryCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticTabPage = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this._CAPV_TimeBox = new System.Windows.Forms.MaskedTextBox();
            this._CAPV_BlockingCombo = new System.Windows.Forms.ComboBox();
            this._CAPV_EntryCombo = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this._AVR_OffTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._AVR_ReturnTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._AVR_ReturnNumberCombo = new System.Windows.Forms.ComboBox();
            this._AVR_StartTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._AVR_BlockingCombo = new System.Windows.Forms.ComboBox();
            this._AVR_StartNumberCombo = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this._ACR_TimeBox = new System.Windows.Forms.MaskedTextBox();
            this._ACR_BlockingCombo = new System.Windows.Forms.ComboBox();
            this._ACR_EntryCombo = new System.Windows.Forms.ComboBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._APV_Crat2TimeBox = new System.Windows.Forms.MaskedTextBox();
            this._APV_Crat1TimeBox = new System.Windows.Forms.MaskedTextBox();
            this._APV_ReadyTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._APV_BlockingTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._APV_SelfresetCombo = new System.Windows.Forms.ComboBox();
            this._APV_BlockingCombo = new System.Windows.Forms.ComboBox();
            this._APV_ConfigCombo = new System.Windows.Forms.ComboBox();
            this._tokTabPage = new System.Windows.Forms.TabPage();
            this._tokDefenseGrid3 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockingCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseSignalCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseAPVBCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseUROVCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseGrid2 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseSignalCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseAPVCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseUROVCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseSignalCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseKoeffCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseAPVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._changeGroupBtn = new System.Windows.Forms.Button();
            this._reserveRadioBtnGroup = new System.Windows.Forms.RadioButton();
            this._mainRadioBtnGroup = new System.Windows.Forms.RadioButton();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg1 = new System.Windows.Forms.OpenFileDialog();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._defendingTabPage.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this._newTabControl.SuspendLayout();
            this._externalTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._automaticTabPage.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this._tokTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            this.groupBox19.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._defendingTabPage);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(813, 487);
            this._tabControl.TabIndex = 0;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.resetSetPointStruct,
            this.readFromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // resetSetPointStruct
            // 
            this.resetSetPointStruct.Name = "resetSetPointStruct";
            this.resetSetPointStruct.Size = new System.Drawing.Size(212, 22);
            this.resetSetPointStruct.Text = "��������� �������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox5);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(805, 461);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this._speedupTimeBox);
            this.groupBox2.Controls.Add(this._speedupOnCombo);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._speedupNumberCombo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(244, 261);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(236, 89);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "��������� �����";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "������������";
            // 
            // _speedupTimeBox
            // 
            this._speedupTimeBox.Location = new System.Drawing.Point(118, 57);
            this._speedupTimeBox.Name = "_speedupTimeBox";
            this._speedupTimeBox.Size = new System.Drawing.Size(112, 20);
            this._speedupTimeBox.TabIndex = 24;
            this._speedupTimeBox.Tag = "3000000";
            this._speedupTimeBox.Text = "0";
            this._speedupTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _speedupOnCombo
            // 
            this._speedupOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._speedupOnCombo.FormattingEnabled = true;
            this._speedupOnCombo.Location = new System.Drawing.Point(118, 38);
            this._speedupOnCombo.Name = "_speedupOnCombo";
            this._speedupOnCombo.Size = new System.Drawing.Size(112, 21);
            this._speedupOnCombo.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "�� ���������";
            // 
            // _speedupNumberCombo
            // 
            this._speedupNumberCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._speedupNumberCombo.FormattingEnabled = true;
            this._speedupNumberCombo.Location = new System.Drawing.Point(118, 18);
            this._speedupNumberCombo.Name = "_speedupNumberCombo";
            this._speedupNumberCombo.Size = new System.Drawing.Size(112, 21);
            this._speedupNumberCombo.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "����� �����";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(244, 150);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(236, 105);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(104, 75);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(104, 56);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 60);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(104, 37);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(104, 17);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._applyLogicSignalsBut);
            this.groupBox5.Controls.Add(this._logicChannelsCombo);
            this.groupBox5.Controls.Add(this._logicSignalsDataGrid);
            this.groupBox5.Location = new System.Drawing.Point(486, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(151, 426);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "���������� �������";
            // 
            // _applyLogicSignalsBut
            // 
            this._applyLogicSignalsBut.Location = new System.Drawing.Point(43, 397);
            this._applyLogicSignalsBut.Name = "_applyLogicSignalsBut";
            this._applyLogicSignalsBut.Size = new System.Drawing.Size(62, 23);
            this._applyLogicSignalsBut.TabIndex = 3;
            this._applyLogicSignalsBut.Text = "�������";
            this._applyLogicSignalsBut.UseVisualStyleBackColor = true;
            this._applyLogicSignalsBut.Click += new System.EventHandler(this._applyLogicSignalsBut_Click);
            // 
            // _logicChannelsCombo
            // 
            this._logicChannelsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._logicChannelsCombo.FormattingEnabled = true;
            this._logicChannelsCombo.Items.AddRange(new object[] {
            "�1 �",
            "�2 �",
            "�3 �",
            "�4 �",
            "�5 ���",
            "�6 ���",
            "�7 ���",
            "�8 ���"});
            this._logicChannelsCombo.Location = new System.Drawing.Point(43, 18);
            this._logicChannelsCombo.Name = "_logicChannelsCombo";
            this._logicChannelsCombo.Size = new System.Drawing.Size(64, 21);
            this._logicChannelsCombo.TabIndex = 1;
            this._logicChannelsCombo.SelectedIndexChanged += new System.EventHandler(this._logicChannelsCombo_SelectedIndexChanged);
            // 
            // _logicSignalsDataGrid
            // 
            this._logicSignalsDataGrid.AllowUserToAddRows = false;
            this._logicSignalsDataGrid.AllowUserToDeleteRows = false;
            this._logicSignalsDataGrid.AllowUserToResizeColumns = false;
            this._logicSignalsDataGrid.AllowUserToResizeRows = false;
            this._logicSignalsDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._logicSignalsDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._logicSignalsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicSignalsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._diskretChannelCol,
            this._diskretValueCol});
            this._logicSignalsDataGrid.Location = new System.Drawing.Point(11, 45);
            this._logicSignalsDataGrid.MultiSelect = false;
            this._logicSignalsDataGrid.Name = "_logicSignalsDataGrid";
            this._logicSignalsDataGrid.RowHeadersVisible = false;
            this._logicSignalsDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._logicSignalsDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._logicSignalsDataGrid.RowTemplate.Height = 20;
            this._logicSignalsDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._logicSignalsDataGrid.ShowCellErrors = false;
            this._logicSignalsDataGrid.ShowCellToolTips = false;
            this._logicSignalsDataGrid.ShowEditingIcon = false;
            this._logicSignalsDataGrid.ShowRowErrors = false;
            this._logicSignalsDataGrid.Size = new System.Drawing.Size(132, 346);
            this._logicSignalsDataGrid.TabIndex = 0;
            // 
            // _diskretChannelCol
            // 
            this._diskretChannelCol.HeaderText = "�";
            this._diskretChannelCol.Name = "_diskretChannelCol";
            this._diskretChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._diskretChannelCol.Width = 24;
            // 
            // _diskretValueCol
            // 
            this._diskretValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._diskretValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._diskretValueCol.HeaderText = "��������";
            this._diskretValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._diskretValueCol.Name = "_diskretValueCol";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(244, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(236, 140);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 117);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "�������, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 97);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����������, ��";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(159, 94);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(159, 113);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(71, 20);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(159, 75);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(159, 56);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 59);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(159, 37);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(159, 18);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._constraintGroupCombo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._indicationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(8, 93);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 141);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // _constraintGroupCombo
            // 
            this._constraintGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroupCombo.FormattingEnabled = true;
            this._constraintGroupCombo.Location = new System.Drawing.Point(153, 114);
            this._constraintGroupCombo.Name = "_constraintGroupCombo";
            this._constraintGroupCombo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroupCombo.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 121);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "������. �������";
            // 
            // _indicationCombo
            // 
            this._indicationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._indicationCombo.FormattingEnabled = true;
            this._indicationCombo.Location = new System.Drawing.Point(153, 95);
            this._indicationCombo.Name = "_indicationCombo";
            this._indicationCombo.Size = new System.Drawing.Size(71, 21);
            this._indicationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 98);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ���������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(153, 54);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(71, 21);
            this._extOffCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(153, 74);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(71, 21);
            this._extOnCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(153, 13);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOffCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(153, 33);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOnCombo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 82);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������, I�";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(153, 55);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(71, 20);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��, A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����, A";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(153, 35);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(71, 20);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "100";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(153, 15);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(71, 20);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "1500";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._TT_Box, "��������� ��� ��������������");
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox18);
            this._outputSignalsPage.Controls.Add(this.groupBox10);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(805, 461);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.groupBox17);
            this.groupBox18.Controls.Add(this._dispepairCheckList);
            this.groupBox18.Location = new System.Drawing.Point(582, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(202, 206);
            this.groupBox18.TabIndex = 27;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "�������������";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._dispepairReleDurationBox);
            this.groupBox17.Location = new System.Drawing.Point(12, 19);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(131, 49);
            this.groupBox17.TabIndex = 26;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "������, ��";
            // 
            // _dispepairReleDurationBox
            // 
            this._dispepairReleDurationBox.Location = new System.Drawing.Point(9, 18);
            this._dispepairReleDurationBox.Name = "_dispepairReleDurationBox";
            this._dispepairReleDurationBox.Size = new System.Drawing.Size(116, 20);
            this._dispepairReleDurationBox.TabIndex = 18;
            this._dispepairReleDurationBox.Tag = "3000000";
            this._dispepairReleDurationBox.Text = "0";
            this._dispepairReleDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "���������� (������.1)",
            "����������� (������.2)",
            "������ (������.3)",
            "������ (������.4)",
            "����������� (������.5)",
            "��������� ����. (������.6)",
            "�� �����������  (������.7)",
            "����� ����. (������.8)"});
            this._dispepairCheckList.Location = new System.Drawing.Point(6, 74);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(190, 124);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._outputLogicCheckList);
            this.groupBox10.Controls.Add(this._outputLogicAcceptBut);
            this.groupBox10.Controls.Add(this._outputLogicCombo);
            this.groupBox10.Location = new System.Drawing.Point(438, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 425);
            this.groupBox10.TabIndex = 5;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "���������� �������";
            // 
            // _outputLogicCheckList
            // 
            this._outputLogicCheckList.CheckOnClick = true;
            this._outputLogicCheckList.FormattingEnabled = true;
            this._outputLogicCheckList.Location = new System.Drawing.Point(3, 52);
            this._outputLogicCheckList.Name = "_outputLogicCheckList";
            this._outputLogicCheckList.ScrollAlwaysVisible = true;
            this._outputLogicCheckList.Size = new System.Drawing.Size(135, 334);
            this._outputLogicCheckList.TabIndex = 4;
            // 
            // _outputLogicAcceptBut
            // 
            this._outputLogicAcceptBut.Location = new System.Drawing.Point(35, 392);
            this._outputLogicAcceptBut.Name = "_outputLogicAcceptBut";
            this._outputLogicAcceptBut.Size = new System.Drawing.Size(59, 23);
            this._outputLogicAcceptBut.TabIndex = 3;
            this._outputLogicAcceptBut.Text = "�������";
            this._outputLogicAcceptBut.UseVisualStyleBackColor = true;
            this._outputLogicAcceptBut.Click += new System.EventHandler(this._outputLogicAcceptBut_Click);
            // 
            // _outputLogicCombo
            // 
            this._outputLogicCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._outputLogicCombo.FormattingEnabled = true;
            this._outputLogicCombo.Items.AddRange(new object[] {
            "��� 1",
            "��� 2 ",
            "��� 3 ",
            "��� 4 ",
            "��� 5",
            "��� 6",
            "��� 7 ",
            "��� 8"});
            this._outputLogicCombo.Location = new System.Drawing.Point(35, 19);
            this._outputLogicCombo.Name = "_outputLogicCombo";
            this._outputLogicCombo.Size = new System.Drawing.Size(66, 21);
            this._outputLogicCombo.TabIndex = 1;
            this._outputLogicCombo.SelectedIndexChanged += new System.EventHandler(this._outputLogicCombo_SelectedIndexChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(8, 239);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 154);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(418, 135);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(8, 5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(424, 234);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            this._outputReleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputReleGrid.Location = new System.Drawing.Point(3, 16);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(418, 215);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.NullValue = null;
            this._releImpulseCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._releImpulseCol.HeaderText = "������, ��";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _defendingTabPage
            // 
            this._defendingTabPage.Controls.Add(this.groupBox20);
            this._defendingTabPage.Controls.Add(this.groupBox19);
            this._defendingTabPage.Location = new System.Drawing.Point(4, 22);
            this._defendingTabPage.Name = "_defendingTabPage";
            this._defendingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._defendingTabPage.Size = new System.Drawing.Size(805, 461);
            this._defendingTabPage.TabIndex = 5;
            this._defendingTabPage.Text = "������";
            this._defendingTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._newTabControl);
            this.groupBox20.Location = new System.Drawing.Point(6, 80);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(756, 375);
            this.groupBox20.TabIndex = 1;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "������";
            // 
            // _newTabControl
            // 
            this._newTabControl.Controls.Add(this._externalTabPage);
            this._newTabControl.Controls.Add(this._automaticTabPage);
            this._newTabControl.Controls.Add(this._tokTabPage);
            this._newTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._newTabControl.Location = new System.Drawing.Point(3, 16);
            this._newTabControl.Name = "_newTabControl";
            this._newTabControl.SelectedIndex = 0;
            this._newTabControl.Size = new System.Drawing.Size(750, 356);
            this._newTabControl.TabIndex = 0;
            // 
            // _externalTabPage
            // 
            this._externalTabPage.Controls.Add(this._externalDefenseGrid);
            this._externalTabPage.Location = new System.Drawing.Point(4, 22);
            this._externalTabPage.Name = "_externalTabPage";
            this._externalTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._externalTabPage.Size = new System.Drawing.Size(742, 330);
            this._externalTabPage.TabIndex = 0;
            this._externalTabPage.Text = "������� ������";
            this._externalTabPage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefEntryCol,
            this._exDefWorkingTimeCol,
            this._exDefAPVcol,
            this._exDefUROVcol});
            this._externalDefenseGrid.Location = new System.Drawing.Point(7, 6);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.Size = new System.Drawing.Size(477, 208);
            this._externalDefenseGrid.TabIndex = 1;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDefNumberCol.Width = 24;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 48;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 74;
            // 
            // _exDefEntryCol
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefEntryCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._exDefEntryCol.HeaderText = "����";
            this._exDefEntryCol.Name = "_exDefEntryCol";
            this._exDefEntryCol.Width = 37;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.NullValue = "0";
            this._exDefWorkingTimeCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._exDefWorkingTimeCol.HeaderText = "��������";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 35;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 43;
            // 
            // _automaticTabPage
            // 
            this._automaticTabPage.Controls.Add(this.groupBox12);
            this._automaticTabPage.Controls.Add(this.groupBox11);
            this._automaticTabPage.Controls.Add(this.groupBox6);
            this._automaticTabPage.Controls.Add(this.groupBox14);
            this._automaticTabPage.Location = new System.Drawing.Point(4, 22);
            this._automaticTabPage.Name = "_automaticTabPage";
            this._automaticTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._automaticTabPage.Size = new System.Drawing.Size(742, 330);
            this._automaticTabPage.TabIndex = 1;
            this._automaticTabPage.Text = "����������";
            this._automaticTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label29);
            this.groupBox12.Controls.Add(this.label30);
            this.groupBox12.Controls.Add(this.label31);
            this.groupBox12.Controls.Add(this._CAPV_TimeBox);
            this.groupBox12.Controls.Add(this._CAPV_BlockingCombo);
            this.groupBox12.Controls.Add(this._CAPV_EntryCombo);
            this.groupBox12.Location = new System.Drawing.Point(272, 182);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(233, 93);
            this.groupBox12.TabIndex = 29;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "����";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 62);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "��������";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 41);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(68, 13);
            this.label30.TabIndex = 38;
            this.label30.Text = "����������";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 13);
            this.label31.TabIndex = 37;
            this.label31.Text = "����";
            // 
            // _CAPV_TimeBox
            // 
            this._CAPV_TimeBox.Location = new System.Drawing.Point(137, 59);
            this._CAPV_TimeBox.Name = "_CAPV_TimeBox";
            this._CAPV_TimeBox.Size = new System.Drawing.Size(87, 20);
            this._CAPV_TimeBox.TabIndex = 18;
            this._CAPV_TimeBox.Tag = "3000000";
            this._CAPV_TimeBox.Text = "0";
            this._CAPV_TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _CAPV_BlockingCombo
            // 
            this._CAPV_BlockingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CAPV_BlockingCombo.FormattingEnabled = true;
            this._CAPV_BlockingCombo.Location = new System.Drawing.Point(137, 38);
            this._CAPV_BlockingCombo.Name = "_CAPV_BlockingCombo";
            this._CAPV_BlockingCombo.Size = new System.Drawing.Size(87, 21);
            this._CAPV_BlockingCombo.TabIndex = 3;
            // 
            // _CAPV_EntryCombo
            // 
            this._CAPV_EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CAPV_EntryCombo.FormattingEnabled = true;
            this._CAPV_EntryCombo.Location = new System.Drawing.Point(137, 19);
            this._CAPV_EntryCombo.Name = "_CAPV_EntryCombo";
            this._CAPV_EntryCombo.Size = new System.Drawing.Size(87, 21);
            this._CAPV_EntryCombo.TabIndex = 1;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.label36);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.label32);
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this._AVR_OffTimeBox);
            this.groupBox11.Controls.Add(this._AVR_ReturnTimeBox);
            this.groupBox11.Controls.Add(this._AVR_ReturnNumberCombo);
            this.groupBox11.Controls.Add(this._AVR_StartTimeBox);
            this.groupBox11.Controls.Add(this._AVR_BlockingCombo);
            this.groupBox11.Controls.Add(this._AVR_StartNumberCombo);
            this.groupBox11.Location = new System.Drawing.Point(272, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(233, 160);
            this.groupBox11.TabIndex = 28;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "���";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 104);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(110, 13);
            this.label35.TabIndex = 45;
            this.label35.Text = "�������� ��������";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 124);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(111, 13);
            this.label36.TabIndex = 44;
            this.label36.Text = "�������� ����. ���.";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 83);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(49, 13);
            this.label37.TabIndex = 43;
            this.label37.Text = "�������";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(104, 13);
            this.label32.TabIndex = 42;
            this.label32.Text = "�������� �������";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 62);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 13);
            this.label33.TabIndex = 41;
            this.label33.Text = "����������";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 22);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(81, 13);
            this.label34.TabIndex = 40;
            this.label34.Text = "������������";
            // 
            // _AVR_OffTimeBox
            // 
            this._AVR_OffTimeBox.Location = new System.Drawing.Point(137, 121);
            this._AVR_OffTimeBox.Name = "_AVR_OffTimeBox";
            this._AVR_OffTimeBox.Size = new System.Drawing.Size(87, 20);
            this._AVR_OffTimeBox.TabIndex = 27;
            this._AVR_OffTimeBox.Tag = "3000000";
            this._AVR_OffTimeBox.Text = "0";
            this._AVR_OffTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _AVR_ReturnTimeBox
            // 
            this._AVR_ReturnTimeBox.Location = new System.Drawing.Point(137, 101);
            this._AVR_ReturnTimeBox.Name = "_AVR_ReturnTimeBox";
            this._AVR_ReturnTimeBox.Size = new System.Drawing.Size(87, 20);
            this._AVR_ReturnTimeBox.TabIndex = 25;
            this._AVR_ReturnTimeBox.Tag = "3000000";
            this._AVR_ReturnTimeBox.Text = "0";
            this._AVR_ReturnTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _AVR_ReturnNumberCombo
            // 
            this._AVR_ReturnNumberCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVR_ReturnNumberCombo.FormattingEnabled = true;
            this._AVR_ReturnNumberCombo.Location = new System.Drawing.Point(137, 80);
            this._AVR_ReturnNumberCombo.Name = "_AVR_ReturnNumberCombo";
            this._AVR_ReturnNumberCombo.Size = new System.Drawing.Size(87, 21);
            this._AVR_ReturnNumberCombo.TabIndex = 23;
            // 
            // _AVR_StartTimeBox
            // 
            this._AVR_StartTimeBox.Location = new System.Drawing.Point(137, 39);
            this._AVR_StartTimeBox.Name = "_AVR_StartTimeBox";
            this._AVR_StartTimeBox.Size = new System.Drawing.Size(87, 20);
            this._AVR_StartTimeBox.TabIndex = 18;
            this._AVR_StartTimeBox.Tag = "3000000";
            this._AVR_StartTimeBox.Text = "0";
            this._AVR_StartTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _AVR_BlockingCombo
            // 
            this._AVR_BlockingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVR_BlockingCombo.FormattingEnabled = true;
            this._AVR_BlockingCombo.Location = new System.Drawing.Point(137, 59);
            this._AVR_BlockingCombo.Name = "_AVR_BlockingCombo";
            this._AVR_BlockingCombo.Size = new System.Drawing.Size(87, 21);
            this._AVR_BlockingCombo.TabIndex = 3;
            // 
            // _AVR_StartNumberCombo
            // 
            this._AVR_StartNumberCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVR_StartNumberCombo.FormattingEnabled = true;
            this._AVR_StartNumberCombo.Location = new System.Drawing.Point(137, 18);
            this._AVR_StartNumberCombo.Name = "_AVR_StartNumberCombo";
            this._AVR_StartNumberCombo.Size = new System.Drawing.Size(87, 21);
            this._AVR_StartNumberCombo.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this._ACR_TimeBox);
            this.groupBox6.Controls.Add(this._ACR_BlockingCombo);
            this.groupBox6.Controls.Add(this._ACR_EntryCombo);
            this.groupBox6.Location = new System.Drawing.Point(16, 182);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(238, 93);
            this.groupBox6.TabIndex = 27;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(10, 62);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(60, 13);
            this.label26.TabIndex = 36;
            this.label26.Text = "��������";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(10, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 13);
            this.label27.TabIndex = 35;
            this.label27.Text = "����������";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(10, 22);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 13);
            this.label28.TabIndex = 34;
            this.label28.Text = "����";
            // 
            // _ACR_TimeBox
            // 
            this._ACR_TimeBox.Location = new System.Drawing.Point(146, 59);
            this._ACR_TimeBox.Name = "_ACR_TimeBox";
            this._ACR_TimeBox.Size = new System.Drawing.Size(87, 20);
            this._ACR_TimeBox.TabIndex = 18;
            this._ACR_TimeBox.Tag = "3000000";
            this._ACR_TimeBox.Text = "0";
            this._ACR_TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _ACR_BlockingCombo
            // 
            this._ACR_BlockingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ACR_BlockingCombo.FormattingEnabled = true;
            this._ACR_BlockingCombo.Location = new System.Drawing.Point(146, 38);
            this._ACR_BlockingCombo.Name = "_ACR_BlockingCombo";
            this._ACR_BlockingCombo.Size = new System.Drawing.Size(87, 21);
            this._ACR_BlockingCombo.TabIndex = 3;
            // 
            // _ACR_EntryCombo
            // 
            this._ACR_EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ACR_EntryCombo.FormattingEnabled = true;
            this._ACR_EntryCombo.Location = new System.Drawing.Point(146, 19);
            this._ACR_EntryCombo.Name = "_ACR_EntryCombo";
            this._ACR_EntryCombo.Size = new System.Drawing.Size(87, 21);
            this._ACR_EntryCombo.TabIndex = 1;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label25);
            this.groupBox14.Controls.Add(this.label24);
            this.groupBox14.Controls.Add(this.label23);
            this.groupBox14.Controls.Add(this.label22);
            this.groupBox14.Controls.Add(this.label21);
            this.groupBox14.Controls.Add(this.label19);
            this.groupBox14.Controls.Add(this.label17);
            this.groupBox14.Controls.Add(this._APV_Crat2TimeBox);
            this.groupBox14.Controls.Add(this._APV_Crat1TimeBox);
            this.groupBox14.Controls.Add(this._APV_ReadyTimeBox);
            this.groupBox14.Controls.Add(this._APV_BlockingTimeBox);
            this.groupBox14.Controls.Add(this._APV_SelfresetCombo);
            this.groupBox14.Controls.Add(this._APV_BlockingCombo);
            this.groupBox14.Controls.Add(this._APV_ConfigCombo);
            this.groupBox14.Location = new System.Drawing.Point(16, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(238, 160);
            this.groupBox14.TabIndex = 26;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = " ���";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(10, 136);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(113, 13);
            this.label25.TabIndex = 33;
            this.label25.Text = "������ �� ��������.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(10, 116);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 13);
            this.label24.TabIndex = 32;
            this.label24.Text = "����� 2 �����";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(10, 98);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 13);
            this.label23.TabIndex = 31;
            this.label23.Text = "����� 1 �����";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(10, 80);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 13);
            this.label22.TabIndex = 30;
            this.label22.Text = "����� ����������";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(103, 13);
            this.label21.TabIndex = 29;
            this.label21.Text = "����� ����������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "����������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "������������";
            // 
            // _APV_Crat2TimeBox
            // 
            this._APV_Crat2TimeBox.Location = new System.Drawing.Point(146, 113);
            this._APV_Crat2TimeBox.Name = "_APV_Crat2TimeBox";
            this._APV_Crat2TimeBox.Size = new System.Drawing.Size(87, 20);
            this._APV_Crat2TimeBox.TabIndex = 21;
            this._APV_Crat2TimeBox.Tag = "3000000";
            this._APV_Crat2TimeBox.Text = "0";
            this._APV_Crat2TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _APV_Crat1TimeBox
            // 
            this._APV_Crat1TimeBox.Location = new System.Drawing.Point(146, 95);
            this._APV_Crat1TimeBox.Name = "_APV_Crat1TimeBox";
            this._APV_Crat1TimeBox.Size = new System.Drawing.Size(87, 20);
            this._APV_Crat1TimeBox.TabIndex = 20;
            this._APV_Crat1TimeBox.Tag = "3000000";
            this._APV_Crat1TimeBox.Text = "0";
            this._APV_Crat1TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _APV_ReadyTimeBox
            // 
            this._APV_ReadyTimeBox.Location = new System.Drawing.Point(146, 77);
            this._APV_ReadyTimeBox.Name = "_APV_ReadyTimeBox";
            this._APV_ReadyTimeBox.Size = new System.Drawing.Size(87, 20);
            this._APV_ReadyTimeBox.TabIndex = 19;
            this._APV_ReadyTimeBox.Tag = "3000000";
            this._APV_ReadyTimeBox.Text = "0";
            this._APV_ReadyTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _APV_BlockingTimeBox
            // 
            this._APV_BlockingTimeBox.Location = new System.Drawing.Point(146, 59);
            this._APV_BlockingTimeBox.Name = "_APV_BlockingTimeBox";
            this._APV_BlockingTimeBox.Size = new System.Drawing.Size(87, 20);
            this._APV_BlockingTimeBox.TabIndex = 18;
            this._APV_BlockingTimeBox.Tag = "3000000";
            this._APV_BlockingTimeBox.Text = "0";
            this._APV_BlockingTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _APV_SelfresetCombo
            // 
            this._APV_SelfresetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APV_SelfresetCombo.Location = new System.Drawing.Point(146, 133);
            this._APV_SelfresetCombo.Name = "_APV_SelfresetCombo";
            this._APV_SelfresetCombo.Size = new System.Drawing.Size(87, 21);
            this._APV_SelfresetCombo.TabIndex = 17;
            // 
            // _APV_BlockingCombo
            // 
            this._APV_BlockingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APV_BlockingCombo.FormattingEnabled = true;
            this._APV_BlockingCombo.Location = new System.Drawing.Point(146, 38);
            this._APV_BlockingCombo.Name = "_APV_BlockingCombo";
            this._APV_BlockingCombo.Size = new System.Drawing.Size(87, 21);
            this._APV_BlockingCombo.TabIndex = 3;
            // 
            // _APV_ConfigCombo
            // 
            this._APV_ConfigCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APV_ConfigCombo.FormattingEnabled = true;
            this._APV_ConfigCombo.Location = new System.Drawing.Point(146, 19);
            this._APV_ConfigCombo.Name = "_APV_ConfigCombo";
            this._APV_ConfigCombo.Size = new System.Drawing.Size(87, 21);
            this._APV_ConfigCombo.TabIndex = 1;
            // 
            // _tokTabPage
            // 
            this._tokTabPage.Controls.Add(this._tokDefenseGrid3);
            this._tokTabPage.Controls.Add(this._tokDefenseGrid2);
            this._tokTabPage.Controls.Add(this._tokDefenseGrid1);
            this._tokTabPage.Location = new System.Drawing.Point(4, 22);
            this._tokTabPage.Name = "_tokTabPage";
            this._tokTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._tokTabPage.Size = new System.Drawing.Size(742, 330);
            this._tokTabPage.TabIndex = 2;
            this._tokTabPage.Text = "������� ������";
            this._tokTabPage.UseVisualStyleBackColor = true;
            // 
            // _tokDefenseGrid3
            // 
            this._tokDefenseGrid3.AllowUserToAddRows = false;
            this._tokDefenseGrid3.AllowUserToDeleteRows = false;
            this._tokDefenseGrid3.AllowUserToResizeColumns = false;
            this._tokDefenseGrid3.AllowUserToResizeRows = false;
            this._tokDefenseGrid3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid3.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol3,
            this._tokDefenseModeCol3,
            this._tokDefenseBlockingCol3,
            this._tokDefenseSignalCol3,
            this._tokDefenseConstraintCol3,
            this._tokDefenseFeatureCol3,
            this._tokDefenseTimeCol3,
            this._tokDefenseAPVBCol3,
            this._tokDefenseUROVCol3,
            this._tokDefenseSpeedupCol3,
            this._tokDefenseSpeedupTimeCol3});
            this._tokDefenseGrid3.Location = new System.Drawing.Point(6, 223);
            this._tokDefenseGrid3.Name = "_tokDefenseGrid3";
            this._tokDefenseGrid3.RowHeadersVisible = false;
            this._tokDefenseGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid3.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this._tokDefenseGrid3.RowTemplate.Height = 24;
            this._tokDefenseGrid3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid3.Size = new System.Drawing.Size(730, 104);
            this._tokDefenseGrid3.TabIndex = 13;
            this._tokDefenseGrid3.Scroll += new System.Windows.Forms.ScrollEventHandler(this._tokDefenseGrid3_Scroll);
            // 
            // _tokDefenseNameCol3
            // 
            this._tokDefenseNameCol3.Frozen = true;
            this._tokDefenseNameCol3.HeaderText = "";
            this._tokDefenseNameCol3.Name = "_tokDefenseNameCol3";
            this._tokDefenseNameCol3.ReadOnly = true;
            this._tokDefenseNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol3.Width = 5;
            // 
            // _tokDefenseModeCol3
            // 
            this._tokDefenseModeCol3.HeaderText = "�����";
            this._tokDefenseModeCol3.Name = "_tokDefenseModeCol3";
            this._tokDefenseModeCol3.Width = 48;
            // 
            // _tokDefenseBlockingCol3
            // 
            this._tokDefenseBlockingCol3.HeaderText = "����������";
            this._tokDefenseBlockingCol3.Name = "_tokDefenseBlockingCol3";
            this._tokDefenseBlockingCol3.Width = 74;
            // 
            // _tokDefenseSignalCol3
            // 
            this._tokDefenseSignalCol3.HeaderText = "��� �������";
            this._tokDefenseSignalCol3.Name = "_tokDefenseSignalCol3";
            this._tokDefenseSignalCol3.Width = 76;
            // 
            // _tokDefenseConstraintCol3
            // 
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this._tokDefenseConstraintCol3.DefaultCellStyle = dataGridViewCellStyle8;
            this._tokDefenseConstraintCol3.HeaderText = "�������, I�";
            this._tokDefenseConstraintCol3.Name = "_tokDefenseConstraintCol3";
            this._tokDefenseConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseConstraintCol3.Width = 71;
            // 
            // _tokDefenseFeatureCol3
            // 
            this._tokDefenseFeatureCol3.HeaderText = "���-��";
            this._tokDefenseFeatureCol3.Name = "_tokDefenseFeatureCol3";
            this._tokDefenseFeatureCol3.ReadOnly = true;
            this._tokDefenseFeatureCol3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseFeatureCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseFeatureCol3.Width = 47;
            // 
            // _tokDefenseTimeCol3
            // 
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = null;
            this._tokDefenseTimeCol3.DefaultCellStyle = dataGridViewCellStyle9;
            this._tokDefenseTimeCol3.HeaderText = "t����, ��.";
            this._tokDefenseTimeCol3.Name = "_tokDefenseTimeCol3";
            this._tokDefenseTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseTimeCol3.Width = 63;
            // 
            // _tokDefenseAPVBCol3
            // 
            this._tokDefenseAPVBCol3.HeaderText = "���";
            this._tokDefenseAPVBCol3.Name = "_tokDefenseAPVBCol3";
            this._tokDefenseAPVBCol3.Width = 35;
            // 
            // _tokDefenseUROVCol3
            // 
            this._tokDefenseUROVCol3.HeaderText = "����";
            this._tokDefenseUROVCol3.Name = "_tokDefenseUROVCol3";
            this._tokDefenseUROVCol3.Width = 43;
            // 
            // _tokDefenseSpeedupCol3
            // 
            this._tokDefenseSpeedupCol3.HeaderText = "���.";
            this._tokDefenseSpeedupCol3.Name = "_tokDefenseSpeedupCol3";
            this._tokDefenseSpeedupCol3.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol3
            // 
            this._tokDefenseSpeedupTimeCol3.HeaderText = "���.���.";
            this._tokDefenseSpeedupTimeCol3.Name = "_tokDefenseSpeedupTimeCol3";
            this._tokDefenseSpeedupTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol3.Width = 57;
            // 
            // _tokDefenseGrid2
            // 
            this._tokDefenseGrid2.AllowUserToAddRows = false;
            this._tokDefenseGrid2.AllowUserToDeleteRows = false;
            this._tokDefenseGrid2.AllowUserToResizeColumns = false;
            this._tokDefenseGrid2.AllowUserToResizeRows = false;
            this._tokDefenseGrid2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid2.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol2,
            this._tokDefenseModeCol2,
            this._tokDefenseBlockingNumberCol2,
            this._tokDefenseSignalCol2,
            this._tokDefenseConstraintCol2,
            this._tokDefenseFeatureCol2,
            this._tokDefenseTimeCol2,
            this._tokDefenseAPVCol2,
            this._tokDefenseUROVCol2,
            this._tokDefenseSpeedupCol2,
            this._tokDefenseSpeedupTimeCol2});
            this._tokDefenseGrid2.Location = new System.Drawing.Point(6, 139);
            this._tokDefenseGrid2.Name = "_tokDefenseGrid2";
            this._tokDefenseGrid2.RowHeadersVisible = false;
            this._tokDefenseGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid2.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this._tokDefenseGrid2.RowTemplate.Height = 24;
            this._tokDefenseGrid2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid2.Size = new System.Drawing.Size(730, 82);
            this._tokDefenseGrid2.TabIndex = 12;
            // 
            // _tokDefenseNameCol2
            // 
            this._tokDefenseNameCol2.Frozen = true;
            this._tokDefenseNameCol2.HeaderText = "";
            this._tokDefenseNameCol2.Name = "_tokDefenseNameCol2";
            this._tokDefenseNameCol2.ReadOnly = true;
            this._tokDefenseNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol2.Width = 5;
            // 
            // _tokDefenseModeCol2
            // 
            this._tokDefenseModeCol2.HeaderText = "�����";
            this._tokDefenseModeCol2.Name = "_tokDefenseModeCol2";
            this._tokDefenseModeCol2.Width = 48;
            // 
            // _tokDefenseBlockingNumberCol2
            // 
            this._tokDefenseBlockingNumberCol2.HeaderText = "����������";
            this._tokDefenseBlockingNumberCol2.Name = "_tokDefenseBlockingNumberCol2";
            this._tokDefenseBlockingNumberCol2.Width = 74;
            // 
            // _tokDefenseSignalCol2
            // 
            this._tokDefenseSignalCol2.HeaderText = "��� �������";
            this._tokDefenseSignalCol2.Name = "_tokDefenseSignalCol2";
            this._tokDefenseSignalCol2.Width = 76;
            // 
            // _tokDefenseConstraintCol2
            // 
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = null;
            this._tokDefenseConstraintCol2.DefaultCellStyle = dataGridViewCellStyle11;
            this._tokDefenseConstraintCol2.HeaderText = "�������, I�";
            this._tokDefenseConstraintCol2.Name = "_tokDefenseConstraintCol2";
            this._tokDefenseConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseConstraintCol2.Width = 71;
            // 
            // _tokDefenseFeatureCol2
            // 
            this._tokDefenseFeatureCol2.HeaderText = "���-��";
            this._tokDefenseFeatureCol2.Name = "_tokDefenseFeatureCol2";
            this._tokDefenseFeatureCol2.ReadOnly = true;
            this._tokDefenseFeatureCol2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseFeatureCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseFeatureCol2.Width = 47;
            // 
            // _tokDefenseTimeCol2
            // 
            dataGridViewCellStyle12.Format = "N0";
            dataGridViewCellStyle12.NullValue = null;
            this._tokDefenseTimeCol2.DefaultCellStyle = dataGridViewCellStyle12;
            this._tokDefenseTimeCol2.HeaderText = "t����, ��.";
            this._tokDefenseTimeCol2.Name = "_tokDefenseTimeCol2";
            this._tokDefenseTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseTimeCol2.Width = 63;
            // 
            // _tokDefenseAPVCol2
            // 
            this._tokDefenseAPVCol2.HeaderText = "���";
            this._tokDefenseAPVCol2.Name = "_tokDefenseAPVCol2";
            this._tokDefenseAPVCol2.Width = 35;
            // 
            // _tokDefenseUROVCol2
            // 
            this._tokDefenseUROVCol2.HeaderText = "����";
            this._tokDefenseUROVCol2.Name = "_tokDefenseUROVCol2";
            this._tokDefenseUROVCol2.Width = 43;
            // 
            // _tokDefenseSpeedupCol2
            // 
            this._tokDefenseSpeedupCol2.HeaderText = "���.";
            this._tokDefenseSpeedupCol2.Name = "_tokDefenseSpeedupCol2";
            this._tokDefenseSpeedupCol2.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol2
            // 
            this._tokDefenseSpeedupTimeCol2.HeaderText = "���.���.";
            this._tokDefenseSpeedupTimeCol2.Name = "_tokDefenseSpeedupTimeCol2";
            this._tokDefenseSpeedupTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol2.Width = 57;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol1,
            this._tokDefenseBlockingNumberCol1,
            this._tokDefenseSignalCol1,
            this._tokDefenseConstraintCol1,
            this._tokDefenseFeatureCol1,
            this._tokDefenseTimeCol1,
            this._tokDefenseKoeffCol1,
            this._tokDefenseAPVCol1,
            this._tokDefense3UROVCol,
            this._tokDefenseSpeedupCol1,
            this._tokDefenseSpeedupTimeCol1});
            this._tokDefenseGrid1.Location = new System.Drawing.Point(6, 6);
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(730, 131);
            this._tokDefenseGrid1.TabIndex = 11;
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 5;
            // 
            // _tokDefenseModeCol1
            // 
            this._tokDefenseModeCol1.HeaderText = "�����";
            this._tokDefenseModeCol1.Name = "_tokDefenseModeCol1";
            this._tokDefenseModeCol1.Width = 48;
            // 
            // _tokDefenseBlockingNumberCol1
            // 
            this._tokDefenseBlockingNumberCol1.HeaderText = "����������";
            this._tokDefenseBlockingNumberCol1.Name = "_tokDefenseBlockingNumberCol1";
            this._tokDefenseBlockingNumberCol1.Width = 74;
            // 
            // _tokDefenseSignalCol1
            // 
            this._tokDefenseSignalCol1.HeaderText = "��� �������";
            this._tokDefenseSignalCol1.Name = "_tokDefenseSignalCol1";
            this._tokDefenseSignalCol1.Width = 76;
            // 
            // _tokDefenseConstraintCol1
            // 
            dataGridViewCellStyle14.NullValue = null;
            this._tokDefenseConstraintCol1.DefaultCellStyle = dataGridViewCellStyle14;
            this._tokDefenseConstraintCol1.HeaderText = "�������, I�";
            this._tokDefenseConstraintCol1.Name = "_tokDefenseConstraintCol1";
            this._tokDefenseConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseConstraintCol1.Width = 71;
            // 
            // _tokDefenseFeatureCol1
            // 
            this._tokDefenseFeatureCol1.HeaderText = "���-��";
            this._tokDefenseFeatureCol1.Name = "_tokDefenseFeatureCol1";
            this._tokDefenseFeatureCol1.Width = 47;
            // 
            // _tokDefenseTimeCol1
            // 
            dataGridViewCellStyle15.NullValue = null;
            this._tokDefenseTimeCol1.DefaultCellStyle = dataGridViewCellStyle15;
            this._tokDefenseTimeCol1.HeaderText = "t����, ��.";
            this._tokDefenseTimeCol1.Name = "_tokDefenseTimeCol1";
            this._tokDefenseTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseTimeCol1.Width = 63;
            // 
            // _tokDefenseKoeffCol1
            // 
            dataGridViewCellStyle16.NullValue = null;
            this._tokDefenseKoeffCol1.DefaultCellStyle = dataGridViewCellStyle16;
            this._tokDefenseKoeffCol1.HeaderText = "�����.";
            this._tokDefenseKoeffCol1.Name = "_tokDefenseKoeffCol1";
            this._tokDefenseKoeffCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseKoeffCol1.Width = 51;
            // 
            // _tokDefenseAPVCol1
            // 
            this._tokDefenseAPVCol1.HeaderText = "���";
            this._tokDefenseAPVCol1.Name = "_tokDefenseAPVCol1";
            this._tokDefenseAPVCol1.Width = 35;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 43;
            // 
            // _tokDefenseSpeedupCol1
            // 
            this._tokDefenseSpeedupCol1.HeaderText = "���.";
            this._tokDefenseSpeedupCol1.Name = "_tokDefenseSpeedupCol1";
            this._tokDefenseSpeedupCol1.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol1
            // 
            this._tokDefenseSpeedupTimeCol1.HeaderText = "���.���.";
            this._tokDefenseSpeedupTimeCol1.Name = "_tokDefenseSpeedupTimeCol1";
            this._tokDefenseSpeedupTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol1.Width = 57;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._changeGroupBtn);
            this.groupBox19.Controls.Add(this._reserveRadioBtnGroup);
            this.groupBox19.Controls.Add(this._mainRadioBtnGroup);
            this.groupBox19.Location = new System.Drawing.Point(6, 7);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(399, 67);
            this.groupBox19.TabIndex = 0;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "������ �������";
            // 
            // _changeGroupBtn
            // 
            this._changeGroupBtn.Location = new System.Drawing.Point(200, 27);
            this._changeGroupBtn.Name = "_changeGroupBtn";
            this._changeGroupBtn.Size = new System.Drawing.Size(193, 23);
            this._changeGroupBtn.TabIndex = 2;
            this._changeGroupBtn.Text = "�������� --> ���������";
            this._changeGroupBtn.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioBtnGroup
            // 
            this._reserveRadioBtnGroup.AutoSize = true;
            this._reserveRadioBtnGroup.Location = new System.Drawing.Point(99, 30);
            this._reserveRadioBtnGroup.Name = "_reserveRadioBtnGroup";
            this._reserveRadioBtnGroup.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioBtnGroup.TabIndex = 1;
            this._reserveRadioBtnGroup.Text = "���������";
            this._reserveRadioBtnGroup.UseVisualStyleBackColor = true;
            // 
            // _mainRadioBtnGroup
            // 
            this._mainRadioBtnGroup.AutoSize = true;
            this._mainRadioBtnGroup.Checked = true;
            this._mainRadioBtnGroup.Location = new System.Drawing.Point(18, 30);
            this._mainRadioBtnGroup.Name = "_mainRadioBtnGroup";
            this._mainRadioBtnGroup.Size = new System.Drawing.Size(75, 17);
            this._mainRadioBtnGroup.TabIndex = 0;
            this._mainRadioBtnGroup.TabStop = true;
            this._mainRadioBtnGroup.Text = "��������";
            this._mainRadioBtnGroup.UseVisualStyleBackColor = true;
            this._mainRadioBtnGroup.CheckedChanged += new System.EventHandler(this._mainRadioBtnGroup_CheckedChanged);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Location = new System.Drawing.Point(1, 491);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(131, 23);
            this._readConfigBut.TabIndex = 1;
            this._readConfigBut.Text = "��������� �� ����-��";
            this.toolTip1.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Location = new System.Drawing.Point(138, 491);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(114, 23);
            this._writeConfigBut.TabIndex = 2;
            this._writeConfigBut.Text = "�������� � ����-��";
            this.toolTip1.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Location = new System.Drawing.Point(528, 491);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(122, 23);
            this._saveConfigBut.TabIndex = 4;
            this._saveConfigBut.Text = "��������� � ����";
            this.toolTip1.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Location = new System.Drawing.Point(400, 491);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(122, 23);
            this._loadConfigBut.TabIndex = 3;
            this._loadConfigBut.Text = "��������� �� �����";
            this.toolTip1.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��500_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��500";
            // 
            // _openConfigurationDlg1
            // 
            this._openConfigurationDlg1.DefaultExt = "bin";
            this._openConfigurationDlg1.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg1.RestoreDirectory = true;
            this._openConfigurationDlg1.Title = "������� ������� ��� ��500";
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(656, 491);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(122, 23);
            this._saveToXmlButton.TabIndex = 33;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 50;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 518);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(813, 22);
            this._statusStrip.TabIndex = 5;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(258, 491);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(136, 23);
            this._resetSetpointsButton.TabIndex = 39;
            this._resetSetpointsButton.Text = "��������� ���. �������";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 540);
            this.Controls.Add(this._resetSetpointsButton);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr500ConfigurationForm_KeyUp);
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._logicSignalsDataGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._defendingTabPage.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this._newTabControl.ResumeLayout(false);
            this._externalTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._automaticTabPage.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this._tokTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _constraintGroupCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox _indicationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView _logicSignalsDataGrid;
        private System.Windows.Forms.ComboBox _logicChannelsCombo;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _applyLogicSignalsBut;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button _outputLogicAcceptBut;
        private System.Windows.Forms.ComboBox _outputLogicCombo;
        private System.Windows.Forms.CheckedListBox _outputLogicCheckList;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg1;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _speedupTimeBox;
        private System.Windows.Forms.ComboBox _speedupOnCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox _speedupNumberCombo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.MaskedTextBox _dispepairReleDurationBox;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.DataGridViewTextBoxColumn _diskretChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _diskretValueCol;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.TabPage _defendingTabPage;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TabControl _newTabControl;
        private System.Windows.Forms.TabPage _externalTabPage;
        private System.Windows.Forms.TabPage _automaticTabPage;
        private System.Windows.Forms.Button _changeGroupBtn;
        private System.Windows.Forms.RadioButton _reserveRadioBtnGroup;
        private System.Windows.Forms.RadioButton _mainRadioBtnGroup;
        private System.Windows.Forms.TabPage _tokTabPage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefEntryCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.MaskedTextBox _CAPV_TimeBox;
        private System.Windows.Forms.ComboBox _CAPV_BlockingCombo;
        private System.Windows.Forms.ComboBox _CAPV_EntryCombo;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.MaskedTextBox _AVR_OffTimeBox;
        private System.Windows.Forms.MaskedTextBox _AVR_ReturnTimeBox;
        private System.Windows.Forms.ComboBox _AVR_ReturnNumberCombo;
        private System.Windows.Forms.MaskedTextBox _AVR_StartTimeBox;
        private System.Windows.Forms.ComboBox _AVR_BlockingCombo;
        private System.Windows.Forms.ComboBox _AVR_StartNumberCombo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox _ACR_TimeBox;
        private System.Windows.Forms.ComboBox _ACR_BlockingCombo;
        private System.Windows.Forms.ComboBox _ACR_EntryCombo;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _APV_Crat2TimeBox;
        private System.Windows.Forms.MaskedTextBox _APV_Crat1TimeBox;
        private System.Windows.Forms.MaskedTextBox _APV_ReadyTimeBox;
        private System.Windows.Forms.MaskedTextBox _APV_BlockingTimeBox;
        private System.Windows.Forms.ComboBox _APV_SelfresetCombo;
        private System.Windows.Forms.ComboBox _APV_BlockingCombo;
        private System.Windows.Forms.ComboBox _APV_ConfigCombo;
        private System.Windows.Forms.DataGridView _tokDefenseGrid3;
        private System.Windows.Forms.DataGridView _tokDefenseGrid2;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseSignalCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseConstraintCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseTimeCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseKoeffCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedupCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockingCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseSignalCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseFeatureCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseTimeCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVBCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedupCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseSignalCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseFeatureCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedupCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol2;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.ToolStripMenuItem resetSetPointStruct;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
    }
}