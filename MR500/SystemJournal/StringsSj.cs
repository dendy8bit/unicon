﻿using System.Collections.Generic;

namespace BEMN.MR500.SystemJournal
{

    public static class StringsSj
    {
        public static Dictionary<int, string> SystemJournalMessages
        {
            get
            {
                return new Dictionary<int, string>
                    {
                        { 0 ,"Журнал пуст"},
                        { 1 ,"Потеря данных осциллографа"},
                        { 2 ,"Ошибка хранения данных"},
                        { 3 ,"Неисправность вн. шины"},
                        { 4 ,"Вн. шина исправна"},
                        { 5 ,"Температура выше нормы"},
                        { 6 ,"Температура в норме"},
                        { 7 ,"МСА 2 неисправен"},
                        { 8 ,"МСА 2 исправен"},
                        { 9 ,"МСА 1 неисправен"},
                        { 10 ,"МСА 1 исправен"},
                        { 11 ,"МРВ неисправен"},
                        { 12 ,"МРВ исправен"},
                        { 13 ,"МСД1 неисправен"},
                        { 14 ,"МСД1 исправен"},
                        { 15 ,"МСД2 неисправен"},
                        { 16 ,"МСД2 исправен"},
                        { 17 ,"Ошибка контрольной суммы уставок"},
                        { 18 ,"Ошибка контрольной суммы данных"},
                        { 19 ,"Ошибка контрольной суммы данных"},
                        { 20 ,"Ошибка журнала системы"},
                        { 21 ,"Ошибка журнала аварий"},
                        { 22 ,"Остановка часов"},
                        { 25 ,"Меню – уставки изменены"},
                        { 26 ,"Пароль изменен"},
                        { 27 ,"Сброс журнала системы"},
                        { 28 ,"Сброс журнала аварий"},
                        { 29 ,"Сброс ресурса выключателя"},
                        { 30 ,"Сброс индикации"},
                        { 31 ,"Изменена группа уставок"},
                        { 32 ,"СДТУ – уставки изменены"},
                        { 33 ,"Ошибка задающего генератора"},
                        { 34 ,"Рестарт устройства"},
                        { 35 ,"Устройство выключено"},
                        { 36 ,"Устройство включено"},
                        { 38 ,"Меню сброс осциллографа "},
                        { 39 ,"СДТУ – сброс осциллографа"},
                        { 41 ,"АЧР отключить"},
                        { 42 ,"АЧР внешняя блокировка"},
                        { 46 ,"Небаланс АЦП Iabc"},
                        { 47 ,"Баланс АЦП Iabc"},
                        { 48 ,"Несимметрия Iabc"},
                        { 49 ,"Симметрия Iabc"},
                        { 50 ,"ТН внеш. неисправность"},
                        { 51 ,"ТН исправен"},
                        { 52 ,"Небаланс АЦП Uabc"},
                        { 53 ,"Баланс АЦП Uabc"},
                        { 54 ,"Несимметрия Uabc"},
                        { 55 ,"Симметрия Uabc"},
                        { 56 ,"Uавс < 5В"},
                        { 57 ,"Uавс > 5В"},
                        { 58 ,"ТННП внеш. неисправность"},
                        { 59 ,"ТННП исправен"},
                        { 60 ,"Частота вне диапазона"},
                        { 61 ,"Частота в норме"},
                        { 62 ,"Выключатель отключен"},
                        { 63 ,"Выключатель включен"},
                        { 64 ,"Блокировка выключателя"},
                        { 65 ,"Отказ выключателя"},
                        { 66 ,"Неисправность выключателя"},
                        { 67 ,"Внеш.неиспр. выключателя"},
                        { 68 ,"Неиспр.управ. выключателя"},
                        { 69 ,"Работа УРОВ"},
                        { 70 ,"Пуск ЛЗШ"},
                        { 71 ,"Защита отключить"},
                        { 72 ,"АПВ блокировано"},
                        { 73 ,"АПВ вн.блокировка"},
                        { 74 ,"Запуск АПВ 1 крат"},
                        { 75 ,"Запуск АПВ 2 крат"},
                        { 76 ,"Запуск АПВ 3 крат"},
                        { 77 ,"Запуск АПВ 4 крат"},
                        { 78 ,"АПВ включить"},
                        { 85 ,"АВР блокирован"},
                        { 86 ,"АВР внеш. блокировка"},
                        { 87 ,"АВР готовность"},
                        { 88 ,"АВР отключить"},
                        { 89 ,"АВР включить"},
                        { 90 ,"АВР вкл. резерв"},
                        { 91 ,"АВР откл. резерв"},
                        { 92 ,"АВР запуск от защиты"},
                        { 93 ,"АВР запуск команда откл."},
                        { 94 ,"АВР запуск по питанию"},
                        { 95 ,"АВР запуск самооткл."},
                        { 96 ,"Кнопка отключить"},
                        { 97 ,"Кнопка включить"},
                        { 98 ,"Ключ отключить"},
                        { 99 ,"Ключ включить"},
                        { 100 ,"Внешнее отключить"},
                        { 101 ,"Внешнее включить"},
                        { 102 ,"СДТУ отключить"},
                        { 103 ,"СДТУ включить"},
                        { 104 ,"Основные уставки"},
                        { 105 ,"Резервные уставки"},
                        { 106 ,"Внеш.резерв. уставки"},
                        { 108 ,"Меню-основные уставки"},
                        { 109 ,"Меню-резервные уставки"},
                        { 110 ,"СДТУ-основные уставки"},
                        { 111 ,"СДТУ-резервные уставки"},
                        { 112 ,"АПВ возврат"},
                        { 113 ,"АПВ возврат F>"},
                        { 114 ,"АПВ возврат F>>"},
                        { 115 ,"АПВ возврат F<"},
                        { 116 ,"АПВ возврат F<<"},
                        { 117 ,"АПВ возврат U>"},
                        { 118 ,"АПВ возврат U>>"},
                        { 119 ,"АПВ возврат U<"},
                        { 120 ,"АПВ возврат U<<"},
                        { 121 ,"АПВ возврат U2>"},
                        { 122 ,"АПВ возврат U2>>"},
                        { 123 ,"АПВ возврат Uo>"},
                        { 124 ,"АПВ возврат Uo>>"},
                        { 125 ,"АПВ возврат ВЗ-1"},
                        { 126 ,"АПВ возврат ВЗ-2"},
                        { 127 ,"АПВ возврат ВЗ-3"},
                        { 128 ,"АПВ возврат ВЗ-4"},
                        { 129 ,"АПВ возврат ВЗ-5"},
                        { 130 ,"АПВ возврат ВЗ-6"},
                        { 131 ,"АПВ возврат ВЗ-7"},
                        { 132 ,"АПВ возврат ВЗ-8"},
                        { 133 ,"U<10B Частота недостоверна"},
                        { 134 ,"U>10B Частота достоверна"},
                        { 135 ,"АВР Меню блокировка"},
                        { 136 ,"АВР СДТУ блокировка"},
                        { 137 ,"СДТУ: логика изменена"},
                        { 138 ,"Меню: запуск логики"},
                        { 139 ,"СДТУ: запуск логики"},
                        { 140 ,"Меню: остановка логики"},
                        { 141 ,"СДТУ: остановка логики"},
                        { 142 ,"Ошибка логики по старту"},
                        { 143 ,"Ошибка логики тайм аут"},
                        { 144 ,"Ошибка логики размер"},
                        { 145 ,"Ошибка логики команда"},
                        { 146 ,"Ошибка логики аргумент"},
                        { 147 ,"Ошибка размера ППЗУ"},
                        { 149 ,"СПЛ1"},
                        { 150 ,"СПЛ2"},
                        { 151 ,"СПЛ3"},
                        { 152 ,"СПЛ4"},
                        { 153 ,"СПЛ5"},
                        { 154 ,"СПЛ6"},
                        { 155 ,"СПЛ7"},
                        { 156 ,"СПЛ8"},
                        { 157 ,"СПЛ9"},
                        { 158 ,"СПЛ10"},
                        { 159 ,"СПЛ11"},
                        { 160 ,"СПЛ12"},
                        { 161 ,"СПЛ13"},
                        { 162 ,"СПЛ14"},
                        { 163 ,"СПЛ15"},
                        { 164 ,"СПЛ16"},
                        { 165 ,"СПЛ17"},
                        { 166 ,"СПЛ18"},
                        { 167 ,"СПЛ19"},
                        { 168 ,"СПЛ20"},
                        { 169 ,"СПЛ21"},
                        { 170 ,"СПЛ22"},
                        { 171 ,"СПЛ23"},
                        { 172 ,"СПЛ24"},
                        { 173 ,"СПЛ25"},
                        { 174 ,"СПЛ26"},
                        { 175 ,"СПЛ27"},
                        { 176 ,"СПЛ28"},
                        { 177 ,"СПЛ29"},
                        { 178 ,"СПЛ30"},
                        { 179 ,"СПЛ31"},
                        { 180 ,"СПЛ32"},
                        { 181 ,"СПЛ33"},
                        { 182 ,"СПЛ34"},
                        { 183 ,"СПЛ35"},
                        { 184 ,"СПЛ36"},
                        { 185 ,"СПЛ37"},
                        { 186 ,"СПЛ38"},
                        { 187 ,"СПЛ39"},
                        { 188 ,"СПЛ40"},
                        { 189 ,"СПЛ41"},
                        { 190 ,"СПЛ42"},
                        { 191 ,"СПЛ43"},
                        { 192 ,"СПЛ44"},
                        { 193 ,"СПЛ45"},
                        { 194 ,"СПЛ46"},
                        { 195 ,"СПЛ47"},
                        { 196 ,"СПЛ48"},
                        { 197 ,"СПЛ49"},
                        { 198 ,"СПЛ50"},
                        { 199 ,"СПЛ51"},
                        { 200 ,"СПЛ52"},
                        { 201 ,"СПЛ53"},
                        { 202 ,"СПЛ54"},
                        { 203 ,"СПЛ55"},
                        { 204 ,"СПЛ56"},
                        { 205 ,"СПЛ57"},
                        { 206 ,"СПЛ58"},
                        { 207 ,"СПЛ59"},
                        { 208 ,"СПЛ60"},
                        { 209 ,"СПЛ61"},
                        { 210 ,"СПЛ62"},
                        { 211 ,"СПЛ63"},
                        { 212 ,"СПЛ64"}
                    };
            }
        }
    }
}
