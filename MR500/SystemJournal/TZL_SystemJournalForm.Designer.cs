namespace BEMN.MR500.SystemJournal
{
    partial class VSystemJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._deserializeSysJournalBut = new System.Windows.Forms.Button();
            this._serializeSysJournalBut = new System.Windows.Forms.Button();
            this._readSysJournalBut = new System.Windows.Forms.Button();
            this._sysJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveToHtmlButton = new System.Windows.Forms.Button();
            this._saveSysJournalHtmlDlg = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��500_v3_�������������";
            this._saveSysJournalDlg.Filter = "��500 - ������ ������� | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������� ��� ��500";
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������ ������� ��� ��500";
            // 
            // _deserializeSysJournalBut
            // 
            this._deserializeSysJournalBut.Location = new System.Drawing.Point(236, 311);
            this._deserializeSysJournalBut.Name = "_deserializeSysJournalBut";
            this._deserializeSysJournalBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeSysJournalBut.TabIndex = 15;
            this._deserializeSysJournalBut.Text = "��������� �� �����";
            this._deserializeSysJournalBut.UseVisualStyleBackColor = true;
            this._deserializeSysJournalBut.Click += new System.EventHandler(this._deserializeSysJournalBut_Click);
            // 
            // _serializeSysJournalBut
            // 
            this._serializeSysJournalBut.Location = new System.Drawing.Point(119, 311);
            this._serializeSysJournalBut.Name = "_serializeSysJournalBut";
            this._serializeSysJournalBut.Size = new System.Drawing.Size(111, 23);
            this._serializeSysJournalBut.TabIndex = 14;
            this._serializeSysJournalBut.Text = "��������� � ����";
            this._serializeSysJournalBut.UseVisualStyleBackColor = true;
            this._serializeSysJournalBut.Click += new System.EventHandler(this._serializeSysJournalBut_Click);
            // 
            // _readSysJournalBut
            // 
            this._readSysJournalBut.Location = new System.Drawing.Point(5, 311);
            this._readSysJournalBut.Name = "_readSysJournalBut";
            this._readSysJournalBut.Size = new System.Drawing.Size(75, 23);
            this._readSysJournalBut.TabIndex = 13;
            this._readSysJournalBut.Text = "���������";
            this._readSysJournalBut.UseVisualStyleBackColor = true;
            this._readSysJournalBut.Click += new System.EventHandler(this._readSysJournalBut_Click);
            // 
            // _sysJournalGrid
            // 
            this._sysJournalGrid.AllowUserToAddRows = false;
            this._sysJournalGrid.AllowUserToDeleteRows = false;
            this._sysJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._sysJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._sysJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._sysJournalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._sysJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._sysJournalGrid.Name = "_sysJournalGrid";
            this._sysJournalGrid.RowHeadersVisible = false;
            this._sysJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._sysJournalGrid.Size = new System.Drawing.Size(517, 303);
            this._sysJournalGrid.TabIndex = 12;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "�����";
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "�����";
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.DataPropertyName = "���������";
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 340);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(517, 22);
            this.statusStrip1.TabIndex = 24;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            this._configProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveToHtmlButton
            // 
            this._saveToHtmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToHtmlButton.Location = new System.Drawing.Point(368, 311);
            this._saveToHtmlButton.Name = "_saveToHtmlButton";
            this._saveToHtmlButton.Size = new System.Drawing.Size(137, 23);
            this._saveToHtmlButton.TabIndex = 25;
            this._saveToHtmlButton.Text = "��������� � HTML";
            this._saveToHtmlButton.UseVisualStyleBackColor = true;
            this._saveToHtmlButton.Click += new System.EventHandler(this._saveToHtmlButton_Click);
            // 
            // _saveSysJournalHtmlDlg
            // 
            this._saveSysJournalHtmlDlg.DefaultExt = "xml";
            this._saveSysJournalHtmlDlg.FileName = "��500_������_�������";
            this._saveSysJournalHtmlDlg.Filter = "*.html | *.html";
            this._saveSysJournalHtmlDlg.Title = "��������� ������ ������� ��� �� 500";
            // 
            // VSystemJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 362);
            this.Controls.Add(this._saveToHtmlButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._deserializeSysJournalBut);
            this.Controls.Add(this._serializeSysJournalBut);
            this.Controls.Add(this._readSysJournalBut);
            this.Controls.Add(this._sysJournalGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "VSystemJournalForm";
            this.Text = "SystemJournalForm";
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.Button _deserializeSysJournalBut;
        private System.Windows.Forms.Button _serializeSysJournalBut;
        private System.Windows.Forms.Button _readSysJournalBut;
        private System.Windows.Forms.DataGridView _sysJournalGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.Button _saveToHtmlButton;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalHtmlDlg;
    }
}