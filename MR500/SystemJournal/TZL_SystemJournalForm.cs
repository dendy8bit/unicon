using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MR500.SystemJournal.Structures;
using BEMN.MR500.Properties;

namespace BEMN.MR500.SystemJournal
{
    public partial class VSystemJournalForm : Form, IFormView
    {
        private const string RECORDS_IN_JOURNAL = "������� � ������� - {0}";
        private const string JOURNAL_SAVED = "������ ��������";
        private const string READ_FAIL = "���������� ��������� ������";
        private const string TABLE_NAME_SYS = "��500_������_�������";
        private const string NUMBER_SYS = "�����";
        private const string TIME_SYS = "�����";
        private const string MESSAGE_SYS = "���������";
        private const string SYSTEM_JOURNAL = "������ �������";
        private const string JOURNAL_READDING = "��� ������ �������";

        private readonly MemoryEntity<SystemJournalStruct> _systemJournalNew;
        private DataTable _dataTable;
        private int _recordNumber;
        private MR500 _device;

        public VSystemJournalForm()
        {
            this.InitializeComponent();
        }

        public VSystemJournalForm(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalNew = device.TZLDevice.SystemJournalNew;
            this._systemJournalNew.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);

            this._systemJournalNew.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._systemJournalNew.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._configProgressBar.Maximum = this._systemJournalNew.Slots.Count;
        }

        #region [Properties]
        /// <summary>
        /// ������� ���������
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]

        #region [Help members]
        /// <summary>
        /// ������������� ��-�� Enabled ��� ���� ������
        /// </summary>
        /// <param name="enabled"></param>
        private void SetButtonsState(bool enabled)
        {
            this._readSysJournalBut.Enabled = enabled;
            this._serializeSysJournalBut.Enabled = enabled;
            this._deserializeSysJournalBut.Enabled = enabled;
            this._saveToHtmlButton.Enabled = enabled;
        }

        private void FailReadJournal()
        {
            this.SetButtonsState(true);
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }



        private void ReadRecord()
        {
            var i = 0;
            foreach (var record in this._systemJournalNew.Value._records)
            {
                if (record.IsEmpty)
                {
                    break;
                }
                this._dataTable.Rows.Add(i + 1, record.GetRecordTime, record.GetRecordMessage);
                i++;
            }
            this._sysJournalGrid.DataSource = this._dataTable;
            this.RecordNumber = this._sysJournalGrid.Rows.Count;
            this.SetButtonsState(true);
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveSysJournalDlg.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openSysJounralDlg.FileName);
                this._sysJournalGrid.DataSource = this._dataTable;
            }
            this.RecordNumber = this._sysJournalGrid.Rows.Count;
        }
        #endregion [Help members]

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(BEMN.MR500.MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(VSystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR500.Properties.Resources.js; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��500_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(this._sysJournalGrid["_indexCol", i].Value, this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value);
            }

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��500_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(table.Rows[i].ItemArray[0], table.Rows[i].ItemArray[1], table.Rows[i].ItemArray[2]);
            }
        }


        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this._configProgressBar.Value = 0;
            this.RecordNumber = 0;
            this.SetButtonsState(false);
            this._statusLabel.Text = JOURNAL_READDING;
            this._systemJournalNew.LoadStruct();
        }

        private void _saveToHtmlButton_Click(object sender, EventArgs e)
        {
            this._saveSysJournalHtmlDlg.FileName = string.Format("������ ������� �� 500 v{0}", _device.DeviceVersion);
            try
            {
                if (DialogResult.OK == _saveSysJournalHtmlDlg.ShowDialog())
                {
                    HtmlExport.Export(_dataTable, _saveSysJournalHtmlDlg.FileName, Resources.MR500SJ);
                    _statusLabel.Text = "������ ��������";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "������ ����������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _statusLabel.Text = "������ �� ��������";
                throw;
            }
        }
    }
}