using System;
using System.Collections;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.MBServer;


namespace BEMN.MR500
{
    public class Oscilloscope : Devices.Oscilloscope
    {
        private MR500 _device;

        public Oscilloscope(Device device)
            : base(device)
        {
            this._device = (MR500)device;
        }

        public override void Initialize()
        {
            base.Initialize();

            _analogChannelsCnt = 5;
            _diskretChannelsCnt = 17;
            _exchangeCnt = 256;
            _oscilloscopeSize = 0x8000;
            _pointCount = 3276;
            _oscJournal = new Device.slot(0x3800, 0x3818);
            _journalLength = 0x18;

            AnalogChannels.Add("Ia");
            AnalogChannels.Add("Ib");
            AnalogChannels.Add("Ic");
            AnalogChannels.Add("Io");
            AnalogChannels.Add("������");

            for (int i = 0; i < 17; i++)
            {
                if (i == 16)
                {
                    DiskretChannels.Add("������");
                }
                else
                {
                    DiskretChannels.Add("�" + (i + 1));
                }
            }
        }

        public override void LoadOscilloscopeSettings()
        {
            this._device.LoadInputSignals();
            base.LoadOscilloscopeSettings();
        }

        public string Reverse(string revString)
        {
            string ret = "";
            for (int i = revString.Length; i > 0; i--)
            {
                ret += revString[i - 1];
            }
            return ret;
        }

        public override void CreateJournal()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>(0xE);
            MR500.CAlarmJournal helper = new MR500.CAlarmJournal(this._device);
            byte[] buffer = Common.TOBYTES(_journalBuffer, false);
            byte[] datetime = new byte[7];
            int[] datetimeReal = new int[7];

            Array.ConstrainedCopy(buffer, 9, datetime, 0, 7);

            double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), this._device.TT, true);// / 1000;
            double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), this._device.TT, true);// / 1000;
            double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), this._device.TT, true);// / 1000;
            double Io = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), this._device.TTNP, false);
            double Ig = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), this._device.TTNP, false);
            double I0 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), this._device.TT, true);
            double I1 = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), this._device.TT, true);// / 1000;
            double I2 = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), this._device.TT, true);

            for (int i = 0; i < 7; i++) 
            {
                string hex = datetime[i].ToString("X");
                try
                {
                    datetimeReal[i] = Convert.ToInt32(hex);
                }
                catch (Exception df) 
                {
                    datetimeReal[i] = Convert.ToInt32(datetime[i].ToString());
                }
            }

            ret.Add("���������", helper.GetMessage(buffer[8]));

            ret.Add("����", string.Format("{0:D2}-{1:D2}-{2:D2}", datetimeReal[0], datetimeReal[1], datetimeReal[2]));
            ret.Add("�����", string.Format("{0:D2}:{1:D2}:{2:D2}", datetimeReal[3], datetimeReal[4], datetimeReal[5]));
            
            ret.Add("���", helper.GetCode(buffer[16], buffer[18]));
            ret.Add("��������",
                Common.TOWORD(buffer[21], buffer[20]) == 0
                    ? helper.GetExternal(buffer[16])
                    : helper.GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19]));

            ret.Add("Ia", string.Format("{0:F2} (A)", Ia));
            ret.Add("Ib", string.Format("{0:F2} (A)", Ib));
            ret.Add("Ic", string.Format("{0:F2} (A)", Ic));
            ret.Add("Io", string.Format("{0:F2} (A)", Io));
            ret.Add("Ig", string.Format("{0:F2} (A)", Ig));
            ret.Add("I0", string.Format("{0:F2} (A)", I0));
            ret.Add("I1", string.Format("{0:F2} (A)", I1));
            ret.Add("I2", string.Format("{0:F2} (A)", I2));

            byte[] b1 = { buffer[38] };
            byte[] b2 = { buffer[39] };
            ret.Add("�����.1-8", Common.BitsToString(new BitArray(b1)));
            ret.Add("�����.9-16", Common.BitsToString(new BitArray(b2)));
            ret.Add("I��", string.Format("{0} (A)", this._device.TT));
            ret.Add("I����", string.Format("{0} (A)", this._device.TTNP));
            ret.Add("����������", this._device.DeviceType);
            _journal = ret;

        }
        protected override void PrepareData_OldVers(byte[] data)
        {
            DeviceName = Device.DeviceType;
            _pointCount = 4095;

            int _KoefAvary = 32768 - 1024 * 8;
            int _KoefZashiti = KoefZashiti;

            double kTT = this._device.TT;
            double kTTNP = this._device.TTNP;
            KoefI = kTT / kTTNP * 8.0;
            KoefShkaliI = 40 * kTT * 1.41;

            for (int i = 0; i < _pointCount - 1; i++)
            {
                int channel = 0;
                //�������
                double value = BitConverter.ToUInt16(data, i * 8 + 0);
                _analogData[channel++][i] = (value - 0x8000) / 0x8000 * KoefShkaliI;
                value = BitConverter.ToUInt16(data, i * 8 + 2);
                _analogData[channel++][i] = (value - 0x8000) / 0x8000 * KoefShkaliI;
                value = BitConverter.ToUInt16(data, i * 8 + 4);
                _analogData[channel++][i] = (value - 0x8000) / 0x8000 * KoefShkaliI;
                value = BitConverter.ToUInt16(data, i * 8 + 6);
                _analogData[channel][i] = (value - 0x8000) / 0x8000 * 5 * kTTNP * 1.41;
                if (i <= _KoefAvary / _KoefZashiti)
                {
                    _analogData[4][i] = -KoefShkaliI;
                }
                else
                {
                    _analogData[4][i] = KoefShkaliI;
                }
            }
        }

        protected override void PrepareData(byte[] data)
        {
            DeviceName = Device.DeviceType;

            int _KoefAvary = 32768 - 2048 * 10;
            int _KoefZashiti = KoefZashiti;

            double kTT = this._device.TT;
            double kTTNP = this._device.TTNP;
            KoefI = kTT / kTTNP * 8.0;
            KoefShkaliI = 40 * kTT * 1.41;  
            
            double k = 0.15625;

            for (int i = 0; i < 17; i++)
            {
                _diskretData.Add(new BitArray(_pointCount));
            }
            for (int i = 0; i < _pointCount; i++)
            {
                int channel = 0;
                //�������
                double value = BitConverter.ToUInt16(data, i * 10 + 0);
                _analogData[channel++][i] = (value - 0x8000) / 0x8000 * KoefShkaliI;
                value = BitConverter.ToUInt16(data, i * 10 + 2);
                _analogData[channel++][i] = (value - 0x8000) / 0x8000 * KoefShkaliI;
                value = BitConverter.ToUInt16(data, i * 10 + 4);
                _analogData[channel++][i] = (value - 0x8000) / 0x8000 * KoefShkaliI;
                value = BitConverter.ToUInt16(data, i * 10 + 6);
                _analogData[channel][i] = (value - 0x8000) / 0x8000 * 5 * kTTNP * 1.41;
                if (i <= _KoefAvary / _KoefZashiti)
                {
                    _analogData[4][i] = -KoefShkaliI;
                }
                else
                {
                    _analogData[4][i] = KoefShkaliI;
                }
                //��������
                double high = BitConverter.ToInt16(data, i * 10 + 8);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j][i] = 0 != ((int)high >> j & 1);
                }
                double low = BitConverter.ToInt16(data, i * 10 + 9);
                for (int j = 0; j < 8; j++)
                {
                    _diskretData[j + 8][i] = 0 != ((int)low >> j & 1);
                }
                if (i <= _KoefAvary / _KoefZashiti)
                {
                    _diskretData[16][i] = true;
                }
                else
                {
                    _diskretData[16][i] = false;
                }
            }
        }
    }
}

