namespace BEMN.MR500
{
    partial class OscilloscopeFormBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._oscJournalDataGrid = new System.Windows.Forms.DataGridView();
            this._dateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ib = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Io = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I� = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._graphGroup = new System.Windows.Forms.GroupBox();
            this._resetOsc = new System.Windows.Forms.Button();
            this._showButton = new System.Windows.Forms.Button();
            this._readOscilloscope = new System.Windows.Forms.Button();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._statusActionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._statusPercentLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._statusProgress = new System.Windows.Forms.ToolStripProgressBar();
            this._saveOscilloscopeDlg = new System.Windows.Forms.SaveFileDialog();
            this._openOscilloscopeDlg = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._loadFromFileButton = new System.Windows.Forms.Button();
            this._saveToFileButton = new System.Windows.Forms.Button();
            this._reloadJournal = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).BeginInit();
            this._graphGroup.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this._oscJournalDataGrid);
            this.groupBox1.Location = new System.Drawing.Point(8, 126);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(860, 175);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "������ ������������";
            // 
            // _oscJournalDataGrid
            // 
            this._oscJournalDataGrid.AllowUserToAddRows = false;
            this._oscJournalDataGrid.AllowUserToDeleteRows = false;
            this._oscJournalDataGrid.AllowUserToResizeColumns = false;
            this._oscJournalDataGrid.AllowUserToResizeRows = false;
            this._oscJournalDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._oscJournalDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscJournalDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._dateCol,
            this._oscTimeColumn,
            this._stage,
            this._type,
            this._value,
            this._Ia,
            this._Ib,
            this._Ic,
            this._Io,
            this._I�,
            this._I0,
            this._I1,
            this._I2});
            this._oscJournalDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._oscJournalDataGrid.Location = new System.Drawing.Point(3, 16);
            this._oscJournalDataGrid.Name = "_oscJournalDataGrid";
            this._oscJournalDataGrid.ReadOnly = true;
            this._oscJournalDataGrid.RowHeadersVisible = false;
            this._oscJournalDataGrid.ShowCellErrors = false;
            this._oscJournalDataGrid.ShowRowErrors = false;
            this._oscJournalDataGrid.Size = new System.Drawing.Size(854, 156);
            this._oscJournalDataGrid.TabIndex = 11;
            // 
            // _dateCol
            // 
            this._dateCol.DataPropertyName = "����";
            this._dateCol.HeaderText = "����";
            this._dateCol.Name = "_dateCol";
            this._dateCol.ReadOnly = true;
            // 
            // _oscTimeColumn
            // 
            this._oscTimeColumn.DataPropertyName = "�����";
            this._oscTimeColumn.HeaderText = "�����";
            this._oscTimeColumn.Name = "_oscTimeColumn";
            this._oscTimeColumn.ReadOnly = true;
            // 
            // _stage
            // 
            this._stage.DataPropertyName = "��� �����������";
            this._stage.HeaderText = "��� �����������";
            this._stage.Name = "_stage";
            this._stage.ReadOnly = true;
            // 
            // _type
            // 
            this._type.DataPropertyName = "��� �����������";
            this._type.HeaderText = "��� �����������";
            this._type.Name = "_type";
            this._type.ReadOnly = true;
            // 
            // _value
            // 
            this._value.DataPropertyName = "�������� �����������";
            this._value.HeaderText = "�������� �����������";
            this._value.Name = "_value";
            this._value.ReadOnly = true;
            // 
            // _Ia
            // 
            this._Ia.DataPropertyName = "Ia";
            this._Ia.HeaderText = "Ia";
            this._Ia.Name = "_Ia";
            this._Ia.ReadOnly = true;
            // 
            // _Ib
            // 
            this._Ib.DataPropertyName = "Ib";
            this._Ib.HeaderText = "Ib";
            this._Ib.Name = "_Ib";
            this._Ib.ReadOnly = true;
            // 
            // _Ic
            // 
            this._Ic.DataPropertyName = "Ic";
            this._Ic.HeaderText = "Ic";
            this._Ic.Name = "_Ic";
            this._Ic.ReadOnly = true;
            // 
            // _Io
            // 
            this._Io.DataPropertyName = "Io";
            this._Io.HeaderText = "Io";
            this._Io.Name = "_Io";
            this._Io.ReadOnly = true;
            // 
            // _I�
            // 
            this._I�.DataPropertyName = "I�";
            this._I�.HeaderText = "I�";
            this._I�.Name = "_I�";
            this._I�.ReadOnly = true;
            // 
            // _I0
            // 
            this._I0.DataPropertyName = "I0";
            this._I0.HeaderText = "I0";
            this._I0.Name = "_I0";
            this._I0.ReadOnly = true;
            // 
            // _I1
            // 
            this._I1.DataPropertyName = "I1";
            this._I1.HeaderText = "I1";
            this._I1.Name = "_I1";
            this._I1.ReadOnly = true;
            // 
            // _I2
            // 
            this._I2.DataPropertyName = "I2";
            this._I2.HeaderText = "I2";
            this._I2.Name = "_I2";
            this._I2.ReadOnly = true;
            // 
            // _graphGroup
            // 
            this._graphGroup.Controls.Add(this._resetOsc);
            this._graphGroup.Controls.Add(this._showButton);
            this._graphGroup.Controls.Add(this._readOscilloscope);
            this._graphGroup.Location = new System.Drawing.Point(12, 6);
            this._graphGroup.Name = "_graphGroup";
            this._graphGroup.Size = new System.Drawing.Size(191, 103);
            this._graphGroup.TabIndex = 2;
            this._graphGroup.TabStop = false;
            this._graphGroup.Text = "�������������";
            // 
            // _resetOsc
            // 
            this._resetOsc.Enabled = false;
            this._resetOsc.Location = new System.Drawing.Point(6, 75);
            this._resetOsc.Name = "_resetOsc";
            this._resetOsc.Size = new System.Drawing.Size(179, 23);
            this._resetOsc.TabIndex = 6;
            this._resetOsc.Text = "�������� �������������";
            this._resetOsc.UseVisualStyleBackColor = true;
            this._resetOsc.Click += new System.EventHandler(this._resetOsc_Click);
            // 
            // _showButton
            // 
            this._showButton.Enabled = false;
            this._showButton.Location = new System.Drawing.Point(7, 47);
            this._showButton.Name = "_showButton";
            this._showButton.Size = new System.Drawing.Size(179, 23);
            this._showButton.TabIndex = 5;
            this._showButton.Text = "�������� �������������";
            this._showButton.UseVisualStyleBackColor = true;
            this._showButton.Click += new System.EventHandler(this._showButton_Click);
            // 
            // _readOscilloscope
            // 
            this._readOscilloscope.Enabled = false;
            this._readOscilloscope.Location = new System.Drawing.Point(6, 19);
            this._readOscilloscope.Name = "_readOscilloscope";
            this._readOscilloscope.Size = new System.Drawing.Size(180, 23);
            this._readOscilloscope.TabIndex = 0;
            this._readOscilloscope.Text = "��������� ������������� ";
            this._readOscilloscope.UseVisualStyleBackColor = true;
            this._readOscilloscope.Click += new System.EventHandler(this._readOscilloscope_Click);
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusActionLabel,
            this._statusPercentLabel,
            this._statusProgress});
            this._statusStrip.Location = new System.Drawing.Point(0, 389);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.Size = new System.Drawing.Size(880, 22);
            this._statusStrip.TabIndex = 3;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _statusActionLabel
            // 
            this._statusActionLabel.Name = "_statusActionLabel";
            this._statusActionLabel.Size = new System.Drawing.Size(16, 17);
            this._statusActionLabel.Text = "...";
            // 
            // _statusPercentLabel
            // 
            this._statusPercentLabel.Name = "_statusPercentLabel";
            this._statusPercentLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _statusProgress
            // 
            this._statusProgress.Maximum = 256;
            this._statusProgress.Name = "_statusProgress";
            this._statusProgress.Size = new System.Drawing.Size(100, 16);
            this._statusProgress.Step = 1;
            // 
            // _saveOscilloscopeDlg
            // 
            this._saveOscilloscopeDlg.DefaultExt = "xml";
            this._saveOscilloscopeDlg.FileName = "������������� ��500";
            this._saveOscilloscopeDlg.Filter = "���� �������������|*.hdr";
            this._saveOscilloscopeDlg.Title = "���������  �������������";
            // 
            // _openOscilloscopeDlg
            // 
            this._openOscilloscopeDlg.DefaultExt = "xml";
            this._openOscilloscopeDlg.Filter = "���� �������������|*.hdr";
            this._openOscilloscopeDlg.RestoreDirectory = true;
            this._openOscilloscopeDlg.Title = "��������� �������������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._loadFromFileButton);
            this.groupBox3.Controls.Add(this._saveToFileButton);
            this.groupBox3.Location = new System.Drawing.Point(209, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(191, 78);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������������";
            // 
            // _loadFromFileButton
            // 
            this._loadFromFileButton.Location = new System.Drawing.Point(7, 19);
            this._loadFromFileButton.Name = "_loadFromFileButton";
            this._loadFromFileButton.Size = new System.Drawing.Size(179, 23);
            this._loadFromFileButton.TabIndex = 10;
            this._loadFromFileButton.Text = "��������� �� �����";
            this._loadFromFileButton.UseVisualStyleBackColor = true;
            // 
            // _saveToFileButton
            // 
            this._saveToFileButton.Enabled = false;
            this._saveToFileButton.Location = new System.Drawing.Point(7, 48);
            this._saveToFileButton.Name = "_saveToFileButton";
            this._saveToFileButton.Size = new System.Drawing.Size(179, 23);
            this._saveToFileButton.TabIndex = 9;
            this._saveToFileButton.Text = "��������� � ����";
            this._saveToFileButton.UseVisualStyleBackColor = true;
            // 
            // _reloadJournal
            // 
            this._reloadJournal.Location = new System.Drawing.Point(635, 350);
            this._reloadJournal.Name = "_reloadJournal";
            this._reloadJournal.Size = new System.Drawing.Size(233, 23);
            this._reloadJournal.TabIndex = 8;
            this._reloadJournal.Text = "��������� ������";
            this._reloadJournal.UseVisualStyleBackColor = true;
            this._reloadJournal.Click += new System.EventHandler(this._reloadJournal_Click);
            // 
            // OscilloscopeFormBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(880, 411);
            this.Controls.Add(this._reloadJournal);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._graphGroup);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OscilloscopeFormBase";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).EndInit();
            this._graphGroup.ResumeLayout(false);
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox _graphGroup;
        private System.Windows.Forms.Button _readOscilloscope;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _statusActionLabel;
        private System.Windows.Forms.ToolStripProgressBar _statusProgress;
        private System.Windows.Forms.ToolStripStatusLabel _statusPercentLabel;
        private System.Windows.Forms.Button _showButton;
        private System.Windows.Forms.SaveFileDialog _saveOscilloscopeDlg;
        private System.Windows.Forms.OpenFileDialog _openOscilloscopeDlg;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button _loadFromFileButton;
        private System.Windows.Forms.Button _saveToFileButton;
        private System.Windows.Forms.DataGridView _oscJournalDataGrid;
        private System.Windows.Forms.Button _reloadJournal;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stage;
        private System.Windows.Forms.DataGridViewTextBoxColumn _type;
        private System.Windows.Forms.DataGridViewTextBoxColumn _value;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ia;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ib;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ic;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Io;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I�;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I0;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2;
        private System.Windows.Forms.Button _resetOsc;
    }
}