﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Interfaces;

namespace BEMN.MR500
{
    public partial class OscMessageForm : Form,IFormView
    {
        public OscMessageForm()
        {
            InitializeComponent();
        }

        public OscMessageForm(Device device)
        {
            InitializeComponent();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(OscMessageForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR500.Properties.Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Осциллограмма"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("http://bemn.by/download/servisnoe-programmnoe-obespechenie/");
        }
    }
}
