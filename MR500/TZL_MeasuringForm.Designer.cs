using BEMN.Forms.Old;

namespace BEMN.MR500
{
    partial class VMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VMeasuringForm));
            this._analogPage = new System.Windows.Forms.TabPage();
            this._CosBox = new System.Windows.Forms.TextBox();
            this._releLed13Label = new System.Windows.Forms.Label();
            this._QBox = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._offSplBtn = new System.Windows.Forms.Button();
            this._onSplBtn = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this._manageLed9 = new BEMN.Forms.LedControl();
            this._selectConstraintGroupBut = new System.Windows.Forms.Button();
            this._resetIndicatBut = new System.Windows.Forms.Button();
            this.label137 = new System.Windows.Forms.Label();
            this._manageLed8 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this._manageLed5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._manageLed4 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this._manageLed3 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._manageLed7 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._manageLed6 = new BEMN.Forms.LedControl();
            this._resetJA_But = new System.Windows.Forms.Button();
            this._resetJS_But = new System.Windows.Forms.Button();
            this._resetFaultBut = new System.Windows.Forms.Button();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this._manageLed2 = new BEMN.Forms.LedControl();
            this._manageLed1 = new BEMN.Forms.LedControl();
            this._PBox = new System.Windows.Forms.TextBox();
            this._releLed13 = new BEMN.Forms.LedControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this._addIndLed4 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._addIndLed3 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._addIndLed2 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._addIndLed1 = new BEMN.Forms.LedControl();
            this._F_Box = new System.Windows.Forms.TextBox();
            this._releLed12Label = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._I1Box = new System.Windows.Forms.TextBox();
            this._I0Box = new System.Windows.Forms.TextBox();
            this._IcBox = new System.Windows.Forms.TextBox();
            this._IbBox = new System.Windows.Forms.TextBox();
            this._IaBox = new System.Windows.Forms.TextBox();
            this._InBox = new System.Windows.Forms.TextBox();
            this._IgBox = new System.Windows.Forms.TextBox();
            this._I2Box = new System.Windows.Forms.TextBox();
            this._releLed12 = new BEMN.Forms.LedControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this._inL_led8 = new BEMN.Forms.LedControl();
            this.label42 = new System.Windows.Forms.Label();
            this._inL_led7 = new BEMN.Forms.LedControl();
            this.label43 = new System.Windows.Forms.Label();
            this._inL_led6 = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._inL_led5 = new BEMN.Forms.LedControl();
            this.label45 = new System.Windows.Forms.Label();
            this._inL_led4 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this._inL_led3 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._inL_led2 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._inL_led1 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._inD_led16 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this._inD_led15 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._inD_led14 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._inD_led13 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._inD_led12 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this._inD_led11 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._inD_led10 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this._inD_led9 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._inD_led8 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._inD_led7 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this._inD_led6 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this._inD_led5 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._inD_led4 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._inD_led3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._inD_led2 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._inD_led1 = new BEMN.Forms.LedControl();
            this._releLed11Label = new System.Windows.Forms.Label();
            this._U2Box = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._outLed8 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._outLed7 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._outLed6 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._outLed5 = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._outLed4 = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._outLed3 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._outLed2 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._outLed1 = new BEMN.Forms.LedControl();
            this._U1Box = new System.Windows.Forms.TextBox();
            this._releLed11 = new BEMN.Forms.LedControl();
            this._U0Box = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._releLed8Label = new System.Windows.Forms.Label();
            this._releLed8 = new BEMN.Forms.LedControl();
            this._releLed7Label = new System.Windows.Forms.Label();
            this._releLed7 = new BEMN.Forms.LedControl();
            this._releLed6Label = new System.Windows.Forms.Label();
            this._releLed6 = new BEMN.Forms.LedControl();
            this._releLed5Label = new System.Windows.Forms.Label();
            this._releLed5 = new BEMN.Forms.LedControl();
            this._releLed4Label = new System.Windows.Forms.Label();
            this._releLed4 = new BEMN.Forms.LedControl();
            this._releLed3Label = new System.Windows.Forms.Label();
            this._releLed3 = new BEMN.Forms.LedControl();
            this._releLed2Label = new System.Windows.Forms.Label();
            this._releLed2 = new BEMN.Forms.LedControl();
            this._releLed1Label = new System.Windows.Forms.Label();
            this._releLed1 = new BEMN.Forms.LedControl();
            this._UcaBox = new System.Windows.Forms.TextBox();
            this._releLed10Label = new System.Windows.Forms.Label();
            this._UbcBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this._indLed8 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this._indLed7 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._indLed6 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._indLed5 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._indLed4 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._indLed3 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._indLed2 = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._indLed1 = new BEMN.Forms.LedControl();
            this._UabBox = new System.Windows.Forms.TextBox();
            this._releLed10 = new BEMN.Forms.LedControl();
            this._UbBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._systemTimeCheck = new System.Windows.Forms.CheckBox();
            this._dateTimeBox = new DateTimeBox();
            this._readTimeCheck = new System.Windows.Forms.CheckBox();
            this._writeTimeBut = new System.Windows.Forms.Button();
            this._UaBox = new System.Windows.Forms.TextBox();
            this._releLed9Label = new System.Windows.Forms.Label();
            this._UnBox = new System.Windows.Forms.TextBox();
            this._releLed9 = new BEMN.Forms.LedControl();
            this._UcBox = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this._diagTab = new System.Windows.Forms.TabControl();
            this._diskretPage = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.ledControl22 = new BEMN.Forms.LedControl();
            this.ledControl21 = new BEMN.Forms.LedControl();
            this.ledControl20 = new BEMN.Forms.LedControl();
            this.ledControl19 = new BEMN.Forms.LedControl();
            this.ledControl18 = new BEMN.Forms.LedControl();
            this.ledControl17 = new BEMN.Forms.LedControl();
            this.ledControl16 = new BEMN.Forms.LedControl();
            this.ledControl15 = new BEMN.Forms.LedControl();
            this.ledControl14 = new BEMN.Forms.LedControl();
            this.ledControl13 = new BEMN.Forms.LedControl();
            this.ledControl12 = new BEMN.Forms.LedControl();
            this.ledControl11 = new BEMN.Forms.LedControl();
            this.ledControl10 = new BEMN.Forms.LedControl();
            this.ledControl9 = new BEMN.Forms.LedControl();
            this.ledControl8 = new BEMN.Forms.LedControl();
            this.ledControl7 = new BEMN.Forms.LedControl();
            this.ledControl6 = new BEMN.Forms.LedControl();
            this.ledControl1 = new BEMN.Forms.LedControl();
            this.ledControl5 = new BEMN.Forms.LedControl();
            this.ledControl2 = new BEMN.Forms.LedControl();
            this.ledControl4 = new BEMN.Forms.LedControl();
            this.ledControl3 = new BEMN.Forms.LedControl();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label129 = new System.Windows.Forms.Label();
            this._faultSignalLed16 = new BEMN.Forms.LedControl();
            this._faultSignalLed15 = new BEMN.Forms.LedControl();
            this.label131 = new System.Windows.Forms.Label();
            this._faultSignalLed14 = new BEMN.Forms.LedControl();
            this.label132 = new System.Windows.Forms.Label();
            this._faultSignalLed13 = new BEMN.Forms.LedControl();
            this.label133 = new System.Windows.Forms.Label();
            this._faultSignalLed12 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._faultSignalLed11 = new BEMN.Forms.LedControl();
            this.label135 = new System.Windows.Forms.Label();
            this._faultSignalLed10 = new BEMN.Forms.LedControl();
            this.label136 = new System.Windows.Forms.Label();
            this._faultSignalLed9 = new BEMN.Forms.LedControl();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label121 = new System.Windows.Forms.Label();
            this._faultSignalLed8 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._faultSignalLed7 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._faultSignalLed6 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._faultSignalLed5 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._faultSignalLed3 = new BEMN.Forms.LedControl();
            this.label127 = new System.Windows.Forms.Label();
            this._faultSignalLed2 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this._faultSignalLed1 = new BEMN.Forms.LedControl();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label115 = new System.Windows.Forms.Label();
            this._faultStateLed6 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._faultStateLed5 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._faultStateLed3 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._faultStateLed2 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._faultStateLed1 = new BEMN.Forms.LedControl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label105 = new System.Windows.Forms.Label();
            this._extDefenseLed8 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._extDefenseLed7 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._extDefenseLed6 = new BEMN.Forms.LedControl();
            this.label108 = new System.Windows.Forms.Label();
            this._extDefenseLed5 = new BEMN.Forms.LedControl();
            this.label109 = new System.Windows.Forms.Label();
            this._extDefenseLed4 = new BEMN.Forms.LedControl();
            this.label110 = new System.Windows.Forms.Label();
            this._extDefenseLed3 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._extDefenseLed2 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._extDefenseLed1 = new BEMN.Forms.LedControl();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._chapv = new BEMN.Forms.LedControl();
            this._achr = new BEMN.Forms.LedControl();
            this.label97 = new System.Windows.Forms.Label();
            this._autoLed8 = new BEMN.Forms.LedControl();
            this.label98 = new System.Windows.Forms.Label();
            this._autoLed7 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this._autoLed6 = new BEMN.Forms.LedControl();
            this.label100 = new System.Windows.Forms.Label();
            this._autoLed5 = new BEMN.Forms.LedControl();
            this.label101 = new System.Windows.Forms.Label();
            this._autoLed4 = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._autoLed3 = new BEMN.Forms.LedControl();
            this.label103 = new System.Windows.Forms.Label();
            this._autoLed2 = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this._autoLed1 = new BEMN.Forms.LedControl();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._I21Led8 = new BEMN.Forms.LedControl();
            this._I21Led7 = new BEMN.Forms.LedControl();
            this._IgLed6 = new BEMN.Forms.LedControl();
            this._IgLed5 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this._InmaxLed4 = new BEMN.Forms.LedControl();
            this._InmaxLed3 = new BEMN.Forms.LedControl();
            this._InmaxLed2 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._InmaxLed1 = new BEMN.Forms.LedControl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._I0maxLed8 = new BEMN.Forms.LedControl();
            this._I0maxLed7 = new BEMN.Forms.LedControl();
            this._I0maxLed6 = new BEMN.Forms.LedControl();
            this._I0maxLed5 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._I0maxLed4 = new BEMN.Forms.LedControl();
            this._I0maxLed3 = new BEMN.Forms.LedControl();
            this._I0maxLed2 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._I0maxLed1 = new BEMN.Forms.LedControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ImaxLed8 = new BEMN.Forms.LedControl();
            this._ImaxLed7 = new BEMN.Forms.LedControl();
            this._ImaxLed6 = new BEMN.Forms.LedControl();
            this._ImaxLed5 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this._ImaxLed4 = new BEMN.Forms.LedControl();
            this._ImaxLed3 = new BEMN.Forms.LedControl();
            this._ImaxLed2 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._ImaxLed1 = new BEMN.Forms.LedControl();
            this._images = new System.Windows.Forms.ImageList(this.components);
            this.label139 = new System.Windows.Forms.Label();
            this.label14_releLed9Label0 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this._analogPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._diagTab.SuspendLayout();
            this._diskretPage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // _analogPage
            // 
            this._analogPage.AllowDrop = true;
            this._analogPage.Controls.Add(this._CosBox);
            this._analogPage.Controls.Add(this._releLed13Label);
            this._analogPage.Controls.Add(this._QBox);
            this._analogPage.Controls.Add(this.groupBox8);
            this._analogPage.Controls.Add(this._PBox);
            this._analogPage.Controls.Add(this._releLed13);
            this._analogPage.Controls.Add(this.groupBox7);
            this._analogPage.Controls.Add(this._F_Box);
            this._analogPage.Controls.Add(this._releLed12Label);
            this._analogPage.Controls.Add(this.groupBox6);
            this._analogPage.Controls.Add(this._releLed12);
            this._analogPage.Controls.Add(this.groupBox5);
            this._analogPage.Controls.Add(this._releLed11Label);
            this._analogPage.Controls.Add(this._U2Box);
            this._analogPage.Controls.Add(this.groupBox4);
            this._analogPage.Controls.Add(this._U1Box);
            this._analogPage.Controls.Add(this._releLed11);
            this._analogPage.Controls.Add(this._U0Box);
            this._analogPage.Controls.Add(this.groupBox3);
            this._analogPage.Controls.Add(this._UcaBox);
            this._analogPage.Controls.Add(this._releLed10Label);
            this._analogPage.Controls.Add(this._UbcBox);
            this._analogPage.Controls.Add(this.groupBox2);
            this._analogPage.Controls.Add(this._UabBox);
            this._analogPage.Controls.Add(this._releLed10);
            this._analogPage.Controls.Add(this._UbBox);
            this._analogPage.Controls.Add(this.groupBox1);
            this._analogPage.Controls.Add(this._UaBox);
            this._analogPage.Controls.Add(this._releLed9Label);
            this._analogPage.Controls.Add(this._UnBox);
            this._analogPage.Controls.Add(this._releLed9);
            this._analogPage.Controls.Add(this._UcBox);
            this._analogPage.ImageIndex = 0;
            this._analogPage.Location = new System.Drawing.Point(4, 23);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogPage.Size = new System.Drawing.Size(576, 390);
            this._analogPage.TabIndex = 0;
            this._analogPage.Text = "���������� �� ";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // _CosBox
            // 
            this._CosBox.Enabled = false;
            this._CosBox.Location = new System.Drawing.Point(626, 197);
            this._CosBox.Name = "_CosBox";
            this._CosBox.Size = new System.Drawing.Size(68, 20);
            this._CosBox.TabIndex = 27;
            this._CosBox.Visible = false;
            // 
            // _releLed13Label
            // 
            this._releLed13Label.AutoSize = true;
            this._releLed13Label.Location = new System.Drawing.Point(774, 141);
            this._releLed13Label.Name = "_releLed13Label";
            this._releLed13Label.Size = new System.Drawing.Size(19, 13);
            this._releLed13Label.TabIndex = 41;
            this._releLed13Label.Text = "13";
            this._releLed13Label.Visible = false;
            // 
            // _QBox
            // 
            this._QBox.Enabled = false;
            this._QBox.Location = new System.Drawing.Point(715, 292);
            this._QBox.Name = "_QBox";
            this._QBox.Size = new System.Drawing.Size(100, 20);
            this._QBox.TabIndex = 26;
            this._QBox.Visible = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox11);
            this.groupBox8.Controls.Add(this._selectConstraintGroupBut);
            this.groupBox8.Controls.Add(this._resetIndicatBut);
            this.groupBox8.Controls.Add(this.label137);
            this.groupBox8.Controls.Add(this._manageLed8);
            this.groupBox8.Controls.Add(this.label53);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this._manageLed5);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this._manageLed4);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Controls.Add(this._manageLed3);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this._manageLed7);
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Controls.Add(this._manageLed6);
            this.groupBox8.Controls.Add(this._resetJA_But);
            this.groupBox8.Controls.Add(this._resetJS_But);
            this.groupBox8.Controls.Add(this._resetFaultBut);
            this.groupBox8.Controls.Add(this._breakerOffBut);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this._breakerOnBut);
            this.groupBox8.Controls.Add(this.label60);
            this.groupBox8.Controls.Add(this._manageLed2);
            this.groupBox8.Controls.Add(this._manageLed1);
            this.groupBox8.Location = new System.Drawing.Point(325, 7);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(248, 250);
            this.groupBox8.TabIndex = 12;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "����������� �������(����)";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._offSplBtn);
            this.groupBox11.Controls.Add(this._onSplBtn);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this._manageLed9);
            this.groupBox11.Location = new System.Drawing.Point(6, 182);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(239, 61);
            this.groupBox11.TabIndex = 72;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "���";
            // 
            // _offSplBtn
            // 
            this._offSplBtn.Location = new System.Drawing.Point(160, 33);
            this._offSplBtn.Name = "_offSplBtn";
            this._offSplBtn.Size = new System.Drawing.Size(75, 23);
            this._offSplBtn.TabIndex = 53;
            this._offSplBtn.Text = "���������";
            this._offSplBtn.UseVisualStyleBackColor = true;
            this._offSplBtn.Click += new System.EventHandler(this.OffSplBtnClock);
            // 
            // _onSplBtn
            // 
            this._onSplBtn.Location = new System.Drawing.Point(160, 11);
            this._onSplBtn.Name = "_onSplBtn";
            this._onSplBtn.Size = new System.Drawing.Size(75, 23);
            this._onSplBtn.TabIndex = 52;
            this._onSplBtn.Text = "��������";
            this._onSplBtn.UseVisualStyleBackColor = true;
            this._onSplBtn.Click += new System.EventHandler(this.OnSplBtnClock);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 59;
            this.label12.Text = "������ ���";
            // 
            // _manageLed9
            // 
            this._manageLed9.Location = new System.Drawing.Point(6, 27);
            this._manageLed9.Name = "_manageLed9";
            this._manageLed9.Size = new System.Drawing.Size(13, 13);
            this._manageLed9.State = BEMN.Forms.LedState.Off;
            this._manageLed9.TabIndex = 67;
            // 
            // _selectConstraintGroupBut
            // 
            this._selectConstraintGroupBut.Location = new System.Drawing.Point(167, 78);
            this._selectConstraintGroupBut.Name = "_selectConstraintGroupBut";
            this._selectConstraintGroupBut.Size = new System.Drawing.Size(75, 23);
            this._selectConstraintGroupBut.TabIndex = 71;
            this._selectConstraintGroupBut.Text = "������.";
            this._selectConstraintGroupBut.UseVisualStyleBackColor = true;
            this._selectConstraintGroupBut.Click += new System.EventHandler(this._selectConstraintGroupBut_Click_1);
            // 
            // _resetIndicatBut
            // 
            this._resetIndicatBut.Location = new System.Drawing.Point(167, 58);
            this._resetIndicatBut.Name = "_resetIndicatBut";
            this._resetIndicatBut.Size = new System.Drawing.Size(75, 21);
            this._resetIndicatBut.TabIndex = 69;
            this._resetIndicatBut.Text = "��. ���.";
            this._resetIndicatBut.UseVisualStyleBackColor = true;
            this._resetIndicatBut.Click += new System.EventHandler(this._resetIndicatBut_Click_1);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(28, 101);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(107, 13);
            this.label137.TabIndex = 68;
            this.label137.Text = "��������� �������";
            this.label137.Visible = false;
            // 
            // _manageLed8
            // 
            this._manageLed8.Location = new System.Drawing.Point(12, 165);
            this._manageLed8.Name = "_manageLed8";
            this._manageLed8.Size = new System.Drawing.Size(13, 13);
            this._manageLed8.State = BEMN.Forms.LedState.Off;
            this._manageLed8.TabIndex = 67;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(9, 194);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(0, 13);
            this.label53.TabIndex = 66;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(28, 123);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(136, 13);
            this.label51.TabIndex = 65;
            this.label51.Text = "������� ��������������";
            // 
            // _manageLed5
            // 
            this._manageLed5.Location = new System.Drawing.Point(12, 102);
            this._manageLed5.Name = "_manageLed5";
            this._manageLed5.Size = new System.Drawing.Size(13, 13);
            this._manageLed5.State = BEMN.Forms.LedState.Off;
            this._manageLed5.TabIndex = 64;
            this._manageLed5.Visible = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(28, 81);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(85, 13);
            this.label58.TabIndex = 63;
            this.label58.Text = "������ �������";
            // 
            // _manageLed4
            // 
            this._manageLed4.Location = new System.Drawing.Point(12, 81);
            this._manageLed4.Name = "_manageLed4";
            this._manageLed4.Size = new System.Drawing.Size(13, 13);
            this._manageLed4.State = BEMN.Forms.LedState.Off;
            this._manageLed4.TabIndex = 62;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(28, 60);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(86, 13);
            this.label62.TabIndex = 61;
            this.label62.Text = "�������������";
            // 
            // _manageLed3
            // 
            this._manageLed3.Location = new System.Drawing.Point(12, 60);
            this._manageLed3.Name = "_manageLed3";
            this._manageLed3.Size = new System.Drawing.Size(13, 13);
            this._manageLed3.State = BEMN.Forms.LedState.Off;
            this._manageLed3.TabIndex = 60;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(28, 165);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(99, 13);
            this.label61.TabIndex = 59;
            this.label61.Text = "����� ������ ��";
            // 
            // _manageLed7
            // 
            this._manageLed7.Location = new System.Drawing.Point(12, 144);
            this._manageLed7.Name = "_manageLed7";
            this._manageLed7.Size = new System.Drawing.Size(13, 13);
            this._manageLed7.State = BEMN.Forms.LedState.Off;
            this._manageLed7.TabIndex = 58;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(28, 145);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(99, 13);
            this.label59.TabIndex = 57;
            this.label59.Text = "����� ������ ��";
            // 
            // _manageLed6
            // 
            this._manageLed6.Location = new System.Drawing.Point(12, 123);
            this._manageLed6.Name = "_manageLed6";
            this._manageLed6.Size = new System.Drawing.Size(13, 13);
            this._manageLed6.State = BEMN.Forms.LedState.Off;
            this._manageLed6.TabIndex = 56;
            // 
            // _resetJA_But
            // 
            this._resetJA_But.Location = new System.Drawing.Point(167, 160);
            this._resetJA_But.Name = "_resetJA_But";
            this._resetJA_But.Size = new System.Drawing.Size(75, 23);
            this._resetJA_But.TabIndex = 53;
            this._resetJA_But.Text = "��������";
            this._resetJA_But.UseVisualStyleBackColor = true;
            this._resetJA_But.Click += new System.EventHandler(this._resetJA_But_Click_1);
            // 
            // _resetJS_But
            // 
            this._resetJS_But.Location = new System.Drawing.Point(167, 138);
            this._resetJS_But.Name = "_resetJS_But";
            this._resetJS_But.Size = new System.Drawing.Size(75, 23);
            this._resetJS_But.TabIndex = 52;
            this._resetJS_But.Text = "��������";
            this._resetJS_But.UseVisualStyleBackColor = true;
            this._resetJS_But.Click += new System.EventHandler(this._resetJS_But_Click_1);
            // 
            // _resetFaultBut
            // 
            this._resetFaultBut.Location = new System.Drawing.Point(167, 116);
            this._resetFaultBut.Name = "_resetFaultBut";
            this._resetFaultBut.Size = new System.Drawing.Size(75, 23);
            this._resetFaultBut.TabIndex = 51;
            this._resetFaultBut.Text = "��������";
            this._resetFaultBut.UseVisualStyleBackColor = true;
            this._resetFaultBut.Click += new System.EventHandler(this._resetFaultBut_Click_1);
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(167, 35);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 47;
            this._breakerOffBut.Text = "���������";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click_1);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(28, 39);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(122, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "����������� �������";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(167, 13);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 45;
            this._breakerOnBut.Text = "��������";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click_1);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(28, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(127, 13);
            this.label60.TabIndex = 38;
            this.label60.Text = "����������� ��������";
            // 
            // _manageLed2
            // 
            this._manageLed2.Location = new System.Drawing.Point(12, 39);
            this._manageLed2.Name = "_manageLed2";
            this._manageLed2.Size = new System.Drawing.Size(13, 13);
            this._manageLed2.State = BEMN.Forms.LedState.Off;
            this._manageLed2.TabIndex = 32;
            // 
            // _manageLed1
            // 
            this._manageLed1.Location = new System.Drawing.Point(12, 18);
            this._manageLed1.Name = "_manageLed1";
            this._manageLed1.Size = new System.Drawing.Size(13, 13);
            this._manageLed1.State = BEMN.Forms.LedState.Off;
            this._manageLed1.TabIndex = 31;
            // 
            // _PBox
            // 
            this._PBox.Enabled = false;
            this._PBox.Location = new System.Drawing.Point(604, 292);
            this._PBox.Name = "_PBox";
            this._PBox.Size = new System.Drawing.Size(104, 20);
            this._PBox.TabIndex = 25;
            this._PBox.Visible = false;
            // 
            // _releLed13
            // 
            this._releLed13.Location = new System.Drawing.Point(755, 141);
            this._releLed13.Name = "_releLed13";
            this._releLed13.Size = new System.Drawing.Size(13, 13);
            this._releLed13.State = BEMN.Forms.LedState.Off;
            this._releLed13.TabIndex = 40;
            this._releLed13.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Controls.Add(this._addIndLed4);
            this.groupBox7.Controls.Add(this.label52);
            this.groupBox7.Controls.Add(this._addIndLed3);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this._addIndLed2);
            this.groupBox7.Controls.Add(this.label56);
            this.groupBox7.Controls.Add(this._addIndLed1);
            this.groupBox7.Location = new System.Drawing.Point(325, 263);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(127, 100);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "���.����������";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(31, 78);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(66, 13);
            this.label49.TabIndex = 31;
            this.label49.Text = "���.������";
            // 
            // _addIndLed4
            // 
            this._addIndLed4.Location = new System.Drawing.Point(12, 57);
            this._addIndLed4.Name = "_addIndLed4";
            this._addIndLed4.Size = new System.Drawing.Size(13, 13);
            this._addIndLed4.State = BEMN.Forms.LedState.Off;
            this._addIndLed4.TabIndex = 30;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(31, 57);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(75, 13);
            this.label52.TabIndex = 25;
            this.label52.Text = "���.�������";
            // 
            // _addIndLed3
            // 
            this._addIndLed3.Location = new System.Drawing.Point(12, 78);
            this._addIndLed3.Name = "_addIndLed3";
            this._addIndLed3.Size = new System.Drawing.Size(13, 13);
            this._addIndLed3.State = BEMN.Forms.LedState.Off;
            this._addIndLed3.TabIndex = 24;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(31, 36);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(73, 13);
            this.label54.TabIndex = 21;
            this.label54.Text = "���.�������";
            // 
            // _addIndLed2
            // 
            this._addIndLed2.Location = new System.Drawing.Point(12, 36);
            this._addIndLed2.Name = "_addIndLed2";
            this._addIndLed2.Size = new System.Drawing.Size(13, 13);
            this._addIndLed2.State = BEMN.Forms.LedState.Off;
            this._addIndLed2.TabIndex = 20;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(31, 15);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(78, 13);
            this.label56.TabIndex = 17;
            this.label56.Text = "���.��������";
            // 
            // _addIndLed1
            // 
            this._addIndLed1.Location = new System.Drawing.Point(12, 15);
            this._addIndLed1.Name = "_addIndLed1";
            this._addIndLed1.Size = new System.Drawing.Size(13, 13);
            this._addIndLed1.State = BEMN.Forms.LedState.Off;
            this._addIndLed1.TabIndex = 16;
            // 
            // _F_Box
            // 
            this._F_Box.Enabled = false;
            this._F_Box.Location = new System.Drawing.Point(626, 269);
            this._F_Box.Name = "_F_Box";
            this._F_Box.Size = new System.Drawing.Size(68, 20);
            this._F_Box.TabIndex = 9;
            this._F_Box.Visible = false;
            // 
            // _releLed12Label
            // 
            this._releLed12Label.AutoSize = true;
            this._releLed12Label.Location = new System.Drawing.Point(774, 126);
            this._releLed12Label.Name = "_releLed12Label";
            this._releLed12Label.Size = new System.Drawing.Size(19, 13);
            this._releLed12Label.TabIndex = 39;
            this._releLed12Label.Text = "12";
            this._releLed12Label.Visible = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._I1Box);
            this.groupBox6.Controls.Add(this._I0Box);
            this.groupBox6.Controls.Add(this._IcBox);
            this.groupBox6.Controls.Add(this._IbBox);
            this.groupBox6.Controls.Add(this._IaBox);
            this.groupBox6.Controls.Add(this._InBox);
            this.groupBox6.Controls.Add(this._IgBox);
            this.groupBox6.Controls.Add(this._I2Box);
            this.groupBox6.Location = new System.Drawing.Point(8, 167);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(158, 142);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���������";
            // 
            // _I1Box
            // 
            this._I1Box.Enabled = false;
            this._I1Box.Location = new System.Drawing.Point(76, 15);
            this._I1Box.Name = "_I1Box";
            this._I1Box.Size = new System.Drawing.Size(68, 20);
            this._I1Box.TabIndex = 24;
            // 
            // _I0Box
            // 
            this._I0Box.Enabled = false;
            this._I0Box.Location = new System.Drawing.Point(7, 111);
            this._I0Box.Name = "_I0Box";
            this._I0Box.Size = new System.Drawing.Size(68, 20);
            this._I0Box.TabIndex = 23;
            // 
            // _IcBox
            // 
            this._IcBox.Enabled = false;
            this._IcBox.Location = new System.Drawing.Point(7, 87);
            this._IcBox.Name = "_IcBox";
            this._IcBox.Size = new System.Drawing.Size(68, 20);
            this._IcBox.TabIndex = 22;
            // 
            // _IbBox
            // 
            this._IbBox.Enabled = false;
            this._IbBox.Location = new System.Drawing.Point(7, 63);
            this._IbBox.Name = "_IbBox";
            this._IbBox.Size = new System.Drawing.Size(68, 20);
            this._IbBox.TabIndex = 21;
            // 
            // _IaBox
            // 
            this._IaBox.Enabled = false;
            this._IaBox.Location = new System.Drawing.Point(7, 39);
            this._IaBox.Name = "_IaBox";
            this._IaBox.Size = new System.Drawing.Size(68, 20);
            this._IaBox.TabIndex = 20;
            // 
            // _InBox
            // 
            this._InBox.Enabled = false;
            this._InBox.Location = new System.Drawing.Point(7, 15);
            this._InBox.Name = "_InBox";
            this._InBox.Size = new System.Drawing.Size(68, 20);
            this._InBox.TabIndex = 19;
            // 
            // _IgBox
            // 
            this._IgBox.Enabled = false;
            this._IgBox.Location = new System.Drawing.Point(76, 63);
            this._IgBox.Name = "_IgBox";
            this._IgBox.Size = new System.Drawing.Size(68, 20);
            this._IgBox.TabIndex = 8;
            // 
            // _I2Box
            // 
            this._I2Box.Enabled = false;
            this._I2Box.Location = new System.Drawing.Point(76, 39);
            this._I2Box.Name = "_I2Box";
            this._I2Box.Size = new System.Drawing.Size(68, 20);
            this._I2Box.TabIndex = 7;
            // 
            // _releLed12
            // 
            this._releLed12.Location = new System.Drawing.Point(755, 126);
            this._releLed12.Name = "_releLed12";
            this._releLed12.Size = new System.Drawing.Size(13, 13);
            this._releLed12.State = BEMN.Forms.LedState.Off;
            this._releLed12.TabIndex = 38;
            this._releLed12.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this._inL_led8);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this._inL_led7);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this._inL_led6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this._inL_led5);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this._inL_led4);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this._inL_led3);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this._inL_led2);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this._inL_led1);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this._inD_led16);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this._inD_led15);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this._inD_led14);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this._inD_led13);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this._inD_led12);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this._inD_led11);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this._inD_led10);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this._inD_led9);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this._inD_led8);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this._inD_led7);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this._inD_led6);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this._inD_led5);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this._inD_led4);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this._inD_led3);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this._inD_led2);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this._inD_led1);
            this.groupBox5.Location = new System.Drawing.Point(186, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(133, 155);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "������� �������";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(100, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 79;
            this.label41.Text = "��8";
            // 
            // _inL_led8
            // 
            this._inL_led8.Location = new System.Drawing.Point(87, 135);
            this._inL_led8.Name = "_inL_led8";
            this._inL_led8.Size = new System.Drawing.Size(13, 13);
            this._inL_led8.State = BEMN.Forms.LedState.Off;
            this._inL_led8.TabIndex = 78;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(100, 120);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 77;
            this.label42.Text = "��7";
            // 
            // _inL_led7
            // 
            this._inL_led7.Location = new System.Drawing.Point(87, 120);
            this._inL_led7.Name = "_inL_led7";
            this._inL_led7.Size = new System.Drawing.Size(13, 13);
            this._inL_led7.State = BEMN.Forms.LedState.Off;
            this._inL_led7.TabIndex = 76;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(100, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 75;
            this.label43.Text = "��6";
            // 
            // _inL_led6
            // 
            this._inL_led6.Location = new System.Drawing.Point(87, 105);
            this._inL_led6.Name = "_inL_led6";
            this._inL_led6.Size = new System.Drawing.Size(13, 13);
            this._inL_led6.State = BEMN.Forms.LedState.Off;
            this._inL_led6.TabIndex = 74;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(100, 90);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 73;
            this.label44.Text = "��5";
            // 
            // _inL_led5
            // 
            this._inL_led5.Location = new System.Drawing.Point(87, 90);
            this._inL_led5.Name = "_inL_led5";
            this._inL_led5.Size = new System.Drawing.Size(13, 13);
            this._inL_led5.State = BEMN.Forms.LedState.Off;
            this._inL_led5.TabIndex = 72;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(100, 75);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 71;
            this.label45.Text = "��4";
            // 
            // _inL_led4
            // 
            this._inL_led4.Location = new System.Drawing.Point(87, 75);
            this._inL_led4.Name = "_inL_led4";
            this._inL_led4.Size = new System.Drawing.Size(13, 13);
            this._inL_led4.State = BEMN.Forms.LedState.Off;
            this._inL_led4.TabIndex = 70;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(100, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 69;
            this.label46.Text = "��3";
            // 
            // _inL_led3
            // 
            this._inL_led3.Location = new System.Drawing.Point(87, 60);
            this._inL_led3.Name = "_inL_led3";
            this._inL_led3.Size = new System.Drawing.Size(13, 13);
            this._inL_led3.State = BEMN.Forms.LedState.Off;
            this._inL_led3.TabIndex = 68;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(100, 45);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(28, 13);
            this.label47.TabIndex = 67;
            this.label47.Text = "��2";
            // 
            // _inL_led2
            // 
            this._inL_led2.Location = new System.Drawing.Point(87, 45);
            this._inL_led2.Name = "_inL_led2";
            this._inL_led2.Size = new System.Drawing.Size(13, 13);
            this._inL_led2.State = BEMN.Forms.LedState.Off;
            this._inL_led2.TabIndex = 66;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(100, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(28, 13);
            this.label48.TabIndex = 65;
            this.label48.Text = "��1";
            // 
            // _inL_led1
            // 
            this._inL_led1.Location = new System.Drawing.Point(87, 30);
            this._inL_led1.Name = "_inL_led1";
            this._inL_led1.Size = new System.Drawing.Size(13, 13);
            this._inL_led1.State = BEMN.Forms.LedState.Off;
            this._inL_led1.TabIndex = 64;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(60, 135);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 63;
            this.label33.Text = "�16";
            // 
            // _inD_led16
            // 
            this._inD_led16.Location = new System.Drawing.Point(47, 135);
            this._inD_led16.Name = "_inD_led16";
            this._inD_led16.Size = new System.Drawing.Size(13, 13);
            this._inD_led16.State = BEMN.Forms.LedState.Off;
            this._inD_led16.TabIndex = 62;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(60, 120);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 61;
            this.label34.Text = "�15";
            // 
            // _inD_led15
            // 
            this._inD_led15.Location = new System.Drawing.Point(47, 120);
            this._inD_led15.Name = "_inD_led15";
            this._inD_led15.Size = new System.Drawing.Size(13, 13);
            this._inD_led15.State = BEMN.Forms.LedState.Off;
            this._inD_led15.TabIndex = 60;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(60, 105);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 59;
            this.label35.Text = "�14";
            // 
            // _inD_led14
            // 
            this._inD_led14.Location = new System.Drawing.Point(47, 105);
            this._inD_led14.Name = "_inD_led14";
            this._inD_led14.Size = new System.Drawing.Size(13, 13);
            this._inD_led14.State = BEMN.Forms.LedState.Off;
            this._inD_led14.TabIndex = 58;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(60, 90);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 57;
            this.label36.Text = "�13";
            // 
            // _inD_led13
            // 
            this._inD_led13.Location = new System.Drawing.Point(47, 90);
            this._inD_led13.Name = "_inD_led13";
            this._inD_led13.Size = new System.Drawing.Size(13, 13);
            this._inD_led13.State = BEMN.Forms.LedState.Off;
            this._inD_led13.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(60, 75);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 55;
            this.label37.Text = "�12";
            // 
            // _inD_led12
            // 
            this._inD_led12.Location = new System.Drawing.Point(47, 75);
            this._inD_led12.Name = "_inD_led12";
            this._inD_led12.Size = new System.Drawing.Size(13, 13);
            this._inD_led12.State = BEMN.Forms.LedState.Off;
            this._inD_led12.TabIndex = 54;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(60, 60);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(28, 13);
            this.label38.TabIndex = 53;
            this.label38.Text = "�11";
            // 
            // _inD_led11
            // 
            this._inD_led11.Location = new System.Drawing.Point(47, 60);
            this._inD_led11.Name = "_inD_led11";
            this._inD_led11.Size = new System.Drawing.Size(13, 13);
            this._inD_led11.State = BEMN.Forms.LedState.Off;
            this._inD_led11.TabIndex = 52;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(60, 45);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 51;
            this.label39.Text = "�10";
            // 
            // _inD_led10
            // 
            this._inD_led10.Location = new System.Drawing.Point(47, 45);
            this._inD_led10.Name = "_inD_led10";
            this._inD_led10.Size = new System.Drawing.Size(13, 13);
            this._inD_led10.State = BEMN.Forms.LedState.Off;
            this._inD_led10.TabIndex = 50;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(60, 30);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(22, 13);
            this.label40.TabIndex = 49;
            this.label40.Text = "�9";
            // 
            // _inD_led9
            // 
            this._inD_led9.Location = new System.Drawing.Point(47, 30);
            this._inD_led9.Name = "_inD_led9";
            this._inD_led9.Size = new System.Drawing.Size(13, 13);
            this._inD_led9.State = BEMN.Forms.LedState.Off;
            this._inD_led9.TabIndex = 48;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(23, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 47;
            this.label25.Text = "�8";
            // 
            // _inD_led8
            // 
            this._inD_led8.Location = new System.Drawing.Point(9, 135);
            this._inD_led8.Name = "_inD_led8";
            this._inD_led8.Size = new System.Drawing.Size(13, 13);
            this._inD_led8.State = BEMN.Forms.LedState.Off;
            this._inD_led8.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 45;
            this.label26.Text = "�7";
            // 
            // _inD_led7
            // 
            this._inD_led7.Location = new System.Drawing.Point(9, 120);
            this._inD_led7.Name = "_inD_led7";
            this._inD_led7.Size = new System.Drawing.Size(13, 13);
            this._inD_led7.State = BEMN.Forms.LedState.Off;
            this._inD_led7.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(23, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "�6";
            // 
            // _inD_led6
            // 
            this._inD_led6.Location = new System.Drawing.Point(9, 105);
            this._inD_led6.Name = "_inD_led6";
            this._inD_led6.Size = new System.Drawing.Size(13, 13);
            this._inD_led6.State = BEMN.Forms.LedState.Off;
            this._inD_led6.TabIndex = 42;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(23, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "�5";
            // 
            // _inD_led5
            // 
            this._inD_led5.Location = new System.Drawing.Point(9, 90);
            this._inD_led5.Name = "_inD_led5";
            this._inD_led5.Size = new System.Drawing.Size(13, 13);
            this._inD_led5.State = BEMN.Forms.LedState.Off;
            this._inD_led5.TabIndex = 40;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(23, 75);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "�4";
            // 
            // _inD_led4
            // 
            this._inD_led4.Location = new System.Drawing.Point(9, 75);
            this._inD_led4.Name = "_inD_led4";
            this._inD_led4.Size = new System.Drawing.Size(13, 13);
            this._inD_led4.State = BEMN.Forms.LedState.Off;
            this._inD_led4.TabIndex = 38;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(23, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 37;
            this.label30.Text = "�3";
            // 
            // _inD_led3
            // 
            this._inD_led3.Location = new System.Drawing.Point(9, 60);
            this._inD_led3.Name = "_inD_led3";
            this._inD_led3.Size = new System.Drawing.Size(13, 13);
            this._inD_led3.State = BEMN.Forms.LedState.Off;
            this._inD_led3.TabIndex = 36;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(23, 45);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "�2";
            // 
            // _inD_led2
            // 
            this._inD_led2.Location = new System.Drawing.Point(9, 45);
            this._inD_led2.Name = "_inD_led2";
            this._inD_led2.Size = new System.Drawing.Size(13, 13);
            this._inD_led2.State = BEMN.Forms.LedState.Off;
            this._inD_led2.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(23, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(22, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "�1";
            // 
            // _inD_led1
            // 
            this._inD_led1.Location = new System.Drawing.Point(9, 30);
            this._inD_led1.Name = "_inD_led1";
            this._inD_led1.Size = new System.Drawing.Size(13, 13);
            this._inD_led1.State = BEMN.Forms.LedState.Off;
            this._inD_led1.TabIndex = 32;
            // 
            // _releLed11Label
            // 
            this._releLed11Label.AutoSize = true;
            this._releLed11Label.Location = new System.Drawing.Point(774, 111);
            this._releLed11Label.Name = "_releLed11Label";
            this._releLed11Label.Size = new System.Drawing.Size(19, 13);
            this._releLed11Label.TabIndex = 37;
            this._releLed11Label.Text = "11";
            this._releLed11Label.Visible = false;
            // 
            // _U2Box
            // 
            this._U2Box.Enabled = false;
            this._U2Box.Location = new System.Drawing.Point(774, 269);
            this._U2Box.Name = "_U2Box";
            this._U2Box.Size = new System.Drawing.Size(68, 20);
            this._U2Box.TabIndex = 18;
            this._U2Box.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._outLed8);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._outLed7);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this._outLed6);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this._outLed5);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this._outLed4);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this._outLed3);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this._outLed2);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this._outLed1);
            this.groupBox4.Location = new System.Drawing.Point(112, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(68, 155);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "���. �������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 135);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "���8";
            // 
            // _outLed8
            // 
            this._outLed8.Location = new System.Drawing.Point(9, 135);
            this._outLed8.Name = "_outLed8";
            this._outLed8.Size = new System.Drawing.Size(13, 13);
            this._outLed8.State = BEMN.Forms.LedState.Off;
            this._outLed8.TabIndex = 46;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(28, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "���7";
            // 
            // _outLed7
            // 
            this._outLed7.Location = new System.Drawing.Point(9, 120);
            this._outLed7.Name = "_outLed7";
            this._outLed7.Size = new System.Drawing.Size(13, 13);
            this._outLed7.State = BEMN.Forms.LedState.Off;
            this._outLed7.TabIndex = 44;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 105);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "���6";
            // 
            // _outLed6
            // 
            this._outLed6.Location = new System.Drawing.Point(9, 105);
            this._outLed6.Name = "_outLed6";
            this._outLed6.Size = new System.Drawing.Size(13, 13);
            this._outLed6.State = BEMN.Forms.LedState.Off;
            this._outLed6.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "���5";
            // 
            // _outLed5
            // 
            this._outLed5.Location = new System.Drawing.Point(9, 90);
            this._outLed5.Name = "_outLed5";
            this._outLed5.Size = new System.Drawing.Size(13, 13);
            this._outLed5.State = BEMN.Forms.LedState.Off;
            this._outLed5.TabIndex = 40;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "���4";
            // 
            // _outLed4
            // 
            this._outLed4.Location = new System.Drawing.Point(9, 75);
            this._outLed4.Name = "_outLed4";
            this._outLed4.Size = new System.Drawing.Size(13, 13);
            this._outLed4.State = BEMN.Forms.LedState.Off;
            this._outLed4.TabIndex = 38;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(28, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "���3";
            // 
            // _outLed3
            // 
            this._outLed3.Location = new System.Drawing.Point(9, 60);
            this._outLed3.Name = "_outLed3";
            this._outLed3.Size = new System.Drawing.Size(13, 13);
            this._outLed3.State = BEMN.Forms.LedState.Off;
            this._outLed3.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 13);
            this.label23.TabIndex = 35;
            this.label23.Text = "���2";
            // 
            // _outLed2
            // 
            this._outLed2.Location = new System.Drawing.Point(9, 45);
            this._outLed2.Name = "_outLed2";
            this._outLed2.Size = new System.Drawing.Size(13, 13);
            this._outLed2.State = BEMN.Forms.LedState.Off;
            this._outLed2.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "���1";
            // 
            // _outLed1
            // 
            this._outLed1.Location = new System.Drawing.Point(9, 30);
            this._outLed1.Name = "_outLed1";
            this._outLed1.Size = new System.Drawing.Size(13, 13);
            this._outLed1.State = BEMN.Forms.LedState.Off;
            this._outLed1.TabIndex = 32;
            // 
            // _U1Box
            // 
            this._U1Box.Enabled = false;
            this._U1Box.Location = new System.Drawing.Point(774, 245);
            this._U1Box.Name = "_U1Box";
            this._U1Box.Size = new System.Drawing.Size(68, 20);
            this._U1Box.TabIndex = 17;
            this._U1Box.Visible = false;
            // 
            // _releLed11
            // 
            this._releLed11.Location = new System.Drawing.Point(755, 111);
            this._releLed11.Name = "_releLed11";
            this._releLed11.Size = new System.Drawing.Size(13, 13);
            this._releLed11.State = BEMN.Forms.LedState.Off;
            this._releLed11.TabIndex = 36;
            this._releLed11.Visible = false;
            // 
            // _U0Box
            // 
            this._U0Box.Enabled = false;
            this._U0Box.Location = new System.Drawing.Point(774, 221);
            this._U0Box.Name = "_U0Box";
            this._U0Box.Size = new System.Drawing.Size(68, 20);
            this._U0Box.TabIndex = 16;
            this._U0Box.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._releLed8Label);
            this.groupBox3.Controls.Add(this._releLed8);
            this.groupBox3.Controls.Add(this._releLed7Label);
            this.groupBox3.Controls.Add(this._releLed7);
            this.groupBox3.Controls.Add(this._releLed6Label);
            this.groupBox3.Controls.Add(this._releLed6);
            this.groupBox3.Controls.Add(this._releLed5Label);
            this.groupBox3.Controls.Add(this._releLed5);
            this.groupBox3.Controls.Add(this._releLed4Label);
            this.groupBox3.Controls.Add(this._releLed4);
            this.groupBox3.Controls.Add(this._releLed3Label);
            this.groupBox3.Controls.Add(this._releLed3);
            this.groupBox3.Controls.Add(this._releLed2Label);
            this.groupBox3.Controls.Add(this._releLed2);
            this.groupBox3.Controls.Add(this._releLed1Label);
            this.groupBox3.Controls.Add(this._releLed1);
            this.groupBox3.Location = new System.Drawing.Point(57, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(49, 155);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "����";
            // 
            // _releLed8Label
            // 
            this._releLed8Label.AutoSize = true;
            this._releLed8Label.Location = new System.Drawing.Point(27, 135);
            this._releLed8Label.Name = "_releLed8Label";
            this._releLed8Label.Size = new System.Drawing.Size(13, 13);
            this._releLed8Label.TabIndex = 31;
            this._releLed8Label.Text = "8";
            // 
            // _releLed8
            // 
            this._releLed8.Location = new System.Drawing.Point(8, 135);
            this._releLed8.Name = "_releLed8";
            this._releLed8.Size = new System.Drawing.Size(13, 13);
            this._releLed8.State = BEMN.Forms.LedState.Off;
            this._releLed8.TabIndex = 30;
            // 
            // _releLed7Label
            // 
            this._releLed7Label.AutoSize = true;
            this._releLed7Label.Location = new System.Drawing.Point(27, 120);
            this._releLed7Label.Name = "_releLed7Label";
            this._releLed7Label.Size = new System.Drawing.Size(13, 13);
            this._releLed7Label.TabIndex = 29;
            this._releLed7Label.Text = "7";
            // 
            // _releLed7
            // 
            this._releLed7.Location = new System.Drawing.Point(8, 120);
            this._releLed7.Name = "_releLed7";
            this._releLed7.Size = new System.Drawing.Size(13, 13);
            this._releLed7.State = BEMN.Forms.LedState.Off;
            this._releLed7.TabIndex = 28;
            // 
            // _releLed6Label
            // 
            this._releLed6Label.AutoSize = true;
            this._releLed6Label.Location = new System.Drawing.Point(27, 105);
            this._releLed6Label.Name = "_releLed6Label";
            this._releLed6Label.Size = new System.Drawing.Size(13, 13);
            this._releLed6Label.TabIndex = 27;
            this._releLed6Label.Text = "6";
            // 
            // _releLed6
            // 
            this._releLed6.Location = new System.Drawing.Point(8, 105);
            this._releLed6.Name = "_releLed6";
            this._releLed6.Size = new System.Drawing.Size(13, 13);
            this._releLed6.State = BEMN.Forms.LedState.Off;
            this._releLed6.TabIndex = 26;
            // 
            // _releLed5Label
            // 
            this._releLed5Label.AutoSize = true;
            this._releLed5Label.Location = new System.Drawing.Point(27, 90);
            this._releLed5Label.Name = "_releLed5Label";
            this._releLed5Label.Size = new System.Drawing.Size(13, 13);
            this._releLed5Label.TabIndex = 25;
            this._releLed5Label.Text = "5";
            // 
            // _releLed5
            // 
            this._releLed5.Location = new System.Drawing.Point(8, 90);
            this._releLed5.Name = "_releLed5";
            this._releLed5.Size = new System.Drawing.Size(13, 13);
            this._releLed5.State = BEMN.Forms.LedState.Off;
            this._releLed5.TabIndex = 24;
            // 
            // _releLed4Label
            // 
            this._releLed4Label.AutoSize = true;
            this._releLed4Label.Location = new System.Drawing.Point(27, 75);
            this._releLed4Label.Name = "_releLed4Label";
            this._releLed4Label.Size = new System.Drawing.Size(13, 13);
            this._releLed4Label.TabIndex = 23;
            this._releLed4Label.Text = "4";
            // 
            // _releLed4
            // 
            this._releLed4.Location = new System.Drawing.Point(8, 75);
            this._releLed4.Name = "_releLed4";
            this._releLed4.Size = new System.Drawing.Size(13, 13);
            this._releLed4.State = BEMN.Forms.LedState.Off;
            this._releLed4.TabIndex = 22;
            // 
            // _releLed3Label
            // 
            this._releLed3Label.AutoSize = true;
            this._releLed3Label.Location = new System.Drawing.Point(27, 60);
            this._releLed3Label.Name = "_releLed3Label";
            this._releLed3Label.Size = new System.Drawing.Size(13, 13);
            this._releLed3Label.TabIndex = 21;
            this._releLed3Label.Text = "3";
            // 
            // _releLed3
            // 
            this._releLed3.Location = new System.Drawing.Point(8, 60);
            this._releLed3.Name = "_releLed3";
            this._releLed3.Size = new System.Drawing.Size(13, 13);
            this._releLed3.State = BEMN.Forms.LedState.Off;
            this._releLed3.TabIndex = 20;
            // 
            // _releLed2Label
            // 
            this._releLed2Label.AutoSize = true;
            this._releLed2Label.Location = new System.Drawing.Point(27, 45);
            this._releLed2Label.Name = "_releLed2Label";
            this._releLed2Label.Size = new System.Drawing.Size(13, 13);
            this._releLed2Label.TabIndex = 19;
            this._releLed2Label.Text = "2";
            // 
            // _releLed2
            // 
            this._releLed2.Location = new System.Drawing.Point(8, 45);
            this._releLed2.Name = "_releLed2";
            this._releLed2.Size = new System.Drawing.Size(13, 13);
            this._releLed2.State = BEMN.Forms.LedState.Off;
            this._releLed2.TabIndex = 18;
            // 
            // _releLed1Label
            // 
            this._releLed1Label.AutoSize = true;
            this._releLed1Label.Location = new System.Drawing.Point(27, 30);
            this._releLed1Label.Name = "_releLed1Label";
            this._releLed1Label.Size = new System.Drawing.Size(13, 13);
            this._releLed1Label.TabIndex = 17;
            this._releLed1Label.Text = "1";
            // 
            // _releLed1
            // 
            this._releLed1.Location = new System.Drawing.Point(8, 30);
            this._releLed1.Name = "_releLed1";
            this._releLed1.Size = new System.Drawing.Size(13, 13);
            this._releLed1.State = BEMN.Forms.LedState.Off;
            this._releLed1.TabIndex = 16;
            // 
            // _UcaBox
            // 
            this._UcaBox.Enabled = false;
            this._UcaBox.Location = new System.Drawing.Point(774, 197);
            this._UcaBox.Name = "_UcaBox";
            this._UcaBox.Size = new System.Drawing.Size(68, 20);
            this._UcaBox.TabIndex = 15;
            this._UcaBox.Visible = false;
            // 
            // _releLed10Label
            // 
            this._releLed10Label.AutoSize = true;
            this._releLed10Label.Location = new System.Drawing.Point(774, 96);
            this._releLed10Label.Name = "_releLed10Label";
            this._releLed10Label.Size = new System.Drawing.Size(19, 13);
            this._releLed10Label.TabIndex = 35;
            this._releLed10Label.Text = "10";
            this._releLed10Label.Visible = false;
            // 
            // _UbcBox
            // 
            this._UbcBox.Enabled = false;
            this._UbcBox.Location = new System.Drawing.Point(774, 173);
            this._UbcBox.Name = "_UbcBox";
            this._UbcBox.Size = new System.Drawing.Size(68, 20);
            this._UbcBox.TabIndex = 14;
            this._UbcBox.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this._indLed8);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._indLed7);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._indLed6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this._indLed5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._indLed4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._indLed3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this._indLed2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._indLed1);
            this.groupBox2.Location = new System.Drawing.Point(8, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(43, 155);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "���.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "8";
            // 
            // _indLed8
            // 
            this._indLed8.Location = new System.Drawing.Point(6, 135);
            this._indLed8.Name = "_indLed8";
            this._indLed8.Size = new System.Drawing.Size(13, 13);
            this._indLed8.State = BEMN.Forms.LedState.Off;
            this._indLed8.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "7";
            // 
            // _indLed7
            // 
            this._indLed7.Location = new System.Drawing.Point(6, 120);
            this._indLed7.Name = "_indLed7";
            this._indLed7.Size = new System.Drawing.Size(13, 13);
            this._indLed7.State = BEMN.Forms.LedState.Off;
            this._indLed7.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "6";
            // 
            // _indLed6
            // 
            this._indLed6.Location = new System.Drawing.Point(6, 105);
            this._indLed6.Name = "_indLed6";
            this._indLed6.Size = new System.Drawing.Size(13, 13);
            this._indLed6.State = BEMN.Forms.LedState.Off;
            this._indLed6.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "5";
            // 
            // _indLed5
            // 
            this._indLed5.Location = new System.Drawing.Point(6, 90);
            this._indLed5.Name = "_indLed5";
            this._indLed5.Size = new System.Drawing.Size(13, 13);
            this._indLed5.State = BEMN.Forms.LedState.Off;
            this._indLed5.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "4";
            // 
            // _indLed4
            // 
            this._indLed4.Location = new System.Drawing.Point(6, 75);
            this._indLed4.Name = "_indLed4";
            this._indLed4.Size = new System.Drawing.Size(13, 13);
            this._indLed4.State = BEMN.Forms.LedState.Off;
            this._indLed4.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "3";
            // 
            // _indLed3
            // 
            this._indLed3.Location = new System.Drawing.Point(6, 60);
            this._indLed3.Name = "_indLed3";
            this._indLed3.Size = new System.Drawing.Size(13, 13);
            this._indLed3.State = BEMN.Forms.LedState.Off;
            this._indLed3.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "2";
            // 
            // _indLed2
            // 
            this._indLed2.Location = new System.Drawing.Point(6, 45);
            this._indLed2.Name = "_indLed2";
            this._indLed2.Size = new System.Drawing.Size(13, 13);
            this._indLed2.State = BEMN.Forms.LedState.Off;
            this._indLed2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "1";
            // 
            // _indLed1
            // 
            this._indLed1.Location = new System.Drawing.Point(6, 30);
            this._indLed1.Name = "_indLed1";
            this._indLed1.Size = new System.Drawing.Size(13, 13);
            this._indLed1.State = BEMN.Forms.LedState.Off;
            this._indLed1.TabIndex = 0;
            // 
            // _UabBox
            // 
            this._UabBox.Enabled = false;
            this._UabBox.Location = new System.Drawing.Point(700, 269);
            this._UabBox.Name = "_UabBox";
            this._UabBox.Size = new System.Drawing.Size(68, 20);
            this._UabBox.TabIndex = 13;
            this._UabBox.Visible = false;
            // 
            // _releLed10
            // 
            this._releLed10.Location = new System.Drawing.Point(755, 96);
            this._releLed10.Name = "_releLed10";
            this._releLed10.Size = new System.Drawing.Size(13, 13);
            this._releLed10.State = BEMN.Forms.LedState.Off;
            this._releLed10.TabIndex = 34;
            this._releLed10.Visible = false;
            // 
            // _UbBox
            // 
            this._UbBox.Enabled = false;
            this._UbBox.Location = new System.Drawing.Point(700, 221);
            this._UbBox.Name = "_UbBox";
            this._UbBox.Size = new System.Drawing.Size(68, 20);
            this._UbBox.TabIndex = 12;
            this._UbBox.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._systemTimeCheck);
            this.groupBox1.Controls.Add(this._dateTimeBox);
            this.groupBox1.Controls.Add(this._readTimeCheck);
            this.groupBox1.Controls.Add(this._writeTimeBut);
            this.groupBox1.Location = new System.Drawing.Point(172, 167);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(126, 142);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���� - �����";
            // 
            // _systemTimeCheck
            // 
            this._systemTimeCheck.Appearance = System.Windows.Forms.Appearance.Button;
            this._systemTimeCheck.AutoSize = true;
            this._systemTimeCheck.Location = new System.Drawing.Point(45, 102);
            this._systemTimeCheck.Name = "_systemTimeCheck";
            this._systemTimeCheck.Size = new System.Drawing.Size(73, 23);
            this._systemTimeCheck.TabIndex = 4;
            this._systemTimeCheck.Text = "���������";
            this._systemTimeCheck.UseVisualStyleBackColor = true;
            this._systemTimeCheck.Click += new System.EventHandler(this._systemTimeCheck_CheckedChanged);
            // 
            // _dateTimeBox
            // 
            this._dateTimeBox.DateTime = new byte[] {
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0))};
            this._dateTimeBox.Location = new System.Drawing.Point(6, 24);
            this._dateTimeBox.Mask = "00-00-00 00:00:00:00";
            this._dateTimeBox.MR700Flag = false;
            this._dateTimeBox.Name = "_dateTimeBox";
            this._dateTimeBox.PiconMicroFlag = false;
            this._dateTimeBox.PromptChar = ' ';
            this._dateTimeBox.Size = new System.Drawing.Size(112, 20);
            this._dateTimeBox.TabIndex = 3;
            this._dateTimeBox.Text = "00000000000000";
            this._dateTimeBox.TZLFlag = false;
            // 
            // _readTimeCheck
            // 
            this._readTimeCheck.Appearance = System.Windows.Forms.Appearance.Button;
            this._readTimeCheck.Checked = true;
            this._readTimeCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this._readTimeCheck.Location = new System.Drawing.Point(45, 50);
            this._readTimeCheck.Name = "_readTimeCheck";
            this._readTimeCheck.Size = new System.Drawing.Size(73, 23);
            this._readTimeCheck.TabIndex = 1;
            this._readTimeCheck.Text = "������";
            this._readTimeCheck.UseVisualStyleBackColor = true;
            this._readTimeCheck.CheckedChanged += new System.EventHandler(this._readTimeCheck_CheckedChanged);
            // 
            // _writeTimeBut
            // 
            this._writeTimeBut.Location = new System.Drawing.Point(45, 76);
            this._writeTimeBut.Name = "_writeTimeBut";
            this._writeTimeBut.Size = new System.Drawing.Size(73, 23);
            this._writeTimeBut.TabIndex = 0;
            this._writeTimeBut.Text = "��������";
            this._writeTimeBut.UseVisualStyleBackColor = true;
            this._writeTimeBut.Click += new System.EventHandler(this._writeTimeBut_Click);
            // 
            // _UaBox
            // 
            this._UaBox.Enabled = false;
            this._UaBox.Location = new System.Drawing.Point(700, 197);
            this._UaBox.Name = "_UaBox";
            this._UaBox.Size = new System.Drawing.Size(68, 20);
            this._UaBox.TabIndex = 11;
            this._UaBox.Visible = false;
            // 
            // _releLed9Label
            // 
            this._releLed9Label.AutoSize = true;
            this._releLed9Label.Location = new System.Drawing.Point(774, 81);
            this._releLed9Label.Name = "_releLed9Label";
            this._releLed9Label.Size = new System.Drawing.Size(13, 13);
            this._releLed9Label.TabIndex = 33;
            this._releLed9Label.Text = "9";
            this._releLed9Label.Visible = false;
            // 
            // _UnBox
            // 
            this._UnBox.Enabled = false;
            this._UnBox.Location = new System.Drawing.Point(700, 173);
            this._UnBox.Name = "_UnBox";
            this._UnBox.Size = new System.Drawing.Size(68, 20);
            this._UnBox.TabIndex = 10;
            this._UnBox.Visible = false;
            // 
            // _releLed9
            // 
            this._releLed9.Location = new System.Drawing.Point(755, 81);
            this._releLed9.Name = "_releLed9";
            this._releLed9.Size = new System.Drawing.Size(13, 13);
            this._releLed9.State = BEMN.Forms.LedState.Off;
            this._releLed9.TabIndex = 32;
            this._releLed9.Visible = false;
            // 
            // _UcBox
            // 
            this._UcBox.Enabled = false;
            this._UcBox.Location = new System.Drawing.Point(700, 245);
            this._UcBox.Name = "_UcBox";
            this._UcBox.Size = new System.Drawing.Size(68, 20);
            this._UcBox.TabIndex = 6;
            this._UcBox.Visible = false;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(20, 193);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(91, 13);
            this.label130.TabIndex = 29;
            this.label130.Text = "�.������������";
            // 
            // _diagTab
            // 
            this._diagTab.Controls.Add(this._analogPage);
            this._diagTab.Controls.Add(this._diskretPage);
            this._diagTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._diagTab.ImageList = this._images;
            this._diagTab.Location = new System.Drawing.Point(0, 0);
            this._diagTab.Name = "_diagTab";
            this._diagTab.SelectedIndex = 0;
            this._diagTab.Size = new System.Drawing.Size(584, 417);
            this._diagTab.TabIndex = 42;
            // 
            // _diskretPage
            // 
            this._diskretPage.Controls.Add(this.groupBox10);
            this._diskretPage.Controls.Add(this.groupBox19);
            this._diskretPage.Controls.Add(this.groupBox18);
            this._diskretPage.Controls.Add(this.groupBox17);
            this._diskretPage.Controls.Add(this.groupBox16);
            this._diskretPage.Controls.Add(this.groupBox15);
            this._diskretPage.Controls.Add(this.groupBox14);
            this._diskretPage.Controls.Add(this.groupBox12);
            this._diskretPage.Controls.Add(this.groupBox9);
            this._diskretPage.ImageIndex = 1;
            this._diskretPage.Location = new System.Drawing.Point(4, 23);
            this._diskretPage.Name = "_diskretPage";
            this._diskretPage.Padding = new System.Windows.Forms.Padding(3);
            this._diskretPage.Size = new System.Drawing.Size(576, 390);
            this._diskretPage.TabIndex = 1;
            this._diskretPage.Text = "���������� ��";
            this._diskretPage.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.ledControl22);
            this.groupBox10.Controls.Add(this.ledControl21);
            this.groupBox10.Controls.Add(this.ledControl20);
            this.groupBox10.Controls.Add(this.ledControl19);
            this.groupBox10.Controls.Add(this.ledControl18);
            this.groupBox10.Controls.Add(this.ledControl17);
            this.groupBox10.Controls.Add(this.ledControl16);
            this.groupBox10.Controls.Add(this.ledControl15);
            this.groupBox10.Controls.Add(this.ledControl14);
            this.groupBox10.Controls.Add(this.ledControl13);
            this.groupBox10.Controls.Add(this.ledControl12);
            this.groupBox10.Controls.Add(this.ledControl11);
            this.groupBox10.Controls.Add(this.ledControl10);
            this.groupBox10.Controls.Add(this.ledControl9);
            this.groupBox10.Controls.Add(this.ledControl8);
            this.groupBox10.Controls.Add(this.ledControl7);
            this.groupBox10.Controls.Add(this.ledControl6);
            this.groupBox10.Controls.Add(this.ledControl1);
            this.groupBox10.Controls.Add(this.ledControl5);
            this.groupBox10.Controls.Add(this.ledControl2);
            this.groupBox10.Controls.Add(this.ledControl4);
            this.groupBox10.Controls.Add(this.ledControl3);
            this.groupBox10.Location = new System.Drawing.Point(633, 11);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(122, 99);
            this.groupBox10.TabIndex = 30;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Invisible";
            this.groupBox10.Visible = false;
            // 
            // ledControl22
            // 
            this.ledControl22.Location = new System.Drawing.Point(24, 79);
            this.ledControl22.Name = "ledControl22";
            this.ledControl22.Size = new System.Drawing.Size(13, 13);
            this.ledControl22.State = BEMN.Forms.LedState.Off;
            this.ledControl22.TabIndex = 46;
            // 
            // ledControl21
            // 
            this.ledControl21.Location = new System.Drawing.Point(81, 47);
            this.ledControl21.Name = "ledControl21";
            this.ledControl21.Size = new System.Drawing.Size(13, 13);
            this.ledControl21.State = BEMN.Forms.LedState.Off;
            this.ledControl21.TabIndex = 45;
            // 
            // ledControl20
            // 
            this.ledControl20.Location = new System.Drawing.Point(43, 47);
            this.ledControl20.Name = "ledControl20";
            this.ledControl20.Size = new System.Drawing.Size(13, 13);
            this.ledControl20.State = BEMN.Forms.LedState.Off;
            this.ledControl20.TabIndex = 44;
            // 
            // ledControl19
            // 
            this.ledControl19.Location = new System.Drawing.Point(81, 31);
            this.ledControl19.Name = "ledControl19";
            this.ledControl19.Size = new System.Drawing.Size(13, 13);
            this.ledControl19.State = BEMN.Forms.LedState.Off;
            this.ledControl19.TabIndex = 43;
            // 
            // ledControl18
            // 
            this.ledControl18.Location = new System.Drawing.Point(43, 31);
            this.ledControl18.Name = "ledControl18";
            this.ledControl18.Size = new System.Drawing.Size(13, 13);
            this.ledControl18.State = BEMN.Forms.LedState.Off;
            this.ledControl18.TabIndex = 42;
            // 
            // ledControl17
            // 
            this.ledControl17.Location = new System.Drawing.Point(24, 47);
            this.ledControl17.Name = "ledControl17";
            this.ledControl17.Size = new System.Drawing.Size(13, 13);
            this.ledControl17.State = BEMN.Forms.LedState.Off;
            this.ledControl17.TabIndex = 41;
            // 
            // ledControl16
            // 
            this.ledControl16.Location = new System.Drawing.Point(24, 31);
            this.ledControl16.Name = "ledControl16";
            this.ledControl16.Size = new System.Drawing.Size(13, 13);
            this.ledControl16.State = BEMN.Forms.LedState.Off;
            this.ledControl16.TabIndex = 40;
            // 
            // ledControl15
            // 
            this.ledControl15.Location = new System.Drawing.Point(7, 31);
            this.ledControl15.Name = "ledControl15";
            this.ledControl15.Size = new System.Drawing.Size(13, 13);
            this.ledControl15.State = BEMN.Forms.LedState.Off;
            this.ledControl15.TabIndex = 39;
            // 
            // ledControl14
            // 
            this.ledControl14.Location = new System.Drawing.Point(7, 63);
            this.ledControl14.Name = "ledControl14";
            this.ledControl14.Size = new System.Drawing.Size(13, 13);
            this.ledControl14.State = BEMN.Forms.LedState.Off;
            this.ledControl14.TabIndex = 38;
            // 
            // ledControl13
            // 
            this.ledControl13.Location = new System.Drawing.Point(7, 47);
            this.ledControl13.Name = "ledControl13";
            this.ledControl13.Size = new System.Drawing.Size(13, 13);
            this.ledControl13.State = BEMN.Forms.LedState.Off;
            this.ledControl13.TabIndex = 37;
            // 
            // ledControl12
            // 
            this.ledControl12.Location = new System.Drawing.Point(7, 79);
            this.ledControl12.Name = "ledControl12";
            this.ledControl12.Size = new System.Drawing.Size(13, 13);
            this.ledControl12.State = BEMN.Forms.LedState.Off;
            this.ledControl12.TabIndex = 36;
            // 
            // ledControl11
            // 
            this.ledControl11.Location = new System.Drawing.Point(81, 63);
            this.ledControl11.Name = "ledControl11";
            this.ledControl11.Size = new System.Drawing.Size(13, 13);
            this.ledControl11.State = BEMN.Forms.LedState.Off;
            this.ledControl11.TabIndex = 35;
            // 
            // ledControl10
            // 
            this.ledControl10.Location = new System.Drawing.Point(62, 63);
            this.ledControl10.Name = "ledControl10";
            this.ledControl10.Size = new System.Drawing.Size(13, 13);
            this.ledControl10.State = BEMN.Forms.LedState.Off;
            this.ledControl10.TabIndex = 34;
            // 
            // ledControl9
            // 
            this.ledControl9.Location = new System.Drawing.Point(43, 63);
            this.ledControl9.Name = "ledControl9";
            this.ledControl9.Size = new System.Drawing.Size(13, 13);
            this.ledControl9.State = BEMN.Forms.LedState.Off;
            this.ledControl9.TabIndex = 33;
            // 
            // ledControl8
            // 
            this.ledControl8.Location = new System.Drawing.Point(24, 63);
            this.ledControl8.Name = "ledControl8";
            this.ledControl8.Size = new System.Drawing.Size(13, 13);
            this.ledControl8.State = BEMN.Forms.LedState.Off;
            this.ledControl8.TabIndex = 32;
            // 
            // ledControl7
            // 
            this.ledControl7.Location = new System.Drawing.Point(62, 47);
            this.ledControl7.Name = "ledControl7";
            this.ledControl7.Size = new System.Drawing.Size(13, 13);
            this.ledControl7.State = BEMN.Forms.LedState.Off;
            this.ledControl7.TabIndex = 31;
            // 
            // ledControl6
            // 
            this.ledControl6.Location = new System.Drawing.Point(62, 31);
            this.ledControl6.Name = "ledControl6";
            this.ledControl6.Size = new System.Drawing.Size(13, 13);
            this.ledControl6.State = BEMN.Forms.LedState.Off;
            this.ledControl6.TabIndex = 30;
            // 
            // ledControl1
            // 
            this.ledControl1.Location = new System.Drawing.Point(7, 14);
            this.ledControl1.Name = "ledControl1";
            this.ledControl1.Size = new System.Drawing.Size(13, 13);
            this.ledControl1.State = BEMN.Forms.LedState.Off;
            this.ledControl1.TabIndex = 25;
            // 
            // ledControl5
            // 
            this.ledControl5.Location = new System.Drawing.Point(81, 14);
            this.ledControl5.Name = "ledControl5";
            this.ledControl5.Size = new System.Drawing.Size(13, 13);
            this.ledControl5.State = BEMN.Forms.LedState.Off;
            this.ledControl5.TabIndex = 29;
            // 
            // ledControl2
            // 
            this.ledControl2.Location = new System.Drawing.Point(24, 14);
            this.ledControl2.Name = "ledControl2";
            this.ledControl2.Size = new System.Drawing.Size(13, 13);
            this.ledControl2.State = BEMN.Forms.LedState.Off;
            this.ledControl2.TabIndex = 26;
            // 
            // ledControl4
            // 
            this.ledControl4.Location = new System.Drawing.Point(62, 14);
            this.ledControl4.Name = "ledControl4";
            this.ledControl4.Size = new System.Drawing.Size(13, 13);
            this.ledControl4.State = BEMN.Forms.LedState.Off;
            this.ledControl4.TabIndex = 28;
            // 
            // ledControl3
            // 
            this.ledControl3.Location = new System.Drawing.Point(43, 14);
            this.ledControl3.Name = "ledControl3";
            this.ledControl3.Size = new System.Drawing.Size(13, 13);
            this.ledControl3.State = BEMN.Forms.LedState.Off;
            this.ledControl3.TabIndex = 27;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label129);
            this.groupBox19.Controls.Add(this._faultSignalLed16);
            this.groupBox19.Controls.Add(this.label130);
            this.groupBox19.Controls.Add(this._faultSignalLed15);
            this.groupBox19.Controls.Add(this.label131);
            this.groupBox19.Controls.Add(this._faultSignalLed14);
            this.groupBox19.Controls.Add(this.label132);
            this.groupBox19.Controls.Add(this._faultSignalLed13);
            this.groupBox19.Controls.Add(this.label133);
            this.groupBox19.Controls.Add(this._faultSignalLed12);
            this.groupBox19.Controls.Add(this.label134);
            this.groupBox19.Controls.Add(this._faultSignalLed11);
            this.groupBox19.Controls.Add(this.label135);
            this.groupBox19.Controls.Add(this._faultSignalLed10);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this._faultSignalLed9);
            this.groupBox19.Location = new System.Drawing.Point(436, 7);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(132, 267);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "������� ������������� 2";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(21, 218);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(94, 13);
            this.label129.TabIndex = 31;
            this.label129.Text = "�. ������ ������";
            // 
            // _faultSignalLed16
            // 
            this._faultSignalLed16.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed16.Name = "_faultSignalLed16";
            this._faultSignalLed16.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed16.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed16.TabIndex = 30;
            // 
            // _faultSignalLed15
            // 
            this._faultSignalLed15.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed15.Name = "_faultSignalLed15";
            this._faultSignalLed15.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed15.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed15.TabIndex = 28;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(21, 166);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(50, 13);
            this.label131.TabIndex = 27;
            this.label131.Text = "�. �����";
            // 
            // _faultSignalLed14
            // 
            this._faultSignalLed14.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed14.Name = "_faultSignalLed14";
            this._faultSignalLed14.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed14.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed14.TabIndex = 26;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(21, 140);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(39, 13);
            this.label132.TabIndex = 25;
            this.label132.Text = "�. ��";
            // 
            // _faultSignalLed13
            // 
            this._faultSignalLed13.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed13.Name = "_faultSignalLed13";
            this._faultSignalLed13.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed13.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed13.TabIndex = 24;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(21, 114);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(39, 13);
            this.label133.TabIndex = 23;
            this.label133.Text = "�. ��";
            // 
            // _faultSignalLed12
            // 
            this._faultSignalLed12.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed12.Name = "_faultSignalLed12";
            this._faultSignalLed12.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed12.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed12.TabIndex = 22;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(21, 63);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(84, 13);
            this.label134.TabIndex = 21;
            this.label134.Text = "�. �����. ���";
            // 
            // _faultSignalLed11
            // 
            this._faultSignalLed11.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed11.Name = "_faultSignalLed11";
            this._faultSignalLed11.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed11.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed11.TabIndex = 20;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(21, 89);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(99, 13);
            this.label135.TabIndex = 19;
            this.label135.Text = "�. ������� ����";
            // 
            // _faultSignalLed10
            // 
            this._faultSignalLed10.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed10.Name = "_faultSignalLed10";
            this._faultSignalLed10.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed10.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed10.TabIndex = 18;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(21, 37);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(90, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "������ �������";
            // 
            // _faultSignalLed9
            // 
            this._faultSignalLed9.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed9.Name = "_faultSignalLed9";
            this._faultSignalLed9.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed9.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed9.TabIndex = 16;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label121);
            this.groupBox18.Controls.Add(this._faultSignalLed8);
            this.groupBox18.Controls.Add(this.label122);
            this.groupBox18.Controls.Add(this._faultSignalLed7);
            this.groupBox18.Controls.Add(this.label123);
            this.groupBox18.Controls.Add(this._faultSignalLed6);
            this.groupBox18.Controls.Add(this.label124);
            this.groupBox18.Controls.Add(this._faultSignalLed5);
            this.groupBox18.Controls.Add(this.label126);
            this.groupBox18.Controls.Add(this._faultSignalLed3);
            this.groupBox18.Controls.Add(this.label127);
            this.groupBox18.Controls.Add(this._faultSignalLed2);
            this.groupBox18.Controls.Add(this.label128);
            this.groupBox18.Controls.Add(this._faultSignalLed1);
            this.groupBox18.Location = new System.Drawing.Point(315, 7);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(115, 271);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "������� ������������� 1";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(21, 192);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(52, 13);
            this.label121.TabIndex = 31;
            this.label121.Text = "�9 - �16";
            // 
            // _faultSignalLed8
            // 
            this._faultSignalLed8.Location = new System.Drawing.Point(6, 192);
            this._faultSignalLed8.Name = "_faultSignalLed8";
            this._faultSignalLed8.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed8.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed8.TabIndex = 30;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(21, 166);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(46, 13);
            this.label122.TabIndex = 29;
            this.label122.Text = "�1 - �8";
            // 
            // _faultSignalLed7
            // 
            this._faultSignalLed7.Location = new System.Drawing.Point(6, 166);
            this._faultSignalLed7.Name = "_faultSignalLed7";
            this._faultSignalLed7.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed7.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed7.TabIndex = 28;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(21, 139);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(32, 13);
            this.label123.TabIndex = 27;
            this.label123.Text = "����";
            // 
            // _faultSignalLed6
            // 
            this._faultSignalLed6.Location = new System.Drawing.Point(6, 140);
            this._faultSignalLed6.Name = "_faultSignalLed6";
            this._faultSignalLed6.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed6.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed6.TabIndex = 26;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(21, 113);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(56, 13);
            this.label124.TabIndex = 25;
            this.label124.Text = "������� I";
            // 
            // _faultSignalLed5
            // 
            this._faultSignalLed5.Location = new System.Drawing.Point(6, 114);
            this._faultSignalLed5.Name = "_faultSignalLed5";
            this._faultSignalLed5.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed5.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed5.TabIndex = 24;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(21, 63);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(70, 13);
            this.label126.TabIndex = 21;
            this.label126.Text = "�.  ���� I2c";
            // 
            // _faultSignalLed3
            // 
            this._faultSignalLed3.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed3.Name = "_faultSignalLed3";
            this._faultSignalLed3.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed3.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed3.TabIndex = 20;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(21, 89);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(88, 13);
            this.label127.TabIndex = 19;
            this.label127.Text = "�. �����������";
            // 
            // _faultSignalLed2
            // 
            this._faultSignalLed2.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed2.Name = "_faultSignalLed2";
            this._faultSignalLed2.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed2.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed2.TabIndex = 18;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(21, 37);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(73, 13);
            this.label128.TabIndex = 17;
            this.label128.Text = "������ ���";
            // 
            // _faultSignalLed1
            // 
            this._faultSignalLed1.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed1.Name = "_faultSignalLed1";
            this._faultSignalLed1.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed1.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed1.TabIndex = 16;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label115);
            this.groupBox17.Controls.Add(this._faultStateLed6);
            this.groupBox17.Controls.Add(this.label116);
            this.groupBox17.Controls.Add(this._faultStateLed5);
            this.groupBox17.Controls.Add(this.label118);
            this.groupBox17.Controls.Add(this._faultStateLed3);
            this.groupBox17.Controls.Add(this.label119);
            this.groupBox17.Controls.Add(this._faultStateLed2);
            this.groupBox17.Controls.Add(this.label120);
            this.groupBox17.Controls.Add(this._faultStateLed1);
            this.groupBox17.Location = new System.Drawing.Point(191, 7);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(118, 271);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "��������� �������������";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(21, 138);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(70, 13);
            this.label115.TabIndex = 27;
            this.label115.Text = "�. ���. ����";
            // 
            // _faultStateLed6
            // 
            this._faultStateLed6.Location = new System.Drawing.Point(6, 139);
            this._faultStateLed6.Name = "_faultStateLed6";
            this._faultStateLed6.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed6.State = BEMN.Forms.LedState.Off;
            this._faultStateLed6.TabIndex = 26;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(21, 112);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(89, 13);
            this.label116.TabIndex = 25;
            this.label116.Text = "� .�����������";
            // 
            // _faultStateLed5
            // 
            this._faultStateLed5.Location = new System.Drawing.Point(6, 113);
            this._faultStateLed5.Name = "_faultStateLed5";
            this._faultStateLed5.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed5.State = BEMN.Forms.LedState.Off;
            this._faultStateLed5.TabIndex = 24;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(21, 63);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(56, 13);
            this.label118.TabIndex = 21;
            this.label118.Text = "�. ������";
            // 
            // _faultStateLed3
            // 
            this._faultStateLed3.Location = new System.Drawing.Point(6, 89);
            this._faultStateLed3.Name = "_faultStateLed3";
            this._faultStateLed3.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed3.State = BEMN.Forms.LedState.Off;
            this._faultStateLed3.TabIndex = 20;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(21, 89);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(58, 13);
            this.label119.TabIndex = 19;
            this.label119.Text = "�. ������";
            // 
            // _faultStateLed2
            // 
            this._faultStateLed2.Location = new System.Drawing.Point(6, 63);
            this._faultStateLed2.Name = "_faultStateLed2";
            this._faultStateLed2.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed2.State = BEMN.Forms.LedState.Off;
            this._faultStateLed2.TabIndex = 18;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(21, 37);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(78, 13);
            this.label120.TabIndex = 17;
            this.label120.Text = "�. ����������";
            // 
            // _faultStateLed1
            // 
            this._faultStateLed1.Location = new System.Drawing.Point(6, 37);
            this._faultStateLed1.Name = "_faultStateLed1";
            this._faultStateLed1.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed1.State = BEMN.Forms.LedState.Off;
            this._faultStateLed1.TabIndex = 16;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label105);
            this.groupBox16.Controls.Add(this._extDefenseLed8);
            this.groupBox16.Controls.Add(this.label106);
            this.groupBox16.Controls.Add(this._extDefenseLed7);
            this.groupBox16.Controls.Add(this.label107);
            this.groupBox16.Controls.Add(this._extDefenseLed6);
            this.groupBox16.Controls.Add(this.label108);
            this.groupBox16.Controls.Add(this._extDefenseLed5);
            this.groupBox16.Controls.Add(this.label109);
            this.groupBox16.Controls.Add(this._extDefenseLed4);
            this.groupBox16.Controls.Add(this.label110);
            this.groupBox16.Controls.Add(this._extDefenseLed3);
            this.groupBox16.Controls.Add(this.label111);
            this.groupBox16.Controls.Add(this._extDefenseLed2);
            this.groupBox16.Controls.Add(this.label112);
            this.groupBox16.Controls.Add(this._extDefenseLed1);
            this.groupBox16.Location = new System.Drawing.Point(9, 180);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(180, 98);
            this.groupBox16.TabIndex = 9;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "������� ������";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(125, 77);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(36, 13);
            this.label105.TabIndex = 47;
            this.label105.Text = "�� - 8";
            // 
            // _extDefenseLed8
            // 
            this._extDefenseLed8.Location = new System.Drawing.Point(106, 77);
            this._extDefenseLed8.Name = "_extDefenseLed8";
            this._extDefenseLed8.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed8.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed8.TabIndex = 46;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(125, 59);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(36, 13);
            this.label106.TabIndex = 45;
            this.label106.Text = "�� - 7";
            // 
            // _extDefenseLed7
            // 
            this._extDefenseLed7.Location = new System.Drawing.Point(106, 59);
            this._extDefenseLed7.Name = "_extDefenseLed7";
            this._extDefenseLed7.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed7.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed7.TabIndex = 44;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(124, 41);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(36, 13);
            this.label107.TabIndex = 43;
            this.label107.Text = "�� - 6";
            // 
            // _extDefenseLed6
            // 
            this._extDefenseLed6.Location = new System.Drawing.Point(106, 41);
            this._extDefenseLed6.Name = "_extDefenseLed6";
            this._extDefenseLed6.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed6.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed6.TabIndex = 42;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(125, 23);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(36, 13);
            this.label108.TabIndex = 41;
            this.label108.Text = "�� - 5";
            // 
            // _extDefenseLed5
            // 
            this._extDefenseLed5.Location = new System.Drawing.Point(106, 23);
            this._extDefenseLed5.Name = "_extDefenseLed5";
            this._extDefenseLed5.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed5.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed5.TabIndex = 40;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(28, 77);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(36, 13);
            this.label109.TabIndex = 39;
            this.label109.Text = "�� - 4";
            // 
            // _extDefenseLed4
            // 
            this._extDefenseLed4.Location = new System.Drawing.Point(8, 77);
            this._extDefenseLed4.Name = "_extDefenseLed4";
            this._extDefenseLed4.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed4.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed4.TabIndex = 38;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(28, 59);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(36, 13);
            this.label110.TabIndex = 37;
            this.label110.Text = "�� - 3";
            // 
            // _extDefenseLed3
            // 
            this._extDefenseLed3.Location = new System.Drawing.Point(8, 59);
            this._extDefenseLed3.Name = "_extDefenseLed3";
            this._extDefenseLed3.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed3.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed3.TabIndex = 36;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(28, 41);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 35;
            this.label111.Text = "�� - 2";
            // 
            // _extDefenseLed2
            // 
            this._extDefenseLed2.Location = new System.Drawing.Point(8, 41);
            this._extDefenseLed2.Name = "_extDefenseLed2";
            this._extDefenseLed2.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed2.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed2.TabIndex = 34;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(28, 23);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 33;
            this.label112.Text = "�� - 1";
            // 
            // _extDefenseLed1
            // 
            this._extDefenseLed1.Location = new System.Drawing.Point(8, 23);
            this._extDefenseLed1.Name = "_extDefenseLed1";
            this._extDefenseLed1.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed1.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed1.TabIndex = 32;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label);
            this.groupBox15.Controls.Add(this.label10);
            this.groupBox15.Controls.Add(this._chapv);
            this.groupBox15.Controls.Add(this._achr);
            this.groupBox15.Controls.Add(this.label97);
            this.groupBox15.Controls.Add(this._autoLed8);
            this.groupBox15.Controls.Add(this.label98);
            this.groupBox15.Controls.Add(this._autoLed7);
            this.groupBox15.Controls.Add(this.label99);
            this.groupBox15.Controls.Add(this._autoLed6);
            this.groupBox15.Controls.Add(this.label100);
            this.groupBox15.Controls.Add(this._autoLed5);
            this.groupBox15.Controls.Add(this.label101);
            this.groupBox15.Controls.Add(this._autoLed4);
            this.groupBox15.Controls.Add(this.label102);
            this.groupBox15.Controls.Add(this._autoLed3);
            this.groupBox15.Controls.Add(this.label103);
            this.groupBox15.Controls.Add(this._autoLed2);
            this.groupBox15.Controls.Add(this.label104);
            this.groupBox15.Controls.Add(this._autoLed1);
            this.groupBox15.Location = new System.Drawing.Point(5, 7);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(180, 167);
            this.groupBox15.TabIndex = 8;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "����������";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(35, 150);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(68, 13);
            this.label.TabIndex = 35;
            this.label.Text = "����� ���.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "��� ����.";
            // 
            // _chapv
            // 
            this._chapv.Location = new System.Drawing.Point(8, 150);
            this._chapv.Name = "_chapv";
            this._chapv.Size = new System.Drawing.Size(13, 13);
            this._chapv.State = BEMN.Forms.LedState.Off;
            this._chapv.TabIndex = 33;
            // 
            // _achr
            // 
            this._achr.Location = new System.Drawing.Point(8, 135);
            this._achr.Name = "_achr";
            this._achr.Size = new System.Drawing.Size(13, 13);
            this._achr.State = BEMN.Forms.LedState.Off;
            this._achr.TabIndex = 32;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(35, 120);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(63, 13);
            this.label97.TabIndex = 31;
            this.label97.Text = "���������";
            // 
            // _autoLed8
            // 
            this._autoLed8.Location = new System.Drawing.Point(8, 120);
            this._autoLed8.Name = "_autoLed8";
            this._autoLed8.Size = new System.Drawing.Size(13, 13);
            this._autoLed8.State = BEMN.Forms.LedState.Off;
            this._autoLed8.TabIndex = 30;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(35, 105);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(134, 13);
            this.label98.TabIndex = 29;
            this.label98.Text = "��������� �����������";
            // 
            // _autoLed7
            // 
            this._autoLed7.Location = new System.Drawing.Point(8, 105);
            this._autoLed7.Name = "_autoLed7";
            this._autoLed7.Size = new System.Drawing.Size(13, 13);
            this._autoLed7.State = BEMN.Forms.LedState.Off;
            this._autoLed7.TabIndex = 28;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(35, 90);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(76, 13);
            this.label99.TabIndex = 27;
            this.label99.Text = "������ ����";
            // 
            // _autoLed6
            // 
            this._autoLed6.Location = new System.Drawing.Point(8, 90);
            this._autoLed6.Name = "_autoLed6";
            this._autoLed6.Size = new System.Drawing.Size(13, 13);
            this._autoLed6.State = BEMN.Forms.LedState.Off;
            this._autoLed6.TabIndex = 26;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(35, 75);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(70, 13);
            this.label100.TabIndex = 25;
            this.label100.Text = "������ ���";
            // 
            // _autoLed5
            // 
            this._autoLed5.Location = new System.Drawing.Point(8, 75);
            this._autoLed5.Name = "_autoLed5";
            this._autoLed5.Size = new System.Drawing.Size(13, 13);
            this._autoLed5.State = BEMN.Forms.LedState.Off;
            this._autoLed5.TabIndex = 24;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(35, 60);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(44, 13);
            this.label101.TabIndex = 23;
            this.label101.Text = "������";
            // 
            // _autoLed4
            // 
            this._autoLed4.Location = new System.Drawing.Point(8, 60);
            this._autoLed4.Name = "_autoLed4";
            this._autoLed4.Size = new System.Drawing.Size(13, 13);
            this._autoLed4.State = BEMN.Forms.LedState.Off;
            this._autoLed4.TabIndex = 22;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(35, 45);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(91, 13);
            this.label102.TabIndex = 21;
            this.label102.Text = "��� ����������";
            // 
            // _autoLed3
            // 
            this._autoLed3.Location = new System.Drawing.Point(8, 45);
            this._autoLed3.Name = "_autoLed3";
            this._autoLed3.Size = new System.Drawing.Size(13, 13);
            this._autoLed3.State = BEMN.Forms.LedState.Off;
            this._autoLed3.TabIndex = 20;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(35, 30);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(123, 13);
            this.label103.TabIndex = 19;
            this.label103.Text = "��� ��������� ������";
            // 
            // _autoLed2
            // 
            this._autoLed2.Location = new System.Drawing.Point(8, 30);
            this._autoLed2.Name = "_autoLed2";
            this._autoLed2.Size = new System.Drawing.Size(13, 13);
            this._autoLed2.State = BEMN.Forms.LedState.Off;
            this._autoLed2.TabIndex = 18;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(35, 15);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(118, 13);
            this.label104.TabIndex = 17;
            this.label104.Text = "��� �������� ������";
            // 
            // _autoLed1
            // 
            this._autoLed1.Location = new System.Drawing.Point(8, 15);
            this._autoLed1.Name = "_autoLed1";
            this._autoLed1.Size = new System.Drawing.Size(13, 13);
            this._autoLed1.State = BEMN.Forms.LedState.Off;
            this._autoLed1.TabIndex = 16;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._I21Led8);
            this.groupBox14.Controls.Add(this._I21Led7);
            this.groupBox14.Controls.Add(this._IgLed6);
            this.groupBox14.Controls.Add(this._IgLed5);
            this.groupBox14.Controls.Add(this.label91);
            this.groupBox14.Controls.Add(this.label92);
            this.groupBox14.Controls.Add(this.label93);
            this.groupBox14.Controls.Add(this.label94);
            this.groupBox14.Controls.Add(this.label95);
            this.groupBox14.Controls.Add(this._InmaxLed4);
            this.groupBox14.Controls.Add(this._InmaxLed3);
            this.groupBox14.Controls.Add(this._InmaxLed2);
            this.groupBox14.Controls.Add(this.label96);
            this.groupBox14.Controls.Add(this._InmaxLed1);
            this.groupBox14.Location = new System.Drawing.Point(195, 284);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(92, 99);
            this.groupBox14.TabIndex = 6;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "In Ig I2/I1 ";
            // 
            // _I21Led8
            // 
            this._I21Led8.Location = new System.Drawing.Point(35, 79);
            this._I21Led8.Name = "_I21Led8";
            this._I21Led8.Size = new System.Drawing.Size(13, 13);
            this._I21Led8.State = BEMN.Forms.LedState.Off;
            this._I21Led8.TabIndex = 24;
            // 
            // _I21Led7
            // 
            this._I21Led7.Location = new System.Drawing.Point(8, 79);
            this._I21Led7.Name = "_I21Led7";
            this._I21Led7.Size = new System.Drawing.Size(13, 13);
            this._I21Led7.State = BEMN.Forms.LedState.Off;
            this._I21Led7.TabIndex = 23;
            // 
            // _IgLed6
            // 
            this._IgLed6.Location = new System.Drawing.Point(35, 63);
            this._IgLed6.Name = "_IgLed6";
            this._IgLed6.Size = new System.Drawing.Size(13, 13);
            this._IgLed6.State = BEMN.Forms.LedState.Off;
            this._IgLed6.TabIndex = 22;
            // 
            // _IgLed5
            // 
            this._IgLed5.Location = new System.Drawing.Point(8, 63);
            this._IgLed5.Name = "_IgLed5";
            this._IgLed5.Size = new System.Drawing.Size(13, 13);
            this._IgLed5.State = BEMN.Forms.LedState.Off;
            this._IgLed5.TabIndex = 21;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(50, 79);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(30, 13);
            this.label91.TabIndex = 20;
            this.label91.Text = "I2/I1";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(50, 63);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(16, 13);
            this.label92.TabIndex = 19;
            this.label92.Text = "Ig";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(50, 47);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(28, 13);
            this.label93.TabIndex = 18;
            this.label93.Text = "In>>";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(50, 31);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 17;
            this.label94.Text = "In>";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(26, 14);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(35, 13);
            this.label95.TabIndex = 16;
            this.label95.Text = "����.";
            // 
            // _InmaxLed4
            // 
            this._InmaxLed4.Location = new System.Drawing.Point(35, 47);
            this._InmaxLed4.Name = "_InmaxLed4";
            this._InmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed4.State = BEMN.Forms.LedState.Off;
            this._InmaxLed4.TabIndex = 6;
            // 
            // _InmaxLed3
            // 
            this._InmaxLed3.Location = new System.Drawing.Point(8, 47);
            this._InmaxLed3.Name = "_InmaxLed3";
            this._InmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed3.State = BEMN.Forms.LedState.Off;
            this._InmaxLed3.TabIndex = 4;
            // 
            // _InmaxLed2
            // 
            this._InmaxLed2.Location = new System.Drawing.Point(35, 31);
            this._InmaxLed2.Name = "_InmaxLed2";
            this._InmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed2.State = BEMN.Forms.LedState.Off;
            this._InmaxLed2.TabIndex = 2;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(5, 14);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(23, 13);
            this.label96.TabIndex = 1;
            this.label96.Text = "��";
            // 
            // _InmaxLed1
            // 
            this._InmaxLed1.Location = new System.Drawing.Point(8, 31);
            this._InmaxLed1.Name = "_InmaxLed1";
            this._InmaxLed1.Size = new System.Drawing.Size(13, 13);
            this._InmaxLed1.State = BEMN.Forms.LedState.Off;
            this._InmaxLed1.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._I0maxLed8);
            this.groupBox12.Controls.Add(this._I0maxLed7);
            this.groupBox12.Controls.Add(this._I0maxLed6);
            this.groupBox12.Controls.Add(this._I0maxLed5);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.label80);
            this.groupBox12.Controls.Add(this.label81);
            this.groupBox12.Controls.Add(this.label82);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this._I0maxLed4);
            this.groupBox12.Controls.Add(this._I0maxLed3);
            this.groupBox12.Controls.Add(this._I0maxLed2);
            this.groupBox12.Controls.Add(this.label84);
            this.groupBox12.Controls.Add(this._I0maxLed1);
            this.groupBox12.Location = new System.Drawing.Point(100, 284);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(89, 99);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "I0 � I2 max";
            // 
            // _I0maxLed8
            // 
            this._I0maxLed8.Location = new System.Drawing.Point(35, 79);
            this._I0maxLed8.Name = "_I0maxLed8";
            this._I0maxLed8.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed8.State = BEMN.Forms.LedState.Off;
            this._I0maxLed8.TabIndex = 24;
            // 
            // _I0maxLed7
            // 
            this._I0maxLed7.Location = new System.Drawing.Point(8, 79);
            this._I0maxLed7.Name = "_I0maxLed7";
            this._I0maxLed7.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed7.State = BEMN.Forms.LedState.Off;
            this._I0maxLed7.TabIndex = 23;
            // 
            // _I0maxLed6
            // 
            this._I0maxLed6.Location = new System.Drawing.Point(35, 63);
            this._I0maxLed6.Name = "_I0maxLed6";
            this._I0maxLed6.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed6.State = BEMN.Forms.LedState.Off;
            this._I0maxLed6.TabIndex = 22;
            // 
            // _I0maxLed5
            // 
            this._I0maxLed5.Location = new System.Drawing.Point(8, 63);
            this._I0maxLed5.Name = "_I0maxLed5";
            this._I0maxLed5.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed5.State = BEMN.Forms.LedState.Off;
            this._I0maxLed5.TabIndex = 21;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(50, 79);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(28, 13);
            this.label79.TabIndex = 20;
            this.label79.Text = "I0>>";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(50, 63);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(22, 13);
            this.label80.TabIndex = 19;
            this.label80.Text = "I0>";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(50, 47);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 18;
            this.label81.Text = "I2>>";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(50, 31);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(22, 13);
            this.label82.TabIndex = 17;
            this.label82.Text = "I2>";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(26, 14);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 13);
            this.label83.TabIndex = 16;
            this.label83.Text = "����.";
            // 
            // _I0maxLed4
            // 
            this._I0maxLed4.Location = new System.Drawing.Point(35, 47);
            this._I0maxLed4.Name = "_I0maxLed4";
            this._I0maxLed4.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed4.State = BEMN.Forms.LedState.Off;
            this._I0maxLed4.TabIndex = 6;
            // 
            // _I0maxLed3
            // 
            this._I0maxLed3.Location = new System.Drawing.Point(8, 47);
            this._I0maxLed3.Name = "_I0maxLed3";
            this._I0maxLed3.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed3.State = BEMN.Forms.LedState.Off;
            this._I0maxLed3.TabIndex = 4;
            // 
            // _I0maxLed2
            // 
            this._I0maxLed2.Location = new System.Drawing.Point(35, 31);
            this._I0maxLed2.Name = "_I0maxLed2";
            this._I0maxLed2.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed2.State = BEMN.Forms.LedState.Off;
            this._I0maxLed2.TabIndex = 2;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(5, 14);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(23, 13);
            this.label84.TabIndex = 1;
            this.label84.Text = "��";
            // 
            // _I0maxLed1
            // 
            this._I0maxLed1.Location = new System.Drawing.Point(8, 31);
            this._I0maxLed1.Name = "_I0maxLed1";
            this._I0maxLed1.Size = new System.Drawing.Size(13, 13);
            this._I0maxLed1.State = BEMN.Forms.LedState.Off;
            this._I0maxLed1.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ImaxLed8);
            this.groupBox9.Controls.Add(this._ImaxLed7);
            this.groupBox9.Controls.Add(this._ImaxLed6);
            this.groupBox9.Controls.Add(this._ImaxLed5);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.label72);
            this.groupBox9.Controls.Add(this.label71);
            this.groupBox9.Controls.Add(this.label70);
            this.groupBox9.Controls.Add(this.label69);
            this.groupBox9.Controls.Add(this._ImaxLed4);
            this.groupBox9.Controls.Add(this._ImaxLed3);
            this.groupBox9.Controls.Add(this._ImaxLed2);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this._ImaxLed1);
            this.groupBox9.Location = new System.Drawing.Point(8, 284);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(89, 99);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "I max";
            // 
            // _ImaxLed8
            // 
            this._ImaxLed8.Location = new System.Drawing.Point(35, 79);
            this._ImaxLed8.Name = "_ImaxLed8";
            this._ImaxLed8.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed8.State = BEMN.Forms.LedState.Off;
            this._ImaxLed8.TabIndex = 24;
            // 
            // _ImaxLed7
            // 
            this._ImaxLed7.Location = new System.Drawing.Point(8, 79);
            this._ImaxLed7.Name = "_ImaxLed7";
            this._ImaxLed7.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed7.State = BEMN.Forms.LedState.Off;
            this._ImaxLed7.TabIndex = 23;
            // 
            // _ImaxLed6
            // 
            this._ImaxLed6.Location = new System.Drawing.Point(35, 63);
            this._ImaxLed6.Name = "_ImaxLed6";
            this._ImaxLed6.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed6.State = BEMN.Forms.LedState.Off;
            this._ImaxLed6.TabIndex = 22;
            // 
            // _ImaxLed5
            // 
            this._ImaxLed5.Location = new System.Drawing.Point(8, 63);
            this._ImaxLed5.Name = "_ImaxLed5";
            this._ImaxLed5.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed5.State = BEMN.Forms.LedState.Off;
            this._ImaxLed5.TabIndex = 21;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(50, 79);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(34, 13);
            this.label73.TabIndex = 20;
            this.label73.Text = "I>>>>";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(50, 63);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(28, 13);
            this.label72.TabIndex = 19;
            this.label72.Text = "I>>>";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(50, 47);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(22, 13);
            this.label71.TabIndex = 18;
            this.label71.Text = "I>>";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(50, 31);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(16, 13);
            this.label70.TabIndex = 17;
            this.label70.Text = "I>";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(26, 14);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 16;
            this.label69.Text = "����.";
            // 
            // _ImaxLed4
            // 
            this._ImaxLed4.Location = new System.Drawing.Point(35, 47);
            this._ImaxLed4.Name = "_ImaxLed4";
            this._ImaxLed4.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed4.State = BEMN.Forms.LedState.Off;
            this._ImaxLed4.TabIndex = 6;
            // 
            // _ImaxLed3
            // 
            this._ImaxLed3.Location = new System.Drawing.Point(8, 47);
            this._ImaxLed3.Name = "_ImaxLed3";
            this._ImaxLed3.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed3.State = BEMN.Forms.LedState.Off;
            this._ImaxLed3.TabIndex = 4;
            // 
            // _ImaxLed2
            // 
            this._ImaxLed2.Location = new System.Drawing.Point(35, 31);
            this._ImaxLed2.Name = "_ImaxLed2";
            this._ImaxLed2.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed2.State = BEMN.Forms.LedState.Off;
            this._ImaxLed2.TabIndex = 2;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(5, 14);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(23, 13);
            this.label68.TabIndex = 1;
            this.label68.Text = "��";
            // 
            // _ImaxLed1
            // 
            this._ImaxLed1.Location = new System.Drawing.Point(8, 31);
            this._ImaxLed1.Name = "_ImaxLed1";
            this._ImaxLed1.Size = new System.Drawing.Size(13, 13);
            this._ImaxLed1.State = BEMN.Forms.LedState.Off;
            this._ImaxLed1.TabIndex = 0;
            // 
            // _images
            // 
            this._images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.TransparentColor = System.Drawing.Color.Transparent;
            this._images.Images.SetKeyName(0, "analog.bmp");
            this._images.Images.SetKeyName(1, "diskret.bmp");
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(27, 150);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(19, 13);
            this.label139.TabIndex = 47;
            this.label139.Text = "10";
            // 
            // label14_releLed9Label0
            // 
            this.label14_releLed9Label0.AutoSize = true;
            this.label14_releLed9Label0.Location = new System.Drawing.Point(27, 165);
            this.label14_releLed9Label0.Name = "label14_releLed9Label0";
            this.label14_releLed9Label0.Size = new System.Drawing.Size(19, 13);
            this.label14_releLed9Label0.TabIndex = 48;
            this.label14_releLed9Label0.Text = "11";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 44;
            this.label13.Text = "4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 43;
            this.label15.Text = "2";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(27, 195);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(19, 13);
            this.label142.TabIndex = 49;
            this.label142.Text = "13";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 46;
            this.label9.Text = "8";
            // 
            // VMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 417);
            this.Controls.Add(this._diagTab);
            this.Controls.Add(this.label139);
            this.Controls.Add(this.label14_releLed9Label0);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.label9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "VMeasuringForm";
            this.Text = "TZL_MeasuringForm";
            this.Activated += new System.EventHandler(this.MeasuringForm_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._analogPage.ResumeLayout(false);
            this._analogPage.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._diagTab.ResumeLayout(false);
            this._diskretPage.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _selectConstraintGroupBut;
        private System.Windows.Forms.Button _resetIndicatBut;
        private System.Windows.Forms.Label label137;
        private BEMN.Forms.LedControl _manageLed8;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _manageLed5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _manageLed4;
        private System.Windows.Forms.Label label62;
        private BEMN.Forms.LedControl _manageLed3;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _manageLed7;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _manageLed6;
        private System.Windows.Forms.Button _resetJA_But;
        private System.Windows.Forms.Button _resetJS_But;
        private System.Windows.Forms.Button _resetFaultBut;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _manageLed2;
        private BEMN.Forms.LedControl _manageLed1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _addIndLed4;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _addIndLed3;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _addIndLed2;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _addIndLed1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox _CosBox;
        private System.Windows.Forms.TextBox _QBox;
        private System.Windows.Forms.TextBox _PBox;
        private System.Windows.Forms.TextBox _I1Box;
        private System.Windows.Forms.TextBox _I0Box;
        private System.Windows.Forms.TextBox _IcBox;
        private System.Windows.Forms.TextBox _IbBox;
        private System.Windows.Forms.TextBox _IaBox;
        private System.Windows.Forms.TextBox _InBox;
        private System.Windows.Forms.TextBox _U2Box;
        private System.Windows.Forms.TextBox _U1Box;
        private System.Windows.Forms.TextBox _U0Box;
        private System.Windows.Forms.TextBox _UcaBox;
        private System.Windows.Forms.TextBox _UbcBox;
        private System.Windows.Forms.TextBox _UabBox;
        private System.Windows.Forms.TextBox _UbBox;
        private System.Windows.Forms.TextBox _UaBox;
        private System.Windows.Forms.TextBox _UnBox;
        private System.Windows.Forms.TextBox _F_Box;
        private System.Windows.Forms.TextBox _IgBox;
        private System.Windows.Forms.TextBox _I2Box;
        private System.Windows.Forms.TextBox _UcBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _inL_led8;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _inL_led7;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _inL_led6;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _inL_led5;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _inL_led4;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _inL_led3;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _inL_led2;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _inL_led1;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _inD_led16;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _inD_led15;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _inD_led14;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _inD_led13;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _inD_led12;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _inD_led11;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _inD_led10;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _inD_led9;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _inD_led8;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _inD_led7;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _inD_led6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _inD_led5;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _inD_led4;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _inD_led3;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _inD_led2;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _inD_led1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _outLed8;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _outLed7;
        private System.Windows.Forms.Label label19;
        private BEMN.Forms.LedControl _outLed6;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _outLed5;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _outLed4;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _outLed3;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _outLed2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _outLed1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _releLed13Label;
        private BEMN.Forms.LedControl _releLed13;
        private System.Windows.Forms.Label _releLed12Label;
        private BEMN.Forms.LedControl _releLed12;
        private System.Windows.Forms.Label _releLed11Label;
        private BEMN.Forms.LedControl _releLed11;
        private System.Windows.Forms.Label _releLed10Label;
        private BEMN.Forms.LedControl _releLed10;
        private System.Windows.Forms.Label _releLed9Label;
        private BEMN.Forms.LedControl _releLed9;
        private System.Windows.Forms.Label _releLed8Label;
        private BEMN.Forms.LedControl _releLed8;
        private System.Windows.Forms.Label _releLed7Label;
        private BEMN.Forms.LedControl _releLed7;
        private System.Windows.Forms.Label _releLed6Label;
        private BEMN.Forms.LedControl _releLed6;
        private System.Windows.Forms.Label _releLed5Label;
        private BEMN.Forms.LedControl _releLed5;
        private System.Windows.Forms.Label _releLed4Label;
        private BEMN.Forms.LedControl _releLed4;
        private System.Windows.Forms.Label _releLed3Label;
        private BEMN.Forms.LedControl _releLed3;
        private System.Windows.Forms.Label _releLed2Label;
        private BEMN.Forms.LedControl _releLed2;
        private System.Windows.Forms.Label _releLed1Label;
        private BEMN.Forms.LedControl _releLed1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _indLed8;
        private System.Windows.Forms.Label label6;
        private BEMN.Forms.LedControl _indLed7;
        private System.Windows.Forms.Label label7;
        private BEMN.Forms.LedControl _indLed6;
        private System.Windows.Forms.Label label8;
        private BEMN.Forms.LedControl _indLed5;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _indLed4;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _indLed3;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _indLed2;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _indLed1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox _systemTimeCheck;
        private DateTimeBox _dateTimeBox;
        private System.Windows.Forms.CheckBox _readTimeCheck;
        private System.Windows.Forms.Button _writeTimeBut;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TabControl _diagTab;
        private System.Windows.Forms.TabPage _diskretPage;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label129;
        private BEMN.Forms.LedControl _faultSignalLed16;
        private BEMN.Forms.LedControl _faultSignalLed15;
        private System.Windows.Forms.Label label131;
        private BEMN.Forms.LedControl _faultSignalLed14;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _faultSignalLed13;
        private System.Windows.Forms.Label label133;
        private BEMN.Forms.LedControl _faultSignalLed12;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _faultSignalLed11;
        private System.Windows.Forms.Label label135;
        private BEMN.Forms.LedControl _faultSignalLed10;
        private System.Windows.Forms.Label label136;
        private BEMN.Forms.LedControl _faultSignalLed9;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _faultSignalLed8;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _faultSignalLed7;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _faultSignalLed6;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _faultSignalLed5;
        private System.Windows.Forms.Label label126;
        private BEMN.Forms.LedControl _faultSignalLed3;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _faultSignalLed2;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _faultSignalLed1;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _faultStateLed6;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _faultStateLed5;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _faultStateLed3;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _faultStateLed2;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _faultStateLed1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _extDefenseLed8;
        private System.Windows.Forms.Label label106;
        private BEMN.Forms.LedControl _extDefenseLed7;
        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _extDefenseLed6;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _extDefenseLed5;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _extDefenseLed4;
        private System.Windows.Forms.Label label110;
        private BEMN.Forms.LedControl _extDefenseLed3;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _extDefenseLed2;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _extDefenseLed1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _autoLed8;
        private System.Windows.Forms.Label label98;
        private BEMN.Forms.LedControl _autoLed7;
        private System.Windows.Forms.Label label99;
        private BEMN.Forms.LedControl _autoLed6;
        private System.Windows.Forms.Label label100;
        private BEMN.Forms.LedControl _autoLed5;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _autoLed4;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _autoLed3;
        private System.Windows.Forms.Label label103;
        private BEMN.Forms.LedControl _autoLed2;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.LedControl _autoLed1;
        private System.Windows.Forms.GroupBox groupBox14;
        private BEMN.Forms.LedControl _I21Led8;
        private BEMN.Forms.LedControl _I21Led7;
        private BEMN.Forms.LedControl _IgLed6;
        private BEMN.Forms.LedControl _IgLed5;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private BEMN.Forms.LedControl _InmaxLed4;
        private BEMN.Forms.LedControl _InmaxLed3;
        private BEMN.Forms.LedControl _InmaxLed2;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _InmaxLed1;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _I0maxLed8;
        private BEMN.Forms.LedControl _I0maxLed7;
        private BEMN.Forms.LedControl _I0maxLed6;
        private BEMN.Forms.LedControl _I0maxLed5;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _I0maxLed4;
        private BEMN.Forms.LedControl _I0maxLed3;
        private BEMN.Forms.LedControl _I0maxLed2;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _I0maxLed1;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _ImaxLed8;
        private BEMN.Forms.LedControl _ImaxLed7;
        private BEMN.Forms.LedControl _ImaxLed6;
        private BEMN.Forms.LedControl _ImaxLed5;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _ImaxLed4;
        private BEMN.Forms.LedControl _ImaxLed3;
        private BEMN.Forms.LedControl _ImaxLed2;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _ImaxLed1;
        private System.Windows.Forms.ImageList _images;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label14_releLed9Label0;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _chapv;
        private BEMN.Forms.LedControl _achr;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl ledControl21;
        private BEMN.Forms.LedControl ledControl20;
        private BEMN.Forms.LedControl ledControl19;
        private BEMN.Forms.LedControl ledControl18;
        private BEMN.Forms.LedControl ledControl17;
        private BEMN.Forms.LedControl ledControl16;
        private BEMN.Forms.LedControl ledControl15;
        private BEMN.Forms.LedControl ledControl14;
        private BEMN.Forms.LedControl ledControl13;
        private BEMN.Forms.LedControl ledControl12;
        private BEMN.Forms.LedControl ledControl11;
        private BEMN.Forms.LedControl ledControl10;
        private BEMN.Forms.LedControl ledControl9;
        private BEMN.Forms.LedControl ledControl8;
        private BEMN.Forms.LedControl ledControl7;
        private BEMN.Forms.LedControl ledControl6;
        private BEMN.Forms.LedControl ledControl1;
        private BEMN.Forms.LedControl ledControl5;
        private BEMN.Forms.LedControl ledControl2;
        private BEMN.Forms.LedControl ledControl4;
        private BEMN.Forms.LedControl ledControl3;
        private BEMN.Forms.LedControl ledControl22;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button _offSplBtn;
        private System.Windows.Forms.Button _onSplBtn;
        private System.Windows.Forms.Label label12;
        private BEMN.Forms.LedControl _manageLed9;


    }
}