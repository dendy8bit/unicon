using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MR500.Properties;
using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace BEMN.MR500
{
    public partial class MConfigurationForm : Form, IFormView
    {
        private MR500 _device;
        private bool _validatingOk = true;
        private bool _connectingErrors;
        private bool butReadClick;
        private const string MR500_BASE_CONFIG_PATH = "\\MR500\\MR500_BaseConfig_v{0}.bin";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "������� ������� ������� ���������";

        public MConfigurationForm()
        {
            this.InitializeComponent();
        }

        public MConfigurationForm(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._groupSelector = new RadioButtonSelector(this._tokDefenseMainConstraintRadio, this._tokDefenseReserveConstraintRadio, this._groupChangeButton);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            this.Init();
        }

        private void Init()
        {
            this._exchangeProgressBar.Value = 0;
            this._device.TZLDevice.InputSignalsLoadOK += new Handler(this._deviceMR700_InputSignalsLoadOK);
            this._device.TZLDevice.InputSignalsLoadFail += new Handler(this._deviceMR700_InputSignalsLoadFail);
            this._device.TZLDevice.KonfCountLoadOK += new Handler(this._deviceMR700_KonfCountLoadOK);
            this._device.TZLDevice.KonfCountLoadFail += new Handler(this._deviceMR700_KonfCountLoadFail);
            this._device.TZLDevice.OutputSignalsLoadOK += new Handler(this._deviceMR700_OutputSignalsLoadOK);
            this._device.TZLDevice.OutputSignalsLoadFail += new Handler(this._deviceMR700_OutputSignalsLoadFail);
            this._device.TZLDevice.ExternalDefensesLoadOK += new Handler(this._deviceMR700_ExternalDefensesLoadOK);
            this._device.TZLDevice.ExternalDefensesLoadFail += new Handler(this._deviceMR700_ExternalDefensesLoadFail);
            this._device.TZLDevice.AutomaticsPageLoadOK += new Handler(this._deviceMR700_AutomaticsPageLoadOK);
            this._device.TZLDevice.AutomaticsPageLoadFail += new Handler(this._deviceMR700_AutomaticsPageLoadFail);
            this._device.TZLDevice.TokDefensesLoadOK += new Handler(this._deviceMR700_TokDefensesMainLoadOK);
            this._device.TZLDevice.TokDefensesLoadFail += new Handler(this._deviceMR700_TokDefensesMainLoadFail);

            this.SubscriptOnSaveHandlers();

            this.PrepareInputSignals();
            this.PrepareOutputSignals();
            this.PrepareExternalDefenses();
            this.PrepareAutomaticsSignals();
            this.PrepareTokDefenses();
        }

        private void _groupSelector_NeedCopyGroup()
        {
            this.WriteTokDefenses(this._groupSelector.SelectedGroup == 0
                ? this._device.TZLDevice.TokDefensesReserve
                : this._device.TZLDevice.TokDefensesMain);
        }

        private void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                if (combo.Items[combo.Items.Count - 1].ToString() == "XXXXX")
                {
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this.butReadClick = true;
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this._device.TZLDevice.Deserialize(this._openConfigurationDlg.FileName);
                }
                catch (FileLoadException exc)
                {
                    this._processLabel.Text = "���� " + Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ��500 ��� ���������";
                    return;
                }
                if (this._tokDefenseMainConstraintRadio.Checked)
                {
                    this.ShowTokDefenses(this._device.TZLDevice.TokDefensesMain);
                }
                else
                {
                    this.ShowTokDefenses(this._device.TZLDevice.TokDefensesReserve);
                }
                this.ReadInputSignals();
                this.ReadExternalDefenses();
                this.ReadOutputSignals();
                this.ReadAutomaticsPage();
                this.ReadKonfCount();
                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
            }
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this.ValidateAll();
            if (this._validatingOk)
            {
                this._saveConfigurationDlg.FileName = string.Format("��500_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._exchangeProgressBar.Value = 0;
                    this._device.TZLDevice.Serialize(this._saveConfigurationDlg.FileName);
                    this._processLabel.Text = "���� " + this._saveConfigurationDlg.FileName + " ��������";
                }
            }
        }
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._connectingErrors = false;
            this._exchangeProgressBar.Value = 0;
            this._validatingOk = true;
            this.ValidateAll();
            if (this._validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ ��500 �" + this._device.DeviceNumber + " ?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "���� ������";
                    this._device.TZLDevice.SaveOutputSignals();
                    this._device.TZLDevice.SaveInputSignals();
                    this._device.TZLDevice.SaveExternalDefenses();
                    this._device.TZLDevice.SaveTokDefenses();
                    this._device.TZLDevice.SaveAutomaticsPage();
                    this._device.TZLDevice.ConfirmConstraint();
                }
            }
        }
        private void ValidateAll()
        {
            this._validatingOk = true;
            //�������� MaskedTextBox
            this.ValidateInputSignals();
            this.ValidateTokDefenses();
            this.ValidateAutomatics();
            //�������� DataGrid
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteOutputSignals()
                                     && this.WriteExternalDefenses()
                                     && this.WriteInputSignals()
                                     && this.WriteAutomaticsPage();
                this._validatingOk &= this._tokDefenseMainConstraintRadio.Checked
                    ? this.WriteTokDefenses(this._device.TZLDevice.TokDefensesMain)
                    : this.WriteTokDefenses(this._device.TZLDevice.TokDefensesReserve);
            }

        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������...";
            this._connectingErrors = false;
            this._device.TZLDevice.LoadInputSignals();
            this._device.TZLDevice.LoadOutputSignals();
            this._device.TZLDevice.LoadExternalDefenses();
            this._device.TZLDevice.LoadAutomaticsPage();
            this._device.TZLDevice.LoadTokDefenses();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��500. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��500. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveOk()
        {
            this._exchangeProgressBar.PerformStep();
        }

        private void OnLoadComplete()
        {
            this._processLabel.Text = this._connectingErrors ? "������ ��������� ���������" : "������ ������� ���������";
        }

        private void OnSaveComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "������ ������� ���������";
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
                this.StartRead();
        }

        private void SubscriptOnSaveHandlers()
        {
            this._device.TZLDevice.LogicSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.LogicSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.OutputSignalsSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.OutputSignalsSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.InputSignalsSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.InputSignalsSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            this._device.TZLDevice.KonfCountSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.KonfCountSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };

            this._device.TZLDevice.ExternalDefensesSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.ExternalDefensesSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.AutomaticsPageSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.AutomaticsPageSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.TokDefensesSaveOK += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TZLDevice.TokDefensesSaveFail += delegate
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
        }

        private void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;
                if (!e.IsValidInput)
                {
                    this.ShowToolTip(box);
                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        this.ShowToolTip(box);
                    }
                }
            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            if (this._validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    this._tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                this._toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                this._validatingOk = false;
            }
        }

        private void ShowToolTip(string msg, DataGridViewCell cell, TabPage outerPage)
        {
            if (!this._validatingOk) return;
            this._tabControl.SelectedTab = outerPage;
            Point cPoint = cell.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Location;
            Point gPoint = cell.DataGridView.Location;
            this._toolTip.Show(msg, outerPage, cPoint.X + gPoint.X, cPoint.Y + gPoint.Y + cell.OwningRow.Height, 2000);
            cell.Selected = true;
            this._validatingOk = false;
        }

        private void Combo_DropDown(object sender, EventArgs e)
        {
            try
            {
                ComboBox combo = (ComboBox)sender;
                combo.DropDown -= new EventHandler(this.Combo_DropDown);
                if (combo.SelectedIndex == combo.Items.Count - 1)
                {
                    combo.SelectedIndex = 0;
                }
                if (combo.Items.Contains("XXXXX"))
                {
                    combo.Items.Remove("XXXXX");
                }
            }
            catch { }
        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                if (combos[i].Items.Count != 0)
                {
                    combos[i].SelectedIndex = 0;
                }
            }
        }

        private void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }

        #region �������� �������
        private void _deviceMR700_OutputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception)
            { }
        }

        private void _deviceMR700_OutputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadOutputSignals));
            }
            catch (Exception eee)
            {
                //MessageBox.Show("��������� �������� �������.", "������");
            }
        }

        private void PrepareOutputSignals()
        {
            this._releTypeCol.Items.Clear();
            this._releTypeCol.Items.AddRange(Strings.TZL.SygnalStr.ToArray());
            this._outIndTypeCol.Items.Clear();
            this._outIndTypeCol.Items.AddRange(Strings.TZL.SygnalStr.ToArray());
            this._releSignalCol.Items.Clear();
            this._outIndSignalCol.Items.Clear();
            this._releSignalCol.Items.AddRange(Strings.TZL.All.ToArray());
            this._outIndSignalCol.Items.AddRange(Strings.TZL.All.ToArray());
            if (this._outputLogicCheckList.Items.Count == 0)
            {
                this._outputLogicCheckList.Items.AddRange(Strings.TZL.OutputSignals.ToArray());
            }
            this._outputLogicCombo.SelectedIndex = 0;
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, "�����������", "���", 0 });
            }
            this._outputIndicatorsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputReleGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "�����������",
                    "���",
                    false,
                    false,
                    false
                });
            }
        }

        private void ReadOutputSignals()
        {
            this._exchangeProgressBar.PerformStep();
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[]
                {
                    i + 1, this._device.TZLDevice.OutputRele[i].Type, this._device.TZLDevice.OutputRele[i].SignalString,
                    this._device.TZLDevice.OutputRele[i].ImpulseUlong
                });
            }
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    this._device.TZLDevice.OutputIndicator[i].Type,
                    this._device.TZLDevice.OutputIndicator[i].SignalString,
                    this._device.TZLDevice.OutputIndicator[i].Indication,
                    this._device.TZLDevice.OutputIndicator[i].Alarm,
                    this._device.TZLDevice.OutputIndicator[i].System
                });
            }
            this._outputLogicCombo.SelectedIndex = 0;
            this.ShowOutputLogicSignals(0);
        }

        private void ShowOutputLogicSignals(int channel)
        {
            try
            {
                BitArray bits = this._device.TZLDevice.TZL_GetOutputLogicSignals(channel);
                for (int i = 0; i < bits.Count; i++)
                {
                    this._outputLogicCheckList.SetItemChecked(i, bits[i]);
                }
            }
            catch { }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;
            if (this._device.TZLDevice.OutputRele.Count == MR500.MR500V2.COutputRele.COUNT && this._outputReleGrid.Rows.Count <= MR500.MR500V2.COutputRele.COUNT)
            {
                for (int i = 0; i < this._outputReleGrid.Rows.Count; i++)
                {
                    this._device.TZLDevice.OutputRele[i].Type = this._outputReleGrid["_releTypeCol", i].Value.ToString();
                    this._device.TZLDevice.OutputRele[i].SignalString = this._outputReleGrid["_releSignalCol", i].Value.ToString();
                    try
                    {
                        ulong value = ulong.Parse(this._outputReleGrid["_releImpulseCol", i].Value.ToString());
                        if (value > MR500.MR500V2.TIMELIMIT)
                        {
                            this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                            ret = false;
                        }
                        else
                        {
                            this._device.TZLDevice.OutputRele[i].ImpulseUlong = value;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                        ret = false;

                    }
                }
            }

            if (this._device.TZLDevice.OutputIndicator.Count == MR500.MR500V2.COutputIndicator.COUNT && this._outputIndicatorsGrid.Rows.Count <= MR500.MR500V2.COutputIndicator.COUNT)
            {
                for (int i = 0; i < this._outputIndicatorsGrid.Rows.Count; i++)
                {
                    this._device.TZLDevice.OutputIndicator[i].Type = this._outputIndicatorsGrid["_outIndTypeCol", i].Value.ToString();
                    this._device.TZLDevice.OutputIndicator[i].SignalString = this._outputIndicatorsGrid["_outIndSignalCol", i].Value.ToString();
                    this._device.TZLDevice.OutputIndicator[i].Indication = (bool)this._outputIndicatorsGrid["_outIndResetCol", i].Value;
                    this._device.TZLDevice.OutputIndicator[i].Alarm = (bool)this._outputIndicatorsGrid["_outIndAlarmCol", i].Value;
                    this._device.TZLDevice.OutputIndicator[i].System = (bool)this._outputIndicatorsGrid["_outIndSystemCol", i].Value;
                }
            }
            return ret;
        }

        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowOutputLogicSignals(this._outputLogicCombo.SelectedIndex);
        }

        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            BitArray bits = new BitArray(this._outputLogicCheckList.Items.Count);
            for (int i = 0; i < this._outputLogicCheckList.Items.Count; i++)
            {
                bits[i] = this._outputLogicCheckList.GetItemChecked(i);

            }
            this._device.TZLDevice.TZL_SetOutputLogicSignals(this._outputLogicCombo.SelectedIndex, bits);
        }
        #endregion

        #region ������� ������

        private void _deviceMR700_ExternalDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void _deviceMR700_ExternalDefensesLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadExternalDefenses));
            }
            catch (InvalidOperationException)
            { }
        }

        private void PrepareExternalDefenses()
        {
            this._externalDefenseGrid.Rows.Clear();
            this._externalDefenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._exDefBlockingCol.Items.Clear();
            this._exDefBlockingCol.Items.AddRange(Strings.TZL.ExternalDefense.ToArray());
            this._exDefModeCol.Items.Clear();
            this._exDefModeCol.Items.AddRange(Strings.TZL.ModesNew.ToArray());
            this._exDefReturnNumberCol.Items.Clear();
            this._exDefReturnNumberCol.Items.AddRange(Strings.TZL.ExternalDefense.ToArray());
            this._exDefWorkingCol.Items.Clear();
            this._exDefWorkingCol.Items.AddRange(Strings.TZL.ExternalDefense.ToArray());
            for (int i = 0; i < MR500.MR500V2.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "��������",
                    "���",
                    "���",
                    0,
                    false,
                    false,
                    "���",
                    0,
                    false,
                    false,
                    false,
                    false,
                    false
                });
                this.OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });
            }
            this._externalDefenseGrid.Height = this._externalDefenseGrid.ColumnHeadersHeight + this._externalDefenseGrid.Rows.Count * this._externalDefenseGrid.RowTemplate.Height + 7;
            this._externalDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._externalDefenseGrid_CellStateChanged);
        }

        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[this._externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(this._externalDefenseGrid, row, "��������", 1, columns);
        }

        private void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (this._exDefModeCol == e.Cell.OwningColumn || this._exDefReturnCol == e.Cell.OwningColumn)
            {
                if (!this.OnExternalDefenseGridModeChanged(e.Cell.RowIndex))
                {
                    GridManager.ChangeCellDisabling(this._externalDefenseGrid, e.Cell.RowIndex, false, 5, new int[] { 6, 7, 8 });
                }
            }
        }

        private void ReadExternalDefenses()
        {
            this._exchangeProgressBar.PerformStep();
            this._externalDefenseGrid.Rows.Clear();

            for (int i = 0; i < MR500.MR500V2.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]
                {
                    i + 1,
                    this._device.TZLDevice.ExternalDefenses[i].Mode1_12,
                    this._device.TZLDevice.ExternalDefenses[i].BlockingNumber,
                    this._device.TZLDevice.ExternalDefenses[i].WorkingNumber,
                    this._device.TZLDevice.ExternalDefenses[i].WorkingTime,
                    this._device.TZLDevice.ExternalDefenses[i].Return,
                    this._device.TZLDevice.ExternalDefenses[i].APV_Return,
                    this._device.TZLDevice.ExternalDefenses[i].ReturnNumber,
                    this._device.TZLDevice.ExternalDefenses[i].ReturnTime,
                    this._device.TZLDevice.ExternalDefenses[i].Osc,
                    this._device.TZLDevice.ExternalDefenses[i].UROV,
                    this._device.TZLDevice.ExternalDefenses[i].APV,
                    this._device.TZLDevice.ExternalDefenses[i].AVR,
                    this._device.TZLDevice.ExternalDefenses[i].Reset
                });
            }
        }

        private bool WriteExternalDefenses()
        {
            bool ret = true;
            if (this._device.TZLDevice.ExternalDefenses.Count == MR500.MR500V2.CExternalDefenses.COUNT && this._externalDefenseGrid.Rows.Count <= MR500.MR500V2.CExternalDefenses.COUNT)
            {
                for (int i = 0; i < this._externalDefenseGrid.Rows.Count; i++)
                {
                    this._device.TZLDevice.ExternalDefenses[i].Mode1_12 = this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                    this._device.TZLDevice.ExternalDefenses[i].BlockingNumber = this._externalDefenseGrid["_exDefBlockingCol", i].Value.ToString();
                    this._device.TZLDevice.ExternalDefenses[i].WorkingNumber = this._externalDefenseGrid["_exDefWorkingCol", i].Value.ToString();
                    this._device.TZLDevice.ExternalDefenses[i].Return = (bool)this._externalDefenseGrid["_exDefReturnCol", i].Value;
                    this._device.TZLDevice.ExternalDefenses[i].APV_Return = (bool)this._externalDefenseGrid["_exDefAPVreturnCol", i].Value;
                    this._device.TZLDevice.ExternalDefenses[i].ReturnNumber = this._externalDefenseGrid["_exDefReturnNumberCol", i].Value.ToString();
                    this._device.TZLDevice.ExternalDefenses[i].Osc = (bool)this._externalDefenseGrid[9, i].Value;
                    this._device.TZLDevice.ExternalDefenses[i].UROV = (bool)this._externalDefenseGrid[10, i].Value;
                    this._device.TZLDevice.ExternalDefenses[i].APV = (bool)this._externalDefenseGrid[11, i].Value;
                    this._device.TZLDevice.ExternalDefenses[i].AVR = (bool)this._externalDefenseGrid[12, i].Value;
                    this._device.TZLDevice.ExternalDefenses[i].Reset = (bool)this._externalDefenseGrid[13, i].Value;
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefReturnTimeCol", i].Value.ToString());
                        if (value > MR500.MR500V2.TIMELIMIT)
                        {
                            throw new OverflowException(MR500.MR500V2.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.TZLDevice.ExternalDefenses[i].ReturnTime = value;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefReturnTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }
                    try
                    {
                        ulong value = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        if (value > MR500.MR500V2.TIMELIMIT)
                        {
                            throw new OverflowException(MR500.MR500V2.TIMELIMIT_ERROR_MSG);
                        }
                        else
                        {
                            this._device.TZLDevice.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                        }
                        this._device.TZLDevice.ExternalDefenses[i].WorkingTime = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefWorkingTimeCol", i], this._externalDefensePage);
                        ret = false;
                    }

                }

            }
            return ret;
        }

        #endregion

        #region ������� �������

        private void ReadInputSignals()
        {
            this._exchangeProgressBar.PerformStep();
            this._TT_Box.Text = this._device.TZLDevice.TT.ToString();
            this._TTNP_Box.Text = this._device.TZLDevice.TTNP.ToString();
            double res;
            if (this._device.TZLDevice.MaxTok > 16) { res = this._device.TZLDevice.MaxTok - 0.01; } else { res = this._device.TZLDevice.MaxTok; }
            this._maxTok_Box.Text = res.ToString();
            this._switcherDurationBox.Text = this._device.TZLDevice.SwitcherDuration.ToString();
            this._switcherImpulseBox.Text = this._device.TZLDevice.SwitcherImpulse.ToString();
            this._switcherTimeBox.Text = this._device.TZLDevice.SwitcherTimeUROV.ToString();
            if (this._device.TZLDevice.SwitcherTokUROV > 24) { res = this._device.TZLDevice.SwitcherTokUROV - 0.01; } else { res = this._device.TZLDevice.SwitcherTokUROV; }
            this._switcherTokBox.Text = res.ToString();

            this._extOffCombo.SelectedItem = this._device.TZLDevice.ExternalOff;
            this._extOnCombo.SelectedItem = this._device.TZLDevice.ExternalOn;
            this._constraintGroupCombo.SelectedItem = this._device.TZLDevice.ConstraintGroup;
            this._keyOffCombo.SelectedItem = this._device.TZLDevice.KeyOff;
            this._keyOnCombo.SelectedItem = this._device.TZLDevice.KeyOn;
            this._signalizationCombo.SelectedItem = this._device.TZLDevice.SignalizationReset;
            this._switcherBlockCombo.SelectedItem = this._device.TZLDevice.SwitcherBlock;
            this._switcherErrorCombo.SelectedItem = this._device.TZLDevice.SwitcherError;
            this._switcherStateOffCombo.SelectedItem = this._device.TZLDevice.SwitcherOff;
            this._switcherStateOnCombo.SelectedItem = this._device.TZLDevice.SwitcherOn;

            this._manageSignalsSDTU_Combo.SelectedItem = this._device.TZLDevice.ManageSignalSDTU;
            this._manageSignalsKeyCombo.SelectedItem = this._device.TZLDevice.ManageSignalKey;
            this._manageSignalsExternalCombo.SelectedItem = this._device.TZLDevice.ManageSignalExternal;
            this._manageSignalsButtonCombo.SelectedItem = this._device.TZLDevice.ManageSignalButton;

            this._TT_typeCombo.SelectedItem = this._device.TZLDevice.TT_Type;
            //this._Sost_ON_OFF.SelectedItem = this._device.TZLDevice.Sost_ON_OFF;
            this._releDispepairBox.Text = this._device.TZLDevice.DispepairImpulse.ToString();

            for (int i = 0; i < this._device.TZLDevice.DispepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.TZLDevice.DispepairSignal[i]);
            }

            this._achrEnter.SelectedItem = this._device.TZLDevice.ACHR_ENTER;
            this._achrBlock.SelectedItem = this._device.TZLDevice.ACHR_BLOCK;
            this._achrTime.Text = this._device.TZLDevice.ACHR_TIME.ToString();

            this._chapvEnter.SelectedItem = this._device.TZLDevice.CHAPV_ENTER;
            this._chapvBlock.SelectedItem = this._device.TZLDevice.CHAPV_BLOCK;
            this._chapvTime.Text = this._device.TZLDevice.CHAPV_TIME.ToString();
            this._chapvOsc.SelectedItem = this._device.TZLDevice.CHAPV_OSC;

            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
            this.ShowLogicKeys(this._device.TZLDevice.Keys);
        }

        private void ReadKonfCount()
        {
            this.oscFix.SelectedItem = this._device.TZLDevice.OscilloscopeFix;
            this.oscPercent.Text = this._device.TZLDevice.OscilloscopePercent.ToString();
            this.oscLength.SelectedItem = this._device.TZLDevice.OscilloscopePeriod;
        }

        private void _deviceMR700_InputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void _deviceMR700_InputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadInputSignals));
            }
            catch (Exception eee)
            {
            }
        }

        private void _deviceMR700_KonfCountLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void _deviceMR700_KonfCountLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadKonfCount));
            }
            catch (InvalidOperationException)
            { }
        }

        private ComboBox[] _inputSignalsCombos;

        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;
        private MaskedTextBox[] _inputSignalsUlongMaskBoxes;

        private void PrepareInputSignals()
        {

            this._inputSignalsCombos = new ComboBox[]
            {
                /*this._Sost_ON_OFF,*/ this._extOffCombo, this._extOnCombo, this._constraintGroupCombo,
                this._keyOffCombo, this._keyOnCombo, this._signalizationCombo, this._switcherBlockCombo,
                this._switcherErrorCombo, this._switcherStateOffCombo, this._switcherStateOnCombo, this._TT_typeCombo,
                this._achrEnter, this._achrBlock, this._chapvEnter, this._chapvBlock, this._chapvOsc,
                this._manageSignalsButtonCombo, this._manageSignalsExternalCombo, this._manageSignalsSDTU_Combo,
                this._manageSignalsKeyCombo, this.oscFix, this.oscLength
            };
            this._inputSignalsDoubleMaskBoxes = new MaskedTextBox[] { this._maxTok_Box, this._switcherTokBox };
            this._inputSignalsUlongMaskBoxes = new MaskedTextBox[]
            {
                this._TTNP_Box, this._TT_Box, this._switcherTimeBox, this._switcherImpulseBox, this._switcherDurationBox,
                this._releDispepairBox, this.oscPercent, this._achrTime, this._chapvTime
            };

            this.ClearCombos(this._inputSignalsCombos);
            this.FillInputSignalsCombos();

            this._logicChannelsCombo.SelectedIndex = 0;
            this.SubscriptCombos(this._inputSignalsCombos);
            this.ShowLogicKeys(this._device.TZLDevice.Keys);
            this.PrepareMaskedBoxes(this._inputSignalsDoubleMaskBoxes, typeof(double));
            this.PrepareMaskedBoxes(this._inputSignalsUlongMaskBoxes, typeof(ulong));
        }

        private void FillInputSignalsCombos()
        {
            this._extOffCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._extOnCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._constraintGroupCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._keyOffCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._keyOnCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.oscLength.Items.AddRange(Strings.TZL.OscLength.ToArray());
            this.oscFix.Items.AddRange(Strings.TZL.OscFix.ToArray());
            this._signalizationCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._switcherBlockCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._switcherErrorCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._switcherStateOffCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._switcherStateOnCombo.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._TT_typeCombo.Items.AddRange(Strings.TZL.TT_Type.ToArray());
            //this._Sost_ON_OFF.Items.AddRange(Strings.TZL.ModesLight.ToArray());
            this._manageSignalsButtonCombo.Items.AddRange(Strings.TZL.Forbidden.ToArray());
            this._manageSignalsKeyCombo.Items.AddRange(Strings.TZL.Control.ToArray());
            this._manageSignalsExternalCombo.Items.AddRange(Strings.TZL.Control.ToArray());
            this._manageSignalsSDTU_Combo.Items.AddRange(Strings.TZL.Forbidden.ToArray());
            this._achrEnter.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._achrBlock.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._chapvEnter.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._chapvBlock.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._chapvOsc.Items.AddRange(Strings.TZL.Osc_Chapv.ToArray());
        }

        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ShowLogicKeys(string keys)
        {
            this._keysDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                if (keys[i] == '0')
                {
                    this._keysDataGrid.Rows.Add(new object[] { i + 1, Strings.TZL.YesNo[0] });
                }
                else
                {
                    this._keysDataGrid.Rows.Add(new object[] { i + 1, Strings.TZL.YesNo[1] });
                }
            }
        }

        private void ShowInputLogicSignals(int channel)
        {
            this._logicSignalsDataGrid.Rows.Clear();
            LogicState[] logicSignals = this._device.TZLDevice.GetInputLogicSignals(channel);
            for (int i = 0; i < 16; i++)
            {
                this._logicSignalsDataGrid.Rows.Add(new object[] {"�" + (i + 1), logicSignals[i].ToString() });
            }
        }

        private void ValidateInputSignals()
        {
            this.ValidateMaskedBoxes(this._inputSignalsUlongMaskBoxes);
            this.ValidateMaskedBoxes(this._inputSignalsDoubleMaskBoxes);
        }

        private bool WriteInputSignals()
        {
            this._device.TZLDevice.TT = UInt16.Parse(this._TT_Box.Text);
            this._device.TZLDevice.TTNP = UInt16.Parse(this._TTNP_Box.Text);
            if (Double.Parse(this._maxTok_Box.Text) > 16)
            {
                this._device.TZLDevice.MaxTok = Double.Parse(this._maxTok_Box.Text) + 0.01;
            }
            else
            {
                this._device.TZLDevice.MaxTok = Double.Parse(this._maxTok_Box.Text);
            }
            this._device.TZLDevice.SwitcherDuration = UInt64.Parse(this._switcherDurationBox.Text);
            this._device.TZLDevice.SwitcherImpulse = UInt64.Parse(this._switcherImpulseBox.Text);
            this._device.TZLDevice.SwitcherTimeUROV = UInt64.Parse(this._switcherTimeBox.Text);
            if (Double.Parse(this._switcherTokBox.Text) > 24)
            {
                this._device.TZLDevice.SwitcherTokUROV = Double.Parse(this._switcherTokBox.Text) + 0.01;
            }
            else
            {
                this._device.TZLDevice.SwitcherTokUROV = Double.Parse(this._switcherTokBox.Text);
            }

            this._device.TZLDevice.ExternalOff = this._extOffCombo.SelectedItem.ToString();
            this._device.TZLDevice.ExternalOn = this._extOnCombo.SelectedItem.ToString();
            this._device.TZLDevice.ConstraintGroup = this._constraintGroupCombo.SelectedItem.ToString();
            this._device.TZLDevice.KeyOff = this._keyOffCombo.SelectedItem.ToString();
            this._device.TZLDevice.KeyOn = this._keyOnCombo.SelectedItem.ToString();
            this._device.TZLDevice.SignalizationReset = this._signalizationCombo.SelectedItem.ToString();
            this._device.TZLDevice.SwitcherBlock = this._switcherBlockCombo.SelectedItem.ToString();
            this._device.TZLDevice.SwitcherError = this._switcherErrorCombo.SelectedItem.ToString();
            this._device.TZLDevice.SwitcherOff = this._switcherStateOffCombo.SelectedItem.ToString();
            this._device.TZLDevice.SwitcherOn = this._switcherStateOnCombo.SelectedItem.ToString();
            this._device.TZLDevice.ACHR_ENTER = this._achrEnter.SelectedItem.ToString();
            this._device.TZLDevice.ACHR_BLOCK = this._achrBlock.SelectedItem.ToString();
            this._device.TZLDevice.ACHR_TIME = Convert.ToInt32(this._achrTime.Text);
            this._device.TZLDevice.CHAPV_ENTER = this._chapvEnter.SelectedItem.ToString();
            this._device.TZLDevice.CHAPV_BLOCK = this._chapvBlock.SelectedItem.ToString();
            this._device.TZLDevice.CHAPV_TIME = Convert.ToInt32(this._chapvTime.Text);
            this._device.TZLDevice.CHAPV_OSC = this._chapvOsc.SelectedItem.ToString();
            CheckedListBoxManager checkManager = new CheckedListBoxManager();
            checkManager.CheckList = this._dispepairCheckList;
            this._device.TZLDevice.DispepairSignal = checkManager.ToBitArray();
            this._device.TZLDevice.DispepairImpulse = ulong.Parse(this._releDispepairBox.Text);

            this._device.TZLDevice.TT_Type = this._TT_typeCombo.SelectedItem.ToString();
            //try
            //{
            //    this._device.TZLDevice.Sost_ON_OFF = this._Sost_ON_OFF.SelectedItem.ToString();
            //}
            //catch { }

            this._device.TZLDevice.ManageSignalButton = this._manageSignalsButtonCombo.SelectedItem.ToString();
            this._device.TZLDevice.ManageSignalExternal = this._manageSignalsExternalCombo.SelectedItem.ToString();
            this._device.TZLDevice.ManageSignalKey = this._manageSignalsKeyCombo.SelectedItem.ToString();
            this._device.TZLDevice.ManageSignalSDTU = this._manageSignalsSDTU_Combo.SelectedItem.ToString();
            bool ret = true;
            ret &= this.WriteOscKonf();
            return ret;

        }

        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            LogicState[] logicSignals = new LogicState[16];
            for (int i = 0; i < this._logicSignalsDataGrid.Rows.Count; i++)
            {
                object o = Enum.Parse(typeof(LogicState), this._logicSignalsDataGrid["_diskretValueCol", i].Value.ToString());
                logicSignals[i] = (LogicState)(o);
            }
            this._device.TZLDevice.SetInputLogicSignals(this._logicChannelsCombo.SelectedIndex, logicSignals);
        }
        #endregion

        #region �������� ����������
        private void _deviceMR700_AutomaticsPageLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void _deviceMR700_AutomaticsPageLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAutomaticsPageLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;
        private RadioButtonSelector _groupSelector;

        private void ValidateAutomatics()
        {
            this.ValidateMaskedBoxes(this._automaticsMaskedBoxes);
            this.ValidateMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint });
        }

        private void PrepareAutomaticsSignals()
        {
            this._automaticCombos = new ComboBox[]
            {
                this.apv_self_off, this.apv_conf, this.apv_blocking,
                this.avr_abrasion, this.avr_blocking, this.avr_reset_blocking,
                this.avr_return, this.avr_start, this.lzsh_confV114
            };
            this._automaticsMaskedBoxes = new MaskedTextBox[]
            {
                this.apv_time_1krat, this.apv_time_2krat, this.apv_time_3krat, this.apv_time_4krat,
                this.apv_time_blocking, this.apv_time_ready, this.avr_disconnection,
                this.avr_time_abrasion, this.avr_time_return,
            };

            this.PrepareMaskedBoxes(this._automaticsMaskedBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(new MaskedTextBox[] { this.lzsh_constraint }, typeof(double));
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);



        }

        private void FillAutomaticCombo()
        {
            this.apv_blocking.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.avr_start.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.avr_blocking.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.avr_reset_blocking.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.avr_abrasion.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.avr_return.Items.AddRange(Strings.TZL.Logic.ToArray());
            this.apv_conf.Items.AddRange(Strings.TZL.Crat.ToArray());
            this.lzsh_confV114.Items.AddRange(Strings.TZL.LZSH114.ToArray());
            this.apv_self_off.Items.AddRange(Strings.TZL.YesNo.ToArray());
        }

        private void ReadAutomaticsPage()
        {
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.TZLDevice.APV_Cnf;
            this.apv_blocking.Text = this._device.TZLDevice.APV_Blocking;
            this.apv_self_off.Text = this._device.TZLDevice.APV_Start;
            this.avr_start.Text = this._device.TZLDevice.AVR_Start;
            this.avr_blocking.Text = this._device.TZLDevice.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.TZLDevice.AVR_Reset;
            this.avr_abrasion.Text = this._device.TZLDevice.AVR_Abrasion;
            this.avr_return.Text = this._device.TZLDevice.AVR_Return;
            this.lzsh_confV114.Text = this._device.TZLDevice.LZSH_CnfV114;

            this.apv_time_blocking.Text = this._device.TZLDevice.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.TZLDevice.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.TZLDevice.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.TZLDevice.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.TZLDevice.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.TZLDevice.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.TZLDevice.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.TZLDevice.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.TZLDevice.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.TZLDevice.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.TZLDevice.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.TZLDevice.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.TZLDevice.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.TZLDevice.AVR_Time_Off.ToString();
            double res;
            if (this._device.TZLDevice.LZSH_Constraint > 24)
            {
                res = this._device.TZLDevice.LZSH_Constraint - 0.01;
            }
            else
            {
                res = this._device.TZLDevice.LZSH_Constraint;
            }
            this.lzsh_constraint.Text = res.ToString();
        }


        private void OnAutomaticsPageLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.apv_conf.Text = this._device.TZLDevice.APV_Cnf;
            this.apv_blocking.Text = this._device.TZLDevice.APV_Blocking;
            this.apv_self_off.Text = this._device.TZLDevice.APV_Start;
            this.avr_start.Text = this._device.TZLDevice.AVR_Start;
            this.avr_blocking.Text = this._device.TZLDevice.AVR_Blocking;
            this.avr_reset_blocking.Text = this._device.TZLDevice.AVR_Reset;
            this.avr_abrasion.Text = this._device.TZLDevice.AVR_Abrasion;
            this.avr_return.Text = this._device.TZLDevice.AVR_Return;
            this.lzsh_confV114.Text = this._device.TZLDevice.LZSH_CnfV114;

            this.apv_time_blocking.Text = this._device.TZLDevice.APV_Time_Blocking.ToString();
            this.apv_time_ready.Text = this._device.TZLDevice.APV_Time_Ready.ToString();
            this.apv_time_1krat.Text = this._device.TZLDevice.APV_Time_1Krat.ToString();
            this.apv_time_2krat.Text = this._device.TZLDevice.APV_Time_2Krat.ToString();
            this.apv_time_3krat.Text = this._device.TZLDevice.APV_Time_3Krat.ToString();
            this.apv_time_4krat.Text = this._device.TZLDevice.APV_Time_4Krat.ToString();

            this.avr_supply_off.Checked = this._device.TZLDevice.AVR_Supply_Off;
            this.avr_self_off.Checked = this._device.TZLDevice.AVR_Self_Off;
            this.avr_switch_off.Checked = this._device.TZLDevice.AVR_Switch_Off;
            this.avr_abrasion_switch.Checked = this._device.TZLDevice.AVR_Abrasion_Switch;
            this.avr_permit_reset_switch.Checked = this._device.TZLDevice.AVR_Reset_Switch;
            this.avr_time_abrasion.Text = this._device.TZLDevice.AVR_Time_Abrasion.ToString();
            this.avr_time_return.Text = this._device.TZLDevice.AVR_Time_Return.ToString();
            this.avr_disconnection.Text = this._device.TZLDevice.AVR_Time_Off.ToString();
            double res;
            if (this._device.TZLDevice.LZSH_Constraint > 24)
            {
                res = this._device.TZLDevice.LZSH_Constraint - 0.01;
            }
            else
            {
                res = this._device.TZLDevice.LZSH_Constraint;
            }
            this.lzsh_constraint.Text = res.ToString();

        }

        private bool WriteAutomaticsPage()
        {
            this._device.TZLDevice.APV_Cnf = this.apv_conf.Text;
            this._device.TZLDevice.APV_Blocking = this.apv_blocking.Text;
            this._device.TZLDevice.APV_Time_Blocking = UInt64.Parse(this.apv_time_blocking.Text);
            this._device.TZLDevice.APV_Time_Ready = UInt64.Parse(this.apv_time_ready.Text);
            this._device.TZLDevice.APV_Time_1Krat = UInt64.Parse(this.apv_time_1krat.Text);
            this._device.TZLDevice.APV_Time_2Krat = UInt64.Parse(this.apv_time_2krat.Text);
            this._device.TZLDevice.APV_Time_3Krat = UInt64.Parse(this.apv_time_3krat.Text);
            this._device.TZLDevice.APV_Time_4Krat = UInt64.Parse(this.apv_time_4krat.Text);
            this._device.TZLDevice.APV_Start = this.apv_self_off.Text;

            this._device.TZLDevice.AVR_Supply_Off = this.avr_supply_off.Checked;
            this._device.TZLDevice.AVR_Self_Off = this.avr_self_off.Checked;
            this._device.TZLDevice.AVR_Switch_Off = this.avr_switch_off.Checked;
            this._device.TZLDevice.AVR_Abrasion_Switch = this.avr_abrasion_switch.Checked;
            this._device.TZLDevice.AVR_Reset_Switch = this.avr_permit_reset_switch.Checked;
            this._device.TZLDevice.AVR_Start = this.avr_start.Text;
            this._device.TZLDevice.AVR_Blocking = this.avr_blocking.Text;
            this._device.TZLDevice.AVR_Reset = this.avr_reset_blocking.Text;
            this._device.TZLDevice.AVR_Abrasion = this.avr_abrasion.Text;
            this._device.TZLDevice.AVR_Time_Abrasion = UInt64.Parse(this.avr_time_abrasion.Text);
            this._device.TZLDevice.AVR_Return = this.avr_return.Text;
            this._device.TZLDevice.AVR_Time_Return = UInt64.Parse(this.avr_time_return.Text);
            this._device.TZLDevice.AVR_Time_Off = UInt64.Parse(this.avr_disconnection.Text);

            this._device.TZLDevice.LZSH_CnfV114 = this.lzsh_confV114.Text;
            if (Double.Parse(this.lzsh_constraint.Text) > 24)
            {
                this._device.TZLDevice.LZSH_Constraint = double.Parse(this.lzsh_constraint.Text) + 0.01;
            }
            else
            {
                this._device.TZLDevice.LZSH_Constraint = double.Parse(this.lzsh_constraint.Text);
            }
            return true;
        }
        #endregion

        #region ������� ������

        private void ChangeTokDefenseCellDisabling1(DataGridView grid, int row)
        {
            this.OnTokDefenseGridModeChanged(grid, row);
        }

        private void ChangeTokDefenseCellDisabling3(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {
                if (0 == row)
                {
                    GridManager.ChangeCellDisabling(grid, row, "��������", 1, 4);
                    GridManager.ChangeCellDisabling(grid, row, "��������", 1, 8);
                }
            }
        }


        private void tokdef4(MR500.MR500V2.CTokDefenses tokDefenses, int i)
        {
            if (tokDefenses[i].Feature == "���������")
            {
                this._tokDefenseGrid.Rows.Add(new object[]
                {
                    tokDefenses[i].Name,
                    tokDefenses[i].Mode1_11,
                    tokDefenses[i].BlockingNumber,
                    tokDefenses[i].Parameter,
                    tokDefenses[i].WorkConstraint,
                    tokDefenses[i].Feature,
                    tokDefenses[i].WorkTime/10,
                    tokDefenses[i].Speedup,
                    tokDefenses[i].SpeedupTime,
                    tokDefenses[i].Oscv1_11,
                    tokDefenses[i].UROV,
                    tokDefenses[i].APV,
                    tokDefenses[i].AVR
                });
                this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid, i);
            }
            else
            {
                this._tokDefenseGrid.Rows.Add(new object[]
                {
                    tokDefenses[i].Name,
                    tokDefenses[i].Mode1_11,
                    tokDefenses[i].BlockingNumber,
                    tokDefenses[i].Parameter,
                    tokDefenses[i].WorkConstraint,
                    tokDefenses[i].Feature,
                    tokDefenses[i].WorkTime,
                    tokDefenses[i].Speedup,
                    tokDefenses[i].SpeedupTime,
                    tokDefenses[i].Oscv1_11,
                    tokDefenses[i].UROV,
                    tokDefenses[i].APV,
                    tokDefenses[i].AVR
                });
                this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid, i);
            }
        }

        private void tokdef4_10(MR500.MR500V2.CTokDefenses tokDefenses, int i)
        {
            this._tokDefenseGrid3.Rows.Add(new object[]
            {
                tokDefenses[i].Name,
                tokDefenses[i].Mode1_11,
                tokDefenses[i].BlockingNumber,
                tokDefenses[i].WorkConstraint,
                tokDefenses[i].WorkTime,
                tokDefenses[i].Speedup,
                tokDefenses[i].SpeedupTime,
                tokDefenses[i].Oscv1_11,
                tokDefenses[i].UROV,
                tokDefenses[i].APV,
                tokDefenses[i].AVR
            });
            this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
        }

        private void ShowTokDefenses(MR500.MR500V2.CTokDefenses tokDefenses)
        {
            this._tokDefenseGrid.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();

            this._tokDefenseIbox.Text = tokDefenses.I.ToString();
            this._tokDefenseInbox.Text = tokDefenses.In.ToString();
            this._tokDefenseI0box.Text = tokDefenses.I0.ToString();
            this._tokDefenseI2box.Text = tokDefenses.I2.ToString();

            for (int i = 0; i < 4; i++)
            {
                this.tokdef4(tokDefenses, i);
            }
            for (int i = 4; i < 10; i++)
            {
                this.tokdef4_10(tokDefenses, i);
            }
            const int Ig_index = 10;
            this._tokDefenseGrid4.Rows.Add(new object[]
            {
                tokDefenses[Ig_index].Name,
                tokDefenses[Ig_index].Mode1_11,
                tokDefenses[Ig_index].BlockingNumber,
                tokDefenses[Ig_index].WorkConstraint,
                tokDefenses[Ig_index].WorkTime,
                tokDefenses[Ig_index].Speedup,
                tokDefenses[Ig_index].SpeedupTime,
                tokDefenses[Ig_index].Oscv1_11,
                tokDefenses[Ig_index].UROV,
                tokDefenses[Ig_index].APV,
                tokDefenses[Ig_index].AVR
            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);

            const int I12_index = 11;
            this._tokDefenseGrid4.Rows.Add(new object[]
            {
                tokDefenses[I12_index].Name,
                tokDefenses[I12_index].Mode1_11,
                tokDefenses[I12_index].BlockingNumber,
                tokDefenses[I12_index].WorkConstraint,
                tokDefenses[I12_index].WorkTime,
                tokDefenses[I12_index].Speedup,
                tokDefenses[I12_index].SpeedupTime,
                tokDefenses[I12_index].Oscv1_11,
                tokDefenses[I12_index].UROV,
                tokDefenses[I12_index].APV,
                tokDefenses[I12_index].AVR
            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);

            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid.Rows.Count; i++)
            {
                this._tokDefenseGrid[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        }

        private void PrepareTokDefenses()
        {
            this._tokDefenseGrid.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();
            this._tokDefenseGrid4.Rows.Clear();
            this._tokDefenseModeCol.Items.Clear();
            this._tokDefenseModeCol.Items.AddRange(Strings.TZL.ModesNew.ToArray());
            this._tokDefense3ModeCol.Items.Clear();
            this._tokDefense3ModeCol.Items.AddRange(Strings.TZL.ModesNew.ToArray());
            this._tokDefense4ModeCol.Items.Clear();
            this._tokDefense4ModeCol.Items.AddRange(Strings.TZL.ModesNew.ToArray());
            this._tokDefenseOSCv11Col.Items.Clear();
            this._tokDefenseOSCv11Col.Items.AddRange(Strings.TZL.Osc_v1_11.ToArray());
            this._tokDefense3OSCv11Col.Items.Clear();
            this._tokDefense3OSCv11Col.Items.AddRange(Strings.TZL.Osc_v1_11.ToArray());
            this._tokDefense4OSCv11Col.Items.Clear();
            this._tokDefense4OSCv11Col.Items.AddRange(Strings.TZL.Osc_v1_11.ToArray());
            this._tokDefenseParameterCol.Items.Clear();
            this._tokDefenseParameterCol.Items.AddRange(Strings.TZL.TokParameter.ToArray());
            this._tokDefenseFeatureCol.Items.Clear();
            this._tokDefenseFeatureCol.Items.AddRange(Strings.TZL.FeatureLight.ToArray());
            this._tokDefenseBlockNumberCol.Items.Clear();
            this._tokDefenseBlockNumberCol.Items.AddRange(Strings.TZL.Logic.ToArray());
            this._tokDefense3BlockingNumberCol.Items.Clear();
            this._tokDefense3BlockingNumberCol.Items.AddRange(Strings.TZL.Logic.ToArray());

            this._tokDefense4BlockNumberCol.Items.Clear();
            this._tokDefense4BlockNumberCol.Items.AddRange(Strings.TZL.Logic.ToArray());

            this._tokDefenseI0box.ValidatingType = this._tokDefenseI2box.ValidatingType =
                this._tokDefenseInbox.ValidatingType = this._tokDefenseIbox.ValidatingType = typeof(ulong);

            this._tokDefenseIbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI2box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseI0box.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
            this._tokDefenseInbox.TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);

            this.ShowTokDefenses(this._device.TZLDevice.TokDefensesMain);

            this._tokDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid4.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);

            this._tokDefenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid4.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
        }

        private void _tokDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (this._tokDefenseModeCol == e.Cell.OwningColumn ||
                this._tokDefenseSpeedUpCol == e.Cell.OwningColumn ||

                this._tokDefense3ModeCol == e.Cell.OwningColumn ||
                this._tokDefense3SpeedupCol == e.Cell.OwningColumn
                )
            {
                this.ChangeTokDefenseCellDisabling1(sender as DataGridView, e.Cell.RowIndex);
            }

            if (this._tokDefense4ModeCol == e.Cell.OwningColumn ||
                (this._tokDefense4SpeedupCol == e.Cell.OwningColumn && 1 != e.Cell.RowIndex))
            {
                this.ChangeTokDefenseCellDisabling3(sender as DataGridView, e.Cell.RowIndex);
            }

        }

        private void _deviceMR700_TokDefensesMainLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnTokDefenseLoadOk()
        {
            this._exchangeProgressBar.PerformStep();

            if (this._tokDefenseMainConstraintRadio.Checked)
            {
                this.ShowTokDefenses(this._device.TZLDevice.TokDefensesMain);
            }
            else
            {
                this.ShowTokDefenses(this._device.TZLDevice.TokDefensesReserve);
            }
        }

        private void _deviceMR700_TokDefensesMainLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnTokDefenseLoadOk));
                Invoke(new OnDeviceEventHandler(this.OnLoadComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        private bool OnTokDefenseGridModeChanged(DataGridView grid, int row)
        {
            int[] columns = new int[grid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(grid, row, "��������", 1, columns);
        }

        private bool WriteTokDefenseItem(MR500.MR500V2.CTokDefenses tokDefenses, DataGridView grid, int rowIndex, int itemIndex)
        {
            bool ret = true;
            bool enabled;
            tokDefenses[itemIndex].Mode = grid[1, rowIndex].Value.ToString();
            enabled = tokDefenses[itemIndex].Mode == "��������";
            tokDefenses[itemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
            double limit = 40;
            if (grid == this._tokDefenseGrid)
            {
                tokDefenses[itemIndex].Parameter = grid[3, rowIndex].Value.ToString();
                tokDefenses[itemIndex].Feature = grid[5, rowIndex].Value.ToString();
            }

            if (grid == this._tokDefenseGrid4)
            {
                if (0 == rowIndex)
                {
                    limit = 5;
                }
                else
                {
                    limit = 100;
                }
            }
            if (grid == this._tokDefenseGrid3)
            {
                if (4 == rowIndex || 5 == rowIndex)
                {
                    limit = 5;
                }
            }
            int workConstraintIndex = grid == this._tokDefenseGrid ? 4 : 3;
            int workTimeIndex = grid == this._tokDefenseGrid ? 6 : 4;
            int speedupIndex = grid == this._tokDefenseGrid ? 7 : 5;
            int speedupTimeIndex = grid == this._tokDefenseGrid ? 8 : 6;
            try
            {
                double value;
                if (double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()) > 24)
                {
                    value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString()) - 0.01;
                }
                else
                {
                    value = double.Parse(grid[workConstraintIndex, rowIndex].Value.ToString());
                }
                if ((value < 0 || value > limit) && !enabled)
                {
                    this.ShowToolTip("������� �����  � ��������� [0 - " + limit + ".0]", grid[workConstraintIndex, rowIndex], this._tokDefensesPage);
                    ret = false;
                }
                else
                {
                    if (value > 24)
                    {
                        tokDefenses[itemIndex].WorkConstraint = (value + 0.01);
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkConstraint = value;
                    }
                }
            }
            catch (Exception)
            {
                this.ShowToolTip("������� �����  � ��������� [0 - " + limit + ".0]", grid[workConstraintIndex, rowIndex], this._tokDefensesPage);
                ret = false;
            }
            try
            {
                ulong value;
                if (grid[workTimeIndex, rowIndex].Value == null)
                {
                    value = 0;
                }
                else
                {
                    value = ulong.Parse(grid[workTimeIndex, rowIndex].Value.ToString());
                }

                if ("���������" == grid[5, rowIndex].Value.ToString())
                {
                    if (value > MR500.MR500V2.KZLIMIT && !enabled)
                    {
                        this.ShowToolTip(MR500.MR500V2.KZLIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._tokDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value * 10;
                    }
                }
                else
                {
                    if (value > MR500.MR500V2.TIMELIMIT && !enabled)
                    {
                        this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._tokDefensesPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value;
                    }
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, grid[workTimeIndex, rowIndex], this._tokDefensesPage);
                ret = false;
            }
            tokDefenses[itemIndex].Speedup = (bool)grid[speedupIndex, rowIndex].Value;
            try
            {
                ulong value = ulong.Parse(grid[speedupTimeIndex, rowIndex].Value.ToString());
                if (value > MR500.MR500V2.TIMELIMIT && !enabled)
                {
                    this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex],
                                this._tokDefensesPage);
                    ret = false;
                }
                else
                {
                    tokDefenses[itemIndex].SpeedupTime = value;
                }

            }
            catch (Exception)
            {
                this.ShowToolTip(MR500.MR500V2.TIMELIMIT_ERROR_MSG, grid[speedupTimeIndex, rowIndex], this._tokDefensesPage);
                ret = false;
            }
            int oscIndex = grid == this._tokDefenseGrid ? 9 : 7;
            int urovIndex = grid == this._tokDefenseGrid ? 10 : 8;
            int apvIndex = grid == this._tokDefenseGrid ? 11 : 9;
            int avrIndex = grid == this._tokDefenseGrid ? 12 : 10;
            tokDefenses[itemIndex].Oscv1_11 = grid[oscIndex, rowIndex].Value.ToString();
            tokDefenses[itemIndex].UROV = (bool)grid[urovIndex, rowIndex].Value;
            tokDefenses[itemIndex].APV = (bool)grid[apvIndex, rowIndex].Value;
            tokDefenses[itemIndex].AVR = (bool)grid[avrIndex, rowIndex].Value;
            return ret;
        }

        private bool WriteTokDefenses(MR500.MR500V2.CTokDefenses tokDefenses)
        {
            bool ret = true;
            tokDefenses.I = ushort.Parse(this._tokDefenseIbox.Text);
            tokDefenses.I0 = ushort.Parse(this._tokDefenseI0box.Text);
            tokDefenses.I2 = ushort.Parse(this._tokDefenseI2box.Text);
            tokDefenses.In = ushort.Parse(this._tokDefenseInbox.Text);
            for (int i = 0; i < 12; i++)
            {
                if (i >= 0 && i < 4)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid, i, i);
                }
                if (i >= 4 && i < 10)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid3, i - 4, i);
                }
                if (i >= 10 && i < 12)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid4, i - 10, i);
                }
            }

            return ret;
        }

        private void ValidateTokDefenses()
        {
            this._tokDefenseIbox.ValidateText();
            this._tokDefenseI2box.ValidateText();
            this._tokDefenseI0box.ValidateText();
            this._tokDefenseInbox.ValidateText();
        }

        private void _tokDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (this._tokDefenseMainConstraintRadio.Checked)
            {
                this._device.TZLDevice.TokDefensesReserve = new MR500.MR500V2.CTokDefenses();
                this.WriteTokDefenses(this._device.TZLDevice.TokDefensesReserve);
                this.ShowTokDefenses(this._device.TZLDevice.TokDefensesMain);
            }
            else
            {
                this._device.TZLDevice.TokDefensesMain = new MR500.MR500V2.CTokDefenses();
                this.WriteTokDefenses(this._device.TZLDevice.TokDefensesMain);
                this.ShowTokDefenses(this._device.TZLDevice.TokDefensesReserve);
            }
        }

        #endregion

        private void _applyKeys_Click(object sender, EventArgs e)
        {
            string keys = "";
            for (int i = 0; i < 16; i++)
            {
                if (this._keysDataGrid.Rows[i].Cells[1].Value.ToString() == "��")
                {
                    keys += "1";
                }
                else
                {
                    keys += "0";
                }
            }

            this._device.TZLDevice.Keys = keys;
        }

        public bool WriteOscKonf()
        {
            this._device.TZLDevice.OscilloscopeFix = this.oscFix.SelectedItem.ToString();
            this._device.TZLDevice.OscilloscopePeriod = this.oscLength.SelectedItem.ToString();
            ushort percent = Convert.ToUInt16(this.oscPercent.Text);
            if (percent >= 1 && percent <= 100)
            {
                this._device.TZLDevice.OscilloscopePercent = percent;
                return true;
            }
            MessageBox.Show("������������ ���������� ������������� ������ ������ � �������� [1...100]%",
                "�������� �������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            this.ValidateAll();
            if (this._validatingOk)
            {
                try
                {
                    KeysForXml();
                    this._device.TZLDevice.PrepareSerialize();
                    this._processLabel.Text = HtmlExport.Export(Resources.MR500Main, Resources.MR500Res, this._device.TZLDevice, "��500", this._device.DeviceVersion);
                }
                catch (Exception e)
                {
                    MessageBox.Show("������ ���������� � ����!", "��������!", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                
            }
        }

        private void KeysForXml()
        {
            _device.TZLDevice.Keys1 = _keysDataGrid[1, 0].Value.ToString();
            _device.TZLDevice.Keys2 = _keysDataGrid[1, 1].Value.ToString();
            _device.TZLDevice.Keys3 = _keysDataGrid[1, 2].Value.ToString();
            _device.TZLDevice.Keys4 = _keysDataGrid[1, 3].Value.ToString();
            _device.TZLDevice.Keys5 = _keysDataGrid[1, 4].Value.ToString();
            _device.TZLDevice.Keys6 = _keysDataGrid[1, 5].Value.ToString();
            _device.TZLDevice.Keys7 = _keysDataGrid[1, 6].Value.ToString();
            _device.TZLDevice.Keys8 = _keysDataGrid[1, 7].Value.ToString();
            _device.TZLDevice.Keys9 = _keysDataGrid[1, 8].Value.ToString();
            _device.TZLDevice.Keys10 = _keysDataGrid[1, 9].Value.ToString();
            _device.TZLDevice.Keys11 = _keysDataGrid[1, 10].Value.ToString();
            _device.TZLDevice.Keys12 = _keysDataGrid[1, 11].Value.ToString();
            _device.TZLDevice.Keys13 = _keysDataGrid[1, 12].Value.ToString();
            _device.TZLDevice.Keys14 = _keysDataGrid[1, 13].Value.ToString();
            _device.TZLDevice.Keys15 = _keysDataGrid[1, 14].Value.ToString();
            _device.TZLDevice.Keys16 = _keysDataGrid[1, 15].Value.ToString();

        }

        private void _tokDefenseGrid1_CellErrorTextChanged(object sender, DataGridViewCellEventArgs e)
        {
            int c = e.ColumnIndex;
            int r = e.RowIndex;
        }

        private void _tokDefenseGrid1_CellErrorTextNeeded(object sender, DataGridViewCellErrorTextNeededEventArgs e)
        {
            string s = e.ErrorText;
        }

        private void MConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.TZLDevice.InputSignalsLoadOK -= new Handler(this._deviceMR700_InputSignalsLoadOK);
            this._device.TZLDevice.InputSignalsLoadFail -= new Handler(this._deviceMR700_InputSignalsLoadFail);
            this._device.TZLDevice.KonfCountLoadOK -= new Handler(this._deviceMR700_KonfCountLoadOK);
            this._device.TZLDevice.KonfCountLoadFail -= new Handler(this._deviceMR700_KonfCountLoadFail);
            this._device.TZLDevice.OutputSignalsLoadOK -= new Handler(this._deviceMR700_OutputSignalsLoadOK);
            this._device.TZLDevice.OutputSignalsLoadFail -= new Handler(this._deviceMR700_OutputSignalsLoadFail);
            this._device.TZLDevice.ExternalDefensesLoadOK -= new Handler(this._deviceMR700_ExternalDefensesLoadOK);
            this._device.TZLDevice.ExternalDefensesLoadFail -= new Handler(this._deviceMR700_ExternalDefensesLoadFail);
            this._device.TZLDevice.AutomaticsPageLoadOK -= new Handler(this._deviceMR700_AutomaticsPageLoadOK);
            this._device.TZLDevice.AutomaticsPageLoadFail -= new Handler(this._deviceMR700_AutomaticsPageLoadFail);
            this._device.TZLDevice.TokDefensesLoadOK -= new Handler(this._deviceMR700_TokDefensesMainLoadOK);
            this._device.TZLDevice.TokDefensesLoadFail -= new Handler(this._deviceMR700_TokDefensesMainLoadFail);
        }
        private void Mr500ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(true);
        }

        #region [����� �������]

        private void ResetSetpoints(bool isDialog)
        {
            try
            {

                string path = (Path.GetDirectoryName(Application.ExecutablePath) +
                                                             string.Format(MR500_BASE_CONFIG_PATH, "3.00"));
                this._device.TZLDevice.Deserialize(path);


                if (this._tokDefenseMainConstraintRadio.Checked)
                {
                    this.ShowTokDefenses(this._device.TZLDevice.TokDefensesMain);
                }
                else
                {
                    this.ShowTokDefenses(this._device.TZLDevice.TokDefensesReserve);
                }
                this.ReadInputSignals();
                this.ReadExternalDefenses();
                this.ReadOutputSignals();
                this.ReadAutomaticsPage();
                this.ReadKonfCount();
                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch (FileLoadException exc)
            {
                if (isDialog)
                {
                    if (MessageBox.Show("���������� ��������� ���� ����������� ������������. �������� �������?",
                            "��������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        this.ResetInputSignals();
                    this.ResetOutputSignals();
                    this.ResetExtDef();
                    this.ResetAutomat();
                    this.ResetTokDef1();
                    this.ResetTokDef2();
                    this.ResetTokDef3();
                }
                else
                {
                    return;
                }
            }
        }






        //����� ������� ��������
        private void ResetInputSignals()
        {
            this._TT_Box.Text = 0.ToString();
            this._TTNP_Box.Text = 0.ToString();
            this._maxTok_Box.Text = 0.ToString();
            this._TT_typeCombo.SelectedIndex = 0;

            this._keyOffCombo.SelectedIndex = 0;
            this._keyOnCombo.SelectedIndex = 0;
            this._extOffCombo.SelectedIndex = 0;
            this._extOnCombo.SelectedIndex = 0;
            this._signalizationCombo.SelectedIndex = 0;
            this._constraintGroupCombo.SelectedIndex = 0;

            this._manageSignalsButtonCombo.SelectedIndex = 0;
            this._manageSignalsKeyCombo.SelectedIndex = 0;
            this._manageSignalsExternalCombo.SelectedIndex = 0;
            this._manageSignalsSDTU_Combo.SelectedIndex = 0;

            this.oscLength.SelectedIndex = 0;
            this.oscFix.SelectedIndex = 0;
            this.oscPercent.Text = 1.ToString();

            this._switcherStateOffCombo.SelectedIndex = 0;
            this._switcherStateOnCombo.SelectedIndex = 0;
            this._switcherErrorCombo.SelectedIndex = 0;
            this._switcherBlockCombo.SelectedIndex = 0;
            this._switcherTimeBox.Text = 0.ToString();
            this._switcherTokBox.Text = 0.ToString();
            this._switcherImpulseBox.Text = 0.ToString();
            this._switcherDurationBox.Text = 0.ToString();

            this._releDispepairBox.Text = 0.ToString();

            //����� ��������������
            for (int i = 0; i < this._device.TZLDevice.DispepairSignal.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.TZLDevice.DispepairSignal[i]);
            }
            this._dispepairCheckList.ClearSelected();

            //����� ��
            for (int i = 0; i < this._logicChannelsCombo.Items.Count; i++)
            {
                this._device.TZLDevice.SetInputLogicSignals(i, new LogicState[16]);
            }
            this._logicSignalsDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                this._logicSignalsDataGrid.Rows.Add(new object[] { i + 1, Strings.TZL.LogycValues[0] });
            }
            this._logicChannelsCombo.SelectedIndex = 0;
            //����� ������
            this._keysDataGrid.Rows.Clear();
            for (int i = 0; i < 16; i++)
            {
                this._keysDataGrid.Rows.Add(new object[] { i + 1, Strings.TZL.YesNo[0] });
            }

        }

        // ����� �������� ��������
        private void ResetOutputSignals()
        {
            //����� ���
            for (int i = 0; i < this._outputLogicCombo.Items.Count; i++)
            {
                this._device.TZLDevice.TZL_SetOutputLogicSignals(i, new BitArray(120));
            }
            this._outputLogicCheckList.ClearSelected();
            this._outputLogicCombo.SelectedIndex = 0;

            this._outputLogicCheckList.Items.Clear();
            if (this._outputLogicCheckList.Items.Count == 0)
            {
                this._outputLogicCheckList.Items.AddRange(Strings.TZL.OutputSignals.ToArray());
            }

            //����� �������� ����
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.COutputRele.COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 1, "�����������", "���", 0 });
            }

            //����� �����������
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "�����������",
                    "���",
                    false,
                    false,
                    false
                });
            }

        }

        //����� ������� �����
        private void ResetExtDef()
        {
            this._externalDefenseGrid.Rows.Clear();
            for (int i = 0; i < MR500.MR500V2.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]
                {
                    i + 1,
                    "��������",
                    "���",
                    "���",
                    0,
                    false,
                    false,
                    "���",
                    0,
                    false,
                    false,
                    false,
                    false,
                    false
                });
                this.OnExternalDefenseGridModeChanged(i);
                GridManager.ChangeCellDisabling(this._externalDefenseGrid, i, false, 5, new int[] { 6, 7, 8 });
            }
        }

        //����� ����������
        private void ResetAutomat()
        {
            this.apv_conf.SelectedIndex = 0;
            this.apv_blocking.SelectedIndex = 0;
            this.apv_time_blocking.Text = 0.ToString();
            this.apv_time_ready.Text = 0.ToString();
            this.apv_time_1krat.Text = 0.ToString();
            this.apv_time_2krat.Text = 0.ToString();
            this.apv_time_3krat.Text = 0.ToString();
            this.apv_time_4krat.Text = 0.ToString();
            this.apv_self_off.SelectedIndex = 0;

            this.lzsh_confV114.SelectedIndex = 0;
            this.lzsh_constraint.Text = 0.ToString();

            this._achrEnter.SelectedIndex = 0;
            this._achrBlock.SelectedIndex = 0;
            this._achrTime.Text = 0.ToString();

            this.avr_supply_off.Checked = false;
            this.avr_switch_off.Checked = false;
            this.avr_self_off.Checked = false;
            this.avr_abrasion_switch.Checked = false;

            this.avr_permit_reset_switch.Checked = false;

            this.avr_start.SelectedIndex = 0;
            this.avr_blocking.SelectedIndex = 0;
            this.avr_reset_blocking.SelectedIndex = 0;
            this.avr_abrasion.SelectedIndex = 0;
            this.avr_time_abrasion.Text = 0.ToString();
            this.avr_return.SelectedIndex = 0;
            this.avr_time_return.Text = 0.ToString();
            this.avr_disconnection.Text = 0.ToString();

            this._chapvEnter.SelectedIndex = 0;
            this._chapvBlock.SelectedIndex = 0;
            this._chapvTime.Text = 0.ToString();
            this._chapvOsc.SelectedIndex = 0;

        }

        private void ResetTokDef1()
        {
            this._device.TZLDevice.TokDefensesMain = new MR500.MR500V2.CTokDefenses();
            this._tokDefenseGrid.Rows.Clear();
            for (int i = 0; i < 4; i++)
            {
                this._tokDefenseGrid.Rows.Add(new object[]
                  {
                        this._device.TZLDevice.TokDefensesMain[i].Name,
                        this._device.TZLDevice.TokDefensesMain[i].Mode1_11,
                        this._device.TZLDevice.TokDefensesMain[i].BlockingNumber,
                        this._device.TZLDevice.TokDefensesMain[i].Parameter,
                        this._device.TZLDevice.TokDefensesMain[i].WorkConstraint,
                        this._device.TZLDevice.TokDefensesMain[i].Feature,
                        this._device.TZLDevice.TokDefensesMain[i].WorkTime/10,
                        this._device.TZLDevice.TokDefensesMain[i].Speedup,
                        this._device.TZLDevice.TokDefensesMain[i].SpeedupTime,
                        this._device.TZLDevice.TokDefensesMain[i].Oscv1_11,
                        this._device.TZLDevice.TokDefensesMain[i].UROV,
                        this._device.TZLDevice.TokDefensesMain[i].APV,
                        this._device.TZLDevice.TokDefensesMain[i].AVR
                  });
                this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid, i);
            }
            for (int i = 0; i < this._tokDefenseGrid.Rows.Count; i++)
            {
                this._tokDefenseGrid[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            this._device.TZLDevice.TokDefensesReserve = new MR500.MR500V2.CTokDefenses();
        }

        // ����� I2, I0, In
        private void ResetTokDef2()
        {
            this._device.TZLDevice.TokDefensesMain = new MR500.MR500V2.CTokDefenses();
            this._tokDefenseGrid3.Rows.Clear();
            for (int i = 4; i < 10; i++)
            {
                this._tokDefenseGrid3.Rows.Add(new object[]
                {
                    this._device.TZLDevice.TokDefensesMain[i].Name,
                    this._device.TZLDevice.TokDefensesMain[i].Mode1_11,
                    this._device.TZLDevice.TokDefensesMain[i].BlockingNumber,
                    this._device.TZLDevice.TokDefensesMain[i].WorkConstraint,
                    this._device.TZLDevice.TokDefensesMain[i].WorkTime,
                    this._device.TZLDevice.TokDefensesMain[i].Speedup,
                    this._device.TZLDevice.TokDefensesMain[i].SpeedupTime,
                    this._device.TZLDevice.TokDefensesMain[i].Oscv1_11,
                    this._device.TZLDevice.TokDefensesMain[i].UROV,
                    this._device.TZLDevice.TokDefensesMain[i].APV,
                    this._device.TZLDevice.TokDefensesMain[i].AVR
                });
                this.ChangeTokDefenseCellDisabling1(this._tokDefenseGrid3, i - 4);
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            this._device.TZLDevice.TokDefensesReserve = new MR500.MR500V2.CTokDefenses();
        }

        //����� Ig, I2/I1
        private void ResetTokDef3()
        {
            this._device.TZLDevice.TokDefensesMain = new MR500.MR500V2.CTokDefenses();
            this._tokDefenseGrid4.Rows.Clear();

            const int Ig_index = 10;
            this._tokDefenseGrid4.Rows.Add(new object[]
            {
                 this._device.TZLDevice.TokDefensesMain[Ig_index].Name,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].Mode1_11,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].BlockingNumber,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].WorkConstraint,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].WorkTime,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].Speedup,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].SpeedupTime,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].Oscv1_11,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].UROV,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].APV,
                 this._device.TZLDevice.TokDefensesMain[Ig_index].AVR
            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 0);

            const int I12_index = 11;
            this._tokDefenseGrid4.Rows.Add(new object[]
            {
                 this._device.TZLDevice.TokDefensesMain[I12_index].Name,
                 this._device.TZLDevice.TokDefensesMain[I12_index].Mode1_11,
                 this._device.TZLDevice.TokDefensesMain[I12_index].BlockingNumber,
                 this._device.TZLDevice.TokDefensesMain[I12_index].WorkConstraint,
                 this._device.TZLDevice.TokDefensesMain[I12_index].WorkTime,
                 this._device.TZLDevice.TokDefensesMain[I12_index].Speedup,
                 this._device.TZLDevice.TokDefensesMain[I12_index].SpeedupTime,
                 this._device.TZLDevice.TokDefensesMain[I12_index].Oscv1_11,
                 this._device.TZLDevice.TokDefensesMain[I12_index].UROV,
                 this._device.TZLDevice.TokDefensesMain[I12_index].APV,
                 this._device.TZLDevice.TokDefensesMain[I12_index].AVR
            });
            this.ChangeTokDefenseCellDisabling3(this._tokDefenseGrid4, 1);

            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupCol", 1].Style.BackColor = SystemColors.Control;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].ReadOnly = true;
            this._tokDefenseGrid4["_tokDefense4SpeedupTimeCol", 1].Style.BackColor = SystemColors.Control;

            for (int i = 0; i < this._tokDefenseGrid4.Rows.Count; i++)
            {
                this._tokDefenseGrid4[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }

            this._device.TZLDevice.TokDefensesReserve = new MR500.MR500V2.CTokDefenses();
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoints(true);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
    }

}