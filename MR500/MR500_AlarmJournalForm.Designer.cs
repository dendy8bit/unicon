namespace BEMN.MR500
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._journalProgress = new System.Windows.Forms.ProgressBar();
            this._deserializeBut = new System.Windows.Forms.Button();
            this._serializeBut = new System.Windows.Forms.Button();
            this._readBut = new System.Windows.Forms.Button();
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InSignals1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InSignals2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._saveSysJouranlHtmlDlg = new System.Windows.Forms.SaveFileDialog();
            this._saveToHtml = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(112, 359);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(13, 13);
            this._journalCntLabel.TabIndex = 17;
            this._journalCntLabel.Text = "0";
            // 
            // _journalProgress
            // 
            this._journalProgress.Location = new System.Drawing.Point(6, 355);
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(100, 17);
            this._journalProgress.TabIndex = 16;
            // 
            // _deserializeBut
            // 
            this._deserializeBut.Location = new System.Drawing.Point(449, 323);
            this._deserializeBut.Name = "_deserializeBut";
            this._deserializeBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeBut.TabIndex = 15;
            this._deserializeBut.Text = "��������� �� �����";
            this._deserializeBut.UseVisualStyleBackColor = true;
            this._deserializeBut.Click += new System.EventHandler(this._deserializeBut_Click);
            // 
            // _serializeBut
            // 
            this._serializeBut.Location = new System.Drawing.Point(332, 323);
            this._serializeBut.Name = "_serializeBut";
            this._serializeBut.Size = new System.Drawing.Size(111, 23);
            this._serializeBut.TabIndex = 14;
            this._serializeBut.Text = "��������� � ����";
            this._serializeBut.UseVisualStyleBackColor = true;
            this._serializeBut.Click += new System.EventHandler(this._serializeBut_Click);
            // 
            // _readBut
            // 
            this._readBut.Location = new System.Drawing.Point(5, 323);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(75, 23);
            this._readBut.TabIndex = 13;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "(*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������  ������ ��� ��500";
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��500_������_������";
            this._saveSysJournalDlg.Filter = "(*.xml) | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������ ��� ��500";
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._I0Col,
            this._UabCol,
            this._UbcCol,
            this._UcaCol,
            this._U2Col,
            this._InSignals1,
            this._InSignals2});
            this._journalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(745, 303);
            this._journalGrid.TabIndex = 19;
            // 
            // _indexCol
            // 
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 71;
            // 
            // _codeCol
            // 
            this._codeCol.HeaderText = "��� �����������";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 93;
            // 
            // _typeCol
            // 
            this._typeCol.HeaderText = "��� �����������";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._typeCol.Width = 93;
            // 
            // _IaCol
            // 
            this._IaCol.HeaderText = "Ia{A}";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 37;
            // 
            // _IbCol
            // 
            this._IbCol.HeaderText = "Ib{A}";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 37;
            // 
            // _IcCol
            // 
            this._IcCol.HeaderText = "Ic{A}";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 37;
            // 
            // _I0Col
            // 
            this._I0Col.HeaderText = "Io{A}";
            this._I0Col.Name = "_I0Col";
            this._I0Col.ReadOnly = true;
            this._I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I0Col.Width = 37;
            // 
            // _UabCol
            // 
            this._UabCol.HeaderText = "Ig{A}";
            this._UabCol.Name = "_UabCol";
            this._UabCol.ReadOnly = true;
            this._UabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UabCol.Width = 37;
            // 
            // _UbcCol
            // 
            this._UbcCol.HeaderText = "I0{A}";
            this._UbcCol.Name = "_UbcCol";
            this._UbcCol.ReadOnly = true;
            this._UbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbcCol.Width = 37;
            // 
            // _UcaCol
            // 
            this._UcaCol.HeaderText = "I1{A}";
            this._UcaCol.Name = "_UcaCol";
            this._UcaCol.ReadOnly = true;
            this._UcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcaCol.Width = 37;
            // 
            // _U2Col
            // 
            this._U2Col.HeaderText = "I2{A}";
            this._U2Col.Name = "_U2Col";
            this._U2Col.ReadOnly = true;
            this._U2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U2Col.Width = 37;
            // 
            // _InSignals1
            // 
            this._InSignals1.HeaderText = "��.������� 1-8";
            this._InSignals1.Name = "_InSignals1";
            this._InSignals1.ReadOnly = true;
            this._InSignals1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InSignals1.Width = 80;
            // 
            // _InSignals2
            // 
            this._InSignals2.HeaderText = "��.������� 9-16";
            this._InSignals2.Name = "_InSignals2";
            this._InSignals2.ReadOnly = true;
            this._InSignals2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InSignals2.Width = 86;
            // 
            // _saveSysJouranlHtmlDlg
            // 
            this._saveSysJouranlHtmlDlg.DefaultExt = "xml";
            this._saveSysJouranlHtmlDlg.FileName = "MR500_������_������";
            this._saveSysJouranlHtmlDlg.Filter = "*.html | *.html";
            this._saveSysJouranlHtmlDlg.Title = "��������� ������ ������ ��� �� 500";
            // 
            // _saveToHtml
            // 
            this._saveToHtml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToHtml.Location = new System.Drawing.Point(581, 323);
            this._saveToHtml.Name = "_saveToHtml";
            this._saveToHtml.Size = new System.Drawing.Size(152, 23);
            this._saveToHtml.TabIndex = 20;
            this._saveToHtml.Text = "��������� � HTML";
            this._saveToHtml.UseVisualStyleBackColor = true;
            this._saveToHtml.Click += new System.EventHandler(this._saveToHtml_Click);
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 375);
            this.Controls.Add(this._saveToHtml);
            this.Controls.Add(this._journalGrid);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._journalProgress);
            this.Controls.Add(this._deserializeBut);
            this.Controls.Add(this._serializeBut);
            this.Controls.Add(this._readBut);
            this.MaximizeBox = false;
            this.Name = "AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.Activated += new System.EventHandler(this.AlarmJournalForm_Activated);
            this.Deactivate += new System.EventHandler(this.AlarmJournalForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmJournalForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.ProgressBar _journalProgress;
        private System.Windows.Forms.Button _deserializeBut;
        private System.Windows.Forms.Button _serializeBut;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InSignals2;
        private System.Windows.Forms.SaveFileDialog _saveSysJouranlHtmlDlg;
        private System.Windows.Forms.Button _saveToHtml;
    }
}