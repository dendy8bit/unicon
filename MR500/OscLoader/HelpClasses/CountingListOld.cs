﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR500.OscLoader.Structures;

namespace BEMN.MR500.OscLoader.HelpClasses
{
    public class CountingListOld
    {
        #region [Constants]
        /// <summary>
        /// Размер одного отсчёта(в словах)
        /// </summary>
        public const int COUNTING_SIZE = 4;
        /// <summary>
        /// Кол-во дискрет
        /// </summary>
        ///  public const int DISCRETS_COUNT = 16;
        /// <summary>
        /// Кол-во токов
        /// </summary>
        public const int CURRENTS_COUNT = 4;

        public static readonly string[] INames = { "Ia", "Ib", "Ic", "In" };
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Массив отсчётов разбитый на слова
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// Массив дискрет 1-16
        /// </summary>
        //private ushort[][] _discrets;
        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        private int _count;

        #region [Токи]
        /// <summary>
        /// Токи
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// Неприведённые значения токов
        /// </summary>
        private readonly short[][] _baseCurrents;

        /// <summary>
        /// Минимальный ток
        /// </summary>
        private double _minI;
        /// <summary>
        /// Максимальный ток
        /// </summary>
        private double _maxI;

        /// <summary>
        /// Минимальный ток
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// Максимальный ток
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        }
        #endregion [Токи]

        #region [Напряжения]
        /// <summary>
        /// Напряжения
        /// </summary>
        private double[][] _voltages;


        /// <summary>
        /// Минимальное напряжение
        /// </summary>
        private double _minU;
        /// <summary>
        /// Максимальное напряжение
        /// </summary>
        private double _maxU;
        /// <summary>
        /// Минимальное напряжение
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// Максимальное напряжение
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }
        #endregion [Напряжения]

        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        private int _alarm;
        private string _dateTime;

        private OscJournalStruct _oscJournalStruct;

        #endregion [Private fields]

        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }
        
        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        public string DateAndTime
        {
            get { return this._dateTime; }
        }

        /// <summary>
        /// Коэффициенты перещёта для токов
        /// </summary>
        private double[] CurrentsKoefs { get; set; }


        #region [Ctor's]
        public CountingListOld(ushort[] pageValue, OscJournalStruct oscJournalStruct, MeasureTransStruct measure)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.FaultTime;

            this._countingArray = new ushort[COUNTING_SIZE][];
            //Общее количество отсчётов
            this._count = pageValue.Length / COUNTING_SIZE;
            //Инициализация массива
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;

            for (int i = 0; i < this._count * COUNTING_SIZE; i++)
            {
                this._countingArray[n][m] = pageValue[i];
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();

                double koef = i == CURRENTS_COUNT - 1 ? measure.TTNP * 5 : measure.TT * 40;

                currentsKoefs[i] = koef * Math.Sqrt(2) / 32768;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;


        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// Превращает каждое слово в инвертированный массив бит(значения 0/1) 
        /// </summary>
        /// <param pinName="sourseArray">Массив массивов бит</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        #endregion [Help members]

        public void Save(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException();
            }

            string hdrPath = Path.ChangeExtension(filePath, "hdr");


            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("МР 500 {0} {1}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.SizeTime);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine(1251);
            }


            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP500,1");
                cgfFile.WriteLine("4,4A,0D");
                int index = 1;
                for (int i = 0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, INames[i],
                        this.CurrentsKoefs[i].ToString(format));
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._count);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public static CountingListOld Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty));

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[,] factors = new double[CURRENTS_COUNT, 2];

            Regex factorRegex = new Regex(@"\d+\,[I]\w+\,\,\,[A]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2, 0] = double.Parse(res, format);
                res = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                factors[i - 2, 1] = double.Parse(res, format);
            }

            int counts = int.Parse(cfgStrings[8].Replace("1000,", string.Empty));

            CountingListOld result = new CountingListOld(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");

            string[] datStrings = File.ReadAllLines(datPath);
            double[][] currents = new double[CURRENTS_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }
            
            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) * factors[j, 0] + factors[j, 1];
                }

            }
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());
            }
            result.Currents = currents;
            result.FilePath = filePath;
            result.IsLoad = true;
            result._dateTime = timeString;
            return result;


        }
        private CountingListOld(int count)
        {
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];

            this._count = count;
        }
    }
}
