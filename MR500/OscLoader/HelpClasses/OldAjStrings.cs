﻿using System.Collections.Generic;

namespace BEMN.MR500.OscLoader.HelpClasses
{
   public  class OldAjStrings
    {

        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                {
                    "Основная",
                    "Резервная"
                };
            }
        }
        /// <summary>
        /// Сработавшая ступень
        /// </summary>
        public static Dictionary<ushort, string> Stage
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {1, "ВЗ-1"},
                    {2,"ВЗ-2"},
                    {3,"ВЗ-3"},
                    {4,"ВЗ-4"},
                    {5,"ВЗ-5"},
                    {6,"ВЗ-6"},
                    {7,"ВЗ-7"},
                    {8,"ВЗ-8"},
                    {9,"I>"},
                    {10,"I>>"},
                    {11,"I>>>"},
                    {12,"I>>>>"},
                    { 13,"I0>"},
                    {14,"I0>>"},
                    {15,"I2>"},
                    {16, "I2>>"},
                    {17,"АЧР"},
                    {18,"АВР"},
                   
                };
            }
        }
        public static List<string> Message
        {
            get
            {
                return new List<string>
                {
                    "Журнал пуст",
                    "Сигнализация",
                    "Отключение",
                    "Блокировка",
                    "Неуспешное АПВ",
                };
            }
        }
        public static Dictionary<ushort, string> Parametr
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0,"B3"}, // 
                    {1,"Ig"},//
                    {2,"In"},// 
                    {3,"Ia"}, // 
                    {4,"Ib"}, // 
                    {5,"Ic"}, // 
                    {6,"I0"}, //
                    {7,"I1"}, // 
                    {8,"I2"}, // 
                };
            }
        }
    }
}
