﻿using System;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR500.OscLoader.Structures;


namespace BEMN.MR500.OscLoader.HelpClasses
{
    /// <summary>
    /// Загружает уставки токов(Iтт)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Private fields]
        private readonly MemoryEntity<MeasureTransStruct> _connections; 
        #endregion [Private fields]

        public MeasureTransStruct MeasureStruct
        {
            get { return this._connections.Value; }
        }

        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail; 
        #endregion [Events]


        #region [Ctor's]
        public CurrentOptionsLoader(Device device)
        {
            this._connections = new MemoryEntity<MeasureTransStruct>("Значения измерительного канала", device, 0x1100);
            this._connections.AllReadOk += _connections_AllReadOk;
            this._connections.AllReadFail += _connections_AllReadFail;
        }
        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        void _connections_AllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        void _connections_AllReadOk(object sender)
        {
           
            if (this.LoadOk != null)
            {
                this.LoadOk.Invoke();
            }
        }  
        #endregion [Memory Entity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запускает загрузку уставок
        /// </summary>
        public void StartRead()
        {
           this._connections.LoadStruct();
        } 
        #endregion [Public members]
    }
}
