﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR500.OscLoader.Structures;


namespace BEMN.MR500.OscLoader
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OldOscPageLoader
    {
        #region [Private fields]
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private MemoryEntity<ArrayStruct> _oscArray;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана один слот
        /// </summary>
        public event Action SlotReadSuccessful;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Осцилограмма не загружена
        /// </summary>
        public event Action OscReadFail;

        
        #endregion [Events]

       public  OldOscPageLoader(Device device)
        {
            ArrayStruct.FullSize = 16384;//длинна осцилоги
            this._oscArray = new MemoryEntity<ArrayStruct>("Отсчёты", device, 0x4000);
            this._oscArray.AllReadOk += o =>
            {
                this._oscArray.RemoveStructQueries();
                this.OscReadComplite();
            };
            this._oscArray.AllReadFail += o =>
            {
                this._oscArray.RemoveStructQueries();
                this.OnRaiseOscReadFail();
            };
            this._oscArray.ReadOk += OnRaiseSlotReadSuccessful;
        }

        #region [Public members]

        public int SlotCount 
        {
            get { return this._oscArray.Slots.Count; }
        }

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>

        public void StartRead(OscJournalStruct journalStruct)
        {
            this._journalStruct = journalStruct;
            this._oscArray.LoadStruct();
            
        }

        private void OnRaiseSlotReadSuccessful(object sender)
        {
            if (this.SlotReadSuccessful != null)
            {
                this.SlotReadSuccessful.Invoke();
            }
        }
        #endregion [Public members]


        #region [Event Raise Members]

        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }

        /// <summary>
        /// Вызывает событие "Осцилограмма не загружена"
        /// </summary>
        private void OnRaiseOscReadFail()
        {
            if (this.OscReadFail != null)
            {
                this.OscReadFail.Invoke();
            }
        }

        #endregion [Event Raise Members]


        #region [Help members]
        
        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            if (this._journalStruct.Version < 2.04)
            {
                int startOsc = this._journalStruct.StartOsc / 2; // в словах
                List<ushort> readMassiv = new List<ushort>(this._oscArray.Value.Data);
                List<ushort> result = new List<ushort>();
                result.AddRange(readMassiv.Skip(startOsc));
                result.AddRange(readMassiv.Take(startOsc));
                this.ResultArray = result.ToArray();
            }
            else
            {
                int a = this._journalStruct.Fault;
                int b = a + 2048 * 10 + 8; // в байтах
                if (b >= 32768)
                {
                    b = b - 32768;
                }
                b = b / 2;
                List<ushort> read = new List<ushort>(this._oscArray.Value.Data);
                List<ushort> result = new List<ushort>(read.Skip(b));
                result.AddRange(read.Take(b));
                this.ResultArray = result.ToArray();
            }
            this.OnRaiseOscReadSuccessful();
        }

        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
