﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR500.OscLoader.HelpClasses;

namespace BEMN.MR500.OscLoader.Structures
{
    public class OscJournalStruct : StructBase
    {
        #region [Constants]
        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}.{3:d2}";
        private const int OSC_SIZE = 0x8000;
        #endregion [Constants]

        [Layout(0)] private ushort _oscCount;
        [Layout(1)] private ushort _faultTime;
        [Layout(2)] private ushort _startOsc;
        [Layout(3)] private ushort _res1;
        [Layout(4, Count = 8)] private byte[] _dateTime;
        [Layout(5)] private ushort _stage; // 2 Код повреждения** 9
        [Layout(6)] private ushort _type; //3 Тип повреждения*** 10
        [Layout(7)] private ushort _value; //4 Значение повреждения //Знач. сраб пар 11
        [Layout(8)] private ushort _ia; 
        [Layout(9)] private ushort _ib; 
        [Layout(10)] private ushort _ic; 
        [Layout(11)] private ushort _io; 
        [Layout(12)] private ushort _ig; 
        [Layout(13)] private ushort _i0; 
        [Layout(14)] private ushort _i1; 
        [Layout(15)] private ushort _i2; 
        [Layout(16)] private ushort _discrets;
        private int _tt;
        private int _ttnp;

        public ushort Fault => (ushort)(this._faultTime + 32768);

        public ushort StartOsc => (ushort)(this._startOsc + 32768);

        public int FaultTime => this.StartOsc > this.Fault
            ? (this.Fault + (OSC_SIZE - this.StartOsc)) / 2 / this.CountingSize
            : (this.Fault - this.StartOsc) / 2 / this.CountingSize;

        public int CountingSize => this.Version > 2.03 ? 5 : 4;

        public double Version { get; set; }

        public int TT
        {
            get { return this._tt; }
            set { this._tt = value; }
        }

        public int TTNP
        {
            get { return this._ttnp; }
            set { this._ttnp = value; }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                int hour = Convert.ToInt32($"{this._dateTime[4]:X2}");
                int min = Convert.ToInt32($"{this._dateTime[5]:X2}");
                int sec = Convert.ToInt32($"{this._dateTime[6]:X2}");
                int msec = Convert.ToInt32($"{this._dateTime[7]:X2}");
                return $"{hour:D2}:{min:D2}:{sec:D2}.{msec:D3}";
            }
        }


        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                int year = Convert.ToInt32($"{this._dateTime[1]:X2}");
                int month = Convert.ToInt32($"{this._dateTime[2]:X2}");
                int day = Convert.ToInt32($"{this._dateTime[3]:X2}");
                return $"{day:D2}:{month:D2}:{year:D2}";
            }

        }

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                int year = Convert.ToInt32($"{this._dateTime[1]:X2}");
                int month = Convert.ToInt32($"{this._dateTime[2]:X2}");
                int day = Convert.ToInt32($"{this._dateTime[3]:X2}");
                int hour = Convert.ToInt32($"{this._dateTime[4]:X2}");
                int min = Convert.ToInt32($"{this._dateTime[5]:X2}");
                int sec = Convert.ToInt32($"{this._dateTime[6]:X2}");
                int msec = Convert.ToInt32($"{this._dateTime[7]:X2}");
                DateTime a = new DateTime(year, month, day, hour, min, sec, msec*10);
                DateTime result = a.AddMilliseconds(alarm);
                return
                    $"{result.Month:D2}/{result.Day:D2}/{result.Year:D2},{result.Hour:D2}:{result.Minute:D2}:{result.Second:D2}.{result.Millisecond:D3}";

            }
            catch (Exception)
            {
                return GetFormattedDateTime;
            }

        }

        public string GetFormattedDateTime
        {
            get
            {
                int year = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[1]));
                int month = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[2]));
                int day = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[3]));
                int hour = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[4]));
                int min = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[5]));
                int sec = Convert.ToInt32(string.Format("{0:X2}", this._dateTime[6]));
                int msec = Convert.ToInt32(string.Format("{0:X3}", this._dateTime[7]));
                return $"{month:D2}/{day:D2}/{year:D2},{hour:D2}:{min:D2}:{sec:D2}.{(msec < 100 ? msec * 10 : msec):D3}";
            }
        }
        public int OscCount
        {
            get
            {
                if (this._oscCount == 0)
                {
                    return this._oscCount;
                }

                if (this._oscCount == 1)
                {
                    return 1;
                }
                return 0;
            }
        }

        public int SizeTime
        {
            get { return 16384 / 5; }
        }
        public string Stage
        {
            get { return Validator.Get(_stage, OldAjStrings.Stage, 0, 1, 2, 3, 4, 5); }
        }

        public string GroupOfSetpoints()
        {

            return Validator.GetJornal(_stage, OldAjStrings.AlarmJournalSetpointsGroup, 8, 9, 10);
        }

        public string WorkParametr
        {
            get { return Validator.Get(this._type, OldAjStrings.Parametr, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        public string ValueParametr 
        {
            get { return ConvertI(this._value,this._tt,true); }
        }

        public string Ia
        {
            get { return ConvertI(this._ia, this._tt, true); }
        }
        public string Ib 
        {
            get { return ConvertI(this._ib, this._tt, true); }
        }
        public string Ic 
        {
            get { return ConvertI(this._ic, this._tt, true); }
        }
        public string Io
        {
            get { return ConvertI(this._io, this._ttnp, false); }
        }
        public string Ig
        {
            get { return ConvertI(this._ig, this._ttnp, false); }
        }
        public string I0
        {
            get { return ConvertI(this._i0,this._tt, true); }
        }
        public string I1
        {
            get { return ConvertI(this._i1, this._tt, true); }
        }
        public string I2
        {
            get { return ConvertI(this._i2, this._tt, true); }
        }

        public string ConvertI(double i,int KoeffNPTT,bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;

            double res= Math.Ceiling(b*i * KoeffNPTT/256)/256;
            return res.ToString("F");
        }
    }
}

