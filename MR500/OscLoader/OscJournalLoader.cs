﻿using System;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR500.OscLoader.Structures;


namespace BEMN.MR500.OscLoader
{
    /// <summary>
    /// Загружает журнал осцилограммы(СТАРЫЙ)
    /// </summary>
    public class OldOscJournalLoader
    {
        #region [Private fields]

        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;


        #endregion [Private fields]


        #region [Events]

        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action LoadOk;

        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action LoadFail;

        #endregion [Events]


        #region [Ctor's]

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param pinName="oscJournal">Объект записи журнала</param>
        /// <param pinName="refreshOscJournal">Объект сброса журнала</param>
        /// <param pinName="oscOptions">Объект параметров журнала</param>
        /// <param pinName="table">DataTable для формирования журнала</param>
        public OldOscJournalLoader(Device device)
        {

            this._oscJournal = new MemoryEntity<OscJournalStruct>("Чтение журнала осцилограммы", device, 0x3800);
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadOk);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);


        }

        #endregion [Ctor's]


        #region [Properties]


        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]

        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.LoadFail != null)
                this.LoadFail.Invoke();
        }

        private void ReadOk()
        {
            if (this.LoadOk != null)
                this.LoadOk.Invoke();
        }

        public void StartReadOscJournal()
        {
            this._oscJournal.LoadStruct();
        }

        public OscJournalStruct RecordStruct
        {
            get { return this._oscJournal.Value; }
        }

        #endregion [Private MemoryEntity Events Handlers]



    }
}

