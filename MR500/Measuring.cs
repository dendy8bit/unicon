using System;
using BEMN.MBServer;

namespace BEMN.MR500
{
    public class Measuring
    {
        
        public static double GetI(ushort value, ushort KoeffNPTT, bool phaseTok)
        {
            int b = phaseTok ? 40 : 5;
            return (double) b *(double) value / 0x10000 * KoeffNPTT;
        }

        public static double GetP(ushort value, ushort KoeffTTTN, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return ((double)b * (double)value / 0x10000);
        }

        public static double GetU(ushort value, double Koeff)
        {
            return(double) value/(double) 0x100* Koeff;
        }

        public static double GetF(ushort value)
        {
            return (double) value/(double) 0x100;
        }

        public static ulong GetTime(ushort value)
        {
            return value < 32768 ? (ulong) value*10 : (ulong) ((value - 32768)*100);
        }

        public static double GetConstraintOnly(ushort value, ConstraintKoefficient koeff)
        {
            double temp = ((double)value * (int)koeff / 65535) / 100;
            return Math.Round(temp, 2);
            //return Math.Floor(temp + 0.5) / 100;
        }

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff)
        {
            float temp = (float) value*(int) koeff/65535;
            return Math.Floor(temp + 0.5)/100;
        }

        public static string ConvertVersion(string value)
        {
            string ret = "";
            for (int i = 0; i < value.Length; i++)
            {
                if (value[i] == '.')
                {
                    ret += ',';
                }
                else
                {
                    if (char.IsNumber(value[i]))
                        ret += value[i];
                }
            }
            return ret;
        }


        public static bool SetConsGA
        {
            get { return _setConsGA; }
            set { _setConsGA = value; }
        }
        private static bool _setConsGA = false;
        public static double GetConstraint(ushort value, ConstraintKoefficient koeff, string version)
        {
            double ret = 0.0f;
            if (version == "123")
            {
                if (ConstraintKoefficient.K_Undefine == koeff)
                {
                    ret = GetConstraint(value, ConstraintKoefficient.K_25600, "123");

                    if (Common.HIBYTE(value) >= 0x80)
                    {
                        ret -= 0x80;
                        ret = ret > 1 ? Math.Floor(ret * 10 + 0.5) / 10 : Math.Floor(ret * 100 + 0.5) / 100;
                        ret *= 1000;
                    }
                }
                else
                {
                    if (_setConsGA)
                    {
                        double temp = (double)value * (int)koeff / 65535;
                        return Math.Round(temp, 7) / 100;
                    }
                    else
                    {
                        double temp = (double)value * (int)koeff / 65535;
                        ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
                    }
                }
                return ret;
            }

            string vers = "0";
            try
            {
                vers = ConvertVersion(version);
            }
            catch (Exception ll)
            {

            }
            if (vers == "")
            {
                vers = ConvertVersion("1.12");
            }



            if (ConstraintKoefficient.K_Undefine == koeff)
            {
                if ((Convert.ToDouble(vers) > 1.11) && (version != ""))
                {
                    ret = GetConstraint(value, ConstraintKoefficient.K_25600, "1.12");
                    if (Common.HIBYTE(value) >= 0x80)
                    {
                        ret -= 0x80;
                        ret = ret > 1 ? Math.Floor(ret * 10 + 0.5) / 10 : Math.Floor(ret * 100 + 0.5) / 100;
                        ret *= 1000;
                    }
                }
                else
                {
                    ret = GetConstraint(value, ConstraintKoefficient.K_25600, "1.11");
                }
            }
            else
            {
                double temp = (double)value * (int)koeff / 65535;
                ret = Math.Round(temp, 0, MidpointRounding.ToEven) / 100;
            }
            return ret;
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            if (value < 0)
            {
                throw new FormatException("������� �� ����� ���� ������ 0");
            }
            return (ushort) (value*65535*100/(int) koeff);
        }

        public static ushort SetTime(ulong value)
        {
            return value < 327680 ? (ushort) (value/10) : (ushort) (value/100 + 32768);
        }
    }
}