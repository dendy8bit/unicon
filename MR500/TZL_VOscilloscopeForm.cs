using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR500.AlarmJournal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace BEMN.MR500
{
    public partial class VOscilloscopeForm : Form, IFormView
    {
        private MR500 _deviceMR500;
        private int _recordIndex;
        private List<ushort[]> _oscilloscopeJournalList = new List<ushort[]>();
        private List<int[]> _avaryStartEndList = new List<int[]>();
        private List<int[]> _oscilloscopeStartEndList = new List<int[]>();
        private List<string[]> _oscilloscopeDateTimeList = new List<string[]>();
        private List<int> _avaryBeginList = new List<int>();
        private List<List<int>> _avaryBeginOnOscList = new List<List<int>>();
        private StartEndOscilloscope _osc;
        private List<StartEndOscilloscope> _oscilloscopeList = new List<StartEndOscilloscope>();
        private int _currentOscilloscope;
        private int _start;
        private int _end;
        private bool _readOsc = true;
        private FileStream _WriteDATFile;
        private FileStream _WriteCFGFile;
        private FileStream _WriteHDRFile;
        private FileStream _ReadDATFile;
        private FileStream _ReadCFGFile;
        private FileStream _ReadHDRFile;
        
        public VOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public VOscilloscopeForm(MR500 device)
        {
            this.InitializeComponent();
            this._deviceMR500 = device;

            this._deviceMR500.TZLDevice.OscJournalLoadOK += HandlerHelper.CreateHandler(this, this.ReadJournalRecord);
            this._deviceMR500.TZLDevice.OscilloscopeLoadOK += HandlerHelper.CreateHandler(this, this.OnLoadOsc);
            this._deviceMR500.TZLDevice.OscilloscopeSaveOK += HandlerHelper.CreateHandler(this, this.OscilloscopeSaveOK);
            this._deviceMR500.TZLDevice.OscilloscopeLoadFail += HandlerHelper.CreateHandler(this, this.OscilloscopeLoadFail);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(VOscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Properties.Resources.oscilloscope.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "�������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this.ReadJournal();
        }

        public void ReadJournal()
        {
            if (!this._deviceMR500.DeviceDlgInfo.IsConnectionMode) return;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscilloscopeCountLabel.Visible = false;
            this._oscilloscopeCountCB.Visible = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscilloscopeCountCB.Items.Clear();
            this._oscilloscopeJournalList.Clear();
            this._avaryStartEndList.Clear();
            this._avaryBeginOnOscList.Clear();
            this._recordIndex = 0;
            this._oscJournalDataGrid.Rows.Clear();
            this._deviceMR500.TZLDevice.SaveOscilloscopeJournal();
            this._oscStatus.Text = "���� ������ ������� ������������...";
            this._deviceMR500.TZLDevice.LoadOscilloscopeJournal(this._recordIndex);
        }

        public void ReadJournalRecord()
        {
            if (this._deviceMR500.TZLDevice.OscExist)
            {
                this._oscilloscopeJournalList.Add(this._deviceMR500.TZLDevice.OscJournal);
                this._deviceMR500.TZLDevice.OscJournalRecord = this._deviceMR500.TZLDevice.OscJournal;

                #region ����-�����

                string _datetime = this._deviceMR500.TZLDevice.OscDay + "." + this._deviceMR500.TZLDevice.OscMonth + "." + this._deviceMR500.TZLDevice.OscYear + ", " + this._deviceMR500.TZLDevice.OscHours + ":" + this._deviceMR500.TZLDevice.OscMinutes + ":" + this._deviceMR500.TZLDevice.OscSeconds + "." + this._deviceMR500.TZLDevice.OscMiliseconds;

                #endregion

                int length = this._deviceMR500.TZLDevice.VOscSize / 2;

                #region ����� ����� ��� ������� � �������������

                int _oscEnd = this._deviceMR500.TZLDevice.OscPoint + this._deviceMR500.TZLDevice.OscLen * (this._deviceMR500.TZLDevice.OscRez);
                string _oscEndSTR = string.Empty;
                string _oscEndSTRList = string.Empty;
                if (_oscEnd > length)
                {
                    _oscEndSTR = ((_oscEnd) / (this._deviceMR500.TZLDevice.OscRez)) + " [" +
                                 (((_oscEnd - length)) / (this._deviceMR500.TZLDevice.OscRez)) + "]";
                    _oscEndSTRList = (_oscEnd - length).ToString();
                }
                else
                {
                    _oscEndSTR = ((_oscEnd) /*/ (_deviceMR700.TZLDevice.OscRez)*/).ToString();
                    _oscEndSTRList = (_oscEnd).ToString();
                }

                #endregion

                this._avaryStartEndList.Add(new int[]
                    {Convert.ToInt32(this._deviceMR500.TZLDevice.OscPoint), Convert.ToInt32(_oscEndSTRList)});

                #region ������ -> ������

                string str = "";
                if (this._recordIndex == 0)
                {
                    str = " ����.";
                }
                this._oscilloscopeCountCB.Items.Add(this._recordIndex + 1);
                this._oscJournalDataGrid.Rows.Add(new object[]
                {
                    (this._recordIndex + 1) + /*" / [" + this._recordIndex +"] "+*/ str,
                    _datetime, this._deviceMR500.TZLDevice.OscReady, this._deviceMR500.TZLDevice.OscPoint /*/ (_deviceMR700.TZLDevice.OscRez))*/.ToString(),
                    _oscEndSTR, this._deviceMR500.TZLDevice.OscBegin /*/ (_deviceMR700.TZLDevice.OscRez))*/.ToString(), this._deviceMR500.TZLDevice.OscLen, this._deviceMR500.TZLDevice.OscAfter,
                    AjStrings.Stage[this._deviceMR500.TZLDevice.OscAlm], this._deviceMR500.TZLDevice.OscRez
                });

                #endregion

                this._recordIndex++;
                this._deviceMR500.TZLDevice.LoadOscilloscopeJournal(this._recordIndex);
            }

            else

            {
                this._oscStatus.Text = "������ ������� ������������ ���������...";
                this._oscilloscopeStartEndList.Clear();
                this._oscilloscopeDateTimeList.Clear();
                this._start = 0;
                this._end = 0;
                if (this._avaryStartEndList.Count != 0)
                {
                    this._oscReadButton.Enabled = true;
                    this._oscilloscopeCountLabel.Visible = true;
                    this._oscilloscopeCountCB.Visible = true;
                    this._oscJournalReadButton.Enabled = true;
                    this._oscLoadButton.Enabled = true;
                    for (int i = 0; i < this._avaryStartEndList.Count; i++)
                    {
                        this.PrepareOscArray(i);
                    }

                }
                else
                {
                    MessageBox.Show("������ ������������ ����.", "��������!");
                    this._oscStatus.Text = "������ ������������ ����.";
                    this._oscJournalReadButton.Enabled = true;
                    this._oscLoadButton.Enabled = true;
                }
            }
        }

        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.ReadJournal();
        }
        
        private void OscShow()
        {
            try
            {
                if (Validator.GetVersionFromRegistry())
                {
                    string fileName = Validator.CreateOscFileNameCfg($"��500 v{this._deviceMR500.DeviceVersion} �������������");
                    this.PrepareWriteComTradeFiles(fileName);
                    Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"), fileName);
                }
                else
                {
                    throw new Exception("���������� .NET Framework 4.0!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("���������� ������� ����������� ������������.\n" + ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        public void PrepareOscArray(int avaryIndex)
        {
            this._start = this._avaryStartEndList[avaryIndex][0];
            this._end = this._avaryStartEndList[avaryIndex][1];
            this._oscilloscopeStartEndList.Add(new int[]
            {
                this._start,
                this._end,
                Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[3].Value.ToString()),
                Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString()),
                Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()),
                Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()),
                Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[9].Value.ToString()),
            });
            this._oscilloscopeDateTimeList.Add(new string[]
            {
                this._oscJournalDataGrid.Rows[avaryIndex].Cells[1].Value.ToString(),
                this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString()
            });
            this._avaryBeginList.Add(Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()) - Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()));
            this._avaryBeginOnOscList.Add(this._avaryBeginList);
            this._avaryBeginList = new List<int>();
        }

        private void OscilloscopeSaveOK()
        {
            try
            {
                if (!this._osc.OscilloscopeLoaded)
                {
                    this._deviceMR500.TZLDevice.LoadOscilloscopePage(this._osc.Index);
                }
            }
            catch { }
        }

        private void OscilloscopeLoadFail()
        {
            if (MessageBox.Show("������������� ��������� � ��������!", "��������!", MessageBoxButtons.OK) == DialogResult.OK)
            {
                this.ShowOscButtonEnaibled();
            }
        }

        private void OnLoadOsc()
        {
            if (this._readOsc)
            {
                this._osc.ArrayToValues();

                if (this._osc.PageLoaded)
                {
                    this._osc.Index++;
                    this._deviceMR500.TZLDevice.SaveOscilloscopePage(this._osc.CurrentPage);
                    this._osc.PageLoaded = false;
                }

                if (this._osc.OscilloscopeLoaded)
                {
                    this._oscilloscopeList.Add(this._osc);
                    this._oscStatus.Text = "�������� ������������� ���������";
                    this._oscStatus.Text = "���� �������������� ������";
                    this._osc.InitArrays();
                    try
                    {
                        ushort[] temp = new ushort[this._osc.Values.Length];
                        int b = (this._osc.Len - this._osc.After) * this._osc.Rez;
                        int begin;
                        if (this._osc.Begin < this._osc.Point)
                        {
                            begin = this._osc.Begin + this._deviceMR500.TZLDevice.VOscSize / 2 - this._osc.Point;
                        }
                        else
                        {
                            begin = this._osc.Begin - this._osc.Point;
                        }
                        int start = begin - b;
                        if (start < 0)
                        {
                            start += this._osc.Len * this._osc.Rez;
                        }
                        Array.ConstrainedCopy(this._osc.Values, start, temp, 0, this._osc.Values.Length - start);
                        Array.ConstrainedCopy(this._osc.Values, 0, temp, this._osc.Values.Length - start, start);
                        Array.ConstrainedCopy(temp, 0, this._osc.Values, 0, this._osc.Values.Length);
                    }
                    catch
                    {
                        MessageBox.Show("������ ����������.");
                    }
                    for (int i = 0; i < this._osc.Values.Length; i += this._deviceMR500.TZLDevice.OscRez)
                    {
                        this._osc.CalculateOscParams(i);
                    }

                    this._osc.CalculateOscKoeffs();
                    
                    this._osc.TriggeredStage = AjStrings.Stage[this._deviceMR500.TZLDevice.OscAlm];


                    this._oscStatus.Text = "�������������� ������ ���������";
                    if (MessageBox.Show("������ ������������� ���������", "", MessageBoxButtons.OK) == DialogResult.OK)
                    {
                        this.ShowOscButtonEnaibled();
                    }
                }
                this.ProgressBarIncriment();
            }
        }


        private void ProgressBarClear()
        {
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Step = 124;
            this._oscProgressBar.Maximum = this._osc.Values.Length; // this._osc.OtschetLengs;
            this._oscProgressBar.Minimum = 0;
        }

        public void ProgressBarIncriment()
        {
            this._oscProgressBar.PerformStep();
        }

        public void ShowOscButtonEnaibled()
        {
            this._oscShowButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
        }

        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            this._oscShowButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._readOsc = true;
            this._oscilloscopeList.Clear();
            this._currentOscilloscope = this._oscilloscopeCountCB.SelectedIndex;
            this._deviceMR500.TZLDevice.LoadInputSignals();
            this._deviceMR500.TZLDevice.OscJournalRecord = this._oscilloscopeJournalList[this._currentOscilloscope];
            try
            {
                this._osc =
                    new StartEndOscilloscope(
                        this._oscilloscopeStartEndList[_currentOscilloscope][0],
                        this._oscilloscopeStartEndList[_currentOscilloscope][1],
                        this._oscilloscopeDateTimeList[_currentOscilloscope][0],
                        this._oscilloscopeDateTimeList[_currentOscilloscope][1],
                        this._oscilloscopeStartEndList[_currentOscilloscope][2],
                        this._oscilloscopeStartEndList[_currentOscilloscope][3],
                        this._oscilloscopeStartEndList[_currentOscilloscope][4],
                        this._oscilloscopeStartEndList[_currentOscilloscope][5],
                        this._oscilloscopeStartEndList[_currentOscilloscope][6],
                        this._deviceMR500);

                this._osc.PageLoaded = false;
                this._osc.OscilloscopeLoaded = false;
                this._oscProgressBar.Value = 0;
                int temp = 0;
                if (this._deviceMR500.TZLDevice.VOscPageSize % this._deviceMR500.TZLDevice.VEnableOscPageSize != 0)
                {
                    temp = 1;
                }
                this._oscProgressBar.Maximum = this._osc.PagesCount *
                                               (this._deviceMR500.TZLDevice.VOscPageSize / this._deviceMR500.TZLDevice.VEnableOscPageSize + temp);
                this._oscProgressBar.Step = 1;
                this._oscStatus.Text = "���� �������� �������������";
                this._deviceMR500.TZLDevice.SaveOscilloscopePage(this._osc.StartPage);
                this._stopReadOsc.Enabled = true;
                this._oscJournalReadButton.Enabled = false;
                this.ProgressBarClear();
            }
            catch
            {
            }
        }

        public class StartEndOscilloscope
        {
            #region ����

            private MR500 _deviceMR500;
            private int _point;
            public string TriggeredStage { get; set; }
            public int Point
            {
                get { return this._point; }
            }
            private int _begin;
            public int Begin
            {
                get { return this._begin; }
            }

            private int _len;

            public int Len
            {
                get { return this._len; }
            }

            private int _after;

            public int After
            {
                get { return this._after; }
            }

            private int _rez;

            public int Rez
            {
                get { return this._rez; }
            }

            private double[] _koeffs = new double[8];
            public double[] Koeffs
            {
                get
                {
                    return this._koeffs;
                }
                //set { _startPage = value; }
            }

            private string _time = "";
            public string Time
            {
                get
                {
                    return this._time;
                }
                set
                {
                    this._time = value;

                }
            }

            private string _avaryTime = "";
            public string AvaryTime
            {
                get
                {
                    return this._avaryTime;
                }
                //set { _startPage = value; }
            }
            //��������� ��������
            private int _currentPage;
            public int CurrentPage
            {
                get
                {
                    if (this._currentPage < this._deviceMR500.TZLDevice.VFullOscSize / this._deviceMR500.TZLDevice.VOscPageSize / 2 - 1)
                    {
                        this._currentPage++;
                    }
                    else
                    {
                        this._currentPage = 0;
                    }

                    return this._currentPage;
                }
                //set { _startPage = value; }
            }

            //������� �������
            private int _index;
            public int Index
            {
                get { return this._index; }
                set { this._index = value; }
            }

            //��������� ��������
            private int _startPage;
            public int StartPage
            {
                get { return this._startPage; }
                //set { _startPage = value; }
            }

            private int _startByteOnStartPage;

            public int ByteOnStartPage
            {
                get { return this._startByteOnStartPage; }
            }

            //�������� ��������
            private int _endPage;
            public int EndPage
            {
                get { return this._endPage; }
                //set { _endPage = value; }
            }

            private int _endByteOnEndPage;

            public int ByteOnEndPage
            {
                get { return this._endByteOnEndPage; }
            }

            private int _lastByteIndex;
            public int LastByteIndex
            {
                get { return this._lastByteIndex; }
            }

            private int _firstByteIndex;
            public int FirstByteIndex
            {
                get { return this._firstByteIndex; }
            }

            //����� �������
            private int _pagesCount;
            public int PagesCount
            {
                get { return this._pagesCount; }
                //set { _pagesCount = value; }
            }

            //�������� �������������
            private ushort[] _values;
            public ushort[] Values
            {
                get { return this._values; }
                //set { _values = value; }
            }

            //�������� ���������
            private bool _pageLoaded;
            public bool PageLoaded
            {
                get
                {
                    return this._pageLoaded;
                }
                set
                {
                    this._sourcePageIndex = 0;
                    this._pageLoaded = value;
                }
            }

            //������������� ���������
            private bool _oscilloscopeLoaded;
            public bool OscilloscopeLoaded
            {
                get
                {
                    return this._oscilloscopeLoaded;
                }
                set
                {
                    this._oscilloscopeLoaded = false;
                }
            }

            //������ ���������� ������������ �������� � ������ Values ������������ ����� �������
            private int _sourceIndex;

            //������ ���������� ������������ �������� � ������ Values ������������ ��������
            private int _sourcePageIndex;

            //������ �������
            private int _otschetLengs;

            private int _oscilloscopeLengs;
            public int OscilloscopeLengs
            {
                get
                {
                    return this._oscilloscopeLengs;
                }
            }

            #region ����
            #region ������� S1
            private string _S1PageIaName = "S1Ia";
            public string S1PageIaName
            {
                get { return this._S1PageIaName; }
            }

            private double[] _S1PageIaValues;
            public double[] S1PageIaValues
            {
                get { return this._S1PageIaValues; }
            }

            private string _S1PageIbName = "S1Ib";
            public string S1PageIbName
            {
                get { return this._S1PageIbName; }
            }

            private double[] _S1PageIbValues;
            public double[] S1PageIbValues
            {
                get { return this._S1PageIbValues; }
            }

            private string _S1PageIcName = "S1Ic";
            public string S1PageIcName
            {
                get { return this._S1PageIcName; }
            }

            private double[] _S1PageIcValues;
            public double[] S1PageIcValues
            {
                get { return this._S1PageIcValues; }
            }

            private string _S1PageInName = "S1In";
            public string S1PageInName
            {
                get { return this._S1PageInName; }
            }

            private double[] _S1PageInValues;
            public double[] S1PageInValues
            {
                get { return this._S1PageInValues; }
            }
            #endregion
            #endregion

            #region ��������
            private List<BitArray> _diskretData;
            public List<BitArray> DiskretData
            {
                get { return this._diskretData; }
            }
            #endregion

            #endregion

            public string ConvertOscTimeToComTradeTime(string Time)
            {
                try
                {
                    string _prepareTime = Time;
                    try

                    {
                        _prepareTime = Time.Split(' ')[0] + Time.Split(' ')[1];
                    }
                    catch { }

                    string[] _oscTime = _prepareTime.Split('-', ',', ':', '.', '/');

                    return _oscTime[1] + "/" + _oscTime[0] + "/"
                        + _oscTime[2] + "," + _oscTime[3] + ":" + _oscTime[4] + ":"
                        + _oscTime[5] + "." + this.ConvertToComTradeTimeValue(_oscTime[6]);
                }
                catch { return "ERROR!"; }
            }

            public string ConvertToComTradeTimeValue(string val)
            {
                if (val.Length < 2)
                {
                    return "0" + val;
                }
                else
                {
                    if (val.Length == 2)
                    {
                        return val + "0";
                    }
                    return val;
                }
            }

            public string ConvertOscTimeToAvaryTime(string Time, int AvaryTime)
            {
                try
                {
                    string _prepareTime = Time;

                    _prepareTime = Time.Split(' ')[0] + Time.Split(' ')[1];

                    string[] _oscTime = _prepareTime.Split('-', ',', ':', '.');

                    DateTime a = new DateTime(Convert.ToInt32(_oscTime[2]), Convert.ToInt32(_oscTime[1]), Convert.ToInt32(_oscTime[0]),
                        Convert.ToInt32(_oscTime[3]), Convert.ToInt32(_oscTime[4]), Convert.ToInt32(_oscTime[5]), Convert.ToInt32(_oscTime[6]));
                    DateTime result = a.AddMilliseconds(AvaryTime);
                    return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                        result.Month,
                        result.Day,
                        result.Year,
                        result.Hour,
                        result.Minute,
                        result.Second,
                        result.Millisecond);
                }
                catch (Exception)
                {

                    return Time;
                }

            }

            public StartEndOscilloscope(int Start, int End, string Time, string AvaryTime, int point, int begin, int len, int after, int rez, MR500 device)
            {
                this._deviceMR500 = device;
                this._otschetLengs = this._deviceMR500.TZLDevice.OscRez;
                this._point = point;
                this._begin = begin;
                this._len = len;
                this._after = after;
                this._rez = rez;
                this._time = this.ConvertOscTimeToComTradeTime(Time);

                this._avaryTime = this.ConvertOscTimeToAvaryTime(Time, (len - after));

                this._startPage = this.CalculateStartPage(Start, out this._startByteOnStartPage);
                this._endPage = this.CalculateEndPage(End, out this._endByteOnEndPage);
                this._currentPage = this._startPage;

                if (this._endPage > this._startPage)
                {
                    this._pagesCount = this._endPage - this._startPage + 1;
                }
                else
                {
                    this._pagesCount = this._endPage + (this._deviceMR500.TZLDevice.VFullOscSize / this._deviceMR500.TZLDevice.VOscPageSize / 2 - this._startPage) + 1;
                }

                this._values = new ushort[this._pagesCount * this._deviceMR500.TZLDevice.VOscPageSize];

                this._firstByteIndex = this._startByteOnStartPage;
                this._lastByteIndex = this._values.Length - (this._deviceMR500.TZLDevice.VOscPageSize - this._endByteOnEndPage) /*+ 12*/;

            }

            public StartEndOscilloscope(List<string[]> datStrings, List<string> cfgStrings, MR500 device)
            {
                this._deviceMR500 = device;
                    
                string[] strings = cfgStrings[4].Split(',');
                this.Koeffs[0] = Convert.ToDouble(strings[5], CultureInfo.InvariantCulture);

                string[] stringsIn = cfgStrings[5].Split(',');
                this.Koeffs[1] = Convert.ToDouble(stringsIn[5], CultureInfo.InvariantCulture);

                this._avaryTime = cfgStrings[26];
                this._time = cfgStrings[25];

                this._values = new ushort[datStrings.Count];

                this._firstByteIndex = 0;
                this._lastByteIndex = datStrings.Count;
                this._otschetLengs = 1;

                this.InitArrays();
                for (int i = 0; i < datStrings.Count; i++)
                {
                    this.CalculateOscParamsFromFile(i, datStrings);
                }
            }

            private int CalculateStartPage(int Start, out int StartByteOnStartPage)
            {
                int res = 0;
                res = Start / this._deviceMR500.TZLDevice.VOscPageSize;
                StartByteOnStartPage = Start % this._deviceMR500.TZLDevice.VOscPageSize;
                return res;
            }

            private int CalculateEndPage(int End, out int EndByteOnEndPage)
            {
                int res = 0;
                res = End / this._deviceMR500.TZLDevice.VOscPageSize;
                EndByteOnEndPage = End % this._deviceMR500.TZLDevice.VOscPageSize;
                return res;
            }

            public void ArrayToValues()
            {
                try
                {
                    if (this._sourcePageIndex + this._deviceMR500.TZLDevice.VOscilloscope.Length >= 1024)
                    {
                        if (this._currentPage < this._deviceMR500.TZLDevice.VFullOscSize / this._deviceMR500.TZLDevice.VOscPageSize / 2 - 1)
                        {
                            this._pageLoaded = true;
                            Array.ConstrainedCopy(this._deviceMR500.TZLDevice.VOscilloscope, 0, this._values, this._sourceIndex, this._deviceMR500.TZLDevice.VOscilloscope.Length);
                            this._sourceIndex += this._deviceMR500.TZLDevice.VOscilloscope.Length;
                        }
                        else
                        {
                            this._pageLoaded = true;
                            Array.ConstrainedCopy(this._deviceMR500.TZLDevice.VOscilloscope, 0, this._values, this._sourceIndex, this._deviceMR500.TZLDevice.VOscilloscope.Length - 4);
                            this._sourceIndex += (this._deviceMR500.TZLDevice.VOscilloscope.Length - 3);
                            if (this._endPage != this._currentPage)
                            {
                                this._lastByteIndex -= 3;
                            }
                        }
                        if ((this._currentPage >= this._endPage) && (this._deviceMR500.TZLDevice.VOscSize / this._deviceMR500.TZLDevice.VOscPageSize - this._currentPage >= this._deviceMR500.TZLDevice.VOscSize / this._deviceMR500.TZLDevice.VOscPageSize - this._endPage))
                        {
                            //InitArrays(); 
                            this._oscilloscopeLoaded = true;
                            this._pageLoaded = false;
                        }
                    }
                    else
                    {
                        Array.ConstrainedCopy(this._deviceMR500.TZLDevice.VOscilloscope, 0, this._values, this._sourceIndex, this._deviceMR500.TZLDevice.VOscilloscope.Length);
                        this._sourceIndex += this._deviceMR500.TZLDevice.VOscilloscope.Length;
                        this._sourcePageIndex += this._deviceMR500.TZLDevice.VOscilloscope.Length;
                    }
                }
                catch { }
            }

            public void InitArrays()
            {
                this._oscilloscopeLengs = (this._lastByteIndex - this._firstByteIndex) / this._otschetLengs;

                ushort[] temp = new ushort[this._lastByteIndex - this._firstByteIndex];
                Array.ConstrainedCopy(this._values, this._firstByteIndex, temp, 0, this._lastByteIndex - this._firstByteIndex);
                this._values = new ushort[this._lastByteIndex - this._firstByteIndex];
                this._values = temp;


                this._S1PageIaValues = new double[this._oscilloscopeLengs];
                this._S1PageIbValues = new double[this._oscilloscopeLengs];
                this._S1PageIcValues = new double[this._oscilloscopeLengs];
                this._S1PageInValues = new double[this._oscilloscopeLengs];

                this._diskretData = new List<BitArray>();

                for (int i = 0; i < 16; i++)
                {
                    this._diskretData.Add(new BitArray(this._oscilloscopeLengs));
                }
            }

            public void CalculateOscKoeffs()
            {
                try
                {
                    this._koeffs[0] = this._deviceMR500.TZLDevice.TT * 40 * Math.Sqrt(2) / 32768;
                    this._koeffs[1] = this._deviceMR500.TZLDevice.TTNP * 5 * Math.Sqrt(2) / 32768;
                    this._koeffs[2] = /*_deviceMR700.TZLDevice.TT_L1*/100 * 40 * Math.Sqrt(2) / 32768;
                    this._koeffs[3] = /*_deviceMR700.TZLDevice.TT_L1*/100 * 40 * Math.Sqrt(2) / 32768;
                    this._koeffs[4] = /*_deviceMR700.TZLDevice.TT_L1*/100 * 40 * Math.Sqrt(2) / 32768;
                    this._koeffs[5] = /*_deviceMR700.TZLDevice.TT_L1*/100 * 40 * Math.Sqrt(2) / 32768;
                    this._koeffs[6] = /*_deviceMR700.TZLDevice.TT_L1*/100 * /*_deviceMR700.TZLDevice.TT_L1*/100 * 40 * Math.Sqrt(2) / 32768;
                    this._koeffs[7] = /*_deviceMR700.TZLDevice.TT_L1*/100 * /*_deviceMR700.TZLDevice.TT_L1*/100 * 40 * Math.Sqrt(2) / 32768;
                }
                catch { }
            }

            public void CalculateOscParamsFromFile(int otschetNum, List<string[]> Values)
            {
                try
                {
                    this._S1PageIaValues[otschetNum] = Convert.ToDouble(Values[otschetNum][0], CultureInfo.InvariantCulture);
                    this._S1PageIbValues[otschetNum] = Convert.ToDouble(Values[otschetNum][1], CultureInfo.InvariantCulture);
                    this._S1PageIcValues[otschetNum] = Convert.ToDouble(Values[otschetNum][2], CultureInfo.InvariantCulture);
                    this._S1PageInValues[otschetNum] = Convert.ToDouble(Values[otschetNum][3], CultureInfo.InvariantCulture);

                    for (int i = 0; i < 16; i++)
                    {
                        this._diskretData[i][otschetNum] = ("0" != (Values[otschetNum][4 + i]));
                    }
                }
                catch (Exception ee)
                {
                    int f = 5;
                }
            }

            public void CalculateOscParams(int OtschetNum)
            {
                try
                {
                    double kTT = this._deviceMR500.TZLDevice.TT;
                    double kTTNP = this._deviceMR500.TZLDevice.TTNP;
                    double KoefI = kTT / kTTNP * 8.0;
                    double KoefShkaliI = 40 * kTT * 1.41;

                    //double value = BitConverter.ToUInt16(data, i * _KoefZashiti + 0);((value - 32768) / 32768) * 40 * kTT * 1.41;
                    this._S1PageIaValues[OtschetNum / this._otschetLengs] = ((((double)this._values[0 + OtschetNum]) - 32768));
                    this._S1PageIbValues[OtschetNum / this._otschetLengs] = ((((double)this._values[1 + OtschetNum]) - 32768));
                    this._S1PageIcValues[OtschetNum / this._otschetLengs] = ((((double)this._values[2 + OtschetNum]) - 32768));
                    this._S1PageInValues[OtschetNum / this._otschetLengs] = ((((double)this._values[3 + OtschetNum]) - 32768));

                    //((value - 32768) / 32768) * 256 * ukTN * 1.41;

                    byte highWord = Common.LOBYTE(this._values[4 + OtschetNum]);
                    for (int j = 0; j < 8; j++)
                    {
                        this._diskretData[j][OtschetNum / this._otschetLengs] = 0 != (highWord >> j & 1);
                    }

                    byte lowWord = Common.HIBYTE(this._values[4 + OtschetNum]);
                    for (int j = 0; j < 8; j++)
                    {
                        this._diskretData[j + 8][OtschetNum / this._otschetLengs] = 0 != (lowWord >> j & 1);
                    }


                }
                catch (Exception ee)
                {
                    int f = 5;
                }
            }
        }

        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._osc.InitArrays();
            for (int i = this._osc.FirstByteIndex; i < this._osc.LastByteIndex; i += this._deviceMR500.TZLDevice.OscRez)
            {
                this._osc.CalculateOscParams(i);
            }
            this._osc.CalculateOscKoeffs();
            this._readOsc = false;
            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
        }

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._readOsc = false;
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo("TempOsc");
                foreach (FileInfo file in dirInfo.GetFiles())
                {
                    file.Delete();
                }
            }
            catch (Exception exc)
            {
            }
        }

        public void ShowSaveWindow()
        {
            IDeviceView idevice = this._deviceMR500;
            this._saveOscilloscopeDlg.FileName = idevice.NodeName + "_�������������";
            this._saveOscilloscopeDlg.Title = "��������� ������������� " + idevice.NodeName;
            if (DialogResult.OK == this._saveOscilloscopeDlg.ShowDialog())
            {
                if (this.PrepareWriteComTradeFiles(this._saveOscilloscopeDlg.FileName))
                {
                    MessageBox.Show("����� ComTrade ������� ���������.");
                }
            }
        }

        public void ShowLoadWindow()
        {
            IDeviceView idevice = this._deviceMR500;
            this._openOscilloscopeDlg.FileName = idevice.NodeName + "_�������������";
            this._openOscilloscopeDlg.Filter = "(*.hdr) | *.hdr";
            this._openOscilloscopeDlg.Title = "������� ������������� " + idevice.NodeName;
            if (DialogResult.OK == this._openOscilloscopeDlg.ShowDialog())
            {
                this.PrepareReadComTradeFiles(this._openOscilloscopeDlg.FileName);
            }
        }

        public bool SaveComTradeFiles()
        {
            return this.SaveHDR() && this.SaveCFG() && this.SaveDAT();
        }

        public string ConvertToComtradeIndex1(string val)
        {
            if (val.Length < 6)
            {
                string nul = "";
                for (int i = 0; i < 6 - val.Length; i++)
                {
                    nul += "0";
                }
                val = nul + val;
            }
            return val;
        }

        public string ConvertToComtradeIndex2(string val)
        {
            if (val.Length < 8)
            {
                string nul = "";
                for (int i = 0; i < 8 - val.Length; i++)
                {
                    nul += "0";
                }
                val = nul + val;
            }
            return val;
        }

        public bool SaveDAT()
        {
            bool res = true;
            StreamWriter _strimDAT = new StreamWriter(this._WriteDATFile);
            try
            {
                for (int i = 0; i < this._osc.OscilloscopeLengs; i++)
                {
                    _strimDAT.Write(this.ConvertToComtradeIndex1((i + 1).ToString()) + ",");
                    //_strimDAT.Write(this.ConvertToComtradeIndex2((i + 1) + "000"));
                    _strimDAT.Write((i + 1) + "000");


                    _strimDAT.Write("," + (this._osc.S1PageIaValues[i]).ToString(CultureInfo.InvariantCulture));
                    _strimDAT.Write("," + (this._osc.S1PageIbValues[i]).ToString(CultureInfo.InvariantCulture));
                    _strimDAT.Write("," + (this._osc.S1PageIcValues[i]).ToString(CultureInfo.InvariantCulture));
                    _strimDAT.Write("," + (this._osc.S1PageInValues[i]).ToString(CultureInfo.InvariantCulture));

                    for (int j = 0; j < this._osc.DiskretData.Count; j++)
                    {
                        try
                        {
                            //��������� ������ ����������. ������� � 3�� ���������
                            if (!_osc.DiskretData[j][i] && _osc.DiskretData[j][i - 8] && _osc.DiskretData[j][i + 8])
                            {
                                _strimDAT.Write(",1");
                                continue;
                            }
                        }
                        catch { }

                        if (this._osc.DiskretData[j][i])
                            _strimDAT.Write(",1");
                        else
                            _strimDAT.Write(",0");
                    }
                    _strimDAT.Write("\r\n");
                }
            }
            catch
            {
                MessageBox.Show("���� " + this._WriteDATFile + " �� ��������!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                res = false;
            }
            finally
            {
                _strimDAT.Close();
            }
            return res;
        }

        public bool SaveCFG()
        {
            bool res = true;
            StreamWriter _strimCFG = new StreamWriter(this._WriteCFGFile);
            try
            {
                _strimCFG.Write(this._deviceMR500.NodeName + "," + this._deviceMR500.DeviceNumber + "," + DateTime.Today.Year + "\r\n");
                _strimCFG.Write(20 + "," + 4 + "A," + 16 + "D" + "\r\n");
                _strimCFG.Write("1,Ia,a,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[0]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("2,Ib,b,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[0]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("3,Ic,c,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[0]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("4,In,n,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[1]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("5,D1,0" + "\r\n");
                _strimCFG.Write("6,D2,0" + "\r\n");
                _strimCFG.Write("7,D3,0" + "\r\n");
                _strimCFG.Write("8,D4,0" + "\r\n");
                _strimCFG.Write("9,D5,0" + "\r\n");
                _strimCFG.Write("10,D6,0" + "\r\n");
                _strimCFG.Write("11,D7,0" + "\r\n");
                _strimCFG.Write("12,D8,0" + "\r\n");
                _strimCFG.Write("13,D9,0" + "\r\n");
                _strimCFG.Write("14,D10,0" + "\r\n");
                _strimCFG.Write("15,D11,0" + "\r\n");
                _strimCFG.Write("16,D12,0" + "\r\n");
                _strimCFG.Write("17,D13,0" + "\r\n");
                _strimCFG.Write("18,D14,0" + "\r\n");
                _strimCFG.Write("19,D15,0" + "\r\n");
                _strimCFG.Write("20,D16,0" + "\r\n");
                _strimCFG.Write("50\r\n");
                _strimCFG.Write("1\r\n");
                _strimCFG.Write("1000," + (this._osc.OscilloscopeLengs - 1) + "\r\n");
                _strimCFG.Write(this._osc.Time + "\r\n");
                _strimCFG.Write(this._osc.AvaryTime + "\r\n");
                _strimCFG.Write("ASCII\r\n");
            }
            catch
            {
                MessageBox.Show("���� " + this._WriteCFGFile + " �� ��������!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                res = false;
            }
            finally
            {
                _strimCFG.Close();
            }
            return res;
        }

        public bool SaveHDR()
        {
            bool res = true;
            StreamWriter _strimHDR = new StreamWriter(this._WriteHDRFile);
            try
            {
                _strimHDR.Write("����� ������������ : " + this._osc.Time + ", �������: " + this._osc.TriggeredStage +/* AjStrings.Stage[_deviceMR500.TZLDevice.OscAlm] +*/ " \r\n");
                _strimHDR.Write("������ ������������� = " + this._osc.OscilloscopeLengs + "\r\n");
                _strimHDR.Write("K�� = " + "\r\n");
                _strimHDR.Write("K���� = " + "\r\n");
                _strimHDR.Write("K�� = " + "\r\n");
                _strimHDR.Write("K���� = " + "\r\n");
                for (int i = 0; i < this._avaryBeginOnOscList[this._currentOscilloscope].Count; i++)
                {
                    if (i != this._avaryBeginOnOscList[this._currentOscilloscope].Count - 1)
                    {
                        _strimHDR.Write(this._avaryBeginOnOscList[this._currentOscilloscope][i] + ",");
                    }
                    else
                    {
                        _strimHDR.Write(this._avaryBeginOnOscList[this._currentOscilloscope][i] + "\r\n");
                    }
                }
                _strimHDR.Write("BEMN" + "\r\n");
            }
            catch
            {
                MessageBox.Show("���� " + this._WriteHDRFile + " �� ��������!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                res = false;
            }
            finally
            {
                _strimHDR.Close();
            }
            return res;
        }

        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this.ShowSaveWindow();
        }

        private string ConvertCommaToPoint(double val)
        {
            try
            {
                string r = "";
                if (val.ToString("0.000000").Contains(","))
                {
                    string[] res = val.ToString("0.000000").Split(',');
                    r = res[0] + "." + res[1];
                }
                else
                {
                    r = val.ToString("0.000000");
                }

                return r;
            }
            catch { return "ERROR!"; }
        }

        private bool PrepareWriteComTradeFiles(string path)
        {
            try
            {
                this._WriteDATFile = new FileStream(Path.ChangeExtension(path, "dat"), FileMode.CreateNew);//new FileStream(path + ".dat", FileMode.CreateNew);
                this._WriteCFGFile = new FileStream(Path.ChangeExtension(path, "cfg"), FileMode.CreateNew);//new FileStream(path + ".cfg", FileMode.CreateNew);
                this._WriteHDRFile = new FileStream(Path.ChangeExtension(path, "hdr"), FileMode.CreateNew);//new FileStream(path + ".hdr", FileMode.CreateNew);

                return this.SaveComTradeFiles();
            }
            catch
            {
                //if (MessageBox.Show("���� " + path + " ��� ����������.\n\n��������?", "WARNING!!!",
                //        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                //{
                this._WriteDATFile = new FileStream(Path.ChangeExtension(path, "dat"), FileMode.Create);
                this._WriteCFGFile = new FileStream(Path.ChangeExtension(path, "cfg"), FileMode.Create);
                this._WriteHDRFile = new FileStream(Path.ChangeExtension(path, "hdr"), FileMode.Create);

                return this.SaveComTradeFiles();
                //}
                //else
                //{
                //    this.ShowSaveWindow();
                //}
            }
            finally
            {
                this._WriteDATFile?.Close();
                this._WriteCFGFile?.Close();
                this._WriteHDRFile?.Close();
            }
            return false;
        }

        private void PrepareReadComTradeFiles(string path)
        {
            try
            {

                this._avaryBeginOnOscList.Clear();
                path = Path.ChangeExtension(path, null);
                this._ReadDATFile = new FileStream(path + ".dat", FileMode.Open);
                this._ReadCFGFile = new FileStream(path + ".cfg", FileMode.Open);
                this._ReadHDRFile = new FileStream(path + ".hdr", FileMode.Open);

                List<string[]> datFileLines = new List<string[]>();
                List<string> cfgFileLines = new List<string>();
                List<string> hdrFileLines = new List<string>();

                string[] _avaryList = new string[] { "0" };
                string _msg = string.Empty;

                bool res = this.PrepareAllFiles(out _avaryList, out datFileLines, ref cfgFileLines, out _msg, ref hdrFileLines);

                if (res)
                {

                    this._osc = new StartEndOscilloscope(datFileLines, cfgFileLines, this._deviceMR500);

                    this._currentOscilloscope = 0;
                    List<int> _avaryListInt = new List<int>();
                    for (int i = 0; i < _avaryList.Length; i++)
                    {
                        _avaryListInt.Add(Convert.ToInt32(_avaryList[i]));
                    }
                    this._avaryBeginOnOscList.Add(_avaryListInt);
                    MessageBox.Show("������������� ���������", "");
                    this._oscShowButton.Enabled = true;

                    string[] firstStrings = hdrFileLines[0].Split(',');

                    this._osc.TriggeredStage = firstStrings[2].Replace("�������: ", "");
                    this._osc.Time = (firstStrings[0] + "," + firstStrings[1]).Replace("����� ������������ : ", "");
                }
                else
                {
                    MessageBox.Show(_msg, "������");
                }
            }
            catch
            {

            }
            finally
            {
                if (this._ReadDATFile != null)
                {
                    this._ReadDATFile.Close();
                }
                if (this._ReadCFGFile != null)
                {
                    this._ReadCFGFile.Close();
                }
                if (this._ReadHDRFile != null)
                {
                    this._ReadHDRFile.Close();
                }
            }
        }

        private bool PrepareAllFiles(out string[] _avaryList, out List<string[]> datFileLines, ref List<string> cfgFileLines, out string _msg, ref List<string> hdrFileLines)
        {
            bool res = true;
            _avaryList = null;
            datFileLines = null;
            _msg = string.Empty;

            try
            {
                if (this.ParseHDR(out _avaryList, out _msg, ref hdrFileLines))
                {
                    if (_msg != string.Empty)
                    {
                        MessageBox.Show(_msg, "��������!");
                    }
                    res = this.ParseCFG(out _msg, ref cfgFileLines);
                    res = this.ParseDAT(out datFileLines, out _msg);
                }
                else { res = false; }
            }
            catch { res = false; }
            return res;
        }

        private bool ParseHDR(out string[] _avaryList, out string _msg, ref List<string> hdrFileLines)
        {
            bool _parsed = true;
            StreamReader _strimReadHDR = null;
            _avaryList = null;
            _msg = string.Empty;

            try
            {
                _strimReadHDR = new StreamReader(this._ReadHDRFile);
                bool _endHdr = false;

                do
                {
                    string _readHdrLine = _strimReadHDR.ReadLine();
                    if (_readHdrLine == null)
                    {
                        _endHdr = true;
                    }
                    else
                    {
                        hdrFileLines.Add(_readHdrLine);
                    }
                } while (!_endHdr);

                if (hdrFileLines[7] != "BEMN")
                {
                    _parsed = false;
                    _avaryList = new string[] { "0" };
                    _msg = "��� ������������� �� �������������� ������'��.";
                }

                if (hdrFileLines.Count >= 8 && _parsed)
                {
                    _avaryList = hdrFileLines[6].Split(',');
                }

                for (int i = 0; i < _avaryList.Length; i++)
                {
                    try
                    {
                        int _temp = Convert.ToInt32(_avaryList[i]);
                    }
                    catch
                    {
                        _msg = "������ ������ ������������� �� ��������!";
                        _avaryList = new string[] { "0" };
                    }
                }
            }
            catch
            {
                _parsed = false;
                _avaryList = new string[] { "0" };
                _msg = "���� .cfg ��������� � �� ����� ���� ������.";
            }
            finally
            {
                if (_strimReadHDR != null)
                {
                    _strimReadHDR.Close();
                }
            }

            return _parsed;
        }

        private bool ParseDAT(out List<string[]> _datFileLines, out string _msg)
        {
            bool _parsed = true;
            StreamReader _strimReadDAT = null;
            _msg = string.Empty;
            _datFileLines = null;

            try
            {
                _strimReadDAT = new StreamReader(this._ReadDATFile);
                _datFileLines = new List<string[]>();
                bool _endDat = false;
                do
                {
                    string _startMas = _strimReadDAT.ReadLine();
                    if (_startMas == null)
                    {
                        _endDat = true;
                    }
                    else
                    {
                        string[] _firstMas = _startMas.Split(',');
                        string[] _clearMas = new string[_firstMas.Length - 2];
                        Array.Copy(_firstMas, 2, _clearMas, 0, _clearMas.Length);
                        _datFileLines.Add(_clearMas);
                    }
                } while (!_endDat);
            }
            catch
            {
                _parsed = false;
                _msg = "���� .dat ��������� � �� ����� ���� ������.";
            }
            finally
            {
                if (_strimReadDAT != null)
                {
                    _strimReadDAT.Close();
                }
            }
            return _parsed;
        }

        private bool ParseCFG(out string _msg, ref List<string> cfgFileLines)
        {
            bool _parsed = true;
            StreamReader _strimReadCFG = null;
            _msg = string.Empty;
            try
            {
                _strimReadCFG = new StreamReader(this._ReadCFGFile);

                bool _endCfg = false;
                do
                {
                    string _startMas = _strimReadCFG.ReadLine();
                    if (_startMas == null)
                    {
                        _endCfg = true;
                    }
                    else
                    {
                        cfgFileLines.Add(_startMas);
                    }
                } while (!_endCfg);
            }
            catch
            {
                _parsed = false;
                _msg = "���� .cfg ��������� � �� ����� ���� ������.";
            }
            finally
            {
                if (_strimReadCFG != null)
                {
                    _strimReadCFG.Close();
                }
            }
            return _parsed;
        }

        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            this._oscShowButton.Enabled = false;
            this.ShowLoadWindow();
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCB.SelectedIndex = e.RowIndex;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }
    }
}