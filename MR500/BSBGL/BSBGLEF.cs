using BEMN.MBServer;
using BMTCD.HelperClasses;
using SchemeEditorSystem;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using BEMN.Compressor;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using BMTCD.�ompilationScheme.Compilers;
using BEMN.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;

namespace BEMN.MR500.BSBGL
{
    public partial class BSBGLEF : Form, IFormView
    {
        #region Fields
        private const int COUNT_EXCHANGES_PROGRAM = 16;
        private const int COUNT_EXCHANGES_PROGECT = 128;
        private MemoryEntity<StartStruct> _currentStartProgramStruct;
        private MemoryEntity<ProgramStorageStruct> _currentStorageStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<ProgramSignalsStruct> _currentSignalsStruct;
        private MR500 _device;
        private NewSchematicForm newForm;
        private SaveFileDialog _saveFile;
        private DockingManager _manager;
        private Compiler _compiller;
        private bool _isOnSimulateMode;
        private MessageBoxForm _formCheck;
        ushort[] _binFile;
        private bool _isRunEmul;
        private byte[] _fromDevice;
        private OutputWindow _outputWindow;
        private LibraryBox _libraryWindow;
        private Content _outputContent;
        private Content _libraryContent;
        private bool _captionBars = true;
        private bool _closeButtons = true;
        private VisualStyle _style;
        private StatusBar _statusBar;
        private Crownwood.Magic.Controls.TabControl _filler;
        
        #endregion


        #region Constructor
        public BSBGLEF()
        {
            this.InitForm();
        }
        public BSBGLEF(MR500 device)
        {
            this._device = device;
            this.InitForm();
            this._currentSourceProgramStruct = this._device.SourceProgramStruct;
            this._currentStartProgramStruct = this._device.ProgramStartStruct;
            this._currentSignalsStruct = this._device.ProgramSignalsStruct;
            this._currentStorageStruct = this._device.ProgramStorageStruct;
            //���������
            this._device.SourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.WriteOk);
            this._device.SourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSaveFail);
            this._device.SourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.SourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.SourceProgramStruct.RemoveStructQueries();
                this.ProgramSaveFail();
            });
            //����� ���������
            this._device.ProgramStorageStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StorageReadOk);
            this._device.ProgramStorageStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStorageReadFail);
            this._device.ProgramStorageStruct.ReadOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.ProgramStorageStruct.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.ProgramStorageStruct.RemoveStructQueries();
                this.ProgramStorageReadFail();
            });
            this._device.ProgramStorageStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StorageWriteOk);
            this._device.ProgramStorageStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.StorageWriteFail);
            this._device.ProgramStorageStruct.WriteOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.ProgramStorageStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.ProgramStorageStruct.RemoveStructQueries();
                this.StorageWriteFail();
            });
            //����� ���������
            this._device.StartStopSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("������� ������� ���������", "", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._device.StartStopSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� �������", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.ProgramStartStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartSaveOk);
            this._device.ProgramStartStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveFail);
            //�������� ��������
            this._device.ProgramSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSignalsLoadOk);
            this._device.ProgramSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.SignalsLoadFail);
        }
        #endregion

        private void ExchangeOk()
        {
            this._formCheck.ProgramExchangeOk();
        }

        private void ProgramStorageReadFail()
        {
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
        }

        private void ProgramSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            this.OnStop();
        }

        private void WriteOk()
        {
            this._formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
            var values = new ushort[1];
            values[0] = 0x00FF;
            StartStruct ss = new StartStruct();
            ss.InitStruct(Common.TOBYTES(values, false));
            this._currentStartProgramStruct.Value = ss;
            this._currentStartProgramStruct.SaveStruct5();

        }
        private void StorageReadOk()
        {
            if (!this._isRunEmul)
            {
                ushort[] value = this._currentStorageStruct.Values;
                this._formCheck.ShowResultMessage(this.UncompresseProject(value)
                    ? InformationMessages.PROJECT_LOADED_OK_OF_DEVICE
                    : InformationMessages.PROJWCT_STORAGE_IS_EMPTY);
            }
            else
            {
                this.OnEmulArchLoaded();
            }
        }

        private void StorageWriteOk()
        {
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            SourceProgramStruct ps = new SourceProgramStruct();
            ps.InitStruct(Common.TOBYTES(this._binFile, false));
            this._currentSourceProgramStruct.Value = ps;
            this._currentSourceProgramStruct.SaveStruct();
            this._isOnSimulateMode = true;
            this._formCheck.ShowMessage(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
        }
        
        private void ProgramSignalsLoadOk()
        {
            if (this._isOnSimulateMode)
            {
                this._compiller.DiskretUpdateVol(this._currentSignalsStruct.Values);
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    src.Schematic.RedrawSchematic();
                    src.Schematic.RedrawBack();
                }
                this.OutMessage(InformationMessages.VARIABLES_UPDATED);
            }
        }

        private void StorageWriteFail()
        {
            this.OutMessage(InformationMessages.ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE);
            this.OnStop();
        }
        private void ProgramStartSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_PROGRAM_START);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            this.OnStop();
        }
        
        void SignalsLoadFail()
        {
            //������������� ���������
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
            if (this._isOnSimulateMode)
            {
                this.OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
            }
        }

        private void StartSaveOk()
        {
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_ARCHIVE_SAVE_OK_START_PROGRAM);
            ProgramSignalsStruct ps = new ProgramSignalsStruct(this._compiller.GetRamRequired());
            ushort[] values = new ushort[this._compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0xA000);
            this._currentSignalsStruct.LoadStructCycle();
        }

        private void StartLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.StartStopSpl.StartAddress = 0x1809;
            this._device.StartStopSpl.Value.Word = 0x00FF;
            this._device.StartStopSpl.SaveStruct5();
        }

        private void StopLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.StartStopSpl.StartAddress = 0x1808;
            this._device.StartStopSpl.Value.Word = 0x00FF;
            this._device.StartStopSpl.SaveStruct5();
        }

        private DialogResult CloseFileProject()
        {
            if (this._filler.SelectedTab == null) return 0;
            switch (MessageBox.Show("��������� ������ �� ����� ?",
                "�������� �������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    this.SaveProjectDoc();                    
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();                    
                    break;
                case DialogResult.No:
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();
                    break;
                case DialogResult.Cancel:
                    return DialogResult.Cancel;
            }
            return DialogResult.None;
        }

        private void OnFileOpenFromDevice(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                if (DialogResult.Cancel == this.CloseFileProject())
                {
                    return;
                }
                this._currentStorageStruct.LoadStruct();
                this._formCheck = new MessageBoxForm();
                this._formCheck.ShowDialog(InformationMessages.DOWNLOADING_ARCHIVE_OF_DEVICE);
                this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
            }
            catch
            {
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                this.OutMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
            }
        }

        void InitForm()
        {
            this.InitializeComponent();
            this._style = VisualStyle.IDE;
            this._manager = new DockingManager(this, this._style);
            this.newForm = new NewSchematicForm();
            this._compiller = new Compiler("MR500");
            this._formCheck = new MessageBoxForm();
        }

        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            this._filler = new Crownwood.Magic.Controls.TabControl
            {
                Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument,
                Dock = DockStyle.Fill,
                Style = this._style,
                IDEPixelBorder = true
            };
            Controls.Add(this._filler);
            this._filler.ClosePressed += new EventHandler(this.OnFileClose);
            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // Create the object that manages the docking state
            this._manager = new DockingManager(this, this._style);
            // Ensure that the RichTextBox is always the innermost control
            this._manager.InnerControl = this._filler;
            // Create and setup the StatusBar object
            this._statusBar = new StatusBar();
            this._statusBar.Dock = DockStyle.Bottom;
            this._statusBar.ShowPanels = true;
            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel();
            statusBarPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            this._statusBar.Panels.Add(statusBarPanel);
            Controls.Add(this._statusBar);
            Controls.Add(this.CreateToolStrip());
            Controls.Add(this.CreateMenus());
            // Ensure that docking occurs after the menu control and status bar controls
            this._manager.OuterControl = this._statusBar;
            // Create Content which contains a RichTextBox
            this.CreateOutputWindow();
            this.CreateLibraryWindow();
            Width = 800;
            Height = 600;
        }

        private void OutMessage(string str)
        {
            this._outputWindow.AddMessage(str + "\r\n");
        }

        private void DefineContentState(Content c)
        {
            c.CaptionBar = this._captionBars;
            c.CloseButton = this._closeButtons;
        }

        #region Docking FOrms Code
        private void CreateOutputWindow()
        {
            this._outputWindow = new OutputWindow();
            this._outputContent = this._manager.Contents.Add(this._outputWindow, "���������");
            this.DefineContentState(this._outputContent);
            this._manager.AddContentWithState(this._outputContent, State.DockBottom);
        }
        private void CreateLibraryWindow()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BSBGLEF));
            this._libraryWindow = new LibraryBox(Properties.Resources.BlockLib);
            this._libraryContent = this._manager.Contents.Add(this._libraryWindow, "����������");
            this.DefineContentState(this._libraryContent);
            this._manager.AddContentWithState(this._libraryContent, State.DockRight);
        }

        #endregion
  
        private void OnFileNew(object sender, EventArgs e)
        {
            this.newForm.ShowDialog();
            if (DialogResult.OK == this.newForm.DialogResult)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this.newForm.NameOfSchema, this.newForm.sheetFormat, "MR500", this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
                this.OutMessage("������ ����� ���� �����: " + this.newForm.NameOfSchema);
                myTab.Schematic.Focus();
            }
        }
        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            this.pageSetupDialog.PageSettings =
                new System.Drawing.Printing.PageSettings();

            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            this.pageSetupDialog.PrinterSettings =
                new System.Drawing.Printing.PrinterSettings();

            //Do not show the network in the printer dialog.
            this.pageSetupDialog.ShowNetwork = false;

            //Show the dialog storing the result.
            DialogResult result = this.pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.printDocument.DefaultPageSettings = this.pageSetupDialog.PageSettings;
                this.printPreviewDialog.Document = this.printDocument;
                // Call the ShowDialog method. This will trigger the document's
                //  PrintPage event.
                this.printPreviewDialog.ShowDialog();
                DialogResult result1 = this.printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    this.printDocument.Print();
                }
            }
        }
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;

                float scale = (float)e.PageBounds.Width /_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics, scale);
            }
        }

        private void OnUndo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        private void OnRedo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        private void OnEditCut(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }
        }

        private void OnEditCopy(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        private void OnEditPaste(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }
        }

        private void OnFileOpen(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this.openFileDialog.FileName, SheetFormat.A0_L, "MR500", this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);

                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                myTab.Schematic.ReadXml(reader);
                myTab.UpdateTitle();
                reader.Close();
                this.OutMessage("�������� ���� �����.");
                myTab.Schematic.Focus();
            }

        }
        private void OnFileOpenProject(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (this._filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("��������� ������� ������ �� ����� ?"
                                                    , "�������� �������", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (this.SaveProjectDoc())
                            {
                                this._filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No:
                            this._filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }

                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                while (reader.Read())
                {
                    if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                    {
                        BSBGLTab myTab = new BSBGLTab();
                        myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, "MR500", this._manager);
                        myTab.Selected = true;
                        this._filler.TabPages.Add(myTab);
                        myTab.Schematic.ReadXml(reader);
                        myTab.UpdateTitle();
                    }
                }
                this.OutMessage("�������� ������.");
                reader.Close();
                this._filler.SelectedTab.Focus();
            }
        }

        private void OnFileClose(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("��������� �������� �� ����� ?"
                                                , "�������� ���������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    if (this.SaveActiveDoc())
                    {
                        BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                        this._filler.TabPages.Remove(tab);
                        tab.Dispose();
                    }
                    break;
                case DialogResult.No:
                {
                    BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                    this._filler.TabPages.Remove(tab);
                    tab.Dispose();
                }
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        private void OnFileCloseProject(object sender, EventArgs e)
        {
            this.CloseFileProject();
        }
        private void OnFileSave(object sender, EventArgs e)
        {

            if (this._filler.TabPages.Count == 0)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveActiveDoc();
        }
        private void OnFileSaveProject(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveProjectDoc();
        }
        private void OnViewToolWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._libraryContent);
        }
        private void OnViewOutputWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._outputContent);

        }

        private bool CompileProject()
        {
            this._compiller.ResetCompiller();
            try
            {
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    this._compiller.AddSource(src.TabName, src.Schematic);
                    this.OutMessage("���������� ����� :" + src.TabName);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return false;
            }
            this._binFile = this._compiller.Make();
            this.OutMessage("�p����� ���������� �����:" + (float)this._compiller.Binarysize * 100 / 1024 + "%.");
            if (this._compiller.Binarysize > 1024)
            {
                MessageBox.Show("��������� ������� ������ ! ", "������ ����������", MessageBoxButtons.OK);
                this.OnStop();
                return false;
            }
            if (this._binFile.Length == 0)
            {
                MessageBox.Show("���������� �������� ���������� ��������� ���������",
                    "������ �����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return false;
            }
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }
            return true;
        }

        private void OnCompileUpload(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("�������� ���� ���������� ��������� � ����������?", "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) != DialogResult.OK) return;

            if (!this.CompileProject()) return;
            
            ushort[] programStorageValue = this.CompresseProject();
            ushort[] values = new ushort[8192];
            programStorageValue.CopyTo(values, 0);
            ProgramStorageStruct pss = new ProgramStorageStruct();
            pss.InitStruct(Common.TOBYTES(values, false));
            this._currentStorageStruct.Value = pss;
            this._currentStorageStruct.SaveStruct();
            this._formCheck = new MessageBoxForm();
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
            this._formCheck.ShowDialog(InformationMessages.LOADING_ARCHIVE_IN_DEVICE);
        }

        private byte[] MakeBinFromXml()
        {
            MemoryStream memstream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("BSBGL_ProjectFile");
            if (this._filler.TabPages.Count == 0) return new byte[0];
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("name", src.TabName);
                src.Schematic.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            return memstream.ToArray();
        }

        private bool CompareScheme()
        {
            byte[] fromScheme = this.MakeBinFromXml();
            if (fromScheme.Length == 0 || fromScheme.Length != this._fromDevice.Length) return false;
            //return !fromScheme.Where((t, i) => t != this._fromDevice[i]).Any();
            bool ret = true;// = !fromScheme.Where((t, i) => t != readCompressedData[i]).Any();
            for (int i = 0; i < fromScheme.Length; i++)
            {
                ret &= fromScheme[i] == this._fromDevice[i];
                if (!ret)
                {
                    return false;
                }
            }
            return true;
        }

        private void OnStartEmul(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                if (MessageBox.Show("��� ������� �����. ��������� �� ����������?", "", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No) return;
            }
            try
            {
                this._isRunEmul = true;
                this._currentStorageStruct.LoadStruct();
                this._formCheck = new MessageBoxForm();
                this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
                this._formCheck.ShowDialog(InformationMessages.DOWNLOADING_ARCHIVE_OF_DEVICE);
            }
            catch
            {
                this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                this.OutMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                this._isRunEmul = false;
            }
        }

        private void OnEmulArchLoaded()
        {
            this._fromDevice = this.Uncompress(this._currentStorageStruct.Values);
            if (this._fromDevice.Length == 0)
            {
                this._formCheck.ShowResultMessage(InformationMessages.PROJWCT_STORAGE_IS_EMPTY);
                return;
            }
            if (this._filler.TabPages.Count != 0)
            {
                if (!this.CompareScheme())
                {
                    DialogResult res =
                        MessageBox.Show(
                            "����� ���������� ���������, ����������� �� ����������, �� ��������� � ��������, �������� � �������. ���������� �������� � ����������� �������?",
                            "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res == DialogResult.No)
                    {
                        this._formCheck.ShowResultMessage("�������� �������������");
                        return;
                    }
                }
                this._filler.TabPages.Clear();
            }
            this.LoadProjectFromBin(this._fromDevice);

            this._isOnSimulateMode = true;
            this.CompileProject();
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }
            this.StartLoadCurrentSignals();
        }

        private void StartLoadCurrentSignals()
        {
            ProgramSignalsStruct ps = new ProgramSignalsStruct(this._compiller.GetRamRequired());
            ushort[] values = new ushort[this._compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0xA000);
            this._currentSignalsStruct.LoadStructCycle();
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_SAVE_OK_EMULATOR_RUNNING);
        }

        void OnStop(object sender, EventArgs e)
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        void OnStop()
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        private bool SaveActiveDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            DialogResult SaveFileRzult;
            this._saveFile = new SaveFileDialog();
            this._saveFile.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            SaveFileRzult = this._saveFile.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter memwriter = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                memwriter.Formatting = Formatting.Indented;
                memwriter.WriteStartDocument();

                if (this._filler.SelectedTab is BSBGLTab)
                {
                    BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                    _sTab.Schematic.WriteXml(memwriter);
                }
                memwriter.WriteEndDocument();
                memwriter.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this._saveFile.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,
                                false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this._saveFile.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,
                                false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private bool SaveProjectDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            this._saveFile = new SaveFileDialog();
            DialogResult SaveFileRzult;
            this._saveFile.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            SaveFileRzult = this._saveFile.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_ProjectFile");
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("name", src.TabName);
                    src.Schematic.WriteXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this._saveFile.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,
                                false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this._saveFile.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,
                                false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private ushort[] CompresseProject()
        {
            ZIPCompressor compr = new ZIPCompressor();
            var buf = this.MakeBinFromXml();
            byte[] compressed = compr.Compress(buf);
            ushort[] compressedWords = new ushort[(compressed.Length + 1) / 2 + 3]; //������ ���������
            compressedWords[0] = (ushort)compressed.Length; // ������ ������� ������� (�����)
            compressedWords[1] = 0x0001; // ������ ����������
            compressedWords[2] = 0x0000; // �R� ������ �������
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressedWords[i] = (ushort)(compressed[(i - 3) * 2 + 1] << 8);
                }
                else
                {
                    compressedWords[i] = 0;
                }
                compressedWords[i] += compressed[(i - 3) * 2];
            }
            return compressedWords;
        }


        private byte[] Uncompress(ushort[] readedData)
        {
            ZIPCompressor compr = new ZIPCompressor();
            byte[] compressed = new byte[readedData[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(readedData[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)readedData[i];
            }
            return compr.Decompress(compressed);
        }

        private void LoadProjectFromBin(byte[] uncompressed)
        {
            MemoryStream memstream = new MemoryStream();
            memstream.Write(uncompressed, 0, uncompressed.Length);
            if (this._filler.SelectedTab != null)
            {
                switch (MessageBox.Show("��������� ������� ������ �� ����� ?"
                                                , "�������� �������", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (this.SaveProjectDoc())
                        {
                            this._filler.TabPages.Clear();
                        }
                        else
                        {
                            return;
                        }
                        break;
                    case DialogResult.No:
                        this._filler.TabPages.Clear();
                        break;
                    case DialogResult.Cancel:
                        return;
                }
            }
            memstream.Seek(0, SeekOrigin.Begin);
            XmlTextReader reader = new XmlTextReader(memstream);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            while (reader.Read())
            {
                if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                {
                    BSBGLTab myTab = new BSBGLTab();
                    myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, "TZL", this._manager);
                    myTab.Selected = true;
                    this._filler.TabPages.Add(myTab);
                    myTab.Schematic.ReadXml(reader);
                }
            }
            this.OutMessage("������ ������� �������� �� ����������");
            reader.Close();
        }


        private bool UncompresseProject(ushort[] compressedWords)
        {
            byte[] uncompressed = this.Uncompress(compressedWords);
            if (uncompressed.Length == 0)
            {
                return false;
            }
            this.LoadProjectFromBin(uncompressed);
            return true;
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.programming.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "����������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void BSBGLEF_FormClosed(object sender, FormClosedEventArgs e)
        {
            this._currentSignalsStruct.RemoveStructQueries();
            if (this._filler.TabPages.Count == 0) return;
            switch (MessageBox.Show("��������� ������� ������ �� ����� ?"
                                            , "�������� �������", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    if (this.SaveProjectDoc())
                    {
                        this._filler.TabPages.Clear();
                    }
                    break;
            }
        }
    }
}
