using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.MR500
{
    public partial class VMeasuringForm : Form, IFormView
    {
        private MR500 _device;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _automationLeds;


        Timer _timer = new Timer();

        public VMeasuringForm()
        {
            this.InitializeComponent();
        }

        public VMeasuringForm(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;
            Init();
        }

        private void Init()
        {
            this._dateTimeBox.TZLFlag = true;
            this._manageLeds = new[]
            {
                this._manageLed1, this._manageLed2, this._manageLed3, this._manageLed4, this._manageLed5,
                this._manageLed6, this._manageLed7, this._manageLed8, new LedControl(), this._manageLed9
            };
            this._additionalLeds = new[]
            {
                this._addIndLed1, this._addIndLed2, this._addIndLed3, this._addIndLed4
            };

            this._indicatorLeds = new[]
            {
                this._indLed1, this._indLed2, this._indLed3, this._indLed4,
                this._indLed5, this._indLed6, this._indLed7, this._indLed8
            };
            this._inputLeds = new[]
            {
                this._inD_led1, this._inD_led2, this._inD_led3, this._inD_led4,
                this._inD_led5, this._inD_led6, this._inD_led7, this._inD_led8,
                this._inD_led9, this._inD_led10, this._inD_led11, this._inD_led12,
                this._inD_led13, this._inD_led14, this._inD_led15, this._inD_led16,
                this._inL_led1, this._inL_led2, this._inL_led3, this._inL_led4,
                this._inL_led5, this._inL_led6, this._inL_led7, this._inL_led8
            };
            this._outputLeds = new[]
            {
                this._outLed1, this._outLed2, this._outLed3, this._outLed4,
                this._outLed5, this._outLed6, this._outLed7, this._outLed8
            };
            this._releLeds = new[]
            {
                this._releLed1, this._releLed2, this._releLed3, this._releLed4,
                this._releLed5, this._releLed6, this._releLed7, this._releLed8,
                this._releLed9, this._releLed10, this._releLed11, this._releLed12, this._releLed13
            };

            this._limitLeds = new[]
            {
                this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4,
                this._ImaxLed5, this._ImaxLed6, this._ImaxLed7, this._ImaxLed8,
                this._I0maxLed1, this._I0maxLed2, this._I0maxLed3, this._I0maxLed4,
                this._I0maxLed5, this._I0maxLed6, this._I0maxLed7, this._I0maxLed8,
                this._InmaxLed1, this._InmaxLed2, this._InmaxLed3, this._InmaxLed4,
                this._IgLed5, this._IgLed6, this._I21Led7, this._I21Led8, this._achr, this._chapv,
                this.ledControl1, this.ledControl2, this.ledControl3, this.ledControl4, this.ledControl5,
                this.ledControl6, this.ledControl7, this.ledControl8, this.ledControl9, this.ledControl10,
                this.ledControl11, this.ledControl12, this.ledControl13, this.ledControl14, this.ledControl15,
                this.ledControl16, this.ledControl17, this.ledControl18, this.ledControl19, this.ledControl20,
                this.ledControl21, this.ledControl22,
                this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8
            };
            this._faultStateLeds = new[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, new LedControl(), 
                this._faultStateLed5, this._faultStateLed6
            };
            this._faultSignalsLeds = new[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, new LedControl(), 
                this._faultSignalLed5, this._faultSignalLed6, this._faultSignalLed7, this._faultSignalLed8,
                this._faultSignalLed9, this._faultSignalLed10, this._faultSignalLed11, this._faultSignalLed12,
                this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15, this._faultSignalLed16
            };
            this._automationLeds = new[]
            {
                this._autoLed1, this._autoLed2, this._autoLed3, this._autoLed4,
                this._autoLed5, this._autoLed6, this._autoLed7, this._autoLed8
            };

            this._device.ConnectionModeChanged += this.StartStopRead;
            this._device.TZLDevice.DiagnosticLoadOk += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadOk);
            this._device.TZLDevice.DiagnosticLoadFail += HandlerHelper.CreateHandler(this, this.OnDiagnosticLoadFail);
            this._device.TZLDevice.DateTimeLoadOk += HandlerHelper.CreateHandler(this, this.OnDateTimeLoadOk);
            this._device.TZLDevice.DateTimeLoadFail += HandlerHelper.CreateHandler(this, this.OnDateTimeLoadFail);
            this._device.TZLDevice.AnalogSignalsLoadOK += HandlerHelper.CreateHandler(this, this.OnAnalogSignalsLoadOk);
            this._device.TZLDevice.AnalogSignalsLoadFail += HandlerHelper.CreateHandler(this,this.OnAnalogSignalsLoadFail);

            this._timer.Interval = 100;
            this._timer.Tick += new EventHandler(this._timer_Tick);
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.TZLDevice.LoadAnalogSignalsCycle();
                this._device.TZLDevice.LoadDiagnosticCycle();
                this._device.TZLDevice.LoadTimeCycle();
            }
            else
            {
                this._device.TZLDevice.RemoveDiagnostic();
                this._device.TZLDevice.RemoveDateTime();
                this._device.TZLDevice.RemoveAnalogSignals();
                this.OnAnalogSignalsLoadFail();
                this.OnDateTimeLoadFail();
                this.OnDiagnosticLoadFail();
            }
        }

        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._device.TZLDevice.MakeDiagnosticQuick();
            this._device.TZLDevice.MakeDateTimeQuick();
            this._device.TZLDevice.MakeAnalogSignalsQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            //this._device.TZLDevice.MakeDiagnosticSlow();
            //this._device.TZLDevice.MakeDateTimeSlow();
            //this._device.TZLDevice.MakeAnalogSignalsSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.TZLDevice.RemoveDiagnostic();
            this._device.TZLDevice.RemoveDateTime();
            this._device.TZLDevice.RemoveAnalogSignals();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }
        
        private void OnAnalogSignalsLoadFail()
        {
            this._InBox.Text = string.Empty;
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;
        }

        private void OnAnalogSignalsLoadOk()
        {
            string s = "";
            this._InBox.Text = String.Format("In = " + s + " {0:F2} �", this._device.TZLDevice.In);
            this._IaBox.Text = String.Format("Ia = " + s + " {0:F2} �", this._device.TZLDevice.Ia);
            this._IbBox.Text = String.Format("Ib = " + s + " {0:F2} �", this._device.TZLDevice.Ib);
            this._IcBox.Text = String.Format("Ic = " + s + " {0:F2} �", this._device.TZLDevice.Ic);
            this._I0Box.Text = String.Format("I0 = " + s + " {0:F2} �", this._device.TZLDevice.I0);
            this._I1Box.Text = String.Format("I1 = " + s + " {0:F2} �", this._device.TZLDevice.I1);
            this._I2Box.Text = String.Format("I2 = " + s + " {0:F2} �", this._device.TZLDevice.I2);
            this._IgBox.Text = String.Format("Ig = {0:F2} �", this._device.TZLDevice.Ig);
        }


        private void OnDateTimeLoadFail()
        {
            this._dateTimeBox.Text = "";
        }


        private void OnDateTimeLoadOk()
        {
            this._dateTimeBox.DateTime = this._device.TZLDevice.DateTime;
        }
        
        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._additionalLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._automationLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
        }

        private void OnDiagnosticLoadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._device.TZLDevice.ManageSignals);
            LedManager.SetLeds(this._additionalLeds, this._device.TZLDevice.AdditionalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._device.TZLDevice.Indicators);
            LedManager.SetLeds(this._inputLeds, this._device.TZLDevice.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._device.TZLDevice.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._device.TZLDevice.Rele);
            LedManager.SetLeds(this._limitLeds, this._device.TZLDevice.LimitSignals);
            LedManager.SetLeds(this._automationLeds, this._device.TZLDevice.Automation);
            LedManager.SetLeds(this._faultSignalsLeds, this._device.TZLDevice.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._device.TZLDevice.FaultState);
        }

        private void _readTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            this._device.TZLDevice.SuspendDateTime(!this._readTimeCheck.Checked);
            if (this._readTimeCheck.Checked)
            {
                this._systemTimeCheck.Checked = false;
            }
        }

        private void _writeTimeBut_Click(object sender, EventArgs e)
        {
            if ((this._dateTimeBox.DateTime[3] < 13 && this._dateTimeBox.DateTime[3] > 0) &&(this._dateTimeBox.DateTime[5] < 32 && this._dateTimeBox.DateTime[5] > 0) &&(this._dateTimeBox.DateTime[7] < 24 && this._dateTimeBox.DateTime[7] >= 0) &&
                (this._dateTimeBox.DateTime[9] < 60 && this._dateTimeBox.DateTime[9] >= 0)&&(this._dateTimeBox.DateTime[11] < 60 && this._dateTimeBox.DateTime[11] >= 0))
            {
                this._device.TZLDevice.DateTime = this._dateTimeBox.DateTime;
                this._device.TZLDevice.SaveDateTime();
                this._readTimeCheck.Checked = this._systemTimeCheck.Checked = false;
            }
            else
            {
                MessageBox.Show("���������� ������� ����");
            }
        }

        private void _systemTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._systemTimeCheck.Checked)
            {
                this._readTimeCheck.Checked = false;
                this._timer.Start();
            }
            else
            {
                this._timer.Stop();
            }
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            string[] dates = System.Text.RegularExpressions.Regex.Split(DateTime.Now.ToShortDateString(), "\\.");
            if (null != dates[2]) //Year
            {
                dates[2] = dates[2].Remove(0, 2);
            }


            if (DateTime.Now.Hour < 10)
            {
                this._dateTimeBox.Text = String.Concat(dates[0], dates[1], dates[2]) + "0" + DateTime.Now.ToLongTimeString() + DateTime.Now.Millisecond.ToString();
            }
            else
            {
                this._dateTimeBox.Text = String.Concat(dates[0], dates[1], dates[2]) + DateTime.Now.ToLongTimeString() + DateTime.Now.Millisecond.ToString();
            }
        }
        
        private void ConfirmSDTU(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��500", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber, this._device);
            }
        }

        private void _resetFaultBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "������������� ������� �������");
        }

        private void _resetJA_But_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "������������� ������� ������");
        }
        
        private void _resetIndicatBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1804, "�������� ���������");
        }

        private void _breakerOffBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _breakerOnBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "������ ���", this._device);
        }

        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "������� ���", this._device);
        }

        private void _selectConstraintGroupBut_Click_1(object sender, EventArgs e)
        {
            string msg = "";
            bool constraintGroup = false;
            if (this._manageLed4.State == LedState.NoSignaled)
            {
                constraintGroup = true;
                msg = "����������� �� �������� ������";
            }
            else if (this._manageLed4.State == LedState.Signaled)
            {
                msg = "����������� �� ��������� ������";
            }
            else
            {
                return;
            }
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��500", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.TZLDevice.SelectConstraintGroup(constraintGroup);
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(BEMN.MR500.MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(VMeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR500.Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

    }
}