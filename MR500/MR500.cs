using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using BEMN.MR500.AlarmJournal;
using BEMN.MR500.BSBGL;
using BEMN.MR500.SystemJournal.Structures;
using BEMN.MR500.AlarmJournal.Structures;
using BEMN.MR500.SystemJournal;
using BEMN.Framework;
using System.Linq;

namespace BEMN.MR500
{
    public class MR500 : Device, IDeviceView, IDeviceVersion
    {
        #region ��������� ������

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;
        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private bool _stopSystemJournal = false;

        [Browsable(false)]
        [XmlIgnore]
        public CSystemJournal SystemJournal
        {
            get { return _systemJournalRecords; }
            set { _systemJournalRecords=value; }
        }

        public struct SystemRecord
        {
            public string msg;
            public string time;
        } ;

        public class CSystemJournal : ICollection
        {
            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return _messages.Count; }
            }

            public SystemRecord this[int i]
            {
                get { return _messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "������ ����" == _messages[i].msg;
            }

            public  bool AddMessage(ushort[] value)
            {
                bool ret;
                SystemRecord msg = CreateMessage(value);
                if ("������ ����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
            }

            private static SystemRecord CreateMessage(ushort[] value)
            {
                SystemRecord msg = new SystemRecord();
                //int index = value[0];
                //if (value[0] > Strings.SystemJournal.Count - 1)
                //{
                //    index = Strings.SystemJournal.Count - 1;
                //}
                msg.msg = Strings.SystemJournal[value[0]];

                msg.time = value[3].ToString("d2") + "-" + value[2].ToString("d2") + "-" + value[1].ToString("d2") + " " +
                           value[4].ToString("d2") + ":" + value[5].ToString("d2") + ":" + value[6].ToString("d2") + ":" +
                           value[7].ToString("d2");

                return msg;
            }

            #region ICollection Members

            public void CopyTo(Array array, int index)
            {
            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return _messages; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _messages.GetEnumerator();
            }

            #endregion
        }

        #endregion

        #region ������ ������

        public const int ALARMJOURNAL_RECORD_CNT = 32;
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];
        private CAlarmJournal _alarmJournalRecords;
        private bool _stopAlarmJournal = false;

        [Browsable(false)]
        [XmlIgnore]
        public CAlarmJournal AlarmJournal
        {
            get { return _alarmJournalRecords; }
            set { _alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ia;
            public string Ib;
            public string Ic;
            public string Io;
            public string Ig;
            public string I0;
            public string I1;
            public string I2;
            public string inSignals1;
            public string inSignals2;
        } ;

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            private MR500 _device;

            public CAlarmJournal()
            {

            }

            public CAlarmJournal(MR500 device)
            {
                _device = device;
                _msgDictionary = new Dictionary<ushort, string>(6);
                _codeDictionary = new Dictionary<byte, string>(19);
                _typeDictionary = new Dictionary<byte, string>(9);

                _msgDictionary.Add(0, "������ ����");
                _msgDictionary.Add(1, "������������");
                _msgDictionary.Add(2, "����������");
                _msgDictionary.Add(3, "����������");
                _msgDictionary.Add(4, "���������� ���");
                _msgDictionary.Add(118, "������ ����");
                _msgDictionary.Add(147, "������ ����");
                _msgDictionary.Add(255, "��� ���������");
                _msgDictionary.Add(84, "��� ���������");

                _codeDictionary.Add(1, "��-1");
                _codeDictionary.Add(2, "��-2");
                _codeDictionary.Add(3, "��-3");
                _codeDictionary.Add(4, "��-4");
                _codeDictionary.Add(5, "��-5");
                _codeDictionary.Add(6, "��-6");
                _codeDictionary.Add(7, "��-7");
                _codeDictionary.Add(8, "��-8");
                _codeDictionary.Add(9, "I>");
                _codeDictionary.Add(10, "I>>");
                _codeDictionary.Add(11, "I>>>");
                _codeDictionary.Add(12, "I>>>>");
                _codeDictionary.Add(13, "I0>");
                _codeDictionary.Add(14, "I0>>");
                _codeDictionary.Add(15, "I2>");
                _codeDictionary.Add(16, "I2>>");
                _codeDictionary.Add(17, "���");
                _codeDictionary.Add(18, "���");


                _typeDictionary.Add(0, "��");
                _typeDictionary.Add(1, "Ig");
                _typeDictionary.Add(2, "Io");
                _typeDictionary.Add(3, "Ia");
                _typeDictionary.Add(4, "Ib");
                _typeDictionary.Add(5, "Ic");
                _typeDictionary.Add(6, "I0");
                _typeDictionary.Add(7, "I1");
                _typeDictionary.Add(8, "I2");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return _messages.Count; }
            }
            
            public AlarmRecord this[int i]
            {
                get { return _messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "������ ����" == _messages[i].msg;
            }

            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = CreateMessage(value);
                if ("�����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
            }

            public string GetMessage(byte b)
            {
                try
                {
                    return _msgDictionary[b];
                }
                catch (KeyNotFoundException)
                {
                    return _msgDictionary[0];
                }
            }

            public string GetDateTime(byte[] datetime)
            {
                return
                    datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
                    " " +
                    datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
                           ":" + datetime[12].ToString("d2");
            }
            
            public string GetCode(byte codeByte, byte phaseByte)
            {

                string group = (0 == (codeByte & 0x80)) ? "��������" : "������.";
                string phase = "";
                phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                phase += (0 == (phaseByte & 0x04)) ? " " : "C";

                byte code = (byte)(codeByte & 0x7F);
                try
                {
                    return _codeDictionary[code] + " " + group + " " + phase;
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }

            public string GetExternal(byte codeByte)
            {
                byte code = (byte)(codeByte & 0x7F);
                try
                {
                    return _codeDictionary[code];
                }
                catch
                {
                    return code.ToString();
                }
            }

            public string GetTypeValue(ushort damageWord, byte typeByte)
            {
                try
                {
                    if (typeByte==0)
                    {
                        return _typeDictionary[typeByte];
                    }
                    double damageValue = typeByte == 1 || typeByte == 2
                        ? Measuring.GetI(damageWord, _device.TTNP, false)
                        : Measuring.GetI(damageWord, _device.TT, true);
                    return _typeDictionary[typeByte] + string.Format(" = {0:F2}", damageValue);
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }

            private AlarmRecord CreateMessage(byte[] buffer)
            {
                AlarmRecord rec = new AlarmRecord();
                rec.msg = GetMessage(buffer[0]);

                byte[] datetime = new byte[14];
                Array.ConstrainedCopy(buffer, 2, datetime, 0, 14);
                rec.time = GetDateTime(datetime);
                                
                rec.code = GetCode(buffer[16], buffer[18]);
                rec.type_value = GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19]);

                double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), _device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), _device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), _device.TT, true);
                double Io = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), _device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), _device.TTNP, false);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), _device.TT, true);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), _device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), _device.TT, true);


                rec.Ia = string.Format("{0:F2}", Ia);
                rec.Ib = string.Format("{0:F2}", Ib);
                rec.Ic = string.Format("{0:F2}", Ic);
                rec.Io = string.Format("{0:F2}", Io);
                rec.Ig = string.Format("{0:F2}", Ig);
                rec.I0 = string.Format("{0:F2}", I0);
                rec.I1 = string.Format("{0:F2}", I1);
                rec.I2 = string.Format("{0:F2}", I2);
                
                

                byte[] b1 = new byte[] {buffer[38]};
                byte[] b2 = new byte[] {buffer[39]};
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }

        #endregion

        #region ������� �������

        private slot _inputSignals = new slot(0x1100, 0x1120);

        public void LoadInputSignals()
        {
            LoadSlot(DeviceNumber, _inputSignals, "LoadInputSignals" + DeviceNumber, this);
        }

        public void SaveInputSignals()
        {
            SaveSlot(DeviceNumber, _inputSignals, "SaveInputSignals" + DeviceNumber, this);
        }

        #region ��������� ���� � ����������

        [XmlElement("��")]
        [DisplayName("��")]
        [Description("��������� ��� ��������������")]
        [CategoryAttribute("��������� ���� � ����������")]
        public ushort TT
        {
            get { return _inputSignals.Value[0]; }
            set { _inputSignals.Value[0] = value; }
        }

        [XmlElement("����")]
        [DisplayName("����")]
        [Description("��������� ��� �������������� ������� ������������������")]
        [Category("��������� ���� � ����������")]
        public ushort TTNP
        {
            get { return _inputSignals.Value[1]; }
            set { _inputSignals.Value[1] = value; }
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Description("������������ ��� ��������")]
        [Category("��������� ���� � ����������")]
        public double MaxTok
        {
            get { return Measuring.GetConstraint(_inputSignals.Value[3], ConstraintKoefficient.K_4000); }
            set { _inputSignals.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        #endregion

        #region ��������� �����

        [XmlElement("�����_�����_���������")]
        public string SpeedupNumber
        {
            get
            {
                ushort index = _inputSignals.Value[4];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[4] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("���������_��_���������")]
        public string SpeedupOn
        {
            get
            {
                ushort index = _inputSignals.Value[5];

                //if (Strings.Forbidden.Count <= index)
                //{
                //    index = (ushort) (Strings.Forbidden.Count - 1);
                //}

                return Strings.Forbidden[index];
            }
            set
            {
                _inputSignals.Value[5] = (ushort)(Strings.Forbidden.IndexOf(value));
            }
        }

        [XmlElement("������������_���������")]
        public ulong SpeedupTime
        {
            get { return Measuring.GetTime(_inputSignals.Value[6]); }
            set { _inputSignals.Value[6] = Measuring.SetTime(value); }
        }

        #endregion

        #region ������� �������

        [XmlElement("����_���������")]
        public string KeyOff
        {
            get
            {
                ushort index = _inputSignals.Value[8];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[8] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("����_��������")]
        public string KeyOn
        {
            get
            {
                ushort index = _inputSignals.Value[9];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[9] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������_���������")]
        public string ExternalOff
        {
            get
            {
                ushort index = _inputSignals.Value[10];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[10] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������_��������")]
        public string ExternalOn
        {
            get
            {
                ushort index = _inputSignals.Value[11];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[ index];
            }
            set { _inputSignals.Value[11] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("������������_�������")]
        public string ConstraintGroup
        {
            get
            {
                ushort index = _inputSignals.Value[12];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[12] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����_���������")]
        public string IndicationReset
        {
            get
            {
                ushort index = _inputSignals.Value[13];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[13] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        #endregion

        #region ������� ����������

        [XmlElement("������_��_������")]
        public string ManageSignalButton
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 0) ? Strings.Forbidden[1] : Strings.Forbidden[0];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? false : true;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 0, bit);
            }
        }

        [XmlElement("������_��_�����")]
        public string ManageSignalKey
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 1) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 1, bit);
            }
        }

        [XmlElement("������_��_��������")]
        public string ManageSignalExternal
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 2) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 2, bit);
            }
        }

        [XmlElement("������_��_����")]
        public string ManageSignalSDTU
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 3) ? Strings.Forbidden[1] : Strings.Forbidden[0];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? false : true;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 3, bit);
            }
        }

        #endregion

        #region ������� ���������� �������

        public const int INPUT_LOGIC_OFFSET = 0x10;

        public class LogicSignalUnitDataTransfer
        {
            [XmlElement("�������")] public List<string> Value = new List<string>();

            public void FillValues(LogicState[] logicStates)
            {
                this.Value.Clear();
                foreach (var logicState in logicStates)
                {
                    this.Value.Add(logicState.ToString());
                }
            }

            public void FillOutValuesFromBitArray(BitArray bitArray)
            {
                this.Value.Clear();
                for (int bitNum = 0; bitNum < bitArray.Count; bitNum++)
                {
                    if (bitArray[bitNum])
                    {
                        this.Value.Add(Strings.OutputSignals[bitNum]);
                    }
                }
            }

        }

        public class InputLogicSignalDataTransfer
        {
            [XmlElement("�1")]
            public LogicSignalUnitDataTransfer And1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�2")]
            public LogicSignalUnitDataTransfer And2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�3")]
            public LogicSignalUnitDataTransfer And3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�4")]
            public LogicSignalUnitDataTransfer And4Signal = new LogicSignalUnitDataTransfer();


            [XmlElement("���1")]
            public LogicSignalUnitDataTransfer Or1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���2")]
            public LogicSignalUnitDataTransfer Or2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���3")]
            public LogicSignalUnitDataTransfer Or3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���4")]
            public LogicSignalUnitDataTransfer Or4Signal = new LogicSignalUnitDataTransfer();

        }

        [XmlElement("�������_����������_�������")]
        public InputLogicSignalDataTransfer InputLogicSignalStruct
        {
            get
            {
                InputLogicSignalDataTransfer toRet = new InputLogicSignalDataTransfer();
                toRet.And1Signal.FillValues(this.GetInputLogicSignals(0));
                toRet.And2Signal.FillValues(this.GetInputLogicSignals(1));
                toRet.And3Signal.FillValues(this.GetInputLogicSignals(2));
                toRet.And4Signal.FillValues(this.GetInputLogicSignals(3));
                toRet.Or1Signal.FillValues(this.GetInputLogicSignals(4));
                toRet.Or2Signal.FillValues(this.GetInputLogicSignals(5));
                toRet.Or3Signal.FillValues(this.GetInputLogicSignals(6));
                toRet.Or4Signal.FillValues(this.GetInputLogicSignals(7));
                return toRet;

            }
            set { }
        }

        public LogicState[] GetInputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[16];
            SetLogicStates(_inputSignals.Value[INPUT_LOGIC_OFFSET + channel*2], true, ref ret);
            SetLogicStates(_inputSignals.Value[INPUT_LOGIC_OFFSET + channel*2 + 1], false, ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < logicSignals.Length; i++)
            {
                if (i < 8)
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        firstWord += (ushort) Math.Pow(2, i);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        firstWord += (ushort) Math.Pow(2, i);
                        firstWord += (ushort) Math.Pow(2, i + 8);
                    }
                }
                else
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        secondWord += (ushort) Math.Pow(2, i - 8);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        secondWord += (ushort) Math.Pow(2, i - 8);
                        secondWord += (ushort) Math.Pow(2, i);
                    }
                }
            }
            _inputSignals.Value[INPUT_LOGIC_OFFSET + channel*2] = firstWord;
            _inputSignals.Value[INPUT_LOGIC_OFFSET + channel*2 + 1] = secondWord;
        }

        private static void SetLogicStates(ushort value, bool firstWord, ref LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = firstWord ? 0 : 8;
            int end = firstWord ? 8 : 16;

            for (int i = start; i < end; i++)
            {
                int pow = firstWord ? i : i - 8;
                logicArray[i] = 0 == ((lowByte) & (byte) (Math.Pow(2, pow))) ? logicArray[i] : LogicState.��;
                logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.��)
                                    ? LogicState.������
                                    : logicArray[i];
            }
        }

        private static bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte) (Math.Pow(2, pow)) == ((hiByte) & (byte) (Math.Pow(2, pow)));
        }

        #endregion

        #endregion

        #region �������� �������

        public class OutputLogicSignalDataTransfer
        {
            [XmlElement("���1")]
            public LogicSignalUnitDataTransfer Vls1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���2")]
            public LogicSignalUnitDataTransfer Vls2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���3")]
            public LogicSignalUnitDataTransfer Vls3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���4")]
            public LogicSignalUnitDataTransfer Vls4Signal = new LogicSignalUnitDataTransfer();


            [XmlElement("���5")]
            public LogicSignalUnitDataTransfer Vls5Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���6")]
            public LogicSignalUnitDataTransfer Vls6Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���7")]
            public LogicSignalUnitDataTransfer Vls7Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���8")]
            public LogicSignalUnitDataTransfer Vls8Signal = new LogicSignalUnitDataTransfer();

        }


        [XmlElement("��������_����������_�������")]
        public OutputLogicSignalDataTransfer OutputLogicSignalStruct
        {
            get
            {
                OutputLogicSignalDataTransfer toRet = new OutputLogicSignalDataTransfer();
                toRet.Vls1Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(0));
                toRet.Vls2Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(1));
                toRet.Vls3Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(2));
                toRet.Vls4Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(3));
                toRet.Vls5Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(4));
                toRet.Vls6Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(5));
                toRet.Vls7Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(6));
                toRet.Vls8Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(7));
                return toRet;

            }
            set { }
        }

        private slot _outputSignals = new slot(0x1120, 0x1140);
        private slot _outputLogic = new slot(0x1150, 0x1178);

        public void LoadOutputSignals()
        {
            LoadSlot(DeviceNumber, _outputLogic, "LoadOutputLogic" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _outputSignals, "LoadOutputSignals" + DeviceNumber, this);
        }

        public void SaveOutputSignals()
        {
            Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, COutputRele.OFFSET, COutputRele.LENGTH);
            Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, COutputIndicator.OFFSET,
                                  COutputIndicator.LENGTH);

            SaveSlot(DeviceNumber, _outputSignals, "SaveOutputSignals" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _outputLogic, "SaveOutputLogicSignals" + DeviceNumber, this);
        }

        #region ���� �������������

        [XmlIgnore]
        [XmlElement("����_�������������")]
        [DisplayName("���� ������������� ������������")]
        [Category("�������� �������")]
        [Editor(typeof (BitsEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray DispepairReleConfiguration
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_outputSignals.Value[0])}); }
            set { _outputSignals.Value[0] = Common.BitsToUshort(value); }
        }

        [XmlElement("����_�������������_������������")]
        [DisplayName("���� ������������� - ������������")]
        [Category("������� �������")]
        public ulong DispepairReleDuration
        {
            get { return Measuring.GetTime(_outputSignals.Value[1]); }
            set { _outputSignals.Value[1] = Measuring.SetTime(value); }
        }

        [XmlIgnore]
        public BitArray DispepairSignal
        {
            get { return new BitArray(new byte[] { Common.LOBYTE(_outputSignals.Value[0]) }); }
            set { _outputSignals.Value[0] = Common.BitsToUshort(value); }
        }

        [XmlElement("�������������")]
        public bool[] DispepairSignalXml
        {
            get { return this.DispepairSignal.Cast<bool>().ToArray(); }
            set { }
        }

        #endregion

        #region �����������

        [XmlElement("�����������_���������")]
        public string SwitcherOff
        {
            get
            {
                ushort index = _outputSignals.Value[2];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[2] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_��������")]
        public string SwitcherOn
        {
            get
            {
                ushort index = _outputSignals.Value[3];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[3] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_������")]
        public string SwitcherError
        {
            get
            {
                ushort index = _outputSignals.Value[4];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[4] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_����������")]
        public string SwitcherBlock
        {
            get
            {
                ushort index = _outputSignals.Value[5];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[5] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_�����")]
        public ulong SwitcherTime
        {
            get { return Measuring.GetTime(_outputSignals.Value[6]); }
            set { _outputSignals.Value[6] = Measuring.SetTime(value); }
        }

        [XmlElement("�����������_�������")]
        public ulong SwitcherImpulse
        {
            get { return Measuring.GetTime(_outputSignals.Value[7]); }
            set { _outputSignals.Value[7] = Measuring.SetTime(value); }
        }

        #endregion

        #region �������� ����

        public class COutputRele : ICollection
        {
            public const int LENGTH = 16;
            public const int COUNT = 8;
            public const int OFFSET = 16;
            private Device _device;

            public COutputRele(Device device)
            {

                SetBuffer(new ushort[LENGTH]);
                foreach (var releItem in _releList)
                {
                    releItem.CurDev = device;
                }
                _device = device;
            }

            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("����������")]
            public int Count
            {
                get { return _releList.Count; }
            }

            public void SetBuffer(ushort[] values)
            {
                _releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", LENGTH,
                                                          "Output rele length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _releList.Add(new OutputReleItem(values[i], values[i + 1], _device));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = _releList[i].Value[0];
                    ret[i*2 + 1] = _releList[i].Value[1];
                }
                return ret;
            }

            public OutputReleItem this[int i]
            {
                get { return _releList[i]; }
                set { _releList[i] = value; }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                _releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _releList.GetEnumerator();
            }

            #endregion
        } ;

        public class OutputReleItem
        {
            private ushort _hiWord;
            private ushort _loWord;
            private Device _device;

            public override string ToString()
            {
                return "�������� ����";
            }

            [XmlAttribute("��������")]
            public ushort[] Value
            {
                get { return new ushort[] {_hiWord, _loWord}; }
                set
                {
                    _hiWord = value[0];
                    _loWord = value[1];
                }
            }

            public OutputReleItem()
            {
            }

            public OutputReleItem(ushort hiWord, ushort loWord, Device device)
            {
                _hiWord = hiWord;
                _loWord = loWord;
                _device = device;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get { return 0 != (_hiWord & 256); }
                set
                {
                    if (value)
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                    }
                    else
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                        _hiWord -= 256;
                    }
                }
            }

            [XmlAttribute("���")]
            public string Type
            {
                get
                {
                    return IsBlinker ? "�������" : "�����������";
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public Device CurDev
            {
                get { return _device; }
                set { _device = value; }
            }
            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    if (_device.DeviceVersion == "2.03H")
                    {
                        ret = Strings.All_V2_03H[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index;
                    if (_device.DeviceVersion == "2.03H")
                    {
                        index = (byte) Strings.All_V2_03H.IndexOf(value);
                    }
                    else
                    {
                        index = (byte) Strings.All.IndexOf(value);
                    }
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("�������")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(_loWord); }
                set { _loWord = Measuring.SetTime(value); }
            }

            [XmlIgnore]
            [Browsable(false)]
            public ushort ImpulseUshort
            {
                get { return _loWord; }
                set { _loWord = value; }
            }
        }

        [XmlElement("��������_����")]
        public COutputRele OutputRele
        {
            get { return _outputRele; }
        }

        #endregion

        #region �������� ����������

        public class COutputIndicator : ICollection
        {
            private Device _device;

            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                _indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _indList.GetEnumerator();
            }

            #endregion

            [DisplayName("����������")]
            public int Count
            {
                get { return _indList.Count; }
            }


            public const int LENGTH = 8;
            public const int COUNT = 4;
            public const int OFFSET = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator(Device device)
            {
                _device = device;
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] values)
            {
                _indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values",  LENGTH,
                                                          "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _indList.Add(new OutputIndicatorItem(values[i], values[i + 1], _device));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = _indList[i].Value[0];
                    ret[i*2 + 1] = _indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get { return _indList[i]; }
                set { _indList[i] = value; }
            }
        } ;

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;
            private Device _device;

            public override string ToString()
            {
                return "�������� ���������";
            }

            [XmlAttribute("��������")]
            public ushort[] Value
            {
                get { return new ushort[] {_hiWord, _loWord}; }
            }

            public OutputIndicatorItem()
            {
            }

            public OutputIndicatorItem(ushort hiWord, ushort loWord, Device mr500)
            {
                _device = mr500;
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get
                {
                    //return 0 != (_hiWord & 256); 
                    return 0 != Common.HIBYTE(_hiWord);
                }
                set
                {
                    if (value)
                    {
                        _hiWord = Common.TOWORD(0xff, Common.LOBYTE(_hiWord));
                    }
                    else
                    {
                        _hiWord = Common.TOWORD(0x0, Common.LOBYTE(_hiWord));
                    }
                }
            }

            [XmlAttribute("���")]
            public string Type
            {
                get
                {
                    //string ret = "";
                    //ret = IsBlinker ? "�������" : "�����������";
                    return IsBlinker ? "�������" : "�����������";
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    if (_device.DeviceVersion == "2.03H")
                    {
                        ret = Strings.All_V2_03H[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index;
                    if (_device.DeviceVersion == "2.03H")
                    {
                        index = (byte) Strings.All_V2_03H.IndexOf(value);
                    }
                    else
                    {
                        index = (byte) Strings.All.IndexOf(value);
                    }
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("���������")]
            public bool Indication
            {
                get { return 0 != (_loWord & 0x1); }
                set { _loWord = value ? (ushort) (_loWord | 0x1) : (ushort) (_loWord & ~0x1); }
            }

            [XmlAttribute("������")]
            public bool Alarm
            {
                get { return 0 != (_loWord & 0x2); }
                set { _loWord = value ? (ushort) (_loWord | 0x2) : (ushort) (_loWord & ~0x2); }
            }

            [XmlAttribute("�������")]
            public bool System
            {
                get { return 0 != (_loWord & 0x4); }
                set { _loWord = value ? (ushort) (_loWord | 0x4) : (ushort) (_loWord & ~0x4); }
            }
        } ;

        [XmlElement("��������_����������")]
        public COutputIndicator OutputIndicator
        {
            get { return _outputIndicator; }
        }

        #endregion

        #region ���������� �������

        public BitArray GetOutputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }

            ushort[] logicBuffer = new ushort[5];
            Array.ConstrainedCopy(_outputLogic.Value, channel*5, logicBuffer, 0, 5);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(Strings.Output.Count);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;
        }

        public void SetOutputLogicSignals(int channel, BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (104 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[5];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i*16 + j < 104)
                    {
                        if (values[i*16 + j])
                        {
                            logicBuffer[i] += (ushort) Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, _outputLogic.Value, channel*5, 5);
        }

        #endregion

        #endregion

        #region ����������

        //���� ����������
        private slot _automaticsPageMain = new slot(0x1000, 0x1018);
        private slot _automaticsPageReserve = new slot(0x1080, 0x1098);
        //private BitArray _bits = new BitArray(8);
        //������� �������� ����������
        public event Handler AutomaticsPageLoadOK;
        public event Handler AutomaticsPageLoadFail;
        public event Handler AutomaticsPageSaveOK;
        public event Handler AutomaticsPageSaveFail;

        private CAutomatics _cautomaticsMain = new CAutomatics();

        [XmlElement("����������_��������")]
        public CAutomatics AutomaticsMain
        {
            get { return _cautomaticsMain; }
            set { _cautomaticsMain = value; }
        }

        private CAutomatics _cautomaticsReserve = new CAutomatics();

        [XmlElement("����������_���������")]
        public CAutomatics AutomaticsReserve
        {
            get { return _cautomaticsReserve; }
            set { _cautomaticsReserve = value; }
        }


        public void LoadAutomaticsPage()
        {
            LoadSlot(DeviceNumber, _automaticsPageReserve, "LoadAutomaticsPageReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _automaticsPageMain, "LoadAutomaticsPageMain" + DeviceNumber, this);
        }

        public void SaveAutomaticsPage()
        {
            SaveSlot(DeviceNumber, _automaticsPageReserve, "SaveAutomaticsPageReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _automaticsPageMain, "SaveAutomaticsPageMain" + DeviceNumber, this);
        }

        public class CAutomatics
        {
            public const int LENGTH = 24;
            private ushort[] _buffer = new ushort[LENGTH];

            public void SetBuffer(ushort[] buffer)
            {
                _buffer = buffer;
            }

            #region ���

            [XmlElement("������������_���")]
            public string APV_Cnf
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[0]);
                    if (index >= Strings.Crat.Count)
                    {
                        index = (byte) (Strings.Crat.Count - 1);
                    }
                    return Strings.Crat[index];
                }
                set { _buffer[0] = Common.TOWORD(Common.HIBYTE(_buffer[0]), (byte) Strings.Crat.IndexOf(value)); }
            }

            [XmlElement("����������_���")]
            public string APV_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[1]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�����_����������_���")]
            public ulong APV_Time_Blocking
            {
                get { return Measuring.GetTime(_buffer[2]); }
                set { _buffer[2] = Measuring.SetTime(value); }
            }

            [XmlElement("�����_����������_���")]
            public ulong APV_Time_Ready
            {
                get { return Measuring.GetTime(_buffer[3]); }
                set { _buffer[3] = Measuring.SetTime(value); }
            }

            [XmlElement("�����_1_�����_���")]
            public ulong APV_Time_1Krat
            {
                get { return Measuring.GetTime(_buffer[4]); }
                set { _buffer[4] = Measuring.SetTime(value); }
            }

            [XmlElement("�����_2_�����_���")]
            public ulong APV_Time_2Krat
            {
                get { return Measuring.GetTime(_buffer[5]); }
                set { _buffer[5] = Measuring.SetTime(value); }
            }

            [XmlElement("������_���_��_��������������")]
            public string APV_Start
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[6]);
                    if (index >= Strings.YesNo.Count)
                    {
                        index = (byte) (Strings.YesNo.Count - 1);
                    }
                    return Strings.YesNo[index];
                }
                set { _buffer[6] = (ushort) Strings.YesNo.IndexOf(value); }
            }

            #endregion

            #region ���

            [XmlElement("����_���")]
            public string ACR_Entry
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[8]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[8] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("����������_���")]
            public string ACR_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[9]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[9] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�������� ���")]
            public ulong ACR_Time
            {
                get { return Measuring.GetTime(_buffer[10]); }
                set { _buffer[10] = Measuring.SetTime(value); }
            }

            #endregion

            #region ����

            [XmlElement("����_����")]
            public string CAPV_Entry
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[11]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[11] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("����������_����")]
            public string CAPV_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[12]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[12] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�������� ����")]
            public ulong CAPV_Time
            {
                get { return Measuring.GetTime(_buffer[13]); }
                set { _buffer[13] = Measuring.SetTime(value); }
            }

            #endregion

            #region ���

            [XmlElement("������_���")]
            public string AVR_StartNumber
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[16]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[16] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("����������_���")]
            public string AVR_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[18]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[18] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�����_�������_���")]
            public ulong AVR_StartTime
            {
                get { return Measuring.GetTime(_buffer[17]); }
                set { _buffer[17] = Measuring.SetTime(value); }
            }

            [XmlElement("�������_���")]
            public string AVR_ReturnNumber
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[19]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[19] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("��������_��������_���")]
            public ulong AVR_ReturnTime
            {
                get { return Measuring.GetTime(_buffer[20]); }
                set { _buffer[20] = Measuring.SetTime(value); }
            }

            [XmlElement("��������_����������_���")]
            public ulong AVR_OffTime
            {
                get { return Measuring.GetTime(_buffer[21]); }
                set { _buffer[21] = Measuring.SetTime(value); }
            }

            #endregion
        }

        #endregion

        #region ����

        private slot _diagnostic = new slot(0x1900, 0x190D);
        private Oscilloscope _oscilloscope;

        private slot _analog = new slot(0x1800, 0x180B);
        private MemoryEntity<OneWordStruct> _resetOsc;
        public MemoryEntity<OneWordStruct> ResetOsc
        {
            get { return this._resetOsc; }
        }

        private COutputRele _outputRele;
        private COutputIndicator _outputIndicator;
        #endregion

        #region �������

        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;
        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler TokDefensesLoadOK;
        public event Handler TokDefensesLoadFail;
        public event Handler TokDefensesSaveOK;
        public event Handler TokDefensesSaveFail;

        public event Handler AnalogSignalsLoadOK;
        public event Handler AnalogSignalsLoadFail;
        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;

        public event Handler ProgramSaveOk;
        public event Handler ProgramSaveFail;
        public event Handler ProgramStartSaveOk;
        public event Handler ProgramStartSaveFail;
        public event Handler ProgramStorageSaveOk;
        public event Handler ProgramStorageSaveFail;
        public event Handler ProgramStorageLoadOk;
        public event Handler ProgramStorageLoadFail;
        public event Handler ProgramSignalsLoadOk;
        public event Handler ProgramSignalsLoadFail;

        #endregion

        public Oscilloscope Oscilloscope
        {
            get { return _oscilloscope; }
        }

        #region ������������ �������������
        MR500V2 _TZLDevice;

        public MR500V2 TZLDevice
        {
            get { return _TZLDevice; }
        }

        public MR500()
        {
            HaveVersion = true;
        }

        public MR500(Modbus mb)
        {
            MB = mb;
            Init();
            
            _outputIndicator = new COutputIndicator(this);
            _outputRele = new COutputRele(this);
            this._resetOsc = new MemoryEntity<OneWordStruct>("����� �������������",this, 0x4000);
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                _TZLDevice = new MR500V2(this);

                this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0xB000);
                this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0001);
                this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
                this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0xA000);
                this._startStopSpl = new MemoryEntity<OneWordStruct>("������/������� ���", this, 0x1808);
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }
        
        private void Init()
        {
            HaveVersion = true;
            _alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2000;
            ushort size = 0x8;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                _systemJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x10;
            }
            start = 0x2800;
            size = 20;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                _alarmJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x40;
            }
            _oscilloscope = new Oscilloscope(this);
            _oscilloscope.Initialize();
        }
        
        #endregion

        #region ���������� �������

        [Category("���������� �������")]
        public double Ig
        {
            get { return Measuring.GetI(_analog.Value[0], TTNP, false); }
        }

        [Category("���������� �������")]
        public double Io
        {
            get { return Measuring.GetI(_analog.Value[1], TTNP, false); }
        }

        [Category("���������� �������")]
        public double Ia
        {
            get { return Measuring.GetI(_analog.Value[2], TT, true); }
        }

        [Category("���������� �������")]
        public double Ib
        {
            get { return Measuring.GetI(_analog.Value[3], TT, true); }
        }

        [Category("���������� �������")]
        public double Ic
        {
            get { return Measuring.GetI(_analog.Value[4], TT, true); }
        }

        [Category("���������� �������")]
        public double I0
        {
            get { return Measuring.GetI(_analog.Value[5], TT, true); }
        }

        [Category("���������� �������")]
        public double I1
        {
            get { return Measuring.GetI(_analog.Value[6], TT, false); }
        }

        [Category("���������� �������")]
        public double I2
        {
            get { return Measuring.GetI(_analog.Value[7], TT, false); }
        }

        #endregion

        #region ���������� �������

        private slot _constraintGroup = new slot(0x0400, 0x0401);

        public void SelectConstraintGroup(bool value)
        {
            _constraintGroup.Value[0] = value ? (ushort) 0 : (ushort) 1;
            SaveSlot(DeviceNumber, _constraintGroup, "��500�" + DeviceNumber + ": ����� ������ �������", this);
        }


        [XmlIgnore]
        [DisplayName("����������� �������")]
        [Description("����������� ������� ����")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray ManageSignals
        {
            get
            {
                BitArray ret = new BitArray(7);
                int j = 0;
                for (int i = 1; i < 8; i++)
                {
                    if (4 == i)
                    {
                        i++;
                    }
                    ret[j++] = Common.GetBit(_diagnostic.Value[0], i);
                }

                return ret;
            }
        }

        [XmlIgnore]
        [DisplayName("����������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Indicators
        {
            get
            {
                BitArray ret = new BitArray(new byte[] {Common.LOBYTE(_diagnostic.Value[6])});
                ret[4] = !ret[4];
                ret[5] = !ret[5];
                return ret;
            }
        }

        [XmlIgnore]
        [DisplayName("������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[]
                                        {
                                            Common.HIBYTE(_diagnostic.Value[8]),
                                            Common.LOBYTE(_diagnostic.Value[9]),
                                            Common.HIBYTE(_diagnostic.Value[9])
                                        });
            }
        }

        [XmlIgnore]
        [DisplayName("����")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Rele
        {
            get { return new BitArray(new byte[] {Common.HIBYTE(_diagnostic.Value[6])}); }
        }

        [XmlIgnore]
        [DisplayName("�������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray OutputSignals
        {
            get { return new BitArray(new byte[] {
                Common.LOBYTE(_diagnostic.Value[12])
            }); }
        }

        [XmlIgnore]
        [DisplayName("������� ��������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]
                                        {
                                            Common.LOBYTE(_diagnostic.Value[10]),
                                            Common.HIBYTE(_diagnostic.Value[10])
                                        });
            }
        }

        [XmlIgnore]
        [DisplayName("��������� �������������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray FaultState
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_diagnostic.Value[4])}); }
        }

        [XmlIgnore]
        [DisplayName("������� �������������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray FaultSignals
        {
            get
            {
                return
                    new BitArray(new byte[] {Common.HIBYTE(_diagnostic.Value[4]), Common.LOBYTE(_diagnostic.Value[5])});
            }
        }

        [XmlIgnore]
        [DisplayName("������� ��")]
        [Category("C������ ������� �����")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray ExternalSignals
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_diagnostic.Value[11])}); }
        }

        [XmlIgnore]
        [DisplayName("������� ����������")]
        [Category("C������ ������� �����")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray AutomaticSignals
        {
            get { return new BitArray(new byte[] {Common.HIBYTE(_diagnostic.Value[12])}); }
        }

        #endregion

        #region ������� ������

        private slot _externalDefensesMain = new slot(0x1020, 0x1040);
        private slot _externalDefensesReserve = new slot(0x10A0, 0x10C0);


        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, _externalDefensesMain, "LoadExternalDefenses1" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _externalDefensesReserve, "LoadExternalDefenses2" + DeviceNumber, this);
        }

        public void SaveExternalDefenses()
        {
            Array.ConstrainedCopy(ExternalDefensesMain.ToUshort(), 0, _externalDefensesMain.Value, 0,
                                  CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(ExternalDefensesReserve.ToUshort(), 0, _externalDefensesReserve.Value, 0,
                                  CExternalDefenses.LENGTH);

            SaveSlot(DeviceNumber, _externalDefensesMain, "SaveExternalDefenses1" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _externalDefensesReserve, "SaveExternalDefenses2" + DeviceNumber, this);
        }

        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 0x20;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(ExternalDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", LENGTH,
                                                          "External defense length must be 48");
                }
                for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                    _defenseList.Add(new ExternalDefenseItem(temp));
                }
                for (int i = 0; i < COUNT; i++)
                {
                    this[i].Name = "�� - " + (i + 1);
                }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return _defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];

                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i*ExternalDefenseItem.LENGTH,
                                          ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ExternalDefenseItem this[int i]
            {
                get { return _defenseList[i]; }
                set { _defenseList[i] = value; }
            }
        } ;

        public class ExternalDefenseItem
        {
            public const int LENGTH = 4;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem()
            {
            }

            public ExternalDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            public override string ToString()
            {
                return "������� ������";
            }

            private string _name;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }


            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value",  LENGTH,
                                                          "External defense item length must be 6");
                }
                _values = buffer;
            }

            [XmlAttribute("�����")]
            public string Mode
            {
                get
                {
                    byte index = (byte) (Common.LOBYTE(_values[0]) & 0x0F);
                    if (index >= Strings.Modes2.Count)
                    {
                        index = (byte) (Strings.Modes2.Count - 1);
                    }
                    return Strings.Modes2[index];
                }
                set { _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes2.IndexOf(value), 0, 1, 2); }
            }

            [XmlAttribute("����������_�����")]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[2] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { _values[2] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("����")]
            public string Entry
            {
                get
                {
                    ushort value = (ushort) (_values[1] & 0xFF);
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { _values[1] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("������������_�����")]
            public ulong WorkingTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("���")]
            public bool APV
            {
                get { return Common.GetBit(_values[0], 14); }
                set { _values[0] = Common.SetBit(_values[0], 14, value); }
            }

            [XmlAttribute("����")]
            public bool UROV
            {
                get { return Common.GetBit(_values[0], 15); }
                set { _values[0] = Common.SetBit(_values[0], 15, value); }
            }
        }

        private CExternalDefenses _cexternalDefensesMain = new CExternalDefenses();
        private CExternalDefenses _cexternalDefensesResereve = new CExternalDefenses();

        [XmlElement("�������_������_��������")]
        public CExternalDefenses ExternalDefensesMain
        {
            get { return _cexternalDefensesMain; }
            set { _cexternalDefensesMain = value; }
        }

        [XmlElement("�������_������_���������")]
        public CExternalDefenses ExternalDefensesReserve
        {
            get { return _cexternalDefensesResereve; }
            set { _cexternalDefensesResereve = value; }
        }

        #endregion

        #region ���� - �����

        private slot _datetime = new slot(0x200, 0x207);

        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, _datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        [Browsable(false)]
        public byte[] DateTime
        {
            get { return Common.TOBYTES(_datetime.Value, true); }
            set { _datetime.Value = Common.TOWORDS(value, true); }
        }

        #endregion

        #region ������� ��������/����������

        public void LoadDiagnostic()
        {
            LoadBitSlot(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, this);
        }

        public void LoadDiagnosticCycle()
        {
            LoadSlotCycle(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignals()
        {
            LoadSlot(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, this);
        }

        public void RemoveDiagnostic()
        {
            MB.RemoveQuery("LoadDiagnostic" + DeviceNumber);
        }

        public void RemoveAnalogSignals()
        {
            MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void RemoveDateTime()
        {
            MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        public void SuspendSystemJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopAlarmJournal = true;
        }

        public void RemoveSystemJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopSystemJournal = true;
        }

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, _datetime, "SaveDateTime" + DeviceNumber, this);
        }

        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, _systemJournal[0], "LoadSJRecord0_" + DeviceNumber, this);
            _stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            if (!_inputSignals.Loaded)
            {
                LoadInputSignals();
            }
            _stopAlarmJournal = false;
            LoadSlot(DeviceNumber, _alarmJournal[0], "LoadALRecord0_" + DeviceNumber, this);
        }

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0, true, "��500 ������ �������������", this);
        }

        #endregion

        #region ������������

        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ��500 ���������", binFileName);
            }

            try
            {
                DeserializeSlot(doc, "/��500_�������/����_�����", _datetime);
                DeserializeSlot(doc, "/��500_�������/�������_�������", _inputSignals);
                DeserializeSlot(doc, "/��500_�������/��������_�������", _outputSignals);
                DeserializeSlot(doc, "/��500_�������/��������_�������_����������_�������", _outputLogic);
                DeserializeSlot(doc, "/��500_�������/�����������", _diagnostic);
                DeserializeSlot(doc, "/��500_�������/������_�������_��������", _externalDefensesMain);
                DeserializeSlot(doc, "/��500_�������/������_�������_���������", _externalDefensesReserve);
                DeserializeSlot(doc, "/��500_�������/����������_��������", _automaticsPageMain);
                DeserializeSlot(doc, "/��500_�������/����������_���������", _automaticsPageReserve);
                DeserializeSlot(doc, "/��500_�������/�������_��������", _tokDefensesMain);
                DeserializeSlot(doc, "/��500_�������/�������_���������", _tokDefensesReserve);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��500 ���������", binFileName);
            }


            ExternalDefensesMain.SetBuffer(_externalDefensesMain.Value);
            ExternalDefensesReserve.SetBuffer(_externalDefensesReserve.Value);
            AutomaticsMain.SetBuffer(_automaticsPageMain.Value);
            AutomaticsReserve.SetBuffer(_automaticsPageReserve.Value);
            TokDefensesMain.SetBuffer(_tokDefensesMain.Value);
            TokDefensesReserve.SetBuffer(_tokDefensesReserve.Value);

            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
            OutputRele.SetBuffer(releBuffer);
            ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, COutputIndicator.OFFSET, outputIndicator, 0,
                                  COutputIndicator.LENGTH);
            OutputIndicator.SetBuffer(outputIndicator);
        }

        private static void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void Serialize(string binFileName)
        {
        /*    string xmlFileName = Path.ChangeExtension(binFileName, ".xml");

            XmlSerializer ser = new XmlSerializer(GetType());
            TextWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
            ser.Serialize(writer, this);
            writer.Close();*/


            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("��500_�������"));

            Array.ConstrainedCopy(ExternalDefensesMain.ToUshort(), 0, _externalDefensesMain.Value, 0,
                                  CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(ExternalDefensesReserve.ToUshort(), 0, _externalDefensesReserve.Value, 0,
                                  CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, COutputRele.OFFSET, COutputRele.LENGTH);
            Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, COutputIndicator.OFFSET,
                                  COutputIndicator.LENGTH);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH);

           // SerializeSlot(doc, "������", _info);
            SerializeSlot(doc, "����_�����", _datetime);
            SerializeSlot(doc, "�������_�������", _inputSignals);
            SerializeSlot(doc, "��������_�������", _outputSignals);
            SerializeSlot(doc, "��������_�������_����������_�������", _outputLogic);
            SerializeSlot(doc, "�����������", _diagnostic);
            SerializeSlot(doc, "������_�������_��������", _externalDefensesMain);
            SerializeSlot(doc, "������_�������_���������", _externalDefensesReserve);
            SerializeSlot(doc, "����������_��������", _automaticsPageMain);
            SerializeSlot(doc, "����������_���������", _automaticsPageReserve);
            SerializeSlot(doc, "�������_��������", _tokDefensesMain);
            SerializeSlot(doc, "�������_���������", _tokDefensesReserve);


            doc.Save(binFileName);
        }

        private static void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }

        #endregion

        #region ������� ������

        private slot _tokDefensesMain = new slot(0x1040, 0x1080);
        //private slot _tokDefenses2Main = new slot(0x1060, 0x1080);
        private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
        //private slot _tokDefenses2Reserve = new slot(0x10E0, 0x1100);

        public void LoadTokDefenses()
        {
            LoadSlot(DeviceNumber, _tokDefensesReserve, "LoadTokDefensesReserve" + DeviceNumber, this);
            //LoadSlot(DeviceNumber, _tokDefenses2Reserve, "LoadTokDefensesReserve2" + DeviceNumber);
            LoadSlot(DeviceNumber, _tokDefensesMain, "LoadTokDefensesMain" + DeviceNumber, this);
            //LoadSlot(DeviceNumber, _tokDefenses2Main, "LoadTokDefensesMain2" + DeviceNumber);
        }

        public void SaveTokDefenses()
        {
            Array.ConstrainedCopy(TokDefensesMain.ToUshort(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH);

            SaveSlot(DeviceNumber, _tokDefensesReserve, "SaveTokDefensesReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _tokDefensesMain, "SaveTokDefensesMain" + DeviceNumber, this);
        }

        private CTokDefenses _ctokDefensesMain = new CTokDefenses();
        private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

        [XmlElement("�������_������_��������")]
        [DisplayName("�������� ������")]
        [Description("������� ������ - �������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CTokDefenses TokDefensesMain
        {
            get { return _ctokDefensesMain; }
            set { _ctokDefensesMain = value; }
        }

        [XmlElement("�������_������_���������")]
        [DisplayName("��������� ������")]
        [Description("������� ������ - ��������� ������ �������")]
        [Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("������� ������")]
        public CTokDefenses TokDefensesReserve
        {
            get { return _ctokDefensesReserve; }
            set { _ctokDefensesReserve = value; }
        }


        public class CTokDefenses : ICollection
        {
            public const int LENGTH = 0x40;
            public const int COUNT = 8;

            #region ICollection Members

            [DisplayName("����������")]
            public int Count
            {
                get { return _items.Count; }
            }

            public void Add(TokDefenseItem item)
            {
                _items.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _items.GetEnumerator();
            }

            #endregion

            private List<TokDefenseItem> _items = new List<TokDefenseItem>(12);

            public CTokDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public TokDefenseItem this[int i]
            {
                get { return _items[i]; }
                set { _items[i] = value; }
            }

            public void SetBuffer(ushort[] buffer)
            {
                _items.Clear();
                for (int i = 0; i < buffer.Length; i += TokDefenseItem.LENGTH)
                {
                    ushort[] item = new ushort[TokDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, item, 0, TokDefenseItem.LENGTH);
                    _items.Add(new TokDefenseItem(item));
                }

                _items[0].Name = "I>";
                _items[0].IsI = true;
                _items[1].Name = "I>>";
                _items[1].IsI = true;
                _items[2].Name = "I>>>";
                _items[2].IsI = true;
                _items[3].Name = "I>>>>";
                _items[3].IsI = true;
                _items[4].Name = "I0>";
                _items[4].IsI0 = true;
                _items[5].Name = "I0>>";
                _items[5].IsI0 = true;
                _items[6].Name = "I2>";
                _items[6].IsI2 = true;
                _items[7].Name = "I2>>";
                _items[7].IsI2 = true;
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];

                for (int i = 0; i < COUNT; i++)
                {
                    Array.ConstrainedCopy(_items[i].Values, 0, buffer, i*TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                }
                return buffer;
            }

            //public ushort[] ToUshort2()
            //{
            //    ushort[] buffer = new ushort[LENGTH / 2];
            //    for (int i = (COUNT / 2); i < COUNT; i++)
            //    {
            //        Array.ConstrainedCopy(_items[i].Values, 0, buffer, i * TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
            //    }
            //    return buffer;
            //}
        } ;

        public class TokDefenseItem
        {
            public const int LENGTH = 8;
            private bool _isI = false;
            private bool _isI0 = false;
            private bool _isI2 = false;

            [XmlIgnore]
            [Browsable(false)]
            public bool IsI2
            {
                get { return _isI2; }
                set { _isI2 = value; }
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsI0
            {
                get { return _isI0; }
                set { _isI0 = value; }
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsI
            {
                get { return _isI; }
                set { _isI = value; }
            }


            private string _name;
            private ushort[] _values = new ushort[LENGTH];

            public TokDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            public TokDefenseItem()
            {

            }
                

            [XmlIgnore]
            [Browsable(false)]
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Tok defense increase1 item value",  LENGTH,
                                                          "Tok defense increase1 item length must be 6");
                }
                _values = buffer;
            }

            [XmlAttribute("��������")]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            [XmlAttribute("�����")]
            public string Mode
            {
                get
                {
                    byte index = (byte) (Common.LOBYTE(_values[0]) & 0xF);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set 
                { 
                    _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2); 
                }
            }

            [XmlAttribute("����������_�����")]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set 
                { 
                    _values[1] = (ushort) Strings.Logic.IndexOf(value); 
                }
            }

            private static string GetStringDataValue(int index, List<string> stringList)
            {
                if (index >= stringList.Count)
                {
                    index = stringList.Count - 1;
                }
                return stringList[index];
            }

            [XmlAttribute("��� �������")]
            [Browsable(false)]
            public string Signal
            {
                get
                {
                    string ret = "";
                    int index = Common.GetBits(_values[0], new int[] {4, 5}) >> 4;
                    if (IsI)
                    {
                        ret = GetStringDataValue(index, Strings.TokSignalI);
                    }
                    else if (IsI0)
                    {
                        ret = GetStringDataValue(index, Strings.TokSignalI0);
                    }
                    else if (IsI2)
                    {
                        ret = GetStringDataValue(index, Strings.TokSignalI2);
                    }
                    return ret;
                }
                set
                {
                    if (IsI)
                    {
                        _values[0] =
                            Common.SetBits(_values[0], (ushort) Strings.TokSignalI.IndexOf(value), new int[] {4, 5});
                    }
                    else if (IsI0)
                    {
                        _values[0] =
                            Common.SetBits(_values[0], (ushort) Strings.TokSignalI0.IndexOf(value), new int[] {4, 5});
                    }
                    else if (IsI2)
                    {
                        _values[0] =
                            Common.SetBits(_values[0], (ushort) Strings.TokSignalI2.IndexOf(value), new int[] {4, 5});
                    }
                }
            }

            [XmlAttribute("������������_�������")]
            public double WorkConstraint
            {
                get
                {
                    return Measuring.GetConstraint(_values[2], ConstraintKoefficient.K_4001);
                }
                set
                {
                    _values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
                }
            }

            [XmlAttribute("��������������")]
            public string Feature
            {
                get
                {
                    string ret;
                    if (IsI)
                    {
                        ret = Common.GetBit(_values[0], 12) ? Strings.FeatureI[1] : Strings.FeatureI[0];
                    }
                    else
                    {
                        ret = "�����������";
                    }

                    return ret;
                }
                set
                {
                    if (IsI)
                    {
                        _values[0] = Common.SetBits(_values[0], (ushort) Strings.FeatureI.IndexOf(value), 12);
                    }
                    else
                    {
                        _values[0] = Common.SetBits(_values[0], 0, 12);
                    }
                }
            }


            [XmlAttribute("��������")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("�����������")]
            public ushort Koefficient
            {
                get { return _values[3]; }
                set { _values[3] = value; }
            }

            [XmlAttribute("���")]
            public bool APV
            {
                get { return Common.GetBit(_values[0], 14); }
                set { _values[0] = Common.SetBit(_values[0], 14, value); }
            }

            [XmlAttribute("����")]
            public bool UROV
            {
                get { return Common.GetBit(_values[0], 15); }
                set { _values[0] = Common.SetBit(_values[0], 15, value); }
            }

            [XmlAttribute("���������")]
            public bool Speedup
            {
                get { return Common.GetBit(_values[0], 13); }
                set { _values[0] = Common.SetBit(_values[0], 13, value); }
            }

            [XmlAttribute("���������_�����")]
            public ulong SpeedupTime
            {
                get { return Measuring.GetTime(_values[4]); }
                set { _values[4] = Measuring.SetTime(value); }
            }
        } ;

        #endregion

        #region Programming
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _startStopSpl;
 
        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return _programStorageStruct; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return _programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return _programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return _sourceProgramStruct; }
        }

        public MemoryEntity<OneWordStruct> StartStopSpl
        {
            get { return _startStopSpl; }
        }
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (MR500); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr500; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��500"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        public const ulong TIMELIMIT = 3000000;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]";
        private const string LOW_VERSIONS = "1.00 - 2.03";
        private const string MIDDLE_VERSIONS = "2.04 - 2.08";
        private const string HI_VERSIONS = "3.00 - 3.07";
        
        #region MR500 2.0

        public class MR500V2
        {
            private MemoryEntity<SystemJournalStruct> _systemJournalNew;
            private MemoryEntity<AlarmJournalStruct> _alarmJournalNew;
            private MemoryEntity<MeasuringTransformer> _measureTransAj;
            
            public MemoryEntity<MeasuringTransformer> MeasureTransAj
            {
                get { return _measureTransAj; }
            }


            MR500 _device;

            public MR500V2() 
            {
                this.Init();
            }

            public MR500V2(MR500 device)
            {
                _device = device;
                this._systemJournalNew = new MemoryEntity<SystemJournalStruct>("��", device, 0x2000);
                this._alarmJournalNew = new MemoryEntity<AlarmJournalStruct>("��", device, 0x2800);
                this._measureTransAj = new MemoryEntity<MeasuringTransformer>("��������� ��������� ��", device, 0x1000);
                
                this.Init();
            }

            public MemoryEntity<SystemJournalStruct> SystemJournalNew
            {
                get { return this._systemJournalNew; }
            }

            public MemoryEntity<AlarmJournalStruct> AlarmJournalNew
            {
                get { return this._alarmJournalNew; }
            }

            private void Init()
            {
                _alarmJournalRecords = new CAlarmJournal(_device);
                ushort start = 0x2000;
                ushort size = 0x8;
                for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
                {
                    _systemJournal[i] = new slot((ushort)start, (ushort)(start + size));
                    start += 0x10;
                }
                start = 0x2800;
                size = 0x1C;
                for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
                {
                    _alarmJournal[i] = new slot(start, (ushort)(start + size));
                    start += 0x40;
                }
                _oscilloscope = new Oscilloscope(_device);
                _oscilloscope.Initialize();
                _outputRele = new COutputRele(_device);

            }



            private string vers = string.Empty;
            #region D_Version
            [XmlElement("������_����������")]
            [DisplayName("������")]
            [Description("������ ����������")]
            [System.ComponentModel.Category("������")]
            public string D_Version
            {
                get
                {
                    return vers;
                }
                set
                {
                    vers = value;
                }
            }
            #endregion

            #region ���������
            public const ulong TIMELIMIT = 3000000;
            public const ulong KZLIMIT = 4000;
            public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]";
            public const string KZLIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 4000]";

            #endregion

            #region ��������� ������

            public const int SYSTEMJOURNAL_RECORD_CNT = 128;
            [Browsable(false)]
            [XmlIgnore]
            public CSystemJournal SystemJournal
            {
                get { return _systemJournalRecords; }
                set { _systemJournalRecords = value; }
            }

            public struct SystemRecord
            {
                public string msg;
                public string time;

            };

            public class CSystemJournal : ICollection
            {

                private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

                public int Count
                {
                    get
                    {
                        return _messages.Count;
                    }
                }

                public SystemRecord this[int i]
                {
                    get
                    {
                        return (SystemRecord)(_messages[i]);
                    }

                }

                public bool IsMessageEmpty(int i)
                {
                    return "������ ����" == _messages[i].msg;
                }

                public bool AddMessage(ushort[] value)
                {
                    bool ret = true;
                    //Common.SwapArrayItems(ref value);
                    SystemRecord msg = CreateMessage(value);
                    if ("������ ����" == msg.msg)
                    {
                        ret = false;
                    }
                    else
                    {
                        ret = true;
                        _messages.Add(msg);
                    }
                    return ret;

                }

                private SystemRecord CreateMessage(ushort[] value)
                {
                    SystemRecord msg = new SystemRecord();

                    switch (value[0])
                    {
                        case 0:
                            msg.msg = "������ ����";
                            break;
                        case 1:
                        case 2:
                            msg.msg = "������ �������� ������";
                            break;
                        case 3:
                            msg.msg = "������������� ��. ����";
                            break;
                        case 4:
                            msg.msg = "��. ���� ��������";
                            break;
                        case 5:
                            msg.msg = "����������� ���� �����";
                            break;
                        case 6:
                            msg.msg = "����������� � �����";
                            break;
                        case 7:
                            msg.msg = "������";
                            break;
                        case 8:
                            msg.msg = "������";
                            break;
                        case 9:
                            msg.msg = "��� ���������� (����)";
                            break;
                        case 10:
                            msg.msg = "��� �������� (����)";
                            break;
                        case 11:
                            msg.msg = "��� ����������";
                            break;
                        case 12:
                            msg.msg = "��� ��������";
                            break;
                        case 13:
                            msg.msg = "���1 ����������";
                            break;
                        case 14:
                            msg.msg = "���1 ��������";
                            break;
                        case 15:
                            msg.msg = "���2 ����������";
                            break;
                        case 16:
                            msg.msg = "���2 ��������";
                            break;
                        case 17:
                            msg.msg = "������ ����������� ����� �������";
                            break;
                        case 18:
                        case 19:
                            msg.msg = "������ ����������� ����� ������";
                            break;
                        case 20:
                            msg.msg = "������ ������� �������";
                            break;
                        case 21:
                            msg.msg = "������ ������� ������";
                            break;
                        case 22:
                            msg.msg = "��������� �����";
                            break;
                        case 23:
                            msg.msg = "������";
                            break;
                        case 24:
                            msg.msg = "���� - ������� ����������";
                            break;
                        case 25:
                            msg.msg = "���� - ������� ��������";
                            break;
                        case 26:
                            msg.msg = "������ �������";
                            break;
                        case 27:
                            msg.msg = "����� ������� �������";
                            break;
                        case 28:
                            msg.msg = "����� ������� ������";
                            break;
                        case 29:
                            msg.msg = "����� ������� �����������";
                            break;
                        case 30:
                            msg.msg = "����� ���������";
                            break;
                        case 31:
                            msg.msg = "�������� ������ �������";
                            break;
                        case 32:
                            msg.msg = "���� � ������� ��������";
                            break;
                        case 33:
                            msg.msg = "������ ��������� ����������";
                            break;
                        case 34:
                            msg.msg = "������� ����������";
                            break;
                        case 35:
                            msg.msg = "���������� ���������";
                            break;
                        case 36:
                            msg.msg = "���������� ��������";
                            break;
                        case 37:
                            msg.msg = "���� ��������";
                            break;
                        case 38:
                            msg.msg = "���� ����� ������������ ";
                            break;
                        case 39:
                            msg.msg = "���� � ����� ������������ ";
                            break;
                        case 40:
                            msg.msg = "����������� ������ ���������� ";
                            break;
                        case 41:
                            msg.msg = "��� ���������";
                            break;
                        case 42:
                            msg.msg = "��� - ������� ����������";
                            break;
                        case 43:
                            msg.msg = "�������� ����";
                            break;
                        case 44:
                        case 45:
                            msg.msg = "������";
                            break;
                        case 46:
                            msg.msg = "�������� ���  Iabc";
                            break;
                        case 47:
                            msg.msg = "������ ���  Iabc";
                            break;
                        case 48:
                            msg.msg = "�����������  Iabc";
                            break;
                        case 49:
                            msg.msg = "���������  Iabc";
                            break;
                        case 50:
                        case 51:
                        case 52:
                        case 53:
                        case 54:
                        case 55:
                        case 56:
                        case 57:
                        case 58:
                        case 59:
                        case 60:
                        case 61:
                            msg.msg = "������";
                            break;
                        case 62:
                            msg.msg = "����������� ��������";
                            break;
                        case 63:
                            msg.msg = "����������� �������";
                            break;
                        case 64:
                            msg.msg = "���������� �����������";
                            break;
                        case 65:
                            msg.msg = "����� �����������";
                            break;
                        case 66:
                            msg.msg = "������������� �����������";
                            break;
                        case 67:
                            msg.msg = "����. ������. �����������";
                            break;
                        case 68:
                            msg.msg = "������. �����. �����������";
                            break;
                        case 69:
                            msg.msg = "������ ����";
                            break;
                        case 70:
                            msg.msg = "���� ���";
                            break;
                        case 71:
                            msg.msg = "������ ���������";
                            break;
                        case 72:
                            msg.msg = "��� �����������";
                            break;
                        case 73:
                            msg.msg = "��� ��.����������";
                            break;
                        case 74:
                            msg.msg = "������ ��� 1 ����";
                            break;
                        case 75:
                            msg.msg = "������ ��� 2 ����";
                            break;
                        case 76:
                            msg.msg = "������ ��� 3 ����";
                            break;
                        case 77:
                            msg.msg = "������ ��� 4 ����";
                            break;
                        case 78:
                            msg.msg = "��� ��������";
                            break;
                        case 79:
                            msg.msg = "��� �����������";
                            break;
                        case 80:
                            msg.msg = "������";
                            break;
                        case 81:
                            msg.msg = "������ ����";
                            break;
                        case 82:
                            msg.msg = "���� �����������";
                            break;
                        case 83:
                            msg.msg = "���� - ������� ����������";
                            break;
                        case 84:
                            msg.msg = "���� ��������";
                            break;
                        case 85:
                            msg.msg = "A�� ����������";
                            break;
                        case 86:
                            msg.msg = "��� ����. ����������";
                            break;
                        case 87:
                            msg.msg = "��� ����������";
                            break;
                        case 88:
                            msg.msg = "��� ���������";
                            break;
                        case 89:
                            msg.msg = "��� ��������";
                            break;
                        case 90:
                            msg.msg = "��� ���. ������";
                            break;
                        case 91:
                            msg.msg = "��� ����. ������";
                            break;
                        case 92:
                            msg.msg = "��� ������ �� ������";
                            break;
                        case 93:
                            msg.msg = "��� ������ ������� ����.";
                            break;
                        case 94:
                            msg.msg = "��� ������ �� �������";
                            break;
                        case 95:
                            msg.msg = "��� ������ ��������.";
                            break;
                        case 96:
                            msg.msg = "������ ���������";
                            break;
                        case 97:
                            msg.msg = "������ ��������";
                            break;
                        case 98:
                            msg.msg = "���� ���������";
                            break;
                        case 99:
                            msg.msg = "���� ��������";
                            break;
                        case 100:
                            msg.msg = "������� ���������";
                            break;
                        case 101:
                            msg.msg = "������� ��������";
                            break;
                        case 102:
                            msg.msg = "���� ���������";
                            break;
                        case 103:
                            msg.msg = "���� ��������";
                            break;
                        case 104:
                            msg.msg = "�������� �������";
                            break;
                        case 105:
                            msg.msg = "��������� �������";
                            break;
                        case 106:
                            msg.msg = "����. ������. �������";
                            break;
                        case 108:
                            msg.msg = "���� - �������� �������";
                            break;
                        case 109:
                            msg.msg = "���� - ��������� �������";
                            break;
                        case 110:
                            msg.msg = "���� - �������� �������";
                            break;
                        case 111:
                            msg.msg = "���� - ��������� �������";
                            break;
                        case 112:
                            msg.msg = "��� �������";
                            break;
                        case 113:
                        case 114:
                        case 115:
                        case 116:
                        case 117:
                        case 118:
                        case 119:
                        case 120:
                        case 121:
                        case 122:
                        case 123:
                        case 124:
                            msg.msg = "������";
                            break;
                        case 125:
                            msg.msg = "��� ������� ��-1";
                            break;
                        case 126:
                            msg.msg = "��� ������� ��-2";
                            break;
                        case 127:
                            msg.msg = "��� ������� ��-3";
                            break;
                        case 128:
                            msg.msg = "��� ������� ��-4";
                            break;
                        case 129:
                            msg.msg = "��� ������� ��-5";
                            break;
                        case 130:
                            msg.msg = "��� ������� ��-6";
                            break;
                        case 131:
                            msg.msg = "��� ������� ��-7";
                            break;
                        case 132:
                            msg.msg = "��� ������� ��-8";
                            break;
                        case 133:
                        case 134:
                            msg.msg = "������";
                            break;
                        case 135:
                            msg.msg = "��� ���� ����������";
                            break;
                        case 136:
                            msg.msg = "��� ���� ����������";
                            break;
                        case 137:
                            msg.msg = "����: ������ ��������";
                            break;
                        case 138:
                            msg.msg = "����: ������ ������";
                            break;
                        case 139:
                            msg.msg = "����: ������ ������";
                            break;
                        case 140:
                            msg.msg = "����: ��������� ������";
                            break;
                        case 141:
                            msg.msg = "����: ��������� ������";
                            break;
                        case 142:
                            msg.msg = "������ ������ �� ������";
                            break;
                        case 143:
                            msg.msg = "������ ������ ���� ���";
                            break;
                        case 144:
                            msg.msg = "������ ������ ������";
                            break;
                        case 145:
                            msg.msg = "������ ������ �������";
                            break;
                        case 146:
                            msg.msg = "������ ������ ��������";
                            break;
                        case 147:
                            msg.msg = "������ ������� ���� ";
                            break;
                        case 148:
                            msg.msg = "������";
                            break;
                        case 149:
                            msg.msg = "��������� ��� � 1";
                            break;
                        case 150:
                            msg.msg = "��������� ��� � 2";
                            break;
                        case 151:
                            msg.msg = "��������� ��� � 3";
                            break;
                        case 152:
                            msg.msg = "��������� ��� � 4";
                            break;
                        case 153:
                            msg.msg = "��������� ��� � 5";
                            break;
                        case 154:
                            msg.msg = "��������� ��� � 6";
                            break;
                        case 155:
                            msg.msg = "��������� ��� � 7";
                            break;
                        case 156:
                            msg.msg = "��������� ��� � 8";
                            break;
                        case 157:
                            msg.msg = "��������� ��� � 9";
                            break;
                        case 158:
                            msg.msg = "��������� ��� � 10";
                            break;
                        case 159:
                            msg.msg = "��������� ��� � 11";
                            break;
                        case 160:
                            msg.msg = "��������� ��� � 12";
                            break;
                        case 161:
                            msg.msg = "��������� ��� � 13";
                            break;
                        case 162:
                            msg.msg = "��������� ��� � 14";
                            break;
                        case 163:
                            msg.msg = "��������� ��� � 15";
                            break;
                        case 164:
                            msg.msg = "��������� ��� � 16";
                            break;
                        case 165:
                            msg.msg = "��������� ��� � 17";
                            break;
                        case 166:
                            msg.msg = "��������� ��� � 18";
                            break;
                        case 167:
                            msg.msg = "��������� ��� � 19";
                            break;
                        case 168:
                            msg.msg = "��������� ��� � 20";
                            break;
                        case 169:
                            msg.msg = "��������� ��� � 21";
                            break;
                        case 170:
                            msg.msg = "��������� ��� � 22";
                            break;
                        case 171:
                            msg.msg = "��������� ��� � 23";
                            break;
                        case 172:
                            msg.msg = "��������� ��� � 24";
                            break;
                        case 173:
                            msg.msg = "��������� ��� � 25";
                            break;
                        case 174:
                            msg.msg = "��������� ��� � 26";
                            break;
                        case 175:
                            msg.msg = "��������� ��� � 27";
                            break;
                        case 176:
                            msg.msg = "��������� ��� � 28";
                            break;
                        case 177:
                            msg.msg = "��������� ��� � 29";
                            break;
                        case 178:
                            msg.msg = "��������� ��� � 30";
                            break;
                        case 179:
                            msg.msg = "��������� ��� � 31";
                            break;
                        case 180:
                            msg.msg = "��������� ��� � 32";
                            break;
                        case 181:
                            msg.msg = "��������� ��� � 33";
                            break;
                        case 182:
                            msg.msg = "��������� ��� � 34";
                            break;
                        case 183:
                            msg.msg = "��������� ��� � 35";
                            break;
                        case 184:
                            msg.msg = "��������� ��� � 36";
                            break;
                        case 185:
                            msg.msg = "��������� ��� � 37";
                            break;
                        case 186:
                            msg.msg = "��������� ��� � 38";
                            break;
                        case 187:
                            msg.msg = "��������� ��� � 39";
                            break;
                        case 188:
                            msg.msg = "��������� ��� � 40";
                            break;
                        case 189:
                            msg.msg = "��������� ��� � 41";
                            break;
                        case 190:
                            msg.msg = "��������� ��� � 42";
                            break;
                        case 191:
                            msg.msg = "��������� ��� � 43";
                            break;
                        case 192:
                            msg.msg = "��������� ��� � 44";
                            break;
                        case 193:
                            msg.msg = "��������� ��� � 45";
                            break;
                        case 194:
                            msg.msg = "��������� ��� � 46";
                            break;
                        case 195:
                            msg.msg = "��������� ��� � 47";
                            break;
                        case 196:
                            msg.msg = "��������� ��� � 48";
                            break;
                        case 197:
                            msg.msg = "��������� ��� � 49";
                            break;
                        case 198:
                            msg.msg = "��������� ��� � 50";
                            break;
                        case 199:
                            msg.msg = "��������� ��� � 51";
                            break;
                        case 200:
                            msg.msg = "��������� ��� � 52";
                            break;
                        case 201:
                            msg.msg = "��������� ��� � 53";
                            break;
                        case 202:
                            msg.msg = "��������� ��� � 54";
                            break;
                        case 203:
                            msg.msg = "��������� ��� � 55";
                            break;
                        case 204:
                            msg.msg = "��������� ��� � 56";
                            break;
                        case 205:
                            msg.msg = "��������� ��� � 57";
                            break;
                        case 206:
                            msg.msg = "��������� ��� � 58";
                            break;
                        case 207:
                            msg.msg = "��������� ��� � 59";
                            break;
                        case 208:
                            msg.msg = "��������� ��� � 60";
                            break;
                        case 209:
                            msg.msg = "��������� ��� � 61";
                            break;
                        case 210:
                            msg.msg = "��������� ��� � 62";
                            break;
                        case 211:
                            msg.msg = "��������� ��� � 63";
                            break;
                        case 212:
                            msg.msg = "��������� ��� � 64";
                            break;
                        case 213:
                            msg.msg = "��������� ��� � 65";
                            break;
                        case 214:
                            msg.msg = "��������� ��� � 66";
                            break;
                        case 215:
                            msg.msg = "��������� ��� � 67";
                            break;
                        case 216:
                            msg.msg = "��������� ��� � 68";
                            break;
                        case 217:
                            msg.msg = "��������� ��� � 69";
                            break;
                        case 218:
                            msg.msg = "��������� ��� � 70";
                            break;
                        case 219:
                            msg.msg = "��������� ��� � 71";
                            break;
                        case 220:
                            msg.msg = "��������� ��� � 72";
                            break;
                        case 221:
                            msg.msg = "��������� ��� � 73";
                            break;
                        case 222:
                            msg.msg = "��������� ��� � 74";
                            break;
                        case 223:
                            msg.msg = "��������� ��� � 75";
                            break;
                        case 224:
                            msg.msg = "��������� ��� � 76";
                            break;
                        case 225:
                            msg.msg = "��������� ��� � 77";
                            break;
                        case 226:
                            msg.msg = "��������� ��� � 78";
                            break;
                        case 227:
                            msg.msg = "��������� ��� � 79";
                            break;
                        case 228:
                            msg.msg = "��������� ��� � 80";
                            break;
                        case 229:
                            msg.msg = "��������� ��� � 81";
                            break;
                        case 230:
                            msg.msg = "��������� ��� � 82";
                            break;
                        case 231:
                            msg.msg = "��������� ��� � 83";
                            break;
                        case 232:
                            msg.msg = "��������� ��� � 84";
                            break;
                        case 233:
                            msg.msg = "��������� ��� � 85";
                            break;
                        case 234:
                            msg.msg = "��������� ��� � 86";
                            break;
                        case 235:
                            msg.msg = "��������� ��� � 87";
                            break;
                        case 236:
                            msg.msg = "��������� ��� � 88";
                            break;
                        case 237:
                            msg.msg = "��������� ��� � 89";
                            break;
                        case 238:
                            msg.msg = "��������� ��� � 90";
                            break;
                        case 239:
                            msg.msg = "��������� ��� � 91";
                            break;
                        case 240:
                            msg.msg = "��������� ��� � 92";
                            break;
                        case 241:
                            msg.msg = "��������� ��� � 93";
                            break;
                        case 242:
                            msg.msg = "��������� ��� � 94";
                            break;
                        case 243:
                            msg.msg = "��������� ��� � 95";
                            break;
                        case 244:
                            msg.msg = "��������� ��� � 96";
                            break;
                        case 245:
                            msg.msg = "��������� ��� � 97";
                            break;
                        case 246:
                            msg.msg = "��������� ��� � 98";
                            break;
                        case 247:
                            msg.msg = "��������� ��� � 99";
                            break;
                        case 248:
                            msg.msg = "��������� ��� � 100";
                            break;
                        case 249:
                            msg.msg = "��������� ��� � 101";
                            break;
                        case 250:
                            msg.msg = "��������� ��� � 102";
                            break;
                        case 251:
                            msg.msg = "��������� ��� � 103";
                            break;
                        case 252:
                            msg.msg = "��������� ��� � 104";
                            break;
                        case 253:
                            msg.msg = "��������� ��� � 105";
                            break;
                        case 254:
                            msg.msg = "��������� ��� � 106";
                            break;

                    }
                    msg.time = value[3].ToString("d2") + "-" + value[2].ToString("d2") + "-" + value[1].ToString("d2") + " " +
                               value[4].ToString("d2") + ":" + value[5].ToString("d2") + ":" + value[6].ToString("d2") + ":" + value[7].ToString("d2");

                    return msg;
                }


                #region ICollection Members

                public void CopyTo(Array array, int index)
                {

                }

                public bool IsSynchronized
                {
                    get { return false; }
                }

                public object SyncRoot
                {
                    get { return _messages; }
                }

                #endregion

                #region IEnumerable Members

                public IEnumerator GetEnumerator()
                {
                    return _messages.GetEnumerator();
                }

                #endregion
            }
            #endregion

            #region ������ ������
            public const int ALARMJOURNAL_RECORD_CNT = 32;

            [Browsable(false)]
            public CAlarmJournal AlarmJournal
            {
                get { return _alarmJournalRecords; }
                set { _alarmJournalRecords = value; }
            }

            public struct AlarmRecord
            {
                public string time;
                public string msg;
                public string code;
                public string type_value;
                public string Ia;
                public string Ib;
                public string Ic;
                public string I0;
                public string I1;
                public string I2;
                public string In;
                public string Ig;
                public string F;
                public string Uab;
                public string Ubc;
                public string Uca;
                public string U0;
                public string U1;
                public string U2;
                public string Un;
                public string inSignals1;
                public string inSignals2;
            } ;

            public class CAlarmJournal
            {
                private Dictionary<ushort, string> _msgDictionary;
                private Dictionary<byte, string> _codeDictionary;
                private Dictionary<byte, string> _typeDictionary;
                private MR500 _device;

                public CAlarmJournal()
                {

                }

                public CAlarmJournal(MR500 device)
                {
                    _device = device;
                    _msgDictionary = new Dictionary<ushort, string>(7);
                    _codeDictionary = new Dictionary<byte, string>(32);
                    _typeDictionary = new Dictionary<byte, string>(28);

                    _msgDictionary.Add(0, "������ ����");
                    _msgDictionary.Add(1, "������������");
                    _msgDictionary.Add(2, "����������");
                    _msgDictionary.Add(3, "������");
                    _msgDictionary.Add(4, "���������� ���");
                    _msgDictionary.Add(5, "�������");
                    _msgDictionary.Add(6, "���������");
                    _msgDictionary.Add(7, "���");

                    _codeDictionary.Add(0, "");
                    _codeDictionary.Add(1, "I>");
                    _codeDictionary.Add(2, "I>>");
                    _codeDictionary.Add(3, "I>>>");
                    _codeDictionary.Add(4, "I>>>>");
                    _codeDictionary.Add(5, "I2>");
                    _codeDictionary.Add(6, "I2>>");
                    _codeDictionary.Add(7, "I0>");
                    _codeDictionary.Add(8, "I0>>");
                    _codeDictionary.Add(9, "In>");
                    _codeDictionary.Add(10, "In>>");
                    _codeDictionary.Add(11, "Ig>");
                    _codeDictionary.Add(12, "I2/I1");
                    _codeDictionary.Add(13, "F>");
                    _codeDictionary.Add(14, "F>>");
                    _codeDictionary.Add(15, "F<");
                    _codeDictionary.Add(16, "F<<");
                    _codeDictionary.Add(17, "U>");
                    _codeDictionary.Add(18, "U>>");
                    _codeDictionary.Add(19, "U<");
                    _codeDictionary.Add(20, "U<<");
                    _codeDictionary.Add(21, "U2>");
                    _codeDictionary.Add(22, "U2>>");
                    _codeDictionary.Add(23, "U0>");
                    _codeDictionary.Add(24, "U0>>");
                    _codeDictionary.Add(25, "��-1");
                    _codeDictionary.Add(26, "��-2");
                    _codeDictionary.Add(27, "��-3");
                    _codeDictionary.Add(28, "��-4");
                    _codeDictionary.Add(29, "��-5");
                    _codeDictionary.Add(30, "��-6");
                    _codeDictionary.Add(31, "��-7");
                    _codeDictionary.Add(32, "��-8");
                    _codeDictionary.Add(33, "������");
                    _codeDictionary.Add(34, "������");
                    _codeDictionary.Add(35, "������");
                    _codeDictionary.Add(36, "������");
                    _codeDictionary.Add(37, "������");
                    _codeDictionary.Add(38, "������");
                    _codeDictionary.Add(39, "������");
                    _codeDictionary.Add(40, "������");

                    _typeDictionary.Add(0, "");
                    _typeDictionary.Add(1, "Ig");
                    _typeDictionary.Add(2, "In");
                    _typeDictionary.Add(3, "Ia");
                    _typeDictionary.Add(4, "Ib");
                    _typeDictionary.Add(5, "Ic");
                    _typeDictionary.Add(6, "I0");
                    _typeDictionary.Add(7, "I1");
                    _typeDictionary.Add(8, "I2");
                    _typeDictionary.Add(9, "Pn");
                    _typeDictionary.Add(10, "Pa");
                    _typeDictionary.Add(11, "Pb");
                    _typeDictionary.Add(12, "Pc");
                    _typeDictionary.Add(13, "P0");
                    _typeDictionary.Add(14, "P1");
                    _typeDictionary.Add(15, "P2");
                    _typeDictionary.Add(16, "F");
                    _typeDictionary.Add(17, "Un");
                    _typeDictionary.Add(18, "Ua");
                    _typeDictionary.Add(19, "Ub");
                    _typeDictionary.Add(20, "Uc");
                    _typeDictionary.Add(21, "U0");
                    _typeDictionary.Add(22, "U1");
                    _typeDictionary.Add(23, "U2");
                    _typeDictionary.Add(24, "Uab");
                    _typeDictionary.Add(25, "Ubc");
                    _typeDictionary.Add(26, "Uca");
                    _typeDictionary.Add(27, "����� �������");
                    _typeDictionary.Add(28, "Lkz");
                }

                private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

                public int Count
                {
                    get { return _messages.Count; }
                }

                public AlarmRecord this[int i]
                {
                    get { return _messages[i]; }
                }

                public bool IsMessageEmpty(int i)
                {
                    bool rez;
                    try
                    {
                        rez = ("������ ����" == _messages[i].msg);
                    }
                    catch
                    {
                        rez = false;
                    }
                    return rez;
                }

                public bool AddMessage(byte[] value)
                {
                    bool ret;
                    Common.SwapArrayItems(ref value);
                    AlarmRecord msg = CreateMessage(value);
                    if ("������ ����" == msg.msg)
                    {
                        ret = false;
                    }
                    else
                    {
                        ret = true;
                        _messages.Add(msg);
                    }
                    return ret;
                }

                public string GetMessage(byte b)
                {
                    return _msgDictionary[b];
                }

                public string GetMessageOsc(byte b)
                {
                    return _codeDictionary[b];
                }

                public string GetDateTime(byte[] datetime)
                {
                    return datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
                               " " +
                               datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
                               ":" + datetime[12].ToString("d2");
                }

                public string GetCode(byte codeByte, byte phaseByte)
                {

                    string group = (0 == (codeByte & 0x80)) ? "��������" : "������.";
                    string phase = "";
                    phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                    //if (phaseByte == 4 || phaseByte == 9)
                    //{
                    //    phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                    //    phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                    //    phase += (0 == (phaseByte & 0x04)) ? " " : "C";
                    //}else
                    //{
                    phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                    phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                    phase += (0 == (phaseByte & 0x04)) ? " " : "C";
                    //}


                    byte code = (byte)(codeByte & 0x7F);
                    try
                    {

                        return _codeDictionary[code] + " " + group + " " + phase;
                    }
                    catch (KeyNotFoundException)
                    {
                        return "";
                    }

                }

                public string GetTypeValue(ushort damageWord, byte typeByte, string code)
                {
                    bool OB = false;
                    double damageValue = 0;
                    switch (code)
                    {
                        case "Ia":
                        case "Ib":
                        case "Ic":
                        case "I0":
                        case "I1":
                        case "I2": damageValue = Measuring.GetI(damageWord, _device._TZLDevice.TT, true);
                            break;
                        case "Pn": damageValue = Measuring.GetP(damageWord, (ushort)(_device._TZLDevice.TTNP * _device._TZLDevice.TNNP), false);
                            break;
                        case "Pa":
                        case "Pb":
                        case "Pc":
                        case "P0":
                        case "P1":
                        case "P2": damageValue = Measuring.GetP(damageWord, (ushort)(_device._TZLDevice.TT * _device._TZLDevice.TN), true);
                            break;
                        case "In":
                        case "Ig": damageValue = Measuring.GetI(damageWord, _device._TZLDevice.TTNP, false);
                            break;
                        case "I2/I1": damageValue = Measuring.GetU(damageWord, 65536);
                            break;
                        case "F":
                        case "Lkz": damageValue = Measuring.GetConstraintOnly(damageWord, ConstraintKoefficient.K_25600);
                            break;
                        case "Un":
                        case "Ua":
                        case "Ub":
                        case "Uc":
                        case "U0":
                        case "U1":
                        case "U2":
                        case "Uab":
                        case "Ubc":
                        case "Uca": damageValue = Math.Round(Measuring.GetU(damageWord, _device._TZLDevice.TN), 1);
                            break;
                        case "����� �������": OB = true;
                            break;
                        default: break;
                    }

                    try
                    {
                        if (OB)
                        {
                            return _typeDictionary[typeByte];
                        }
                        else
                        {
                            return _typeDictionary[typeByte] + string.Format(" = {0:F2}", damageValue);
                        }
                    }
                    catch (KeyNotFoundException)
                    {
                        return "";
                    }
                }

                private AlarmRecord CreateMessage(byte[] buffer)
                {
                    try
                    {
                        Measuring.SetConsGA = true;
                        AlarmRecord rec = new AlarmRecord();
                        rec.msg = GetMessage(buffer[0]);
                        byte[] datetime = new byte[14];
                        Array.ConstrainedCopy(buffer, 2, datetime, 0, 14);
                        rec.time = GetDateTime(datetime);

                        if (buffer[0] == 0)
                        {
                            rec.code = "";
                        }
                        else
                        {
                            rec.code = GetCode(buffer[16], buffer[18]);
                        }
                        double damageValue =
                            Measuring.GetConstraintOnly(Common.TOWORD(buffer[21], buffer[20]),
                                ConstraintKoefficient.K_25600);
                        if (buffer[19] == 0)
                        {
                            rec.type_value = "";
                        }
                        else
                        {
                            rec.type_value = GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19],
                                _typeDictionary[buffer[19]]);
                        }

                        double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), _device._TZLDevice.TT, true);
                        double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), _device._TZLDevice.TT, true);
                        double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), _device._TZLDevice.TT, true);
                        double I0 = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), _device._TZLDevice.TT, true);
                        double I1 = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), _device._TZLDevice.TT, true);
                        double I2 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), _device._TZLDevice.TT, true);
                        double In = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), _device._TZLDevice.TTNP, false);
                        double Ig = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), _device._TZLDevice.TTNP, false);
                        double F = Measuring.GetF(Common.TOWORD(buffer[39], buffer[38]));
                        double Uab = Measuring.GetU(Common.TOWORD(buffer[41], buffer[40]), _device._TZLDevice.TN);
                        double Ubc = Measuring.GetU(Common.TOWORD(buffer[43], buffer[42]), _device._TZLDevice.TN);
                        double Uca = Measuring.GetU(Common.TOWORD(buffer[45], buffer[44]), _device._TZLDevice.TN);
                        double U0 = Measuring.GetU(Common.TOWORD(buffer[47], buffer[46]), _device._TZLDevice.TN);
                        double U1 = Measuring.GetU(Common.TOWORD(buffer[49], buffer[48]), _device._TZLDevice.TN);
                        double U2 = Measuring.GetU(Common.TOWORD(buffer[51], buffer[50]), _device._TZLDevice.TN);
                        double Un = Measuring.GetU(Common.TOWORD(buffer[53], buffer[52]), _device._TZLDevice.TNNP);
                        Measuring.SetConsGA = false;

                        rec.Ia = string.Format("{0:F2}", Ia);
                        rec.Ib = string.Format("{0:F2}", Ib);
                        rec.Ic = string.Format("{0:F2}", Ic);
                        rec.I0 = string.Format("{0:F2}", I0);
                        rec.I1 = string.Format("{0:F2}", I1);
                        rec.I2 = string.Format("{0:F2}", I2);
                        rec.In = string.Format("{0:F2}", In);
                        rec.Ig = string.Format("{0:F2}", Ig);
                        rec.F = string.Format("{0:F2}", F);
                        rec.Uab = string.Format("{0:F2}", Uab);
                        rec.Ubc = string.Format("{0:F2}", Ubc);
                        rec.Uca = string.Format("{0:F2}", Uca);
                        rec.U0 = string.Format("{0:F2}", U0);
                        rec.U1 = string.Format("{0:F2}", U1);
                        rec.U2 = string.Format("{0:F2}", U2);
                        rec.Un = string.Format("{0:F2}", Un);
                        byte[] b1 = new byte[] {buffer[54]};
                        byte[] b2 = new byte[] {buffer[55]};
                        rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                        rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                        return rec;
                    }
                    catch (Exception e)
                    {
                        AlarmRecord rec = new AlarmRecord();
                        rec.msg = "���������� ��������� ������";
                        return rec;
                    }         
                }
            }
            #endregion

            #region ����
            #region
            private slot _program = new slot(0xB000, 0xB400);
            private slot _programstart = new slot(0x0001, 0x0002);
            private slot _programStorage = new slot(0xC000, 0xE000);
            private slot _programSignals = new slot(0xA000, 0xA200);

            private slot _diagnostic = new slot(0x1800, 0x180F);
            private slot _inputSignals = new slot(0x1000, 0x103C);
            private slot _outputSignals = new slot(0x1200, 0x1270);
            private slot _externalDefenses = new slot(0x1050, 0x1080);
            private slot _countOsc = new slot(0x3F00, 0x3F03);//(0x1000, 0x103C);
            private slot _datetime = new slot(0x200, 0x207);
            private slot _analog = new slot(0x1900, 0x1910);

            private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
            private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];

            private bool _stopAlarmJournal = false;
            private bool _stopSystemJournal = false;

            private CSystemJournal _systemJournalRecords = new CSystemJournal();
            private CAlarmJournal _alarmJournalRecords;
            private COutputRele _outputRele;
            private COutputIndicator _outputIndicator = new COutputIndicator();
            #endregion
            private slot _konfCount = new slot(0x1274, 0x1275);
            private slot _oscCrush = new slot(0x3800, 0x3801);

            #region ����� �����������
            private static ushort _startOscJournalAdress = 0x0800;
            private static ushort _startOscJournal = _startOscJournalAdress;
            private static ushort _sizeOscJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OscJournal)) / 2 - 2);
            private static ushort _endOscJournal = (ushort)(_startOscJournal + _sizeOscJournal);

            private static ushort _startOscAdress = 0x0900;
            private static ushort _startOscilloscope = _startOscAdress;
            private static ushort _sizeOscilloscope = (ushort)0x7c;//�����
            private static ushort _endOscilloscope = (ushort)(_startOscilloscope + _sizeOscilloscope);

            //������ �������������
            private slot _oscilloscopeJournalReadSlot = new slot(_startOscJournal, _endOscJournal);

            //����� ������ ������� �����������
            private slot _oscilloscopeJournalWriteSlot = new slot(_startOscJournal, (ushort)(_startOscJournal + 1));

            //������ �������������
            private List<slot> CurrentOscilloscope = new List<slot>();
            private slot _oscilloscopeRead = new slot(_startOscilloscope, _endOscilloscope);

            //����� ������ ������� �����������
            private slot _oscilloscopePageWrite = new slot(_startOscilloscope, (ushort)(_startOscilloscope + 1));
            #endregion

            #endregion

            #region �������
            public event Handler DiagnosticLoadOk;
            public event Handler DiagnosticLoadFail;
            public event Handler DateTimeLoadOk;
            public event Handler DateTimeLoadFail;
            public event Handler InputSignalsLoadOK;
            public event Handler InputSignalsLoadFail;
            public event Handler InputSignalsSaveOK;
            public event Handler InputSignalsSaveFail;
            public event Handler KonfCountLoadOK;
            public event Handler KonfCountLoadFail;
            public event Handler KonfCountSaveOK;
            public event Handler KonfCountSaveFail;
            public event Handler ExternalDefensesLoadOK;
            public event Handler ExternalDefensesLoadFail;
            public event Handler ExternalDefensesSaveOK;
            public event Handler ExternalDefensesSaveFail;
            public event Handler TokDefensesLoadOK;
            public event Handler TokDefensesLoadFail;
            public event Handler TokDefensesSaveOK;
            public event Handler TokDefensesSaveFail;
            public event Handler LogicLoadOK;
            public event Handler LogicLoadFail;
            public event Handler LogicSaveOK;
            public event Handler LogicSaveFail;
            public event Handler AnalogSignalsLoadOK;
            public event Handler AnalogSignalsLoadFail;
            public event Handler SystemJournalLoadOk;
            public event IndexHandler SystemJournalRecordLoadOk;
            public event IndexHandler SystemJournalRecordLoadFail;
            public event Handler AlarmJournalLoadOk;
            public event IndexHandler AlarmJournalRecordLoadOk;
            public event IndexHandler AlarmJournalRecordLoadFail;
            public event Handler OutputSignalsLoadOK;
            public event Handler OutputSignalsLoadFail;
            public event Handler OutputSignalsSaveOK;
            public event Handler OutputSignalsSaveFail;

            #endregion

            #region ����� �����������

            public event Handler OscJournalLoadOK;
            public event Handler OscJournalLoadFail;
            public event Handler OscJournalSaveOK;
            public event Handler OscJournalSaveFail;
            public event Handler OscilloscopeLoadOK;
            public event Handler OscilloscopeLoadFail;
            public event Handler OscilloscopeSaveOK;
            public event Handler OscilloscopeSaveFail;
            #endregion

            #region �������� ����
            public class COutputRele : ICollection
            {
                Device _device;
                public static int LENGTH;
                public static int COUNT;
                double vers;

                public COutputRele()
                {
                    LENGTH = 16;
                    COUNT = 8;
                    SetBuffer(new ushort[LENGTH]);
                }

                public COutputRele(Device device)
                {
                    _device = device;
                    try
                    {
                        if (string.IsNullOrEmpty(_device.Info.Version))
                            vers = 1.11;
                        else
                            vers = ConvertVersion(_device.Info.Version);
                    }
                    catch (Exception f)
                    {
                        vers = 1.11;
                    }
                    LENGTH = 16;
                    COUNT = 8;
                    SetBuffer(new ushort[LENGTH]);
                }

                public double ConvertVersion(string oldVersion)
                {
                    return  double.Parse(oldVersion.Replace('.',','), System.Globalization.NumberStyles.AllowDecimalPoint);
                }

                private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

                [DisplayName("����������")]
                public int Count
                {
                    get
                    {
                        return _releList.Count;
                    }
                }

                public void SetBuffer(ushort[] values)
                {
                    _releList.Clear();
                    if (values.Length != LENGTH)
                    {
                        throw new ArgumentOutOfRangeException("Rele values", (object)LENGTH, "Output rele length must be 16");
                    }
                    for (int i = 0; i < LENGTH; i += 2)
                    {
                        _releList.Add(new OutputReleItem(values[i], values[i + 1]));
                    }
                }

                public ushort[] ToUshort()
                {
                    ushort[] ret = new ushort[LENGTH];
                    for (int i = 0; i < COUNT; i++)
                    {
                        ret[i * 2] = _releList[i].Value[0];
                        ret[i * 2 + 1] = _releList[i].Value[1];
                    }
                    return ret;
                }

                public OutputReleItem this[int i]
                {
                    get
                    {
                        return _releList[i];
                    }
                    set
                    {
                        _releList[i] = value;
                    }
                }

                #region ICollection Members

                public void Add(OutputReleItem item)
                {
                    _releList.Add(item);
                }

                public void CopyTo(Array array, int index)
                {

                }
                [Browsable(false)]
                public bool IsSynchronized
                {
                    get { return false; }
                }
                [Browsable(false)]
                public object SyncRoot
                {
                    get { return this; }
                }

                #endregion

                #region IEnumerable Members

                public IEnumerator GetEnumerator()
                {
                    return _releList.GetEnumerator();
                }

                #endregion
            };

            public class OutputReleItem
            {

                private ushort _hiWord;
                private ushort _loWord;
                private string _blinker = "";
                private string _signal = "";
                private string _typeSignal = "";

                public override string ToString()
                {
                    return "�������� ����";
                }

                #region ��������
                [XmlAttribute("��������")]
                [DisplayName("��������")]
                [Description("������ � ������� ����")]
                [System.ComponentModel.Category("������� ���")]
                [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]

                #endregion
                public ushort[] Value
                {
                    get
                    {
                        return new ushort[] { _hiWord, _loWord };
                    }
                    set
                    {
                        _hiWord = value[0];
                        _loWord = value[1];
                    }
                }

                public OutputReleItem() { }

                public OutputReleItem(ushort hiWord, ushort loWord)
                {
                    SetItem(hiWord, loWord);
                }

                public void SetItem(ushort hiWord, ushort loWord)
                {
                    _hiWord = hiWord;
                    _loWord = loWord;
                }

                public string Reverse(string revString)
                {
                    string ret = "";
                    for (int i = revString.Length; i > 0; i--)
                    {
                        ret += revString[i - 1];
                    }
                    return ret;
                }

                public string LoadTypeSygnals(ushort TypeSygnals)
                {
                    byte[] buffer1 = Common.TOBYTE(TypeSygnals);
                    byte[] b1 = new byte[] { buffer1[0] };
                    byte[] b2 = new byte[] { buffer1[1] };
                    string r1 = Common.BitsToString(new BitArray(b1));
                    r1 = Reverse(r1);
                    string r2 = Common.BitsToString(new BitArray(b2));
                    r2 = Reverse(r2);
                    string res = r1 + r2;
                    return res;
                }

                public ushort SetSygnals(string TypeSygnals)
                {
                    return Common.BitsToUshort(Common.StringToBits(TypeSygnals));
                }
                
                [XmlIgnore]
                [Browsable(false)]
                public bool IsBlinker
                {
                    get
                    {
                        string a = LoadTypeSygnals(_hiWord);
                        return 1 == Convert.ToInt16(a[0].ToString());
                    }
                    set
                    {
                        if (value)
                        {
                            _blinker = "1";
                        }
                        else
                        {
                            _blinker = "0";
                        }
                    }
                }
                
                [XmlAttribute("���")]
                public string Type
                {
                    get
                    {
                        string ret = "";
                        ret = IsBlinker ? "�������" : "�����������";
                        return ret;
                    }
                    set
                    {
                        if ("�������" == value)
                        {
                            IsBlinker = true;
                        }
                        else if ("�����������" == value)
                        {
                            IsBlinker = false;
                        }
                        else
                        {
                            throw new ArgumentException("Rele type undefined", "Type");
                        }
                    }
                }
                
                [XmlAttribute("������")]
                public string SignalString
                {
                    get
                    {
                        _signal = "";
                        _typeSignal = LoadTypeSygnals(_hiWord);
                        for (int i = (_typeSignal.Length - 9); i < _typeSignal.Length; i++)
                        {
                            _signal += _typeSignal[i];
                        }
                        return Strings.TZL.All[SetSygnals(Reverse(_signal))];
                    }
                    set
                    {
                        _typeSignal = "";
                        _signal = LoadTypeSygnals((ushort)Strings.TZL.All.IndexOf(value));
                        for (int i = 0; i < _signal.Length; i++)
                        {
                            if (i == 0)
                            {
                                _typeSignal += _blinker;
                            }
                            else
                            {
                                _typeSignal += _signal[i];
                            }
                        }
                        _typeSignal = Reverse(_typeSignal);
                        _hiWord = SetSygnals(_typeSignal);
                    }
                }
                
                [XmlAttribute("�������")]
                public ulong ImpulseUlong
                {
                    get { return Measuring.GetTime(_loWord); }
                    set { _loWord = Measuring.SetTime(value); }
                }
            }
            
            [XmlElement("��������_����")]
            public COutputRele OutputRele
            {
                get
                {
                    return _outputRele;
                }
                set
                {
                    _outputRele = value;
                }
            }

            #endregion

            #region �������� ����������
            public class COutputIndicator : ICollection
            {
                #region ICollection Members

                public void Add(OutputIndicatorItem item)
                {
                    _indList.Add(item);
                }

                public void CopyTo(Array array, int index)
                {

                }
                [Browsable(false)]
                public bool IsSynchronized
                {
                    get { return false; }
                }

                [Browsable(false)]
                public object SyncRoot
                {
                    get { return this; }
                }

                #endregion

                #region IEnumerable Members

                public IEnumerator GetEnumerator()
                {
                    return _indList.GetEnumerator();
                }

                #endregion

                [DisplayName("����������")]
                public int Count
                {
                    get
                    {
                        return _indList.Count;
                    }
                }


                public const int LENGTH = 16;
                public const int COUNT = 8;

                private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

                public COutputIndicator()
                {
                    SetBuffer(new ushort[LENGTH]);
                }

                public void SetBuffer(ushort[] values)
                {
                    _indList.Clear();
                    if (values.Length != LENGTH)
                    {
                        throw new ArgumentOutOfRangeException("Output Indicator values", (object)LENGTH, "Output indicator length must be 16");
                    }
                    for (int i = 0; i < LENGTH; i += 2)
                    {
                        _indList.Add(new OutputIndicatorItem(values[i], values[i + 1]));
                    }
                }

                public ushort[] ToUshort()
                {
                    ushort[] ret = new ushort[LENGTH];
                    for (int i = 0; i < COUNT; i++)
                    {
                        ret[i * 2] = _indList[i].Value[0];
                        ret[i * 2 + 1] = _indList[i].Value[1];
                    }
                    return ret;
                }

                public OutputIndicatorItem this[int i]
                {
                    get
                    {
                        return _indList[i];
                    }
                    set
                    {
                        _indList[i] = value;
                    }
                }

            };

            public class OutputIndicatorItem
            {
                private ushort _hiWord;
                private ushort _loWord;
                private string _blinker = "";
                private string _signal = "";
                private string _typeSignal = "";

                public override string ToString()
                {
                    return "�������� ���������";
                }
                
                [XmlAttribute("��������")]
                public ushort[] Value
                {
                    get
                    {
                        return new ushort[] { _hiWord, _loWord };
                    }
                }

                public OutputIndicatorItem() { }

                public OutputIndicatorItem(ushort hiWord, ushort loWord)
                {
                    SetItem(hiWord, loWord);
                }

                public void SetItem(ushort hiWord, ushort loWord)
                {
                    _hiWord = hiWord;
                    _loWord = loWord;
                }

                public string Reverse(string revString)
                {
                    string ret = "";
                    for (int i = revString.Length; i > 0; i--)
                    {
                        ret += revString[i - 1];
                    }
                    return ret;
                }

                public string LoadTypeSygnals(ushort TypeSygnals)
                {
                    byte[] buffer1 = Common.TOBYTE(TypeSygnals);
                    byte[] b1 = new byte[] { buffer1[0] };
                    byte[] b2 = new byte[] { buffer1[1] };
                    string r1 = Common.BitsToString(new BitArray(b1));
                    r1 = Reverse(r1);
                    string r2 = Common.BitsToString(new BitArray(b2));
                    r2 = Reverse(r2);
                    string res = r1 + r2;
                    return res;
                }

                public ushort SetSygnals(string TypeSygnals)
                {
                    return Common.BitsToUshort(Common.StringToBits(TypeSygnals));
                }
                
                [XmlIgnore]
                public bool IsBlinker
                {
                    get
                    {
                        string a = LoadTypeSygnals(_hiWord);
                        return 1 == Convert.ToInt16(a[0].ToString());
                    }
                    set
                    {
                        if (value)
                        {
                            _blinker = "1";
                        }
                        else
                        {
                            _blinker = "0";
                        }
                    }
                }
                
                [XmlAttribute("���")]
                public string Type
                {
                    get
                    {
                        return this.IsBlinker ? "�������" : "�����������";
                    }
                    set
                    {
                        if ("�������" == value)
                        {
                            IsBlinker = true;

                        }
                        else if ("�����������" == value)
                        {
                            IsBlinker = false;
                        }
                        else
                        {
                            throw new ArgumentException("Rele type undefined", "Type");
                        }
                    }
                }
                
                [XmlAttribute("������")]
                public string SignalString
                {
                    get
                    {
                        _signal = "";
                        _typeSignal = LoadTypeSygnals(_hiWord);
                        for (int i = (_typeSignal.Length - 9); i < _typeSignal.Length; i++)
                        {
                            _signal += _typeSignal[i];
                        }
                        return Strings.TZL.All[SetSygnals(Reverse(_signal))];
                    }
                    set
                    {
                        _typeSignal = "";
                        _signal = LoadTypeSygnals((ushort)Strings.TZL.All.IndexOf(value));
                        for (int i = 0; i < _signal.Length; i++)
                        {
                            if (i == 0)
                            {
                                _typeSignal += _blinker;
                            }
                            else
                            {
                                _typeSignal += _signal[i];
                            }
                        }
                        _typeSignal = Reverse(_typeSignal);
                        _hiWord = SetSygnals(_typeSignal);
                    }
                }

                [XmlAttribute("���������")]
                public bool Indication
                {
                    get
                    {
                        return 0 != (_loWord & 0x1);
                    }
                    set
                    {
                        _loWord = value ? (ushort)(_loWord | 0x1) : (ushort)(_loWord & ~0x1);
                    }
                }

                [XmlAttribute("������")]
                public bool Alarm
                {
                    get
                    {
                        return 0 != (_loWord & 0x2);
                    }
                    set
                    {
                        _loWord = value ? (ushort)(_loWord | 0x2) : (ushort)(_loWord & ~0x2);
                    }
                }

                [XmlAttribute("�������")]
                public bool System
                {
                    get
                    {
                        return 0 != (_loWord & 0x4);
                    }
                    set
                    {
                        _loWord = value ? (ushort)(_loWord | 0x4) : (ushort)(_loWord & ~0x4);
                    }
                }
            }
            
            [XmlElement("��������_����������")]
            public COutputIndicator OutputIndicator
            {
                get
                {
                    return _outputIndicator;
                }
                set
                {
                    _outputIndicator = value;
                }
            }

            #endregion
            //-
            #region ���������� �������
            
            public double In
            {
                get
                {
                    return Measuring.GetI(_analog.Value[0], TTNP, false);
                }
            }
            
            public double Ia
            {
                get
                {
                    return Measuring.GetI(_analog.Value[1], TT, true);
                }
            }
            
            public double Ib
            {
                get
                {
                    return Measuring.GetI(_analog.Value[2], TT, true);
                }
            }
            
            public double Ic
            {
                get
                {
                    return Measuring.GetI(_analog.Value[3], TT, true);
                }
            }
            
            public double I0
            {
                get
                {
                    return Measuring.GetI(_analog.Value[4], TT, true);
                }
            }
            
            public double I1
            {
                get
                {
                    return Measuring.GetI(_analog.Value[5], TT, true);
                }
            }
            
            public double I2
            {
                get
                {
                    return Measuring.GetI(_analog.Value[6], TT, true);
                }
            }

            public double Ig
            {
                get
                {
                    return Measuring.GetI(_analog.Value[7], TTNP, false);
                }
            }
            #endregion
            //-
            #region ���������� �������
            
            [XmlIgnore]
            public BitArray ManageSignals
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0]), Common.HIBYTE(_diagnostic.Value[0]) });
                }
            }
            
            [XmlIgnore]
            
            public BitArray AdditionalSignals
            {
                get
                {
                    BitArray temp = new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[2]) });
                    BitArray ret = new BitArray(4);
                    for (int i = 0; i < 4; i++)
                    {
                        ret[i] = temp[i + 4];
                    }
                    return ret;
                }
            }
            
            [XmlIgnore]
            public BitArray Indicators
            {
                get
                {
                    return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[2]) });
                }
            }
            
            [XmlIgnore]
            public BitArray InputSignals
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[0x9]),
                                                 Common.HIBYTE(_diagnostic.Value[0x9]),
                                                 Common.LOBYTE(_diagnostic.Value[0xA])});
                }
            }
            
            [XmlIgnore]
            public BitArray Rele
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[3]),
                                                 Common.HIBYTE(_diagnostic.Value[3])});
                }
            }
            
            [XmlIgnore]
            public BitArray OutputSignals
            {
                get
                {
                    return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[0xA]) });
                }
            }
            
            [XmlIgnore]
            public BitArray LimitSignals
            {
                get
                {
                    return new BitArray(new byte[]
                    {
                        Common.LOBYTE(_diagnostic.Value[0xB]),
                        Common.HIBYTE(_diagnostic.Value[0xB]),
                        Common.LOBYTE(_diagnostic.Value[0xC]),
                        Common.HIBYTE(_diagnostic.Value[0xC]),
                        Common.LOBYTE(_diagnostic.Value[0xD]),
                        Common.HIBYTE(_diagnostic.Value[0xD]),
                        Common.LOBYTE(_diagnostic.Value[0xE])
                    });
                }
            }
            
            [XmlIgnore]
            public BitArray FaultState
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[4]) });
                }
            }
            
            [XmlIgnore]
            public BitArray FaultSignals
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_diagnostic.Value[5]), Common.HIBYTE(_diagnostic.Value[5]) });
                }
            }
            
            [XmlIgnore]
            public BitArray Automation
            {
                get
                {
                    return new BitArray(new byte[] { Common.HIBYTE(_diagnostic.Value[8]) });
                }
            }

            #endregion
            //-
            #region ��������� ���� � ����������
            
            [XmlElement("��")]
            public ushort TT
            {
                get
                {
                    return _inputSignals.Value[1];
                }
                set
                {
                    _inputSignals.Value[1] = value;
                }
            }
            
            [XmlElement("����")]
            public ushort TTNP
            {
                get
                {
                    return _inputSignals.Value[2];
                }
                set
                {
                    _inputSignals.Value[2] = value;
                }
            }
            
            [XmlElement("���_�T")]
            public string TT_Type
            {
                get
                {
                    return Strings.TZL.TT_Type[_inputSignals.Value[0]];
                }
                set
                {
                    _inputSignals.Value[0] = (ushort)Strings.TZL.TT_Type.IndexOf(value);
                }
            }

            [XmlElement("����")]
            public double TNNP
            {
                get
                {
                    return 1;
                }
                set
                {
                    
                }
            }

            [XmlElement("��")]
            public double TN
            {
                get { return 1; }
                set {  }
            }

            [XmlElement("���")]
            public string ACHR_ENTER
            {
                get
                {
                    return Strings.TZL.Logic[_inputSignals.Value[8]];
                }
                set
                {
                    _inputSignals.Value[8] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }
            
            [XmlElement("���_����������")]
            public string ACHR_BLOCK
            {
                get
                {
                    return Strings.TZL.Logic[_inputSignals.Value[9]];
                }
                set
                {
                    _inputSignals.Value[9] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }

            [XmlElement("���_�����")]
            public int ACHR_TIME
            {
                get 
                {
                    return _inputSignals.Value[10] >= 0 && _inputSignals.Value[10] <= 32767
                        ? _inputSignals.Value[10]*10
                        : (_inputSignals.Value[10] - 32768)*100;
                }
                set
                {
                    if (value > 300000)
                    {
                        _inputSignals.Value[10] = (ushort) (value/100 + 32768);
                    }
                    else
                    {
                        _inputSignals.Value[10] = (ushort) (value/10);
                    }
                }
            }
            
            [XmlElement("����")]
            public string CHAPV_ENTER
            {
                get
                {
                    return Strings.TZL.Logic[_inputSignals.Value[12]];
                }
                set
                {
                    _inputSignals.Value[12] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }
            
            [XmlElement("����_����������")]
            public string CHAPV_BLOCK
            {
                get
                {
                    return Strings.TZL.Logic[_inputSignals.Value[13]];
                }
                set
                {
                    _inputSignals.Value[13] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }
            
            [XmlElement("����_�����")]
            public int CHAPV_TIME
            {
                get
                {
                    return _inputSignals.Value[14] >= 0 && _inputSignals.Value[14] <= 32767
                        ? _inputSignals.Value[14] * 10
                        : (_inputSignals.Value[14] - 32768) * 100;
                }
                set
                {
                    if (value > 300000)
                    {
                        _inputSignals.Value[14] = (ushort)(value / 100 + 32768);
                    }
                    else
                    {
                        _inputSignals.Value[14] = (ushort)(value / 10);
                    }
                }
            }
            
            [XmlElement("����_���")]
            public string CHAPV_OSC
            {
                get
                {
                    return Strings.TZL.Osc_Chapv[_inputSignals.Value[11]];
                }
                set
                {
                    _inputSignals.Value[11] = (ushort)Strings.TZL.Osc_Chapv.IndexOf(value);
                }
            }
            
            [XmlElement("������������_���")]
            public double MaxTok
            {
                get
                {
                    return Measuring.GetConstraint(_inputSignals.Value[3], ConstraintKoefficient.K_4000, "123");
                }
                set
                {
                    _inputSignals.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
                }
            }

            #endregion
            //-
            #region ������� �������

            public virtual double POscilloscopeCrush
            {
                set
                {
                    _oscCrush.Value[0] = (ushort)value;
                }
            }

            public virtual double OscilloscopeKonfCount
            {
                get
                {
                    return _konfCount.Value[0];
                }
                set
                {
                    _konfCount.Value[0] = (ushort)value;
                }
            }

            public virtual string OscilloscopeFix
            {
                get
                {
                    int rez = 0;

                    if (Common.GetBit(_konfCount.Value[0], 7))
                    {
                        rez = 1;
                    }

                    return Strings.TZL.OscFix[rez];
                }
                set
                {
                    bool rez = !(Strings.TZL.OscFix.IndexOf(value) == 0);

                    this._konfCount.Value[0] = Common.SetBit(_konfCount.Value[0], 7, rez);
                }
            }

            public virtual ushort OscilloscopePercent
            {
                get
                {
                    return (ushort)(Common.GetBits(_konfCount.Value[0], 8, 9, 10, 11, 12, 13, 14, 15) >> 8);
                }
                set
                {
                    _konfCount.Value[0] = Common.SetBits(_konfCount.Value[0], value, 8, 9, 10, 11, 12, 13, 14, 15);
                }
            }


            public virtual string OscilloscopePeriod
            {
                get
                {
                    return Strings.TZL.OscLength[Common.GetBits(_konfCount.Value[0], 0, 1, 2)];
                }
                set
                {
                    _konfCount.Value[0] = Common.SetBits(_konfCount.Value[0], (ushort)Strings.TZL.OscLength.IndexOf(value), 0, 1, 2);
                }
            }
            
            [XmlElement("����_��������_")]
            public string KeyOn
            {
                get
                {
                    ushort index = _inputSignals.Value[0x10];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[0x10] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }

            }

            [XmlElement("����_���������_")]
            public string KeyOff
            {
                get
                {
                    ushort index = _inputSignals.Value[0x11];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort) (Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[index];
                }
                set { _inputSignals.Value[0x11] = (ushort) Strings.TZL.Logic.IndexOf(value); }
            }

            [XmlElement("�������_��������_")]
            public string ExternalOn
            {
                get
                {
                    ushort index = _inputSignals.Value[0x12];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort) (Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[index];
                }
                set { _inputSignals.Value[0x12] = (ushort)Strings.TZL.Logic.IndexOf(value); }
            }
            
            [XmlElement("�������_���������_")]
            public string ExternalOff
            {
                get
                {
                    ushort index = _inputSignals.Value[0x13];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _inputSignals.Value[0x13] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }
            
            [XmlElement("�����_������������_")]
            public string SignalizationReset
            {
                get
                {
                    ushort index = _inputSignals.Value[0x14];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[0x14] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }
            }
            
            [XmlElement("������������_�������_")]
            public string ConstraintGroup
            {
                get
                {
                    ushort index = _inputSignals.Value[0x15];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[0x15] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }


            }
            
            [XmlIgnore]
            public BitArray DispepairSignal
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_inputSignals.Value[0x1D]) });
                }
                set
                {
                    _inputSignals.Value[0x1D] = Common.BitsToUshort(value);
                }
            }

            [XmlElement("�������������")]
            public bool[] DispepairSignalXml
            {
                get { return this.DispepairSignal.Cast<bool>().ToArray(); }
                set { }
            }

            [XmlElement("�������_�������������")]
            public ulong DispepairImpulse
            {
                get
                {
                    return Measuring.GetTime(_inputSignals.Value[0x1E]);
                }
                set
                {
                    _inputSignals.Value[0x1E] = Measuring.SetTime(value);
                }
            }

            #region �����

            [XmlElement("����1")]
            public string Keys1 { get; set; }
            [XmlElement("����2")]
            public string Keys2 { get; set; }
            [XmlElement("����3")]
            public string Keys3 { get; set; }
            [XmlElement("����4")]
            public string Keys4 { get; set; }
            [XmlElement("����5")]
            public string Keys5 { get; set; }
            [XmlElement("����6")]
            public string Keys6 { get; set; }
            [XmlElement("����7")]
            public string Keys7 { get; set; }
            [XmlElement("����8")]
            public string Keys8 { get; set; }
            [XmlElement("����9")]
            public string Keys9 { get; set; }
            [XmlElement("����10")]
            public string Keys10 { get; set; }
            [XmlElement("����11")]
            public string Keys11 { get; set; }
            [XmlElement("����12")]
            public string Keys12 { get; set; }
            [XmlElement("����13")]
            public string Keys13 { get; set; }
            [XmlElement("����14")]
            public string Keys14 { get; set; }
            [XmlElement("����15")]
            public string Keys15 { get; set; }
            [XmlElement("����16")]
            public string Keys16 { get; set; }

            [XmlElement("�����")]
            public string Keys
            {
                get
                {
                    return LoadKeys(_inputSignals.Value);
                }
                set
                {
                    _inputSignals.Value[15] = SetKeys(value);
                }
            }
            public string LoadKeys(ushort[] masInputSignals)
            {
                byte[] buffer1 = Common.TOBYTES(masInputSignals, false);
                byte[] b1 = { buffer1[30] };
                byte[] b2 = { buffer1[31] };
                string r1 = Common.BitsToString(new BitArray(b1));
                string r2 = Common.BitsToString(new BitArray(b2));
                string res = r1 + r2;
                return res;
            }
            
            public ushort SetKeys(string masInputSignals)
            {
                return Common.BitsToUshort(Common.StringToBits(masInputSignals));
            }

            #endregion
            
            #endregion

            #region ������� ����������

            [XmlElement("������_��_������")]
            public string ManageSignalButton
            {
                get
                {

                    string ret = Common.GetBit(_inputSignals.Value[0x3B], 0) ? Strings.TZL.Forbidden[0] : Strings.TZL.Forbidden[1];
                    return ret;
                }
                set
                {
                    bool bit = (value == Strings.TZL.Forbidden[0]) ? true : false;
                    _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 0, bit);
                }
            }
            [XmlElement("������_��_�����")]
            public string ManageSignalKey
            {
                get
                {

                    string ret = Common.GetBit(_inputSignals.Value[0x3B], 1) ? Strings.TZL.Control[0] : Strings.TZL.Control[1];
                    return ret;
                }
                set
                {
                    bool bit = (value == Strings.TZL.Control[0]) ? true : false;
                    _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 1, bit);
                }
            }
            [XmlElement("������_��_��������")]
            public string ManageSignalExternal
            {
                get
                {

                    string ret = Common.GetBit(_inputSignals.Value[0x3B], 2) ? Strings.TZL.Control[0] : Strings.TZL.Control[1];
                    return ret;
                }
                set
                {
                    bool bit = (value == Strings.TZL.Control[0]) ? true : false;
                    _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 2, bit);
                }
            }
            [XmlElement("������_��_����")]
            public string ManageSignalSDTU
            {
                get
                {

                    string ret = Common.GetBit(_inputSignals.Value[0x3B], 3) ? Strings.TZL.Forbidden[0] : Strings.TZL.Forbidden[1];
                    return ret;
                }
                set
                {
                    bool bit = (value == Strings.TZL.Forbidden[0]) ? true : false;
                    _inputSignals.Value[0x3B] = Common.SetBit(_inputSignals.Value[0x3B], 3, bit);
                }
            }

            #endregion

            #region �����������
            [XmlElement("�����������_���������")]
            public string SwitcherOff
            {

                get
                {
                    ushort index = _inputSignals.Value[50];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[50] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }


            }

            [XmlElement("�����������_��������")]
            public string SwitcherOn
            {
                get
                {
                    ushort index = _inputSignals.Value[51];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[51] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }


            }
            [XmlElement("�����������_������")]
            public string SwitcherError
            {
                get
                {
                    ushort index = _inputSignals.Value[52];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[52] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }


            }

            [XmlElement("�����������_����������")]
            public string SwitcherBlock
            {
                get
                {
                    ushort index = _inputSignals.Value[53];

                    if (Strings.TZL.Logic.Count <= index)
                    {
                        index = (ushort)(Strings.TZL.Logic.Count - 1);
                    }

                    return Strings.TZL.Logic[(int)index];
                }
                set
                {
                    _inputSignals.Value[53] = (ushort)(Strings.TZL.Logic.IndexOf(value));
                }

            }

            [XmlElement("�����������_�����_����")]
            public ulong SwitcherTimeUROV
            {
                get
                {
                    return Measuring.GetTime(_inputSignals.Value[54]);
                }
                set
                {
                    _inputSignals.Value[54] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����������_���_����")]
            public double SwitcherTokUROV
            {
                get
                {
                    return Measuring.GetConstraint(_inputSignals.Value[55], ConstraintKoefficient.K_4000, "123");
                }
                set
                {
                    _inputSignals.Value[55] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
                }

            }

            [XmlElement("�����������_�������")]
            public ulong SwitcherImpulse
            {
                get
                {
                    return Measuring.GetTime(_inputSignals.Value[56]);
                }
                set
                {
                    _inputSignals.Value[56] = Measuring.SetTime(value);
                }

            }

            [XmlElement("�����������_������������")]
            public ulong SwitcherDuration
            {
                get
                {
                    return Measuring.GetTime(_inputSignals.Value[57]);
                }
                set
                {
                    _inputSignals.Value[57] = Measuring.SetTime(value);
                }

            }

            [XmlElement("���������")]
            public string Sost_ON_OFF
            {
                get
                {
                    return Strings.TZL.ModesLight[_inputSignals.Value[58]];
                }
                set
                {
                    _inputSignals.Value[58] = (ushort)Strings.TZL.ModesLight.IndexOf(value);
                }
            }
            #endregion

            #region �������� ���������� �������

            public class OutputLogicSignalDataTransfer
            {
                [XmlElement("���1")]
                public LogicSignalUnitDataTransfer Vls1Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���2")]
                public LogicSignalUnitDataTransfer Vls2Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���3")]
                public LogicSignalUnitDataTransfer Vls3Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���4")]
                public LogicSignalUnitDataTransfer Vls4Signal = new LogicSignalUnitDataTransfer();


                [XmlElement("���5")]
                public LogicSignalUnitDataTransfer Vls5Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���6")]
                public LogicSignalUnitDataTransfer Vls6Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���7")]
                public LogicSignalUnitDataTransfer Vls7Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���8")]
                public LogicSignalUnitDataTransfer Vls8Signal = new LogicSignalUnitDataTransfer();

            }


            [XmlElement("��������_����������_�������")]
            public OutputLogicSignalDataTransfer OutputLogicSignalStruct
            {
                get
                {
                    OutputLogicSignalDataTransfer toRet = new OutputLogicSignalDataTransfer();
                    toRet.Vls1Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(0));
                    toRet.Vls2Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(1));
                    toRet.Vls3Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(2));
                    toRet.Vls4Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(3));
                    toRet.Vls5Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(4));
                    toRet.Vls6Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(5));
                    toRet.Vls7Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(6));
                    toRet.Vls8Signal.FillOutValuesFromBitArray(this.GetOutputLogicSignals(7));
                    return toRet;

                }
                set { }
            }

            private const int OUTPUTLOGIC_BITS_CNT = 120;
            private const int OUTPUTLOGIC_BITS_CNT_TZL = 104;
            public BitArray GetOutputLogicSignals(int channel)
            {
                if (channel < 0 || channel > 7)
                {
                    throw new ArgumentOutOfRangeException("Channel");
                }

                ushort[] logicBuffer = new ushort[8];
                Array.ConstrainedCopy(_outputSignals.Value, channel * 8, logicBuffer, 0, 8);
                BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
                BitArray ret = new BitArray(OUTPUTLOGIC_BITS_CNT);
                for (int i = 0; i < ret.Count; i++)
                {
                    ret[i] = buffer[i];
                }
                return ret;

            }

            public BitArray TZL_GetOutputLogicSignals(int channel)
            {
                if (channel < 0 || channel > 7)
                {
                    throw new ArgumentOutOfRangeException("Channel");
                }

                ushort[] logicBuffer = new ushort[8];
                Array.ConstrainedCopy(_outputSignals.Value, channel * 8, logicBuffer, 0, 8);
                BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
                BitArray ret = new BitArray(OUTPUTLOGIC_BITS_CNT);
                for (int i = 0; i < ret.Count; i++)
                {
                    ret[i] = buffer[i];
                }
                return ret;

            }

            public void SetOutputLogicSignals(int channel, BitArray values)
            {
                if (channel < 0 || channel > 7)
                {
                    throw new ArgumentOutOfRangeException("Channel");
                }
                if (120 != values.Count)
                {
                    throw new ArgumentOutOfRangeException("OutputLogic bits count");
                }

                ushort[] logicBuffer = new ushort[8];
                try
                {
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 16; j++)
                        {
                            if (i * 16 + j < 120)
                            {
                                if (values[i * 16 + j])
                                {
                                    logicBuffer[i] += (ushort)Math.Pow(2, j);
                                }
                            }
                        }
                    }

                    Array.ConstrainedCopy(logicBuffer, 0, _outputSignals.Value, channel * 8, 8);
                }
                catch { }
            }

            public void TZL_SetOutputLogicSignals(int channel, BitArray values)
            {
                if (channel < 0 || channel > 7)
                {
                    throw new ArgumentOutOfRangeException("Channel");
                }
                if (120 != values.Count)
                {
                    throw new ArgumentOutOfRangeException("OutputLogic bits count");
                }

                ushort[] logicBuffer = new ushort[8];
                try
                {
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 16; j++)
                        {
                            if (i * 16 + j < 120)
                            {
                                if (values[i * 16 + j])
                                {
                                    logicBuffer[i] += (ushort)Math.Pow(2, j);
                                }
                            }
                        }
                    }

                    Array.ConstrainedCopy(logicBuffer, 0, _outputSignals.Value, channel * 8, 8);
                }
                catch { }
            }
            #endregion

            #region ������� ���������� �������

            public class LogicSignalUnitDataTransfer
            {
                [XmlElement("�������")] public List<string> Value = new List<string>();

                public void FillValues(LogicState[] logicStates)
                {
                    this.Value.Clear();
                    foreach (var logicState in logicStates)
                    {
                        this.Value.Add(logicState.ToString());
                    }
                }

                public void FillOutValuesFromBitArray(BitArray bitArray)
                {
                    this.Value.Clear();
                    for (int bitNum = 0; bitNum < bitArray.Count; bitNum++)
                    {
                        if (bitArray[bitNum])
                        {
                            this.Value.Add(Strings.OutputSignals[bitNum]);
                        }
                    }
                }

            }

            public class InputLogicSignalDataTransfer
            {
                [XmlElement("�1")]
                public LogicSignalUnitDataTransfer And1Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("�2")]
                public LogicSignalUnitDataTransfer And2Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("�3")]
                public LogicSignalUnitDataTransfer And3Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("�4")]
                public LogicSignalUnitDataTransfer And4Signal = new LogicSignalUnitDataTransfer();


                [XmlElement("���1")]
                public LogicSignalUnitDataTransfer Or1Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���2")]
                public LogicSignalUnitDataTransfer Or2Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���3")]
                public LogicSignalUnitDataTransfer Or3Signal = new LogicSignalUnitDataTransfer();
                [XmlElement("���4")]
                public LogicSignalUnitDataTransfer Or4Signal = new LogicSignalUnitDataTransfer();

            }

            [XmlElement("�������_����������_�������")]
            public InputLogicSignalDataTransfer InputLogicSignalStruct
            {
                get
                {
                    InputLogicSignalDataTransfer toRet = new InputLogicSignalDataTransfer();
                    toRet.And1Signal.FillValues(this.GetInputLogicSignals(0));
                    toRet.And2Signal.FillValues(this.GetInputLogicSignals(1));
                    toRet.And3Signal.FillValues(this.GetInputLogicSignals(2));
                    toRet.And4Signal.FillValues(this.GetInputLogicSignals(3));
                    toRet.Or1Signal.FillValues(this.GetInputLogicSignals(4));
                    toRet.Or2Signal.FillValues(this.GetInputLogicSignals(5));
                    toRet.Or3Signal.FillValues(this.GetInputLogicSignals(6));
                    toRet.Or4Signal.FillValues(this.GetInputLogicSignals(7));
                    return toRet;

                }
                set { }
            }

            public LogicState[] GetInputLogicSignals(int channel)
            {
                if (channel < 0 || channel > 7)
                {
                    throw new ArgumentOutOfRangeException("Channel");
                }
                LogicState[] ret = new LogicState[16];
                SetLogicStates(_inputSignals.Value[0x22 + channel * 2], true, ref ret);
                SetLogicStates(_inputSignals.Value[0x22 + channel * 2 + 1], false, ref ret);
                return ret;
            }

            public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
            {
                if (channel < 0 || channel > 7)
                {
                    throw new ArgumentOutOfRangeException("Channel");
                }
                ushort firstWord = 0;
                ushort secondWord = 0;

                for (int i = 0; i < logicSignals.Length; i++)
                {
                    if (i < 8)
                    {
                        if (LogicState.�� == logicSignals[i])
                        {
                            firstWord += (ushort)Math.Pow(2, i);
                        }
                        if (LogicState.������ == logicSignals[i])
                        {
                            firstWord += (ushort)Math.Pow(2, i);
                            firstWord += (ushort)Math.Pow(2, i + 8);
                        }
                    }
                    else
                    {
                        if (LogicState.�� == logicSignals[i])
                        {
                            secondWord += (ushort)Math.Pow(2, i - 8);
                        }
                        if (LogicState.������ == logicSignals[i])
                        {
                            secondWord += (ushort)Math.Pow(2, i - 8);
                            secondWord += (ushort)Math.Pow(2, i);
                        }
                    }
                }
                _inputSignals.Value[0x22 + channel * 2] = firstWord;
                _inputSignals.Value[0x22 + channel * 2 + 1] = secondWord;
            }

            void SetLogicStates(ushort value, bool firstWord, ref  LogicState[] logicArray)
            {
                byte lowByte = Common.LOBYTE(value);
                byte hiByte = Common.HIBYTE(value);

                int start = firstWord ? 0 : 8;
                int end = firstWord ? 8 : 16;

                for (int i = start; i < end; i++)
                {
                    int pow = firstWord ? i : i - 8;
                    logicArray[i] = (byte)(Math.Pow(2, pow)) != ((lowByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.��;
                    //logicArray[i] = (byte)(Math.Pow(2, pow)) != ((hiByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.������;
                    logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.��) ? LogicState.������ : logicArray[i];
                }
            }
            private bool IsLogicEquals(byte hiByte, int pow)
            {
                return (byte)(Math.Pow(2, pow)) == ((hiByte) & (byte)(Math.Pow(2, pow)));
            }



            #endregion

            #region �������� ����������
            //���� ����������
            private slot _automaticsPage = new slot(0x103c, 0x1050);
            private BitArray _bits = new BitArray(8);
            //������� �������� ����������
            public event Handler AutomaticsPageLoadOK;
            public event Handler AutomaticsPageLoadFail;
            public event Handler AutomaticsPageSaveOK;
            public event Handler AutomaticsPageSaveFail;

            public void LoadAutomaticsPage()
            {
                _device.LoadSlot(_device.DeviceNumber, _automaticsPage, "LoadAutomaticsPage" + _device.DeviceNumber, _device);
            }

            public void SaveAutomaticsPage()
            {
                _device.SaveSlot(_device.DeviceNumber, _automaticsPage, "SaveAutomaticsPage" + _device.DeviceNumber, _device);
            }

            [XmlElement("������������_���")]
            public string APV_Cnf
            {
                get
                {
                    byte index = (byte)(Common.LOBYTE(_automaticsPage.Value[0]));
                    if (index >= Strings.TZL.Crat.Count)
                    {
                        index = (byte)(Strings.TZL.Crat.Count - 1);
                    }
                    return Strings.TZL.Crat[index];
                }
                set { _automaticsPage.Value[0] = Common.TOWORD(Common.HIBYTE(_automaticsPage.Value[0]), (byte)Strings.TZL.Crat.IndexOf(value)); }
            }

            [XmlElement("����������_���")]
            public string APV_Blocking
            {
                get
                {
                    byte index = (byte)(Common.LOBYTE(_automaticsPage.Value[1]));
                    if (index >= Strings.TZL.Logic.Count)
                    {
                        index = (byte)(Strings.TZL.Logic.Count - 1);
                    }
                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _automaticsPage.Value[1] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }

            [XmlElement("�����_����������_���")]
            public ulong APV_Time_Blocking
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[2]);
                }
                set
                {
                    _automaticsPage.Value[2] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����_����������_���")]
            public ulong APV_Time_Ready
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[3]);
                }
                set
                {
                    _automaticsPage.Value[3] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����_1_�����_���")]

            public ulong APV_Time_1Krat
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[4]);
                }
                set
                {
                    _automaticsPage.Value[4] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����_2_�����_���")]

            public ulong APV_Time_2Krat
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[5]);
                }
                set
                {
                    _automaticsPage.Value[5] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����_3_�����_���")]

            public ulong APV_Time_3Krat
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[6]);
                }
                set
                {
                    _automaticsPage.Value[6] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����_4_�����_���")]

            public ulong APV_Time_4Krat
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[7]);
                }
                set
                {
                    _automaticsPage.Value[7] = Measuring.SetTime(value);
                }
            }

            [XmlElement("������_���_��_��������������")]

            public string APV_Start
            {
                get
                {
                    byte index = (byte)(Common.HIBYTE(_automaticsPage.Value[0]));
                    if (index >= Strings.TZL.YesNo.Count)
                    {
                        index = (byte)(Strings.TZL.YesNo.Count - 1);
                    }
                    return Strings.TZL.YesNo[index];

                }
                set { _automaticsPage.Value[0] = Common.TOWORD((byte)Strings.TZL.YesNo.IndexOf(value), Common.LOBYTE(_automaticsPage.Value[0])); }
            }

            [XmlElement("����������_�������_���")]

            public bool AVR_Supply_Off
            {
                get
                {
                    return AVR_Add_Signals[0];
                }
                set
                {
                    SetCurrentBit(8, 0, value);
                }
            }

            private void SetCurrentBit(byte num_word, byte num_bit, bool val)
            {
                if (val)
                {
                    if (((_automaticsPage.Value[num_word] >> num_bit) & 1) == 0)
                    {
                        _automaticsPage.Value[num_word] += (ushort)Math.Pow(2, num_bit);
                    }

                }
                else
                {
                    if (((_automaticsPage.Value[num_word] >> num_bit) & 1) == 1)
                    {
                        _automaticsPage.Value[num_word] -= (ushort)Math.Pow(2, num_bit);
                    }
                }
            }

            [XmlElement("�������_����������_�����������_���")]
            public bool AVR_Switch_Off
            {
                get
                {
                    return AVR_Add_Signals[2];
                }
                set
                {
                    SetCurrentBit(8, 2, value);
                }

            }

            [XmlElement("��������������_���")]
            public bool AVR_Self_Off
            {
                get
                {
                    return AVR_Add_Signals[1];
                }
                set
                {
                    SetCurrentBit(8, 1, value);
                }
            }

            [XmlElement("������������_������_���")]
            public bool AVR_Abrasion_Switch
            {
                get
                {
                    return AVR_Add_Signals[3];
                }
                set
                {
                    SetCurrentBit(8, 3, value);
                }
            }

            [XmlElement("����������_������_���_��_���������_�����������")]

            public bool AVR_Reset_Switch
            {
                get
                {
                    return AVR_Add_Signals[7];
                }
                set
                {
                    SetCurrentBit(8, 7, value);
                }
            }

            [XmlIgnore]
            private BitArray AVR_Add_Signals
            {
                get
                {
                    return _bits;
                }
                set
                {
                    _bits = value;
                }

            }

            [XmlElement("����������_���")]
            public string AVR_Blocking
            {
                get
                {
                    ushort index = _automaticsPage.Value[9];
                    if (index >= Strings.TZL.Logic.Count)
                    {
                        index = (byte)(Strings.TZL.Logic.Count - 1);
                    }
                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _automaticsPage.Value[9] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }

            [XmlElement("�����_����������_���")]
            public string AVR_Reset
            {
                get
                {
                    ushort index = _automaticsPage.Value[10];
                    if (index >= Strings.TZL.Logic.Count)
                    {
                        index = (byte)(Strings.TZL.Logic.Count - 1);
                    }
                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _automaticsPage.Value[10] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }

            }

            [XmlElement("������_���")]
            public string AVR_Start
            {
                get
                {
                    ushort index = _automaticsPage.Value[11];
                    if (index >= Strings.TZL.Logic.Count)
                    {
                        index = (byte)(Strings.TZL.Logic.Count - 1);
                    }
                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _automaticsPage.Value[11] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }

            }

            [XmlElement("������������_���")]
            public string AVR_Abrasion
            {
                get
                {
                    ushort index = _automaticsPage.Value[12];
                    if (index >= Strings.TZL.Logic.Count)
                    {
                        index = (byte)(Strings.TZL.Logic.Count - 1);
                    }
                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _automaticsPage.Value[12] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }

            }

            [XmlElement("�����_������������_���")]
            public ulong AVR_Time_Abrasion
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[13]);
                }
                set
                {
                    _automaticsPage.Value[13] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�������_���")]
            public string AVR_Return
            {
                get
                {
                    ushort index = _automaticsPage.Value[14];
                    if (index >= Strings.TZL.Logic.Count)
                    {
                        index = (byte)(Strings.TZL.Logic.Count - 1);
                    }
                    return Strings.TZL.Logic[index];
                }
                set
                {
                    _automaticsPage.Value[14] = (ushort)Strings.TZL.Logic.IndexOf(value);
                }
            }

            [XmlElement("�����_��������_���")]
            public ulong AVR_Time_Return
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[15]);
                }
                set
                {
                    _automaticsPage.Value[15] = Measuring.SetTime(value);
                }
            }

            [XmlElement("�����_����������_���")]
            public ulong AVR_Time_Off
            {
                get
                {
                    return Measuring.GetTime(_automaticsPage.Value[16]);
                }
                set
                {
                    _automaticsPage.Value[16] = Measuring.SetTime(value);
                }
            }
            
            [XmlElement("������������_���114")]
            public string LZSH_CnfV114
            {
                get
                {
                    ushort index = _automaticsPage.Value[18];
                    if (index >= Strings.TZL.LZSH114.Count)
                    {
                        index = (byte)(Strings.TZL.LZSH114.Count - 1);
                    }
                    return Strings.TZL.LZSH114[index];
                }
                set
                {
                    _automaticsPage.Value[18] = (ushort)Strings.TZL.LZSH114.IndexOf(value);
                }
            }

            [XmlElement("�������_���")]
            public double LZSH_Constraint
            {
                get
                {
                    return Measuring.GetConstraint(_automaticsPage.Value[19], ConstraintKoefficient.K_4000, "");
                }
                set
                {
                    _automaticsPage.Value[19] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
                }
            }

            #endregion

            #region ������� ������

            public class CExternalDefenses : ICollection
            {
                public const int LENGTH = 48;
                public const int COUNT = 8;

                #region ICollection Members

                public void Add(ExternalDefenseItem item)
                {
                    _defenseList.Add(item);
                }

                public void CopyTo(Array array, int index)
                {

                }
                [Browsable(false)]
                public bool IsSynchronized
                {
                    get { return false; }
                }
                [Browsable(false)]
                public object SyncRoot
                {
                    get { return this; }
                }

                #endregion

                #region IEnumerable Members

                public IEnumerator GetEnumerator()
                {
                    return _defenseList.GetEnumerator();
                }

                #endregion

                public CExternalDefenses()
                {
                    SetBuffer(new ushort[LENGTH]);
                }

                private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

                public void SetBuffer(ushort[] buffer)
                {
                    _defenseList.Clear();
                    if (buffer.Length != LENGTH)
                    {
                        throw new ArgumentOutOfRangeException("External defense values", (object)LENGTH, "External defense length must be 48");
                    }
                    for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                    {
                        ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                        Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                        _defenseList.Add(new ExternalDefenseItem(temp));
                    }
                }
                [DisplayName("����������")]
                public int Count
                {
                    get
                    {
                        return _defenseList.Count;
                    }
                }

                public ushort[] ToUshort()
                {
                    ushort[] buffer = new ushort[LENGTH];
                    for (int i = 0; i < Count; i++)
                    {
                        Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i * ExternalDefenseItem.LENGTH, ExternalDefenseItem.LENGTH);
                    }
                    return buffer;
                }

                public ExternalDefenseItem this[int i]
                {
                    get
                    {
                        return _defenseList[i];
                    }
                    set
                    {
                        _defenseList[i] = value;
                    }
                }
            };

            public class ExternalDefenseItem
            {
                public const int LENGTH = 6;
                private ushort[] _values = new ushort[LENGTH];

                public ExternalDefenseItem() { }
                public ExternalDefenseItem(ushort[] buffer)
                {
                    SetItem(buffer);
                }
                public override string ToString()
                {
                    return "������� ������";
                }

                [XmlAttribute("��������")]
                public ushort[] Values
                {
                    get
                    {
                        return _values;
                    }
                    set
                    {
                        SetItem(value);
                    }
                }
                public void SetItem(ushort[] buffer)
                {
                    if (buffer.Length != LENGTH)
                    {
                        throw new ArgumentOutOfRangeException("External defense item value", (object)LENGTH, "External defense item length must be 6");
                    }
                    _values = buffer;
                }
                
                [XmlAttribute("�����_v1.12")]
                public string Mode1_12
                {
                    get
                    {
                        ushort index = Common.GetBits(_values[0], 0, 1);
                        if (index >= Strings.TZL.ModesNew.Count)
                        {
                            index = (byte)(Strings.TZL.ModesNew.Count - 1);
                        }
                        return Strings.TZL.ModesNew[index];
                    }
                    set
                    {
                        _values[0] = Common.SetBits(_values[0], (ushort)Strings.TZL.ModesNew.IndexOf(value), 0, 1);
                    }
                }

                [XmlAttribute("���")]
                public bool Osc
                {
                    get
                    {
                        return 0 != (_values[0] & 16);
                    }
                    set
                    {
                        _values[0] = value ? (ushort)(_values[0] |= 16) : (ushort)(_values[0] & ~16);
                    }
                }

                [XmlAttribute("���")]
                public bool AVR
                {
                    get
                    {
                        return 0 != (_values[0] & 32);
                    }
                    set
                    {
                        _values[0] = value ? (ushort)(_values[0] |= 32) : (ushort)(_values[0] & ~32);
                    }
                }


                [XmlAttribute("�����")]
                public bool Reset
                {
                    get
                    {
                        return Common.GetBit(_values[0], 13);
                    }
                    set
                    {
                        _values[0] = Common.SetBit(_values[0], 13, value);
                    }
                }

                [XmlAttribute("���")]
                public bool APV
                {
                    get
                    {
                        return 0 != (_values[0] & 64);
                    }
                    set
                    {
                        _values[0] = value ? (ushort)(_values[0] |= 64) : (ushort)(_values[0] & ~64);
                    }
                }
                [XmlAttribute("����")]
                public bool UROV
                {
                    get
                    {
                        return 0 != (_values[0] & 128);
                    }
                    set
                    {
                        _values[0] = value ? (ushort)(_values[0] |= 128) : (ushort)(_values[0] & ~128);
                    }
                }

                [XmlAttribute("�������_���")]
                public bool APV_Return
                {
                    get
                    {
                        return 0 != (_values[0] & 0x8000);
                    }
                    set
                    {
                        _values[0] = value ? (ushort)(_values[0] |= 0x8000) : (ushort)(_values[0] & ~0x8000);
                    }
                }

                [XmlAttribute("�������")]
                public bool Return
                {
                    get
                    {
                        return 0 != (_values[0] & 0x4000);
                    }
                    set
                    {
                        _values[0] = value ? (ushort)(_values[0] |= 0x4000) : (ushort)(_values[0] & ~0x4000);
                    }
                }


                [XmlAttribute("����������_�����")]
                public string BlockingNumber
                {
                    get
                    {
                        ushort value = _values[1];
                        if (value >= Strings.TZL.ExternalDefense.Count)
                        {
                            value = (ushort)(Strings.TZL.ExternalDefense.Count - 1);
                        }
                        return Strings.TZL.ExternalDefense[value];
                    }
                    set
                    {
                        _values[1] = (ushort)Strings.TZL.ExternalDefense.IndexOf(value);
                    }
                }

                [XmlAttribute("������������_�����")]
                public string WorkingNumber
                {
                    get
                    {
                        ushort value = _values[2];
                        if (value >= Strings.TZL.ExternalDefense.Count)
                        {
                            value = (ushort)(Strings.TZL.ExternalDefense.Count - 1);
                        }
                        return Strings.TZL.ExternalDefense[value];
                    }
                    set
                    {
                        _values[2] = (ushort)Strings.TZL.ExternalDefense.IndexOf(value);
                    }
                }

                [XmlAttribute("������������_�����")]
                public ulong WorkingTime
                {
                    get
                    {
                        return Measuring.GetTime(_values[3]);
                    }
                    set
                    {
                        _values[3] = Measuring.SetTime(value);
                    }
                }

                [XmlAttribute("�������_�����")]
                public string ReturnNumber
                {
                    get
                    {
                        ushort value = _values[4];
                        if (value >= Strings.TZL.ExternalDefense.Count)
                        {
                            value = (ushort)(Strings.TZL.ExternalDefense.Count - 1);
                        }
                        return Strings.TZL.ExternalDefense[value];
                    }
                    set
                    {
                        _values[4] = (ushort)Strings.TZL.ExternalDefense.IndexOf(value);
                    }
                }

                [XmlAttribute("�������_�����")]
                public ulong ReturnTime
                {
                    get
                    {
                        return Measuring.GetTime(_values[5]);
                    }
                    set
                    {
                        _values[5] = Measuring.SetTime(value);
                    }
                }
            }

            private CExternalDefenses _cexternalDefenses = new CExternalDefenses();

            [XmlElement("�������_������")]
            public CExternalDefenses ExternalDefenses
            {
                get { return _cexternalDefenses; }
                set { _cexternalDefenses = value; }
            }


            #endregion

            #region ������� ������

            private slot _tokDefensesMain = new slot(0x1080, 0x10C0);
            private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
            private slot _tokDefenses2Main = new slot(0x1100, 0x1120);
            private slot _tokDefenses2Reserve = new slot(0x1120, 0x1140);

            public void LoadTokDefenses()
            {
                _device.LoadSlot(_device.DeviceNumber, _tokDefenses2Reserve, "LoadTokDefensesReserve2" + _device.DeviceNumber,_device);
                _device.LoadSlot(_device.DeviceNumber, _tokDefensesReserve, "LoadTokDefensesReserve" + _device.DeviceNumber, _device);
                _device.LoadSlot(_device.DeviceNumber, _tokDefenses2Main, "LoadTokDefensesMain2" + _device.DeviceNumber,_device);
                _device.LoadSlot(_device.DeviceNumber, _tokDefensesMain, "LoadTokDefensesMain" + _device.DeviceNumber,_device);
            }

            public void SaveTokDefenses()
            {
                Array.ConstrainedCopy(TokDefensesMain.ToUshort1(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
                Array.ConstrainedCopy(TokDefensesMain.ToUshort2(), 0, _tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
                Array.ConstrainedCopy(TokDefensesReserve.ToUshort1(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
                Array.ConstrainedCopy(TokDefensesReserve.ToUshort2(), 0, _tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);

                _device.SaveSlot(_device.DeviceNumber, _tokDefenses2Main, "SaveTokDefensesMain2" + _device.DeviceNumber, _device);
                _device.SaveSlot(_device.DeviceNumber, _tokDefenses2Reserve, "SaveTokDefensesReserve2" + _device.DeviceNumber,_device);
                _device.SaveSlot(_device.DeviceNumber, _tokDefensesReserve, "SaveTokDefensesReserve" + _device.DeviceNumber,_device);
                _device.SaveSlot(_device.DeviceNumber, _tokDefensesMain, "SaveTokDefensesMain" + _device.DeviceNumber,_device);
            }

            private CTokDefenses _ctokDefensesMain = new CTokDefenses();
            private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

            [XmlElement("�������_������_��������")]
            public CTokDefenses TokDefensesMain
            {
                get
                {

                    return _ctokDefensesMain;
                }
                set
                {
                    _ctokDefensesMain = value;
                }
            }

            [XmlElement("�������_������_���������")]
            public CTokDefenses TokDefensesReserve
            {
                get { return _ctokDefensesReserve; }
                set { _ctokDefensesReserve = value; }
            }


            public class CTokDefenses : ICollection
            {
                public const int LENGTH1 = 64;
                public const int COUNT1 = 10;
                public const int LENGTH2 = 16;
                public const int COUNT2 = 2;
                private ushort[] _buffer;


                #region ICollection Members

                [DisplayName("����������")]
                public int Count
                {
                    get
                    {
                        return _items.Count;
                    }
                }

                public void Add(TokDefenseItem item)
                {
                    _items.Add(item);
                }

                public void CopyTo(Array array, int index)
                {

                }
                [Browsable(false)]
                public bool IsSynchronized
                {
                    get { return false; }
                }
                [Browsable(false)]
                public object SyncRoot
                {
                    get { return this; }
                }

                #endregion

                #region IEnumerable Members

                public IEnumerator GetEnumerator()
                {
                    return _items.GetEnumerator();
                }

                #endregion

                public ushort I
                {
                    get { return _buffer[0]; }
                    set { _buffer[0] = value; }
                }
                public ushort I0
                {
                    get { return _buffer[1]; }
                    set { _buffer[1] = value; }
                }
                public ushort In
                {
                    get { return _buffer[2]; }
                    set { _buffer[2] = value; }
                }
                public ushort I2
                {
                    get { return _buffer[3]; }
                    set { _buffer[3] = value; }
                }

                private List<TokDefenseItem> _items = new List<TokDefenseItem>(12);

                public CTokDefenses()
                {
                    SetBuffer(new ushort[160]);
                }
                public TokDefenseItem this[int i]
                {
                    get
                    {
                        return _items[i];
                    }
                    set
                    {
                        _items[i] = value;
                    }
                }

                public void SetBuffer(ushort[] buffer)
                {
                    _buffer = buffer;
                    _items.Clear();
                    for (int i = 0; i < TokDefenseItem.LENGTH * 12; i += TokDefenseItem.LENGTH)
                    {
                        if (i == TokDefenseItem.LENGTH * 11)
                        {
                            i += 2;
                        }
                        ushort[] item = new ushort[TokDefenseItem.LENGTH];
                        Array.ConstrainedCopy(_buffer, i + 4, item, 0, TokDefenseItem.LENGTH);
                        _items.Add(new TokDefenseItem(item));
                    }

                    for (int i = 0; i < 12; i++)
                    {
                        _items[i].IsIncrease = i < 4 ? true : false;
                        _items[i].IsKoeff500 = i < 8 ? false : true;
                        _items[i].IsI12 = false;

                    }

                    _items[11].IsI12 = true;

                    _items[0].Name = "I>";
                    _items[1].Name = "I>>";
                    _items[2].Name = "I>>>";
                    _items[3].Name = "I>>>>";
                    _items[4].Name = "I2>";
                    _items[5].Name = "I2>>";
                    _items[6].Name = "I0>";
                    _items[7].Name = "I0>>";
                    _items[8].Name = "In>";
                    _items[9].Name = "In>>";
                    _items[10].Name = "Ig";
                    _items[11].Name = "I2/I1";
                }

                public ushort[] ToUshort1()
                {
                    ushort[] buffer = new ushort[LENGTH1];
                    buffer[0] = I;
                    buffer[1] = I0;
                    buffer[2] = In;
                    buffer[3] = I2;

                    for (int i = 0; i < COUNT1; i++)
                    {
                        Array.ConstrainedCopy(_items[i].Values, 0, buffer, 4 + i * TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                    }
                    return buffer;
                }
                public ushort[] ToUshort2()
                {
                    ushort[] buffer = new ushort[LENGTH2];

                    Array.ConstrainedCopy(_items[10].Values, 0, buffer, 0, TokDefenseItem.LENGTH);
                    Array.ConstrainedCopy(_items[11].Values, 0, buffer, 2 + TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                    return buffer;
                }
            };

            public class TokDefenseItem
            {
                public const int LENGTH = 6;
                private bool _isKoeff500;

                [XmlIgnore]
                [Browsable(false)]
                public bool IsKoeff500
                {
                    get { return _isKoeff500; }
                    set { _isKoeff500 = value; }
                }

                private bool _isIncrease;
                private string _name;
                private ushort[] _values = new ushort[LENGTH];

                public TokDefenseItem() { }
                public TokDefenseItem(ushort[] buffer)
                {
                    SetItem(buffer);
                }

                private bool _isI12 = false;

                public bool IsI12
                {
                    get { return _isI12; }
                    set { _isI12 = value; }
                }


                [XmlIgnore]
                [Browsable(false)]
                public bool IsIncrease
                {
                    get { return _isIncrease; }
                    set { _isIncrease = value; }
                }
                [XmlIgnore]
                [Browsable(false)]
                public string Name
                {
                    get { return _name; }
                    set { _name = value; }
                }

                public void SetItem(ushort[] buffer)
                {
                    if (buffer.Length != LENGTH)
                    {
                        throw new ArgumentOutOfRangeException("Tok defense increase1 item value", (object)LENGTH, "Tok defense increase1 item length must be 6");
                    }
                    _values = buffer;
                }

                [XmlAttribute("��������")]
                public ushort[] Values
                {
                    get { return _values; }
                    set { SetItem(value); }
                }


                [XmlAttribute("�����")]
                public string Mode
                {
                    get
                    {
                        ushort index = Common.GetBits(_values[0], 0, 1, 2);
                        if (index >= Strings.TZL.Modes.Count)
                        {
                            index = (byte)(Strings.Modes.Count - 1);
                        }
                        return Strings.TZL.Modes[index];
                    }
                    set
                    {
                        _values[0] = Common.SetBits(_values[0], (ushort)Strings.TZL.Modes.IndexOf(value), 0, 1, 2);
                    }
                }

                [XmlAttribute("�����_v1.11")]
                public string Mode1_11
                {
                    get
                    {
                        ushort index = Common.GetBits(_values[0], 0, 1);
                        if (index >= Strings.TZL.ModesNew.Count)
                        {
                            index = (byte)(Strings.TZL.ModesNew.Count - 1);
                        }
                        return Strings.TZL.ModesNew[index];
                    }
                    set
                    {
                        _values[0] = Common.SetBits(_values[0], (ushort)Strings.TZL.ModesNew.IndexOf(value), 0, 1);
                    }
                }

                [XmlAttribute("���")]
                public bool Osc
                {
                    get { return Common.GetBit(_values[0], 4); }
                    set { _values[0] = Common.SetBit(_values[0], 4, value); }
                }

                [XmlAttribute("�����������_v1_11")]
                public string Oscv1_11
                {
                    get
                    {
                        ushort temp = Common.GetBits(_values[0], 3, 4);
                        byte value = (byte)(Common.LOBYTE(temp) >> 3);
                        value = value >= Strings.TZL.Osc_v1_11.Count ? (byte)(Strings.TZL.Osc_v1_11.Count - 1) : value;
                        return Strings.TZL.Osc_v1_11[value];
                    }
                    set
                    {
                        _values[0] = Common.SetBits(_values[0], (ushort)Strings.TZL.Osc_v1_11.IndexOf(value), 3, 4);
                    }
                }

                [XmlAttribute("���")]
                public bool AVR
                {
                    get { return Common.GetBit(_values[0], 5); }
                    set { _values[0] = Common.SetBit(_values[0], 5, value); }
                }

                [XmlAttribute("���")]
                public bool APV
                {
                    get { return Common.GetBit(_values[0], 6); }
                    set { _values[0] = Common.SetBit(_values[0], 6, value); }
                }

                [XmlAttribute("����")]
                public bool UROV
                {
                    get { return Common.GetBit(_values[0], 7); }
                    set { _values[0] = Common.SetBit(_values[0], 7, value); }
                }

                [XmlAttribute("����������_�����")]
                public string BlockingNumber
                {
                    get
                    {
                        ushort value = _values[1];
                        if (value >= Strings.TZL.Logic.Count)
                        {
                            value = (ushort)(Strings.TZL.Logic.Count - 1);
                        }
                        return Strings.TZL.Logic[value];
                    }
                    set
                    {
                        _values[1] = (ushort)Strings.TZL.Logic.IndexOf(value);
                    }
                }

                [XmlAttribute("���������")]
                public bool Speedup
                {
                    get { return Common.GetBit(_values[0], 15); }
                    set { _values[0] = Common.SetBit(_values[0], 15, value); }
                }


                [XmlAttribute("��������")]
                [Browsable(false)]
                public string Parameter
                {
                    get
                    {
                        string ret;
                        if (IsIncrease)
                        {
                            ret = Common.GetBit(_values[0], 8) ? Strings.TZL.TokParameter[1] : Strings.TZL.TokParameter[0];
                        }
                        else
                        {
                            ret = Common.GetBit(_values[0], 8) ? Strings.TZL.EngineParameter[1] : Strings.TZL.EngineParameter[0];
                        }

                        return ret;
                    }
                    set
                    {
                        bool b = true;
                        if (IsIncrease)
                        {
                            if (Strings.TZL.TokParameter[0] == value)
                            {
                                b = false;
                            }
                        }
                        else
                        {
                            if (Strings.TZL.EngineParameter[0] == value)
                            {
                                b = false;
                            }
                        }

                        _values[0] = Common.SetBit(_values[0], 8, b);
                    }
                }

                [XmlAttribute("��������������")]
                public string Feature
                {
                    get
                    {
                        string ret;
                        if (IsIncrease)
                        {
                            ret = Common.GetBit(_values[0], 12) ? Strings.TZL.FeatureLight[1] : Strings.TZL.Feature[0];
                        }
                        else
                        {
                            ret = Common.GetBit(_values[0], 12) ? Strings.TZL.FeatureLight[0] : Strings.TZL.Feature[0];
                        }

                        return ret;
                    }
                    set
                    {
                        bool b = true;
                        if (IsIncrease)
                        {
                            if (Strings.TZL.FeatureLight[0] == value)
                            {
                                b = false;
                            }
                        }
                        else
                        {
                            if (Strings.TZL.Feature[0] == value)
                            {
                                b = false;
                            }
                        }

                        _values[0] = Common.SetBit(_values[0], 12, b);
                    }
                }

                [XmlAttribute("������������_�������")]
                public double WorkConstraint
                {
                    get
                    {
                        double ret = 0.0;
                        ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                        if (IsKoeff500)
                        {
                            koeff = ConstraintKoefficient.K_500;
                        }
                        if (IsI12)
                        {
                            koeff = ConstraintKoefficient.K_10000;
                        }
                        ret = Measuring.GetConstraint(_values[2], koeff, "123");
                        return ret;
                    }
                    set
                    {
                        ConstraintKoefficient koeff = ConstraintKoefficient.K_4000;
                        if (IsKoeff500)
                        {
                            koeff = ConstraintKoefficient.K_500;
                        }
                        if (IsI12)
                        {
                            koeff = ConstraintKoefficient.K_10000;
                        }
                        _values[2] = Measuring.SetConstraint(value, koeff);
                    }
                }

                [XmlAttribute("������������_�����")]
                public ulong WorkTime
                {
                    get
                    {
                        return Measuring.GetTime(_values[3]);
                    }
                    set
                    {
                        _values[3] = Measuring.SetTime(value);
                    }
                }

                [XmlAttribute("���������_�����")]
                public ulong SpeedupTime
                {
                    get
                    {
                        return Measuring.GetTime(_values[5]);
                    }
                    set
                    {
                        _values[5] = Measuring.SetTime(value);
                    }
                }

            }

            #endregion

            #region �������������
            private Oscilloscope _oscilloscope;

            public Oscilloscope Oscilloscope
            {
                get { return _oscilloscope; }
            }
            #region ����� �����������

            #region ������
            ushort[] _oscilloscopeJournalRecord = new ushort[_sizeOscJournal];
            public ushort[] OscJournal
            {
                get
                {
                    return _oscilloscopeJournalReadSlot.Value;
                }
            }

            public bool OscExist
            {
                get
                {
                    if (_oscilloscopeJournalReadSlot.Value[19] != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public ushort[] OscJournalRecord
            {
                get
                {
                    return _oscilloscopeJournalRecord;
                }
                set
                {
                    _oscilloscopeJournalRecord = value;
                }
            }

            public string OscYear
            {
                get
                {
                    if (_oscilloscopeJournalRecord[1] < 10)
                    {
                        return "0" + _oscilloscopeJournalRecord[1].ToString();
                    }
                    else
                    {
                        return _oscilloscopeJournalRecord[1].ToString();
                    }
                }
            }

            public string OscMonth
            {
                get
                {
                    if (_oscilloscopeJournalRecord[2] < 10)
                    {
                        return "0" + _oscilloscopeJournalRecord[2].ToString();
                    }
                    else
                    {
                        return _oscilloscopeJournalRecord[2].ToString();
                    }
                }
            }

            public string OscDay
            {
                get
                {
                    if (_oscilloscopeJournalRecord[3] < 10)
                    {
                        return "0" + _oscilloscopeJournalRecord[3].ToString();
                    }
                    else
                    {
                        return _oscilloscopeJournalRecord[3].ToString();
                    }
                }
            }

            public string OscHours
            {
                get
                {
                    if (_oscilloscopeJournalRecord[4] < 10)
                    {
                        return "0" + _oscilloscopeJournalRecord[4].ToString();
                    }
                    else
                    {
                        return _oscilloscopeJournalRecord[4].ToString();
                    }
                }
            }

            public string OscMinutes
            {
                get
                {
                    if (_oscilloscopeJournalRecord[5] < 10)
                    {
                        return "0" + _oscilloscopeJournalRecord[5].ToString();
                    }
                    else
                    {
                        return _oscilloscopeJournalRecord[5].ToString();
                    }
                }
            }

            public string OscSeconds
            {
                get
                {
                    if (_oscilloscopeJournalRecord[6] < 10)
                    {
                        return "0" + _oscilloscopeJournalRecord[6].ToString();
                    }
                    else
                    {
                        return _oscilloscopeJournalRecord[6].ToString();
                    }
                }
            }

            public string OscMiliseconds
            {
                get
                {
                    if (_oscilloscopeJournalRecord[7] < 10)
                    {
                        return "0" + (_oscilloscopeJournalRecord[7] * 10).ToString();
                    }
                    else
                    {
                        return (_oscilloscopeJournalRecord[7] * 10).ToString();
                    }
                }
            }

            public int OscReady
            {
                get
                {
                    return Common.UshortUshortToInt(_oscilloscopeJournalRecord[9], _oscilloscopeJournalRecord[8]);
                }
            }

            public int OscPoint
            {
                get
                {
                    return Common.UshortUshortToInt(_oscilloscopeJournalRecord[11], _oscilloscopeJournalRecord[10]);
                }
            }

            public int OscBegin
            {
                get
                {
                    return Common.UshortUshortToInt(_oscilloscopeJournalRecord[13], _oscilloscopeJournalRecord[12]);
                }
            }

            public int OscLen
            {
                get
                {
                    return Common.UshortUshortToInt(_oscilloscopeJournalRecord[15], _oscilloscopeJournalRecord[14]);
                }
            }

            public int OscAfter
            {
                get
                {
                    return Common.UshortUshortToInt(_oscilloscopeJournalRecord[17], _oscilloscopeJournalRecord[16]);
                }
            }

            public ushort OscAlm
            {
                get
                {
                    return _oscilloscopeJournalRecord[18];
                }
            }

            public ushort OscRez
            {
                get
                {
                    return _oscilloscopeJournalRecord[19];
                }
            }
            #endregion

            #region ������������
            private static int _enableOscPageSize = 0x7c;//0x64;//

            public int VEnableOscPageSize
            {
                get { return _enableOscPageSize; }
                set { _enableOscPageSize = value; }
            }

            private static int _oscPageSize = 0x400;

            public int VOscPageSize
            {
                get { return _oscPageSize; }
                set { _oscPageSize = value; }
            }

            private static int _fullOscSize = 0x1A000;//106496

            public int VFullOscSize
            {
                get { return _fullOscSize; }
                set { _fullOscSize = value; }
            }

            private static int _oscSize = 0x19FFA;//106490

            public int VOscSize
            {
                get { return _oscSize; }
                set { _oscSize = value; }
            }

            public ushort[] VOscilloscope
            {
                get
                {
                    return _oscilloscopeRead.Value;
                }
            }
            #endregion

            #endregion
            #endregion

            #region ���� - �����

            public void LoadTimeCycle()
            {
                _device.LoadSlotCycle(_device.DeviceNumber, _datetime, "LoadDateTime" + _device.DeviceNumber, new TimeSpan(1000), 10,_device);
            }
            [Browsable(false)]
            public byte[] DateTime
            {
                get
                {
                    return Common.TOBYTES(_datetime.Value, true);
                }
                set
                {
                    _datetime.Value = Common.TOWORDS(value, true);
                }
            }
            #endregion

            #region ������� ��������/����������
            #region
            public void LoadDiagnostic()
            {
                _device.LoadBitSlot(_device.DeviceNumber, _diagnostic, "LoadDiagnostic" + _device.DeviceNumber,_device);
            }

            public void LoadDiagnosticCycle()
            {
                _device.LoadSlotCycle(_device.DeviceNumber, _diagnostic, "LoadDiagnostic" + _device.DeviceNumber, new TimeSpan(1000), 10,_device);
            }

            public void LoadAnalogSignalsCycle()
            {
                _device.LoadSlotCycle(_device.DeviceNumber, _analog, "LoadAnalogSignals" + _device.DeviceNumber, new TimeSpan(1000), 10,_device);
            }

            public void LoadAnalogSignals()
            {
                _device.LoadSlot(_device.DeviceNumber, _analog, "LoadAnalogSignals" + _device.DeviceNumber,_device);
            }
            #endregion
            public void LoadInputSignals()
            {
                _device.LoadSlot(_device.DeviceNumber, _inputSignals, "LoadInputSignals" + _device.DeviceNumber,_device);
                _device.LoadSlot(_device.DeviceNumber, _konfCount, "�" + _device.DeviceNumber + " ��������� ������������",_device);
            }

            //������ ������������
            int _recordIndex = 0;
            public void LoadOscilloscopeJournal(int recordIndex)
            {
                _recordIndex = recordIndex;
                _device.LoadSlot(_device.DeviceNumber, _oscilloscopeJournalReadSlot, "LoadOscilloscopeJournal" + recordIndex + _device.DeviceNumber,_device);
            }

            //�������������
            private int _oscIndexRead = 0;
            int _temp = 0;
            public void LoadOscilloscopePage(int oscIndexRead)
            {
                if (_oscPageSize % _enableOscPageSize != 0)
                {
                    _temp = 1;
                }
                _oscilloscopeRead = new slot(_startOscilloscope, _endOscilloscope);
                CurrentOscilloscope.Clear();
                _oscIndexRead = oscIndexRead;
                for (int i = 0; i < _oscPageSize / _enableOscPageSize + _temp; i++)
                {

                    _device.LoadSlot(_device.DeviceNumber, _oscilloscopeRead, "LoadOscilloscope" + oscIndexRead + i + _device.DeviceNumber,_device);
                    CurrentOscilloscope.Add(_oscilloscopeRead);
                    if ((i + 1) < (_oscPageSize / _enableOscPageSize) || (_oscPageSize % _enableOscPageSize == 0))
                    {
                        _oscilloscopeRead = new slot((ushort)(_oscilloscopeRead.Start + _enableOscPageSize), (ushort)(_oscilloscopeRead.End + _enableOscPageSize));
                    }
                    else
                    {
                        if ((i + 1) == (_oscPageSize / _enableOscPageSize))
                        {
                            _oscilloscopeRead = new slot((ushort)(_oscilloscopeRead.Start + _enableOscPageSize), (ushort)(_oscilloscopeRead.End + _oscPageSize % _enableOscPageSize));
                        }
                    }
                }
            }
            #region
            public void LoadOutputSignals()
            {
                OutputRele = new COutputRele(_device);
                _device.LoadSlot(_device.DeviceNumber, _outputSignals, "LoadOutputSignals" + _device.DeviceNumber,_device);
            }

            public void LoadExternalDefenses()
            {
                _device.LoadSlot(_device.DeviceNumber, _externalDefenses, "LoadExternalDefenses" + _device.DeviceNumber,_device);
            }

            public void SaveExternalDefenses()
            {
                if (CExternalDefenses.COUNT == ExternalDefenses.Count)
                {
                    Array.ConstrainedCopy(ExternalDefenses.ToUshort(), 0, _externalDefenses.Value, 0, CExternalDefenses.LENGTH);
                }
                _device.SaveSlot(_device.DeviceNumber, _externalDefenses, "SaveExternalDefenses" + _device.DeviceNumber,_device);
            }

            public void SaveOutputSignals()
            {
                if (COutputRele.COUNT == OutputRele.Count)
                {
                    Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, 0x40, COutputRele.LENGTH);
                }
                if (COutputIndicator.COUNT == OutputIndicator.Count)
                {
                    Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, 0x60, COutputIndicator.LENGTH);
                }
                _device.SaveSlot(_device.DeviceNumber, _outputSignals, "SaveOutputSignals" + _device.DeviceNumber,_device);
            }
            #endregion
            public void SaveInputSignals()
            {
                _device.SaveSlot(_device.DeviceNumber, _inputSignals, "SaveInputSignals" + _device.DeviceNumber,_device);
                _device.SaveSlot(_device.DeviceNumber, _konfCount, "SaveKonfCount" + _device.DeviceNumber,_device);
            }


            public void CrushOsc()
            {
                _device.SaveSlot6(_device.DeviceNumber, _oscCrush, "����� ������������� - ����������� ����",_device);
            }
            #region
            public void RemoveDiagnostic()
            {
                _device.MB.RemoveQuery("LoadDiagnostic" + _device.DeviceNumber);
            }

            public void RemoveAnalogSignals()
            {
                _device.MB.RemoveQuery("LoadAnalogSignals" + _device.DeviceNumber);
            }

            public void MakeDiagnosticQuick()
            {
                _device.MakeQueryQuick("LoadDiagnostic" + _device.DeviceNumber);
            }

            public void MakeAnalogSignalsQuick()
            {
                _device.MakeQueryQuick("LoadAnalogSignals" + _device.DeviceNumber);
            }

            public void MakeDiagnosticSlow()
            {
                _device.MakeQuerySlow("LoadDiagnostic" + _device.DeviceNumber);
            }

            public void MakeAnalogSignalsSlow()
            {
                _device.MakeQuerySlow("LoadAnalogSignals");
            }

            public void RemoveDateTime()
            {
                _device.MB.RemoveQuery("LoadDateTime" + _device.DeviceNumber);
            }

            public void MakeDateTimeQuick()
            {
                _device.MakeQueryQuick("LoadDateTime" + _device.DeviceNumber);
            }

            public void MakeDateTimeSlow()
            {
                _device.MakeQuerySlow("LoadDateTime" + _device.DeviceNumber);
            }

            public void SuspendDateTime(bool suspend)
            {
                _device.MB.SuspendQuery("LoadDateTime" + _device.DeviceNumber, suspend);
            }

            public void SuspendSystemJournal(bool suspend)
            {
                List<Query> q = _device.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + _device.DeviceNumber);
                for (int i = 0; i < q.Count; i++)
                {
                    _device.MB.SuspendQuery(q[i].name, suspend);
                }
            }

            public void SuspendAlarmJournal(bool suspend)
            {
                List<Query> q = _device.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + _device.DeviceNumber);
                for (int i = 0; i < q.Count; i++)
                {
                    _device.MB.SuspendQuery(q[i].name, suspend);
                }
            }

            public void RemoveAlarmJournal()
            {
                List<Query> q = _device.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + _device.DeviceNumber);
                for (int i = 0; i < q.Count; i++)
                {
                    _device.MB.RemoveQuery(q[i].name);
                }
                //_stopAlarmJournal = true;

            }

            public void RemoveSystemJournal()
            {
                List<Query> q = _device.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + _device.DeviceNumber);
                for (int i = 0; i < q.Count; i++)
                {
                    _device.MB.RemoveQuery(q[i].name);
                }
                _stopSystemJournal = true;
            }

            public void SaveDateTime()
            {
                _device.SaveSlot(_device.DeviceNumber, _datetime, "SaveDateTime" + _device.DeviceNumber,_device);
            }

            public void LoadSystemJournal()
            {
                _device.LoadSlot(_device.DeviceNumber, _systemJournal[0], "LoadSJRecord0_" + _device.DeviceNumber,_device);
                _stopSystemJournal = false;
            }

            public void LoadAlarmJournal()
            {
                //LoadInputSignals();
                if (!_inputSignals.Loaded)
                {
                    LoadInputSignals();
                }
                _stopAlarmJournal = false;
                _device.LoadSlot(_device.DeviceNumber, _alarmJournal[0], "LoadALRecord0_" + _device.DeviceNumber,_device);
            }

            public void SaveProgram(ushort[] data)
            {

                _program = new slot(_program.Start, (ushort)(_program.Start + data.Length));
                _program.Value = data;
                Query query = new Query();
                query.name = "SaveProgram_" + _device.DeviceNumber;
                query.device = _device.DeviceNumber;
                query.func = 0x10;
                query.priority = 10;
                query.dataStart = _program.Start;
                _program.Loaded = false;

                query.writeBuffer = _program.Value;
                query.globalSize = _program.Size;
                query.localSize = 0x40;

                _device.MB.AddQuery(query);
                StartProgram();
            }
            public void StartProgram()
            {
                ushort[] startcmd = new ushort[1];
                startcmd[0] = 0x00FF;

                _programstart = new slot(_programstart.Start, (ushort)(_programstart.Start + startcmd.Length));
                _programstart.Value = startcmd;
                Query query1 = new Query();
                query1.name = "SaveProgramStart_" + _device.DeviceNumber;
                query1.device = _device.DeviceNumber;
                query1.func = 0x5;
                query1.priority = 10;
                query1.dataStart = _programstart.Start;
                _programstart.Loaded = false;

                query1.writeBuffer = _programstart.Value;
                query1.globalSize = _programstart.Size;
                query1.localSize = 0x01;

                _device.MB.AddQuery(query1);
            }
            public void SaveProgramStorage(ushort[] data)
            {

                _programStorage = new slot(_programStorage.Start, (ushort)(_programStorage.Start + data.Length));
                _programStorage.Value = data;
                Query query = new Query();
                query.name = "SaveProgramStorage_" + _device.DeviceNumber;
                query.device = _device.DeviceNumber;
                query.func = 0x10;
                query.priority = 10;
                query.dataStart = _programStorage.Start;
                _programStorage.Loaded = false;

                query.writeBuffer = _programStorage.Value;
                query.globalSize = _programStorage.Size;
                query.localSize = 0x4;
                _device.MB.AddQuery(query);
            }
            public void LoadProgramStorage()
            {
                _programStorage = new slot(0xC000, 0xE000);
                Query query = new Query();
                query.name = "LoadProgramStorage_" + _device.DeviceNumber;
                query.device = _device.DeviceNumber;
                query.func = 0x04;
                query.priority = 10;
                query.dataStart = _programStorage.Start;
                //_programStorage.Loaded = false;

                query.writeBuffer = _programStorage.Value;
                query.globalSize = _programStorage.Size;
                query.localSize = 0x40;
                _device.MB.AddQuery(query);

                // LoadSlot(DeviceNumber, _programStorage, "LoadProgramStorage_" + DeviceNumber);
            }
            public ushort[] GetProgrammStorage()
            {
                return _programStorage.Value;
            }
            public void LoadProgramSignals(int count)
            {
                _programSignals = new slot(_programSignals.Start, (ushort)(_programSignals.Start + count));
                Query query = new Query();
                query.name = "LoadProgramSignals_" + _device.DeviceNumber;
                query.device = _device.DeviceNumber;
                query.func = 0x04;
                query.priority = 10;
                query.dataStart = _programSignals.Start;
                // _programSignals.Loaded = false;

                query.writeBuffer = _programSignals.Value;
                query.globalSize = _programSignals.Size;
                query.localSize = 0x40;
                query.raiseSpan = new TimeSpan(1000);
                _device.MB.AddQuery(query);
            }
            public void StopLoadSignals()
            {
                _device.MB.RemoveQuery("LoadProgramSignals_" + _device.DeviceNumber);
            }
            public ushort[] GetProgramSignals()
            {
                return this._programSignals.Value;
            }
            #endregion

            //����� ������� ������������
            public void SaveOscilloscopeJournal()
            {
                _oscilloscopeJournalWriteSlot.Value[0] = (ushort)0;
                _device.SaveSlot6(_device.DeviceNumber, _oscilloscopeJournalWriteSlot, "SaveOscilloscopeJournal" + _device.DeviceNumber,_device);
                //SaveSlot(DeviceNumber, _oscilloscopeJournalWriteSlot, "SaveOscilloscopeJournal" + DeviceNumber);
            }

            private int _oscIndexWrite = 0;
            public void SaveOscilloscopePage(int oscIndexWrite)
            { 
                _oscIndexWrite = oscIndexWrite;
                _oscilloscopePageWrite.Value[0] = (ushort)oscIndexWrite;
                _device.SaveSlot6(_device.DeviceNumber, _oscilloscopePageWrite, "SaveOscilloscope" + oscIndexWrite + _device.DeviceNumber,_device);
            }


            #endregion

            #region ������������
            public void Deserialize(string binFileName)
            {
               //��� ����� �������� �� ����� ��������� � ��������� �������� �����, ���������� ����� ��� ����������� �������� ������ ������
                string device = "/��500_�������";
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(binFileName);
                }
                catch (XmlException)
                {
                    throw new FileLoadException("���� ������� ��500 ���������", binFileName);
                }

                try
                {
                    if (doc.SelectSingleNode("��74X_�������")!=null )
                    {
                        device = "/��74X_�������";
                    }

                    DeserializeSlot(doc, device+"/����_�����", _datetime);
                    DeserializeSlot(doc, device+"//�������_�������", _inputSignals);
                    DeserializeSlot(doc, device+"/��������_�������", _outputSignals);
                    DeserializeSlot(doc, device+"/�����������", _diagnostic);
                    DeserializeSlot(doc, device + "/������_�������", _externalDefenses);
                    DeserializeSlot(doc, device + "/������_�������_��������", _tokDefensesMain);
                    DeserializeSlot(doc, device + "/������_�������_���������", _tokDefensesReserve);
                    DeserializeSlot(doc, device + "/������_�������2_��������", _tokDefenses2Main);
                    DeserializeSlot(doc, device + "/������_�������2_���������", _tokDefenses2Reserve);
                    DeserializeSlot(doc, device + "/����������", _automaticsPage);
                    _bits = new BitArray(new []{Common.LOBYTE(_automaticsPage.Value[8])});
                    DeserializeSlot(doc, device + "/������������_�����������", _konfCount);
                }
                catch (NullReferenceException)
                {
                    throw new FileLoadException("���� ������� ��500 ���������", binFileName);
                }

                try
                {
                    ExternalDefenses.SetBuffer(_externalDefenses.Value);
                    ushort[] buffer = new ushort[_tokDefensesReserve.Size + _tokDefenses2Reserve.Size];
                    Array.ConstrainedCopy(_tokDefensesReserve.Value, 0, buffer, 0, _tokDefensesReserve.Size);
                    Array.ConstrainedCopy(_tokDefenses2Reserve.Value, 0, buffer, _tokDefensesReserve.Size, _tokDefenses2Reserve.Size);
                    TokDefensesReserve.SetBuffer(buffer);

                    buffer = new ushort[_tokDefensesMain.Size + _tokDefenses2Main.Size];
                    Array.ConstrainedCopy(_tokDefensesMain.Value, 0, buffer, 0, _tokDefensesMain.Size);
                    Array.ConstrainedCopy(_tokDefenses2Main.Value, 0, buffer, _tokDefensesMain.Size, _tokDefenses2Main.Size);
                    TokDefensesMain.SetBuffer(buffer);
                    ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                    Array.ConstrainedCopy(_outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                    OutputRele.SetBuffer(releBuffer);
                    ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                    Array.ConstrainedCopy(_outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                    OutputIndicator.SetBuffer(outputIndicator);
                }
                catch
                {
                    TokDefensesReserve = new CTokDefenses();
                    TokDefensesMain = new CTokDefenses();
                    OutputRele = new COutputRele(_device);
                    OutputIndicator = new COutputIndicator();
                    throw new FileLoadException("���� ������� ��500 ���������", binFileName);
                }
            }
            
            void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
            {
                slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
            }

            public void PrepareSerialize()
            {
                this.D_Version = this._device.Info.Version == HI_VERSIONS ? "3.00" : this._device.Info.Version;
            }

            public void Serialize(string binFileName)
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("��500_�������"));

                Array.ConstrainedCopy(ExternalDefenses.ToUshort(), 0, _externalDefenses.Value, 0, CExternalDefenses.LENGTH);
                Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, 0x40, COutputRele.LENGTH);
                Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, 0x60, COutputIndicator.LENGTH);
                Array.ConstrainedCopy(TokDefensesMain.ToUshort1(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH1);
                Array.ConstrainedCopy(TokDefensesMain.ToUshort2(), 0, _tokDefenses2Main.Value, 0, CTokDefenses.LENGTH2);
                Array.ConstrainedCopy(TokDefensesReserve.ToUshort1(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH1);
                Array.ConstrainedCopy(TokDefensesReserve.ToUshort2(), 0, _tokDefenses2Reserve.Value, 0, CTokDefenses.LENGTH2);
                
                SerializeSlot(doc, "����_�����", _datetime);
                SerializeSlot(doc, "�������_�������", _inputSignals);
                SerializeSlot(doc, "��������_�������", _outputSignals);
                SerializeSlot(doc, "�����������", _diagnostic);
                SerializeSlot(doc, "������_�������", _externalDefenses);
                SerializeSlot(doc, "������_�������_��������", _tokDefensesMain);
                SerializeSlot(doc, "������_�������_���������", _tokDefensesReserve);
                SerializeSlot(doc, "������_�������2_��������", _tokDefenses2Main);
                SerializeSlot(doc, "������_�������2_���������", _tokDefenses2Reserve);
                SerializeSlot(doc, "����������", _automaticsPage);
                SerializeSlot(doc, "������������_�����������", _konfCount);

                doc.Save(binFileName);
            }

            void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
            {
                XmlElement element = doc.CreateElement(nodeName);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
                doc.DocumentElement.AppendChild(element);
            }
            #endregion

            #region ������ �������
            private slot _constraintSelector = new slot(0x400, 0x401);

            public void ConfirmConstraint()
            {
                _device.SetBit(_device.DeviceNumber, 0, true, "��7500 ������ �������������",_device);
            }

            public void SelectConstraintGroup(bool mainGroup)
            {
                _constraintSelector.Value[0] = mainGroup ? (ushort)0 : (ushort)1;
                _device.SaveSlot(_device.DeviceNumber, _constraintSelector, "��500 �" + _device.DeviceNumber + " ������������ ������ �������",_device);
            }

            #endregion

            public void mb_CompleteExchangeMR7002_0(Query query)
            {
                #region ������� ������
                if ("LoadTokDefensesMain2" + _device.DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _tokDefenses2Main.Value = Common.TOWORDS(query.readBuffer, true);
                    }
                }
                if ("LoadTokDefensesReserve2" + _device.DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _tokDefenses2Reserve.Value = Common.TOWORDS(query.readBuffer, true);
                    }
                }
                if ("LoadTokDefensesReserve" + _device.DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        ushort[] buffer = new ushort[_tokDefensesReserve.Size + _tokDefenses2Reserve.Size];
                        _tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                        Array.ConstrainedCopy(_tokDefensesReserve.Value, 0, buffer, 0, _tokDefensesReserve.Size);
                        Array.ConstrainedCopy(_tokDefenses2Reserve.Value, 0, buffer, _tokDefensesReserve.Size, _tokDefenses2Reserve.Size);
                        _ctokDefensesReserve.SetBuffer(buffer);
                    }
                }
                if ("LoadTokDefensesMain" + _device.DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        ushort[] buffer = new ushort[_tokDefensesMain.Size + _tokDefenses2Main.Size];
                        _tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                        Array.ConstrainedCopy(_tokDefensesMain.Value, 0, buffer, 0, _tokDefensesMain.Size);
                        Array.ConstrainedCopy(_tokDefenses2Main.Value, 0, buffer, _tokDefensesMain.Size, _tokDefenses2Main.Size);
                        _ctokDefensesMain.SetBuffer(buffer);
                        if (null != TokDefensesLoadOK)
                        {
                            TokDefensesLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != TokDefensesLoadFail)
                        {
                            TokDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveTokDefensesMain" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, TokDefensesSaveOK, TokDefensesSaveFail);
                }
                #endregion

                #region ������ ������

                if (_device.IsIndexQuery(query.name, "LoadALRecord"))
                {
                    int index = _device.GetIndex(query.name, "LoadALRecord");
                    if (0 == query.fail)
                    {
                        bool add = false;
                        add = _alarmJournalRecords.AddMessage(query.readBuffer);
                        if (add)
                        {
                            if (_alarmJournalRecords.IsMessageEmpty(index))
                            {
                                if (null != AlarmJournalLoadOk)
                                {
                                    AlarmJournalLoadOk(this);
                                }
                            }
                            else
                            {
                                if (null != AlarmJournalRecordLoadOk)
                                {
                                    AlarmJournalRecordLoadOk(this, index);
                                }
                                index += 1;
                                if (index < ALARMJOURNAL_RECORD_CNT && !_stopAlarmJournal)
                                {
                                    _device.LoadSlot(_device.DeviceNumber, _alarmJournal[index], "LoadALRecord" + index + "_" + _device.DeviceNumber,_device);
                                }
                                else
                                {
                                    if (null != AlarmJournalLoadOk)
                                    {
                                        AlarmJournalLoadOk(this);
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (null != AlarmJournalLoadOk)
                            {
                                AlarmJournalLoadOk(this);
                            }
                        }
                    }
                    else
                    {
                        if (null != AlarmJournalRecordLoadFail)
                        {
                            AlarmJournalRecordLoadFail(this, index);
                        }
                    }

                }
                #endregion

                #region ������ �������
                if (_device.IsIndexQuery(query.name, "LoadSJRecord"))
                {
                    int index = _device.GetIndex(query.name, "LoadSJRecord");
                    if (0 == query.fail)
                    {
                        if (false == _systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                        {
                            if (null != SystemJournalLoadOk)
                            {
                                SystemJournalLoadOk(this);
                            }
                        }
                        else
                        {
                            if (null != SystemJournalRecordLoadOk)
                            {
                                SystemJournalRecordLoadOk(this, index);
                            }
                            index += 1;
                            if (index < SYSTEMJOURNAL_RECORD_CNT && !_stopSystemJournal)
                            {
                                _device.LoadSlot(_device.DeviceNumber, _systemJournal[index], "LoadSJRecord" + index + "_" + _device.DeviceNumber,_device);
                            }
                            else
                            {
                                if (null != SystemJournalLoadOk)
                                {
                                    SystemJournalLoadOk(this);
                                }
                            }

                        }
                    }
                    else
                    {
                        if (null != SystemJournalRecordLoadFail)
                        {
                            SystemJournalRecordLoadFail(this, index);
                        }
                    }

                }
                #endregion

                #region �������� �������
                if ("LoadOutputSignals" + _device.DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                        ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                        Array.ConstrainedCopy(_outputSignals.Value, 0x40, releBuffer, 0, COutputRele.LENGTH);
                        _outputRele.SetBuffer(releBuffer);

                        ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                        Array.ConstrainedCopy(_outputSignals.Value, 0x60, outputIndicator, 0, COutputIndicator.LENGTH);
                        _outputIndicator.SetBuffer(outputIndicator);
                        if (null != OutputSignalsLoadOK)
                        {
                            OutputSignalsLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != OutputSignalsLoadFail)
                        {
                            OutputSignalsLoadFail(this);
                        }
                    }
                }
                if ("SaveOutputSignals" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, OutputSignalsSaveOK, OutputSignalsSaveFail);
                }
                #endregion

                #region ������� ������
                if ("LoadExternalDefenses" + _device.DeviceNumber == query.name)
                {

                    if (0 == query.fail)
                    {
                        _externalDefenses.Value = Common.TOWORDS(query.readBuffer, true);
                        ExternalDefenses.SetBuffer(_externalDefenses.Value);
                        if (null != ExternalDefensesLoadOK)
                        {
                            ExternalDefensesLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != ExternalDefensesLoadFail)
                        {
                            ExternalDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveExternalDefenses" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, ExternalDefensesSaveOK, ExternalDefensesSaveFail);
                }
                #endregion

                #region ������� �������
                #region
                if ("LoadInputSignals" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, InputSignalsLoadOK, InputSignalsLoadFail, ref _inputSignals);
                }
                if ("SaveInputSignals" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, InputSignalsSaveOK, InputSignalsSaveFail);
                }
                #endregion
                if (query.name == "�" + _device.DeviceNumber + " ��������� ������������")
                {
                    _device.Raise(query, KonfCountLoadOK, KonfCountLoadFail, ref _konfCount);
                }
                if ("SaveKonfCount" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, KonfCountSaveOK, KonfCountSaveFail);
                }
                #endregion

                #region �������������
                if ("LoadCount" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, LogicLoadOK, LogicLoadFail, ref _countOsc);
                }

                if ("SaveNum" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, LogicSaveOK, LogicSaveFail);
                }

                #region ����� �����������
                #region ������
                if ("LoadOscilloscopeJournal" + _recordIndex + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, OscJournalLoadOK, OscJournalLoadFail, ref _oscilloscopeJournalReadSlot);
                }
                if ("SaveOscilloscopeJournal" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, OscJournalSaveOK, OscJournalSaveFail);
                }
                #endregion

                #region ������������
                for (int i = 0; i < _oscPageSize / _enableOscPageSize + _temp; i++)
                    if ("LoadOscilloscope" + _oscIndexRead + i + _device.DeviceNumber == query.name)
                    {
                        if (this.CurrentOscilloscope.Count == 0) return;
                        _oscilloscopeRead = CurrentOscilloscope[i];
                        _device.Raise(query, OscilloscopeLoadOK, OscilloscopeLoadFail, ref _oscilloscopeRead/* _oscilloscopeRead*/);
                    }

                if ("SaveOscilloscope" + _oscIndexWrite + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, OscilloscopeSaveOK, OscilloscopeSaveFail);
                }
                #endregion
                #endregion
                #endregion

                #region ��� �������� ����������
                if ("LoadAutomaticsPage" + _device.DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _automaticsPage.Value = Common.TOWORDS(query.readBuffer, true);
                        AVR_Add_Signals = new BitArray(new byte[] { Common.LOBYTE(_automaticsPage.Value[8]) });
                        if (null != AutomaticsPageLoadOK)
                        {
                            AutomaticsPageLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != AutomaticsPageLoadFail)
                        {
                            AutomaticsPageLoadFail(this);
                        }
                    }

                }
                if ("SaveAutomaticsPage" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, AutomaticsPageSaveOK, AutomaticsPageSaveFail);
                }
                #endregion

                #region  ����������� ���� ����� �������


                if ("LoadDiagnostic" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, DiagnosticLoadOk, DiagnosticLoadFail, ref _diagnostic);
                }
                if ("LoadDateTime" + _device.DeviceNumber == query.name)
                {
                    _device.Raise(query, DateTimeLoadOk, DateTimeLoadFail, ref _datetime);
                }


                if ("LoadAnalogSignals" + _device.DeviceNumber == query.name)
                {
                    if (!_inputSignals.Loaded)
                    {
                        LoadInputSignals();
                    }
                    else
                    {
                        _device.Raise(query, AnalogSignalsLoadOK, AnalogSignalsLoadFail, ref _analog);
                    }
                }
                #endregion
            }
        }
        #endregion

        private new void mb_CompleteExchange(object sender, Query query)
        {
            double vers = 0;
            try
            {
                vers = Common.VersionConverter(Info.Version);
            }
            catch { }

            if (vers >= 3.0)
            {
                _TZLDevice.mb_CompleteExchangeMR7002_0(query);
            }
            else
            {
                if (this._oscilloscope == null) // ��� �������� ������� �� ������ ����� ������������� �������� ������ (��������� ������� � ������������ ��� ����������
                    // � ����������� �������������� � �����.
                {
                    this.MB.CompleteExchange -= this.mb_CompleteExchange;
                    return;
                }
                _oscilloscope.OnMbCompleteExchange(sender, query);

                #region ������ ������

                if (IsIndexQuery(query.name, "LoadALRecord"))
                {
                    int index = GetIndex(query.name, "LoadALRecord");
                    if (0 == query.fail)
                    {
                        _alarmJournalRecords.AddMessage(query.readBuffer);
                        if (_alarmJournalRecords.IsMessageEmpty(index))
                        {
                            if (null != AlarmJournalLoadOk)
                            {
                                AlarmJournalLoadOk(this);
                            }
                        }
                        else
                        {
                            if (null != AlarmJournalRecordLoadOk)
                            {
                                AlarmJournalRecordLoadOk(this, index);
                            }
                            index += 1;
                            if (index < ALARMJOURNAL_RECORD_CNT && !_stopAlarmJournal)
                            {
                                LoadSlot(DeviceNumber, _alarmJournal[index], "LoadALRecord" + index + "_" + DeviceNumber, this);
                            }
                            else
                            {
                                if (null != AlarmJournalLoadOk)
                                {
                                    AlarmJournalLoadOk(this);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (null != AlarmJournalRecordLoadFail)
                        {
                            AlarmJournalRecordLoadFail(this, index);
                        }
                    }
                }

                #endregion

                #region ������ �������

                if (IsIndexQuery(query.name, "LoadSJRecord"))
                {
                    int index = GetIndex(query.name, "LoadSJRecord");
                    if (0 == query.fail)
                    {
                        if (false == _systemJournalRecords.AddMessage(Common.TOWORDS(query.readBuffer, true)))
                        {
                            if (null != SystemJournalLoadOk)
                            {
                                SystemJournalLoadOk(this);
                            }
                        }
                        else
                        {
                            if (null != SystemJournalRecordLoadOk)
                            {
                                SystemJournalRecordLoadOk(this, index);
                            }
                            index += 1;
                            if (index < SYSTEMJOURNAL_RECORD_CNT && !_stopSystemJournal)
                            {
                                LoadSlot(DeviceNumber, _systemJournal[index], "LoadSJRecord" + index + "_" + DeviceNumber, this);
                            }
                            else
                            {
                                if (null != SystemJournalLoadOk)
                                {
                                    SystemJournalLoadOk(this);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (null != SystemJournalRecordLoadFail)
                        {
                            SystemJournalRecordLoadFail(this, index);
                        }
                    }
                }

                #endregion

                #region �������� �������

                if ("LoadOutputLogic" + DeviceNumber == query.name)
                {
                    _outputLogic.Value = Common.TOWORDS(query.readBuffer, true);
                }
                if ("LoadOutputSignals" + DeviceNumber == query.name)
                {
                    _outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                    ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                    Array.ConstrainedCopy(_outputSignals.Value, COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
                    _outputRele.SetBuffer(releBuffer);

                    ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                    Array.ConstrainedCopy(_outputSignals.Value, COutputIndicator.OFFSET, outputIndicator, 0,
                                          COutputIndicator.LENGTH);
                    _outputIndicator.SetBuffer(outputIndicator);

                    if (0 == query.fail)
                    {
                        if (null != OutputSignalsLoadOK)
                        {
                            OutputSignalsLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != OutputSignalsLoadFail)
                        {
                            OutputSignalsLoadFail(this);
                        }
                    }
                }
                if ("SaveOutputSignals" + DeviceNumber == query.name)
                {
                    Raise(query, OutputSignalsSaveOK, OutputSignalsSaveFail);
                }

                #endregion

                #region ������� ������

                if ("LoadExternalDefenses1" + DeviceNumber == query.name)
                {
                    _externalDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                }
                if ("LoadExternalDefenses2" + DeviceNumber == query.name)
                {
                    _externalDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);

                    ExternalDefensesMain.SetBuffer(_externalDefensesMain.Value);
                    ExternalDefensesReserve.SetBuffer(_externalDefensesReserve.Value);

                    if (0 == query.fail)
                    {
                        if (null != ExternalDefensesLoadOK)
                        {
                            ExternalDefensesLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != ExternalDefensesLoadFail)
                        {
                            ExternalDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveExternalDefenses1" + DeviceNumber == query.name)
                {
                    Raise(query, ExternalDefensesSaveOK, ExternalDefensesSaveFail);
                }

                #endregion

                #region ������� �������

                if ("LoadInputSignals" + DeviceNumber == query.name)
                {
                    Raise(query, InputSignalsLoadOK, InputSignalsLoadFail, ref _inputSignals);
                }
                if ("SaveInputSignals" + DeviceNumber == query.name)
                {
                    Raise(query, InputSignalsSaveOK, InputSignalsSaveFail);
                }

                #endregion

                #region ������� ������

                if ("LoadTokDefensesReserve" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                        _ctokDefensesReserve.SetBuffer(_tokDefensesReserve.Value);
                    }
                }
                if ("LoadTokDefensesMain" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        _tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                        _ctokDefensesMain.SetBuffer(_tokDefensesMain.Value);
                        if (null != TokDefensesLoadOK)
                        {
                            TokDefensesLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != TokDefensesLoadFail)
                        {
                            TokDefensesLoadFail(this);
                        }
                    }
                }
                if ("SaveTokDefensesMain" + DeviceNumber == query.name)
                {
                    Raise(query, TokDefensesSaveOK, TokDefensesSaveFail);
                }

                #endregion

                #region ����������

                if ("LoadAutomaticsPageReserve" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        Array.ConstrainedCopy(Common.TOWORDS(query.readBuffer, true), 0, _automaticsPageReserve.Value, 0,
                                              CAutomatics.LENGTH);
                        AutomaticsReserve.SetBuffer(_automaticsPageReserve.Value);
                    }
                }

                if ("LoadAutomaticsPageMain" + DeviceNumber == query.name)
                {
                    if (0 == query.fail)
                    {
                        Array.ConstrainedCopy(Common.TOWORDS(query.readBuffer, true), 0, _automaticsPageMain.Value, 0,
                                              CAutomatics.LENGTH);
                        AutomaticsMain.SetBuffer(_automaticsPageMain.Value);
                        if (null != AutomaticsPageLoadOK)
                        {
                            AutomaticsPageLoadOK(this);
                        }
                    }
                    else
                    {
                        if (null != AutomaticsPageLoadFail)
                        {
                            AutomaticsPageLoadFail(this);
                        }
                    }
                }
                if ("SaveAutomaticsPageMain" + DeviceNumber == query.name)
                {
                    Raise(query, AutomaticsPageSaveOK, AutomaticsPageSaveFail);
                }

                #endregion

                if ("LoadDiagnostic" + DeviceNumber == query.name)
                {
                    Raise(query, DiagnosticLoadOk, DiagnosticLoadFail, ref _diagnostic);
                }
                if ("LoadDateTime" + DeviceNumber == query.name)
                {
                    Raise(query, DateTimeLoadOk, DateTimeLoadFail, ref _datetime);
                }
                
                if ("LoadAnalogSignals" + DeviceNumber == query.name)
                {
                    if (!_inputSignals.Loaded)
                    {
                        LoadInputSignals();
                    }
                    else
                    {
                        Raise(query, AnalogSignalsLoadOK, AnalogSignalsLoadFail, ref _analog);
                    }
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        public Type[] Forms
        {
            get
            {
                if (this.DeviceVersion == HI_VERSIONS)
                {
                    this.DeviceVersion = "3.00";
                }

                if (this.DeviceVersion == MIDDLE_VERSIONS)
                {
                    this.DeviceVersion = "2.08";
                }

                if (this.DeviceVersion == LOW_VERSIONS)
                {
                    this.DeviceVersion = "2.03";
                }
                var ver = Common.VersionConverter(this.DeviceVersion);

                if (ver >= 3.0)
                {
                    return new[]
                        {
                            typeof (BSBGLEF),
                            typeof (VOscilloscopeForm),
                            typeof (VAlarmJournalForm),
                            typeof (VSystemJournalForm),
                            typeof (MConfigurationForm),
                            typeof (VMeasuringForm)
                        };
                }
                if (ver >= 2.04)
                {
                    return new[]
                        {
                            typeof (OscilloscopeFormBase),
                            typeof (AlarmJournalForm),
                            typeof (SystemJournalForm),
                            typeof (ConfigurationForm),
                            typeof (MeasuringForm)
                        };
                }

                return new[]
                    {
                        //typeof (OscMessageForm),
                        typeof (OscilloscopeFormBase),
                        typeof (AlarmJournalForm),
                        typeof (SystemJournalForm),
                        typeof (ConfigurationForm),
                        typeof (MeasuringForm)
                    };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                    {
                        LOW_VERSIONS,
                        MIDDLE_VERSIONS,
                        HI_VERSIONS,
                    };
            }
        }
    }
}