<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <script type="text/javascript">
          function translateBoolean(value, elementId){
          var result = "";
          if(value.toString().toLowerCase() == "true")
          result = "��";
          else if(value.toString().toLowerCase() == "false")
          result = "���";
          else
          result = "������������ ��������"
          document.getElementById(elementId).innerHTML = result;
          }
        </script>
      </head>
      <body>

        <xsl:if test="MR7002_0/������_���������� >= 3.0">
          <h3>
            ���������� ��500<xsl:value-of select="MR7002_0/DeviceType"/> ������ �� <xsl:value-of select="MR7002_0/������_����������"/>
          </h3>
          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <b>
              <u>������������ �������� ��������</u>
            </b>
          </DIV>
          <b>������������ �������� ����</b>
          <table border="1">
            <tr bgcolor="#c1ced5">
              <th>
                �����<br/>����
              </th>
              <th>���</th>
              <th>������</th>
              <th>������, ��</th>
            </tr>
            <xsl:for-each select="MR7002_0/��������_����">
              <tr>
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@���"/>
                </td>
                <td>
                  <xsl:value-of select="@������"/>
                </td>
                <td>
                  <xsl:value-of select="@�������"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
          <p></p>
          <b>������������ ������������ �����������</b>
          <table border="1">
            <tr bgcolor="#c1ced5">
              <th>
                �����<br/>����������
              </th>
              <th>���</th>
              <th>������</th>
              <th>
                ����� ���������<br/>�� ������ ���.
              </th>
              <th>
                ����� ���������<br/>�� ��
              </th>
              <th>
                ����� ���������<br/>�� ��
              </th>
            </tr>
            <xsl:for-each select="MR7002_0/��������_����������">
              <tr>
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@���"/>
                </td>
                <td>
                  <xsl:value-of select="@������"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">indexing_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���������"/>,"indexing_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">alarm_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@������"/>,"alarm_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">sys_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������"/>,"sys_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </xsl:for-each>
          </table>
          <p></p>

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <b>
              <u>������������ ������������� ���������������</u>
            </b>
          </DIV>
          <b>������������ ������������� ��������������� ����</b>
          <table border="1">
            <tr bgcolor="#ced5c1">
              <th>
                I� ����������� ���������<br/>��� ��, �
              </th>
              <th>��� ��</th>
              <th>
                ���������� ���������<br/>��� ����, �
              </th>
              <th>
                Imax ������������<br/>���, I�
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/��"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/���_�T"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������������_���"/>
              </td>
            </tr>
          </table>
          <p></p>
          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <b>
              <u>������������ ������� ��������</u>
            </b>
          </DIV>
          <b>������������ ������������</b>
          <table border="1">
            <tr bgcolor="#ced5c1">
              <th>
                ����� � ������������<br/> ������������
              </th>
              <th>��������</th>
              <th>
                ������������ <br/>����������, %
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/OscilloscopePeriod"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/OscilloscopeFix"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/OscilloscopePercent"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ������� ��������</b>
          <table border="1">
            <tr bgcolor="#ced5c1">
              <th>
                ����<br/> ���������
              </th>
              <th>
                ����<br/> ��������
              </th>
              <th>
                ������� <br/>���������
              </th>
              <th>
                �������<br/> ��������
              </th>
              <th>
                �����<br/> ���������
              </th>
              <th>
                ������� �� ���������<br/> ������ �������
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/����_��������_"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����_���������_"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�������_��������_"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�������_���������_"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_������������_"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������������_�������_"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ���������� ����������</b>
          <table border="1">
            <tr bgcolor="#ced5c1">
              <th>�� �����</th>
              <th>�� ������</th>
              <th>�������</th>
              <th>�� ����</th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/������_��_�����"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������_��_������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������_��_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������_��_����"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ���������� �����������</b>
          <table border="1">
            <tr bgcolor="#ced5c1">
              <th>�������</th>
              <th>��������</th>
              <th>�������������</th>
              <th>
                ���������� <br/>���������
              </th>
              <th>T����, ��</th>
              <th>I����, I�</th>
              <th>������� ��, ��</th>
              <th>
                ������������<br/> ���������, ��
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/�����������_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_�����_����"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_���_����"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_�������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����������_������������"/>
              </td>
            </tr>
          </table>
          <p></p>
          <div style="height:90px"> </div>
          <b>������������ ����������� ������ ��� ���</b>
          <table border="1">
            <tr bgcolor="#ced5c1">
              <th>����� 1-16</th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/�����"/>
              </td>
            </tr>
          </table>
          <p></p>

          <!--������� ���������� �������-->

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <b>
              <u>������� ���������� �������</u>
            </b>
          </DIV>
          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" align="center" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������ �1 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/�1/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>


            <div id="col2" style="margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������ �2 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/�2/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="margin-right:20px;">
              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������ �3 (�)</b>
                  <table border="1" align="center" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/�3/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������ �4 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/�4/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>


          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" align="center" style="float:left; margin-right:20px;">

              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������ �5 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/���1/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>


            <div id="col2" align="center" style="margin-right:20px;">
              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������ �6 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/���2/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" align="center" style="margin-right:20px;">
              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������ �7 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/���3/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" align="center" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������ �8 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR7002_0/�������_����������_�������/���4/�������">
                      <tr>
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>

          <b>���� �������������</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="#c1ced5">
              <th>����������</th>
              <th>�����������</th>
              <th>������</th>
              <th>������</th>
              <th>�����������</th>
              <th>�������������� ��������� ����-���������</th>
              <th>����� y�������� ������������</th>
              <th>����� �����������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:variable  name = "v1" select = "MR7002_0/�������������[1]" />
                <xsl:if test="$v1 = 'false' " >��� </xsl:if>
                <xsl:if test="$v1 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v2" select= "MR7002_0/�������������[2]" />
                <xsl:if test="$v2 = 'false' " >��� </xsl:if>
                <xsl:if test="$v2 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v3" select= "MR7002_0/�������������[3]" />
                <xsl:if test="$v3 = 'false' " >��� </xsl:if>
                <xsl:if test="$v3 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v4" select= "MR7002_0/�������������[4]" />
                <xsl:if test="$v4 = 'false' " >��� </xsl:if>
                <xsl:if test="$v4 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v5" select = "MR7002_0/�������������[5]" />
                <xsl:if test="$v5 = 'false' " >��� </xsl:if>
                <xsl:if test="$v5 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v6" select= "MR7002_0/�������������[6]" />
                <xsl:if test="$v6 = 'false' " >��� </xsl:if>
                <xsl:if test="$v6 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v7" select= "MR7002_0/�������������[7]" />
                <xsl:if test="$v7 = 'false' " >��� </xsl:if>
                <xsl:if test="$v7 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v8" select= "MR7002_0/�������������[8]" />
                <xsl:if test="$v8 = 'false' " >��� </xsl:if>
                <xsl:if test="$v8 = 'true' " > ���� </xsl:if>
              </td>
            </tr>
          </table>

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <b>
              <u>������������ ����������</u>
            </b>
          </DIV>
          <b>������������ ���</b>
          <table border="1">
            <tr bgcolor="#bcbcbc">
              <th>
                ������������<br/>���
              </th>
              <th>
                �������<br/>����������
              </th>
              <th>
                �����. ����� �������<br/>���������, ��
              </th>
              <th>T�����, ��</th>
              <th>T1����, ��</th>
              <th>T2����, ��</th>
              <th>T3����, ��</th>
              <th>T4����, ��</th>
              <th>
                ��� �� ����������.<br/>����������
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_1_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_2_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_3_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_4_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������_���_��_��������������"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ���</b>
          <table border="1">
            <tr bgcolor="#bcbcbc">
              <th>
                ���� ���<br/>�� ����.<br/>�������
              </th>
              <th>
                ���� ���<br/>�� ����.���.<br/>����-�
              </th>
              <th>
                ���� ���<br/>�� ����������.<br/>����-�
              </th>
              <th>
                ���� ���<br/>�� ������
              </th>
              <th>
                ������<br/>����-��
              </th>
              <th>
                ������<br/>������<br/> ����-��
              </th>
              <th>
                ����������<br/>������<br/>����-�� ��<br/>����. ���.
              </th>
              <th>
                ������<br/>�����
              </th>
              <th>
                ������<br/>����������
              </th>
              <th>
                �����,<br/>��
              </th>
              <th>
                ������<br/>��������
              </th>
              <th>
                ������,<br/>��
              </th>
              <th>
                �����,<br/>��
              </th>
            </tr>
            <tr>
              <td id="td1">
                <script>
                  translateBoolean(<xsl:value-of select="MR7002_0/����������_�������_���"/>,"td1");
                </script>
              </td>
              <td id="td2">
                <script>
                  translateBoolean(<xsl:value-of select="MR7002_0/�������_����������_�����������_���"/>,"td2");
                </script>
              </td>
              <td id="td3">
                <script>
                  translateBoolean(<xsl:value-of select="MR7002_0/��������������_���"/>,"td3");
                </script>
              </td>
              <td id="td4">
                <script>
                  translateBoolean(<xsl:value-of select="MR7002_0/������������_������_���"/>,"td4");
                </script>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_����������_���"/>
              </td>
              <td id="td5">
                <script>
                  translateBoolean(<xsl:value-of select="MR7002_0/����������_������_���_��_���������_�����������"/>,"td5");
                </script>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_��������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�����_����������_���"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ���</b>
          <table border="1">
            <tr bgcolor="#bcbcbc">
              <th>���� ���</th>
              <th>����������</th>
              <th>�����., ��</th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/���"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/���_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/���_�����"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ����</b>
          <table border="1">
            <tr bgcolor="#bcbcbc">
              <th>���� ����</th>
              <th>����������</th>
              <th>�����., ��</th>
              <th>���-��</th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/����"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����_�����"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/����_���"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ���</b>
          <table border="1">
            <tr bgcolor="#bcbcbc">
              <th>������������ ���</th>
              <th>
                �������<br/> �� ���� ���, I�
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR7002_0/������������_���114"/>
              </td>
              <td>
                <xsl:value-of select="MR7002_0/�������_���"/>
              </td>
            </tr>
          </table>
          <p></p>
          <b>������������ ������� �����</b>
          <table border="1">
            <tr bgcolor="#bcbcbc">
              <th>����� ��</th>
              <th>�����</th>
              <th>����-��</th>
              <th>
                ���� <br/>������.
              </th>
              <th>T����, ��</th>
              <th>�������</th>
              <th>
                ���<br/>��<br/>��������
              </th>
              <th>
                ���� <br/>��������
              </th>
              <th>������, ��</th>
              <th>���-��</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <th>�����</th>
            </tr>
            <xsl:for-each select="MR7002_0/�������_������">
              <tr>
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@�����"/>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">vozvratVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">APVvozvratVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <td>
                  <xsl:value-of select="@�������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@�������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">ocsVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"ocsVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">urovVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@����"/>,"urovVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">apvVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"apvVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">avrVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"avrVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">sbrosVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�����"/>,"sbrosVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </xsl:for-each>
          </table>
          <p></p>
          <div style="height:300px"> </div>
          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <b>
              <u>������������ �����</u>
            </b>
          </DIV>
          <b>������������ ������� �����. �������� �������</b>
          <table border="1">
            <tr bgcolor="#d0d0d0">
              <th>
                C������ <br/>�������� ������
              </th>
              <th>�����</th>
              <th>����������</th>
              <th>��������</th>
              <th>I����, I�</th>
              <th>
                ��������������<br/> ������������
              </th>
              <th>T����, ��/����.</th>
              <th>�����.</th>
              <th>T�����, ��</th>
              <th>���-��</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
            </tr>
            <xsl:for-each select="MR7002_0/�������_������_��������">
              <tr>
                <td>
                  <xsl:if test="position() &lt;=4">
                    I><xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="position() =5">I2></xsl:if>
                  <xsl:if test="position() =6">I2>></xsl:if>
                  <xsl:if test="position() =7">I0></xsl:if>
                  <xsl:if test="position() =8">I0>></xsl:if>
                  <xsl:if test="position() =9">In></xsl:if>
                  <xsl:if test="position() =10">In>></xsl:if>
                  <xsl:if test="position() =11">Ig></xsl:if>
                  <xsl:if test="position() =12">I2/I1</xsl:if>
                </td>
                <td>
                  <xsl:value-of select="@�����"/>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@��������"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�������"/>
                </td>
                <td>
                  <xsl:value-of select="@��������������"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">Uskorenie_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenie_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <td>
                  <xsl:value-of select="@���������_�����"/>
                </td>

                <xsl:element name="td">
                  <xsl:attribute name="id">OscI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"OscI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

                <xsl:element name="td">
                  <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

                <xsl:element name="td">
                  <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

                <xsl:element name="td">
                  <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

              </tr>
            </xsl:for-each>
          </table>


        </xsl:if>
        <xsl:if test="MR500/DeviceVersion &lt; 3.0">

          <h3>
            ���������� ��500<xsl:value-of select="MR500/DeviceType"/> ������ �� <xsl:value-of select="MR500/DeviceVersion"/>
          </h3>
          <p></p>
          <h1>������� �������</h1>
          <h3>��������� ����</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>��������� ��� ��</th>
              <th>��������� ��� ����</th>
              <th>����. ��� ��������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/��"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����"/>
              </td>
              <td>
                <xsl:value-of select="MR500/������������_���"/>
              </td>
            </tr>
          </table>
          <h3>������� �������</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>���� ���������</th>
              <th>���� ��������</th>
              <th>����. ���������</th>
              <th>����. ��������</th>
              <th>����� ���������</th>
              <th>������. �������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/����_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�������_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�������_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�����_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/������������_�������"/>
              </td>
            </tr>
          </table>
          <h3>�����������</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>��������� ���������</th>
              <th>��������� ��������</th>
              <th>�������������</th>
              <th>����������</th>
              <th>����� ����������</th>
              <th>�������, ��</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/�����������_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�����������_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�����������_������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�����������_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�����������_�����"/>
              </td>
              <td>
                <xsl:value-of select="MR500/�����������_�������"/>
              </td>
            </tr>
          </table>
          <h3>������� ����������</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>�� ������</th>
              <th>�� �����</th>
              <th>�������</th>
              <th>�� ����</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/������_��_������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/������_��_�����"/>
              </td>
              <td>
                <xsl:value-of select="MR500/������_��_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/������_��_����"/>
              </td>
            </tr>
          </table>
          <h3>��������� �����</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>����� �����</th>
              <th>�� ���������</th>
              <th>������������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/�����_�����_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/���������_��_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500/������������_���������"/>
              </td>
            </tr>
          </table>

          <!--������� ���������� �������-->

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <h3>      ������� ���������� �������    </h3>
          </DIV>
          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �1 (�)</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/�1/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>


            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �2 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/�2/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �3 (�)</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/�3/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �4 (�)</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/�4/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>
          <p>
            <b></b>
          </p>

          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">

              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �5 (���)</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/���1/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>


            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �6 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/���2/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���.c���. �7 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/���3/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="margin-right:20px;">
              <table border="1" cellspacing="0">
                <td>
                  <b>���.c���. �8 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/�������_����������_�������/���4/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>
          <h1>�������� �������</h1>
          <h3>�������� ����</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>����� ����</th>
              <th>���</th>
              <th>������</th>
              <th>������, ��</th>
            </tr>
            <xsl:for-each select="MR500/��������_����">
              <tr align="center">

                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@���"/>
                </td>
                <td>
                  <xsl:value-of select="@������"/>
                </td>
                <td>
                  <xsl:value-of select="@�������"/>
                </td>

              </tr>
            </xsl:for-each>
          </table>
          <div style="height:50px"> </div>
          <h3>����������</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>����� ����������</th>
              <th>���</th>
              <th>������</th>
              <th>����� �����. �� ���. ���.</th>
              <th>����� �����. �� ��</th>
              <th>����� �����. �� ��</th>
            </tr>
            <xsl:for-each select="MR500/��������_����������">
              <tr align="center">
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@���"/>
                </td>
                <td>
                  <xsl:value-of select="@������"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">indexing_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���������"/>,"indexing_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">alarm_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@������"/>,"alarm_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">sys_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������"/>,"sys_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </xsl:for-each>
          </table>
          <!--�������� ���������� �������-->


          <h2>�������� ���������� �������</h2>

          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���1</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���1/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </table>
            </div>
            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���2</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���2/�������">
                      <tr align="center">

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���3</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���3/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���4</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���4/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>
          <p>
            <b></b>
          </p>

          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">

              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���5</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���5/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </table>
            </div>
            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���6</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���6/�������">
                      <tr align="center">

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���7</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���7/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="float:left; margin-right:20px;">
              <table border="1" cellspacing="0" align="center">
                <td>
                  <b>���������� ������� ���8</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500/��������_����������_�������/���8/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>
          <h2></h2>

          <p></p>
          <h3>���� �������������</h3>
          <table border="1" cellspacing="0">
            <tr bgcolor="#c1ced5" align="center">
              <th>����������</th>
              <th>�����������</th>
              <th>������</th>
              <th>������</th>
              <th>�����������</th>
              <th>��������� ����.</th>
              <th>�� �����������</th>
              <th>����� ����.</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:variable  name = "v1" select = "MR500/�������������[1]" />
                <xsl:if test="$v1 = 'false' " >��� </xsl:if>
                <xsl:if test="$v1 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v2" select= "MR500/�������������[2]" />
                <xsl:if test="$v2 = 'false' " >��� </xsl:if>
                <xsl:if test="$v2 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v3" select= "MR500/�������������[3]" />
                <xsl:if test="$v3 = 'false' " >��� </xsl:if>
                <xsl:if test="$v3 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v4" select= "MR500/�������������[4]" />
                <xsl:if test="$v4 = 'false' " >��� </xsl:if>
                <xsl:if test="$v4 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v5" select = "MR500/�������������[5]" />
                <xsl:if test="$v5 = 'false' " >��� </xsl:if>
                <xsl:if test="$v5 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v6" select= "MR500/�������������[6]" />
                <xsl:if test="$v6 = 'false' " >��� </xsl:if>
                <xsl:if test="$v6 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v7" select= "MR500/�������������[7]" />
                <xsl:if test="$v7 = 'false' " >��� </xsl:if>
                <xsl:if test="$v7 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v8" select= "MR500/�������������[8]" />
                <xsl:if test="$v8 = 'false' " >��� </xsl:if>
                <xsl:if test="$v8 = 'true' " > ���� </xsl:if>
              </td>
            </tr>
          </table>
          <h3>������� ����</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>������, ��</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/����_�������������_������������"/>
              </td>
            </tr>
          </table>
          <h1>����������</h1>
          <h2>�������� �������</h2>
          <h3>���</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>������������</th>
              <th>����������</th>
              <th>����� ����������</th>
              <th>����� ����������</th>
              <th>����� 1 �����</th>
              <th>����� 2 �����</th>
              <th>������ �� ��������������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/����������_��������/������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/�����_����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/�����_����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/�����_1_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/�����_2_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/������_���_��_��������������"/>
              </td>
            </tr>
          </table>
          <h3>���</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>����</th>
              <th>����������</th>
              <th>��������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/����������_��������/����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/��������_x0020_���"/>
              </td>
            </tr>
          </table>
          <h3>���</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>������������</th>
              <th>�������� �������</th>
              <th>����������</th>
              <th>�������</th>
              <th>�������� ��������</th>
              <th>�������� ����. ���.</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/����������_��������/������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/�����_�������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/�������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/��������_��������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/��������_����������_���"/>
              </td>
            </tr>
          </table>
          <h3>����</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>����</th>
              <th>����������</th>
              <th>��������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500/����������_��������/����_����"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/����������_����"/>
              </td>
              <td>
                <xsl:value-of select="MR500/����������_��������/��������_x0020_����"/>
              </td>
            </tr>
          </table>
          <div style="height:120px"> </div>


          <h1>������� ������</h1>
          <h2>�������� �������</h2>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>�����</th>
              <th>�����</th>
              <th>����������</th>
              <th>����</th>
              <th>��������</th>
              <th>���</th>
              <th>����</th>
            </tr>
            <xsl:for-each select="MR500/�������_������_��������">
              <tr align="center">
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@�����"/>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">apv<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"apv<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">urov<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@����"/>,"urov<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </xsl:for-each>
          </table>
          <div style="height:200px"> </div>

          <h1>������� ������</h1>
          <h2>�������� �������</h2>
          <h3>������ I></h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th> </th>
              <th>�����</th>
              <th>����������</th>
              <th>��� �������</th>
              <th>�������</th>
              <th>����-��</th>
              <th>t����, ��</th>
              <th>�����.</th>
              <th>���</th>
              <th>����</th>
              <th>���.</th>
              <th>���.���</th>
            </tr>
            <xsl:for-each select="MR500/�������_������_��������">
              <tr align="center">
                <xsl:if test="position() &lt; 5">
                  <td>
                    <xsl:if test="position() =1">I></xsl:if>
                    <xsl:if test="position() =2">I>></xsl:if>
                    <xsl:if test="position() =3">I>>></xsl:if>
                    <xsl:if test="position() =4">I>>>></xsl:if>
                  </td>
                  <td>
                    <xsl:value-of select="@�����"/>
                  </td>
                  <td>
                    <xsl:value-of select="@����������_�����"/>
                  </td>
                  <td>
                    <xsl:value-of select="@���_x0020_�������"/>
                  </td>
                  <td>
                    <xsl:value-of select="@������������_�������"/>
                  </td>
                  <td>
                    <xsl:value-of select="@��������������"/>
                  </td>
                  <td>
                    <xsl:value-of select="@��������"/>
                  </td>
                  <td>
                    <xsl:value-of select="@�����������"/>
                  </td>
                  <xsl:element name="td">
                    <xsl:attribute name="id">apv_1<xsl:value-of select="position()"/></xsl:attribute>
                    <script>translateBoolean(<xsl:value-of select="@���"/>,"apv_1<xsl:value-of select="position()"/>");</script>
                  </xsl:element>
                  <xsl:element name="td">
                    <xsl:attribute name="id">urov_1<xsl:value-of select="position()"/></xsl:attribute>
                    <script>translateBoolean(<xsl:value-of select="@����"/>,"urov_1<xsl:value-of select="position()"/>");</script>
                  </xsl:element>
                  <xsl:element name="td">
                    <xsl:attribute name="id">usk_1<xsl:value-of select="position()"/></xsl:attribute>
                    <script>translateBoolean(<xsl:value-of select="@����"/>,"usk_1<xsl:value-of select="position()"/>");</script>
                  </xsl:element>
                  <td>
                    <xsl:value-of select="@���������_�����"/>
                  </td>
                </xsl:if>
              </tr>
            </xsl:for-each>
          </table>

          <h3>������ I0></h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th> </th>
              <th>�����</th>
              <th>����������</th>
              <th>��� �������</th>
              <th>�������</th>
              <th>t����, ��</th>

              <th>���</th>
              <th>����</th>
              <th>���.</th>
              <th>���.���</th>
            </tr>
            <xsl:for-each select="MR500/�������_������_��������">
              <tr align="center">
                <xsl:if test="position() &gt; 4">
                  <xsl:if test="position() &lt; 7">
                    <td>
                      <xsl:if test="position() =5">I0></xsl:if>
                      <xsl:if test="position() =6">I0>></xsl:if>
                    </td>
                    <td>
                      <xsl:value-of select="@�����"/>
                    </td>
                    <td>
                      <xsl:value-of select="@����������_�����"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���_x0020_�������"/>
                    </td>

                    <xsl:variable name="ust" select="@������������_�������" />
                    <xsl:variable name="del" select="8" />
                    <xsl:if test=" (@���_x0020_������� = '���������') or (@���_x0020_������� = '���������')">
                      <td>
                        <xsl:value-of select="$ust div $del"/>
                      </td>
                    </xsl:if>

                    <xsl:if test=" @���_x0020_������� = '������'">
                      <td>
                        <xsl:value-of select="@������������_�������"/>
                      </td>
                    </xsl:if>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>

                    <xsl:element name="td">
                      <xsl:attribute name="id">apv_2<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="@���"/>,"apv_2<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">urov_2<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="@����"/>,"urov_2<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">usk_2<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="@����"/>,"usk_2<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <td>
                      <xsl:value-of select="@���������_�����"/>
                    </td>
                  </xsl:if>
                </xsl:if>
              </tr>
            </xsl:for-each>
          </table>
          <h3>������ I2></h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th> </th>
              <th>�����</th>
              <th>����������</th>
              <th>��� �������</th>
              <th>�������</th>
              <th>t����, ��</th>

              <th>���</th>
              <th>����</th>
              <th>���.</th>
              <th>���.���</th>
            </tr>
            <xsl:for-each select="MR500/�������_������_��������">
              <tr align="center">
                <xsl:if test="position() &gt; 6">
                  <xsl:if test="position() &lt; 9">
                    <td>
                      <xsl:if test="position() =7">I2></xsl:if>
                      <xsl:if test="position() =8">I2>></xsl:if>
                    </td>
                    <td>
                      <xsl:value-of select="@�����"/>
                    </td>
                    <td>
                      <xsl:value-of select="@����������_�����"/>
                    </td>
                    <td>
                      <xsl:value-of select="@���_x0020_�������"/>
                    </td>
                    <td>
                      <xsl:value-of select="@������������_�������"/>
                    </td>
                    <td>
                      <xsl:value-of select="@��������"/>
                    </td>

                    <xsl:element name="td">
                      <xsl:attribute name="id">apv_3<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="@���"/>,"apv_3<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">urov_3<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="@����"/>,"urov_3<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <xsl:element name="td">
                      <xsl:attribute name="id">usk_3<xsl:value-of select="position()"/></xsl:attribute>
                      <script>translateBoolean(<xsl:value-of select="@����"/>,"usk_3<xsl:value-of select="position()"/>");</script>
                    </xsl:element>
                    <td>
                      <xsl:value-of select="@���������_�����"/>
                    </td>
                  </xsl:if>
                </xsl:if>
              </tr>
            </xsl:for-each>
          </table>
          <div style="height:50px"> </div>

        </xsl:if>
        <xsl:if test="MR500V2/������_���������� >= 3.0">
          <h3>
            ���������� ��500<xsl:value-of select="MR500V2/DeviceType"/> ������ �� <xsl:value-of select="MR500V2/������_����������"/>
          </h3>
         
          
          <!--������� ���������� �������-->

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <h2>������� ���������� �������</h2>
          </DIV>
          <h2></h2>
          
          <h3>������������ ������������� ��������������� ����</h3>
          <table border="1" style="text-align: center;">
            <tr bgcolor="#ced5c1">
              <th>
                I� ����������� ���������<br/>��� ��, �
              </th>
              <th>��� ��</th>
              <th>
                ���������� ���������<br/>��� ����, �
              </th>
              <th>
                Imax ������������<br/>���, I�
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR500V2/��"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/���_�T"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������������_���"/>
              </td>
            </tr>
          </table>

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <h2>������������ ������� ��������</h2>            
          </DIV>
          <h3>������������ ������������</h3>
          <table border="1" style="text-align: center;">
            <tr bgcolor="#ced5c1">
              <th>
                ����� � ������������<br/> ������������
              </th>
              <th>��������</th>
              <th>
                ������������ <br/>����������, %
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR500V2/OscilloscopePeriod"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/OscilloscopeFix"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/OscilloscopePercent"/>
              </td>
            </tr>
          </table>
          <h3>������������ ������� ��������</h3>
          <table border="1" style="text-align: center;">
            <tr bgcolor="#ced5c1">
              <th>
                ����<br/> ���������
              </th>
              <th>
                ����<br/> ��������
              </th>
              <th>
                ������� <br/>���������
              </th>
              <th>
                �������<br/> ��������
              </th>
              <th>
                �����<br/> ���������
              </th>
              <th>
                ������� �� ���������<br/> ������ �������
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR500V2/����_��������_"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����_���������_"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�������_��������_"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�������_���������_"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_������������_"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������������_�������_"/>
              </td>
            </tr>
          </table>

          <h3>������������ ���������� ����������</h3>
          <table border="1" style="text-align: center;">
            <tr bgcolor="#ced5c1">
              <th>�� �����</th>
              <th>�� ������</th>
              <th>�������</th>
              <th>�� ����</th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR500V2/������_��_�����"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������_��_������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������_��_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������_��_����"/>
              </td>
            </tr>
          </table>

          <h3>������������ ���������� �����������</h3>
          <table border="1" style="text-align: center;">
            <tr bgcolor="#ced5c1">
              <th>�������</th>
              <th>��������</th>
              <th>�������������</th>
              <th>
                ���������� <br/>���������
              </th>
              <th>T����, ��</th>
              <th>I����, I�</th>
              <th>������� ��, ��</th>
              <th>
                ������������<br/> ���������, ��
              </th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR500V2/�����������_��������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_���������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_�����_����"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_���_����"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_�������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����������_������������"/>
              </td>
            </tr>
          </table>

          <div style="height:90px"> </div>
          <h3>������������ ����������� ������ ��� ���</h3>
          <table border="1" style="text-align: center;">
            <tr bgcolor="#ced5c1">
              <th>���� 1</th>
              <th>���� 2</th>
              <th>���� 3</th>
              <th>���� 4</th>
              <th>���� 5</th>
              <th>���� 6</th>
              <th>���� 7</th>
              <th>���� 8</th>
              <th>���� 9</th>
              <th>���� 10</th>
              <th>���� 11</th>
              <th>���� 12</th>
              <th>���� 13</th>
              <th>���� 14</th>
              <th>���� 15</th>
              <th>���� 16</th>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="MR500V2/����1"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����2"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����3"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����4"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����5"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����6"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����7"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����8"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����9"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����10"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����11"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����12"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����13"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����14"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����15"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����16"/>
              </td>
            </tr>
          </table>


          <h3>������� ���������� �������</h3>
          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �1 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/�1/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>


            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �2 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/�2/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �3 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/�3/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="float:left; margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �4 (�)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/�4/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>
          <p>
            <b></b>
          </p>

          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; text-align:center; margin-right:20px;">

              <table border="1" cellspacing="0">
                <td>
                  <b>���.c���. �5 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/���1/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>


            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �6 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/���2/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �7 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/���3/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="margin-right:20px;">
              <table border="1" style="text-align: center;" cellspacing="0">
                <td>
                  <b>���.c���. �8 (���)</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="#ced5c1">
                      <th>����</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/�������_����������_�������/���4/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of  select="format-number(position(), '�#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>

          <h3>���� �������������</h3>
          <table border="1" cellspacing="0">
            <tr bgcolor="#ced5c1">
              <th>����������</th>
              <th>��� - ������</th>
              <th>�����������</th>
              <th>������</th>
              <th>�����������</th>
              <th>������</th>
              <th>������</th>
              <th>������</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:variable  name = "v1" select = "MR500V2/�������������[1]" />
                <xsl:if test="$v1 = 'false' " >��� </xsl:if>
                <xsl:if test="$v1 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v2" select= "MR500V2/�������������[2]" />
                <xsl:if test="$v2 = 'false' " >��� </xsl:if>
                <xsl:if test="$v2 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v3" select= "MR500V2/�������������[3]" />
                <xsl:if test="$v3 = 'false' " >��� </xsl:if>
                <xsl:if test="$v3 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v4" select= "MR500V2/�������������[4]" />
                <xsl:if test="$v4 = 'false' " >��� </xsl:if>
                <xsl:if test="$v4 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v5" select = "MR500V2/�������������[5]" />
                <xsl:if test="$v5 = 'false' " >��� </xsl:if>
                <xsl:if test="$v5 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v6" select= "MR500V2/�������������[6]" />
                <xsl:if test="$v6 = 'false' " >��� </xsl:if>
                <xsl:if test="$v6 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v7" select= "MR500V2/�������������[7]" />
                <xsl:if test="$v7 = 'false' " >��� </xsl:if>
                <xsl:if test="$v7 = 'true' " > ���� </xsl:if>
              </td>
              <td>
                <xsl:variable  name = "v8" select= "MR500V2/�������������[8]" />
                <xsl:if test="$v8 = 'false' " >��� </xsl:if>
                <xsl:if test="$v8 = 'true' " > ���� </xsl:if>
              </td>
            </tr>
          </table>

          <!--�������� ���������� �������-->

          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <h2>������������ �������� ��������</h2>            
          </DIV>
          <h3>������������ �������� ����</h3>
          <table border="1">
            <tr bgcolor="#c1ced5">
              <th>
                �����<br/>����
              </th>
              <th>���</th>
              <th>������</th>
              <th>������, ��</th>
            </tr>
            <xsl:for-each select="MR500V2/��������_����">
              <tr align="center">
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@���"/>
                </td>
                <td>
                  <xsl:value-of select="@������"/>
                </td>
                <td>
                  <xsl:value-of select="@�������"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>

          <h3>������������ ������������ �����������</h3>
          <table border="1">
            <tr bgcolor="#c1ced5" align="center">
              <th>
                �����<br/>����������
              </th>
              <th>���</th>
              <th>������</th>
              <th>
                ����� ���������<br/>�� ������ ���.
              </th>
              <th>
                ����� ���������<br/>�� ��
              </th>
              <th>
                ����� ���������<br/>�� ��
              </th>
            </tr>
            <xsl:for-each select="MR500V2/��������_����������">
              <tr align="center">
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@���"/>
                </td>
                <td>
                  <xsl:value-of select="@������"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">indexing_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���������"/>,"indexing_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">alarm_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@������"/>,"alarm_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">sys_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������"/>,"sys_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </xsl:for-each>
          </table>

          <h3>�������� ���������� �������</h3>

          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���1</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���1/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </table>
            </div>
            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���2</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���2/�������">
                      <tr align="center">

                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���3</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���3/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="float:left; margin-right:20px;">
              <table border="1"  align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���4</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���4/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>
          <p>
            <b></b>
          </p>

          <div id="the whole thing" style="width:100%; overflow: hidden;">
            <div id="col1" style="float:left; margin-right:20px;">

              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���5</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���5/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </table>
            </div>
            <div id="col2" style="float:left; margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���6</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���6/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col3" style="float:left; margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���7</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���7/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>
            <div id="col4" style="float:left; margin-right:20px;">
              <table border="1" align="center" cellspacing="0">
                <td>
                  <b>���������� ������� ���8</b>
                  <table border="1" cellspacing="0" align="center">

                    <tr bgcolor="#c1ced5" align="center">
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="MR500V2/��������_����������_�������/���8/�������">
                      <tr align="center">
                        <td>
                          <xsl:value-of select="current()"/>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>

                </td>
              </table>
            </div>

          </div>


          <P></P>
          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <h2>������������ ����������</h2>
          </DIV>

          <h3>������������ ���</h3>
          <table border="1">
            <tr bgcolor="#bcbcbc" align="center">
              <th>
                ������������<br/>���
              </th>
              <th>
                �������<br/>����������
              </th>
              <th>
                �����. ����� �������<br/>���������, ��
              </th>
              <th>T�����, ��</th>
              <th>T1����, ��</th>
              <th>T2����, ��</th>
              <th>T3����, ��</th>
              <th>T4����, ��</th>
              <th>
                ��� �� ����������.<br/>����������
              </th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500V2/������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_1_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_2_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_3_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_4_�����_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������_���_��_��������������"/>
              </td>
            </tr>
          </table>

          <h3>������������ ���</h3>
          <table border="1">
            <tr bgcolor="#bcbcbc" align="center">
              <th>
                ���� ���<br/>�� ����.<br/>�������
              </th>
              <th>
                ���� ���<br/>�� ����.���.<br/>����-�
              </th>
              <th>
                ���� ���<br/>�� ����������.<br/>����-�
              </th>
              <th>
                ���� ���<br/>�� ������
              </th>
              <th>
                ������<br/>����-��
              </th>
              <th>
                ������<br/>������<br/> ����-��
              </th>
              <th>
                ����������<br/>������<br/>����-�� ��<br/>����. ���.
              </th>
              <th>
                ������<br/>�����
              </th>
              <th>
                ������<br/>����������
              </th>
              <th>
                �����,<br/>��
              </th>
              <th>
                ������<br/>��������
              </th>
              <th>
                ������,<br/>��
              </th>
              <th>
                �����,<br/>��
              </th>
            </tr>
            <tr align="center">
              <td id="td1">
                <script>
                  translateBoolean(<xsl:value-of select="MR500V2/����������_�������_���"/>,"td1");
                </script>
              </td>
              <td id="td2">
                <script>
                  translateBoolean(<xsl:value-of select="MR500V2/�������_����������_�����������_���"/>,"td2");
                </script>
              </td>
              <td id="td3">
                <script>
                  translateBoolean(<xsl:value-of select="MR500V2/��������������_���"/>,"td3");
                </script>
              </td>
              <td id="td4">
                <script>
                  translateBoolean(<xsl:value-of select="MR500V2/������������_������_���"/>,"td4");
                </script>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_����������_���"/>
              </td>
              <td id="td5">
                <script>translateBoolean(<xsl:value-of select="MR500V2/����������_������_���_��_���������_�����������"/>,"td5");</script>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_������������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_��������_���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�����_����������_���"/>
              </td>
            </tr>
          </table>

          <h3>������������ ���</h3>
          <table border="1">
            <tr bgcolor="#bcbcbc" align="center">
              <th>���� ���</th>
              <th>����������</th>
              <th>�����., ��</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500V2/���"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/���_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/���_�����"/>
              </td>
            </tr>
          </table>

          <h3>������������ ����</h3>
          <table border="1">
            <tr bgcolor="#bcbcbc" align="center">
              <th>���� ����</th>
              <th>����������</th>
              <th>�����., ��</th>
              <th>���-��</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500V2/����"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����_����������"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����_�����"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/����_���"/>
              </td>
            </tr>
          </table>

          <h3>������������ ���</h3>
          <table border="1">
            <tr bgcolor="#bcbcbc" align="center">
              <th>������������ ���</th>
              <th>
                �������<br/> �� ���� ���, I�
              </th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="MR500V2/������������_���114"/>
              </td>
              <td>
                <xsl:value-of select="MR500V2/�������_���"/>
              </td>
            </tr>
          </table>

          <h3>������������ ������� �����</h3>
          <table border="1">
            <tr bgcolor="#bcbcbc" align="center">
              <th>����� ��</th>
              <th>�����</th>
              <th>����-��</th>
              <th>
                ���� <br/>������.
              </th>
              <th>T����, ��</th>
              <th>�������</th>
              <th>
                ���<br/>��<br/>��������
              </th>
              <th>
                ���� <br/>��������
              </th>
              <th>������, ��</th>
              <th>���-��</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <th>�����</th>
            </tr>
            <xsl:for-each select="MR500V2/�������_������">
              <tr align="center">
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="@�����_v1.12"/>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">vozvratVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������"/>,"vozvratVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">APVvozvratVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�������_���"/>,"APVvozvratVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <td>
                  <xsl:value-of select="@�������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@�������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">ocsVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"ocsVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">urovVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@����"/>,"urovVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">apvVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"apvVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">avrVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"avrVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="td">
                  <xsl:attribute name="id">sbrosVZ_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@�����"/>,"sbrosVZ_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </xsl:for-each>
          </table>

          <div style="height:300px"> </div>
          <DIV style="FONT-SIZE: 22px; LINE-HEIGHT: 35px">
            <h2>������������ �����</h2>
          </DIV>

          <h3>������������ ������� �����. �������� �������</h3>
          <table border="1">
            <tr bgcolor="#d0d0d0" align="center">
              <th>
                C������ <br/>�������� ������
              </th>
              <th>�����</th>
              <th>����������</th>
              <th>��������</th>
              <th>I����, I�</th>
              <th>
                ��������������<br/> ������������
              </th>
              <th>T����, ��/����.</th>
              <th>�����.</th>
              <th>T�����, ��</th>
              <th>���-��</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
            </tr>
            <xsl:for-each select="MR500V2/�������_������_��������">
              <tr align="center">
                <td>
                  <xsl:if test="position() &lt;=4">
                    I><xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="position() =5">I2></xsl:if>
                  <xsl:if test="position() =6">I2>></xsl:if>
                  <xsl:if test="position() =7">I0></xsl:if>
                  <xsl:if test="position() =8">I0>></xsl:if>
                  <xsl:if test="position() =9">In></xsl:if>
                  <xsl:if test="position() =10">In>></xsl:if>
                  <xsl:if test="position() =11">Ig></xsl:if>
                  <xsl:if test="position() =12">I2/I1</xsl:if>
                </td>
                <td>
                  <xsl:value-of select="@�����"/>
                </td>
                <td>
                  <xsl:value-of select="@����������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="@��������"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�������"/>
                </td>
                <td>
                  <xsl:value-of select="@��������������"/>
                </td>
                <td>
                  <xsl:value-of select="@������������_�����"/>
                </td>
                <xsl:element name="td">
                  <xsl:attribute name="id">Uskorenie_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���������"/>,"Uskorenie_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <td>
                  <xsl:value-of select="@���������_�����"/>
                </td>

                <xsl:element name="td">
                  <xsl:attribute name="id">OscI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"OscI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

                <xsl:element name="td">
                  <xsl:attribute name="id">UROVI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@����"/>,"UROVI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

                <xsl:element name="td">
                  <xsl:attribute name="id">apvI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"apvI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

                <xsl:element name="td">
                  <xsl:attribute name="id">avrI_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>translateBoolean(<xsl:value-of select="@���"/>,"avrI_<xsl:value-of select="position()"/>");</script>
                </xsl:element>

              </tr>
            </xsl:for-each>
          </table>


        </xsl:if>

      </body>



    </html>
  </xsl:template>
</xsl:stylesheet>
