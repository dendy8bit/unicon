using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR500.OscLoader;
using BEMN.MR500.OscLoader.HelpClasses;
using BEMN.MR500.OscLoader.Structures;

namespace BEMN.MR500
{
    public partial class OscilloscopeFormBase : Form , IFormView
    {
        #region [Constants]
        private const string OSC_JOURNAL_IS_EMPTY = "������ ������������ ����";
        private const string SAVE_OSC_FAIL = "������ ���������� �����";
        private const string READING_JURNAL = "������ ������� ������������ ";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string READING_OSC = "���� ������ �������������";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string FAIL_LOAD_OSC = "���������� ��������� ������������";

        #endregion [Constants]
        
        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OldOscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OldOscJournalLoader _oldOscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;

        private readonly MemoryEntity<OneWordStruct> _resetOscStruct;

        private int TT;
        private int TTnp;


        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private CountingListOld _countingListold;

        private readonly DataTable _table;
        private MR500 _device;
        #endregion [Private fields]
        
        public OscilloscopeFormBase()
        {
            this.InitializeComponent();
        }

        public OscilloscopeFormBase(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._resetOscStruct = device.ResetOsc;
            if (Common.VersionConverter(this._device.DeviceVersion) < 2.04)
            {
                this._loadFromFileButton.Click += _loadOldFromFileButton_Click;
                this._saveToFileButton.Click += this._saveOldToFileButton_Click;
            }
            else
            {
                this._loadFromFileButton.Click += _loadFromFileButton_Click;
                this._saveToFileButton.Click += this._saveToFileButton_Click;
            }
            
            //��������� �������
            this._oldOscJournalLoader = new OldOscJournalLoader(device);
            this._oldOscJournalLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oldOscJournalLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._resetOscStruct.WriteFail+=HandlerHelper.CreateHandler( this,() =>
            {
                MessageBox.Show("��������� �������� �������������");
            });
            this._resetOscStruct.WriteOk += HandlerHelper.CreateHandler(this, () =>
            {
                this._resetOsc.Enabled = false;
               this.StartRead();
            });
            //��������� ������� �����
            this._currentOptionsLoader = new CurrentOptionsLoader(device);
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, () =>
            {
                this.TT = this._currentOptionsLoader.MeasureStruct.TT;
                this.TTnp = this._currentOptionsLoader.MeasureStruct.TTNP;
                this._oldOscJournalLoader.StartReadOscJournal();
                
            });
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //��������� ������������
            this._pageLoader = new OldOscPageLoader(device);
            this._statusProgress.Maximum = this._pageLoader.SlotCount;
            this._pageLoader.SlotReadSuccessful += HandlerHelper.CreateActionHandler(this, this._statusProgress.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadFail += HandlerHelper.CreateActionHandler(this, this.OscReadFail);

            this._table = this.GetJournalDataTable();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(OscilloscopeFormBase); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Properties.Resources.oscilloscope.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "�������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void ReadRecord()
        {

            if (this._oldOscJournalLoader.RecordStruct.OscCount == 0)
            {
                this._statusActionLabel.Text = OSC_JOURNAL_IS_EMPTY;
                this._readOscilloscope.Enabled = false;
            }
            else
            {
                this._readOscilloscope.Enabled = true;
                this._resetOsc.Enabled = true;
                this._statusActionLabel.Text = "������ ������� ��������";
                this._oldOscJournalLoader.RecordStruct.TT = this.TT;
                this._oldOscJournalLoader.RecordStruct.TTNP = this.TTnp;
                this._table.Rows.Add
                    (
                        this._oldOscJournalLoader.RecordStruct.GetDate,
                        this._oldOscJournalLoader.RecordStruct.GetTime,
                        this._oldOscJournalLoader.RecordStruct.Stage,
                        this._oldOscJournalLoader.RecordStruct.WorkParametr,
                        this._oldOscJournalLoader.RecordStruct.ValueParametr,
                        this._oldOscJournalLoader.RecordStruct.Ia,
                        this._oldOscJournalLoader.RecordStruct.Ib,
                        this._oldOscJournalLoader.RecordStruct.Ic,
                        this._oldOscJournalLoader.RecordStruct.Io,
                        this._oldOscJournalLoader.RecordStruct.Ig,
                        this._oldOscJournalLoader.RecordStruct.I0,
                        this._oldOscJournalLoader.RecordStruct.I1,
                        this._oldOscJournalLoader.RecordStruct.I2

                    );
                this._oscJournalDataGrid.DataSource = this._table;
                this._oscJournalDataGrid.Refresh();
            }
        }

        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusActionLabel.Text = READ_OSC_FAIL;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusActionLabel.Text = OSC_LOAD_SUCCESSFUL;
            this._saveToFileButton.Enabled = true;
            if (Common.VersionConverter(this._device.DeviceVersion) <2.04)
            {
                this.CountingListOld = new CountingListOld(this._pageLoader.ResultArray, this._oldOscJournalLoader.RecordStruct, this._currentOptionsLoader.MeasureStruct);
            }
            else
            {
                this.CountingList = new CountingList(this._pageLoader.ResultArray, this._oldOscJournalLoader.RecordStruct, this._currentOptionsLoader.MeasureStruct);
            }
                

            this._saveToFileButton.Enabled = true;
            this._showButton.Enabled = true;
        }
        /// <summary>
        /// ������������ �� ���������
        /// </summary>
        private void OscReadFail()
        {
            this._statusActionLabel.Text = FAIL_LOAD_OSC;
            this._saveToFileButton.Enabled = false;
            this._showButton.Enabled = false;
        }

        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._showButton.Enabled = true;
            }
        }

        public CountingListOld CountingListOld
        {
            get { return this._countingListold; }
            set
            {
                this._countingListold = value;
                this._showButton.Enabled = true;
            }
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��500_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].HeaderText);
            }
            return table;
        }

        private void OscShow()
        {

            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[1800], new OscJournalStruct(),
                    new MeasureTransStruct());
            }

            if (Validator.GetVersionFromRegistry())
                {
                    string fileName;
                    if (this._countingList.IsLoad)
                    {
                        fileName = this._countingList.FilePath;
                    }
                    else
                    {
                        fileName = Validator.CreateOscFileNameCfg($"��500 v{this._device.DeviceVersion} �������������");
                        this._countingList.Save(fileName);
                    }
                    System.Diagnostics.Process.Start(
                        Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                            "Oscilloscope.exe"),
                        fileName);
                }
        }

        private void OldOscShow()
        {

            if (this.CountingListOld == null)
            {
                this.CountingListOld = new CountingListOld(new ushort[1800], new OscJournalStruct(),
                    new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingListold.IsLoad)
                {
                    fileName = this._countingListold.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"{this._device.DeviceType} v{this._device.DeviceVersion} �������������");
                    this._countingListold.Save(fileName);
                }
                System.Diagnostics.Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                        "Oscilloscope.exe"), fileName);
            }
        }

        private void _showButton_Click(object sender, EventArgs e)
        {
            if (Common.VersionConverter(this._device.DeviceVersion) < 2.04)
            {
                this.OldOscShow();
            }
            else
            {
                this.OscShow();
            }
            
        }
         private void _reloadJournal_Click(object sender, EventArgs e)
         {
             this._statusActionLabel.Text = READING_JURNAL;
                    this.StartRead();
                }
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._table.Clear();
            this._oscJournalDataGrid.Refresh();
            this._currentOptionsLoader.StartRead();
        }
        
        private void _readOscilloscope_Click(object sender, EventArgs e)
        {
            this._statusActionLabel.Text = READING_OSC;
            this._statusProgress.Value = 0;
            OscJournalStruct oscJournal = this._oldOscJournalLoader.RecordStruct;
            oscJournal.Version = Common.VersionConverter(this._device.DeviceVersion);
            this._pageLoader.StartRead(oscJournal);
        }

        private void _saveToFileButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._countingList.Save(this._saveOscilloscopeDlg.FileName);
                }
                catch
                {
                    MessageBox.Show(SAVE_OSC_FAIL);
                }
            }
        }

        private void _saveOldToFileButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._countingListold.Save(this._saveOscilloscopeDlg.FileName);
                }
                catch
                {
                    MessageBox.Show(SAVE_OSC_FAIL);
                }
            }
        }

        private void _loadFromFileButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusActionLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                    this._openOscilloscopeDlg.FileName);
            }
            catch
            {
                this._statusActionLabel.Text = "���������� ��������� ������������";
            }
        }

        private void _loadOldFromFileButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingListOld = CountingListOld.Load(this._openOscilloscopeDlg.FileName);
                this._statusActionLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                    this._openOscilloscopeDlg.FileName);

            }
            catch 
            {
                this._statusActionLabel.Text = "���������� ��������� ������������";
            }
        }

        private void _resetOsc_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("�������� �������������?", "��������", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                this._resetOscStruct.Value.Word = 0;
                this._resetOscStruct.SaveStruct6();
            }
        }
    }
}