using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MR500.Properties;

namespace BEMN.MR500
{
    public partial class SystemJournalForm : Form , IFormView
    {
        private MR500 _device;
        private DataTable _dataTable;

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._device.SystemJournalRecordLoadOk += new IndexHandler(this._device_SystemJournalRecordLoadOk);
            this._device.SystemJournalRecordLoadFail += new IndexHandler(this._device_SystemJournalRecordLoadFail);
        }

        void _device_SystemJournalRecordLoadFail(object sender, int index)
        {
            this._readSysJournalBut.Enabled = true;
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnSysJournalIndexLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnSysJournalIndexLoadOk(int i)
        {
            this._journalProgress.Increment(1);
            this._journalCntLabel.Text = (i + 1).ToString();
            this._sysJournalGrid.Rows.Add(new object[] { i + 1, this._device.SystemJournal[i].time, this._device.SystemJournal[i].msg });
            if (i == 127)
            {
                this._readSysJournalBut.Enabled = true;
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR500.Properties.Resources.js; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            FillTable();

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                _dataTable.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void FillTable()
        {
            _dataTable = new DataTable("��500_������_�������");
            _dataTable.Columns.Add("�����");
            _dataTable.Columns.Add("�����");
            _dataTable.Columns.Add("���������");
            for (int i = 0; i < _sysJournalGrid.Rows.Count; i++)
            {
                this._dataTable.Rows.Add(this._sysJournalGrid["_indexCol", i].Value,
                    this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value);
            }
        }


        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��500_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(new object[]{table.Rows[i].ItemArray[0],
                                                      table.Rows[i].ItemArray[1],
                                                      table.Rows[i].ItemArray[2]});
            }
        }

        private void SystemJournalForm_Activated(object sender, EventArgs e)
        {
            this._device.SuspendSystemJournal(false);
        }

        private void SystemJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._device.SuspendSystemJournal(true);
        }

        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveSystemJournal();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readSysJournalBut.Enabled = false;
            this._device.RemoveSystemJournal();
            this._device.SystemJournal = new MR500.CSystemJournal();
            this._journalProgress.Value = 0;
            this._journalProgress.Maximum = MR500.SYSTEMJOURNAL_RECORD_CNT;
            this._sysJournalGrid.Rows.Clear();
            this._device.LoadSystemJournal();
        }

        private void _saveToHtmlButton_Click(object sender, EventArgs e)
        {
            this._saveSysJournalHtmlDlg.FileName = string.Format("������ ������� �� 500 v{0}", _device.DeviceVersion);

            try
            {
                if (DialogResult.OK == _saveSysJournalHtmlDlg.ShowDialog())
                {
                    FillTable();
                    HtmlExport.Export(this._dataTable, this._saveSysJournalHtmlDlg.FileName, Resources.MR500SJ);
                    _journalCntLabel.Text = "������ ��������";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "������ ����������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _journalCntLabel.Text = "������ �� ��������";
                throw;
            }
        }

    }

}