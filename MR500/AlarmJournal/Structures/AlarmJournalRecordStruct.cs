﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR500.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";

        #endregion [Constants]

        #region [Private fields]

        [Layout(0)] private ushort _message; //1
        [Layout(1)] private ushort _year; //2
        [Layout(2)] private ushort _month; //3
        [Layout(3)] private ushort _date; //4
        [Layout(4)] private ushort _hour; //5
        [Layout(5)] private ushort _minute; //6
        [Layout(6)] private ushort _second; //7
        [Layout(7)] private ushort _millisecond; //8
        [Layout(8)] private ushort _stage; // 2 Код повреждения** 9
        [Layout(9)] private ushort _type; //3 Тип повреждения*** 10
        [Layout(10)] private ushort _value; //4 Значение повреждения //Знач. сраб пар 11
        [Layout(11)] private ushort _ia; //5 Значение  //F 12
        [Layout(12)] private ushort _ib; //6 Значение   //Ua 13 
        [Layout(13)] private ushort _ic; //7 Значение   //Ub 14 
        [Layout(14)] private ushort _i0; //8 Значение //Uc 15
        [Layout(15)] private ushort _i1; //9 Значение //Uab 16
        [Layout(16)] private ushort _i2; //10 Значение  //Ubc 17 
        [Layout(17)] private ushort _in; //11 Значение //Uca 18
        [Layout(18)] private ushort _ig; //12 Значение //U0 19
        [Layout(19, Count = 8)] private ushort[] _res;
        [Layout(20)] private ushort _discrets;

        #endregion [Private fields]

        #region [Properties]
        public bool IsEmpty
        {
            get
            {
                return this._date + this._month + this._year + this._hour + this._minute + this._second +
                       this._millisecond + this._stage + this._type + this._value == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        public string TypeDamage
        {
            get
            {
                string res = string.Empty;
                res += Common.GetBit(this._type, 3) ? "_" : "";
                res += Common.GetBit(this._type, 0) ? "A" : "";
                res += Common.GetBit(this._type, 1) ? "B" : "";
                res += Common.GetBit(this._type, 2) ? "C" : "";
                return res;
            }
        }

        public string Stage
        {
            get { return Validator.Get(_stage,AjStrings.Stage,0,1,2,3,4,5) ; }
        }

        public string GroupOfSetpoints(double version)
        {

            return Validator.GetJornal(_stage, AjStrings.AlarmJournalSetpointsGroup, 8, 9, 10);
        }

        public string WorkParametr
        {
            get { return Validator.Get(this._type, AjStrings.Parametr, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        public string ValueParametr(MeasuringTransformer measure)
        {
            int index = Common.GetBits(this._type, 8, 9, 10, 11, 12, 13, 14, 15) >> 8;
            //Iг,In
            if ((index == 1) || (index == 2))
            {
                return ValuesConverterCommon.Analog.GetI(this._value, measure.TTNP * 5);
            }
            //Ia,Ib... I2
            if ((index >= 3) & (index <= 8))
            {
                return ValuesConverterCommon.Analog.GetI(this._value, measure.TT * 40);
            }
            if (index == 27)
            {
                return ValuesConverterCommon.Analog.GetI(this._value, 100).Replace("А", "%");
            }
            return string.Empty;
        }

        public ushort Value
        {
            get { return _value; }
        }

        public ushort Ia
        {
            get { return _ia; }
        }

        public ushort Ib
        {
            get { return _ib; }
        }

        public ushort Ic
        {
            get { return _ic; }
        }

        public ushort I0
        {
            get { return _i0; }
        }

        public ushort I1
        {
            get { return _i1; }
        }

        public ushort I2
        {
            get { return _i2; }
        }

        public ushort In
        {
            get { return _in; }
        }

        public ushort Ig
        {
            get { return _ig; }
        }

        public string Discret
        {
            get { return  "  " + GetMask(this._discrets); }
        }

        /// <summary>
        /// Ивертирует двоичное представление числа
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Инвертированое двоичное представление</returns>
        private string GetMask(ushort value)
        {
            var chars = Convert.ToString(value, 2).PadLeft(16, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }
        
        public string Message
        {
            get { return Validator.Get(_message, AjStrings.Message); }
        }
       
        #endregion [Properties]
    }
}
