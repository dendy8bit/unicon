namespace BEMN.MR500.AlarmJournal
{
    partial class VAlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._openSysJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._deserializeBut = new System.Windows.Forms.Button();
            this._serializeBut = new System.Windows.Forms.Button();
            this._readBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._saveToHtmlButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._valueParametr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Dcol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _openSysJounralDlg
            // 
            this._openSysJounralDlg.DefaultExt = "xml";
            this._openSysJounralDlg.Filter = "*.xml) | *.xml";
            this._openSysJounralDlg.RestoreDirectory = true;
            this._openSysJounralDlg.Title = "������� ������  ������ ��� ��500";
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��500_������_������";
            this._saveSysJournalDlg.Filter = "*.xml) | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������ ��� ��500";
            // 
            // _deserializeBut
            // 
            this._deserializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._deserializeBut.Location = new System.Drawing.Point(500, 324);
            this._deserializeBut.Name = "_deserializeBut";
            this._deserializeBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeBut.TabIndex = 21;
            this._deserializeBut.Text = "��������� �� �����";
            this._deserializeBut.UseVisualStyleBackColor = true;
            this._deserializeBut.Click += new System.EventHandler(this._deserializeBut_Click);
            // 
            // _serializeBut
            // 
            this._serializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._serializeBut.Location = new System.Drawing.Point(383, 324);
            this._serializeBut.Name = "_serializeBut";
            this._serializeBut.Size = new System.Drawing.Size(111, 23);
            this._serializeBut.TabIndex = 20;
            this._serializeBut.Text = "��������� � ����";
            this._serializeBut.UseVisualStyleBackColor = true;
            this._serializeBut.Click += new System.EventHandler(this._serializeBut_Click);
            // 
            // _readBut
            // 
            this._readBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBut.Location = new System.Drawing.Point(5, 324);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(75, 23);
            this._readBut.TabIndex = 19;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 357);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(786, 22);
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            this._configProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(108, 17);
            this._statusLabel.Text = "������ � �������";
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._stage,
            this._typeCol,
            this._codeCol,
            this._valueParametr,
            this._group,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._I0Col,
            this._I1Col,
            this._I2Col,
            this._InCol,
            this._IgCol,
            this._Dcol});
            this._journalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(786, 303);
            this._journalGrid.TabIndex = 24;
            // 
            // _saveToHtmlButton
            // 
            this._saveToHtmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToHtmlButton.Location = new System.Drawing.Point(632, 324);
            this._saveToHtmlButton.Name = "_saveToHtmlButton";
            this._saveToHtmlButton.Size = new System.Drawing.Size(142, 23);
            this._saveToHtmlButton.TabIndex = 25;
            this._saveToHtmlButton.Text = "��������� � HTML";
            this._saveToHtmlButton.UseVisualStyleBackColor = true;
            this._saveToHtmlButton.Click += new System.EventHandler(this._saveToHtmlButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "html";
            this._saveJournalHtmlDialog.FileName = "������ ������ ��500";
            this._saveJournalHtmlDialog.Filter = "*.html | *.html";
            this._saveJournalHtmlDialog.Title = "���������  ������ ������ ��� ��500";
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "�";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "�����";
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._msgCol.DataPropertyName = "���������";
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 71;
            // 
            // _stage
            // 
            this._stage.DataPropertyName = "�������";
            this._stage.HeaderText = "�������";
            this._stage.Name = "_stage";
            this._stage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._stage.Width = 54;
            // 
            // _typeCol
            // 
            this._typeCol.DataPropertyName = "��� �����������";
            this._typeCol.HeaderText = "��� �����������";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._typeCol.Width = 93;
            // 
            // _codeCol
            // 
            this._codeCol.DataPropertyName = "�������� ������������";
            this._codeCol.HeaderText = "�������� ������������";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 126;
            // 
            // _valueParametr
            // 
            this._valueParametr.DataPropertyName = "�������� ��������� ������������";
            this._valueParametr.HeaderText = "�������� ��������� ������������";
            this._valueParametr.Name = "_valueParametr";
            this._valueParametr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._valueParametr.Width = 176;
            // 
            // _group
            // 
            this._group.DataPropertyName = "������ �������";
            this._group.HeaderText = "������ �������";
            this._group.Name = "_group";
            this._group.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._group.Width = 82;
            // 
            // _IaCol
            // 
            this._IaCol.DataPropertyName = "Ia{A}";
            this._IaCol.HeaderText = "Ia{A}";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 37;
            // 
            // _IbCol
            // 
            this._IbCol.DataPropertyName = "Ib{A}";
            this._IbCol.HeaderText = "Ib{A}";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 37;
            // 
            // _IcCol
            // 
            this._IcCol.DataPropertyName = "Ic{A}";
            this._IcCol.HeaderText = "Ic{A}";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 37;
            // 
            // _I0Col
            // 
            this._I0Col.DataPropertyName = "I0{A}";
            this._I0Col.HeaderText = "I0{A}";
            this._I0Col.Name = "_I0Col";
            this._I0Col.ReadOnly = true;
            this._I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I0Col.Width = 37;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "I1{A}";
            this._I1Col.HeaderText = "I1{A}";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 37;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "I2{A}";
            this._I2Col.HeaderText = "I2{A}";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 37;
            // 
            // _InCol
            // 
            this._InCol.DataPropertyName = "In{A}";
            this._InCol.HeaderText = "In{A}";
            this._InCol.Name = "_InCol";
            this._InCol.ReadOnly = true;
            this._InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InCol.Width = 37;
            // 
            // _IgCol
            // 
            this._IgCol.DataPropertyName = "Ig{A}";
            this._IgCol.HeaderText = "Ig{A}";
            this._IgCol.Name = "_IgCol";
            this._IgCol.ReadOnly = true;
            this._IgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IgCol.Width = 37;
            // 
            // _Dcol
            // 
            this._Dcol.DataPropertyName = "������� ������� 1 . . . 16";
            this._Dcol.HeaderText = "������� ������� 1 . . . 16";
            this._Dcol.Name = "_Dcol";
            this._Dcol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Dcol.Width = 104;
            // 
            // VAlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 379);
            this.Controls.Add(this._saveToHtmlButton);
            this.Controls.Add(this._journalGrid);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._deserializeBut);
            this.Controls.Add(this._serializeBut);
            this.Controls.Add(this._readBut);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 418);
            this.Name = "VAlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog _openSysJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.Button _deserializeBut;
        private System.Windows.Forms.Button _serializeBut;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.Button _saveToHtmlButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stage;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _valueParametr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _group;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Dcol;
    }
}