﻿using System.Collections.Generic;

namespace BEMN.MR500.AlarmJournal
{
    public static class AjStrings
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Основная",
                        "Резервная"
                    };
            }
        }
        /// <summary>
        /// Сработавшая ступень
        /// </summary>
        public static Dictionary<ushort, string> Stage
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {1,"I>"},
                    {2,"I>>"},
                    {3,"I>>>"},
                    {4,"I>>>>"},
                    {5,"I2>"},
                    {6, "I2>>"},
                    {7,"I0>"},
                    {8,"I0>>"},
                    {9,"In>"},
                    {10,"In>>"},
                    {11,"Iг>"},
                    {12,"I2/I1"}, //12                       
                    {25, "ВЗ-1"},
                    {26,"ВЗ-2"},
                    {27,"ВЗ-3"},
                    {28,"ВЗ-4"},
                    {29,"ВЗ-5"},
                    {30,"ВЗ-6"},
                    {31,"ВЗ-7"},
                    {32,"ВЗ-8"}
                };
            }
        }
        public static List<string> Message
        {
            get
            {
                return new List<string>
                    {
                        "Журнал пуст",
                        "Сигнализация",
                        "Отключение",
                        "Работа",
                        "Неуспешное АПВ",
                        "Журнал пуст",
                        "Журнал пуст",
                        "Нет сообщения",
                        "Нет сообщения"

                    };
            }
        }
        public static Dictionary<ushort, string> Parametr
        {
            get
            {
                return new Dictionary<ushort, string>
                   {
                       {0,"B3"}, // 
                       {1,"Ig"},//
                       {2,"In"},// 
                       {3,"Ia"}, // 
                       {4,"Ib"}, // 
                       {5,"Ic"}, // 
                       {6,"I0"}, //
                       {7,"I1"}, // 
                       {8,"I2"}, // 
                       {27,"Обрыв провода"}
                   };
            }
        }
    }
}
