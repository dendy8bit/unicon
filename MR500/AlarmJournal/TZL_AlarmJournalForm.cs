using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR500.AlarmJournal.Structures;
using BEMN.MR500.Properties;

namespace BEMN.MR500.AlarmJournal
{
    public partial class VAlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "������ � ������� - {0}";
        private const string READ_AJ_FAIL = "���������� ��������� ������ ������";
        private const string READ_AJ = "������ ������� ������";
        private const string TABLE_NAME = "��500_������_������";
        private const string JOURNAL_IS_EMPTY = "������ ����";
        private const string JOURNAL_SAVED = "������ ��������";
        private const string PARAMETERS_LOADED = "��������� ���������";
        private const string FAULT_LOAD_PARAMETERS = "���������� ��������� ���������";
        private const string RECORDS_IN_JOURNAL_PATTERN = "� ������� {0} ���������";
        private const string FILE_NAME_PATTERN = "������ ������ �� 500 v{0}";
        #endregion [Constants]


        #region [Private fields]


        private DataTable _table;
        private readonly MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private readonly MemoryEntity<MeasuringTransformer> _measuringChannel;
        private MR500 _device;
        #endregion [Private fields]


        #region [Ctor's]
        public VAlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public VAlarmJournalForm(MR500 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._alarmRecord = device.TZLDevice.AlarmJournalNew;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);
            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.TZLDevice.MeasureTransAj;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);

            this._table = this.GetJournalDataTable();
            this._journalGrid.DataSource = this._table;
            this._configProgressBar.Maximum = this._alarmRecord.Slots.Count;
        }
        #endregion [Ctor's]


        #region [Help members]
        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }
        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
            this.Reading = false;
        }
        private void JournalReadFail()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.Reading = false;
        }
        private void ReadJournalOk()
        {
            int i = 1;
            MeasuringTransformer measure = this._measuringChannel.Value;
            double version = Common.VersionConverter(this._device.DeviceVersion);
            foreach (AlarmJournalRecordStruct record in this._alarmRecord.Value.Records.Where(record => !record.IsEmpty))
            {
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Stage,
                        record.TypeDamage,
                        record.WorkParametr,
                        record.ValueParametr(measure),
                        record.GroupOfSetpoints(version),
                        ValuesConverterCommon.Analog.GetISmall(record.Ia, measure.TT*40),
                        ValuesConverterCommon.Analog.GetISmall(record.Ib, measure.TT*40),
                        ValuesConverterCommon.Analog.GetISmall(record.Ic, measure.TT*40),
                        ValuesConverterCommon.Analog.GetISmall(record.I0, measure.TT*40),
                        ValuesConverterCommon.Analog.GetISmall(record.I1, measure.TT*40),
                        ValuesConverterCommon.Analog.GetISmall(record.I2, measure.TT*40),
                        ValuesConverterCommon.Analog.GetISmall(record.In, measure.TTNP*5),
                        ValuesConverterCommon.Analog.GetISmall(record.Ig, measure.TTNP*5),
                        record.Discret
                    );
                i++;
            }
            this._journalGrid.DataSource = this._table;
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this._journalGrid.Update();
            this.Reading = false;
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].HeaderText);
            }
            return table;
        }
        #endregion [Help members]


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR500); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(VAlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion


        #region [Event Handlers]

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this.Reading = true;
            this._measuringChannel.LoadStruct();
        }


        private bool Reading
        {
            set
            {
                this._readBut.Enabled = !value;
                this._serializeBut.Enabled = !value;
                this._deserializeBut.Enabled = !value;
            }
        }
        
        #endregion [Event Handlers]

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveSysJournalDlg.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveSysJournalDlg.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            if (this._openSysJounralDlg.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                this._table.ReadXml(this._openSysJounralDlg.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveToHtmlButton_Click(object sender, EventArgs e)
        {
            this._saveJournalHtmlDialog.FileName = string.Format(FILE_NAME_PATTERN, _device.DeviceVersion);
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR500AJ_300);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
