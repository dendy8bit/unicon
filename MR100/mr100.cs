using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Data;

using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Devices;
using BEMN.Utils;
using BEMN.Forms;
using DescriptionAttribute=System.ComponentModel.DescriptionAttribute;

//using NUnit.Framework;

namespace BEMN.MR100.Forms
{
    
    
    public class MeasuringForm : System.Windows.Forms.Form ,IFormView
    {
        [Browsable(false)]
        public Type ClassType
        {
            get
            {
                return this.GetType();
            }
        }

        private string _deviceVersion;
        private MR100Class _device;
        private GroupBox _analogGroup;
        private Label label4;
        private TextBox _uBk2Box;
        private Label label3;
        private TextBox _uBk1Box;
        private Label _uSpool;
        private TextBox _uSpoolBox;
        private Label _inILabel;
        private TextBox _inI;
        private GroupBox _falgsGroup;
        private LedControl _secondStepLed;
        private LedControl _secondIOStepLed;
        private LedControl _firstStepLed;
        private LedControl _firstStepIOLed;
        private Label label1;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private LedControl _errorBKKvitLed;
        private LedControl _errorBkFoundedLed;
        private LedControl _levelWorkLed;
        private LedControl _levelActiveLed;
        private Label label6;
        private Label label5;
        private Label label2;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private LedControl _errorCRCProgramLed;
        private LedControl _errorValuesCRC;
        private LedControl _errorChainKvitLed;
        private LedControl _errorChainLed;
        private Label label15;
        private LedControl _errorACP;
        private GroupBox _avariaGroup;
        private DataGridView _journalGrid;
        private DataGridViewTextBoxColumn Msg;
        private Button _kvitBut;
        private GroupBox _versionGroup;
        private Button _refreshVersionBut;
        private Label label17;
        private Label label16;
        private TextBox _versionBox;
        private TextBox _serialBox;
        private Label _lblAutomatOff;
        private LedControl _automatLedOff;

        private LedControl[] _leds;

       public MeasuringForm()
       {
         
       }

        public MeasuringForm(MR100Class device)
       {
           InitializeComponent();
           
            //_leds = new LedControl[] { _firstStepIOLed,_firstStepLed,_secondIOStepLed,_secondStepLed,
            //                           _levelActiveLed,_levelWorkLed,_errorBkFoundedLed,_errorBKKvitLed,
            //                           _errorChainLed,_errorChainKvitLed,_errorACP,_errorValuesCRC,_errorCRCProgramLed};

           _device = device;

            _deviceVersion = _device.DeviceVersion;
            this.InitializeLeds(_deviceVersion);
           _device._signalsOk += new Handler(_device__signalsOk);
           _device._journalOk += new Handler(_device__journalOk);
           _device._versionOk += new Handler(_device__versionOk);
           _device._singnalsFail += new Handler(_device__singnalsFail);

           _device.LoadNominal();
           _device.LoadJournalCycle();
           _device.LoadSignalsCycle();
           //_device.LoadVersion();
                                                
                    
       }

        public double VersionConverter(string version)
        {
            var ver = Common.VersionConverter(version);
            return ver;
        }

        private void InitializeLeds(string vers)
        {
            if (this.VersionConverter(vers) >= 5.30)
            {
                _automatLedOff.Visible = true;
                _lblAutomatOff.Visible = true;
                _leds = new LedControl[] { _firstStepIOLed,_firstStepLed,_secondIOStepLed,_secondStepLed,
                                       _levelActiveLed,_levelWorkLed,_errorBkFoundedLed,_errorBKKvitLed,
                                       _errorChainLed,_errorChainKvitLed,_errorACP,_errorValuesCRC,_errorCRCProgramLed,_automatLedOff};
            }
            else
            {
                _leds = new LedControl[] { _firstStepIOLed,_firstStepLed,_secondIOStepLed,_secondStepLed,
                                       _levelActiveLed,_levelWorkLed,_errorBkFoundedLed,_errorBKKvitLed,
                                       _errorChainLed,_errorChainKvitLed,_errorACP,_errorValuesCRC,_errorCRCProgramLed};
            }
        }

        void _device__versionOk(object sender)
        {
            if (IsVersionBoxCreated)
            {
                Invoke(new OnDeviceEventHandler(GetVersion));    
            }
            
        }


        public void GetVersion()
        {
            _versionBox.Text = _device.Info.Version;
            _serialBox.Text = _device.Info.Plant;
           
        }
        

        void _device__singnalsFail(object sender)
        {
                   
            Invoke(new OnDeviceEventHandler(OffSignals));
        }

        private void OffSignals()
        {
            //try
            //{
            //    _device.PortNum = _device.Port;
            //    _device.LoadVersion();

            //}
            //catch (Exception r)
            //{
            //    _device.LoadVersion();
            //}

            try
            {
                for (int i = 0; i < _leds.Length; i++)
                {
                    _leds[i].State = LedState.Off;
                }

                _journalGrid.Rows.Clear();
                _uBk1Box.Text = "0.0";
                _uBk2Box.Text = "0.0";
                _uSpoolBox.Text = "0.0";
                _inI.Text = "0.0";
            }
            catch (InvalidOperationException)
            {
            	
            }
            
        }

        private void GetJournal()
        {
            _journalGrid.Rows.Clear();
            for(int i = 0; i < _device.AlarmJournal.Count; i++)
            {
                _journalGrid.Rows.Add(new object[] { _device.AlarmJournal[i].dscr, _device.AlarmJournal[i].prm });
                
            }
           
        }

        private void _device__journalOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(GetJournal));
            }
            catch(ObjectDisposedException)
            {

            }
            
        }


        
        private void GetSignals()
        {
            if (this.Created)
            {
                _inI.Text = String.Format("{0:F2} �", _device.I_In);
                _uSpoolBox.Text = String.Format("{0:F2} �", _device.USpool);
                _uBk1Box.Text = String.Format("{0:F2} �", _device.U_BK1);
                _uBk2Box.Text = String.Format("{0:F2} �", _device.U_BK2);
                 LedManager.SetLeds(_leds, _device.Flags);
            }

        }

        void _device__signalsOk(object sender)
        {
            if (this.Created)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(GetSignals));
                }
                catch (ObjectDisposedException)
                {
                    //MessageBox.Show(e.ToString());
                }
                
            }
           
            
                       
        }
       
       private void form_Activated(object sender, System.EventArgs e)
       {
           try
           {
               //_device._signalsOk += new Handler(_device__signalsOk);
               //_device._journalOk += new Handler(_device__journalOk);
               //_device._versionOk += new Handler(_device__versionOk);
               //_device._singnalsFail += new Handler(_device__singnalsFail);

               //_device.LoadNominal();
               //_device.LoadJournalCycle();
               //_device.LoadSignalsCycle();
               //_device.LoadVersion();
               _device.MB.GetQuery("nominal" + _device.DeviceNumber).raiseSpan = new TimeSpan(_device.MB.Port.WaitByte * 10000);
               _device.MB.GetQuery("journal" + _device.DeviceNumber).raiseSpan = new TimeSpan(_device.MB.Port.WaitByte * 10000);
               _device.MB.GetQuery("signals" + _device.DeviceNumber).raiseSpan = new TimeSpan(_device.MB.Port.WaitByte * 10000);
           }
           catch (NullReferenceException)
           {


           }
           
         
       }
     
                   
       #region IFormView
       public Type FormDevice
       {
          get
          {
            return typeof(MR100Class);
          }
       }

        public bool Deletable
        {
            get
            {
                return false;
            }
        }

        public bool ForceShow
        {
            get
            {
                return false;
            }
        }

       
      public string NodeName
      {
         get
         {
            return "��������� ����������";
         }
      }

        public INodeView[] ChildNodes
        {
            get
            {
                return null;
            }
        }

        public Type ThisType
        {
            get
            {
                return this.GetType();
            }
        }

      public Image NodeImage
      {
         get
         {
             return AssemblyResources.Resources.measuring.ToBitmap();
         }
      }
        public bool IsMustBeAdded(object parentTag)
        {
            return true;
        }
  
        #endregion

        private bool IsVersionBoxCreated
        {
            get
            {
                return LedManager.IsControlsCreated(new Control[] { _versionBox, _serialBox }) != false;
            }
        }

        private void InitializeComponent()
        {
            this._analogGroup = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this._uBk2Box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._uBk1Box = new System.Windows.Forms.TextBox();
            this._uSpool = new System.Windows.Forms.Label();
            this._uSpoolBox = new System.Windows.Forms.TextBox();
            this._inILabel = new System.Windows.Forms.Label();
            this._inI = new System.Windows.Forms.TextBox();
            this._falgsGroup = new System.Windows.Forms.GroupBox();
            this._lblAutomatOff = new System.Windows.Forms.Label();
            this._automatLedOff = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._kvitBut = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this._firstStepLed = new BEMN.Forms.LedControl();
            this._levelActiveLed = new BEMN.Forms.LedControl();
            this._levelWorkLed = new BEMN.Forms.LedControl();
            this._errorACP = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._firstStepIOLed = new BEMN.Forms.LedControl();
            this.label1 = new System.Windows.Forms.Label();
            this._errorCRCProgramLed = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._secondStepLed = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this._errorValuesCRC = new BEMN.Forms.LedControl();
            this._secondIOStepLed = new BEMN.Forms.LedControl();
            this.label5 = new System.Windows.Forms.Label();
            this._errorBkFoundedLed = new BEMN.Forms.LedControl();
            this._errorChainKvitLed = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._errorBKKvitLed = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._errorChainLed = new BEMN.Forms.LedControl();
            this._avariaGroup = new System.Windows.Forms.GroupBox();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this.Msg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parametr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._versionGroup = new System.Windows.Forms.GroupBox();
            this._refreshVersionBut = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._versionBox = new System.Windows.Forms.TextBox();
            this._serialBox = new System.Windows.Forms.TextBox();
            this._analogGroup.SuspendLayout();
            this._falgsGroup.SuspendLayout();
            this._avariaGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this._versionGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // _analogGroup
            // 
            this._analogGroup.Controls.Add(this.label4);
            this._analogGroup.Controls.Add(this._uBk2Box);
            this._analogGroup.Controls.Add(this.label3);
            this._analogGroup.Controls.Add(this._uBk1Box);
            this._analogGroup.Controls.Add(this._uSpool);
            this._analogGroup.Controls.Add(this._uSpoolBox);
            this._analogGroup.Controls.Add(this._inILabel);
            this._analogGroup.Controls.Add(this._inI);
            this._analogGroup.Location = new System.Drawing.Point(12, 12);
            this._analogGroup.Name = "_analogGroup";
            this._analogGroup.Size = new System.Drawing.Size(177, 180);
            this._analogGroup.TabIndex = 0;
            this._analogGroup.TabStop = false;
            this._analogGroup.Text = "���������� ��������";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "U �� ��2";
            // 
            // _uBk2Box
            // 
            this._uBk2Box.Enabled = false;
            this._uBk2Box.Location = new System.Drawing.Point(101, 127);
            this._uBk2Box.Name = "_uBk2Box";
            this._uBk2Box.Size = new System.Drawing.Size(66, 20);
            this._uBk2Box.TabIndex = 6;
            this._uBk2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "U �� ��1";
            // 
            // _uBk1Box
            // 
            this._uBk1Box.Enabled = false;
            this._uBk1Box.Location = new System.Drawing.Point(101, 91);
            this._uBk1Box.Name = "_uBk1Box";
            this._uBk1Box.Size = new System.Drawing.Size(66, 20);
            this._uBk1Box.TabIndex = 4;
            this._uBk1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _uSpool
            // 
            this._uSpool.AutoSize = true;
            this._uSpool.Location = new System.Drawing.Point(9, 58);
            this._uSpool.Name = "_uSpool";
            this._uSpool.Size = new System.Drawing.Size(75, 13);
            this._uSpool.TabIndex = 3;
            this._uSpool.Text = "U �� �������";
            // 
            // _uSpoolBox
            // 
            this._uSpoolBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this._uSpoolBox.Enabled = false;
            this._uSpoolBox.Location = new System.Drawing.Point(101, 55);
            this._uSpoolBox.Name = "_uSpoolBox";
            this._uSpoolBox.Size = new System.Drawing.Size(66, 20);
            this._uSpoolBox.TabIndex = 2;
            this._uSpoolBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._uSpoolBox.TextChanged += new System.EventHandler(this._uSpoolBox_TextChanged);
            // 
            // _inILabel
            // 
            this._inILabel.AutoSize = true;
            this._inILabel.Location = new System.Drawing.Point(9, 22);
            this._inILabel.Name = "_inILabel";
            this._inILabel.Size = new System.Drawing.Size(69, 13);
            this._inILabel.TabIndex = 1;
            this._inILabel.Text = "������� ���";
            // 
            // _inI
            // 
            this._inI.Enabled = false;
            this._inI.Location = new System.Drawing.Point(101, 19);
            this._inI.Name = "_inI";
            this._inI.Size = new System.Drawing.Size(66, 20);
            this._inI.TabIndex = 0;
            this._inI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _falgsGroup
            // 
            this._falgsGroup.Controls.Add(this._lblAutomatOff);
            this._falgsGroup.Controls.Add(this._automatLedOff);
            this._falgsGroup.Controls.Add(this.label15);
            this._falgsGroup.Controls.Add(this._kvitBut);
            this._falgsGroup.Controls.Add(this.label11);
            this._falgsGroup.Controls.Add(this._firstStepLed);
            this._falgsGroup.Controls.Add(this._levelActiveLed);
            this._falgsGroup.Controls.Add(this._levelWorkLed);
            this._falgsGroup.Controls.Add(this._errorACP);
            this._falgsGroup.Controls.Add(this.label10);
            this._falgsGroup.Controls.Add(this.label2);
            this._falgsGroup.Controls.Add(this.label9);
            this._falgsGroup.Controls.Add(this.label12);
            this._falgsGroup.Controls.Add(this._firstStepIOLed);
            this._falgsGroup.Controls.Add(this.label1);
            this._falgsGroup.Controls.Add(this._errorCRCProgramLed);
            this._falgsGroup.Controls.Add(this.label13);
            this._falgsGroup.Controls.Add(this._secondStepLed);
            this._falgsGroup.Controls.Add(this.label6);
            this._falgsGroup.Controls.Add(this.label14);
            this._falgsGroup.Controls.Add(this._errorValuesCRC);
            this._falgsGroup.Controls.Add(this._secondIOStepLed);
            this._falgsGroup.Controls.Add(this.label5);
            this._falgsGroup.Controls.Add(this._errorBkFoundedLed);
            this._falgsGroup.Controls.Add(this._errorChainKvitLed);
            this._falgsGroup.Controls.Add(this.label8);
            this._falgsGroup.Controls.Add(this._errorBKKvitLed);
            this._falgsGroup.Controls.Add(this.label7);
            this._falgsGroup.Controls.Add(this._errorChainLed);
            this._falgsGroup.Location = new System.Drawing.Point(195, 12);
            this._falgsGroup.Name = "_falgsGroup";
            this._falgsGroup.Size = new System.Drawing.Size(410, 180);
            this._falgsGroup.TabIndex = 1;
            this._falgsGroup.TabStop = false;
            this._falgsGroup.Text = "���������";
            // 
            // _lblAutomatOff
            // 
            this._lblAutomatOff.AutoSize = true;
            this._lblAutomatOff.Location = new System.Drawing.Point(25, 151);
            this._lblAutomatOff.Name = "_lblAutomatOff";
            this._lblAutomatOff.Size = new System.Drawing.Size(104, 13);
            this._lblAutomatOff.TabIndex = 27;
            this._lblAutomatOff.Text = "������� ��������";
            this._lblAutomatOff.Visible = false;
            // 
            // _automatLedOff
            // 
            this._automatLedOff.Location = new System.Drawing.Point(6, 151);
            this._automatLedOff.Name = "_automatLedOff";
            this._automatLedOff.Size = new System.Drawing.Size(13, 13);
            this._automatLedOff.State = BEMN.Forms.LedState.Off;
            this._automatLedOff.TabIndex = 26;
            this._automatLedOff.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "������������� ���";
            // 
            // _kvitBut
            // 
            this._kvitBut.Location = new System.Drawing.Point(200, 146);
            this._kvitBut.Name = "_kvitBut";
            this._kvitBut.Size = new System.Drawing.Size(184, 23);
            this._kvitBut.TabIndex = 3;
            this._kvitBut.Text = "������������";
            this._kvitBut.UseVisualStyleBackColor = true;
            this._kvitBut.Click += new System.EventHandler(this._kvitBut_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(168, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "������������ CRC ���������";
            // 
            // _firstStepLed
            // 
            this._firstStepLed.Location = new System.Drawing.Point(6, 16);
            this._firstStepLed.Name = "_firstStepLed";
            this._firstStepLed.Size = new System.Drawing.Size(13, 13);
            this._firstStepLed.State = BEMN.Forms.LedState.Off;
            this._firstStepLed.TabIndex = 1;
            // 
            // _levelActiveLed
            // 
            this._levelActiveLed.Location = new System.Drawing.Point(200, 94);
            this._levelActiveLed.Name = "_levelActiveLed";
            this._levelActiveLed.Size = new System.Drawing.Size(13, 13);
            this._levelActiveLed.State = BEMN.Forms.LedState.Off;
            this._levelActiveLed.TabIndex = 8;
            // 
            // _levelWorkLed
            // 
            this._levelWorkLed.Location = new System.Drawing.Point(200, 116);
            this._levelWorkLed.Name = "_levelWorkLed";
            this._levelWorkLed.Size = new System.Drawing.Size(13, 13);
            this._levelWorkLed.State = BEMN.Forms.LedState.Off;
            this._levelWorkLed.TabIndex = 9;
            // 
            // _errorACP
            // 
            this._errorACP.Location = new System.Drawing.Point(6, 132);
            this._errorACP.Name = "_errorACP";
            this._errorACP.Size = new System.Drawing.Size(13, 13);
            this._errorACP.State = BEMN.Forms.LedState.Off;
            this._errorACP.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(216, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "����. �������";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "��������� 1-�� �������";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(217, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "����. ���������";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "������������ CRC �������";
            // 
            // _firstStepIOLed
            // 
            this._firstStepIOLed.Location = new System.Drawing.Point(200, 16);
            this._firstStepIOLed.Name = "_firstStepIOLed";
            this._firstStepIOLed.Size = new System.Drawing.Size(13, 13);
            this._firstStepIOLed.State = BEMN.Forms.LedState.Off;
            this._firstStepIOLed.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "����. �� 1-� �������";
            // 
            // _errorCRCProgramLed
            // 
            this._errorCRCProgramLed.Location = new System.Drawing.Point(6, 113);
            this._errorCRCProgramLed.Name = "_errorCRCProgramLed";
            this._errorCRCProgramLed.Size = new System.Drawing.Size(13, 13);
            this._errorCRCProgramLed.State = BEMN.Forms.LedState.Off;
            this._errorCRCProgramLed.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(215, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(156, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "������. �� �� ������������";
            // 
            // _secondStepLed
            // 
            this._secondStepLed.Location = new System.Drawing.Point(6, 35);
            this._secondStepLed.Name = "_secondStepLed";
            this._secondStepLed.Size = new System.Drawing.Size(13, 13);
            this._secondStepLed.State = BEMN.Forms.LedState.Off;
            this._secondStepLed.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "��������� 2-�� �������";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 72);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "������������� ��";
            // 
            // _errorValuesCRC
            // 
            this._errorValuesCRC.Location = new System.Drawing.Point(6, 94);
            this._errorValuesCRC.Name = "_errorValuesCRC";
            this._errorValuesCRC.Size = new System.Drawing.Size(13, 13);
            this._errorValuesCRC.State = BEMN.Forms.LedState.Off;
            this._errorValuesCRC.TabIndex = 18;
            // 
            // _secondIOStepLed
            // 
            this._secondIOStepLed.Location = new System.Drawing.Point(200, 35);
            this._secondIOStepLed.Name = "_secondIOStepLed";
            this._secondIOStepLed.Size = new System.Drawing.Size(13, 13);
            this._secondIOStepLed.State = BEMN.Forms.LedState.Off;
            this._secondIOStepLed.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(216, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "����. �� 2-� �������";
            // 
            // _errorBkFoundedLed
            // 
            this._errorBkFoundedLed.Location = new System.Drawing.Point(6, 55);
            this._errorBkFoundedLed.Name = "_errorBkFoundedLed";
            this._errorBkFoundedLed.Size = new System.Drawing.Size(13, 13);
            this._errorBkFoundedLed.State = BEMN.Forms.LedState.Off;
            this._errorBkFoundedLed.TabIndex = 10;
            // 
            // _errorChainKvitLed
            // 
            this._errorChainKvitLed.Location = new System.Drawing.Point(200, 74);
            this._errorChainKvitLed.Name = "_errorChainKvitLed";
            this._errorChainKvitLed.Size = new System.Drawing.Size(13, 13);
            this._errorChainKvitLed.State = BEMN.Forms.LedState.Off;
            this._errorChainKvitLed.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "���������� ������. ��";
            // 
            // _errorBKKvitLed
            // 
            this._errorBKKvitLed.Location = new System.Drawing.Point(200, 55);
            this._errorBKKvitLed.Name = "_errorBKKvitLed";
            this._errorBKKvitLed.Size = new System.Drawing.Size(13, 13);
            this._errorBKKvitLed.State = BEMN.Forms.LedState.Off;
            this._errorBKKvitLed.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(215, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "������. �� ��������� �� �� �������.";
            // 
            // _errorChainLed
            // 
            this._errorChainLed.Location = new System.Drawing.Point(6, 74);
            this._errorChainLed.Name = "_errorChainLed";
            this._errorChainLed.Size = new System.Drawing.Size(13, 13);
            this._errorChainLed.State = BEMN.Forms.LedState.Off;
            this._errorChainLed.TabIndex = 16;
            // 
            // _avariaGroup
            // 
            this._avariaGroup.Controls.Add(this._journalGrid);
            this._avariaGroup.Location = new System.Drawing.Point(12, 198);
            this._avariaGroup.Name = "_avariaGroup";
            this._avariaGroup.Size = new System.Drawing.Size(385, 128);
            this._avariaGroup.TabIndex = 2;
            this._avariaGroup.TabStop = false;
            this._avariaGroup.Text = "������ ������";
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.SystemColors.ActiveCaptionText;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Msg,
            this.Parametr});
            this._journalGrid.Location = new System.Drawing.Point(14, 19);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.ReadOnly = true;
            this._journalGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this._journalGrid.Size = new System.Drawing.Size(356, 101);
            this._journalGrid.TabIndex = 0;
            // 
            // Msg
            // 
            this.Msg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Msg.HeaderText = "���������";
            this.Msg.Name = "Msg";
            this.Msg.ReadOnly = true;
            this.Msg.Width = 170;
            // 
            // Parametr
            // 
            this.Parametr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Parametr.HeaderText = "��������";
            this.Parametr.Name = "Parametr";
            this.Parametr.ReadOnly = true;
            this.Parametr.Width = 150;
            // 
            // _versionGroup
            // 
            this._versionGroup.Controls.Add(this._refreshVersionBut);
            this._versionGroup.Controls.Add(this.label17);
            this._versionGroup.Controls.Add(this.label16);
            this._versionGroup.Controls.Add(this._versionBox);
            this._versionGroup.Controls.Add(this._serialBox);
            this._versionGroup.Location = new System.Drawing.Point(405, 198);
            this._versionGroup.Name = "_versionGroup";
            this._versionGroup.Size = new System.Drawing.Size(200, 128);
            this._versionGroup.TabIndex = 4;
            this._versionGroup.TabStop = false;
            this._versionGroup.Text = "������ ����������";
            // 
            // _refreshVersionBut
            // 
            this._refreshVersionBut.Location = new System.Drawing.Point(115, 97);
            this._refreshVersionBut.Name = "_refreshVersionBut";
            this._refreshVersionBut.Size = new System.Drawing.Size(75, 23);
            this._refreshVersionBut.TabIndex = 28;
            this._refreshVersionBut.Text = "��������";
            this._refreshVersionBut.UseVisualStyleBackColor = true;
            this._refreshVersionBut.Click += new System.EventHandler(this._refreshVersionBut_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "������ ��������";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "�������� �����";
            // 
            // _versionBox
            // 
            this._versionBox.Enabled = false;
            this._versionBox.Location = new System.Drawing.Point(115, 59);
            this._versionBox.Name = "_versionBox";
            this._versionBox.Size = new System.Drawing.Size(79, 20);
            this._versionBox.TabIndex = 1;
            // 
            // _serialBox
            // 
            this._serialBox.Enabled = false;
            this._serialBox.Location = new System.Drawing.Point(115, 19);
            this._serialBox.Name = "_serialBox";
            this._serialBox.Size = new System.Drawing.Size(79, 20);
            this._serialBox.TabIndex = 0;
            // 
            // MeasuringForm
            // 
            this.ClientSize = new System.Drawing.Size(617, 337);
            this.Controls.Add(this._versionGroup);
            this.Controls.Add(this._avariaGroup);
            this.Controls.Add(this._falgsGroup);
            this.Controls.Add(this._analogGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MeasuringForm";
            this.Text = "��������� ����������";
            this.Activated += new System.EventHandler(this.form_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._analogGroup.ResumeLayout(false);
            this._analogGroup.PerformLayout();
            this._falgsGroup.ResumeLayout(false);
            this._falgsGroup.PerformLayout();
            this._avariaGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this._versionGroup.ResumeLayout(false);
            this._versionGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.DataGridViewTextBoxColumn Parametr;

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            try
            {
                //_device.RemoveAllQuerys();
                //_device._singnalsFail -= new Handler(_device__singnalsFail);
                //_device._signalsOk -= new Handler(_device__signalsOk);
                //_device._journalOk -= new Handler(_device__journalOk);
                //_device._versionOk -= new Handler(_device__versionOk);
                _device.MB.GetQuery("nominal" + _device.DeviceNumber).raiseSpan = new TimeSpan(_device.MB.Port.WaitByte * 10000 * 10);
                _device.MB.GetQuery("journal" + _device.DeviceNumber).raiseSpan = new TimeSpan(_device.MB.Port.WaitByte * 10000 * 10);
                _device.MB.GetQuery("signals" + _device.DeviceNumber).raiseSpan = new TimeSpan(_device.MB.Port.WaitByte * 10000 * 10);

            }
            catch (NullReferenceException)
            {

            }
           
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _device.RemoveAllQuerys();
            _device._singnalsFail -= new Handler(_device__singnalsFail);
            _device._signalsOk -= new Handler(_device__signalsOk);
            _device._journalOk -= new Handler(_device__journalOk);
            _device._versionOk -= new Handler(_device__versionOk);
            
        }

        private void _kvitBut_Click(object sender, EventArgs e)
        {
            _device.Kvit();
        }

        private void _refreshVersionBut_Click(object sender, EventArgs e)
        {
            _device.LoadVersion(_device);
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            //_device.Port = _device.PortNum;
        }

        private void _uSpoolBox_TextChanged(object sender, EventArgs e)
        {

        }

   }
}

namespace BEMN.MR100
{
    public class Measuring
    {


        public static double MeasuringConverter(double value, double nominal)
        {
            return (double)((value * nominal) / 0x7FFF);
        }

        public static double TokAdductorDirect(ushort value, double nominal, int koef)
        {
            double tok = 0.0;
            tok = value / 327.67 * nominal * koef;
            //tok = tok / 327.7;
            tok = System.Math.Floor(tok + 0.5);
            tok /= 100;
            return tok;
        }

        public static ushort TokAdductorInvert(double tok, double Nominal, int koef)
        {
            return (ushort)((tok * 32767) / Nominal / koef);
        }

        public static float NominalConverterDirect(byte[] nominal)
        {
            float ret = 0;
            if (4 != nominal.Length)
            {
                throw new ApplicationException("������� ������ ���� � 4-� ������");
            }

            ret = BitConverter.ToSingle(nominal, 0);

            return ret;
        }

        public static byte[] NominalConverterInvert(float nominal)
        {
            return BitConverter.GetBytes(nominal);
        }
    } 
        public class MR100Class : Device, IDeviceView, IDeviceLog, INodeSerializable, ISlave
        {
            #region reconnect
            private bool _disconnected = true;
            private byte _port = 0;

            public byte Port
            {
                get { return _port; }
                set { _port = value; }
            }
            #endregion

            #region Version
            public string MR100Version { get; set; }
            #endregion

            #region Test
            /* [TestFixture]
            public class MR100Test
            {
                [Test]
                public void PropTest()
                {
                    MR100Class mr100 = new MR100Class();
                    mr100.Nominal  = 2;
                    Assert.AreEqual(mr100.Nominal, 2);
                    mr100.TokEndurance1 = 22.2;
                    Assert.AreEqual(mr100.TokEndurance1, 22.2);
                    mr100.TokEndurance2 = 22.4;
                    Assert.AreEqual(mr100.TokEndurance2, 22.4);
                }

                [Test]
                public void SerializeCalibrateTest()
                {
                    MR100Class device = new MR100Class();
                    for (int i = 0; i < device.BKoeff.Size; i++)
                    {
                        device.BKoeff[i] = 1983;
                        device.AKoeff[i] = 65535;
                        device.PaKoeff[i] = 1;
                    }
                    device.SerializeCalibrate("test.calibrate");
                    for (int i = 0; i < device.BKoeff.Size; i++)
                    {
                        device.BKoeff[i] = 0;
                        device.AKoeff[i] = 0;
                        device.PaKoeff[i] = 0;
                    }
                    device.DeserializeCalibrate("test.calibrate");
                    for (int i = 0; i < device.BKoeff.Size; i++)
                    {
                        Assert.AreEqual(device.BKoeff[i], 1983);
                        Assert.AreEqual(device.AKoeff[i], 65535);
                        Assert.AreEqual(device.PaKoeff[i], 1);
                    }

                }


                [Test]
                public void SerializeAllMemoryTest()
                {
                    MR100Class device = new MR100Class();
                    for (int i = 0; i < device._all_memory.Value.Length; i++)
                    {
                        device._all_memory.Value[i] = 1983;
                    }
                    device.SerializeAllMemory("test.memory");

                    for (int i = 0; i < device._all_memory.Value.Length; i++)
                    {
                        device._all_memory.Value[i] = 0;
                    }

                    device.DeserializeAllMemory("test.memory");

                    for (int i = 0; i < device._all_memory.Value.Length; i++)
                    {
                        Assert.AreEqual(device._all_memory.Value[i], 1983);
                    }

                }

                [Test]
                public void SerializeXmlTest()
                {
                    SerialServer.CServerClass srv = new SerialServer.CServerClass();
                    MR100Class device1 = new MR100Class(new Modbus(srv.GetPort(1)));
                    device1.MB.Cascade = 11;
                    device1.DeviceNumber = 2;
                    device1.TokEndurance1 = 22.2;
                    XmlDocument doc = new XmlDocument();
                    doc.AppendChild(device1.ToXml(doc));
                    doc.Save("mr100.xml");

                    MR100Class device2 = new MR100Class();
                    device2.FromXml(doc.DocumentElement);
                    Assert.AreEqual(device1.DeviceNumber, device2.DeviceNumber);
                    Assert.AreEqual(device1.MB.Cascade, device2.MB.Cascade);
                    Assert.AreEqual(device1.MB.Port.PortNum, device2.MB.Port.PortNum);
                    Assert.AreEqual(device1.TokEndurance1, device2.TokEndurance1);

                }

                [Test]
                public void SerializeConstraint()
                {
                    MR100Class device1 = new MR100Class();
                    device1.Nominal = 2.2;
                    device1.TokEndurance1 = 22.2;
                    device1.SerializeConstraint("mr100.xml");
                    MR100Class device2 = new MR100Class();
                    device2.DeserializeConstraint("mr100.xml");
                    Assert.AreEqual(device1.Nominal, device2.Nominal);
                    Assert.AreEqual(device1.TokEndurance1, device2.TokEndurance1);
                }

            }*/
            #endregion
            
            #region Class Journal
            public struct Message
            {
                public string dscr;
                public string prm;

            };

            public class Journal
            {
                private ushort[] _value;
                private ArrayList _messages = new ArrayList();

                public int Count
                {
                    get
                    {
                        return _messages.Count;
                    }
                }

                public Message this[int i]
                {
                    get
                    {
                        return (Message)(_messages[i]);
                    }

                }

                public void Init(ushort[] value, double nominal)
                {
                    _value = value;
                    _messages.Clear();
                    for (int i = 0; i < value.Length - 1; i += 2)
                    {
                        _messages.Add(CreateMessage(nominal, i));

                    }
                }

                private Message CreateMessage(double nominal, int i)
                {
                    Message msg = new Message();

                    switch (_value[i])
                    {
                        case 1:
                            msg.dscr = "��������� 1-�� �������";
                            msg.prm = String.Format("{0:F2} �", Measuring.MeasuringConverter(_value[i + 1], nominal * 10));
                            break;
                        case 2:
                            msg.dscr = "��������� 2-�� �������";
                            msg.prm = String.Format("{0:F2} �", Measuring.MeasuringConverter(_value[i + 1], nominal * 10));
                            break;
                        case 3:
                            msg.dscr = "�������� ����";
                            switch (_value[i + 1])
                            {
                                case 1:
                                    msg.prm = "���� �� ������. �����. ����� ����������";
                                    break;
                                case 2:
                                    msg.prm = "���� �� ������������� ��������� ���� ���������";
                                    break;
                                case 3:
                                    msg.prm = "���� �� ������ ����������";
                                    break;
                                case 4:
                                    msg.prm = "���� �� ������. �����. ����� ����������";
                                    break;
                                default:
                                    msg.prm = "������������ ��������";
                                    break;
                            }

                            break;
                        case 4:
                            msg.dscr = "��������� ������������";
                            msg.prm = "";
                            break;

                        default:
                            msg.dscr = "������������ ��������";
                            msg.prm = "������������ ��������";
                            break;
                    }
                    return msg;
                }



                public Journal()
                { }



            }
            #endregion

            #region Funcs Serialize
            public void SerializeConstraint(string fname)
            {
                if (null == fname || 0 == fname.Length)
                {
                    throw new NullReferenceException();
                }

                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR100"));
                XmlElement constraintEl = doc.CreateElement("Constraints");

                XmlElement baude = doc.CreateElement("BaudeRate");
                baude.InnerText = BaudeRate.ToString();

                XmlElement offImpulse = doc.CreateElement("OffImpulse");
                offImpulse.InnerText = OffImpulse.ToString();
                XmlElement rascepitelTime = doc.CreateElement("RascepitelTime");
                rascepitelTime.InnerText = RascepitelTime.ToString();
                XmlElement timeEndurance1 = doc.CreateElement("TimeEndurance1");
                timeEndurance1.InnerText = TimeEndurance1.ToString();
                XmlElement timeEndurance2 = doc.CreateElement("TimeEndurance2");
                timeEndurance2.InnerText = TimeEndurance2.ToString();
                XmlElement tokEndurance1 = doc.CreateElement("TokEndurance1");
                tokEndurance1.InnerText = TokEndurance1.ToString();
                XmlElement tokEndurance2 = doc.CreateElement("TokEndurance2");
                tokEndurance2.InnerText = TokEndurance2.ToString();
                XmlElement UROV_time = doc.CreateElement("UROV_Time");
                UROV_time.InnerText = UROV_Time.ToString();
                XmlElement releMode = doc.CreateElement("ReleMode");
                releMode.InnerText = ReleMode.ToString();
                XmlElement defenseMode = doc.CreateElement("DefenseMode");
                defenseMode.InnerText = Common.BitsToString(DefenseMode);
                XmlElement nominal = doc.CreateElement("Nominal");
                nominal.InnerText = Nominal.ToString();
                XmlElement address = doc.CreateElement("Address");
                address.InnerText = DeviceAddress.ToString();


                constraintEl.AppendChild(baude);
                constraintEl.AppendChild(releMode);
                constraintEl.AppendChild(offImpulse);
                constraintEl.AppendChild(rascepitelTime);
                constraintEl.AppendChild(timeEndurance1);
                constraintEl.AppendChild(timeEndurance2);
                constraintEl.AppendChild(tokEndurance1);
                constraintEl.AppendChild(tokEndurance2);
                constraintEl.AppendChild(UROV_time);
                constraintEl.AppendChild(defenseMode);
                constraintEl.AppendChild(nominal);
                constraintEl.AppendChild(address);

                doc.DocumentElement.AppendChild(constraintEl);

                doc.Save(fname);

            }

            public void SerializeCalibrate(string fname)
            {
                if (null == fname || 0 == fname.Length)
                {
                    throw new NullReferenceException();
                }

                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR100"));
                XmlElement calibrateEl = doc.CreateElement("Calibrate");

                for (int i = 0; i < BKoeff.Size; i++)
                {
                    XmlElement channel = doc.CreateElement("Channel" + i.ToString());

                    XmlElement bEl = doc.CreateElement("BKoeff");
                    bEl.InnerText = BKoeff[i].ToString();
                    XmlElement aEl = doc.CreateElement("AKoeff");
                    aEl.InnerText = AKoeff[i].ToString();
                    XmlElement paEl = doc.CreateElement("PaKoeff");
                    paEl.InnerText = PaKoeff[i].ToString();

                    channel.AppendChild(bEl);
                    channel.AppendChild(aEl);
                    channel.AppendChild(paEl);

                    calibrateEl.AppendChild(channel);
                }



                doc.DocumentElement.AppendChild(calibrateEl);
                doc.Save(fname);

            }

            public void SerializeAllMemory(string fname)
            {
                if (null == fname || 0 == fname.Length)
                {
                    throw new NullReferenceException();
                }

                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR100"));
                XmlElement allEl = doc.CreateElement("AllMemory");
                allEl.InnerText = Convert.ToBase64String(Common.TOBYTES(_all_memory.Value, true));
                doc.DocumentElement.AppendChild(allEl);
                doc.Save(fname);

            }

            public void DeserializeAllMemory(string fname)
            {
                if (null == fname || 0 == fname.Length)
                {
                    throw new NullReferenceException();
                }

                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(fname);
                }
                catch (FileNotFoundException e)
                {
                    throw e;
                }

                _all_memory.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode("/MR100/AllMemory").InnerText), true);
            }

            public void DeserializeCalibrate(string fname)
            {
                if (null == fname || 0 == fname.Length)
                {
                    throw new NullReferenceException();
                }

                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(fname);
                }
                catch (FileNotFoundException e)
                {
                    throw e;
                }

                try
                {
                    for (int i = 0; i < BKoeff.Size; i++)
                    {
                        BKoeff[i] = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Calibrate/Channel" + i + "/BKoeff").InnerText);
                        AKoeff[i] = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Calibrate/Channel" + i + "/AKoeff").InnerText);
                        PaKoeff[i] = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Calibrate/Channel" + i + "/PaKoeff").InnerText);
                    }

                }
                catch (NullReferenceException e)
                {
                    throw e;
                }

            }

            public void DeserializeConstraint(string fname)
            {
                if (null == fname || 0 == fname.Length)
                {
                    throw new NullReferenceException();
                }

                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(fname);
                }
                catch (FileNotFoundException e)
                {

                    throw e;
                }

                try
                {
                    Nominal = Convert.ToDouble(doc.SelectSingleNode("/MR100/Constraints/Nominal").InnerText);
                    BaudeRate = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/BaudeRate").InnerText);
                    OffImpulse = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/OffImpulse").InnerText);
                    RascepitelTime = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/RascepitelTime").InnerText);
                    TimeEndurance1 = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/TimeEndurance1").InnerText);
                    TimeEndurance2 = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/TimeEndurance2").InnerText);
                    TokEndurance1 = Convert.ToDouble(doc.SelectSingleNode("/MR100/Constraints/TokEndurance1").InnerText);
                    TokEndurance2 = Convert.ToDouble(doc.SelectSingleNode("/MR100/Constraints/TokEndurance2").InnerText);
                    UROV_Time = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/UROV_Time").InnerText);
                    ReleMode = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/ReleMode").InnerText);
                    DeviceAddress = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/Address").InnerText);

                }
                catch (NullReferenceException e)
                {
                    throw e;
                }

                try
                {
                    DefenseMode = Common.StringToBits(doc.SelectSingleNode("/MR100/Constraints/DefenseMode").InnerText);
                }
                catch (ApplicationException e)
                {

                    throw e;
                }

            }
            #endregion

            #region IDeviceView members
            [Browsable(false)]
            public Type ClassType
            {
                get
                {
                    return this.GetType();
                }
            }

            [Browsable(false)]
            public bool Deletable
            {
                get
                {
                    return true;
                }
            }

            [Browsable(false)]
            public bool ForceShow
            {
                get
                {
                    return false;
                }

            }

            [Browsable(false)]
            public Image NodeImage
            {
                get
                {
                    return AssemblyResources.Resources.mr100.ToBitmap();

                }
            }

            [DescriptionAttribute("�������� ����������"), System.ComponentModel.CategoryAttribute("�����")]
            [DisplayName("��������")]
            public string NodeName
            {
                get
                {
                    return "��100";
                }
            }

            [Browsable(false)]
            public INodeView[] ChildNodes
            {
                get
                {
                    return null;
                }
            }

            #endregion

            #region Construct
            public MR100Class()
            {
                Init();
            }

            public MR100Class(Modbus mb)
                //: base(mb)
            {
                Init();
                MB = mb;
                Port = PortNum;
            }

            [XmlIgnore]
            [TypeConverter(typeof(RussianExpandableObjectConverter))]
            public override Modbus MB
            {
                get
                {
                    return mb;
                }
                set
                {
                    mb = value;
                    if (null != mb)
                    {
                        mb.CompleteExchange+=new CompleteExchangeHandler(mb_CompleteExchange);
                        //MB.CompleteExchange += new CompleteExchangeHandler(MB_CompleteExchange);

                    }
                }
            }

            //void MB_CompleteExchange(object sender, Query query)
            //{
            //    OnKoeffQuery(query, AKoeff, "AKoeff", false);
            //    OnKoeffQuery(query, BKoeff, "BKoeff", false);
            //    OnKoeffQuery(query, PaKoeff, "PaKoeff", true);

            //    if (query.name == "save4PaKoeff" + DeviceNumber)
            //    {
            //        Raise(query, CalibrateSaveOk, CalibrateSaveFail);

            //    }

            //    if (query.name == "constraint2" + DeviceNumber)
            //    {
            //        _constraint2.Value = Common.TOWORDS(query.readBuffer, true);
            //    }

            //    if (query.name == "all_memory" + DeviceNumber)
            //    {
            //        Raise(query, _allOk, _allFail, ref _all_memory);
            //    }

            //    if (query.name == "all_memorySave" + DeviceNumber)
            //    {
            //        Raise(query, _allSaveOk, _allSaveFail);
            //    }

            //    if (query.name == "saveConstraint1" + DeviceNumber ||
            //        query.name == "saveConstraint2" + DeviceNumber ||
            //        query.name == "saveNominal" + DeviceNumber)
            //    {
            //        if (query.fail != 0 && query.name == "saveConstraint1" + DeviceNumber)
            //        {
            //            if (null != _constraintSaveFail)
            //            {
            //                _constraintSaveFail(this);
            //            }
            //        }
            //        else
            //        {
            //            if (query.name == "saveNominal" + DeviceNumber)
            //            {
            //                if (null != _constraintSaveOk)
            //                {
            //                    _constraintSaveOk(this);
            //                }

            //            }
            //        }

            //    }

            //    if (query.name == "nominal" + DeviceNumber)
            //    {
            //        Raise(query, _nominalOk, _nomainalFail, ref _nominal);

            //    }


            //    if (query.name == "journal" + DeviceNumber)
            //    {

            //        Raise(query, _journalOk, _journalFail, ref _journal);
            //        _journalRecord.Init(_journal.Value, Nominal);

            //    }

            //    if (query.name == "signals" + DeviceNumber)
            //    {
            //        Raise(query, _signalsOk, _singnalsFail, ref _signals);
            //        _flags = new BitArray(new byte[] { Common.LOBYTE(_signals.Value[0x10]), Common.HIBYTE(_signals.Value[0x10]) });
            //    }


            //    if (query.name == "signalsWord" + DeviceNumber)
            //    {
            //        Raise(query, SignalsWordLoadOk, SignalsWordLoadFail, ref _signals);
            //        _flags = new BitArray(new byte[] { Common.LOBYTE(_signals.Value[0x10]), Common.HIBYTE(_signals.Value[0x10]) });
            //    }
            //    base.mb_CompleteExchange(sender, query);
            //}

            private new void mb_CompleteExchange(object sender, Query query)
            {
                if (query.fail == 1) 
                {
                    LoadVersion(this);
                }
                try
                {
                    if (_disconnected)
                    {
                        PortNum = Port;
                        
                        //LoadVersion();
                    }
                }
                catch (Exception r)
                {
                    //_device.LoadVersion();
                }

                OnKoeffQuery(query, AKoeff, "AKoeff", false);
                OnKoeffQuery(query, BKoeff, "BKoeff", false);
                OnKoeffQuery(query, PaKoeff, "PaKoeff", true);

                if (query.name == "save4PaKoeff" + DeviceNumber)
                {
                    Raise(query, CalibrateSaveOk, CalibrateSaveFail);

                }

                if (query.name == "constraint2" + DeviceNumber)
                {
                    _constraint2.Value = Common.TOWORDS(query.readBuffer, true);
                }

                if (query.name == "all_memory" + DeviceNumber)
                {
                    Raise(query, _allOk, _allFail, ref _all_memory);
                }

                if (query.name == "all_memorySave" + DeviceNumber)
                {
                    Raise(query, _allSaveOk, _allSaveFail);
                }

                if (query.name == "saveConstraint1" + DeviceNumber ||
                    query.name == "saveConstraint2" + DeviceNumber ||
                    query.name == "saveNominal" + DeviceNumber)
                {
                    if (query.fail != 0 && query.name == "saveConstraint1" + DeviceNumber)
                    {
                        if (null != _constraintSaveFail)
                        {
                            _constraintSaveFail(this);
                        }
                    }
                    else
                    {
                        if (query.name == "saveNominal" + DeviceNumber)
                        {
                            if (null != _constraintSaveOk)
                            {
                                _constraintSaveOk(this);
                            }

                        }
                    }

                }

                if (query.name == "constraint1" + DeviceNumber)
                {
                    //if (_constraint1.Loaded)
                    //{
                    //    if (_loadProperties && null != PropertiesLoadOk)
                    //    {
                    //        _loadProperties = false;
                    //        PropertiesLoadOk(this);
                    //    }
                    //}
                    //else
                    //{
                    //    if (_loadProperties && null != PropertiesLoadFail)
                    //    {
                    //        _loadProperties = false;
                    //        PropertiesLoadFail(this);
                    //    }
                    //}

                    if (0 != query.fail)
                    {
                        if (null != _constraint1Fail)
                        {
                            _constraint1Fail(sender);
                        }
                    }
                    else
                    {
                        //if (_nominal.Loaded)
                        //{
                            Raise(query, _constraint1Ok, _constraint1Fail, ref _constraint1);
                        //}
                        //else
                        //{
                        //    LoadSlot(DeviceNumber, _nominal, "nominal" + DeviceNumber);
                        //    LoadSlot(DeviceNumber, _constraint1, "constraint1" + DeviceNumber);
                        //}
                    }

                }


                if (query.name == "nominal" + DeviceNumber)
                {
                    Raise(query, _nominalOk, _nomainalFail, ref _nominal);

                }


                if (query.name == "journal" + DeviceNumber)
                {

                    Raise(query, _journalOk, _journalFail, ref _journal);
                    _journalRecord.Init(_journal.Value, Nominal);

                }

                if (query.name == "signals" + DeviceNumber)
                {
                    Raise(query, _signalsOk, _singnalsFail, ref _signals);
                    _flags = new BitArray(new byte[] { Common.LOBYTE(_signals.Value[0x10]), Common.HIBYTE(_signals.Value[0x10]) });
                }


                if (query.name == "signalsWord" + DeviceNumber)
                {
                    Raise(query, SignalsWordLoadOk, SignalsWordLoadFail, ref _signals);
                    _flags = new BitArray(new byte[] { Common.LOBYTE(_signals.Value[0x10]), Common.HIBYTE(_signals.Value[0x10]) });
                }
                base.mb_CompleteExchange(sender, query);
            }

            private void Init()
            {
                this._versionOk += new Handler(MR100Class__versionOk);
                this._versionFail += new Handler(MR100Class__versionFail);
                HaveVersion = true;
                BKoeff = new SlotArray(_BKoeff);
                AKoeff = new SlotArray(_AKoeff);
                PaKoeff = new SlotArray(_PaKoeff);

                _logHash = CreateLogHash();
                this.DeviceNumberChanged += new DeviceNumberChangedHandler(MR100DeviceNumberChanged);
                
                for (int i = 0; i < 5; i++)
                {
                    _coeffMin[i] = new ushort[3];
                    _coeffMax[i] = new ushort[3];
                    for (int j = 0; j < 3; j++)
                    {
                        _coeffMax[i][j] = 65535;
                    }
                }
                
            }

            void MR100Class__versionFail(object sender)
            {
                _disconnected = true;
            }

            void MR100Class__versionOk(object sender)
            {
                //this.MR100Version = this.DeviceVersion;
                _disconnected = false;
            }
            #endregion

            private void MR100DeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
            {
                if (oldNumber != newNumber)
                {
                    _logHash = CreateLogHash();
                }
            }

            private BitArray _flags;

            private Journal _journalRecord = new Journal();

            private int _koef;

            #region Prop Device
            [Browsable(false)]
            public ushort[][] CoeffMax
            {
                get
                {
                    return _coeffMax;
                }
                set
                {
                    _coeffMax = value;
                }
            }

            [Browsable(false)]
            public ushort[][] CoeffMin
            {
                get
                {
                    return _coeffMin;
                }
                set
                {
                    _coeffMin = value;
                }
            }

            //public new Modbus MB
            //{
            //    get
            //    {
            //        return mb;
            //    }
            //    set
            //    {
            //        mb = value;
            //        if (null != mb)
            //        {
            //            mb.CompleteExchange += new CompleteExchangeHandler(mb_CompleteExchange);
            //        }
            //    }
            //}

            #endregion

            #region Events

            public event Handler _allSaveOk;
            public event Handler _allSaveFail;

            public event Handler _allOk;
            public event Handler _allFail;

            public event Handler _loadCalibrateOk;
            public event Handler _loadCalibrateFail;

            public event Handler _nominalOk;
            public event Handler _nomainalFail;

            public event Handler _constraint1Ok;
            public event Handler _constraint1Fail;

            public event Handler _constraintSaveFail;
            public event Handler _constraintSaveOk;

            public event Handler _journalOk;
            public event Handler _journalFail;

            public event Handler _signalsOk;
            public event Handler _singnalsFail;


            public event Handler SignalsWordLoadOk;
            public event Handler SignalsWordLoadFail;

            public event Handler CalibrateSaveOk;
            public event Handler CalibrateSaveFail;



            #endregion

            #region Slots

            private slot _signals = new slot(0, 0x12);
            private slot _journal = new slot(0x140, 0x148);
            private slot _nominal = new slot(0x124, 0x126);
            public slot _constraint1 = new slot(0x100, 0x110);
            private slot _constraint2 = new slot(0x130, 0x132);
            private slot _all_memory = new slot(0x100, 0x150);


            private slot[] _BKoeff = new slot[] { new slot(0x110,0x111), 
                                               new slot(0x113,0x114),
                                               new slot(0x116,0x117),
                                               new slot(0x119,0x11A),
                                               new slot(0x11C,0x11D)};

            private slot[] _AKoeff = new slot[] { new slot(0x111,0x112), 
                                               new slot(0x114,0x115),
                                               new slot(0x117,0x118),
                                               new slot(0x11A,0x11B),
                                               new slot(0x11D,0x11E)};


            private slot[] _PaKoeff = new slot[] { new slot(0x112,0x113), 
                                                new slot(0x115,0x116),
                                                new slot(0x118,0x119),
                                                new slot(0x11B,0x11C),
                                                new slot(0x11E,0x11F)};

            public bool Calibrate = false;

            #endregion

            #region Properties Slot





            public SlotArray BKoeff;
            public SlotArray AKoeff;
            public SlotArray PaKoeff;




            public void LoadCalibrate()
            {
                for (int i = 0; i < BKoeff.Size; i++)
                {
                    LoadSlot(DeviceNumber, BKoeff.GetSlot(i), "load" + i + "BKoeff" + DeviceNumber, this);
                    LoadSlot(DeviceNumber, AKoeff.GetSlot(i), "load" + i + "AKoeff" + DeviceNumber, this);
                    LoadSlot(DeviceNumber, PaKoeff.GetSlot(i), "load" + i + "PaKoeff" + DeviceNumber, this);
                }
            }

            public void SaveAKoeff(int channel)
            {
                SaveSlot(DeviceNumber, AKoeff.GetSlot(channel), "saveAKoeff" + DeviceNumber, this);
            }

            public void SaveBKoeff(int channel)
            {
                SaveSlot(DeviceNumber, BKoeff.GetSlot(channel), "saveBKoeff" + DeviceNumber, this);
            }

            public void SavePaKoeff(int channel)
            {
                SaveSlot(DeviceNumber, PaKoeff.GetSlot(channel), "savePaKoeff" + DeviceNumber, this);
            }

            public void SaveCalibrate()
            {
                for (int i = 0; i < BKoeff.Size; i++)
                {
                    SaveSlot(DeviceNumber, BKoeff.GetSlot(i), "save" + i + "BKoeff" + DeviceNumber, this);
                    SaveSlot(DeviceNumber, AKoeff.GetSlot(i), "save" + i + "AKoeff" + DeviceNumber, this);
                    SaveSlot(DeviceNumber, PaKoeff.GetSlot(i), "save" + i + "PaKoeff" + DeviceNumber, this);
                }
            }

            public bool IsCoeffLimitsValid()
            {
                bool ret = true;
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (CoeffMax[i][j] < CoeffMin[i][j])
                        {
                            ret = false;
                        }
                    }
                }
                return ret;
            }

            public bool IsCoeffInLimits(SlotArray coeff, int coeffInd, int channel)
            {
                return coeff[channel] >= CoeffMin[channel][coeffInd] && coeff[channel] <= CoeffMax[channel][coeffInd];
            }

            public ushort GetWordSignal(int channel)
            {
                if (channel < 0 || channel > 4)
                {
                    throw new ApplicationException("����� ���������� " + channel + "�� ����������");
                }

                return _signals.Value[channel];

            }

            private ushort[][] _coeffMin = new ushort[5][];
            private ushort[][] _coeffMax = new ushort[5][];

            [Browsable(false)]
            public BitArray Flags
            {
                get
                {
                    return _flags;
                }
            }

            [Browsable(false)]
            public double USpool
            {
                get
                {
                    return Measuring.MeasuringConverter(_signals.Value[1], 250);
                }

            }

            [Browsable(false)]
            public double U_BK1
            {
                get
                {
                    return Measuring.MeasuringConverter(_signals.Value[2], 250);
                }

            }

            [Browsable(false)]
            public double U_BK2
            {
                get
                {
                    return Measuring.MeasuringConverter(_signals.Value[4], 250);
                }

            }

            [Browsable(false)]
            public double I_In
            {
                get
                {
                    return Measuring.MeasuringConverter(_signals.Value[0], Nominal * 10);
                }

            }

            [Browsable(false)]
            public bool IsNominalLoaded
            {
                get
                {
                    return _nominal.Loaded;
                }
            }

            [DescriptionAttribute("�������� ���� �����"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("�������")]
            public double Nominal
            {
                get
                {
                    double ret = 0.0;

                    ret = Measuring.NominalConverterDirect(Common.TOBYTES(_nominal.Value, true)) / 10;

                    return ret;
                }
                set
                {
                    byte[] buffer = Measuring.NominalConverterInvert((float)(value * 10));
                    _nominal.Value = Common.TOWORDS(buffer, true);

                }

            }

            public int Koef
            {
                get { return _koef; }
                set { _koef = value; }
            }
        

            [Browsable(false)]
            public Journal AlarmJournal
            {
                get
                {
                    return _journalRecord;
                }
            }

            [DescriptionAttribute("����� �������� ������������ 1-� �������(��)"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("����� 1-� �������")]
            public ushort TimeEndurance1
            {
                get
                {
                    return (ushort)(_constraint1.Value[0] * 10);
                }
                set
                {
                    _constraint1.Value[0] = (ushort)(value / 10);
                }
            }

            [DescriptionAttribute("��� ������������ 1-� �������"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("��� 1-� �������")]
            public double TokEndurance1
            {
                get
                {
                    return Measuring.TokAdductorDirect(_constraint1.Value[1], Nominal, Koef);
                }
                set
                {
                    _constraint1.Value[1] = Measuring.TokAdductorInvert(value, Nominal, Koef);
                }
            }

            [DescriptionAttribute("����� �������� ������������ 2-� �������(��)"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("����� 2-� �������")]
            public ushort TimeEndurance2
            {
                get
                {
                    return (ushort)(_constraint1.Value[2]);
                }
                set
                {
                    _constraint1.Value[2] = (ushort)(value);
                }
            }

            [DescriptionAttribute("��� ������������ 2-� �������(��)"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("��� 2-� �������")]
            public double TokEndurance2
            {
                get
                {
                    return Measuring.TokAdductorDirect(_constraint1.Value[3], Nominal, Koef);
                }
                set
                {
                    _constraint1.Value[3] = Measuring.TokAdductorInvert(value, Nominal,Koef);
                }
            }

            [DescriptionAttribute("����� �������� ����(��)"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("����")]
            public ushort UROV_Time
            {
                get
                {
                    return (ushort)(_constraint1.Value[4] * 10);
                }
                set
                {
                    _constraint1.Value[4] = (ushort)(value / 10);
                }
            }

            [DescriptionAttribute("������������ ������� ���������� (10 * ��)"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("������� ����������")]
            public ushort OffImpulse
            {
                get
                {
                    return (ushort)(_constraint1.Value[5] * 10);
                }
                set
                {
                    _constraint1.Value[5] = (ushort)(value / 10);
                }
            }

            [DescriptionAttribute("����� ������������ ����������� (��)"), System.ComponentModel.CategoryAttribute("�������")]
            [DisplayName("����� �����������")]
            public ushort RascepitelTime
            {
                get
                {
                    return (ushort)(_constraint1.Value[6] * 10);
                }
                set
                {
                    _constraint1.Value[6] = (ushort)(value / 10);
                }
            }

            [Browsable(false)]
            public ushort ReleMode
            {
                get
                {
                    return _constraint1.Value[7];
                }
                set
                {
                    _constraint1.Value[7] = value;
                }
            }

            [Browsable(false)]
            public BitArray DefenseMode
            {
                get
                {
                    return new BitArray(new byte[] { Common.LOBYTE(_constraint1.Value[8]), Common.HIBYTE(_constraint1.Value[8]) });
                }
                set
                {
                    ushort bt = 0;
                    for (int i = 0; i < value.Count; i++)
                    {
                        if (value[i])
                        {
                            bt += (ushort)(Math.Pow(2, i));
                        }
                    }

                    _constraint1.Value[8] = bt;
                }

            }

            [DescriptionAttribute("�������� ������� � �����"), System.ComponentModel.CategoryAttribute("������������ RS-485")]
            [DisplayName("��������")]
            public ushort BaudeRate
            {
                get
                {
                    return (ushort)(_constraint2.Value[0x0]);
                }
                set
                {
                    _constraint2.Value[0x0] = (ushort)(value);
                }
            }

            [DescriptionAttribute("����� ����������"), System.ComponentModel.CategoryAttribute("������������ RS-485")]
            [DisplayName("�����")]
            public ushort DeviceAddress
            {
                get
                {
                    return _constraint2.Value[0x1];
                }
                set
                {
                    _constraint2.Value[0x1] = value;
                }
            }


            #endregion

            #region Load Methods

            public void RemoveAllQuerys()
            {
                MB.RemoveQuery("signals" + DeviceNumber);
                MB.RemoveQuery("journal" + DeviceNumber);
                MB.RemoveQuery("nominal" + DeviceNumber);
            }

            public void MakeConstraintsNull()
            {
                for (int i = 0; i < 0x08; i++)
                {
                    _constraint1.Value[i] = 0;
                }
            }

            public void LoadAllCycle()
            {
                LoadSlotCycle(DeviceNumber, _nominal, "nominal" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
                LoadSlotCycle(DeviceNumber, _signals, "signals" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
                LoadSlotCycle(DeviceNumber, _journal, "journal" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
                LoadSlotCycle(DeviceNumber, _constraint1, "constraint" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);

            }

            public void LoadSignalsCycle()
            {
                LoadSlotCycle(DeviceNumber, _signals, "signals" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
                LoadSlotCycle(DeviceNumber, _nominal, "nominal" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
            }

            public void LoadNominal()
            {
                LoadSlotCycle(DeviceNumber, _nominal, "nominal" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
            }

            public void LoadSignals()
            {
                LoadSlot(DeviceNumber, _nominal, "nominal" + DeviceNumber, this);
                LoadSlot(DeviceNumber, _signals, "signals" + DeviceNumber, this);
            }

            public void LoadSignalsWord()
            {
                LoadSlot(DeviceNumber, _signals, "signalsWord" + DeviceNumber, this);
            }

            public void LoadAllMemory()
            {
                LoadSlot(DeviceNumber, _all_memory, "all_memory" + DeviceNumber, this);
            }

            public void SaveAllMemory()
            {
                SaveSlot(DeviceNumber, _all_memory, "all_memorySave" + DeviceNumber, this);
            }

            public void LoadJournalCycle()
            {
                LoadSlotCycle(DeviceNumber, _journal, "journal" + DeviceNumber, new TimeSpan(this.mb.Port.WaitByte * 10000), 10, this);
            }

            public void Kvit()
            {
                slot kvit = new slot(0x12, 0x13);
                kvit.Value[0] = 1;
                SaveSlot(DeviceNumber, kvit, "saveKvit", this);
            }

            public void LoadConstraint()
            {
                LoadSlot(DeviceNumber, _nominal, "nominal" + DeviceNumber, this);
                LoadSlot(DeviceNumber, _constraint2, "constraint2" + DeviceNumber, this);
                LoadSlot(DeviceNumber, _constraint1, "constraint1" + DeviceNumber, this);
            }

            public void SaveConstraint()
            {
                SaveSlot(DeviceNumber, _constraint1, "saveConstraint1" + DeviceNumber, this);
                SaveSlot(DeviceNumber, _constraint2, "saveConstraint2" + DeviceNumber, this);
                SaveSlot(DeviceNumber, _nominal, "saveNominal" + DeviceNumber, this);
            }

            public void ClearMemory()
            {
                for (int i = 0; i < 0x50; i++)
                {
                    _all_memory.Value[i] = 0;
                }

                SaveSlot(DeviceNumber, _all_memory, "all_memorySave" + DeviceNumber, this);
            }

            #endregion

            private Hashtable CreateLogHash()
            {
                Hashtable ret = new Hashtable();
                ret.Add("journal" + DeviceNumber, "��100 �" + DeviceNumber + " ������ ������� ������" + DeviceNumber);
                ret.Add("nominal" + DeviceNumber, "��100 �" + DeviceNumber + " ������ �������� ��������" + DeviceNumber);
                ret.Add("signals" + DeviceNumber, "��100 �" + DeviceNumber + " ������ ��������� ����������" + DeviceNumber);
                ret.Add("saveKvit" + DeviceNumber, "��100 �" + DeviceNumber + " ������������" + DeviceNumber);

                ret.Add("constraint1" + DeviceNumber, "��100 �" + DeviceNumber + " ������ �������" + DeviceNumber);
                ret.Add("constraint2" + DeviceNumber, "��100 �" + DeviceNumber + " ������ �������" + DeviceNumber);
                ret.Add("saveConstraint1" + DeviceNumber, "��100 �" + DeviceNumber + " ������ �������" + DeviceNumber);
                ret.Add("saveConstraint2" + DeviceNumber, "��100 �" + DeviceNumber + " ������ �������" + DeviceNumber);

                ret.Add("saveNominal" + DeviceNumber, "��100 �" + DeviceNumber + " ������ �������� ��������" + DeviceNumber);
                ret.Add("all_memory" + DeviceNumber, "��100 �" + DeviceNumber + " �������� ������" + DeviceNumber);
                ret.Add("version" + DeviceNumber, "��100 �" + DeviceNumber + " ������ ������" + DeviceNumber);
                for (int i = 0; i < AKoeff.Size; i++)
                {
                    ret.Add("load" + i + "AKoeff" + DeviceNumber, "��100 �" + DeviceNumber + " �������� ������������ �" + DeviceNumber);
                }
                for (int i = 0; i < BKoeff.Size; i++)
                {
                    ret.Add("load" + i + "BKoeff" + DeviceNumber, "��100 �" + DeviceNumber + " �������� ������������ B" + DeviceNumber);
                }
                for (int i = 0; i < PaKoeff.Size; i++)
                {
                    ret.Add("load" + i + "PaKoeff" + DeviceNumber, "��100 �" + DeviceNumber + " �������� ������������ �" + DeviceNumber);
                }
                return ret;
            }

            private int IsKoeffQuery(string qname, string pattern)
            {
                int ret = -1;
                Match m = Regex.Match(qname, "load(?<index>\\d)" + pattern + "\\d");
                if (m.Success)
                {
                    ret = Convert.ToInt32(m.Result("${index}"));

                }
                return ret;
            }

            private void OnKoeffQuery(Query query, SlotArray slots, string pattern, bool raise)
            {
                int index = -1;
                if ((index = IsKoeffQuery(query.name, pattern)) >= 0)
                {
                    if (0 == query.fail)
                    {
                        slots.GetSlot(index).Loaded = true;
                    }

                    if (raise && index == (slots.Size - 1))
                    {
                        slot sl = slots.GetSlot(index);
                        Raise(query, _loadCalibrateOk, _loadCalibrateFail, ref sl);
                    }
                    slots.GetSlot(index).Value = Common.TOWORDS(query.readBuffer, true);

                    index = -1;
                }

            }



            #region IDeviceLog Members

            [Browsable(false)]
            public Hashtable LogHash
            {
                get
                {
                    return _logHash;
                    //throw new Exception("The method or operation is not implemented."); 
                }
            }

            private Hashtable _logHash;

            #endregion

            #region INodeSerializable Members

            public override XmlElement ToXml(XmlDocument doc)
            {
                XmlElement ret = base.ToXml(doc);
                XmlElement coeffLims = doc.CreateElement("CoeffLimits");
                for (int i = 0; i < 5; i++)
                {
                    XmlElement channel = doc.CreateElement("Channel" + i);
                    XmlElement min = doc.CreateElement("Min");
                    min.InnerText = Convert.ToBase64String(Common.TOBYTES(CoeffMin[i], true));
                    XmlElement max = doc.CreateElement("Max");
                    max.InnerText = Convert.ToBase64String(Common.TOBYTES(CoeffMax[i], true));
                    channel.AppendChild(min);
                    channel.AppendChild(max);
                    coeffLims.AppendChild(channel);
                }

                ret.AppendChild(coeffLims);
                return ret;
            }

            public override void FromXml(XmlElement element)
            {
                ushort[][] coeffMin = new ushort[5][];
                ushort[][] coeffMax = new ushort[5][];

                base.FromXml(element);
                foreach (XmlElement cel in element.ChildNodes)
                {
                    if ("CoeffLimits" == cel.Name)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            coeffMin[i] = Common.TOWORDS(Convert.FromBase64String(cel.ChildNodes[i].ChildNodes[0].InnerText), true);
                            coeffMax[i] = Common.TOWORDS(Convert.FromBase64String(cel.ChildNodes[i].ChildNodes[1].InnerText), true);
                        }
                        CoeffMax = coeffMax;
                        CoeffMin = coeffMin;
                    }
                }

                MB = mb;
            }

            #endregion

            #region IDeviceLog Members

            public event LoadHandler PropertiesLoadOk;

            public event LoadHandler PropertiesLoadFail;
            
            private bool _loadProperties = false;

            public void LoadProperties()
            {
                _loadProperties = true;
                LoadConstraint();
            }

            public void SaveProperties()
            {
                SaveConstraint();
            }


            #endregion

            #region ISlave Members

            public void CreateRequests()
            {
                byte type = 0;
                byte period = 0;
                byte paramCnt = 0x1;
                byte command = 0x0;

                if (null != ParentDevice)
                {
                    MemoryBlocks.Clear();
                    Requests.Clear();
                    //������ �� ���
                    MemoryManager.Block block1 = (ParentRAM as MemoryManager).CreateBlock(0x2);
                    MemoryBlocks.Add(block1);
                    ushort addrDB = (ushort)block1.Start;
                    Requests.Add(new Request(period, DeviceNumber, type, command, 0x0, addrDB, paramCnt, "���������� ���"));
                    //������ �����������
                    MemoryManager.Block block2 = (ParentRAM as MemoryManager).CreateBlock(0x2);
                    MemoryBlocks.Add(block2);
                    addrDB = (ushort)block2.Start;
                    Requests.Add(new Request(period, DeviceNumber, type, command, 0x108, addrDB, paramCnt, "�����������"));
                }

            }

            public void RemoveRequests()
            {
                this.Requests.Clear();

                if (null != ParentDevice)
                {
                    for (int i = 0; i < MemoryBlocks.Count; i++)
                    {
                        MemoryManager.Block block = MemoryBlocks[i] as MemoryManager.Block;
                        (ParentRAM as MemoryManager).DeleteBlock(ref block);
                    }
                }
            }

            private ArrayList _requests = new ArrayList();

            [Browsable(false)]
            public ArrayList Requests
            {
                get
                {
                    return _requests;
                }
                set
                {
                    _requests = value;
                }
            }

            ArrayList _memory = new ArrayList();
            [Browsable(false)]
            public ArrayList MemoryBlocks
            {
                get
                {
                    return _memory;
                }
                set
                {
                    _memory = value;
                }
            }

            [Browsable(false)]
            public object ParentRAM
            {
                get
                {
                    if (null == ParentDevice)
                    {
                        return null;
                    }
                    if (ParentDevice is IPicon)
                    {
                        return ((IPicon)ParentDevice).RAM as MemoryManager;
                    }
                    else
                    {
                        throw new DeviceNotSupportedException(ParentDevice.ToString() + " �� ������������ ������");
                    }

                }
            }

            [Browsable(false)]
            public bool IsManualAddressDB
            {
                get
                {
                    return _isManualAddressDB;
                }
                set
                {
                    _isManualAddressDB = value;

                    if (IsManualAddressDB && 0 != MemoryBlocks.Count)
                    {
                        RemoveRequests();
                    }
                    else
                    {
                        CreateRequests();
                    }
                }
            }

            [Browsable(false)]
            public byte ModuleType
            {
                get
                {
                    return 0x0;
                }

            }

            private bool _isManualAddressDB = true;

            [Browsable(false)]
            public Image FailImage
            {
                get
                {
                    return NodeImage;
                }
            }

            [Browsable(false)]
            public Image StatErrorImage
            {
                get
                {
                    return NodeImage;
                }
            }

            private Device _parentDevice;

            [Browsable(false)]
            public object ParentDevice
            {
                get { return (object)_parentDevice; }
                set { _parentDevice = (Device)value; }
            }

            [Browsable(false)]
            public DataTable Data
            {
                get
                {
                    System.Data.DataTable ret = new System.Data.DataTable();
                    ret.Columns.Add("����������", typeof(string));
                    ret.Columns.Add("��������", typeof(string));
                    ret.Columns.Add("��������", typeof(string));

                    if (0 != _requests.Count)
                    {
                        ushort addrDB = (_requests[0] as Request).addrDB;
                        string var = "MR100_" + DeviceNumber + "_I";
                        string value = (addrDB).ToString("X4");
                        string descr = "��� ";
                        ret.Rows.Add(new object[] { var, value, descr });

                    }

                    return ret;
                }
            }


            #endregion
        }
}
