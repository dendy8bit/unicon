using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using BEMN.Interfaces;
using BEMN.Devices;
using BEMN.Forms;


namespace BEMN.MR100.Forms
{
    public partial class ConfigurationForm : Form , IFormView
    {
        private delegate void SetShowMessageHandler(string msg);

        #region IFormView
        [Browsable(false)]
        public Type ClassType
        {
            get
            {
                return this.GetType();
            }
        }
        public bool ForceShow
        {
            get
            {
                return false;
            }
        }

        public Type FormDevice
        {
            get
            {
                return typeof(MR100Class);
            }
        }

        public INodeView[] ChildNodes
        {
            get
            {
                return null;
            }
        }


        public string NodeName
        {
            get
            {
                return "�������";
            }
        }

        public Image NodeImage
        {
            get
            {
                return AssemblyResources.Resources.config.ToBitmap();
            }
        }

        public bool Deletable
        {
            get
            {
                return false;
            }
        }
     
        #endregion

        private MR100Class _device;
        private ReadWriteForm rw_form;

        public ConfigurationForm()
        {
            InitializeComponent();
          
        }
        
        ToolTip tip = new ToolTip();
        
        public ConfigurationForm(MR100Class device)
        {
            byte[] buf = Measuring.NominalConverterInvert(20);
                        
            float f = Measuring.NominalConverterDirect(buf) / 10;
            device.Nominal = 2;
            device.Nominal.ToString();

            InitializeComponent();
             _device = device;
             _device._nominalOk += new Handler(_device__nominalOk);
             _device._nomainalFail += new Handler(_device__nomainalFail);
             _device._constraint1Ok += new Handler(_device__constraintOk);
             _device._constraint1Fail += new Handler(_device__constraintFail);
             _device._constraintSaveOk += new Handler(_device__constraintSaveOk);
             _device._constraintSaveFail += new Handler(_device__constraintSaveFail);

             if (Device.IsDeviceConnectedToPort(_device))
             {
                 rw_form = new ReadWriteForm();
                 BEMN.MR100.Forms.ConfigurationForm fffff = this as BEMN.MR100.Forms.ConfigurationForm;
                 panel1.Enabled = false;
                 rw_form.Show();
                 fffff.Activate();
                 _device.Koef = Convert.ToInt32(_numComboBox.Text);
                 _device.LoadConstraint();
             }
            
             tip = new ToolTip();
             
        }

        void _device__nomainalFail(object sender)
        {

        }

        void _device__nominalOk(object sender)
        {
            //
        }

        void _device__constraintSaveFail(object sender)
        {
            try
            {
                Invoke(new SetShowMessageHandler(SetShowMessage), new object[] { "������� �� co���������"});
                Invoke(new OnDeviceEventHandler(CloseRWDlg));
            }
            catch (InvalidOperationException)
            {    
                
            }
            
        }

        void _device__constraintSaveOk(object sender)
        {
            try
            {                
                Invoke(new SetShowMessageHandler(SetShowMessage), new object[] {"���������.��������."});
                Invoke(new OnDeviceEventHandler(CloseRWDlg));
                Invoke(new OnDeviceEventHandler(ReadConstraint));
            }
            catch (InvalidOperationException)
            {              
                
            }
            
        }

        private void GetConstraint()
        {            
            _timeEndurance1Box.Text = _device.TimeEndurance1.ToString();
            _timeEndurance2Box.Text = _device.TimeEndurance2.ToString();
          
            _tokEndurance1Box.Text = _device.TokEndurance1.ToString();
            _tokEndurance2Box.Text = _device.TokEndurance2.ToString();

            _shuntBox.Text = _device.Nominal.ToString();
            _UROVtimeBox.Text = _device.UROV_Time.ToString();
            _impulseBox.Text = _device.OffImpulse.ToString();
            _rascepitelBox.Text = _device.RascepitelTime.ToString();

            _RSAddrBox.Text = _device.DeviceAddress.ToString();
            _RSBaudeBox.Text = _device.BaudeRate.ToString();

            
            _BKCheck.Checked = !_device.DefenseMode[0];
            _chainCheck.Checked  = !_device.DefenseMode[1];
            _in1Check.Checked = !_device.DefenseMode[3];
            _in2Check.Checked = !_device.DefenseMode[4];
                        

            try
            {
                _releModeBox.SelectedIndex = (int)(_device.ReleMode);
                _RSBaudeBox.SelectedIndex = (int)(_device.BaudeRate);
            }
            catch (ArgumentOutOfRangeException)
            {
                _releModeBox.SelectedIndex = 0;
                _RSBaudeBox.SelectedIndex = 0;
            }

            this.Focus();
            
        }
      
        private bool IsConstrictCtrlCreated
        {
            get
            {
                return LedManager.IsControlsCreated(new Control[] {_timeEndurance1Box,
                                                                        _timeEndurance2Box,
                                                                        _tokEndurance1Box,
                                                                        _tokEndurance2Box,
                                                                        _UROVtimeBox,
                                                                        _impulseBox,
                                                                        _rascepitelBox});
            }
        }


        void _device__constraintOk(object sender)
        {
            if (IsConstrictCtrlCreated /*&& _device.IsNominalLoaded*/)
            {
                Invoke(new OnDeviceEventHandler(GetConstraint));
            }

            Invoke(new OnDeviceEventHandler(CloseRWDlg));

        }

        private void CloseRWDlg()
        {
            //try
            //{
            //    _device.PortNum = _device.Port;
            //    _device.LoadVersion();

            //}
            //catch (Exception r)
            //{
            //    _device.LoadVersion();
            //}


            if (this != null && this.Created)
            {
                panel1.Enabled = true;
                if (rw_form != null && rw_form.Created)
                {
                    rw_form.Close();
                }
            }
            this.Focus();

        }

        private void SetShowMessage(string msg)
        {

            if (this != null && this.Created)
            {
                panel1.Enabled = true;
                if (rw_form != null && rw_form.Created)
                {
                    rw_form.LabelText = msg;
                    rw_form.Refresh();
                    System.Threading.Thread.Sleep(2000);
                }
            }
        }

        void SetEmptyValue()
        {
            _timeEndurance1Box.Text = "0";
            _timeEndurance2Box.Text = "0";
            _tokEndurance1Box.Text = "0";
            _tokEndurance2Box.Text = "0";
            _UROVtimeBox.Text = "0";
            _impulseBox.Text = "0";
            _rascepitelBox.Text = "0";

            _RSAddrBox.Text = "0";
            _RSBaudeBox.Text = "0";
            _numComboBox.Text = "2";
            _shuntBox.Text = "0";
            _BKCheck.Checked = false;
            _chainCheck.Checked = false;
            _in1Check.Checked = false;
            _in2Check.Checked = false;

            _releModeBox.SelectedIndex = 0;
            _RSBaudeBox.SelectedIndex = 0;
        }

        void _device__constraintFail (object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(CloseRWDlg));
                Invoke(new OnDeviceEventHandler(SetEmptyValue));
            }
            catch (InvalidOperationException)
            {         }
        }

        private void ReadConstraint()
        {
            if (Device.IsDeviceConnectedToPort(_device))
            {
                rw_form = new ReadWriteForm();
                rw_form.LabelText = "���� ������ �������";
                BEMN.MR100.Forms.ConfigurationForm fffff = this as BEMN.MR100.Forms.ConfigurationForm;
                panel1.Enabled = false;
                rw_form.Show();
                fffff.Activate();
                _device.LoadConstraint();
            }
            
        }

        private void WriteConstraint()
        {
            if (Device.IsDeviceConnectedToPort(_device))
            {
                rw_form = new ReadWriteForm();
                rw_form.LabelText = "���� ������ �������";
                BEMN.MR100.Forms.ConfigurationForm fffff = this as BEMN.MR100.Forms.ConfigurationForm;
                panel1.Enabled = false;
                rw_form.Show();
                fffff.Activate();
                SetConstraint();
                _device.SaveConstraint();
            }
        }
       
        private void _readBut_Click(object sender, EventArgs e)
        {
            ReadConstraint();
        }

        private void _writeBut_Click(object sender, EventArgs e)
        {
            WriteConstraint();
        }

        private void _saveBut_Click(object sender, EventArgs e)
        {
            if (null != _device)
            {
                _saveConstrictDlg.FileName = string.Format("��100_�������_������ {0:F1}", this._device.DeviceVersion);
                _saveConstrictDlg.ShowDialog();
                if (null != _saveConstrictDlg.FileName && 0 != _saveConstrictDlg.FileName.Length)
                {
                    SetConstraint();
                    _device.SerializeConstraint(_saveConstrictDlg.FileName);
                }
            }
        }

        private void _closeBut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (null != _device )
            {
                _device._constraint1Ok -= new Handler(_device__constraintOk);
                _device._constraint1Fail -= new Handler(_device__constraintFail);
                _timeEndurance1Box.Leave -= new System.EventHandler(TimeLeaveFocus);
            }
            
        }

        private void SetConstraint()
        {
            try
            {   
             
                _device.DeviceAddress = Convert.ToUInt16(_RSAddrBox.Text);
                _device.OffImpulse = Convert.ToUInt16(_impulseBox.Text);
                _device.RascepitelTime = Convert.ToUInt16(_rascepitelBox.Text);

                _device.TimeEndurance1 = Convert.ToUInt16(_timeEndurance1Box.Text);
                _device.TimeEndurance2 = Convert.ToUInt16(_timeEndurance2Box.Text);

                _device.Nominal = Convert.ToSingle(_shuntBox.Text);
                _device.Koef = Convert.ToInt32(_numComboBox.Text);
                
                _device.TokEndurance1 = Convert.ToDouble(_tokEndurance1Box.Text);
                _device.TokEndurance2 = Convert.ToDouble(_tokEndurance2Box.Text);
               
                
                _device.UROV_Time = Convert.ToUInt16(_UROVtimeBox.Text);
            }
            catch (System.FormatException)
            {
                MessageBox.Show("������� �������� �������� �������. ��������� �������� ������.");
            }
            
            try
            {
                _device.ReleMode = (ushort)(_releModeBox.SelectedIndex);
                _device.BaudeRate = (ushort)(_RSBaudeBox.SelectedIndex);
            }
            catch (NullReferenceException)
            {

                _device.ReleMode = 0;
                _device.BaudeRate = 0;
            }


            _device.DefenseMode = new BitArray(new bool[] {!_BKCheck.Checked,
                                                           !_chainCheck.Checked,
                                                            true,
                                                            !_in1Check.Checked,
                                                            !_in2Check.Checked });
                     

        }

        private void _loadBut_Click(object sender, EventArgs e)
        {
            if (null != _device)
            {
                _openConstrictDlg.ShowDialog();

                if (null != _openConstrictDlg.FileName && 0 != _openConstrictDlg.FileName.Length)
                {
                    try
                    {
                        
                        _device.DeserializeConstraint(_openConstrictDlg.FileName);
                    }
                    catch (NullReferenceException)
                    {
                        MessageBox.Show("���� " + _openConstrictDlg.FileName + " ���������");
                        return;
                    }
                    catch(System.IO.FileNotFoundException)
                    {
                                                
                        return;
                    }

                    GetConstraint();
                }
                
            }
            
        }

     
        private void _nullConstraintBut_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("�������� ������� ?", "��������", MessageBoxButtons.YesNo))
            {
                _device.MakeConstraintsNull();
                _device.SaveConstraint();
            }
                        
        }

        private void TimeLeaveFocus(object sender, EventArgs e)
        {
                                  
            if(sender is TextBox)
            {
                TextBox txt = (TextBox)sender;
                try
                {
                    uint current_value = uint.Parse(txt.Text);
                    if (current_value < 0 || current_value > 10000)
                    {
                        UncorrectValue(txt);
                    }
                }
                catch(FormatException)
                {
                   UncorrectValue(txt);
                }
               
            }
        }

        private void CurrentLeaveFocus(object sender, EventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txt = (TextBox)sender;
                try
                {
                    double current_value = double.Parse(txt.Text);
                    if (current_value < 0 || current_value > double.Parse(_shuntBox.Text) * 10)
                    {
                        MessageBox.Show("�������� ������ ���� ������ � ��������� 0-" + double.Parse(_shuntBox.Text) * 10, "��������!");
                        txt.Focus();
                        txt.SelectAll();
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("������� �����", "��������!");
                    txt.Focus();
                    txt.SelectAll();
                }
                
            }

        }

        private void ShuntLeaveFocus(object sender, EventArgs e)
        {
            if (sender is TextBox)
            {
                TextBox txt = (TextBox)sender;
                try
                {
                    double current_value = double.Parse(txt.Text);
                    if (current_value < 0 || current_value > 10000)
                    {
                        UncorrectValue(txt);
                    }
                }
                catch (FormatException)
                {
                    UncorrectValue(txt);
                }

            }

        }

        private void UncorrectValue(TextBox txt)
        {
            MessageBox.Show("�������� ������ ���� ������ � ��������� 0-10000", "��������!");
            txt.Focus();
            txt.SelectAll();

        }

        private void _in1Check_CheckedChanged(object sender, EventArgs e)
        {
            if (_in1Check.Checked)
            {
                _timeEndurance1Box.Enabled = true;
                _tokEndurance1Box.Enabled = true;    
            }
            else
            {
                _timeEndurance1Box.Enabled = false;
                _tokEndurance1Box.Enabled = false;    
            }
            
        }

        private void _in2Check_CheckedChanged(object sender, EventArgs e)
        {
            if (_in2Check.Checked)
            {
                _timeEndurance2Box.Enabled = true;
                _tokEndurance2Box.Enabled = true;
            }
            else
            {
                _timeEndurance2Box.Enabled = false;
                _tokEndurance2Box.Enabled = false;
            }
        }

        private void TokBoxEnter(object sender, EventArgs e)
        {
            double limit = 0;
            try
            {
                limit = double.Parse(_shuntBox.Text);
            }
            catch (FormatException)
            {
                limit = 0;
            }
                
            tip.SetToolTip(sender as TextBox, "����� �� 0 �� " + limit * 10 + " (������� x 10)");
         }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            //_device.Port = _device.PortNum;
            ReadConstraint();
        }

        private void _numComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
     }
}
