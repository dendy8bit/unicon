namespace BEMN.MR100.Forms
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._constraintGroup = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._numComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._in2Check = new System.Windows.Forms.CheckBox();
            this._shuntGroup = new System.Windows.Forms.GroupBox();
            this._shuntBox = new System.Windows.Forms.TextBox();
            this._in1Check = new System.Windows.Forms.CheckBox();
            this._rascepitelBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._releModeBox = new System.Windows.Forms.ComboBox();
            this._impulseBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._UROVtimeBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._tokEndurance2Box = new System.Windows.Forms.TextBox();
            this._timeEndurance2Box = new System.Windows.Forms.TextBox();
            this._tokEndurance1Box = new System.Windows.Forms.TextBox();
            this._timeEndurance1Box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._defenseModeGroup = new System.Windows.Forms.GroupBox();
            this._chainCheck = new System.Windows.Forms.CheckBox();
            this._BKCheck = new System.Windows.Forms.CheckBox();
            this._RS_Group = new System.Windows.Forms.GroupBox();
            this._RSAddrBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._RSBaudeBox = new System.Windows.Forms.ComboBox();
            this._readBut = new System.Windows.Forms.Button();
            this._writeBut = new System.Windows.Forms.Button();
            this._saveBut = new System.Windows.Forms.Button();
            this._loadBut = new System.Windows.Forms.Button();
            this._nullConstraintBut = new System.Windows.Forms.Button();
            this._openConstrictDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConstrictDlg = new System.Windows.Forms.SaveFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this._constraintGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._shuntGroup.SuspendLayout();
            this._defenseModeGroup.SuspendLayout();
            this._RS_Group.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _constraintGroup
            // 
            this._constraintGroup.Controls.Add(this.groupBox1);
            this._constraintGroup.Controls.Add(this.label1);
            this._constraintGroup.Controls.Add(this.label4);
            this._constraintGroup.Controls.Add(this._in2Check);
            this._constraintGroup.Controls.Add(this._shuntGroup);
            this._constraintGroup.Controls.Add(this._in1Check);
            this._constraintGroup.Controls.Add(this._rascepitelBox);
            this._constraintGroup.Controls.Add(this.label8);
            this._constraintGroup.Controls.Add(this.label7);
            this._constraintGroup.Controls.Add(this._releModeBox);
            this._constraintGroup.Controls.Add(this._impulseBox);
            this._constraintGroup.Controls.Add(this.label6);
            this._constraintGroup.Controls.Add(this._UROVtimeBox);
            this._constraintGroup.Controls.Add(this.label5);
            this._constraintGroup.Controls.Add(this._tokEndurance2Box);
            this._constraintGroup.Controls.Add(this._timeEndurance2Box);
            this._constraintGroup.Controls.Add(this._tokEndurance1Box);
            this._constraintGroup.Controls.Add(this._timeEndurance1Box);
            this._constraintGroup.Controls.Add(this.label3);
            this._constraintGroup.Controls.Add(this.label2);
            this._constraintGroup.Location = new System.Drawing.Point(11, 3);
            this._constraintGroup.Name = "_constraintGroup";
            this._constraintGroup.Size = new System.Drawing.Size(325, 224);
            this._constraintGroup.TabIndex = 0;
            this._constraintGroup.TabStop = false;
            this._constraintGroup.Text = "�������";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._numComboBox);
            this.groupBox1.Location = new System.Drawing.Point(195, 154);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 38);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "�������";
            this.groupBox1.Visible = false;
            // 
            // _numComboBox
            // 
            this._numComboBox.FormattingEnabled = true;
            this._numComboBox.Items.AddRange(new object[] {
            "2",
            "10"});
            this._numComboBox.Location = new System.Drawing.Point(13, 11);
            this._numComboBox.Name = "_numComboBox";
            this._numComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._numComboBox.Size = new System.Drawing.Size(79, 21);
            this._numComboBox.TabIndex = 13;
            this._numComboBox.Text = "10";
            this._numComboBox.SelectedIndexChanged += new System.EventHandler(this._numComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(201, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "��� ������������, �";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "����� - ������� ��������������";
            // 
            // _in2Check
            // 
            this._in2Check.AutoSize = true;
            this._in2Check.Location = new System.Drawing.Point(7, 84);
            this._in2Check.Name = "_in2Check";
            this._in2Check.Size = new System.Drawing.Size(113, 17);
            this._in2Check.TabIndex = 3;
            this._in2Check.Text = "2-��.\'����������\'";
            this._in2Check.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._in2Check.UseVisualStyleBackColor = true;
            this._in2Check.CheckedChanged += new System.EventHandler(this._in2Check_CheckedChanged);
            // 
            // _shuntGroup
            // 
            this._shuntGroup.Controls.Add(this._shuntBox);
            this._shuntGroup.Location = new System.Drawing.Point(195, 105);
            this._shuntGroup.Name = "_shuntGroup";
            this._shuntGroup.Size = new System.Drawing.Size(121, 45);
            this._shuntGroup.TabIndex = 9;
            this._shuntGroup.TabStop = false;
            this._shuntGroup.Text = "��� �������/�����";
            // 
            // _shuntBox
            // 
            this._shuntBox.Location = new System.Drawing.Point(13, 21);
            this._shuntBox.MaxLength = 5;
            this._shuntBox.Name = "_shuntBox";
            this._shuntBox.Size = new System.Drawing.Size(79, 20);
            this._shuntBox.TabIndex = 0;
            this._shuntBox.Text = "0";
            this._shuntBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._shuntBox.Leave += new System.EventHandler(this.ShuntLeaveFocus);
            // 
            // _in1Check
            // 
            this._in1Check.AutoSize = true;
            this._in1Check.Location = new System.Drawing.Point(7, 38);
            this._in1Check.Name = "_in1Check";
            this._in1Check.Size = new System.Drawing.Size(95, 17);
            this._in1Check.TabIndex = 2;
            this._in1Check.Text = "1-��.\'�������\'";
            this._in1Check.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._in1Check.UseVisualStyleBackColor = true;
            this._in1Check.CheckedChanged += new System.EventHandler(this._in1Check_CheckedChanged);
            // 
            // _rascepitelBox
            // 
            this._rascepitelBox.Location = new System.Drawing.Point(121, 171);
            this._rascepitelBox.MaxLength = 5;
            this._rascepitelBox.Name = "_rascepitelBox";
            this._rascepitelBox.Size = new System.Drawing.Size(68, 20);
            this._rascepitelBox.TabIndex = 5;
            this._rascepitelBox.Text = "0";
            this._rascepitelBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._rascepitelBox.Leave += new System.EventHandler(this.TimeLeaveFocus);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "����� �����������";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "����� ����";
            // 
            // _releModeBox
            // 
            this._releModeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releModeBox.FormattingEnabled = true;
            this._releModeBox.Items.AddRange(new object[] {
            "����",
            "�������������",
            "���� ��� ������."});
            this._releModeBox.Location = new System.Drawing.Point(121, 197);
            this._releModeBox.Name = "_releModeBox";
            this._releModeBox.Size = new System.Drawing.Size(121, 21);
            this._releModeBox.TabIndex = 6;
            // 
            // _impulseBox
            // 
            this._impulseBox.Location = new System.Drawing.Point(121, 149);
            this._impulseBox.MaxLength = 5;
            this._impulseBox.Name = "_impulseBox";
            this._impulseBox.Size = new System.Drawing.Size(68, 20);
            this._impulseBox.TabIndex = 4;
            this._impulseBox.Text = "0";
            this._impulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._impulseBox.Leave += new System.EventHandler(this.TimeLeaveFocus);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "������� ����������";
            // 
            // _UROVtimeBox
            // 
            this._UROVtimeBox.Location = new System.Drawing.Point(121, 126);
            this._UROVtimeBox.MaxLength = 5;
            this._UROVtimeBox.Name = "_UROVtimeBox";
            this._UROVtimeBox.Size = new System.Drawing.Size(68, 20);
            this._UROVtimeBox.TabIndex = 3;
            this._UROVtimeBox.Text = "0";
            this._UROVtimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._UROVtimeBox.Leave += new System.EventHandler(this.TimeLeaveFocus);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "����� ����";
            // 
            // _tokEndurance2Box
            // 
            this._tokEndurance2Box.Enabled = false;
            this._tokEndurance2Box.Location = new System.Drawing.Point(208, 82);
            this._tokEndurance2Box.MaxLength = 5;
            this._tokEndurance2Box.Name = "_tokEndurance2Box";
            this._tokEndurance2Box.Size = new System.Drawing.Size(81, 20);
            this._tokEndurance2Box.TabIndex = 8;
            this._tokEndurance2Box.Text = "0";
            this._tokEndurance2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._tokEndurance2Box.Enter += new System.EventHandler(this.TokBoxEnter);
            this._tokEndurance2Box.Leave += new System.EventHandler(this.CurrentLeaveFocus);
            // 
            // _timeEndurance2Box
            // 
            this._timeEndurance2Box.Enabled = false;
            this._timeEndurance2Box.Location = new System.Drawing.Point(121, 82);
            this._timeEndurance2Box.MaxLength = 5;
            this._timeEndurance2Box.Name = "_timeEndurance2Box";
            this._timeEndurance2Box.Size = new System.Drawing.Size(68, 20);
            this._timeEndurance2Box.TabIndex = 2;
            this._timeEndurance2Box.Text = "0";
            this._timeEndurance2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._timeEndurance2Box.Leave += new System.EventHandler(this.TimeLeaveFocus);
            // 
            // _tokEndurance1Box
            // 
            this._tokEndurance1Box.Enabled = false;
            this._tokEndurance1Box.Location = new System.Drawing.Point(208, 38);
            this._tokEndurance1Box.MaxLength = 5;
            this._tokEndurance1Box.Name = "_tokEndurance1Box";
            this._tokEndurance1Box.Size = new System.Drawing.Size(81, 20);
            this._tokEndurance1Box.TabIndex = 7;
            this._tokEndurance1Box.Text = "0";
            this._tokEndurance1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._tokEndurance1Box.Enter += new System.EventHandler(this.TokBoxEnter);
            this._tokEndurance1Box.Leave += new System.EventHandler(this.CurrentLeaveFocus);
            // 
            // _timeEndurance1Box
            // 
            this._timeEndurance1Box.AcceptsReturn = true;
            this._timeEndurance1Box.AcceptsTab = true;
            this._timeEndurance1Box.Enabled = false;
            this._timeEndurance1Box.Location = new System.Drawing.Point(121, 38);
            this._timeEndurance1Box.MaxLength = 5;
            this._timeEndurance1Box.Name = "_timeEndurance1Box";
            this._timeEndurance1Box.Size = new System.Drawing.Size(68, 20);
            this._timeEndurance1Box.TabIndex = 1;
            this._timeEndurance1Box.Text = "0";
            this._timeEndurance1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._timeEndurance1Box.Leave += new System.EventHandler(this.TimeLeaveFocus);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "��� ������������, �";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "����� ������������, ��";
            // 
            // _defenseModeGroup
            // 
            this._defenseModeGroup.Controls.Add(this._chainCheck);
            this._defenseModeGroup.Controls.Add(this._BKCheck);
            this._defenseModeGroup.Location = new System.Drawing.Point(340, 133);
            this._defenseModeGroup.Name = "_defenseModeGroup";
            this._defenseModeGroup.Size = new System.Drawing.Size(138, 81);
            this._defenseModeGroup.TabIndex = 2;
            this._defenseModeGroup.TabStop = false;
            this._defenseModeGroup.Text = "����� ������";
            // 
            // _chainCheck
            // 
            this._chainCheck.AutoSize = true;
            this._chainCheck.Location = new System.Drawing.Point(13, 48);
            this._chainCheck.Name = "_chainCheck";
            this._chainCheck.Size = new System.Drawing.Size(95, 17);
            this._chainCheck.TabIndex = 1;
            this._chainCheck.Text = "�������� ��";
            this._chainCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._chainCheck.UseVisualStyleBackColor = true;
            // 
            // _BKCheck
            // 
            this._BKCheck.AutoSize = true;
            this._BKCheck.Location = new System.Drawing.Point(13, 23);
            this._BKCheck.Name = "_BKCheck";
            this._BKCheck.Size = new System.Drawing.Size(93, 17);
            this._BKCheck.TabIndex = 0;
            this._BKCheck.Text = "�������� ��";
            this._BKCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._BKCheck.UseVisualStyleBackColor = true;
            // 
            // _RS_Group
            // 
            this._RS_Group.Controls.Add(this._RSAddrBox);
            this._RS_Group.Controls.Add(this.label14);
            this._RS_Group.Controls.Add(this.label15);
            this._RS_Group.Controls.Add(this._RSBaudeBox);
            this._RS_Group.Location = new System.Drawing.Point(340, 15);
            this._RS_Group.Name = "_RS_Group";
            this._RS_Group.Size = new System.Drawing.Size(138, 91);
            this._RS_Group.TabIndex = 1;
            this._RS_Group.TabStop = false;
            this._RS_Group.Text = " RS-485";
            // 
            // _RSAddrBox
            // 
            this._RSAddrBox.Location = new System.Drawing.Point(62, 58);
            this._RSAddrBox.MaxLength = 3;
            this._RSAddrBox.Name = "_RSAddrBox";
            this._RSAddrBox.Size = new System.Drawing.Size(64, 20);
            this._RSAddrBox.TabIndex = 2;
            this._RSAddrBox.Text = "1";
            this._RSAddrBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 61);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "�����";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "��������";
            // 
            // _RSBaudeBox
            // 
            this._RSBaudeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RSBaudeBox.FormattingEnabled = true;
            this._RSBaudeBox.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
            this._RSBaudeBox.Location = new System.Drawing.Point(62, 27);
            this._RSBaudeBox.Name = "_RSBaudeBox";
            this._RSBaudeBox.Size = new System.Drawing.Size(64, 21);
            this._RSBaudeBox.TabIndex = 1;
            // 
            // _readBut
            // 
            this._readBut.Location = new System.Drawing.Point(15, 233);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(75, 23);
            this._readBut.TabIndex = 3;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _writeBut
            // 
            this._writeBut.Location = new System.Drawing.Point(96, 233);
            this._writeBut.Name = "_writeBut";
            this._writeBut.Size = new System.Drawing.Size(75, 23);
            this._writeBut.TabIndex = 4;
            this._writeBut.Text = "��������";
            this._writeBut.UseVisualStyleBackColor = true;
            this._writeBut.Click += new System.EventHandler(this._writeBut_Click);
            // 
            // _saveBut
            // 
            this._saveBut.Location = new System.Drawing.Point(286, 233);
            this._saveBut.Name = "_saveBut";
            this._saveBut.Size = new System.Drawing.Size(75, 23);
            this._saveBut.TabIndex = 6;
            this._saveBut.Text = "���������";
            this._saveBut.UseVisualStyleBackColor = true;
            this._saveBut.Click += new System.EventHandler(this._saveBut_Click);
            // 
            // _loadBut
            // 
            this._loadBut.Location = new System.Drawing.Point(205, 233);
            this._loadBut.Name = "_loadBut";
            this._loadBut.Size = new System.Drawing.Size(75, 23);
            this._loadBut.TabIndex = 5;
            this._loadBut.Text = "���������";
            this._loadBut.UseVisualStyleBackColor = true;
            this._loadBut.Click += new System.EventHandler(this._loadBut_Click);
            // 
            // _nullConstraintBut
            // 
            this._nullConstraintBut.Location = new System.Drawing.Point(403, 233);
            this._nullConstraintBut.Name = "_nullConstraintBut";
            this._nullConstraintBut.Size = new System.Drawing.Size(75, 23);
            this._nullConstraintBut.TabIndex = 7;
            this._nullConstraintBut.Text = "�������� ";
            this._nullConstraintBut.UseVisualStyleBackColor = true;
            this._nullConstraintBut.Visible = false;
            this._nullConstraintBut.Click += new System.EventHandler(this._nullConstraintBut_Click);
            // 
            // _openConstrictDlg
            // 
            this._openConstrictDlg.CheckPathExists = false;
            this._openConstrictDlg.DefaultExt = "xml";
            this._openConstrictDlg.Filter = "(*.xml) | *.xml";
            this._openConstrictDlg.RestoreDirectory = true;
            this._openConstrictDlg.Title = "������� ���� ������� ��100";
            // 
            // _saveConstrictDlg
            // 
            this._saveConstrictDlg.CheckPathExists = false;
            this._saveConstrictDlg.DefaultExt = "xml";
            this._saveConstrictDlg.FileName = "��100_�������";
            this._saveConstrictDlg.Filter = "(*.xml) | *.xml";
            this._saveConstrictDlg.RestoreDirectory = true;
            this._saveConstrictDlg.Title = "��������� ���� ������� ��100";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._constraintGroup);
            this.panel1.Controls.Add(this._nullConstraintBut);
            this.panel1.Controls.Add(this._defenseModeGroup);
            this.panel1.Controls.Add(this._saveBut);
            this.panel1.Controls.Add(this._RS_Group);
            this.panel1.Controls.Add(this._loadBut);
            this.panel1.Controls.Add(this._readBut);
            this.panel1.Controls.Add(this._writeBut);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(485, 273);
            this.panel1.TabIndex = 8;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 274);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this._constraintGroup.ResumeLayout(false);
            this._constraintGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this._shuntGroup.ResumeLayout(false);
            this._shuntGroup.PerformLayout();
            this._defenseModeGroup.ResumeLayout(false);
            this._defenseModeGroup.PerformLayout();
            this._RS_Group.ResumeLayout(false);
            this._RS_Group.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _constraintGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _tokEndurance1Box;
        private System.Windows.Forms.TextBox _timeEndurance1Box;
        private System.Windows.Forms.TextBox _tokEndurance2Box;
        private System.Windows.Forms.TextBox _timeEndurance2Box;
        private System.Windows.Forms.TextBox _impulseBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _UROVtimeBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _releModeBox;
        private System.Windows.Forms.TextBox _rascepitelBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox _defenseModeGroup;
        
        private System.Windows.Forms.GroupBox _RS_Group;
        private System.Windows.Forms.TextBox _RSAddrBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _RSBaudeBox;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.Button _writeBut;
        private System.Windows.Forms.Button _saveBut;
        private System.Windows.Forms.Button _loadBut;
        private System.Windows.Forms.Button _nullConstraintBut;
        private System.Windows.Forms.CheckBox _in1Check;
        private System.Windows.Forms.CheckBox _chainCheck;
        private System.Windows.Forms.CheckBox _BKCheck;
        private System.Windows.Forms.CheckBox _in2Check;
        private System.Windows.Forms.OpenFileDialog _openConstrictDlg;
        private System.Windows.Forms.GroupBox _shuntGroup;
        private System.Windows.Forms.TextBox _shuntBox;
        private System.Windows.Forms.SaveFileDialog _saveConstrictDlg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _numComboBox;
        private System.Windows.Forms.Panel panel1;
    }
}