﻿/*
 * Created by SharpDevelop.
 * User: Сергей
 * Date: 06.10.2006
 * Time: 12:58
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace MR100
{
	/// <summary>
	/// Description of ReadWriteForm.
	/// </summary>
	public partial class ReadWriteForm 
	{
		public ReadWriteForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}

        public string LabelText
        {
            get
            {
                return this.textBox1.Text;
            }
            set
            {
                this.textBox1.Text = value;
            }
        }
	}
}
