using System;

namespace BEMN.MR100
{
    public class ValuesConverterMr100
    {


        public static double MeasuringConverter(double value, double nominal)
        {
            return (double)((value * nominal) / 0x7FFF) ;
        }

        public static double TokAdductorDirect(ushort value, double nominal)
        {
            double tok = (double)value / 32767 * nominal * 10;
            if (Math.Ceiling(tok)-tok < 0.1)
            {
                tok = Math.Ceiling(tok);
            }
            else
            {
                tok = Math.Round(tok, 3);
            }
            return tok;
        }

        public static ushort TokAdductorInvert(double tok, double nominal)
        {
            return (ushort)((tok * 32767) / nominal / 10);
        }

        public static float NominalConverterDirect(byte[] nominal)
        {
            float ret = 0;
            if (4 != nominal.Length)
            {
                throw new ApplicationException("������� ������ ���� � 4-� ������");
            }

            ret = BitConverter.ToSingle(nominal, 0);

            return ret;
        }

        public static byte[] NominalConverterInvert(float nominal)
        {
            return BitConverter.GetBytes(nominal);
        }
    }
}