namespace BEMN.MR100.Configuration
{
    partial class Mr100ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._constraintGroup = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this._in2Check = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this._timeEndurance2Box = new System.Windows.Forms.MaskedTextBox();
            this._tokEndurance2Box = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._in1Check = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this._timeEndurance1Box = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._tokEndurance1Box = new System.Windows.Forms.MaskedTextBox();
            this._rascepitelBox = new System.Windows.Forms.MaskedTextBox();
            this._versionLabel1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._releModeBox = new System.Windows.Forms.ComboBox();
            this._impulseBox = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._shuntGroup = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this._uShCb = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._shuntBox = new System.Windows.Forms.MaskedTextBox();
            this._defenseModeGroup = new System.Windows.Forms.GroupBox();
            this._chainCheck = new System.Windows.Forms.CheckBox();
            this._BKCheck = new System.Windows.Forms.CheckBox();
            this._readBut = new System.Windows.Forms.Button();
            this._writeBut = new System.Windows.Forms.Button();
            this._saveBut = new System.Windows.Forms.Button();
            this._loadBut = new System.Windows.Forms.Button();
            this._nullConstraintBut = new System.Windows.Forms.Button();
            this._openConstrictDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConstrictDlg = new System.Windows.Forms.SaveFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.jsBox = new System.Windows.Forms.GroupBox();
            this.resetCb = new System.Windows.Forms.CheckBox();
            this.autoCb = new System.Windows.Forms.CheckBox();
            this.kvintCb = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.timeOffAutomat = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._RS_Group = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this._RSAddrBox = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._RSBaudeBox = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._constraintGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._shuntGroup.SuspendLayout();
            this._defenseModeGroup.SuspendLayout();
            this.panel1.SuspendLayout();
            this.jsBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this._RS_Group.SuspendLayout();
            this.SuspendLayout();
            // 
            // _constraintGroup
            // 
            this._constraintGroup.Controls.Add(this.label9);
            this._constraintGroup.Controls.Add(this.label11);
            this._constraintGroup.Controls.Add(this.label10);
            this._constraintGroup.Controls.Add(this.groupBox2);
            this._constraintGroup.Controls.Add(this.groupBox1);
            this._constraintGroup.Location = new System.Drawing.Point(11, 87);
            this._constraintGroup.Name = "_constraintGroup";
            this._constraintGroup.Size = new System.Drawing.Size(325, 247);
            this._constraintGroup.TabIndex = 0;
            this._constraintGroup.TabStop = false;
            this._constraintGroup.Text = "������";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 228);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "0 - 10 I� ��� U�=75 ��,";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "0 - 12,5 I� ��� U�=60 ��,";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(232, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "* - ��� ������������ �������� � ���������:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._in2Check);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._timeEndurance2Box);
            this.groupBox2.Controls.Add(this._tokEndurance2Box);
            this.groupBox2.Location = new System.Drawing.Point(6, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 83);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "   2-� ������� \'����������\'";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "��� ������������*, �";
            // 
            // _in2Check
            // 
            this._in2Check.AutoSize = true;
            this._in2Check.Location = new System.Drawing.Point(0, 0);
            this._in2Check.Name = "_in2Check";
            this._in2Check.Size = new System.Drawing.Size(15, 14);
            this._in2Check.TabIndex = 3;
            this._in2Check.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._in2Check.UseVisualStyleBackColor = true;
            this._in2Check.CheckedChanged += new System.EventHandler(this._in2Check_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(220, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "����������� ��������� ��������������";
            // 
            // _timeEndurance2Box
            // 
            this._timeEndurance2Box.Enabled = false;
            this._timeEndurance2Box.Location = new System.Drawing.Point(232, 22);
            this._timeEndurance2Box.Name = "_timeEndurance2Box";
            this._timeEndurance2Box.Size = new System.Drawing.Size(68, 20);
            this._timeEndurance2Box.TabIndex = 2;
            this._timeEndurance2Box.Text = "0";
            this._timeEndurance2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tokEndurance2Box
            // 
            this._tokEndurance2Box.Enabled = false;
            this._tokEndurance2Box.Location = new System.Drawing.Point(232, 48);
            this._tokEndurance2Box.Name = "_tokEndurance2Box";
            this._tokEndurance2Box.Size = new System.Drawing.Size(68, 20);
            this._tokEndurance2Box.TabIndex = 8;
            this._tokEndurance2Box.Text = "0";
            this._tokEndurance2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._in1Check);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._timeEndurance1Box);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._tokEndurance1Box);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 83);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "   1-� ������� \'�������\'";
            // 
            // _in1Check
            // 
            this._in1Check.AutoSize = true;
            this._in1Check.Location = new System.Drawing.Point(0, 0);
            this._in1Check.Name = "_in1Check";
            this._in1Check.Size = new System.Drawing.Size(15, 14);
            this._in1Check.TabIndex = 2;
            this._in1Check.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._in1Check.UseVisualStyleBackColor = true;
            this._in1Check.CheckedChanged += new System.EventHandler(this._in1Check_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "����� ������������, ��";
            // 
            // _timeEndurance1Box
            // 
            this._timeEndurance1Box.Enabled = false;
            this._timeEndurance1Box.Location = new System.Drawing.Point(232, 23);
            this._timeEndurance1Box.Name = "_timeEndurance1Box";
            this._timeEndurance1Box.Size = new System.Drawing.Size(68, 20);
            this._timeEndurance1Box.TabIndex = 1;
            this._timeEndurance1Box.Text = "0";
            this._timeEndurance1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "��� ������������*, �";
            // 
            // _tokEndurance1Box
            // 
            this._tokEndurance1Box.Enabled = false;
            this._tokEndurance1Box.Location = new System.Drawing.Point(232, 49);
            this._tokEndurance1Box.Name = "_tokEndurance1Box";
            this._tokEndurance1Box.Size = new System.Drawing.Size(68, 20);
            this._tokEndurance1Box.TabIndex = 7;
            this._tokEndurance1Box.Text = "0";
            this._tokEndurance1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rascepitelBox
            // 
            this._rascepitelBox.Location = new System.Drawing.Point(238, 31);
            this._rascepitelBox.Name = "_rascepitelBox";
            this._rascepitelBox.Size = new System.Drawing.Size(68, 20);
            this._rascepitelBox.TabIndex = 5;
            this._rascepitelBox.Text = "0";
            this._rascepitelBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _versionLabel1
            // 
            this._versionLabel1.AutoSize = true;
            this._versionLabel1.Location = new System.Drawing.Point(6, 39);
            this._versionLabel1.Name = "_versionLabel1";
            this._versionLabel1.Size = new System.Drawing.Size(160, 13);
            this._versionLabel1.TabIndex = 0;
            this._versionLabel1.Text = "�������� �� �������� ��, ��";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "����� ����";
            // 
            // _releModeBox
            // 
            this._releModeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releModeBox.FormattingEnabled = true;
            this._releModeBox.Location = new System.Drawing.Point(185, 84);
            this._releModeBox.Name = "_releModeBox";
            this._releModeBox.Size = new System.Drawing.Size(121, 21);
            this._releModeBox.TabIndex = 6;
            // 
            // _impulseBox
            // 
            this._impulseBox.Location = new System.Drawing.Point(238, 9);
            this._impulseBox.Name = "_impulseBox";
            this._impulseBox.Size = new System.Drawing.Size(68, 20);
            this._impulseBox.TabIndex = 4;
            this._impulseBox.Text = "0";
            this._impulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(184, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "������� ������� ����������, ��";
            // 
            // _shuntGroup
            // 
            this._shuntGroup.Controls.Add(this.label8);
            this._shuntGroup.Controls.Add(this._uShCb);
            this._shuntGroup.Controls.Add(this.label12);
            this._shuntGroup.Controls.Add(this._shuntBox);
            this._shuntGroup.Location = new System.Drawing.Point(11, 10);
            this._shuntGroup.Name = "_shuntGroup";
            this._shuntGroup.Size = new System.Drawing.Size(325, 72);
            this._shuntGroup.TabIndex = 9;
            this._shuntGroup.TabStop = false;
            this._shuntGroup.Text = "��������� �������������� �����";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "����������� ��� ����� I�, �";
            // 
            // _uShCb
            // 
            this._uShCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._uShCb.FormattingEnabled = true;
            this._uShCb.Location = new System.Drawing.Point(238, 47);
            this._uShCb.Name = "_uShCb";
            this._uShCb.Size = new System.Drawing.Size(68, 21);
            this._uShCb.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(215, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "����������� ���������� ����� U�, ��";
            // 
            // _shuntBox
            // 
            this._shuntBox.Location = new System.Drawing.Point(238, 22);
            this._shuntBox.Name = "_shuntBox";
            this._shuntBox.Size = new System.Drawing.Size(68, 20);
            this._shuntBox.TabIndex = 0;
            this._shuntBox.Text = "0";
            this._shuntBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _defenseModeGroup
            // 
            this._defenseModeGroup.Controls.Add(this._chainCheck);
            this._defenseModeGroup.Controls.Add(this._BKCheck);
            this._defenseModeGroup.Location = new System.Drawing.Point(342, 11);
            this._defenseModeGroup.Name = "_defenseModeGroup";
            this._defenseModeGroup.Size = new System.Drawing.Size(153, 71);
            this._defenseModeGroup.TabIndex = 2;
            this._defenseModeGroup.TabStop = false;
            this._defenseModeGroup.Text = "�������� �����������";
            // 
            // _chainCheck
            // 
            this._chainCheck.AutoSize = true;
            this._chainCheck.Location = new System.Drawing.Point(6, 19);
            this._chainCheck.Name = "_chainCheck";
            this._chainCheck.Size = new System.Drawing.Size(140, 17);
            this._chainCheck.TabIndex = 1;
            this._chainCheck.Text = "���� ���������� (��)";
            this._chainCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._chainCheck.UseVisualStyleBackColor = true;
            // 
            // _BKCheck
            // 
            this._BKCheck.AutoSize = true;
            this._BKCheck.Location = new System.Drawing.Point(6, 42);
            this._BKCheck.Name = "_BKCheck";
            this._BKCheck.Size = new System.Drawing.Size(129, 17);
            this._BKCheck.TabIndex = 0;
            this._BKCheck.Text = "����-��������� (��)";
            this._BKCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._BKCheck.UseVisualStyleBackColor = true;
            // 
            // _readBut
            // 
            this._readBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBut.Location = new System.Drawing.Point(3, 458);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(112, 41);
            this._readBut.TabIndex = 3;
            this._readBut.Text = "��������� ������� �� ����������";
            this.toolTip1.SetToolTip(this._readBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _writeBut
            // 
            this._writeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeBut.Location = new System.Drawing.Point(121, 458);
            this._writeBut.Name = "_writeBut";
            this._writeBut.Size = new System.Drawing.Size(111, 41);
            this._writeBut.TabIndex = 4;
            this._writeBut.Text = "�������� ������� � ����������";
            this.toolTip1.SetToolTip(this._writeBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeBut.UseVisualStyleBackColor = true;
            this._writeBut.Click += new System.EventHandler(this._writeBut_Click);
            // 
            // _saveBut
            // 
            this._saveBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveBut.Location = new System.Drawing.Point(384, 458);
            this._saveBut.Name = "_saveBut";
            this._saveBut.Size = new System.Drawing.Size(114, 41);
            this._saveBut.TabIndex = 6;
            this._saveBut.Text = "��������� ������� � ����";
            this.toolTip1.SetToolTip(this._saveBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveBut.UseVisualStyleBackColor = true;
            this._saveBut.Click += new System.EventHandler(this._saveBut_Click);
            // 
            // _loadBut
            // 
            this._loadBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadBut.Location = new System.Drawing.Point(264, 458);
            this._loadBut.Name = "_loadBut";
            this._loadBut.Size = new System.Drawing.Size(114, 41);
            this._loadBut.TabIndex = 5;
            this._loadBut.Text = "��������� ������� �� �����";
            this.toolTip1.SetToolTip(this._loadBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadBut.UseVisualStyleBackColor = true;
            this._loadBut.Click += new System.EventHandler(this._loadBut_Click);
            // 
            // _nullConstraintBut
            // 
            this._nullConstraintBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._nullConstraintBut.Location = new System.Drawing.Point(384, 424);
            this._nullConstraintBut.Name = "_nullConstraintBut";
            this._nullConstraintBut.Size = new System.Drawing.Size(114, 27);
            this._nullConstraintBut.TabIndex = 7;
            this._nullConstraintBut.Text = "�������� �������";
            this._nullConstraintBut.UseVisualStyleBackColor = true;
            this._nullConstraintBut.Click += new System.EventHandler(this._nullConstraintBut_Click);
            // 
            // _openConstrictDlg
            // 
            this._openConstrictDlg.CheckPathExists = false;
            this._openConstrictDlg.DefaultExt = "xml";
            this._openConstrictDlg.Filter = "(*.xml) | *.xml";
            this._openConstrictDlg.RestoreDirectory = true;
            this._openConstrictDlg.Title = "������� ���� ������� ��100";
            // 
            // _saveConstrictDlg
            // 
            this._saveConstrictDlg.CheckPathExists = false;
            this._saveConstrictDlg.DefaultExt = "xml";
            this._saveConstrictDlg.FileName = "��100_�������";
            this._saveConstrictDlg.Filter = "(*.xml) | *.xml";
            this._saveConstrictDlg.RestoreDirectory = true;
            this._saveConstrictDlg.Title = "��������� ���� ������� ��100";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.jsBox);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this._constraintGroup);
            this.panel1.Controls.Add(this._shuntGroup);
            this.panel1.Controls.Add(this._nullConstraintBut);
            this.panel1.Controls.Add(this._defenseModeGroup);
            this.panel1.Controls.Add(this._saveBut);
            this.panel1.Controls.Add(this._loadBut);
            this.panel1.Controls.Add(this._readBut);
            this.panel1.Controls.Add(this._writeBut);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(501, 502);
            this.panel1.TabIndex = 8;
            // 
            // jsBox
            // 
            this.jsBox.Controls.Add(this.resetCb);
            this.jsBox.Controls.Add(this.autoCb);
            this.jsBox.Controls.Add(this.kvintCb);
            this.jsBox.Location = new System.Drawing.Point(342, 88);
            this.jsBox.Name = "jsBox";
            this.jsBox.Size = new System.Drawing.Size(153, 95);
            this.jsBox.TabIndex = 11;
            this.jsBox.TabStop = false;
            this.jsBox.Text = "������ � ��";
            this.jsBox.Visible = false;
            // 
            // resetCb
            // 
            this.resetCb.AutoSize = true;
            this.resetCb.Location = new System.Drawing.Point(6, 65);
            this.resetCb.Name = "resetCb";
            this.resetCb.Size = new System.Drawing.Size(57, 17);
            this.resetCb.TabIndex = 2;
            this.resetCb.Text = "�����";
            this.resetCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.resetCb.UseVisualStyleBackColor = true;
            // 
            // autoCb
            // 
            this.autoCb.AutoSize = true;
            this.autoCb.Location = new System.Drawing.Point(6, 19);
            this.autoCb.Name = "autoCb";
            this.autoCb.Size = new System.Drawing.Size(120, 17);
            this.autoCb.TabIndex = 1;
            this.autoCb.Text = "������� ��������";
            this.autoCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.autoCb.UseVisualStyleBackColor = true;
            // 
            // kvintCb
            // 
            this.kvintCb.AutoSize = true;
            this.kvintCb.Location = new System.Drawing.Point(6, 42);
            this.kvintCb.Name = "kvintCb";
            this.kvintCb.Size = new System.Drawing.Size(98, 17);
            this.kvintCb.TabIndex = 0;
            this.kvintCb.Text = "������������";
            this.kvintCb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.kvintCb.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.timeOffAutomat);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this._rascepitelBox);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this._versionLabel1);
            this.groupBox3.Controls.Add(this._impulseBox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._releModeBox);
            this.groupBox3.Location = new System.Drawing.Point(11, 331);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(325, 120);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // timeOffAutomat
            // 
            this.timeOffAutomat.Location = new System.Drawing.Point(238, 53);
            this.timeOffAutomat.Name = "timeOffAutomat";
            this.timeOffAutomat.Size = new System.Drawing.Size(68, 20);
            this.timeOffAutomat.TabIndex = 8;
            this.timeOffAutomat.Text = "0";
            this.timeOffAutomat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "����� ���������� ��������, ��";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.ContextMenuStrip = this.contextMenu;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(521, 541);
            this.tabControl1.TabIndex = 9;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.clearSetpointsItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(258, 114);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(257, 22);
            this.readFromDeviceItem.Text = "��������� ������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(257, 22);
            this.writeToDeviceItem.Text = "�������� ������� � ����������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(257, 22);
            this.readFromFileItem.Text = "��������� ������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(257, 22);
            this.writeToFileItem.Text = "��������� ������� � ����";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(257, 22);
            this.clearSetpointsItem.Text = "�������� �������";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(513, 515);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "�������";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._RS_Group);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(513, 515);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "������������ RS-485";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _RS_Group
            // 
            this._RS_Group.Controls.Add(this.button1);
            this._RS_Group.Controls.Add(this.button2);
            this._RS_Group.Controls.Add(this._RSAddrBox);
            this._RS_Group.Controls.Add(this.label14);
            this._RS_Group.Controls.Add(this.label15);
            this._RS_Group.Controls.Add(this._RSBaudeBox);
            this._RS_Group.Location = new System.Drawing.Point(6, 6);
            this._RS_Group.Name = "_RS_Group";
            this._RS_Group.Size = new System.Drawing.Size(386, 92);
            this._RS_Group.TabIndex = 2;
            this._RS_Group.TabStop = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(6, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(190, 36);
            this.button1.TabIndex = 5;
            this.button1.Text = "��������� ������������ RS-485 �� ����������";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(196, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(184, 36);
            this.button2.TabIndex = 6;
            this.button2.Text = "�������� ������������ RS-485 � ����������";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // _RSAddrBox
            // 
            this._RSAddrBox.Location = new System.Drawing.Point(250, 21);
            this._RSAddrBox.Name = "_RSAddrBox";
            this._RSAddrBox.Size = new System.Drawing.Size(64, 20);
            this._RSAddrBox.TabIndex = 2;
            this._RSAddrBox.Text = "1";
            this._RSAddrBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(193, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "�����";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "��������";
            // 
            // _RSBaudeBox
            // 
            this._RSBaudeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RSBaudeBox.FormattingEnabled = true;
            this._RSBaudeBox.Location = new System.Drawing.Point(77, 21);
            this._RSBaudeBox.Name = "_RSBaudeBox";
            this._RSBaudeBox.Size = new System.Drawing.Size(64, 21);
            this._RSBaudeBox.TabIndex = 1;
            // 
            // Mr100ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 544);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Mr100ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr100ConfigurationForm_KeyUp);
            this._constraintGroup.ResumeLayout(false);
            this._constraintGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._shuntGroup.ResumeLayout(false);
            this._shuntGroup.PerformLayout();
            this._defenseModeGroup.ResumeLayout(false);
            this._defenseModeGroup.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.jsBox.ResumeLayout(false);
            this.jsBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this._RS_Group.ResumeLayout(false);
            this._RS_Group.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _constraintGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _tokEndurance1Box;
        private System.Windows.Forms.MaskedTextBox _timeEndurance1Box;
        private System.Windows.Forms.MaskedTextBox _tokEndurance2Box;
        private System.Windows.Forms.MaskedTextBox _timeEndurance2Box;
        private System.Windows.Forms.MaskedTextBox _impulseBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _releModeBox;
        private System.Windows.Forms.MaskedTextBox _rascepitelBox;
        private System.Windows.Forms.Label _versionLabel1;
        private System.Windows.Forms.GroupBox _defenseModeGroup;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.Button _writeBut;
        private System.Windows.Forms.Button _saveBut;
        private System.Windows.Forms.Button _loadBut;
        private System.Windows.Forms.Button _nullConstraintBut;
        private System.Windows.Forms.CheckBox _in1Check;
        private System.Windows.Forms.CheckBox _chainCheck;
        private System.Windows.Forms.CheckBox _BKCheck;
        private System.Windows.Forms.CheckBox _in2Check;
        private System.Windows.Forms.OpenFileDialog _openConstrictDlg;
        private System.Windows.Forms.GroupBox _shuntGroup;
        private System.Windows.Forms.MaskedTextBox _shuntBox;
        private System.Windows.Forms.SaveFileDialog _saveConstrictDlg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox jsBox;
        private System.Windows.Forms.CheckBox resetCb;
        private System.Windows.Forms.CheckBox autoCb;
        private System.Windows.Forms.CheckBox kvintCb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox _uShCb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox _RS_Group;
        private System.Windows.Forms.MaskedTextBox _RSAddrBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _RSBaudeBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.MaskedTextBox timeOffAutomat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}