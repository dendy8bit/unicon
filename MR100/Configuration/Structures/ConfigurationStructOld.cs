using System;
using System.Collections;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR100.Configuration.Structures
{
    public class ConfigurationStructOld : StructBase
    {

        [Layout(0)] private ushort _timeEndurance1;
        [Layout(1)] private ushort _tokEndurance1;
        [Layout(2)] private ushort _timeEndurance2;
        [Layout(3)] private ushort _tokEndurance2;
        [Layout(4)] private ushort _urovTime;
        [Layout(5)] private ushort _offImpulse;
        [Layout(6)] private ushort _rascepitelTime;
        [Layout(7)] private ushort _releMode;
        [Layout(8)] private ushort _defenseMode;
        [Layout(9, Count = 0x1B, Ignore = true)] private ushort[] _ignored;
        [Layout(10)] private ushort _nominal1;
        [Layout(11)] private ushort _nominal2;
        [Layout(12)] private ushort _uShunt;
        [Layout(13, Count = 9)] private ushort[] _res;


        public ushort ReleModeUshort
        {
            get { return _releMode; }
            set { _releMode = value; }
        }
        
        public BitArray DefenseMode
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(_defenseMode), Common.HIBYTE(_defenseMode) });
            }
            set
            {
                ushort bt = 0;
                for (int i = 0; i < value.Count; i++)
                {
                    if (value[i])
                    {
                        bt += (ushort)(Math.Pow(2, i));
                    }
                }

                _defenseMode = bt;
            }
        }

        [XmlElement(ElementName = "���_�������")]
        [BindingProperty(0)]
        public double Nominal
        {
            get
            {
                var val =ValuesConverterMr100.NominalConverterDirect(Common.TOBYTES(new[] { _nominal1, _nominal2 }, true)) / 10;
                return Math.Round(val, 2);
            }
            set
            {
                byte[] buffer = ValuesConverterMr100.NominalConverterInvert((float)(value * 10));
                var mass = Common.TOWORDS(buffer, true);
                _nominal1 = mass[0];
                _nominal2 = mass[1];
            }
        }


        [BindingProperty(1)]
        public string USh
        {
            get { return Validator.Get(_uShunt, ConfigStrings.UShunt, 0); }
            set { _uShunt = Validator.Set(value, ConfigStrings.UShunt, _uShunt, 0); }
        }

        double RealNominal
        {
            get
            {
                if (USh == ConfigStrings.UShunt[0])
                {
                    return Nominal;
                }
                else
                {
                    return Nominal*1.25;
                }
            }
        }

        [XmlElement(ElementName = "�����_1_�������")]
        [BindingProperty(2)]
        public ushort TimeEndurance1
        {
            get { return (ushort) (_timeEndurance1*10); }
            set { _timeEndurance1 = (ushort) (value/10); }
        }


        [XmlElement(ElementName = "���_������������_1_�������")]
        [BindingProperty(3)]
        public double TokEndurance1
        {
            get { return ValuesConverterMr100.TokAdductorDirect(_tokEndurance1, RealNominal); }
            set { _tokEndurance1 = ValuesConverterMr100.TokAdductorInvert(value, RealNominal); }
        }

        [XmlElement(ElementName = "�����_2_�������")]
        [BindingProperty(4)]
        public ushort TimeEndurance2
        {
            get { return (ushort) (_timeEndurance2); }
            set { _timeEndurance2 = (ushort) (value); }
        }

        [XmlElement(ElementName = "���_������������_2_�������")]
        [BindingProperty(5)]
        public double TokEndurance2
        {
            get { return ValuesConverterMr100.TokAdductorDirect(_tokEndurance2, RealNominal); }
            set { _tokEndurance2 = ValuesConverterMr100.TokAdductorInvert(value, RealNominal); }
        }

        [XmlElement(ElementName = "�������_����������")]
        [BindingProperty(6)]
        public ushort OffImpulse
        {
            get { return (ushort) (_offImpulse*10); }
            set { _offImpulse = (ushort) (value/10); }
        }

        [XmlElement(ElementName = "�����_�����������")]
        [BindingProperty(7)]
        public ushort RascepitelTime
        {
            get { return (ushort) (_rascepitelTime*10); }
            set { _rascepitelTime = (ushort) (value/10); }
        }

        [BindingProperty(8)]
        public bool Bk
        {
            get { return !Common.GetBit(_defenseMode, 0); }
            set { _defenseMode = Common.SetBit(_defenseMode, 0, !value); }
        }

        [BindingProperty(9)]
        public bool Co
        {
            get { return !Common.GetBit(_defenseMode, 1); }
            set { _defenseMode = Common.SetBit(_defenseMode, 1, !value); }
        }

        [BindingProperty(10)]
        public bool In1
        {
            get { return !Common.GetBit(_defenseMode, 3); }
            set { _defenseMode = Common.SetBit(_defenseMode, 3, !value); }
        }

        [BindingProperty(11)]
        public bool In2
        {
            get { return !Common.GetBit(_defenseMode, 4); }
            set { _defenseMode = Common.SetBit(_defenseMode, 4, !value); }
        }

        [BindingProperty(12)]
        public string ReleMode
        {
            get { return Validator.Get(_releMode, ConfigStrings.RelayModes); }
            set { _releMode = Validator.Set(value, ConfigStrings.RelayModes); }
        }

        [BindingProperty(13)]
        public ushort Urov
        {
            get { return (ushort) (_urovTime*10); }
            set { _urovTime = (ushort) (value/10); }
        }
    }
}