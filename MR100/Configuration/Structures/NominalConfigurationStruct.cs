﻿using System;
using System.Xml.Serialization;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MR100.Configuration.Structures
{
    public class NominalConfigurationStruct :StructBase
    {
        [Layout(0)]
        private ushort _nominal1;
        [Layout(1)]
        private ushort _nominal2;
        [Layout(2, Count = 10)]
        private ushort[] res;

        [Layout(3)]
        private ushort _speed;
        [Layout(4)]
        private ushort _address;

        //Необходим для пересчета Тока срабатывания в конфигурации
        [XmlElement(ElementName = "Ток датчика")]
        [BindingProperty(0)]
        public double Nominal
        {
            get
            {
                return Math.Round((ValuesConverterMr100.NominalConverterDirect(Common.TOBYTES(new[] { _nominal1, _nominal2 }, true)) / 10),3);
            }
            set
            {
                byte[] buffer = ValuesConverterMr100.NominalConverterInvert((float)(value * 10));
                var mass = Common.TOWORDS(buffer, true);
                _nominal1 = mass[0];
                _nominal2 = mass[1];
            }
        }

        public ushort BaudeRate
        {
            get { return _speed; }
            set { _speed = value; }
        }
        [XmlElement(ElementName = "Скорость порта")]
        [BindingProperty(1)]
        public string Speed
        {
            get { return Validator.Get(_speed, ConfigStrings.SpeedModes); }
            set { _speed = Validator.Set(value, ConfigStrings.SpeedModes); }
        }
        [XmlElement(ElementName = "Адрес устройства")]
        [BindingProperty(2)]
        public ushort Address
        {
            get { return _address; }
            set { _address = value; }
        }
    }
}
