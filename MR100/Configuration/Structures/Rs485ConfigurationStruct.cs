﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR100.Configuration.Structures
{
    public class Rs485ConfigurationStruct :StructBase
    {

        [Layout(0)]
        private ushort _speed;
        [Layout(1)]
        private ushort _address;

       

        [XmlElement(ElementName = "Скорость порта")]
        [BindingProperty(0)]
        public string Speed
        {
            get { return Validator.Get(_speed, ConfigStrings.SpeedModes); }
            set { _speed = Validator.Set(value, ConfigStrings.SpeedModes); }
        }
        [XmlElement(ElementName = "Адрес устройства")]
        [BindingProperty(1)]
        public ushort Address
        {
            get { return _address; }
            set { _address = value; }
        }
    }
}
