using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR100.Configuration.Structures;

namespace BEMN.MR100.Configuration
{
    public partial class Mr100ConfigurationForm : Form , IFormView
    {
        #region [IFormView]

        [Browsable(false)]
        public Type ClassType => GetType();

        public bool ForceShow => false;

        public Type FormDevice => typeof(Mr100Device);

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes => null;

        public string NodeName => "������������";
        
        public Image NodeImage => Resources.config.ToBitmap();

        public bool Deletable => false;

        #endregion [IFormView]

        private Mr100Device _device;
        private IValidator _currentValidator;
        private ToolTip _toolTip = new ToolTip();
        private double _version;
        private object _currentSetpointsStructOld;
        private object _currentSetpointsStructNew;
        private NewStructValidator<Rs485ConfigurationStruct> _rs485Validator;

        public Mr100ConfigurationForm()
        {
            InitializeComponent();
        }
        
        private bool IsProcess
        {
            set
            {
                _readBut.Enabled = !value;
                _writeBut.Enabled = !value;
                _loadBut.Enabled = !value;
                _saveBut.Enabled = !value;
                _nullConstraintBut.Enabled = !value;
            }
        }
        
        public Mr100ConfigurationForm(Mr100Device device)
        {
            InitializeComponent();
            _device = device;
            _toolTip = new ToolTip();
            device.ConfigurationOld.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadOkOld);
            device.ConfigurationOld.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������������ ��������"));
            device.ConfigurationOld.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������ ������ ������������"));
            device.ConfigurationOld.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������ ������ ������������"));

            device.ConfigurationNew.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadOkNew);
            device.ConfigurationNew.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������������ ��������"));
            device.ConfigurationNew.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������ ������ ������������"));
            device.ConfigurationNew.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������ ������ ������������"));

            device.Rs485Config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _rs485Validator.Set(device.Rs485Config.Value);
                MessageBoxShow("������������ RS-485 ���������");
            });
            device.Rs485Config.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������������ RS-485 ��������. \r\n��������! �������� \"��������\" ����������� \r\n� ���������� ����� ������������ ����������"));
            device.Rs485Config.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������ ������ ������������ RS-485"));
            device.Rs485Config.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBoxShow("������ ������ ������������ RS-485"));
            this.Init();
        }

        private void MessageBoxShow(string message)
        {
            IsProcess = false;
            MessageBox.Show(message);
        }

        private void ReadOkOld()
        {
            IsProcess = false;
            _currentSetpointsStructOld = _device.ConfigurationOld.Value;
            ShowConfigurationOld();
            MessageBox.Show("������������ ���������");
        }

        private void ReadOkNew()
        {
            IsProcess = false;
            _currentSetpointsStructNew = _device.ConfigurationNew.Value;
            ShowConfigurationNew();
            MessageBox.Show("������������ ���������");
        }

        private void Init()
        {
            _version = Common.VersionConverter(_device.DeviceVersion);
            var iFuncRule = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (_uShCb.SelectedItem.ToString() == ConfigStrings.UShunt[0])
                    {
                        return new CustomDoubleRule(0, Math.Round(double.Parse(_shuntBox.Text) * 10, 3));
                    }
                    else
                    {
                        return new CustomDoubleRule(0, Math.Round(double.Parse(_shuntBox.Text) * 12.5, 3));
                    }

                }
                catch (Exception)
                {
                    return new UshortTo10KRule();
                }
            });

            if (_version >= 5.30)
            {
                if (_version >= 5.40)
                {
                    jsBox.Visible = true;
                }
                _versionLabel1.Text = "�������� �������� ��, �";
                _BKCheck.Visible = false;

                _currentValidator = new NewStructValidator<ConfigurationStructNew>
                    (
                    this._toolTip,
                    new ControlInfoText(_shuntBox, new CustomDoubleRule(0, 10000)),
                    new ControlInfoCombo(_uShCb, ConfigStrings.UShunt),
                    new ControlInfoText(_timeEndurance1Box, new CustomUshortRule(20, 10000)),
                    new ControlInfoTextDependent(_tokEndurance1Box, iFuncRule),
                    new ControlInfoText(_timeEndurance2Box, new CustomUshortRule(0, 4000)),
                    new ControlInfoTextDependent(_tokEndurance2Box, iFuncRule),
                    new ControlInfoText(_impulseBox, new CustomUshortRule(200, 1000)),
                    new ControlInfoText(_rascepitelBox, new CustomUshortRule(1, 20)),

                    new ControlInfoCheck(_chainCheck),
                    new ControlInfoCheck(_in1Check),
                    new ControlInfoCheck(_in2Check),
                    new ControlInfoCombo(_releModeBox, ConfigStrings.RelayModes),
                    new ControlInfoCheck(autoCb),
                    new ControlInfoCheck(kvintCb),
                    new ControlInfoCheck(resetCb),
                    new ControlInfoText(timeOffAutomat, new CustomUshortRule(100, 1000))
                    );
                this._currentSetpointsStructNew = new ConfigurationStructNew();
            }
            else
            {
                _versionLabel1.Text = "�������� �� �������� ��, ��";
                _currentValidator = new NewStructValidator<ConfigurationStructOld>
                    (
                    this._toolTip,
                    new ControlInfoText(_shuntBox, new CustomDoubleRule(0, 10000)),
                    new ControlInfoCombo(_uShCb, ConfigStrings.UShunt),
                    new ControlInfoText(_timeEndurance1Box, new CustomUshortRule(20, 10000)),
                    new ControlInfoTextDependent(_tokEndurance1Box, iFuncRule),
                    new ControlInfoText(_timeEndurance2Box, new CustomUshortRule(0, 4000)),
                    new ControlInfoTextDependent(_tokEndurance2Box, iFuncRule),
                    new ControlInfoText(_impulseBox, new CustomUshortRule(200, 1000)),
                    new ControlInfoText(_rascepitelBox, new CustomUshortRule(0, 6000)),
                    new ControlInfoCheck(_BKCheck),
                    new ControlInfoCheck(_chainCheck),
                    new ControlInfoCheck(_in1Check),
                    new ControlInfoCheck(_in2Check),
                    new ControlInfoCombo(_releModeBox, ConfigStrings.RelayModes),
                    new ControlInfoText(timeOffAutomat, new CustomUshortRule(100, 1000))
                    );
                this._currentSetpointsStructOld = new ConfigurationStructOld();
            }
            _rs485Validator = new NewStructValidator
                <Rs485ConfigurationStruct>
                (
                this._toolTip,
                new ControlInfoCombo(_RSBaudeBox, ConfigStrings.SpeedModes),
                new ControlInfoText(_RSAddrBox, new CustomUshortRule(1, 247))
                );
        }
        
        private void ShowConfigurationOld()
        {
            _currentValidator.Set(_currentSetpointsStructOld);
        }

        private void ShowConfigurationNew()
        {
            _currentValidator.Set(this._currentSetpointsStructNew);
        }
        
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (_version >= 5.30)
            {
                _device.ConfigurationNew.LoadStruct();
            }
            else
            {
                _device.ConfigurationOld.LoadStruct();
            }
            IsProcess = true;
        }

        private void StartWrite()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string message;
            if (!_currentValidator.Check(out message,true))
            {
                MessageBox.Show("������� �������� �������");
                return;
            }
            
            if (_version >= 5.30)
            {
                var str = _currentValidator.Get();
                _device.ConfigurationNew.Value = (ConfigurationStructNew)str;
                _device.ConfigurationNew.SaveStruct();
            }
            else
            {
                var str = _currentValidator.Get();
                _device.ConfigurationOld.Value = (ConfigurationStructOld)str;
                _device.ConfigurationOld.SaveStruct();
            }
            IsProcess = true;
        }

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeBut_Click(object sender, EventArgs e)
        {
            this.StartWrite();
        }

        private void _saveBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            if (null != _device)
            {
                string message;
                if (!_currentValidator.Check(out message, true))
                {
                    MessageBox.Show("������� �������� �������");
                    return;
                }
                _saveConstrictDlg.FileName = string.Format("��100_�������_������ {0:F1}.xml", this._device.DeviceVersion);
                if (_saveConstrictDlg.ShowDialog() == DialogResult.OK)
                {
                    if (_version >= 5.30)
                    {
                        SerializeNew(_saveConstrictDlg.FileName);
                    }
                    else
                    {
                        SerializeOld(_saveConstrictDlg.FileName);
                    }
                }
            }
        }
        public void SerializeNew(string binFileName)
        {
            var doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("MR100"));
            var values = ((ConfigurationStructNew) _currentValidator.Get()).GetValues();
            XmlElement element = doc.CreateElement("MR100_SET_POINTS");
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
            if (doc.DocumentElement == null)
            {
                throw new NullReferenceException();
            }
            doc.DocumentElement.AppendChild(element);
            doc.Save(binFileName);
            MessageBox.Show(string.Format("���� {0} ������� ��������", binFileName));
        }

        public void SerializeOld(string fname)
        {
            var str = (ConfigurationStructOld)_currentValidator.Get();

            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("MR100"));
            XmlElement constraintEl = doc.CreateElement("Constraints");
            XmlElement offImpulse = doc.CreateElement("OffImpulse");
            offImpulse.InnerText = str.OffImpulse.ToString();
            XmlElement rascepitelTime = doc.CreateElement("RascepitelTime");
            rascepitelTime.InnerText = str.RascepitelTime.ToString();
            XmlElement timeEndurance1 = doc.CreateElement("TimeEndurance1");
            timeEndurance1.InnerText = str.TimeEndurance1.ToString();
            XmlElement timeEndurance2 = doc.CreateElement("TimeEndurance2");
            timeEndurance2.InnerText = str.TimeEndurance2.ToString();
            XmlElement tokEndurance1 = doc.CreateElement("TokEndurance1");
            tokEndurance1.InnerText = str.TokEndurance1.ToString();
            XmlElement tokEndurance2 = doc.CreateElement("TokEndurance2");
            tokEndurance2.InnerText = str.TokEndurance2.ToString();
            XmlElement releMode = doc.CreateElement("ReleMode");
            releMode.InnerText = str.ReleModeUshort.ToString();
            XmlElement defenseMode = doc.CreateElement("DefenseMode");
            defenseMode.InnerText = Common.BitsToString(str.DefenseMode);
            XmlElement nominal = doc.CreateElement("Nominal");
            nominal.InnerText = str.Nominal.ToString();
            constraintEl.AppendChild(releMode);
            constraintEl.AppendChild(offImpulse);
            constraintEl.AppendChild(rascepitelTime);
            constraintEl.AppendChild(timeEndurance1);
            constraintEl.AppendChild(timeEndurance2);
            constraintEl.AppendChild(tokEndurance1);
            constraintEl.AppendChild(tokEndurance2);
            constraintEl.AppendChild(defenseMode);
            constraintEl.AppendChild(nominal);
            doc.DocumentElement.AppendChild(constraintEl);
            doc.Save(fname);
        }
        
        private void _loadBut_Click(object sender, EventArgs e)
        {
           ReadFromFile(); 
        }

        private void ReadFromFile()
        {
            if (_openConstrictDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (_version >= 5.30)
                    {
                        this.DeserializeNew(_openConstrictDlg.FileName);
                        ShowConfigurationNew();
                    }
                    else
                    {
                        this.DeserializeOld(_openConstrictDlg.FileName);
                        ShowConfigurationOld();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("���� " + _openConstrictDlg.FileName + " ���������");
                }
            }
        }
        public void DeserializeNew(string binFileName)
        {
            var doc = new XmlDocument();
            doc.Load(binFileName);
            var a = doc.FirstChild.SelectSingleNode("MR100_SET_POINTS");
            if (a == null)
                throw new NullReferenceException();
            var values = Convert.FromBase64String(a.InnerText);
            var configurationStruct = new ConfigurationStructNew();
            configurationStruct.InitStruct(values);
            this._currentSetpointsStructNew = configurationStruct;
            this.ShowConfigurationNew();
            MessageBox.Show(string.Format("���� {0} ������� ��������", binFileName));
        }

        public void DeserializeOld(string fname)
        {
            ConfigurationStructOld config = new ConfigurationStructOld();

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(fname);
            }
            catch (FileNotFoundException e)
            {
                throw e;
            }

            try
            {
                config.Nominal = Convert.ToDouble(doc.SelectSingleNode("/MR100/Constraints/Nominal").InnerText);
                config.OffImpulse = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/OffImpulse").InnerText);
                config.RascepitelTime =
                    Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/RascepitelTime").InnerText);
                config.TimeEndurance1 =
                    Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/TimeEndurance1").InnerText);
                config.TimeEndurance2 =
                    Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/TimeEndurance2").InnerText);
                config.TokEndurance1 =
                    Convert.ToDouble(doc.SelectSingleNode("/MR100/Constraints/TokEndurance1").InnerText);
                config.TokEndurance2 =
                    Convert.ToDouble(doc.SelectSingleNode("/MR100/Constraints/TokEndurance2").InnerText);
                config.ReleModeUshort = Convert.ToUInt16(doc.SelectSingleNode("/MR100/Constraints/ReleMode").InnerText);
                config.DefenseMode =
                    Common.StringToBits(doc.SelectSingleNode("/MR100/Constraints/DefenseMode").InnerText);
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            _currentSetpointsStructOld = config;
            ShowConfigurationOld();
        }

        private void _nullConstraintBut_Click(object sender, EventArgs e)
        {
            this.ClearSetPoints();
        }

        private void ClearSetPoints()
        {
            if (DialogResult.Yes == MessageBox.Show("�������� ������� ?", "��������", MessageBoxButtons.YesNo))
            {
                _currentValidator.Reset();
            }
        }

        private void _in1Check_CheckedChanged(object sender, EventArgs e)
        {
            if (_in1Check.Checked)
            {
                _timeEndurance1Box.Enabled = true;
                _tokEndurance1Box.Enabled = true;    
            }
            else
            {
                _timeEndurance1Box.Enabled = false;
                _tokEndurance1Box.Enabled = false;    
            }
        }

        private void _in2Check_CheckedChanged(object sender, EventArgs e)
        {
            if (_in2Check.Checked)
            {
                _timeEndurance2Box.Enabled = true;
                _tokEndurance2Box.Enabled = true;
            }
            else
            {
                _timeEndurance2Box.Enabled = false;
                _tokEndurance2Box.Enabled = false;
            }
        }
        
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                StartRead();
                _device.Rs485Config.LoadStruct();
            }
            _currentValidator.Reset();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _device.Rs485Config.LoadStruct();
        }

        private void button2_Click(object sender, EventArgs e)
        {
        
            string mess;
            if (_rs485Validator.Check(out mess,true))
            {
                _device.Rs485Config.Value = _rs485Validator.Get();
                _device.Rs485Config.SaveStruct();
            }
        }
        
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.StartWrite();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == clearSetpointsItem)
            {
                this.ClearSetPoints();
            }
        }

        private void Mr100ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    StartWrite();
                    break;
                case Keys.R:
                    StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}
