﻿using System.Collections.Generic;

namespace BEMN.MR100.Configuration
{
    public static class ConfigStrings
    {
        /* public static List<string> RelayModes
        {
            get
            {
                return new List<string>
                    {
                        "УРОВ",
                        "Неисправность",
                        "УРОВ или неиспр."
                    };
            }
        }*/

        public static Dictionary<ushort,string> RelayModes
        {
            get
            {
                return new Dictionary<ushort, string>
                    {
                        //"УРОВ",
                        {1,"Неисправность"},
                       // "УРОВ или неиспр."
                    };
            }
        } 

        public static List<string> SpeedModes
        {
            get
            {
                return new List<string>
                {
                    "300",
                    "600",
                    "1200",
                    "2400",
                    "4800",
                    "9600",
                    "14400",
                    "19200",
                    "38400",
                    "57600",
                    "115200"
                };
            }
        }

        public static List<string> UShunt
        {
            get
            {
                return new List<string>
                {
                    "75",
                    "60"
                };
            }
        }
    }
}
