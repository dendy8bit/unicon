using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR100.Calibrate;
using BEMN.MR100.Calibrate.Structures;
using BEMN.MR100.Configuration;
using BEMN.MR100.Configuration.Structures;
using BEMN.MR100.Measuring;
using BEMN.MR100.Measuring.Structures;

namespace BEMN.MR100
{
    public class Mr100Device : Device, IDeviceView, IDeviceVersion
    {
        #region [IDeviceView]
        [Browsable(false)]
        public Type ClassType => this.GetType();
        
        [Browsable(false)]
        public bool Deletable => true;

        [Browsable(false)]
        public bool ForceShow => false;

        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.mr100new; 

        [Description("�������� ����������"), Category("�����")]
        [DisplayName("��������")]
        public string NodeName => "��100"; 

        [Browsable(false)]
        public INodeView[] ChildNodes => null;

        #endregion [IDeviceView]

        #region [IDeviceVersion]

        public Type[] Forms
        {
            get
            {
                switch (this.DeviceVersion)
                {
                    case "5.20 � ����":
                    {
                        this.DeviceVersion = "1";
                        break;
                    }
                    case "5.40 - 6.10":
                    {
                        this.DeviceVersion = "5.40";
                        break;
                    }
                }

                return new[]
                {
                    typeof (Mr100ConfigurationForm),
                    typeof (Mr100MeasuringForm),
                    typeof (Mr100CalibrateForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "5.20 � ����",
                    "5.40 - 6.10",
                    "7.00"
                };
            }
        }

        #endregion [IDeviceVersion]
        
        #region [Fields]
        private MemoryEntity<ConfigurationStructOld> _configurationOld;
        private MemoryEntity<ConfigurationStructNew> _configurationNew;
        private MemoryEntity<Rs485ConfigurationStruct> _rs485Config; 
        private MemoryEntity<NominalStruct> _nominal1;
        private MemoryEntity<DataBaseStruct> _db;
        private MemoryEntity<JournalStruct> _journalS;
        private MemoryEntity<CalibrateStruct> _calibrate;
        private MemoryEntity<FactorStruct> _channel1;
        private MemoryEntity<FactorStruct> _channel2;
        private MemoryEntity<FactorStruct> _channel3;
        private MemoryEntity<FactorStruct> _channel4;
        private MemoryEntity<FactorStruct> _channel5;
        private MemoryEntity<AllMemoryStruct> _allMemory;
        private MemoryEntity<AllMemoryStruct> _clearAllMemory;
        #endregion [Fields]

        #region [Constructor's]

        public Mr100Device()
        {
            HaveVersion = true;
        }

        public Mr100Device(Modbus mb)
        {
            MB = mb;
            HaveVersion = true;
            this.InitMemory();
        }

        [XmlIgnore]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set { base.MB = value; }
        }

        private void InitMemory()
        {
            _allMemory = new MemoryEntity<AllMemoryStruct>("��� ������", this, 0x100);
            _clearAllMemory = new MemoryEntity<AllMemoryStruct>("������ ���� ������", this, 0x100);
            _channel1 = new MemoryEntity<FactorStruct>("�����1", this, 0x110);
            _channel2 = new MemoryEntity<FactorStruct>("�����2", this, 0x113);
            _channel3 = new MemoryEntity<FactorStruct>("�����3", this, 0x116);
            _channel4 = new MemoryEntity<FactorStruct>("�����4", this, 0x119);
            _channel5 = new MemoryEntity<FactorStruct>("�����5", this, 0x11C);
            _rs485Config = new MemoryEntity<Rs485ConfigurationStruct>("�������������������", this, 0x130);
            _configurationOld = new MemoryEntity<ConfigurationStructOld>("������������", this, 0x100);
            _configurationNew = new MemoryEntity<ConfigurationStructNew>("������������New", this, 0x100);
            _nominal1 = new MemoryEntity<NominalStruct>("�������", this, 0x124);
            _db = new MemoryEntity<DataBaseStruct>("���������", this, 0x0);
            _journalS = new MemoryEntity<JournalStruct>("������", this, 0x140);
            _calibrate = new MemoryEntity<CalibrateStruct>("����������", this, 0x110);
        }

        #endregion [Constructor's]

        #region [Properties]
        public string Mr100Version { get; set; }
        
        public MemoryEntity<AllMemoryStruct> AllMemory => _allMemory;

        public MemoryEntity<AllMemoryStruct> ClearAllMemory => _clearAllMemory;

        public MemoryEntity<ConfigurationStructOld> ConfigurationOld => _configurationOld;

        public MemoryEntity<ConfigurationStructNew> ConfigurationNew => _configurationNew;

        public MemoryEntity<Rs485ConfigurationStruct> Rs485Config => _rs485Config;

        public MemoryEntity<NominalStruct> Nominal1 => _nominal1;

        public MemoryEntity<DataBaseStruct> Db => _db;

        public MemoryEntity<JournalStruct> JournalS => _journalS;

        public MemoryEntity<CalibrateStruct> Calibrate => _calibrate;

        public MemoryEntity<FactorStruct> Channel1 => _channel1;

        public MemoryEntity<FactorStruct> Channel2 => _channel2;

        public MemoryEntity<FactorStruct> Channel3 => _channel3;

        public MemoryEntity<FactorStruct> Channel4 => _channel4;

        public MemoryEntity<FactorStruct> Channel5 => _channel5;

        #endregion [Properties]      

        public void Kvit()
        {
            slot kvit = new slot(0x12, 0x13);
            kvit.Value[0] = 1;
            SaveSlot(DeviceNumber, kvit, "saveKvit", this);
        }
    }
}