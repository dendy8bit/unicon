﻿using System;
using System.Windows.Forms;

namespace BEMN.MR100.Calibrate
{
    public partial class Mr100PasswordForm : Form
    {
        private const string PASSWORD = "1111";
        public Mr100PasswordForm()
        {
            InitializeComponent();
        }

        private void AcceptBtn_Click(object sender, EventArgs e)
        {
            if (_passwordBox.Text == PASSWORD)
            {
                MessageBox.Show("Пароль принят", "Пароль", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Введен неверный пароль!", "Пароль", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
