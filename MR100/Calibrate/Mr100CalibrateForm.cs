using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR100.Calibrate.Structures;
using BEMN.MR100.Measuring.Structures;
using System.Linq;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;

namespace BEMN.MR100.Calibrate
{    
    public partial class Mr100CalibrateForm : Form , IFormView
    {
        #region [IFormView]

        public bool ForceShow => false;

        public bool Deletable => false;
        
        public Type FormDevice => typeof(Mr100Device);

        public bool Multishow { get; private set; }

        [Browsable(false)]
        public Type ClassType => GetType();
        
        public string NodeName => "���������� ����";
        
        public Image NodeImage => Framework.Properties.Resources.calibrate;

        public INodeView[] ChildNodes => null;

        #endregion [IFormView]

        private Mr100Device _device;
        private AveragerCount<DataBaseStruct> _averager;
        private MemoryEntity<FactorStruct> _currentFactor;
        private int _pos1;
        private ushort _b;
        private ushort _k = 0; // ������� ���������� �������� this._b c ������� ���� ���������� 
        private ushort _currentCount; // ���������� ������ � ����� _averager_IncIndex()
        //private int _sum = 0; // ����� ������� ����
        private int _sumMax = 0; // ����� ������� ����

        public Mr100CalibrateForm()
        {
            this.InitializeComponent();
        }
        
        public Mr100CalibrateForm(Mr100Device device)
        {
            this.InitializeComponent();
            this._device = device;
            
            this._device.Calibrate.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.CalibrateReadOk);
            this._device.Calibrate.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.RefreshFactors);
            this._device.Channel1.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.ChannelAllWriteOk);
            this._device.Channel2.AllWriteOk += HandlerHelper.CreateReadArrayHandler( this.ChannelAllWriteOk);
            this._device.Channel3.AllWriteOk += HandlerHelper.CreateReadArrayHandler( this.ChannelAllWriteOk);
            this._device.Channel4.AllWriteOk += HandlerHelper.CreateReadArrayHandler( this.ChannelAllWriteOk);
            this._device.Channel5.AllWriteOk += HandlerHelper.CreateReadArrayHandler( this.ChannelAllWriteOk);
            this._device.AllMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("���� ������� �������.");
                this.RefreshFactors();
            });
            this._device.AllMemory.AllWriteFail +=  HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("������ ������ �����.");
                this.RefreshFactors();
            });

            this._device.AllMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._saveMemoryDlg.ShowDialog() == DialogResult.OK)
                {
                    this.SerializeAllMemory(this._saveMemoryDlg.FileName, this._device.AllMemory.Value.Data);
                }
            });
            this._device.AllMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("������. �� ������� ��������� ������");
            });

            this._device.ClearAllMemory.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("���������� �������� ������, ������ �����") );
            this._device.ClearAllMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("������ ���������� ������� �������"));

            this._averager = new AveragerCount<DataBaseStruct>(this._device.Db);
            

            if (Common.VersionConverter(this._device.DeviceVersion) < 7.0)
            {
                this.plus.Visible = false;
                this.minus.Visible = false;
                this._inIMinus_Box.Visible = false;
                this._setIinMinusBtn.Visible = false;
            }
        }

        private void _averager_IncIndex()
        {
            //this._currentCount++;
            int index = this._averager.ValueList.Count;
            if (this._currentFactor == this._device.Channel1)
            {
                this._b = this._averager.ValueList[index - 1].InPlus;
                this._sumMax += this._averager.ValueList[index - 1].InPlus;
            }
            if (this._currentFactor == this._device.Channel2)
            {
                this._b = this._averager.ValueList[index - 1].USpoolNativ;
                this._sumMax += this._averager.ValueList[index - 1].USpoolNativ;
            }
            if (this._currentFactor == this._device.Channel3)
            {
                this._b = this._averager.ValueList[index - 1].UBk1Nativ;
                this._sumMax += this._averager.ValueList[index - 1].UBk1Nativ;
            }
            if (this._currentFactor == this._device.Channel4)
            {
                this._b = this._averager.ValueList[index - 1].InMinus;
                this._sumMax += this._averager.ValueList[index - 1].InMinus;
            }
            if (this._currentFactor == this._device.Channel5)
            {
                this._b = this._averager.ValueList[index - 1].UBk2Nativ;
                this._sumMax += this._averager.ValueList[index - 1].UBk2Nativ;
            }

            this._currentLabel.Invoke((MethodInvoker)delegate { this._currentLabel.Text = this._b.ToString(); });
        }
        

        private void _averager_Tick()
        {
            if (this._pos1 == 1)
            {
                if (this._currentFactor == this._device.Channel1)
                {
                    this._b = (ushort) this._averager.ValueList.Average(o => o.InPlus);
                }
                if (this._currentFactor == this._device.Channel2)
                {
                    this._b = (ushort) this._averager.ValueList.Average(o => o.USpoolNativ);
                }
                if (this._currentFactor == this._device.Channel3)
                {
                    this._b = (ushort) this._averager.ValueList.Average(o => o.UBk1Nativ);
                }
                if (this._currentFactor == this._device.Channel4)
                {
                    this._b = (ushort)this._averager.ValueList.Average(o => o.InMinus);
                }
                if (this._currentFactor == this._device.Channel5)
                {
                    this._b = (ushort) this._averager.ValueList.Average(o => o.UBk2Nativ);
                }
                FactorStruct temp = FactorStruct.Default.Clone<FactorStruct>();
                temp.B = this._b;
                this._currentFactor.Value = temp;
                MessageBox.Show("���������� �������� ��������������� ���������� �������� � ������� ��.");
                this._currentFactor.SaveStruct();
            }
            if (this._pos1 == 2)
            {
                int pa = 1;
                double atmp = 0;
                double koeff = 0.0;
                if (this._currentFactor == this._device.Channel1)
                {
                    //atmp = this._averager.ValueList.Average(o => o.InPlus);
                    atmp = this._sumMax / int.Parse(this._numericSamples.Text);
                    koeff = double.Parse(this._inIPlus_Box.Text);
                }
                if (this._currentFactor == this._device.Channel2)
                {
                    //atmp = this._averager.ValueList.Average(o => o.USpoolNativ);
                    atmp = this._sumMax / int.Parse(this._numericSamples.Text);
                    koeff = double.Parse(this._UrascBox.Text);
                }
                if (this._currentFactor == this._device.Channel3)
                {
                    //atmp = this._averager.ValueList.Average(o => o.UBk1Nativ);
                    atmp = this._sumMax / int.Parse(this._numericSamples.Text);
                    koeff = double.Parse(this._Ubk1Box.Text);
                }
                if (this._currentFactor == this._device.Channel4)
                {
                    //atmp = this._averager.ValueList.Average(o => o.InMinus);
                    atmp = this._sumMax / int.Parse(this._numericSamples.Text);
                    koeff = double.Parse(this._inIMinus_Box.Text);
                }
                if (this._currentFactor == this._device.Channel5)
                {
                    //atmp = this._averager.ValueList.Average(o => o.UBk2Nativ);
                    atmp = this._sumMax / int.Parse(this._numericSamples.Text);
                    koeff = double.Parse(this._Ubk2Box.Text);
                }
                //atmp -= this._b;
                atmp = (ulong) (0x8000*0x8000*koeff/100/atmp);

                if (atmp == 0)
                {
                    atmp = 0x8000;
                }

                while (atmp > 0xFFFF)
                {
                    atmp -= 0x7FFF;
                    pa++;
                }

                this._sumMax = 0;
                this._currentFactor.Value.A = (ushort) atmp;
                this._currentFactor.Value.Pa = (ushort) pa;
                this._currentFactor.Value.B = 0;
                this._currentFactor.SaveStruct();
            }
        }

        private void ChannelAllWriteOk()
        {
            this._pos1++;
            if ((this._pos1 ==1)||(this._pos1 ==2))
            {
                this._averager.Start((int) this._numericSamples.Value);
            }
            if (this._pos1 == 3)
            {
                this.RefreshFactors();
                MessageBox.Show("���������� ���������.");
                if (this._currentFactor == this._device.Channel1)
                {
                    this._currentLabel.Invoke((MethodInvoker) delegate
                    {
                        this._inIPlus_Box.BackColor = Color.Green;
                        this._inIPlus_Box.ReadOnly = true;
                    });
                }
                if (this._currentFactor == this._device.Channel2)
                {
                    this._currentLabel.Invoke((MethodInvoker)delegate
                    {
                        this._UrascBox.BackColor = Color.Green;
                        this._UrascBox.ReadOnly = true;
                    });
                }
                if (this._currentFactor == this._device.Channel3)
                {
                    this._currentLabel.Invoke((MethodInvoker)delegate
                    {
                        this._Ubk1Box.BackColor = Color.Green;
                        this._Ubk1Box.ReadOnly = true;
                    });
                }
                if (this._currentFactor == this._device.Channel4)
                {
                    this._currentLabel.Invoke((MethodInvoker)delegate
                    {
                        this._inIMinus_Box.BackColor = Color.Green;
                        this._inIMinus_Box.ReadOnly = true;
                    });
                }
                if (this._currentFactor == this._device.Channel5)
                {
                    this._currentLabel.Invoke((MethodInvoker)delegate
                    {
                        this._Ubk2Box.BackColor = Color.Green;
                        this._Ubk2Box.ReadOnly = true;
                    });
                }
                this._averager.MemoryEntityRemove();
                this._averager.Tick -= this._averager_Tick; // ����� ���������� ����� ������ ��������� DataBaseStruct n-��� (��� n = _numericSamples.Text)
                this._averager.IncIndex -= this._averager_IncIndex; // ����� ���������� ������ ��� ����� ������ ��������� DataBaseStruct

                this.LoadCalibrate(); // ������ ���������� -> ��������� ����� ����               
            }
        }

        private void LoadCalibrate()
        {
            this._device.Calibrate.LoadStruct(new TimeSpan(0, 0, 0, 1));
        }

        private void CalibrateReadOk()
        {
            this._coeffList.Items.Clear();
            for (int i = 0; i < this._device.Calibrate.Value.Factors.Length; i++)
            {
                FactorStruct factor = this._device.Calibrate.Value.Factors[i];
                this._coeffList.Items.Add(factor.ToString(i));
            }
        }
        
        private void _nullCoeffButton_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("�� ������� ��� ������ ������� ��� ����������?","��������",MessageBoxButtons.OKCancel)==DialogResult.OK)
            {
                this._device.Calibrate.Value = CalibrateStruct.Default;
                this._device.Calibrate.SaveStruct();
                _inIPlus_Box.BackColor = Color.White;
                _inIPlus_Box.ReadOnly = false;
                _inIMinus_Box.BackColor = Color.White;
                _inIMinus_Box.ReadOnly = false;
                _UrascBox.BackColor = Color.White;
                _UrascBox.ReadOnly = false;
                _Ubk1Box.BackColor = Color.White;
                _Ubk1Box.ReadOnly = false;
                _Ubk2Box.BackColor = Color.White;
                _Ubk2Box.ReadOnly = false;
            }
        }

        private void LoadMemoryFromFile()
        {
            if (DialogResult.Yes !=
                MessageBox.Show("������ ����������� �� ����� ������������� ��������� � ����������. ����������?",
                                "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                return;
            }
            if (DialogResult.OK == this._openMemoryDlg.ShowDialog())
            {

                try
                {
                    XmlDocument doc = new XmlDocument();

                    doc.Load(this._openMemoryDlg.FileName);

                    ushort[] data = Common.TOWORDS(
                        Convert.FromBase64String(doc.SelectSingleNode("/MR100/AllMemory").InnerText), true);

                    this._device.AllMemory.Value = new AllMemoryStruct {Data = data};
                    this._device.AllMemory.SaveStruct();

                }
                catch (NullReferenceException)
                {
                    MessageBox.Show("���� " + this._openMemoryDlg.FileName + " ���������");
                }
            }
        }

        public void SerializeAllMemory(string fname, ushort[] values)
        {
            if (string.IsNullOrEmpty(fname))
            {
                throw new NullReferenceException();
            }
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("MR100"));
            XmlElement allEl = doc.CreateElement("AllMemory");
            allEl.InnerText = Convert.ToBase64String(Common.TOBYTES(values, true));
            doc.DocumentElement.AppendChild(allEl);
            doc.Save(fname);
            MessageBox.Show("���� ��������");
        }

        private void _saveMemoryBut_Click(object sender, EventArgs e)
        {
            this._device.AllMemory.LoadStruct();
        }

        private void _loadMemoryBut_Click(object sender, EventArgs e)
        {
            this.LoadMemoryFromFile();
        }

        private void nullCalibr_CheckedChanged(object sender, EventArgs e)
        {
            if (this.nullCalibr.Checked)
            {
                this._Ubk1Box.Enabled = false;
                this._Ubk2Box.Enabled = false;
            }
            else 
            {
                this._Ubk1Box.Enabled = true;
                this._Ubk2Box.Enabled = true;
            }
        }

        private void StartSetFactor(MemoryEntity<FactorStruct> currentFactor)
        {
            this._averager.Tick += this._averager_Tick; // ����� ���������� ����� ������ ��������� DataBaseStruct n-��� (��� n = _numericSamples.Text)
            this._averager.IncIndex += this._averager_IncIndex; // ����� ���������� ������ ��� ����� ������ ��������� DataBaseStruct
            MessageBox.Show("���������� �������� 0 �� ����� ������.");

            this._pos1 = 0;
            this._currentFactor = currentFactor;
            this._currentFactor.Value = FactorStruct.Default;
            this._currentFactor.SaveStruct();
        }

        private void ReadBtnClick(object sender, EventArgs e)
        {
            this.RefreshFactors();
        }

        private void RefreshFactors()
        {
            this._device.Calibrate.LoadStruct(new TimeSpan(0, 0, 0, 1));
        }
        
        private void _setIinPlusBut_Click(object sender, EventArgs e)
        {
            this.StartSetFactor(this._device.Channel1);
        }

        private void _setIinMinusBut_Click(object sender, EventArgs e)
        {
            this.StartSetFactor(this._device.Channel4);
        }

        private void _setUrascBut_Click(object sender, EventArgs e)
        {
            this.StartSetFactor(this._device.Channel2);
        }

        private void _setUbk1But_Click(object sender, EventArgs e)
        {
            this.StartSetFactor(this._device.Channel3);
        }

        private void _setUbk2But_Click(object sender, EventArgs e)
        {
            this.StartSetFactor(this._device.Channel5);
        }

        private void Mr100CalibrateForm_Shown(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                MessageBox.Show("���������� ������������ ���������� � ������� ������. ���� ���������� ����� �������",
                    "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Close();
                return;
            }
            Mr100PasswordForm passForm = new Mr100PasswordForm();
            if (passForm.ShowDialog() != DialogResult.OK)
            {
                Close();
                return;
            }
            this.RefreshFactors();
        }

        private void _clearMemoryBtn_Click(object sender, EventArgs e)
        {
            this._device.ClearAllMemory.Value.Data = new ushort[this._device.ClearAllMemory.Value.Data.Length];
            this._device.ClearAllMemory.SaveStruct();
        }

        

        private void Mr100CalibrateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._averager.Tick -= this._averager_Tick; // ����� ���������� ����� ������ ��������� DataBaseStruct n-��� (��� n = _numericSamples.Text)
            this._averager.IncIndex -= this._averager_IncIndex; // ����� ���������� ������ ��� ����� ������ ��������� DataBaseStruct
            this._averager.MemoryEntityRemove();
        }
    }
}
