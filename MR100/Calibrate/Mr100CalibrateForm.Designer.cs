namespace BEMN.MR100.Calibrate
{
    partial class Mr100CalibrateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._openCalibrateDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveCalibrateDlg = new System.Windows.Forms.SaveFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ReadBtn = new System.Windows.Forms.Button();
            this._saveMemoryBut = new System.Windows.Forms.Button();
            this._loadMemoryBut = new System.Windows.Forms.Button();
            this._nullCoeffButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._numericSamples = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nullCalibr = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this._setUbk2But = new System.Windows.Forms.Button();
            this._UrascBox = new System.Windows.Forms.TextBox();
            this._Ubk1Box = new System.Windows.Forms.TextBox();
            this.minus = new System.Windows.Forms.Label();
            this.plus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._Ubk2Box = new System.Windows.Forms.TextBox();
            this._setUbk1But = new System.Windows.Forms.Button();
            this._setIinMinusBtn = new System.Windows.Forms.Button();
            this._setIinPlusBtn = new System.Windows.Forms.Button();
            this._setUrascBut = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this._inIMinus_Box = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._inIPlus_Box = new System.Windows.Forms.TextBox();
            this._saveMemoryDlg = new System.Windows.Forms.SaveFileDialog();
            this._openMemoryDlg = new System.Windows.Forms.OpenFileDialog();
            this._coeffList = new System.Windows.Forms.ListBox();
            this._clearMemoryBtn = new System.Windows.Forms.Button();
            this._currentLabel = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numericSamples)).BeginInit();
            this.SuspendLayout();
            // 
            // _openCalibrateDlg
            // 
            this._openCalibrateDlg.CheckPathExists = false;
            this._openCalibrateDlg.DefaultExt = "xml";
            this._openCalibrateDlg.Filter = "(*.xml) | *.xml";
            this._openCalibrateDlg.RestoreDirectory = true;
            this._openCalibrateDlg.Title = "������� ���� ���������� ��100";
            // 
            // _saveCalibrateDlg
            // 
            this._saveCalibrateDlg.CheckPathExists = false;
            this._saveCalibrateDlg.DefaultExt = "xml";
            this._saveCalibrateDlg.FileName = "��100_����������";
            this._saveCalibrateDlg.Filter = "(*.xml) | *.xml";
            this._saveCalibrateDlg.RestoreDirectory = true;
            this._saveCalibrateDlg.Title = "��������� ���� ���������� ��100";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ReadBtn);
            this.groupBox3.Controls.Add(this._saveMemoryBut);
            this.groupBox3.Controls.Add(this._loadMemoryBut);
            this.groupBox3.Controls.Add(this._nullCoeffButton);
            this.groupBox3.Location = new System.Drawing.Point(271, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(204, 136);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "���������";
            // 
            // ReadBtn
            // 
            this.ReadBtn.BackColor = System.Drawing.SystemColors.Control;
            this.ReadBtn.Location = new System.Drawing.Point(9, 48);
            this.ReadBtn.Name = "ReadBtn";
            this.ReadBtn.Size = new System.Drawing.Size(183, 23);
            this.ReadBtn.TabIndex = 22;
            this.ReadBtn.Text = "���������";
            this.ReadBtn.UseVisualStyleBackColor = false;
            this.ReadBtn.Click += new System.EventHandler(this.ReadBtnClick);
            // 
            // _saveMemoryBut
            // 
            this._saveMemoryBut.BackColor = System.Drawing.SystemColors.Control;
            this._saveMemoryBut.Location = new System.Drawing.Point(9, 77);
            this._saveMemoryBut.Name = "_saveMemoryBut";
            this._saveMemoryBut.Size = new System.Drawing.Size(183, 23);
            this._saveMemoryBut.TabIndex = 17;
            this._saveMemoryBut.Text = "��������� ��� ������ � ����";
            this._saveMemoryBut.UseVisualStyleBackColor = false;
            this._saveMemoryBut.Click += new System.EventHandler(this._saveMemoryBut_Click);
            // 
            // _loadMemoryBut
            // 
            this._loadMemoryBut.BackColor = System.Drawing.SystemColors.Control;
            this._loadMemoryBut.Location = new System.Drawing.Point(9, 102);
            this._loadMemoryBut.Name = "_loadMemoryBut";
            this._loadMemoryBut.Size = new System.Drawing.Size(183, 23);
            this._loadMemoryBut.TabIndex = 16;
            this._loadMemoryBut.Text = "��������� ��� ������ �� �����";
            this._loadMemoryBut.UseVisualStyleBackColor = false;
            this._loadMemoryBut.Click += new System.EventHandler(this._loadMemoryBut_Click);
            // 
            // _nullCoeffButton
            // 
            this._nullCoeffButton.BackColor = System.Drawing.Color.LightCoral;
            this._nullCoeffButton.Location = new System.Drawing.Point(9, 19);
            this._nullCoeffButton.Name = "_nullCoeffButton";
            this._nullCoeffButton.Size = new System.Drawing.Size(183, 23);
            this._nullCoeffButton.TabIndex = 10;
            this._nullCoeffButton.Text = "�������� ������������";
            this._nullCoeffButton.UseVisualStyleBackColor = false;
            this._nullCoeffButton.Click += new System.EventHandler(this._nullCoeffButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._numericSamples);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.nullCalibr);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this._setUbk2But);
            this.groupBox2.Controls.Add(this._UrascBox);
            this.groupBox2.Controls.Add(this._Ubk1Box);
            this.groupBox2.Controls.Add(this.minus);
            this.groupBox2.Controls.Add(this.plus);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this._Ubk2Box);
            this.groupBox2.Controls.Add(this._setUbk1But);
            this.groupBox2.Controls.Add(this._setIinMinusBtn);
            this.groupBox2.Controls.Add(this._setIinPlusBtn);
            this.groupBox2.Controls.Add(this._setUrascBut);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._inIMinus_Box);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this._inIPlus_Box);
            this.groupBox2.Location = new System.Drawing.Point(6, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 192);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "����������";
            // 
            // _numericSamples
            // 
            this._numericSamples.Location = new System.Drawing.Point(129, 166);
            this._numericSamples.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this._numericSamples.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._numericSamples.Name = "_numericSamples";
            this._numericSamples.Size = new System.Drawing.Size(42, 20);
            this._numericSamples.TabIndex = 20;
            this._numericSamples.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "���������� �������";
            // 
            // nullCalibr
            // 
            this.nullCalibr.AutoSize = true;
            this.nullCalibr.Location = new System.Drawing.Point(89, 17);
            this.nullCalibr.Name = "nullCalibr";
            this.nullCalibr.Size = new System.Drawing.Size(60, 17);
            this.nullCalibr.TabIndex = 13;
            this.nullCalibr.Text = "��101";
            this.nullCalibr.UseVisualStyleBackColor = true;
            this.nullCalibr.CheckedChanged += new System.EventHandler(this.nullCalibr_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(61, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "% �� ��������� �����";
            // 
            // _setUbk2But
            // 
            this._setUbk2But.Location = new System.Drawing.Point(173, 138);
            this._setUbk2But.Name = "_setUbk2But";
            this._setUbk2But.Size = new System.Drawing.Size(75, 23);
            this._setUbk2But.TabIndex = 8;
            this._setUbk2But.Text = "����������";
            this._setUbk2But.UseVisualStyleBackColor = true;
            this._setUbk2But.Click += new System.EventHandler(this._setUbk2But_Click);
            // 
            // _UrascBox
            // 
            this._UrascBox.Location = new System.Drawing.Point(103, 98);
            this._UrascBox.MaxLength = 3;
            this._UrascBox.Name = "_UrascBox";
            this._UrascBox.Size = new System.Drawing.Size(55, 20);
            this._UrascBox.TabIndex = 2;
            this._UrascBox.Text = "100";
            this._UrascBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Ubk1Box
            // 
            this._Ubk1Box.Location = new System.Drawing.Point(103, 119);
            this._Ubk1Box.MaxLength = 3;
            this._Ubk1Box.Name = "_Ubk1Box";
            this._Ubk1Box.Size = new System.Drawing.Size(55, 20);
            this._Ubk1Box.TabIndex = 3;
            this._Ubk1Box.Text = "100";
            this._Ubk1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // minus
            // 
            this.minus.AutoSize = true;
            this.minus.Location = new System.Drawing.Point(86, 80);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(10, 13);
            this.minus.TabIndex = 0;
            this.minus.Text = "-";
            // 
            // plus
            // 
            this.plus.AutoSize = true;
            this.plus.Location = new System.Drawing.Point(86, 59);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(13, 13);
            this.plus.TabIndex = 0;
            this.plus.Text = "+";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "U ����.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "������� ���";
            // 
            // _Ubk2Box
            // 
            this._Ubk2Box.Location = new System.Drawing.Point(103, 140);
            this._Ubk2Box.MaxLength = 3;
            this._Ubk2Box.Name = "_Ubk2Box";
            this._Ubk2Box.Size = new System.Drawing.Size(55, 20);
            this._Ubk2Box.TabIndex = 4;
            this._Ubk2Box.Text = "100";
            this._Ubk2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _setUbk1But
            // 
            this._setUbk1But.Location = new System.Drawing.Point(173, 117);
            this._setUbk1But.Name = "_setUbk1But";
            this._setUbk1But.Size = new System.Drawing.Size(75, 23);
            this._setUbk1But.TabIndex = 7;
            this._setUbk1But.Text = "����������";
            this._setUbk1But.UseVisualStyleBackColor = true;
            this._setUbk1But.Click += new System.EventHandler(this._setUbk1But_Click);
            // 
            // _setIinMinusBtn
            // 
            this._setIinMinusBtn.Location = new System.Drawing.Point(173, 75);
            this._setIinMinusBtn.Name = "_setIinMinusBtn";
            this._setIinMinusBtn.Size = new System.Drawing.Size(75, 23);
            this._setIinMinusBtn.TabIndex = 5;
            this._setIinMinusBtn.Text = "����������";
            this._setIinMinusBtn.UseVisualStyleBackColor = true;
            this._setIinMinusBtn.Click += new System.EventHandler(this._setIinMinusBut_Click);
            // 
            // _setIinPlusBtn
            // 
            this._setIinPlusBtn.Location = new System.Drawing.Point(173, 54);
            this._setIinPlusBtn.Name = "_setIinPlusBtn";
            this._setIinPlusBtn.Size = new System.Drawing.Size(75, 23);
            this._setIinPlusBtn.TabIndex = 5;
            this._setIinPlusBtn.Text = "����������";
            this._setIinPlusBtn.UseVisualStyleBackColor = true;
            this._setIinPlusBtn.Click += new System.EventHandler(this._setIinPlusBut_Click);
            // 
            // _setUrascBut
            // 
            this._setUrascBut.Location = new System.Drawing.Point(173, 96);
            this._setUrascBut.Name = "_setUrascBut";
            this._setUrascBut.Size = new System.Drawing.Size(75, 23);
            this._setUrascBut.TabIndex = 6;
            this._setUrascBut.Text = "����������";
            this._setUrascBut.UseVisualStyleBackColor = true;
            this._setUrascBut.Click += new System.EventHandler(this._setUrascBut_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "U �1";
            // 
            // _inIMinus_Box
            // 
            this._inIMinus_Box.Location = new System.Drawing.Point(103, 77);
            this._inIMinus_Box.MaxLength = 3;
            this._inIMinus_Box.Name = "_inIMinus_Box";
            this._inIMinus_Box.Size = new System.Drawing.Size(55, 20);
            this._inIMinus_Box.TabIndex = 1;
            this._inIMinus_Box.Text = "100";
            this._inIMinus_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "U �2";
            // 
            // _inIPlus_Box
            // 
            this._inIPlus_Box.Location = new System.Drawing.Point(103, 56);
            this._inIPlus_Box.MaxLength = 3;
            this._inIPlus_Box.Name = "_inIPlus_Box";
            this._inIPlus_Box.Size = new System.Drawing.Size(55, 20);
            this._inIPlus_Box.TabIndex = 1;
            this._inIPlus_Box.Text = "100";
            this._inIPlus_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _saveMemoryDlg
            // 
            this._saveMemoryDlg.CheckPathExists = false;
            this._saveMemoryDlg.DefaultExt = "xml";
            this._saveMemoryDlg.FileName = "��100_������";
            this._saveMemoryDlg.Filter = "(*.xml) | *.xml";
            this._saveMemoryDlg.RestoreDirectory = true;
            this._saveMemoryDlg.Title = "��������� ���� ������ ��100";
            // 
            // _openMemoryDlg
            // 
            this._openMemoryDlg.CheckPathExists = false;
            this._openMemoryDlg.DefaultExt = "xml";
            this._openMemoryDlg.Filter = "(*.xml) | *.xml";
            this._openMemoryDlg.RestoreDirectory = true;
            this._openMemoryDlg.Title = "������� ���� ������ ��100";
            // 
            // _coeffList
            // 
            this._coeffList.Enabled = false;
            this._coeffList.FormattingEnabled = true;
            this._coeffList.Location = new System.Drawing.Point(271, 145);
            this._coeffList.Name = "_coeffList";
            this._coeffList.Size = new System.Drawing.Size(204, 82);
            this._coeffList.TabIndex = 19;
            // 
            // _clearMemoryBtn
            // 
            this._clearMemoryBtn.BackColor = System.Drawing.Color.LightCoral;
            this._clearMemoryBtn.Location = new System.Drawing.Point(12, 200);
            this._clearMemoryBtn.Name = "_clearMemoryBtn";
            this._clearMemoryBtn.Size = new System.Drawing.Size(117, 23);
            this._clearMemoryBtn.TabIndex = 10;
            this._clearMemoryBtn.Text = "�������� ������";
            this._clearMemoryBtn.UseVisualStyleBackColor = false;
            this._clearMemoryBtn.Click += new System.EventHandler(this._clearMemoryBtn_Click);
            // 
            // _currentLabel
            // 
            this._currentLabel.AutoSize = true;
            this._currentLabel.Location = new System.Drawing.Point(160, 205);
            this._currentLabel.Name = "_currentLabel";
            this._currentLabel.Size = new System.Drawing.Size(13, 13);
            this._currentLabel.TabIndex = 20;
            this._currentLabel.Text = "0";
            // 
            // Mr100CalibrateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 235);
            this.Controls.Add(this._currentLabel);
            this.Controls.Add(this._coeffList);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this._clearMemoryBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mr100CalibrateForm";
            this.Text = "���������� ��������� ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr100CalibrateForm_FormClosing);
            this.Shown += new System.EventHandler(this.Mr100CalibrateForm_Shown);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numericSamples)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog _openCalibrateDlg;
        private System.Windows.Forms.SaveFileDialog _saveCalibrateDlg;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button _nullCoeffButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _setUbk2But;
        private System.Windows.Forms.TextBox _UrascBox;
        private System.Windows.Forms.TextBox _Ubk1Box;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _Ubk2Box;
        private System.Windows.Forms.Button _setUbk1But;
        private System.Windows.Forms.Button _setIinPlusBtn;
        private System.Windows.Forms.Button _setUrascBut;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _inIPlus_Box;
        private System.Windows.Forms.Button _saveMemoryBut;
        private System.Windows.Forms.Button _loadMemoryBut;
        private System.Windows.Forms.SaveFileDialog _saveMemoryDlg;
        private System.Windows.Forms.OpenFileDialog _openMemoryDlg;
        private System.Windows.Forms.ListBox _coeffList;
        private System.Windows.Forms.CheckBox nullCalibr;
        private System.Windows.Forms.NumericUpDown _numericSamples;
        private System.Windows.Forms.Button ReadBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button _clearMemoryBtn;
        private System.Windows.Forms.Label minus;
        private System.Windows.Forms.Label plus;
        private System.Windows.Forms.Button _setIinMinusBtn;
        private System.Windows.Forms.TextBox _inIMinus_Box;
        private System.Windows.Forms.Label _currentLabel;
    }
}
