﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR100.Calibrate.Structures
{
   public class CalibrateStruct:StructBase
   {
       public static readonly CalibrateStruct Default = new CalibrateStruct
       {
           _factors = new[]
           {
               FactorStruct.Default,
               FactorStruct.Default,
               FactorStruct.Default,
               FactorStruct.Default,
               FactorStruct.Default
           }
       };

       [Layout(0, Count = 5)] private FactorStruct[] _factors;
       public FactorStruct[] Factors
       {
           get { return _factors; }
           set { _factors = value; }
       }
   }
}
