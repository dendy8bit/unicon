﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR100.Calibrate.Structures
{
    public class AllMemoryStruct : StructBase
    {
        [Layout(0, Count = 80)]
        private ushort[] _mass ;

        public ushort[] Data
        {
            get { return _mass; }
            set
            {
                _mass = new ushort[80];
                Array.ConstrainedCopy(value, 0, _mass, 0, 80);
            }
        }
    }
}