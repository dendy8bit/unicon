﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR100.Calibrate.Structures
{
    public class FactorStruct : StructBase
    {
        public static FactorStruct Default = new FactorStruct { _a = 0x8000 , _b = 0x0, _pa = 0x1 };
        [Layout(0)] private ushort _b;
        [Layout(1)] private ushort _a;
        [Layout(2)] private ushort _pa;

        public ushort B
        {
            get { return _b; }
            set { _b = value; }
        }

        public ushort A
        {
            get { return _a; }
            set { _a = value; }
        }

        public ushort Pa
        {
            get { return _pa; }
            set { _pa = value; }
        }

        public string ToString(int index)
        {
            return string.Format("B{3} = {0:X}; A{3} = {1:X4}, {1}; Pa{3} = {2:X}", B, A, Pa, index + 1);
        }
    }
}
