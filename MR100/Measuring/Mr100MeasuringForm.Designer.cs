﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BEMN.Forms;

namespace BEMN.MR100.Measuring
{
    partial class Mr100MeasuringForm
    {
        private GroupBox _analogGroup;
        private Label label4;
        private TextBox _uBk2Box;
        private Label label3;
        private TextBox _uBk1Box;
        private Label _uSpool;
        private TextBox _uSpoolBox;
        private Label _inILabel;
        private TextBox _inIplus;
        private GroupBox _falgsGroup;
        private LedControl _secondStepLed;
        private LedControl _secondIOStepLed;
        private LedControl _firstStepLed;
        private LedControl _firstStepIOLed;
        private Label label1;
        private Label _errorBKKvitLabel;
        private Label _errorBkFoundedLabel;
        private Label label9;
        private Label label10;
        private LedControl _errorBKKvitLed;
        private LedControl _errorBkFoundedLed;
        private LedControl _levelWorkLed;
        private LedControl _levelActiveLed;
        private Label label6;
        private Label label5;
        private Label label2;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private LedControl _errorCRCProgramLed;
        private LedControl _errorValuesCRC;
        private LedControl _errorChainKvitLed;
        private LedControl _errorChainLed;
        private Label label15;
        private LedControl _errorACP;
        private GroupBox _avariaGroup;
        private DataGridView _journalGrid;
        private DataGridViewTextBoxColumn Msg;
        private Button _kvitBut;
        private GroupBox _versionGroup;
        private Label label17;
        private Label label16;
        private TextBox _versionBox;
        private TextBox _serialBox;
        private Label _lblAutomatOff;
        private LedControl _automatLedOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn Parametr;

        private void InitializeComponent()
        {
            this._analogGroup = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this._uBk2Box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._uBk1Box = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._uSpool = new System.Windows.Forms.Label();
            this._uSpoolBox = new System.Windows.Forms.TextBox();
            this._inILabel = new System.Windows.Forms.Label();
            this._inIminus = new System.Windows.Forms.TextBox();
            this._inIplus = new System.Windows.Forms.TextBox();
            this._falgsGroup = new System.Windows.Forms.GroupBox();
            this._relayLabel = new System.Windows.Forms.Label();
            this._d2Label = new System.Windows.Forms.Label();
            this._lblAutomatOff = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._kvitBut = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._errorBkFoundedLabel = new System.Windows.Forms.Label();
            this._errorBKKvitLabel = new System.Windows.Forms.Label();
            this._avariaGroup = new System.Windows.Forms.GroupBox();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this.Msg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parametr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._versionGroup = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._versionBox = new System.Windows.Forms.TextBox();
            this._serialBox = new System.Windows.Forms.TextBox();
            this.plus = new System.Windows.Forms.Label();
            this.minus = new System.Windows.Forms.Label();
            this._relayLed = new BEMN.Forms.LedControl();
            this._d2Led = new BEMN.Forms.LedControl();
            this._automatLedOff = new BEMN.Forms.LedControl();
            this._firstStepLed = new BEMN.Forms.LedControl();
            this._levelActiveLed = new BEMN.Forms.LedControl();
            this._levelWorkLed = new BEMN.Forms.LedControl();
            this._errorACP = new BEMN.Forms.LedControl();
            this._firstStepIOLed = new BEMN.Forms.LedControl();
            this._errorCRCProgramLed = new BEMN.Forms.LedControl();
            this._secondStepLed = new BEMN.Forms.LedControl();
            this._errorValuesCRC = new BEMN.Forms.LedControl();
            this._secondIOStepLed = new BEMN.Forms.LedControl();
            this._errorBkFoundedLed = new BEMN.Forms.LedControl();
            this._errorChainKvitLed = new BEMN.Forms.LedControl();
            this._errorBKKvitLed = new BEMN.Forms.LedControl();
            this._errorChainLed = new BEMN.Forms.LedControl();
            this._analogGroup.SuspendLayout();
            this._falgsGroup.SuspendLayout();
            this._avariaGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this._versionGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // _analogGroup
            // 
            this._analogGroup.Controls.Add(this.label4);
            this._analogGroup.Controls.Add(this._uBk2Box);
            this._analogGroup.Controls.Add(this.label3);
            this._analogGroup.Controls.Add(this._uBk1Box);
            this._analogGroup.Controls.Add(this.label7);
            this._analogGroup.Controls.Add(this.minus);
            this._analogGroup.Controls.Add(this.plus);
            this._analogGroup.Controls.Add(this._uSpool);
            this._analogGroup.Controls.Add(this._uSpoolBox);
            this._analogGroup.Controls.Add(this._inILabel);
            this._analogGroup.Controls.Add(this._inIminus);
            this._analogGroup.Controls.Add(this._inIplus);
            this._analogGroup.Location = new System.Drawing.Point(12, 12);
            this._analogGroup.Name = "_analogGroup";
            this._analogGroup.Size = new System.Drawing.Size(177, 180);
            this._analogGroup.TabIndex = 0;
            this._analogGroup.TabStop = false;
            this._analogGroup.Text = "Аналоговые величины";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "U на Д2";
            // 
            // _uBk2Box
            // 
            this._uBk2Box.Location = new System.Drawing.Point(104, 123);
            this._uBk2Box.Name = "_uBk2Box";
            this._uBk2Box.ReadOnly = true;
            this._uBk2Box.Size = new System.Drawing.Size(66, 20);
            this._uBk2Box.TabIndex = 6;
            this._uBk2Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "U на Д1";
            // 
            // _uBk1Box
            // 
            this._uBk1Box.Location = new System.Drawing.Point(104, 97);
            this._uBk1Box.Name = "_uBk1Box";
            this._uBk1Box.ReadOnly = true;
            this._uBk1Box.Size = new System.Drawing.Size(66, 20);
            this._uBk1Box.TabIndex = 4;
            this._uBk1Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(164, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "*нр-независимый расцепитель";
            // 
            // _uSpool
            // 
            this._uSpool.AutoSize = true;
            this._uSpool.Location = new System.Drawing.Point(12, 74);
            this._uSpool.Name = "_uSpool";
            this._uSpool.Size = new System.Drawing.Size(31, 13);
            this._uSpool.TabIndex = 3;
            this._uSpool.Text = "Uнр*";
            // 
            // _uSpoolBox
            // 
            this._uSpoolBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this._uSpoolBox.Location = new System.Drawing.Point(104, 71);
            this._uSpoolBox.Name = "_uSpoolBox";
            this._uSpoolBox.ReadOnly = true;
            this._uSpoolBox.Size = new System.Drawing.Size(66, 20);
            this._uSpoolBox.TabIndex = 2;
            this._uSpoolBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inILabel
            // 
            this._inILabel.AutoSize = true;
            this._inILabel.Location = new System.Drawing.Point(12, 22);
            this._inILabel.Name = "_inILabel";
            this._inILabel.Size = new System.Drawing.Size(69, 13);
            this._inILabel.TabIndex = 1;
            this._inILabel.Text = "Входной ток";
            // 
            // _inIminus
            // 
            this._inIminus.Location = new System.Drawing.Point(104, 45);
            this._inIminus.Name = "_inIminus";
            this._inIminus.ReadOnly = true;
            this._inIminus.Size = new System.Drawing.Size(66, 20);
            this._inIminus.TabIndex = 0;
            this._inIminus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inIplus
            // 
            this._inIplus.Location = new System.Drawing.Point(104, 19);
            this._inIplus.Name = "_inIplus";
            this._inIplus.ReadOnly = true;
            this._inIplus.Size = new System.Drawing.Size(66, 20);
            this._inIplus.TabIndex = 0;
            this._inIplus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _falgsGroup
            // 
            this._falgsGroup.Controls.Add(this._relayLabel);
            this._falgsGroup.Controls.Add(this._relayLed);
            this._falgsGroup.Controls.Add(this._d2Label);
            this._falgsGroup.Controls.Add(this._d2Led);
            this._falgsGroup.Controls.Add(this._lblAutomatOff);
            this._falgsGroup.Controls.Add(this._automatLedOff);
            this._falgsGroup.Controls.Add(this.label15);
            this._falgsGroup.Controls.Add(this._kvitBut);
            this._falgsGroup.Controls.Add(this.label11);
            this._falgsGroup.Controls.Add(this._firstStepLed);
            this._falgsGroup.Controls.Add(this._levelActiveLed);
            this._falgsGroup.Controls.Add(this._levelWorkLed);
            this._falgsGroup.Controls.Add(this._errorACP);
            this._falgsGroup.Controls.Add(this.label10);
            this._falgsGroup.Controls.Add(this.label2);
            this._falgsGroup.Controls.Add(this.label9);
            this._falgsGroup.Controls.Add(this.label12);
            this._falgsGroup.Controls.Add(this._firstStepIOLed);
            this._falgsGroup.Controls.Add(this.label1);
            this._falgsGroup.Controls.Add(this._errorCRCProgramLed);
            this._falgsGroup.Controls.Add(this.label13);
            this._falgsGroup.Controls.Add(this._secondStepLed);
            this._falgsGroup.Controls.Add(this.label6);
            this._falgsGroup.Controls.Add(this.label14);
            this._falgsGroup.Controls.Add(this._errorValuesCRC);
            this._falgsGroup.Controls.Add(this._secondIOStepLed);
            this._falgsGroup.Controls.Add(this.label5);
            this._falgsGroup.Controls.Add(this._errorBkFoundedLed);
            this._falgsGroup.Controls.Add(this._errorChainKvitLed);
            this._falgsGroup.Controls.Add(this._errorBkFoundedLabel);
            this._falgsGroup.Controls.Add(this._errorBKKvitLed);
            this._falgsGroup.Controls.Add(this._errorBKKvitLabel);
            this._falgsGroup.Controls.Add(this._errorChainLed);
            this._falgsGroup.Location = new System.Drawing.Point(195, 12);
            this._falgsGroup.Name = "_falgsGroup";
            this._falgsGroup.Size = new System.Drawing.Size(453, 180);
            this._falgsGroup.TabIndex = 1;
            this._falgsGroup.TabStop = false;
            this._falgsGroup.Text = "Состояния";
            // 
            // _relayLabel
            // 
            this._relayLabel.AutoSize = true;
            this._relayLabel.Location = new System.Drawing.Point(25, 151);
            this._relayLabel.Name = "_relayLabel";
            this._relayLabel.Size = new System.Drawing.Size(88, 13);
            this._relayLabel.TabIndex = 31;
            this._relayLabel.Text = "Состояние реле";
            // 
            // _d2Label
            // 
            this._d2Label.AutoSize = true;
            this._d2Label.Location = new System.Drawing.Point(243, 55);
            this._d2Label.Name = "_d2Label";
            this._d2Label.Size = new System.Drawing.Size(79, 13);
            this._d2Label.TabIndex = 29;
            this._d2Label.Text = "Состояние Д2";
            // 
            // _lblAutomatOff
            // 
            this._lblAutomatOff.AutoSize = true;
            this._lblAutomatOff.Location = new System.Drawing.Point(25, 55);
            this._lblAutomatOff.Name = "_lblAutomatOff";
            this._lblAutomatOff.Size = new System.Drawing.Size(125, 13);
            this._lblAutomatOff.TabIndex = 27;
            this._lblAutomatOff.Text = "Автомат отключен (Д1)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Неисправность АЦП";
            // 
            // _kvitBut
            // 
            this._kvitBut.Location = new System.Drawing.Point(224, 151);
            this._kvitBut.Name = "_kvitBut";
            this._kvitBut.Size = new System.Drawing.Size(184, 23);
            this._kvitBut.TabIndex = 3;
            this._kvitBut.Text = "Квитировать";
            this._kvitBut.UseVisualStyleBackColor = true;
            this._kvitBut.Click += new System.EventHandler(this._kvitBut_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(168, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Несовпадение CRC программы";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(243, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "УРОВ активен";
            this.label10.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Сработала 1-ая ступень";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(243, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Ошибка отключения";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Несовпадение CRC уставок";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(243, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Сработал ИО 1-й ступени";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(243, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(156, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Неиспр. ЦО не сквитирована";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Сработала 2-ая ступень";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Неисправность ЦО";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(243, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Сработал ИО 2-й ступени";
            // 
            // _errorBkFoundedLabel
            // 
            this._errorBkFoundedLabel.AutoSize = true;
            this._errorBkFoundedLabel.Location = new System.Drawing.Point(25, 55);
            this._errorBkFoundedLabel.Name = "_errorBkFoundedLabel";
            this._errorBkFoundedLabel.Size = new System.Drawing.Size(129, 13);
            this._errorBkFoundedLabel.TabIndex = 14;
            this._errorBkFoundedLabel.Text = "Обнаружена неиспр. БК";
            // 
            // _errorBKKvitLabel
            // 
            this._errorBKKvitLabel.AutoSize = true;
            this._errorBKKvitLabel.Location = new System.Drawing.Point(243, 55);
            this._errorBKKvitLabel.Name = "_errorBKKvitLabel";
            this._errorBKKvitLabel.Size = new System.Drawing.Size(197, 13);
            this._errorBKKvitLabel.TabIndex = 15;
            this._errorBKKvitLabel.Text = "Неиспр. БК устранена но не сквитир.";
            // 
            // _avariaGroup
            // 
            this._avariaGroup.Controls.Add(this._journalGrid);
            this._avariaGroup.Location = new System.Drawing.Point(12, 198);
            this._avariaGroup.Name = "_avariaGroup";
            this._avariaGroup.Size = new System.Drawing.Size(385, 128);
            this._avariaGroup.TabIndex = 2;
            this._avariaGroup.TabStop = false;
            this._avariaGroup.Text = "Журнал системы";
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.ColumnHeadersVisible = false;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Msg,
            this.Parametr});
            this._journalGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._journalGrid.Location = new System.Drawing.Point(3, 16);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.ReadOnly = true;
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this._journalGrid.Size = new System.Drawing.Size(379, 109);
            this._journalGrid.TabIndex = 0;
            // 
            // Msg
            // 
            this.Msg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Msg.HeaderText = "Сообщение";
            this.Msg.Name = "Msg";
            this.Msg.ReadOnly = true;
            this.Msg.Width = 170;
            // 
            // Parametr
            // 
            this.Parametr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Parametr.HeaderText = "Параметр";
            this.Parametr.Name = "Parametr";
            this.Parametr.ReadOnly = true;
            this.Parametr.Width = 150;
            // 
            // _versionGroup
            // 
            this._versionGroup.Controls.Add(this.label17);
            this._versionGroup.Controls.Add(this.label16);
            this._versionGroup.Controls.Add(this._versionBox);
            this._versionGroup.Controls.Add(this._serialBox);
            this._versionGroup.Location = new System.Drawing.Point(405, 198);
            this._versionGroup.Name = "_versionGroup";
            this._versionGroup.Size = new System.Drawing.Size(243, 128);
            this._versionGroup.TabIndex = 4;
            this._versionGroup.TabStop = false;
            this._versionGroup.Text = "Версия устройства";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Версия прошивки";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Серийный номер";
            // 
            // _versionBox
            // 
            this._versionBox.Enabled = false;
            this._versionBox.Location = new System.Drawing.Point(147, 59);
            this._versionBox.Name = "_versionBox";
            this._versionBox.Size = new System.Drawing.Size(79, 20);
            this._versionBox.TabIndex = 1;
            // 
            // _serialBox
            // 
            this._serialBox.Enabled = false;
            this._serialBox.Location = new System.Drawing.Point(147, 19);
            this._serialBox.Name = "_serialBox";
            this._serialBox.Size = new System.Drawing.Size(79, 20);
            this._serialBox.TabIndex = 0;
            // 
            // plus
            // 
            this.plus.AutoSize = true;
            this.plus.Location = new System.Drawing.Point(87, 22);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(13, 13);
            this.plus.TabIndex = 3;
            this.plus.Text = "+";
            // 
            // minus
            // 
            this.minus.AutoSize = true;
            this.minus.Location = new System.Drawing.Point(87, 48);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(10, 13);
            this.minus.TabIndex = 3;
            this.minus.Text = "-";
            // 
            // _relayLed
            // 
            this._relayLed.Location = new System.Drawing.Point(6, 151);
            this._relayLed.Name = "_relayLed";
            this._relayLed.Size = new System.Drawing.Size(13, 13);
            this._relayLed.State = BEMN.Forms.LedState.Off;
            this._relayLed.TabIndex = 30;
            // 
            // _d2Led
            // 
            this._d2Led.Location = new System.Drawing.Point(224, 55);
            this._d2Led.Name = "_d2Led";
            this._d2Led.Size = new System.Drawing.Size(13, 13);
            this._d2Led.State = BEMN.Forms.LedState.Off;
            this._d2Led.TabIndex = 28;
            // 
            // _automatLedOff
            // 
            this._automatLedOff.Location = new System.Drawing.Point(6, 55);
            this._automatLedOff.Name = "_automatLedOff";
            this._automatLedOff.Size = new System.Drawing.Size(13, 13);
            this._automatLedOff.State = BEMN.Forms.LedState.Off;
            this._automatLedOff.TabIndex = 26;
            // 
            // _firstStepLed
            // 
            this._firstStepLed.Location = new System.Drawing.Point(6, 16);
            this._firstStepLed.Name = "_firstStepLed";
            this._firstStepLed.Size = new System.Drawing.Size(13, 13);
            this._firstStepLed.State = BEMN.Forms.LedState.Off;
            this._firstStepLed.TabIndex = 1;
            // 
            // _levelActiveLed
            // 
            this._levelActiveLed.Location = new System.Drawing.Point(224, 130);
            this._levelActiveLed.Name = "_levelActiveLed";
            this._levelActiveLed.Size = new System.Drawing.Size(13, 13);
            this._levelActiveLed.State = BEMN.Forms.LedState.Off;
            this._levelActiveLed.TabIndex = 8;
            this._levelActiveLed.Visible = false;
            // 
            // _levelWorkLed
            // 
            this._levelWorkLed.Location = new System.Drawing.Point(224, 94);
            this._levelWorkLed.Name = "_levelWorkLed";
            this._levelWorkLed.Size = new System.Drawing.Size(13, 13);
            this._levelWorkLed.State = BEMN.Forms.LedState.Off;
            this._levelWorkLed.TabIndex = 9;
            // 
            // _errorACP
            // 
            this._errorACP.Location = new System.Drawing.Point(6, 132);
            this._errorACP.Name = "_errorACP";
            this._errorACP.Size = new System.Drawing.Size(13, 13);
            this._errorACP.State = BEMN.Forms.LedState.Off;
            this._errorACP.TabIndex = 24;
            // 
            // _firstStepIOLed
            // 
            this._firstStepIOLed.Location = new System.Drawing.Point(224, 16);
            this._firstStepIOLed.Name = "_firstStepIOLed";
            this._firstStepIOLed.Size = new System.Drawing.Size(13, 13);
            this._firstStepIOLed.State = BEMN.Forms.LedState.Off;
            this._firstStepIOLed.TabIndex = 0;
            // 
            // _errorCRCProgramLed
            // 
            this._errorCRCProgramLed.Location = new System.Drawing.Point(6, 113);
            this._errorCRCProgramLed.Name = "_errorCRCProgramLed";
            this._errorCRCProgramLed.Size = new System.Drawing.Size(13, 13);
            this._errorCRCProgramLed.State = BEMN.Forms.LedState.Off;
            this._errorCRCProgramLed.TabIndex = 19;
            // 
            // _secondStepLed
            // 
            this._secondStepLed.Location = new System.Drawing.Point(6, 35);
            this._secondStepLed.Name = "_secondStepLed";
            this._secondStepLed.Size = new System.Drawing.Size(13, 13);
            this._secondStepLed.State = BEMN.Forms.LedState.Off;
            this._secondStepLed.TabIndex = 3;
            // 
            // _errorValuesCRC
            // 
            this._errorValuesCRC.Location = new System.Drawing.Point(6, 94);
            this._errorValuesCRC.Name = "_errorValuesCRC";
            this._errorValuesCRC.Size = new System.Drawing.Size(13, 13);
            this._errorValuesCRC.State = BEMN.Forms.LedState.Off;
            this._errorValuesCRC.TabIndex = 18;
            // 
            // _secondIOStepLed
            // 
            this._secondIOStepLed.Location = new System.Drawing.Point(224, 35);
            this._secondIOStepLed.Name = "_secondIOStepLed";
            this._secondIOStepLed.Size = new System.Drawing.Size(13, 13);
            this._secondIOStepLed.State = BEMN.Forms.LedState.Off;
            this._secondIOStepLed.TabIndex = 2;
            // 
            // _errorBkFoundedLed
            // 
            this._errorBkFoundedLed.Location = new System.Drawing.Point(6, 55);
            this._errorBkFoundedLed.Name = "_errorBkFoundedLed";
            this._errorBkFoundedLed.Size = new System.Drawing.Size(13, 13);
            this._errorBkFoundedLed.State = BEMN.Forms.LedState.Off;
            this._errorBkFoundedLed.TabIndex = 10;
            // 
            // _errorChainKvitLed
            // 
            this._errorChainKvitLed.Location = new System.Drawing.Point(224, 74);
            this._errorChainKvitLed.Name = "_errorChainKvitLed";
            this._errorChainKvitLed.Size = new System.Drawing.Size(13, 13);
            this._errorChainKvitLed.State = BEMN.Forms.LedState.Off;
            this._errorChainKvitLed.TabIndex = 17;
            // 
            // _errorBKKvitLed
            // 
            this._errorBKKvitLed.Location = new System.Drawing.Point(224, 55);
            this._errorBKKvitLed.Name = "_errorBKKvitLed";
            this._errorBKKvitLed.Size = new System.Drawing.Size(13, 13);
            this._errorBKKvitLed.State = BEMN.Forms.LedState.Off;
            this._errorBKKvitLed.TabIndex = 11;
            // 
            // _errorChainLed
            // 
            this._errorChainLed.Location = new System.Drawing.Point(6, 74);
            this._errorChainLed.Name = "_errorChainLed";
            this._errorChainLed.Size = new System.Drawing.Size(13, 13);
            this._errorChainLed.State = BEMN.Forms.LedState.Off;
            this._errorChainLed.TabIndex = 16;
            // 
            // Mr100MeasuringForm
            // 
            this.ClientSize = new System.Drawing.Size(661, 337);
            this.Controls.Add(this._versionGroup);
            this.Controls.Add(this._avariaGroup);
            this.Controls.Add(this._falgsGroup);
            this.Controls.Add(this._analogGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mr100MeasuringForm";
            this.Text = "Состояние устройства";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._analogGroup.ResumeLayout(false);
            this._analogGroup.PerformLayout();
            this._falgsGroup.ResumeLayout(false);
            this._falgsGroup.PerformLayout();
            this._avariaGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this._versionGroup.ResumeLayout(false);
            this._versionGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        private Label _d2Label;
        private LedControl _d2Led;
        private Label _relayLabel;
        private LedControl _relayLed;
        private Label label7;
        private TextBox _inIminus;
        private Label minus;
        private Label plus;
    }
}
