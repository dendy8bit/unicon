﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR100.Measuring.Structures
{
    public class NominalStruct : StructBase
    {
        [Layout(0)] private ushort _nominal1;
        [Layout(1)] private ushort _nominal2;
        [Layout(2)] private ushort _uShunt;


        public double BaseNominal
        {
            get
            {
                return ValuesConverterMr100.NominalConverterDirect(Common.TOBYTES(new[] { _nominal1, _nominal2 }, true)) / 10;
            }
            set
            {
                byte[] buffer = ValuesConverterMr100.NominalConverterInvert((float)(value * 10));
                var mass = Common.TOWORDS(buffer, true);
                _nominal1 = mass[0];
                _nominal2 = mass[1];
            }
        }

        public double Nominal
        {
            get
            {
                if (Common.GetBit(_uShunt, 0))
                {
                    return BaseNominal * 1.25;
                }
                return BaseNominal;
            }
        }
    }
}
