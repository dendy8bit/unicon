﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR100.Measuring.Structures
{
    public  class JournalStruct : StructBase
    {
        [Layout(0, Count = 4)] private MessageStruct[] _messages;

        public MessageStruct[] Messages
        {
            get { return _messages; }
        }
    }
}
