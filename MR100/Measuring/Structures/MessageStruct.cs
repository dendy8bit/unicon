﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR100.Measuring.Structures
{
   public class MessageStruct:StructBase
   {
       [Layout(0)] private ushort _code ;
       [Layout(1)] private ushort _parametr;

       public string Code(double version)
       {
           if (version >= 5.40)
           {
               
           }
           return Validator.GetJornal(_code, Codes); 
       }

       public string GetParametr(double nominal)
       {
           if ((_code==1)||(_code==2))
           {
              return String.Format("{0:F2} А", ValuesConverterMr100.MeasuringConverter(_parametr, nominal * 10));
           }
           if (_code == 3)
           {
               return string.Empty;
           }
           if ((_code == 4)||(_code == 5)||(_code == 6))
           {
               return string.Empty;
           }
           return _parametr.ToString();
       }

       private static List<string> Codes
       {
           get
           {
               return new List<string>
                   {
                       "0",
                       "Сработала 1-ая ступень",
                       "Сработала 2-ая ступень",
                       "Ошибка отключения",
                       "Выполнено квитирование",
                       "Автомат отключен",
                       "Выполнен сброс"
                   };
           }
       }
   }
}
