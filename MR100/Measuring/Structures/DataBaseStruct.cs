﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR100.Measuring.Structures
{
    public class DataBaseStruct : StructBase
    {
        [Layout(0)] private ushort _iInPlus;
        [Layout(1)] private ushort _uSpool;
        [Layout(2)] private ushort _uBk1;
        [Layout(3)] private ushort _iInMinus;
        [Layout(4)] private ushort _uBk2;
        [Layout(5, Count = 11)] private ushort[] _resArr;
        [Layout(6)] private ushort _flags;

        public double GetInPlus(double nominal)
        {
            return ValuesConverterMr100.MeasuringConverter(this._iInPlus, nominal*10);
        }

        public double GetInMinus(double nominal)
        {
            return ValuesConverterMr100.MeasuringConverter(this._iInMinus, nominal * 10);
        }

        public double USpool
        {
            get { return ValuesConverterMr100.MeasuringConverter(USpoolNativ, 250); }
        }
        
        public double UBk1
        {
            get { return ValuesConverterMr100.MeasuringConverter(UBk1Nativ, 250); }
        }

        public double UBk2
        {
            get { return ValuesConverterMr100.MeasuringConverter(UBk2Nativ, 250); }
        }

        public BitArray Flags
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_flags), Common.HIBYTE(_flags)}); }
        }

        public ushort InPlus
        {
            get { return _iInPlus; }
        }

        public ushort InMinus
        {
            get { return _iInMinus; }
        }

        public ushort USpoolNativ
        {
            get { return _uSpool; }
            set { _uSpool = value; }
        }

        public ushort UBk1Nativ
        {
            get { return _uBk1; }
            set { _uBk1 = value; }
        }

        public ushort UBk2Nativ
        {
            get { return _uBk2; }
            set { _uBk2 = value; }
        }
    }
}
