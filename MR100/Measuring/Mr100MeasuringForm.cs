using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using BEMN.MBServer;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR100.Measuring
{
    public partial class Mr100MeasuringForm : Form ,IFormView
    {
        [Browsable(false)]
        public Type ClassType
        {
            get { return GetType(); }
        }

        private Mr100Device _device;
        private LedControl[] _leds;

        public Mr100MeasuringForm()
        {
            this.InitializeComponent();
        }

        public Mr100MeasuringForm(Mr100Device device)
        {
            this.InitializeComponent();

            this._device = device;
            this.InitializeLeds();
            this._device.ConnectionModeChanged += this.StartLoad;
            this._device._versionFail += HandlerHelper.CreateHandler(this, this.ClearVersion);
            this._device.Nominal1.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.Db.LoadStruct();
                this._device.JournalS.LoadStruct();
            });
            this._device.Nominal1.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ReadNominalFail);
            this._device.Db.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadDbOk);
            this._device.Db.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ReadDbFail);
            this._device.JournalS.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);
            this._device.JournalS.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalFail);
            this._versionBox.Text = this._device.Info.Version;
            this._serialBox.Text = this._device.Info.Plant;
        }

        private void ReadNominalFail()
        {
            this._journalGrid.Rows.Clear();
            this.ReadDbFail();
        }

        private void ReadJournalFail()
        {
            this._journalGrid.Rows.Clear();
        }

        private void ReadJournalOk()
        {
            this._journalGrid.Rows.Clear();
            var journal = this._device.JournalS.Value;
            foreach (var message in journal.Messages)
            {
                this._journalGrid.Rows.Add
                    (
                        message.Code(Common.VersionConverter(this._device.DeviceVersion)),
                        message.GetParametr(this._device.Nominal1.Value.Nominal)
                    );
            }
        }

        private void ReadDbFail()
        {
            this._inIplus.Text = "";
            this._uSpoolBox.Text = "";
            this._uBk1Box.Text = "";
            this._uBk2Box.Text = "";
            LedManager.TurnOffLeds(this._leds);
        }

        private void ReadDbOk()
        {
            this._inIplus.Text = string.Format("{0:F2} �", this._device.Db.Value.GetInPlus(this._device.Nominal1.Value.Nominal));
            this._inIminus.Text = string.Format("{0:F2} �", this._device.Db.Value.GetInMinus(this._device.Nominal1.Value.Nominal));
            this._uSpoolBox.Text = string.Format("{0:F2} �", this._device.Db.Value.USpool);
            this._uBk1Box.Text = string.Format("{0:F2} �", this._device.Db.Value.UBk1);
            this._uBk2Box.Text = string.Format("{0:F2} �", this._device.Db.Value.UBk2);
            LedManager.SetLeds(this._leds, this._device.Db.Value.Flags);
        }



        private void InitializeLeds()
        {
            this._leds = new[]
            {
                this._firstStepIOLed, this._firstStepLed, this._secondIOStepLed, this._secondStepLed,
                this._levelActiveLed, this._levelWorkLed, this._errorBkFoundedLed, this._errorBKKvitLed,
                this._errorChainLed, this._errorChainKvitLed, this._errorACP, this._errorValuesCRC,
                this._errorCRCProgramLed, this._automatLedOff, this._d2Led, this._relayLed
            };
            if (Common.VersionConverter(this._device.DeviceVersion) < 7.0)
            {
                this.plus.Visible = false;
                this.minus.Visible = false;
                this._inIminus.Visible = false;
            }
            if (Common.VersionConverter(this._device.DeviceVersion) >= 5.30)
            {
                this._automatLedOff.Visible = true;
                this._lblAutomatOff.Visible = true;
                this._d2Led.Visible = true;
                this._d2Label.Visible = true;
                
                this._errorBkFoundedLed.Visible = false;
                this._errorBkFoundedLabel.Visible = false;
                this._errorBKKvitLed.Visible = false;
                this._errorBKKvitLabel.Visible = false;

                this._relayLed.Visible = true;
                this._relayLabel.Visible = true;
            }
            else
            {
                this._automatLedOff.Visible = false;
                this._lblAutomatOff.Visible = false;
                this._d2Led.Visible = false;
                this._d2Label.Visible = false;

                this._errorBkFoundedLed.Visible = true;
                this._errorBkFoundedLabel.Visible = true;
                this._errorBKKvitLed.Visible = true;
                this._errorBKKvitLabel.Visible = true;

                this._relayLed.Visible = false;
                this._relayLabel.Visible = false;
            }
        }
        
        public void ClearVersion()
        {
            this._versionBox.Text = "";
            this._serialBox.Text = "";
        }

        public void GetVersion()
        {
            this._versionBox.Text = this._device.Info.Version;
            this._serialBox.Text = this._device.Info.Plant; 
        }
        
                   
       #region IFormView
       public Type FormDevice
       {
          get
          {
            return typeof(Mr100Device);
          }
       }

        public bool Multishow { get; private set; }

        public bool Deletable
        {
            get
            {
                return false;
            }
        }

        public bool ForceShow
        {
            get
            {
                return false;
            }
        }

       
      public string NodeName
      {
         get
         {
            return "��������� ����������";
         }
      }

        public INodeView[] ChildNodes
        {
            get
            {
                return null;
            }
        }

        public Type ThisType
        {
            get
            {
                return GetType();
            }
        }

      public Image NodeImage
      {
         get
         {
             return Resources.measuring.ToBitmap();
         }
      }
        public bool IsMustBeAdded(object parentTag)
        {
            return true;
        }
  
        #endregion


        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.Nominal1.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartLoad;
        }

        private void _kvitBut_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._device.Kvit();
        }
        
        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartLoad();
        }

        private void StartLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Nominal1.LoadStructCycle();
            }
            else
            {
                this._device.Nominal1.RemoveStructQueries();
                this.ReadNominalFail();
            }
        }
    }
}