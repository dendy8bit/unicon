using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using Microsoft.Win32;
using System.Configuration.Install;
using System.Runtime.InteropServices;
using System.IO;

namespace BEMN.Framework
{   
    [RunInstaller(true)]
    public class UniconInstaller : Installer 
    {
     
        const string keyName = "Software\\BEMN\\Unicon";

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            string rootDir = Path.GetDirectoryName(Context.Parameters["ARGS"]);
            string prjDir = rootDir + "/Projects";

            if (!string.IsNullOrEmpty(rootDir))
            {
                string cachePath = Path.Combine(rootDir, "cache.xml");
            if (File.Exists(cachePath))
                File.Delete(cachePath);

            string settingsPath = Path.Combine(rootDir, "Framerowk.settings");
            if (File.Exists(settingsPath))
                File.Delete(settingsPath);
            }
            
            if (Directory.Exists(prjDir))
            {
                string[] prjFiles = Directory.GetFiles(prjDir);
                
                if (0 != prjFiles.Length)
                {
                    string msg = "Delete project files \n";
                    for (int i = 0; i < prjFiles.Length; i++)
                    {
                        msg += prjFiles[i];
                        msg += (i == prjFiles.Length  - 1 ) ? "?" : "\n";
                    }
                    
                    if (DialogResult.Yes == MessageBox.Show(msg, "", MessageBoxButtons.YesNo))
                    {
                        for (int i = 0; i < prjFiles.Length; i++)
                        {
                            File.Delete(prjFiles[i]);
                        }
                    }
                }
            }
            base.Uninstall(savedState);
        }
        protected override void OnBeforeInstall(System.Collections.IDictionary savedState)
        {
            base.OnBeforeInstall(savedState);
            
            RegistryKey uniconKey = Registry.CurrentUser.OpenSubKey(keyName,true);
            string code = this.Context.Parameters["Code"];
            bool desktopFlag =  ("1" == this.Context.Parameters["DESKTOP"]);
            bool menuFlag = ("1" == this.Context.Parameters["MENU"]);
                   
            if (!desktopFlag)
            {
                RemoveDesktopShortcuts(code);
            }
        
            if (!menuFlag)
            {
                string menuFolder = Environment.GetFolderPath(Environment.SpecialFolder.Programs);
                if (Directory.Exists(menuFolder + "/BelEMN"))
                {
                    Directory.Delete(menuFolder + "/BelEMN", true);    
                }
                
            }
      
            if (null != uniconKey)
            {                
                string[] versionKeys = uniconKey.GetSubKeyNames();
                if (0 != versionKeys.Length)
                {
                     foreach (string  versionKey in versionKeys)
                     {
                        if (versionKey != code)
                        {
                            if (DialogResult.Yes == MessageBox.Show("Delete previos version ?", "", MessageBoxButtons.YesNo))
                            {
                                string env = System.Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.User);
                                string fname = "Unicon_" + DateTime.Now.ToFileTime().ToString() + ".bat";
                                System.IO.TextWriter writer = new System.IO.StreamWriter(env + "/" + fname);
                                writer.WriteLine("Msiexec /x " + versionKey + " /quiet");
                                writer.Close();
                                System.Diagnostics.Process.Start(env + "//" + fname);
                                uniconKey.DeleteSubKey(versionKey);
                            }
                        }
                    }
                }
                               
                uniconKey.CreateSubKey(code);
            }
            else
            { 
                Registry.CurrentUser.CreateSubKey(keyName).CreateSubKey(this.Context.Parameters["Code"]);
            }
            
        }

        private static void RemoveDesktopShortcuts(string code)
        {
            string desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string[] shortcuts = Directory.GetFiles(desktopFolder);
            for (int i = 0; i < shortcuts.Length; i++)
            {
                TextReader reader = new StreamReader(shortcuts[i]);

                if (reader.ReadToEnd().Contains(code))
                {
                    reader.Close();
                    File.Delete(shortcuts[i]);
                }
            }
        }
    }
}
