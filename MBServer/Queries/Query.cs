using System;
using System.Linq;
using BEMN.Interfaces;
using BEMN.MBServer.Exceptions;

namespace BEMN.MBServer.Queries
{
    /// <summary>
    /// ����� Query ������ ��� ������������� ������� 
    /// � ����������.  
    /// </summary>
    /// <remarks>
    /// ����� Query ������������ ��������� � ������� Modbus <see cref="Modbus"/>. ����� ��������
    /// �����������, ����� ����������� �������,������� ����� �������������� � �������-����������� :
    /// ToBytes(), CheckQuery() <see cref="CheckQuery"/>� ��.
    /// ��� ������������� ������� ���������� ��������� ��������� ���� : 
    /// <list type="bullet">
    /// <item> 
    /// <description>dataStart <see cref="dataStart"/></description>
    /// </item>
    /// <item>
    /// <description>device <see cref="device"/></description>
    /// </item>
    /// <item>
    /// <description>func <see cref="func"/></description>
    /// </item>
    /// <item>
    /// <description>globalSize <see cref="globalSize"/> ��� localSize <see cref="localSize"/> </description>
    /// </item>
    /// <item>
    /// <description>name - <see cref="name"/> - ���� �������� ����� ����� ���������. </description>
    /// </item>
    /// <item>
    /// <description> writeBuffer <see cref="writeBuffer"/> - ���� ������ ������ ���-�� ��������.</description>
    /// </item>
    /// </list>
    /// </remarks>
    /// <example>
    /// 1.������� - ����� ������� 100 ���� �� ������ 0x0000,� ���������� 0x40.
    /// ������ ������������� ������ ������, �.�. �� ����� ������� ����� ��������.
    /// <code>
    /// Query q = new Query();
    /// q.dataStart = 0;
    /// q.device = 0x40;
    /// q.func = 3; //������� Modbus ��� ������ ����.
    /// q.localSize = 100;
    /// q.name = "someName";
    /// </code>
    /// 2. ������� ������ � �������� 1, �� ������ ������������� ������� �� 10 ����,�� 1 ������ ����� 
    /// ������� ������ 1 ����.
    /// <code>
    /// Query q = new Query();
    /// q.dataStart = 0;
    /// q.device = 0x40;
    /// q.func = 3; //������� Modbus ��� ������ ����.
    /// q.localSize = 10;
    /// q.globalSize = 100;
    /// q.name = "someName";
    /// </code>
    /// 3. ������� - ������ ������������� �� ����.����� 2 ����� ������ - 5 ���� ��� - 2 ����� ������....
    /// ����� ������ 5 ������ ���������� � ������ ���� ������ {0xFFFF,0xAAAA}. � ������ ������ ������
    /// ������ ������������.  ����� ���������� - 0x01, ����� ������ ������ 0x0020. ����� ����� ��������
    /// 20 ����.
    /// <code>
    /// Query q = new Query();
    /// q.dataStart = 0x0020;
    /// q.device = 0x01;
    /// q.func = 16;	// ������� Modbus ��� ������ ����;
    /// q.localSize = 2;	// �� 1 ������ ���������� 2 �����;
    /// q.globalSize = 20;
    /// q.name = "someName";
    /// q.jump = 5;	// ����� ����� ���������� 5 ���� ����;
    /// q.bCycle = true; //������ ����� �����������;
    /// q.raiseSpan = new TimeSpan(5000); // 5000 ms = 5� - ������ ������� �������;
    /// q.bDeleteOnError = true; // � ������ ������ ������ ������ ���������;
    /// q.writeBuffer = new byte[0xFFFF,0xAAAA];
    /// </code>
    /// </example>
    public class Query
    {
        #region Fields
        /// <summary>
        /// ����� ����������.
        /// </summary>
        /// <remarks>
        /// ����� ���������� ������ �������� ����������� ��������� Modbus:
        /// �������� 1..255, 0 - ����������������� �������.
        /// </remarks>
        public byte device;
        /// <summary>
        /// ��������� ����� ������/������.
        /// </summary>
        /// <remarks>
        /// ����� � �������� ����� ������������� ������/������. ����� �������������
        /// ������������ �� �������������. ������������ WriteOnly.
        /// </remarks>
        public ushort dataStart { get; set; }
        /// <summary>
        /// �������� �������� ��� �/�.
        /// </summary>
        /// <remarks>
        /// ����� ��������� ���� ������ ���� �� �������� ������, � ����������������
        /// ��� ����. �.�. : ������-�����-������-�����. 
        /// ���� �������� ������ ���� ����� ����� ������ ����� "�����".
        /// </remarks>
        public ushort jump;
        /// <summary>
        /// ��������� �������.
        /// </summary>
        /// <remarks>
        /// ��������� ������� ��������� ����������� ��� ��������� � �������.
        /// ��� ������ ���������, ��� ������� ������ ������� �� ��������� �������.
        /// �������� �������� 0..65535. �� ��������� ����� 0. ���� ������ �������� � �������,
        /// �� �������� ��� ��������� ������, ��� ����� ������� � �������� ������ � 
        /// ������ �����������.
        /// </remarks>
        public ushort priority;
        /// <summary>
        /// ���� ��������� �������.
        /// </summary>
        /// <remarks>
        /// ���� true, �� ������ �� ��������������.
        /// �� ��������� false.
        /// </remarks>
        public bool bSuspend;
        /// <summary>
        /// ���� ����������� �������.
        /// </summary>
        /// <remarks>
        /// ���� true,�� ������ ���������. ���������� ������ �������� ������������
        /// <see cref="raiseSpan"/>
        /// �� ��������� false.
        /// </remarks>
        public bool bCycle;
        /// <summary>
        /// ���� �������� �������.
        /// </summary>
        /// <remarks>
        ///  ���� true, �� ��������� �������� �������. ��� ���������� �����
        ///  �������, ������ ����� ������������� ������. ����������� ����� ������
        ///  ������, ��� ��� ������ ������� ����� ���� ������������ ���������� ���������.
        ///  �� ��������� false.
        ///  </remarks>
        public bool bDelete;
        /// <summary>
        /// ���� ��������������� �������� ������� ��� ��������� �������.
        /// </summary>
        /// <remarks>
        /// ���� true, �� ������ ����� ������ �� ������� ��� ���� �� 1 ��������� 
        /// ������.
        /// �� ��������� false.
        /// </remarks>
        public bool bDeleteOnError;
        /// <summary>
        /// ���� �������������� ��������� ���������� �������.
        /// </summary>
        /// <remarks>
        /// ��� ����������� �������������. ��������� 
        /// �������� ����������� ������� �� ����������������. 
        /// �� ��������� �� ���������. 
        /// </remarks>
        public bool bSuspendByCycle;
        /// <summary>
        /// ������ ��������(������������) �� 1 �����.
        /// </summary>
        public ushort localSize;

        /// <summary>
        /// ������ ��������(������������) �� ��� ������.
        /// </summary>
        /// <remarks>
        /// ������ ������ ��������� 64k�. �� ����� ���� 0.
        /// ������ ���� ������ <see cref="localSize"/>
        /// </remarks>
        public ushort globalSize { get; set; }

        /// <summary>
        /// H���� �-��� Modbus
        /// </summary>
        /// <remarks>
        /// � ��������� ������ ��������������:
        /// <list type="bullet">
        /// <item>
        /// <description> 3,4 - ������ �����.</description>
        /// </item>
        /// <item>
        /// <description> 16 - ������ �����.</description>
        /// </item>
        /// </list>
        /// </remarks>
        public byte func;
        /// <summary>
        /// ����� ������.
        /// </summary>
        /// <remarks>
        /// � ���� ���������� ����������� ������. ������ ��������� 64��.
        /// </remarks>
        public byte[] readBuffer;
        /// <summary>
        /// ����� ������.
        /// </summary>						
        /// <remarks>
        /// ������������ ������ � �-����� ������ Modbus - 16.
        /// </remarks>
        public ushort[] writeBuffer;
        /// <summary>
        /// ��� �������
        /// </summary>
        /// <remarks>
        /// ���������� ��� �������. ���������� ��� ������������ ���������,
        /// ������ ���������, ������� �������� �������, ��� ��������� ����������� ������
        /// (���� ������ ����� ����������, �� ��� ������ ����� �������� �� ���������). 
        /// ��� �-��� ������ <see cref="func"/> ������ �� ������������.
        /// </remarks>
        public string name;
        /// <summary>
        /// ����� �������� �������.
        /// </summary>
        /// <remarks>
        /// ����������� �������������, �� ����� ���������� ������� � �������.
        /// </remarks>
        public DateTime creationTime;
        /// <summary>
        /// �������� ������� ���������� �������.
        /// </summary>
        /// <remarks>
        /// �� ��������� 0. ������������ ������ ��� ��������� ��������. <see cref="bCycle"/>
        /// </remarks>
        public TimeSpan raiseSpan;
        /// <summary>
        /// ���-�� ���������� �������.
        /// </summary>
        public ulong ok;
        /// <summary>
        /// ���-�� ��������� �������.
        /// </summary>
        public ulong fail;
        /// <summary>
        /// ���� ���������� ������.
        /// </summary>
        public bool error;
        /// <summary>
        /// ���-�� ���� �������.
        /// </summary>
        public ulong all;
        /// <summary>
        /// ����� ����������/����������� ����.
        /// </summary>
        public ushort loadedBytes;

        /// <summary>
        /// ��� ����������� �������������
        /// </summary>
        public ushort remainder;
        /// <summary>
        /// ��� ������� � ������ - �����
        /// </summary>
        public byte module;
        /// <summary>
        /// ��� ������� � ������ - ����� �������
        /// </summary>
        public byte moduleFunc;
        /// <summary>
        /// ���� ��� ��������� ������ ������� ���
        /// </summary>
        public bool isReadBits;
        /// <summary>
        /// ������ �� ������ �������
        /// </summary>
        public object deviceObj;
        /// <summary>
        /// ���������� �������� �������� ��������� ��� ��������� �������
        /// </summary>
        public int countOfMaxRequest;
        /// <summary>
        /// ����������� ������� ����� �������� �����-������
        /// </summary>
        public string errMessage;

        #endregion	Fields

        #region Exceptions
        /// <exception cref="System.Exception"> ������ ������� ��������� ����� �/� ����.</exception>
        public ApplicationException eWrongLocalSize;
        /// <exception cref="System.Exception"> ������ - ������� ������/������ ������� ���-�� ����.</exception>
        public ApplicationException eNullGlobalSize;
        /// <exception cref="System.Exception"> ������ - �������������� �-��� Modbus.</exception>
        public ApplicationException eUnknownFunc;
        /// <exception cref="System.Exception"> ������ - ������� ����� ������.</exception>
        public ApplicationException eNullBuffer;
        /// <exception cref="System.Exception"> ������ - ����� ������ ������� ���.</exception>
        public ApplicationException eSmallBuffer;
        #endregion Exceptions

        #region Methods

        /// <summary>
        /// �����������.
        /// </summary>
        public Query()
        {
            eNullGlobalSize = new ApplicationException("���������� ���� ������ ������� ��� �� ������.");
            eWrongLocalSize = new ApplicationException("��������� � ���������� ����� �� �������.");
            eUnknownFunc = new ApplicationException("����������� �������.");
            eNullBuffer = new ApplicationException("����� ������ �� ����� ���� ������.");
            eSmallBuffer = new ApplicationException("����� ������ ������� ���.");

            creationTime = DateTime.Now;
            raiseSpan = new TimeSpan(0, 0, 0);
            dataStart = localSize = globalSize = jump = priority = loadedBytes = 0;
            device = func = 0;
            ok = fail = all = 0;
            bSuspendByCycle = bCycle = bSuspend = bDelete = false;
            bDeleteOnError = isReadBits = false;
            deviceObj = null;
            countOfMaxRequest = 5;
        }
        /// <summary>
        /// ������� ����� ��� ������
        /// </summary>
        /// <returns>�����</returns>
        public byte[] CreateWriteBuffer()
        {
            byte[] arr = Common.TOBYTES(this.writeBuffer, true);
            int start = this.loadedBytes;
            int end = this.loadedBytes + this.localSize * 2;
            byte[] ret = new byte[end - start];
            for (int i = start; i < end; i++)
            {
                if (i < arr.Length)
                {
                    ret[i - start] = arr[i];
                }
            }
            return ret;
        }
        private byte[] CreateWriteWordRequest(byte[] buffer)
        {
            byte[] request = new byte[8];
            request[0] = this.device;
            request[1] = this.func;
            request[2] = Common.HIBYTE(this.dataStart);
            request[3] = Common.LOBYTE(this.dataStart);
            request[4] = buffer[0]; //Common.HIBYTE(query.localSize);
            request[5] = buffer[1];//buffer[1]; //Common.LOBYTE(query.localSize);
            
            ushort crc = CRC16.CalcCrcFast(request, request.Length - 2);
            request[6] = Common.HIBYTE(crc);
            request[7] = Common.LOBYTE(crc);

            return request;
        }

        private byte[] CreateWriteWordsRequest(byte[] buffer)
        {
            byte[] request = new byte[9 + this.localSize * 2];
            request[0] = this.device;
            request[1] = this.func;
            request[2] = Common.HIBYTE(this.dataStart);
            request[3] = Common.LOBYTE(this.dataStart);
            request[4] = Common.HIBYTE(this.localSize);
            request[5] = Common.LOBYTE(this.localSize);
            request[6] = (byte)(this.localSize * 2);
            //���� �����-�� ����� ���������� �������� ������� ������� ����� ������,
            //(����� ������� ������������ query.localSize)
            if (buffer.Length >= request.Length)
            {
                for (int i = 0; i < request[6]; i++)
                {
                    request[i + 7] = buffer[i];
                }
            }
            else
            {
                buffer.CopyTo(request, 7);
            }
            ushort crc = CRC16.CalcCrcFast(request, request.Length - 2);
            request[request.Length - 2] = Common.HIBYTE(crc);
            request[request.Length - 1] = Common.LOBYTE(crc);

            return request;
        }

        private byte[] CreateSetBitRequest()
        {
            byte[] request = new byte[8];
            request[0] = this.device;
            request[1] = this.func;
            request[2] = Common.HIBYTE(this.dataStart);
            request[3] = Common.LOBYTE(this.dataStart);
            request[4] = Common.LOBYTE(this.writeBuffer[0]);
            request[5] = 0;
            ushort crc = CRC16.CalcCrcFast(request, 6);
            request[6] = Common.HIBYTE(crc);
            request[7] = Common.LOBYTE(crc);
            return request;
        }

        /// <summary>
        /// ������� ������� � ����.
        /// </summary>
        /// <returns> ���������� �������</returns>
        /// <remarks>
        /// ������� �� ��������� ������������ ������, �������� � �������. 
        /// ������� ������ ����� ������ 8 ����. CRC �������������� ������� <see cref="CRC16"/>
        /// </remarks>
        private byte[] CreateReadWordsRequest()
        {
            byte[] request = new byte[8];
            request[0] = this.device;
            request[1] = this.func;
            request[2] = Common.HIBYTE(this.dataStart);
            request[3] = Common.LOBYTE(this.dataStart);
            request[4] = Common.HIBYTE(this.localSize);
            request[5] = Common.LOBYTE(this.localSize);
            ushort crc = CRC16.CalcCrcFast(request, 6);
            request[6] = Common.HIBYTE(crc);
            request[7] = Common.LOBYTE(crc);
            return request;
        }
        
        /// <summary>
        /// ����������� ������ � ���� ������� ������
        /// </summary>
        /// <returns>�����</returns>
        public byte[] ToBytes()
        {
            byte[] buffer;

            switch (func)
            {
                case 0x01:
                case 0x02:
                    buffer = CreateReadWordsRequest();
                    break;
                case 0x03:
                case 0x04:
                    buffer = CreateReadWordsRequest();
                    break;
                case 0x05:
                    buffer = CreateSetBitRequest();
                    break;
                case 0x06:
                    buffer = CreateWriteWordRequest(CreateWriteBuffer());
                    break;
                case 0x10:
                    buffer = CreateWriteWordsRequest(CreateWriteBuffer());
                    break;
                case 0x12:
                    buffer = CreateModuleRequest();
                    break;
                default:
                    throw this.eUnknownFunc;
            }

            return buffer;
        }

        private byte[] CreateModuleRequest()
        {
            byte[] request = new byte[7];
            request[0] = this.device;
            request[1] = this.func;
            request[2] = this.module;
            request[3] = this.moduleFunc;
            request[4] = Common.LOBYTE(this.localSize);
            ushort crc = CRC16.CalcCrcFast(request, 5);
            request[5] = Common.HIBYTE(crc);
            request[6] = Common.LOBYTE(crc);
            return request;
        }

        /// <summary>
        /// CheckQuery. �������� ������������� ������� �� ������. ��� ������ 
        /// ��������� ����������.
        /// </summary>
        /// <remarks>
        /// ����������� ����.������ :
        /// <list type="bullet">
        /// <item>
        /// <description>
        ///  ������ ������� ��������� ����� �/� ����
        /// </description>
        /// </item>
        /// <item>
        /// <description>
        ///  ������ - ������� ������/������ ������� ���-�� ����.
        /// </description>
        /// </item>
        /// <item>
        /// <description>
        ///   ������ - �������������� �-��� Modbus.
        /// </description>
        /// </item>
        /// <item>
        /// <description>
        ///   ������ - ������� ����� ������.
        /// </description>
        /// </item>
        /// <item>
        /// <description>
        ///  ������ - ����� ������ ������� ���.
        /// </description>
        /// </item>
        /// </list>
        /// /// </remarks>
        public void CheckQuery()
        {
            if (null == this.name)
            {
                throw new ApplicationException("��� ������� 0");
            }
            if (0 == this.localSize)
            {
                this.localSize = this.globalSize;
            }

            if (0x12 != this.func && 0x05 != this.func && 0 == this.globalSize)
            {
                throw this.eNullGlobalSize;
            }

            if (0x12 != this.func && 0x05 != this.func && 0 != (this.globalSize%this.localSize))
            {
                this.remainder = (ushort) (this.globalSize%this.localSize);
            }

            if (0x10 == this.func)
            {
                if (null == this.writeBuffer)
                {
                    throw this.eNullBuffer;
                }
                if (this.writeBuffer.Length < this.globalSize/this.localSize)
                {
                    throw this.eSmallBuffer;
                }
            }

            if (0x04 == this.func || 0x12 == this.func)
            {
                this.readBuffer = new byte[this.globalSize*2];
            }
            if (0x01 == this.func || 0x02 == this.func)
            {
                this.readBuffer = new byte[this.globalSize*2];
                this.localSize = (ushort) (this.globalSize*16);
            }
            //�����-�� ������������� ��������
            if ((0x01 == this.func || 0x02 == this.func) && (this.localSize%8) != 0)
            {
                throw new ApplicationException("���� ����� ������ ������ ������ 8.");
            }

            if (0x05 == this.func && this.writeBuffer.Length != 1)
            {
                throw new SetBitWriteBufferException("��� ��������� ���� writeBuffer ������ ��������� '1' ��� '0'");
            }
        }

        /// <summary>
        /// ������������� ������ ���� ������ ������ ����� ������ ����������
        /// </summary>
        /// <param name="answer">������ ���� ������ �� ����������</param>
        /// <remarks>
        /// ������� �� ����������� ���� ����� answer ������ ��� ����� 5. 
        /// ����� ��� "�����������" ������ �� ������. �.�. ����� ������ �����
        /// </remarks>
        public void SetReadBitsBuffer(byte[] answer)
        {
            if (answer == null || answer.Length <= 5) return;
            if ((answer.Length - 5)*8 != this.localSize) return;
            for (int i = 3; i < answer.Length - 2; i++)
            {
                this.readBuffer[i - 3] = answer[i];
            }
            Common.SwapArrayItems(ref this.readBuffer);
        }
        /// <summary>
        /// ��������� ����� ������ �������.
        /// </summary>
        /// <param name="answer"> ����� ��������� �� �����.</param>
        /// <remarks>
        /// ������� �� ����������� ���� ����� answer ������ ��� ����� 5. 
        /// ����� ��� "�����������" ������ �� ������. �.�. ����� ������ �����
        /// 01 04 01 AA FF CRC CRC, ��� 
        /// <list type="bullet">
        /// <item>
        /// <description> 01 - ����� ���������� </description>
        /// </item>
        /// <item>
        /// <description> 04 - ����� ������� Modbus </description>
        /// </item>
        /// <item>
        /// <description> AAFF - ����� ����������� �� ���������� </description>
        /// </item>
        /// <item>
        /// <description> CRCCRC - CRC ������</description>
        /// </item>
        /// </list>
        /// � ���� ������ � ����� ������ ������� <see cref="Query.readBuffer"/> ����������� ������ ����������,
        /// AAFF.
        /// </remarks>
        public void SetReadWordsBuffer(byte[] answer)
        {
            if (answer == null || answer.Length <= 5) return;

            if (answer.Length - 5 == this.readBuffer.Length && this.readBuffer.Length <= 128)
            {
                for (int i = 3; i < answer.Length - 2; i++)
                {
                    this.readBuffer[i - 3] = answer[i];
                }
            }
            else if (answer.Length - 6 == this.readBuffer.Length && this.readBuffer.Length > 128)
            {
                for (int i = 4; i < answer.Length - 2; i++)
                {
                    this.readBuffer[i - 4] = answer[i];
                }
            }
            else
            {
                this.readBuffer = new byte[answer.Length - 5];
                for (int i = 3; i < answer.Length - 2; i++)
                {
                    this.readBuffer[i - 3] = answer[i];
                }
            }
        }
        /// <summary>
        /// ������� ���������� ��������� ���� 3� � 4� �������
        /// </summary>
        /// <param name="answer">����� ����������</param>
        public void CalcLoadedBytes3H(byte[] answer)
        {
            if (answer == null) return;
            if (answer.Length > 5 && answer.Length <= 128 + 5)
            {
                this.loadedBytes += (ushort) (answer.Length - 5);
            }
            else
            {
                this.loadedBytes += (ushort)(answer.Length - 6);
            }
        }
        /// <summary>
        /// ������� ���������� ��������� ���� ��� 16� �������
        /// </summary>
        /// <param name="answer">����� ����������</param>
        public void CalcLoadedBytes16H(byte[] answer)
        {
            if (answer == null) return;
            if (answer.Length >= 6)
            {
                this.loadedBytes += (ushort)(Common.TOWORD(answer[4], answer[5]) * 2);
            }
        }
        /// <summary>
        /// ��������� ������� ������ �������. ���������� true, ���� ��� ������.
        /// �������� ����������� � ����������� �������� �����-������.
        /// </summary>
        /// <param name="answer">������ ���� ������ �� ������</param>
        /// <returns></returns>
        public bool CheckError(byte[] answer)
        {
            this.all++;
            this.errMessage = string.Empty;
            if (answer == null)
            {
                this.fail++;
                this.errMessage = "���������� �� ��������";
                return false;
            }
            if (answer.Length < 2)
            {
                this.fail++;
                this.errMessage = "����� �������� �����";
                return false;
            }
            if (this.device != answer[0] || this.func != answer[1])
            {
                this.fail++;
                this.errMessage = "������ ����� - ������";
                return false;
            }
            if ((this.func == 0x01 || this.func == 0x02) && (this.localSize / 8 != answer.Length - 5))
            {
                this.fail++;
                this.errMessage = "������ ������ n ���";
                return false;
            }
            if (this.func == 0x03 || this.func == 0x04)
            {
                if (answer.Length > 259) // �������� �� "�������" ������� ������
                {
                    ushort len;
                    IDeviceInfo dev = this.deviceObj as IDeviceInfo;
                    double version = Common.VersionConverter(dev.DeviceVersion);
                    if ((dev.DeviceType == "MR771" && version <= 1.05)
                        || (dev.DeviceType == "MR761" && version >= 3.00 && version < 3.02)
                        || (dev.DeviceType == "MR762" && version >= 3.00 && version < 3.02)
                        || (dev.DeviceType == "MR763" && version >= 3.00 && version < 3.02))
                    {
                        len = Common.TOWORD(answer[3], answer[2]); //����� � ������
                    }
                    else
                    {
                        len = Common.TOWORD(answer[2], answer[3]);
                    }
                    if (len != (answer.Length - 6)/2) //��� ��������, � ������� ����� ������� ������ ������ 64 ����
                    {
                        this.fail++;
                        this.errMessage = "�������� ���������� ��������� ����";
                        return false;
                    }
                }
                else if (this.localSize * 2 != answer.Length - 5)
                {
                    this.fail++;
                    this.errMessage = "�������� ���������� ��������� ����";
                    return false;
                }
            }
            if (CRC16.VerifyRespCrc(answer))
            {
                this.fail++;
                this.errMessage = "�������� CRC";
                return false;
            }
            this.SwapAnswerData(answer);

            this.errMessage = "OK";
            this.ok++;
            return true;
        }

        private void SwapAnswerData(byte[] answer)
        {
            IDeviceInfo dev = this.deviceObj as IDeviceInfo;
            if (dev == null || (this.func != 3 && this.func != 4) || answer.Length <= 259) return;
            double version = Common.VersionConverter(dev.DeviceVersion);
            if ((dev.DeviceType == "MR771" && version <= 1.05) 
                || (dev.DeviceType == "MR761" && version >= 3.00 && version < 3.02)
                || (dev.DeviceType == "MR762" && version >= 3.00 && version < 3.02)
                || (dev.DeviceType == "MR763" && version >= 3.00 && version < 3.02))
            {
                byte[] buff = answer.Skip(4).Take(answer.Length - 6).ToArray();
                Common.SwapArrayItems(ref buff);
                buff.CopyTo(answer, 4);
            }
        }
        
        internal Query Clone()
        {
            return (Query)MemberwiseClone();
        }

        #endregion	Methods
    }
}