using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using BEMN.Interfaces;
using SerialServer;
using BEMN.MBServer.Queries;
using BEMN.MBServer.Exceptions;
using ThreadState = System.Threading.ThreadState;


namespace BEMN.MBServer
{
    /// <summary>
    /// ���������� ��������� Modbus. ��� ������ ��������� ��������� SerialServer.
    /// </summary>
    /// <remarks>
    /// ����������� ������ 4 �������. <see cref="Query.func"/>
    /// </remarks>
    public class Modbus : INodeSerializable
    {
        #region Events
        /// <summary>
        /// ������� ��������� ������
        /// </summary>
        public event OkExchangeHandler OkExchange;
        /// <summary>
        /// ������� ���������� ������
        /// </summary>
        public event FailExchangeHandler FailExchange;
        /// <summary>
        /// ������� �������� ����� �������
        /// </summary>
        public event CompleteExchangeHandler CompleteExchange;
        /// <summary>
        /// ������� ����� �������
        /// </summary>
        public event SuspendHandler Suspend;
        /// <summary>
        /// ������� ��������� ��������� ���� ��������.
        /// </summary>
        /// <remarks>
        /// ������������ ����� ���������� ��� ������� � ����� ������� �����������.
        /// ����� ����� ������� ������������� ��� �������������. <see cref="AddQuery"/>
        /// </remarks>
        public event ExitHandler Stop;
        /// <summary>
        /// ������� ������� ������ �������.
        /// </summary>
        public event ExitHandler Start;

        #endregion Events

        #region Fields

        private static List<Socket> _opendSockets = new List<Socket>();
        private CPort _port;                                               // ��������� com-�����
        private bool _invalidPort;                                         // �������� ���� 
        private List<Query> _queryArray;                                   // ������ �������� 
        private Thread exchange;                                           // ������� ����� 

        private IPEndPoint _endPoint;                                      // Ip-����� � ������ ���������� � �������� ����� ������������
        private string _ip;                                                // Ip-����� 
        private int _tcpPort;                                              // Tcp-���� 

        private static readonly object Sinx = new object();                // ������ ��� ���������� �������
        private bool flag = true;                                          // ������������ ��� ������ ������ ��������� (version on task) �������

        public static bool VersionReadSuccess { get; set; }                // ������������ ��� ���������� ��������� ������ �������

        #region Const
        private const string ERR_MSG_INVALID_LENGTH = "����� �������� �����";
        private const string ERR_MSG_DEVICE_DOES_NOT_ANSWER = "���������� �� ��������";
        private const string SKIP = "skip";
        #endregion

        #endregion Fields

        #region Properties
        /// <summary>
        /// ���� ������������. true, ���� ������� ���
        /// </summary>
        public bool Logging { get; set; }
        /// <summary>
        /// ���� ������������ �������
        /// </summary>
        public bool SuspendQueries { get; set; }
        /// <summary>
        /// ���������� ���� �������. 
        /// </summary>
        /// <remarks>
        /// ��������� ��� ����� ������� ���� ��������.
        /// </remarks>
        [DisplayName("��� ������")]
        [XmlIgnore]
        [Category("�����")]
        public ulong All { get; private set; }

        /// <summary>
        /// ���������� ����������� �������.
        /// </summary>
        /// <remarks>
        /// ��������� ��� ����� ������� ���� ���������� ��������.
        /// </remarks>
        /// 
        [DisplayName("���������� ������")]
        [XmlIgnore]
        [Category("�����")]
        public ulong Ok { get; private set; }

        /// <summary>
        /// ���������� ��������� �������
        /// </summary>
        /// <remarks>
        /// ��������� ��� ����� ������� ���� ��������� ��������.
        /// </remarks>
        /// 
        [DisplayName("��������� ������")]
        [XmlIgnore]
        [Category("�����")]
        public ulong Fail { get; private set; }

        /// <summary>
        /// �������� �� ����������� ������������ �� TCP
        /// </summary>
        public bool NetworkEnabled { get; private set; }
        /// <summary>
        /// ������ C�� ������
        /// </summary>
        [Browsable(false)]
        [XmlIgnore]
        public static CServerClass SerialServer { get; set; } = new CServerClass();
        /// <summary>
        /// �������� �� ������� ����
        /// </summary>
        [DisplayName("������ �����")]
        [Description("���� '����', �� ���� ���������� �������")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        [XmlIgnore]
        public bool IsPortInvalid => this._invalidPort || !this._port.Opened;

        /// <summary>
        /// �������� �� �����
        /// </summary>
        public bool IsDisconnect
        {
            get
            {
                if (!this.NetworkEnabled) return this._invalidPort;
                if (this.Socket == null) return false;

                return !this.Socket.Connected;
            }
        }
        /// <summary>
        /// ID ��������
        /// </summary>
        public static int ProcessID { get; } = Process.GetCurrentProcess().Id;

        /// <summary>
        /// ������������ ����.
        /// </summary>
        /// 
        [XmlIgnore]
        [Browsable(false)]
        public CPort Port
        {
            get => this._port;
            set
            {
                this._port = value;
                this._invalidPort = false;
                try
                {
                    this._port.Open();
                }
                catch
                {
                    this._invalidPort = true;
                }
            }
        }

        /// <summary>
        /// ��������
        /// </summary>
        public uint BaudeRate => _port?.BaudeRate ?? 0;

        /// <summary>
        /// ������� �� � ������ ������ �������
        /// </summary>
        [XmlIgnore]
        public bool IsQueryListEmpty => this._queryArray.Count == 0;

        /// <summary>
        /// ��������� ������ �������. true - �������,false - ��������.
        /// </summary>
        ///
        [Browsable(false)]
        public bool IsActive
        {
            get
            {
                return this.exchange.ThreadState == ThreadState.Running ||
                       this.exchange.ThreadState == ThreadState.WaitSleepJoin ||
                       this.exchange.ThreadState == ThreadState.Background;
            }
        }
        /// <summary>
        /// ��� �����������
        /// </summary>
        public ModbusType ModbusType { get; private set; }

        public NetworkStream TcpStream { get; private set; }

        /// <summary>
        /// ������� ����� ��� TCP �����
        /// </summary>
        public Socket Socket { get; private set; }

        public int ReceiveTimeout { get; private set; }

        #endregion

        #region Constructors
        /// <summary>
        /// ����������� Modbus.
        /// </summary>
        public Modbus()
        {
            this.Port = new CPort();
            this._queryArray = new List<Query>();
            this.SuspendQueries = false;
        }

        /// <summary>
        /// ����������� Modbus.
        /// </summary>
        /// <param name="port"> ������������ ����. ��� ����� ������� "�� ����" <see cref="_port"/></param>
        /// <remarks>
        /// ����������� ��������� ����, ���� �� ��� ������.
        /// </remarks>
        public Modbus(CPort port)
        {
            this.Port = port;
            this._queryArray = new List<Query>();
            this.SuspendQueries = false;
        }
        #endregion Constructors

        #region Methods

        public void SetModbusType(ModbusType modbusType)
        {
            this.ModbusType = modbusType;
        }

        /// <summary>
        /// ����������� � ��������� ����� �� TCP ����� Socket
        /// </summary>
        public void SetNetworkConnection(Socket socket, ModbusType mbType)
        {
            this.DisableNetworkConnection();

            this.Socket = socket;
            this._endPoint = (IPEndPoint) socket.RemoteEndPoint;
            this.ReceiveTimeout = this.Socket.ReceiveTimeout;
            
            this.NetworkEnabled = true;
            this.ModbusType = mbType;

            if (this.CheckConnection())
            {
                this.TcpStream = new NetworkStream(Socket);
            }
            else
            {
                MessageBox.Show("���������� ���������� ����������.");
            }
        }


        private bool CheckConnection()
        {
            if (this.Socket.Connected) return true;

            this.Socket.Connect(this._endPoint);
            int milSecond = 0;
            do
            {
                Thread.Sleep(1);
                milSecond++;
            }
            while (this.Socket.Connected == false || milSecond < 100);

            return milSecond < 100;
        }
        

        /// <summary>
        /// �������������/��������� ��������� �������.
        /// </summary>
        /// <param name="name"> ��� �������. <see cref="Query.name"/></param>
        /// <param name="bSuspend"> ���� ���������. true - ����������, false - ���������.</param>
        /// <returns> true - �������� ������ �������, false - ������ � ����� ������ �� ������.</returns>
        /// <remarks>
        /// � �������� name ������ ����������� ������������ ��� �������,
        /// � ��������� ������ ������� ������ ��������� false, ������ �� ������.
        /// ������� �������, ��� ������������ ������� ��������� �� ������ � ������� ����������
        /// � ���������� ������� �� �������. 
        /// </remarks>
        public bool SuspendQuery(string name, bool bSuspend)
        {
            Query query = this._queryArray.FirstOrDefault(q => q.name == name);
            if (query == null) return false;
            query.bSuspend = bSuspend;
            return true;
        }
        
        public void SetNewPort(CPort port)
        {
            if (port == null) return;

            this.Port = port;
            this._queryArray.Clear();
            this.SuspendQueries = false;
        }

        /// <summary>
        /// ������� ������ ��������� ������
        /// </summary>
        public void Dispose()
        {
            if (this.Port != null && !this.IsPortInvalid && this.Port.Opened)
            {
                this.Port.Close();
            }
         
            if (this.NetworkEnabled)
            {
                this.DisableNetworkConnection();
            }
        }
        /// <summary>
        /// ������������� ������ �� TCP
        /// </summary>
        public void DisableNetworkConnection()
        {
            try
            {
                this.NetworkEnabled = false;

                if (this.Socket == null) return;

                if (_opendSockets.Contains(this.Socket))
                {
                    _opendSockets.Remove(this.Socket);
                }
                this.Socket.Close();
                this.Socket = null;
                this.TcpStream.Close();
                this.TcpStream = null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// ��������� ��� ���-�����������
        /// </summary>
        public static void ClearTcp()
        {
            foreach (Socket socket in _opendSockets)
            {
                socket.Close();
            }
        }

        private void ReadBits(Query query)
        {
            byte[] answer;
            this.BaseRequest(query, out answer);
            query.SetReadBitsBuffer(answer);
            this.RaiseExchange(query);
        }

        private void SetBit(Query query)
        {
            byte[] answer;
            this.BaseRequest(query, out answer);
            this.RaiseExchange(query);
        }
        
        private void ReadWords(Query query)
        {
            byte[] answer; // ������� ������ ����
            this.BaseRequest(query, out answer);
            query.SetReadWordsBuffer(answer);
            query.CalcLoadedBytes3H(answer);
            this.RaiseExchange(query);
        }

        /// <summary>
        /// ���������� 16 �-��� Modbus.
        /// </summary>
        /// <param name="query"> ������ � ������� �������� <see cref="Query.func"/> ����� 16.</param>
        /// <returns> true - ���� ������/������ �������, ����� - false.</returns>
        /// <remarks>
        /// ���� ���� �� ������, ������ ����������. 
        /// </remarks>
        private void WriteWords(Query query)
        {
            byte[] answer;
            this.BaseRequest(query, out answer);
            query.CalcLoadedBytes16H(answer);
            this.RaiseExchange(query);
        }

        private void BaseRequest(Query query, out byte[] answer)
        {
            int count = query.countOfMaxRequest; // ���������� ��������
            do
            {
                query.error = false;
                answer = new byte[0];
                try
                {
                    this.All++;
                    byte[] inputBytes = query.ToBytes(); // ������� ������ � ���� ������� ����
                    if (!this.NetworkEnabled && this.ModbusType != ModbusType.ModbusTcp)
                    {
                        this.WriteReadRtu(inputBytes, out answer, query); // ���������: ������ � ���� ������� ����, ������ �� ������, ��� ������
                    }
                    else if (this.ModbusType == ModbusType.ModbusTcp)
                    {
                        this.WriteReadTcp(inputBytes, out answer, query);
                    }
                    else
                    {
                        this.WriteReadTcpRtu(inputBytes, out answer, query);
                    }
                    
                    if (!query.CheckError(answer))
                    {
                        throw new Exception(query.errMessage);
                    }
                    query.fail = 0;
                    this.Ok++;
                }
                catch (Exception e)
                {
                    if (e is COMException)
                    {
                        query.errMessage = ERR_MSG_DEVICE_DOES_NOT_ANSWER;
                    }
                    if (this.Logging)
                    {
                        Logger.ErrorQuery(e);
                    }
                    query.fail++;
                    this.Fail++;
                    query.error = true;
                    count--;
                    //if (this.NetworkEnabled)
                    //{
                    //    this.Reconnect();
                    //    continue;
                    //}
                    if (this._port.Opened)
                    {
                        this._port.PurgeCOM();
                    }
                    else
                    {
                        this.ReconnectComPort();
                    }
                    if (this.FailExchange != null && query.countOfMaxRequest != 0)
                        this.FailExchange(this, query, SKIP);
                }
            } while (0 < count && query.error);
        }

        private void WriteReadTcpRtu(byte[] inputBytes, out byte[] answer, Query query)
        {
            if (this.Logging)
            {
                Logger.GetQuery(inputBytes, query.name, query.deviceObj, "������", false);
            }
            // �������� �������
            this.Socket.Send(inputBytes, 0, inputBytes.Length, SocketFlags.None);
            // �������� ������
            int millSec = 0;
            do
            {
                Thread.Sleep(1);
                millSec++;
                if (millSec > this.ReceiveTimeout) throw new SocketException();
            } while (this.Socket.Available == 0);
            // ��������� ������ �� ������
            List<byte> buffer = new List<byte>();
            do
            {
                int availableBytes = this.Socket.Available;
                byte[] currByte = new byte[availableBytes];
                int byteCounter = this.Socket.Receive(currByte, currByte.Length, SocketFlags.None);
                if (byteCounter.Equals(availableBytes))
                {
                    buffer.AddRange(currByte);
                }
                Thread.Sleep(this.ReceiveTimeout/20);
            }
            while (this.Socket.Available > 0);

            if (buffer.Count < 6)
            {
                answer = null;
                throw new Exception(ERR_MSG_INVALID_LENGTH);
            }

            answer = buffer.ToArray();
            ushort crc = CRC16.CalcCrcFast(answer, answer.Length - 2);
            answer[answer.Length - 2] = Common.HIBYTE(crc);
            answer[answer.Length - 1] = Common.LOBYTE(crc);
            if (this.Logging)
            {
                Logger.GetQuery(buffer.ToArray(), query.name, query.deviceObj, "�����", true);
            }
        }

        private void WriteReadTcp(byte[] inputBytes, out byte[] answer, Query query)
        {
            byte[] input = new byte[inputBytes.Length + 6 - 2];
            input[5] = (byte)(inputBytes.Length - 2);
            Array.Copy(inputBytes, 0, input, 6, inputBytes.Length - 2);
           
            if (this.Logging)
            {
                Logger.GetQuery(input, query.name, query.deviceObj, "������", false);
            }

            // �������� �������
            this.Socket.Send(input, 0, input.Length, SocketFlags.None);
            // �������� ������
            int millSec = 0;

            do
            {
                Thread.Sleep(10);
                millSec++;
                if (millSec > this.ReceiveTimeout / 10)
                    throw new SocketException();
            } while (this.Socket.Available == 0);
            
            // ��������� ������ �� ������
            List<byte> buffer = new List<byte>();
            do
            {
                int availableBytes = this.Socket.Available;
                byte[] currByte = new byte[availableBytes];
                int byteCounter = this.Socket.Receive(currByte, currByte.Length, SocketFlags.None);
                if (byteCounter.Equals(availableBytes))
                {
                    buffer.AddRange(currByte);
                }
                Thread.Sleep(this.ReceiveTimeout/20);
            }
            while (this.Socket.Available > 0);

            if (buffer.Count < 6)
            {
                answer = null;
                throw new Exception(ERR_MSG_INVALID_LENGTH);
            }
            if (this.ModbusType == ModbusType.ModbusTcp)
            {
                answer = new byte[buffer.Count - 6 + 2];
                Array.Copy(buffer.ToArray(), 6, answer, 0, answer.Length - 2);
            }
            else
            {
                answer = buffer.ToArray();
            }
            ushort crc = CRC16.CalcCrcFast(answer, answer.Length - 2);
            answer[answer.Length - 2] = Common.HIBYTE(crc);
            answer[answer.Length - 1] = Common.LOBYTE(crc);
            if (this.Logging)
            {
                Logger.GetQuery(buffer.ToArray(), query.name, query.deviceObj, "�����", true);
            }
        }

        private void Reconnect()
        {
            try
            {
                TcpConnectionForm connectionForm = new TcpConnectionForm();
                connectionForm.SocketConnection(this._endPoint.Address.ToString(), this._endPoint.Port, this.ReceiveTimeout, true);
                while (!connectionForm.Complite)
                {
                    Application.DoEvents();
                }

                if (connectionForm.Socket == null)
                {
                    throw new Exception();
                }

                this.SetNetworkConnection(connectionForm.Socket, this.ModbusType);
            }
            catch (Exception)
            {
                MessageBox.Show("���������� ���������� ����������. ��� ����������� ������ ���������� ����������������");
                this.CloseTcpClient();
            }
        }

        private void CreateMessageBoxReconnect(string errMessage)
        {
            DialogResult dRes = MessageBox.Show(new Form { TopMost = false }, errMessage, "������ �����", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dRes == DialogResult.Yes)
            {
                try
                {
                    TcpConnectionForm connectionForm = new TcpConnectionForm();
                    connectionForm.SocketConnection(this._endPoint.Address.ToString(), this._endPoint.Port, this.ReceiveTimeout);
                    while (!connectionForm.Complite)
                    {
                        Application.DoEvents();
                    }
                    
                    if (connectionForm.Socket == null)
                    {
                        throw new Exception();
                    }

                    this.SetNetworkConnection(connectionForm.Socket, this.ModbusType);
                }
                catch (Exception)
                {
                    MessageBox.Show("���������� ���������� ����������. ��� ����������� ������ ���������� ����������������");
                    this.CloseTcpClient();
                }
            }
            else
            {
                this.CloseTcpClient();
            } 
        }

        private void CloseTcpClient()
        {
            if (this.Socket == null) return;
            this.DisableNetworkConnection();
        }

        private void WriteReadRtu(byte[] buffer, out byte[] answer, Query query)
        {
            if (this.Logging)
            {
                Logger.GetQuery(buffer, query.name, query.deviceObj, "������", false);
            }
            int countIn = 256;
            if (query.func == 4) // ���� ������� 0x04
            {
                int countBytes = Common.TOWORD(buffer[4], buffer[5])*2;
                if (countBytes < 128)
                {
                    countIn = countBytes + 5;
                }
                else
                {
                    countIn = countBytes + 6;
                }
            }
            if (buffer[1] == 5) // ���� ������� 0x05
            {
                Thread.Sleep(1000);
            }

            answer = this._port.Opened ? this._port.WriteRead(buffer, countIn) as byte[] : null; // ������/������ � ���������� - ����� � answer

            if (this.Logging)
            {
                Logger.GetQuery(answer, query.name, query.deviceObj, "�����", true);
            }

            if (answer != null) return;

            Thread.Sleep(1000);

            throw new Exception(ERR_MSG_DEVICE_DOES_NOT_ANSWER);
        }

        /// <summary>
        /// �����, ���������� ������� RaiseOkExchange ��� RaiseFailExchange
        /// </summary>
        private void RaiseExchange(Query query) 
        {
            if (!query.error)
            {
                this.RaiseOkExchange(query); // �������� �����
            }
            else
            {
                this.RaiseFailExchange(query, query.errMessage); // ��������� �����
            }
        }

        private void RaiseFailExchange(Query query, string errMsg)
        {
            this.FailExchange?.Invoke(this, query, errMsg);
        }
        
        private void RaiseOkExchange(Query query)
        {
            this.OkExchange?.Invoke(this, query);
        }

        /// <summary>
        /// ��������� ������ � �������.
        /// </summary>
        /// <param name="query"> ������.</param>
        /// <remarks>
        /// ���������� �������� ������� <see cref="Query.CheckQuery"/>
        /// ���� ������ � ����� ������ ��� ����������, �� ������������ ��� ������.
        /// ��������� ����� �������, ���� ��� �� ��� �������./>
        /// </remarks>
        public void AddQuery(Query query)
        {
            // �������� �� ������������ �������� �������
            query.CheckQuery();

            // ���� ������ ����� - ��������� � ������ ��������
            if (!this.IsQeryInQueue(query.name))
            {
                this.InsertQuery(query);
            }

            // ���� ����� ������ ������ �������� �� "version on task"
            if (!query.name.StartsWith("version on task"))
            {
                this.StartThread();
            }

            // ���� ��� ������� "version on task"
            else
            {
                int second = 0;

                // ���� �������� ���� ������ ���, ��������� Exchange � ���� �� ������
                if (!flag)
                {
                    this.flag = !flag;
                    this.Exchange();

                    //do
                    //{
                    //    Thread.Sleep(100);
                    //    second++;
                    //}
                    //while (second < 30); // ����� Exchange ������ ������ ���������� �� 3 �������, ����� �������� ������ ������

                    Thread.CurrentThread.Abort(); // ��������� ������� ����� �� ���� �����, ����� �� �������� �������� ��������� ������
                }

                // ���� �������� ���� ������ ��� ������ ��������� �������������
                this.flag = !flag;

                // ������ ������ � �������� ����� � ��������� ������
                if (this.exchange == null || this.exchange.ThreadState == ThreadState.Unstarted || this.exchange.ThreadState == ThreadState.Stopped)
                {
                    this.exchange = new Thread(this.Exchange);
                    this.exchange.Name = "QeryQueueThread";
                    this.exchange.Start();
                }

                if (this.SuspendQueries)
                {
                    this.exchange?.Suspend();
                }

                do
                {
                    Thread.Sleep(1000);
                    second++;
                }
                while (VersionReadSuccess == false && second < 5); // �� 5 ������ ��� ��������� ������ ������ ����� �� ������ � device.IsConnect = false; � ����������� device.IsConnect = true;
            }
        }

        private void StartThread()
        {
            //TODO ���������� �� �����
            if (this.exchange == null || this.exchange.ThreadState == ThreadState.Unstarted || this.exchange.ThreadState == ThreadState.Stopped)
            {
                this.exchange = new Thread(this.Exchange);
                this.exchange.Name = "QeryQueueThread";
                this.exchange.Start();
            }
            if (this.SuspendQueries)
            {
                this.exchange?.Suspend();
            }
        }

        private void InsertQuery(Query query)
        {
            int i;

            bool added = false;
            for (i = 0; i < this._queryArray.Count; i++)
            {
                if (query.priority > this._queryArray[i].priority)
                {
                    this._queryArray.Insert(i, query);
                    added = true;
                    break;
                }
            }

            if (added == false)
            {
                this._queryArray.Add(query);
            }
        }
        
        private void CallMbFunc(Query query)
        {
            switch (query.func)
            {
                case 0x01:
                case 0x02:
                    this.ReadBits(query);
                    break;
                case 0x03:
                case 0x04:
                    this.ReadWords(query);
                    break;
                case 0x05:
                    this.SetBit(query);
                    break;
                case 0x06:
                    this.WriteWords(query);
                    break;
                case 0x10:
                    this.WriteWords(query);
                    break;
                case 0x12:
                    this.ReadWords(query);
                    break;
                default:
                    throw query.eUnknownFunc;
            }
        }

        private void Exchange()
        {
            this.Start?.Invoke(this);
            try
            {
                while (0 != this._queryArray.Count)
                {
                    // ������� ������������ ������
                    if (this.CleanQuery())
                    {
                        break;
                    }
                    this.Stop?.Invoke(this);
                    for (int i = 0; i < this._queryArray.Count; i++)
                    {
                        Query query = this._queryArray[i];
                        Thread.Sleep(query.raiseSpan);
                        if (query.bDelete || query.bSuspend)
                        {
                            continue;
                        }
                        lock (Sinx) // ���������� ����� � ������������� ���������, ��������: "���������" � "��-����"
                        {
                            this.CallMbFunc(query);
                        }

                        //TODO ����������� �� ������
                        if (query.bSuspendByCycle)
                        {
                            this.OnCycle(query, i);
                            continue;
                        }

                        this.CompleteExchange?.Invoke(this, query); // ����� ������� CompleteExchange (�������� ����� �������)

                        this.OnCycle(query, i);

                        if (!this.NetworkEnabled && (this._port.Opened || this._invalidPort) && query.error)
                        {
                            // TODO ����������� InsertQuery ��� ��������� �������� ������� � ������������ ������
                            this.ReconnectComPort();
                        }
                        else
                        {
                            this._invalidPort = false;
                        }

                        //if (this.NetworkEnabled && /*this._tcpClient != null*/ this.Socket != null && this.IsDisconnect)
                        //{
                        //    this.CreateMessageBoxReconnect("����������� �����������. ����������?");
                        //}
                        
                        if (query.error && this.NetworkEnabled)
                        {
                            int count;
                            do
                            {
                                count = this._queryArray.Count;
                                RemoveQuery(this._queryArray[count - 1].name);
                                CleanQuery();
                            } while (0 < count - 1);
                            return;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
            }
        }

        private void ReconnectComPort()
        {
            try
            {
                this._port.Close();
                this._port.Open();
            }
            catch (Exception)
            {
                this._invalidPort = true;
            }
        }
        /// <summary>
        /// ������������ ����������� ������.
        /// </summary>
        /// <param name="query">������.</param>
        /// <param name="i">����� ������� � �������.</param>
        /// <remarks>
        /// ��������� ��� ������������� ������, � ����������� ��
        /// ��������� <see cref="Query.raiseSpan"/>. ���� ������
        /// ���������, �� ���������.
        /// </remarks>
        protected void OnCycle(Query query, int i)
        {
            if (query.bCycle)
            {
                Query queryCopy = this._queryArray[i].Clone();
                this.SuspendCycle(queryCopy);
                this._queryArray[i] = queryCopy;
            }
            else
            {
                query.bDelete = true;
                this._queryArray[i] = query;
            }
        }
        /// <summary>
        /// ����������������/��������� ����������� ������.
        /// </summary>
        /// <param name="query"> ������ �� ������. </param>
        /// 
        protected void SuspendCycle(Query query)
        {
            if ((query.raiseSpan.TotalMilliseconds > 0) && (query.creationTime + query.raiseSpan > DateTime.Now))
            {
                query.bSuspendByCycle = true;
            }
            else
            {
                query.creationTime = DateTime.Now;
                query.bSuspendByCycle = false;
            }
        }
        
        /// <summary>
        /// ���������� ������ �� ��� �����.
        /// </summary>
        /// <param name="name">��� �������.</param>
        /// <returns>������.</returns>
        /// <remarks>
        /// ���� ������� � ����� ������ ��� � �������,���������� null.
        /// </remarks>
        public Query GetQuery(string name) => this._port != null ? this._queryArray.FirstOrDefault(q => q.name == name && !q.bDelete) : null;

        /// <summary>
        /// �������� �� ������������� � ������� �������� �������
        /// </summary>
        /// <param name="name">��� �������</param>
        private bool IsQeryInQueue(string name)
        {
            try
            {
                return this._queryArray.Any(q => q.name == name && !q.bDelete);
            }
            catch(InvalidOperationException)
            {
                return this.IsQeryInQueue(name);
            }
        }

        /// <summary>
        /// ���������� ������, ��� �-���� ������������� �������� ����������� ���������
        /// </summary>
        /// <param name="expression">������� ����������� ���������</param>
        /// <returns>������ �������� ��� null, ���� ��������� �� �������� �������� �������</returns>
        public List<Query> GetQueryByExpression(string expression)
        {
            List<Query> ret = new List<Query>();
            if (this._port != null)
            {
                ret.AddRange(this._queryArray.Where(t => System.Text.RegularExpressions.Regex.IsMatch(t.name, expression) && !t.bDelete));
            }
            return ret;
        }
        
        /// <summary>
        /// ������� ��� ������� � ��������� ������
        /// </summary>
        /// <param name="name">��� �������</param>
        /// <returns>
        /// true - ���� ������� �������.false - ���� �������� � ����� 
        /// ������ ��� � �������.
        /// </returns> 
        public bool RemoveQuery(string name)
        {
            Query[] queries = this._queryArray.Where(q => q.name == name).Select(q => q).ToArray();
            if (queries.Length == 0) return false;
            foreach (Query query in queries)
            {
                query.bDelete = true;
                query.bSuspend = true;
            }
            return true;
        }
        
        private bool CleanQuery()
        {
            bool bRet = 0 == this._queryArray.Count;

            for (int j = 0; j < this._queryArray.Count; j++)
            {
                if (this._queryArray[j].bDelete)
                {
                    this._queryArray.RemoveAt(j);
                    j--;
                }
            }

            return bRet;
        }
        
        #endregion Methods

        #region INodeSerializable Members

        /// <summary>
        /// ������������� ������ � Xml
        /// </summary>
        /// <param name="doc">XML ��������</param>
        /// <returns>XML ���� �������</returns>
        public XmlElement ToXml(XmlDocument doc)
        {
            XmlElement ret = doc.CreateElement("Modbus");


            XmlElement portNum = doc.CreateElement("Port");
            if (null != this.Port)
            {
                portNum.InnerText = this.Port.PortNum.ToString();
                ret.AppendChild(portNum);
            }

            XmlElement connectionType = doc.CreateElement("ConnectionType");
            connectionType.InnerText = this.ModbusType.ToString();
            ret.AppendChild(connectionType);

            XmlElement tcpEnabled = doc.CreateElement("TcpEnabled");
            tcpEnabled.InnerText = this.NetworkEnabled.ToString();
            ret.AppendChild(tcpEnabled);
            
            if (null != this._endPoint)
            {
                XmlElement ip = doc.CreateElement("Ip");
                ip.InnerText = this._endPoint.Address.ToString();
                ret.AppendChild(ip);
                XmlElement tcpPort = doc.CreateElement("TcpPort");
                tcpPort.InnerText = this._endPoint.Port.ToString();
                ret.AppendChild(tcpPort);
            }

            XmlElement timeout = doc.CreateElement("Timeout");  
            timeout.InnerText = this.ReceiveTimeout.ToString();
            ret.AppendChild(timeout);

            XmlElement log = doc.CreateElement("Logging");
            log.InnerText = this.Logging.ToString();
            ret.AppendChild(log);

            return ret;
        }

        /// <summary>
        /// ������������� ������ �� XML
        /// </summary>
        /// <param name="element">XML ���� �������</param>
        public void FromXml(XmlElement element)
        {
            CServer portSrv = SerialServer;
            uint processID = Convert.ToUInt32(Process.GetCurrentProcess().Id);

            foreach (XmlElement cel in element.ChildNodes)
            {
                switch (cel.Name)
                {
                    case "Port":
                        try
                        {
                            //Crossplatform with Win9X
                            try
                            {
                                this.Port = portSrv.GetNamedPort(Convert.ToByte(cel.InnerText), processID);
                            }
                            catch
                            {
                                this.Port = portSrv.GetPort(Convert.ToByte("0"));
                            }
                        }
                        catch (InvalidPortException e)
                        {
                            throw e;
                        }

                        break;

                    case "TcpEnabled":
                    {
                        this.NetworkEnabled = bool.Parse(cel.InnerText);
                        break;
                    }
                    case "ConnectionType":
                    {
                        this.ModbusType = this.GetMbTypeFromStr(cel.InnerText);
                        break;
                    }
                    case "Ip":
                    {
                        this._ip = cel.InnerText;
                        break;
                    }
                    case "TcpPort":
                    {
                        this._tcpPort = int.Parse(cel.InnerText);
                        break;
                    }
                    case "Timeout":
                    {
                        this.ReceiveTimeout = int.Parse(cel.InnerText);
                        break;
                    }
                    case "Logging":
                    {
                        this.Logging = Convert.ToBoolean(cel.InnerText);
                        break;
                    }
                }
            }
            if (this.NetworkEnabled)
            {
                try
                {
                    //TcpClient client = new TcpClient(this._ip, this._tcpPort) {ReceiveTimeout = this._receiveTimeout};
                    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    socket.ReceiveTimeout = this.ReceiveTimeout;
                    this._endPoint = new IPEndPoint(IPAddress.Parse(this._ip), this._tcpPort);
                    this.SetNetworkConnection(socket, this.ModbusType);
                }
                catch (Exception)
                {

                    throw new SocketException();
                }
            }
        }

        private ModbusType GetMbTypeFromStr(string type)
        {
            switch (type)
            {
                case "ModbusRtu":
                    return ModbusType.ModbusRtu;
                case "ModbusTcp":
                    return ModbusType.ModbusTcp;
                case "ModbusOnTcp":
                    return ModbusType.ModbusOnTcp;
                default:
                    return ModbusType.ModbusRtu;
            }
        }

        #endregion
    }

    #region Delegates
    /// <summary>
    /// ������� ��������� ��������� OkExchange.
    /// </summary>
    /// <remarks>
    /// ������� OkExchangeHandler ����� ���������� ��� �������� ������ � �����������.
    /// ���������:sender - ������ ���������. query - ��������� ������. 
    /// </remarks>
    public delegate void OkExchangeHandler(object sender, Query query);
    /// <summary>
    /// ������� ��������� ��������� CompleteExchange.
    /// </summary>
    /// <remarks>
    /// ������� CompleteExchangeHandler ����� ���������� ��� �������� ���������� ����� �������� .
    /// ���������:sender - ������ ���������. query - ��������� ��������� ������. 
    /// </remarks>
    public delegate void CompleteExchangeHandler(object sender, Query query);
    /// <summary>
    /// ������� ��������� ��������� FailExchange.
    /// </summary>
    /// <remarks>
    /// ������� FailExchangeHandler ����� ���������� ��� ��������� ������ � �����������.
    /// ���������:sender - ������ ���������. query - ��������� ������,err - ������. 
    /// </remarks>
    public delegate void FailExchangeHandler(object sender, Query query, string err);
    /// <summary>
    /// ������� ��������� ��������� Stop.
    /// </summary>
    /// <remarks>
    /// ���������� ��� ��������� ������� Stop <see cref="Modbus.Stop"/>
    /// </remarks>
    public delegate void ExitHandler(object sender);
    /// <summary>
    /// ������� ��������� ��������� Suspend.
    /// </summary>
    /// <remarks>
    /// ���������� ��� ��������� ������� Suspend <see cref="Modbus.Suspend"/>
    /// </remarks>
    public delegate void SuspendHandler(object sender, bool bSuspend);

    #endregion Delegates
}