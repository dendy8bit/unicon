﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace BEMN.MBServer
{
    public partial class TcpConnectionForm : Form
    {
        public bool Complite { get; private set; }
        public Socket Socket { get; private set; }
        private int index;
        private string _serverIP;
        private int _port;
        private int _receiveTimeout;
        private readonly Timer _timer;
        private int _timerIndex ;
        private bool _cancel;
        private bool _reconnect;

        public TcpConnectionForm()
        {
            this.InitializeComponent();
            this._timer = new Timer();
            this._timer.Interval = 1000;
            this._timer.Tick += this._timer_Tick;
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            this._timerIndex++;
            if (this._timerIndex >3)
            {
                this._timerIndex = 0;
            }
            string add = new string(Enumerable.Repeat('>', this._timerIndex).ToArray());

            Text = "Подключение " + add;
        }

        /// <summary>
        /// Подключить сокет по заданному IP и порту
        /// </summary>
        /// <param name="serverIP">IP подключаемого устройства</param>
        /// <param name="port">Порт подключения</param>
        /// <param name="receiveTimeout">Время ожидания ответа</param>
        public void SocketConnection(string serverIP, int port, int receiveTimeout, bool reconnect = false)
        {
            this._serverIP = serverIP;
            this._port = port;
            this._receiveTimeout = receiveTimeout;
            this.label2.Text = $"IP - {serverIP}";
            this.label3.Text = $"Port - {port}";
            
            // Проверяем, находится ли устройство в сети
            Ping ping = new Ping();
            PingReply pingReply;

            int counter = 0;
            do
            {
                Thread.Sleep(100);
                pingReply = ping.Send(serverIP);
                counter++;
            }
            while (pingReply?.Status != IPStatus.Success && counter < 3);

            // если нет
            if (pingReply != null && pingReply.Status != IPStatus.Success)
            {
                MessageBox.Show(new Form { TopMost = true }, "Отсутсвует устройство в сети", "Статус связи", MessageBoxButtons.OK);
                this.Socket = null;
                this._cancel = true;
                this.Complite = true;
                Close();
                return;
            }
            
            // если находится в сети
            Show(); // показвываем форму
            this._timer.Start();
            this.StartSocketConnect();
        }

        private void StartSocketConnect()
        {
            // Проверяем какое количество раз подключаемся, выводим соответствующее сообщение
            this.ConnectionState();

            // Проверяет, является ли строка ip-адресом, выполняется если не является
            if (!IPAddress.TryParse(this._serverIP, out IPAddress ipAddress))
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(this._serverIP);
                for (int i = 0; i < ipHostInfo.AddressList.Length; i++)
                {
                    if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[i];
                        break;
                    }
                }
            }

            // ip-адрес с портом
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, this._port);

            // Создаем Soket c IPv4, без дублирования данных, тип - TCP
            this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Время ожидания (по умолчанию 3000, если не меняли)
            this.Socket.ReceiveTimeout = this._receiveTimeout;

            //SetKeepAlive(this.Socket, 15000, 1000);
            this.Socket.BeginConnect(remoteEP, this.OnSocketConnected, this.Socket);
        }

        private void ConnectionState()
        {
            if (this.index == 1)
            {
                this.label1.Text = "Попытка повторного подключения";
            }
            if (this.index > 1)
            {
                this.label1.Text = "Невозможно подключиться";
                this._timer.Stop();
                this._timer.Tick -= this._timer_Tick;
                Text = "Подключение";
                this.button1.Enabled = true;
                return;
            }
            this.index++;
        }

        #region [SetKeepAlive]
        /// <summary>
        /// Sets socket to keepalive mode
        /// </summary>
        /// <param name="s">Socket</param>
        /// <param name="keepalive_time">time</param>
        /// <param name="keepalive_interval">interval</param>
        //private static void SetKeepAlive(Socket s, ulong keepalive_time, ulong keepalive_interval)
        //{
        //    int bytes_per_long = 32 / 8;
        //    byte[] keep_alive = new byte[3 * bytes_per_long];
        //    ulong[] input_params = new ulong[3];

        //    int bits_per_byte = 8;

        //    if (keepalive_time == 0 || keepalive_interval == 0)
        //        input_params[0] = 0;
        //    else
        //        input_params[0] = 1;
        //    input_params[1] = keepalive_time;
        //    input_params[2] = keepalive_interval;

        //    for (int i1 = 0; i1 < input_params.Length; i1++)
        //    {
        //        keep_alive[i1 * bytes_per_long + 3] = (byte)(input_params[i1] >> ((bytes_per_long - 1) * bits_per_byte) & 0xff);
        //        keep_alive[i1 * bytes_per_long + 2] = (byte)(input_params[i1] >> ((bytes_per_long - 2) * bits_per_byte) & 0xff);
        //        keep_alive[i1 * bytes_per_long + 1] = (byte)(input_params[i1] >> ((bytes_per_long - 3) * bits_per_byte) & 0xff);
        //        keep_alive[i1 * bytes_per_long + 0] = (byte)(input_params[i1] >> ((bytes_per_long - 4) * bits_per_byte) & 0xff);
        //    }

        //    s.IOControl(IOControlCode.KeepAliveValues, keep_alive, null);
        //}
        #endregion [SetKeepAlive]

        private void ConnectOk()
        {
            this.label1.Text = "Подключение завершено успешно";
            this._timer.Stop();
            this._timer.Tick -= this._timer_Tick;
            Text = "Подключение";
            Close();
        }

        private void OnSocketConnected(IAsyncResult ar)
        {
            if (this._cancel)
            {
                return;
            }
            try
            {
                Socket s = (Socket) ar.AsyncState;
                
                // Завершает ожидающий асинхронный запрос на подключение
                s.EndConnect(ar);

                if (this.Socket.Connected == false)
                {
                    // Проверяет, является ли строка ip-адресом, выполняется если не является
                    if (!IPAddress.TryParse(this._serverIP, out IPAddress ipAddress))
                    {
                        IPHostEntry ipHostInfo = Dns.GetHostEntry(this._serverIP);
                        for (int i = 0; i < ipHostInfo.AddressList.Length; i++)
                        {
                            if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                            {
                                ipAddress = ipHostInfo.AddressList[i];
                                break;
                            }
                        }
                    }

                    IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, this._port);

                    this.Socket.Connect(ipEndPoint);
                }

                this.Complite = true;

                // Подключение завершено успешно
                Invoke(new Action(this.ConnectOk));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                this.Socket = null;
                if(!IsDisposed)
                    Invoke(new Action(this.StartSocketConnect));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.index = 1;
            this.button1.Enabled = false;
            this._timer.Start();
            this.StartSocketConnect();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Socket = null;
            this._cancel = true;
            this.Complite = true;
            Close();
        }
    }
}
