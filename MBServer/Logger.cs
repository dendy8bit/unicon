﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace BEMN.MBServer
{
    /// <summary>
    /// Статический класс для ведения лога запросов и ответов с устройством
    /// </summary>
    public static class Logger
    {
        private static List<string> _buffer = new List<string>();

        private const string FileName = "\\log.txt";
        private const string SaveDirectory = "\\Logs Directory";
        private const string ConnectionFailPattern = "Ошибка связи c {0}. Ответ от устройства не был получен";

        public static bool LogIsEmpty
        {
            get { return _buffer.Count == 0; }
        }

        /// <summary>
        /// Статический метод,который записывает в буфер лог
        /// </summary>
        /// <param name="array">Массив байт, который записывается в устройство или считывается с него</param>
        /// <param name="queryName">Имя запроса</param>
        /// <param name="device">Устройство</param>
        /// <param name="action">Действие (запрос или ответ)</param>
        /// <param name="isAnswer">True, если ответ</param>
        public static void GetQuery(byte[] array, string queryName, object device, string action, bool isAnswer)
        {
            try
            {
                Type devType = device.GetType();
                PropertyInfo devNumProp = devType.GetProperty("DeviceNumber");
                PropertyInfo portProp = devType.GetProperty("PortNum");
                PropertyInfo captionProp = devType.GetProperty("Caption");
                string info = string.Format("Номер устройства:{0} Номер порта:{1} {2}",
                    devNumProp.GetValue(device, null), portProp.GetValue(device, null),
                    captionProp.GetValue(device, null));

                if (array == null)
                {
                    _buffer.Add(string.Format(ConnectionFailPattern, info));
                    _buffer.Add(Environment.NewLine);
                }
                else
                {
                    _buffer.Add(action+'\t'+info);
                    _buffer.Add("\t" + queryName);
                    _buffer.Add(string.Format("\t[{0},{1}]", DateTime.Now, DateTime.Now.Millisecond));
                    for (int i = 0; i < array.Length; i += 16)
                    {
                        string row = "\t\t" + Convert.ToString(i, 16).ToUpper().PadLeft(4, '0') + "\t";
                        int end = i + 15 < array.Length - 1 ? i + 15 : array.Length - 1;
                        row += ByteToString(array, i, end);
                        _buffer.Add(row);
                    }
                    if(isAnswer)
                        _buffer.Add(Environment.NewLine);
                }
                if (_buffer.Count > 10000)
                {
                    SaveLog();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Невозможно записать лог", "Ошибка записи", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// При возникновении ошибки в связи в лог записывается только сообщение об ошибке
        /// </summary>
        /// <param name="error">Сообщение ошибки</param>
        public static void ErrorQuery(Exception error)
        {
            try
            {
                string path = Path.GetDirectoryName(Application.ExecutablePath);
                path = path + SaveDirectory + FileName;
                File.AppendAllText(path, string.Format("{0}, {1}", error, error.Message));
            }
            catch (Exception)
            {
                MessageBox.Show("Невозможно записать лог\n" + error.Message, "Ошибка записи", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Сохранение лога в файл
        /// </summary>
        public static void SaveLog()
        {
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            path = path + SaveDirectory;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (_buffer != null && _buffer.Count != 0)
            {
                foreach (var row in _buffer)
                {
                    File.AppendAllText(path + FileName, row + Environment.NewLine);
                }
                _buffer.Clear();
            }         
        }

        private static string ByteToString(byte[] array, int start, int end)
        {
            string ret = string.Empty;
            for (int i = start; i <= end; i++)
            {
                ret += Convert.ToString(array[i], 16).ToUpper().PadLeft(2, '0') + " ";
            }
            return ret;
        }



        public static void LogException(Exception e)
        {
            File.AppendAllText("Error.log", DateTime.Now.ToString() + e + Environment.NewLine);
        }
    }
}
