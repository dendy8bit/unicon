using System;

namespace BEMN.MBServer.Exceptions
{
    /// <summary>
    /// ���������� - ���� ���������� ������������
    /// </summary>
    public class InvalidPortException : ApplicationException
    {
        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="msg">���������</param>
        public InvalidPortException(string msg)
            : base(msg)
        {

        }
    };
}