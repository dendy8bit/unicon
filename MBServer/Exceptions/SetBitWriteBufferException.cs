using System;

namespace BEMN.MBServer.Exceptions
{
    /// <summary>
    /// ���������� - ������ � ������� ��������� ���� ( �-��� 5)
    /// </summary>
    public class SetBitWriteBufferException : ApplicationException
    {
        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="msg">���������</param>
        public SetBitWriteBufferException(string msg)
            : base(msg)
        {

        }
    };
}