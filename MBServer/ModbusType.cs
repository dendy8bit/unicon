﻿
namespace BEMN.MBServer
{
    /// <summary>
    /// Способы передачи информации
    /// </summary>
    public enum ModbusType
    {
        /// <summary>
        /// Протокол MODBUS-RTU;
        /// </summary>
        ModbusRtu,
        /// <summary>
        /// Протокол MODBUS-TCP;
        /// </summary>
        ModbusTcp,
        /// <summary>
        /// Передача MODBUS-RTU по TCP без преобразования
        /// </summary>
        ModbusOnTcp
    }
}
