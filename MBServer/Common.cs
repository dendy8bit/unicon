using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BEMN.MBServer
{
    /// <summary>
    /// Summary description for Common.
    /// </summary>
    public class Common
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static double ConvertVersionFromString(string[] version)
        {
            if (version != null)
            {
                try
                {
                    var versionInString =
                        string.Concat(version[0].Replace("T", string.Empty), CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator,
                                      version[1].Replace("T", string.Empty)).Replace("�� ", string.Empty);

                    return Convert.ToDouble(versionInString);
                }
                catch
                {
                    return 0;
                }

            }
            return 0;
        }
        /// <summary>
        /// �������� ����� � 3-� �������� ������
        /// </summary>
        /// <param name="number">�������� ��������</param>
        /// <returns>���������� ��������</returns>
        public static double NumberToThreeChar(double number)
        {
           
            if (Math.Abs(number )< 10)
            {
                return Math.Round(number, 2, MidpointRounding.AwayFromZero);
            }
            if (Math.Abs(number )< 100)
            {
                return Math.Round(number, 1);
            }

            return Math.Round(number, 0);
        }

        /// <summary>
        /// ����� ����� ��� ���������� ����� � ������
        /// </summary>
        /// <param name="stringVersion">�������������� ������</param>
        /// <param name="deviceName">��� �������</param>
        /// <param name="doubleVersion">������ ������������ � double</param>
        /// <returns></returns>
        public static string ChangedVersionForZerooo(string stringVersion, string deviceName, double doubleVersion)
        {
            if (deviceName == "MR901" && doubleVersion == 3 || deviceName == "MR902" && doubleVersion == 3)
            {
                stringVersion += ".00";
            }

            return stringVersion;
        }

        /// <summary>
        /// �������� ����� � 4-� �������� ������
        /// </summary>
        /// <param name="number">�������� ��������</param>
        /// <returns>���������� ��������</returns>
        public static double NumberToFourChar(double number)
        {

            if (Math.Abs(number) < 10)
            {
                return Math.Round(number, 3);
            }
            if (Math.Abs(number) < 100)
            {
                return Math.Round(number, 2);
            }
            if (Math.Abs(number) < 1000)
            {
                return Math.Round(number, 1);
            }
            return Math.Round(number, 0);
        }

        /// <summary>
        /// ��������� ������ bool �� �������� ���������
        /// </summary>
        /// <param name="bitArray">�������� ������</param>
        /// <param name="startIndex">������ ������</param>
        /// <param name="endIndex">������ �����</param>
        /// <returns></returns>
        public static bool[] GetBitsArray(BitArray bitArray, int startIndex, int endIndex)
        {
            var lenght = endIndex - startIndex+1;
            var bits = new bool[lenght];
            for (int i = 0; i < lenght; i++)
            {
                bits[i] = bitArray[i + startIndex];
            }
            return bits;
        }

        /// <summary>
        /// ��������� ������ bool �� �������� ���������
        /// </summary>
        /// <param name="ushortArray">����� ����</param>
        /// <param name="startIndex">������ ������</param>
        /// <param name="endIndex">������ �����</param>
        /// <returns></returns>
        public static bool[] GetBitsArray(ushort[] ushortArray, int startIndex, int endIndex)
        {
            BitArray bitArray = new BitArray(Common.TOBYTES(ushortArray,false));
            int lenght = endIndex - startIndex + 1;
            bool[] bits = new bool[lenght];
            for (int i = 0; i < lenght; i++)
            {
                bits[i] = bitArray[i + startIndex];
            }
            return bits;
        }

        /// <summary>
        /// ��������� ������ bool �� �������� �����
        /// </summary>
        public static bool[] GetBitsArray(int intVal)
        {
            List<bool> bits = new List<bool>();
            for (int i = 0; i < 32; i++)
            {
                bool bit = ((intVal & (int)Math.Pow(2, i)) >> i) > 0;
                bits.Add(bit);
            }
            return bits.ToArray();
        }



        /// <summary>
        /// ��������� ������ bool �� �����
        /// </summary>
        /// <param name="ushortVal">����� ����</param>
        /// <returns></returns>
        public static bool[] GetBitsArray(ushort ushortVal)
        {
            List<bool> bits = new List<bool>();
            for (int i = 0; i < 16; i++)
            {
                bool bit = ((ushortVal & (ushort)Math.Pow(2, i)) >> i) > 0;
                bits.Add(bit);
            }
            return bits.ToArray();
        }

        /// <summary>
        /// ���������� ������� ���� �����
        /// </summary>
        /// <param name="v">�����.</param>
        /// <returns>��.����</returns>
        public static byte LOBYTE(int v)
        {
            return (byte)(v & 0xff);
        }
        /// <summary>
        /// ���������� ������� ���� �����.
        /// </summary>
        /// <param name="v">�����.</param>
        /// <returns>��.����</returns>
        public static byte HIBYTE(int v)
        {
            return (byte)(v >> 8);
        }
        
        /// <summary>
        /// ������������ 2 ����� � �����
        /// </summary>
        /// <param name="high">��.����</param>
        /// <param name="low">��.����</param>
        /// <returns>�����.</returns>
        public static ushort TOWORD(byte high, byte low)
        {
            ushort ret = (ushort)high;
            return (ushort)((ushort)(ret << 8) + (ushort)low);
        }
        /// <summary>
        /// ������������ 4 ����� � 32-��������� �����
        /// </summary>
        /// <param name="first">������� ���� �������� �����</param>
        /// <param name="second">������� ���� �������� �����</param>
        /// <param name="third">������� ���� �������� �����</param>
        /// <param name="fourth">������� ���� �������� �����</param>
        /// <returns>32-��������� �����</returns>
        public static uint ToWord32(byte first, byte second, byte third, byte fourth)
        {
            return (uint)((first << 24) + (second << 16) + (third << 8) + fourth);
        }
        /// <summary>
        /// ������������ 4 ����� � 32-��������� �����
        /// </summary>
        /// <param name="mass">������ �� 4� ����</param>
        /// <returns>32-��������� �����</returns>
        public static uint ToWord32(byte[] mass)
        {
            return mass.Length != 4 ? 0 : ToWord32(mass[2], mass[3], mass[0], mass[1]);
        }

        /// <summary>
        /// ������������ 2 ushort � int
        /// </summary>
        /// <param name="Word1">1� ushort</param>
        /// <param name="Word2">2� ushort</param>
        /// <returns>Int</returns>
        public static int UshortUshortToInt(ushort Word1, ushort Word2)
        {
            string d1 = Word1.ToString("X2");
            string d2 = Word2.ToString("X2");
            if (d2.Length < 4)
                do
                {
                    d2 = "0" + d2;
                } while (d2.Length < 4);

            return int.Parse(d1 + d2, System.Globalization.NumberStyles.HexNumber);
        }

        /// <summary>
        /// ������������ 2 ushort � uint
        /// </summary>
        /// <param name="Word1">1� ushort</param>
        /// <param name="Word2">2� ushort</param>
        /// <returns>Int</returns>
        public static uint UshortUshortToUInt(ushort Word1, ushort Word2)
        {
            string d1 = Word1.ToString("X2");
            string d2 = Word2.ToString("X2");
            if (d2.Length < 4)
                do
                {
                    d2 = "0" + d2;
                } while (d2.Length < 4);

            return uint.Parse(d1 + d2, System.Globalization.NumberStyles.HexNumber);
        }





        //0000001000000101
        /// <summary>?
        /// ������������ ������ ���� � ������ ����. 
        /// </summary>
        /// <param name="bytes">������ ����</param>
        /// <param name="bDirect"> ������� ����.false - ������,true - �������</param>
        /// <returns></returns>
        public static ushort[] TOWORDS(byte[] bytes, bool bDirect)
        {

            if (0 != bytes.Length % 2)
            {
                byte[] buffer = bytes;
                bytes = new byte[bytes.Length + 1];
                Array.ConstrainedCopy(buffer,0,bytes,0,buffer.Length);
            }
            ushort[] ret = new ushort[bytes.Length / 2];
            int j = 0;
            for (int i = 0; i < bytes.Length; i += 2)
            {
                if (bDirect)
                {
                    ret[j++] = TOWORD(bytes[i], bytes[i + 1]);
                }
                else
                {
                    ret[j++] = TOWORD(bytes[i + 1], bytes[i]);
                }

            }
            return ret;
        }

        /// <summary>
        /// ������ ������� ��. � ��. ���� �����.
        /// </summary>
        /// <param name="val">�����.</param>
        /// <returns>����������� �����.</returns>
        public static ushort SwapByte(ushort val)
        {
            return TOWORD(LOBYTE(val), HIBYTE(val));
        }

        /// <summary>
        /// ������ ������� ������� �������� �������. 
        /// </summary>
        /// <param name="array">������.</param>
        /// <returns>����������� ������.</returns>
        public static void SwapArrayItems<T>(ref T[] array)
        {
            T temp;
            try
            {
                for (int i = 0; i < array.Length; i += 2)
                {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }
        /// <summary>
        /// ������ ������� ������� �������� �������. 
        /// </summary>
        /// <param name="array">�������������� ������, � ������� ������������ ������ ��������</param>
        /// <returns>����������� ������.</returns>
        public static T[] SwapArrayItems<T>(T[] array)
        {
            try
            {
                T[] ret = new T[array.Length];
                for (int i = 0; i < array.Length; i += 2)
                {
                    ret[i] = array[i + 1];
                    ret[i + 1] = array[i];
                }
                return ret;
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }

        /// <summary>
        /// ������ ������� ������� �������� � ������
        /// </summary>
        /// <param name="list">��������� ������, � ������� ���� ������� �������� ������� ��������</param>
        /// <typeparam name="T">��� ������ ������</typeparam>
        public static List<T> SwapListItems<T>(IEnumerable<T> list)
        {
            List<T> ret = list.ToList();
            try
            {
                for (int i = 0; i < ret.Count; i += 2)
                {
                    T temp = ret[i];
                    ret[i] = ret[i + 1];
                    ret[i + 1] = temp;
                }
                return ret;
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }

        /// <summary>
        /// ������������ ���� � ���� ��������� ����� ���� '01010101'
        /// </summary>
        /// <param name="value">��������</param>
        /// <param name="reverse">������������� �������</param>
        /// <returns></returns>
        public static string ByteToMask(byte value, bool reverse)
        {
            var chars = Convert.ToString(value, 2).PadLeft(8, '0').ToCharArray();
            if (reverse)
            {
                Array.Reverse(chars);
            }
            
            return new string(chars);
        }

        /// <summary>
        /// ������������ BitArray � ������ ���� '010001'
        /// </summary>
        /// <param name="bits">������� �������������</param>
        /// <returns>��������� �������������</returns>
        public static string BitsToString(BitArray bits)
        {
            string ret = "";
            for (int i = 0; i < bits.Count; i++)
            {
                if (bits[i])
                {
                    ret += "1";
                }
                else
                {
                    ret += "0";
                }
            }
            return ret;
        }

        /// <summary>
        /// ������������ ������ ���� '010001' � BitArray
        /// </summary>
        /// <param name="str">��������� �������������</param>
        /// <returns>������� �������������</returns>
        public static BitArray StringToBits(string str)
        {
            bool[] bMas = new bool[str.Length];

            for (int i = 0; i < str.Length; i++)
            {
                if ('1' == str[i])
                {
                    bMas[i] = true;
                }
                else if ('0' == str[i])
                {
                    bMas[i] = false;
                }
                else
                {
                    throw new ApplicationException("������ " + str + " ���������� ��������������� � ����. ������ " + bMas[i].ToString());
                }
            }

            return new BitArray(bMas);
        }

        /// <summary>
        /// ������������ ������ ���� � ������ ����
        /// </summary>
        /// <param name="words"> ������ ����.</param>
        /// <param name="bDirect">������� ����. true - �������, false - ��.���� ������ ������ � ��.������.</param>
        /// <returns>������ ����.</returns>
        public static byte[] TOBYTES(ushort[] words, bool bDirect)
        {
            byte[] buffer = new byte[words.Length * 2];
            for (int i = 0, j = 0; i < words.Length; i++)
            {
                if (bDirect)
                {
                    buffer[j++] = Common.HIBYTE(words[i]);
                    buffer[j++] = Common.LOBYTE(words[i]);

                }
                else
                {
                    buffer[j++] = Common.LOBYTE(words[i]);
                    buffer[j++] = Common.HIBYTE(words[i]);
                }
            }
            return buffer;
        }

        /// <summary>
        /// ������������ ������ ���� � ������ ����
        /// </summary>
        /// <param name="words"> ���� ����.</param>
        /// <param name="bDirect">������� ����. true - �������, false - ��.���� ������ ������ � ��.������.</param>
        /// <returns>������ ����.</returns>
        public static byte[] TOBYTES(List<ushort> words, bool bDirect)
        {
            return TOBYTES(words.ToArray(), bDirect);
        }

        /// <summary>
        /// ������������ ����� � ������ ����
        /// </summary>
        /// <param name="word">�����</param>
        /// <returns>������ ����</returns>
        public static byte[] TOBYTE(ushort word , bool bDirect = true)
        {
            byte[] buffer = new byte[2];
            if (bDirect)
            {
                buffer[0] = Common.HIBYTE(word);
                buffer[1] = Common.LOBYTE(word);
            }
            else
            {
                buffer[0] = Common.LOBYTE(word);
                buffer[1] = Common.HIBYTE(word);
            }
            return buffer;
        }

        /// <summary>
        /// ������������ ��������� ����� � �����
        /// </summary>
        /// <param name="bits">�������� �����</param>
        /// <returns>�����</returns>
        public static ushort BitsToUshort(BitArray bits)
        {
            ushort temp = 0;
            for (int i = 0; i < bits.Count; i++)
            {
                temp += bits[i] ? (ushort)Math.Pow(2, i) : (ushort)0;
            }
            return temp;
        }

        /// <summary>
        /// ������������ ��������� ����� � �����
        /// </summary>
        /// <param name="bits">�������� �����</param>
        /// <returns>�����</returns>
        public static ushort BitsToUshort(List<bool> bits)
        {
            ushort temp = 0;
            for (int i = 0; i < bits.Count; i++)
            {
                temp += bits[i] ? (ushort)Math.Pow(2, i) : (ushort)0;
            }
            return temp;
        }

        /// <summary>
        /// ������������ ��������� ����� � ������ ����
        /// </summary>
        /// <param name="bits">��������� �����</param>
        /// <returns>�����</returns>
        public static ushort[] BitsToUshorts(List<bool> bits)
        {
            List<ushort> retUshorts = new List<ushort>();
            int count = bits.Count%16 > 0 ? bits.Count/16 + 1 : bits.Count/16;
            for (int i = 0;i<count; i++)
            {
                ushort temp = 0;
                for (int j = 0; j < 16; j++)
                {
                    temp += bits[j+i*16] ? (ushort)Math.Pow(2, j) : (ushort)0;
                }
                retUshorts.Add(temp);
            }
            
            return retUshorts.ToArray();
        }

        /// <summary>
        /// ����������� ��������� �������� ������ � ���������
        /// </summary>
        /// <param name="ver"></param>
        /// <returns></returns>
        public static double VersionConverter(string ver)
        {
            ver = ver.Replace(".",CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace("H", string.Empty).Replace("S", string.Empty).Replace("T",string.Empty).Replace("A", string.Empty);
            //TODO ����� ��� �������� ��7
            if (ver == "$MR801")
            {
                ver = "3.00";
            }
            return Convert.ToDouble(ver, CultureInfo.CurrentCulture);
        }

        public static string VersionLiteral(string Ver)
        {
            if (Ver.Contains("S"))
                return "S";
            if(Ver.Contains("H"))
                return "H";
            if (Ver.Contains("A"))
                return "H";
            return string.Empty;
        }

        /// <summary>
        /// ������������� ��� � �����
        /// </summary>
        /// <param name="value">�����</param>
        /// <param name="index">����� ���� � �����</param>
        /// <param name="bit">�������� ����</param>
        /// <returns>����� ����� ��������� ����</returns>
        public static ushort SetBit(ushort value, int index, bool bit)
        {
            ushort ret = value;
            ushort temp = (ushort)Math.Pow(2, index);
            ret = bit ? (ushort)(value |= temp) : (ushort)(value & ~temp);
            return ret;
        }

        /// <summary>
        /// ���������� �������� ���� � �����
        /// </summary>
        /// <param name="value">�����</param>
        /// <param name="index">����� ����</param>
        /// <returns>�������� ����</returns>
        public static bool GetBit(ushort value, int index)
        {
            return 0 != (value & (ushort)Math.Pow(2, index));
        }

        /// <summary>
        /// ���������� �������� ����� � ����� ������ 
        /// �������� : ���� ����� �����, � �� �����, ����� �������� ���-��� �� n ���, ��� n = ����� ���������� ����.
        /// ������ : int number = GetBits(value,8,9) >> 8
        /// </summary>
        /// <param name="value">�����</param>
        /// <param name="indexes">������ �����</param>
        /// <returns>�������� ����� - �������� �������� �����, ������������ � indexes</returns>
        public static ushort GetBits(ushort value, params int[] indexes)
        {
            ushort mask = 0;
            for (int i = 0; i < indexes.Length; i++)
            {
                mask |= (ushort)(1 << indexes[i]);
            }
            return (ushort)(value & mask);
        }

        /// <summary>
        /// ������������� ���� � ����� �� �����. �������� -������ ����� ������ ���� �� �����������, ��� ���������.
        /// </summary>
        /// <param name="value">�����</param>
        /// <param name="bits">������� �����</param>
        /// <param name="indexes">������ ��������������� �����</param>
        /// <returns>������� ����� ����� ����������� �����</returns>
        public static ushort SetBits(ushort value, ushort bits, params int[] indexes)
        {
            ushort mask = Common.GetBits(ushort.MaxValue, indexes);
            value = (ushort)(value & ~mask);
            return (ushort)(value | (bits << indexes[0]));
        }
        /// <summary>
        /// �������������� ������� ���� � ������ ASCII ��������
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public static char[] GetChars(byte[] byteArray)
        {
            System.Text.ASCIIEncoding ASCII = new System.Text.ASCIIEncoding();
            return ASCII.GetChars(byteArray);
        }

        public static string CharsToString(char[] charArray)
        {
            string res = string.Empty;
            for (int i = 0; i < charArray.Length; i++)
            {
                res += charArray[i];
            }
            int f = res.IndexOf("\0");

            if (f > -1)
            {
                string resTmp = string.Empty;
                for (int i = 0; i < f; i++)
                {
                    resTmp += res[i];
                }
                res = resTmp;
            }

            return res;
        }
    }


}

