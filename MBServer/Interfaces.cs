using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Xml;

namespace BEMN.Interfaces
{
    /// <summary>
    /// ��������� ����������� ������� ��������/���������� 
    /// </summary>
    /// <param name="sender">������ - �������� �������</param>
    public delegate void LoadHandler(object sender);

    /// <summary>
    /// ��������� ����������� ������� �������� ������������ ��������
    /// </summary>
    public delegate void LoadCounter();

    /// <summary>
    /// ��������� ����������� ������� ����� ������ ����
    /// </summary>
    /// <param name="sender">�������� �������</param>
    /// <param name="e">CustomNodeTextEventArgs ��������</param>
    public delegate void CustomNodeTextHandler(object sender, CustomNodeTextEventArgs e);

    /// <summary>
    /// �������� ��� ������� CustomNodeTextChanged
    /// </summary>
    public  class CustomNodeTextEventArgs : EventArgs
    {
        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="text">����� ����</param>
        public  CustomNodeTextEventArgs(string text)
        {
            _text = text;
        }
        private string  _text;
        /// <summary>
        /// ����� ����� ����
        /// </summary>
        public string  Text
        {
            get { return _text; }
            set { _text = value; }
        }

    }

    /// <summary>
    /// ��������� ����� ����
    /// </summary>
    public  interface ICustomizeNode
    {
        event CustomNodeTextHandler CustomNodeTextChanged;
    }
    
    /// <summary>
    /// ��������� ���� ������. ����� ����������� ������ ���������, ����� ��������� ����� ������.
    /// </summary>
    public interface INodeView
	{
        /// <summary>
        /// ��� ������ ������������ ���������
        /// </summary>
        Type ClassType
        {
            get;
        }
        /// <summary>
        /// ��������� ���� (false)
        /// </summary>
        bool ForceShow
        {
            get;
        }
       /// <summary>
       /// ������ ����
       /// </summary>
        Image NodeImage
        {
            get;
        }

        /// <summary>
        /// ����� ����� ����
        /// </summary>
        string NodeName
        {
            get;
        }
              
        /// <summary>
        /// ���������� �������� �����
        /// </summary>
        INodeView[] ChildNodes
        {
            get;
        }

        /// <summary>
        /// ���� ����� �������(true)
        /// </summary>
        bool Deletable
        {
            get;
        }
                      
	}
    
    /// <summary>
    /// ��������� ��� ���������� ���������� � ������ �����
    /// </summary>
    public interface IDeviceLog
    {        
        /// <summary>
        /// ��� ��� ������� - ������ � ������ �����
        /// </summary>
        Hashtable LogHash
        {
            get;
        }

        /// <summary>
        /// ������� �������� �������� ������� ����������
        /// </summary>
        event LoadHandler PropertiesLoadOk;

        /// <summary>
        /// ������� ������ ��� �������� ������� ����������
        /// </summary>
        event LoadHandler PropertiesLoadFail;

        /// <summary>
        /// ��������� ��-�� ����������
        /// </summary>
        void LoadProperties();

        /// <summary>
        /// ��������� ��-�� ����������
        /// </summary>
        void SaveProperties();
        
    }

    /// <summary>
    /// ��������� ���� ������ ��� ����������. ����� ����������� ������ ���������, ����� ���������
    /// ������� ����������.
    /// </summary>
	public interface IDeviceView : INodeView
	{
	}

    public interface IDeviceInfo
    {
        string DeviceVersion { get; }
        string DeviceType { get; }
        string DevicePlant { get; }
    }

    /// <summary>
    /// ��������� ���� ������ ��� ����
    /// </summary>
	public interface IFormView : INodeView
	{
        /// <summary>
        /// ��� ���������� ������������ ������
        /// </summary>
        [Browsable(false)]
		Type FormDevice
		{
			get;
		}
        /// <summary>
        /// ����������� ��������� ��������� ���������� ����
        /// </summary>
        bool Multishow
        {
            get;
        }
    }
    
    /// <summary>
    /// ����� ������������. ����� Device ������������� �� ���������.
    /// ��� ������������� �������� Device ������� �������������� �-��� ������������,
    /// � ��. ������ - ����������� ��������� ������.
    /// </summary>
    public interface INodeSerializable
    {
        /// <summary>
        /// ���������� �������� 
        /// </summary>
        /// <param name="doc">XML ��������</param>
        /// <returns>XML �������, �������� ���� </returns>
        XmlElement ToXml(XmlDocument doc);

        /// <summary>
        /// �������������� ��������
        /// </summary>
        /// <param name="element">XML ������� � ����������� ���.</param>
        void FromXml(XmlElement element);
    }
}
