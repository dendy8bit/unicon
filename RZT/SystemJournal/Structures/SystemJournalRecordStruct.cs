﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.RZT.SystemJournal.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SystemJournalRecordStruct : IStruct, IStructInit
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";

        #endregion [Constants]


        #region [Private fields]

        private ushort _year;
        private ushort _month;
        private ushort _date;
        private ushort _hour;
        private ushort _minute;
        private ushort _second;
        private ushort _millisecond;
        private ushort _message;

        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
               return this._message == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetRecordTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );

            }
        }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string GetRecordMessage
        {
            get
            {
                return StringsSj.Message.ContainsKey(this._message) 
                    ? StringsSj.Message[this._message] 
                    : this._message.ToString();
            }
        }
        #endregion [Properties]


        #region [IStruct Members]

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLength);
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            var index = 0;
this._message = Common.TOWORD(array[index + 1], array[index]);
index += sizeof(UInt16);
            this._year = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);
            this._month = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);
            this._date = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);
            this._hour = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);
            this._minute = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);
            this._second = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);
            this._millisecond = Common.TOWORD(array[index + 1], array[index]);
            
            
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._year,
                    this._month,
                    this._date,
                    this._hour,
                    this._minute,
                    this._second,
                    this._millisecond,
                    this._message
                };
        }

        #endregion [IStructInit Members]
    }
}
