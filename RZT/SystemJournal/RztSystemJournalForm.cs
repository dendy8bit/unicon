﻿using System;
using System.Data;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.RZT.SystemJournal.Structures;

namespace BEMN.RZT.SystemJournal
{
    public partial class RztSystemJournalForm : Form, IFormView
    {

        #region [Ctor's]
        private readonly RztDevice _device;

        public RztSystemJournalForm()
        {
            this.InitializeComponent();
        }

        public RztSystemJournalForm(RztDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._systemJournal = device.SystemJournal;
            
            this._systemJournal.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._systemJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
        }

        #endregion [Ctor's]


        #region [Constants]

        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "РЗТ110_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string JOURNAL_READDING = "Идёт чтение журнала";

        #endregion [Constants]


        protected string[] ColumnsNames
        {
            get
            {
                return new[]
                {
                    NUMBER_SYS,
                    TIME_SYS,
                    MESSAGE_SYS
                };
            }
        }

        protected string TableName
        {
            get { return TABLE_NAME_SYS; }
        }

        #region [Private fields]

        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;

        #endregion [Private fields]


        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (RztDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public Type ClassType
        {
            get { return typeof(RztSystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public System.Drawing.Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }

        #endregion [IFormView Members]


        #region [Properties]

        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);

            }
        }

        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Устанавливает св-во Enabled для всех кнопок
        /// </summary>
        /// <param name="enabled"></param>
        private void SetButtonsState(bool enabled)
        {
            this._readJournalButton.Enabled = enabled;
            this._saveJournalButton.Enabled = enabled;
            this._loadJournalButton.Enabled = enabled;
            this._dropJsButton.Enabled = enabled;
        }

        private void FailReadJournal()
        {
            this.SetButtonsState(true);
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }

        private void StartReadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this._configProgressBar.Value = 0;
            this.RecordNumber = 0;
            this.SetButtonsState(false);
            this._statusLabel.Text = JOURNAL_READDING;
            this._systemJournal.LoadStruct();
        }

        private void ReadRecord()
        {
            var i = 0;
            foreach (var record in this._systemJournal.Value._records)
            {
                if (record.IsEmpty)
                {
                    break;
                }
                this._dataTable.Rows.Add( i+1, record.GetRecordTime, record.GetRecordMessage);
                i++;
            }
            this._systemJournalGrid.DataSource = this._dataTable;
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
            this.SetButtonsState(true);
        }
        
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(this.TableName);
            foreach (var columnName in this.ColumnsNames)
            {
                table.Columns.Add(columnName);
            }
            return table;
        }

        private void SaveJournalToFile()
        {

            this._saveJournalDialog.FileName = string.Format("РЗТ-110 Журнал Системы {0}", DateTime.Now.ToString().Replace(':','.'));
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
                this._systemJournalGrid.DataSource = this._dataTable;
                this.RecordNumber = this._systemJournalGrid.Rows.Count;
            }
        }

        #endregion [Help members]


        #region [Events Handlers]

        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this.StartReadJournal();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        
        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._systemJournal.RemoveStructQueries();
        }

        private void _dropJsButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Данные журнала системы\r\nбудут потеряны. Вы уверены,\r\nчто хотите очистить журнал?", string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.SetBitByAdress(0x1801, "Drop JS");
                this._dataTable.Clear();
                this.RecordNumber = 0;
                this._configProgressBar.Value = 0;
                this.StartReadJournal();
            }
        }
        #endregion [Events Handlers]

        private void SetBitByAdress(ushort adress, string requestName)
        {
            this. _device.SetBit(this._device.DeviceNumber, adress, true, requestName, this);
        }
    }
}
