﻿namespace BEMN.RZT.SystemJournal
{
    partial class RztSystemJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._saveJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this._openJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveJournalButton = new System.Windows.Forms.Button();
            this._loadJournalButton = new System.Windows.Forms.Button();
            this._readJournalButton = new System.Windows.Forms.Button();
            this._systemJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dropJsButton = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _saveJournalDialog
            // 
            this._saveJournalDialog.DefaultExt = "xml";
            this._saveJournalDialog.FileName = "РЗТ-110 Журнал Системы";
            this._saveJournalDialog.Filter = "РЗТ-110 Журнал Системы | *.xml";
            this._saveJournalDialog.Title = "Сохранить  журнал системы для РЗТ-110";
            // 
            // _openJournalDialog
            // 
            this._openJournalDialog.DefaultExt = "xml";
            this._openJournalDialog.FileName = "РЗТ-110 Журнал Системы";
            this._openJournalDialog.Filter = "РЗТ-110 Журнал Системы | *.xml";
            this._openJournalDialog.RestoreDirectory = true;
            this._openJournalDialog.Title = "Открыть журнал системы для РЗТ-110";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 443);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(630, 22);
            this.statusStrip1.TabIndex = 20;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Maximum = 128;
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            this._configProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _saveJournalButton
            // 
            this._saveJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveJournalButton.Location = new System.Drawing.Point(155, 417);
            this._saveJournalButton.Name = "_saveJournalButton";
            this._saveJournalButton.Size = new System.Drawing.Size(137, 23);
            this._saveJournalButton.TabIndex = 16;
            this._saveJournalButton.Text = "Сохранить";
            this._saveJournalButton.UseVisualStyleBackColor = true;
            this._saveJournalButton.Click += new System.EventHandler(this._saveJournalButton_Click);
            // 
            // _loadJournalButton
            // 
            this._loadJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadJournalButton.Location = new System.Drawing.Point(298, 417);
            this._loadJournalButton.Name = "_loadJournalButton";
            this._loadJournalButton.Size = new System.Drawing.Size(137, 23);
            this._loadJournalButton.TabIndex = 17;
            this._loadJournalButton.Text = "Загрузить";
            this._loadJournalButton.UseVisualStyleBackColor = true;
            this._loadJournalButton.Click += new System.EventHandler(this._loadJournalButton_Click);
            // 
            // _readJournalButton
            // 
            this._readJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readJournalButton.Location = new System.Drawing.Point(12, 417);
            this._readJournalButton.Name = "_readJournalButton";
            this._readJournalButton.Size = new System.Drawing.Size(137, 23);
            this._readJournalButton.TabIndex = 15;
            this._readJournalButton.Text = "Прочитать журнал";
            this._readJournalButton.UseVisualStyleBackColor = true;
            this._readJournalButton.Click += new System.EventHandler(this._readJournalButton_Click);
            // 
            // _systemJournalGrid
            // 
            this._systemJournalGrid.AllowUserToAddRows = false;
            this._systemJournalGrid.AllowUserToDeleteRows = false;
            this._systemJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._systemJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._systemJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._systemJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._systemJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._systemJournalGrid.Name = "_systemJournalGrid";
            this._systemJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._systemJournalGrid.Size = new System.Drawing.Size(630, 411);
            this._systemJournalGrid.TabIndex = 13;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "Номер";
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Время";
            this._timeCol.HeaderText = "Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.Width = 65;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.DataPropertyName = "Сообщение";
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            // 
            // _dropJsButton
            // 
            this._dropJsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._dropJsButton.Location = new System.Drawing.Point(441, 417);
            this._dropJsButton.Name = "_dropJsButton";
            this._dropJsButton.Size = new System.Drawing.Size(183, 23);
            this._dropJsButton.TabIndex = 80;
            this._dropJsButton.Text = "Очистить журнал системы";
            this._dropJsButton.UseVisualStyleBackColor = true;
            this._dropJsButton.Click += new System.EventHandler(this._dropJsButton_Click);
            // 
            // RztSystemJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 465);
            this.Controls.Add(this._dropJsButton);
            this.Controls.Add(this._readJournalButton);
            this.Controls.Add(this._systemJournalGrid);
            this.Controls.Add(this._saveJournalButton);
            this.Controls.Add(this._loadJournalButton);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(450, 500);
            this.Name = "RztSystemJournalForm";
            this.Text = "Mr600SystemJournalFormV2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.SystemJournalForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog _saveJournalDialog;
        private System.Windows.Forms.OpenFileDialog _openJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _saveJournalButton;
        private System.Windows.Forms.Button _loadJournalButton;
        private System.Windows.Forms.Button _readJournalButton;
        private System.Windows.Forms.DataGridView _systemJournalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.Button _dropJsButton;

    }
}