﻿using System.Collections.Generic;
using System.Linq;

namespace BEMN.RZT.Configuration
{
    public static class StringsConfig
    {

        
             public static List<string> DeviceNumbers
        {
            get { return Enumerable.Range(1, 247).Select(o => o.ToString()).ToList(); }
        }

        public static List<string> ExternalDefensesNames
        {
            get
            {
                return new List<string>
                    {
                        "ВЗ-1",
                        "ВЗ-2"
                    };
            }
        }
        public static List<string> CurrentDefensesNames
        {
            get
            {
                return new List<string>
                    {
                         "I>",
                    "I>>",
                    "I>>>"
                    };
            }
        }
        public static List<string> RelaysNames
        {
            get
            {
                return new List<string>
                    {
                        "1",
                        "2",
                        "3"
                    };
            }
        }
        /// <summary>
        /// Фиксация осц
        /// </summary>
        public static List<string> OscFicsation
        {
            get
            {
                return new List<string>
                    {
                        "По первой аварии",
                        "По последней аварии"
                    };
            }
        }

        public static List<string> Speed
        {
            get
            {
                return new List<string>
                    {
                        "1200",//0
                        "2400",//1
                        "4800",//2
                        "9600",//3
                        "19200",//4
                        "38400",//5
                        "57600",//6
                        "115200"//7
                    };
            }
        }

        public static List<string> OscLenght
        {
            get
            {
                return new List<string>
                    {
                        "1н.п.  11059 мс",//0
                        "1  5529 мс",//1
                        "2  3686 мс",//2
                        "3  2764 мс",//3
                        "4  2211 мс",//4
                        "5  1843 мс",//5
                        "6  1579 мс",//6
                        "7  1382 мс",//7
                        "8  1228 мс",
                        "9  1105 мс",
                        "10  1005 мс",
                        "11  921 мс",
                        "12  850 мс",
                        "13  789 мс",
                        "14  737 мс",
                        "15  691 мс",
                        "16  650 мс",
                        "17  614 мс",
                        "18  582 мс",
                        "19  552 мс",
                        "20  526 мс",
                        "21  502 мс",
                        "22  480 мс",
                        "23  460 мс",
                        "24  442 мс",
                        "25  425 мс",
                        "26  409 мс",
                        "27  394 мс",
                        "28  381 мс",
                        "29  368 мс",
                        "30  356 мс",
                        "31  345 мс"
                    };
            }
        }

        public static List<string> ChannelsNames
        {
            get
            {
                return new List<string>
                    {
                        "Ia",
                        "Ib",
                        "Ic",
                        "Uк"
                    };
            }
        }

        public static List<string> ChannelsNamesPattern
        {
            get
            {
                return new List<string>
                    {
                        "Ia = {0}",
                        "Ib = {0}",
                        "Ic = {0}",
                        "Uк = {0}"
                    };
            }
        }
        public static List<string> ChargeCapacitor
        {
            get
            {
                return new List<string>
                    {
                        "220 В",
                        "240 В",
                        "260 В",
                        "280 В",
                        "300 В"
                    };
            }
        }


        public static List<string> ModePower
        {
            get
            {
                return new List<string>
                    {
                        "Минимальный",
                        "Средний",
                        "Максимальный"
                    };
            }
        }

        public static List<string> ModePowerForce
        {
            get
            {
                return new List<string>
                    {
                        "Средний",
                        "Максимальный"
                    };
            }
        }
        /// <summary>
        /// Тип реле
        /// </summary>
        public static List<string> RelayType
        {
            get
            {
                return new List<string>
                    {
                        "Повторитель",
                        "Блинкер    "
                    };
            }
        }

        public static List<string> OffOn
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Введено"
                    };
            }
        }
        /// <summary>
        /// Режим защит по напряжению
        /// </summary>
        public static List<string> ExternalDefenseMode
        {
            get
            {
                return new List<string>
                    {
                        "Выведено    ",
                        "Форсирование",
                        "Сигнализация",
                        "Отключение  "
                    };
            }

        }

        public static List<string> Osc
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Пуск по ИО",
                        "Пуск по Защите"
                    };
            }
        }

        public static List<string> FeatureLight
        {
            get
            {
                return new List<string>
                    {
                        "Независимая",
                        "Зависимая"
                    };
            }
        }
        public static List<string> BusDirection
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "От шин",
                        "К шинам"
                    };
            }
        }

        public static List<string> TokParameter
        {
            get
            {
                return new List<string>
                    {
                        "Одна фаза",
                        "Все фазы",
                    };
            }
        }

        public static List<string> CurrentDefenseMode
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Введено",
                        "Сигнализация",
                        "Отключение",
                        "Осциллограф"
                    };
            }
        }
        public static List<string> SignalsBlockingCurrentDefense
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Д1 Инв.",
                        "Д1",
                        "Д2 Инв.",
                        "Д2",
                     
                    };
            }
        }
        /*
        public static List<string> Signals
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Д1 Инв.",
                        "Д1",
                        "Д2 Инв.",
                        "Д2",
                        "Резерв 1",
                        "Резерв 2",
                        "Резерв 3",
                        "Резерв 4",
                        "Резерв 5",
                        "Резерв 6",
                        "Резерв 7",
                        "Резерв 8",
                        "Резерв 9",
                        "Резерв 10",
                        "Резерв 11",
                        "Резерв 12",
                        "I> ИО Инв.",
                        "I> ИО",
                        "I> Инв.",
                        "I>",
                        "I>> ИО Инв.",
                        "I>> ИО",
                        "I>> Инв.",
                        "I>>",
                        "I>>> ИО Инв.",
                        "I>>> ИО",
                        "I>>> Инв.",
                        "I>>>"
                    };
            }
        }
        */

        public static Dictionary<ushort,string> Signals
        {
            get
            {
                return new Dictionary<ushort, string>
                    {
                        {0,"Нет"} ,
                        {1,"Д1 Инв."} ,
                        {2,"Д1"}    ,
                        {3,"Д2 Инв."} ,
                        {4,"Д2"},
                        {17,"I> ИО Инв."},
                        {18,"I> ИО"},
                        {19,"I> Инв."},
                        {20,"I>"},
                        {21,"I>> ИО Инв."},
                        {22,"I>> ИО"},
                        {23,"I>> Инв."},
                        {24,"I>>"},
                        {25,"I>>> ИО Инв."},
                        {26,"I>>> ИО"},
                        {27,"I>>> Инв."},
                        {28,"I>>>"}
                    };
            }
        }

        public static Dictionary<ushort, string> RelaySignals
        {
            get
            {
                return new Dictionary<ushort, string>
                    {
                        {0,"Нет"},
                        {3,"Uк>Uyст Инв."},
                        {4,"Uк>Uyст"},
                        {5,"Неисправность Инв."},
                        {6,"Неисправность"},
                        {7,"Группа уставок - основная"},
                        {8,"Группа уставок - резервная"},
                        {9,"Отключить Инв."},
                        {10,"Отключить"},
                        {11,"Сигнализация Инв."},
                        {12,"Сигнализация"},
                        {13,"Авария Инв."},
                        {14,"Авария"},
                        {15,"Форсирование Инв."},
                        {16,"Форсирование"},
                        {17,"Д1 Инв."},
                        {18,"Д1"},
                        {19,"Д2 Инв."},
                        {20,"Д2"},
                        {33,"I> ИО Инв."},
                        {34,"I> ИО"},
                        {35,"I> СРАБ Инв."},
                        {36,"I> СРАБ"},
                        {37,"I>> ИО Инв."},
                        {38,"I>> ИО"},
                        {39,"I>> СРАБ Инв."},
                        {40,"I>> СРАБ"},
                        {41,"I>>> ИО Инв."},
                        {42,"I>>> ИО"},
                        {43,"I>>> СРАБ Инв."},
                        {44,"I>>> СРАБ"},
                        {49,"ВЗ-1 Инв."},
                        {50,"ВЗ-1"},
                        {51,"ВЗ-2 Инв."},
                        {52,"ВЗ-2"}
                    };
            }
        }
    }
}
