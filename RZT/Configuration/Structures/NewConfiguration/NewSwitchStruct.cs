﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.RZT.Configuration.Structures.NewConfiguration
{
    public class NewSwitchStruct : StructBase
    {
        #region [Fields]

        [Layout(0)] private ushort _time;
        [Layout(1)] private ushort _block;
        [Layout(2)] private ushort _config;
        [Layout(3)] private ushort _tok;
        #endregion [Public fields]

        #region [Properties]
        [BindingProperty(0)]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
        [BindingProperty(1)]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SignalsBlockingCurrentDefense); }
            set { this._block = Validator.Set(value, StringsConfig.SignalsBlockingCurrentDefense); }
        }

        [BindingProperty(2)]
        public bool Break
        {
            get { return Common.GetBit(_config, 0); }
            set { _config = Common.SetBit(_config, 0, value); }
        }
        [BindingProperty(3)]
        public bool HighResist
        {
            get { return Common.GetBit(_config, 1); }
            set { _config = Common.SetBit(_config, 1, value); }
        }
        [BindingProperty(4)]
        public bool BlockOnTok
        {
            get { return Common.GetBit(_config, 2); }
            set { _config = Common.SetBit(_config, 2, value); }
        }
        [BindingProperty(5)]
        public double SwitchTok
        {
            get { return ValuesConverterCommon.GetUstavka40(_tok); }
            set { _tok = ValuesConverterCommon.SetUstavka40(value); }
        }
        [BindingProperty(6)]
        public double SwitchTok8
        {
            get { return ValuesConverterCommon.GetUstavka8(_tok); }
            set { _tok = ValuesConverterCommon.SetUstavka8(value); }
        }

        public void GetRecalculateTok(bool isNewVers)
        {
            if (isNewVers)
            {
                SwitchTok8 = SwitchTok8/Math.Sqrt(3);
            }
            else
            {
                SwitchTok = SwitchTok / Math.Sqrt(3);
            }
        }

        public void SetRecalculateTok(bool isNewVers)
        {
            if (isNewVers)
            {
                SwitchTok8 = SwitchTok8 * Math.Sqrt(3);
            }
            else
            {
                SwitchTok = SwitchTok * Math.Sqrt(3);
            }
        }
        #endregion
    }
}
