﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.RZT.Configuration.Structures.NewConfiguration
{
    public class NewMeasuringStruct : StructBase
    {
        #region [Public fields]
        [Layout(0)] private ushort _charge;
        [Layout(1)] private ushort _voltage;
        [Layout(2)] private ushort _addConfig;
        [Layout(3)] private ushort _reserve;
        [Layout(4)] private ushort _tt;
        #endregion [Public fields]

        [BindingProperty(0)]
        public string Charge
        {
            get { return Validator.Get(this._charge, StringsConfig.ChargeCapacitor); }
            set { this._charge = Validator.Set(value, StringsConfig.ChargeCapacitor); }
        }
        [BindingProperty(1)]
        public double Voltage
        {
            get { return Math.Round(this._voltage * 400.0 / 65535.0, 1); }
            set { this._voltage = (ushort)Math.Round(value / 400.0 * 65535.0); }
        }

        [BindingProperty(2)]
        public bool Forsing
        {
            get { return Common.GetBit(_addConfig, 0); }
            set { _addConfig = Common.SetBit(_addConfig, 0, value); }
        }
        [BindingProperty(3)]
        public bool AlarmSign
        {
            get { return Common.GetBit(_addConfig, 1); }
            set { _addConfig = Common.SetBit(_addConfig, 1, value); }
        }
        [BindingProperty(4)]
        public bool AlarmProt
        {
            get { return Common.GetBit(_addConfig, 2); }
            set { _addConfig = Common.SetBit(_addConfig, 2, value); }
        }
        [BindingProperty(5)]
        public bool AlarmOff
        {
            get { return Common.GetBit(_addConfig, 3); }
            set { _addConfig = Common.SetBit(_addConfig, 3, value); }
        }
        [BindingProperty(6)]
        public bool JournIo
        {
            get { return Common.GetBit(_addConfig, 4); }
            set { _addConfig = Common.SetBit(_addConfig, 4, value); }
        }
        [BindingProperty(7)]
        public bool JournProt
        {
            get { return Common.GetBit(_addConfig, 5); }
            set { _addConfig = Common.SetBit(_addConfig, 5, value); }
        }
        [BindingProperty(8)]
        public bool JournOff
        {
            get { return Common.GetBit(_addConfig, 6); }
            set { _addConfig = Common.SetBit(_addConfig, 6, value); }
        }

        [BindingProperty(9)]
        public ushort Tt
        {
            get { return _tt; }
            set { _tt = value; }
        }

        public bool Type
        {
            get { return Common.GetBit(_addConfig, 7); }
            set { _addConfig = Common.SetBit(_addConfig, 7, value); }
        }
    }
}
