﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.RZT.Configuration.Structures.CurrentDefenses;
using BEMN.RZT.Configuration.Structures.ExternalDefences;
using BEMN.RZT.Configuration.Structures.Relay;

namespace BEMN.RZT.Configuration.Structures.NewConfiguration
{
    public class NewConfigurationStruct : StructBase
    {
        [Layout(0)] private NewMeasuringStruct _measuring;
        [Layout(1)] private ExternalSignalsStruct _externalSignals;
        [Layout(2)] private NewSwitchStruct _switch;
        [Layout(3)] private AllOutputRelaysStruct _relays;
        [Layout(4)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(5)] private AllDefenseStuct _allDefense;
        [Layout(6, Count = 8)] private ushort[] _resevArray;
        [Layout(7)] private SystemStruct _system;

        [BindingProperty(0)]
        public NewMeasuringStruct Measuring
        {
            get { return _measuring; }
            set { _measuring = value; }
        }
        [BindingProperty(1)]
        public ExternalSignalsStruct ExternalSignals
        {
            get { return _externalSignals; }
            set { _externalSignals = value; }
        }
        [BindingProperty(2)]
        public NewSwitchStruct Switch
        {
            get { return _switch; }
            set { _switch = value; }
        }
        [BindingProperty(3)]
        public AllOutputRelaysStruct Relays
        {
            get { return _relays; }
            set { _relays = value; }
        }
        [BindingProperty(4)]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return _allExternalDefenses; }
            set { _allExternalDefenses = value; }
        }
        [BindingProperty(5)]
        public AllDefenseStuct AllDefense
        {
            get { return _allDefense; }
            set { _allDefense = value; }
        }
        [BindingProperty(6)]
        public SystemStruct System
        {
            get { return _system; }
            set { _system = value; }
        }

        public void GetRecalculatedSetpoint(bool isNewVers)
        {
            _switch.GetRecalculateTok(isNewVers);
            foreach (CurrentDefenseStruct def in _allDefense.Main.Rows)
            {
                def.GetRecalculateTok(isNewVers);
            }
            foreach (CurrentDefenseStruct def in _allDefense.Reserve.Rows)
            {
                def.GetRecalculateTok(isNewVers);
            }
        }
        public void SetRecalculatedSetpoint(bool isNewVers)
        {
            _switch.SetRecalculateTok(isNewVers);
            foreach (CurrentDefenseStruct def in _allDefense.Main.Rows)
            {
                def.SetRecalculateTok(isNewVers);
            }
            foreach (CurrentDefenseStruct def in _allDefense.Reserve.Rows)
            {
                def.SetRecalculateTok(isNewVers);
            }
        }
    }
}
