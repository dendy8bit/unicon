﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.RZT.Configuration.Structures
{
    public class MeasuringConfigStruct : StructBase
    {
        #region [Public fields]
        [Layout(0)] private ushort _charge;
        [Layout(1)] private ushort _voltage;
        [Layout(2)] private ushort _mode;
        [Layout(3)] private ushort _modeForse;
        [Layout(4)] private ushort _tt;
        #endregion [Public fields]

        [BindingProperty(0)]
        public string Charge
        {
            get { return Validator.Get(this._charge,StringsConfig.ChargeCapacitor); }
            set { this._charge =Validator.Set(value,StringsConfig.ChargeCapacitor) ; }
        }
        [BindingProperty(1)]
        public double Voltage
        {
            get { return Math.Round(this._voltage*400.0/65535.0,1); }
            set { this._voltage = (ushort) Math.Round(value/400.0*65535.0); }
        }
        [BindingProperty(2)]
        public string ModePower
        {
            get { return Validator.Get(this._mode,StringsConfig.ModePower, 0, 1) ; }
            set { this._mode = Validator.Set(value, StringsConfig.ModePower, this._mode, 0, 1); }
        }
        [BindingProperty(3)]
        public string ModePowerForse
        {
            get
            {
                if (this._modeForse == 0)
                {
                    return StringsConfig.ModePowerForce[0];
                }
                return Validator.Get((ushort) (this._modeForse -1), StringsConfig.ModePowerForce);
            }
            set { this._modeForse = (ushort) (Validator.Set(value, StringsConfig.ModePowerForce)+1); }
        }
        [BindingProperty(4)]
        public ushort Tt
        {
            get { return _tt; }
            set { _tt = value; }
        }

        public bool Type
        {
            get { return Common.GetBit(_mode, 7); }
            set { _mode = Common.SetBit(_mode, 7, value); }
        }
    }
}
