﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.RZT.Configuration.Structures.Relay
{
    /// <summary>
    /// выходные реле(9)
    /// </summary>
    public class AllOutputRelaysStruct : StructBase, IDgvRowsContainer<RelayStruct>
    {
        public const int RELAY_COUNT = 4;
        [Layout(0, Count = RELAY_COUNT)]
        private RelayStruct[] _relays;





        public RelayStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }
    }
}
