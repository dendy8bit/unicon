﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.RZT.Configuration.Structures.Relay
{
    /// <summary>
    /// Выходное реле
    /// </summary>
    public class RelayStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _type;
        [Layout(1)]
        private ushort _waitTime; 
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип реле
        /// </summary>
        [BindingProperty(0)]
        public string RelayType
        {
            get { return Validator.Get(this._type, StringsConfig.RelayType, 15); }
            set { this._type = Validator.Set(value, StringsConfig.RelayType, this._type, 15); }
        }

        /// <summary>
        /// Сигнал реле
        /// </summary>
        [BindingProperty(1)]
        public string Signal
        {
            get
            {
             
                return Validator.Get(this._type,StringsConfig.RelaySignals, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9) ;
            }
            set
            {
               
                this._type = Validator.Set(value,StringsConfig.RelaySignals,this._type, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9) ;
            }
        }
        /// <summary>
        /// Импульс
        /// </summary>
        [BindingProperty(2)]
        public int WaitTime
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._waitTime);
            }
            set
            {
                this._waitTime = ValuesConverterCommon.SetWaitTime(value);
            }
        } 
        #endregion [Properties]



    }
}