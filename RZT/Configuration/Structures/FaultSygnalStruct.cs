﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.RZT.Configuration.Structures
{
    public class IndicatorFaultConfigStruct:StructBase, IBitField
    {
        [Layout(0)]
        private ushort _config;

        public BitArray Bits
        {
            get
            {
                var result = new bool[5];
                for (int i = 0; i < 5; i++)
                {
                    result[i] = Common.GetBit(this._config, i);
                }
                return new BitArray(result);
            }
            set
            {
                for (int i = 0; i < 5; i++)
                {
                    this._config = Common.SetBit(this._config, i, value[i]);
                }
            }
        }
    }

    public class RelayFaultConfigStruct : StructBase, IBitField
    {
        [Layout(0)]
        private ushort _config;

        public BitArray Bits
        {
            get
            {
                var result = new bool[4];
                for (int i = 0; i < 5; i++)
                {

                    result = new[]
                        {
                            Common.GetBit(this._config, 0),
                            Common.GetBit(this._config, 1),
                            Common.GetBit(this._config, 2),
                            Common.GetBit(this._config, 4),
                        };
                }
                return new BitArray(result);
            }
            set
            {
                this._config = Common.SetBit(this._config, 0, value[0]);
                this._config = Common.SetBit(this._config, 1, value[1]);
                this._config = Common.SetBit(this._config, 2, value[2]);
                this._config = Common.SetBit(this._config, 3, false);
                this._config = Common.SetBit(this._config, 4, value[3]);
            }
        }
    }

    /// <summary>
    /// параметры сигнала НЕИСПРАВНОСТЬ
    /// </summary>
    public class IndicatorFaultSygnalStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)]
        private IndicatorFaultConfigStruct _config;
        [Layout(1)]
        private ushort _wait;

        #endregion [Private fields]
        [BindingProperty(0)]
        public IndicatorFaultConfigStruct Config
        {
            get { return _config; }
            set { _config = value; }
        }

        /// <summary>
        /// Импульс неисправности
        /// </summary>
        [BindingProperty(1)]
        public int WaitTime
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._wait);
            }
            set
            {
                this._wait = ValuesConverterCommon.SetWaitTime(value);
            }
        }

      
    }

    /// <summary>
    /// параметры сигнала НЕИСПРАВНОСТЬ
    /// </summary>
    public class RelayFaultSygnalStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)]
        private RelayFaultConfigStruct _config;
        [Layout(1)]
        private ushort _wait;

        #endregion [Private fields]
        [BindingProperty(0)]
        public RelayFaultConfigStruct Config
        {
            get { return _config; }
            set { _config = value; }
        }

        /// <summary>
        /// Импульс неисправности
        /// </summary>
        [BindingProperty(1)]
        public int WaitTime
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._wait);
            }
            set
            {
                this._wait = ValuesConverterCommon.SetWaitTime(value);
            }
        }


    }
}