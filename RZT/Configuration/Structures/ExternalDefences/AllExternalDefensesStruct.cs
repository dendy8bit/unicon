﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.RZT.Configuration.Structures.ExternalDefences
{
    /// <summary>
    /// конфигурации внешних защит 
    /// </summary>
    public class AllExternalDefensesStruct : StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        public const int COUNT = 4;
        [Layout(0, Count = COUNT)]
        private ExternalDefenseStruct[] _externalDefenses;





        public ExternalDefenseStruct[] Rows
        {
            get
            {
                return _externalDefenses;
            }
            set
            {
                _externalDefenses = value;
            }
        }
    }
}