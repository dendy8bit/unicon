﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.RZT.Configuration.Structures.CurrentDefenses
{

    public class GroupDefenseStruct : StructBase, IDgvRowsContainer<CurrentDefenseStruct>
    {
         
        [Layout(0 ,Count = 4)]
        private CurrentDefenseStruct[] _currentDefenses;



        public CurrentDefenseStruct[] Rows
        {
            get
            {
                return this._currentDefenses;
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    this._currentDefenses[i] = value[i].Clone<CurrentDefenseStruct>();
                }
                
            }
        }
    }
}
