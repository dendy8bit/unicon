﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.RZT.Configuration.Structures.CurrentDefenses
{

    public class AllDefenseStuct : StructBase, ISetpointContainer<GroupDefenseStruct>
    {
        [Layout(0)]
        public GroupDefenseStruct Main;
        [Layout(1)]
        public GroupDefenseStruct Reserve;




        public GroupDefenseStruct[] Setpoints
        {
            get
            {
                return new[]
                    {
                        this.Main.Clone<GroupDefenseStruct>(),
                        this.Reserve.Clone<GroupDefenseStruct>()
                    };
            }
            set
            {
                this.Main = value[0].Clone<GroupDefenseStruct>();
                this.Reserve = value[1].Clone<GroupDefenseStruct>();
            }
        }
    }
}
