﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.RZT.Configuration.Structures
{
    public class OffStruct : StructBase
    {
        #region [Public fields]

        [Layout(0)]
        private ushort _time;
        [Layout(1)]
        private ushort _block;
        [Layout(2)]
        private ushort _control;
        [Layout(3)]
        private ushort _reserv;
        #endregion [Public fields]

         [BindingProperty(0)]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
        [BindingProperty(1)]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SignalsBlockingCurrentDefense); }
            set { this._block = Validator.Set(value, StringsConfig.SignalsBlockingCurrentDefense); }
        }
        [BindingProperty(2)]
        public string Control
        {
            get { return Validator.Get(this._control,StringsConfig.OffOn); }
            set { this._control = Validator.Set(value,StringsConfig.OffOn) ; }
        }
  
    }
}
