﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.RZT.Configuration.Structures
{
    public class ExternalSignalsStruct :StructBase
    {
        #region [Public fields]

        [Layout(0)]
        private ushort _resetAlarm;
        [Layout(1)]
        private ushort _groupOfSetpoints;
        [Layout(2)]
        private ushort _pulseTime;
        [Layout(3)]
        private IndicatorFaultSygnalStruct _faultIndicator; //индикатор НЕИСПРАВНОСТЬ           4 байт
        [Layout(4)]
        private RelayFaultSygnalStruct _faultRelay; //реле НЕИСПРАВНОСТЬ           4 байт
        #endregion [Public fields]

        [BindingProperty(0)]
        public int PulseTime
        {
            get { return ValuesConverterCommon.GetWaitTime(_pulseTime) ; }
            set { _pulseTime = ValuesConverterCommon.SetWaitTime(value); }
        }
        [BindingProperty(1)]
        public string ResetAlarm
        {
            get { return Validator.Get(this._resetAlarm, StringsConfig.SignalsBlockingCurrentDefense); }
            set { this._resetAlarm = Validator.Set(value, StringsConfig.SignalsBlockingCurrentDefense); }
        }
        [BindingProperty(2)]
        public string GroupOfSetpoints
        {
            get { return Validator.Get(this._groupOfSetpoints, StringsConfig.SignalsBlockingCurrentDefense); }
            set { this._groupOfSetpoints = Validator.Set(value, StringsConfig.SignalsBlockingCurrentDefense); }
        }


         [BindingProperty(3)]
        public IndicatorFaultSygnalStruct FaultIndicator
        {
            get { return _faultIndicator; }
            set { _faultIndicator = value; }
        }
         [BindingProperty(4)]
         public RelayFaultSygnalStruct FaultRelay
        {
            get { return _faultRelay; }
            set { _faultRelay = value; }
        }
    }
}

