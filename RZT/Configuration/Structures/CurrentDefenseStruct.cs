﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.RZT.Configuration.Structures
{
    public class CurrentDefenseStruct : StructBase
    {
        #region [Public fields]
        [Layout(0)]
        private ushort _mode;
        [Layout(1)]
        private ushort _block;
        [Layout(2)]
        private ushort _ustavka;
        [Layout(3)]
        private ushort _time;
        [Layout(4)]
        private ushort _res1;
        [Layout(5)]
        private ushort _res2;
        #endregion [Public fields]

        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._mode, StringsConfig.ExternalDefenseMode, 0, 1, 2); }
            set
            {
                this._mode = Validator.Set(value, StringsConfig.ExternalDefenseMode, this._mode, 0, 1, 2);
            }
        }

        [BindingProperty(1)]
        public string Block
        {
            get { return Validator.Get(this._block,StringsConfig.SignalsBlockingCurrentDefense); }
            set { this._block = Validator.Set(value,StringsConfig.SignalsBlockingCurrentDefense) ; }
        }

       
        [BindingProperty(2)]
        public string Parameter
        {
            get { return Common.GetBit(_mode, 8) ? StringsConfig.TokParameter[1] : StringsConfig.TokParameter[0]; }
            set
            {
                var b = StringsConfig.TokParameter[0] != value;
                _mode = Common.SetBit(_mode, 8, b);
            }
        }

        [BindingProperty(3)]
        public double Ustavka40
        {
            get { return ValuesConverterCommon.GetUstavka40(_ustavka); }
            set { _ustavka = ValuesConverterCommon.SetUstavka40(value); }
        }
        [BindingProperty(4)]
        public double Ustavka8
        {
            get { return ValuesConverterCommon.GetUstavka8(_ustavka); }
            set { _ustavka = ValuesConverterCommon.SetUstavka8(value); }
        }

        public bool Type { get; set; }

        [BindingProperty(5)]
        public string Feature
        {
            get
            {
                return Common.GetBit(_mode, 12) ? StringsConfig.FeatureLight[1] : StringsConfig.FeatureLight[0];
                
            }
            set
            {
                bool b = StringsConfig.FeatureLight[0] != value;


                _mode = Common.SetBit(_mode, 12, b);
            }
        }

        [BindingProperty(6)]
        public int WorkTime
        {
            get
            {

                if (Feature == StringsConfig.FeatureLight[0])
                    return ValuesConverterCommon.GetWaitTime(_time);
                else
                    return _time;

            }
            set
            {
                if (Feature == StringsConfig.FeatureLight[0])
                    _time = ValuesConverterCommon.SetWaitTime(value);
                else
                {
                    _time = (ushort) value;
                }
            }
        }
        
        [BindingProperty(7)]
        public string Osc
        {
            get
            {
                return Validator.Get(_mode, StringsConfig.Osc, 3, 4);

            }
            set
            {
                _mode = Validator.Set(value, StringsConfig.Osc, _mode, 3, 4);
            }
        }

        public void GetRecalculateTok(bool isNewVers)
        {
            if (isNewVers)
            {
                Ustavka8 = Ustavka8 / Math.Sqrt(3);
            }
            else
            {
                Ustavka40 = Ustavka40 / Math.Sqrt(3);
            }
        }

        public void SetRecalculateTok(bool isNewVers)
        {
            if (isNewVers)
            {
                Ustavka8 = Ustavka8 * Math.Sqrt(3);
            }
            else
            {
                Ustavka40 = Ustavka40 * Math.Sqrt(3);
            }
        }
    }
}
