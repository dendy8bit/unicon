﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.RZT.Configuration.Structures
{
    public class SystemStruct : StructBase
    {
        [Layout(0)]
        private ushort _address;
        [Layout(1)]
        private ushort _speed;
        [Layout(2)]
        private ushort _delay;
        [Layout(3)]
        private ushort _status;
        [Layout(4)]
        private ushort _osc;


        [BindingProperty(0)]
        public string Address
        {
            get { return this._address.ToString(); }
            set { this._address = ushort.Parse(value) ; }
        }
        [BindingProperty(1)]
        public string Speed
        {
            get { return Validator.Get(this._speed,StringsConfig.Speed); }
            set { this._speed = Validator.Set(value,StringsConfig.Speed); }
        }

        /// <summary>
        /// Длительность
        /// </summary>
        [BindingProperty(2)]
        public string Duration
        {
            get { return Validator.Get(this._osc, StringsConfig.OscLenght, 0, 1, 2, 3, 4, 5, 6); }
            set { this._osc = Validator.Set(value, StringsConfig.OscLenght, this._osc, 0, 1, 2, 3, 4, 5, 6); }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(3)]
        public string Fixation
        {
            get { return Validator.Get(this._osc, StringsConfig.OscFicsation, 7); }
            set { this._osc = Validator.Set(value, StringsConfig.OscFicsation, this._osc, 7); }
        }

        /// <summary>
        /// Длительность предзаписи
        /// </summary>
        [BindingProperty(4)]
        public ushort PreRecording
        {
            get { return (ushort)(Common.GetBits(this._osc, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._osc = Common.SetBits(this._osc, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
        /// <summary>
        /// Rs485
        /// </summary>
        [BindingProperty(5)]
        public bool Rs485
        {
            get { return Common.GetBit(this._status, 0); }
            set { this._status = Common.SetBit(this._status, 0, value); }
        }
        /// <summary>
        /// Реле неисправноть
        /// </summary>
        [BindingProperty(6)]
        public bool RelayFaultBool
        {
            get { return Common.GetBit(this._status, 1); }
            set { this._status = Common.SetBit(this._status, 1, value); }
        }

      
    }
}
