﻿namespace BEMN.RZT.Configuration
{
    partial class RztConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this._voltageChargeCapacitorBox = new System.Windows.Forms.MaskedTextBox();
            this._chargeCapacitorBox = new System.Windows.Forms.ComboBox();
            this._modePower = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._modePowerForse = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._ttBox = new System.Windows.Forms.MaskedTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._setpointsGroupBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this._alarmBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this._dispepairRelayCheckList = new System.Windows.Forms.CheckedListBox();
            this.label23 = new System.Windows.Forms.Label();
            this._outputSignalsDispepairTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._relayFaultCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this._dispepairIndicatorCheckList = new System.Windows.Forms.CheckedListBox();
            this.ControllSwitchGrBox = new System.Windows.Forms.GroupBox();
            this._controlBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._blockingBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this._pulseBox = new System.Windows.Forms.MaskedTextBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._transformSchemeGrBox = new System.Windows.Forms.GroupBox();
            this._starType = new System.Windows.Forms.RadioButton();
            this._triangleType = new System.Windows.Forms.RadioButton();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.AddSettingsGrBox = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this._forsingCheck = new System.Windows.Forms.CheckBox();
            this.ControllSwitchGrBox2 = new System.Windows.Forms.GroupBox();
            this._blockingBox2 = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this._tokCheck = new System.Windows.Forms.CheckBox();
            this._highCheck = new System.Windows.Forms.CheckBox();
            this._breakCheck = new System.Windows.Forms.CheckBox();
            this.JsSettingsGrBox = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this._journIo = new System.Windows.Forms.CheckBox();
            this._journOff = new System.Windows.Forms.CheckBox();
            this._journProt = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this._pulseTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._fixsation = new System.Windows.Forms.ComboBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this._speed = new System.Windows.Forms.ComboBox();
            this._address = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._rs485CheckBox = new System.Windows.Forms.CheckBox();
            this.ModePowerGrBox = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._switchTok = new System.Windows.Forms.MaskedTextBox();
            this.labelTok = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.IndicatorAlarmGrBox = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._alarmSign = new System.Windows.Forms.CheckBox();
            this._alarmOff = new System.Windows.Forms.CheckBox();
            this._alarmProt = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._mainRadioButton = new System.Windows.Forms.RadioButton();
            this._reserveRadioButton = new System.Windows.Forms.RadioButton();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._ustSrab40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ustSrab8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseOSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscExDefenseColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._resetConfigBut = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.ControllSwitchGrBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this._transformSchemeGrBox.SuspendLayout();
            this.AddSettingsGrBox.SuspendLayout();
            this.ControllSwitchGrBox2.SuspendLayout();
            this.JsSettingsGrBox.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.ModePowerGrBox.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.IndicatorAlarmGrBox.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _voltageChargeCapacitorBox
            // 
            this._voltageChargeCapacitorBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._voltageChargeCapacitorBox.Location = new System.Drawing.Point(213, 65);
            this._voltageChargeCapacitorBox.Name = "_voltageChargeCapacitorBox";
            this._voltageChargeCapacitorBox.Size = new System.Drawing.Size(60, 20);
            this._voltageChargeCapacitorBox.TabIndex = 27;
            this._voltageChargeCapacitorBox.Tag = "1500";
            this._voltageChargeCapacitorBox.Text = "0";
            this._voltageChargeCapacitorBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _chargeCapacitorBox
            // 
            this._chargeCapacitorBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._chargeCapacitorBox.FormattingEnabled = true;
            this._chargeCapacitorBox.Location = new System.Drawing.Point(197, 27);
            this._chargeCapacitorBox.Name = "_chargeCapacitorBox";
            this._chargeCapacitorBox.Size = new System.Drawing.Size(76, 21);
            this._chargeCapacitorBox.TabIndex = 37;
            this._chargeCapacitorBox.SelectedIndexChanged += new System.EventHandler(this._chargeCapacitorBox_SelectedIndexChanged);
            // 
            // _modePower
            // 
            this._modePower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modePower.FormattingEnabled = true;
            this._modePower.Location = new System.Drawing.Point(163, 16);
            this._modePower.Name = "_modePower";
            this._modePower.Size = new System.Drawing.Size(110, 21);
            this._modePower.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "в нормальном режиме";
            // 
            // _modePowerForse
            // 
            this._modePowerForse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modePowerForse.FormattingEnabled = true;
            this._modePowerForse.Location = new System.Drawing.Point(163, 43);
            this._modePowerForse.Name = "_modePowerForse";
            this._modePowerForse.Size = new System.Drawing.Size(110, 21);
            this._modePowerForse.TabIndex = 41;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 40;
            this.label2.Text = "в режиме форсирования";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "Iн - ном. первичный ток ТТ, А";
            // 
            // _ttBox
            // 
            this._ttBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ttBox.Location = new System.Drawing.Point(239, 26);
            this._ttBox.Name = "_ttBox";
            this._ttBox.Size = new System.Drawing.Size(60, 20);
            this._ttBox.TabIndex = 42;
            this._ttBox.Tag = "1500";
            this._ttBox.Text = "0";
            this._ttBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 703);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(786, 22);
            this.statusStrip1.TabIndex = 44;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 90;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _setpointsGroupBox
            // 
            this._setpointsGroupBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsGroupBox.FormattingEnabled = true;
            this._setpointsGroupBox.Location = new System.Drawing.Point(185, 64);
            this._setpointsGroupBox.Name = "_setpointsGroupBox";
            this._setpointsGroupBox.Size = new System.Drawing.Size(76, 21);
            this._setpointsGroupBox.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "Перекл. на рез. гр. уставок";
            // 
            // _alarmBox
            // 
            this._alarmBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._alarmBox.FormattingEnabled = true;
            this._alarmBox.Location = new System.Drawing.Point(185, 24);
            this._alarmBox.Name = "_alarmBox";
            this._alarmBox.Size = new System.Drawing.Size(76, 21);
            this._alarmBox.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Сброс сигнализации";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Controls.Add(this._relayFaultCheckBox);
            this.groupBox3.Location = new System.Drawing.Point(12, 399);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(348, 171);
            this.groupBox3.TabIndex = 49;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Реле \"Неисправность\"";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._dispepairRelayCheckList);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this._outputSignalsDispepairTimeBox);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(7, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 148);
            this.panel1.TabIndex = 61;
            // 
            // _dispepairRelayCheckList
            // 
            this._dispepairRelayCheckList.CheckOnClick = true;
            this._dispepairRelayCheckList.FormattingEnabled = true;
            this._dispepairRelayCheckList.Items.AddRange(new object[] {
            "Аппаратная",
            "Программная",
            "Низкий заряд конденсатора",
            "Цепи отключения"});
            this._dispepairRelayCheckList.Location = new System.Drawing.Point(6, 32);
            this._dispepairRelayCheckList.Name = "_dispepairRelayCheckList";
            this._dispepairRelayCheckList.Size = new System.Drawing.Size(319, 64);
            this._dispepairRelayCheckList.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(153, 120);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(21, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "мс";
            // 
            // _outputSignalsDispepairTimeBox
            // 
            this._outputSignalsDispepairTimeBox.Location = new System.Drawing.Point(6, 117);
            this._outputSignalsDispepairTimeBox.Name = "_outputSignalsDispepairTimeBox";
            this._outputSignalsDispepairTimeBox.Size = new System.Drawing.Size(141, 20);
            this._outputSignalsDispepairTimeBox.TabIndex = 18;
            this._outputSignalsDispepairTimeBox.Tag = "3000000";
            this._outputSignalsDispepairTimeBox.Text = "0";
            this._outputSignalsDispepairTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Типы неисправностей";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Твозвр, мс";
            // 
            // _relayFaultCheckBox
            // 
            this._relayFaultCheckBox.AutoSize = true;
            this._relayFaultCheckBox.Location = new System.Drawing.Point(134, 0);
            this._relayFaultCheckBox.Name = "_relayFaultCheckBox";
            this._relayFaultCheckBox.Size = new System.Drawing.Size(15, 14);
            this._relayFaultCheckBox.TabIndex = 61;
            this._relayFaultCheckBox.UseVisualStyleBackColor = true;
            this._relayFaultCheckBox.CheckedChanged += new System.EventHandler(this._relayFaultCheckBox_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this._dispepairIndicatorCheckList);
            this.groupBox1.Location = new System.Drawing.Point(366, 399);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 134);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Индикатор \"Неисправность\"";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Типы неисправностей";
            // 
            // _dispepairIndicatorCheckList
            // 
            this._dispepairIndicatorCheckList.CheckOnClick = true;
            this._dispepairIndicatorCheckList.FormattingEnabled = true;
            this._dispepairIndicatorCheckList.Items.AddRange(new object[] {
            "Аппаратная",
            "Программная",
            "Низкий заряд конденсатора",
            "Недостаточное питание для выполнения функций защиты",
            "Цепи отключения"});
            this._dispepairIndicatorCheckList.Location = new System.Drawing.Point(9, 46);
            this._dispepairIndicatorCheckList.Name = "_dispepairIndicatorCheckList";
            this._dispepairIndicatorCheckList.Size = new System.Drawing.Size(324, 79);
            this._dispepairIndicatorCheckList.TabIndex = 5;
            // 
            // ControllSwitchGrBox
            // 
            this.ControllSwitchGrBox.Controls.Add(this._controlBox);
            this.ControllSwitchGrBox.Controls.Add(this.label8);
            this.ControllSwitchGrBox.Controls.Add(this._blockingBox);
            this.ControllSwitchGrBox.Controls.Add(this.label9);
            this.ControllSwitchGrBox.Location = new System.Drawing.Point(21, 262);
            this.ControllSwitchGrBox.Name = "ControllSwitchGrBox";
            this.ControllSwitchGrBox.Size = new System.Drawing.Size(284, 82);
            this.ControllSwitchGrBox.TabIndex = 51;
            this.ControllSwitchGrBox.TabStop = false;
            this.ControllSwitchGrBox.Text = "Контроль цепи отключения";
            // 
            // _controlBox
            // 
            this._controlBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._controlBox.FormattingEnabled = true;
            this._controlBox.Location = new System.Drawing.Point(185, 16);
            this._controlBox.Name = "_controlBox";
            this._controlBox.Size = new System.Drawing.Size(76, 21);
            this._controlBox.TabIndex = 54;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 53;
            this.label8.Text = "Режим";
            // 
            // _blockingBox
            // 
            this._blockingBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingBox.FormattingEnabled = true;
            this._blockingBox.Location = new System.Drawing.Point(185, 49);
            this._blockingBox.Name = "_blockingBox";
            this._blockingBox.Size = new System.Drawing.Size(76, 21);
            this._blockingBox.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Блокировка*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(397, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(220, 13);
            this.label11.TabIndex = 50;
            this.label11.Text = "Импульс команды отключения (Тимп), мс";
            // 
            // _pulseBox
            // 
            this._pulseBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._pulseBox.Location = new System.Drawing.Point(623, 26);
            this._pulseBox.Name = "_pulseBox";
            this._pulseBox.Size = new System.Drawing.Size(60, 20);
            this._pulseBox.TabIndex = 49;
            this._pulseBox.Tag = "1500";
            this._pulseBox.Text = "0";
            this._pulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            this._outputReleGrid.Location = new System.Drawing.Point(6, 302);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(418, 71);
            this._outputReleGrid.TabIndex = 52;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 24;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Items.AddRange(new object[] {
            "Повторитель",
            "Блинкер",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "Твозвр, мс";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(167, 671);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 57;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(12, 671);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(151, 23);
            this._readConfigBut.TabIndex = 56;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(640, 671);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(136, 23);
            this._saveConfigBut.TabIndex = 59;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(485, 671);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(149, 23);
            this._loadConfigBut.TabIndex = 58;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.FileName = "РЗТ-110";
            this._saveConfigurationDlg.Filter = "Уставки РЗТ-110 (*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для РЗТ-100";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.FileName = "РЗТ-110";
            this._openConfigurationDlg.Filter = "Уставки РЗТ-110 (*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для РЗТ-110";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.ContextMenuStrip = this.contextMenu;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.MinimumSize = new System.Drawing.Size(786, 665);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(786, 665);
            this.tabControl1.TabIndex = 60;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(212, 22);
            this.clearSetpointsItem.Text = "Обнулить уставки";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._transformSchemeGrBox);
            this.tabPage1.Controls.Add(this.AddSettingsGrBox);
            this.tabPage1.Controls.Add(this.ControllSwitchGrBox2);
            this.tabPage1.Controls.Add(this.JsSettingsGrBox);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this._pulseTextBox);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.ModePowerGrBox);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this._pulseBox);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.ControllSwitchGrBox);
            this.tabPage1.Controls.Add(this._switchTok);
            this.tabPage1.Controls.Add(this.labelTok);
            this.tabPage1.Controls.Add(this._ttBox);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(778, 639);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Общие настройки";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _transformSchemeGrBox
            // 
            this._transformSchemeGrBox.Controls.Add(this._starType);
            this._transformSchemeGrBox.Controls.Add(this._triangleType);
            this._transformSchemeGrBox.Controls.Add(this.label36);
            this._transformSchemeGrBox.Controls.Add(this.label35);
            this._transformSchemeGrBox.Location = new System.Drawing.Point(21, 52);
            this._transformSchemeGrBox.Name = "_transformSchemeGrBox";
            this._transformSchemeGrBox.Size = new System.Drawing.Size(286, 62);
            this._transformSchemeGrBox.TabIndex = 65;
            this._transformSchemeGrBox.TabStop = false;
            this._transformSchemeGrBox.Text = "Схема соединения ТТ";
            // 
            // _starType
            // 
            this._starType.AutoSize = true;
            this._starType.Location = new System.Drawing.Point(6, 19);
            this._starType.Name = "_starType";
            this._starType.Size = new System.Drawing.Size(62, 17);
            this._starType.TabIndex = 64;
            this._starType.Text = "Звезда";
            this._starType.UseVisualStyleBackColor = true;
            // 
            // _triangleType
            // 
            this._triangleType.AutoSize = true;
            this._triangleType.Location = new System.Drawing.Point(148, 19);
            this._triangleType.Name = "_triangleType";
            this._triangleType.Size = new System.Drawing.Size(90, 17);
            this._triangleType.TabIndex = 64;
            this._triangleType.TabStop = true;
            this._triangleType.Text = "Треугольник";
            this._triangleType.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(145, 39);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(108, 13);
            this.label36.TabIndex = 43;
            this.label36.Text = "Iср.вт = 1,732*Iуст*5";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 39);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 13);
            this.label35.TabIndex = 43;
            this.label35.Text = "Iср.вт = Iуст*5";
            // 
            // AddSettingsGrBox
            // 
            this.AddSettingsGrBox.Controls.Add(this.label27);
            this.AddSettingsGrBox.Controls.Add(this._forsingCheck);
            this.AddSettingsGrBox.Location = new System.Drawing.Point(362, 262);
            this.AddSettingsGrBox.Name = "AddSettingsGrBox";
            this.AddSettingsGrBox.Size = new System.Drawing.Size(193, 88);
            this.AddSettingsGrBox.TabIndex = 63;
            this.AddSettingsGrBox.TabStop = false;
            this.AddSettingsGrBox.Text = "Уровень отбора мощности";
            this.AddSettingsGrBox.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(24, 39);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(109, 13);
            this.label27.TabIndex = 42;
            this.label27.Text = "в режиме ожидания";
            // 
            // _forsingCheck
            // 
            this._forsingCheck.AutoSize = true;
            this._forsingCheck.Location = new System.Drawing.Point(6, 19);
            this._forsingCheck.Name = "_forsingCheck";
            this._forsingCheck.Size = new System.Drawing.Size(150, 17);
            this._forsingCheck.TabIndex = 41;
            this._forsingCheck.Text = "Без ограничения отбора";
            this._forsingCheck.UseVisualStyleBackColor = true;
            // 
            // ControllSwitchGrBox2
            // 
            this.ControllSwitchGrBox2.Controls.Add(this._blockingBox2);
            this.ControllSwitchGrBox2.Controls.Add(this.label31);
            this.ControllSwitchGrBox2.Controls.Add(this.label30);
            this.ControllSwitchGrBox2.Controls.Add(this.label28);
            this.ControllSwitchGrBox2.Controls.Add(this.label29);
            this.ControllSwitchGrBox2.Controls.Add(this._tokCheck);
            this.ControllSwitchGrBox2.Controls.Add(this._highCheck);
            this.ControllSwitchGrBox2.Controls.Add(this._breakCheck);
            this.ControllSwitchGrBox2.Location = new System.Drawing.Point(21, 262);
            this.ControllSwitchGrBox2.Name = "ControllSwitchGrBox2";
            this.ControllSwitchGrBox2.Size = new System.Drawing.Size(284, 99);
            this.ControllSwitchGrBox2.TabIndex = 55;
            this.ControllSwitchGrBox2.TabStop = false;
            this.ControllSwitchGrBox2.Text = "Контроль цепи отключения";
            this.ControllSwitchGrBox2.Visible = false;
            // 
            // _blockingBox2
            // 
            this._blockingBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingBox2.FormattingEnabled = true;
            this._blockingBox2.Location = new System.Drawing.Point(179, 72);
            this._blockingBox2.Name = "_blockingBox2";
            this._blockingBox2.Size = new System.Drawing.Size(76, 21);
            this._blockingBox2.TabIndex = 52;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 56);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(108, 13);
            this.label31.TabIndex = 51;
            this.label31.Text = "Блокировка по току";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 36);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(228, 13);
            this.label30.TabIndex = 51;
            this.label30.Text = "Высокое сопротивление цепей отключения";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(137, 13);
            this.label28.TabIndex = 51;
            this.label28.Text = "Обрыв цепей отключения";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 75);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 13);
            this.label29.TabIndex = 51;
            this.label29.Text = "Блокировка*";
            // 
            // _tokCheck
            // 
            this._tokCheck.AutoSize = true;
            this._tokCheck.Location = new System.Drawing.Point(240, 56);
            this._tokCheck.Name = "_tokCheck";
            this._tokCheck.Size = new System.Drawing.Size(15, 14);
            this._tokCheck.TabIndex = 41;
            this._tokCheck.UseVisualStyleBackColor = true;
            // 
            // _highCheck
            // 
            this._highCheck.AutoSize = true;
            this._highCheck.Location = new System.Drawing.Point(240, 36);
            this._highCheck.Name = "_highCheck";
            this._highCheck.Size = new System.Drawing.Size(15, 14);
            this._highCheck.TabIndex = 41;
            this._highCheck.UseVisualStyleBackColor = true;
            // 
            // _breakCheck
            // 
            this._breakCheck.AutoSize = true;
            this._breakCheck.Location = new System.Drawing.Point(240, 16);
            this._breakCheck.Name = "_breakCheck";
            this._breakCheck.Size = new System.Drawing.Size(15, 14);
            this._breakCheck.TabIndex = 41;
            this._breakCheck.UseVisualStyleBackColor = true;
            // 
            // JsSettingsGrBox
            // 
            this.JsSettingsGrBox.Controls.Add(this.label32);
            this.JsSettingsGrBox.Controls.Add(this._journIo);
            this.JsSettingsGrBox.Controls.Add(this._journOff);
            this.JsSettingsGrBox.Controls.Add(this._journProt);
            this.JsSettingsGrBox.Location = new System.Drawing.Point(559, 262);
            this.JsSettingsGrBox.Name = "JsSettingsGrBox";
            this.JsSettingsGrBox.Size = new System.Drawing.Size(193, 88);
            this.JsSettingsGrBox.TabIndex = 62;
            this.JsSettingsGrBox.TabStop = false;
            this.JsSettingsGrBox.Text = "Вывод сообщений в ЖС";
            this.JsSettingsGrBox.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(23, 67);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(166, 13);
            this.label32.TabIndex = 42;
            this.label32.Text = "по цепи независимого питания";
            // 
            // _journIo
            // 
            this._journIo.AutoSize = true;
            this._journIo.Location = new System.Drawing.Point(6, 14);
            this._journIo.Name = "_journIo";
            this._journIo.Size = new System.Drawing.Size(94, 17);
            this._journIo.TabIndex = 41;
            this._journIo.Text = "Пуск защиты";
            this._journIo.UseVisualStyleBackColor = true;
            // 
            // _journOff
            // 
            this._journOff.AutoSize = true;
            this._journOff.Location = new System.Drawing.Point(6, 52);
            this._journOff.Name = "_journOff";
            this._journOff.Size = new System.Drawing.Size(154, 17);
            this._journOff.TabIndex = 41;
            this._journOff.Text = "Действие на отключение";
            this._journOff.UseVisualStyleBackColor = true;
            // 
            // _journProt
            // 
            this._journProt.AutoSize = true;
            this._journProt.Location = new System.Drawing.Point(6, 33);
            this._journProt.Name = "_journProt";
            this._journProt.Size = new System.Drawing.Size(143, 17);
            this._journProt.TabIndex = 41;
            this._journProt.Text = "Срабатывание защиты";
            this._journProt.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 517);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(619, 13);
            this.label24.TabIndex = 61;
            this.label24.Text = "* - Дискретные входа Д1, Д2 могут отсутствовать в РЗТ-110. РЗТ-110 комплектуется " +
    "дискретными входами  по заказу";
            // 
            // _pulseTextBox
            // 
            this._pulseTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._pulseTextBox.Location = new System.Drawing.Point(623, 52);
            this._pulseTextBox.Name = "_pulseTextBox";
            this._pulseTextBox.Size = new System.Drawing.Size(60, 20);
            this._pulseTextBox.TabIndex = 50;
            this._pulseTextBox.Tag = "1500";
            this._pulseTextBox.Text = "0";
            this._pulseTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(319, 54);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(298, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Задержка на повторную команду отключения (Тповт), мс";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.groupBox9);
            this.groupBox10.Controls.Add(this.groupBox13);
            this.groupBox10.Location = new System.Drawing.Point(21, 362);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(488, 152);
            this.groupBox10.TabIndex = 60;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Система";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._fixsation);
            this.groupBox9.Controls.Add(this._oscWriteLength);
            this.groupBox9.Controls.Add(this._oscLength);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Location = new System.Drawing.Point(173, 19);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(309, 123);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Осциллограф";
            // 
            // _fixsation
            // 
            this._fixsation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._fixsation.FormattingEnabled = true;
            this._fixsation.Location = new System.Drawing.Point(148, 46);
            this._fixsation.Name = "_fixsation";
            this._fixsation.Size = new System.Drawing.Size(142, 21);
            this._fixsation.TabIndex = 12;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(148, 74);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(142, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "";
            this._oscWriteLength.Text = "1";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(148, 19);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(142, 21);
            this._oscLength.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(134, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Длит. предзаписи осц, %";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Фиксация осц.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Длит. периода осц.";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.panel2);
            this.groupBox13.Controls.Add(this._rs485CheckBox);
            this.groupBox13.Location = new System.Drawing.Point(6, 19);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(161, 95);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Связь по порту RS-485";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this._speed);
            this.panel2.Controls.Add(this._address);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(0, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(155, 63);
            this.panel2.TabIndex = 2;
            // 
            // _speed
            // 
            this._speed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._speed.FormattingEnabled = true;
            this._speed.Location = new System.Drawing.Point(64, 37);
            this._speed.Name = "_speed";
            this._speed.Size = new System.Drawing.Size(83, 21);
            this._speed.TabIndex = 12;
            // 
            // _address
            // 
            this._address.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._address.FormattingEnabled = true;
            this._address.Location = new System.Drawing.Point(64, 10);
            this._address.Name = "_address";
            this._address.Size = new System.Drawing.Size(83, 21);
            this._address.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Адрес";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Скорость";
            // 
            // _rs485CheckBox
            // 
            this._rs485CheckBox.AutoSize = true;
            this._rs485CheckBox.Location = new System.Drawing.Point(132, 0);
            this._rs485CheckBox.Name = "_rs485CheckBox";
            this._rs485CheckBox.Size = new System.Drawing.Size(15, 14);
            this._rs485CheckBox.TabIndex = 2;
            this._rs485CheckBox.UseVisualStyleBackColor = true;
            this._rs485CheckBox.CheckedChanged += new System.EventHandler(this._rs485CheckBox_CheckedChanged);
            // 
            // ModePowerGrBox
            // 
            this.ModePowerGrBox.Controls.Add(this._modePower);
            this.ModePowerGrBox.Controls.Add(this._modePowerForse);
            this.ModePowerGrBox.Controls.Add(this.label2);
            this.ModePowerGrBox.Controls.Add(this.label1);
            this.ModePowerGrBox.Location = new System.Drawing.Point(362, 262);
            this.ModePowerGrBox.Name = "ModePowerGrBox";
            this.ModePowerGrBox.Size = new System.Drawing.Size(315, 82);
            this.ModePowerGrBox.TabIndex = 54;
            this.ModePowerGrBox.TabStop = false;
            this.ModePowerGrBox.Text = "Уровень отбора мощности";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this._voltageChargeCapacitorBox);
            this.groupBox6.Controls.Add(this._chargeCapacitorBox);
            this.groupBox6.Location = new System.Drawing.Point(362, 148);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(315, 108);
            this.groupBox6.TabIndex = 53;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Параметры конденсатора";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(162, 13);
            this.label10.TabIndex = 54;
            this.label10.Text = "команды отключения (Uуст), В";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 13);
            this.label12.TabIndex = 45;
            this.label12.Text = "Напряжение заряда (Uз)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "Напряжение разрешения";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._alarmBox);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this._setpointsGroupBox);
            this.groupBox4.Location = new System.Drawing.Point(21, 148);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(284, 108);
            this.groupBox4.TabIndex = 52;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Входные сигналы*";
            // 
            // _switchTok
            // 
            this._switchTok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTok.Location = new System.Drawing.Point(245, 122);
            this._switchTok.Name = "_switchTok";
            this._switchTok.Size = new System.Drawing.Size(60, 20);
            this._switchTok.TabIndex = 42;
            this._switchTok.Tag = "1500";
            this._switchTok.Text = "0";
            this._switchTok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._switchTok.Visible = false;
            // 
            // labelTok
            // 
            this.labelTok.AutoSize = true;
            this.labelTok.Location = new System.Drawing.Point(28, 124);
            this.labelTok.Name = "labelTok";
            this.labelTok.Size = new System.Drawing.Size(201, 13);
            this.labelTok.TabIndex = 43;
            this.labelTok.Text = "Ток блок-ки контроля цепей откл-я, Iн";
            this.labelTok.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.IndicatorAlarmGrBox);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this._externalDefenseGrid);
            this.tabPage2.Controls.Add(this._outputReleGrid);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(778, 639);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Выходные сигналы и защиты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // IndicatorAlarmGrBox
            // 
            this.IndicatorAlarmGrBox.Controls.Add(this.label34);
            this.IndicatorAlarmGrBox.Controls.Add(this.label33);
            this.IndicatorAlarmGrBox.Controls.Add(this._alarmSign);
            this.IndicatorAlarmGrBox.Controls.Add(this._alarmOff);
            this.IndicatorAlarmGrBox.Controls.Add(this._alarmProt);
            this.IndicatorAlarmGrBox.Location = new System.Drawing.Point(436, 285);
            this.IndicatorAlarmGrBox.Name = "IndicatorAlarmGrBox";
            this.IndicatorAlarmGrBox.Size = new System.Drawing.Size(271, 108);
            this.IndicatorAlarmGrBox.TabIndex = 63;
            this.IndicatorAlarmGrBox.TabStop = false;
            this.IndicatorAlarmGrBox.Text = "Индикатор \"Авария\"";
            this.IndicatorAlarmGrBox.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(23, 90);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(166, 13);
            this.label34.TabIndex = 21;
            this.label34.Text = "по цепи независимого питания";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(84, 13);
            this.label33.TabIndex = 21;
            this.label33.Text = "Типы сигналов";
            // 
            // _alarmSign
            // 
            this._alarmSign.AutoSize = true;
            this._alarmSign.Location = new System.Drawing.Point(6, 38);
            this._alarmSign.Name = "_alarmSign";
            this._alarmSign.Size = new System.Drawing.Size(98, 17);
            this._alarmSign.TabIndex = 42;
            this._alarmSign.Text = "Сигнализация";
            this._alarmSign.UseVisualStyleBackColor = true;
            // 
            // _alarmOff
            // 
            this._alarmOff.AutoSize = true;
            this._alarmOff.Location = new System.Drawing.Point(6, 76);
            this._alarmOff.Name = "_alarmOff";
            this._alarmOff.Size = new System.Drawing.Size(154, 17);
            this._alarmOff.TabIndex = 43;
            this._alarmOff.Text = "Действие на отключение";
            this._alarmOff.UseVisualStyleBackColor = true;
            // 
            // _alarmProt
            // 
            this._alarmProt.AutoSize = true;
            this._alarmProt.Location = new System.Drawing.Point(6, 57);
            this._alarmProt.Name = "_alarmProt";
            this._alarmProt.Size = new System.Drawing.Size(221, 17);
            this._alarmProt.TabIndex = 44;
            this._alarmProt.Text = "Срабатывание защиты на отключение";
            this._alarmProt.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 573);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(619, 13);
            this.label25.TabIndex = 62;
            this.label25.Text = "* - Дискретные входа Д1, Д2 могут отсутствовать в РЗТ-110. РЗТ-110 комплектуется " +
    "дискретными входами  по заказу";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox5);
            this.groupBox8.Controls.Add(this._tokDefenseGrid1);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(684, 153);
            this.groupBox8.TabIndex = 61;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Токовые защиты";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._mainRadioButton);
            this.groupBox5.Controls.Add(this._reserveRadioButton);
            this.groupBox5.Location = new System.Drawing.Point(6, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(184, 35);
            this.groupBox5.TabIndex = 57;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Группа уставок";
            // 
            // _mainRadioButton
            // 
            this._mainRadioButton.AutoSize = true;
            this._mainRadioButton.Checked = true;
            this._mainRadioButton.Location = new System.Drawing.Point(17, 13);
            this._mainRadioButton.Name = "_mainRadioButton";
            this._mainRadioButton.Size = new System.Drawing.Size(75, 17);
            this._mainRadioButton.TabIndex = 6;
            this._mainRadioButton.TabStop = true;
            this._mainRadioButton.Text = "Основная";
            this._mainRadioButton.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioButton
            // 
            this._reserveRadioButton.AutoSize = true;
            this._reserveRadioButton.Location = new System.Drawing.Point(98, 14);
            this._reserveRadioButton.Name = "_reserveRadioButton";
            this._reserveRadioButton.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioButton.TabIndex = 7;
            this._reserveRadioButton.Text = "Резервная";
            this._reserveRadioButton.UseVisualStyleBackColor = true;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._tokDefenseGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._tokDefenseGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseParameterCol,
            this._ustSrab40,
            this._ustSrab8,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseOSCv11Col});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid1.DefaultCellStyle = dataGridViewCellStyle5;
            this._tokDefenseGrid1.Location = new System.Drawing.Point(6, 56);
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(670, 89);
            this._tokDefenseGrid1.TabIndex = 56;
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 5;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.HeaderText = "Режим";
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 48;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.HeaderText = "Блокировка*";
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 78;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.HeaderText = "Параметр";
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 64;
            // 
            // _ustSrab40
            // 
            dataGridViewCellStyle3.NullValue = null;
            this._ustSrab40.DefaultCellStyle = dataGridViewCellStyle3;
            this._ustSrab40.HeaderText = "Iуст, Iн";
            this._ustSrab40.Name = "_ustSrab40";
            this._ustSrab40.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ustSrab40.Width = 42;
            // 
            // _ustSrab8
            // 
            this._ustSrab8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._ustSrab8.HeaderText = "Iуст, Iн";
            this._ustSrab8.Name = "_ustSrab8";
            this._ustSrab8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ustSrab8.Width = 62;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.HeaderText = "Хар-ка";
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 47;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle4.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._tokDefenseWorkTimeCol.HeaderText = "tсраб, мс/коэф.";
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 85;
            // 
            // _tokDefenseOSCv11Col
            // 
            this._tokDefenseOSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._tokDefenseOSCv11Col.HeaderText = "Осциллограф";
            this._tokDefenseOSCv11Col.Name = "_tokDefenseOSCv11Col";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 460);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(0, 13);
            this.label22.TabIndex = 60;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 174);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 13);
            this.label21.TabIndex = 59;
            this.label21.Text = "Внешние защиты*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 282);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 58;
            this.label20.Text = "Реле";
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDefenseGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._oscExDefenseColumn});
            this._externalDefenseGrid.Location = new System.Drawing.Point(6, 197);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(719, 82);
            this._externalDefenseGrid.TabIndex = 54;
            // 
            // _extDefNumberCol
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._extDefNumberCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._extDefNumberCol.FillWeight = 28.83845F;
            this._extDefNumberCol.HeaderText = "№";
            this._extDefNumberCol.MinimumWidth = 24;
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._extDefNumberCol.Width = 40;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.FillWeight = 67.46588F;
            this._exDefModeCol.HeaderText = "Режим";
            this._exDefModeCol.MinimumWidth = 50;
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 110;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.FillWeight = 106.9584F;
            this._exDefBlockingCol.HeaderText = "Блокировка";
            this._exDefBlockingCol.MinimumWidth = 50;
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._exDefWorkingCol.FillWeight = 129.7117F;
            this._exDefWorkingCol.HeaderText = "Срабатывание";
            this._exDefWorkingCol.MinimumWidth = 50;
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.FillWeight = 94.74427F;
            this._exDefWorkingTimeCol.HeaderText = "tсраб., мс";
            this._exDefWorkingTimeCol.MinimumWidth = 50;
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 65;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.FillWeight = 78.17878F;
            this._exDefReturnCol.HeaderText = "Возвр.";
            this._exDefReturnCol.MinimumWidth = 50;
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 50;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.FillWeight = 143.1574F;
            this._exDefReturnNumberCol.HeaderText = "Вход возврата";
            this._exDefReturnNumberCol.MinimumWidth = 50;
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.FillWeight = 113.3605F;
            this._exDefReturnTimeCol.HeaderText = "tвозвр., мс";
            this._exDefReturnTimeCol.MinimumWidth = 50;
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 70;
            // 
            // _oscExDefenseColumn
            // 
            this._oscExDefenseColumn.FillWeight = 137.5848F;
            this._oscExDefenseColumn.HeaderText = "Осциллограф";
            this._oscExDefenseColumn.MinimumWidth = 50;
            this._oscExDefenseColumn.Name = "_oscExDefenseColumn";
            this._oscExDefenseColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._oscExDefenseColumn.Width = 80;
            // 
            // _resetConfigBut
            // 
            this._resetConfigBut.Location = new System.Drawing.Point(322, 671);
            this._resetConfigBut.Name = "_resetConfigBut";
            this._resetConfigBut.Size = new System.Drawing.Size(158, 23);
            this._resetConfigBut.TabIndex = 61;
            this._resetConfigBut.Text = "Загрузить баз. уставки";
            this._resetConfigBut.UseVisualStyleBackColor = true;
            this._resetConfigBut.Click += new System.EventHandler(this._resetConfigBut_Click);
            // 
            // RztConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 725);
            this.Controls.Add(this._resetConfigBut);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 726);
            this.Name = "RztConfigurationForm";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.RztConfigurationForm_Load);
            this.Shown += new System.EventHandler(this.RztConfigurationForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RztConfigurationForm_KeyUp);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ControllSwitchGrBox.ResumeLayout(false);
            this.ControllSwitchGrBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this._transformSchemeGrBox.ResumeLayout(false);
            this._transformSchemeGrBox.PerformLayout();
            this.AddSettingsGrBox.ResumeLayout(false);
            this.AddSettingsGrBox.PerformLayout();
            this.ControllSwitchGrBox2.ResumeLayout(false);
            this.ControllSwitchGrBox2.PerformLayout();
            this.JsSettingsGrBox.ResumeLayout(false);
            this.JsSettingsGrBox.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ModePowerGrBox.ResumeLayout(false);
            this.ModePowerGrBox.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.IndicatorAlarmGrBox.ResumeLayout(false);
            this.IndicatorAlarmGrBox.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox _voltageChargeCapacitorBox;
        private System.Windows.Forms.ComboBox _chargeCapacitorBox;
        private System.Windows.Forms.ComboBox _modePower;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _modePowerForse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _ttBox;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.ComboBox _setpointsGroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox _alarmBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _outputSignalsDispepairTimeBox;
        private System.Windows.Forms.CheckedListBox _dispepairRelayCheckList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox _dispepairIndicatorCheckList;
        private System.Windows.Forms.GroupBox ControllSwitchGrBox;
        private System.Windows.Forms.ComboBox _controlBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _blockingBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _pulseBox;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox ModePowerGrBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton _mainRadioButton;
        private System.Windows.Forms.RadioButton _reserveRadioButton;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _fixsation;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.ComboBox _speed;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox _address;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _pulseTextBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox _rs485CheckBox;
        private System.Windows.Forms.CheckBox _relayFaultCheckBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _resetConfigBut;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox JsSettingsGrBox;
        private System.Windows.Forms.CheckBox _journIo;
        private System.Windows.Forms.CheckBox _journOff;
        private System.Windows.Forms.CheckBox _journProt;
        private System.Windows.Forms.GroupBox ControllSwitchGrBox2;
        private System.Windows.Forms.ComboBox _blockingBox2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox _tokCheck;
        private System.Windows.Forms.CheckBox _highCheck;
        private System.Windows.Forms.CheckBox _breakCheck;
        private System.Windows.Forms.MaskedTextBox _switchTok;
        private System.Windows.Forms.Label labelTok;
        private System.Windows.Forms.GroupBox IndicatorAlarmGrBox;
        private System.Windows.Forms.CheckBox _alarmSign;
        private System.Windows.Forms.CheckBox _alarmOff;
        private System.Windows.Forms.CheckBox _alarmProt;
        private System.Windows.Forms.GroupBox AddSettingsGrBox;
        private System.Windows.Forms.CheckBox _forsingCheck;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _oscExDefenseColumn;
        private System.Windows.Forms.GroupBox _transformSchemeGrBox;
        private System.Windows.Forms.RadioButton _starType;
        private System.Windows.Forms.RadioButton _triangleType;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ustSrab40;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ustSrab8;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseOSCv11Col;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
    }
}