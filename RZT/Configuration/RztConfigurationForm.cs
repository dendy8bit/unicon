﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.RZT.Configuration.Structures;
using BEMN.RZT.Configuration.Structures.CurrentDefenses;
using BEMN.RZT.Configuration.Structures.ExternalDefences;
using BEMN.RZT.Configuration.Structures.NewConfiguration;
using BEMN.RZT.Configuration.Structures.Relay;

namespace BEMN.RZT.Configuration
{
    public partial class RztConfigurationForm : Form, IFormView
    {
        #region [Constants]
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "RZT_SET_POINTS";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";
        private const string BASE_CONFIG_PATH = "\\RZT\\RZT_BaseConfig.xml";
        #endregion [Constants]

        #region [Private fields]
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private readonly MemoryEntity<NewConfigurationStruct> _newConfig;
        
        private readonly RztDevice _device;
        private double _version;
        private NewStructValidator<MeasuringConfigStruct> _measuringValidator;
        private NewStructValidator<NewMeasuringStruct> _newMeasuringValidator;
        private NewStructValidator<ExternalSignalsStruct> _externalSignalsValidator;
        private NewCheckedListBoxValidator<IndicatorFaultConfigStruct> _faultConfigIndicatorValidator;
        private NewCheckedListBoxValidator<RelayFaultConfigStruct> _faultConfigRelayValidator;
        private NewStructValidator<RelayFaultSygnalStruct> _faultRelayValidator;
        private NewStructValidator<OffStruct> _offValidator;
        private NewStructValidator<NewSwitchStruct> _newSwitchValidator;
        private NewDgwValidatior<AllOutputRelaysStruct, RelayStruct> _relayDgwValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct> _externalDefenseDgwValidator;
        private NewDgwValidatior<GroupDefenseStruct, CurrentDefenseStruct> _currentDefenseDgwValidator;
        private RadioButtonSelector _radioButtonSelector;
        private SetpointsValidator<AllDefenseStuct, GroupDefenseStruct> _setpoints;
        private NewStructValidator<SystemStruct> _systemValidator;
        private IValidator _configUnion;
        private List<ushort> _listUustMax;
        #endregion [Private fields]
        
        #region [Ctor's]
        public RztConfigurationForm()
        {
            this.InitializeComponent();
        }

        public RztConfigurationForm(RztDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._configuration = device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._progressBar.PerformStep());
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadFail);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => this._device.WriteConfig());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._progressBar.PerformStep());
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteFail);

            this._newConfig = device.NewConfig;
            this._newConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._newConfig.ReadOk += HandlerHelper.CreateHandler(this, () => this._progressBar.PerformStep());
            this._newConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadFail);
            this._newConfig.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => this._device.WriteConfig());
            this._newConfig.WriteOk += HandlerHelper.CreateHandler(this, () => this._progressBar.PerformStep());
            this._newConfig.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteFail);
            
            this._device.WriteConfigOk +=HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteOk);
            this._device.WriteConfigFail += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteFail);
            this._progressBar.Maximum = this._configuration.Slots.Count;
            this.Init();
        }
        
        private void Init()
        {
            this._version = Common.VersionConverter(this._device.DeviceVersion);
            this._listUustMax = new List<ushort> { 213, 232, 252, 271, 290 };
            Func<IValidatingRule> voltageChargeFunc = new Func<IValidatingRule>(() =>
            {
                int ind = this._chargeCapacitorBox.SelectedIndex;
                switch (ind)
                {
                    case 0:
                    {
                        return new CustomUshortRule(0, this._listUustMax[ind]);
                    }
                    case 1:
                    {
                        return new CustomUshortRule(0, this._listUustMax[ind]);
                    }
                    case 2:
                    {
                        return new CustomUshortRule(0, this._listUustMax[ind]);
                    }
                    case 3:
                    {
                        return new CustomUshortRule(0, this._listUustMax[ind]);
                    }
                    case 4:
                    {
                        return new CustomUshortRule(0, this._listUustMax[ind]);
                    }
                    default:
                    {
                        return new CustomUshortRule(0, this._listUustMax[0]);
                    }
                }
            });
            var func1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    return this._triangleType.Checked
                        ? (IValidatingRule)new CustomDoubleRule(0, Math.Round(40 / Math.Sqrt(3), 0))
                        : RulesContainer.Ustavka40;
                }
                catch (Exception)
                {
                    return RulesContainer.Ustavka40;
                }
            });

            var func2 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    return this._triangleType.Checked
                        ? new CustomDoubleRule(0, Math.Round(8 / Math.Sqrt(3), 0))
                        : new CustomDoubleRule(0, 8);
                }
                catch (Exception)
                {
                    return new CustomDoubleRule(0, 8);
                }
            });

            if (this._version < 1.03)
            {
                this._measuringValidator = new NewStructValidator<MeasuringConfigStruct>
                    (
                    this._toolTip,
                    new ControlInfoCombo(this._chargeCapacitorBox, StringsConfig.ChargeCapacitor),
                    new ControlInfoTextDependent(this._voltageChargeCapacitorBox, voltageChargeFunc),
                    new ControlInfoCombo(this._modePower, StringsConfig.ModePower),
                    new ControlInfoCombo(this._modePowerForse, StringsConfig.ModePowerForce),
                    new ControlInfoText(this._ttBox, RulesContainer.UshortRule)
                    );
                this._offValidator = new NewStructValidator<OffStruct>
                    (
                    this._toolTip,
                    new ControlInfoText(this._pulseBox, RulesContainer.TimeRule),
                    new ControlInfoCombo(this._blockingBox, StringsConfig.SignalsBlockingCurrentDefense),
                    new ControlInfoCombo(this._controlBox, StringsConfig.OffOn)
                    );
            }
            else
            {
                this.ModePowerGrBox.Visible = this.ControllSwitchGrBox.Visible = false;
                this.JsSettingsGrBox.Visible = this.IndicatorAlarmGrBox.Visible = this.AddSettingsGrBox.Visible = this.ControllSwitchGrBox2.Visible = true;
                this.labelTok.Visible = this._switchTok.Visible = true;
                this._newMeasuringValidator = new NewStructValidator<NewMeasuringStruct>
                    (
                    this._toolTip,
                    new ControlInfoCombo(this._chargeCapacitorBox, StringsConfig.ChargeCapacitor),
                    new ControlInfoTextDependent(this._voltageChargeCapacitorBox, voltageChargeFunc),
                    new ControlInfoCheck(this._forsingCheck),
                    new ControlInfoCheck(this._alarmSign),
                    new ControlInfoCheck(this._alarmProt),
                    new ControlInfoCheck(this._alarmOff),
                    new ControlInfoCheck(this._journIo),
                    new ControlInfoCheck(this._journProt),
                    new ControlInfoCheck(this._journOff),
                    new ControlInfoText(this._ttBox, RulesContainer.UshortRule)
                    );

                this._newSwitchValidator = this._version < 1.10
                    ? new NewStructValidator<NewSwitchStruct>
                        (
                        this._toolTip,
                        new ControlInfoText(this._pulseBox, RulesContainer.TimeRule),
                        new ControlInfoCombo(this._blockingBox2, StringsConfig.SignalsBlockingCurrentDefense),
                        new ControlInfoCheck(this._breakCheck),
                        new ControlInfoCheck(this._highCheck),
                        new ControlInfoCheck(this._tokCheck),
                        new ControlInfoTextDependent(this._switchTok, func1),
                        new ControlInfoTextDependent(new MaskedTextBox(), func2)
                        )
                    : new NewStructValidator<NewSwitchStruct>
                        (
                        this._toolTip,
                        new ControlInfoText(this._pulseBox, RulesContainer.TimeRule),
                        new ControlInfoCombo(this._blockingBox2, StringsConfig.SignalsBlockingCurrentDefense),
                        new ControlInfoCheck(this._breakCheck),
                        new ControlInfoCheck(this._highCheck),
                        new ControlInfoCheck(this._tokCheck),
                        new ControlInfoTextDependent(new MaskedTextBox(), func1),
                        new ControlInfoTextDependent(this._switchTok, func2)
                        );
            }

            this._faultConfigIndicatorValidator = new NewCheckedListBoxValidator<IndicatorFaultConfigStruct>(this._dispepairIndicatorCheckList);
            this._faultConfigRelayValidator = new NewCheckedListBoxValidator<RelayFaultConfigStruct>(this._dispepairRelayCheckList);
            this._faultRelayValidator = new NewStructValidator<RelayFaultSygnalStruct>
                (
                this._toolTip,
                new ControlInfoValidator(this._faultConfigRelayValidator),
                new ControlInfoText(this._outputSignalsDispepairTimeBox, RulesContainer.TimeRule)
                );

            this._externalSignalsValidator = new NewStructValidator<ExternalSignalsStruct>
                (
                this._toolTip,
                new ControlInfoText(this._pulseTextBox, RulesContainer.TimeRule),
                new ControlInfoCombo(this._alarmBox, StringsConfig.SignalsBlockingCurrentDefense),
                new ControlInfoCombo(this._setpointsGroupBox, StringsConfig.SignalsBlockingCurrentDefense),
                new ControlInfoValidator(new NewStructValidator<IndicatorFaultSygnalStruct>(this._toolTip,
                    new ControlInfoValidator(this._faultConfigIndicatorValidator))),
                new ControlInfoValidator(this._faultRelayValidator)
                );

            if (this._version < 1.02)
            {
                this._relayDgwValidator = new NewDgwValidatior<AllOutputRelaysStruct, RelayStruct>
                    (
                    this._outputReleGrid,
                    2,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.RelaysNames, ColumnsType.NAME),
                    new ColumnInfoCombo(new List<string>{"Повторитель"}),
                    new ColumnInfoCombo(StringsConfig.RelaySignals),
                    new ColumnInfoText(RulesContainer.TimeRule)
                    );
            }
            else
            {
                this._relayDgwValidator = new NewDgwValidatior<AllOutputRelaysStruct, RelayStruct>
                    (
                    this._outputReleGrid,
                    2,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.RelaysNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.RelayType),
                    new ColumnInfoCombo(StringsConfig.RelaySignals),
                    new ColumnInfoText(RulesContainer.TimeRule)
                    );
            }

            this._systemValidator = new NewStructValidator<SystemStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._address, StringsConfig.DeviceNumbers),
                new ControlInfoCombo(this._speed, StringsConfig.Speed),
                new ControlInfoCombo(this._oscLength, StringsConfig.OscLenght),
                new ControlInfoCombo(this._fixsation, StringsConfig.OscFicsation),
                new ControlInfoText(this._oscWriteLength, new CustomUshortRule(1, 100)),
                new ControlInfoCheck(this._rs485CheckBox),
                new ControlInfoCheck(this._relayFaultCheckBox)
                );

            this._externalDefenseDgwValidator = new NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct>
                (
                this._externalDefenseGrid,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ExternalDefenseMode),
                new ColumnInfoCombo(StringsConfig.Signals),
                new ColumnInfoCombo(StringsConfig.Signals),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Signals),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8))
                }
            };
            
            if (this._version < 1.1)
            {
                this._currentDefenseDgwValidator = new NewDgwValidatior<GroupDefenseStruct, CurrentDefenseStruct>
                    (this._tokDefenseGrid1,
                    3,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.ExternalDefenseMode),
                    new ColumnInfoCombo(StringsConfig.SignalsBlockingCurrentDefense),
                    new ColumnInfoCombo(StringsConfig.TokParameter),
                    new ColumnInfoTextDependent(func1, true),
                    new ColumnInfoTextDependent(func2, false),
                    new ColumnInfoCombo(StringsConfig.FeatureLight),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(StringsConfig.Osc)
                    )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv(this._tokDefenseGrid1,
                            new TurnOffRule(1, StringsConfig.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8))
                    }
                };
                this._ustSrab40.Visible = true;
                this._ustSrab8.Visible = false;
            }
            else
            {
                this._currentDefenseDgwValidator = new NewDgwValidatior<GroupDefenseStruct, CurrentDefenseStruct>
                (this._tokDefenseGrid1,
                3,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ExternalDefenseMode),
                new ColumnInfoCombo(StringsConfig.SignalsBlockingCurrentDefense),
                new ColumnInfoCombo(StringsConfig.TokParameter),
                new ColumnInfoTextDependent(func1, false),
                new ColumnInfoTextDependent(func2, true),
                new ColumnInfoCombo(StringsConfig.FeatureLight),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(StringsConfig.Osc))
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv(this._tokDefenseGrid1,
                            new TurnOffRule(1, StringsConfig.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8))
                    }
                };
                this._ustSrab40.Visible = false;
                this._ustSrab8.Visible = true;
            }
            this._radioButtonSelector = new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton);
            this._setpoints = new SetpointsValidator<AllDefenseStuct, GroupDefenseStruct>(this._radioButtonSelector, this._currentDefenseDgwValidator);

            if (this._version < 1.03)
            {
                this._resetConfigBut.Text = @"Обнулить уставки";

                this._configUnion = new StructUnion<ConfigurationStruct>
                    (this._measuringValidator, this._externalSignalsValidator, this._offValidator, this._relayDgwValidator, this._externalDefenseDgwValidator, this._setpoints, this._systemValidator
                    );
            }
            else
            {
                this._configUnion = new StructUnion<NewConfigurationStruct>
                    (this._newMeasuringValidator, this._externalSignalsValidator, this._newSwitchValidator, this._relayDgwValidator, this._externalDefenseDgwValidator, this._setpoints, this._systemValidator
                    );
            }
        }
        #endregion [Ctor's]

        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            MessageBox.Show("Ошибка записи конфигурации");
            this._statusLabel.Text = "Ошибка записи конфигурации";
        }

        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            MessageBox.Show("Ошибка чтения конфигурации");
            this._statusLabel.Text = "Ошибка чтения конфигурации";
        }

        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            MessageBox.Show("Конфигурация записана");
            this._statusLabel.Text = "Конфигурация записана";
        }

        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this.ShowConfig();
            MessageBox.Show("Конфигурация прочитана");
            this._statusLabel.Text = "Конфигурация прочитана";
        }
        
        private void ShowConfig()
        {
            if (this._version < 1.03)
            {
                ConfigurationStruct conf = this._configuration.Value;
                this._starType.Checked = !conf.Measuring.Type;
                this._triangleType.Checked = conf.Measuring.Type;
                if (conf.Measuring.Type)
                {
                    conf.GetRecalculatedSetpoint(false);
                }
                this._configUnion.Set(conf);
            }
            else
            {
                NewConfigurationStruct conf = this._newConfig.Value;
                this._starType.Checked = !conf.Measuring.Type;
                this._triangleType.Checked = conf.Measuring.Type;
                if (conf.Measuring.Type)
                {
                    conf.GetRecalculatedSetpoint(this._version >= 1.1);
                }
                this._configUnion.Set(conf);
            }
        }

        private void RztConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                this.ReadConfiguration();
        }
        
        private void ReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._statusLabel.Text = "Идёт чтение конфигурации";
            this._device.ReadConfig();
            this._progressBar.Value = 0;
            this.IsProcess = true;
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.03)
            {
                this._configuration.LoadStruct();
            }
            else
            {
                this._newConfig.LoadStruct();
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadConfiguration();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Записать конфигурацию РЗТ-110?", "Запись конфигурации", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._progressBar.Value = 0;
                string message;
                if (this._configUnion.Check(out message, true))
                {
                    this._statusLabel.Text = "Идёт запись конфигурации";
                    this.IsProcess = true;
                    double vers = Common.VersionConverter(this._device.DeviceVersion);
                    if (vers < 1.03)
                    {
                        ConfigurationStruct cs = (ConfigurationStruct)this._configUnion.Get();
                        cs.Measuring.Type = this._triangleType.Checked;
                        if (cs.Measuring.Type)
                        {
                            cs.SetRecalculatedSetpoint(false);
                        }
                        this._configuration.Value = cs;
                        this._configuration.SaveStruct();
                    }
                    else
                    {
                        NewConfigurationStruct conf = (NewConfigurationStruct)this._configUnion.Get();
                        conf.Measuring.Type = this._triangleType.Checked;
                        if (conf.Measuring.Type)
                        {
                            conf.SetRecalculatedSetpoint(vers >= 1.1);
                        }
                        this._newConfig.Value = conf;
                        this._newConfig.SaveStruct();
                    }
                }
                else
                {
                    if (message == null)
                    {
                        MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
                    }
                    else
                    {
                        MessageBox.Show(message + " Конфигурация не может быть записана.");
                    }
                }
            }  
        }
        
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
            }
        }
        
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("РЗТ-110_Уставки_версия {0:F1}.xml", this._device.DeviceVersion);
            string mes;
            if (!this._configUnion.Check(out mes, false))
            {
                MessageBox.Show("Невозможно сохранить конфигурацию, обнаружены некорректные уставки", "Ошибка сохранения", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("RZT"));
                
                StructBase sb = this.RecalcBeforeSerialize();
                ushort[] values = sb.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private StructBase RecalcBeforeSerialize()
        {
            double vers = Common.VersionConverter(this._device.DeviceVersion);
            if (vers < 1.03)
            {
                ConfigurationStruct cs = (ConfigurationStruct)this._configUnion.Get();
                cs.Measuring.Type = this._triangleType.Checked;
                if (cs.Measuring.Type)
                {
                    cs.SetRecalculatedSetpoint(false);
                }
                return cs;
            }
            else
            {
                NewConfigurationStruct conf = (NewConfigurationStruct)this._configUnion.Get();
                conf.Measuring.Type = this._triangleType.Checked;
                if (conf.Measuring.Type)
                {
                    conf.SetRecalculatedSetpoint(vers >= 1.1);
                }
                return conf;
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile(); 
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }
        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.03)
                {
                    var configurationStruct = new ConfigurationStruct();
                    configurationStruct.InitStruct(values);
                    this._configuration.Value = configurationStruct;
                }
                else
                {
                    var configurationStruct = new NewConfigurationStruct();
                    configurationStruct.InitStruct(values);
                    this._newConfig.Value = configurationStruct;
                }
                this.ShowConfig();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void RztConfigurationForm_Shown(object sender, EventArgs e)
        {
            this.ResetSetpoint(false);
        }

        private void _chargeCapacitorBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = this._chargeCapacitorBox.SelectedIndex;
            ushort value = this._listUustMax[index];
            int u;
            bool flag = int.TryParse(this._voltageChargeCapacitorBox.Text, out u);
            if (flag && u <= value) 
                return;
            this._voltageChargeCapacitorBox.Text = value.ToString();
            MessageBox.Show("Диапазон изменён. Установлено максимальное значение Uуст");
        }

        private void _relayFaultCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.panel1.Enabled = this._relayFaultCheckBox.Checked;
        }

        private void _rs485CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.panel2.Enabled = this._rs485CheckBox.Checked;
        }

        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this.ResetSetpoint(true);
        }

        private void ResetSetpoint(bool dialog)
        {
            if (this._version < 1.03)
            {
                if (dialog)
                {
                    DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
                    if (res == DialogResult.No) return;
                }
                this._configUnion.Reset();
                this._configuration.Value = (ConfigurationStruct) this._configUnion.Get();
                this.ShowConfig();
            }
            else
            {
                if (dialog)
                {
                    DialogResult res = MessageBox.Show("Загрузить базовые уставки?", "Внимание", MessageBoxButtons.YesNo);
                    if (res != DialogResult.Yes) return;
                }
                try
                {
                    XmlDocument doc = new XmlDocument();

                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + BASE_CONFIG_PATH);

                    XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                    if (a == null)
                        throw new NullReferenceException();

                    byte[] values = Convert.FromBase64String(a.InnerText);
                    NewConfigurationStruct config = new NewConfigurationStruct();
                    config.InitStruct(values);
                    this._newConfig.Value = config;
                    this.ShowConfig();
                    this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
                }
                catch
                {
                    if (dialog)
                    {
                        if (
                            MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                                "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    this._configUnion.Reset();
                }
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.ReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoint(true);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();

            }
        }

        private void RztConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.ReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }



        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(RztDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(RztConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }
        #endregion [IFormView Members]
    }
}
