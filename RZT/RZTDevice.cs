﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.RZT.AlarmJournal;
using BEMN.RZT.AlarmJournal.Structures;
using BEMN.RZT.Configuration;
using BEMN.RZT.Configuration.Structures;
using BEMN.RZT.Configuration.Structures.NewConfiguration;
using BEMN.RZT.Measuring;
using BEMN.RZT.Measuring.Structures;
using BEMN.RZT.Osc;
using BEMN.RZT.Osc.Structures;
using BEMN.RZT.SystemJournal;
using BEMN.RZT.SystemJournal.Structures;


namespace BEMN.RZT
{
    public class RztDevice : Device, IDeviceView, IDeviceVersion
    {
        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<NewConfigurationStruct> _newConfig;
        private MemoryEntity<DiscretBdStruct> _discretBd;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<MeasuringConfigStruct> _measuringChannel;
        private MemoryEntity<MeasuringConfigStruct> _measuringChannelAj;
        private MemoryEntity<AnalogBdStruct> _analogBd;
        private MemoryEntity<OneWordStruct> _changeGroup;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<AlarmJournalStruct> _alarmRecord;
        /// <summary>
        /// Записи журнала
        /// </summary>
        private MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<MeasuringConfigStruct> _voltageConfiguration;
        /// <summary>
        /// Записи журнала
        /// </summary>
        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return _oscJournal; }
        }
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        public MemoryEntity<OneWordStruct> RefreshOscJournal
        {
            get { return _refreshOscJournal; }
        }
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return _oscOptions; }
        }
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        public MemoryEntity<OneWordStruct> SetStartPage
        {
            get { return _setStartPage; }
        }
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        public MemoryEntity<OscPage> OscPage
        {
            get { return _oscPage; }
        }
        public MemoryEntity<MeasuringConfigStruct> VoltageConfiguration
        {
            get { return _voltageConfiguration; }
        }
        
        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<AlarmJournalStruct> AlarmRecord
        {
            get { return _alarmRecord; }
        }
        
        public MemoryEntity<OneWordStruct> ChangeGroup
        {
            get { return this._changeGroup; }
        }
        public MemoryEntity<AnalogBdStruct> AnalogBd
        {
            get { return _analogBd; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return _configuration; }
        }

        public MemoryEntity<NewConfigurationStruct> NewConfig
        {
            get { return _newConfig; }
        }

        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<DiscretBdStruct> DiscretBd
        {
            get { return _discretBd; }
        }
        public MemoryEntity<MeasuringConfigStruct> MeasuringChannelAj
        {
            get { return _measuringChannelAj; }
        }
        public MemoryEntity<MeasuringConfigStruct> MeasuringChannel
        {
            get { return _measuringChannel; }
        }
        
        #region [Ctor's]
        public RztDevice()
        {
            HaveVersion = true;
        }

        public RztDevice(Modbus mb)
        {
            MB = mb;
            HaveVersion = true;
            this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000);
            this._newConfig = new MemoryEntity<NewConfigurationStruct>("Конфигурация v1.03", this, 0x1000);
            this._dateTime = new MemoryEntity<DateTimeStruct>("Дата и время", this, 0x200);
            this._discretBd = new MemoryEntity<DiscretBdStruct>("Дискретные сигналы", this, 0x1800);
            this._measuringChannelAj = new MemoryEntity<MeasuringConfigStruct>("Измерительный канал ЖС", this, 0x1000);
            this._analogBd = new MemoryEntity<AnalogBdStruct>("Аналоговые сигналы", this, 0x1900);
            this._changeGroup = new MemoryEntity<OneWordStruct>("Переключение групп уставок", this, 0x400);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("ЖС", this, 0x2000);
            this._alarmRecord = new MemoryEntity<AlarmJournalStruct>("ЖА", this, 0x2800);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал Осц", this, 0x800);
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Сброс Журнала Осц", this, 0x800);
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Конфигурация осциллографа", this, 0x106C);
            this._setStartPage = new MemoryEntity<OneWordStruct>("Установка страницы осциллографа", this, 0x900);
            this._oscPage = new MemoryEntity<OscPage>("Чтение страницы осциллографа", this, 0x900);
            this._measuringChannel = new MemoryEntity<MeasuringConfigStruct>("Измерительный канал", this, 0x1000);
            this._voltageConfiguration = new MemoryEntity<MeasuringConfigStruct>("Измерительный канал Осц", this, 0x1000);
        }


        #endregion [Ctor's]

        public override Modbus MB
        {
            get { return base.MB; }
            set
            {
                base.MB = value;
                this.MB.CompleteExchange += CompleteExchange;
            }
        }
        public void WriteConfig()
        {
            this.SetBit(DeviceNumber, 0x0, true, "Save_settings_RZT", this);
        }
        public void ReadConfig()
        {
            this.SetBit(DeviceNumber, 0x0, false, "Read_settings_RZT", this);
        }

        public event Action WriteConfigOk;
        public event Action WriteConfigFail;

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "Save_settings_RZT")
            {
                if (query.error)
                {
                    if (WriteConfigFail != null)
                    {
                        WriteConfigFail.Invoke();
                    }
                }
                else
                {
                    if (WriteConfigOk != null)
                    {
                        WriteConfigOk.Invoke();
                    }
                }
            }
        }

        #region [IDeviceView]
        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(RztDevice); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.rzt110; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "РЗТ-110"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }
        #endregion [IDeviceView]

        #region [IDeviceVersion]
        public Type[] Forms
        {
            get
            {
                return new Type[]
                {
                    typeof(RztAlarmJournalForm),
                    typeof(RztConfigurationForm),
                    typeof(RztMeasuringForm),
                    typeof(RztSystemJournalForm),
                    typeof(RztOscilloscopeForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "1.02",
                    "1.03",
                    "1.10",
                    "1.11",
                    "1.12"
                };
            }
        }
        #endregion [IDeviceVersion]
    }
}
