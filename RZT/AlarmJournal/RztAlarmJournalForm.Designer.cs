﻿namespace BEMN.RZT.AlarmJournal
{
    partial class RztAlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._FCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dis1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dis2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dis3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InSignals1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this._dropJaButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._UbcCol,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._FCol,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._UabCol,
            this._d1,
            this._d2,
            this._d3,
            this._d4,
            this._d5,
            this._dis1,
            this._dis2,
            this._dis3,
            this._InSignals1});
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(645, 487);
            this._journalGrid.TabIndex = 13;
            // 
            // _indexCol
            // 
            this._indexCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._indexCol.DataPropertyName = "_indexCol";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._timeCol.DataPropertyName = "_timeCol";
            this._timeCol.HeaderText = "Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _UbcCol
            // 
            this._UbcCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._UbcCol.DataPropertyName = "_UbcCol";
            this._UbcCol.HeaderText = "Группа уставок";
            this._UbcCol.Name = "_UbcCol";
            this._UbcCol.ReadOnly = true;
            this._UbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbcCol.Width = 82;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._msgCol.DataPropertyName = "_msgCol";
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 71;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._codeCol.DataPropertyName = "_codeCol";
            this._codeCol.HeaderText = "Ступень";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 54;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._typeCol.DataPropertyName = "_typeCol";
            this._typeCol.HeaderText = "Тип повреждения";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._typeCol.Width = 93;
            // 
            // _FCol
            // 
            this._FCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._FCol.DataPropertyName = "_FCol";
            this._FCol.HeaderText = "Значение повреждения";
            this._FCol.Name = "_FCol";
            this._FCol.ReadOnly = true;
            this._FCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._FCol.Width = 119;
            // 
            // _IaCol
            // 
            this._IaCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._IaCol.DataPropertyName = "_IaCol";
            this._IaCol.HeaderText = "Ia";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 22;
            // 
            // _IbCol
            // 
            this._IbCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._IbCol.DataPropertyName = "_IbCol";
            this._IbCol.HeaderText = "Ib";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 22;
            // 
            // _IcCol
            // 
            this._IcCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._IcCol.DataPropertyName = "_IcCol";
            this._IcCol.HeaderText = "Ic";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 22;
            // 
            // _UabCol
            // 
            this._UabCol.DataPropertyName = "_UabCol";
            this._UabCol.HeaderText = "U";
            this._UabCol.Name = "_UabCol";
            this._UabCol.ReadOnly = true;
            this._UabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UabCol.Width = 21;
            // 
            // _d1
            // 
            this._d1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._d1.DataPropertyName = "_d1";
            this._d1.HeaderText = "I>";
            this._d1.Name = "_d1";
            this._d1.ReadOnly = true;
            this._d1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d1.Width = 22;
            // 
            // _d2
            // 
            this._d2.DataPropertyName = "_d2";
            this._d2.HeaderText = "I>>";
            this._d2.Name = "_d2";
            this._d2.ReadOnly = true;
            this._d2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d2.Width = 28;
            // 
            // _d3
            // 
            this._d3.DataPropertyName = "_d3";
            this._d3.HeaderText = "I>>>";
            this._d3.Name = "_d3";
            this._d3.ReadOnly = true;
            this._d3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d3.Width = 34;
            // 
            // _d4
            // 
            this._d4.DataPropertyName = "_d4";
            this._d4.HeaderText = "ВЗ-1";
            this._d4.Name = "_d4";
            this._d4.ReadOnly = true;
            this._d4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d4.Width = 36;
            // 
            // _d5
            // 
            this._d5.DataPropertyName = "_d5";
            this._d5.HeaderText = "ВЗ-2";
            this._d5.Name = "_d5";
            this._d5.ReadOnly = true;
            this._d5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._d5.Width = 36;
            // 
            // _dis1
            // 
            this._dis1.DataPropertyName = "_dis1";
            this._dis1.HeaderText = "Д1";
            this._dis1.Name = "_dis1";
            this._dis1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dis1.Width = 28;
            // 
            // _dis2
            // 
            this._dis2.DataPropertyName = "_dis2";
            this._dis2.HeaderText = "Д2";
            this._dis2.Name = "_dis2";
            this._dis2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dis2.Width = 28;
            // 
            // _dis3
            // 
            this._dis3.DataPropertyName = "_dis3";
            this._dis3.HeaderText = "Д3";
            this._dis3.Name = "_dis3";
            this._dis3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dis3.Visible = false;
            this._dis3.Width = 28;
            // 
            // _InSignals1
            // 
            this._InSignals1.DataPropertyName = "_InSignals1";
            this._InSignals1.HeaderText = "Вх.сигналы 3-1";
            this._InSignals1.Name = "_InSignals1";
            this._InSignals1.ReadOnly = true;
            this._InSignals1.Visible = false;
            this._InSignals1.Width = 99;
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(310, 493);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._loadAlarmJournalButton.TabIndex = 24;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 493);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._readAlarmJournalButton.TabIndex = 22;
            this._readAlarmJournalButton.Text = "Прочитать";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this._readAlarmJournalButton_Click);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(161, 493);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._saveAlarmJournalButton.TabIndex = 23;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 519);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(645, 22);
            this.statusStrip1.TabIndex = 25;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Maximum = 32;
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            this._configProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "РЗТ-110 Журнал аварий";
            this._openAlarmJournalDialog.Filter = "(Журнал аварий РЗТ-110) | *.xml";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для РЗТ-110";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "РЗТ-110 Журнал аварий";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий РЗТ-110) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для РЗТ-110";
            // 
            // _dropJaButton
            // 
            this._dropJaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._dropJaButton.Location = new System.Drawing.Point(459, 493);
            this._dropJaButton.Name = "_dropJaButton";
            this._dropJaButton.Size = new System.Drawing.Size(179, 23);
            this._dropJaButton.TabIndex = 81;
            this._dropJaButton.Text = "Очистить журнал аварий";
            this._dropJaButton.UseVisualStyleBackColor = true;
            this._dropJaButton.Click += new System.EventHandler(this._dropJaButton_Click);
            // 
            // RztAlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 541);
            this.Controls.Add(this._dropJaButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.Controls.Add(this._journalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(661, 579);
            this.Name = "RztAlarmJournalForm";
            this.Text = "Mr600AlarmJournalFormV2";
            this.Load += new System.EventHandler(this.AlarmJournalFormV2_Load);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _FCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d4;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d5;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dis1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dis2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dis3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InSignals1;
        private System.Windows.Forms.Button _dropJaButton;

    }
}