﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.RZT.AlarmJournal.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AlarmJournalRecordStruct : IStruct, IStructInit
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";

        #endregion [Constants]


        #region [Private fields]

        private ushort _message; //1
        private ushort _year; //2
        private ushort _month; //3
        private ushort _date; //4
        private ushort _hour; //5
        private ushort _minute; //6
        private ushort _second; //7
        private ushort _millisecond; //8

        private ushort _code; // 2 Код повреждения** 9
        private ushort _reserv1; //3 Тип повреждения*** 10
        private ushort _reserv2; //4 Значение повреждения //Знач. сраб пар 11
        private ushort _typeDamage; //5 Значение  //F 12
        private ushort _valueDamage; //6 Значение   //Ua 13 
        private ushort _ia; //7 Значение   //Ub 14 
        private ushort _ib; //8 Значение //Uc 15
        private ushort _ic; //9 Значение //Uab 16
        private ushort _u; //10 Значение  //Ubc 17 
        private ushort _dickrets; //11 Значение //Uca 18
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] resevArray;
        #endregion [Private fields]

        #region [Properties]
        public bool IsEmpty
        {
            get
            {
                return _message == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }
        public string GroupOfSetpoints
        {
            get { return Common.GetBit(this._code, 7) ? "Резервные" : "Основные"; }
        }
        public string Code
        {
            get { return StringsAj.CodeDamage[Common.GetBits(_code, 0, 1, 2, 3, 4, 5)]; }
        }

        public ushort Reserv1
        {
            get { return _reserv1; }
        }

        public ushort Reserv2
        {
            get { return _reserv2; }
        }

        public string TypeDamage
        {
            get
            {
             var index =   Common.GetBits(_typeDamage, 8, 9, 10) >> 8;
             return StringsAj.TypeDamage[index];
            }
        }

        public string ValueDamage(double tt, double koef)
        {
          return _typeDamage != 0 ?   ValuesConverterCommon.Analog.GetIrzt(_valueDamage, tt, koef) : string.Empty;         
        }

        public ushort Ia
        {
            get { return _ia; }
        }

        public ushort Ib
        {
            get { return _ib; }
        }

        public ushort Ic
        {
            get { return _ic; }
        }

        public ushort U
        {
            get { return _u; }
        }

        public string Dickrets
        {
            get { return Common.BitsToString(new BitArray(new bool[]
                {
                    Common.GetBit(_dickrets,2),
                    Common.GetBit(_dickrets,1),
                    Common.GetBit(_dickrets,0)
                })) ; }
        }
        public string D1
        {
            get { return Common.GetBit(_dickrets, 0) ? "Да" : "Нет"; }
        }
        public string D2
        {
            get { return Common.GetBit(_dickrets, 1) ? "Да" : "Нет"; }
        }
            public string D3
        {
            get { return Common.GetBit(_dickrets, 2) ? "Да" : "Нет"; }
        }
        public string Imax1
        {
            get { return Common.GetBit(this._dickrets,8) ? "Да" : "Нет"; }
        }

        public string Imax2
        {
            get { return Common.GetBit(this._dickrets, 9) ? "Да" : "Нет"; }
        }

        public string Imax3
        {
            get { return Common.GetBit(this._dickrets, 10) ? "Да" : "Нет"; }
        }

        public string Vz1
        {
            get { return Common.GetBit(this._dickrets, 11) ? "Да" : "Нет"; }
        }

        public string Vz2
        {
            get { return Common.GetBit(this._dickrets, 12) ? "Да" : "Нет"; }
        }
        
        public string Message
        {
            get { return Validator.GetJornal(_message, StringsAj.Message); }
        }

        #endregion [Properties]

        #region [IStruct Members]

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLength);
        }

        #endregion [IStruct Members]

        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            var values = Common.TOWORDS(array, false);
            //Сообщени и время
            this._message = values[0];
            this._year = values[1];
            this._month = values[2];
            this._date = values[3];
            this._hour = values[4];
            this._minute = values[5];
            this._second = values[6];
            this._millisecond = values[7];

            this._code = values[8]; // 2 Код повреждения**
           // this._reserv1 = values[9]; //3 Тип повреждения***
            /*this._reserv2 = values[10];*/ //4 Значение повреждения //Знач. сраб пар
            this._typeDamage = values[9]; //5 Значение  //F
            this._valueDamage = values[10]; //6 Значение   //Ua
            this._ia = values[11]; //7 Значение   //Ub
            this._ib = values[12]; //8 Значение //Uc
            this._ic = values[13]; //9 Значение //Uab
            this._u = values[14]; //10 Значение  //Ubc
            this._dickrets = values[15]; //11 Значение //Uca
        /*    this._u0 = values[18]; //12 Значение //U0
            this._u1 = values[19]; //13 Значение //U1
            this._u2 = values[20]; //14 Значение //U2
            this._un = values[21]; //15 Значение //Un
            this.reserv1 = values[22]; //17 рез
            this._discret = values[23]; //16 Значение //Дискрет
            this.reserv2 = values[24]; //18 рез
            this.reserv3 = values[25]; //19 рез
            this.reserv4 = values[26]; //20 рез
            this.reserv5 = values[27]; //21 рез*/



        }

        public ushort[] GetValues()
        {
            throw new NotImplementedException();
        }

        #endregion [IStructInit Members]
    }
}
