﻿using System.Collections.Generic;

namespace BEMN.RZT.AlarmJournal
{
    public static class StringsAj
    {
        public static List<string> Message
        {
            get
            {
                return new List<string>
                    {
                        "Журнал пуст",
                        "Форсирование",
                        "Сигнализация",
                        "На отключение",
                        "Отключение"
                    };
            }
        }
        public static List<string> CodeDamage
        {
            get
            {
                return new List<string>
                    {
                        "",
                        "I>",
                        "I>>",
                        "I>>>",
                        "ВЗ-1",
                        "ВЗ-2",
                        "Uк>Uуст"
                    };
            }
        }

        public static List<string> TypeDamage
        {
            get
            {
                return new List<string>
                    {
                        "",
                        "Ia",
                        "Ib",
                        "Ic",
                        "U"
                    };
            }
        } 
    }
}
