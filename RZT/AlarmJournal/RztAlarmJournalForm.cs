﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.MeasuringClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.RZT.AlarmJournal.Structures;
using BEMN.RZT.Configuration.Structures;

namespace BEMN.RZT.AlarmJournal
{
    public partial class RztAlarmJournalForm : Form, IFormView
    {
        private readonly MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private readonly MemoryEntity<MeasuringConfigStruct> _measuringChannel;
        private DataTable _table;
        private readonly RztDevice _device;

        private const string READ_AJ = "Чтение журнала аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string RECORDS_IN_JOURNAL_PATTERN = "В журнале {0} сообщений";
        private const string PARAMETERS_LOADED = "Параметры загружены";
        private const string FAULT_LOAD_PARAMETERS = "Невозможно загрузить параметры";
        private const string FAULT_READ_JOURNAL = "Невозможно прочитать журнал";

        public RztAlarmJournalForm()
        {
            InitializeComponent();
        }

        public RztAlarmJournalForm(RztDevice device)
        {
            this._device = device;
            InitializeComponent();

            this._alarmRecord = device.AlarmRecord;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadJournalOk);
            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, JournalReadFail);

            this._measuringChannel = device.MeasuringChannelAj;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, MeasuringChannelLoadFail);
        }

        private void JournalReadFail()
        {
            this._statusLabel.Text = FAULT_READ_JOURNAL;
            this.ButtonsEnabled = true;
        }

        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
            this._statusLabel.Text = FAULT_READ_JOURNAL;
            this.ButtonsEnabled = true;
        }

        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }

        private void ReadJournalOk()
        {
            int i = 1;
            ushort tt = _measuringChannel.Value.Tt;
            double koef = Common.VersionConverter(_device.DeviceVersion) >= 1.10 ? 8 : 40;
            if (this._measuringChannel.Value.Type)
            {
                koef /= Math.Sqrt(3);
            }
            if (Common.VersionConverter(_device.DeviceVersion)>=1.00)
            foreach (var record in this._alarmRecord.Value.Records)
            {
                if (record.IsEmpty)
                {
                    this._journalGrid.DataSource = this._table;
                    this._statusLabel.Text = this._table.Rows.Count == 0 
                        ? "Журнал пуст" 
                        : string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
                    this.ButtonsEnabled = true;
                    return;
                }
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.GroupOfSetpoints,
                        record.Message,
                        record.Code,
                        record.TypeDamage,
                        record.ValueDamage(tt, koef),
                        ValuesConverterCommon.Analog.GetIrzt(record.Ia, tt, koef),
                        ValuesConverterCommon.Analog.GetIrzt(record.Ib, tt, koef),
                        ValuesConverterCommon.Analog.GetIrzt(record.Ic, tt, koef),
                        ValuesConverterCommon.Analog.GetUrzt(record.U, 1),
                        record.Imax1,
                        record.Imax2,
                        record.Imax3,
                        record.Vz1,
                        record.Vz2,
                        record.D1,
                        record.D2,
                        record.D3
                    ); 
                i++;
            }
            this._journalGrid.DataSource = this._table;
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this.ButtonsEnabled = true;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(RztDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(RztAlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "Журнал аварий"; }
        }
        #endregion [IFormView Members]

        private void AlarmJournalFormV2_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._journalGrid.DataSource = this._table;
            this.StartRead();
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("RZT_AJ");
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].Name);
            }
            return table;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this.ButtonsEnabled = false;
            this._measuringChannel.LoadStruct();
        }

        private bool ButtonsEnabled
        {
            set
            {
                this._saveAlarmJournalButton.Enabled = this._loadAlarmJournalButton.Enabled =
                    this._readAlarmJournalButton.Enabled = this._dropJaButton.Enabled = value;
            }
        }

        private void _readAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            this._saveAlarmJournalDialog.FileName = string.Format("РЗТ-110 Журнал аварий {0}", DateTime.Now.ToString().Replace(':', '.'));
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
                this._statusLabel.Text = "Журнал сохранён";
            }
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            }
        }

        private void _dropJaButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Данные журнала аварий\r\nбудут потеряны. Вы уверены,\r\nчто хотите очистить журнал?", string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.SetBitByAdress(0x1802, "Drop JA");
                this.StartRead();
            }
        }

        private void SetBitByAdress(ushort adress, string requestName)
        {
            this._device.SetBit(this._device.DeviceNumber, adress, true, requestName, this);
        }
    }
}
