﻿using System.Collections;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.RZT.Measuring.Structures
{
    /// <summary>
    /// Аналоговые сигналы
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct DiscretBdStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
        private ushort[] _array; //15

        #region [IStruct Members]
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLength);
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            this._array = Common.TOWORDS(array, false);

        }

        public ushort[] GetValues()
        {
            return this._array;
        }
        #endregion [IStructInit Members]

        /// <summary>
        /// Индикаторы
        /// </summary>
        public BitArray Indicators
        {
            get
            {
                bool[] values = new[]
                    {
                        Common.GetBit(this._array[0], 1),
                        Common.GetBit(this._array[0], 2),
                        Common.GetBit(this._array[3], 2)
                    };
                return new BitArray(values);
            }
        }
        /// <summary>
        /// Реле
        /// </summary>
        public BitArray Relay
        {
            get
            {
                bool[] values = new[]
                    {
                        Common.GetBit(this._array[0], 8),
                        Common.GetBit(this._array[0], 9),
                        Common.GetBit(this._array[0], 0)
                    };
                return new BitArray(values);
            }
        }
        
        /// <summary>
        /// Состояние неисправности
        /// </summary>
        public BitArray Fault
        {
            get
            {
                bool[] array = new[]
                    {
                        Common.GetBit(this._array[2], 0),
                        Common.GetBit(this._array[2], 1),
                        Common.GetBit(this._array[2], 2),
                        Common.GetBit(this._array[2], 3),
                        Common.GetBit(this._array[2], 4),
                        Common.GetBit(this._array[2], 5),
                        Common.GetBit(this._array[2], 6),
                        Common.GetBit(this._array[2], 7),
                        Common.GetBit(this._array[2], 8),
                        Common.GetBit(this._array[2], 9),
                        Common.GetBit(this._array[2], 10),
                        Common.GetBit(this._array[2], 11),
                        Common.GetBit(this._array[2], 12),
                        Common.GetBit(this._array[2], 13),
                        Common.GetBit(this._array[2], 14),
                        Common.GetBit(this._array[2], 15),
                        Common.GetBit(this._array[3], 0),
                        Common.GetBit(this._array[3], 1), //нет основного питания
                        Common.GetBit(this._array[3], 2)  //неисправность цепи отключения
                    };
                return new BitArray(array);
            }
        }
        /// <summary>
        /// Д1-Д8
        /// </summary>
        public BitArray Discrets
        {
            get
            {
                bool[] array = new[]
                    {
                        Common.GetBit(this._array[4], 8),
                        Common.GetBit(this._array[4], 9),
                        Common.GetBit(this._array[4], 10)
                    };
                return new BitArray(array);
            }
        }
  


        public BitArray SignalsState
        {
            get
            {
                bool[] array = new[]
                    {
                        Common.GetBit(this._array[4], 0),
                        Common.GetBit(this._array[4], 1),
                        Common.GetBit(this._array[4], 2),
                        Common.GetBit(this._array[4], 3),
                        Common.GetBit(this._array[4], 4),
                        Common.GetBit(this._array[4], 5),
                        Common.GetBit(this._array[4], 6),
                        Common.GetBit(this._array[4], 7)
                    };
                return new BitArray(array);
            }
        }


        public BitArray DefensesState
        {
            get
            {
                bool[] array = new[]
                    {
                        Common.GetBit(this._array[5], 0),
                        Common.GetBit(this._array[5], 1),
                        Common.GetBit(this._array[5], 2),
                        Common.GetBit(this._array[5], 3),
                        Common.GetBit(this._array[5], 4),
                        Common.GetBit(this._array[5], 5)
                    };
                return new BitArray(array);
            }
        }



        /// <summary>
        /// Внешние защиты
        /// </summary>
        public BitArray ExternalDefenses
        {
            get
            {
                bool[] array = new[]
                {
                    Common.GetBit(this._array[5], 8),
                    Common.GetBit(this._array[5], 9)
                };
                return new BitArray(array);
            }
        }

        /// <summary>
        /// Управляющие сигналы(СДТУ)
        /// </summary>
        public BitArray ManageSignals
        {
            get
            {
                bool[] array = new[]
                    {
                        !Common.GetBit(this._array[4], 3), //основная группа уставок
                        Common.GetBit(this._array[4], 3), //Резервная группа уставок 
                        Common.GetBit(this._array[0], 5), //Наличие неисправностей
                        Common.GetBit(this._array[0], 6), //Новая запись ЖС
                        Common.GetBit(this._array[0], 7),  //Новая запись ЖА
              //          Common.GetBit(this._array[7], 3),  //Uabc < 5V
              //          Common.GetBit(this._array[7], 7),  //Un < 5V
            
                    };
                return new BitArray(array);
            }
        }
       
    }
}
