﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.RZT.Measuring.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AnalogBdStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)] public ushort[] AnalogValues;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLength);
        }
        #endregion [IStruct Members]

        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {

            int index = 0;
            this.AnalogValues = new ushort[4];
            for (int i = 0; i < this.AnalogValues.Length; i++)
            {
                this.AnalogValues[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(UInt16);
            }
        }

        public ushort[] GetValues()
        {
            return this.AnalogValues;
        }

        #endregion [IStructInit Members]
    }
}
