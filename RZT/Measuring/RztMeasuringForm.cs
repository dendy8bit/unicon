﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.RZT.Configuration;
using BEMN.RZT.Configuration.Structures;
using BEMN.RZT.Measuring.Structures;

namespace BEMN.RZT.Measuring
{
    public partial class RztMeasuringForm : Form, IFormView
    {
        #region [Constants]
        private const string MAIN_GROUP_SETPOINTS_ON = "Включить основную группу уставок ?";
        private const string RESET_SJ_OK = "Сбросить журнал системы?";
        private const string RESET_SJ = "Сбросить журнал системы";
        private const string RESET_FAULT_OK = "Сбросить неисправность?";
        private const string RESET_FAULT = "Сбросить неисправность";
        private const string RESET_AJ_OK = "Сбросить журнал аварий?";
        private const string RESET_AJ = "Сбросить журнал аварий";
        private const string RESERV_GROUP_SETPOINTS_ON = "Включить резервную группу уставок ?";
        private const string RESET_IND_OK = "Сбросить индикацию?";
        private const string RESET_IND = "Сбросить индикацию";

        #endregion [Constants]


        #region [Private fields]

        private readonly RztDevice _device;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<DiscretBdStruct> _discretBd;
        private readonly MemoryEntity<MeasuringConfigStruct> _measuringChannel;
        private readonly MemoryEntity<AnalogBdStruct> _analogBd;
        private TextBox[] _analogBoxes;

        #region [Массивы индикаторов]
        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _indicators;
        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;
        /// <summary>
        /// Состояние неисправности
        /// </summary>
        private LedControl[] _fault;
        /// <summary>
        /// Д1-Д8
        /// </summary>
        private LedControl[] _discrets;
        //
        private LedControl[] _signalsState;
        /// <summary>
        /// U0, U1, U2
        /// </summary>
        private LedControl[] _ISignals;
        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;
        /// <summary>
        /// Управляющие сигналы(СДТУ)
        /// </summary>
        private LedControl[] _manageSignals;
        #endregion [Массивы индикаторов]


        #endregion [Private fields]


        #region [Ctor's]

        public RztMeasuringForm()
        {
            InitializeComponent();
        }

        public RztMeasuringForm(RztDevice device)
        {
            InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            
            this._dateTime = this._device.DateTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, DateTimeLoad);

            this._discretBd = this._device.DiscretBd;
            this._discretBd.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, DiscretBdLoadOk);
            this._discretBd.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, DiscretBdLoadFail);

            this._analogBd = this._device.AnalogBd;
            this._analogBd.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, AnalogBdLoadOk);
            this._analogBd.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, AnalogBdLoadFail);

            this._measuringChannel = this._device.MeasuringChannel;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._analogBd.LoadStruct);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, MeasuringChannelLoadFail);
            this._device.Configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._measuringChannel.LoadStruct);

            this.Prepare();
        }
        private void Prepare()
        {
            this._analogBoxes = new[]
            {
                this._box1, this._box2, this._box3, this._box4
            };
            this._manageSignals = new[]
            {
                this._mainGroupLed, this._resGroupLed, this._faultLed, this._sjLed, this._ajLed
            };
            this._externalDefenses = new[]
            {
                this._extDefenseLed1, this._extDefenseLed2
            };

            this._ISignals = new[]
            {
                this._I1, this._I2, this._I3, this._I4, this._I5, this._I6
            };
            this._signalsState = new[]
            {
                new LedControl(), this._UmaxLed2, this._UmaxLed3, this._UmaxLed4,
                this._UmaxLed5, this._UmaxLed6, this._UmaxLed7, this._UmaxLed8
            };

            this._discrets = new[]
            {
                this._inD_led1, this._inD_led2
            };
            this._fault = new[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, new LedControl(), 
                this._faultSignalLed5, new LedControl(), new LedControl(), new LedControl(),
                this._faultSignalLed9, this._faultSignalLed10, this._faultSignalFlash, this._faultSignalLed11,
                this._faultSignalLed12, this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15,
                this._faultSignalLed16, this._faultSignalLed17, this._faultSignalLed18
            };
            this._indicators = new[]
            {
                this._indLed1,this._indLed2,this._indLed3
            };
            this._relays = new[]
            {
                this._releLed1,this._releLed2,this._releFault
            };
        }
        #endregion [Ctor's]

        #region [Methods]
        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show("Данные каналов не загружены");
        }

        private void AnalogBdLoadOk()
        {
            ushort[] analog = this._analogBd.Value.AnalogValues;
            double koef = Common.VersionConverter(_device.DeviceVersion) >= 1.10 ? 8 : 40;
            if (this._measuringChannel.Value.Type)
            {
                koef /= Math.Sqrt(3);
            }
            ushort tt = this._measuringChannel.Value.Tt;
            string[] result = new string[analog.Length];
            for (int i = 0; i < analog.Length; i++)
            {
                string value = i != 3 
                    ? ValuesConverterCommon.Analog.GetIrzt(analog[i], tt, koef)
                    : ValuesConverterCommon.Analog.GetUrzt(analog[i],1);
                result[i] = string.Format(StringsConfig.ChannelsNamesPattern[i], value);
            }
            this.SetTextBoxes(this._analogBoxes, result);
        }

        private void SetTextBoxes(TextBox[] boxes, string[] values)
        {
            int l = boxes.Length;
            if (l > values.Length) l = values.Length;
            for (int i = 0; i < l; i++)
            {
                boxes[i].Text = values[i];
            }
        }

        private void AnalogBdLoadFail()
        {
            foreach (var analogBox in _analogBoxes)
            {
                analogBox.Text = string.Empty;
            }
        }
        
        /// <summary>
        /// Загружены аналоговые сигналы
        /// </summary>
        private void DiscretBdLoadOk()
        {
           LedManager.SetLeds(this._indicators,this._discretBd.Value.Indicators);
           LedManager.SetLeds(this._relays, this._discretBd.Value.Relay);
           LedManager.SetLeds(this._fault, this._discretBd.Value.Fault);
           LedManager.SetLeds(this._discrets, this._discretBd.Value.Discrets);
           LedManager.SetLeds(this._signalsState, this._discretBd.Value.SignalsState);
           LedManager.SetLeds(this._ISignals, this._discretBd.Value.DefensesState);
           LedManager.SetLeds(this._externalDefenses, this._discretBd.Value.ExternalDefenses);
           LedManager.SetLeds(this._manageSignals, this._discretBd.Value.ManageSignals);
        }

        private void DiscretBdLoadFail()
        {
            LedManager.TurnOffLeds(this._indicators);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._fault);
            LedManager.TurnOffLeds(this._discrets);
            LedManager.TurnOffLeds(this._signalsState);
            LedManager.TurnOffLeds(this._ISignals);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._manageSignals);
        }
        
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }
        private void _dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }
        /// <summary>
        /// Загрузка формы
        /// </summary>
        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            StartStopRead();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._dateTime.RemoveStructQueries();
            this._measuringChannel.RemoveStructQueries();
            this._discretBd.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._dateTime.LoadStructCycle();
                this._measuringChannel.LoadStructCycle();
                this._discretBd.LoadStructCycle();
            }
            else
            {
                this._dateTime.RemoveStructQueries();
                this._measuringChannel.RemoveStructQueries();
                this._discretBd.RemoveStructQueries();
                this.AnalogBdLoadFail();
                this.DiscretBdLoadFail();
            }
        }

        private void SetBitByAdress(ushort adress, string requestName)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(requestName+"?", string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.SetBit(this._device.DeviceNumber, adress, true, requestName, this);
            }
        }

        private void _constraintToggleButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(MAIN_GROUP_SETPOINTS_ON, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.ChangeGroup.Value = new OneWordStruct(0);
                this._device.ChangeGroup.SaveStruct();
            }
        }
        /// <summary>
        /// Сбросить журнал системы
        /// </summary>
        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x1806, RESET_SJ);
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x1805, RESET_FAULT);
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x1807, RESET_AJ);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(RESERV_GROUP_SETPOINTS_ON, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.ChangeGroup.Value = new OneWordStruct(1);
                this._device.ChangeGroup.SaveStruct();
            }
        }

        private void _resetInd_But_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x1804, RESET_IND);
        }

        private void _dropJsButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Данные журнала системы\r\nбудут потеряны. Вы уверены,\r\nчто хотите очистить журнал?", string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.SetBit(this._device.DeviceNumber, 0x1801, true, "Drop JS", this);
            }
        }

        private void _dropJaButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Данные журнала аварий\r\nбудут потеряны. Вы уверены,\r\nчто хотите очистить журнал?", string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.SetBit(this._device.DeviceNumber, 0x1802, true, "Drop JA", this);
            }
        }

        private void _dropJoButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Сбросить журнал осциллографа?", string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.SetBit(this._device.DeviceNumber, 0x1803, true, "Drop JO", this);
            }
        }
        #endregion


        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (RztDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public Type ClassType
        {
            get { return typeof(RztMeasuringForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        #endregion [IFormView Members]

    }
}
