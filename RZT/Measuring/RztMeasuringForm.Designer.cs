﻿namespace BEMN.RZT.Measuring
{
    partial class RztMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this._faultSignalLed18 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label124 = new System.Windows.Forms.Label();
            this._faultSignalLed5 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this._faultSignalLed3 = new BEMN.Forms.LedControl();
            this.label127 = new System.Windows.Forms.Label();
            this._faultSignalLed2 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this._faultSignalLed1 = new BEMN.Forms.LedControl();
            this._faultSignalLed17 = new BEMN.Forms.LedControl();
            this._faultSignalLed16 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this._faultSignalFlash = new BEMN.Forms.LedControl();
            this.label129 = new System.Windows.Forms.Label();
            this._faultSignalLed15 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._faultSignalLed14 = new BEMN.Forms.LedControl();
            this.label131 = new System.Windows.Forms.Label();
            this._faultSignalLed13 = new BEMN.Forms.LedControl();
            this.label132 = new System.Windows.Forms.Label();
            this._faultSignalLed12 = new BEMN.Forms.LedControl();
            this.label133 = new System.Windows.Forms.Label();
            this._faultSignalLed11 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._faultSignalLed10 = new BEMN.Forms.LedControl();
            this.label136 = new System.Windows.Forms.Label();
            this._faultSignalLed9 = new BEMN.Forms.LedControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._I6 = new BEMN.Forms.LedControl();
            this._I5 = new BEMN.Forms.LedControl();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._I4 = new BEMN.Forms.LedControl();
            this._I3 = new BEMN.Forms.LedControl();
            this._I2 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._I1 = new BEMN.Forms.LedControl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label111 = new System.Windows.Forms.Label();
            this._extDefenseLed2 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._extDefenseLed1 = new BEMN.Forms.LedControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this._inD_led2 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._inD_led1 = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this._releFault = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._releLed2 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._releLed1 = new BEMN.Forms.LedControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this._UmaxLed8 = new BEMN.Forms.LedControl();
            this._UmaxLed7 = new BEMN.Forms.LedControl();
            this._UmaxLed6 = new BEMN.Forms.LedControl();
            this._UmaxLed5 = new BEMN.Forms.LedControl();
            this._UmaxLed4 = new BEMN.Forms.LedControl();
            this._UmaxLed3 = new BEMN.Forms.LedControl();
            this._UmaxLed2 = new BEMN.Forms.LedControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._dropJoButton = new System.Windows.Forms.Button();
            this._dropJaButton = new System.Windows.Forms.Button();
            this._dropJsButton = new System.Windows.Forms.Button();
            this._resetInd_But = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this._resGroupLed = new BEMN.Forms.LedControl();
            this.label137 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this._sjLed = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._faultLed = new BEMN.Forms.LedControl();
            this._ajLed = new BEMN.Forms.LedControl();
            this._resetJA_But = new System.Windows.Forms.Button();
            this._resetJS_But = new System.Windows.Forms.Button();
            this._resetFaultBut = new System.Windows.Forms.Button();
            this._constraintToggleButton = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this._mainGroupLed = new BEMN.Forms.LedControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._box4 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._box1 = new System.Windows.Forms.TextBox();
            this._box3 = new System.Windows.Forms.TextBox();
            this._box2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this._indLed3 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._indLed2 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._indLed1 = new BEMN.Forms.LedControl();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox7.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this._faultSignalLed18);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.groupBox18);
            this.groupBox7.Controls.Add(this._faultSignalLed17);
            this.groupBox7.Controls.Add(this._faultSignalLed16);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.groupBox19);
            this.groupBox7.Location = new System.Drawing.Point(159, 362);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(378, 204);
            this.groupBox7.TabIndex = 42;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Сигналы неисправности";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(27, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(183, 26);
            this.label2.TabIndex = 39;
            this.label2.Text = "Недостаточное питание для выполнения функций защиты";
            // 
            // _faultSignalLed18
            // 
            this._faultSignalLed18.Location = new System.Drawing.Point(12, 169);
            this._faultSignalLed18.Name = "_faultSignalLed18";
            this._faultSignalLed18.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed18.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed18.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Неиспр. цепи отключения";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label124);
            this.groupBox18.Controls.Add(this._faultSignalLed5);
            this.groupBox18.Controls.Add(this.label126);
            this.groupBox18.Controls.Add(this._faultSignalLed3);
            this.groupBox18.Controls.Add(this.label127);
            this.groupBox18.Controls.Add(this._faultSignalLed2);
            this.groupBox18.Controls.Add(this.label128);
            this.groupBox18.Controls.Add(this._faultSignalLed1);
            this.groupBox18.Location = new System.Drawing.Point(6, 19);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(154, 94);
            this.groupBox18.TabIndex = 23;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Аппаратные";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(21, 75);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(56, 13);
            this.label124.TabIndex = 25;
            this.label124.Text = "Каналов I";
            // 
            // _faultSignalLed5
            // 
            this._faultSignalLed5.Location = new System.Drawing.Point(6, 76);
            this._faultSignalLed5.Name = "_faultSignalLed5";
            this._faultSignalLed5.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed5.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed5.TabIndex = 24;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(21, 38);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(99, 13);
            this.label126.TabIndex = 21;
            this.label126.Text = "Ошибка  шины I2c";
            // 
            // _faultSignalLed3
            // 
            this._faultSignalLed3.Location = new System.Drawing.Point(6, 57);
            this._faultSignalLed3.Name = "_faultSignalLed3";
            this._faultSignalLed3.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed3.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed3.TabIndex = 20;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(21, 57);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(117, 13);
            this.label127.TabIndex = 19;
            this.label127.Text = "Ошибка температуры";
            // 
            // _faultSignalLed2
            // 
            this._faultSignalLed2.Location = new System.Drawing.Point(6, 38);
            this._faultSignalLed2.Name = "_faultSignalLed2";
            this._faultSignalLed2.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed2.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed2.TabIndex = 18;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(21, 19);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(73, 13);
            this.label128.TabIndex = 17;
            this.label128.Text = "Ошибка ОЗУ";
            // 
            // _faultSignalLed1
            // 
            this._faultSignalLed1.Location = new System.Drawing.Point(6, 19);
            this._faultSignalLed1.Name = "_faultSignalLed1";
            this._faultSignalLed1.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed1.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed1.TabIndex = 16;
            // 
            // _faultSignalLed17
            // 
            this._faultSignalLed17.Location = new System.Drawing.Point(12, 144);
            this._faultSignalLed17.Name = "_faultSignalLed17";
            this._faultSignalLed17.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed17.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed17.TabIndex = 36;
            // 
            // _faultSignalLed16
            // 
            this._faultSignalLed16.Location = new System.Drawing.Point(12, 119);
            this._faultSignalLed16.Name = "_faultSignalLed16";
            this._faultSignalLed16.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed16.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed16.TabIndex = 35;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(27, 119);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(167, 13);
            this.label33.TabIndex = 34;
            this.label33.Text = "Низкий заряд на конденсаторе";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label5);
            this.groupBox19.Controls.Add(this._faultSignalFlash);
            this.groupBox19.Controls.Add(this.label129);
            this.groupBox19.Controls.Add(this._faultSignalLed15);
            this.groupBox19.Controls.Add(this.label130);
            this.groupBox19.Controls.Add(this._faultSignalLed14);
            this.groupBox19.Controls.Add(this.label131);
            this.groupBox19.Controls.Add(this._faultSignalLed13);
            this.groupBox19.Controls.Add(this.label132);
            this.groupBox19.Controls.Add(this._faultSignalLed12);
            this.groupBox19.Controls.Add(this.label133);
            this.groupBox19.Controls.Add(this._faultSignalLed11);
            this.groupBox19.Controls.Add(this.label134);
            this.groupBox19.Controls.Add(this._faultSignalLed10);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this._faultSignalLed9);
            this.groupBox19.Location = new System.Drawing.Point(216, 19);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(156, 133);
            this.groupBox19.TabIndex = 24;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Программные";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Ошибка размера flash";
            // 
            // _faultSignalFlash
            // 
            this._faultSignalFlash.Location = new System.Drawing.Point(6, 57);
            this._faultSignalFlash.Name = "_faultSignalFlash";
            this._faultSignalFlash.Size = new System.Drawing.Size(13, 13);
            this._faultSignalFlash.State = BEMN.Forms.LedState.Off;
            this._faultSignalFlash.TabIndex = 32;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(21, 279);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(94, 13);
            this.label129.TabIndex = 31;
            this.label129.Text = "О. задачи логики";
            this.label129.Visible = false;
            // 
            // _faultSignalLed15
            // 
            this._faultSignalLed15.Location = new System.Drawing.Point(6, 280);
            this._faultSignalLed15.Name = "_faultSignalLed15";
            this._faultSignalLed15.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed15.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed15.TabIndex = 30;
            this._faultSignalLed15.Visible = false;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(20, 133);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(123, 13);
            this.label130.TabIndex = 29;
            this.label130.Text = "Ошибка осциллографа";
            this.label130.Visible = false;
            // 
            // _faultSignalLed14
            // 
            this._faultSignalLed14.Location = new System.Drawing.Point(6, 133);
            this._faultSignalLed14.Name = "_faultSignalLed14";
            this._faultSignalLed14.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed14.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed14.TabIndex = 28;
            this._faultSignalLed14.Visible = false;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(21, 114);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(79, 13);
            this.label131.TabIndex = 27;
            this.label131.Text = "Ошибка часов";
            // 
            // _faultSignalLed13
            // 
            this._faultSignalLed13.Location = new System.Drawing.Point(6, 114);
            this._faultSignalLed13.Name = "_faultSignalLed13";
            this._faultSignalLed13.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed13.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed13.TabIndex = 26;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(21, 95);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(68, 13);
            this.label132.TabIndex = 25;
            this.label132.Text = "Ошибка ЖА";
            // 
            // _faultSignalLed12
            // 
            this._faultSignalLed12.Location = new System.Drawing.Point(6, 95);
            this._faultSignalLed12.Name = "_faultSignalLed12";
            this._faultSignalLed12.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed12.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed12.TabIndex = 24;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(21, 76);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(68, 13);
            this.label133.TabIndex = 23;
            this.label133.Text = "Ошибка ЖС";
            // 
            // _faultSignalLed11
            // 
            this._faultSignalLed11.Location = new System.Drawing.Point(6, 76);
            this._faultSignalLed11.Name = "_faultSignalLed11";
            this._faultSignalLed11.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed11.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed11.TabIndex = 22;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(21, 38);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(113, 13);
            this.label134.TabIndex = 21;
            this.label134.Text = "Ошибка коэфф. АЦП";
            // 
            // _faultSignalLed10
            // 
            this._faultSignalLed10.Location = new System.Drawing.Point(6, 38);
            this._faultSignalLed10.Name = "_faultSignalLed10";
            this._faultSignalLed10.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed10.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed10.TabIndex = 18;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(21, 19);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(90, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "Ошибка уставок";
            // 
            // _faultSignalLed9
            // 
            this._faultSignalLed9.Location = new System.Drawing.Point(6, 19);
            this._faultSignalLed9.Name = "_faultSignalLed9";
            this._faultSignalLed9.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed9.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed9.TabIndex = 16;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox12);
            this.groupBox4.Controls.Add(this.groupBox16);
            this.groupBox4.Location = new System.Drawing.Point(12, 247);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(235, 109);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Состояние защит";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._I6);
            this.groupBox12.Controls.Add(this._I5);
            this.groupBox12.Controls.Add(this.label80);
            this.groupBox12.Controls.Add(this.label81);
            this.groupBox12.Controls.Add(this.label82);
            this.groupBox12.Controls.Add(this.label83);
            this.groupBox12.Controls.Add(this._I4);
            this.groupBox12.Controls.Add(this._I3);
            this.groupBox12.Controls.Add(this._I2);
            this.groupBox12.Controls.Add(this.label84);
            this.groupBox12.Controls.Add(this._I1);
            this.groupBox12.Location = new System.Drawing.Point(6, 15);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(107, 88);
            this.groupBox12.TabIndex = 21;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Токовые";
            // 
            // _I6
            // 
            this._I6.Location = new System.Drawing.Point(35, 63);
            this._I6.Name = "_I6";
            this._I6.Size = new System.Drawing.Size(13, 13);
            this._I6.State = BEMN.Forms.LedState.Off;
            this._I6.TabIndex = 22;
            // 
            // _I5
            // 
            this._I5.Location = new System.Drawing.Point(8, 63);
            this._I5.Name = "_I5";
            this._I5.Size = new System.Drawing.Size(13, 13);
            this._I5.State = BEMN.Forms.LedState.Off;
            this._I5.TabIndex = 21;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(50, 63);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(28, 13);
            this.label80.TabIndex = 19;
            this.label80.Text = "I>>>";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(50, 47);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(22, 13);
            this.label81.TabIndex = 18;
            this.label81.Text = "I>>";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(50, 31);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(16, 13);
            this.label82.TabIndex = 17;
            this.label82.Text = "I>";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(26, 14);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 13);
            this.label83.TabIndex = 16;
            this.label83.Text = "Сраб.";
            // 
            // _I4
            // 
            this._I4.Location = new System.Drawing.Point(35, 47);
            this._I4.Name = "_I4";
            this._I4.Size = new System.Drawing.Size(13, 13);
            this._I4.State = BEMN.Forms.LedState.Off;
            this._I4.TabIndex = 6;
            // 
            // _I3
            // 
            this._I3.Location = new System.Drawing.Point(8, 47);
            this._I3.Name = "_I3";
            this._I3.Size = new System.Drawing.Size(13, 13);
            this._I3.State = BEMN.Forms.LedState.Off;
            this._I3.TabIndex = 4;
            // 
            // _I2
            // 
            this._I2.Location = new System.Drawing.Point(35, 31);
            this._I2.Name = "_I2";
            this._I2.Size = new System.Drawing.Size(13, 13);
            this._I2.State = BEMN.Forms.LedState.Off;
            this._I2.TabIndex = 2;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(5, 14);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(23, 13);
            this.label84.TabIndex = 1;
            this.label84.Text = "ИО";
            // 
            // _I1
            // 
            this._I1.Location = new System.Drawing.Point(8, 31);
            this._I1.Name = "_I1";
            this._I1.Size = new System.Drawing.Size(13, 13);
            this._I1.State = BEMN.Forms.LedState.Off;
            this._I1.TabIndex = 0;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label111);
            this.groupBox16.Controls.Add(this._extDefenseLed2);
            this.groupBox16.Controls.Add(this.label112);
            this.groupBox16.Controls.Add(this._extDefenseLed1);
            this.groupBox16.Location = new System.Drawing.Point(118, 15);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(94, 69);
            this.groupBox16.TabIndex = 22;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Внешние";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(28, 44);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(36, 13);
            this.label111.TabIndex = 35;
            this.label111.Text = "ВЗ - 2";
            // 
            // _extDefenseLed2
            // 
            this._extDefenseLed2.Location = new System.Drawing.Point(9, 44);
            this._extDefenseLed2.Name = "_extDefenseLed2";
            this._extDefenseLed2.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed2.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed2.TabIndex = 34;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(28, 23);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(36, 13);
            this.label112.TabIndex = 33;
            this.label112.Text = "ВЗ - 1";
            // 
            // _extDefenseLed1
            // 
            this._extDefenseLed1.Location = new System.Drawing.Point(9, 23);
            this._extDefenseLed1.Name = "_extDefenseLed1";
            this._extDefenseLed1.Size = new System.Drawing.Size(13, 13);
            this._extDefenseLed1.State = BEMN.Forms.LedState.Off;
            this._extDefenseLed1.TabIndex = 32;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this._inD_led2);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this._inD_led1);
            this.groupBox5.Location = new System.Drawing.Point(12, 138);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(84, 55);
            this.groupBox5.TabIndex = 39;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Дискреты*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(20, 34);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(22, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "Д2";
            // 
            // _inD_led2
            // 
            this._inD_led2.Location = new System.Drawing.Point(6, 34);
            this._inD_led2.Name = "_inD_led2";
            this._inD_led2.Size = new System.Drawing.Size(13, 13);
            this._inD_led2.State = BEMN.Forms.LedState.Off;
            this._inD_led2.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(20, 19);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(22, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Д1";
            // 
            // _inD_led1
            // 
            this._inD_led1.Location = new System.Drawing.Point(6, 19);
            this._inD_led1.Name = "_inD_led1";
            this._inD_led1.Size = new System.Drawing.Size(13, 13);
            this._inD_led1.State = BEMN.Forms.LedState.Off;
            this._inD_led1.TabIndex = 32;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this._releFault);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this._releLed2);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this._releLed1);
            this.groupBox3.Location = new System.Drawing.Point(107, 138);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(117, 70);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Реле";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Неисправность";
            // 
            // _releFault
            // 
            this._releFault.Location = new System.Drawing.Point(6, 49);
            this._releFault.Name = "_releFault";
            this._releFault.Size = new System.Drawing.Size(13, 13);
            this._releFault.State = BEMN.Forms.LedState.Off;
            this._releFault.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "2";
            // 
            // _releLed2
            // 
            this._releLed2.Location = new System.Drawing.Point(6, 34);
            this._releLed2.Name = "_releLed2";
            this._releLed2.Size = new System.Drawing.Size(13, 13);
            this._releLed2.State = BEMN.Forms.LedState.Off;
            this._releLed2.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "1";
            // 
            // _releLed1
            // 
            this._releLed1.Location = new System.Drawing.Point(6, 19);
            this._releLed1.Name = "_releLed1";
            this._releLed1.Size = new System.Drawing.Size(13, 13);
            this._releLed1.State = BEMN.Forms.LedState.Off;
            this._releLed1.TabIndex = 16;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.label39);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this._UmaxLed8);
            this.groupBox9.Controls.Add(this._UmaxLed7);
            this.groupBox9.Controls.Add(this._UmaxLed6);
            this.groupBox9.Controls.Add(this._UmaxLed5);
            this.groupBox9.Controls.Add(this._UmaxLed4);
            this.groupBox9.Controls.Add(this._UmaxLed3);
            this.groupBox9.Controls.Add(this._UmaxLed2);
            this.groupBox9.Location = new System.Drawing.Point(12, 362);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 204);
            this.groupBox9.TabIndex = 36;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Состояние сигналов";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(25, 178);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(84, 13);
            this.label34.TabIndex = 39;
            this.label34.Text = "Форсирование";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(25, 152);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 13);
            this.label35.TabIndex = 38;
            this.label35.Text = "Авария";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(25, 125);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 13);
            this.label36.TabIndex = 37;
            this.label36.Text = "Сигнализация";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(25, 99);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(69, 13);
            this.label37.TabIndex = 36;
            this.label37.Text = "Отключение";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(25, 73);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(85, 13);
            this.label38.TabIndex = 35;
            this.label38.Text = "Группа уставок";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(25, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(51, 13);
            this.label39.TabIndex = 34;
            this.label39.Text = "Uк>Uyст";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(25, 48);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(86, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "Неисправность";
            // 
            // _UmaxLed8
            // 
            this._UmaxLed8.Location = new System.Drawing.Point(6, 178);
            this._UmaxLed8.Name = "_UmaxLed8";
            this._UmaxLed8.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed8.State = BEMN.Forms.LedState.Off;
            this._UmaxLed8.TabIndex = 24;
            // 
            // _UmaxLed7
            // 
            this._UmaxLed7.Location = new System.Drawing.Point(6, 152);
            this._UmaxLed7.Name = "_UmaxLed7";
            this._UmaxLed7.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed7.State = BEMN.Forms.LedState.Off;
            this._UmaxLed7.TabIndex = 23;
            // 
            // _UmaxLed6
            // 
            this._UmaxLed6.Location = new System.Drawing.Point(6, 125);
            this._UmaxLed6.Name = "_UmaxLed6";
            this._UmaxLed6.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed6.State = BEMN.Forms.LedState.Off;
            this._UmaxLed6.TabIndex = 22;
            // 
            // _UmaxLed5
            // 
            this._UmaxLed5.Location = new System.Drawing.Point(6, 100);
            this._UmaxLed5.Name = "_UmaxLed5";
            this._UmaxLed5.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed5.State = BEMN.Forms.LedState.Off;
            this._UmaxLed5.TabIndex = 21;
            // 
            // _UmaxLed4
            // 
            this._UmaxLed4.Location = new System.Drawing.Point(6, 73);
            this._UmaxLed4.Name = "_UmaxLed4";
            this._UmaxLed4.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed4.State = BEMN.Forms.LedState.Off;
            this._UmaxLed4.TabIndex = 6;
            // 
            // _UmaxLed3
            // 
            this._UmaxLed3.Location = new System.Drawing.Point(6, 48);
            this._UmaxLed3.Name = "_UmaxLed3";
            this._UmaxLed3.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed3.State = BEMN.Forms.LedState.Off;
            this._UmaxLed3.TabIndex = 4;
            // 
            // _UmaxLed2
            // 
            this._UmaxLed2.Location = new System.Drawing.Point(6, 22);
            this._UmaxLed2.Name = "_UmaxLed2";
            this._UmaxLed2.Size = new System.Drawing.Size(13, 13);
            this._UmaxLed2.State = BEMN.Forms.LedState.Off;
            this._UmaxLed2.TabIndex = 2;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._dropJoButton);
            this.groupBox8.Controls.Add(this._dropJaButton);
            this.groupBox8.Controls.Add(this._dropJsButton);
            this.groupBox8.Controls.Add(this._resetInd_But);
            this.groupBox8.Controls.Add(this.button1);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this._resGroupLed);
            this.groupBox8.Controls.Add(this.label137);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this._sjLed);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this._faultLed);
            this.groupBox8.Controls.Add(this._ajLed);
            this.groupBox8.Controls.Add(this._resetJA_But);
            this.groupBox8.Controls.Add(this._resetJS_But);
            this.groupBox8.Controls.Add(this._resetFaultBut);
            this.groupBox8.Controls.Add(this._constraintToggleButton);
            this.groupBox8.Controls.Add(this.label50);
            this.groupBox8.Controls.Add(this._mainGroupLed);
            this.groupBox8.Location = new System.Drawing.Point(477, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(267, 309);
            this.groupBox8.TabIndex = 32;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Управляющие сигналы(СДТУ)";
            // 
            // _dropJoButton
            // 
            this._dropJoButton.Location = new System.Drawing.Point(12, 267);
            this._dropJoButton.Name = "_dropJoButton";
            this._dropJoButton.Size = new System.Drawing.Size(245, 23);
            this._dropJoButton.TabIndex = 81;
            this._dropJoButton.Text = "Сброс осциллографа";
            this._dropJoButton.UseVisualStyleBackColor = true;
            this._dropJoButton.Click += new System.EventHandler(this._dropJoButton_Click);
            // 
            // _dropJaButton
            // 
            this._dropJaButton.Location = new System.Drawing.Point(12, 238);
            this._dropJaButton.Name = "_dropJaButton";
            this._dropJaButton.Size = new System.Drawing.Size(245, 23);
            this._dropJaButton.TabIndex = 80;
            this._dropJaButton.Text = "Очистить журнал аварий";
            this._dropJaButton.UseVisualStyleBackColor = true;
            this._dropJaButton.Click += new System.EventHandler(this._dropJaButton_Click);
            // 
            // _dropJsButton
            // 
            this._dropJsButton.Location = new System.Drawing.Point(12, 209);
            this._dropJsButton.Name = "_dropJsButton";
            this._dropJsButton.Size = new System.Drawing.Size(245, 23);
            this._dropJsButton.TabIndex = 79;
            this._dropJsButton.Text = "Очистить журнал системы";
            this._dropJsButton.UseVisualStyleBackColor = true;
            this._dropJsButton.Click += new System.EventHandler(this._dropJsButton_Click);
            // 
            // _resetInd_But
            // 
            this._resetInd_But.Location = new System.Drawing.Point(12, 180);
            this._resetInd_But.Name = "_resetInd_But";
            this._resetInd_But.Size = new System.Drawing.Size(245, 23);
            this._resetInd_But.TabIndex = 78;
            this._resetInd_But.Text = "Сброс индикации";
            this._resetInd_But.UseVisualStyleBackColor = true;
            this._resetInd_But.Click += new System.EventHandler(this._resetInd_But_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 77;
            this.button1.Text = "Перекл.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(31, 69);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(145, 13);
            this.label61.TabIndex = 76;
            this.label61.Text = "Резервная группа уставок ";
            // 
            // _resGroupLed
            // 
            this._resGroupLed.Location = new System.Drawing.Point(12, 69);
            this._resGroupLed.Name = "_resGroupLed";
            this._resGroupLed.Size = new System.Drawing.Size(13, 13);
            this._resGroupLed.State = BEMN.Forms.LedState.Off;
            this._resGroupLed.TabIndex = 75;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(31, 127);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(99, 13);
            this.label137.TabIndex = 68;
            this.label137.Text = "Новая запись ЖС";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(31, 156);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(99, 13);
            this.label51.TabIndex = 65;
            this.label51.Text = "Новая запись ЖА";
            // 
            // _sjLed
            // 
            this._sjLed.Location = new System.Drawing.Point(12, 127);
            this._sjLed.Name = "_sjLed";
            this._sjLed.Size = new System.Drawing.Size(13, 13);
            this._sjLed.State = BEMN.Forms.LedState.Off;
            this._sjLed.TabIndex = 64;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(31, 98);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 13);
            this.label58.TabIndex = 63;
            this.label58.Text = "Наличие неисправностей";
            // 
            // _faultLed
            // 
            this._faultLed.Location = new System.Drawing.Point(12, 98);
            this._faultLed.Name = "_faultLed";
            this._faultLed.Size = new System.Drawing.Size(13, 13);
            this._faultLed.State = BEMN.Forms.LedState.Off;
            this._faultLed.TabIndex = 62;
            // 
            // _ajLed
            // 
            this._ajLed.Location = new System.Drawing.Point(12, 156);
            this._ajLed.Name = "_ajLed";
            this._ajLed.Size = new System.Drawing.Size(13, 13);
            this._ajLed.State = BEMN.Forms.LedState.Off;
            this._ajLed.TabIndex = 56;
            // 
            // _resetJA_But
            // 
            this._resetJA_But.Location = new System.Drawing.Point(182, 151);
            this._resetJA_But.Name = "_resetJA_But";
            this._resetJA_But.Size = new System.Drawing.Size(75, 23);
            this._resetJA_But.TabIndex = 53;
            this._resetJA_But.Text = "Сбросить";
            this._resetJA_But.UseVisualStyleBackColor = true;
            this._resetJA_But.Click += new System.EventHandler(this._resetJA_But_Click);
            // 
            // _resetJS_But
            // 
            this._resetJS_But.Location = new System.Drawing.Point(182, 122);
            this._resetJS_But.Name = "_resetJS_But";
            this._resetJS_But.Size = new System.Drawing.Size(75, 23);
            this._resetJS_But.TabIndex = 52;
            this._resetJS_But.Text = "Сбросить";
            this._resetJS_But.UseVisualStyleBackColor = true;
            this._resetJS_But.Click += new System.EventHandler(this._resetJS_But_Click);
            // 
            // _resetFaultBut
            // 
            this._resetFaultBut.Location = new System.Drawing.Point(182, 93);
            this._resetFaultBut.Name = "_resetFaultBut";
            this._resetFaultBut.Size = new System.Drawing.Size(75, 23);
            this._resetFaultBut.TabIndex = 51;
            this._resetFaultBut.Text = "Сбросить";
            this._resetFaultBut.UseVisualStyleBackColor = true;
            this._resetFaultBut.Click += new System.EventHandler(this._resetFaultBut_Click);
            // 
            // _constraintToggleButton
            // 
            this._constraintToggleButton.Location = new System.Drawing.Point(182, 35);
            this._constraintToggleButton.Name = "_constraintToggleButton";
            this._constraintToggleButton.Size = new System.Drawing.Size(75, 23);
            this._constraintToggleButton.TabIndex = 47;
            this._constraintToggleButton.Text = "Перекл.";
            this._constraintToggleButton.UseVisualStyleBackColor = true;
            this._constraintToggleButton.Click += new System.EventHandler(this._constraintToggleButton_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(31, 40);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(140, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "Основная группа уставок ";
            // 
            // _mainGroupLed
            // 
            this._mainGroupLed.Location = new System.Drawing.Point(12, 40);
            this._mainGroupLed.Name = "_mainGroupLed";
            this._mainGroupLed.Size = new System.Drawing.Size(13, 13);
            this._mainGroupLed.State = BEMN.Forms.LedState.Off;
            this._mainGroupLed.TabIndex = 32;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox2);
            this.groupBox6.Controls.Add(this.groupBox1);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(263, 120);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Измерения";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._box4);
            this.groupBox2.Location = new System.Drawing.Point(128, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(116, 69);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Напряжение конденсатора";
            // 
            // _box4
            // 
            this._box4.Enabled = false;
            this._box4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._box4.Location = new System.Drawing.Point(6, 34);
            this._box4.Name = "_box4";
            this._box4.Size = new System.Drawing.Size(101, 22);
            this._box4.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._box1);
            this.groupBox1.Controls.Add(this._box3);
            this.groupBox1.Controls.Add(this._box2);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(116, 97);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Токи";
            // 
            // _box1
            // 
            this._box1.Enabled = false;
            this._box1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._box1.Location = new System.Drawing.Point(6, 19);
            this._box1.Name = "_box1";
            this._box1.Size = new System.Drawing.Size(101, 22);
            this._box1.TabIndex = 9;
            // 
            // _box3
            // 
            this._box3.Enabled = false;
            this._box3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._box3.Location = new System.Drawing.Point(6, 71);
            this._box3.Name = "_box3";
            this._box3.Size = new System.Drawing.Size(101, 22);
            this._box3.TabIndex = 11;
            // 
            // _box2
            // 
            this._box2.Enabled = false;
            this._box2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._box2.Location = new System.Drawing.Point(6, 45);
            this._box2.Name = "_box2";
            this._box2.Size = new System.Drawing.Size(101, 22);
            this._box2.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(327, 32);
            this.label3.TabIndex = 43;
            this.label3.Text = "* - Дискретные входа Д1, Д2 могут отсутствовать в РЗТ-110. РЗТ-110 комплектуется " +
    "дискретными входами  по заказу";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label7);
            this.groupBox10.Controls.Add(this._indLed3);
            this.groupBox10.Controls.Add(this.label8);
            this.groupBox10.Controls.Add(this._indLed2);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Controls.Add(this._indLed1);
            this.groupBox10.Location = new System.Drawing.Point(230, 138);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(209, 70);
            this.groupBox10.TabIndex = 38;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Индикаторы";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(176, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Неисправность цепи отключения";
            // 
            // _indLed3
            // 
            this._indLed3.Location = new System.Drawing.Point(6, 49);
            this._indLed3.Name = "_indLed3";
            this._indLed3.Size = new System.Drawing.Size(13, 13);
            this._indLed3.State = BEMN.Forms.LedState.Off;
            this._indLed3.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Авария";
            // 
            // _indLed2
            // 
            this._indLed2.Location = new System.Drawing.Point(6, 34);
            this._indLed2.Name = "_indLed2";
            this._indLed2.Size = new System.Drawing.Size(13, 13);
            this._indLed2.State = BEMN.Forms.LedState.Off;
            this._indLed2.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Неисправность";
            // 
            // _indLed1
            // 
            this._indLed1.Location = new System.Drawing.Point(6, 19);
            this._indLed1.Name = "_indLed1";
            this._indLed1.Size = new System.Drawing.Size(13, 13);
            this._indLed1.State = BEMN.Forms.LedState.Off;
            this._indLed1.TabIndex = 16;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(543, 358);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 33;
            this._dateTimeControl.TimeChanged += new System.Action(this._dateTimeControl_TimeChanged);
            // 
            // RztMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(756, 571);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this._dateTimeControl);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "RztMeasuringForm";
            this.Text = "Mr600MeasuringFormV2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label124;
        private Forms.LedControl _faultSignalLed5;
        private System.Windows.Forms.Label label126;
        private Forms.LedControl _faultSignalLed3;
        private System.Windows.Forms.Label label127;
        private Forms.LedControl _faultSignalLed2;
        private System.Windows.Forms.Label label128;
        private Forms.LedControl _faultSignalLed1;
        private Forms.LedControl _faultSignalLed17;
        private Forms.LedControl _faultSignalLed16;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label5;
        private Forms.LedControl _faultSignalFlash;
        private System.Windows.Forms.Label label129;
        private Forms.LedControl _faultSignalLed15;
        private System.Windows.Forms.Label label130;
        private Forms.LedControl _faultSignalLed14;
        private System.Windows.Forms.Label label131;
        private Forms.LedControl _faultSignalLed13;
        private System.Windows.Forms.Label label132;
        private Forms.LedControl _faultSignalLed12;
        private System.Windows.Forms.Label label133;
        private Forms.LedControl _faultSignalLed11;
        private System.Windows.Forms.Label label134;
        private Forms.LedControl _faultSignalLed10;
        private System.Windows.Forms.Label label136;
        private Forms.LedControl _faultSignalLed9;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox12;
        private Forms.LedControl _I6;
        private Forms.LedControl _I5;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private Forms.LedControl _I4;
        private Forms.LedControl _I3;
        private Forms.LedControl _I2;
        private System.Windows.Forms.Label label84;
        private Forms.LedControl _I1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label111;
        private Forms.LedControl _extDefenseLed2;
        private System.Windows.Forms.Label label112;
        private Forms.LedControl _extDefenseLed1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label31;
        private Forms.LedControl _inD_led2;
        private System.Windows.Forms.Label label32;
        private Forms.LedControl _inD_led1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label15;
        private Forms.LedControl _releLed2;
        private System.Windows.Forms.Label label16;
        private Forms.LedControl _releLed1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private Forms.LedControl _UmaxLed8;
        private Forms.LedControl _UmaxLed7;
        private Forms.LedControl _UmaxLed6;
        private Forms.LedControl _UmaxLed5;
        private Forms.LedControl _UmaxLed4;
        private Forms.LedControl _UmaxLed3;
        private Forms.LedControl _UmaxLed2;
        private Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _dropJoButton;
        private System.Windows.Forms.Button _dropJaButton;
        private System.Windows.Forms.Button _dropJsButton;
        private System.Windows.Forms.Button _resetInd_But;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label61;
        private Forms.LedControl _resGroupLed;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label51;
        private Forms.LedControl _sjLed;
        private System.Windows.Forms.Label label58;
        private Forms.LedControl _faultLed;
        private Forms.LedControl _ajLed;
        private System.Windows.Forms.Button _resetJA_But;
        private System.Windows.Forms.Button _resetJS_But;
        private System.Windows.Forms.Button _resetFaultBut;
        private System.Windows.Forms.Button _constraintToggleButton;
        private System.Windows.Forms.Label label50;
        private Forms.LedControl _mainGroupLed;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _box4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _box1;
        private System.Windows.Forms.TextBox _box3;
        private System.Windows.Forms.TextBox _box2;
        private System.Windows.Forms.Label label2;
        private Forms.LedControl _faultSignalLed18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Forms.LedControl _releFault;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label7;
        private Forms.LedControl _indLed3;
        private System.Windows.Forms.Label label8;
        private Forms.LedControl _indLed2;
        private System.Windows.Forms.Label label9;
        private Forms.LedControl _indLed1;


    }
}