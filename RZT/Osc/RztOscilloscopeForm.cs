﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.RZT.Configuration.Structures;
using BEMN.RZT.Osc.HelpClasses;
using BEMN.RZT.Osc.Structures;

namespace BEMN.RZT.Osc
{
    public partial class RztOscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "Осциллограмма";
        private const string READ_OSC_FAIL = "Невозможно прочитать журнал осциллографа";
        private const string RECORDS_IN_JOURNAL = "Осциллограмм в журнале - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "Осцилограмма успешно загружена";
        private const string READ_OSC_STOPPED = "Чтение осциллограммы прекращено";
        private const string ERROR_SAVE_OSC = "Ошибка сохранения файла";
        private const string ERROR_LOAD_OSC = "Невозможно загрузить осцилограмму";
        private const string OSC_LOAD_OK_PATTERN = "Осцилограммы загружена из файла {0}";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Загрузчик журнала
        /// </summary>
        private  OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// Загрузчик страниц
        /// </summary>
        private  OscPageLoader _pageLoader;

        private OscJournalStruct _journalStruct;


        /// <summary>
        /// Данные осц
        /// </summary>
        private CountingList _countingList;
        private readonly MemoryEntity<MeasuringConfigStruct> _voltageConfiguration;
        private readonly RztDevice _device;
        private DataTable _dataTable;
        #endregion [Private fields]


        #region [Ctor's]
        public RztOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public RztOscilloscopeForm(RztDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._dataTable = this.GetJournalDataTable();
            this._oscJournalDataGrid.DataSource = this._dataTable;
            device.PortNumberChanged += this.device_PortNumberChanged;
            
            this._voltageConfiguration = device.VoltageConfiguration;
            this._voltageConfiguration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.VoltageConfigurationReadOk);
            this._voltageConfiguration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);

            //Загрузчик журнала
            this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal, device.OscOptions, this.GetJournalDataTable());
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoader.JournalIsEmpty += HandlerHelper.CreateActionHandler(this, this.JournalEmpty);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            //Загрузчик страниц
            this._pageLoader = new OscPageLoader(device);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
        }

        private void JournalEmpty()
        {
            this._statusLabel.Text = JOURNAL_IS_EMPTY;
            MessageBox.Show(JOURNAL_IS_EMPTY);
        }
        
        private void device_PortNumberChanged(object sender, PortNumberChangedEventArgs e)
        {
            this._oscJournalLoader = new OscJournalLoader(this._device.OscJournal, this._device.RefreshOscJournal, this._device.OscOptions, this.GetJournalDataTable());
            this._pageLoader = new OscPageLoader(this._device);
        } 
        #endregion [Ctor's]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(RztDevice); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(RztOscilloscopeForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]

        /// <summary>
        /// Прочитан весь журнал
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.RecordNumber == 0)
            {
                this._statusLabel.Text = "Журнал пуст";
            }
            this._oscJournalReadButton.Enabled = true;
        }

        private void VoltageConfigurationReadOk()
        {
            this._oscJournalLoader.StartReadJournal();
        }

        /// <summary>
        /// Невозможно прочитать журнал - выводим сообщение об ошибке
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
        }


        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            try
            {
//this._oscJournalDataGrid.Refresh();
            var number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
                this._dataTable.Rows.Add(this._oscJournalLoader.GetRecord);
                this._oscJournalDataGrid.DataSource = this._dataTable;

                
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            
        }

        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;

            try
            {
                this.CountingList = this._pageLoader.CountingList;
            }
            catch (Exception e)
            {
                MessageBox.Show("Данные осциллограммы повреждены или неверны", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// Определяет возможность выбрать осцилограмму для чтения
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._oscProgressBar.Value = 0;
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("RZT_журнал_осциллографа");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]

        /// <summary>
        /// Загрузка формы
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._oscJournalLoader.Table;
            this.StartRead();
        }

        /// <summary>
        /// Перечитать журнал
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                this._oscJournalReadButton.Enabled = false;
                this._oscJournalLoader.Reset();
                this._dataTable.Rows.Clear();
                this._oscilloscopeCountCb.Items.Clear();
                this.CanSelectOsc = false;
                this._voltageConfiguration.LoadStruct();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                this._oscJournalReadButton.Enabled = true;
            }
            
        }

        /// <summary>
        /// Прочитать осциллограмму
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._voltageConfiguration.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //Включаем возможность остановить чтение осцилограммы
            this._stopReadOsc.Enabled = true;
            this._oscJournalReadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
        }

        /// <summary>
        /// Сохранить осциллограмму в файл
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this._saveOscilloscopeDlg.FileName = string.Format("РЗТ110 осц-ма {0}",
                                                               this._pageLoader.CountingList.DateAndTime.Replace('/', '.').Replace(':', '.').Replace(',', ' '));
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._pageLoader.CountingList.Save(this._saveOscilloscopeDlg.FileName);
                }
                catch (Exception)
                {

                    MessageBox.Show(ERROR_SAVE_OSC);
                }
            }
        }

        /// <summary>
        /// Загрузить осциллограмму из файла
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format(OSC_LOAD_OK_PATTERN, this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = ERROR_LOAD_OSC;
            }

        }

        /// <summary>
        /// Остановить чтение осцилограммы
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            if (this.CountingList == null)
            {
                var vers = Common.VersionConverter(_device.DeviceVersion);
                var koef = 0;
                if (vers >= 1.10)
                {
                    koef = 8;
                }
                else
                {
                    koef = 40;
                }
                this.CountingList = new CountingList(new ushort[1800], new OscJournalStruct(), new MeasuringConfigStruct(),koef);
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"РЗТ-110 v{this._device.DeviceVersion} Осциллограмма");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                var resForm = new RztOscilloscopeResultForm(this.CountingList);
                resForm.Show();
            }

            
        }
        #endregion [Event Handlers]

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
            
        }
    }
}
