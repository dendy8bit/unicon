﻿using System;
using System.Collections.Generic;
using System.Data;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.RZT.Osc.Structures;

namespace BEMN.RZT.Osc.HelpClasses
{
   public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        private readonly MemoryEntity<OscOptionsStruct> _oscOptions;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;
        /// <summary>
        /// Сам журнал
        /// </summary>
        private readonly DataTable _table;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        public event Action JournalIsEmpty;
        /// <summary>
        /// Весь журнал успешно прочитан
        /// </summary>
        public event Action AllJournalReadOk;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        /// <param name="oscOptions">Объект параметров журнала</param>
        /// <param name="table">DataTable для формирования журнала</param>
        public OscJournalLoader(MemoryEntity<OscJournalStruct> oscJournal, MemoryEntity<OneWordStruct> refreshOscJournal, MemoryEntity<OscOptionsStruct> oscOptions, DataTable table)
        {
            this._oscRecords = new List<OscJournalStruct>();
            //Длинна осцилограммы
            this._oscOptions = oscOptions;

            //Если длинна прочитана - сбрасываем журнал
            this._oscOptions.AllReadOk += o => this._refreshOscJournal.SaveStruct6();
            this._oscOptions.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

            //Сброс журнала на нулевую запись
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

            this._table = table;
        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// DataTable журнала осциллографа
        /// </summary>
        public DataTable Table
        {
            get { return _table; }
        }
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return _recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return _oscRecords; }
        }
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        public OscOptionsStruct OscSizeOptions
        {
            get { return this._oscOptions.Value; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void StartReadOscJournal()
        {
            this._recordNumber = 0;
            this.Table.Clear();
            this._oscJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }
       public object[] GetRecord
       {
           get { return this._oscJournal.Value.GetRecord; }
       }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStruct.RecordIndex = this.RecordNumber;
                //this.Table.Rows.Add(this._oscJournal.Value.GetRecord);
                this.OscRecords.Add(this._oscJournal.Value);
                this._recordNumber = this.RecordNumber + 1;
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this._recordNumber == 0)
                {
                    if (this.JournalIsEmpty != null)
                    {
                        this.JournalIsEmpty.Invoke();
                    }
                }
                if (AllJournalReadOk != null)
                    this.AllJournalReadOk.Invoke();
            }
        }
        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            //   this._table.Clear();

            this._oscOptions.LoadStruct();
        }
        #endregion [Public members]

        internal void Reset()
        {
            this._table.Clear();
            this._oscRecords.Clear();
        }
    }
}
