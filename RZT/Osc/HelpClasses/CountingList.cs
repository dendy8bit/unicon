using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using System.Linq;
using System.Text;
using BEMN.RZT.Configuration.Structures;
using BEMN.RZT.Osc.Structures;

namespace BEMN.RZT.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 5;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 13;

        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 3;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        public const int VOLTAGES_COUNT = 1;

        public static readonly string[] INames = new[] { "Ia", "Ib", "Ic" };
        public static readonly string[] UNames = new[] { "Uk" };

        public static readonly string[] DNames = new[]
            {
                "D1",
                "D2",
                "�������� U��",
                "������ �����",
                "���� ����������",
                "U�>U���",
                "�������������",
                "���. ����. ���.",
                "����������",
                "������������",
                "������",
                "����������",
                "��������. ���."
            };
        #endregion [Constants]
        
        #region [Private fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ ������� 1-40
        /// </summary>
        private ushort[][] _discrets;

        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private ushort[][] _baseCurrents;

        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI = 0;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI = 0;

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [����]


        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltage;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private ushort[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU = 0;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU = 0;
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// ������������ ����������
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltage
        {
            get { return this._voltage; }
            set { this._voltage = value; }
        }  
        #endregion [����������]




        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;


        private OscJournalStruct _oscJournalStruct;

   


        #endregion [Private fields]

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }
        private string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }

        private string _stage;
        
        public string Stage
        {
            get { return this._stage; }
        }
        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[,] CurrentsKoefs { get; set; }
        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private double[,] VoltageKoefs { get; set; }
        public string FilePath { get; private set; }
        public bool IsLoad { get; private set; }

        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, MeasuringConfigStruct measure,int koef)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.FormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }


            this._discrets = this.DiscretArrayInit(this._countingArray[4]);
            this._discrets = new[]
                {
                    this._discrets[0],
                    this._discrets[1],
                    this._discrets[2],
                    this._discrets[3],
                    this._discrets[8],
                    this._discrets[9],
                    this._discrets[10],
                    this._discrets[11],
                    this._discrets[12],
                    this._discrets[13],
                    this._discrets[14],
                    this._discrets[15],
                    this._discrets[7]
                };

            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new ushort[CURRENTS_COUNT][];
            double[,] currentsKoefs = new double[CURRENTS_COUNT,2];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[ i+1];//.Select(a => (short) a).ToArray();

           currentsKoefs[i,0] = measure.Tt * koef *2 * Math.Sqrt(2) / 65536.0;
           currentsKoefs[i, 1] = -32768 * currentsKoefs[i, 0];
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i,0] * a + currentsKoefs[i,1]).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;


            this._voltage = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new ushort[VOLTAGES_COUNT][];
            double[,] voltageKoefs = new double[VOLTAGES_COUNT,2];

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[0];//.Select(a => (short)a).ToArray();



                voltageKoefs[i,0] =/* koef*2*Math.Sqrt(2)*/400.0 / 65536.0;
                voltageKoefs[i, 1] = /*-32768 * voltageKoefs[i, 0]*/0;
                this._voltage[i] = this._baseVoltages[i].Select(a => voltageKoefs[i,0] * (double)a+voltageKoefs[i,1]).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltage[i].Max());
                this._minU = Math.Min(this._minU, this._voltage[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;

        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("��� {0} {1} ������� - {2}", this._oscJournalStruct.GetDate,
                    this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);
            }
            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("RZT110,1");
                cgfFile.WriteLine("17,4A,13D");
                int index = 1;
                for (int i =0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,A,{2},{3},0,-32768,32767,1,1,P", index, INames[i], this.CurrentsKoefs[i, 0].ToString(format), this.CurrentsKoefs[i, 1].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Voltage.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

                    cgfFile.WriteLine("{0},{1},,,V,{2},{3},0,-32768,32767,1,1,P", index, UNames[i], this.VoltageKoefs[i, 0].ToString(format), this.VoltageKoefs[i, 1].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},{1},0", index, DNames[i]);
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);
                cgfFile.WriteLine(this._oscJournalStruct.FormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.FormattedDateTimeAlarm);
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}",i,i*1000);
                    foreach (ushort[] current  in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);      
                    }
                    foreach (ushort[] voltage in this._baseVoltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);   
                    }
                   
                    datFile.WriteLine();
                }
            }
        }

        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[1], buff[2]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));
            string stage = hdrStrings[3];
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[,] factors = new double[VOLTAGES_COUNT + CURRENTS_COUNT,2];
            Regex factorRegex = new Regex(@"\d+\,[IU]\w+\,\,\,[AV]\,(?<value1>[0-9\.\-]+)\,(?<value2>[0-9\.\-]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value1"].Value;
                string res1 = factorRegex.Match(cfgStrings[i]).Groups["value2"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2,0] = double.Parse(res, format);
                factors[i - 2, 1] = double.Parse(res1, format);
            }
            int counts = int.Parse(cfgStrings[21].Replace("1000,", string.Empty));

            CountingList result = new CountingList(counts) {_alarm = alarm};
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            string dataPattern =
                @"^\d+\,\d+,(?<I1>\-?\d+),(?<I2>\-?\d+),(?<I3>\-?\d+),(?<U1>\-?\d+),(?<D1>\d+),(?<D2>\d+),(?<D3>\d+),(?<D4>\d+),(?<D5>\d+),(?<D6>\d+),(?<D7>\d+),(?<D8>\d+),(?<D9>\d+),(?<D10>\d+),(?<D11>\d+),(?<D12>\d+),(?<D13>\d+)";
            Regex dataRegex = new Regex(dataPattern);
            double[][] currents = new double[CURRENTS_COUNT][];
            double[][] voltages = new double[VOLTAGES_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }
            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["I" + (j + 1)].Value) * factors[j, 0] + factors[j, 1];
                }

                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    voltages[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["U" + (j + 1)].Value) * factors[j + CURRENTS_COUNT, 0] + factors[j + CURRENTS_COUNT, 1];
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }
            }
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());                
            }

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, voltages[i].Max());
                result._minU = Math.Min(result.MinU, voltages[i].Min());
            }

            result.Currents = currents;
            result.Discrets = discrets;
            result.Voltage = voltages;
            result.FilePath = filePath;
            result._dateTime = timeString;
            result._stage = stage;
            result.IsLoad = true;
            return result;
        }
        private CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new ushort[CURRENTS_COUNT][];

            this._voltage = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new ushort[VOLTAGES_COUNT][];

            this._count = count;
        }

    }
}
