﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MBServer;
using BEMN.RZT.Configuration.Structures;
using BEMN.RZT.Osc.Structures;

namespace BEMN.RZT.Osc.HelpClasses
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoader
    {
        #region [Constants]
        /// <summary>
        /// Размер одной страницы в словах
        /// </summary>
        private const int PAGE_SIZE = 1024;

        public const int FULL_OSC_SIZE_IN_WORDS = 55295;
        public const int FULL_OSC_SIZE_IN_PAGES = 54;
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Структура для установки номаре страницы
        /// </summary>
      //  ChangeOneWord _setPageStruct;
        /// <summary>
        /// Список страниц
        /// </summary>
        private List<OscPage> _pages;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;
        /// <summary>
        /// Параматры осцилографа
        /// </summary>
     //   private OscOptionsStruct _oscOptions;
        /// <summary>
        /// Осцилограмма в виде отсчётов
        /// </summary>
        private CountingList _countingList;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Количество страниц которые нужно прочитать
        /// </summary>
        private int _pageCount;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;
        private MeasuringConfigStruct _oscOptions;
        private RztDevice _device;
        #endregion [Events]



        #region [Ctor's]
        public OscPageLoader(RztDevice device)
        {
            _device = device;
           
            this._setStartPage = device.SetStartPage;
            this._oscPage = device.OscPage;

            //Установка начальной страницы осциллограммы
            this._setStartPage.AllWriteOk += o => this._oscPage.LoadStruct();

            //Страницы осциллограммы
            this._oscPage.AllReadOk += HandlerHelper.CreateReadArrayHandler(PageReadOk);
        }
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        /// <param name="oscOptions">Параматры осцилографа</param>
        public void StartRead(OscJournalStruct journalStruct, MeasuringConfigStruct oscOptions)
        {
            this._oscOptions = oscOptions;
            this._needStop = false;

            this._journalStruct = journalStruct;
            this._startPage = journalStruct.OscStartIndex;

            this._startWordIndex = this._journalStruct.Point % OscPageLoader.PAGE_SIZE;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStruct.Len * this._journalStruct.SizeReference;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex;
            //Количество страниц которые нужно прочитать
            this._pageCount = (int)Math.Ceiling(lenOscInWords / OscPageLoader.PAGE_SIZE);


            this._endPage = this._startPage + this._pageCount;
            this._pages = new List<OscPage>();
            this.WritePageNumber((ushort)this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        }
        #endregion [Event Raise Members]


        #region [Help members]
        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();
                return;
            }
            this._pages.Add(this._oscPage.Value.Clone<OscPage>());
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < FULL_OSC_SIZE_IN_PAGES)
                {
                    this.WritePageNumber((ushort)this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort)(this._startPage - FULL_OSC_SIZE_IN_PAGES));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();

        }


       


        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {

            //Результирующий массив
            var resultMassiv = new ushort[this._resultLenInWords];
         
            var startPageValueArray = this._pages[0].Words;
            int destanationIndex = 0;


            //Копируем часть первой страницы
            if (this._journalStruct.Begin < this._journalStruct.Point)
            {
           
              Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex-1);
              destanationIndex += startPageValueArray.Length - this._startWordIndex - 1;
            }
            else
            {
                Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex);
                destanationIndex += startPageValueArray.Length - this._startWordIndex ;
            }
            

            for (int i = 1; i < this._pages.Count - 1; i++)
            {
                var pageValue = this._pages[i].Words;
                Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                destanationIndex += pageValue.Length;
            }
            var endPage = this._pages[this._pages.Count - 1];
            Array.ConstrainedCopy(endPage.Words, 0, resultMassiv, destanationIndex, this._resultLenInWords - destanationIndex);
            //   }

            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            var b = (this._journalStruct.Len - this._journalStruct.After) * this._journalStruct.SizeReference;            //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStruct.Begin < this._journalStruct.Point)              // Если BEGIN меньше POINT, то:
            {
                //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStruct.Begin + FULL_OSC_SIZE_IN_WORDS - this._journalStruct.Point;
            }
            else                                                                    //Если BEGIN больше POINT, то:
            {
                c = this._journalStruct.Begin - this._journalStruct.Point;          //c = BEGIN – POINT
            }
            var start = c - b;                                                      //START = c – b
            if (start < 0)                                                          //Если START меньше 0, то:
            {
                start += this._journalStruct.Len * this._journalStruct.SizeReference; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//
          //  resultMassiv[0] = 0;
          //  resultMassiv[1] = 0;
          //  resultMassiv[2] = 0;
          //  resultMassiv[3] = 0;
          //  resultMassiv[4] = 0;
            //Перевёрнутый массив
            var invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);
            
            var vers = Common.VersionConverter(_device.DeviceVersion);
            var koef = 0;
            if (vers >= 1.10)
            {
                koef = 8;
            }
            else
            {
                koef = 40;
            }

            this._countingList = new CountingList(invertedMass, this._journalStruct, this._oscOptions, koef);

          //  this._countingList = new CountingList(resultMassiv, this._journalStruct, this._oscOptions);
        }
        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setStartPage.Value = new OneWordStruct(pageNumber);
            this._setStartPage.SaveStruct6();
        }
        #endregion [Help members]


        #region [Properties]
        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount
        {
            get { return this._pageCount; }
        }
        /// <summary>
        /// Осцилограмма в виде отсчётов
        /// </summary>
        public CountingList CountingList
        {
            get { return _countingList; }
        }

        #endregion [Properties]
    }
}
