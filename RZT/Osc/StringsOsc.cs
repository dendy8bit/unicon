﻿using System.Collections.Generic;

namespace BEMN.RZT.Osc
{
   public static class StringsOsc
    {
       public static List<string> DiscretsNames
       {
           get
           {
               return new List<string>
                   {
                       "Д1",
                       "Д2",
                       "Контр. Uкл",
                       "Кн.\"Сброс\"",
                       "Реле ОТКЛ",
                       "Uк > Uуст",
                       "Неиспр.",
                       "Рез. группа",
                       "Отключить",
                       "Сигнал-ция",
                       "Авария",
                       "Форсирование",
                       "Недост. питание"
                   };
           }
       } 
    }
}
