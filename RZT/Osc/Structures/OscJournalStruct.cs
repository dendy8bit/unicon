﻿using System;
using System.Globalization;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;
using BEMN.RZT.AlarmJournal;

namespace BEMN.RZT.Osc.Structures
{
    public struct OscJournalStruct : IStruct, IStructInit
    {
        #region [Constants]
        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}.{3:d2}";
        private const string NUMBER_PATTERN = "{0}";

        /// <summary>
        /// Размер одной страницы 
        /// </summary>
        private const int OSCLEN = 1024;

        #endregion [Constants]

        public static int RecordIndex;

        #region [Private fields]

        private ushort _year;
        private ushort _month;
        private ushort _date;
        private ushort _hour;
        private ushort _minute;
        private ushort _second;
        private ushort _millisecond;
        private ushort _reserv;

        /// <summary>
        /// "READY" если 0 - осциллограмма готова 
        /// </summary>
        private int _ready;

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        private int _point;

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        private int _begin;

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        private int _len;

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        private int _after;

        /// <summary>
        /// "ALM" Номер (последней) сработавшей защиты 
        /// </summary>
        private ushort _numberDefence;

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        private ushort _sizeReference;

        #endregion [Private fields]


        #region [IStruct Members]

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLength);
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            ushort[] dwMas = Common.TOWORDS(array, false);
            this._reserv = dwMas[0];
            this._year = dwMas[1];
            this._month = dwMas[2];
            this._date = dwMas[3];
            this._hour = dwMas[4];
            this._minute = dwMas[5];
            this._second = dwMas[6];
            this._millisecond = dwMas[7];
            
            this._ready = Common.UshortUshortToInt(dwMas[9], dwMas[8]);
            this._point = Common.UshortUshortToInt(dwMas[11], dwMas[10]);
            this._begin = Common.UshortUshortToInt(dwMas[13], dwMas[12]);
            this._len = Common.UshortUshortToInt(dwMas[15], dwMas[14]);
            this._after = Common.UshortUshortToInt(dwMas[17], dwMas[16]);
            this._numberDefence = dwMas[18];
            this._sizeReference = dwMas[19];
        }

        public ushort[] GetValues()
        {
            throw new NotImplementedException();
        }

        #endregion [IStructInit Members]


        #region [Properties]
        /// <summary>
        /// Сработанная ступень
        /// </summary>
        public string Stage
        {
            get
            {
                return StringsAj.CodeDamage[this._numberDefence];
               // return string.Format("Числовое значение - {0}", this._numberDefence);
                    /*return Strings.AlarmJournalStage[];*/
            }
        }

        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                    {
                        this.GetNumber, //Номер
                        this.GetDate,
                        this.GetTime, //Время
                        this.Stage,
                        this._ready, //Готовность
                        this.Point, //Адрес начала
                        this.GetEnd, //Адрес конца
                        this.Begin, //Смещение
                        this.Len, //Размер
                        this.After, //№ сработавшей защиты
                        
                        this.SizeReference
                    };
            }
        }

        public bool IsEmpty
        {
            get
            {
                //пустая запись
                return this.SizeReference == 0 |
                    //осциллограмма не готова, тоже признак конца журнала
                       this._ready != 0;
            }
        }
        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return (ushort)(this.Point / OscJournalStruct.OSCLEN); }
        }
        
        /// <summary>
        /// Номер соощения
        /// </summary>
        private string GetNumber
        {
            get
            {
                var result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        TIME_PATTERN,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                return string.Format
                    (
                        DATE_PATTERN,
                        this._date,
                        this._month,
                        this._year
                    );
            }
        }

        public string FormattedDateTimeAlarm
        {
            get
            {
                DateTime a = new DateTime(this._year, this._month, this._date, this._hour, this._minute, this._second,
                    this._millisecond < 100 ? this._millisecond * 10 : this._millisecond);
                DateTime result = a.AddMilliseconds(this.Len - this.After);
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                    result.Month,
                    result.Day,
                    result.Year,
                    result.Hour,
                    result.Minute,
                    result.Second,
                    result.Millisecond);
            }
        }

        public string FormattedDateTime
        {
            get
            {
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     this._month,
                                     this._date,
                                     this._year,
                                     this._hour,
                                     this._minute,
                                     this._second,
                    this._millisecond < 100 ? this._millisecond * 10 : this._millisecond);
            }
        }

        private static int OscSize = 0x77FF4;

        /// <summary>
        /// Адрес конца
        /// </summary>
        private string GetEnd
        {
            get
            {
                int num = OscSize;
                int num2 = this.Point + (this.Len * this.SizeReference);
                return num2 > num ? string.Format("{0} [{1}]", num2, num2 - num) : num2.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        public int Len
        {
            get { return _len; }
        }

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        public int After
        {
            get { return _after; }
        }

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        public int Begin
        {
            get { return _begin; }
        }

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        public int Point
        {
            get { return _point; }
        }

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeReference
        {
            get { return _sizeReference; }
        }

        #endregion [Private Properties]
    }
}
