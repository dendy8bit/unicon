﻿using System;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.RZT.Osc.Structures
{
    /// <summary>
    /// Параматры осцилографа
    /// </summary>
    public struct OscOptionsStruct : IStruct, IStructInit
    {
        private ushort _options;
       /// <summary>
       /// Код режима работы осциллографа
       /// </summary>
        public ushort Code
        {
            get { return Common.GetBits(this._options, 0, 1, 2); }
        }

        public ushort PreRecording
        {
            get { return Common.GetBits(this._options, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        #region [IStruct Members]

        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(GetType(), len);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, GetType(), slotLength);
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            this._options = Common.TOWORD(array[1], array[0]);
        }

        public ushort[] GetValues()
        {
            throw new NotImplementedException();
        }

        #endregion [IStructInit Members]
    }
}
