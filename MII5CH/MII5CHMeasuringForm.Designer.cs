﻿namespace BEMN.MII5CH
{
    partial class MII5CHMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.channel5TextBox = new System.Windows.Forms.TextBox();
            this.channel4TextBox = new System.Windows.Forms.TextBox();
            this.channel3TextBox = new System.Windows.Forms.TextBox();
            this.channel2TextBox = new System.Windows.Forms.TextBox();
            this.channel1TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._resist2 = new System.Windows.Forms.TextBox();
            this._resist1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._om2 = new System.Windows.Forms.Label();
            this._om1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._minus = new System.Windows.Forms.Label();
            this._plus = new System.Windows.Forms.Label();
            this._status = new System.Windows.Forms.Label();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._c1 = new System.Windows.Forms.TextBox();
            this._mkf2 = new System.Windows.Forms.Label();
            this._mkf1 = new System.Windows.Forms.Label();
            this._inOutGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.channel5TextBox);
            this.groupBox1.Controls.Add(this.channel4TextBox);
            this.groupBox1.Controls.Add(this.channel3TextBox);
            this.groupBox1.Controls.Add(this.channel2TextBox);
            this.groupBox1.Controls.Add(this.channel1TextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(177, 136);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Значения аналоговых каналов";
            // 
            // channel5TextBox
            // 
            this.channel5TextBox.Location = new System.Drawing.Point(82, 110);
            this.channel5TextBox.Name = "channel5TextBox";
            this.channel5TextBox.ReadOnly = true;
            this.channel5TextBox.Size = new System.Drawing.Size(56, 20);
            this.channel5TextBox.TabIndex = 5;
            this.channel5TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // channel4TextBox
            // 
            this.channel4TextBox.Location = new System.Drawing.Point(82, 90);
            this.channel4TextBox.Name = "channel4TextBox";
            this.channel4TextBox.ReadOnly = true;
            this.channel4TextBox.Size = new System.Drawing.Size(56, 20);
            this.channel4TextBox.TabIndex = 5;
            this.channel4TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // channel3TextBox
            // 
            this.channel3TextBox.Location = new System.Drawing.Point(82, 70);
            this.channel3TextBox.Name = "channel3TextBox";
            this.channel3TextBox.ReadOnly = true;
            this.channel3TextBox.Size = new System.Drawing.Size(56, 20);
            this.channel3TextBox.TabIndex = 5;
            this.channel3TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // channel2TextBox
            // 
            this.channel2TextBox.Location = new System.Drawing.Point(82, 50);
            this.channel2TextBox.Name = "channel2TextBox";
            this.channel2TextBox.ReadOnly = true;
            this.channel2TextBox.Size = new System.Drawing.Size(56, 20);
            this.channel2TextBox.TabIndex = 5;
            this.channel2TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // channel1TextBox
            // 
            this.channel1TextBox.Location = new System.Drawing.Point(82, 30);
            this.channel1TextBox.Name = "channel1TextBox";
            this.channel1TextBox.ReadOnly = true;
            this.channel1TextBox.Size = new System.Drawing.Size(56, 20);
            this.channel1TextBox.TabIndex = 5;
            this.channel1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Коэф.пульс.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Канал 4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Канал 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Канал 2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(144, 113);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(144, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "мВ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(144, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "В";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(144, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "В";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(144, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "В";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Канал 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._resist2);
            this.groupBox2.Controls.Add(this._resist1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this._om2);
            this.groupBox2.Controls.Add(this._om1);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(195, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(135, 65);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Сопротивление плеч";
            // 
            // _resist2
            // 
            this._resist2.Location = new System.Drawing.Point(35, 39);
            this._resist2.Name = "_resist2";
            this._resist2.ReadOnly = true;
            this._resist2.Size = new System.Drawing.Size(65, 20);
            this._resist2.TabIndex = 5;
            this._resist2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _resist1
            // 
            this._resist1.Location = new System.Drawing.Point(35, 19);
            this._resist1.Name = "_resist1";
            this._resist1.ReadOnly = true;
            this._resist1.Size = new System.Drawing.Size(65, 20);
            this._resist1.TabIndex = 5;
            this._resist1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "\"-\"";
            // 
            // _om2
            // 
            this._om2.AutoSize = true;
            this._om2.Location = new System.Drawing.Point(106, 42);
            this._om2.Name = "_om2";
            this._om2.Size = new System.Drawing.Size(0, 13);
            this._om2.TabIndex = 0;
            // 
            // _om1
            // 
            this._om1.AutoSize = true;
            this._om1.Location = new System.Drawing.Point(106, 22);
            this._om1.Name = "_om1";
            this._om1.Size = new System.Drawing.Size(0, 13);
            this._om1.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "\"+\"";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._minus);
            this.groupBox3.Controls.Add(this._plus);
            this.groupBox3.Controls.Add(this._status);
            this.groupBox3.Location = new System.Drawing.Point(195, 83);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(241, 65);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Статус";
            // 
            // _minus
            // 
            this._minus.AutoSize = true;
            this._minus.Location = new System.Drawing.Point(6, 38);
            this._minus.Name = "_minus";
            this._minus.Size = new System.Drawing.Size(17, 13);
            this._minus.TabIndex = 0;
            this._minus.Text = "\'--\'";
            // 
            // _plus
            // 
            this._plus.AutoSize = true;
            this._plus.Location = new System.Drawing.Point(6, 20);
            this._plus.Name = "_plus";
            this._plus.Size = new System.Drawing.Size(17, 13);
            this._plus.TabIndex = 0;
            this._plus.Text = "\'+\'";
            // 
            // _status
            // 
            this._status.AutoSize = true;
            this._status.Location = new System.Drawing.Point(24, 28);
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(147, 13);
            this._status.TabIndex = 0;
            this._status.Text = "Подключено плечо клеммы";
            // 
            // _contextMenu
            // 
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._c1);
            this.groupBox4.Controls.Add(this._mkf2);
            this.groupBox4.Controls.Add(this._mkf1);
            this.groupBox4.Location = new System.Drawing.Point(336, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(100, 65);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ёмкость сети";
            // 
            // _c1
            // 
            this._c1.Location = new System.Drawing.Point(6, 19);
            this._c1.Name = "_c1";
            this._c1.ReadOnly = true;
            this._c1.Size = new System.Drawing.Size(65, 20);
            this._c1.TabIndex = 5;
            this._c1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _mkf2
            // 
            this._mkf2.AutoSize = true;
            this._mkf2.Location = new System.Drawing.Point(106, 42);
            this._mkf2.Name = "_mkf2";
            this._mkf2.Size = new System.Drawing.Size(0, 13);
            this._mkf2.TabIndex = 0;
            // 
            // _mkf1
            // 
            this._mkf1.AutoSize = true;
            this._mkf1.Location = new System.Drawing.Point(106, 22);
            this._mkf1.Name = "_mkf1";
            this._mkf1.Size = new System.Drawing.Size(0, 13);
            this._mkf1.TabIndex = 0;
            // 
            // _inOutGroupBox
            // 
            this._inOutGroupBox.Location = new System.Drawing.Point(12, 154);
            this._inOutGroupBox.Name = "_inOutGroupBox";
            this._inOutGroupBox.Size = new System.Drawing.Size(177, 190);
            this._inOutGroupBox.TabIndex = 1;
            this._inOutGroupBox.TabStop = false;
            this._inOutGroupBox.Text = "Ввод-вывод";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioButton3);
            this.groupBox11.Controls.Add(this.radioButton7);
            this.groupBox11.Controls.Add(this.radioButton2);
            this.groupBox11.Controls.Add(this.radioButton6);
            this.groupBox11.Controls.Add(this.radioButton1);
            this.groupBox11.Controls.Add(this.radioButton4);
            this.groupBox11.Controls.Add(this.radioButton5);
            this.groupBox11.Location = new System.Drawing.Point(195, 154);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(241, 190);
            this.groupBox11.TabIndex = 23;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Режимы работы";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Enabled = false;
            this.radioButton3.Location = new System.Drawing.Point(6, 65);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(136, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "2";
            this.radioButton3.Text = "Подключено плечо \"-\"";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Enabled = false;
            this.radioButton7.Location = new System.Drawing.Point(6, 157);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(86, 17);
            this.radioButton7.TabIndex = 7;
            this.radioButton7.TabStop = true;
            this.radioButton7.Tag = "65535";
            this.radioButton7.Text = "Калибровка";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Enabled = false;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(139, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "1";
            this.radioButton2.Text = "Подключено плечо \"+\"";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Enabled = false;
            this.radioButton6.Location = new System.Drawing.Point(6, 134);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(140, 17);
            this.radioButton6.TabIndex = 6;
            this.radioButton6.TabStop = true;
            this.radioButton6.Tag = "5";
            this.radioButton6.Text = "Без подключения плеч";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Enabled = false;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(77, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "0";
            this.radioButton1.Text = "Ожидание";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Enabled = false;
            this.radioButton4.Location = new System.Drawing.Point(6, 88);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(203, 17);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "3";
            this.radioButton4.Text = "Включение двух плеч попеременно";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Enabled = false;
            this.radioButton5.Location = new System.Drawing.Point(6, 111);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(209, 17);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "4";
            this.radioButton5.Text = "Включение двух плеч одновременно";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // MII5CHMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 349);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this._inOutGroupBox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MII5CHMeasuringForm";
            this.Text = "MeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Channel5MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.Channel5MeasuringForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox channel5TextBox;
        private System.Windows.Forms.TextBox channel4TextBox;
        private System.Windows.Forms.TextBox channel3TextBox;
        private System.Windows.Forms.TextBox channel2TextBox;
        private System.Windows.Forms.TextBox channel1TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _resist2;
        private System.Windows.Forms.TextBox _resist1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _status;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.Label _minus;
        private System.Windows.Forms.Label _plus;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label _om2;
        private System.Windows.Forms.Label _om1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox _c1;
        private System.Windows.Forms.Label _mkf2;
        private System.Windows.Forms.Label _mkf1;
        private System.Windows.Forms.GroupBox _inOutGroupBox;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
    }
}