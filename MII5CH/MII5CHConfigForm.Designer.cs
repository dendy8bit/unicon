﻿namespace BEMN.MII5CH
{
    partial class MII5CHConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MII5CHConfigForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readRs485Button = new System.Windows.Forms.Button();
            this._versionPage = new System.Windows.Forms.TabPage();
            this._readVersionBtn = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ldrRevision = new System.Windows.Forms.MaskedTextBox();
            this._ldrCodProc = new System.Windows.Forms.MaskedTextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._ldrFuse = new System.Windows.Forms.MaskedTextBox();
            this._ldrName = new System.Windows.Forms.MaskedTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._devRevision = new System.Windows.Forms.MaskedTextBox();
            this._devMod = new System.Windows.Forms.MaskedTextBox();
            this._devPodtip = new System.Windows.Forms.MaskedTextBox();
            this._devName = new System.Windows.Forms.MaskedTextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._appRevision = new System.Windows.Forms.MaskedTextBox();
            this._appMod = new System.Windows.Forms.MaskedTextBox();
            this._appPodtip = new System.Windows.Forms.MaskedTextBox();
            this._appName = new System.Windows.Forms.MaskedTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._analogPage = new System.Windows.Forms.TabPage();
            this._passInsertBox = new System.Windows.Forms.GroupBox();
            this._validatePassButton = new System.Windows.Forms.Button();
            this._passTB = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._channelCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._readChannelConfigBtn = new System.Windows.Forms.Button();
            this._writeChannelConfigBtn = new System.Windows.Forms.Button();
            this._descriptionGroup = new System.Windows.Forms.GroupBox();
            this._descriptionBox = new BEMN.Forms.RichTextBox.AdvRichTextBox();
            this._continueBtn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._Uin = new System.Windows.Forms.MaskedTextBox();
            this._rangeOfScale = new System.Windows.Forms.MaskedTextBox();
            this._koeffpA = new System.Windows.Forms.MaskedTextBox();
            this._koeffA = new System.Windows.Forms.MaskedTextBox();
            this._koeffB = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._statusStep = new System.Windows.Forms.Label();
            this._RS485Page = new System.Windows.Forms.TabPage();
            this._RS485WriteBtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._RS485dataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._RS485ToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._RS485ToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._RS485AddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._RS485doubleSpeedCombo = new System.Windows.Forms.ComboBox();
            this._RS485paritetCHETCombo = new System.Windows.Forms.ComboBox();
            this._RS485paritetYNCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._RS485stopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._RS485speedsCombo = new System.Windows.Forms.ComboBox();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this._USBtabPage = new System.Windows.Forms.TabPage();
            this._usbReadBtn = new System.Windows.Forms.Button();
            this._usbWriteBtn = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._USBdataBitsCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._USBToSendAfterTB = new System.Windows.Forms.MaskedTextBox();
            this._USBToSendBeforeTB = new System.Windows.Forms.MaskedTextBox();
            this._USBAddressTB = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this._USBdoubleSpeedCombo = new System.Windows.Forms.ComboBox();
            this._USBparitetCHETCombo = new System.Windows.Forms.ComboBox();
            this._USBparitetYNCombo = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._USBstopBitsCombo = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this._USBspeedsCombo = new System.Windows.Forms.ComboBox();
            this.setpointPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._timeout = new System.Windows.Forms.MaskedTextBox();
            this._resistOn = new System.Windows.Forms.MaskedTextBox();
            this._readSetpointBtn = new System.Windows.Forms.Button();
            this._writeSetpointBtn = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1.SuspendLayout();
            this._versionPage.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._analogPage.SuspendLayout();
            this._passInsertBox.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._descriptionGroup.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._RS485Page.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._configurationTabControl.SuspendLayout();
            this._USBtabPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.setpointPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 430);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(434, 22);
            this.statusStrip1.TabIndex = 17;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _progressBar
            // 
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(80, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readRs485Button
            // 
            this._readRs485Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readRs485Button.Location = new System.Drawing.Point(7, 368);
            this._readRs485Button.Name = "_readRs485Button";
            this._readRs485Button.Size = new System.Drawing.Size(75, 23);
            this._readRs485Button.TabIndex = 15;
            this._readRs485Button.Text = "Прочитать";
            this._readRs485Button.UseVisualStyleBackColor = true;
            this._readRs485Button.Click += new System.EventHandler(this._readRs485Button_Click);
            // 
            // _versionPage
            // 
            this._versionPage.Controls.Add(this._readVersionBtn);
            this._versionPage.Controls.Add(this.groupBox9);
            this._versionPage.Controls.Add(this.groupBox7);
            this._versionPage.Controls.Add(this.groupBox1);
            this._versionPage.Location = new System.Drawing.Point(4, 22);
            this._versionPage.Name = "_versionPage";
            this._versionPage.Padding = new System.Windows.Forms.Padding(3);
            this._versionPage.Size = new System.Drawing.Size(423, 398);
            this._versionPage.TabIndex = 5;
            this._versionPage.Text = "Версия";
            this._versionPage.UseVisualStyleBackColor = true;
            // 
            // _readVersionBtn
            // 
            this._readVersionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readVersionBtn.Location = new System.Drawing.Point(7, 368);
            this._readVersionBtn.Name = "_readVersionBtn";
            this._readVersionBtn.Size = new System.Drawing.Size(75, 23);
            this._readVersionBtn.TabIndex = 17;
            this._readVersionBtn.Text = "Прочитать";
            this._readVersionBtn.UseVisualStyleBackColor = true;
            this._readVersionBtn.Click += new System.EventHandler(this._readVersionBtn_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ldrRevision);
            this.groupBox9.Controls.Add(this._ldrCodProc);
            this.groupBox9.Controls.Add(this.groupBox10);
            this.groupBox9.Controls.Add(this._ldrFuse);
            this.groupBox9.Controls.Add(this._ldrName);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.label39);
            this.groupBox9.Location = new System.Drawing.Point(7, 245);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(192, 122);
            this.groupBox9.TabIndex = 16;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Версия загрузчика";
            // 
            // _ldrRevision
            // 
            this._ldrRevision.Location = new System.Drawing.Point(126, 88);
            this._ldrRevision.Name = "_ldrRevision";
            this._ldrRevision.ReadOnly = true;
            this._ldrRevision.Size = new System.Drawing.Size(60, 20);
            this._ldrRevision.TabIndex = 15;
            // 
            // _ldrCodProc
            // 
            this._ldrCodProc.Location = new System.Drawing.Point(126, 42);
            this._ldrCodProc.Name = "_ldrCodProc";
            this._ldrCodProc.ReadOnly = true;
            this._ldrCodProc.Size = new System.Drawing.Size(60, 20);
            this._ldrCodProc.TabIndex = 15;
            // 
            // groupBox10
            // 
            this.groupBox10.Location = new System.Drawing.Point(115, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(5, 110);
            this.groupBox10.TabIndex = 7;
            this.groupBox10.TabStop = false;
            // 
            // _ldrFuse
            // 
            this._ldrFuse.Location = new System.Drawing.Point(126, 65);
            this._ldrFuse.Name = "_ldrFuse";
            this._ldrFuse.ReadOnly = true;
            this._ldrFuse.Size = new System.Drawing.Size(60, 20);
            this._ldrFuse.TabIndex = 16;
            // 
            // _ldrName
            // 
            this._ldrName.Location = new System.Drawing.Point(126, 19);
            this._ldrName.Name = "_ldrName";
            this._ldrName.ReadOnly = true;
            this._ldrName.Size = new System.Drawing.Size(60, 20);
            this._ldrName.TabIndex = 16;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 21);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Загрузчик : ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 90);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(103, 13);
            this.label37.TabIndex = 3;
            this.label37.Text = "Версия прошивки :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 67);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(91, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Текущие фьюзы";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 44);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(89, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Код процессора";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._devRevision);
            this.groupBox7.Controls.Add(this._devMod);
            this.groupBox7.Controls.Add(this._devPodtip);
            this.groupBox7.Controls.Add(this._devName);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Location = new System.Drawing.Point(7, 124);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(192, 115);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Версия устройства";
            // 
            // _devRevision
            // 
            this._devRevision.Location = new System.Drawing.Point(126, 87);
            this._devRevision.Name = "_devRevision";
            this._devRevision.ReadOnly = true;
            this._devRevision.Size = new System.Drawing.Size(60, 20);
            this._devRevision.TabIndex = 13;
            // 
            // _devMod
            // 
            this._devMod.Location = new System.Drawing.Point(126, 64);
            this._devMod.Name = "_devMod";
            this._devMod.ReadOnly = true;
            this._devMod.Size = new System.Drawing.Size(60, 20);
            this._devMod.TabIndex = 14;
            // 
            // _devPodtip
            // 
            this._devPodtip.Location = new System.Drawing.Point(126, 41);
            this._devPodtip.Name = "_devPodtip";
            this._devPodtip.ReadOnly = true;
            this._devPodtip.Size = new System.Drawing.Size(60, 20);
            this._devPodtip.TabIndex = 15;
            // 
            // _devName
            // 
            this._devName.Location = new System.Drawing.Point(126, 18);
            this._devName.Name = "_devName";
            this._devName.ReadOnly = true;
            this._devName.Size = new System.Drawing.Size(60, 20);
            this._devName.TabIndex = 16;
            // 
            // groupBox8
            // 
            this.groupBox8.Location = new System.Drawing.Point(115, 10);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(5, 96);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Устройство : ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 90);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 13);
            this.label33.TabIndex = 3;
            this.label33.Text = "Ревизия :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 67);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(78, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Модификация";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 44);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Подтип";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._appRevision);
            this.groupBox1.Controls.Add(this._appMod);
            this.groupBox1.Controls.Add(this._appPodtip);
            this.groupBox1.Controls.Add(this._appName);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 115);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Версия программы";
            // 
            // _appRevision
            // 
            this._appRevision.Location = new System.Drawing.Point(126, 87);
            this._appRevision.Name = "_appRevision";
            this._appRevision.ReadOnly = true;
            this._appRevision.Size = new System.Drawing.Size(60, 20);
            this._appRevision.TabIndex = 12;
            // 
            // _appMod
            // 
            this._appMod.Location = new System.Drawing.Point(126, 64);
            this._appMod.Name = "_appMod";
            this._appMod.ReadOnly = true;
            this._appMod.Size = new System.Drawing.Size(60, 20);
            this._appMod.TabIndex = 12;
            // 
            // _appPodtip
            // 
            this._appPodtip.Location = new System.Drawing.Point(126, 41);
            this._appPodtip.Name = "_appPodtip";
            this._appPodtip.ReadOnly = true;
            this._appPodtip.Size = new System.Drawing.Size(60, 20);
            this._appPodtip.TabIndex = 12;
            // 
            // _appName
            // 
            this._appName.Location = new System.Drawing.Point(126, 18);
            this._appName.Name = "_appName";
            this._appName.ReadOnly = true;
            this._appName.Size = new System.Drawing.Size(60, 20);
            this._appName.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(115, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(5, 96);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Устройство : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Версия прошивки :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Модификация";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Подтип";
            // 
            // _analogPage
            // 
            this._analogPage.Controls.Add(this._passInsertBox);
            this._analogPage.Controls.Add(this.groupBox12);
            this._analogPage.Controls.Add(this._statusStep);
            this._analogPage.Location = new System.Drawing.Point(4, 22);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Size = new System.Drawing.Size(423, 398);
            this._analogPage.TabIndex = 3;
            this._analogPage.Text = "Калибровка каналов";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // _passInsertBox
            // 
            this._passInsertBox.Controls.Add(this._validatePassButton);
            this._passInsertBox.Controls.Add(this._passTB);
            this._passInsertBox.Controls.Add(this.label31);
            this._passInsertBox.Location = new System.Drawing.Point(7, 295);
            this._passInsertBox.Name = "_passInsertBox";
            this._passInsertBox.Size = new System.Drawing.Size(229, 61);
            this._passInsertBox.TabIndex = 46;
            this._passInsertBox.TabStop = false;
            this._passInsertBox.Text = "Изменение конфигурации";
            // 
            // _validatePassButton
            // 
            this._validatePassButton.Location = new System.Drawing.Point(131, 24);
            this._validatePassButton.Name = "_validatePassButton";
            this._validatePassButton.Size = new System.Drawing.Size(90, 23);
            this._validatePassButton.TabIndex = 45;
            this._validatePassButton.Text = "Принять";
            this._validatePassButton.UseVisualStyleBackColor = true;
            this._validatePassButton.Click += new System.EventHandler(this._validatePassButton_Click);
            // 
            // _passTB
            // 
            this._passTB.Location = new System.Drawing.Point(57, 26);
            this._passTB.Name = "_passTB";
            this._passTB.PasswordChar = '*';
            this._passTB.Size = new System.Drawing.Size(68, 20);
            this._passTB.TabIndex = 45;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 29);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 13);
            this.label31.TabIndex = 45;
            this.label31.Text = "Пароль";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.groupBox4);
            this.groupBox12.Controls.Add(this._descriptionGroup);
            this.groupBox12.Controls.Add(this._continueBtn);
            this.groupBox12.Controls.Add(this.groupBox6);
            this.groupBox12.Enabled = false;
            this.groupBox12.Location = new System.Drawing.Point(7, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(413, 286);
            this.groupBox12.TabIndex = 20;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Калибровка";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._channelCombo);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this._readChannelConfigBtn);
            this.groupBox4.Controls.Add(this._writeChannelConfigBtn);
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(175, 106);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Выбор аналогового канала";
            // 
            // _channelCombo
            // 
            this._channelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._channelCombo.FormattingEnabled = true;
            this._channelCombo.Items.AddRange(new object[] {
            "Канал 1",
            "Канал 2",
            "Канал 3",
            "Канал 4"});
            this._channelCombo.Location = new System.Drawing.Point(93, 19);
            this._channelCombo.Name = "_channelCombo";
            this._channelCombo.Size = new System.Drawing.Size(76, 21);
            this._channelCombo.TabIndex = 1;
            this._channelCombo.SelectedIndexChanged += new System.EventHandler(this._channelCombo_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Канал";
            // 
            // _readChannelConfigBtn
            // 
            this._readChannelConfigBtn.Enabled = false;
            this._readChannelConfigBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readChannelConfigBtn.Location = new System.Drawing.Point(9, 46);
            this._readChannelConfigBtn.Name = "_readChannelConfigBtn";
            this._readChannelConfigBtn.Size = new System.Drawing.Size(160, 23);
            this._readChannelConfigBtn.TabIndex = 2;
            this._readChannelConfigBtn.Text = "Прочитать конфигурацию";
            this._readChannelConfigBtn.UseVisualStyleBackColor = true;
            this._readChannelConfigBtn.Click += new System.EventHandler(this._readChannelConfigBtnClick);
            // 
            // _writeChannelConfigBtn
            // 
            this._writeChannelConfigBtn.Enabled = false;
            this._writeChannelConfigBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeChannelConfigBtn.Location = new System.Drawing.Point(9, 75);
            this._writeChannelConfigBtn.Name = "_writeChannelConfigBtn";
            this._writeChannelConfigBtn.Size = new System.Drawing.Size(160, 23);
            this._writeChannelConfigBtn.TabIndex = 3;
            this._writeChannelConfigBtn.Text = "Записать конфигурацию";
            this._writeChannelConfigBtn.UseVisualStyleBackColor = true;
            this._writeChannelConfigBtn.Click += new System.EventHandler(this._analogConfigWriteBtn_Click);
            // 
            // _descriptionGroup
            // 
            this._descriptionGroup.Controls.Add(this._descriptionBox);
            this._descriptionGroup.Location = new System.Drawing.Point(184, 19);
            this._descriptionGroup.Name = "_descriptionGroup";
            this._descriptionGroup.Size = new System.Drawing.Size(224, 207);
            this._descriptionGroup.TabIndex = 21;
            this._descriptionGroup.TabStop = false;
            // 
            // _descriptionBox
            // 
            this._descriptionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._descriptionBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionBox.Location = new System.Drawing.Point(3, 16);
            this._descriptionBox.Name = "_descriptionBox";
            this._descriptionBox.ReadOnly = true;
            this._descriptionBox.SelectionAlignment = BEMN.Forms.RichTextBox.TextAlign.Justify;
            this._descriptionBox.Size = new System.Drawing.Size(218, 188);
            this._descriptionBox.TabIndex = 10;
            this._descriptionBox.Text = resources.GetString("_descriptionBox.Text");
            // 
            // _continueBtn
            // 
            this._continueBtn.Enabled = false;
            this._continueBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._continueBtn.Location = new System.Drawing.Point(271, 232);
            this._continueBtn.Name = "_continueBtn";
            this._continueBtn.Size = new System.Drawing.Size(137, 23);
            this._continueBtn.TabIndex = 20;
            this._continueBtn.Text = "Продолжить";
            this._continueBtn.UseVisualStyleBackColor = true;
            this._continueBtn.Click += new System.EventHandler(this._continueBtn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this._Uin);
            this.groupBox6.Controls.Add(this._rangeOfScale);
            this.groupBox6.Controls.Add(this._koeffpA);
            this.groupBox6.Controls.Add(this._koeffA);
            this.groupBox6.Controls.Add(this._koeffB);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Location = new System.Drawing.Point(6, 131);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(175, 149);
            this.groupBox6.TabIndex = 19;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Конфигурация канала";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Сигнал на входе";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Предел шкалы";
            // 
            // _Uin
            // 
            this._Uin.Location = new System.Drawing.Point(117, 124);
            this._Uin.Name = "_Uin";
            this._Uin.Size = new System.Drawing.Size(52, 20);
            this._Uin.TabIndex = 8;
            this._Uin.Tag = "65535";
            this._Uin.Text = "0";
            // 
            // _rangeOfScale
            // 
            this._rangeOfScale.Location = new System.Drawing.Point(117, 100);
            this._rangeOfScale.Name = "_rangeOfScale";
            this._rangeOfScale.Size = new System.Drawing.Size(52, 20);
            this._rangeOfScale.TabIndex = 7;
            this._rangeOfScale.Tag = "65535";
            this._rangeOfScale.Text = "0";
            this._rangeOfScale.TextChanged += new System.EventHandler(this._rangeOfScale_TextChanged);
            // 
            // _koeffpA
            // 
            this._koeffpA.Location = new System.Drawing.Point(117, 74);
            this._koeffpA.Name = "_koeffpA";
            this._koeffpA.Size = new System.Drawing.Size(52, 20);
            this._koeffpA.TabIndex = 6;
            this._koeffpA.Tag = "65535";
            this._koeffpA.Text = "0";
            // 
            // _koeffA
            // 
            this._koeffA.Location = new System.Drawing.Point(117, 48);
            this._koeffA.Name = "_koeffA";
            this._koeffA.Size = new System.Drawing.Size(52, 20);
            this._koeffA.TabIndex = 5;
            this._koeffA.Tag = "65535";
            this._koeffA.Text = "0";
            // 
            // _koeffB
            // 
            this._koeffB.Location = new System.Drawing.Point(117, 22);
            this._koeffB.Name = "_koeffB";
            this._koeffB.Size = new System.Drawing.Size(52, 20);
            this._koeffB.TabIndex = 4;
            this._koeffB.Tag = "65535";
            this._koeffB.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Степень pA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 51);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Коэффициент А";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Коэффициент B";
            // 
            // _statusStep
            // 
            this._statusStep.AutoSize = true;
            this._statusStep.Location = new System.Drawing.Point(13, 372);
            this._statusStep.Name = "_statusStep";
            this._statusStep.Size = new System.Drawing.Size(0, 13);
            this._statusStep.TabIndex = 16;
            // 
            // _RS485Page
            // 
            this._RS485Page.Controls.Add(this._RS485WriteBtn);
            this._RS485Page.Controls.Add(this.groupBox3);
            this._RS485Page.Controls.Add(this._readRs485Button);
            this._RS485Page.Location = new System.Drawing.Point(4, 22);
            this._RS485Page.Name = "_RS485Page";
            this._RS485Page.Padding = new System.Windows.Forms.Padding(3);
            this._RS485Page.Size = new System.Drawing.Size(423, 398);
            this._RS485Page.TabIndex = 0;
            this._RS485Page.Text = "RS-485";
            this._RS485Page.UseVisualStyleBackColor = true;
            // 
            // _RS485WriteBtn
            // 
            this._RS485WriteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._RS485WriteBtn.Location = new System.Drawing.Point(88, 368);
            this._RS485WriteBtn.Name = "_RS485WriteBtn";
            this._RS485WriteBtn.Size = new System.Drawing.Size(75, 23);
            this._RS485WriteBtn.TabIndex = 10;
            this._RS485WriteBtn.Text = "Записать";
            this._RS485WriteBtn.UseVisualStyleBackColor = true;
            this._RS485WriteBtn.Click += new System.EventHandler(this._RS485WriteBtnClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._RS485dataBitsCombo);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._RS485ToSendAfterTB);
            this.groupBox3.Controls.Add(this._RS485ToSendBeforeTB);
            this.groupBox3.Controls.Add(this._RS485AddressTB);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._RS485doubleSpeedCombo);
            this.groupBox3.Controls.Add(this._RS485paritetCHETCombo);
            this.groupBox3.Controls.Add(this._RS485paritetYNCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._RS485stopBitsCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._RS485speedsCombo);
            this.groupBox3.Location = new System.Drawing.Point(7, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(287, 210);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Конфигурация RS-485";
            // 
            // _RS485dataBitsCombo
            // 
            this._RS485dataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485dataBitsCombo.FormattingEnabled = true;
            this._RS485dataBitsCombo.Location = new System.Drawing.Point(181, 42);
            this._RS485dataBitsCombo.Name = "_RS485dataBitsCombo";
            this._RS485dataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485dataBitsCombo.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Количество бит данных";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(163, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Таймаут после выдачи данных";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(145, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Таймаут до выдачи данных";
            // 
            // _RS485ToSendAfterTB
            // 
            this._RS485ToSendAfterTB.Location = new System.Drawing.Point(181, 180);
            this._RS485ToSendAfterTB.Name = "_RS485ToSendAfterTB";
            this._RS485ToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._RS485ToSendAfterTB.TabIndex = 23;
            this._RS485ToSendAfterTB.Tag = "255";
            this._RS485ToSendAfterTB.Text = "0";
            // 
            // _RS485ToSendBeforeTB
            // 
            this._RS485ToSendBeforeTB.Location = new System.Drawing.Point(181, 161);
            this._RS485ToSendBeforeTB.Name = "_RS485ToSendBeforeTB";
            this._RS485ToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._RS485ToSendBeforeTB.TabIndex = 22;
            this._RS485ToSendBeforeTB.Tag = "255";
            this._RS485ToSendBeforeTB.Text = "0";
            // 
            // _RS485AddressTB
            // 
            this._RS485AddressTB.Location = new System.Drawing.Point(181, 142);
            this._RS485AddressTB.Name = "_RS485AddressTB";
            this._RS485AddressTB.Size = new System.Drawing.Size(97, 20);
            this._RS485AddressTB.TabIndex = 19;
            this._RS485AddressTB.Tag = "255";
            this._RS485AddressTB.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Адрес устройства";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Удвоение скорости";
            // 
            // _RS485doubleSpeedCombo
            // 
            this._RS485doubleSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485doubleSpeedCombo.FormattingEnabled = true;
            this._RS485doubleSpeedCombo.Location = new System.Drawing.Point(181, 122);
            this._RS485doubleSpeedCombo.Name = "_RS485doubleSpeedCombo";
            this._RS485doubleSpeedCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485doubleSpeedCombo.TabIndex = 16;
            // 
            // _RS485paritetCHETCombo
            // 
            this._RS485paritetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485paritetCHETCombo.Enabled = false;
            this._RS485paritetCHETCombo.FormattingEnabled = true;
            this._RS485paritetCHETCombo.Location = new System.Drawing.Point(181, 102);
            this._RS485paritetCHETCombo.Name = "_RS485paritetCHETCombo";
            this._RS485paritetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485paritetCHETCombo.TabIndex = 15;
            // 
            // _RS485paritetYNCombo
            // 
            this._RS485paritetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485paritetYNCombo.FormattingEnabled = true;
            this._RS485paritetYNCombo.Location = new System.Drawing.Point(181, 82);
            this._RS485paritetYNCombo.Name = "_RS485paritetYNCombo";
            this._RS485paritetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485paritetYNCombo.TabIndex = 14;
            this._RS485paritetYNCombo.SelectedIndexChanged += new System.EventHandler(this._RS485paritetYNCombo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Паритет";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Количество стоп бит";
            // 
            // _RS485stopBitsCombo
            // 
            this._RS485stopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485stopBitsCombo.FormattingEnabled = true;
            this._RS485stopBitsCombo.Location = new System.Drawing.Point(181, 62);
            this._RS485stopBitsCombo.Name = "_RS485stopBitsCombo";
            this._RS485stopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485stopBitsCombo.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Скорость передачи";
            // 
            // _RS485speedsCombo
            // 
            this._RS485speedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._RS485speedsCombo.FormattingEnabled = true;
            this._RS485speedsCombo.Location = new System.Drawing.Point(181, 22);
            this._RS485speedsCombo.Name = "_RS485speedsCombo";
            this._RS485speedsCombo.Size = new System.Drawing.Size(97, 21);
            this._RS485speedsCombo.TabIndex = 9;
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._configurationTabControl.Controls.Add(this._USBtabPage);
            this._configurationTabControl.Controls.Add(this._RS485Page);
            this._configurationTabControl.Controls.Add(this._analogPage);
            this._configurationTabControl.Controls.Add(this.setpointPage);
            this._configurationTabControl.Controls.Add(this._versionPage);
            this._configurationTabControl.Location = new System.Drawing.Point(0, 3);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(431, 424);
            this._configurationTabControl.TabIndex = 16;
            // 
            // _USBtabPage
            // 
            this._USBtabPage.Controls.Add(this._usbReadBtn);
            this._USBtabPage.Controls.Add(this._usbWriteBtn);
            this._USBtabPage.Controls.Add(this.groupBox5);
            this._USBtabPage.Location = new System.Drawing.Point(4, 22);
            this._USBtabPage.Name = "_USBtabPage";
            this._USBtabPage.Padding = new System.Windows.Forms.Padding(3);
            this._USBtabPage.Size = new System.Drawing.Size(423, 398);
            this._USBtabPage.TabIndex = 6;
            this._USBtabPage.Text = "USB";
            this._USBtabPage.UseVisualStyleBackColor = true;
            // 
            // _usbReadBtn
            // 
            this._usbReadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._usbReadBtn.Location = new System.Drawing.Point(88, 368);
            this._usbReadBtn.Name = "_usbReadBtn";
            this._usbReadBtn.Size = new System.Drawing.Size(75, 23);
            this._usbReadBtn.TabIndex = 16;
            this._usbReadBtn.Text = "Записать";
            this._usbReadBtn.UseVisualStyleBackColor = true;
            this._usbReadBtn.Visible = false;
            this._usbReadBtn.Click += new System.EventHandler(this._usbReadButtonClick);
            // 
            // _usbWriteBtn
            // 
            this._usbWriteBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._usbWriteBtn.Location = new System.Drawing.Point(7, 368);
            this._usbWriteBtn.Name = "_usbWriteBtn";
            this._usbWriteBtn.Size = new System.Drawing.Size(75, 23);
            this._usbWriteBtn.TabIndex = 17;
            this._usbWriteBtn.Text = "Прочитать";
            this._usbWriteBtn.UseVisualStyleBackColor = true;
            this._usbWriteBtn.Click += new System.EventHandler(this._USBWriteBtnClick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._USBdataBitsCombo);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this._USBToSendAfterTB);
            this.groupBox5.Controls.Add(this._USBToSendBeforeTB);
            this.groupBox5.Controls.Add(this._USBAddressTB);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this._USBdoubleSpeedCombo);
            this.groupBox5.Controls.Add(this._USBparitetCHETCombo);
            this.groupBox5.Controls.Add(this._USBparitetYNCombo);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this._USBstopBitsCombo);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this._USBspeedsCombo);
            this.groupBox5.Location = new System.Drawing.Point(7, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(287, 210);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Конфигурация USB";
            // 
            // _USBdataBitsCombo
            // 
            this._USBdataBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBdataBitsCombo.FormattingEnabled = true;
            this._USBdataBitsCombo.Location = new System.Drawing.Point(181, 42);
            this._USBdataBitsCombo.Name = "_USBdataBitsCombo";
            this._USBdataBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._USBdataBitsCombo.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(126, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Количество бит данных";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 183);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(163, 13);
            this.label17.TabIndex = 25;
            this.label17.Text = "Таймаут после выдачи данных";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 164);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(145, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Таймаут до выдачи данных";
            // 
            // _USBToSendAfterTB
            // 
            this._USBToSendAfterTB.Location = new System.Drawing.Point(181, 180);
            this._USBToSendAfterTB.Name = "_USBToSendAfterTB";
            this._USBToSendAfterTB.Size = new System.Drawing.Size(97, 20);
            this._USBToSendAfterTB.TabIndex = 23;
            this._USBToSendAfterTB.Tag = "255";
            this._USBToSendAfterTB.Text = "0";
            // 
            // _USBToSendBeforeTB
            // 
            this._USBToSendBeforeTB.Location = new System.Drawing.Point(181, 161);
            this._USBToSendBeforeTB.Name = "_USBToSendBeforeTB";
            this._USBToSendBeforeTB.Size = new System.Drawing.Size(97, 20);
            this._USBToSendBeforeTB.TabIndex = 22;
            this._USBToSendBeforeTB.Tag = "255";
            this._USBToSendBeforeTB.Text = "0";
            // 
            // _USBAddressTB
            // 
            this._USBAddressTB.Location = new System.Drawing.Point(181, 142);
            this._USBAddressTB.Name = "_USBAddressTB";
            this._USBAddressTB.Size = new System.Drawing.Size(97, 20);
            this._USBAddressTB.TabIndex = 19;
            this._USBAddressTB.Tag = "255";
            this._USBAddressTB.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 145);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(98, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Адрес устройства";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 125);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(107, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Удвоение скорости";
            // 
            // _USBdoubleSpeedCombo
            // 
            this._USBdoubleSpeedCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBdoubleSpeedCombo.FormattingEnabled = true;
            this._USBdoubleSpeedCombo.Location = new System.Drawing.Point(181, 122);
            this._USBdoubleSpeedCombo.Name = "_USBdoubleSpeedCombo";
            this._USBdoubleSpeedCombo.Size = new System.Drawing.Size(97, 21);
            this._USBdoubleSpeedCombo.TabIndex = 16;
            // 
            // _USBparitetCHETCombo
            // 
            this._USBparitetCHETCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBparitetCHETCombo.FormattingEnabled = true;
            this._USBparitetCHETCombo.Location = new System.Drawing.Point(181, 102);
            this._USBparitetCHETCombo.Name = "_USBparitetCHETCombo";
            this._USBparitetCHETCombo.Size = new System.Drawing.Size(97, 21);
            this._USBparitetCHETCombo.TabIndex = 15;
            // 
            // _USBparitetYNCombo
            // 
            this._USBparitetYNCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBparitetYNCombo.FormattingEnabled = true;
            this._USBparitetYNCombo.Location = new System.Drawing.Point(181, 82);
            this._USBparitetYNCombo.Name = "_USBparitetYNCombo";
            this._USBparitetYNCombo.Size = new System.Drawing.Size(97, 21);
            this._USBparitetYNCombo.TabIndex = 14;
            this._USBparitetYNCombo.SelectedIndexChanged += new System.EventHandler(this._USBparitetYNCombo_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 85);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "Паритет";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 65);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(112, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Количество стоп бит";
            // 
            // _USBstopBitsCombo
            // 
            this._USBstopBitsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBstopBitsCombo.FormattingEnabled = true;
            this._USBstopBitsCombo.Location = new System.Drawing.Point(181, 62);
            this._USBstopBitsCombo.Name = "_USBstopBitsCombo";
            this._USBstopBitsCombo.Size = new System.Drawing.Size(97, 21);
            this._USBstopBitsCombo.TabIndex = 11;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 25);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(105, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Скорость передачи";
            // 
            // _USBspeedsCombo
            // 
            this._USBspeedsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._USBspeedsCombo.Enabled = false;
            this._USBspeedsCombo.FormattingEnabled = true;
            this._USBspeedsCombo.Location = new System.Drawing.Point(181, 22);
            this._USBspeedsCombo.Name = "_USBspeedsCombo";
            this._USBspeedsCombo.Size = new System.Drawing.Size(97, 21);
            this._USBspeedsCombo.TabIndex = 9;
            // 
            // setpointPage
            // 
            this.setpointPage.Controls.Add(this.groupBox11);
            this.setpointPage.Controls.Add(this.groupBox13);
            this.setpointPage.Location = new System.Drawing.Point(4, 22);
            this.setpointPage.Name = "setpointPage";
            this.setpointPage.Padding = new System.Windows.Forms.Padding(3);
            this.setpointPage.Size = new System.Drawing.Size(423, 398);
            this.setpointPage.TabIndex = 7;
            this.setpointPage.Text = "Уставки";
            this.setpointPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioButton3);
            this.groupBox11.Controls.Add(this.radioButton7);
            this.groupBox11.Controls.Add(this.radioButton2);
            this.groupBox11.Controls.Add(this.radioButton6);
            this.groupBox11.Controls.Add(this.radioButton1);
            this.groupBox11.Controls.Add(this.radioButton4);
            this.groupBox11.Controls.Add(this.radioButton5);
            this.groupBox11.Location = new System.Drawing.Point(6, 121);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(227, 181);
            this.groupBox11.TabIndex = 22;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Режимы работы";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 65);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(136, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "";
            this.radioButton3.Text = "Подключено плечо \"-\"";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(6, 157);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(86, 17);
            this.radioButton7.TabIndex = 7;
            this.radioButton7.TabStop = true;
            this.radioButton7.Tag = "";
            this.radioButton7.Text = "Калибровка";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(139, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "";
            this.radioButton2.Text = "Подключено плечо \"+\"";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(6, 134);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(140, 17);
            this.radioButton6.TabIndex = 6;
            this.radioButton6.TabStop = true;
            this.radioButton6.Tag = "";
            this.radioButton6.Text = "Без подключения плеч";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(77, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "";
            this.radioButton1.Text = "Ожидание";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(6, 88);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(203, 17);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "";
            this.radioButton4.Text = "Включение двух плеч попеременно";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(6, 111);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(209, 17);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "";
            this.radioButton5.Text = "Включение двух плеч одновременно";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._timeout);
            this.groupBox13.Controls.Add(this._resistOn);
            this.groupBox13.Controls.Add(this._readSetpointBtn);
            this.groupBox13.Controls.Add(this._writeSetpointBtn);
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.label27);
            this.groupBox13.Controls.Add(this.label28);
            this.groupBox13.Controls.Add(this.label30);
            this.groupBox13.Location = new System.Drawing.Point(6, 6);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(411, 109);
            this.groupBox13.TabIndex = 20;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Уставки";
            // 
            // _timeout
            // 
            this._timeout.Location = new System.Drawing.Point(216, 45);
            this._timeout.Name = "_timeout";
            this._timeout.Size = new System.Drawing.Size(63, 20);
            this._timeout.TabIndex = 21;
            // 
            // _resistOn
            // 
            this._resistOn.Location = new System.Drawing.Point(216, 19);
            this._resistOn.Name = "_resistOn";
            this._resistOn.Size = new System.Drawing.Size(63, 20);
            this._resistOn.TabIndex = 21;
            // 
            // _readSetpointBtn
            // 
            this._readSetpointBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readSetpointBtn.Location = new System.Drawing.Point(6, 80);
            this._readSetpointBtn.Name = "_readSetpointBtn";
            this._readSetpointBtn.Size = new System.Drawing.Size(75, 23);
            this._readSetpointBtn.TabIndex = 20;
            this._readSetpointBtn.Text = "Прочитать";
            this._readSetpointBtn.Click += new System.EventHandler(this._setpointReadButtonClick);
            // 
            // _writeSetpointBtn
            // 
            this._writeSetpointBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeSetpointBtn.Location = new System.Drawing.Point(87, 80);
            this._writeSetpointBtn.Name = "_writeSetpointBtn";
            this._writeSetpointBtn.Size = new System.Drawing.Size(75, 23);
            this._writeSetpointBtn.TabIndex = 20;
            this._writeSetpointBtn.Text = "Записать";
            this._writeSetpointBtn.Click += new System.EventHandler(this._setpointWriteBtnClick);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(286, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(23, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Ом";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(161, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Сопротивление срабатывания";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 48);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(204, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Задержка между подключениями плеч";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(286, 48);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(21, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "мс";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить калибровку каналов для МИИ";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть калибровку каналов МИИ";
            // 
            // MII5CHConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 452);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MII5CHConfigForm";
            this.Text = "Channel5ConfigForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MII5CHConfigForm_FormClosing);
            this.Load += new System.EventHandler(this.Channel5ConfigForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this._versionPage.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._analogPage.ResumeLayout(false);
            this._analogPage.PerformLayout();
            this._passInsertBox.ResumeLayout(false);
            this._passInsertBox.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._descriptionGroup.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._RS485Page.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._configurationTabControl.ResumeLayout(false);
            this._USBtabPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.setpointPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _readRs485Button;
        private System.Windows.Forms.TabPage _versionPage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.Label _statusStep;
        private System.Windows.Forms.TabPage _RS485Page;
        private System.Windows.Forms.Button _RS485WriteBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _RS485dataBitsCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _RS485ToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _RS485ToSendBeforeTB;
        private System.Windows.Forms.MaskedTextBox _RS485AddressTB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _RS485doubleSpeedCombo;
        private System.Windows.Forms.ComboBox _RS485paritetCHETCombo;
        private System.Windows.Forms.ComboBox _RS485paritetYNCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _RS485stopBitsCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _RS485speedsCombo;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.MaskedTextBox _ldrRevision;
        private System.Windows.Forms.MaskedTextBox _ldrCodProc;
        private System.Windows.Forms.MaskedTextBox _ldrFuse;
        private System.Windows.Forms.MaskedTextBox _ldrName;
        private System.Windows.Forms.MaskedTextBox _devRevision;
        private System.Windows.Forms.MaskedTextBox _devMod;
        private System.Windows.Forms.MaskedTextBox _devPodtip;
        private System.Windows.Forms.MaskedTextBox _devName;
        private System.Windows.Forms.MaskedTextBox _appRevision;
        private System.Windows.Forms.MaskedTextBox _appMod;
        private System.Windows.Forms.MaskedTextBox _appPodtip;
        private System.Windows.Forms.MaskedTextBox _appName;
        private System.Windows.Forms.Button _readVersionBtn;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.TabPage _USBtabPage;
        private System.Windows.Forms.Button _usbReadBtn;
        private System.Windows.Forms.Button _usbWriteBtn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _USBdataBitsCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _USBToSendAfterTB;
        private System.Windows.Forms.MaskedTextBox _USBToSendBeforeTB;
        private System.Windows.Forms.MaskedTextBox _USBAddressTB;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox _USBdoubleSpeedCombo;
        private System.Windows.Forms.ComboBox _USBparitetCHETCombo;
        private System.Windows.Forms.ComboBox _USBparitetYNCombo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox _USBstopBitsCombo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox _USBspeedsCombo;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _channelCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button _readChannelConfigBtn;
        private System.Windows.Forms.Button _writeChannelConfigBtn;
        private System.Windows.Forms.GroupBox _descriptionGroup;
        private Forms.RichTextBox.AdvRichTextBox _descriptionBox;
        private System.Windows.Forms.Button _continueBtn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox _Uin;
        private System.Windows.Forms.MaskedTextBox _rangeOfScale;
        private System.Windows.Forms.MaskedTextBox _koeffpA;
        private System.Windows.Forms.MaskedTextBox _koeffA;
        private System.Windows.Forms.MaskedTextBox _koeffB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage setpointPage;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.MaskedTextBox _resistOn;
        private System.Windows.Forms.Button _readSetpointBtn;
        private System.Windows.Forms.Button _writeSetpointBtn;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox _timeout;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox _passInsertBox;
        private System.Windows.Forms.Button _validatePassButton;
        private System.Windows.Forms.TextBox _passTB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
    }
}