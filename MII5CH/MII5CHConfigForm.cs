﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.ChannelFive;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Byte;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MII5CH.Structures;

namespace BEMN.MII5CH
{
    public partial class MII5CHConfigForm : Form, IFormView
    {
        #region Private fields
        private MII5CHDevice _device;
        private AnalogExtConfig _currentConfigStruct;
        private MemoryEntity<OneWordStruct> _valueChannelWord; 
        private MemoryEntity<AnalogExtConfig> _currentAnalogConfigMem;
        private MemoryEntity<OneWordStruct> _modeWord;
        private NewStructValidator<MemConfigConnection> _configUsbValidator;
        private NewStructValidator<MemConfigConnection> _configRs485Validator;
        private NewStructValidator<AnalogExtConfig> _analogConfigValidator;
        private NewStructValidator<AddSetpointStruct> _setpointValidator;
        private MemoryEntity<MemConfigRS485> _configRs485;
        private ushort _currentModeVal;
        private int _step;
        private int _counter;
        private int _selectedChannel;
        private double _temp;
        private Mode _mode;
        private const string XML_HEAD = "MII_SET_POINTS";
        private const string PASS = "1111";
        private const string INCORRECT_PASSWORD = "Неверный пароль";

        private enum Mode
        {
            Waiting = 0,
            OnlyPlusOn = 1,
            OnlyMinusOn = 2,
            VariableOn = 3,
            Connection = 4,
            NotConnection = 5,
            Calibrate = 65525
        }
        #endregion

        #region Intialization
        public MII5CHConfigForm()
        {
            InitializeComponent();
        }

        public MII5CHConfigForm(MII5CHDevice device)
        {
            InitializeComponent();
            _device = device;
            InitMemoryEntities();
            InitValidators();
        }

        private void InitMemoryEntities()
        {
            #region USB
            _device.ConfigUsb.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadUSBOk);
            _device.ConfigUsb.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.READ_USB_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });
            _device.ConfigUsb.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_USB_OK;
                _statusLabel.BackColor = Color.LightGreen;
            });
            _device.ConfigUsb.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_USB_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });

            _device.ConfigUsb.ReadOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.ConfigUsb.ReadFail += HandlerHelper.CreateHandler(this, PerformStep);
            _device.ConfigUsb.WriteOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.ConfigUsb.WriteFail += HandlerHelper.CreateHandler(this, PerformStep);
            #endregion

            #region RS485

            this._configRs485 = _device.ConfigRs485Old;
            _device.ConfigRs485Old.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRs485Ok);
            _device.ConfigRs485Old.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.READ_RS485_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });
            _device.ConfigRs485Old.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_RS485_OK;
                _statusLabel.BackColor = Color.LightGreen;
            });
            _device.ConfigRs485Old.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_RS485_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });

            _device.ConfigRs485Old.ReadOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.ConfigRs485Old.ReadFail += HandlerHelper.CreateHandler(this, PerformStep);
            _device.ConfigRs485Old.WriteOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.ConfigRs485Old.WriteFail += HandlerHelper.CreateHandler(this, PerformStep);
            #endregion

            #region Calibration

            InitAnalogMemEntity(_device.CalibrovkaAnalog1, 1);
            InitAnalogMemEntity(_device.CalibrovkaAnalog2, 2);
            InitAnalogMemEntity(_device.CalibrovkaAnalog3, 3);
            InitAnalogMemEntity(_device.CalibrovkaAnalog4, 4);
            InitAnalogMemEntity(_device.CalibrovkaAnalog5, 5);
            _modeWord = _device.Mode;
            _modeWord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, SwitchRadioButton);
            _modeWord.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, _modeWord.LoadStruct);
            _modeWord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.READ_WRITE_MOD_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });
            _modeWord.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.READ_WRITE_MOD_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });

            _valueChannelWord = _device.ValueWord;
            _valueChannelWord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, MeasuringKoefficient);
            #endregion

            #region Setpoint
            _device.Setpoint.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, SetpointReadOk);
            _device.Setpoint.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.READ_SETPOINT_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });
            _device.Setpoint.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_SETPOINT_OK;
                _statusLabel.BackColor = Color.LightGreen;
            });
            _device.Setpoint.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusLabel.Text = ConfigurationStringConst.WRITE_SETPOINT_OK;
                _statusLabel.BackColor = Color.IndianRed;
            });

            _device.Setpoint.ReadOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.Setpoint.ReadFail += HandlerHelper.CreateHandler(this, PerformStep);
            _device.Setpoint.WriteOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.Setpoint.WriteFail += HandlerHelper.CreateHandler(this, PerformStep);
            #endregion

            #region Version
            _device.Version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadVersionComplite);
            _device.Version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._appName.Text = this._appMod.Text = this._appPodtip.Text = this._appRevision.Text = ConfigurationStringConst.NAN;
                this._devMod.Text = this._devName.Text = this._devPodtip.Text = this._devRevision.Text = ConfigurationStringConst.NAN;
                this._ldrCodProc.Text = this._ldrFuse.Text = this._ldrName.Text = this._ldrRevision.Text = ConfigurationStringConst.NAN;
                _statusLabel.Text = ConfigurationStringConst.READ_VERSION_FAIL;
                _statusLabel.BackColor = Color.IndianRed;
            });

            _device.Version.ReadOk += HandlerHelper.CreateHandler(this, PerformStep);
            _device.Version.ReadFail += HandlerHelper.CreateHandler(this, PerformStep);
            #endregion
        }

        private void InitValidators()
        {
            ToolTip toolTip = new ToolTip();
            _configUsbValidator = new NewStructValidator<MemConfigConnection>(
                toolTip,
                new ControlInfoCombo(_USBspeedsCombo, StringData.Speeds),
                new ControlInfoCombo(_USBdataBitsCombo, StringData.DataBits),
                new ControlInfoCombo(_USBstopBitsCombo, StringData.StopBits),
                new ControlInfoCombo(_USBparitetCHETCombo, StringData.ParitetChet),
                new ControlInfoCombo(_USBparitetYNCombo, StringData.Paritet),
                new ControlInfoCombo(_USBdoubleSpeedCombo, StringData.DoubleSpeed),
                new ControlInfoText(_USBAddressTB, new CustomByteRule(1, 247)),
                new ControlInfoText(_USBToSendAfterTB, new CustomByteRule(0, 255)),
                new ControlInfoText(_USBToSendBeforeTB, new CustomByteRule(0, 255))
            );

            _configRs485Validator = new NewStructValidator<MemConfigConnection>(
                toolTip,
                new ControlInfoCombo(_RS485speedsCombo, StringData.Speeds),
                new ControlInfoCombo(_RS485dataBitsCombo, StringData.DataBits),
                new ControlInfoCombo(_RS485stopBitsCombo, StringData.StopBits),
                new ControlInfoCombo(_RS485paritetCHETCombo, StringData.ParitetChet),
                new ControlInfoCombo(_RS485paritetYNCombo, StringData.Paritet),
                new ControlInfoCombo(_RS485doubleSpeedCombo, StringData.DoubleSpeed),
                new ControlInfoText(_RS485AddressTB,new CustomByteRule(1,247)),
                new ControlInfoText(_RS485ToSendAfterTB, new CustomByteRule(0,255)),
                new ControlInfoText(_RS485ToSendBeforeTB, new CustomByteRule(0,255))
            );
            _analogConfigValidator = new NewStructValidator<AnalogExtConfig>(
                toolTip,
                new ControlInfoText(_koeffB, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(_koeffA, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(_koeffpA, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(_rangeOfScale, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));
            
            this._setpointValidator = new NewStructValidator<AddSetpointStruct>(
                toolTip,
                new ControlInfoText(this._timeout, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._resistOn, new CustomIntRule(100, 6553500)));
        }

        private void InitAnalogMemEntity(MemoryEntity<AnalogExtConfig> analogConfig, int ind)
        {
            analogConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, AnalogConfigReadOk);
            analogConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusStep.Text = string.Format(ConfigurationStringConst.READ_ANALOG_CONFIG_FAIL, ind);
                _statusStep.BackColor = Color.IndianRed;
            });
            analogConfig.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusStep.Text = string.Format(ConfigurationStringConst.WRITE_ANALOG_CONFIG_OK, ind);
                _statusStep.BackColor = Color.LightGreen;
                _currentAnalogConfigMem.LoadStruct();
            });
            analogConfig.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _statusStep.Text = string.Format(ConfigurationStringConst.WRITE_ANALOG_CONFIG_FAIL, ind);
                _statusStep.BackColor = Color.IndianRed;
            });
        }

        private void Channel5ConfigForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._USBtabPage.Enabled = false;
                this._RS485Page.Enabled = false;
                this._analogPage.Enabled = false;
                this.setpointPage.Enabled = false;
                this._versionPage.Enabled = false;
                return;
            }
            _progressBar.Maximum = this._device.ConfigUsb.Slots.Count + this._device.ConfigRs485.Slots.Count +
                                   this._device.Version.Slots.Count+this._device.Setpoint.Slots.Count;
            _device.ConfigUsb.LoadStruct();
            //_device.ConfigRs485.LoadStruct();
            _configRs485.LoadStruct();
            this._device.Setpoint.LoadStruct();
            _device.Version.LoadStruct();
            _modeWord.LoadStruct();
            _configUsbValidator.Reset();
            _configRs485Validator.Reset();
            _analogConfigValidator.Reset();
            _setpointValidator.Reset();
            _selectedChannel = -1;
            _mode = Mode.NotConnection;
        }

        private void MII5CHConfigForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _valueChannelWord.RemoveStructQueries();
            if (_step > 0)
            {
                _modeWord.Value.Word = _currentModeVal;
                _modeWord.SaveStruct();
                _currentAnalogConfigMem.Value = _currentConfigStruct;
                _currentAnalogConfigMem.SaveStruct();
            }
        }
        #endregion

        #region USB
        public void ReadUSBOk()
        {
            _statusLabel.Text = ConfigurationStringConst.READ_USB_OK;
            _statusLabel.BackColor = Color.LightGreen;
            _configUsbValidator.Set(_device.ConfigUsb.Value);
        }

        private void _usbReadButtonClick(object sender, EventArgs e)
        {
            try
            {
                string mes;
                if (_configUsbValidator.Check(out mes, true))
                {
                    _device.ConfigUsb.Value = _configUsbValidator.Get();
                    _device.ConfigUsb.SaveStruct();
                }
                else throw new Exception("Invalid USB-config data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            
        }

        private void _USBWriteBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            _statusLabel.Text = string.Empty;
            _statusLabel.BackColor = Color.Transparent;
            _progressBar.Maximum = _device.ConfigUsb.Slots.Count;
            _device.ConfigUsb.LoadStruct();
        }

        private void _USBparitetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = sender as ComboBox;
            if (cb == null || cb.SelectedIndex == -1) return;
            _USBparitetCHETCombo.Enabled = cb.SelectedIndex != 0;
        }
        #endregion USB

        #region RS485
        public void ReadRs485Ok()
        {
            //_statusLabel.Text = ConfigurationStringConst.READ_RS485_OK;
            //_statusLabel.BackColor = Color.LightGreen;
            //_configRs485Validator.Set(_device.ConfigRs485.Value);
            try
            {
                _RS485speedsCombo.SelectedItem = _configRs485.Value.RS485Speed;
                _RS485dataBitsCombo.SelectedItem = _configRs485.Value.RS485DataBits;
                _RS485stopBitsCombo.SelectedItem = _configRs485.Value.RS485StopBits;
                _RS485paritetYNCombo.SelectedItem = _configRs485.Value.RS485ParitetOnOff;
                _RS485paritetCHETCombo.SelectedItem = _configRs485.Value.RS485ParitetChet;
                _RS485doubleSpeedCombo.SelectedItem = _configRs485.Value.RS485DoubleSpeed;
                _RS485AddressTB.Text = _configRs485.Value.RS485Address.ToString();
                _RS485ToSendBeforeTB.Text = _configRs485.Value.RS485ToSendBefore.ToString();
                _RS485ToSendAfterTB.Text = _configRs485.Value.RS485ToSendAfter.ToString();
                if (_RS485paritetYNCombo.SelectedIndex > 0)
                {
                    _RS485paritetCHETCombo.Enabled = true;
                }
                _statusLabel.Text = ConfigurationStringConst.READ_RS485_OK;
                _statusLabel.BackColor = Color.Green;
            }
            catch (Exception)
            { }
        }

        private void _readRs485Button_Click(object sender, EventArgs e)
        {
            _statusLabel.Text = string.Empty;
            _statusLabel.BackColor = Color.Transparent;
            _progressBar.Maximum = _device.ConfigRs485.Slots.Count;
            //_device.ConfigRs485.LoadStruct();
            _configRs485.LoadStruct();
        }

        private void _RS485WriteBtnClick(object sender, EventArgs e)
        {
            try
            {
                string mes;
                if (_configRs485Validator.Check(out mes, true) && WriteRS485())
                {
                    //_device.ConfigRs485.Value = _configRs485Validator.Get();
                    //_device.ConfigRs485.SaveStruct();
                    _configRs485.SaveStruct();
                }
                else throw new Exception("Invalid RS485-config data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        public bool WriteRS485()
        {
            bool res = true;
            try
            {
                MemConfigRS485 config = new MemConfigRS485();
                config.InitStruct(new byte[_configRs485.Values.Length * 2]);
                config.RS485Speed = _RS485speedsCombo.SelectedItem.ToString();
                config.RS485DataBits = _RS485dataBitsCombo.SelectedItem.ToString();
                config.RS485StopBits = _RS485stopBitsCombo.SelectedItem.ToString();
                config.RS485ParitetOnOff = _RS485paritetYNCombo.SelectedItem.ToString();
                config.RS485ParitetChet = _RS485paritetCHETCombo.SelectedItem.ToString();
                config.RS485DoubleSpeed = _RS485doubleSpeedCombo.SelectedItem.ToString();
                config.RS485Address = Convert.ToByte(_RS485AddressTB.Text);
                config.RS485ToSendBefore = Convert.ToByte(_RS485ToSendBeforeTB.Text);
                config.RS485ToSendAfter = Convert.ToByte(_RS485ToSendAfterTB.Text);
                this._configRs485.Value = config;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        private void _RS485paritetYNCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = sender as ComboBox;
            if (cb == null || cb.SelectedIndex == -1) return;
            _RS485paritetCHETCombo.Enabled = cb.SelectedIndex != 0;
        }
        #endregion

        #region Версия
        public void ReadVersionComplite()
        {
            #region AppVers
            _appName.Text = _device.Version.Value.AppName;
            _appPodtip.Text = _device.Version.Value.AppPodtip;
            _appMod.Text = _device.Version.Value.AppMod;
            _appRevision.Text = _device.Version.Value.AppVersion;
            #endregion

            #region DevVers
            _devName.Text = _device.Version.Value.DevName;
            _devPodtip.Text = _device.Version.Value.DevPodtip;
            _devMod.Text = _device.Version.Value.DevMod;
            _devRevision.Text = _device.Version.Value.DevVersion;
            #endregion

            #region LdrVers
            _ldrName.Text = _device.Version.Value.LdrName;
            _ldrCodProc.Text = _device.Version.Value.LdrCodProc;
            _ldrFuse.Text = _device.Version.Value.LdrFuseProc;
            _ldrRevision.Text = _device.Version.Value.LdrVersion;
            #endregion

            _statusLabel.Text = ConfigurationStringConst.READ_VERSION_OK;
            _statusLabel.BackColor = Color.LightGreen;
        }

        private void _readVersionBtn_Click(object sender, EventArgs e)
        {
            _statusLabel.Text = string.Empty;
            _statusLabel.BackColor = Color.Transparent;
            _progressBar.Maximum = _device.Version.Slots.Count;
            _device.Version.LoadStruct();
        }
        #endregion

        #region Calibration
        
        private void AnalogConfigReadOk()
        {
            _analogConfigValidator.Set(_currentAnalogConfigMem.Value);
            switch (this._step)
            {
                case 0:
                    this._currentConfigStruct = this._currentAnalogConfigMem.Value;
                    break;
                case 1:
                    this._continueBtn.Enabled = true;
                    this._statusStep.Text = ConfigurationStringConst.DEFUULT_CONFIG_SAVE_OK;
                    this._descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, this._step);
                    this._descriptionBox.Text = this._selectedChannel != 3 ? ConfigurationStringConst.Info1 : ConfigurationStringConst.Info1Channel4;
                    break;
                case 2:
                    this._continueBtn.Enabled = true;
                    this._statusStep.Text = ConfigurationStringConst.B_SAVED;
                    this._descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, this._step);
                    this._descriptionBox.Text = this._selectedChannel != 3
                        ? ConfigurationStringConst.Info2
                        : ConfigurationStringConst.Info2Channel4;
                    if (_channelCombo.SelectedItem == "Канал 4")
                    {
                        this._Uin.Enabled = false;
                        this._rangeOfScale.Enabled = true;
                    }
                    else
                    {
                        this._rangeOfScale.Enabled = this._Uin.Enabled = true;
                    }
                    break;
                case 3:
                    this._continueBtn.Enabled = true;
                    this._statusStep.Text = ConfigurationStringConst.COMPLETE;
                    this._descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, this._step);
                    this._descriptionBox.Text = ConfigurationStringConst.Info3;
                    if (this._currentAnalogConfigMem == this._device.CalibrovkaAnalog1 || this._currentAnalogConfigMem == this._device.CalibrovkaAnalog2)
                        this._descriptionBox.Text += ConfigurationStringConst.InfoDop;
                    this._rangeOfScale.Enabled = this._Uin.Enabled = this._koeffA.Enabled = this._koeffB.Enabled = this._koeffpA.Enabled = true;
                    this._continueBtn.Text = ConfigurationStringConst.BREAK;
                    break;
            }
        }
        
        private void _channelCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var box = sender as ComboBox;
            if (box == null) return;
            if(_selectedChannel == box.SelectedIndex) return;
            if (_selectedChannel != box.SelectedIndex && _step != 0)
            {
                var res = MessageBox.Show(ConfigurationStringConst.CALIBRATE_IS_NOT_COMPLETE, "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.No)
                {
                    _channelCombo.SelectedIndex = _selectedChannel;
                    return;
                }
                else
                {
                    _modeWord.Value.Word = _currentModeVal;
                    _modeWord.SaveStruct();
                    _currentAnalogConfigMem.Value = _currentConfigStruct;
                    _currentAnalogConfigMem.SaveStruct();
                }
            }
            _selectedChannel = box.SelectedIndex;
            if (_selectedChannel == -1) return;
            _readChannelConfigBtn.Enabled = _writeChannelConfigBtn.Enabled = _continueBtn.Enabled = true;
            _step = 0;
            switch (_selectedChannel)
            {
                case 0:
                    _currentAnalogConfigMem = _device.CalibrovkaAnalog1;
                    _valueChannelWord.StartAddress = 0x0000;
                    break;
                case 1:
                    _currentAnalogConfigMem = _device.CalibrovkaAnalog2;
                    _valueChannelWord.StartAddress = 0x0001;
                    break;
                case 2:
                    _currentAnalogConfigMem = _device.CalibrovkaAnalog3;
                    _valueChannelWord.StartAddress = 0x0002;
                    break;
                case 3:
                    _currentAnalogConfigMem = _device.CalibrovkaAnalog4;
                    _valueChannelWord.StartAddress = 0x0003;
                    break;
                case 4:
                    _currentAnalogConfigMem = _device.CalibrovkaAnalog5;
                    _valueChannelWord.StartAddress = 0x0004;
                    break;
            }
            _currentAnalogConfigMem.LoadStruct();
        }

        private void _continueBtn_Click(object sender, EventArgs e)
        {
            switch (_step)
            {
                case 0:
                    _step++;
                    SaveDefaultParameters();
                    _statusStep.Text = string.Empty;
                    _statusLabel.Text = string.Empty;
                    this.radioButton5.Enabled = this.radioButton6.Enabled = this.radioButton7.Enabled = false;
                    _koeffA.Enabled = _koeffB.Enabled = _koeffpA.Enabled = _rangeOfScale.Enabled = _Uin.Enabled = false;
                    _continueBtn.Enabled = _readChannelConfigBtn.Enabled = _writeChannelConfigBtn.Enabled = false;
                    break;
                case 1:
                    _step++;
                    _continueBtn.Enabled = false;
                    _statusStep.Text = ConfigurationStringConst.B_SAVING;
                    _counter = 0;
                    _temp = 0;
                    _valueChannelWord.LoadStructCycle();
                    break;
                case 2:
                    string str;
                    if (!CheckUin()||!_analogConfigValidator.Check(out  str, true)) return;
                    _step++;
                    _continueBtn.Enabled = false;
                    _statusStep.Text = ConfigurationStringConst.A_MEASSURING;
                    _counter = 0;
                    _temp = 0;
                    _valueChannelWord.LoadStructCycle();
                    break;
                case 3:
                    _step = 0;
                    _modeWord.Value.Word = _currentModeVal;
                    _modeWord.SaveStruct();
                    this.radioButton5.Enabled = this.radioButton6.Enabled = this.radioButton7.Enabled = true;
                    _koeffA.Enabled = _koeffB.Enabled = _koeffpA.Enabled = _rangeOfScale.Enabled = _Uin.Enabled = true;
                    _readChannelConfigBtn.Enabled = _writeChannelConfigBtn.Enabled = true;
                    _descriptionGroup.Text = string.Empty;
                    _descriptionBox.Text = ConfigurationStringConst.Info0;
                    _continueBtn.Text = ConfigurationStringConst.CONTINUE;
                    break;
            }
        }

        private void SaveDefaultParameters()
        {
            _currentModeVal = _modeWord.Value.Word;
            _modeWord.Value.Word = 0xFFFF;
            _modeWord.SaveStruct();
            _koeffB.Text = Convert.ToString(0x0000);
            _koeffA.Text = Convert.ToString(0x7FFF);
            _koeffpA.Text = Convert.ToString(0x0000);
            _rangeOfScale.Text = Convert.ToString(_currentConfigStruct.RangeOfScale);
            _currentAnalogConfigMem.Value = _analogConfigValidator.Get();
            _currentAnalogConfigMem.SaveStruct();
        }

        private void MeasuringKoefficient()
        {
            if (_counter != 100)
            {
                _temp += _valueChannelWord.Value.Word;
                _counter++;
            }
            else
            {
                _valueChannelWord.RemoveStructQueries();
                _temp = _temp / 100;
                switch (_step)
                {
                    case 2:
                        SaveNewKoefB();
                        break;
                    case 3:
                        MeasureAndSaveKoefA();
                        break;
                }
            }
        }

        private void SaveNewKoefB()
        {
            _koeffB.Text = ((ushort) _temp).ToString("D");
            _currentAnalogConfigMem.Value = _analogConfigValidator.Get();
            _currentAnalogConfigMem.SaveStruct();
        }

        private bool CheckUin()
        {
            try
            {
                double uin;
                ushort limit;
                bool res = ushort.TryParse(_rangeOfScale.Text, out limit);
                res &= double.TryParse(_Uin.Text, out uin);
                if (uin > limit || !res) throw new Exception("Проверьте правильность ввода диапазона шкалы и входного сигнала и нажмите кнопку \"Продолжить\"!");
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void MeasureAndSaveKoefA()
        {
            var num = 0x8000*double.Parse(_Uin.Text)/(_temp - _currentAnalogConfigMem.Value.B)/double.Parse(_rangeOfScale.Text);
            if (num < 0)
            {
                _descriptionBox.Text = ConfigurationStringConst.MES_ERROR_MEASURE_KOEF_A;
                _statusStep.Text = ConfigurationStringConst.ERR_KOEF_A;
                _step--;
                return;
            }
            ushort _pA = 0;
            while (num > 1)
            {
                num /= 2;
                _pA++;
            }
            _koeffA.Text = ((ushort)(num*0xFFFF)).ToString("D");
            _koeffpA.Text = _pA.ToString("D");
            string str;
            if (_analogConfigValidator.Check(out str, true))
            {
                _currentAnalogConfigMem.Value = _analogConfigValidator.Get();
                _currentAnalogConfigMem.SaveStruct();
            }
        }

        private void SwitchRadioButton()
        {
            Mode mode = (Mode)this._modeWord.Value.Word;
            switch (mode)
            {
                case Mode.Waiting:
                    this._mode = mode;
                    this.radioButton1.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.OnlyPlusOn:
                    this._mode = mode;
                    this.radioButton2.Checked = true;
                    this.radioButton1.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.OnlyMinusOn:
                    this._mode = mode;
                    this.radioButton3.Checked = true;
                    this.radioButton2.Checked = this.radioButton1.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.VariableOn:
                    this._mode = mode;
                    this.radioButton4.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton1.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.Connection:
                    this._mode = mode;
                    this.radioButton5.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton1.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.NotConnection:
                    this._mode = mode;
                    this.radioButton6.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton1.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.Calibrate:
                    this._mode = mode;
                    this.radioButton7.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton1.Checked = false;
                    break;
            }
        }

        #endregion Calibration

        #region Setpoint
        public void SetpointReadOk()
        {
            _statusLabel.Text = ConfigurationStringConst.WRITE_SETPOINT_OK;
            _statusLabel.BackColor = Color.LightGreen;
            this._setpointValidator.Set(_device.Setpoint.Value);
        }

        private void _setpointReadButtonClick(object sender, EventArgs e)
        {
            _statusLabel.Text = string.Empty;
            _statusLabel.BackColor = Color.Transparent;
            _progressBar.Maximum = _device.Setpoint.Slots.Count;
            _device.Setpoint.LoadStruct();
        }

        private void _setpointWriteBtnClick(object sender, EventArgs e)
        {
            try
            {
                string mes;
                if (this._setpointValidator.Check(out mes, true))
                {
                    _device.Setpoint.Value = this._setpointValidator.Get();
                    _device.Setpoint.SaveStruct();
                }
                else throw new Exception("Invalid setpoint data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
        #endregion

        #region Other

        private void _readChannelConfigBtnClick(object sender, EventArgs e)
        {
            _currentAnalogConfigMem.LoadStruct();
        }

        private void _analogConfigWriteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string s;
                if (_analogConfigValidator.Check(out s, true))
                {
                    _currentAnalogConfigMem.Value = _analogConfigValidator.Get();
                    _currentAnalogConfigMem.SaveStruct();
                }
                else throw new Exception("Invalid analog data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void PerformStep()
        {
            _progressBar.PerformStep();
        }

        private void RadioButtonChanged(object o, EventArgs e)
        {
            RadioButton r = o as RadioButton;
            if (r == null || !r.Checked) return;

            Mode mode = Mode.NotConnection;

            if (r == this.radioButton1)
            {
                mode = Mode.Waiting;
            }
            if (r == this.radioButton2)
            {
                mode = Mode.OnlyPlusOn;
            }
            if (r == this.radioButton3)
            {
                mode = Mode.OnlyMinusOn;
            }
            if (r == this.radioButton4)
            {
                mode = Mode.VariableOn;
            }
            if (r == this.radioButton5)
            {
                mode = Mode.Connection;
            }
            if (r == this.radioButton6)
            {
                mode = Mode.NotConnection;
            }
            if (r == this.radioButton7)
            {
                mode = Mode.Calibrate;
            }
            if (mode != this._mode) this.SetMode(mode);
        }

        private void SetMode(Mode mode)
        {
            switch (mode)
            {
                case Mode.Waiting:
                    this._currentModeVal = 0x0000;
                    this._modeWord.Value.Word = 0x0000;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.OnlyPlusOn:
                    this._currentModeVal = 0x0001;
                    this._modeWord.Value.Word = 0x0001;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.OnlyMinusOn:
                    this._currentModeVal = 0x0002;
                    this._modeWord.Value.Word = 0x0002;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.VariableOn:
                    this._currentModeVal = 0x0003;
                    this._modeWord.Value.Word = 0x0003;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.Connection:
                    this._currentModeVal = 0x0004;
                    this._modeWord.Value.Word = 0x0004;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.NotConnection:
                    this._currentModeVal = 0x0005;
                    this._modeWord.Value.Word = 0x0005;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.Calibrate:
                    this._currentModeVal = 0xFFFF;
                    this._modeWord.Value.Word = 0xFFFF;
                    this._modeWord.SaveStruct();
                    break;
            }
        }
        #endregion
        

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MII5CHDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MII5CHConfigForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void _validatePassButton_Click(object sender, EventArgs e)
        {
            this.PassCheck();
        }

        private void PassCheck()
        {
            if (this._passTB.Text == "")
            {
                return;
            }
            else if (this._passTB.Text == PASS)
            {
                this._passTB.Text = string.Empty;
                this.groupBox12.Enabled = true;
                this._passInsertBox.Enabled = false;
                this._channelCombo.Enabled = true;
            }
            else
            {
                MessageBox.Show(INCORRECT_PASSWORD);
                this._passTB.Text = string.Empty;
            }
        }

        private void _rangeOfScale_TextChanged(object sender, EventArgs e)
        {
            if (_channelCombo.SelectedItem == "Канал 4")
            {
                this._Uin.Enabled = false;
                int rangeValue;
                if (this._rangeOfScale.Text == "")
                {
                    return;
                }
                else
                {
                    rangeValue = Convert.ToInt32(this._rangeOfScale.Text);
                }
                rangeValue = rangeValue / 2;
                this._Uin.Text = Convert.ToString(rangeValue);
            }
            else
            {
                this._Uin.Enabled = true;
            }
        }
    }
}
