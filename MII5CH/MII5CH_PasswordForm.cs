﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BEMN.MII5CH
{
    public partial class MII5CH_PasswordForm : Form
    {
        public MII5CH_PasswordForm()
        {
            InitializeComponent();
        }

        private const string PASSWORD = "1111";



        private void AcceptBtn_Click(object sender, EventArgs e)
        {
            if (_passwordBox.Text == PASSWORD)
            {
                //MessageBox.Show("Пароль принят", "Пароль", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Введен неверный пароль!", "Пароль", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
