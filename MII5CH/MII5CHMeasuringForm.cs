﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BEMN.ChannelFive;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MII5CH.Structures;

namespace BEMN.MII5CH
{
    public partial class MII5CHMeasuringForm : Form, IFormView
    {
        #region Fields
        private const string ERROR_VALUE = "-------";
        private MemoryEntity<UioRam> _uioRam;
        private MemoryEntity<AllAnalogConfig> _analogConfig;
        private MemoryEntity<RelayDiscretLedStruct> _ledStruct;
        private MemoryEntity<SomeStruct> _signature;
        private MII5CHDevice _device;
        private LedControl[] _discretLeds;
        private LedControl[] _relayLeds;
        private LedControl[] _diodLeds;
        private Label[] _discretsLabels;
        private Label[] _relayLabels;
        private Label[] _diodsLabel;
        private byte _countOfDiscrets;
        private byte _countOfRelays;
        private byte _countOfDiods;

        private RadioButton[] _radioButtons;
        #endregion

        #region Inner struct
        public struct BitInfo
        {
            public ushort BitStartAddress;
            public ushort BitIndex;
            public bool BitValue;
        }
        #endregion

        #region Constructor

        public MII5CHMeasuringForm()
        {
            this.InitializeComponent();
        }

        public MII5CHMeasuringForm(MII5CHDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._uioRam = device.UioRam;
            this._uioRam.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadOk);
            this._uioRam.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._analogConfig = device.AllAnalog;
            this._analogConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._uioRam.LoadStruct);
            this._analogConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._signature = device.Signature;
            this._signature.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignatureReadOk);
            this._signature.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.SignatureReadFail);

            this._ledStruct = device.LedStruct;
            this._ledStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.LedStateReadOk);
            this._ledStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.LedStateReadFail);

            this._radioButtons = new[]
            {
                this.radioButton1, this.radioButton2, this.radioButton3, this.radioButton4,
                this.radioButton5, this.radioButton6, this.radioButton7
            };
            device.ModeMeasuring.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._radioButtons.First(r => Convert.ToUInt16(r.Tag) == device.ModeMeasuring.Value.Word).Checked = true;
            });
            device.ModeMeasuring.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                foreach (RadioButton radioButton in this._radioButtons)
                {
                    radioButton.Checked = false;
                }
            });
        }
        #endregion

        private void MeasuringReadOk()
        {
            this.GetValueString(this._uioRam.Value.Analog1, this._analogConfig.Value.RangeOfScale1, this.channel1TextBox);
            this.GetValueString(this._uioRam.Value.Analog2, this._analogConfig.Value.RangeOfScale2, this.channel2TextBox);
            this.GetValueString(this._uioRam.Value.Analog3, this._analogConfig.Value.RangeOfScale3, this.channel3TextBox);
            this.GetValueString(this._uioRam.Value.Analog4, this._analogConfig.Value.RangeOfScale4, this.channel4TextBox);
            this.GetValueString(this._uioRam.Value.Pulse, 100, this.channel5TextBox);

            this.GetResist(this._uioRam.Value.ResistPlus, this._resist1, this._om1);
            this.GetResist(this._uioRam.Value.ResistMinus, this._resist2, this._om2);
            this.GetCapacity(this._uioRam.Value.Capacity, this._c1, this._mkf1);
            this._plus.ForeColor = this._uioRam.Value.Status[0] ? SystemColors.ControlText : SystemColors.ControlDark;
            this._plus.BackColor = this._uioRam.Value.Status[0] ? Color.DarkSeaGreen : SystemColors.Control;
            this._minus.ForeColor = this._uioRam.Value.Status[1] ? SystemColors.ControlText : SystemColors.ControlDark;
            this._minus.BackColor = this._uioRam.Value.Status[1] ? Color.DarkSeaGreen : SystemColors.Control;
        }

        private void GetValueString(ushort value, ushort limit, TextBox box)
        {
            if (value <= 0x7FFF)
            {
                box.BackColor = SystemColors.Control;
                box.Text = string.Format("{0:F2}", (double)limit / 0x7FFF * value);
                return;
            }
            this.GetOtherValue(value, box);
        }

        private void GetResist(ushort value, TextBox box, Label label)
        {
            if (value <= 0x7FFF)
            {
                box.BackColor = SystemColors.Control;
                double resist = value * 100;
                if (resist >= 1000000)
                {
                    label.Text = "МОм";
                    box.Text = string.Format("{0:F2}", resist / 1000000);
                    return;
                }
                if (resist >= 1000)
                {
                    label.Text = "кОм";
                    box.Text = string.Format("{0:F2}", resist / 1000);
                    return;
                }
                label.Text = "Ом";
                box.Text = string.Format("{0:F2}", resist);
                return;
            }
            this.GetOtherValue(value, box, label);
        }

        private void GetCapacity(ushort value, TextBox box, Label label)
        {
            if (value <= 0x7FFF)
            {
                label.Text = "мкФ";
                double c = (double)value / 100;
                box.BackColor = SystemColors.Control;
                box.Text = string.Format("{0:F2}", c);
                return;
            }
            this.GetOtherValue(value, box, label);
        }

        private void GetOtherValue(ushort value, TextBox box, Label label = null)
        {
            if (label != null)
            {
                label.Text = string.Empty;
            }
            switch (value)
            {
                case 0xFFFF:
                    box.BackColor = Color.Yellow;
                    box.Text = ConfigurationStringConst.UPPER_LIMIT;
                    break;
                case 0x8000:
                    box.BackColor = Color.Yellow;
                    box.Text = ConfigurationStringConst.LOWER_LIMIT;
                    break;
                case 0x9000:
                    box.BackColor = Color.LightGreen;
                    box.Text = ConfigurationStringConst.CALCULATION;
                    break;
                default:
                    box.BackColor = Color.IndianRed;
                    box.Text = string.Format("0x{0:X}", value);
                    break;
            }
        }

        private void MeasuringReadFail()
        {
            this.channel1TextBox.Text = ERROR_VALUE;
            this.channel2TextBox.Text = ERROR_VALUE;
            this.channel3TextBox.Text = ERROR_VALUE;
            this.channel4TextBox.Text = ERROR_VALUE;
            this._resist1.Text = ERROR_VALUE;
            this._resist2.Text = ERROR_VALUE;
            this._c1.Text = ERROR_VALUE;
            this._plus.ForeColor = SystemColors.ControlDark;
            this._minus.ForeColor = SystemColors.ControlDark;
            this._plus.BackColor = SystemColors.Control;
            this._minus.BackColor = SystemColors.Control;
        }

        private void SignatureReadFail()
        {
            this._countOfDiscrets = 4;
            this._countOfRelays = 3;
            this._countOfDiods = 2;
            this.InitControls();
            this._ledStruct.LoadStructCycle();
            this._analogConfig.LoadStructCycle();
            this._device.ModeMeasuring.LoadStructCycle();
        }

        private void SignatureReadOk()
        {
            byte[] buf = Common.TOBYTES(this._signature.Values, true);
            if (!CRC16.VerifyRespCrc(buf))
            {
                MessageBox.Show("Значение CRC не совпадает", "Ошибка CRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Common.SwapArrayItems(ref buf);
            if (!this.VerifyConfigVersion(buf))
            {
                MessageBox.Show("В устройстве записана конфигурация версии не 1.0", "Ошибка версии конфигурации",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            byte sm1 = buf[16];
            byte sm2 = buf[17];
            byte sm3 = buf[18];
            this._countOfDiscrets = buf[sm1];
            this._countOfRelays = buf[sm2];
            this._countOfDiods = buf[sm3];
            this.InitControls();
            this._ledStruct.LoadStructCycle();
            this._analogConfig.LoadStructCycle();
            this._device.ModeMeasuring.LoadStructCycle();
        }

        private void LedStateReadOk()
        {
            LedManager.SetLeds(this._discretLeds, this._ledStruct.Value.Discrets);
            LedManager.SetLeds(this._relayLeds, this._ledStruct.Value.Relay);
            LedManager.SetLeds(this._diodLeds, this._ledStruct.Value.Diods);
        }

        private void LedStateReadFail()
        {
            LedManager.TurnOffLeds(this._discretLeds);
            LedManager.TurnOffLeds(this._relayLeds);
            LedManager.TurnOffLeds(this._diodLeds);
        }

        private void InitControls()
        {
            GroupBox discretsGroup, relayGroup, diodGroup;
            this.InitGroupBox(out discretsGroup, this._countOfDiscrets, 15, 0, "Дискреты");
            this.InitGroupBox(out relayGroup, this._countOfRelays, discretsGroup.Top, discretsGroup.Height, "Реле");

            this.InitLeds(ref this._discretLeds, this._discretsLabels, discretsGroup, this._countOfDiscrets, "Д{0}");
            this.InitLeds(ref this._relayLeds, this._relayLabels, relayGroup, this._countOfRelays, "Р{0}", true);
            foreach (var led in this._relayLeds)
            {
                led.LedClicked += this.RelayLedMouseClick;
            }
            if (this._countOfDiods == 0) return;
            this.InitGroupBox(out diodGroup, this._countOfDiods, relayGroup.Top, relayGroup.Height, "Светодиоды");
            this.InitLeds(ref this._diodLeds, this._diodsLabel, diodGroup, this._countOfDiods, "C{0}");
            foreach (var led in this._diodLeds)
            {
                led.LedClicked += this.DiodLedMouseClick;
            }
        }

        private void InitGroupBox(out GroupBox box, int count, int preY, int preHeigth, string title)
        {
            box = new GroupBox();
            box.Height = count > 16 ? 85 : 55;
            box.Width = count > 16 ? 405 : ((2 * count - 1) * 13 + 12);
            if (box.Width < 83)
                box.Width = 83;
            box.Location = new Point(9, preY + preHeigth);
            box.Text = title;
            this._inOutGroupBox.Controls.Add(box);
            this._inOutGroupBox.Update();
        }

        private void InitLeds(ref LedControl[] leds, Label[] labels, GroupBox group, int count, string s, bool relay = false)
        {
            leds = new LedControl[count];
            labels = new Label[count];
            for (int i = 0; i < leds.Length; i++)
            {
                if (i < 16)
                {
                    leds[i] = new LedControl { Location = new Point(6 + i * 25, 19) };
                    labels[i] = new Label { Location = new Point(2 + i * 25, 35), Width = i < 9 ? 20 : 26, Height = 15 };
                }
                else
                {
                    leds[i] = new LedControl { Location = new Point(6 + (i - 16) * 25, 51) };
                    labels[i] = new Label { Location = new Point(2 + (i - 16) * 25, 67), Width = 26 };
                }
                labels[i].Text = string.Format(s, relay ? i + 2 : i + 1);
                group.Controls.Add(leds[i]);
                group.Controls.Add(labels[i]);
            }
        }

        private bool VerifyConfigVersion(byte[] buf)
        {
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            char[] charBuf = new char[16];
            int byteUsed = 0;
            int charUsed = 0;
            bool complete = false;
            dec.Convert(buf, 0, 16, charBuf, 0, 16, true, out byteUsed, out charUsed, out complete);
            string configStr = new string(charBuf, 0, 13);
            string vers = new string(charBuf, 13, 3);
            if ((configStr.ToUpper() == "CONFIGVERSION") && (vers == "1.0"))
            {
                complete = true;
            }
            else
            {
                complete = false;
            }
            return complete;
        }

        private void Channel5MeasuringForm_Load(object sender, EventArgs e)
        {
            this._signature.LoadStruct();
        }

        private void Channel5MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._analogConfig.RemoveStructQueries();
            this._ledStruct.RemoveStructQueries();
            this._device.ModeMeasuring.RemoveStructQueries();
            this._signature.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, this._signature.LoadStruct);
        }

        private void RelayLedMouseClick(object sender, MouseEventArgs e)
        {
            LedControl led = sender as LedControl;
            this.SetBitState(led, this._relayLeds, 0x00C0, this._ledStruct.Value.Relay, e.Button, false);
        }

        private void DiodLedMouseClick(object sender, MouseEventArgs e)
        {
            LedControl led = sender as LedControl;
            this.SetBitState(led, this._diodLeds, 0x00E0, this._ledStruct.Value.Diods, e.Button, true);
        }

        private void SetBitState(LedControl currentLed, LedControl[] leds, ushort startAddr, BitArray currentState, MouseButtons click, bool relayOrDiod)
        {
            try
            {
                List<LedControl> ledList = new List<LedControl>();
                ledList.AddRange(leds);
                int ledInd = ledList.IndexOf(currentLed);
                bool value = currentLed.State == LedState.Signaled;
                switch (click)
                {
                    //По левой кнопке инвертируем бит
                    case MouseButtons.Left:
                        this._device.SetBit(this._device.DeviceNumber, (ushort)(startAddr + ledInd), value,
                                "SetRelayValue" + this._device.DeviceNumber, this._device);
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        this._contextMenu.Items.Clear();

                        BitInfo info;
                        info.BitIndex = (ushort)ledInd;
                        info.BitValue = value;
                        info.BitStartAddress = startAddr;
                        string contextItem;
                        if (relayOrDiod)
                        {
                            contextItem = string.Format(currentState[ledInd]
                               ? "Выключить светодиод № - {0}"
                               : "Включить светодиод № - {0}", ledInd + 1);
                        }
                        else
                        {
                            contextItem = string.Format(currentState[ledInd]
                                ? "Выключить реле № - {0}"
                                : "Включить реле № - {0}", ledInd + 1);
                        }
                        this._contextMenu.Items.Add(contextItem);

                        this._contextMenu.Items[0].Tag = info;
                        this._contextMenu.ItemClicked += this._contextMenu_ItemClicked;
                        this._contextMenu.Show(MousePosition.X, MousePosition.Y);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Невозможно изменить состояние реле", "Ошибка");
            }
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            BitInfo info = (BitInfo)e.ClickedItem.Tag;

            this._device.SetBit(this._device.DeviceNumber, (ushort)(info.BitStartAddress + info.BitIndex), info.BitValue,
                "SetRelayValue" + this._device.DeviceNumber, this._device);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MII5CHDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MII5CHMeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
