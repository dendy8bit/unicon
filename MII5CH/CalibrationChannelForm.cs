﻿using BEMN.ChannelFive;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MII5CH.Structures;
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using System.Xml;

namespace BEMN.MII5CH
{
    public partial class CalibrationChannelForm : Form, IFormView
    {
        #region Private Fields

        private MII5CHDevice _device;
        private AnalogExtConfig _currentConfigStruct;
        private MemoryEntity<AllAnalogConfig> _allAnalogConfigStruct;
        private MemoryEntity<OneWordStruct> _valueChannelWord;
        private MemoryEntity<AnalogExtConfig> _currentAnalogConfigMem;
        private MemoryEntity<OneWordStruct> _modeWord;
        private NewStructValidator<MemConfigConnection> _configUsbValidator;
        private NewStructValidator<MemConfigConnection> _configRs485Validator;
        private NewStructValidator<AnalogExtConfig> _analogConfigValidator;
        private NewStructValidator<AddSetpointStruct> _setpointValidator;
        private NewStructValidator<AnalogExtConfig> _analogConfigValidator1;
        private NewStructValidator<AnalogExtConfig> _analogConfigValidator2;
        private NewStructValidator<AnalogExtConfig> _analogConfigValidator3;
        private NewStructValidator<AnalogExtConfig> _analogConfigValidator4;


        #endregion

        #region Constants

        private const string PASS = "1111";
        private const string INCORRECT_PASSWORD = "Неверный пароль";
        private const string XML_HEAD = "MII_SET_POINTS";

        private enum Mode
        {
            Waiting = 0,
            OnlyPlusOn = 1,
            OnlyMinusOn = 2,
            VariableOn = 3,
            Connection = 4,
            NotConnection = 5,
            Calibrate = 65525
        }

        private Mode _mode;
        private ushort _currentModeVal;
        private int _step;
        private int _counter;
        private int _selectedChannel;
        private double _temp;
        private bool _stateCalibration;

        #endregion

        #region C'tors

        public CalibrationChannelForm()
        {
            InitializeComponent();
        }

        public CalibrationChannelForm(MII5CHDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this.InitMemoryEntities();
            this.InitValidators();

        }
        
        private void InitValidators()
        {
            ToolTip toolTip = new ToolTip();
            
            this._analogConfigValidator = new NewStructValidator<AnalogExtConfig>(
                toolTip,
                new ControlInfoText(this._koeffB, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffA, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffpA, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._rangeOfScale, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));

            this._analogConfigValidator1 = new NewStructValidator<AnalogExtConfig>(
                toolTip,
                new ControlInfoText(this._koeffB1, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffA1, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffpA1, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._rangeOfScale1, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));

            this._analogConfigValidator2 = new NewStructValidator<AnalogExtConfig>(
                toolTip,
                new ControlInfoText(this._koeffB2, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffA2, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffpA2, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._rangeOfScale2, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));

            this._analogConfigValidator3 = new NewStructValidator<AnalogExtConfig>(
                toolTip,
                new ControlInfoText(this._koeffB3, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffA3, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffpA3, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._rangeOfScale3, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));

            this._analogConfigValidator4 = new NewStructValidator<AnalogExtConfig>(
                toolTip,
                new ControlInfoText(this._koeffB4, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffA4, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._koeffpA4, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._rangeOfScale4, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));


        }

        private void InitMemoryEntities()
        {
            this.InitAnalogMemEntity(this._device.CalibrovkaAnalog1, 1);
            this.InitAnalogMemEntity(this._device.CalibrovkaAnalog2, 2);
            this.InitAnalogMemEntity(this._device.CalibrovkaAnalog3, 3);
            this.InitAnalogMemEntity(this._device.CalibrovkaAnalog4, 4);
            this.InitAnalogMemEntity(this._device.CalibrovkaAnalog5, 5);
            this._modeWord = this._device.Mode;
            this._modeWord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SwitchRadioButton);
            this._modeWord.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._modeWord.LoadStruct);
            this._modeWord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.READ_WRITE_MOD_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });
            this._modeWord.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusLabel.Text = ConfigurationStringConst.READ_WRITE_MOD_FAIL;
                this._statusLabel.BackColor = Color.IndianRed;
            });

            this._valueChannelWord = this._device.ValueWord;
            this._valueChannelWord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringKoefficient);
        }

        #endregion

        #region Event

        private void CalibrationChannelForm_Load(object sender, EventArgs e)
        {
            this._modeWord.LoadStruct();
            this._mode = Mode.NotConnection;
            this._selectedChannel = -1;
        }

        private void RadioButtonChanged(object o, EventArgs e)
        {
            RadioButton r = o as RadioButton;
            if (r == null || !r.Checked) return;

            Mode mode = Mode.NotConnection;

            if (r == this.radioButton1)
            {
                mode = Mode.Waiting;
            }
            if (r == this.radioButton2)
            {
                mode = Mode.OnlyPlusOn;
            }
            if (r == this.radioButton3)
            {
                mode = Mode.OnlyMinusOn;
            }
            if (r == this.radioButton4)
            {
                mode = Mode.VariableOn;
            }
            if (r == this.radioButton5)
            {
                mode = Mode.Connection;
            }
            if (r == this.radioButton6)
            {
                mode = Mode.NotConnection;
            }
            if (r == this.radioButton7)
            {
                mode = Mode.Calibrate;
            }
            if (mode != this._mode) this.SetMode(mode);
        }

        private void SetMode(Mode mode)
        {
            switch (mode)
            {
                case Mode.Waiting:
                    this._currentModeVal = 0x0000;
                    this._modeWord.Value.Word = 0x0000;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.OnlyPlusOn:
                    this._currentModeVal = 0x0001;
                    this._modeWord.Value.Word = 0x0001;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.OnlyMinusOn:
                    this._currentModeVal = 0x0002;
                    this._modeWord.Value.Word = 0x0002;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.VariableOn:
                    this._currentModeVal = 0x0003;
                    this._modeWord.Value.Word = 0x0003;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.Connection:
                    this._currentModeVal = 0x0004;
                    this._modeWord.Value.Word = 0x0004;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.NotConnection:
                    this._currentModeVal = 0x0005;
                    this._modeWord.Value.Word = 0x0005;
                    this._modeWord.SaveStruct();
                    break;
                case Mode.Calibrate:
                    this._currentModeVal = 0xFFFF;
                    this._modeWord.Value.Word = 0xFFFF;
                    this._modeWord.SaveStruct();
                    break;
            }
        }

        private void _validatePassButton_Click(object sender, EventArgs e)
        {
            this.PassCheck();
        }
        

        private void _readChannelConfigBtn_Click(object sender, EventArgs e)
        {
            this.ReadChannel();
        }

        private void _writeChannelConfigBtn_Click(object sender, EventArgs e)
        {
            this._statusLabel.Text = String.Empty;
            try
            {
                string s;
                if (this._analogConfigValidator.Check(out s, true) && this._analogConfigValidator1.Check(out s, true) && this._analogConfigValidator2.Check(out s, true) && this._analogConfigValidator3.Check(out s, true) && this._analogConfigValidator4.Check(out s, true))
                {
                    this._currentAnalogConfigMem.Value = this._analogConfigValidator.Get();
                    this._currentAnalogConfigMem.Value = this._analogConfigValidator1.Get();
                    this._currentAnalogConfigMem.Value = this._analogConfigValidator2.Get();
                    this._currentAnalogConfigMem.Value = this._analogConfigValidator3.Get();
                    this._currentAnalogConfigMem.Value = this._analogConfigValidator4.Get();
                    this._currentAnalogConfigMem.SaveStruct();
                }
                else throw new Exception("Invalid analog data");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void _continueBtn_Click(object sender, EventArgs e)
        {
            this._statusLabel.Text = String.Empty;

            if (this._currentConfigStruct == null)
            {
                MessageBox.Show("Конфигурация аналогового канала не прочитана. Калибровка невозможна!", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //this._progressBar.Value = 0;

            switch (this._step)
            {
                case 0:
                    this._step++;
                    this.SaveDefaultParameters();
                    this._statusStep.Text = string.Empty;
                    this.radioButton5.Enabled = this.radioButton6.Enabled = this.radioButton7.Enabled = false;
                    this._koeffA.Enabled = this._koeffB.Enabled = this._koeffpA.Enabled = this._rangeOfScale.Enabled = this._Uin.Enabled = false;
                    this._continueBtn.Enabled = this._readChannelConfigBtn.Enabled = this._writeChannelConfigBtn.Enabled = false;
                    break;
                case 1:
                    this._step++;
                    this._continueBtn.Enabled = false;
                    this._statusStep.Text = ConfigurationStringConst.B_SAVING;
                    this._counter = 0;
                    this._temp = 0;
                    this._valueChannelWord.LoadStructCycle();
                    break;
                case 2:
                    string str;
                    if (!this.CheckUin() || !this._analogConfigValidator.Check(out str, true) && !this._analogConfigValidator1.Check(out str, true) && !this._analogConfigValidator2.Check(out str, true) && !this._analogConfigValidator3.Check(out str, true) && !this._analogConfigValidator4.Check(out str, true)) return;
                    this._step++;
                    this._continueBtn.Enabled = false;
                    this._statusStep.Text = ConfigurationStringConst.A_MEASSURING;
                    this._counter = 0;
                    this._temp = 0;
                    this._valueChannelWord.LoadStructCycle();
                    this._statusLabel.Text = String.Empty;
                    break;
                case 3:
                    this._step = 0;
                    this._modeWord.Value.Word = this._currentModeVal;
                    this._modeWord.SaveStruct();
                    this.radioButton5.Enabled = this.radioButton6.Enabled = this.radioButton7.Enabled = true;
                    this._koeffA.Enabled = this._koeffB.Enabled = this._koeffpA.Enabled = this._rangeOfScale.Enabled = this._Uin.Enabled = true;
                    this._readChannelConfigBtn.Enabled = this._writeChannelConfigBtn.Enabled = true;
                    this._descriptionBox.Text = ConfigurationStringConst.Info0;
                    this._continueBtn.Text = ConfigurationStringConst.CONTINUE;
                    break;
            }

        }

        private void _channelCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            var box = sender as ComboBox;
            if (box == null) return;
            if (this._selectedChannel == box.SelectedIndex) return;
            if (this._selectedChannel != box.SelectedIndex && this._step != 0)
            {
                var res = MessageBox.Show(ConfigurationStringConst.CALIBRATE_IS_NOT_COMPLETE, "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.No)
                {
                    this._channelCombo.SelectedIndex = this._selectedChannel;
                    return;
                }
                else
                {
                    this._modeWord.Value.Word = this._currentModeVal;
                    this._modeWord.SaveStruct();
                    this._currentAnalogConfigMem.Value = this._currentConfigStruct;
                    this._currentAnalogConfigMem.SaveStruct();
                }
            }

            this._selectedChannel = box.SelectedIndex;
            if (this._selectedChannel == -1) return;
            this._readChannelConfigBtn.Enabled = this._writeChannelConfigBtn.Enabled = this._continueBtn.Enabled = true;
            this._step = 0;
            switch (this._selectedChannel)
            {
                case 0:
                    this._currentAnalogConfigMem = this._device.CalibrovkaAnalog1;
                    this._valueChannelWord.StartAddress = 0x0000;
                    break;
                case 1:
                    this._currentAnalogConfigMem = this._device.CalibrovkaAnalog2;
                    this._valueChannelWord.StartAddress = 0x0001;
                    break;
                case 2:
                    this._currentAnalogConfigMem = this._device.CalibrovkaAnalog3;
                    this._valueChannelWord.StartAddress = 0x0002;
                    break;
                case 3:
                    this._currentAnalogConfigMem = this._device.CalibrovkaAnalog4;
                    this._valueChannelWord.StartAddress = 0x0003;
                    break;
                case 4:
                    this._currentAnalogConfigMem = this._device.CalibrovkaAnalog5;
                    this._valueChannelWord.StartAddress = 0x0004;
                    break;
            }
            this._currentAnalogConfigMem.LoadStruct();
        }

        private void CalibrationChannelForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._valueChannelWord.RemoveStructQueries();
            if (this._step > 0)
            {
                this._modeWord.Value.Word = this._currentModeVal;
                this._modeWord.SaveStruct();
                this._currentAnalogConfigMem.Value = this._currentConfigStruct;
                this._currentAnalogConfigMem.SaveStruct();
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        #endregion

        #region Help members
        private void MeasuringKoefficient()
        {
            if (this._counter != 100)
            {
                this._temp += this._valueChannelWord.Value.Word;
                this._counter++;
            }
            else
            {
                this._valueChannelWord.RemoveStructQueries();
                this._temp = this._temp / 100;
                switch (this._step)
                {
                    case 2:
                        this.SaveNewKoefB();
                        break;
                    case 3:
                        this.MeasureAndSaveKoefA();
                        break;
                }
            }
        }

        /*
        private void ChannelReadOk()
        {
            this._progressBar.Value = this._progressBar.Maximum;
            this._currentConfigStruct = this._currentAnalogConfigMem.Value;
            this._statusLabel.Text = "Канал прочитан";
        }

        private void ChannelReadFail()
        {
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = "Ошибка чтения канала";
        }
        */

        private void SaveNewKoefB()
        {
            this._koeffB.Text = ((ushort)this._temp).ToString("D");
            this._currentAnalogConfigMem.Value = this._analogConfigValidator.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator1.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator2.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator3.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator4.Get();
            this._currentAnalogConfigMem.SaveStruct();
        }

        private void MeasureAndSaveKoefA()
        {
            var num = 0x8000 * double.Parse(this._Uin.Text) / (this._temp - this._currentAnalogConfigMem.Value.B) / double.Parse(this._rangeOfScale.Text);
            if (num < 0)
            {
                this._descriptionBox.Text = ConfigurationStringConst.MES_ERROR_MEASURE_KOEF_A;
                this._statusStep.Text = ConfigurationStringConst.ERR_KOEF_A;
                this._step--;
                return;
            }
            ushort _pA = 0;
            while (num > 1)
            {
                num /= 2;
                _pA++;
            }
            this._koeffA.Text = ((ushort)(num * 0xFFFF)).ToString("D");
            this._koeffpA.Text = _pA.ToString("D");
            string str;
            if (this._analogConfigValidator.Check(out str, true) || this._analogConfigValidator1.Check(out str, true) && this._analogConfigValidator2.Check(out str, true) && this._analogConfigValidator3.Check(out str, true) && this._analogConfigValidator4.Check(out str, true))
            {
                this._currentAnalogConfigMem.Value = this._analogConfigValidator.Get();
                this._currentAnalogConfigMem.Value = this._analogConfigValidator1.Get();
                this._currentAnalogConfigMem.Value = this._analogConfigValidator2.Get();
                this._currentAnalogConfigMem.Value = this._analogConfigValidator3.Get();
                this._currentAnalogConfigMem.Value = this._analogConfigValidator4.Get();
                this._currentAnalogConfigMem.SaveStruct();
            }
        }

        private void SwitchRadioButton()
        {
            Mode mode = (Mode)this._modeWord.Value.Word;
            switch (mode)
            {
                case Mode.Waiting:
                    this._mode = mode;
                    this.radioButton1.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.OnlyPlusOn:
                    this._mode = mode;
                    this.radioButton2.Checked = true;
                    this.radioButton1.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.OnlyMinusOn:
                    this._mode = mode;
                    this.radioButton3.Checked = true;
                    this.radioButton2.Checked = this.radioButton1.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.VariableOn:
                    this._mode = mode;
                    this.radioButton4.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton1.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.Connection:
                    this._mode = mode;
                    this.radioButton5.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton1.Checked = this.radioButton6.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.NotConnection:
                    this._mode = mode;
                    this.radioButton6.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton1.Checked = this.radioButton7.Checked = false;
                    break;
                case Mode.Calibrate:
                    this._mode = mode;
                    this.radioButton7.Checked = true;
                    this.radioButton2.Checked = this.radioButton3.Checked = this.radioButton4.Checked =
                        this.radioButton5.Checked = this.radioButton6.Checked = this.radioButton1.Checked = false;
                    break;
            }
        }

        private void InitAnalogMemEntity(MemoryEntity<AnalogExtConfig> analogConfig, int ind)
        {
            analogConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogConfigReadOk);
            analogConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusStep.Text = string.Format(ConfigurationStringConst.READ_ANALOG_CONFIG_FAIL, ind);
                this._statusStep.BackColor = Color.IndianRed;
            });
            analogConfig.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusStep.Text = string.Format(ConfigurationStringConst.WRITE_ANALOG_CONFIG_OK, ind);
                this._statusStep.BackColor = Color.LightGreen;
                this._currentAnalogConfigMem.LoadStruct();
            });
            analogConfig.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._statusStep.Text = string.Format(ConfigurationStringConst.WRITE_ANALOG_CONFIG_FAIL, ind);
                this._statusStep.BackColor = Color.IndianRed;
            });
        }

        private void SaveDefaultParameters()
        {
            this._currentModeVal = this._modeWord.Value.Word;
            this._modeWord.Value.Word = 0xFFFF;
            this._modeWord.SaveStruct();
            this._koeffB.Text = Convert.ToString(0x0000);
            this._koeffA.Text = Convert.ToString(0x7FFF);
            this._koeffpA.Text = Convert.ToString(0x0000);
            this._rangeOfScale.Text = Convert.ToString(this._currentConfigStruct.RangeOfScale);
            this._koeffB1.Text = Convert.ToString(0x0000);
            this._koeffA1.Text = Convert.ToString(0x7FFF);
            this._koeffpA1.Text = Convert.ToString(0x0000);
            this._rangeOfScale1.Text = Convert.ToString(this._currentConfigStruct.RangeOfScale);
            this._koeffB2.Text = Convert.ToString(0x0000);
            this._koeffA2.Text = Convert.ToString(0x7FFF);
            this._koeffpA2.Text = Convert.ToString(0x0000);
            this._rangeOfScale2.Text = Convert.ToString(this._currentConfigStruct.RangeOfScale);
            this._koeffB3.Text = Convert.ToString(0x0000);
            this._koeffA3.Text = Convert.ToString(0x7FFF);
            this._koeffpA3.Text = Convert.ToString(0x0000);
            this._rangeOfScale3.Text = Convert.ToString(this._currentConfigStruct.RangeOfScale);
            this._koeffB4.Text = Convert.ToString(0x0000);
            this._koeffA4.Text = Convert.ToString(0x7FFF);
            this._koeffpA4.Text = Convert.ToString(0x0000);
            this._rangeOfScale4.Text = Convert.ToString(this._currentConfigStruct.RangeOfScale);
            this._currentAnalogConfigMem.Value = this._analogConfigValidator.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator1.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator2.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator3.Get();
            this._currentAnalogConfigMem.Value = this._analogConfigValidator4.Get();
            this._currentAnalogConfigMem.SaveStruct();
        }

        private bool CheckUin()
        {
            try
            {
                double uin;
                ushort limit;
                bool res = ushort.TryParse(this._rangeOfScale.Text, out limit);
                res &= double.TryParse(this._Uin.Text.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator)
                    .Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator), out uin);
                if (uin > limit || !res) throw new Exception("Проверьте правильность ввода диапазона шкалы и входного сигнала и нажмите кнопку \"Продолжить\"!");
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        #region Save/Load File

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentConfigStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка закрузки файла");
            }
        }

        private void SaveInFile()
        {
            if (this._analogConfigValidator.Check() && this._analogConfigValidator1.Check() && this._analogConfigValidator2.Check() && this._analogConfigValidator3.Check() && this._analogConfigValidator4.Check())
            {
                this._statusLabel.Text = "Идет запись конфигурации...";
                this._saveConfigurationDlg.FileName =
                    string.Format("{0}", this._device.DeviceVersion);
                if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog())
                {
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }

        public void Serialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MII"));

                var values = this._currentConfigStruct.GetValues();

                XmlElement element = doc.CreateElement(string.Format(XML_HEAD));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка сохранения файла");
                this._statusLabel.Text = "Ошибка сохранения файла...";
            }
        }

        private void ShowConfiguration()
        {
            this._currentAnalogConfigMem.Value = this._currentConfigStruct;
            this._analogConfigValidator.Set(this._currentConfigStruct);
            this._analogConfigValidator1.Set(this._currentConfigStruct);
            this._analogConfigValidator2.Set(this._currentConfigStruct);
            this._analogConfigValidator3.Set(this._currentConfigStruct);
            this._analogConfigValidator4.Set(this._currentConfigStruct);
        }

        #endregion
        
        private void PassCheck()
        {
            if (this._passTB.Text == "")
            {
                return;
            }
            else if (this._passTB.Text == PASS)
            {
                this._passTB.Text = string.Empty;
                this.groupBox6.Enabled = true;
                this._loadConfigBut.Enabled = true;
                this._saveConfigBut.Enabled = true;
                this._passInsertBox.Enabled = false;
                this._channelCombo.Enabled = true;
                this._statusLabel.Text = "Форма загружена успешно";
                this._statusLabel.BackColor = Color.LightGreen;
                this._progressBar.Value = 100;
            }
            else
            {
                MessageBox.Show(INCORRECT_PASSWORD);
                this._passTB.Text = string.Empty;
            }
        }


        private void ReadChannel()
        {
            this._statusStep.Text = "Идет чтение канала...";
            this._statusLabel.Text = String.Empty;
            if (this._currentConfigStruct == null)
            {
                this._statusStep.Text = "Конфигурация аналогового канала не прочитана";
                this._statusStep.BackColor = Color.IndianRed;
                return;
            }
            else
            {
                this._statusStep.Text = string.Format(ConfigurationStringConst.READ_ANALOG_CONFIG_OK, _channelCombo.SelectedIndex + 1);
                this._statusStep.BackColor = Color.LightGreen;

                this._currentAnalogConfigMem.LoadStruct();
            }
        }
        
        private void AnalogConfigReadOk()
        {
            this._analogConfigValidator.Set(this._currentAnalogConfigMem.Value);
            this._analogConfigValidator1.Set(this._currentAnalogConfigMem.Value);
            this._analogConfigValidator2.Set(this._currentAnalogConfigMem.Value);
            this._analogConfigValidator3.Set(this._currentAnalogConfigMem.Value);
            this._analogConfigValidator4.Set(this._currentAnalogConfigMem.Value);
            switch (this._step)
            {
                case 0:
                    this._currentConfigStruct = this._currentAnalogConfigMem.Value;
                    break;
                case 1:
                    this._progressBar.Value = 0;
                    this._continueBtn.Enabled = true;
                    this._statusStep.Text = ConfigurationStringConst.DEFUULT_CONFIG_SAVE_OK;
                    this._descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, this._step);
                    this._descriptionBox.Text = this._selectedChannel != 3 ? ConfigurationStringConst.Info1 : ConfigurationStringConst.Info1Channel4;
                    this._progressBar.Increment(33);
                    break;
                case 2:
                    this._continueBtn.Enabled = true;
                    this._statusStep.Text = ConfigurationStringConst.B_SAVED;
                    this._descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, this._step);
                    this._descriptionBox.Text = this._selectedChannel != 3 ? ConfigurationStringConst.Info2 : ConfigurationStringConst.Info1Channel4;
                    this._rangeOfScale.Enabled = this._Uin.Enabled = true;
                    this._progressBar.Increment(33);
                    break;
                case 3:
                    this._progressBar.Value = 100;
                    this._continueBtn.Enabled = true;
                    this._statusStep.Text = ConfigurationStringConst.COMPLETE;
                    this._descriptionGroup.Text = string.Format(ConfigurationStringConst.STEP, this._step);
                    this._descriptionBox.Text = ConfigurationStringConst.Info3;
                    if (this._currentAnalogConfigMem == this._device.CalibrovkaAnalog1 || this._currentAnalogConfigMem == this._device.CalibrovkaAnalog2)
                        this._descriptionBox.Text += ConfigurationStringConst.InfoDop;
                    this._rangeOfScale.Enabled = this._Uin.Enabled = this._koeffA.Enabled = this._koeffB.Enabled = this._koeffpA.Enabled = true;
                    this._continueBtn.Text = ConfigurationStringConst.BREAK;
                    MessageBox.Show("Калибровка завершена успешно!");
                    this._stateCalibration = true;
                    /*
                    var _channelState = _channelCombo.SelectedIndex;

                    if (this._stateCalibration == true && _channelState == _channelCombo.SelectedIndex)
                    {
                        this._koeffA.Enabled = this._koeffB.Enabled = this._koeffpA.Enabled = this._rangeOfScale.Enabled = this._Uin.Enabled = false;
                    }
                    */
                    break;
            }
            
        }


        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MII5CHDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(CalibrationChannelForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Калибровка каналов"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

    }
}
