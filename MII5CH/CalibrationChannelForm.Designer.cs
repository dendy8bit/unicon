﻿namespace BEMN.MII5CH
{
    partial class CalibrationChannelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalibrationChannelForm));
            this._descriptionGroup = new System.Windows.Forms.GroupBox();
            this._descriptionBox = new BEMN.Forms.RichTextBox.AdvRichTextBox();
            this._continueBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._channelCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._readChannelConfigBtn = new System.Windows.Forms.Button();
            this._writeChannelConfigBtn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._Uin = new System.Windows.Forms.MaskedTextBox();
            this._rangeOfScale = new System.Windows.Forms.MaskedTextBox();
            this._koeffpA = new System.Windows.Forms.MaskedTextBox();
            this._koeffA = new System.Windows.Forms.MaskedTextBox();
            this._koeffB = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._passInsertBox = new System.Windows.Forms.GroupBox();
            this._validatePassButton = new System.Windows.Forms.Button();
            this._passTB = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this._statusStep = new System.Windows.Forms.Label();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._channelControl = new System.Windows.Forms.TabControl();
            this._channelTab1 = new System.Windows.Forms.TabPage();
            this._channelTab2 = new System.Windows.Forms.TabPage();
            this._channelTab3 = new System.Windows.Forms.TabPage();
            this._channelTab4 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._Uin1 = new System.Windows.Forms.MaskedTextBox();
            this._rangeOfScale1 = new System.Windows.Forms.MaskedTextBox();
            this._koeffpA1 = new System.Windows.Forms.MaskedTextBox();
            this._koeffA1 = new System.Windows.Forms.MaskedTextBox();
            this._koeffB1 = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._Uin2 = new System.Windows.Forms.MaskedTextBox();
            this._rangeOfScale2 = new System.Windows.Forms.MaskedTextBox();
            this._koeffpA2 = new System.Windows.Forms.MaskedTextBox();
            this._koeffA2 = new System.Windows.Forms.MaskedTextBox();
            this._koeffB2 = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this._Uin3 = new System.Windows.Forms.MaskedTextBox();
            this._rangeOfScale3 = new System.Windows.Forms.MaskedTextBox();
            this._koeffpA3 = new System.Windows.Forms.MaskedTextBox();
            this._koeffA3 = new System.Windows.Forms.MaskedTextBox();
            this._koeffB3 = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this._Uin4 = new System.Windows.Forms.MaskedTextBox();
            this._rangeOfScale4 = new System.Windows.Forms.MaskedTextBox();
            this._koeffpA4 = new System.Windows.Forms.MaskedTextBox();
            this._koeffA4 = new System.Windows.Forms.MaskedTextBox();
            this._koeffB4 = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._descriptionGroup.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._passInsertBox.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this._channelControl.SuspendLayout();
            this._channelTab1.SuspendLayout();
            this._channelTab2.SuspendLayout();
            this._channelTab3.SuspendLayout();
            this._channelTab4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // _descriptionGroup
            // 
            this._descriptionGroup.Controls.Add(this._descriptionBox);
            this._descriptionGroup.Location = new System.Drawing.Point(190, 12);
            this._descriptionGroup.Name = "_descriptionGroup";
            this._descriptionGroup.Size = new System.Drawing.Size(224, 207);
            this._descriptionGroup.TabIndex = 52;
            this._descriptionGroup.TabStop = false;
            // 
            // _descriptionBox
            // 
            this._descriptionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._descriptionBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._descriptionBox.Location = new System.Drawing.Point(3, 16);
            this._descriptionBox.Name = "_descriptionBox";
            this._descriptionBox.ReadOnly = true;
            this._descriptionBox.SelectionAlignment = BEMN.Forms.RichTextBox.TextAlign.Justify;
            this._descriptionBox.Size = new System.Drawing.Size(218, 188);
            this._descriptionBox.TabIndex = 10;
            this._descriptionBox.Text = resources.GetString("_descriptionBox.Text");
            // 
            // _continueBtn
            // 
            this._continueBtn.Enabled = false;
            this._continueBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._continueBtn.Location = new System.Drawing.Point(193, 225);
            this._continueBtn.Name = "_continueBtn";
            this._continueBtn.Size = new System.Drawing.Size(221, 23);
            this._continueBtn.TabIndex = 51;
            this._continueBtn.Text = "Продолжить";
            this._continueBtn.UseVisualStyleBackColor = true;
            this._continueBtn.Click += new System.EventHandler(this._continueBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._channelCombo);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this._readChannelConfigBtn);
            this.groupBox4.Controls.Add(this._writeChannelConfigBtn);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(175, 106);
            this.groupBox4.TabIndex = 53;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Выбор аналогового канала";
            // 
            // _channelCombo
            // 
            this._channelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._channelCombo.Enabled = false;
            this._channelCombo.FormattingEnabled = true;
            this._channelCombo.Items.AddRange(new object[] {
            "Канал 1",
            "Канал 2",
            "Канал 3",
            "Канал 4"});
            this._channelCombo.Location = new System.Drawing.Point(93, 19);
            this._channelCombo.Name = "_channelCombo";
            this._channelCombo.Size = new System.Drawing.Size(76, 21);
            this._channelCombo.TabIndex = 1;
            this._channelCombo.SelectedIndexChanged += new System.EventHandler(this._channelCombo_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Канал";
            // 
            // _readChannelConfigBtn
            // 
            this._readChannelConfigBtn.Enabled = false;
            this._readChannelConfigBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readChannelConfigBtn.Location = new System.Drawing.Point(9, 46);
            this._readChannelConfigBtn.Name = "_readChannelConfigBtn";
            this._readChannelConfigBtn.Size = new System.Drawing.Size(160, 23);
            this._readChannelConfigBtn.TabIndex = 2;
            this._readChannelConfigBtn.Text = "Прочитать";
            this._readChannelConfigBtn.UseVisualStyleBackColor = true;
            this._readChannelConfigBtn.Click += new System.EventHandler(this._readChannelConfigBtn_Click);
            // 
            // _writeChannelConfigBtn
            // 
            this._writeChannelConfigBtn.Enabled = false;
            this._writeChannelConfigBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._writeChannelConfigBtn.Location = new System.Drawing.Point(9, 75);
            this._writeChannelConfigBtn.Name = "_writeChannelConfigBtn";
            this._writeChannelConfigBtn.Size = new System.Drawing.Size(160, 23);
            this._writeChannelConfigBtn.TabIndex = 3;
            this._writeChannelConfigBtn.Text = "Записать";
            this._writeChannelConfigBtn.UseVisualStyleBackColor = true;
            this._writeChannelConfigBtn.Click += new System.EventHandler(this._writeChannelConfigBtn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this._Uin);
            this.groupBox6.Controls.Add(this._rangeOfScale);
            this.groupBox6.Controls.Add(this._koeffpA);
            this.groupBox6.Controls.Add(this._koeffA);
            this.groupBox6.Controls.Add(this._koeffB);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Enabled = false;
            this.groupBox6.Location = new System.Drawing.Point(12, 124);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(175, 149);
            this.groupBox6.TabIndex = 50;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Конфигурация канала";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Сигнал на входе";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Предел шкалы";
            // 
            // _Uin
            // 
            this._Uin.Location = new System.Drawing.Point(117, 124);
            this._Uin.Name = "_Uin";
            this._Uin.Size = new System.Drawing.Size(52, 20);
            this._Uin.TabIndex = 8;
            this._Uin.Tag = "65535";
            this._Uin.Text = "0";
            // 
            // _rangeOfScale
            // 
            this._rangeOfScale.Location = new System.Drawing.Point(117, 100);
            this._rangeOfScale.Name = "_rangeOfScale";
            this._rangeOfScale.Size = new System.Drawing.Size(52, 20);
            this._rangeOfScale.TabIndex = 7;
            this._rangeOfScale.Tag = "65535";
            this._rangeOfScale.Text = "0";
            // 
            // _koeffpA
            // 
            this._koeffpA.Enabled = false;
            this._koeffpA.Location = new System.Drawing.Point(117, 74);
            this._koeffpA.Name = "_koeffpA";
            this._koeffpA.Size = new System.Drawing.Size(52, 20);
            this._koeffpA.TabIndex = 6;
            this._koeffpA.Tag = "65535";
            this._koeffpA.Text = "0";
            // 
            // _koeffA
            // 
            this._koeffA.Location = new System.Drawing.Point(117, 48);
            this._koeffA.Name = "_koeffA";
            this._koeffA.Size = new System.Drawing.Size(52, 20);
            this._koeffA.TabIndex = 5;
            this._koeffA.Tag = "65535";
            this._koeffA.Text = "0";
            // 
            // _koeffB
            // 
            this._koeffB.Location = new System.Drawing.Point(117, 22);
            this._koeffB.Name = "_koeffB";
            this._koeffB.Size = new System.Drawing.Size(52, 20);
            this._koeffB.TabIndex = 4;
            this._koeffB.Tag = "65535";
            this._koeffB.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Степень pA";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 51);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Коэффициент А";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Коэффициент B";
            // 
            // _passInsertBox
            // 
            this._passInsertBox.Controls.Add(this._validatePassButton);
            this._passInsertBox.Controls.Add(this._passTB);
            this._passInsertBox.Controls.Add(this.label31);
            this._passInsertBox.Location = new System.Drawing.Point(12, 279);
            this._passInsertBox.Name = "_passInsertBox";
            this._passInsertBox.Size = new System.Drawing.Size(241, 61);
            this._passInsertBox.TabIndex = 56;
            this._passInsertBox.TabStop = false;
            this._passInsertBox.Text = "Изменение конфигурации";
            // 
            // _validatePassButton
            // 
            this._validatePassButton.Location = new System.Drawing.Point(131, 24);
            this._validatePassButton.Name = "_validatePassButton";
            this._validatePassButton.Size = new System.Drawing.Size(104, 23);
            this._validatePassButton.TabIndex = 45;
            this._validatePassButton.Text = "Принять";
            this._validatePassButton.UseVisualStyleBackColor = true;
            this._validatePassButton.Click += new System.EventHandler(this._validatePassButton_Click);
            // 
            // _passTB
            // 
            this._passTB.Location = new System.Drawing.Point(57, 26);
            this._passTB.Name = "_passTB";
            this._passTB.PasswordChar = '*';
            this._passTB.Size = new System.Drawing.Size(68, 20);
            this._passTB.TabIndex = 45;
            this._passTB.Text = "1111";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 29);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 13);
            this.label31.TabIndex = 45;
            this.label31.Text = "Пароль";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 372);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(874, 22);
            this.statusStrip1.TabIndex = 57;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _progressBar
            // 
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(80, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioButton3);
            this.groupBox11.Controls.Add(this.radioButton7);
            this.groupBox11.Controls.Add(this.radioButton2);
            this.groupBox11.Controls.Add(this.radioButton6);
            this.groupBox11.Controls.Add(this.radioButton1);
            this.groupBox11.Controls.Add(this.radioButton4);
            this.groupBox11.Controls.Add(this.radioButton5);
            this.groupBox11.Location = new System.Drawing.Point(417, 12);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(227, 207);
            this.groupBox11.TabIndex = 58;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Режимы работы";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 70);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(136, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Tag = "";
            this.radioButton3.Text = "Подключено плечо \"-\"";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(6, 175);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(86, 17);
            this.radioButton7.TabIndex = 7;
            this.radioButton7.TabStop = true;
            this.radioButton7.Tag = "";
            this.radioButton7.Text = "Калибровка";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(139, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Tag = "";
            this.radioButton2.Text = "Подключено плечо \"+\"";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(6, 149);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(140, 17);
            this.radioButton6.TabIndex = 6;
            this.radioButton6.TabStop = true;
            this.radioButton6.Tag = "";
            this.radioButton6.Text = "Без подключения плеч";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(77, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Tag = "";
            this.radioButton1.Text = "Ожидание";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(6, 95);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(203, 17);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Tag = "";
            this.radioButton4.Text = "Включение двух плеч попеременно";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(6, 122);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(209, 17);
            this.radioButton5.TabIndex = 5;
            this.radioButton5.TabStop = true;
            this.radioButton5.Tag = "";
            this.radioButton5.Text = "Включение двух плеч одновременно";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.RadioButtonChanged);
            // 
            // _statusStep
            // 
            this._statusStep.AutoSize = true;
            this._statusStep.Location = new System.Drawing.Point(9, 348);
            this._statusStep.Name = "_statusStep";
            this._statusStep.Size = new System.Drawing.Size(0, 13);
            this._statusStep.TabIndex = 59;
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить калибровку каналов для МИИ";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть калибровку каналов МИИ";
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Enabled = false;
            this._loadConfigBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._loadConfigBut.Location = new System.Drawing.Point(259, 285);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(155, 23);
            this._loadConfigBut.TabIndex = 60;
            this._loadConfigBut.Text = "Загрузить из файла";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Enabled = false;
            this._saveConfigBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._saveConfigBut.Location = new System.Drawing.Point(259, 317);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(155, 23);
            this._saveConfigBut.TabIndex = 61;
            this._saveConfigBut.Text = "Сохранить в файл";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _channelControl
            // 
            this._channelControl.Controls.Add(this._channelTab1);
            this._channelControl.Controls.Add(this._channelTab2);
            this._channelControl.Controls.Add(this._channelTab3);
            this._channelControl.Controls.Add(this._channelTab4);
            this._channelControl.Location = new System.Drawing.Point(650, 15);
            this._channelControl.Name = "_channelControl";
            this._channelControl.SelectedIndex = 0;
            this._channelControl.Size = new System.Drawing.Size(213, 204);
            this._channelControl.TabIndex = 62;
            // 
            // _channelTab1
            // 
            this._channelTab1.BackColor = System.Drawing.SystemColors.Control;
            this._channelTab1.Controls.Add(this.groupBox1);
            this._channelTab1.Location = new System.Drawing.Point(4, 22);
            this._channelTab1.Name = "_channelTab1";
            this._channelTab1.Padding = new System.Windows.Forms.Padding(3);
            this._channelTab1.Size = new System.Drawing.Size(205, 178);
            this._channelTab1.TabIndex = 0;
            this._channelTab1.Text = "Канал 1";
            // 
            // _channelTab2
            // 
            this._channelTab2.BackColor = System.Drawing.SystemColors.Control;
            this._channelTab2.Controls.Add(this.groupBox2);
            this._channelTab2.Location = new System.Drawing.Point(4, 22);
            this._channelTab2.Name = "_channelTab2";
            this._channelTab2.Padding = new System.Windows.Forms.Padding(3);
            this._channelTab2.Size = new System.Drawing.Size(205, 178);
            this._channelTab2.TabIndex = 1;
            this._channelTab2.Text = "Канал 2";
            // 
            // _channelTab3
            // 
            this._channelTab3.BackColor = System.Drawing.SystemColors.Control;
            this._channelTab3.Controls.Add(this.groupBox3);
            this._channelTab3.Location = new System.Drawing.Point(4, 22);
            this._channelTab3.Name = "_channelTab3";
            this._channelTab3.Padding = new System.Windows.Forms.Padding(3);
            this._channelTab3.Size = new System.Drawing.Size(205, 178);
            this._channelTab3.TabIndex = 2;
            this._channelTab3.Text = "Канал 3";
            // 
            // _channelTab4
            // 
            this._channelTab4.BackColor = System.Drawing.SystemColors.Control;
            this._channelTab4.Controls.Add(this.groupBox5);
            this._channelTab4.Location = new System.Drawing.Point(4, 22);
            this._channelTab4.Name = "_channelTab4";
            this._channelTab4.Padding = new System.Windows.Forms.Padding(3);
            this._channelTab4.Size = new System.Drawing.Size(205, 178);
            this._channelTab4.TabIndex = 3;
            this._channelTab4.Text = "Канал 4";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._Uin1);
            this.groupBox1.Controls.Add(this._rangeOfScale1);
            this.groupBox1.Controls.Add(this._koeffpA1);
            this.groupBox1.Controls.Add(this._koeffA1);
            this.groupBox1.Controls.Add(this._koeffB1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(7, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(193, 166);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Конфигурация канала";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Сигнал на входе";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Предел шкалы";
            // 
            // _Uin1
            // 
            this._Uin1.Location = new System.Drawing.Point(117, 124);
            this._Uin1.Name = "_Uin1";
            this._Uin1.Size = new System.Drawing.Size(52, 20);
            this._Uin1.TabIndex = 8;
            this._Uin1.Tag = "65535";
            this._Uin1.Text = "0";
            // 
            // _rangeOfScale1
            // 
            this._rangeOfScale1.Location = new System.Drawing.Point(117, 100);
            this._rangeOfScale1.Name = "_rangeOfScale1";
            this._rangeOfScale1.Size = new System.Drawing.Size(52, 20);
            this._rangeOfScale1.TabIndex = 7;
            this._rangeOfScale1.Tag = "65535";
            this._rangeOfScale1.Text = "0";
            // 
            // _koeffpA1
            // 
            this._koeffpA1.Enabled = false;
            this._koeffpA1.Location = new System.Drawing.Point(117, 74);
            this._koeffpA1.Name = "_koeffpA1";
            this._koeffpA1.Size = new System.Drawing.Size(52, 20);
            this._koeffpA1.TabIndex = 6;
            this._koeffpA1.Tag = "65535";
            this._koeffpA1.Text = "0";
            // 
            // _koeffA1
            // 
            this._koeffA1.Location = new System.Drawing.Point(117, 48);
            this._koeffA1.Name = "_koeffA1";
            this._koeffA1.Size = new System.Drawing.Size(52, 20);
            this._koeffA1.TabIndex = 5;
            this._koeffA1.Tag = "65535";
            this._koeffA1.Text = "0";
            // 
            // _koeffB1
            // 
            this._koeffB1.Location = new System.Drawing.Point(117, 22);
            this._koeffB1.Name = "_koeffB1";
            this._koeffB1.Size = new System.Drawing.Size(52, 20);
            this._koeffB1.TabIndex = 4;
            this._koeffB1.Tag = "65535";
            this._koeffB1.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Степень pA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Коэффициент А";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Коэффициент B";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this._Uin2);
            this.groupBox2.Controls.Add(this._rangeOfScale2);
            this.groupBox2.Controls.Add(this._koeffpA2);
            this.groupBox2.Controls.Add(this._koeffA2);
            this.groupBox2.Controls.Add(this._koeffB2);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(7, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 166);
            this.groupBox2.TabIndex = 52;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Конфигурация канала";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Сигнал на входе";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Предел шкалы";
            // 
            // _Uin2
            // 
            this._Uin2.Location = new System.Drawing.Point(117, 124);
            this._Uin2.Name = "_Uin2";
            this._Uin2.Size = new System.Drawing.Size(52, 20);
            this._Uin2.TabIndex = 8;
            this._Uin2.Tag = "65535";
            this._Uin2.Text = "0";
            // 
            // _rangeOfScale2
            // 
            this._rangeOfScale2.Location = new System.Drawing.Point(117, 100);
            this._rangeOfScale2.Name = "_rangeOfScale2";
            this._rangeOfScale2.Size = new System.Drawing.Size(52, 20);
            this._rangeOfScale2.TabIndex = 7;
            this._rangeOfScale2.Tag = "65535";
            this._rangeOfScale2.Text = "0";
            // 
            // _koeffpA2
            // 
            this._koeffpA2.Enabled = false;
            this._koeffpA2.Location = new System.Drawing.Point(117, 74);
            this._koeffpA2.Name = "_koeffpA2";
            this._koeffpA2.Size = new System.Drawing.Size(52, 20);
            this._koeffpA2.TabIndex = 6;
            this._koeffpA2.Tag = "65535";
            this._koeffpA2.Text = "0";
            // 
            // _koeffA2
            // 
            this._koeffA2.Location = new System.Drawing.Point(117, 48);
            this._koeffA2.Name = "_koeffA2";
            this._koeffA2.Size = new System.Drawing.Size(52, 20);
            this._koeffA2.TabIndex = 5;
            this._koeffA2.Tag = "65535";
            this._koeffA2.Text = "0";
            // 
            // _koeffB2
            // 
            this._koeffB2.Location = new System.Drawing.Point(117, 22);
            this._koeffB2.Name = "_koeffB2";
            this._koeffB2.Size = new System.Drawing.Size(52, 20);
            this._koeffB2.TabIndex = 4;
            this._koeffB2.Tag = "65535";
            this._koeffB2.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Степень pA";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Коэффициент А";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Коэффициент B";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this._Uin3);
            this.groupBox3.Controls.Add(this._rangeOfScale3);
            this.groupBox3.Controls.Add(this._koeffpA3);
            this.groupBox3.Controls.Add(this._koeffA3);
            this.groupBox3.Controls.Add(this._koeffB3);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(7, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 166);
            this.groupBox3.TabIndex = 52;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Конфигурация канала";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Сигнал на входе";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Предел шкалы";
            // 
            // _Uin3
            // 
            this._Uin3.Location = new System.Drawing.Point(117, 124);
            this._Uin3.Name = "_Uin3";
            this._Uin3.Size = new System.Drawing.Size(52, 20);
            this._Uin3.TabIndex = 8;
            this._Uin3.Tag = "65535";
            this._Uin3.Text = "0";
            // 
            // _rangeOfScale3
            // 
            this._rangeOfScale3.Location = new System.Drawing.Point(117, 100);
            this._rangeOfScale3.Name = "_rangeOfScale3";
            this._rangeOfScale3.Size = new System.Drawing.Size(52, 20);
            this._rangeOfScale3.TabIndex = 7;
            this._rangeOfScale3.Tag = "65535";
            this._rangeOfScale3.Text = "0";
            // 
            // _koeffpA3
            // 
            this._koeffpA3.Enabled = false;
            this._koeffpA3.Location = new System.Drawing.Point(117, 74);
            this._koeffpA3.Name = "_koeffpA3";
            this._koeffpA3.Size = new System.Drawing.Size(52, 20);
            this._koeffpA3.TabIndex = 6;
            this._koeffpA3.Tag = "65535";
            this._koeffpA3.Text = "0";
            // 
            // _koeffA3
            // 
            this._koeffA3.Location = new System.Drawing.Point(117, 48);
            this._koeffA3.Name = "_koeffA3";
            this._koeffA3.Size = new System.Drawing.Size(52, 20);
            this._koeffA3.TabIndex = 5;
            this._koeffA3.Tag = "65535";
            this._koeffA3.Text = "0";
            // 
            // _koeffB3
            // 
            this._koeffB3.Location = new System.Drawing.Point(117, 22);
            this._koeffB3.Name = "_koeffB3";
            this._koeffB3.Size = new System.Drawing.Size(52, 20);
            this._koeffB3.TabIndex = 4;
            this._koeffB3.Tag = "65535";
            this._koeffB3.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Степень pA";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 51);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Коэффициент А";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Коэффициент B";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this._Uin4);
            this.groupBox5.Controls.Add(this._rangeOfScale4);
            this.groupBox5.Controls.Add(this._koeffpA4);
            this.groupBox5.Controls.Add(this._koeffA4);
            this.groupBox5.Controls.Add(this._koeffB4);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Enabled = false;
            this.groupBox5.Location = new System.Drawing.Point(7, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(193, 166);
            this.groupBox5.TabIndex = 52;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Конфигурация канала";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 127);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Сигнал на входе";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 103);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "Предел шкалы";
            // 
            // _Uin4
            // 
            this._Uin4.Location = new System.Drawing.Point(117, 124);
            this._Uin4.Name = "_Uin4";
            this._Uin4.Size = new System.Drawing.Size(52, 20);
            this._Uin4.TabIndex = 8;
            this._Uin4.Tag = "65535";
            this._Uin4.Text = "0";
            // 
            // _rangeOfScale4
            // 
            this._rangeOfScale4.Location = new System.Drawing.Point(117, 100);
            this._rangeOfScale4.Name = "_rangeOfScale4";
            this._rangeOfScale4.Size = new System.Drawing.Size(52, 20);
            this._rangeOfScale4.TabIndex = 7;
            this._rangeOfScale4.Tag = "65535";
            this._rangeOfScale4.Text = "0";
            // 
            // _koeffpA4
            // 
            this._koeffpA4.Enabled = false;
            this._koeffpA4.Location = new System.Drawing.Point(117, 74);
            this._koeffpA4.Name = "_koeffpA4";
            this._koeffpA4.Size = new System.Drawing.Size(52, 20);
            this._koeffpA4.TabIndex = 6;
            this._koeffpA4.Tag = "65535";
            this._koeffpA4.Text = "0";
            // 
            // _koeffA4
            // 
            this._koeffA4.Location = new System.Drawing.Point(117, 48);
            this._koeffA4.Name = "_koeffA4";
            this._koeffA4.Size = new System.Drawing.Size(52, 20);
            this._koeffA4.TabIndex = 5;
            this._koeffA4.Tag = "65535";
            this._koeffA4.Text = "0";
            // 
            // _koeffB4
            // 
            this._koeffB4.Location = new System.Drawing.Point(117, 22);
            this._koeffB4.Name = "_koeffB4";
            this._koeffB4.Size = new System.Drawing.Size(52, 20);
            this._koeffB4.TabIndex = 4;
            this._koeffB4.Tag = "65535";
            this._koeffB4.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 77);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Степень pA";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(87, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "Коэффициент А";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 25);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Коэффициент B";
            // 
            // CalibrationChannelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 394);
            this.Controls.Add(this._channelControl);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._statusStep);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this._descriptionGroup);
            this.Controls.Add(this._continueBtn);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this._passInsertBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CalibrationChannelForm";
            this.Text = "Калибровка каналов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CalibrationChannelForm_FormClosing);
            this.Load += new System.EventHandler(this.CalibrationChannelForm_Load);
            this._descriptionGroup.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._passInsertBox.ResumeLayout(false);
            this._passInsertBox.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this._channelControl.ResumeLayout(false);
            this._channelTab1.ResumeLayout(false);
            this._channelTab2.ResumeLayout(false);
            this._channelTab3.ResumeLayout(false);
            this._channelTab4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox _descriptionGroup;
        private Forms.RichTextBox.AdvRichTextBox _descriptionBox;
        private System.Windows.Forms.Button _continueBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _channelCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button _readChannelConfigBtn;
        private System.Windows.Forms.Button _writeChannelConfigBtn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox _Uin;
        private System.Windows.Forms.MaskedTextBox _rangeOfScale;
        private System.Windows.Forms.MaskedTextBox _koeffpA;
        private System.Windows.Forms.MaskedTextBox _koeffA;
        private System.Windows.Forms.MaskedTextBox _koeffB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox _passInsertBox;
        private System.Windows.Forms.Button _validatePassButton;
        private System.Windows.Forms.TextBox _passTB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label _statusStep;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.TabControl _channelControl;
        private System.Windows.Forms.TabPage _channelTab1;
        private System.Windows.Forms.TabPage _channelTab2;
        private System.Windows.Forms.TabPage _channelTab3;
        private System.Windows.Forms.TabPage _channelTab4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _Uin1;
        private System.Windows.Forms.MaskedTextBox _rangeOfScale1;
        private System.Windows.Forms.MaskedTextBox _koeffpA1;
        private System.Windows.Forms.MaskedTextBox _koeffA1;
        private System.Windows.Forms.MaskedTextBox _koeffB1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox _Uin2;
        private System.Windows.Forms.MaskedTextBox _rangeOfScale2;
        private System.Windows.Forms.MaskedTextBox _koeffpA2;
        private System.Windows.Forms.MaskedTextBox _koeffA2;
        private System.Windows.Forms.MaskedTextBox _koeffB2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox _Uin3;
        private System.Windows.Forms.MaskedTextBox _rangeOfScale3;
        private System.Windows.Forms.MaskedTextBox _koeffpA3;
        private System.Windows.Forms.MaskedTextBox _koeffA3;
        private System.Windows.Forms.MaskedTextBox _koeffB3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox _Uin4;
        private System.Windows.Forms.MaskedTextBox _rangeOfScale4;
        private System.Windows.Forms.MaskedTextBox _koeffpA4;
        private System.Windows.Forms.MaskedTextBox _koeffA4;
        private System.Windows.Forms.MaskedTextBox _koeffB4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
    }
}