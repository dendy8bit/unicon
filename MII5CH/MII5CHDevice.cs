﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Queries;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MII5CH.Structures;

namespace BEMN.MII5CH
{
    public class MII5CHDevice : Device, IDeviceView, IDeviceVersion
    {
        #region Private fields
        private MemoryEntity<UioRam> _uioRam;
        private MemoryEntity<SomeStruct> _signature;
        private MemoryEntity<RelayDiscretLedStruct> _ledStruct;
        private MemoryEntity<AllAnalogConfig> _allAnalogConfig;
        private MemoryEntity<OneWordStruct> _valueWord;
        private MemoryEntity<VersionDevice> _version;
        private MemoryEntity<MemConfigConnection> _configUsb;
        private MemoryEntity<MemConfigConnection> _configRs485;
        private MemoryEntity<MemConfigRS485> _configRs485Old;
        private MemoryEntity<AddSetpointStruct> _setpoint;
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog1;
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog2;
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog3;
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog4;
        private MemoryEntity<AnalogExtConfig> _calibrovkaAnalog5;
        private MemoryEntity<OneWordStruct> _modeWord;
        #endregion

        #region Constructors
        public MII5CHDevice()
        {
            this.Init();
        }

        public MII5CHDevice(Modbus mb) : this(mb, false)
        { }

        public MII5CHDevice(Modbus mb, bool log)
        {
            this.Init();
            this.MB = mb;
            this.MB.Logging = log;
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null == mb) return;
                mb.CompleteExchange += mb_CompleteExchange;
                this.InitStructures();
            }
        }

        private void Init()
        {
            HaveVersion = true;
        }

        private void InitStructures()
        {
            this._uioRam = new MemoryEntity<UioRam>("RAM ввода-вывода", this, 0x0000);
            this._ledStruct = new MemoryEntity<RelayDiscretLedStruct>("Дискреты, реле и светодиоды", this, 0x000A);
            this._allAnalogConfig = new MemoryEntity<AllAnalogConfig>("Вся конфигурация аналоговых каналов", this, 0x1500);
            this._setpoint = new MemoryEntity<AddSetpointStruct>("Уставки", this, 0x1521);
            this._valueWord = new MemoryEntity<OneWordStruct>("Чтение значения канала", this, 0x0000);
            this._configUsb = new MemoryEntity<MemConfigConnection>("USB", this, 0x1000);
            this._configRs485 = new MemoryEntity<MemConfigConnection>("RS-485", this, 0x1008);
            this._configRs485Old = new MemoryEntity<MemConfigRS485>("RS-485", this, 0x1008);
            this._calibrovkaAnalog1 = new MemoryEntity<AnalogExtConfig>("Конфигурация 1 аналогового канала", this, 0x1500);
            this._calibrovkaAnalog2 = new MemoryEntity<AnalogExtConfig>("Конфигурация 2 аналогового канала", this, 0x1504);
            this._calibrovkaAnalog3 = new MemoryEntity<AnalogExtConfig>("Конфигурация 3 аналогового канала", this, 0x1508);
            this._calibrovkaAnalog4 = new MemoryEntity<AnalogExtConfig>("Конфигурация 4 аналогового канала", this, 0x150C);
            this._calibrovkaAnalog5 = new MemoryEntity<AnalogExtConfig>("Конфигурация 5 аналогового канала", this, 0x1510);
            this._version = new MemoryEntity<VersionDevice>("Версия прошивки, устройства и загрузчика", this, 0x1F00);
            this._modeWord = new MemoryEntity<OneWordStruct>("Режим работы", this, 0x1520);
            this.ModeMeasuring = new MemoryEntity<OneWordStruct>("Режим работы (Измерения)", this, 0x0009);
        }
        #endregion

        #region Properties

        public MemoryEntity<UioRam> UioRam
        {
            get { return this._uioRam; }
        }

        public MemoryEntity<SomeStruct> Signature
        {
            get
            {
                this._signature = new MemoryEntity<SomeStruct>("Конфигурация дискрет, реле и светодиодов", this, 0x1E00);
                this._signature.Values = new ushort[0x80];
                this._signature.Slots = QueriesForm.SetSlots(this._signature.Values, 0x1E00);
                return this._signature;
            }
        }

        public MemoryEntity<RelayDiscretLedStruct> LedStruct
        {
            get { return this._ledStruct; }
        }

        public MemoryEntity<AllAnalogConfig> AllAnalog
        {
            get { return this._allAnalogConfig; }
        }

        public MemoryEntity<OneWordStruct> ValueWord
        {
            get { return this._valueWord; }
        }

        public MemoryEntity<VersionDevice> Version
        {
            get { return this._version; }
        }

        public MemoryEntity<MemConfigConnection> ConfigUsb
        {
            get { return this._configUsb; }
        }

        public MemoryEntity<MemConfigConnection> ConfigRs485
        {
            get { return this._configRs485; }
        }

        public MemoryEntity<MemConfigRS485> ConfigRs485Old
        {
            get { return this._configRs485Old; }
        }

        public MemoryEntity<AnalogExtConfig> CalibrovkaAnalog1
        {
            get { return this._calibrovkaAnalog1; }
        }

        public MemoryEntity<AnalogExtConfig> CalibrovkaAnalog2
        {
            get { return this._calibrovkaAnalog2; }
        }
        public MemoryEntity<AnalogExtConfig> CalibrovkaAnalog3
        {
            get { return this._calibrovkaAnalog3; }
        }
        public MemoryEntity<AnalogExtConfig> CalibrovkaAnalog4
        {
            get { return this._calibrovkaAnalog4; }
        }
        public MemoryEntity<AnalogExtConfig> CalibrovkaAnalog5
        {
            get { return this._calibrovkaAnalog5; }
        }

        public MemoryEntity<AddSetpointStruct> Setpoint
        {
            get { return this._setpoint; }
        }

        public MemoryEntity<OneWordStruct> ModeMeasuring { get; private set; }

        public MemoryEntity<OneWordStruct> Mode
        {
            get { return this._modeWord; }
        }
        #endregion

        #region Read version
        public override void LoadVersion(object deviceObj)
        {
            System.Threading.Thread.Sleep(500);
            LoadSlot(DeviceNumber, new slot(0x1F00, 0x1F18), "version" + DeviceNumber, this);
            //this._version.LoadStruct();
        }
        #endregion

        #region INodeView Members

        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MII5CHDevice); }
        }

        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mlk; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МЛК МИИ"; }
        }

        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion

        #region IDeviceVersion Members
        public Type[] Forms
        {
            get
            {
                return new[]
                {
                    typeof (MII5CHConfigForm),
                    typeof (MII5CHMeasuringForm),
                    typeof (MII5CHQueriesForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "МЛК МИИ 1.0",
                    "МЛК МИИ 1.1"
                };
            }
        }
        #endregion
    }
}
