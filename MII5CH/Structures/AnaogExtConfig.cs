﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MII5CH.Structures
{
    public class AnalogExtConfig : StructBase
    {
        #region Private fields
        [Layout(0)] private ushort _b;				//	коэффициент
        [Layout(1)] private ushort _a;				//	коэффициент
        [Layout(2)] private ushort _pA;			    //	степень
        [Layout(3)] private ushort _rangeOfScale;   //  диапазон шкалы
        #endregion

        #region Properties

        [BindingProperty(0)]
        public ushort B
        {
            get { return _b; }
            set { _b = value; }
        }

        [BindingProperty(1)]
        public ushort A
        {
            get { return _a; }
            set { _a = value; }
        }

        [BindingProperty(2)]
        public ushort Pa
        {
            get { return _pA; }
            set { _pA = value; }
        }

        [BindingProperty(3)]
        public ushort RangeOfScale
        {
            get { return _rangeOfScale; }
            set { _rangeOfScale = value; }
        } 

        #endregion
    }
}
