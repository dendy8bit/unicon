﻿using BEMN.ChannelFive;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MII5CH.Structures
{
    public class MemConfigConnection : StructBase
    {
        [Layout(0)] private UsartConfig _config;

        #region Properties

        [BindingProperty(0)]
        public string Speed
        {
            get
            {
                return Validator.Get(_config.Init, StringData.Speeds, 0, 1, 2, 3);
            }
            set
            {
                _config.Init = Validator.Set(value, StringData.Speeds, _config.Init, 0, 1, 2, 3);
            }
        }

        [BindingProperty(1)]
        public string DataBits
        {
            get
            {
                return Validator.Get(_config.Init, StringData.DataBits, 4, 5, 6);
            }
            set
            {
                _config.Init = Validator.Set(value, StringData.DataBits, _config.Init, 4, 5, 6);
            }
        }

        [BindingProperty(2)]
        public string StopBits
        {
            get
            {
                return Validator.Get(_config.Init, StringData.StopBits, 7);
            }
            set
            {
                _config.Init = Validator.Set(value, StringData.StopBits, _config.Init, 7);
            }
        }

        [BindingProperty(3)]
        public string ParitetChet
        {
            get
            {
                return Validator.Get(_config.Init, StringData.ParitetChet, 8);
            }
            set
            {
                _config.Init = Validator.Set(value, StringData.ParitetChet, _config.Init, 8);
            }
        }

        [BindingProperty(4)]
        public string ParitetOnOff
        {
            get
            {
                return Validator.Get(_config.Init, StringData.Paritet, 9);
            }
            set
            {
                _config.Init = Validator.Set(value, StringData.Paritet, _config.Init, 9);
            }
        }

        [BindingProperty(5)]
        public string DoubleSpeed
        {
            get
            {
                return Validator.Get(_config.Init, StringData.StopBits, 10);
            }
            set
            {
                _config.Init = Validator.Set(value, StringData.StopBits, _config.Init, 10);
            }
        }

        [BindingProperty(6)]
        public byte Address
        {
            get
            {
                return Common.LOBYTE(_config.Address);
            }
            set
            {
                byte hiByte = Common.HIBYTE(_config.Address);
                _config.Address = Common.TOWORD(hiByte, value);
            }
        }

        [BindingProperty(7)]
        public byte ToSendAfter
        {
            get
            {
                return Common.LOBYTE(_config.TimeAuts);
            }
            set
            {
                byte hiByte = Common.HIBYTE(_config.TimeAuts);
                _config.TimeAuts = Common.TOWORD(hiByte, value);
            }
        }

        [BindingProperty(8)]
        public byte ToSendBefore
        {
            get
            {
                return Common.HIBYTE(_config.TimeAuts);
            }
            set
            {
                byte loByte = Common.LOBYTE(_config.TimeAuts);
                _config.TimeAuts = Common.TOWORD(value, loByte);
            }
        }

        #endregion
    }
}
