﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MII5CH.Structures
{
    public class AddSetpointStruct: StructBase
    {
        [Layout(0)] private ushort _timeout;
        [Layout(1, Count = 6, Ignore = true)] private ushort[] _reserve;
        [Layout(2)] private ushort _resist;

        [BindingProperty(0)]
        public ushort Timeout
        {
            get { return this._timeout; }
            set { this._timeout = value; }
        }
        
        [BindingProperty(1)]
        public int ResistOn
        {
            get { return (this._resist*100); }
            set { this._resist = (ushort)(value/100); }
        }
    }
}
