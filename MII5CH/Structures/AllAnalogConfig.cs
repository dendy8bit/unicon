﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MII5CH.Structures
{
    public class AllAnalogConfig : StructBase
    {
        [Layout(0)] private AnalogExtConfig _channelConfig1;
        [Layout(1)] private AnalogExtConfig _channelConfig2;
        [Layout(2)] private AnalogExtConfig _channelConfig3;
        [Layout(3)] private AnalogExtConfig _channelConfig4;

        [BindingProperty(0)]
        public ushort RangeOfScale1
        {
            get { return _channelConfig1.RangeOfScale; }
        }

        [BindingProperty(1)]
        public ushort RangeOfScale2
        {
            get { return _channelConfig2.RangeOfScale; }
        }

        [BindingProperty(2)]
        public ushort RangeOfScale3
        {
            get { return _channelConfig3.RangeOfScale; }
        }

        [BindingProperty(3)]
        public ushort RangeOfScale4
        {
            get { return _channelConfig4.RangeOfScale; }
        }
    }
}
