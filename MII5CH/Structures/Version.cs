﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MII5CH.Structures
{
    public class VersionDevice : StructBase
    { 
        [Layout(0, Count = 24)] private ushort[] _versionInfo;
        
        #region Properties for version of application
        [BindingProperty(0)]
        public string AppName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string appName = GetParam(words.ToArray())[0];
                return appName;
            }
        }

        [BindingProperty(1)]
        public string AppPodtip
        {
            get
            {
                try
                {
                    List<ushort> words = new List<ushort>();
                    for (int i = 0; i < 8; i++)
                    {
                        words.Add(_versionInfo[i]);
                    }
                    string appPodtip = GetParam(words.ToArray())[1];
                    return appPodtip;
                }
                catch (Exception)
                {
                    return "SC";
                }
            }
        }

        [BindingProperty(2)]
        public string AppMod
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string appMod = GetParam(words.ToArray())[2];
                return appMod;
            }
        }

        [BindingProperty(3)]
        public string AppVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string appVersion = GetParam(words.ToArray())[3];
                return appVersion;
            }
        }
        #endregion
        
        #region Properties for version of device
        [BindingProperty(4)]
        public string DevName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devName = GetParam(words.ToArray())[0];
                return devName;
            }
        }

        [BindingProperty(5)]
        public string DevPodtip
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devPodtip = GetParam(words.ToArray())[1];
                return devPodtip;
            }
        }

        [BindingProperty(6)]
        public string DevMod
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devMod = GetParam(words.ToArray())[2];
                return devMod;
            }
        }

        [BindingProperty(7)]
        public string DevVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devVersion = GetParam(words.ToArray())[3];
                return devVersion;
            }
        }
        #endregion

        #region Properties for version of firmware
        [BindingProperty(8)]
        public string LdrName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 8; i < 16; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string ldrName = GetParam(words.ToArray())[0];
                return ldrName;
            }
        }

        [BindingProperty(9)]
        public string LdrVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 8; i < 16; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string ldrVers = GetParam(words.ToArray())[3];
                return ldrVers;
            }
        }

        // три байта во второй строке версии
        [BindingProperty(10)]
        public string LdrCodProc
        {
            get
            {
                List<byte> buf = new List<byte>();
                List<string> cod = new List<string>();
                buf.AddRange(Common.TOBYTE(_versionInfo[10]));
                buf.Add(Common.TOBYTE(_versionInfo[11])[0]);
                foreach (var b in buf)
                {
                    if (Convert.ToString(b, 16).Length < 2)
                    {
                        cod.Add("0" + Convert.ToString(b, 16));
                    }
                    else
                    {
                        cod.Add(Convert.ToString(b, 16));
                    }
                    cod.Add(".");
                }
                cod.RemoveAt(cod.Count-1);
                string ret = string.Empty;
                foreach (var c in cod)
                {
                    ret += c;
                }
                return ret;
            }
        }

        [BindingProperty(11)]
        public string LdrFuseProc
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_versionInfo[12]));
                buf.AddRange(Common.TOBYTE(_versionInfo[13]));
                string ret = "";
                ret += Convert.ToString(buf[0], 16) + "." + Convert.ToString(buf[1], 16) + "." +
                       Convert.ToString(buf[2], 16) + "." + Convert.ToString(buf[3], 16);
                return ret;
            }
        }
        #endregion
        
        #region Methods
        string[] GetParam(ushort[] words)
        {
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            byte[] byteBuf = Common.TOBYTES(words, false);
            char[] charBuf = new char[byteBuf.Length];
            int byteUsed = 0;
            int charUsed = 0;
            bool complete = false;

            try
            {
                dec.Convert(byteBuf, 0, byteBuf.Length, charBuf, 0, byteBuf.Length, true, out byteUsed, out charUsed,
                    out complete);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }

            string[] param = new string(charBuf, 0, 16).Split(new string[] { " ", "    " },
                StringSplitOptions.RemoveEmptyEntries);
            return param;
        }
        #endregion
    }
}
