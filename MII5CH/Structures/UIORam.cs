﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MII5CH.Structures
{
    public class UioRam : StructBase
    {
        [Layout(0)] private ushort _analog1;
        [Layout(1)] private ushort _analog2;
        [Layout(2)] private ushort _analog3;
        [Layout(3)] private ushort _analog4;
        [Layout(4)] private ushort _pulse;
        [Layout(5)] private ushort _resist1;
        [Layout(6)] private ushort _resist2;
        [Layout(7)] private ushort _cap;
        [Layout(8)] private ushort _status;
        [Layout(9)] private ushort _mode;


        #region Properties
        public ushort Analog1
        {
            get { return _analog1; }
        }
        public ushort Analog2
        {
            get { return _analog2; }
        }
        public ushort Analog3
        {
            get { return _analog3; }
        }
        public ushort Analog4
        {
            get { return _analog4; }
        }

        public ushort Pulse
        {
            get { return _pulse; }
        }

        public ushort ResistPlus
        {
            get { return _resist1; }
        }

        public ushort ResistMinus
        {
            get { return _resist2; }
        }

        public ushort Capacity
        {
            get { return _cap; }
        }

        public bool[] Status
        {
            get
            {
                return new[] { Common.GetBit(_status, 0), Common.GetBit(_status, 1) };
            }
        }


        #endregion
        
    }
}
