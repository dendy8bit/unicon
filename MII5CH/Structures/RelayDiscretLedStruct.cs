﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MII5CH.Structures
{
    public class RelayDiscretLedStruct : StructBase
    {
        [Layout(0, Count = 2)] private ushort[] _discrets;
        [Layout(1, Count = 2)] private ushort[] _relays;
        [Layout(2, Count = 2)] private ushort[] _leds;

        /// <summary>
        /// Дискреты, начиная с младшего бита
        /// </summary>
        public BitArray Discrets
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_discrets[0]).Reverse().ToArray());
                buf.AddRange(Common.TOBYTE(_discrets[1]).Reverse().ToArray());
                BitArray ret = new BitArray(buf.ToArray());
                return ret;
            }
        }

        public BitArray Relay
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_relays[0]).Reverse().ToArray());
                buf.AddRange(Common.TOBYTE(_relays[1]).Reverse().ToArray());
                BitArray ret = new BitArray(buf.ToArray());
                return ret;
            }
        }
        public BitArray Diods
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_leds[0]).Reverse().ToArray());
                buf.AddRange(Common.TOBYTE(_leds[1]).Reverse().ToArray());
                BitArray ret = new BitArray(buf.ToArray());
                return ret;
            }
        }
    }
}
