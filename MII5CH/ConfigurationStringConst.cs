﻿namespace BEMN.ChannelFive
{
    public static class ConfigurationStringConst
    {
        #region Константы
        public const string CANAL_ERR = "Неиспр. канал";
        public const string LOWER_LIMIT = "Нижн.пред.";
        public const string UPPER_LIMIT = "Верх.пред.";
        public const string CALCULATION = "Не рассчитано";

        public const string NAN = "NaN";
        public const string BAD_DATA = "проверьте правильность введенных данных";
        public const string ERROR_WRITE = "Ошибка записи";
        public const string READ_USB_FAIL = "Не удалось загрузить параметры USB";
        public const string READ_USB_OK = "Конфигурация USB загружена успешно";
        public const string READ_RS485_FAIL = "Не удалось загрузить параметры RS-485";
        public const string READ_RS485_OK = "Конфигурация RS-485 загружена успешно";
        public const string READ_SETPOINT_FAIL = "Не удалось загрузить уставки";
        public const string READ_SETPOINT_OK = "Уставки загружены успешно";
        public const string READ_ANALOG_CONFIG_FAIL = "Не удалось загрузить конфигурацию {0} аналогового канала";
        public const string READ_ANALOG_CONFIG_OK = "конфигурация {0} аналогового канала загружены успешно";
        public const string READ_VERSION_OK = "Версия загружена успешно";
        public const string READ_VERSION_FAIL = "Не удалось прочитать версию";
        public const string WRITE_USB_OK = "Конфигурация USB записана успешно";
        public const string WRITE_USB_FAIL = "Не удалось записать конфигурацию USB";
        public const string WRITE_RS485_OK = "Конфигурация RS-485 записана успешно";
        public const string WRITE_RS485_FAIL = "Не удалось записать конфигурацию RS-485";
        public const string WRITE_SETPOINT_OK = "Уставки записаны успешно";
        public const string WRITE_SETPOINT_FAIL = "Не удалось записать уставки";
        public const string WRITE_ANALOG_CONFIG_OK = "Конфигурация {0} аналогового канала записана успешно";
        public const string WRITE_ANALOG_CONFIG_FAIL = "Не удалось записать конфигурацию {0} аналогового канала";
        public const string STEP = "Шаг {0}";
        public const string DEFUULT_CONFIG_SAVE_OK = "Конфигурация по умолчанию записана успешно";
        public const string Info0 = "   Выберите из списка калибруемый канал, нажмите кнопку \"Прочитать конфигурацию\" " +
                                    "и после успешного прочтения конфигурации нажмите кнопку \"Продолжить\". " +
                                    "В конфигурацию канала будут записаны начальные значения";

        public const string Info1 =
            "   Для получения \"Коэффициента В\" убедитесь, что на калибруемый аналоговый вход нет физических подключений и нажмите кнопку \"Продолжить\".";

        public const string Info2 = "   Для получения \"Коэффициента А\":\n" +
                                     "   а) установите величину верхнего предела шкалы измерения (поле \"Предел шкалы\").\n" +
                                     "   б) установите величину эталонного сигнала, который будет подан на аналоговый вход (поле \"Сигнал на входе\").\n" +
                                     "   в) создайте физическое подключение на аналоговый вход с источником эталонного сигнала и нажмите кнопку \"Продолжить\".";

        public const string Info3 = "Калибровка завершена. При необходимости коэффициенты можно подправить и сохранить в устройство.";

        public const string Info1Channel4 =
            "   Для получения \"Коэффициента В\" подайте на калибруемый аналоговый вход значение эталонного сигнала (рекомендуемое значение -75мВ), создать физическое подключение и нажмите кнопку \"Продолжить\".";

        public const string Info2Channel4 = "   Для получения \"Коэффициента А\":\n" +
                                            "   а) установите величину верхнего предела шкалы измерения (поле \"Предел шкалы\"). Рекомендуется значение 150мВ.\n" +
                                            "   б) убрать физическое подключение на аналоговый вход с источником сигнала и нажмите кнопку \"Продолжить\".";

        public const string InfoDop =
            " Обратите ВНИМАНИЕ, что дельта окончания измерения может меняться в зависимости от предела шкалы!";
        public const string B_SAVED = "Коэффициент В сохранен";
        public const string B_SAVING = "Расчет и запись коэффициента В";
        public const string A_MEASSURING = "Идет расчет коеффициента А";
        public const string NO_POWER_SUPPLY = "Нет подключения с источником сигнала!";
        public const string COMPLETE = "Калибровка завершена успешно";
        public const string CONTINUE = "Продолжить";
        public const string BREAK = "Завершить";
        public const string WRITING_GISTER_ERROR = "Произошла ошибка записи";
        public const string READ_WRITE_MOD_FAIL = "Не удается установить режим работы";
        public const string MES_ERROR_MEASURE_KOEF_A = "Проверьте полярность на входе. Нажмите кнопку \"Продолжить\".";
        public const string ERR_KOEF_A = "Невозможно вычислить коеффициент А";
        public const string CALIBRATE_IS_NOT_COMPLETE =
            "Предыдущая калибровка не была завершена. Вы точно хотите ее завершить и выбрать другой канал?";

        #endregion
    }
}
