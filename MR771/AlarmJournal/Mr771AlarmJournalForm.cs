﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR771.AlarmJournal.Structures;
using BEMN.MR771.Configuration;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;
using BEMN.MR771.Properties;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR771.AlarmJournal
{
    public partial class Mr771AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР771_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string DEVICE_NAME = "MR771";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        #endregion [Constants]
        
        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;   
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private Mr771 _device;
        private List<string> _messages;
        private FileDriver _fileDriver;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr771AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr771AlarmJournalForm(Mr771 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._currentOptionsLoader = device.CurrentOptionsLoaderAj;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this.StartReadJournal);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);

            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && device.AllAlarmJournal != null)
            {
                this._journalLoader = new AlarmJournalLoader(device.AllAlarmJournal, device.SetPageAlarmJournal);
                this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
                this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
            }
            else
            {
                this._alarmJournal = device.AlarmJournal;
                this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
                this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    if (this._failCounter < 80)
                    {
                        this._failCounter++;
                    }
                    else
                    {
                        this._alarmJournal.RemoveStructQueries();
                        this._failCounter = 0;
                        this.ButtonsEnabled = true;
                    }
                });
                this._setPageAlarmJournal = device.SetPageAlarmJournal;
                this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,this._alarmJournal.LoadStruct);
                this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,this.FailReadJournal);
            }

            this._fileDriver = new FileDriver(device, this);
        } 
        #endregion [Ctor's]


        #region [Help members]

        private void StartReadJournal()
        {
            this._table.Clear();
            this._failCounter = this._recordNumber = 0;
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && this._device.AllAlarmJournal != null)
            {
                this._journalLoader.Clear();
                this._journalLoader.StartRead();
            }
            else
            {
                this.SavePageNumber();
            }
        }

        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof (ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals) serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }

            this.StartReadOption();
        }

        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void ReadRecord()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    AlarmJournalRecordStruct record = this._alarmJournal.Value;
                    MeasureTransStruct measure = this._currentOptionsLoader.Connections[this._alarmJournal.Value.GroupOfSetpoints].Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetResist((short) record.Rab, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xab, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rbc, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xbc, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rca, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xca, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            this.GetStep(record.NumberOfTriggeredParametr),
                            ValuesConverterCommon.Analog.GetResist((short) record.Ra1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xa1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rb1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xb1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rc1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xc1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl*40), // 3I0
                            ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx*40),
                            ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                            ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                            ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                            ValuesConverterCommon.Analog.GetF(record.F),
                            ValuesConverterCommon.Analog.GetQ(record.Q),
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();
                    this.SavePageNumber();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch(Exception e)
            {
                this._table.Rows.Add
                    (
                        this._recordNumber, "", "Ошибка ("+e.Message+")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", ""
                    );
            }
        }

        private void ReadRecordFromBinFile()
        {
            ReadAllRecords();
        }

        /*
        private void ReadRecordFromBinFile()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    AlarmJournalRecordStruct record = this._alarmJournal.Value;
                    MeasureTransStruct measure = this._currentOptionsLoader.Connections[record.GroupOfSetpoints].Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    this._table.Rows.Add
                    (
                        this._recordNumber,
                        record.GetTime,
                        AjStrings.Message[record.Message],
                        triggeredDef,
                        parameter,
                        parametrValue,
                        AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        this.GetStep(record.NumberOfTriggeredParametr),
                        ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40), // 3I0
                        ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                        ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetQ(record.Q),
                        record.D1To8,
                        record.D9To16,
                        record.D17To24,
                        record.D25To32,
                        record.D33To40
                    );
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                (
                    this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", ""
                );
            }

        }
        */



        private void ReadAllRecords()
        {
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }
            this._recordNumber = 1;
            foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoader.Connections[record.GroupOfSetpoints].Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);

                    this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetResist((short) record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            this.GetStep(record.NumberOfTriggeredParametr), 
                            ValuesConverterCommon.Analog.GetResist((short) record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl*40), // 3I0
                            ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx*40),
                            ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                            ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                            ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                            ValuesConverterCommon.Analog.GetF(record.F),
                            ValuesConverterCommon.Analog.GetQ(record.Q),
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );
                }
                catch (Exception e)
                {
                    this._table.Rows.Add
                        (
                            this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                            "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                            "", "", "", ""
                        );
                }
                this._recordNumber++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber - 1);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredDefence(AlarmJournalRecordStruct record)
        {
            return record.TriggeredDefense == 61 
                ? this._messages[record.ValueOfTriggeredParametr] 
                : AjStrings.TriggeredDefense[record.TriggeredDefense];
        }

        private string GetParameter(AlarmJournalRecordStruct record)
        {
            if (record.TriggeredDefense == 15 || record.TriggeredDefense >= 44 && record.TriggeredDefense <= 59 || record.TriggeredDefense == 61 )
            {
                return string.Empty;
            }
            return AjStrings.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalRecordStruct record, MeasureTransStruct measure)
        {
            int parameter = record.NumberOfTriggeredParametr;
            ushort value = record.ValueOfTriggeredParametr;
            if ((parameter >= 36 && parameter <= 40) || parameter == 42 || parameter == 65)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
            }
            if (parameter == 41 || parameter == 43)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittx * 40);
            }
            if ((parameter >= 0) && (parameter <= 35))
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short) value, (short) value1,
                    measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
            }
            if ((parameter >= 44) && (parameter <= 52))
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
            }

            if (parameter == 53)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
            }

            if (parameter == 54 || parameter == 69)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.Kthx1Value);
            }

            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }
            if (parameter == AjStrings.OmpInd)
            {
                return ValuesConverterCommon.Analog.GetOmp(value);
            }
            if (parameter == 60)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }
            if (parameter == 61 || parameter == 67 || parameter == 68 || (parameter >= 56 && parameter <= 58))
            {
                return string.Empty;
            }
            if (parameter == 64)
            {
                return ValuesConverterCommon.Analog.GetSignF((short) value);
            }
            return value.ToString();
        }

        private string GetStep(int param)
        {
            if ((param >= 0 && param <= 11) || param >= 36) return string.Format("Контур Ф-N{0}", 1);// по умолчанию 1я ступень
            if (param > 11 && param <= 17) return string.Format("Контур Ф-N{0}", 2);
            if (param > 17 && param <= 23) return string.Format("Контур Ф-N{0}", 3);
            if (param > 23 && param <= 29) return string.Format("Контур Ф-N{0}", 4);
            if (param > 29 && param <= 35) return string.Format("Контур Ф-N{0}", 5);
            return string.Format("Контур Ф-N{0}", 1); // по умолчанию 1я ступень
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
            }
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void Mr771AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            if (Device.AutoloadConfig && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void _readAlarmJournalButtonClick(object sender, EventArgs e)
        {
            if (this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void StartReadOption()
        {
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            int j = 0;
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int countRecord = journalBytes.Count / size;
                for (int i = 0; j < countRecord - 2; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                    this._alarmJournal.Value = journal;
                    this.ReadRecordFromBinFile();
                    j++;
                }
            }
            else
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR731AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
        
        private void Mr771AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._alarmJournal?.RemoveStructQueries();
            this._journalLoader?.ClearEvents();
        }
        #endregion [Event Handlers]

        
        #region [IFormView Members]
        public Type FormDevice => typeof(Mr771); 
        public bool Multishow { get; private set; }
        public INodeView[] ChildNodes => new INodeView[] { }; 
        public Type ClassType => typeof(Mr771AlarmJournalForm);
        public bool Deletable => false;
        public bool ForceShow => false;
        public Image NodeImage => Resources.ja;
        public string NodeName => ALARM_JOURNAL;
        #endregion [IFormView Members]
    }
}
