﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR771.AlarmJournal.Structures
{
    public class AllAlarmJournalStruct : StructBase
    {
        [Layout(0, Count = 18)] AlarmJournalRecordStruct[] _alarmJournalRecords;

        public List<AlarmJournalRecordStruct> AllJournalRecords
        {
            get { return new List<AlarmJournalRecordStruct>(this._alarmJournalRecords.Where(j=>!j.IsEmpty));}
        }
    }
}
