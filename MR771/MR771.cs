﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR771.AlarmJournal;
using BEMN.MR771.AlarmJournal.Structures;
using BEMN.MR771.BSBGL;
using BEMN.MR771.Configuration;
using BEMN.MR771.Configuration.Structures;
using BEMN.MR771.Configuration.Structures.ConfigSystem;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;
using BEMN.MR771.Configuration.Structures.Oscope;
using BEMN.MR771.Diagnostics;
using BEMN.MR771.Emulation;
using BEMN.MR771.Emulation.Structures;
using BEMN.MR771.FaceSideDevice;
using BEMN.MR771.FileSharingService;
using BEMN.MR771.Measuring;
using BEMN.MR771.Measuring.Structures;
using BEMN.MR771.Osc;
using BEMN.MR771.Osc.Structures;
using BEMN.MR771.SystemJournal;
using BEMN.MR771.SystemJournal.Structures;

namespace BEMN.MR771
{
    public class Mr771 : Device, IDeviceView, IDeviceVersion
    {
        #region Const

        public const int BAUDE_RATE_MAX = 921600;
        private const string NODE_NAME = "МР771";
        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const ushort START_ADDR_MEAS_TRANS = 0x1278;
        #endregion

        #region Field

        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<AllAlarmJournalStruct> _allAlarmJournal;
        private MemoryEntity<OneWordStruct> _setPageAlarmJournal;
        private CurrentOptionsLoader _currentOptionsLoader;
        private CurrentOptionsLoader _currentOptionsLoaderAj;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<FullSystemJournalStruct1024> _full1024;
        private MemoryEntity<FullSystemJournalStruct64> _full64;  
        private MemoryEntity<OneWordStruct> _saveSysJournalIndex;

        private MemoryEntity<ConfigurationStruct> _configuration;

        private MemoryEntity<OneWordStruct> _iMinWord; 
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<OneWordStruct> _groupUstavki;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<MeasureTransStruct> _measureTrans;

        private MemoryEntity<OneWordStruct> _refreshOscJournal;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<AllOscJournalStruct> _allOscJournal; 
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<OscopeAllChannelsStruct> _allChannels;

        private MemoryEntity<WriteStructEmul> _writeSructEmulation; 
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull; 
        private MemoryEntity<ReadStructEmul> _readSructEmulation; 

        private ushort _groupSetPointSize;

        private slot _slot = new Device.slot(0x100, 0x100 + 0x29);
        public event Handler DisplayInfoLoadOk;
        public event Handler DisplayInfoLoadFail;

        public slot DisplayInfoSlot => _slot;
        #endregion

        #region Programming

        private MemoryEntity<OneWordStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<LogicProgramSignals> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _isSplOn;
        private MemoryEntity<SomeStruct> _stateSpl;
        
        public MemoryEntity<OneWordStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return this._stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return this._startSpl; }
        }

        public MemoryEntity<SomeStruct> StateSpl
        {
            get { return this._stateSpl; }
        }

        public MemoryEntity<OneWordStruct> IsSplOn
        {
            get { return this._isSplOn; }
        }
        
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<LogicProgramSignals> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }
        #endregion

        #region Constructor

        public Mr771()
        {
            HaveVersion = true;
        }

        public Mr771(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
        }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                if (mb != null)
                {
                    mb.CompleteExchange -= this.CompleteExchange;
                }
                mb = value;
                mb.CompleteExchange += this.CompleteExchange;
            }
        }
        
        private void InitMemoryEntity()
        {
            double version = Common.VersionConverter(DeviceVersion);
            int slotLen = this.MB.BaudeRate == BAUDE_RATE_MAX && version >= 1.05 ? 1024 : 64;
            this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Запись журнала аварий", this,0x0700, slotLen);
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая база данных", this, 0x0E00, slotLen);
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300, slotLen);
            this._programSignalsStruct = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals", this, 0x4100, slotLen);
            this._programPageStruct = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000, slotLen);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Запись журнала системы", this, 0x0600);
            if (this.MB.BaudeRate == BAUDE_RATE_MAX && version >= 1.07)
            {
                this._allAlarmJournal = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this,0x0700, slotLen);
                this._allOscJournal = new MemoryEntity<AllOscJournalStruct>("Журнал осциллографа (весь)", this, 0x0800, slotLen);
                this._full1024 = new MemoryEntity<FullSystemJournalStruct1024>("Запись журнала системы 1024", this, 0x0600, slotLen);
                this._full64 = null;
            }
            else
            {
                this._full1024 = null;
                this._full64 = new MemoryEntity<FullSystemJournalStruct64>("Запись журнала системы 64", this, 0x0600);
            }
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x0800);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x05A0);
            this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений", this, START_ADDR_MEAS_TRANS);
            this._groupUstavki = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная база данных", this, 0x0D00);
            this._iMinWord = new MemoryEntity<OneWordStruct>("Минимально отображаемый ток", this, 0xFC05);
            this._dateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);
            this._isSplOn = new MemoryEntity<OneWordStruct>("Состояние логики", this, 0x0D14);
            this._saveSysJournalIndex = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, 0x0600);
            this._setPageAlarmJournal = new MemoryEntity<OneWordStruct>("Обновление журнала аварий", this, 0x0700);
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._writeSructEmulation=new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ",this,0x5800);
            this._writeSructEmulationNull=new MemoryEntity<WriteStructEmul>("Запись  нулевых аналоговых сигналов ",this,0x5800);
            this._readSructEmulation=new MemoryEntity<ReadStructEmul>("Чтение времени и статуса эмуляции",this,0x582E);
            this._currentOptionsLoader = new CurrentOptionsLoader(this);
            this._currentOptionsLoaderAj = new CurrentOptionsLoader(this);
            if (version >= 1.08)
            {
                this._stateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики v1.08", this, 0x0D17);
                ushort[] values = new ushort[4];
                this._stateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
                this._stateSpl.Values = values;
                this._allChannels = new MemoryEntity<OscopeAllChannelsStruct>("Уставки осциллографа", this, 0x2A14, slotLen); // 0x290C + 2*22*6 - конфигурация ТУ и ТБ
                this.DiscretDataBase108 = new MemoryEntity<DiscretDataBaseStruct108>("Дискретная база данных V1.07", this, 0x0D00);
                if (version >= 1.11)
                {
                    this.Configuration111 = new MemoryEntity<ConfigurationStruct111>("Конфигурация устройства V1.11", this, 0x1000, slotLen);
                }
                else
                {
                    this.Configuration108 = new MemoryEntity<ConfigurationStruct108>("Конфигурация устройства V1.07", this, 0x1000, slotLen);
                }
            }
            else
            {
                this._stateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики", this, 0x0D14);
                ushort[] values = new ushort[4];
                this._stateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D14);
                this._stateSpl.Values = values;
                if (version >= 1.05 && version < 1.08)
                {
                    this._allChannels = new MemoryEntity<OscopeAllChannelsStruct>("Уставки осциллографа", this, 0x290C, slotLen);
                    this.Configuration105 = new MemoryEntity<ConfigurationStruct105>("Конфигурация устройства V1.05", this, 0x1000, slotLen);
                }
                else
                {
                    this._allChannels = new MemoryEntity<OscopeAllChannelsStruct>("Уставки осциллографа", this, 0x290C, slotLen);
                    this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация устройства", this, 0x1000, slotLen);
                }
            }
            this._oscPage = Common.VersionConverter(DeviceVersion) >= 1.06
                ? new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900, slotLen)
                : new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);

            ConfigRsParam = new MemoryEntity<ConfigNetStruct>("Параметры RS-485", this, 0x2B08, slotLen);

        }
        #endregion

        #region Properties
        
        public string DisplayInfo { get; set; }

        public void LoadDisplayInfo()
        {
            LoadSlotCycle(DeviceNumber, _slot, "LoadDisplayInfo" + DeviceNumber, new TimeSpan(500), 10, this);
        }

        public MemoryEntity<ConfigNetStruct> ConfigRsParam { get; private set; }

        public MemoryEntity<WriteStructEmul> WriteStructEmulation
        {
            get { return this._writeSructEmulation; }
        }

        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull
        {
            get { return this._writeSructEmulationNull; }
        }
        public MemoryEntity<ReadStructEmul> ReadStructEmulation
        {
            get { return this._readSructEmulation; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return this._alarmJournal; }
        }

        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournal
        {
            get { return this._allAlarmJournal; }
        } 
        public MemoryEntity<OneWordStruct> SetPageAlarmJournal
        {
            get { return this._setPageAlarmJournal; }
        }

        public CurrentOptionsLoader CurrentOptionsLoader
        {
            get { return this._currentOptionsLoader; }
        }

        public CurrentOptionsLoader CurrentOptionsLoaderAj
        {
            get { return this._currentOptionsLoaderAj; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<FullSystemJournalStruct1024> Full1SysJournal024
        {
            get { return this._full1024; }
        }

        public MemoryEntity<FullSystemJournalStruct64> FullSysJournal64
        {
            get { return this._full64; }
        }

        public MemoryEntity<OneWordStruct> SaveSysJournalIndex
        {
            get { return this._saveSysJournalIndex; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return this._configuration; }
        }

        public MemoryEntity<ConfigurationStruct105> Configuration105 { get; private set; }
        public MemoryEntity<ConfigurationStruct108> Configuration108 { get; private set; }
        public MemoryEntity<ConfigurationStruct111> Configuration111 { get; private set; }

        public MemoryEntity<OneWordStruct> Imin
        {
            get { return this._iMinWord; }
        }
        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<OneWordStruct> GroupUstavki
        {
            get { return this._groupUstavki; }
        }
        
        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<DiscretDataBaseStruct108> DiscretDataBase108 { get; private set; }
        
        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return this._measureTrans; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return this._oscJournal; }
        }

        public MemoryEntity<AllOscJournalStruct> AllOscJournal
        {
            get { return this._allOscJournal; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return this._setOscStartPage; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return this._oscOptions; }
        }

        public MemoryEntity<OscopeAllChannelsStruct> AllChannels
        {
            get { return this._allChannels; }
        }
        #endregion
        
        #region Methods

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return START_ADDR_MEAS_TRANS;
            return (ushort)(START_ADDR_MEAS_TRANS + this._groupSetPointSize * group);
        }

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "Сохранить конфигурацию" && Common.VersionConverter(this.DeviceVersion) < 1.12)
            {
                MessageBox.Show(query.error ? ERROR_SAVE_CONFIG : WRITE_OK);
            }

            if (query.name == "LoadDisplayInfo" + DeviceNumber)
            {
                Raise(query, null, null, ref _slot);
                GetDisplayInfo(_slot.Value);
            }

            mb_CompleteExchange(sender, query);
        }

        private void GetDisplayInfo(ushort[] buffer)
        {
            byte[] byteBuf = Common.TOBYTES(buffer, true);
            
            char[] charBuf = new char[byteBuf.Length];
            char[] chars = new char[byteBuf.Length];
            try
            {
                for (int i = 0; i < charBuf.Length; i++)
                {
                    charBuf[i] = DecodeSymbols(byteBuf[i]);
                }

                chars = Common.SwapArrayItems(charBuf);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }
            
            try
            {
                DisplayInfo = new string(chars);

                DisplayInfoLoadOk?.Invoke(this);
            }
            catch (ArgumentOutOfRangeException)
            {
                DisplayInfoLoadFail?.Invoke(this);
            }
        }

        private char DecodeSymbols(byte lit)
        {
            if (Info.IsAz)
            {
                switch (lit)
                {
                    case 0x00: return 'Ç';
                    case 0x01: return 'Ş';
                    case 0x02: return 'Ə';
                    case 0x03: return 'Ö';
                    case 0x04: return 'Ü';
                    case 0x05: return 'Ğ';
                    case 0x06: return 'İ';
                    case 0x07: return 'ş';
                    case 0x0B: return 'ö';
                    case 0x0C: return 'ü';
                    case 0x0D: return 'ğ';
                    case 0xC5: return 'ə';
                    case 0xD1: return 'ı';
                    case 0xEB: return 'ç';
                    case 0x021: return '!';
                    case 0x022: return '"'; //	'"'
                    case 0x023: return '#'; //	'#'
                    case 0x024: return '$'; //	'$'
                    case 0x025: return '%'; //	'%'
                    case 0x026: return '&'; //	'&'
                    case 0x027: return '\'';    //	'''
                    case 0x028: return '('; //	'('
                    case 0x029: return ')'; //	')'
                    case 0x02A: return '*'; //	'*'
                    case 0x02B: return '+'; //	'+'
                    case 0x02C: return ','; //	','
                    case 0x02D: return '-'; //	'-'
                    case 0x02E: return '.'; //	'.'
                    case 0x02F: return '/'; //	'/'
                    case 0x030: return '0'; //	'0'
                    case 0x031: return '1'; //	'1'
                    case 0x032: return '2'; //	'2'
                    case 0x033: return '3'; //	'3'
                    case 0x034: return '4'; //	'4'
                    case 0x035: return '5'; //	'5'
                    case 0x036: return '6'; //	'6'
                    case 0x037: return '7'; //	'7'
                    case 0x038: return '8'; //	'8'
                    case 0x039: return '9'; //	'9'
                    case 0x03A: return ':'; //	':'
                    case 0x03B: return ';'; //	';'
                    case 0x03C: return '<'; //	'<'
                    case 0x03D: return '='; //	'='
                    case 0x03E: return '>'; //	'>'
                    case 0x03F: return '?'; //	'?'
                    case 0x040: return '@'; //	'@'
                    case 0x041: return 'A'; //	'A'
                    case 0x042: return 'B'; //	'B'
                    case 0x043: return 'C'; //	'C'
                    case 0x044: return 'D'; //	'D'
                    case 0x045: return 'E'; //	'E'
                    case 0x046: return 'F'; //	'F'
                    case 0x047: return 'G'; //	'G'
                    case 0x048: return 'H'; //	'H'
                    case 0x049: return 'I'; //	'I'
                    case 0x04A: return 'J'; //	'J'
                    case 0x04B: return 'K'; //	'K'
                    case 0x04C: return 'L'; //	'L'
                    case 0x04D: return 'M'; //	'M'
                    case 0x04E: return 'N'; //	'N'
                    case 0x04F: return 'O'; //	'O'
                    case 0x050: return 'P'; //	'P'
                    case 0x051: return 'Q'; //	'Q'
                    case 0x052: return 'R'; //	'R'
                    case 0x053: return 'S'; //	'S'
                    case 0x054: return 'T'; //	'T'
                    case 0x055: return 'U'; //	'U'
                    case 0x056: return 'V'; //	'V'
                    case 0x057: return 'W'; //	'W'
                    case 0x058: return 'X'; //	'X'
                    case 0x059: return 'Y'; //	'Y'
                    case 0x05A: return 'Z'; //	'Z'
                    case 0x05B: return '['; //	'['
                    case 0x05C: return ' '; //	'\'
                    case 0x05D: return ']'; //	']'
                    case 0x05E: return '^'; //	'^'
                    case 0x05F: return '_'; //	'_'
                    case 0x060: return '`'; //	'`'
                    case 0x061: return 'a'; //	'a'
                    case 0x062: return 'b'; //	'b'
                    case 0x063: return 'c'; //	'c'
                    case 0x064: return 'd'; //	'd'
                    case 0x065: return 'e'; //	'e'
                    case 0x066: return 'f'; //	'f'
                    case 0x067: return 'g'; //	'g'
                    case 0x068: return 'h'; //	'h'
                    case 0x069: return 'i'; //	'i'
                    case 0x06A: return 'j'; //	'j'
                    case 0x06B: return 'k'; //	'k'
                    case 0x06C: return 'l'; //	'l'
                    case 0x06D: return 'm'; //	'm'
                    case 0x06E: return 'n'; //	'n'
                    case 0x06F: return 'o'; //	'o'
                    case 0x070: return 'p'; //	'p'
                    case 0x071: return 'q'; //	'q'
                    case 0x072: return 'r'; //	'r'
                    case 0x073: return 's'; //	's'
                    case 0x074: return 't'; //	't'
                    case 0x075: return 'u'; //	'u'
                    case 0x076: return 'v'; //	'v'
                    case 0x077: return 'w'; //	'w'
                    case 0x078: return 'x'; //	'x'
                    case 0x079: return 'y'; //	'y'
                    case 0x07A: return 'z'; //	'z'
                                            //////////
                                            //	case 0x0c5: return 'э';	//	'U'
                    default: return ' ';
                }

            }
            switch (lit)
            {
                case 0x021: return '!';
                case 0x022: return '"'; //	'"'
                case 0x023: return '#'; //	'#'
                case 0x024: return '$'; //	'$'
                case 0x025: return '%'; //	'%'
                case 0x026: return '&'; //	'&'
                case 0x027: return (char) 0x27;    //	'''
                case 0x028: return '('; //	'('
                case 0x029: return ')'; //	')'
                case 0x02A: return '*'; //	'*'
                case 0x02B: return '+'; //	'+'
                case 0x02C: return ','; //	','
                case 0x02D: return '-'; //	'-'
                case 0x02E: return '.'; //	'.'
                case 0x02F: return '/'; //	'/'
                case 0x030: return '0'; //	'0'
                case 0x031: return '1'; //	'1'
                case 0x032: return '2'; //	'2'
                case 0x033: return '3'; //	'3'
                case 0x034: return '4'; //	'4'
                case 0x035: return '5'; //	'5'
                case 0x036: return '6'; //	'6'
                case 0x037: return '7'; //	'7'
                case 0x038: return '8'; //	'8'
                case 0x039: return '9'; //	'9'
                case 0x03A: return ':'; //	':'
                case 0x03B: return ';'; //	';'
                case 0x03C: return '<'; //	'<'
                case 0x03D: return '='; //	'='
                case 0x03E: return '>'; //	'>'
                case 0x03F: return '?'; //	'?'
                case 0x040: return '@'; //	'@'
                case 0x041: return 'A'; //	'A'
                case 0x042: return 'B'; //	'B'
                case 0x043: return 'C'; //	'C'
                case 0x044: return 'D'; //	'D'
                case 0x045: return 'E'; //	'E'
                case 0x046: return 'F'; //	'F'
                case 0x047: return 'G'; //	'G'
                case 0x048: return 'H'; //	'H'
                case 0x049: return 'I'; //	'I'
                case 0x04A: return 'J'; //	'J'
                case 0x04B: return 'K'; //	'K'
                case 0x04C: return 'L'; //	'L'
                case 0x04D: return 'M'; //	'M'
                case 0x04E: return 'N'; //	'N'
                case 0x04F: return 'O'; //	'O'
                case 0x050: return 'P'; //	'P'
                case 0x051: return 'Q'; //	'Q'
                case 0x052: return 'R'; //	'R'
                case 0x053: return 'S'; //	'S'
                case 0x054: return 'T'; //	'T'
                case 0x055: return 'U'; //	'U'
                case 0x056: return 'V'; //	'V'
                case 0x057: return 'W'; //	'W'
                case 0x058: return 'X'; //	'X'
                case 0x059: return 'Y'; //	'Y'
                case 0x05A: return 'Z'; //	'Z'
                case 0x05B: return '['; //	'['
                case 0x05C: return ' '; //	'\'
                case 0x05D: return ']'; //	']'
                case 0x05E: return '^'; //	'^'
                case 0x05F: return '_'; //	'_'
                case 0x060: return '`'; //	'`'
                case 0x061: return 'a'; //	'a'
                case 0x062: return 'b'; //	'b'
                case 0x063: return 'c'; //	'c'
                case 0x064: return 'd'; //	'd'
                case 0x065: return 'e'; //	'e'
                case 0x066: return 'f'; //	'f'
                case 0x067: return 'g'; //	'g'
                case 0x068: return 'h'; //	'h'
                case 0x069: return 'i'; //	'i'
                case 0x06A: return 'j'; //	'j'
                case 0x06B: return 'k'; //	'k'
                case 0x06C: return 'l'; //	'l'
                case 0x06D: return 'm'; //	'm'
                case 0x06E: return 'n'; //	'n'
                case 0x06F: return 'o'; //	'o'
                case 0x070: return 'p'; //	'p'
                case 0x071: return 'q'; //	'q'
                case 0x072: return 'r'; //	'r'
                case 0x073: return 's'; //	's'
                case 0x074: return 't'; //	't'
                case 0x075: return 'u'; //	'u'
                case 0x076: return 'v'; //	'v'
                case 0x077: return 'w'; //	'w'
                case 0x078: return 'x'; //	'x'
                case 0x079: return 'y'; //	'y'
                case 0x07A: return 'z'; //	'z'
                                        //////////
                case 0x0a0: return 'Б'; //	'0'
                case 0x0a1: return 'Г'; //	'1'
                case 0x0a2: return 'ё'; //	'2'
                case 0x0a3: return 'Ж'; //	'3'
                case 0x0a4: return 'З'; //	'4'
                case 0x0a5: return 'И'; //	'5'
                case 0x0a6: return 'Й'; //	'6'
                case 0x0a7: return 'Л'; //	'7'
                case 0x0a8: return 'П'; //	'8'
                case 0x0a9: return 'У'; //	'9'
                case 0x0aA: return 'Ф'; //	':'
                case 0x0aB: return 'Ч'; //	';'
                case 0x0aC: return 'Ш'; //	'<'
                case 0x0aD: return 'Ъ'; //	'='
                case 0x0aE: return 'Ы'; //	'>'
                case 0x0aF: return 'Э'; //	'?'
                case 0x0b0: return 'Ю'; //	'@'
                case 0x0b1: return 'Я'; //	'A'
                case 0x0b2: return 'б'; //	'B'
                case 0x0b3: return 'в'; //	'C'
                case 0x0b4: return 'г'; //	'D'
                case 0x0b5: return 'Ё'; //	'E'
                case 0x0b6: return 'ж'; //	'F'
                case 0x0b7: return 'з'; //	'G'
                case 0x0b8: return 'и'; //	'H'
                case 0x0b9: return 'й'; //	'I'
                case 0x0bA: return 'к'; //	'J'
                case 0x0bB: return 'л'; //	'K'
                case 0x0bC: return 'м'; //	'L'
                case 0x0bD: return 'н'; //	'M'
                case 0x0bE: return 'п'; //	'N'
                case 0x0bF: return 'т'; //	'O'
                case 0x0c0: return 'ч'; //	'P'
                case 0x0c1: return 'ш'; //	'Q'
                case 0x0c2: return 'ъ'; //	'R'
                case 0x0c3: return 'ы'; //	'S'
                case 0x0c4: return 'ь'; //	'T'
                case 0x0c5: return 'э'; //	'U'
                case 0x0c6: return 'ю'; //	'V'
                case 0x0c7: return 'я'; //	'W'
                case 0x0e0: return 'Д'; //	'P'
                case 0x0e1: return 'Ц'; //	'Q'
                case 0x0e2: return 'Щ'; //	'R'
                case 0x0e3: return 'д'; //	'S'
                case 0x0e4: return 'ф'; //	'T'
                case 0x0e5: return 'ц'; //	'U'
                case 0x0e6: return 'щ'; //	'V'
                default: return ' ';
            }
        }

        #endregion

        #region [IDeviceVersion members]

        public Type[] Forms
        {
            get
            {
                StringsConfig.CurrentVersion = Common.VersionConverter(DeviceVersion);
                this._groupSetPointSize = Common.VersionConverter(DeviceVersion) >= 1.07
                    ? new GroupSetpoint108().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize
                    : new GroupSetpointStruct().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize;
                this.InitMemoryEntity();
                List<Type> forms = new List<Type>
                {
                    typeof (BSBGLEF),
                    typeof (Mr771AlarmJournalForm),
                    typeof (Mr771SystemJournalForm),
                    typeof (Mr771ConfigurationForm),
                    typeof (Mr771MeasuringForm),
                    typeof (Mr771OscilloscopeForm),
                    typeof (FaceSideDeviceForm),
                    typeof (FileSharingForm)
                    //typeof (AllInformationsDeviceForm)
                };

                if (Common.VersionConverter(DeviceVersion) >= 1.06)
                {
                    forms.Add(typeof (EmulationForm));
                }
                
                return forms.ToArray();
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.02",
                    "1.03",
                    "1.04",
                    "1.05",
                    "1.06",
                    "1.07",
                    "1.08",
                    "1.09",
                    "1.10",
                    "1.11",
                    "1.12"
                };
            }
        }
        #endregion    
        
        #region [IDeviceView members]
        public Type ClassType
        {
            get { return typeof(Mr771); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mrBig; }
        }

        public string NodeName
        {
            get { return NODE_NAME; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[]{}; }
        }

        public bool Deletable
        {
            get { return true; }
        }
        #endregion
    }
}
