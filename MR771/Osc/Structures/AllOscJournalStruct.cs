﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR771.Osc.Structures
{
    public class AllOscJournalStruct : StructBase
    {
        [Layout(0, Count = 40)] OscJournalStruct[] _allJournalStructs;

        public List<OscJournalStruct> AllJournalStructs
        {
            get { return new List<OscJournalStruct>(this._allJournalStructs.Where(j => !j.IsEmpty).ToArray()); }
        }
    }
}
