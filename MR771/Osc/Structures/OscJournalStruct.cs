﻿using System;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR771.AlarmJournal;
using BEMN.MR771.Configuration;

namespace BEMN.MR771.Osc.Structures
{
    public class OscJournalStruct : StructBase
    {
        #region [Constants]

        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}.{3:d3}";
        private const string NUMBER_PATTERN = "{0}";
        /// <summary>
        /// Размер одной страницы 
        /// </summary>
        private const int OSCLEN = 1024;
        public static int RecordIndex;
        
        #endregion [Constants]

        #region [Private fields]
        // 0 - 7 слова дата и время аварии
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _reserv;

        /// <summary>
        /// "READY" - признак готовности осциллограммы, если 0 - осциллограмма готова 
        /// </summary>
        [Layout(8)] private int _ready;

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        [Layout(9)] private int _point;

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        [Layout(10)] private int _begin;

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        [Layout(11)] private int _len;

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        [Layout(12)] private int _after;

        /// <summary>
        /// "ALM" Номер (последней) сработавшей защиты 
        /// </summary>
        [Layout(13)] private ushort _numberDefence;

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        [Layout(14)] private ushort _sizeReference;

        #endregion [Private fields]
        
        #region [Properties]
        /// <summary>
        /// Сработавшая защита
        /// </summary>
        public string Stage
        {
            get
            {
                return Validator.GetJornal(this._numberDefence, AjStrings.TriggeredDefense, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            }
        }

        public ushort GroupIndex
        {
            get { return (ushort)(Common.GetBits(this._numberDefence, 12, 13, 14, 15) >> 12); }
        }

        public string Group
        {
            get { return Validator.Get(this.GroupIndex, StringsConfig.GroupsNames); }
        }
        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                    {
                        this.GetNumber, //Номер
                        this.GetDate,
                        this.GetTime, //Время
                        this.Group,   //группа уставок
                        this.Stage,   //защита
                        this.Len, //Размер
                        this._ready, //Готовность
                        this.Point, //Адрес начала
                        this.GetEnd, //Адрес конца
                        this.Begin, //Смещение
                        this.After, //№ сработавшей защиты
                        this.SizeReference
                    };
            }
        }
        //пустая запись или осциллограмма не готова, тоже признак конца журнала
        public bool IsEmpty
        {
            get { return this.SizeReference == 0 || this._ready != 0; }
        }
        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return (ushort) (this.Point/OSCLEN); }
        }

        /// <summary>
        /// Номер соощения
        /// </summary>
        private string GetNumber
        {
            get
            {
                var result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        public int Year
        {
            get { return _year + 2000; }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        TIME_PATTERN,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                return string.Format
                    (
                        DATE_PATTERN,
                        this._date,
                        this._month,
                        this.Year
                    );
            }
        }

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                DateTime a = new DateTime(this.Year, this._month, this._date, this._hour, this._minute, this._second, this._millisecond);
                DateTime result = a.AddMilliseconds(-alarm);
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     result.Month,
                                     result.Day,
                                     result.Year,
                                     result.Hour,
                                     result.Minute,
                                     result.Second,
                                     result.Millisecond);

            }
            catch (Exception)
            {
                return GetFormattedDateTime;
            }

        }

        public string GetFormattedDateTime
        {
            get
            {
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     this._month,
                                     this._date,
                                     this.Year,
                                     this._hour,
                                     this._minute,
                                     this._second,
                                     this._millisecond);
            }
        }

        private static int OscSize = 0x77FF4;

        /// <summary>
        /// Адрес конца
        /// </summary>
        private string GetEnd
        {
            get
            {
                int num = OscSize;
                int num2 = this.Point + (this.Len*this.SizeReference);
                return num2 > num ? string.Format("{0} [{1}]", num2, num2 - num) : num2.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        public int Len
        {
            get { return _len; }
        }

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        public int After
        {
            get { return _after; }
        }

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        public int Begin
        {
            get { return _begin; }
        }

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        public int Point
        {
            get { return _point; }
        }

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeReference
        {
            get { return _sizeReference; }
        }

        #endregion [Private Properties]
    }
}
