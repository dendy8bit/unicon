﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR771.Osc.Structures;

namespace BEMN.MR771.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// С версии 1.07 возможность читать сразу все записи журнала осциллграфа
        /// </summary>
        private readonly MemoryEntity<AllOscJournalStruct> _allOscJournal; 
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        public OscJournalLoader(MemoryEntity<OscJournalStruct> oscJournal, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStruct>();
            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //запись индекса ЖО
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="allOscJournal">Журнал осциллографа</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        public OscJournalLoader(MemoryEntity<AllOscJournalStruct> allOscJournal, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStruct>();
            //Записи журнала
            this._allOscJournal = allOscJournal;
            this._allOscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadAllRecords);
            this._allOscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //запись индекса ЖО
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._allOscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Количество записей в журнале осциллографа
        /// </summary>
        public int OscCount
        {
            get { return this._oscRecords.Count; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return this._oscRecords; }
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStruct.RecordIndex = this.OscCount;
                this._oscRecords.Add(this._oscJournal.Value.Clone<OscJournalStruct>());
                this._recordNumber++;
                this.SaveIndex();
                if (this.ReadRecordOk == null) return;
                this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        }

        private void ReadAllRecords()
        {
            if (this._allOscJournal.Value.AllJournalStructs.Count != 0)
            {
                this._oscRecords.AddRange(this._allOscJournal.Value.AllJournalStructs);
                this._recordNumber += this._allOscJournal.Value.AllJournalStructs.Count;
                this.SaveIndex();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        }

        #endregion [Private MemoryEntity Events Handlers]

        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            OscJournalStruct.RecordIndex = 0;
            this.SaveIndex();
        }

        public void ClearEvents()
        {
            if (this._allOscJournal != null)
            {
                this._allOscJournal.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadAllRecords);
                this._allOscJournal.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
                this._refreshOscJournal.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this._allOscJournal.LoadStruct);
            }
            if (this._oscJournal != null)
            {
                this._oscJournal.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
                this._oscJournal.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
                this._refreshOscJournal.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            }
            this._refreshOscJournal.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        #endregion [Public members]

        internal void Clear()
        {
            this._oscRecords.Clear();
        }

        private void SaveIndex()
        {
            this._refreshOscJournal.Value.Word = (ushort)this._recordNumber;
            this._refreshOscJournal.SaveStruct6();
        }
    }
}
