using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR771.Configuration;
using BEMN.MR771.Configuration.Structures.Oscope;
using BEMN.MR771.Osc.HelpClasses;
using BEMN.MR771.Osc.Loaders;
using BEMN.MR771.Osc.ShowOsc;
using BEMN.MR771.Osc.Structures;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses.FileOperations;

namespace BEMN.MR771.Osc
{
    public partial class Mr771OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string READ_OSC_ERROR = "������ ������ �������������";

        private const string LIST_FILE_NAME = "commonp.xml";
        private const string LIST_FILE_NAME_OSC = "oscsign.xml";
        private const string DEVICE_NAME = "MR771";
        #endregion [Constants]

        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� ����� � ����������
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;

        private readonly MemoryEntity<OscOptionsStruct> _oscilloscopeSettings;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;

        private MemoryEntity<OscopeAllChannelsStruct> _oscopeStruct;
        private Mr771 _device;
        private FileDriver _fileDriver;
        private List<string> _signalsList = new List<string>();

        #endregion [Private fields]
        
        #region [Ctor's]
        public Mr771OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public Mr771OscilloscopeForm(Mr771 device)
        {
            this.InitializeComponent();
            this._device = device;

            Action failReadOscJournalDelegate = HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);

            //if (Common.VersionConverter(_device.DeviceVersion) >= 1.12)
            //{
            //    _fileDriver = new FileDriver(_device, this);

            //    StringsConfig.RelaySignals = StringsConfig.DefaultRelaySignals;
            //    StringsConfig.VlsSignals = StringsConfig.DefaultVLSSignals;

            //    StringsConfig.SignaturesForOsc = StringsConfig.DefaultSignaturesForOsc;
            //}

            // ��������� �������
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
            // ��������� �������
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && device.AllOscJournal != null)
            {
                this._oscJournalLoader = new OscJournalLoader(device.AllOscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
            }
            else
            {
                this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            }
            this._oscJournalLoader.ReadJournalFail += failReadOscJournalDelegate;
            // ������� �����������
            this._oscilloscopeSettings = device.OscOptions;
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            this._oscilloscopeSettings.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            // ������ ������������
            this._oscopeStruct = this._device.AllChannels;
            this._oscopeStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscilloscopeSettings.LoadStruct);
            this._oscopeStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            // ��������� ������� �����
            this._currentOptionsLoader = device.CurrentOptionsLoader;
            this._currentOptionsLoader.LoadOk += this._oscopeStruct.LoadStruct; // ������� ����������� ��������� ��������� ������� ����� ������ ���������
            this._currentOptionsLoader.LoadFail += failReadOscJournalDelegate;

            this._table = this.GetJournalDataTable(); // ��������� ����� �������
        }

        #endregion [Ctor's]
        
        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        private void ReadStop()
        {
            this._statusLabel.Text = this._pageLoader.Error ? READ_OSC_ERROR : READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this.EnableButtons = true;
            this._oscProgressBar.Value = 0;
        }

        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            this._oscilloscopeCountCb.Items.Add(this._oscJournalLoader.OscCount);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oscJournalLoader.OscCount);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void ReadAllRecords()
        {
            this.EnableButtons = true;
            if (this._oscJournalLoader.OscCount == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                return;
            }

            for (int i = 1; i <= this._oscJournalLoader.OscCount; i++)
            {
                this._oscilloscopeCountCb.Items.Add(i);
            }
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._oscJournalLoader.OscCount);
            foreach (OscJournalStruct record in this._oscJournalLoader.OscRecords)
            {
                this._table.Rows.Add(record.GetRecord);
                OscJournalStruct.RecordIndex++;
            }
            this._oscJournalDataGrid.Refresh();

            //if (Common.VersionConverter(_device.DeviceVersion) >= 1.12)
            //{
            //    if (!string.IsNullOrEmpty(Framework.Framework.PasswordForSignatures))
            //    {
            //        _fileDriver.Password = Framework.Framework.PasswordForSignatures;

            //        _fileDriver.ReadFile(SignaturesListRead, LIST_FILE_NAME);
            //    }
            //    else
            //    {
            //        MessageBox.Show("��� ������ �������� ��������, ��������� ������ ������ � ���������� �������!",
            //            "��������!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}
        }

        private void SignaturesListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "�������� ������� ���������")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfConfigurations), root);
                            ListsOfConfigurations lists = (ListsOfConfigurations)serializer.Deserialize(reader);

                            AddedItemInList(lists.SignalsList.MessagesList, StringsConfig.RelaySignals);
                            AddedItemInListVLS(lists.SignalsList.MessagesList, StringsConfig.VlsSignals);

                            _fileDriver.ReadFile(SignaturesListReadForOsc, LIST_FILE_NAME_OSC);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ ������ �������� ��������. " + mes + "\n������ ����� ������������� �� ���������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._statusLabel.Text = "������ ������ �������� ��������";
            }

            if (readBytes.Length != 0)
            {
                try
                {

                }
                catch (Exception ex)
                {
                }
            }
        }

        private void SignaturesListReadForOsc(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "�������� ������� ���������")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(SignaturesForOsc), root);
                            SignaturesForOsc lists = (SignaturesForOsc)serializer.Deserialize(reader);

                            StringsConfig.SignaturesForOsc = lists.Signatures.MessagesList;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ ������ �������� ��������. " + mes + "\n������ ����� ������������� �� ���������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._statusLabel.Text = "������ ������ �������� ��������";

                StringsConfig.SignaturesForOsc = StringsConfig.DefaultSignaturesForOsc;
            }

            if (readBytes.Length != 0)
            {
                try
                {

                }
                catch (Exception ex)
                {
                }
            }
        }

        public void AddedItemInList(List<string> signalsList, List<string> list)
        {
            list?.Clear();

            for (int i = 0; i < (signalsList.Count * 2) - 1; i++)
            {
                try
                {
                    if ((i % 2) == 0 && i != 0)
                    {
                        list.Add(signalsList[i / 2].Trim() + " ���.");
                    }
                    else
                    {
                        if (i == 0 || i == 1)
                        {
                            list.Add(signalsList[i].Trim());
                            continue;
                        }
                        list.Add(signalsList[(i + 1) / 2].Trim());
                    }
                }
                catch (Exception exception)
                {

                }
            }
        }

        public void AddedItemInListVLS(List<string> signalsList, List<string> list)
        {
            list?.Clear();

            for (int i = 0; i < signalsList.Count; i++)
            {
                if (i > 72 && i < 89 || i == 0) continue;

                try
                {
                    list.Add(signalsList[i].Trim());
                }
                catch (Exception exception)
                {

                }
            }
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.OscCount == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }

            this.EnableButtons = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            int group = this._journalStruct.GroupIndex;
            try
            {
                this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct,
                    this._currentOptionsLoader.Connections[group].Value, this._oscopeStruct.Value.Rows);
            }
            catch
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                this.EnableButtons = true;
            }

            this.EnableButtons = true;
            this._oscReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]
        
        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]
        
        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("��771_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]
        
        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            if (_device.IsConnect && _device.DeviceDlgInfo.IsConnectionMode) this.StartRead();
        }

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._pageLoader.ClearEvents();
            this._oscJournalLoader.ClearEvents();
        }

        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                MessageBox.Show("������������� �� ���������");
                return;
            }
            try
            {
                if (Validator.GetVersionFromRegistry())
                {
                    string fileName;
                    if (this._countingList.IsLoad)
                    {
                        fileName = this._countingList.FilePath;
                    }
                    else
                    {
                        fileName = Validator.CreateOscFileNameCfg($"��771 v{this._device.DeviceVersion} �������������");
                        this._countingList.Save(fileName, Common.VersionConverter(_device.DeviceVersion));
                    }
                    string oscPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe");
                    System.Diagnostics.Process.Start(oscPath, fileName);
                }
                else
                {
                    if (!Settings.Default.OscFlag)
                    {
                        LoadNetForm loadNetForm = new LoadNetForm();
                        loadNetForm.ShowDialog(this);
                        Settings.Default.OscFlag = loadNetForm.OscFlag;
                        Settings.Default.Save();
                    }

                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Mr771OscilloscopeResultForm resForm = new Mr771OscilloscopeResultForm(this.CountingList);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }
        private void StartRead()
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Clear();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._currentOptionsLoader.StartRead(); // ������ ������ ������
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._countingList.Save(this._saveOscilloscopeDlg.FileName, Common.VersionConverter(_device.DeviceVersion));
                    this._statusLabel.Text = "������������ ���������";
                }
                catch (Exception)
                {
                    this._statusLabel.Text = "������ ����������";
                }
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}", this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        #endregion [Event Handlers]
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr771); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr771OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return Mr771OscilloscopeForm.OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]
    }
}