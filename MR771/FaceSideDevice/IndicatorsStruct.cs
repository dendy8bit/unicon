﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR771.FaceSideDevice
{
    public class IndicatorsStruct : StructBase
    {
        [Layout(0, Count = 2)] private ushort[] _indicators;

        public virtual List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._indicators, 0, 31); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i = i + 2)    // выделяем попарно состояния одного диода
                {
                    retList.Add(new[] { allIndArray[i], allIndArray[i + 1] });// (bool зеленый, bool красный)
                }
                return retList;
            }
        }
    }
}
