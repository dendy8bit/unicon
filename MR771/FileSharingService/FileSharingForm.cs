﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.Devices.FileOperationsForFileSharing;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.MR771.FileSharingService
{
    public partial class FileSharingForm : Form, IFormView
    {
        private FileDriver _fileDriver;
        private string _file;
        private MessageBoxForm _messageBoxForm;
        private OpenFileDialog _opnDialog;
        private PasswordForm _passwordForm;
        private string _progFile = @"progMod.obj";
        private Mr771 _device;
        private bool _isChooseFirmware;
        private string _selectedText;

        private string _comannds = "Команды:\n" +
                                   "\n- GETELEMENTDIR (прочитать текущий элемент директории);\n" +
                                   "- CREATEDIR (создание директории);\n" +
                                   "- NEWPASSWORD (смена пароля)";

        private string _pass = "4321";

        public FileSharingForm()
        {
            this.InitializeComponent();
        }

        public FileSharingForm(Mr771 device)
        {
            this.InitializeComponent();

            _device = device;

            this._fileDriver = new FileDriver(device, this);

            _opnDialog = new OpenFileDialog();
            _opnDialog.Filter = @"obj File (*.obj)|*.obj";

            _messageBoxForm = new MessageBoxForm();

            _passwordForm = new PasswordForm();
        }

        private bool IsVisibleElement
        {
            set
            {
                _backgroundImagePanel.Visible = !value;
                _passButton.Visible = !value;
                _passTextBox.Visible = !value;
                _aboutFunctionLabel.Visible = !value;
            }
        }
        private bool IsEnabledElement
        {
            set
            {
                _pathToFileGroupBox.Enabled = value;
                _passButton.Enabled = value;
                _passTextBox.Enabled = value;
                _aboutFunctionLabel.Enabled = value;
            }
        }

        private bool IsVisiblePassForUser
        {
            set
            {
                _passwordMTB.Visible = !value;
                _changePasswordBTN.Visible = !value;
            }
        }

        private void OnAllFilesReadOk(List<string> arg1, string arg2)
        {
            if (arg2 == "Операция успешно выполнена")
            {
                foreach (var elemnt in arg1)
                {
                    filesInDeviceTreeView.Nodes.Add(elemnt);
                }

                _deleteButton.Enabled = true;
            }
            else
            {
                MessageBox.Show(arg2, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CommandOperationOk(string arg1, string arg2)
        {
            if (arg2 == "Операция успешно выполнена")
            {
                MessageBox.Show("Команда выполнена успешно!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(arg2, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnSendCommandComplete(string arg1, string arg2)
        {
            if (arg2 == "Операция успешно выполнена")
            {
                filesInDeviceTreeView.Nodes.Add(arg1);
            }
            else
            {
                MessageBox.Show(arg2, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private byte[] ReadDataFromFile(string file)
        {
            byte[] wBytes;
            using (FileStream fileStream = new FileStream(file, FileMode.Open))
            {
                // преобразуем строку в байты
                wBytes = new byte[fileStream.Length];
                // считываем данные
                fileStream.Read(wBytes, 0, wBytes.Length);
            }
            return wBytes;
        }

        private void OnFileWriteOk(bool res, string message)
        {
            switch (message)
            {
                case "Операция успешно выполнена":
                    _fileDriver.WriteFile(OnFilesWriteOk, ReadDataFromFile(_progFile), Path.GetFileName(_progFile));
                    _messageBoxForm.ShowDialog("Идет запись файла загрузчика");
                    break;
                case "Не найден путь":
                    _fileDriver.SendCommand(CreateReadOk, "CREATEDIR Boot");
                    _messageBoxForm.ShowMessage("Создание каталога");
                    break;
                default:
                    _messageBoxForm.ShowResultMessage(message);
                    break;
            }
        }

        private void CreateReadOk(string str1, string str2)
        {
            if (str2 == "Операция успешно выполнена")
            {
                _fileDriver.WriteFile(OnFileWriteOk, ReadDataFromFile(_file), Path.GetFileName(_file));
            }
            else
            {
                _messageBoxForm.ShowResultMessage(str2);
            }
        }
    
        private void OnFilesWriteOk(bool res, string message)
        {
            if (message == "Операция успешно выполнена")
            {
                _messageBoxForm.ShowResultMessage("Запись файлов успешно выполнена!");
                writeToDeviceBtn.Enabled = false;
            }
            else
            {
                _messageBoxForm.ShowResultMessage(message);
                writeToDeviceBtn.Enabled = false;
            }
        }

        private void OnFileDeleteOk(bool res, string message)
        {
            if (message == "Операция успешно выполнена")
            {
                MessageBox.Show("Удаление файла прошло успешно.", "Удаление файла", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show($"Удаление файла завершилось с ошибкой. {message}", "Удаление файла", MessageBoxButtons.OK);
            }
        }

        private void FileSharingForm_Load(object sender, EventArgs e)
        {
            IsEnabledElement = _device.DeviceDlgInfo.IsConnectionMode;
        }

        private void readFileNamesInDevice_Click(object sender, EventArgs e)
        {
            try
            {
                filesInDeviceTreeView.Nodes.Clear();
                _fileDriver.GetAllElementInDir(OnAllFilesReadOk, "GETELEMENTDIR 0:");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }


        private void writeToDeviceBtn_Click(object sender, EventArgs e)
        {
            if (_passwordForm.ShowDialog() == DialogResult.OK)
            {
                _fileDriver.Password = _passwordForm.Password;

                try
                {
                    _fileDriver.WriteFile(OnFileWriteOk, ReadDataFromFile(_file), Path.GetFileName(_file));

                    _messageBoxForm.SetProgressBarStyle(ProgressBarStyle.Marquee);
                    _messageBoxForm.Caption = "Запись файлов";
                    _messageBoxForm.OkBtnVisibility = false;
                    _messageBoxForm.ShowMessage("Идет запись файла: " + _file);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void _sendCommandButton_Click(object sender, EventArgs e)
        {
            _fileDriver.SendCommand(OnSendCommandComplete, _pathTextBox.Text);
        }

        private void readFromDeviceBtn_Click(object sender, EventArgs e)
        {
            if (_opnDialog.ShowDialog() == DialogResult.OK)
            {
                _file = _opnDialog.FileName;
                pathInPCTextBox.Text = _file;
                writeToDeviceBtn.Enabled = true;
            }
        }

        private void _changePasswordButton_Click(object sender, EventArgs e)
        {
            try
            {
                string[] passwords;
                passwords = _pathTextBox.Text.Split();

                if (!IsPasswordValid(passwords))
                {
                    MessageBox.Show("Чтобы изменить пароль, введите в поле команды старый пароль и новый пароль через пробел: \n(Например: АААА ББББ)", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                _fileDriver.OldPassword = passwords.First();
                _fileDriver.Password = passwords.Last();
                _fileDriver.SendCommand(CommandOperationOk, "NEWPASSWORD");
            }
            catch (Exception ex)
            {
               
            }
            
        }

        private bool IsPasswordValid(string[] passwords)
        {
            for (int i = 0; i < passwords.Length; i++)
            {
                if (passwords[i].Length < 4 || passwords[i].Length > 4) return false;
            }

            return true;
        }
        
        private void _passButton_Click(object sender, EventArgs e)
        {
            IsVisiblePassForUser = _passTextBox.Text == _pass;
            IsVisibleElement = _passTextBox.Text == _pass;
            _pathTextBox.Clear();
        }

        private void _infoCommandsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_comannds, "Поддерживаемые команды", MessageBoxButtons.OK);
        }

        private void _changePasswordBTN_Click(object sender, EventArgs e)
        {
            try
            {
                string[] passwords;
                passwords = _passwordMTB.Text.Split();

                if (!IsPasswordValid(passwords))
                {
                    MessageBox.Show("Чтобы изменить пароль, введите в поле команды старый пароль и новый пароль через пробел: \n(Например: АААА ББББ)", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                _fileDriver.OldPassword = passwords.First();
                _fileDriver.Password = passwords.Last();
                _fileDriver.SendCommand(CommandOperationOk, "NEWPASSWORD");
            }
            catch (Exception ex)
            {

            }
        }
        private void filesInDeviceTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                _selectedText = string.Concat(e.Node.Text.TakeWhile(chr => chr != ';'));
            }
            catch {}
        }

        private void _deleteButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_selectedText)) return;
            if (MessageBox.Show("Удалить выбранный файл?", "Внимание!", MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question) == DialogResult.OK)
            {
                if (_passwordForm.ShowDialog() == DialogResult.OK)
                {
                    _fileDriver.Password = _passwordForm.Password;

                    _fileDriver.DeleteFile(OnFileDeleteOk, _selectedText);
                }
            }
            
        }
        
        #region [IFormView Members]

        public Type FormDevice => typeof(Mr771);

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes => new INodeView[] { };

        public Type ClassType => typeof(FileSharingForm);

        public bool Deletable => false;

        public bool ForceShow => false;

        public Image NodeImage => Properties.Resources.filecervice;

        public string NodeName => "Работа с файлами";

        #endregion [IFormView Members]

    }
}
