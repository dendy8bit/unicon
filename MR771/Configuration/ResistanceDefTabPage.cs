﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.MR771.Configuration.Structures.Defenses.Z;

namespace BEMN.MR771.Configuration
{
    public partial class ResistanceDefTabPage : UserControl
    {
        private const string PRIMARY_STR = "r,Ом перв.";
        private const string SECOND_STR = "r,Ом втор.";
        private NewStructValidator<ResistanceDefensesStruct> _resistDefValidator;
        private NewStructValidator<ResistanceDefensesStructNew> _resistDefValidatorNew;
        private bool _isPrimary;

        public ResistanceDefTabPage()
        {
            this.InitializeComponent();
        }

        public void Initialization()
        {
            if (StringsConfig.CurrentVersion < 1.05)
            {
                this.apvCheck.Visible = true;
                this.ApvCombo.Visible = false;
            }

            if (StringsConfig.CurrentVersion >= 1.11)
            {
                this.label23.Visible = false;
                this._resetStep.Visible = false;
                this.label24.Visible = false;
            }

            var currentDefenseFunc1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new SignValidatingRule(-256.00, 256.00);
                    }
                    return RulesContainer.Ustavka256;
                }
                catch (Exception)
                {
                    return RulesContainer.Ustavka256;
                }
            });

            var currentDefenseFunc2 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return RulesContainer.Ustavka256;
                    }
                    return new CustomUshortRule(0, 89);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(0, 89);
                }
            });

            if (StringsConfig.CurrentVersion >= 1.11)
            {
                this._resistDefValidatorNew = new NewStructValidator<ResistanceDefensesStructNew>
                (
                    this.toolTip,
                    new ControlInfoCombo(this.modeCombo, StringsConfig.DefenseModes),
                    new ControlInfoCombo(this.typeCombo, StringsConfig.ResistDefType),
                    new ControlInfoCombo(this.blockCombo, StringsConfig.SwitchSignals),
                    new ControlInfoTextDependent(this.rTextBox1, currentDefenseFunc1),
                    new ControlInfoTextDependent(this.xTextBox1, currentDefenseFunc1),
                    new ControlInfoTextDependent(this.cornerRadTextBox1, currentDefenseFunc2),
                    new ControlInfoCombo(this.directionCombo, StringsConfig.ResistDefDirection),
                    new ControlInfoCheck(this.uStartCheck),
                    new ControlInfoText(this.uStartMaskedText, RulesContainer.Ustavka256),
                    new ControlInfoText(this.icpTextBox, RulesContainer.Ustavka40),
                    new ControlInfoText(this.tcpTextBox, RulesContainer.TimeRule),
                    new ControlInfoCombo(this.inpAccelerationCombo, StringsConfig.RelaySignals),
                    new ControlInfoText(this.tyTextBox, RulesContainer.TimeRule),
                    new ControlInfoCombo(this.logicCombo, StringsConfig.ContureV2),
                    new ControlInfoCombo(this.blockFromTnCmb, StringsConfig.BlockTn),
                    new ControlInfoCheck(this.blockFromLoadCheck),
                    new ControlInfoCheck(this.blockFromSwingCheck),
                    new ControlInfoCheck(this._damageFaza),
                    new ControlInfoCombo(this.oscilloscopeCombo, StringsConfig.OscModes),
                    new ControlInfoCheck(this.urovCheck),
                    new ControlInfoCombo(this.ApvCombo, StringsConfig.Apv),
                    new ControlInfoCheck(this._steeredModeAccelerChBox)
                );
            }
            else
            {
                this._resistDefValidator = new NewStructValidator<ResistanceDefensesStruct>
                (
                    this.toolTip,
                    new ControlInfoCombo(this.modeCombo, StringsConfig.DefenseModes),
                    new ControlInfoCombo(this.typeCombo, StringsConfig.ResistDefType),
                    new ControlInfoCombo(this.blockCombo, StringsConfig.SwitchSignals),
                    new ControlInfoTextDependent(this.rTextBox1, currentDefenseFunc1),
                    new ControlInfoTextDependent(this.xTextBox1, currentDefenseFunc1),
                    new ControlInfoTextDependent(this.cornerRadTextBox1, currentDefenseFunc2),
                    new ControlInfoCombo(this.directionCombo, StringsConfig.ResistDefDirection),
                    new ControlInfoCheck(this.uStartCheck),
                    new ControlInfoText(this.uStartMaskedText, RulesContainer.Ustavka256),
                    new ControlInfoText(this.icpTextBox, RulesContainer.Ustavka40),
                    new ControlInfoText(this.tcpTextBox, RulesContainer.TimeRule),
                    new ControlInfoCombo(this.inpAccelerationCombo, StringsConfig.RelaySignals),
                    new ControlInfoText(this.tyTextBox, RulesContainer.TimeRule),
                    new ControlInfoCombo(this.logicCombo, StringsConfig.ContureV2),
                    new ControlInfoCombo(this.blockFromTnCmb, StringsConfig.BlockTn),
                    new ControlInfoCheck(this.blockFromLoadCheck),
                    new ControlInfoCheck(this.blockFromSwingCheck),
                    new ControlInfoCheck(this._damageFaza),
                    new ControlInfoCheck(this._resetStep),
                    new ControlInfoCombo(this.oscilloscopeCombo, StringsConfig.OscModes),
                    new ControlInfoCheck(this.urovCheck),
                    new ControlInfoCheck(this.apvCheck),
                    new ControlInfoCombo(this.ApvCombo, StringsConfig.Apv),
                    new ControlInfoCheck(this._steeredModeAccelerChBox)
                );
            }
        }

        public NewStructValidator<ResistanceDefensesStruct> ResistDefValidator => this._resistDefValidator;
        public NewStructValidator<ResistanceDefensesStructNew> ResistDefValidatorNew => this._resistDefValidatorNew;

        public void RefreshValueInComboBox(List<string> list)
        {
            inpAccelerationCombo.Items.Clear();

            foreach (var t in list)
            {
                this.inpAccelerationCombo.Items.Add(t);
            }
        }

        [Browsable(false)]
        public bool IsPrimary
        {
            get { return this._isPrimary; }
            set
            {
                this._isPrimary = value;
                this.panel1.Visible = this.cornerRadTextBox1.Visible = !value;
                this.panel2.Visible = this.cornerRadTextBox2.Visible = value;
                if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
                {
                    this._frLabel.Text = @"f, град.";
                }
                else
                {
                    this._frLabel.Text = value ? PRIMARY_STR : SECOND_STR;
                }
            }
        }

        // Режим
        public string Mode => this.modeCombo.SelectedItem.ToString();

        // Тип
        public string Type => this.typeCombo.SelectedItem.ToString();

        // Блокировка
        public string Block => this.blockCombo.SelectedItem.ToString();

        // R, Ом втор.
        public string R => this.IsPrimary ? this.rTextBox2.Text : this.rTextBox1.Text;

        // X, Ом втор.
        public string X => this.IsPrimary ? this.xTextBox2.Text : this.xTextBox1.Text;

        // f, град
        public string CornerRadius => this.IsPrimary ? this.cornerRadTextBox2.Text : this.cornerRadTextBox1.Text;

        // tcp, мс
        public string Tcp => this.tcpTextBox.Text;

        // Icp, In
        public string Icp => this.icpTextBox.Text;

        // Вход ускорения
        public string Accleration => this.inpAccelerationCombo.SelectedItem.ToString();

        // ty, мс
        public string Tu => this.tyTextBox.Text;

        // Направление
        public string Direction => this.directionCombo.SelectedItem.ToString();

        // Uпуск, В (checkBox)
        public bool StartOnU => this.uStartCheck.Checked;

        // Uпуск, В (textBox)
        public string Ustart => this.uStartMaskedText.Text;

        // Контур
        public string Logic => this.logicCombo.SelectedItem.ToString();

        // Блок.от неиспр.ТН
        public string BlockFromTn => this.blockFromTnCmb.SelectedItem.ToString();

        // Блок. от нагрузки
        public bool BlockFromLoad => this.blockFromLoadCheck.Checked;

        // Блок. от качания
        public bool BlockFromSwing => this.blockFromSwingCheck.Checked;

        // Ненапр. при уск.
        public bool NonDirectBoost => this._steeredModeAccelerChBox.Checked;

        // Пуск от ОПФ
        public bool DamageFaza => this._damageFaza.Checked;

        // Сброс 1фКЗ при мфКЗ*
        public bool ResetStep => this._resetStep.Checked;

        // Осциллограф
        public string Oscilloscope => this.oscilloscopeCombo.SelectedItem.ToString();
        
        // УРОВ
        public bool Urov => this.urovCheck.Checked;

        // АПВ
        public string Apv
        {
            get
            {
                if (StringsConfig.CurrentVersion >= 1.05)
                {
                    return this.ApvCombo.SelectedItem.ToString();
                }
                return this.apvCheck.Checked ? "Есть" : "Нет";
            }
        }

        // Ненаправ. при ускор.
        public bool SteeredModeAcceler => this._steeredModeAccelerChBox.Checked;

        public void GetPrimaryValue(double koef)
        {
            this._frLabel.Text = this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0] ? "f, град." : "r, Ом перв.";
            double val = Math.Round(Convert.ToDouble(this.rTextBox1.Text) * koef, 2);
            this.rTextBox2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            val = Math.Round(Convert.ToDouble(this.xTextBox1.Text) * koef, 2);
            this.xTextBox2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
            {
                val = Math.Round(Convert.ToDouble(this.cornerRadTextBox1.Text)*koef, 2);
                this.cornerRadTextBox2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            else
            {
                this.cornerRadTextBox2.Text = this.cornerRadTextBox1.Text;
            }
        }

        public void GetSecondValue(double koef)
        {
            this._frLabel.Text = this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0] ? "f, град." : "r, Ом втор.";
            double val = Math.Round(Convert.ToDouble(this.rTextBox2.Text) * koef, 2);
            this.rTextBox1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            val = Math.Round(Convert.ToDouble(this.xTextBox2.Text) * koef, 2);
            this.xTextBox1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
            {
                val = Math.Round(Convert.ToDouble(this.cornerRadTextBox2.Text) * koef, 2);
                this.cornerRadTextBox1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            else
            {
                this.cornerRadTextBox1.Text = this.cornerRadTextBox2.Text;
            }
        }

        private void typeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this._frLabel.Text = @"f, град.";
            }
            else
            {
                this._frLabel.Text = !this.IsPrimary ? "r, Ом втор." : "r, Ом перв.";
            }
        }     
    }
}
