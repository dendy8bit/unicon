﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR771.Configuration.Structures.ConfigSystem;
using BEMN.MR771.Configuration.Structures.Goose;
using BEMN.MR771.Configuration.Structures.InputSignals;
using BEMN.MR771.Configuration.Structures.Oscope;
using BEMN.MR771.Configuration.Structures.RelayInd;
using BEMN.MR771.Configuration.Structures.Switch;
using BEMN.MR771.Configuration.Structures.UROV;

namespace BEMN.MR771.Configuration.Structures
{
    [Serializable]  
    [XmlRoot(ElementName = "МР771")]
    public class ConfigurationStruct111 : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType
        {
            get { return "МР771"; }
        }

        [XmlElement(ElementName = "Первичные_уставки")]
        public bool Primary { get; set; }

        [XmlElement(ElementName = "Размер_осциллограмы")]
        public string SizeOsc { get; set; }

        [XmlElement(ElementName = "Группа")]
        public int Group { get; set; }

        #region [Private fields]

        [Layout(0)] private AllGroupSetpointStruct111 _allGroupSetpoints;
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [Layout(1)] private SwitchStruct _sw;
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [Layout(2)] private InputSignalStruct _impsg;
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [Layout(3)] private OscopeStruct _osc;
        /// <summary>
        /// Параметры автоматики
        /// </summary>
        [Layout(4)] private AutomaticsParametersStruct _automatics;
        /// <summary>
        /// конфигурациия RS485
        /// </summary>
        [Layout(5, Ignore = true)] private ConfigNetStruct _cnfRS485;
        /// <summary>
        /// конфигурациия Ethernet
        /// </summary>
        [Layout(6)] private ConfigIPAddress _ipAddress;
        [Layout(7, Count = 3, Ignore = true)] private ushort[] _macAddress;
        /// <summary>
        /// Вход опорного канала
        /// </summary>
        [Layout(8)] private ConfigAddStruct _cnfAdd;
        /// <summary>
        /// УРОВ
        /// </summary>
        [Layout(9)] private UrovStruct _urov;
        /// <summary>
        /// Конфигурация гусов
        /// </summary>
        [Layout(10)] private GooseConfig _gooseConfig;

        //[Layout(9, Count = 128)] ushort[] _reserve1;
        //[Layout(10, Count = 384)] ushort[] _reserve2;

        #endregion
        /// <summary>
        /// Группы уставок
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_всех_групп_уставок")]
        [BindingProperty(0)]
        public AllGroupSetpointStruct111 AllGroupSetpoints
        {
            get { return this._allGroupSetpoints; }
            set { this._allGroupSetpoints = value; }
        }
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(1)]
        public SwitchStruct Sw
        {
            get { return this._sw; }
            set { this._sw = value; }
        }
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(2)]
        public InputSignalStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(3)]
        public OscopeStruct Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }
        [XmlElement(ElementName = "Конфигурация_реле,индикаторов,неисправностей")]
        [BindingProperty(4)]
        public AutomaticsParametersStruct Automatics
        {
            get { return this._automatics; }
            set { this._automatics = value; }
        }

        [XmlElement(ElementName = "IP")]
        [BindingProperty(5)]
        public ConfigIPAddress IP
        {
            get { return this._ipAddress; }
            set { this._ipAddress = value; }
        }

        [XmlElement(ElementName = "Вход_опорного_канала")]
        [BindingProperty(6)]
        public ConfigAddStruct ConfigAdd
        {
            get { return this._cnfAdd; }
            set { this._cnfAdd = value; }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        [XmlElement(ElementName = "УРОВ")]
        [BindingProperty(7)]
        public UrovStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }
        /// <summary>
        /// Конфигурация гусов
        /// </summary>
        [XmlElement(ElementName = "Гусы")]
        [BindingProperty(8)]
        public GooseConfig GooseConfig
        {
            get { return this._gooseConfig; }
            set { this._gooseConfig = value; }
        }

    }
}
