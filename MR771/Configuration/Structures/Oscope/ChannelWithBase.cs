﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR771.Configuration.Structures.Oscope
{
    public class ChannelWithBase : StructBase
    {
        private byte _base;
        private ushort _channel;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "База")]
        public string BaseStr
        {
            get { return Validator.Get(this._base, StringsConfig.OscBases); }
            set { this._base = (byte)Validator.Set(value, StringsConfig.OscBases); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get
            {
                List<string> list = StringsConfig.OscChannelSignals[this._base];
                return Validator.Get(this._channel, list);
            }
            set
            {
                List<string> list = StringsConfig.OscChannelSignals[this._base];
                this._channel = Validator.Set(value, list);
            }
        }

        public byte Base
        {
            get { return this._base; }
            set { this._base = value; }
        }

        public ushort Channel
        {
            get { return this._channel; }
            set { this._channel = value; }
        }
    }
}
