﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR771.Configuration.Structures.Sihronizm
{
    /// <summary>
    /// конфигурациия улавливания синхронизма (УС)
    /// </summary>
    public class SinhronizmStruct : StructBase 
    {
        #region [Private fields]

        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _automaticBlock; // блокировка
        [Layout(2)] private ushort _ustUminno; //уставка порог отсутствия напряжения
        [Layout(3)] private ushort _ustUmin; //уставка min уровень напряжения
        [Layout(4)] private ushort _ustUmax; //уставка max уровень напряжения
        [Layout(5)] private ushort _twait; //время ожидания условий синхронизма
        [Layout(6)] private ushort _ton; //время включения выключателя (для несинхронного режима)
        [Layout(7)] private ushort _tdelay; //время задержки (для синхронного режима)
        [Layout(8)] private SinhronizmAddStruct _manual; //группа для ручного включения
        [Layout(9)] private SinhronizmAddStruct _automatic; //группа для автоматического включения
        [Layout(10)] private ushort _automaticIn1;
        [Layout(11)] private ushort _automaticIn2;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Umin. отс, В
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Umin")]
        public double UminOts
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ustUminno); }
            set { this._ustUminno = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// Umin. нал, В
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Umin_нал")]
        public double UminNal
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ustUmin); }
            set { this._ustUmin = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// Umax. нал, В
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Umax_нал")]
        public double UmaxNal
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ustUmax); }
            set { this._ustUmax = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// tож, мс
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "tож")]
        public int Twait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._twait); }
            set { this._twait = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// tсинхр, мс
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "tсинхр")]
        public int Tsinhr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tdelay); }
            set { this._tdelay = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// t вкл, мс
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "tвкл")]
        public ushort Ton
        {
            get { return this._ton; }
            set { this._ton = value; }
        }


        /// <summary>
        /// U1
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "U1")]
        public string U1Xml
        {
            get { return Validator.Get(this._config, StringsConfig.Usinhr, 0, 1, 2); }
            set { this._config = Validator.Set(value, StringsConfig.Usinhr, this._config, 0, 1, 2); }
        }

        /// <summary>
        /// U2
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "U2")]
        public string U2Xml
        {
            get { return Validator.Get(this._config, StringsConfig.Usinhr, 3, 4, 5); }
            set { this._config = Validator.Set(value, StringsConfig.Usinhr, this._config, 3, 4, 5); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "группа_для_ручного_включения")]
        public SinhronizmAddStruct Manual
        {
            get { return this._manual; }
            set { this._manual = value; }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "группа_для_автоматического_включения")]
        public SinhronizmAddStruct Automatic
        {
            get { return this._automatic; }
            set { this._automatic = value; }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "блокировка")]
        public string Block
        {
            get { return Validator.Get(this._automaticBlock, StringsConfig.SwitchSignals); }
            set { this._automaticBlock = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "дискрет_U1_нет_U2_есть")]
        public string Discret1
        {
            get { return Validator.Get(this._automaticIn1, StringsConfig.SwitchSignals); }
            set { this._automaticIn1 = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "дискрет_U1_есть_U2_нет")]
        public string Discret2
        {
            get { return Validator.Get(this._automaticIn2, StringsConfig.SwitchSignals); }
            set { this._automaticIn2 = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Камп")]
        public double Kamp
        {
            get { return ValuesConverterCommon.GetUstavka256(this._manual.Param); }
            set { this._manual.Param = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "f")]
        public ushort F
        {
            get { return this._automatic.Param; }
            set { this._automatic.Param = value; }
        }
        #endregion [Properties]
    }
}
