﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR771.Configuration.Structures
{
    public class AllGroupSetpointStruct111 : StructBase, ISetpointContainer<GroupSetpoint111>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpoint111[] _groupSetpoints;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint111[] Setpoints
        {
            get
            {
                var res = new GroupSetpoint111[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpoint111>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
