﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR771.Configuration.Structures.AcCountLoad;
using BEMN.MR771.Configuration.Structures.Apv;
using BEMN.MR771.Configuration.Structures.CheckTn;
using BEMN.MR771.Configuration.Structures.Defenses;
using BEMN.MR771.Configuration.Structures.Defenses.Z;
using BEMN.MR771.Configuration.Structures.Engine;
using BEMN.MR771.Configuration.Structures.HFL;
using BEMN.MR771.Configuration.Structures.Ls;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;
using BEMN.MR771.Configuration.Structures.Omp;
using BEMN.MR771.Configuration.Structures.Sihronizm;
using BEMN.MR771.Configuration.Structures.Swing;
using BEMN.MR771.Configuration.Structures.Vls;

namespace BEMN.MR771.Configuration.Structures
{
    public class GroupSetpoint108 : StructBase
    {
        #region Fields
        [Layout(0)] private DefensesSetpointsStruct _defensesSetpoints;
        [Layout(1)] private ResistanceStruct _resistanceParam;
        [Layout(2)] private AcCountLoadStruct _acCountLoad;
        [Layout(3)] private CheckTnStruct _checkTn;
        [Layout(4)] private SwingStruct _swing;
        [Layout(5)] private ApvStruct _apv;
        [Layout(6)] private TermConfigStruct _termConfig;
        [Layout(7)] private MeasureTransStruct _measureTrans;
        [Layout(8)] private AllInputLogicStruct _inputLogicSignal;
        [Layout(9)] private AllOutputLogicSignalStruct _outputLogicSignal;
        [Layout(10)] private SinhronizmStruct _sinhronizm;
        [Layout(11)] private ConfigurationOpmStruct _omp;
        [Layout(12)] private DzStruct _hfl1;
        [Layout(13)] private TznpStruct _hfl2;
        #endregion

        #region Property
        [BindingProperty(0)]
        public DefensesSetpointsStruct DefensesSetpoints
        {
            get { return this._defensesSetpoints; }
            set { this._defensesSetpoints = value; }
        }

        [BindingProperty(1)]
        public ResistanceStruct ResistanceParam
        {
            get { return this._resistanceParam; }
            set { this._resistanceParam = value; }
        }

        [BindingProperty(2)]
        public AcCountLoadStruct AcCountLoad
        {
            get { return this._acCountLoad; }
            set { this._acCountLoad = value; }
        }

        [BindingProperty(3)]
        public CheckTnStruct CheckTn
        {
            get { return this._checkTn; }
            set { this._checkTn = value; }
        }

        [BindingProperty(4)]
        public SwingStruct Swing
        {
            get { return this._swing; }
            set { this._swing = value; }
        }

        [BindingProperty(5)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }

        [BindingProperty(6)]
        public TermConfigStruct TermConfig
        {
            get { return this._termConfig; }
            set { this._termConfig = value; }
        }

        [BindingProperty(7)]
        public MeasureTransStruct MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }

        [BindingProperty(8)]
        public AllInputLogicStruct InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }

        [BindingProperty(9)]
        public AllOutputLogicSignalStruct OutputLogicSignal
        {
            get { return this._outputLogicSignal; }
            set { this._outputLogicSignal = value; }
        }

        [BindingProperty(10)]
        public SinhronizmStruct Sinhronizm
        {
            get { return this._sinhronizm; }
            set { this._sinhronizm = value; }
        }

        [BindingProperty(11)]
        public ConfigurationOpmStruct Omp
        {
            get { return this._omp; }
            set { this._omp = value; }
        }

        [BindingProperty(12)]
        public DzStruct Hfl1
        {
            get { return this._hfl1; }
            set { this._hfl1 = value; }
        }

        [BindingProperty(13)]
        public TznpStruct Hfl2
        {
            get { return this._hfl2; }
            set { this._hfl2 = value; }
        }

        #endregion Property
    }
}
