﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR771.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        #region [Public field]

        [XmlElement(ElementName = "канал_I_защиты")] [Layout(0)] private KanalITransStruct _i1; //канал I защиты
        [XmlElement(ElementName = "канал_U_защиты")] [Layout(1)] private KanalUTransStruct _u1; //канал U защиты 

        #endregion [Public field]
        /// <summary>
        /// Канал I
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Канал_I")]
        public KanalITransStruct ChannelI
        {
            get { return this._i1; }
            set { this._i1 = value; }
        }
        /// <summary>
        /// Канал U
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Канал_U")]
        public KanalUTransStruct ChannelU
        {
            get { return this._u1; }
            set { this._u1 = value; }
        }
    }
}
