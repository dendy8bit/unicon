﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR771.Configuration.Structures.HFL
{
    public class TznpStruct : StructBase
    {
        [Layout(0)] private ushort _config;             //конфигурация
        [Layout(1)] private ushort _hfs;                //вход высокочастотного сигнала
        [Layout(2)] private ushort _time_echo;          //время задержки ВЧС (tср ТС)
        [Layout(3)] private ushort _umin_echo;          //пуск по напряжению
        [Layout(4)] private ushort _block_echo;         //блокировка логики эхо сигнала
        [Layout(5)] private ushort _time_hfs;           //время возврата ВЧС (tвз ТС)
        [Layout(6)] private ushort _ctrl_hls;           //телесигнал контроля ВЧ по ДЗ или по ТЗНП
        [Layout(7)] private ushort _time_initdeblock;   //время перезапуска схемы деблокировки
        [Layout(8)] private ushort _time_fics;          //время фиксации ступеней с сокр. и обратной зоной в схемах блок.
        [Layout(9)] private ushort _time_off;           //импульс отключения выкл.
        [Layout(10)] private ushort _time_fics_ext;     //время фиксации ИО для расширеной зоны в логике ВЧБ
        [Layout(11)] private ushort _block_off;         //блокировка выходного сигнала отключения
        [Layout(12)] private ushort _block_hfs;         //блокировка выходного сигнала отправки ВЧ
        [Layout(13)] private ushort _time_deblock;      //время срабатывания схемы деблокировки
        [Layout(14)] private ushort _block_reverse;     //блокировка логики определения реверса тока
        [Layout(15)] private ushort _abbreviatf;        //сокращенная зона F-F для ДЗ, или сокращенная зона для ТЗНП
        [Layout(16)] private ushort _extendf;           //расширеная зона F-F для ДЗ, или расширеная зона для ТЗНП
        [Layout(17)] private ushort _reversef;          //обратная зона F-F для ДЗ, или обратная зона для ТЗНП
        [Layout(18)] private ushort _abbreviatn;        //сокращенная зона F-N для ДЗ, или резерв для ТЗНП
        [Layout(19)] private ushort _extendn;           //расширеная зона F-N для ДЗ, или резерв для ТЗНП
        [Layout(20)] private ushort _reversen;          //обратная зона F- N для ДЗ, или резерв для ТЗНП
        [Layout(21)] private ushort _time_rev;          //время реверса

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим_Tznp")]
        public string TznpMode
        {
            get { return Validator.Get(this._config, StringsConfig.HflModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.HflModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Аббрев")]
        public string Abbrev
        {
            get { return Validator.Get(this._abbreviatf > 0? (ushort)(this._abbreviatf - 170) : this._abbreviatf, StringsConfig.HflSignalsTZNP); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsTZNP);
                this._abbreviatf = val > 0 ? (ushort)(val + 170) : val;
            }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Расширение")]
        public string Extend
        {
            get { return Validator.Get(this._extendf > 0 ? (ushort)(this._extendf - 170) : this._extendf, StringsConfig.HflSignalsTZNP); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsTZNP);
                this._extendf = val > 0 ? (ushort)(val + 170) : val;
            }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Реверс")]
        public string Reverse
        {
            get { return Validator.Get(this._reversef > 0 ? (ushort)(this._reversef - 170) : this._reversef, StringsConfig.HflSignalsTZNP); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsTZNP);
                this._reversef = val > 0 ? (ushort)(val + 170) : val;
            }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "ТС")]
        public string TS
        {
            get { return Validator.Get(this._hfs, StringsConfig.SwitchSignals); }
            set { this._hfs = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Время_возврата_ТС")]
        public int TimeReturnTs
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_hfs); }
            set { this._time_hfs = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Деблок")]
        public string Deblock
        {
            get { return Validator.Get(this._config, StringsConfig.HflDeblock, 4, 5); }
            set { this._config = Validator.Set(value, StringsConfig.HflDeblock, this._config, 4, 5); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Контроль_ТС")]
        public string ControlTS
        {
            get { return Validator.Get(this._ctrl_hls, StringsConfig.SwitchSignals); }
            set { this._ctrl_hls = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Время_деблока")]
        public int TimeDeblock
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_initdeblock); }
            set { this._time_initdeblock = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Время_мин_импульса")]
        public int TimeMinImpulse
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_fics); }
            set { this._time_fics = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Время_выкл")]
        public int TimeOff
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_off); }
            set { this._time_off = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Время_выкл_ТС")]
        public int TimeOffTB
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_fics_ext); }
            set { this._time_fics_ext = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Блок_выкл")]
        public string BlockOff
        {
            get { return Validator.Get(this._block_off, StringsConfig.SwitchSignals); }
            set { this._block_off = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Блок_ТС")]
        public string BlockTS
        {
            get { return Validator.Get(this._block_hfs, StringsConfig.SwitchSignals); }
            set { this._block_hfs = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Направление")]
        public string Direction
        {
            get { return Validator.Get(this._config, StringsConfig.HflDirection, 10); }
            set { this._config = Validator.Set(value, StringsConfig.HflDirection, this._config, 10); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config, StringsConfig.Apv, 14); }
            set { this._config = Validator.Set(value, StringsConfig.Apv, this._config, 14); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "Уров")]
        public string UROV
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn, 12); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 12); }
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "АПВ")]
        public string APV
        {
            get { return Validator.Get(this._config, StringsConfig.Apv, 13); }
            set { this._config = Validator.Set(value, StringsConfig.Apv, this._config, 13); }
        }

        [BindingProperty(18)]
        [XmlElement(ElementName = "Режим_эхо")]
        public string ModeEcho
        {
            get { return Validator.Get(this._config, StringsConfig.HflEcho, 2, 3); }
            set { this._config = Validator.Set(value, StringsConfig.HflEcho, this._config, 2, 3); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "ТС_вкл")]
        public int OnTS
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_echo); }
            set { this._time_echo = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(20)]
        [XmlElement(ElementName = "Umin")]
        public double Umin
        {
            get { return ValuesConverterCommon.GetUstavka256(this._umin_echo); }
            set { this._umin_echo = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(21)]
        [XmlElement(ElementName = "Блок_эхо")]
        public string BlockEcho
        {
            get { return Validator.Get(this._block_echo, StringsConfig.SwitchSignals); }
            set { this._block_echo = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(22)]
        [XmlElement(ElementName = "Блок_режим")]
        public string BlockMode
        {
            get { return Validator.Get(this._config, StringsConfig.BlockTn, 8, 9); }
            set { this._config = Validator.Set(value, StringsConfig.BlockTn, this._config, 8, 9); }
        }

        [BindingProperty(23)]
        [XmlElement(ElementName = "Режим_реверс")]
        public string ModeReverse
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn, 6); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 6); }
        }

        [BindingProperty(24)]
        [XmlElement(ElementName = "Т_деблока")]
        public int Tdeblock
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_deblock); }
            set { this._time_deblock = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(25)]
        [XmlElement(ElementName = "Тфикс")]
        public int Tfics
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_rev); }
            set { this._time_rev = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(26)]
        [XmlElement(ElementName = "Сокр_зона")]
        public bool Zone
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }

        [BindingProperty(27)]
        [XmlElement(ElementName = "Блок_реверс")]
        public string BlockReverse
        {
            get { return Validator.Get(this._block_reverse, StringsConfig.SwitchSignals); }
            set { this._block_reverse = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
