﻿using System.Drawing;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR771.Configuration.Structures.AcCountLoad;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;
using BEMN.MR771.Configuration.Structures.Swing;

namespace BEMN.MR771.Configuration.Structures.Defenses.Z
{
    public class ConfigResistDiagramStructNew : StructBase
    {
        private ResistanceStruct _resistanceStruct;
        private AcCountLoadStructNew _acCountLoadStruct;
        private SwingStruct _oscillationStruct;
        private AllResistanceDefensesStructNew _resistanceDefensesStruct;
        private MeasureTransStruct _measureTransStruct;

        [BindingProperty(0)]
        public ResistanceStruct ResistanceStruct
        {
            get { return this._resistanceStruct; }
            set { this._resistanceStruct = value; }
        }

        [BindingProperty(1)]
        public AcCountLoadStructNew AcCountLoadStruct
        {
            get { return this._acCountLoadStruct; }
            set { this._acCountLoadStruct = value; }
        }

        [BindingProperty(2)]
        public SwingStruct Oscillation

        {
            get { return this._oscillationStruct; }
            set { this._oscillationStruct = value; }
        }

        [BindingProperty(3)]
        public AllResistanceDefensesStructNew ResistanceDefensesStructNew
        {
            get { return this._resistanceDefensesStruct; }
            set { this._resistanceDefensesStruct = value; }
        }

        [BindingProperty(4)]
        public MeasureTransStruct MeasureTrans
        {
            get { return this._measureTransStruct; }
            set { this._measureTransStruct = value; }
        }

        public double KoefMisha
        {
            get
            {
                return this._measureTransStruct.ChannelU.KthlValue * this._measureTransStruct.ChannelI.InpInValue /
                       this._measureTransStruct.ChannelI.Ittl;
            }
        }
        public double Ittl
        {
            get { return this._measureTransStruct.ChannelI.Ittl; }
        }
        #region SaveOptions

        public PieChartOptions GetPieChartOptions()
        {
            PieChartOptions options = new PieChartOptions();
            this.SetChannelPieChartOptions(options);
            this.SetZnOptions(options);
            this.SetPieChartDirectionCharacteristicOption(options);
            this.SetSwingOptions(options);
            this.SetPieChartDefChaaracteristics(options);

            return options;
        }

        private void SetChannelPieChartOptions(PieChartOptions options)
        {
            options.IaChannel = new ChannelPieChartOptions("Ia")
            {
                A = true
            };
            options.IbChannel = new ChannelPieChartOptions("Ib")
            {
                B = true
            };
            options.IcChannel = new ChannelPieChartOptions("Ic")
            {
                C = true
            };

            options.UaChannel = new ChannelPieChartOptions("Ua")
            {
                A = true
            };
            options.UbChannel = new ChannelPieChartOptions("Ub")
            {
                B = true
            };
            options.UcChannel = new ChannelPieChartOptions("Uc")
            {
                C = true
            };
        }

        private void SetZnOptions(PieChartOptions options)
        {
            ZnOptions z1 = new ZnOptions()
            {
                R0 = this.ResistanceStruct.R0Step1,
                X0 = this.ResistanceStruct.X0Step1,
                R1 = this.ResistanceStruct.R1Step1,
                X1 = this.ResistanceStruct.X1Step1
            };
            options.Z1 = z1; // значнеия R0, R1, X0, X1

            ZnOptions z2 = new ZnOptions()
            {
                R0 = this.ResistanceStruct.R0Step2,
                X0 = this.ResistanceStruct.X0Step2,
                R1 = this.ResistanceStruct.R1Step2,
                X1 = this.ResistanceStruct.X1Step2
            };
            options.Z2 = z2; // значнеия R0, R1, X0, X1

            ZnOptions z3 = new ZnOptions()
            {
                R0 = this.ResistanceStruct.R0Step3,
                X0 = this.ResistanceStruct.X0Step3,
                R1 = this.ResistanceStruct.R1Step3,
                X1 = this.ResistanceStruct.X1Step3
            };
            options.Z3 = z3; // значнеия R0, R1, X0, X1

            ZnOptions z4 = new ZnOptions()
            {
                R0 = this.ResistanceStruct.R0Step4,
                X0 = this.ResistanceStruct.X0Step4,
                R1 = this.ResistanceStruct.R1Step4,
                X1 = this.ResistanceStruct.X1Step4
            };
            options.Z4 = z4; // значнеия R0, R1, X0, X1

            ZnOptions z5 = new ZnOptions()
            {
                R0 = this.ResistanceStruct.R0Step5,
                X0 = this.ResistanceStruct.X0Step5,
                R1 = this.ResistanceStruct.R1Step5,
                X1 = this.ResistanceStruct.X1Step5
            };
            options.Z5 = z5; // значнеия R0, R1, X0, X1
        }

        private void SetPieChartDirectionCharacteristicOption(PieChartOptions options)
        {
            PieChartDirectionCharacteristicOption corners = new PieChartDirectionCharacteristicOption();
            corners.F1 = this._resistanceStruct.C1; // у1, град.
            corners.F2 = this._resistanceStruct.C2; // у2, град.
            options.Corners = corners;

            PieChartChargeCharacteristicOption lin = new PieChartChargeCharacteristicOption();
            lin.R1 = this._acCountLoadStruct.R1Line; // R1, Ом перв.
            lin.R2 = this._acCountLoadStruct.R2Line; // R2, Ом перв.
            lin.F = this._acCountLoadStruct.CornerLine;  // у1, град.
            PieChartChargeCharacteristicOption faz = new PieChartChargeCharacteristicOption();
            //faz.R1 = this._acCountLoadStruct.R1Faza;
            //faz.R2 = this._acCountLoadStruct.R2Faza;
            //faz.F = this._acCountLoadStruct.CornerFaza;
            options.Linear = lin;
            options.Phase = faz;
        }

        private void SetSwingOptions(PieChartOptions options)
        {
            if (this._oscillationStruct.Type == StringsConfig.ResistDefType[0]) // если Полигональная
            {
                PieChartPolyCharacteristicOption oscillationPoly = new PieChartPolyCharacteristicOption();
                oscillationPoly.R = this._oscillationStruct.R1;
                oscillationPoly.X = this._oscillationStruct.X1;
                oscillationPoly.F = this._oscillationStruct.C1;

                oscillationPoly.BlockFromLoad = Block.NONE;
                oscillationPoly.Direction = Direction.NONE;
                oscillationPoly.Color = Color.FromArgb(-12703965);
                options.Kachanie = oscillationPoly;

                if ((this._oscillationStruct.R1 != 0) && (this._oscillationStruct.X1 != 0))
                {


                    oscillationPoly = new PieChartPolyCharacteristicOption();
                    oscillationPoly.R = (this._oscillationStruct.R1 + this._oscillationStruct.Dr); // Dr - dz, Ом перв.
                    oscillationPoly.X = (this._oscillationStruct.X1 + this._oscillationStruct.Dr); // Dr - dz, Ом перв. 
                    oscillationPoly.F = this._oscillationStruct.C1;

                    oscillationPoly.BlockFromLoad = Block.NONE;
                    oscillationPoly.Direction = Direction.NONE;
                    oscillationPoly.Color = Color.FromArgb(-12703965);
                }
                options.Kachanie2 = oscillationPoly;
            }
            else
            {
                PieChartRoundCharacteristicOption oscillationRound = new PieChartRoundCharacteristicOption();
                oscillationRound.R = this._oscillationStruct.R1;
                oscillationRound.X = this._oscillationStruct.X1;
                oscillationRound.Radius = this._oscillationStruct.C1;
                oscillationRound.BlockFromLoad = Block.NONE;
                oscillationRound.Direction = Direction.NONE;
                oscillationRound.Color = Color.FromArgb(-12703965);
                options.Kachanie = oscillationRound;

                if (this._oscillationStruct.C1 != 0)
                {
                    oscillationRound = new PieChartRoundCharacteristicOption();
                    oscillationRound.R = this._oscillationStruct.R1;
                    oscillationRound.X = this._oscillationStruct.X1;
                    oscillationRound.Radius = (this._oscillationStruct.C1 + this._oscillationStruct.Dr);
                    oscillationRound.BlockFromLoad = Block.NONE;
                    oscillationRound.Color = Color.FromArgb(-12703965);
                    oscillationRound.Direction = Direction.NONE;
                }
                options.Kachanie2 = oscillationRound;
            }
        }

        private void SetPieChartDefChaaracteristics(PieChartOptions options)
        {
            Color[] colors = new Color[]
                {
                    Color.FromArgb(0x50dc143c),
                    Color.FromArgb(0x50aa00ff),
                    Color.FromArgb(0x502196f3),
                    Color.FromArgb(0x5000796b),
                    Color.FromArgb(0x50cddc39),
                    Color.FromArgb(0x50ff9800),
                    Color.FromArgb(0x50607d8b),
                    Color.FromArgb(0x508d6e63),
                    Color.FromArgb(0x5064dd17),
                    Color.FromArgb(0x50ffff00)
                };
            for (int i = 0; i < AllResistanceDefensesStructNew.RESIST_DEF_COUNT; i++)
            {
                if (this._resistanceDefensesStruct[i].Mode == StringsConfig.DefenseModes[0])
                    continue;
                if (this._resistanceDefensesStruct[i].Type == StringsConfig.ResistDefType[0]) // если Полигональная
                {
                    PieChartPolyCharacteristicOption poly = new PieChartPolyCharacteristicOption();
                    poly.R = this._resistanceDefensesStruct[i].UstR;
                    poly.X = this._resistanceDefensesStruct[i].UstX;
                    poly.F = this._resistanceDefensesStruct[i].Ustr;
                    poly.BlockFromLoad = this._resistanceDefensesStruct[i].BlockFromLoad
                        ? this.GetBlockEnum(this._resistanceDefensesStruct[i].ContureInd)
                        : Block.NONE;
                    poly.Direction = this.GetDirectionEnum(this._resistanceDefensesStruct[i].DirectionInd);
                    poly.Name = string.Format("Ступень Z {0}", i + 1);
                    poly.Color = colors[i];
                    options.Characteristics.Add(poly);

                }
                else
                {
                    PieChartRoundCharacteristicOption round = new PieChartRoundCharacteristicOption();
                    round.R = this._resistanceDefensesStruct[i].UstR;
                    round.X = this._resistanceDefensesStruct[i].UstX;
                    round.Radius = this._resistanceDefensesStruct[i].Ustr;
                    round.BlockFromLoad = this._resistanceDefensesStruct[i].BlockFromLoad
                        ? this.GetBlockEnum(this._resistanceDefensesStruct[i].ContureInd)
                        : Block.NONE;
                    round.Direction = this.GetDirectionEnum(this._resistanceDefensesStruct[i].DirectionInd);
                    round.Name = string.Format("Ступень Z {0}", i + 1);
                    round.Color = colors[i];
                    options.Characteristics.Add(round);

                }
            }
        }

        private Block GetBlockEnum(int ind)
        {
            return ind == 0 ? Block.LINE : Block.PHASE;
        }

        private Direction GetDirectionEnum(int ind)
        {
            switch (ind)
            {
                case 0: return Direction.NONE;
                case 1: return Direction.DIRECT;
                case 2: return Direction.INVERSION;
                default: return Direction.NONE;
            }
        }
        #endregion SaveOptions
    }
}

