﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR771.Configuration.Structures.Defenses.Q
{
    /// <summary>
    /// блокировка по тепловой модели
    /// </summary>

    [Serializable]
    [XmlRoot(ElementName = "блокировка_по_тепловой_модели")]
    public class DefenseTermBlockStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] ushort _config;				//конфигурация
        [Layout(1)] ushort _val;				//уставка срабатывания
        [Layout(2)] ushort _wait;				//время блокировки
        [Layout(3, Count = 5)] ushort[] _rez; 
        #endregion [Private fields]


        #region [Properties]
     
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn); }
        }
        /// <summary>
        /// уставка срабатывания
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "уставка_срабатывания")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetUstavka256(this._val); }
            set { this._val = ValuesConverterCommon.SetUstavka256(value); }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
         [BindingProperty(2)]
         [XmlElement(ElementName = "Время_срабатывания")]
        public ushort Time
        {
            get { return this._wait; }
            set { this._wait = value; }
        } 
        #endregion [Properties]


   
    }
}
