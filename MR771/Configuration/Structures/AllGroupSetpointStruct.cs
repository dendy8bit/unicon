﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR771.Configuration.Structures
{
    public class AllGroupSetpointStruct : StructBase, ISetpointContainer<GroupSetpointStruct>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpointStruct[] _groupSetpoints;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpointStruct[] Setpoints
        {
            get
            {
                var res = new GroupSetpointStruct[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpointStruct>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
