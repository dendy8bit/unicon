﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR771.Configuration.Structures.Engine
{
    /// <summary>
    /// Конфигурация тепловой модели
    /// </summary>
    [XmlRoot(ElementName = "конфигурациия_тепловой_модели")]
    public class TermConfigStruct : StructBase 
    {
        #region [Private fields]

        [Layout(0)] private ushort _config;             //привязка к стороне 0,1,2 РЕЗЕРВ!!!
        [Layout(1)] private ushort _timeHot;            //постоянная времени нагрева
        [Layout(2)] private ushort _timeCold;           //постоянная времени охлаждения
        [Layout(3)] private ushort _termNom;            //Iдоп
        [Layout(4, Count = 3)] private ushort[] _res1;
        [Layout(5)] private ushort _reset;              //вход сброса тепловой модели (теплового состояния)
        [Layout(6, Count = 2)] private ushort[] _res; 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// постоянная времени нагрева
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Tгор")]
        public ushort TimeHot
        {
            get { return this._timeHot; }
            set { this._timeHot = value; }
        }
        /// <summary>
        /// постоянная времени охлаждения
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Tхол")]
        public ushort TimeCold
        {
            get { return this._timeCold; }
            set { this._timeCold = value; }
        }
        /// <summary>
        /// постоянная времени охлаждения
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Iдоп")]
        public double Iadd
        {
            get { return ValuesConverterCommon.GetIn(this._termNom); }
            set { this._termNom = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Q сброс
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Q_сброс")]
        public string QresetXml
        {
            get { return Validator.Get(this._reset, StringsConfig.SwitchSignals); }
            set { this._reset = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        #endregion [Properties]

    }
}
