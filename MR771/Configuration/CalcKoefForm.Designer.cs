﻿namespace BEMN.MR771.Configuration
{
    partial class CalcKoefForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Ko1Amp = new System.Windows.Forms.Label();
            this.closeBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Ko1Angle = new System.Windows.Forms.Label();
            this.Z0Z1Amp1 = new System.Windows.Forms.Label();
            this.Ko2Amp = new System.Windows.Forms.Label();
            this.Z0Z1Amp2 = new System.Windows.Forms.Label();
            this.Ko3Amp = new System.Windows.Forms.Label();
            this.Z0Z1Amp3 = new System.Windows.Forms.Label();
            this.Ko4Amp = new System.Windows.Forms.Label();
            this.Z0Z1Amp4 = new System.Windows.Forms.Label();
            this.Ko5Amp = new System.Windows.Forms.Label();
            this.Z0Z1Amp5 = new System.Windows.Forms.Label();
            this.Z0Z1Angle1 = new System.Windows.Forms.Label();
            this.Ko2Angle = new System.Windows.Forms.Label();
            this.Z0Z1Angle2 = new System.Windows.Forms.Label();
            this.Ko3Angle = new System.Windows.Forms.Label();
            this.Z0Z1Angle3 = new System.Windows.Forms.Label();
            this.Ko4Angle = new System.Windows.Forms.Label();
            this.Z0Z1Angle4 = new System.Windows.Forms.Label();
            this.Ko5Angle = new System.Windows.Forms.Label();
            this.Z0Z1Angle5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(156, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Z0/Z1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ko";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko1Amp
            // 
            this.Ko1Amp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko1Amp.AutoSize = true;
            this.Ko1Amp.Location = new System.Drawing.Point(308, 26);
            this.Ko1Amp.Name = "Ko1Amp";
            this.Ko1Amp.Size = new System.Drawing.Size(145, 24);
            this.Ko1Amp.TabIndex = 0;
            this.Ko1Amp.Text = "label1";
            this.Ko1Amp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // closeBtn
            // 
            this.closeBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeBtn.Location = new System.Drawing.Point(543, 302);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(75, 23);
            this.closeBtn.TabIndex = 1;
            this.closeBtn.Text = "OK";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.CloseClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label13, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label14, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label16, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label17, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label18, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label19, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label20, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label22, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.Ko1Angle, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.Ko1Amp, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Amp1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.Ko2Amp, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Amp2, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.Ko3Amp, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Amp3, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.Ko4Amp, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Amp4, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.Ko5Amp, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Amp5, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Angle1, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.Ko2Angle, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Angle2, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.Ko3Angle, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Angle3, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.Ko4Angle, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Angle4, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.Ko5Angle, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.Z0Z1Angle5, 3, 10);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(610, 280);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(308, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(145, 24);
            this.label11.TabIndex = 0;
            this.label11.Text = "Амплитуда";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(460, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(146, 24);
            this.label13.TabIndex = 0;
            this.label13.Text = "Угол, градусы";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(156, 76);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "Ko";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(156, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 24);
            this.label14.TabIndex = 0;
            this.label14.Text = "Ko";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(156, 176);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(145, 24);
            this.label15.TabIndex = 0;
            this.label15.Text = "Ko";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(156, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 24);
            this.label16.TabIndex = 0;
            this.label16.Text = "Ko";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(156, 101);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 24);
            this.label17.TabIndex = 0;
            this.label17.Text = "Z0/Z1";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(156, 151);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(145, 24);
            this.label18.TabIndex = 0;
            this.label18.Text = "Z0/Z1";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(156, 201);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(145, 24);
            this.label19.TabIndex = 0;
            this.label19.Text = "Z0/Z1";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(156, 251);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 28);
            this.label20.TabIndex = 0;
            this.label20.Text = "Z0/Z1";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 26);
            this.label21.Name = "label21";
            this.tableLayoutPanel1.SetRowSpan(this.label21, 2);
            this.label21.Size = new System.Drawing.Size(145, 49);
            this.label21.TabIndex = 0;
            this.label21.Text = "Зона Ф-N1";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 76);
            this.label22.Name = "label22";
            this.tableLayoutPanel1.SetRowSpan(this.label22, 2);
            this.label22.Size = new System.Drawing.Size(145, 49);
            this.label22.TabIndex = 0;
            this.label22.Text = "Зона Ф-N2";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(4, 126);
            this.label23.Name = "label23";
            this.tableLayoutPanel1.SetRowSpan(this.label23, 2);
            this.label23.Size = new System.Drawing.Size(145, 49);
            this.label23.TabIndex = 0;
            this.label23.Text = "Зона Ф-N3";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(4, 176);
            this.label24.Name = "label24";
            this.tableLayoutPanel1.SetRowSpan(this.label24, 2);
            this.label24.Size = new System.Drawing.Size(145, 49);
            this.label24.TabIndex = 0;
            this.label24.Text = "Зона Ф-N4";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 226);
            this.label25.Name = "label25";
            this.tableLayoutPanel1.SetRowSpan(this.label25, 2);
            this.label25.Size = new System.Drawing.Size(145, 53);
            this.label25.TabIndex = 0;
            this.label25.Text = "Зона Ф-N5";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko1Angle
            // 
            this.Ko1Angle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko1Angle.AutoSize = true;
            this.Ko1Angle.Location = new System.Drawing.Point(460, 26);
            this.Ko1Angle.Name = "Ko1Angle";
            this.Ko1Angle.Size = new System.Drawing.Size(146, 24);
            this.Ko1Angle.TabIndex = 0;
            this.Ko1Angle.Text = "label1";
            this.Ko1Angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Amp1
            // 
            this.Z0Z1Amp1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Amp1.AutoSize = true;
            this.Z0Z1Amp1.Location = new System.Drawing.Point(308, 51);
            this.Z0Z1Amp1.Name = "Z0Z1Amp1";
            this.Z0Z1Amp1.Size = new System.Drawing.Size(145, 24);
            this.Z0Z1Amp1.TabIndex = 0;
            this.Z0Z1Amp1.Text = "label1";
            this.Z0Z1Amp1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko2Amp
            // 
            this.Ko2Amp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko2Amp.AutoSize = true;
            this.Ko2Amp.Location = new System.Drawing.Point(308, 76);
            this.Ko2Amp.Name = "Ko2Amp";
            this.Ko2Amp.Size = new System.Drawing.Size(145, 24);
            this.Ko2Amp.TabIndex = 0;
            this.Ko2Amp.Text = "label1";
            this.Ko2Amp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Amp2
            // 
            this.Z0Z1Amp2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Amp2.AutoSize = true;
            this.Z0Z1Amp2.Location = new System.Drawing.Point(308, 101);
            this.Z0Z1Amp2.Name = "Z0Z1Amp2";
            this.Z0Z1Amp2.Size = new System.Drawing.Size(145, 24);
            this.Z0Z1Amp2.TabIndex = 0;
            this.Z0Z1Amp2.Text = "label1";
            this.Z0Z1Amp2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko3Amp
            // 
            this.Ko3Amp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko3Amp.AutoSize = true;
            this.Ko3Amp.Location = new System.Drawing.Point(308, 126);
            this.Ko3Amp.Name = "Ko3Amp";
            this.Ko3Amp.Size = new System.Drawing.Size(145, 24);
            this.Ko3Amp.TabIndex = 0;
            this.Ko3Amp.Text = "label1";
            this.Ko3Amp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Amp3
            // 
            this.Z0Z1Amp3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Amp3.AutoSize = true;
            this.Z0Z1Amp3.Location = new System.Drawing.Point(308, 151);
            this.Z0Z1Amp3.Name = "Z0Z1Amp3";
            this.Z0Z1Amp3.Size = new System.Drawing.Size(145, 24);
            this.Z0Z1Amp3.TabIndex = 0;
            this.Z0Z1Amp3.Text = "label1";
            this.Z0Z1Amp3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko4Amp
            // 
            this.Ko4Amp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko4Amp.AutoSize = true;
            this.Ko4Amp.Location = new System.Drawing.Point(308, 176);
            this.Ko4Amp.Name = "Ko4Amp";
            this.Ko4Amp.Size = new System.Drawing.Size(145, 24);
            this.Ko4Amp.TabIndex = 0;
            this.Ko4Amp.Text = "label1";
            this.Ko4Amp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Amp4
            // 
            this.Z0Z1Amp4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Amp4.AutoSize = true;
            this.Z0Z1Amp4.Location = new System.Drawing.Point(308, 201);
            this.Z0Z1Amp4.Name = "Z0Z1Amp4";
            this.Z0Z1Amp4.Size = new System.Drawing.Size(145, 24);
            this.Z0Z1Amp4.TabIndex = 0;
            this.Z0Z1Amp4.Text = "label1";
            this.Z0Z1Amp4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko5Amp
            // 
            this.Ko5Amp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko5Amp.AutoSize = true;
            this.Ko5Amp.Location = new System.Drawing.Point(308, 226);
            this.Ko5Amp.Name = "Ko5Amp";
            this.Ko5Amp.Size = new System.Drawing.Size(145, 24);
            this.Ko5Amp.TabIndex = 0;
            this.Ko5Amp.Text = "label1";
            this.Ko5Amp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Amp5
            // 
            this.Z0Z1Amp5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Amp5.AutoSize = true;
            this.Z0Z1Amp5.Location = new System.Drawing.Point(308, 251);
            this.Z0Z1Amp5.Name = "Z0Z1Amp5";
            this.Z0Z1Amp5.Size = new System.Drawing.Size(145, 28);
            this.Z0Z1Amp5.TabIndex = 0;
            this.Z0Z1Amp5.Text = "label1";
            this.Z0Z1Amp5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Angle1
            // 
            this.Z0Z1Angle1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Angle1.AutoSize = true;
            this.Z0Z1Angle1.Location = new System.Drawing.Point(460, 51);
            this.Z0Z1Angle1.Name = "Z0Z1Angle1";
            this.Z0Z1Angle1.Size = new System.Drawing.Size(146, 24);
            this.Z0Z1Angle1.TabIndex = 0;
            this.Z0Z1Angle1.Text = "label1";
            this.Z0Z1Angle1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko2Angle
            // 
            this.Ko2Angle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko2Angle.AutoSize = true;
            this.Ko2Angle.Location = new System.Drawing.Point(460, 76);
            this.Ko2Angle.Name = "Ko2Angle";
            this.Ko2Angle.Size = new System.Drawing.Size(146, 24);
            this.Ko2Angle.TabIndex = 0;
            this.Ko2Angle.Text = "label1";
            this.Ko2Angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Angle2
            // 
            this.Z0Z1Angle2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Angle2.AutoSize = true;
            this.Z0Z1Angle2.Location = new System.Drawing.Point(460, 101);
            this.Z0Z1Angle2.Name = "Z0Z1Angle2";
            this.Z0Z1Angle2.Size = new System.Drawing.Size(146, 24);
            this.Z0Z1Angle2.TabIndex = 0;
            this.Z0Z1Angle2.Text = "label1";
            this.Z0Z1Angle2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko3Angle
            // 
            this.Ko3Angle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko3Angle.AutoSize = true;
            this.Ko3Angle.Location = new System.Drawing.Point(460, 126);
            this.Ko3Angle.Name = "Ko3Angle";
            this.Ko3Angle.Size = new System.Drawing.Size(146, 24);
            this.Ko3Angle.TabIndex = 0;
            this.Ko3Angle.Text = "label1";
            this.Ko3Angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Angle3
            // 
            this.Z0Z1Angle3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Angle3.AutoSize = true;
            this.Z0Z1Angle3.Location = new System.Drawing.Point(460, 151);
            this.Z0Z1Angle3.Name = "Z0Z1Angle3";
            this.Z0Z1Angle3.Size = new System.Drawing.Size(146, 24);
            this.Z0Z1Angle3.TabIndex = 0;
            this.Z0Z1Angle3.Text = "label1";
            this.Z0Z1Angle3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko4Angle
            // 
            this.Ko4Angle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko4Angle.AutoSize = true;
            this.Ko4Angle.Location = new System.Drawing.Point(460, 176);
            this.Ko4Angle.Name = "Ko4Angle";
            this.Ko4Angle.Size = new System.Drawing.Size(146, 24);
            this.Ko4Angle.TabIndex = 0;
            this.Ko4Angle.Text = "label1";
            this.Ko4Angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Angle4
            // 
            this.Z0Z1Angle4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Angle4.AutoSize = true;
            this.Z0Z1Angle4.Location = new System.Drawing.Point(460, 201);
            this.Z0Z1Angle4.Name = "Z0Z1Angle4";
            this.Z0Z1Angle4.Size = new System.Drawing.Size(146, 24);
            this.Z0Z1Angle4.TabIndex = 0;
            this.Z0Z1Angle4.Text = "label1";
            this.Z0Z1Angle4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ko5Angle
            // 
            this.Ko5Angle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ko5Angle.AutoSize = true;
            this.Ko5Angle.Location = new System.Drawing.Point(460, 226);
            this.Ko5Angle.Name = "Ko5Angle";
            this.Ko5Angle.Size = new System.Drawing.Size(146, 24);
            this.Ko5Angle.TabIndex = 0;
            this.Ko5Angle.Text = "label1";
            this.Ko5Angle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z0Z1Angle5
            // 
            this.Z0Z1Angle5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Z0Z1Angle5.AutoSize = true;
            this.Z0Z1Angle5.Location = new System.Drawing.Point(460, 251);
            this.Z0Z1Angle5.Name = "Z0Z1Angle5";
            this.Z0Z1Angle5.Size = new System.Drawing.Size(146, 28);
            this.Z0Z1Angle5.TabIndex = 0;
            this.Z0Z1Angle5.Text = "label1";
            this.Z0Z1Angle5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CalcKoefForm
            // 
            this.AcceptButton = this.closeBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.closeBtn;
            this.ClientSize = new System.Drawing.Size(634, 337);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.closeBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CalcKoefForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Коэффициенты";
            this.Load += new System.EventHandler(this.CalcKoefForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Label Ko1Amp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label Ko1Angle;
        private System.Windows.Forms.Label Z0Z1Amp1;
        private System.Windows.Forms.Label Ko2Amp;
        private System.Windows.Forms.Label Z0Z1Amp2;
        private System.Windows.Forms.Label Ko3Amp;
        private System.Windows.Forms.Label Z0Z1Amp3;
        private System.Windows.Forms.Label Ko4Amp;
        private System.Windows.Forms.Label Z0Z1Amp4;
        private System.Windows.Forms.Label Ko5Amp;
        private System.Windows.Forms.Label Z0Z1Amp5;
        private System.Windows.Forms.Label Z0Z1Angle1;
        private System.Windows.Forms.Label Ko2Angle;
        private System.Windows.Forms.Label Z0Z1Angle2;
        private System.Windows.Forms.Label Ko3Angle;
        private System.Windows.Forms.Label Z0Z1Angle3;
        private System.Windows.Forms.Label Ko4Angle;
        private System.Windows.Forms.Label Z0Z1Angle4;
        private System.Windows.Forms.Label Ko5Angle;
        private System.Windows.Forms.Label Z0Z1Angle5;
    }
}