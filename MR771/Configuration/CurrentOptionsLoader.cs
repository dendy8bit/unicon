﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR771.Configuration
{
    /// <summary>
    /// Загружает уставки токов(Iтт) и напряжений (Ктн)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Private fields]
        private const int COUNT_GROUPS = 6;

        public  MemoryEntity<MeasureTransStruct>[] Connections { get; }
        private int _numberOfGroup;
        #endregion [Private fields]

        #region [Events]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        private Mr771 _device;

        #endregion [Events]


        #region [Ctor's]

        public CurrentOptionsLoader(Mr771 device)
        {
            this._device = device;
            this.Connections = new MemoryEntity<MeasureTransStruct>[COUNT_GROUPS];
            this._numberOfGroup = 0;
            for (int i = 0; i < this.Connections.Length; i++)
            {
                this.Connections[i] = new MemoryEntity<MeasureTransStruct>(
                        string.Format("Параметры измерительного трансформатора гр{0}", i + 1), this._device,
                        this._device.GetStartAddrMeasTrans(i));
                this.Connections[i].AllReadOk += this._connections_AllReadOk;
                this.Connections[i].AllReadFail += this._connections_AllReadFail;
            }
        }

        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        private void _connections_AllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        private void _connections_AllReadOk(object sender)
        {
            
            //switch (this._numberOfGroup)
            //{
            //    case 0:
            //    case 1:
            //    case 2:
            //    case 3:
            //    case 4:
            //        this._connections[++this._numberOfGroup].LoadStruct();
            //        break;
            //    case 5:
            //        if (this.LoadOk != null)
            //        {
            //            this.LoadOk.Invoke();
            //        }
            //        break;
            //}
            
            this._numberOfGroup++;
            if (this._numberOfGroup < COUNT_GROUPS)
            {
                this.Connections[this._numberOfGroup].LoadStruct();
            }
            else
            {
                this.LoadOk?.Invoke();
            }
            
        }

        #endregion [Memory Entity Events Handlers]


        #region [Methods]

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this.Connections[0].LoadStruct();
        }
        
        #endregion [Methods]
    }
}
