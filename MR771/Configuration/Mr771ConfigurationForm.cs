﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR771.Configuration.Structures;
using BEMN.MR771.Configuration.Structures.Apv;
using BEMN.MR771.Configuration.Structures.CheckTn;
using BEMN.MR771.Configuration.Structures.ConfigSystem;
using BEMN.MR771.Configuration.Structures.Defenses;
using BEMN.MR771.Configuration.Structures.Defenses.External;
using BEMN.MR771.Configuration.Structures.Defenses.F;
using BEMN.MR771.Configuration.Structures.Defenses.I;
using BEMN.MR771.Configuration.Structures.Defenses.I2I1;
using BEMN.MR771.Configuration.Structures.Defenses.Istar;
using BEMN.MR771.Configuration.Structures.Defenses.Q;
using BEMN.MR771.Configuration.Structures.Defenses.U;
using BEMN.MR771.Configuration.Structures.Defenses.Z;
using BEMN.MR771.Configuration.Structures.Engine;
using BEMN.MR771.Configuration.Structures.Goose;
using BEMN.MR771.Configuration.Structures.HFL;
using BEMN.MR771.Configuration.Structures.InputSignals;
using BEMN.MR771.Configuration.Structures.Ls;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;
using BEMN.MR771.Configuration.Structures.Omp;
using BEMN.MR771.Configuration.Structures.Oscope;
using BEMN.MR771.Configuration.Structures.RelayInd;
using BEMN.MR771.Configuration.Structures.Sihronizm;
using BEMN.MR771.Configuration.Structures.Switch;
using BEMN.MR771.Configuration.Structures.UROV;
using BEMN.MR771.Configuration.Structures.Vls;
using BEMN.MR771.Properties;
using BEMN.Forms.TreeView;

namespace BEMN.MR771.Configuration
{
    public partial class Mr771ConfigurationForm : Form, IFormView
    {
        #region [Constants]

        private const string READING_CONFIG = "Чтение конфигурации";
        private const string WRITING_CONFIG_CAPTION = "Запись конфигурации";
        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";
        private const string WRITING_CONFIG = "Идет запись конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR771_SET_POINTS";
        private const string INVALID_PORT = "Порт недоступен.";
        private const string MR771_BASE_CONFIG_PATH = "\\MR771\\MR771_BaseConfig_v{0}.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";
        private const string LIST_FILE_NAME = "commonp.xml";
        private const string LIST_FILE_NAME_FOR_OSC = "oscsign.xml";
        private const string DEVICE_NAME = "MR771";
        private const string READING_BDLIST = "Идет чтение подписей сигналов";
        private const string WRITING_BDLIST = "Идет запись подписей сигналов";
        private const string READING_BDLIST_OK = "Чтение подписей успешно завершено";
        #endregion [Constants]

        #region [Private fields]

        private double _version;
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private readonly MemoryEntity<ConfigurationStruct105> _configuration105;
        private readonly MemoryEntity<ConfigurationStruct108> _configuration108;
        private readonly MemoryEntity<ConfigurationStruct111> _configuration111;
        /// <summary>
        /// Текущие уставки
        /// </summary>
        private ConfigurationStruct _currentSetpointsStruct;

        private ConfigurationStruct105 _currentSetpointsStruct105;
        private ConfigurationStruct108 _currentSetpointsStruct107;
        private ConfigurationStruct111 _currentSetpointsStruct111;
        private MaskedTextBox[] _primary;
        private MaskedTextBox[] _second;
        private readonly Mr771 _device;
        private bool _return;
        private ComboboxSelector _groupSetpointSelector;
        private CheckedListBox[] _vlsBoxesGr;
        
        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;
        private FileDriver _fileDriver;

        private MessageBoxForm _formCheck;

        private List<TabPage> _lsTabPages;
        private List<TabPage> _vlsTabPages;
        private List<TabControl> _tabControls;

        #region [Валидаторы]

        #region Уставки группы

        #region [Защиты]

        /// <summary>
        /// Углы МЧ
        /// </summary>
        private NewStructValidator<CornerStruct> _cornerValidatorGr1;
        /// <summary>
        /// I*
        /// </summary>
        private NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct> _iStarValidatorGr1;
        /// <summary>
        /// I2/I1
        /// </summary>
        private NewStructValidator<DefenseI2I1Struct> _i2I1ValidatorGr1;
        /// <summary>
        /// I
        /// </summary>
        private NewDgwValidatior<AllMtzMainStruct, MtzMainStruct> _iValidatorGr1;
        /// <summary>
        /// U
        /// </summary>
        private NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> _uValidatorGr1;
        /// <summary>
        /// FreqDefB
        /// </summary>
        private NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> _fBValidator;
        /// <summary>
        /// FreqDefM
        /// </summary>
        private NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> _fMValidator;
        /// <summary>
        /// External
        /// </summary>
        private NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> _externalValidatorGr1;
        /// <summary>
        /// Q
        /// </summary>
        private NewDgwValidatior<AllDefenseQStruct, DefenseQStruct> _qValidatorGr1;
        /// <summary>
        /// Блокировка по нагреву
        /// </summary>
        private NewStructValidator<DefenseTermBlockStruct> _termBlockValidatorGr1;
        
        private StructUnion<DefensesSetpointsStruct> _defensesUnionGr1;
        private StructUnion<DefensesSetpointsStructNew> _defensesUnionGr1New;

        #endregion [Защиты]

        /// <summary>
        /// Контроль цепей ТН
        /// </summary>
        private NewStructValidator<CheckTnStruct> _chechTnValidator;
        /// <summary>
        /// АПВ
        /// </summary>
        private NewStructValidator<ApvStruct> _apvValidator;
        /// <summary>
        /// Тепловая модель
        /// </summary>
        private NewStructValidator<TermConfigStruct> _termConfValidator;
        /// <summary>
        /// Канал I
        /// </summary>
        private NewStructValidator<KanalITransStruct> _iTransValidator;
        /// <summary>
        /// Канал U
        /// </summary>
        private NewStructValidator<KanalUTransStruct> _uTransValidator;
        /// <summary>
        /// Конфигурация измерительного трансформатора
        /// </summary>
        private StructUnion<MeasureTransStruct> _measureTransUnionGr1;
        // ЛС
        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<AllInputLogicStruct> _inputLogicUnion;
        private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>();
        private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();
        // ВЛС
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<AllOutputLogicSignalStruct> _vlsUnion;
        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        // Синхроизм
        private NewStructValidator<SinhronizmAddStruct> _manualSinhronizmValidator;
        private NewStructValidator<SinhronizmAddStruct> _autoSinhronizmValidator;
        private NewStructValidator<SinhronizmStruct> _sinhronizmValidator;
        /// <summary>
        /// ОМП
        /// </summary>
        private NewStructValidator<ConfigurationOpmStruct> _ompValidatorGr1;
        
        private StructUnion<GroupSetpointStruct> _setpointUnionV1;
        private SetpointsValidator<AllGroupSetpointStruct, GroupSetpointStruct> _setpointsValidatorV1;

        private StructUnion<GroupSetpoint108> _setpointUnionV107;
        private SetpointsValidator<AllGroupSetpointStruct108, GroupSetpoint108> _setpointsValidatorV107;

        private StructUnion<GroupSetpoint111> _setpointUnionV111;
        private SetpointsValidator<AllGroupSetpointStruct111, GroupSetpoint111> _setpointsValidatorV111;
        #endregion Уставки

        // Валидатор выключателя
        private NewStructValidator<SwitchStruct> _switchValidator;
        // Валидатор входных сигналов
        private NewStructValidator<InputSignalStruct> _inputSignalsValidator;

        #region Валидаторы сциллографа
        private NewStructValidator<OscopeConfigStruct> _oscopeConfigValidator;
        private DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase> _channelsValidator;
        private StructUnion<OscopeStruct> _oscopeUnion;
        #endregion

        #region Реле и индикаторы
        private NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> _releyValidator;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<FaultStruct> _faultValidator;
        private StructUnion<AutomaticsParametersStruct> _automaticsParametersUnion;
        #endregion Реле и индикаторы

        private NewStructValidator<Goose> _gooseValidator;
        private SetpointsValidator<GooseConfig, Goose> _allGooseSetpointValidator;

        private NewStructValidator<ConfigAddStruct> _configAddValidator;
        private StructUnion<ConfigResistDiagramStruct> _resistConfigUnion;
        private StructUnion<ConfigResistDiagramStructNew> _resistConfigUnionNew;

        private StructUnion<ConfigurationStruct> _configurationValidator;
        private StructUnion<ConfigurationStruct105> _configurationValidator105;
        private StructUnion<ConfigurationStruct108> _configurationValidator108;
        private StructUnion<ConfigurationStruct111> _configurationValidator111;


        private NewStructValidator<ChannelStruct> _startOscChannelValidator;

        private NewStructValidator<DzStruct> _hfl1Validator;
        private NewStructValidator<TznpStruct> _hfl2Validator;
        private NewStructValidator<UrovStruct> _urovValidator;
        #endregion [Валидаторы]

        #endregion [Private fields]

        #region [Ctor's]

        public Mr771ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr771ConfigurationForm(Mr771 device)
        {
            this._device = device;
            this._version = Common.VersionConverter(this._device.DeviceVersion);

            //Архив программы
            //this._fileDriver = new FileDriver(this._device, this);
            
            this.InitializeComponent();
            this._formCheck = new MessageBoxForm();

            //if (_version < 1.12)
            //{
                _configurationTabControl.TabPages.Remove(_signaturesSignalsPage);
                _signaturesForOscGroupBox.Visible = false;
            //}

            //SetupToolTipForTabControls();

            //Default init list
            InitDefaultList();
            
            if (this._version >= 1.11)
            {
                //if (_version >= 1.12)
                //{
                //    _progressBar.Visible = false;

                //    InitSignaturesControl();
                //}
               
                this._configuration111 = device.Configuration111;
                this._configuration111.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration111.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration111.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration111.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration111.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration111.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration111.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration111.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._currentSetpointsStruct111 = new ConfigurationStruct111();
                this._progressBar.Maximum = this._configuration111.Slots.Count;
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._configuration108 = device.Configuration108;
                this._configuration108.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration108.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration108.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration108.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration108.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration108.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration108.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration108.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._currentSetpointsStruct107 = new ConfigurationStruct108();
                this._progressBar.Maximum = this._configuration108.Slots.Count;
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                this._configuration105 = device.Configuration105;
                this._configuration105.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration105.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration105.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration105.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration105.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration105.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration105.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration105.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._currentSetpointsStruct105 = new ConfigurationStruct105();
                this._progressBar.Maximum = this._configuration105.Slots.Count;
            }
            else
            {
                this._configuration = device.Configuration;
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._currentSetpointsStruct = new ConfigurationStruct();
                this._progressBar.Maximum = this._configuration.Slots.Count;
            }

            this._copySetpoinsGroupComboBox.DataSource = StringsConfig.CopyGroupsNames;
            this._groupSetpointSelector = new ComboboxSelector(this._setpointsComboBox, StringsConfig.GroupsNames);
            this._groupSetpointSelector.OnRefreshInfoTable += OnRefreshInfoTable;

            Init();

            InitSetpointsAndStructUnion();

            AddLsAndVls();
        }

        private void AddLsAndVls()
        {
            // ЛС
            dataGridsViewLsAND.Add(this._lsAndDgv1Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv2Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv3Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv4Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv5Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv6Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv7Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv8Gr1);

            dataGridsViewLsOR.Add(this._lsOrDgv1Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv2Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv3Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv4Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv5Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv6Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv7Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv8Gr1);

            // ВЛС
            allVlsCheckedListBoxs.Add(this.VLSclb1Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb2Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb3Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb4Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb5Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb6Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb7Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb8Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb9Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb10Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb11Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb12Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb13Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb14Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb15Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb16Gr1);
        }

        private void InitSetpointsAndStructUnion()
        {
            if (this._version >= 1.11)
            {
                this._setpointUnionV111 = new StructUnion<GroupSetpoint111>(
                    this._defensesUnionGr1New,
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidatorNew,
                    this._chechTnValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this._apvValidator,
                    this._termConfValidator,
                    this._measureTransUnionGr1,
                    this._inputLogicUnion,
                    this._vlsUnion,
                    this._sinhronizmValidator,
                    this._ompValidatorGr1,
                    this._hfl1Validator,
                    this._hfl2Validator);

                this._setpointsValidatorV111 = new SetpointsValidator<AllGroupSetpointStruct111, GroupSetpoint111>
                    (this._groupSetpointSelector, this._setpointUnionV111);
            }

            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._setpointUnionV107 = new StructUnion<GroupSetpoint108>(
                    this._defensesUnionGr1,
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidator,
                    this._chechTnValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this._apvValidator,
                    this._termConfValidator,
                    this._measureTransUnionGr1,
                    this._inputLogicUnion,
                    this._vlsUnion,
                    this._sinhronizmValidator,
                    this._ompValidatorGr1,
                    this._hfl1Validator,
                    this._hfl2Validator);

                this._setpointsValidatorV107 = new SetpointsValidator<AllGroupSetpointStruct108, GroupSetpoint108>
                    (this._groupSetpointSelector, this._setpointUnionV107);
            }

            if (this._version >= 1.09)
            {
                this._clearCurrentLsButton.Visible = true;
                this._clearAllLsSygnalButton.Visible = true;
                this._clearCurrentLsnButton.Visible = true;
                this._clearAllLsnSygnalButton.Visible = true;
                this._resetCurrentVlsButton.Visible = true;
                this._resetAllVlsButton.Visible = true;
                this._reserveGroupBox.Visible = true;
            }

            else
            {
                this._setpointUnionV1 = new StructUnion<GroupSetpointStruct>
                (
                    this._defensesUnionGr1,
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidator,
                    this._chechTnValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this._apvValidator,
                    this._termConfValidator,
                    this._measureTransUnionGr1,
                    this._inputLogicUnion,
                    this._vlsUnion,
                    this._sinhronizmValidator,
                    this._ompValidatorGr1
                );
                this._setpointsValidatorV1 = new SetpointsValidator<AllGroupSetpointStruct, GroupSetpointStruct>
                    (this._groupSetpointSelector, this._setpointUnionV1);
            }

            if (this._version < 1.08)
            {
                this._configurationTabControl.TabPages.Remove(this._ethernetPage);
                this._setpointsTab.TabPages.Remove(this._hfl);
            }

            if (this._version >= 1.08)
            {
                ComboBox[] bgs =
                {
                    this.goin1, this.goin2, this.goin3, this.goin4, this.goin5, this.goin6, this.goin7, this.goin8, this.goin9,
                    this.goin10,
                    this.goin11, this.goin12, this.goin13, this.goin14, this.goin15, this.goin16, this.goin17, this.goin18,
                    this.goin19, this.goin20,
                    this.goin21, this.goin22, this.goin23, this.goin24, this.goin25, this.goin26, this.goin27, this.goin28,
                    this.goin29, this.goin30,
                    this.goin31, this.goin32, this.goin33, this.goin34, this.goin35, this.goin36, this.goin37, this.goin38,
                    this.goin39, this.goin40,
                    this.goin41, this.goin42, this.goin43, this.goin44, this.goin45, this.goin46, this.goin47, this.goin48,
                    this.goin49, this.goin50,
                    this.goin51, this.goin52, this.goin53, this.goin54, this.goin55, this.goin56, this.goin57, this.goin58,
                    this.goin59, this.goin60,
                    this.goin61, this.goin62, this.goin63, this.goin64
                };

                NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                    this._toolTip,
                    new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)),
                    new ControlInfoCombo(this._reserveCB, StringsConfig.Reserve));

                NewStructValidator<Goose> gooseValidator = new NewStructValidator<Goose>
                (
                    this._toolTip,
                    new ControlInfoCombo(this.operationBGS, StringsConfig.GooseConfig),
                    new ControlInfoMultiCombo(bgs, StringsConfig.GooseSignal)
                );

                this._allGooseSetpointValidator = new SetpointsValidator<GooseConfig, Goose>
                    (new ComboboxSelector(this.currentBGS, StringsConfig.GooseNames), gooseValidator);

                if (this._version >= 1.11)
                {
                    this._configurationValidator111 = new StructUnion<ConfigurationStruct111>
                    (
                        this._setpointsValidatorV111,
                        this._switchValidator,
                        this._inputSignalsValidator,
                        this._oscopeUnion,
                        this._automaticsParametersUnion,
                        ethernetValidator,
                        this._configAddValidator,
                        _urovValidator,
                        this._allGooseSetpointValidator
                    );
                }
                else
                {
                    this._configurationValidator108 = new StructUnion<ConfigurationStruct108>
                    (
                        this._setpointsValidatorV107,
                        this._switchValidator,
                        this._inputSignalsValidator,
                        this._oscopeUnion,
                        this._automaticsParametersUnion,
                        ethernetValidator,
                        this._configAddValidator,
                        _urovValidator,
                        this._allGooseSetpointValidator
                    );
                }
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                this._configurationTabControl.TabPages.Remove(this._gooseTabPage);
                this._configurationValidator105 = new StructUnion<ConfigurationStruct105>
                (
                    this._setpointsValidatorV1,
                    this._switchValidator,
                    this._inputSignalsValidator,
                    this._oscopeUnion,
                    this._automaticsParametersUnion,
                    this._configAddValidator,
                    _urovValidator
                );
            }
            else
            {
                this._configurationTabControl.TabPages.Remove(this._gooseTabPage);
                this._configurationValidator = new StructUnion<ConfigurationStruct>
                (
                    this._setpointsValidatorV1,
                    this._switchValidator,
                    this._inputSignalsValidator,
                    this._oscopeUnion,
                    this._automaticsParametersUnion,
                    this._configAddValidator
                );
            }
        }

        private void InitSignaturesControl()
        {
            signaturesSignalsConfigControl.SignalsList = StringsConfig.DefaultSignaturesSignals;
            signaturesSignalsConfigControl.DefaultList = StringsConfig.DefaultSignaturesSignals;
            signaturesSignalsConfigControl.AcceptedSignals += UpdateElementsInForm;
            signaturesOsc.CopySignalsAction += CopySignalsInOscDataGrid;

            signaturesOsc.FillDefaultList(InputLogicStruct.DISCRETS_COUNT, OscopeAllChannelsStruct.KANAL_COUNT);
        }

        private static void InitDefaultList()
        {
            StringsConfig.RelaySignals = StringsConfig.DefaultRelaySignals;
            StringsConfig.SwitchSignals = StringsConfig.DefaultSwitchSignals;
            StringsConfig.VlsSignals = StringsConfig.DefaultVLSSignals;
            StringsConfig.ExternalDafenseSrab = StringsConfig.DefaultExternalDafenseSignals;
            StringsConfig.LsSignals = StringsConfig.DefaultLsSignals;
            StringsConfig.HflSignalsDZ = StringsConfig.DefaultHflSignalsDZ;
            StringsConfig.HflSignalsTZNP = StringsConfig.DefaultHflSignalsTZNP;
            StringsConfig.SignaturesForOsc = StringsConfig.DefaultSignaturesForOsc;
        }

        private void CopySignalsInOscDataGrid()
        {
            var diskretsList = AddSignalsInListFromDataGridOscBase(out var oscList);

            StringsConfig.OscList = new List<string>();
            StringsConfig.OscList.AddRange(oscList);

            signaturesOsc.CopySignalsInDataGrid(diskretsList, oscList);
        }

        private List<string> AddSignalsInListFromDataGridOscBase(out List<string> oscList)
        {
            List<string> diskretsList = new List<string>();
            oscList = new List<string>();

            diskretsList.AddRange(StringsConfig.VlsSignals.GetRange(0, 40));

            for (int i = 0; i < _oscChannelsGrid.RowCount; i++)
            {
                if (_oscChannelsGrid.Rows[i].Cells[2].Value.ToString() != "НЕТ")
                {
                    oscList.Add(_oscChannelsGrid.Rows[i].Cells[2].Value.ToString());
                }
                else
                {
                    oscList.Add("");
                }
            }
            return diskretsList;
        }

        private void SetupToolTipForTabControls()
        {
            _lsTabPages = new List<TabPage>
            {
                tabPage39,
                tabPage40,
                tabPage41,
                tabPage42,
                tabPage43,
                tabPage44,
                tabPage45,
                tabPage46,
                tabPage31,
                tabPage32,
                tabPage33,
                tabPage34,
                tabPage35,
                tabPage36,
                tabPage37,
                tabPage38,
            };

            _vlsTabPages = new List<TabPage>
            {
                VLS1,
                VLS2,
                VLS3,
                VLS4,
                VLS5,
                VLS6,
                VLS7,
                VLS8,
                VLS9,
                VLS10,
                VLS11,
                VLS12,
                VLS13,
                VLS14,
                VLS15,
                VLS16,
            };

            _tabControls = new List<TabControl>
            {
                _lsFirsTabControl,
                _lsSecondTabControl,
                _vlsTabControl
            };

            for (int i = 0; i < _tabControls.Count; i++)
            {
                _tabControls[i].ShowToolTips = true;
            }
        }

        private void UpdateElementsInForm()
        {
            UpdateStatus("Применение подписей сигналов", true);

            this._currentSetpointsStruct111 = this._configurationValidator111.Get();
            ushort[] configValues = this._currentSetpointsStruct111.GetValues();
            byte[] value = Common.TOBYTES(configValues, false);

            UpdateListsInStringsConfig();
            
            Init();

            this._currentSetpointsStruct111.InitStruct(value);

            this.ShowConfiguration();

            UpdateStatus("Применение подписей сигналов успешно завершено");
        }

        private void Init()
        {
            this.resistanceDefTabCtr1.Initialization();
            this.resistanceDefTabCtr1.IsPrimary = false;

            this._second = new[] {this._xud1s, this._xud2s, this._xud3s, this._xud4s, this._xud5s};
            this._primary = new[] {this._xud1p, this._xud2p, this._xud3p, this._xud4p, this._xud5p};
            
            bool visibility = Common.VersionConverter(this._device.DeviceVersion) >= 1.05;
            this._difensesFBDataGridGr1.Columns[2].Visible = visibility;
            this._difensesFBDataGridGr1.Columns[4].Visible = visibility;
            
            this._difensesFMDataGridGr1.Columns[2].Visible = visibility;
            this._difensesFMDataGridGr1.Columns[4].Visible = visibility;
            if (visibility)
            {
                this._switchIUrov.Visible = this.IurovLabel.Visible = false;
                this._tUrov.Visible = this.tUROVlabel.Visible = false;
                this.UROVgroupBox.Visible = true;
                this._automatPage.Text = "Выключатель и УРОВ";
                this._blockSDTU.Visible = this._blockSDTUlabel.Visible = true;
            }
            else
            {
                this._iAPVModeColumn.Visible = this._i0APVColumn.Visible = this.I2I1Apv.Visible = this._uBAPVColumn.Visible
                    = this._uMAPVColumn.Visible = this._engineDefenseAPV.Visible = this._externalDifAPV.Visible = true;
                this._iAPVModeColumn1.Visible = this._i0APVColumn1.Visible = this.i2i1APV1.Visible = this._uBAPVColumn1.Visible 
                    = this._uMAPVColumn1.Visible = this._engineDefenseAPV1.Visible = this._engineDefenseAPV1.Visible = false;
                
                this._difensesFBDataGridGr1.Columns[3].HeaderText =
                    this._difensesFBDataGridGr1.Columns[3].HeaderText.Split('|')[0];
                this._difensesFMDataGridGr1.Columns[3].HeaderText =
                    this._difensesFMDataGridGr1.Columns[3].HeaderText.Split('|')[0];
            }
            
            #region Уставки

            #region [Защиты]

            //углы
            this._cornerValidatorGr1 = new NewStructValidator<CornerStruct>
                (
                this._toolTip,
                new ControlInfoText(this._iCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i0CornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._inCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i2CornerGr1, RulesContainer.UshortTo360)
                );
            // I
            this._iValidatorGr1 = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                (
                new[] {this._difensesIDataGridGr1, this._difensesI8DataGridGr1},
                new[] {6, 1},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesI, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka256, true, false), //4
                new ColumnInfoCheck(true, false),
                new ColumnInfoCombo(StringsConfig.BlockTn),
                new ColumnInfoCombo(StringsConfig.Direction, ColumnsType.COMBO, true, false),
                new ColumnInfoCombo(StringsConfig.Undir, ColumnsType.COMBO, true, false),
                new ColumnInfoCombo(StringsConfig.Logic),
                new ColumnInfoCombo(StringsConfig.Characteristic, ColumnsType.COMBO, true, false),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000, true, false),
                new ColumnInfoCombo(StringsConfig.RelaySignals, ColumnsType.COMBO, true, false),
                new ColumnInfoText(RulesContainer.TimeRule, true, false),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.Ustavka100, true, false),
                new ColumnInfoCheck(true, false),
                new ColumnInfoCheck(true, false),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesIDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesI8DataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22)
                        )
                }
            };
            //I*
            this._iStarValidatorGr1 = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>
                (
                this._difensesI0DataGridGr1,
                AllDefenseStarStruct.DEF_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesIStar, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Direction),
                new ColumnInfoCombo(StringsConfig.Undir),
                new ColumnInfoCombo(StringsConfig.I),
                new ColumnInfoCombo(StringsConfig.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesI0DataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                        )
                }
            };

            //I2I1
            this._i2I1ValidatorGr1 = new NewStructValidator<DefenseI2I1Struct>
               (
               this._toolTip,
               new ControlInfoCombo(this.I2I1ModeComboGr1, StringsConfig.DefenseModes),
               new ControlInfoCombo(this.I2I1BlockingComboGr1, StringsConfig.SwitchSignals),
               new ControlInfoText(this.I2I1TBGr1, RulesContainer.Ustavka100),
               new ControlInfoText(this.I2I1tcpTBGr1, RulesContainer.TimeRule),
               new ControlInfoCombo(this.I2I1OscComboGr1, StringsConfig.OscModes),
               new ControlInfoCheck(this.I2I1UrovCheckGr1),
               new ControlInfoCheck(this.I2I1Apv),
               new ControlInfoCombo(this.i2i1APV1, StringsConfig.Apv),
               new ControlInfoCombo(this._discretIn3Cmb, StringsConfig.SwitchSignals)
               );
            
            //U
            this._uValidatorGr1 = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>
                (
                new[] {this._difensesUBDataGridGr1, this._difensesUMDataGridGr1},
                new[] {4, 4},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.UStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.UmaxDefenseMode, ColumnsType.COMBO, true, false), //1
                new ColumnInfoCombo(StringsConfig.UminDefenseMode, ColumnsType.COMBO, false, true), //2
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoCheck(), //7
                new ColumnInfoCheck(false, true), //8
                new ColumnInfoCombo(StringsConfig.BlockTn, ColumnsType.COMBO, false, true),
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesUBDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesUMDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
                        )
                }
            };

            Func<IValidatingRule> fBDefFunc = () =>
            {
                if (!this._difensesFBDataGridGr1.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFBDataGridGr1.Tag as DataGridViewCell;
                    if (cell == null ||
                        this._difensesFBDataGridGr1[2, cell.RowIndex].Value.ToString() == StringsConfig.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };

            //F>
            this._fBValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFBDataGridGr1, 4, this._toolTip,
                new ColumnInfoCombo(StringsConfig.FBStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes), //1
                new ColumnInfoCombo(StringsConfig.FreqDefType), //2
                new ColumnInfoTextDependent(fBDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCombo(StringsConfig.Apv),//13
                new ColumnInfoCheck(), //14
                new ColumnInfoCheck()  //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFBDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ,15),
                        new TurnOffRule(2, StringsConfig.FreqDefType[0], false, 4)
                        )
                }
            };

            Func<IValidatingRule> fMDefFunc = () =>
            {
                if (!this._difensesFMDataGridGr1.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFMDataGridGr1.Tag as DataGridViewCell;
                    if (cell == null || this._difensesFMDataGridGr1[2, cell.RowIndex].Value.ToString() == StringsConfig.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };
            //F<
            this._fMValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFMDataGridGr1, 4, this._toolTip,
                new ColumnInfoCombo(StringsConfig.FMStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes), //1
                new ColumnInfoCombo(StringsConfig.FreqDefType), //2
                new ColumnInfoTextDependent(fMDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCombo(StringsConfig.Apv),//13
                new ColumnInfoCheck(),  //14
                new ColumnInfoCheck() //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFMDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(2, StringsConfig.FreqDefType[0], false, 4)
                        )
                }
            };

            this._qValidatorGr1 = new NewDgwValidatior<AllDefenseQStruct, DefenseQStruct>(
                this._engineDefensesGridGr1,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.QStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka256), //2
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //3
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._engineDefensesGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7)
                        )
                }
            };

            this._termBlockValidatorGr1 = new NewStructValidator<DefenseTermBlockStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._engineQmodeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._engineQconstraintGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._engineQtimeGr1, RulesContainer.UshortRule)
                );

            this._externalValidatorGr1 = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                this._externalDefensesGr1,
                16,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),//5
                new ColumnInfoCheck(), //6
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab), //7
                new ColumnInfoCombo(StringsConfig.OscModes),//8
                new ColumnInfoCheck(),//9
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefensesGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                        )
                }
            };

            this._defensesUnionGr1 = new StructUnion<DefensesSetpointsStruct>
                (
                this._cornerValidatorGr1,
                this._iValidatorGr1,
                this._iStarValidatorGr1,
                this._i2I1ValidatorGr1,
                this._uValidatorGr1,
                this._fBValidator,
                this._fMValidator,
                this._qValidatorGr1,
                this._termBlockValidatorGr1,
                this._externalValidatorGr1,
                this.resistanceDefTabCtr1.ResistanceUnion
                );

            this._defensesUnionGr1New = new StructUnion<DefensesSetpointsStructNew>
            (
                this._cornerValidatorGr1,
                this._iValidatorGr1,
                this._iStarValidatorGr1,
                this._i2I1ValidatorGr1,
                this._uValidatorGr1,
                this._fBValidator,
                this._fMValidator,
                this._qValidatorGr1,
                this._termBlockValidatorGr1,
                this._externalValidatorGr1,
                this.resistanceDefTabCtr1.ResistanceUnionNew
            );

            #endregion [защиты]

            // Контроль цепей ТН
            this._chechTnValidator = new NewStructValidator<CheckTnStruct>(
                this._toolTip,
                new ControlInfoCombo(this._errorL_comboGr1, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._errorX_comboGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this._i2u2CheckGr1),
                new ControlInfoText(this._u2ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i2ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoCheck(this._i0u0CheckGr1),
                new ControlInfoText(this._u0ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i0ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._uMinContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._uMaxContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._iMinContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iMaxContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._uDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._tdContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._tsContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._resetTnGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this.checkBox1)
                );
            //АПВ
            this._apvValidator = new NewStructValidator<ApvStruct>(
                this._toolTip,
                new ControlInfoCombo(this._apvModeGr1, StringsConfig.ApvModes),
                new ControlInfoCheck(this._blokFromUrov),
                new ControlInfoCombo(this._disableApv, StringsConfig.SwitchSignals),
                new ControlInfoText(this._timeDisable, RulesContainer.TimeRule),
                new ControlInfoCombo(this._typeDisable, StringsConfig.DisableType),
                new ControlInfoCombo(this._apvBlockGr1, StringsConfig.SwitchSignals),
                new ControlInfoText(this._apvTblockGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvTreadyGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat1Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat2Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat3Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat4Gr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._apvSwitchOffGr1, StringsConfig.BeNo)
                );

            //Тепловая модель
            this._termConfValidator = new NewStructValidator<TermConfigStruct>(
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineIadded, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._engineQresetGr1, StringsConfig.SwitchSignals)
                );

            // Измерительный трансформатор
            this._iTransValidator = new NewStructValidator<KanalITransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._TT_typeComboGr1, StringsConfig.TtType),
                new ControlInfoText(this._Im_BoxGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._ITTL_BoxGr1, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX_BoxGr1, RulesContainer.UshortRule),
                new ControlInfoCombo(this._inpIn, StringsConfig.InpIn)
                );
            this._uTransValidator = new NewStructValidator<KanalUTransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._Uo_typeComboGr1, StringsConfig.UoType),
                new ControlInfoText(this._KTHL_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoText(this._KTHX_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoText(this._KTHX1_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._KTHLkoef_comboGr1, StringsConfig.KthKoefs),
                new ControlInfoCombo(this._KTHXkoef_comboGr1, StringsConfig.KthKoefs),
                new ControlInfoCombo(this._KTHX1koef_comboGr1, StringsConfig.KthKoefs),
                new ControlInfoCombo(this._errorX1_comboGr1, StringsConfig.SwitchSignals)
                );
            this._measureTransUnionGr1 = new StructUnion<MeasureTransStruct>(this._iTransValidator, this._uTransValidator);

            DataGridView[] lsBoxes =
            {
                this._lsAndDgv1Gr1, this._lsAndDgv2Gr1, this._lsAndDgv3Gr1, this._lsAndDgv4Gr1,
                this._lsAndDgv5Gr1, this._lsAndDgv6Gr1, this._lsAndDgv7Gr1, this._lsAndDgv8Gr1,
                this._lsOrDgv1Gr1, this._lsOrDgv2Gr1, this._lsOrDgv3Gr1, this._lsOrDgv4Gr1,
                this._lsOrDgv5Gr1, this._lsOrDgv6Gr1, this._lsOrDgv7Gr1, this._lsOrDgv8Gr1
            };
            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }
            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[AllInputLogicStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    lsBoxes[i],
                    InputLogicStruct.DISCRETS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.VlsSignals.GetRange(0, 40), ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.LsState)
                    );
            }
            this._inputLogicUnion = new StructUnion<AllInputLogicStruct>(this._inputLogicValidator);
            
            _vlsBoxesGr = new[]
            {
                this.VLSclb1Gr1, this.VLSclb2Gr1, this.VLSclb3Gr1, this.VLSclb4Gr1, this.VLSclb5Gr1, this.VLSclb6Gr1,
                this.VLSclb7Gr1, this.VLSclb8Gr1,this.VLSclb9Gr1, this.VLSclb10Gr1, this.VLSclb11Gr1, this.VLSclb12Gr1,
                this.VLSclb13Gr1, this.VLSclb14Gr1, this.VLSclb15Gr1, this.VLSclb16Gr1
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(_vlsBoxesGr[i], StringsConfig.VlsSignals);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalStruct>(this._vlsValidator);
            
            // ОМП
            this._ompValidatorGr1 = new NewStructValidator<ConfigurationOpmStruct>(
                this._toolTip,
                new ControlInfoCombo(this._OMPmode_comboGr1, StringsConfig.OmpModes),
                new ControlInfoText(this._xud1s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud2s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud3s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud4s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud5s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._l1, RulesContainer.Ustavka256),
                new ControlInfoText(this._l2, RulesContainer.Ustavka256),
                new ControlInfoText(this._l3, RulesContainer.Ustavka256),
                new ControlInfoText(this._l4, RulesContainer.Ustavka256)
                );

            // Синхронизм
            this._manualSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrManualModeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrManualUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrManualNoYesGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualYesNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualNoNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoText(this._sinhrManualdFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrManualdFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrManualdFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrManual),
                new ControlInfoCheck(this._catchSinchrManual),
                new ControlInfoCheck(this._blockTNmanual)
                );

            this._autoSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrAutoModeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrAutoUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrAutoNoYesGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoYesNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoNoNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoText(this._sinhrAutodFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrAutodFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrAutodFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrAuto),
                new ControlInfoCheck(this._catchSinchrAuto),
                new ControlInfoCheck(this._blockTNauto)
                );

            this._sinhronizmValidator = new NewStructValidator<SinhronizmStruct>
                (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOtsGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwaitGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhrGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTonGr1, RulesContainer.UshortTo600),
                new ControlInfoCombo(this._sinhrU1Gr1, StringsConfig.Usinhr),
                new ControlInfoCombo(this._sinhrU2Gr1, StringsConfig.Usinhr),
                new ControlInfoValidator(this._manualSinhronizmValidator),
                new ControlInfoValidator(this._autoSinhronizmValidator),
                new ControlInfoCombo(this._blockSinhCmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn1Cmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn2Cmb, StringsConfig.SwitchSignals),
                new ControlInfoText(this._sinhrKamp, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrF, new CustomUshortRule(0, 360))
                );

            _hfl1Validator = new NewStructValidator<DzStruct>(
                this._toolTip, 
                new ControlInfoCombo(this.modeDZ, StringsConfig.HflModes),
                new ControlInfoCheck(this.konturFF),
                new ControlInfoCombo(this.abbrevDZFF, StringsConfig.HflSignalsDZ),
                new ControlInfoCombo(this.extendDZFF, StringsConfig.HflSignalsDZ),
                new ControlInfoCombo(this.reverseDZFF, StringsConfig.HflSignalsDZ),
                new ControlInfoCheck(this.konturFN),
                new ControlInfoCombo(this.abbrevDZFN, StringsConfig.HflSignalsDZ),
                new ControlInfoCombo(this.extendDZFN, StringsConfig.HflSignalsDZ),
                new ControlInfoCombo(this.reverseDZFN, StringsConfig.HflSignalsDZ),
                new ControlInfoCombo(this.inpTSDZ, StringsConfig.SwitchSignals),
                new ControlInfoText(this.tvzTSDZ, RulesContainer.TimeRule),
                new ControlInfoCombo(this.deblockDZ, StringsConfig.HflDeblock),
                new ControlInfoCombo(this.controlDZ, StringsConfig.SwitchSignals),
                new ControlInfoText(this.tsrabDebDZ, RulesContainer.TimeRule), 
                new ControlInfoText(this.tminImpDZ, RulesContainer.TimeRule), 
                new ControlInfoText(this.toffDZ, RulesContainer.TimeRule), 
                new ControlInfoText(this.toffTBDZ, RulesContainer.TimeRule),
                new ControlInfoCombo(this.blockSwithDZ, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this.blockTSDZ, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this.OscDZ, StringsConfig.Apv),
                new ControlInfoCombo(this.urovDZ, StringsConfig.OffOn),
                new ControlInfoCombo(this.apvDZ, StringsConfig.Apv),
                new ControlInfoCombo(this.modeEchoDZ, StringsConfig.HflEcho),
                new ControlInfoText(this.tsrabTSDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.uminDZ, RulesContainer.Ustavka256),
                new ControlInfoCombo(this.blockEchoDZ, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this.blockTnDZ, StringsConfig.BlockTn),
                new ControlInfoCombo(this.modeReverseDZ, StringsConfig.OffOn),
                new ControlInfoText(this.treverseDZ, RulesContainer.TimeRule),
                new ControlInfoText(this.tficsDZ, RulesContainer.TimeRule),
                new ControlInfoCheck(this.zoneDZ),
                new ControlInfoCombo(this.blockReverseDZ, StringsConfig.SwitchSignals)
                );

            _hfl2Validator = new NewStructValidator<TznpStruct>(
                this._toolTip,
                new ControlInfoCombo(this.modeDZNP, StringsConfig.HflModes),
                new ControlInfoCombo(this.abbrevDZNP, StringsConfig.HflSignalsTZNP),
                new ControlInfoCombo(this.extendDZNP, StringsConfig.HflSignalsTZNP),
                new ControlInfoCombo(this.reverseDZNP, StringsConfig.HflSignalsTZNP),
                new ControlInfoCombo(this.inpTSDZNP, StringsConfig.SwitchSignals),
                new ControlInfoText(this.tvzTSDZNP, RulesContainer.TimeRule),
                new ControlInfoCombo(this.deblockDZNP, StringsConfig.HflDeblock),
                new ControlInfoCombo(this.controlDZNP, StringsConfig.SwitchSignals),
                new ControlInfoText(this.tsrabDebDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.tminImpDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.toffDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.toffTBDZNP, RulesContainer.TimeRule),
                new ControlInfoCombo(this.blockSwitchDZNP, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this.blockTSDZNP, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this.directionDZNP, StringsConfig.HflDirection),
                new ControlInfoCombo(this.oscDZNP, StringsConfig.Apv),
                new ControlInfoCombo(this.urovDZNP, StringsConfig.OffOn),
                new ControlInfoCombo(this.apvDZNP, StringsConfig.Apv),
                new ControlInfoCombo(this.modeEchoDZNP, StringsConfig.HflEcho),
                new ControlInfoText(this.tsrabTSDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.uminDZNP, RulesContainer.Ustavka256),
                new ControlInfoCombo(this.blockEchoDZNP, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this.blockTnDZNP, StringsConfig.BlockTn),
                new ControlInfoCombo(this.modeReverseDZNP, StringsConfig.OffOn),
                new ControlInfoText(this.treverseDZNP, RulesContainer.TimeRule),
                new ControlInfoText(this.tficsDZNP, RulesContainer.TimeRule),
                new ControlInfoCheck(this.zoneDZNP),
                new ControlInfoCombo(this.blockReverseDZNP, StringsConfig.SwitchSignals)
                );
            #endregion Уставки

            // Выключатель
            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                   new ControlInfoText(this._tUrov, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchIUrov, RulesContainer.Ustavka40),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                   new ControlInfoCombo(this._controlSolenoidCombo, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._comandOtkl,StringsConfig.ImpDlit),
                   new ControlInfoCombo(this._blockSDTU, StringsConfig.SwitchSignals)
                   );  
            // Входные сигналы
            this._inputSignalsValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUst1ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst2ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst3ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst4ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst5ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst6ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals)
                );

            #region [Осц]
            this._oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                     new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                     new ControlInfoCombo(this._oscLength, StringsConfig.OscCount)
                     );

            Func<string, List<string>> func = str =>
            {
                if (string.IsNullOrEmpty(str)) return new List<string>();
                int index = StringsConfig.OscBases.IndexOf(str);
                return index != -1 ? StringsConfig.OscChannelSignals[index] : new List<string>();
            };

            this._channelsValidator = new DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase>
                (
                this._oscChannelsGrid,
                OscopeAllChannelsStruct.KANAL_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.OscChannelNames, ColumnsType.NAME),
                new ColumnInfoComboControl(StringsConfig.OscBases, 2),
                new ColumnInfoComboDependent(func, 1)
                );
            this._oscChannelsGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            this._startOscChannelValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, StringsConfig.RelaySignals)
                );

            this._oscopeUnion = new StructUnion<OscopeStruct>(this._oscopeConfigValidator, this._channelsValidator, this._startOscChannelValidator);

            #endregion [Осц]

            #region [Реле и Индикаторы]

            this._releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                this._outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.Mode)
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoCheck(this._fault6CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            this._automaticsParametersUnion = new StructUnion<AutomaticsParametersStruct>
                (this._releyValidator, this._indicatorValidator, this._faultValidator);

            #endregion [Реле и Индикаторы]

            this._configAddValidator = new NewStructValidator<ConfigAddStruct>
                (this._toolTip, 
                new ControlInfoCombo(this._inpAddCombo, StringsConfig.InpOporSignals),
                new ControlInfoCheck(this._resetSystemCheckBox),
                new ControlInfoCheck(this._resetAlarmCheckBox),
                new ControlInfoCheck(this._fixErrorFCheckBox)
                );

            if (this._version < 1.11)
            {
                this._resistConfigUnion = new StructUnion<ConfigResistDiagramStruct>
                (
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this.resistanceDefTabCtr1.ResistanceUnion,
                    this._measureTransUnionGr1
                );
                this.resistanceDefTabCtr1.ResistConfigUnion = this._resistConfigUnion;
            }
            else
            {
                this._resistConfigUnionNew = new StructUnion<ConfigResistDiagramStructNew>
                (
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidatorNew,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this.resistanceDefTabCtr1.ResistanceUnionNew,
                    this._measureTransUnionGr1
                );
                this.resistanceDefTabCtr1.ResistConfigUnionNew = this._resistConfigUnionNew;
            }

            _urovValidator = new NewStructValidator<UrovStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._urovIcheck),
                new ControlInfoCheck(this._urovBkCheck),
                new ControlInfoCheck(this._urovSelf),
                new ControlInfoText(this._tUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._tUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._Iurov, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._urovPusk, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._urovBlock, StringsConfig.SwitchSignals)
                );
            
            Func<IValidatingRule> currentFuncOmpPrimary = () =>
            {
                double koef = this.GetKoeff();
                return new DoubleToComaRule(0, 2 * koef, 4);
            };

            //нужен только для инициализации валидаторов текстбоксов
            NewStructValidator<OneWordStruct> _primaryResistValidator = new NewStructValidator<OneWordStruct>
                (
                this._toolTip,
                new ControlInfoTextDependent(this._xud1p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud2p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud3p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud4p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud5p, currentFuncOmpPrimary)
                );
        }
        #endregion [Ctor's]

        #region [MemoryEntity Events Handlers]

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_OK;
            
            SetpointsValueSet();

            this.ShowConfiguration();

            //if (_version >= 1.12)
            //{
            //    signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, StringsConfig.DefaultSignaturesSignals);

            //    AddSignalsInListFromDataGridOscBase(out var oscList);

            //    StringsConfig.OscList = new List<string>();
            //    StringsConfig.OscList.AddRange(oscList);

            //    signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DISCRETS_COUNT, oscList);

            //    UpdateStatus(READ_OK);
            //    _formCheck?.ShowResultMessage(READ_OK);
            //}
            //else
            //{
                _statusLabel.Text = READ_OK;
            //}
        }

        private void SetpointsValueSet()
        {
            if (this._version >= 1.11)
            {
                this._currentSetpointsStruct111 = this._configuration111.Value;
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._currentSetpointsStruct107 = this._configuration108.Value;
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                this._currentSetpointsStruct105 = this._configuration105.Value;
            }
            else
            {
                this._currentSetpointsStruct = this._configuration.Value;
            }
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;

            //if (_version >= 1.12)
            //{
            //    signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, StringsConfig.DefaultSignaturesSignals);
                
            //    AddSignalsInListFromDataGridOscBase(out var oscList);

            //    StringsConfig.OscList = new List<string>();
            //    StringsConfig.OscList.AddRange(oscList);

            //    signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DISCRETS_COUNT, oscList);
                
            //    _formCheck.ShowResultMessage(READ_FAIL);
            //}
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            //if (_version >= 1.12)
            //{
            //    _formCheck.ShowMessage(WRITING_BDLIST);

            //    AddSignalsInListFromDataGridOscBase(out var oscList);

            //    StringsConfig.OscList = new List<string>();
            //    StringsConfig.OscList.AddRange(oscList);

            //    if (!string.IsNullOrEmpty(Framework.Framework.PasswordForSignatures))
            //    {
            //        signaturesSignalsConfigControl.PreparationListSignatures();

            //        WritingListSignatures(signaturesSignalsConfigControl.SignalsList);
            //    }
            //    else
            //    {
            //        ConfigAndSignaturesWrited();
            //    }
            //}
            //else
            //{
                ConfigAndSignaturesWrited();
            //}
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        #endregion [MemoryEntity Events Handlers]

        #region [Help members]

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }
        
        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            if (this._version >= 1.11)
            {
                this._configurationValidator111.Set(this._currentSetpointsStruct111);
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._configurationValidator108.Set(this._currentSetpointsStruct107);
            }
            else if (this._version >= 1.05)
            {
                this._configurationValidator105.Set(this._currentSetpointsStruct105);
            }
            else
            {
                this._configurationValidator.Set(this._currentSetpointsStruct);
            }
            if (this._resistCheck.Checked) this.GetPrimaryResistValue();

            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi2Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._clearSetpointBtn.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool IsWriteConfiguration()
        {
            this.IsProcess = true;
            string message;
            this._statusLabel.Text = "Проверка уставок";
            this.statusStrip1.Update();
            if (this._version >= 1.11)
            {
                if (this._configurationValidator111.Check(out message, true))
                {
                    if (this._resistCheck.Checked)
                        this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._statusLabel.Text = WRITING_CONFIG;
                    this._currentSetpointsStruct111 = this._configurationValidator111.Get();
                    this._configuration111.Value = this._currentSetpointsStruct111;
                    return true;
                }
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                if (this._configurationValidator108.Check(out message, true))
                {
                    if (this._resistCheck.Checked)
                        this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._statusLabel.Text = WRITING_CONFIG;
                    this._currentSetpointsStruct107 = this._configurationValidator108.Get();
                    this._configuration108.Value = this._currentSetpointsStruct107;
                    return true;
                }
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                if (this._configurationValidator105.Check(out message, true))
                {
                    if (this._resistCheck.Checked)
                        this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._statusLabel.Text = WRITING_CONFIG;
                    this._currentSetpointsStruct105 = this._configurationValidator105.Get();
                    this._configuration105.Value = this._currentSetpointsStruct105;
                    return true;
                }
            }
            else
            {
                if (this._configurationValidator.Check(out message, true))
                {
                    if (this._resistCheck.Checked)
                        this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._statusLabel.Text = WRITING_CONFIG;
                    this._currentSetpointsStruct = this._configurationValidator.Get();
                    this._configuration.Value = this._currentSetpointsStruct;
                    return true;
                }
            }
            MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
            this.IsProcess = false;
            return false;
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            this.IsProcess = true;

            this._statusLabel.Text = READING_CONFIG;
            this._progressBar.Value = 0;
            if (this._version >= 1.11)
            {
                this._configuration111.LoadStruct();
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._configuration108.LoadStruct();
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                this._configuration105.LoadStruct();
            }
            else
            {
                this._configuration.LoadStruct();
            }
        }

        private double GetKoeff()
        {
            double ittf, ktnf, tokIn;
            MeasureTransStruct str = this._measureTransUnionGr1.Get();
            ittf = str.ChannelI.Ittl;
            ktnf = str.ChannelU.KthlValue;
            tokIn = str.ChannelI.InpInValue;
            return ktnf / ittf * tokIn;
        }

        private void GetSecondResistValue()
        {
            this.IsProcess = true;
            double koef = 1/this.GetKoeff();
            for (int i = 0; i < this._primary.Length; i++)
            {
                this._second[i].Text = string.Format("{0:F4}", Convert.ToDouble(this._primary[i].Text)*koef);
            }
            this.resistanceDefTabCtr1.GetSecondValue(koef);
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        private void GetPrimaryResistValue()
        {
            this.IsProcess = true;
            double koef = this.GetKoeff();
            for (int i = 0; i < this._second.Length; i++)
            {
                this._primary[i].Text = string.Format("{0:F4}", Convert.ToDouble(this._second[i].Text)*koef);
            }
            this.resistanceDefTabCtr1.GetPrimaryValue(koef);
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        #endregion [Help members]

        #region [Event handlers]

        private void Mr771ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (this._device.IsConnect && _device.DeviceDlgInfo.IsConnectionMode)
            {
                //if (_version >= 1.12)
                //{
                //    this.ReadConfigAndSignals();
                //}
                //else
                //{
                    StartRead();
                //}
            }
            else
            {
                this.ResetSetpoints(false);
            }
        }

        private async void ReadConfigAndSignals()
        {
            UpdateStatus(READING_CONFIG, true);

            IsProcess = true;

            //if (!string.IsNullOrEmpty(Framework.Framework.PasswordForSignatures))
            //{
            //    _fileDriver.Password = Framework.Framework.PasswordForSignatures;
                
            //    _fileDriver.ReadFile(SignaturesListRead, LIST_FILE_NAME);
                
            //    InitFormCheck(READING_CONFIG, READING_BDLIST);
            //}
            //else
            //{
            //    if (DialogResult.OK == MessageBox.Show("Для чтения подписей сигналов, требуется ввести пароль в настройках проекта!",
            //        "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information))
            //    {
            //        this.StartRead();
            //    }
            //}
        }

        private void InitFormCheck(string caption, string message)
        {
            _formCheck.SetProgressBarStyle(ProgressBarStyle.Marquee);
            _formCheck.Caption = caption;
            _formCheck.OkBtnVisibility = false;
            _formCheck.ShowDialog(message);
        }

        private async void SignaturesForOscListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(SignaturesForOsc), root);
                            SignaturesForOsc lists = (SignaturesForOsc)serializer.Deserialize(reader);

                            signaturesOsc.Messages = lists.Signatures.MessagesList;
                            
                            _formCheck.ShowMessage(READING_CONFIG);

                            StartRead();

                            UpdateListsInStringsConfig();

                            try { Init(); }
                            catch (Exception e){}
                        }
                    }
                }
                
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                UpdateListsInStringsConfig();

                try { Init(); }
                catch (Exception e) { }

                this.StartRead();
            }
        }

        private async void SignaturesListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfConfigurations), root);
                            ListsOfConfigurations lists = (ListsOfConfigurations)serializer.Deserialize(reader);

                            signaturesSignalsConfigControl.SignalsList = lists.SignalsList.MessagesList;
                            
                            _fileDriver.ReadFile(SignaturesForOscListRead, LIST_FILE_NAME_FOR_OSC);
                        }
                    }
                }
                else if (mes == "Неверный пароль")
                {
                    if (DialogResult.OK ==
                        MessageBox.Show(
                            "Введен неверный пароль!" + "\nПожалуйста, обратитесь к поставщику производителя.", "Внимание!", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning))
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                signaturesSignalsConfigControl.SignalsList = StringsConfig.DefaultSignaturesSignals;

                signaturesSignalsConfigControl.AddedItemInList(StringsConfig.RelaySignals);

                signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, StringsConfig.DefaultSignaturesSignals);

                signaturesOsc.Messages = StringsConfig.DefaultSignaturesForOsc;

                _fileDriver.ReadFile(SignaturesForOscListRead, LIST_FILE_NAME_FOR_OSC);
            }
        }

        //private void ClearVlsAndLs()
        //{
        //    allVlsCheckedListBoxs.Clear();
        //    dataGridsViewLsOR.Clear();
        //    dataGridsViewLsAND.Clear();
        //}

        private void UpdateListsInStringsConfig()
        {
            try
            {
                signaturesSignalsConfigControl.SetToolTipForTabPages(_lsTabPages, 41);
                signaturesSignalsConfigControl.SetToolTipForTabPages(_vlsTabPages, 41 + 16);

                signaturesSignalsConfigControl.AddedItemInList(StringsConfig.RelaySignals);
                signaturesSignalsConfigControl.AddedItemInListInvSignals("Z 1 ИО", "", StringsConfig.SwitchSignals);
                signaturesSignalsConfigControl.AddedItemInListInvSignals("ВНЕШ", "", StringsConfig.ExternalDafenseSrab);
                signaturesSignalsConfigControl.AddedItemInListVLS("ВЛС1", "ВЛС16", "", StringsConfig.VlsSignals);
                signaturesSignalsConfigControl.AddedItemInListVLS("НЕТ", "ССЛ48", "I> 1 ИО", StringsConfig.HflSignalsDZ);
                signaturesSignalsConfigControl.AddedItemInListVLS("НЕТ", "I<", "I2/I1> ИО", StringsConfig.HflSignalsTZNP);
            }
            catch (Exception ex) {}
            
        }

        private void WriteSignaturesForOscIsCompleted(bool sucсess, string mes)
        {
            switch (mes)
            {
                case "Операция успешно выполнена":

                    this._statusLabel.Text = "Запись подписей успешно завершена";
                    
                    Init();
                    this.ShowConfiguration();
                    AddSignalsInListFromDataGridOscBase(out var oscList);

                    StringsConfig.OscList = new List<string>();
                    StringsConfig.OscList.AddRange(oscList);

                    signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DISCRETS_COUNT, oscList);
                    
                    ConfigAndSignaturesWrited();
                    break;
                default:
                    break;
            }
        }

        private void WriteSignaturesIsCompleted(bool sucсess, string mes)
        {
            switch (mes)
            {
                case "Неверный пароль":

                    if (DialogResult.OK == MessageBox.Show(
                            "Введен неверный пароль!" + "\nПожалуйста, обратитесь к поставщику производителя.", "Внимание!", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning))
                    {
                        ConfigAndSignaturesWrited();
                    }
                    break;

                case "Операция успешно выполнена":

                    this._statusLabel.Text = "Запись подписей успешно завершена";
                    
                    try
                    {
                        signaturesSignalsConfigControl.ClearDgv();
                        signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList, StringsConfig.DefaultSignaturesSignals);

                        UpdateListsInStringsConfig();

                        signaturesOsc.PreparationListSignatures();
                        WritingListSignaturesForOsc(signaturesOsc.Messages);

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ошибка заполнения таблицы");
                    }
                    break;
                default:
                    ConfigAndSignaturesWrited();
                    break;
            }
        }
        public void UpdateStatus(string status, bool cursorWait = false, bool isFormLoad = false)
        {
            this.Invoke(new Action(() =>
            {
                this._statusLabel.Text = status;
                //update the statusStrip
                this.statusStrip1.Update();
                if (cursorWait)
                    this.Cursor = Cursors.WaitCursor;
                else
                    this.Cursor = Cursors.Default;
            }));
        }

        private void ConfigAndSignaturesWrited()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0xd00, true, "Сохранить конфигурацию", this._device);

            //if (_version >= 1.12)
            //{
            //    UpdateStatus(WRITE_OK);
            //    _formCheck.ShowResultMessage(WRITE_OK);
            //}
        }

        private void WritingListSignatures(List<string> list)
        {
            //if (!string.IsNullOrEmpty(Framework.Framework.PasswordForSignatures))
            //{
            //    _fileDriver.Password = Framework.Framework.PasswordForSignatures;

            //    WritingSignaturesProcess(list);

            //    this._statusLabel.Text = "Идет запись подписей сигналов";
            //}
        }

        private void WritingListSignaturesForOsc(List<string> list)
        {
            WritingSignaturesProcessForOsc(list);

            this._statusLabel.Text = "Идет запись подписей сигналов";
        }

        private void WritingSignaturesProcessForOsc(List<string> list)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.GetEncoding(1251)))
                {
                    writer.Formatting = Formatting.Indented;

                    signaturesOsc.SettingsSignaturesForOsc(
                        this._device.DeviceType,
                        list,
                        Common.VersionConverter(_device.DeviceVersion)
                        ).XmlToFile(writer);
                    
                    byte[] buff = stream.GetBuffer().Take((int)stream.Length).ToArray();

                    this._fileDriver.WriteFile(this.WriteSignaturesForOscIsCompleted, buff, LIST_FILE_NAME_FOR_OSC);
                }
            }
        }

        private void WritingSignaturesProcess(List<string> list)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.GetEncoding(1251)))
                {
                    writer.Formatting = Formatting.Indented;
                    signaturesSignalsConfigControl.ListConfiguration(
                        this._device.DeviceType,
                        list,
                        Common.VersionConverter(_device.DeviceVersion)
                    ).XmlToFile(writer);
                    byte[] buff = stream.GetBuffer().Take((int) stream.Length).ToArray();

                    this._fileDriver.WriteFile(this.WriteSignaturesIsCompleted, buff, LIST_FILE_NAME);
                }
            }
        }

        private void Mr771ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._configuration111?.RemoveStructQueries();
            this._configuration108?.RemoveStructQueries();
            this._configuration105?.RemoveStructQueries();
            this._configuration?.RemoveStructQueries();
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            if (/*Device.AutoloadConfig && */this._device.DeviceDlgInfo.IsConnectionMode && _device.IsConnect)
            {
                //if (_version >= 1.12)
                //{
                //    this.ReadConfigAndSignals();
                //}
                //else
                //{
                    this.StartRead();

                    Init();
                //}
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode && !this._device.IsConnect) return;

            WriteConfiguration();
        }

        private void WriteConfiguration()
        {
            //if (string.IsNullOrEmpty(Framework.Framework.PasswordForSignatures) && _version >= 1.12)
            //{
            //    if (DialogResult.OK == MessageBox.Show("Для записи подписей сигналов, требуется ввести пароль в настройках проекта!",
            //            "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information))
            //    {
            //    }
            //}
            if (TextboxSupport.PassedValidation)
            {
                if (this._version >= 1.08)
                {
                    DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 771 №{0}? " +
                                                                        "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                            this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                        "Запись", MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                else
                {
                    DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 771 №{0}? ",
                            this._device.DeviceNumber),
                        "Запись", MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                
                WriteConfig();

                //if (_version >= 1.12)
                //{
                //    UpdateStatus(WRITING_CONFIG_CAPTION, true);

                //    InitFormCheck(WRITING_CONFIG_CAPTION, WRITING_CONFIG);
                //}
                //else
                //{
                    _statusLabel.Text = WRITING_CONFIG;
                //}
            }
            else
            {
                MessageBox.Show(
                    "На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.",
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void WriteConfig()
        {
            this._progressBar.Value = 0;
            if (!this.IsWriteConfiguration()) return;
            if (this._version >= 1.11)
            {
                this._configuration111.SaveStruct();
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._configuration108.SaveStruct();
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                this._configuration105.SaveStruct();
            }
            else
            {
                this._configuration.SaveStruct();
            }
        }
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null) return;
            int ind = comboBox.SelectedIndex;

            const int OSC_SIZE = 54528*2;

            this._oscSizeTextBox.Text = ((int) (OSC_SIZE/(ind + 2))).ToString(); //StringsConfig.OscLen[ind];
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        private void _clearSetpointsButton_Click(object sender, EventArgs e)
        {
            ClearSetpoints();
        }

        private void ClearSetpoints()
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (this._version >= 1.11)
            {
                signaturesSignalsConfigControl.ClearDgv();
                signaturesOsc.ClearDgv();

                this._configurationValidator111.Reset("MR771");
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                this._configurationValidator108.Reset("MR771");
            }
            else if (this._version >= 1.05 && this._version < 1.08)
            {
                this._configurationValidator105.Reset("MR771");
            }
            else
            {
                this._configurationValidator.Reset();
            }

            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        private void _resistCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._return)
            {
                this._return = false;
                return;
            }
            if (this._resistCheck.Checked)
            {
                DialogResult res =
                    MessageBox.Show("Сопротивления будут пересчитаны в первичные величины. Обратите внимание на корректность введенных значений IТТф и KТНф",
                        "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                string mes;

                if (this._version < 1.11)
                {
                    bool check = this._measureTransUnionGr1.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);

                    if (check)
                    {
                        if (res == DialogResult.OK)
                        {
                            this.omp1.Visible = false;
                            this.omp2.Visible = true;
                            this.resistanceDefTabCtr1.IsPrimary = true;
                            this.GetPrimaryResistValue();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                else
                {
                    bool check = this._measureTransUnionGr1.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidatorNew.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);

                    if (check)
                    {
                        if (res == DialogResult.OK)
                        {
                            this.omp1.Visible = false;
                            this.omp2.Visible = true;
                            this.resistanceDefTabCtr1.IsPrimary = true;
                            this.GetPrimaryResistValue();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                
                this._return = true;
                this._resistCheck.Checked = false;
            }
            else
            {
                DialogResult res = MessageBox.Show("Сопротивления будут пересчитаны во вторичные величины к размерности Ом*Iн.вт. Обратите внимание на корректность введенных значений IТТф и KТНф",
                    "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                string mes;

                if (this._version < 1.11)
                {
                    bool check = this._measureTransUnionGr1.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);
                    if (check)
                    {
                        if (res == DialogResult.OK)
                        {
                            this.omp1.Visible = true;
                            this.omp2.Visible = false;
                            this.resistanceDefTabCtr1.IsPrimary = false;
                            this.GetSecondResistValue();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    bool check = this._measureTransUnionGr1.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidatorNew.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);
                    if (check)
                    {
                        if (res == DialogResult.OK)
                        {
                            this.omp1.Visible = true;
                            this.omp2.Visible = false;
                            this.resistanceDefTabCtr1.IsPrimary = false;
                            this.GetSecondResistValue();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                this._return = true;
                this._resistCheck.Checked = true;
            }
        }

        private void Mr771ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                //if (_version >= 1.12)
                //{
                //    this.ReadConfigAndSignals();
                //}
                //else
                //{
                    StartRead();
                //}
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                WriteConfiguration();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.resetSetpointsItem)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                ClearSetpoints();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]
            {
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.resetSetpointsItem,
                this.clearSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem
            });
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void konturFF_CheckedChanged(object sender, EventArgs e)
        {
            this.konturFFGroup.Enabled = this.konturFF.Checked;
        }

        private void konturFN_CheckedChanged(object sender, EventArgs e)
        {
            this.konturFNGroup.Enabled = this.konturFN.Checked;
        }
        
        #region [Обнуление сигналов ЛС 1-8]

        /// <summary>
        /// Отслеживаем, какой из табов выбран в ЛС 1-8
        /// </summary>

        public bool _tP39;
        public bool _tP40;
        public bool _tP41;
        public bool _tP42;
        public bool _tP43;
        public bool _tP44;
        public bool _tP45;
        public bool _tP46;

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lsFirsTabControl.SelectedTab == tabPage39)
            {
                _tP39 = true;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage40)
            {
                _tP39 = false;
                _tP40 = true;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage41)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = true;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage42)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = true;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage43)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = true;
                _tP44 = false;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage44)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = true;
                _tP45 = false;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage45)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = true;
                _tP46 = false;
                return;
            }
            if (_lsFirsTabControl.SelectedTab == tabPage46)
            {
                _tP39 = false;
                _tP40 = false;
                _tP41 = false;
                _tP42 = false;
                _tP43 = false;
                _tP44 = false;
                _tP45 = false;
                _tP46 = true;
                return;
            }
        }
        
        /// <summary>
        /// Обнуление сигналов ЛС 1-8
        /// </summary>
        private void ResetCurrentLs()
        {
            if (_tP39)
            {
                for (int i = 0; i < _lsAndDgv1Gr1.Rows.Count; i++)
                {
                    _lsAndDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP40)
            {
                for (int i = 0; i < _lsAndDgv2Gr1.Rows.Count; i++)
                {
                    _lsAndDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP41)
            {
                for (int i = 0; i < _lsAndDgv3Gr1.Rows.Count; i++)
                {
                    _lsAndDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP42)
            {
                for (int i = 0; i < _lsAndDgv4Gr1.Rows.Count; i++)
                {
                    _lsAndDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP43)
            {
                for (int i = 0; i < _lsAndDgv5Gr1.Rows.Count; i++)
                {
                    _lsAndDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP44)
            {
                for (int i = 0; i < _lsAndDgv6Gr1.Rows.Count; i++)
                {
                    _lsAndDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP45)
            {
                for (int i = 0; i < _lsAndDgv7Gr1.Rows.Count; i++)
                {
                    _lsAndDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP46)
            {
                for (int i = 0; i < _lsAndDgv8Gr1.Rows.Count; i++)
                {
                    _lsAndDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
        }

        private void ClearAllLs()
        {
            for (int i = 0; i < _lsAndDgv1Gr1.Rows.Count; i++)
            {
                _lsAndDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv2Gr1.Rows.Count; i++)
            {
                _lsAndDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
            
            for (int i = 0; i < _lsAndDgv3Gr1.Rows.Count; i++)
            {
                _lsAndDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
            
            for (int i = 0; i < _lsAndDgv4Gr1.Rows.Count; i++)
            {
                _lsAndDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv5Gr1.Rows.Count; i++)
            {
                _lsAndDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv6Gr1.Rows.Count; i++)
            {
                _lsAndDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
            for (int i = 0; i < _lsAndDgv7Gr1.Rows.Count; i++)
            {
                _lsAndDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < _lsAndDgv8Gr1.Rows.Count; i++)
            {
                _lsAndDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }
            
        }

        private void _clearCurrentLsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ЛС", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            if (_lsFirsTabControl.SelectedTab == tabPage39)
            {
                _tP39 = true;
            }
            this.ResetCurrentLs();
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
        }

        private void _clearAllLsSygnalButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигналы ЛС 1-8?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this.ClearAllLs();
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
        }

        #endregion

        #region [Обнуление сигналов ЛС 9-16]

        public bool _tP31;
        public bool _tP32;
        public bool _tP33;
        public bool _tP34;
        public bool _tP35;
        public bool _tP36;
        public bool _tP37;
        public bool _tP38;

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lsSecondTabControl.SelectedTab == tabPage31)
            {
                _tP31 = true;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage32)
            {
                _tP31 = false;
                _tP32 = true;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage33)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = true;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage34)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = true;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage35)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = true;
                _tP36 = false;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage36)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = true;
                _tP37 = false;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage37)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = true;
                _tP38 = false;
                return;
            }
            if (_lsSecondTabControl.SelectedTab == tabPage38)
            {
                _tP31 = false;
                _tP32 = false;
                _tP33 = false;
                _tP34 = false;
                _tP35 = false;
                _tP36 = false;
                _tP37 = false;
                _tP38 = true;
                return;
            }
        }

        /// <summary>
        /// Обнуление сигналов ЛС 9-16
        /// </summary>
        private void ResetCurrentLsn()
        {
            if (_tP31)
            {
                for (int i = 0; i < _lsOrDgv1Gr1.Rows.Count; i++)
                {
                    _lsOrDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP32)
            {
                for (int i = 0; i < _lsOrDgv2Gr1.Rows.Count; i++)
                {
                    _lsOrDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP33)
            {
                for (int i = 0; i < _lsOrDgv3Gr1.Rows.Count; i++)
                {
                    _lsOrDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP34)
            {
                for (int i = 0; i < _lsOrDgv4Gr1.Rows.Count; i++)
                {
                    _lsOrDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP35)
            {
                for (int i = 0; i < _lsOrDgv5Gr1.Rows.Count; i++)
                {
                    _lsOrDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP36)
            {
                for (int i = 0; i < _lsOrDgv6Gr1.Rows.Count; i++)
                {
                    _lsOrDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP37)
            {
                for (int i = 0; i < _lsOrDgv7Gr1.Rows.Count; i++)
                {
                    _lsOrDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (_tP38)
            {
                for (int i = 0; i < _lsOrDgv8Gr1.Rows.Count; i++)
                {
                    _lsOrDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
        }

        private void ClearAllLsn()
        {
            for (int i = 0; i < _lsOrDgv1Gr1.Rows.Count; i++)
            {
                _lsOrDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv2Gr1.Rows.Count; i++)
            {
                _lsOrDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }


            for (int i = 0; i < _lsOrDgv3Gr1.Rows.Count; i++)
            {
                _lsOrDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }


            for (int i = 0; i < _lsOrDgv4Gr1.Rows.Count; i++)
            {
                _lsOrDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv5Gr1.Rows.Count; i++)
            {
                _lsOrDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv6Gr1.Rows.Count; i++)
            {
                _lsOrDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv7Gr1.Rows.Count; i++)
            {
                _lsOrDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < _lsOrDgv8Gr1.Rows.Count; i++)
            {
                _lsOrDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

        }

        private void _clearCurrentLsnButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (_lsSecondTabControl.SelectedTab == tabPage31)
            {
                _tP31 = true;
            }

            this.ResetCurrentLsn();
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        private void _clearAllLsnSygnalButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигналы ЛС 9-16?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this.ClearAllLsn();
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }

        #endregion

        #region [Обнуление сигналов ВЛС]

        private bool _tBVLS1;
        private bool _tBVLS2;
        private bool _tBVLS3;
        private bool _tBVLS4;
        private bool _tBVLS5;
        private bool _tBVLS6;
        private bool _tBVLS7;
        private bool _tBVLS8;
        private bool _tBVLS9;
        private bool _tBVLS10;
        private bool _tBVLS11;
        private bool _tBVLS12;
        private bool _tBVLS13;
        private bool _tBVLS14;
        private bool _tBVLS15;
        private bool _tBVLS16;
        
        private void ResetCurrentVlsSignal()
        {
            if (_tBVLS1)
            {
                for (int i = 0; i < VLSclb1Gr1.Items.Count; i++)
                {
                    VLSclb1Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS2)
            {
                for (int i = 0; i < VLSclb2Gr1.Items.Count; i++)
                {
                    VLSclb2Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS3)
            {
                for (int i = 0; i < VLSclb3Gr1.Items.Count; i++)
                {
                    VLSclb3Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS4)
            {
                for (int i = 0; i < VLSclb4Gr1.Items.Count; i++)
                {
                    VLSclb4Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS5)
            {
                for (int i = 0; i < VLSclb5Gr1.Items.Count; i++)
                {
                    VLSclb5Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS6)
            {
                for (int i = 0; i < VLSclb6Gr1.Items.Count; i++)
                {
                    VLSclb6Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS7)
            {
                for (int i = 0; i < VLSclb7Gr1.Items.Count; i++)
                {
                    VLSclb7Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS8)
            {
                for (int i = 0; i < VLSclb8Gr1.Items.Count; i++)
                {
                    VLSclb8Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS9)
            {
                for (int i = 0; i < VLSclb9Gr1.Items.Count; i++)
                {
                    VLSclb9Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS10)
            {
                for (int i = 0; i < VLSclb10Gr1.Items.Count; i++)
                {
                    VLSclb10Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS11)
            {
                for (int i = 0; i < VLSclb11Gr1.Items.Count; i++)
                {
                    VLSclb11Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS12)
            {
                for (int i = 0; i < VLSclb12Gr1.Items.Count; i++)
                {
                    VLSclb12Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS13)
            {
                for (int i = 0; i < VLSclb13Gr1.Items.Count; i++)
                {
                    VLSclb13Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS14)
            {
                for (int i = 0; i < VLSclb14Gr1.Items.Count; i++)
                {
                    VLSclb14Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS15)
            {
                for (int i = 0; i < VLSclb15Gr1.Items.Count; i++)
                {
                    VLSclb1Gr1.SetItemChecked(i, false);
                }
            }
            if (_tBVLS16)
            {
                for (int i = 0; i < VLSclb16Gr1.Items.Count; i++)
                {
                    VLSclb16Gr1.SetItemChecked(i, false);
                }
            }
        }

        private void _resetCurrentVlsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ВЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (_vlsTabControl.SelectedTab == VLS1)
            {
                this._tBVLS1 = true;
            }
            this.ResetCurrentVlsSignal();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
        }

        private void _resetAllVlsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигнал ВЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this._vlsUnion.Reset();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.ChangeTextVls();

            if (_vlsTabControl.SelectedTab == VLS1)
            {
                _tBVLS1 = true;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS2)
            {
                _tBVLS1 = false;
                _tBVLS2 = true;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS3)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = true;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS4)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = true;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS5)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = true;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS6)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = true;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS7)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = true;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS8)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = true;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS9)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = true;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS10)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = true;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS11)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = true;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS12)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = true;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS13)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = true;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS14)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = true;
                _tBVLS15 = false;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS15)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = true;
                _tBVLS16 = false;
                return;
            }
            if (_vlsTabControl.SelectedTab == VLS16)
            {
                _tBVLS1 = false;
                _tBVLS2 = false;
                _tBVLS3 = false;
                _tBVLS4 = false;
                _tBVLS5 = false;
                _tBVLS6 = false;
                _tBVLS7 = false;
                _tBVLS8 = false;
                _tBVLS9 = false;
                _tBVLS10 = false;
                _tBVLS11 = false;
                _tBVLS12 = false;
                _tBVLS13 = false;
                _tBVLS14 = false;
                _tBVLS15 = false;
                _tBVLS16 = true;
                return;
            }

        }

        #endregion
        
        #endregion [Event handlers]

        #region [Save/Load File Members]

        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {
                XmlDocument doc = new XmlDocument();
               
                if (this._version >= 1.08)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(MR771_BASE_CONFIG_PATH, this._device.DeviceVersion));
                }
                else if (this._version >= 1.05 && this._version < 1.08)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR771_BASE_CONFIG_PATH, this._device.DeviceVersion));
                }
                else
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR771_BASE_CONFIG_PATH, "1.02"));
                }

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (this._version >= 1.11)
                {
                    this._currentSetpointsStruct111.InitStruct(values);
                }
                else if (this._version >= 1.08 && this._version < 1.11)
                {
                    this._currentSetpointsStruct107.InitStruct(values);
                }
                else if (this._version >= 1.05 && this._version < 1.08)
                {
                    this._currentSetpointsStruct105.InitStruct(values);
                }
                else
                {
                    this._currentSetpointsStruct.InitStruct(values);
                }
                
                this.ShowConfiguration();

                //if (_version >= 1.12)
                //{
                //    InitSignaturesStruct();
                //}
                
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. \nОбнулить уставки?",
                            "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }

                if (this._version >= 1.11)
                {
                    this._configurationValidator111.Reset();
                }
                else if (this._version >= 1.08 && this._version < 1.11)
                {
                    this._configurationValidator108.Reset();
                }
                else if (this._version >= 1.05 && this._version < 1.08)
                {
                    this._configurationValidator105.Reset();
                }
                else
                {
                    this._configurationValidator.Reset();
                }

                //if (_version >= 1.12)
                //{
                //    InitSignaturesStruct();
                //}
            }
        }

        private void InitSignaturesStruct()
        {
            signaturesSignalsConfigControl.ListConfiguration(
                this._device.DeviceType,
                StringsConfig.DefaultSignaturesSignals,
                Common.VersionConverter(_device.DeviceVersion)
            );
            signaturesOsc.SettingsSignaturesForOsc(
                this._device.DeviceType,
                StringsConfig.DefaultSignaturesForOsc,
                100);
            signaturesSignalsConfigControl.FillSignaturesSignalsDgv(signaturesSignalsConfigControl.SignalsList,
                StringsConfig.DefaultSignaturesSignals);

            AddSignalsInListFromDataGridOscBase(out var oscList);

            StringsConfig.OscList = new List<string>();
            StringsConfig.OscList.AddRange(oscList);

            signaturesOsc.FillSignaturesSignalsDgv(signaturesOsc.Messages, InputLogicStruct.DISCRETS_COUNT, oscList);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveInFile();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveInFile()
        {
            if (this.IsWriteConfiguration())
            {
                this._statusLabel.Text = "Идет запись конфигурации в файл";
                this._saveConfigurationDlg.FileName = 
                    string.Format("МР771_Уставки_версия_{0}.xml",this._device.DeviceVersion);
                if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog())
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                CursorWaiting(true);
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                CursorWaiting(true);

                this._statusLabel.Text = "Идет чтение конфигурации из файла";

                this.Deserialize(this._openConfigurationDlg.FileName);
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
            }
        }
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("Mr771"));

                ushort[] configValues;
                List<string> signalsInDeviceValues = null;
                List<string> signalsUserValues = null;
                List<string> signalsInDeviceValuesForOsc = null;
                List<string> signalsUserValuesForOsc = null;
                
                if (this._version >= 1.11)
                {
                    configValues = this._currentSetpointsStruct111.GetValues();
                    //if (_version >= 1.12)
                    //{
                    //    signalsInDeviceValues = signaturesSignalsConfigControl.SaveToFile(out signalsInDeviceValues, 1);
                    //    signalsUserValues = signaturesSignalsConfigControl.SaveToFile(out signalsUserValues, 2);
                    //    signalsInDeviceValuesForOsc = signaturesOsc.SaveToFile(out signalsInDeviceValuesForOsc, 1);
                    //    signalsUserValuesForOsc = signaturesOsc.SaveToFile(out signalsUserValuesForOsc, 2);
                    //}
                }
                else if (this._version >= 1.08 && this._version < 1.11)
                {
                    configValues = this._currentSetpointsStruct107.GetValues();
                }
                else if (this._version >= 1.05 && this._version < 1.08)
                {
                    configValues = this._currentSetpointsStruct105.GetValues();
                }
                else
                {
                    configValues = this._currentSetpointsStruct.GetValues();
                }

                XmlElement configElement = doc.CreateElement(XML_HEAD);
                configElement.InnerText = Convert.ToBase64String(Common.TOBYTES(configValues, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }

                XmlElement sugnatures = doc.CreateElement("SIGNATURES");

                XmlElement sugnaturesForSignals = doc.CreateElement("SIGNATURES_FOR_SIGN");
                XmlElement sugnaturesForOsc = doc.CreateElement("SIGNATURES_FOR_OSC");

                XmlElement signaturesInDeviceElementForSignals = doc.CreateElement("SIGNATURES_IN_DEVICE");
                XmlElement signaturesUserElement = doc.CreateElement("SIGNATURES_USERS");

                XmlElement signaturesInDeviceElementForOsc = doc.CreateElement("SIGNATURES_IN_DEVICE");

                XmlElement signaturesUserElementForOsc = doc.CreateElement("SIGNATURES_USERS");

                //if (_version >= 1.12)
                //{
                    
                //    if (signalsInDeviceValues != null)
                //        for (int i = 0; i < signalsInDeviceValues.Count; i++)
                //        {
                //            XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                //            XmlText signalsText = doc.CreateTextNode(signalsInDeviceValues[i]);
                //            signalsCount.AppendChild(signalsText);
                //            signaturesInDeviceElementForSignals.AppendChild(signalsCount);
                //        }
                    
                //    if (signalsUserValues != null)
                //        for (int i = 0; i < signalsUserValues.Count; i++)
                //        {
                //            XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                //            XmlText signalsText = doc.CreateTextNode(signalsUserValues[i]);
                //            signalsCount.AppendChild(signalsText);
                //            signaturesUserElement.AppendChild(signalsCount);
                //        }
                    
                //    if (signalsInDeviceValuesForOsc != null)
                //        for (int i = 0; i < signalsInDeviceValuesForOsc.Count; i++)
                //        {
                //            XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                //            XmlText signalsText = doc.CreateTextNode(signalsInDeviceValuesForOsc[i]);
                //            signalsCount.AppendChild(signalsText);
                //            signaturesInDeviceElementForOsc.AppendChild(signalsCount);
                //        }
                    
                //    if (signalsUserValuesForOsc != null)
                //        for (int i = 0; i < signalsUserValuesForOsc.Count; i++)
                //        {
                //            XmlElement signalsCount = doc.CreateElement($"SIGNALS_{i + 1}");
                //            XmlText signalsText = doc.CreateTextNode(signalsUserValuesForOsc[i]);
                //            signalsCount.AppendChild(signalsText);
                //            signaturesUserElementForOsc.AppendChild(signalsCount);
                //        }

                //    sugnaturesForSignals.AppendChild(signaturesInDeviceElementForSignals);
                //    sugnaturesForSignals.AppendChild(signaturesUserElement);

                //    sugnaturesForOsc.AppendChild(signaturesInDeviceElementForOsc);
                //    sugnaturesForOsc.AppendChild(signaturesUserElementForOsc);

                //    sugnatures.AppendChild(sugnaturesForSignals);
                //    sugnatures.AppendChild(sugnaturesForOsc);
                //}
               
                doc.DocumentElement.AppendChild(configElement);
                
                //if (_version >= 1.12) doc.DocumentElement.AppendChild(sugnatures);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
                this.IsProcess = false;

                CursorWaiting(false);
            }
            catch
            {
                CursorWaiting(false);
                MessageBox.Show(FILE_SAVE_FAIL);
                this.IsProcess = false;
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode configNode = doc.FirstChild.SelectSingleNode(XML_HEAD);
                
                if (configNode == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(configNode.InnerText);
                
                if (this._version >= 1.11)
                {
                    //if (_version >= 1.12)
                    //{
                    //    XmlNode signatures = doc.LastChild.SelectSingleNode("SIGNATURES");

                    //    XmlNode signalsNode = signatures.FirstChild;
                    //    XmlNode oscNode = signatures.LastChild;

                    //    XmlNode inDeviceNodeForSign = signalsNode.FirstChild;
                    //    XmlNode inUsersNodeForSing = signalsNode.LastChild;

                    //    XmlNode inDeviceNodeForOsc = oscNode.FirstChild;
                    //    XmlNode inUsersNodeForOsc = oscNode.LastChild;

                    //    List<string> signalsInDeviceForSign = new List<string>();
                    //    List<string> signalsUserForSign = new List<string>();

                    //    List<string> signalsInDeviceForOsc = new List<string>();
                    //    List<string> signalsUserForOsc = new List<string>();


                    //    if (inDeviceNodeForSign != null)
                    //        for (int i = 0; i < inDeviceNodeForSign.ChildNodes.Count; i++)
                    //        {
                    //            signalsInDeviceForSign.Add(inDeviceNodeForSign.ChildNodes[i].InnerText);
                    //        }

                    //    if (inUsersNodeForSing != null)
                    //        for (int i = 0; i < inUsersNodeForSing.ChildNodes.Count; i++)
                    //        {
                    //            signalsUserForSign.Add(inUsersNodeForSing.ChildNodes[i].InnerText);
                    //        }

                    //    signaturesSignalsConfigControl.ClearDgv();
                    //    if (inDeviceNodeForSign != null || inUsersNodeForSing != null)
                    //    {
                    //        signaturesSignalsConfigControl.SignalsList = signalsInDeviceForSign;
                    //        signaturesSignalsConfigControl.FillSignaturesSignalsDgv(
                    //            signaturesSignalsConfigControl.SignalsList, StringsConfig.DefaultSignaturesSignals,
                    //            true, signalsInDeviceForSign, signalsUserForSign);
                    //    }
                    //    else
                    //    {
                    //        signaturesSignalsConfigControl.FillSignaturesSignalsDgv(
                    //            signaturesSignalsConfigControl.SignalsList, StringsConfig.DefaultSignaturesSignals,
                    //            true);
                    //    }

                    //    if (inDeviceNodeForOsc != null)
                    //        for (int i = 0; i < inDeviceNodeForOsc.ChildNodes.Count; i++)
                    //        {
                    //            signalsInDeviceForOsc.Add(inDeviceNodeForOsc.ChildNodes[i].InnerText);
                    //        }

                    //    if (inUsersNodeForOsc != null)
                    //        for (int i = 0; i < inUsersNodeForOsc.ChildNodes.Count; i++)
                    //        {
                    //            signalsUserForOsc.Add(inUsersNodeForOsc.ChildNodes[i].InnerText);
                    //        }

                    //    signaturesSignalsConfigControl.ClearDgv();
                    //    if (inDeviceNodeForOsc != null || inUsersNodeForOsc != null)
                    //    {
                    //        signaturesOsc.Messages = signalsInDeviceForOsc;
                    //        signaturesOsc.FillSignaturesSignalsDgv(signalsInDeviceForOsc, InputLogicStruct.DISCRETS_COUNT, null, true, signalsInDeviceForOsc,
                    //            signalsUserForOsc);
                    //    }
                    //    else
                    //    {
                    //        signaturesOsc.FillSignaturesSignalsDgv(signalsInDeviceForOsc, InputLogicStruct.DISCRETS_COUNT, null, true);
                    //    }

                    //    UpdateListsInStringsConfig();

                    //    allVlsCheckedListBoxs.Clear();
                    //    dataGridsViewLsOR.Clear();
                    //    dataGridsViewLsAND.Clear();

                    //    Init();

                    //}

                    this._currentSetpointsStruct111.InitStruct(values);
                }
                else if (this._version >= 1.08 && this._version < 1.11)
                {
                    this._currentSetpointsStruct107.InitStruct(values);
                }
                else if (this._version >= 1.05 && this._version < 1.08)
                {
                    this._currentSetpointsStruct105.InitStruct(values);
                }
                else
                {
                    this._currentSetpointsStruct.InitStruct(values);
                }
                
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);

                CursorWaiting(false);
            }
            catch( Exception e)
            {
                CursorWaiting(false);

                this._statusLabel.Text = string.Format("Ошибка загрузки файла {0}", binFileName);

                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private static void CursorWaiting(bool useWaitCursor)
        {
            Application.UseWaitCursor = useWaitCursor;
            Cursor.Current = useWaitCursor ? Cursors.WaitCursor : Cursors.Default;
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveToHtml();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.IsWriteConfiguration())
                {
                    this._statusLabel.Text = "Идет запись конфигурации в HTML";
                    ExportGroupForm exportGroup = new ExportGroupForm();
                    if (exportGroup.ShowDialog() != DialogResult.OK)
                    {
                        this.IsProcess = false;
                        this._statusLabel.Text = string.Empty;
                        return;
                    }

                    if (this._version >= 1.11)
                    {
                        this._currentSetpointsStruct111.DeviceVersion = this._version;
                        this._currentSetpointsStruct111.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct111.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct111.SizeOsc = this._oscSizeTextBox.Text;
                        this._currentSetpointsStruct111.Group = (int)exportGroup.SelectedGroup;
                        this.SaveToHtml(this._currentSetpointsStruct111, exportGroup.SelectedGroup);
                    }
                    else if (this._version >= 1.08 && this._version < 1.11)
                    {
                        this._currentSetpointsStruct107.DeviceVersion = this._version;
                        this._currentSetpointsStruct107.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct107.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct107.SizeOsc = this._oscSizeTextBox.Text;
                        this._currentSetpointsStruct107.Group = (int)exportGroup.SelectedGroup;
                        this.SaveToHtml(this._currentSetpointsStruct107, exportGroup.SelectedGroup);
                    }
                    else if (this._version >= 1.05)
                    {
                        this._currentSetpointsStruct105.DeviceVersion = this._version;
                        this._currentSetpointsStruct105.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct105.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct105.SizeOsc = this._oscSizeTextBox.Text;
                        this._currentSetpointsStruct105.Group = (int)exportGroup.SelectedGroup;
                        this.SaveToHtml(this._currentSetpointsStruct105, exportGroup.SelectedGroup);
                    }
                    else
                    {
                        this._currentSetpointsStruct.DeviceVersion = this._version;
                        this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct.SizeOsc = this._oscSizeTextBox.Text;
                        this._currentSetpointsStruct.Group = (int)exportGroup.SelectedGroup;
                        this.SaveToHtml(this._currentSetpointsStruct, exportGroup.SelectedGroup);
                    }
                }
                this.IsProcess = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
                this.IsProcess = false;
            }
        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = string.Format("{0} Уставки версия {1} все группы", "МР771", this._device.DeviceVersion);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR771All, fileName, str);
            }
            else
            {
                fileName = string.Format("{0} Уставки версия {1} группа{2}", "МР771", this._device.DeviceVersion, (int)group);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR771Group, fileName, str);
            }
        }

        #endregion [Save/Load File Members]

        #region [Switch setpoints]

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;
            if (this._version >= 1.11)
            {
                if (this._setpointUnionV111.Check(out message, true))
                {
                    GroupSetpoint111[] allSetpoints =
                        this.CopySetpoints<AllGroupSetpointStruct111, GroupSetpoint111>(this._setpointUnionV111, this._setpointsValidatorV111);
                    this._currentSetpointsStruct111.AllGroupSetpoints.Setpoints = allSetpoints;
                    this._setpointsValidatorV111.Set(this._currentSetpointsStruct111.AllGroupSetpoints);
                }
                else
                {
                    MessageBox.Show("Обнаружены некорректные уставки");
                    return;
                }
            }
            else if (this._version >= 1.08 && this._version < 1.11)
            {
                if (this._setpointUnionV107.Check(out message, true))
                {
                    GroupSetpoint108[] allSetpoints =
                        this.CopySetpoints<AllGroupSetpointStruct108, GroupSetpoint108>(this._setpointUnionV107, this._setpointsValidatorV107);
                    this._currentSetpointsStruct107.AllGroupSetpoints.Setpoints = allSetpoints;
                    this._setpointsValidatorV107.Set(this._currentSetpointsStruct107.AllGroupSetpoints);
                }
                else
                {
                    MessageBox.Show("Обнаружены некорректные уставки");
                    return;
                }
            }
            else
            {
                if (this._setpointUnionV1.Check(out message, true))
                {
                    GroupSetpointStruct[] allSetpoints =
                        this.CopySetpoints<AllGroupSetpointStruct, GroupSetpointStruct>(this._setpointUnionV1, this._setpointsValidatorV1);
                    if (this._version >= 1.05)
                    {
                        this._currentSetpointsStruct105.AllGroupSetpoints.Setpoints = allSetpoints;
                        this._setpointsValidatorV1.Set(this._currentSetpointsStruct105.AllGroupSetpoints);
                    }
                    else
                    {
                        this._currentSetpointsStruct.AllGroupSetpoints.Setpoints = allSetpoints;
                        this._setpointsValidatorV1.Set(this._currentSetpointsStruct.AllGroupSetpoints);
                    }
                }
                else
                {
                    MessageBox.Show("Обнаружены некорректные уставки");
                    return;
                }
            }
            MessageBox.Show("Копирование завершено");
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2) union.Get();
            T2[] allSetpoints = ((T1) setpointValidator.Get()).Setpoints;
            if ((string) this._copySetpoinsGroupComboBox.SelectedItem == StringsConfig.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        private void OnRefreshInfoTable()
        {
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
        }
        
        #endregion

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr771); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr771ConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        #endregion [IFormView Members]       

        #region [Event CreateTree]

        private void _grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }


        private void VLSclbGr1_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                TreeViewVLS.StateNodes(this.treeViewForVLS);
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
                TreeViewVLS.ExpandCurrentTreeNode(this._vlsTabControl, this.treeViewForVLS);
                TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
            }
            catch (Exception exception)
            {
            }
            
        }
        private void treeViewForVLS_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left) return;
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this._vlsTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }
        private void _lsAndDgvGr1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandCurrentTreeNode(this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }
        private void _lsOrDgvGr1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
            TreeViewLS.ExpandCurrentTreeNode(this._lsSecondTabControl, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }
        private void treeViewForLsAND_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsAND);
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this._lsFirsTabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }
        private void treeViewForLsOR_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsOR);
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this._lsSecondTabControl, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void _wrapBtn_Click(object sender, EventArgs e)
        {
            if (this._wrapBtn.Text == "Свернуть")
            {
                this.treeViewForVLS.CollapseAll();
                this._wrapBtn.Text = "Развернуть";
            }
            else
            {
                this.treeViewForVLS.ExpandAll();
                this._wrapBtn.Text = "Свернуть";
            }
        }
        #endregion [Event CreateTree]
    }
}