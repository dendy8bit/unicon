﻿using System;
using System.Linq;
using System.Threading;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR771.Configuration
{
    public class SignValidatingRule : IValidatingRule<double>
    {
        public SignValidatingRule(double min, double max)
        {
            this.Max = max;
            this.Min = min;
        }

        public bool IsValid(string value)
        {
            try
            {
                return this.IsValid(this.Parse(value));
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsValid(double value)
        {
            return ((value.CompareTo(this.Min) >= 0) && (value.CompareTo(this.Max) <= 0));
        }

        protected virtual string PrepareString(string value)
        {
            var separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            return value.Replace(",", separator).Replace(".", separator);
        }

        public double Parse(string value)
        {
            return double.Parse(value);
        }

        public double Min
        {
            get;
            set;
        }

        public double Max
        {
            get;
            set;
        }

        public bool IsValidKey(char key)
        {
            return !((key == '-') | char.IsDigit(key) | (key == '\b') |
                  Thread.CurrentThread.CurrentUICulture.NumberFormat.NumberDecimalSeparator.Contains(key));
        }

        object IValidatingRule.Parse(string value)
        {
            return this.Parse(value);
        }

        object IValidatingRule.Min
        {
            get { return this.Min; }
        }

        object IValidatingRule.Max
        {
            get { return this.Max; }
        }

        public string ErrorMessage
        {
            get { return string.Format("{0}..{1}", this.Min, this.Max); }
        }
    }
}
