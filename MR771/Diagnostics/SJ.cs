﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BEMN.MR771.Diagnostics
{
    [Serializable]
    [XmlRoot("DocumentElement")]
    [XmlType("МР771_журнал_системы")]
    public class SystemJournal
    {
        [XmlElement(ElementName = "Номер")]
        public string RecordNumber { get; set; }
        [XmlElement(ElementName = "Время")]
        public string RecordTime { get; set; }
        [XmlElement(ElementName = "Сообщение")]
        public string RecordMessage { get; set; }

        public SystemJournal() { }

        public SystemJournal(string recordNumber, string recordTime, string recordMessage)
        {
            RecordNumber = recordNumber;
            RecordTime = recordTime;
            RecordMessage = recordMessage;
        }
    }
}
