﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BEMN.MR771.Diagnostics
{
    [Serializable]
    [XmlRoot("DocumentElement")]
    [XmlType("МР771_журнал_аварий")]
    public class AJ
    {
        [XmlElement(ElementName = "_indexCol")]
        public string RecordNumber { get; set; }
        [XmlElement(ElementName = "_timeCol")]
        public string Time { get; set; }
        [XmlElement(ElementName = "_msg1Col")]
        public string Message { get; set; }
        [XmlElement(ElementName = "_msgCol")]
        public string TriggeredDefence { get; set; }
        [XmlElement(ElementName = "_codeCol")]
        public string Parametr { get; set; }
        [XmlElement(ElementName = "_typeCol")]
        public string ParametrValue { get; set; }
        [XmlElement(ElementName = "_groupCol")]
        public string Group { get; set; }
        [XmlElement(ElementName = "_rabCol")]
        public string Rab { get; set; }
        [XmlElement(ElementName = "_xabCol")]
        public string Xab { get; set; }
        [XmlElement(ElementName = "_rbcCol")]
        public string Rbc { get; set; }
        [XmlElement(ElementName = "_xbcCol")]
        public string Xbc { get; set; }
        [XmlElement(ElementName = "_rcaCol")]
        public string Rca { get; set; }
        [XmlElement(ElementName = "_xcaCol")]
        public string Xca { get; set; }
        [XmlElement(ElementName = "_stepColumn")]
        public string NumberOfTriggeredParametr { get; set; }
        [XmlElement(ElementName = "_ra1Col")]
        public string Ra1 { get; set; }
        [XmlElement(ElementName = "_xa1Col")]
        public string Xa1 { get; set; }
        [XmlElement(ElementName = "_rb1Col")]
        public string Rb1 { get; set; }
        [XmlElement(ElementName = "_xb1Col")]
        public string Xb1 { get; set; }
        [XmlElement(ElementName = "_rc1Col")]
        public string Rc1 { get; set; }
        [XmlElement(ElementName = "_xc1Col")]
        public string Xc1 { get; set; }
        [XmlElement(ElementName = "_Ida1Col")]
        public string Ia { get; set; }
        [XmlElement(ElementName = "_Ita1Col")]
        public string Ib { get; set; }
        [XmlElement(ElementName = "_Ida2Col")]
        public string Ic { get; set; }
        [XmlElement(ElementName = "_Ita2Col")]
        public string I1 { get; set; }
        [XmlElement(ElementName = "_Ida3Col")]
        public string I2 { get; set; }
        [XmlElement(ElementName = "_Ita3Col")]
        public string I0 { get; set; }
        [XmlElement(ElementName = "_I1Col")]
        public string In { get; set; }
        [XmlElement(ElementName = "_I2Col")]
        public string Ig { get; set; }
        [XmlElement(ElementName = "_I3Col")]
        public string Ua { get; set; }
        [XmlElement(ElementName = "_I4Col")]
        public string Ub { get; set; }
        [XmlElement(ElementName = "_I5Col")]
        public string Uc { get; set; }
        [XmlElement(ElementName = "_I6Col")]
        public string Uab { get; set; }
        [XmlElement(ElementName = "_I7Col")]
        public string Ubc { get; set; }
        [XmlElement(ElementName = "_I8Col")]
        public string Uca { get; set; }
        [XmlElement(ElementName = "_I9Col")]
        public string U1 { get; set; }
        [XmlElement(ElementName = "_I10Col")]
        public string U2 { get; set; }
        [XmlElement(ElementName = "_I11Col")]
        public string U0 { get; set; }
        [XmlElement(ElementName = "_I12Col")]
        public string Un { get; set; }
        [XmlElement(ElementName = "_I13Col")]
        public string Un1 { get; set; }
        [XmlElement(ElementName = "_I14Col")]
        public string F { get; set; }
        [XmlElement(ElementName = "_I15Col")]
        public string Q { get; set; }
        [XmlElement(ElementName = "_D0Col")]
        public string D1To8 { get; set; }
        [XmlElement(ElementName = "_D1Col")]
        public string D9To16 { get; set; }
        [XmlElement(ElementName = "_D2Col")]
        public string D17To24 { get; set; }
        [XmlElement(ElementName = "_D3Col")]
        public string D25To32 { get; set; }
        [XmlElement(ElementName = "_D4Col")]
        public string D33To40 { get; set; }

        public AJ() { }

        public AJ(string recordNumber, string time, string message, string triggeredDefence, string parametr,
            string parametrValue, string group, string rab, string xab, string rbc, string xbc, string rca,
            string xca, string numberOfTriggeredParametr, string ra1, string xa1, string rb1, string xb1, string rc1, 
            string xc1, string ia,string ib, string ic, string i1, string i2, string i0, string iN, string ig, string ua, 
            string ub, string uc, string uab, string ubc, string uca, string u1, string u2, string u0, string un, string un1,
            string f, string q, string d1to8, string d9to16, string d17to24, string d25to32, string d33to40)
        {
            RecordNumber = recordNumber;
            Time = time;
            Message = message;
            TriggeredDefence = triggeredDefence;
            Parametr = parametr;
            ParametrValue = parametrValue;
            Group = group;
            Rab = rab;
            Xab = xab;
            Rbc = rbc;
            Xbc = xbc;
            Rca = rca;
            Xca = xca;
            NumberOfTriggeredParametr = numberOfTriggeredParametr;
            Ra1 = ra1;
            Xa1 = xa1;
            Rb1 = rb1;
            Xb1 = xb1;
            Rc1 = rc1;
            Xc1 = xc1;
            Ia = ia;
            Ib = ib;
            Ic = ic;
            I1 = i1;
            I2 = i2;
            I0 = i0;
            In = iN;
            Ig = ig;
            Ua = ua;
            Ub = ub;
            Uc = uc;
            Uab = uab;
            Ubc = ubc;
            Uca = uca;
            U1 = u1;
            U2 = u2;
            U0 = u0;
            Un = un;
            Un1 = un1;
            F = f;
            Q = q;
            D1To8 = d1to8;
            D9To16 = d9to16;
            D17To24 = d17to24;
            D25To32 = d25to32;
            D33To40 = d33to40;
        }

    }
}
