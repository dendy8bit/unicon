﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Compressor;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR771.AlarmJournal;
using BEMN.MR771.AlarmJournal.Structures;
using BEMN.MR771.Configuration;
using BEMN.MR771.Configuration.Structures;
using BEMN.MR771.Configuration.Structures.MeasuringTransformer;
using BEMN.MR771.Configuration.Structures.Oscope;
using BEMN.MR771.Osc.HelpClasses;
using BEMN.MR771.Osc.Loaders;
using BEMN.MR771.Osc.Structures;
using BEMN.MR771.SystemJournal;
using BEMN.MR771.SystemJournal.Structures;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR771.Diagnostics
{
    public partial class AllInformationsDeviceForm : Form, IFormView
    {
        #region Main constant and verables

        private Mr771 _device;
        private MessageBoxForm _messageBoxForm;
        private FileDriver _fileDriver;

        private const string DEVICE_NAME = "MR771";
        private const string XML_HEAD = "MR771_SET_POINTS";
        private const string LIST_FILE_NAME = "jlist.xml";
        private const string ARH_NANE = "logarch.zip";

        //счетчик показа сообщений если равен 0 то показывае resultMessage
        private int _count;
        //счетчик читаемых осциллограмм
        private int _countOscForRead;

        #endregion
        
        #region SJ constant and verables

        private readonly MemoryEntity<OneWordStruct> _saveIndex;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private int _recordNumber;
        private JournalLoader _systemJournalLoader;
        private List<SystemJournal> _systemJournalData;
        private IEnumerable<CheckBox> _checkBoxs;

        #endregion

        #region AJ constant and verables

        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderAJ;
        private List<AJ> _alarmJournalData;

        #endregion

        #region Configuration constant and verables

        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        private readonly MemoryEntity<ConfigurationStruct105> _configuration105;
        private readonly MemoryEntity<ConfigurationStruct108> _configuration108;
        private readonly MemoryEntity<ConfigurationStruct111> _configuration111;

        private ConfigurationStruct _currentSetpointsStruct;
        private ConfigurationStruct105 _currentSetpointsStruct105;
        private ConfigurationStruct108 _currentSetpointsStruct107;
        private ConfigurationStruct111 _currentSetpointsStruct111;

        #endregion

        #region Osc constant and verables

        /// <summary>
        /// Загрузчик страниц
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// Загрузчик журнала
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderOsc;

        private readonly MemoryEntity<OscOptionsStruct> _oscilloscopeSettings;
        /// <summary>
        /// Данные осц
        /// </summary>
        private CountingList _countingList;
        private OscJournalStruct _journalStruct;

        private MemoryEntity<OscopeAllChannelsStruct> _oscopeStruct;

        #endregion

        #region Constructor
        public AllInformationsDeviceForm()
        {
            InitializeComponent();
        }

        public AllInformationsDeviceForm(Mr771 device)
        {
            InitializeComponent();

            _device = device;
            _messageBoxForm = new MessageBoxForm();
            _checkBoxs = new List<CheckBox>();
            _oscCountMTB.MaxLength = 2;

            #region SJ 

            _systemJournalData = new List<SystemJournal>();
            

            if (this._device.Full1SysJournal024 != null)
            {
                this._systemJournalLoader = new JournalLoader(this._device.Full1SysJournal024, this._device.FullSysJournal64,
                    this._device.SaveSysJournalIndex);
                this._systemJournalLoader.AllJournalReadOk +=
                    HandlerHelper.CreateActionHandler(this, () => { this.AllReadSJRecords(); });
                this._systemJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this,
                    () => { _count--; });
                //subscribe events
                this._systemJournal = null;

                this._systemJournalLoader.MessagesList = PropFormsStrings.GetNewJournalList();
            }
            else
            {
                this._systemJournalLoader = null;
                this._systemJournal = device.SystemJournal;
                this._saveIndex = device.SaveSysJournalIndex;
                this._saveIndex.AllWriteOk +=
                    HandlerHelper.CreateReadArrayHandler(this, () => { this.StartReadSystemJournal(); });
                this._saveIndex.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this,
                    () => { _count--; });
                this._systemJournal.AllReadOk +=
                    HandlerHelper.CreateReadArrayHandler(this, () => { this.ReadRecord(); });

                this._systemJournal.Value.MessagesList = PropFormsStrings.GetNewJournalList();
            }

            #endregion

            #region AJ

            _alarmJournalData = new List<AJ>();
            

            this._currentOptionsLoaderAJ = device.CurrentOptionsLoaderAj;
            this._currentOptionsLoaderAJ.LoadOk += HandlerHelper.CreateActionHandler(this, this.StartReadJournal);
            this._currentOptionsLoaderAJ.LoadFail += HandlerHelper.CreateActionHandler(this, () => { });

            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && device.AllAlarmJournal != null)
            {
                this._journalLoader = new AlarmJournalLoader(device.AllAlarmJournal, device.SetPageAlarmJournal);
                this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.AllReadAJRecords);
                this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, () => { _count--; });

                _journalLoader.MessagesList = PropFormsStrings.GetNewAlarmList();
            }
            else
            {
                this._alarmJournal = device.AlarmJournal;
                this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadAJRecord);
                this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _alarmJournal.RemoveStructQueries();
                    _count--;
                });
                this._setPageAlarmJournal = device.SetPageAlarmJournal;
                this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._alarmJournal.LoadStruct);
                this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _setPageAlarmJournal.RemoveStructQueries();
                    _count--;
                });

                _alarmJournal.Value.MessagesList = PropFormsStrings.GetNewAlarmList();
            }

            #endregion

            #region Configuration

            if (StringsConfig.CurrentVersion >= 1.11)
            {
                this._configuration111 = device.Configuration111;
                this._configuration111.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration111.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration111.RemoveStructQueries();
                    _count--;
                });
                this._currentSetpointsStruct111 = new ConfigurationStruct111();
            }
            else if (StringsConfig.CurrentVersion >= 1.08 && StringsConfig.CurrentVersion < 1.11)
            {
                this._configuration108 = device.Configuration108;
                this._configuration108.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration108.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    _configuration108.RemoveStructQueries();
                    _count--;
                });
                this._currentSetpointsStruct107 = new ConfigurationStruct108();
            }
            else if (StringsConfig.CurrentVersion >= 1.05 && StringsConfig.CurrentVersion < 1.08)
            {
                this._configuration105 = device.Configuration105;
                this._configuration105.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration105.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    _configuration105.RemoveStructQueries();
                    _count--;
                });
                this._currentSetpointsStruct105 = new ConfigurationStruct105();
            }
            else
            {
                this._configuration = device.Configuration;
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    _configuration.RemoveStructQueries();
                    _count--;
                });
                this._currentSetpointsStruct = new ConfigurationStruct();
            }

            #endregion

            #region Osc

            // Загрузчик страниц
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, () => {_messageBoxForm.ShowMessage("Сохранение файлов осциллограмм");});
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            // Загрузчик журнала
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && device.AllOscJournal != null)
            {
                this._oscJournalLoader = new OscJournalLoader(device.AllOscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
            }
            else
            {
                this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            }
            // Уставки осцилографа
            this._oscilloscopeSettings = device.OscOptions;
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            // Каналы осциллографа
            this._oscopeStruct = this._device.AllChannels;
            this._oscopeStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscilloscopeSettings.LoadStruct();
            });
            // Загрузчик уставок токов
            this._currentOptionsLoaderOsc = device.CurrentOptionsLoader;
            this._currentOptionsLoaderOsc.LoadOk += this._oscopeStruct.LoadStruct; // успешно прочитанная структура измерений вызовет метод чтения структуры

            #endregion

            this._fileDriver = new FileDriver(this._device, this);
        }
        
        #endregion
        
        #region AJ Func

        private void StartReadJournal()
        {
            _messageBoxForm.ShowMessage("Сохранение файла ЖА");
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && this._device.AllAlarmJournal != null)
            {
                this._journalLoader.Clear();
                this._journalLoader.StartRead();
            }
            else
            {
                this.SavePageNumber();
            }
        }

        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private void StartReadOption()
        {
            this._currentOptionsLoaderAJ.StartRead();
        }

        private void ReadAJRecord()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;

                    AlarmJournalRecordStruct record = this._alarmJournal.Value;
                    MeasureTransStruct measure = this._currentOptionsLoaderAJ.Connections[this._alarmJournal.Value.GroupOfSetpoints].Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);

                    AJ alarmJournalItem = new AJ(
                        _recordNumber.ToString(), 
                        record.GetTime, 
                        AjStrings.Message[record.Message],
                        triggeredDef,
                        parameter,
                        parametrValue,
                        AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        this.GetStep(record.NumberOfTriggeredParametr),
                        ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40), // 3I0
                        ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                        ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetQ(record.Q),
                        record.D1To8,
                        record.D9To16,
                        record.D17To24,
                        record.D25To32,
                        record.D33To40
                        );

                    _alarmJournalData.Add(alarmJournalItem);

                    this.SavePageNumber();
                }
                else
                {
                }
            }
            catch (Exception e)
            {
            }
        }

        private void AllReadAJRecords()
        {
            _count--;
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                return;
            }
            this._recordNumber = 1;
            foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoaderAJ.Connections[record.GroupOfSetpoints].Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);


                    AJ alarmJournalItem = new AJ(
                            this._recordNumber.ToString(),
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            this.GetStep(record.NumberOfTriggeredParametr),
                            ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40), // 3I0
                            ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),
                            ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                            ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                            ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                            ValuesConverterCommon.Analog.GetF(record.F),
                            ValuesConverterCommon.Analog.GetQ(record.Q),
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );

                    _alarmJournalData.Add(alarmJournalItem);
                }

                catch (Exception e)
                {
                    
                }
                this._recordNumber++;
            }

            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<AJ>));
                using (FileStream file = new FileStream($"{folderBrowserDialog.SelectedPath}\\Журнал Аварий {_device.DeviceType} версия {_device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(file, _alarmJournalData);
                }

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage($"Файл ЖА сохранен");
                }
                
            }
            catch (Exception e)
            {
            }
        }

        private string GetTriggeredDefence(AlarmJournalRecordStruct record)
        {
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07 && _device.AllAlarmJournal != null)
            {
                return record.TriggeredDefense == 61
                    ? _journalLoader.MessagesList[record.ValueOfTriggeredParametr]
                    : AjStrings.TriggeredDefense[record.TriggeredDefense];
            }
            return record.TriggeredDefense == 61
                ? _alarmJournal.Value.MessagesList[record.ValueOfTriggeredParametr]
                : AjStrings.TriggeredDefense[record.TriggeredDefense];
        }

        private string GetParameter(AlarmJournalRecordStruct record)
        {
            if (record.TriggeredDefense == 15 || record.TriggeredDefense >= 44 && record.TriggeredDefense <= 59 || record.TriggeredDefense == 61)
            {
                return string.Empty;
            }
            return AjStrings.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalRecordStruct record, MeasureTransStruct measure)
        {
            int parameter = record.NumberOfTriggeredParametr;
            ushort value = record.ValueOfTriggeredParametr;
            if ((parameter >= 36 && parameter <= 40) || parameter == 42 || parameter == 65)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
            }
            if (parameter == 41 || parameter == 43)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittx * 40);
            }
            if ((parameter >= 0) && (parameter <= 35))
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short)value, (short)value1,
                    measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
            }
            if ((parameter >= 44) && (parameter <= 52))
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
            }

            if (parameter == 53)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
            }

            if (parameter == 54 || parameter == 69)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.Kthx1Value);
            }

            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }
            if (parameter == AjStrings.OmpInd)
            {
                return ValuesConverterCommon.Analog.GetOmp(value);
            }
            if (parameter == 60)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }
            if (parameter == 61 || parameter == 67 || parameter == 68 || (parameter >= 56 && parameter <= 58))
            {
                return string.Empty;
            }
            if (parameter == 64)
            {
                return ValuesConverterCommon.Analog.GetSignF((short)value);
            }
            return value.ToString();
        }

        private string GetStep(int param)
        {
            if ((param >= 0 && param <= 11) || param >= 36) return string.Format("Контур Ф-N{0}", 1);// по умолчанию 1я ступень
            if (param > 11 && param <= 17) return string.Format("Контур Ф-N{0}", 2);
            if (param > 17 && param <= 23) return string.Format("Контур Ф-N{0}", 3);
            if (param > 23 && param <= 29) return string.Format("Контур Ф-N{0}", 4);
            if (param > 29 && param <= 35) return string.Format("Контур Ф-N{0}", 5);
            return string.Format("Контур Ф-N{0}", 1); // по умолчанию 1я ступень
        }

        #endregion

        #region SJ Func

        private void StartReadSystemJournal()
        {
            this.RecordNumber = 0;
            this._systemJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    //_messageBoxForm.ShowMessage("Подписи прочитаны");
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals) serializer.Deserialize(reader);
                            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07)
                            {
                                this._systemJournalLoader.MessagesList = lists.SysJournal.MessagesList;
                            }
                            else
                            {
                                this._systemJournal.Value.MessagesList = lists.SysJournal.MessagesList;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                if (this._device.Full1SysJournal024 != null)
                {
                    this._systemJournalLoader.MessagesList = PropFormsStrings.GetNewJournalList();
                }
                else
                {
                    this._systemJournal.Value.MessagesList = PropFormsStrings.GetNewJournalList();
                }
            }

            if (this._device.Full1SysJournal024 != null)
            {
                _messageBoxForm.ShowMessage("Сохранение файла ЖС");
                this._systemJournalLoader.StartRead();
            }
            else
            {
                //_messageBoxForm.ShowMessage("Чтение ЖС");
                this._saveIndex.Value.Word = 0;
                this._saveIndex.SaveStruct();
            }
        }
        
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set { this._recordNumber = value; }
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                SystemJournal sj = new SystemJournal(this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage
                );
                _systemJournalData.Add(sj);
            }
            else
            {
                _count--;
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(List<SystemJournal>));
                    using (FileStream file = new FileStream($"{folderBrowserDialog.SelectedPath}\\Журнал Системы {_device.DeviceType} версия {_device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                    {
                        formatter.Serialize(file, _systemJournalData);
                    }

                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Файл ЖС сохранен");
                    }
                }
                catch (Exception e)
                {
                }
                //_messageBoxForm.ShowResultMessage("ЖC прочитан");
            }
        }

        private void AllReadSJRecords()
        {
            _count--;

            if (this._systemJournalLoader.JournalRecords.Count != 0)
            {
                for (int i = 0; i < this._systemJournalLoader.JournalRecords.Count; i++)
                {
                    SystemJournal sj = new SystemJournal((i + 1).ToString(),
                        this._systemJournalLoader.JournalRecords[i].GetRecordTime,
                        this._systemJournalLoader.JournalRecords[i].GetRecordMessage
                    );
                    _systemJournalData.Add(sj);
                }

                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(List<SystemJournal>));
                    using (FileStream file = new FileStream($"{folderBrowserDialog.SelectedPath}\\Журнал Системы {_device.DeviceType} версия {_device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                    {
                        formatter.Serialize(file, _systemJournalData);
                    }

                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Файл ЖС сохранен");
                    }
                }
                catch (Exception e)
                {
                }
            }
            else
            {
               //_messageBoxForm.ShowResultMessage("ЖC прочитан");
            }
            _fileDriver.CloseAll(OnClosedFiles);
        }

        #endregion

        #region Configuration Func

        private void ConfigurationReadOk()
        {
            _count--;
            if (StringsConfig.CurrentVersion >= 1.11)
            {
                this._currentSetpointsStruct111 = this._configuration111.Value;
            }
            else if (StringsConfig.CurrentVersion >= 1.08 && StringsConfig.CurrentVersion < 1.11)
            {
                this._currentSetpointsStruct107 = this._configuration108.Value;
            }
            else if (StringsConfig.CurrentVersion >= 1.05 && StringsConfig.CurrentVersion < 1.08)
            {
                this._currentSetpointsStruct105 = this._configuration105.Value;
            }
            else
            {
                this._currentSetpointsStruct = this._configuration.Value;
            }

            Serialize($"{folderBrowserDialog.SelectedPath}\\Конфигурация {_device.DeviceType} версия {_device.DeviceVersion}.xml");
        }

        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("Mr771"));
                ushort[] values;
                if (StringsConfig.CurrentVersion >= 1.11)
                {
                    values = this._currentSetpointsStruct111.GetValues();
                }
                else if (StringsConfig.CurrentVersion >= 1.08 && StringsConfig.CurrentVersion < 1.11)
                {
                    values = this._currentSetpointsStruct107.GetValues();
                }
                else if (StringsConfig.CurrentVersion >= 1.05 && StringsConfig.CurrentVersion < 1.08)
                {
                    values = this._currentSetpointsStruct105.GetValues();
                }
                else
                {
                    values = this._currentSetpointsStruct.GetValues();
                }

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Файл конфигурации сохранен");
                }
            }
            catch
            {
                
            }
        }

        private void StartRead()
        {
            _messageBoxForm.ShowMessage("Сохранение файла конфигурации");

            if (StringsConfig.CurrentVersion >= 1.11)
            {
                this._configuration111.LoadStruct();
            }
            else if (StringsConfig.CurrentVersion >= 1.08 && StringsConfig.CurrentVersion < 1.11)
            {
                this._configuration108.LoadStruct();
            }
            else if (StringsConfig.CurrentVersion >= 1.05 && StringsConfig.CurrentVersion < 1.08)
            {
                this._configuration105.LoadStruct();
            }
            else
            {
                this._configuration.LoadStruct();
            }
        }

        #endregion

        #region Programming Func

        private void LogicReadOfDevice(byte[] readBytes, string mess)
        {
            _count--;

            if (readBytes != null && readBytes.Length != 0 && mess == "Операция успешно выполнена")
            {
                this.LoadProjectFromBin(this.Uncompress(readBytes));
            }
            _fileDriver.CloseAll(OnClosedFiles);
        }
        private byte[] Uncompress(byte[] readBytes)
        {
            ushort[] readData = Common.TOWORDS(readBytes, false);
            ZIPCompressor compr = new ZIPCompressor();
            byte[] compressed = new byte[readData[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(readData[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)readData[i];
            }
            return compr.Decompress(compressed);
        }

        private void LoadProjectFromBin(byte[] uncompressed)
        {
            //Создаем файл логики, чтобы с ним работать
            try
            {
                using (FileStream fs = new FileStream($"{folderBrowserDialog.SelectedPath}\\logic.xml",
                    FileMode.Create, FileAccess.Write,
                    FileShare.None, uncompressed.Length,
                    false))
                {
                    fs.Write(uncompressed, 0, uncompressed.Length);
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load($"{folderBrowserDialog.SelectedPath}\\logic.xml");

                XmlNodeList aNodes = doc.SelectNodes("//Source/SchematicData");
                
                if (aNodes != null)
                {
                    foreach (XmlNode aNode in aNodes)
                    {
                        XmlAttribute device = doc.CreateAttribute("device");
                        device.Value = DEVICE_NAME;
                        aNode.Attributes?.Append(device);
                    }
                }

                doc.Save($"{folderBrowserDialog.SelectedPath}\\Проект логики {_device.DeviceType} версия {_device.DeviceVersion}.prj");

                //Удаляем файл т.к. он больше не нужен
                File.Delete($"{folderBrowserDialog.SelectedPath}\\logic.xml");

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Файл логики сохранен");
                }
            }
            catch (Exception e)
            {
            }
        }

        #endregion

        #region Osc Func

        /// <summary>
        /// Прочитан весь журнал
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.OscCount == 0)
            {
            }
        }

        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            _count--;
            _countOscForRead++;

            int group = this._journalStruct.GroupIndex;
            try
            {
                this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct,
                    this._currentOptionsLoaderOsc.Connections[group].Value, this._oscopeStruct.Value.Rows);
                Directory.CreateDirectory($"{folderBrowserDialog.SelectedPath}\\Осцилограммы");
                string date = _oscJournalLoader.OscRecords[_countOscForRead - 1].GetDate;
                string time = _oscJournalLoader.OscRecords[_countOscForRead - 1].GetTime.Replace(":", ".");
                Directory.CreateDirectory($"{folderBrowserDialog.SelectedPath}\\Осцилограммы\\Оциллограмма {date} {time}");
                _countingList.Save($"{folderBrowserDialog.SelectedPath}\\Осцилограммы\\Оциллограмма {date} {time}\\Осциллограмма_МР771", Common.VersionConverter(_device.DeviceVersion));

                if (_countOscForRead < Convert.ToInt32(_oscCountMTB.Text))
                {
                    this._journalStruct = this._oscJournalLoader.OscRecords[_countOscForRead];
                    this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);
                }

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage($"Файлы осциллограммы {_countOscForRead} сохранены");
                }
            }
            catch(Exception ex)
            {
                _messageBoxForm.ShowResultMessage("Ошибка сохранения осциллограммы");
            }
        }

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
            }
        }

        /// <summary>
        /// Прочитан весь журнал
        /// </summary>
        private void ReadAllRecords()
        {
            _oscCB.Enabled = _oscJournalLoader.OscCount != 0;
            _oscCountMTB.Enabled = _oscJournalLoader.OscCount != 0;
            _oscCountMTB.Text = _oscJournalLoader.OscCount != 0 ? "1" : "";

            _oscInDeviceLabel.Text = $"В устройстве {_oscJournalLoader.OscCount} осц.";
        }

        private void StartReadOsc()
        {
            this._journalStruct = this._oscJournalLoader.OscRecords[_countOscForRead];
            this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);
        }

        #endregion

        #region Main Func
        private void OnClosedFiles(bool res, string message)
        {
            if (!res) _messageBoxForm.ShowMessage(@"Не удалось закрыть открытые файлы в устройстве. " + message);
        }

        public void SaveInfo(IEnumerable<System.Windows.Forms.CheckBox> checkBoxs)
        {
            _alarmJournalData.Clear();
            _systemJournalData.Clear();
            _count = 0;
            _countOscForRead = 0;

            foreach (var item in checkBoxs)
            {
                if (item.Checked && item.Text == "Журнал Системы")
                {
                    _count++;
                    StartReadSJ();
                }

                if (item.Checked && item.Text == "Журнал Аварий")
                {
                    _count++;
                    this.StartReadOption();
                }

                if (item.Checked && item.Text == "Конфигурация")
                {
                    _count++;
                    this.StartRead();
                }

                if (item.Checked && item.Text == "Программирование (логика)")
                {
                    _count++;
                    this._fileDriver.ReadFile(this.LogicReadOfDevice, ARH_NANE);
                }

                if (item.Checked && item.Text == "Осциллограммы")
                {
                    _count += Convert.ToInt32(_oscCountMTB.Text);
                    StartReadOsc();
                }
            }

            this._messageBoxForm.SetProgressBarStyle(ProgressBarStyle.Marquee);
            this._messageBoxForm.OkBtnVisibility = false;
            this._messageBoxForm.ShowDialog("Идет сохранение данных");
        }

        private void StartReadSJ()
        {
            if (this._device.Full1SysJournal024 != null)
            {
                //_messageBoxForm.ShowMessage("Сохранение файла ЖС");
                this._systemJournalLoader.StartRead();
            }
            else
            {
                //_messageBoxForm.ShowMessage("Чтение ЖС");
                this._saveIndex.Value.Word = 0;
                this._saveIndex.SaveStruct();
            }
        }

        private void CheckCB()
        {
            _checkBoxs = Controls.OfType<System.Windows.Forms.CheckBox>();

            _saveDataButton.Enabled = _checkBoxs.Any(ch => ch.Checked);
        }

        private void saveData_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                SaveInfo(_checkBoxs);
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckCB();
        }

        private void AllInformationsDeviceForm_Load(object sender, EventArgs e)
        {
            this._currentOptionsLoaderOsc.StartRead(); // читает первую запись
            CheckCB();
        }

        private void _oscCountMTB_TextChanged(object sender, EventArgs e)
        {
            _lastOscLabel.Visible = _oscCountMTB.Text == "1";

            try
            {
                if (_oscCountMTB.Text.Length > 2 || _oscCountMTB.Text.Any(char.IsLetter))
                {
                    _oscCountMTB.Text = _oscCountMTB.Text.Remove(_oscCountMTB.Text.Length - 1);
                    _oscCountMTB.SelectionStart = _oscCountMTB.Text.Length;
                }

                if (_oscCountMTB.Text == "" || _oscCountMTB.Text == "0")
                {
                    _oscCountMTB.Text = "1";
                }
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

        #region IFormView

        public Type FormDevice => typeof(Mr771);

        public bool Multishow { get; private set; }

        public Type ClassType => typeof(AllInformationsDeviceForm);

        public bool ForceShow => false;

        public Image NodeImage => Framework.Properties.Resources.information_icon.ToBitmap();

        public string NodeName => "Данные из устройства";

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        #endregion
    }
}
