﻿namespace BEMN.MR771.Diagnostics
{
    partial class AllInformationsDeviceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._sjCB = new System.Windows.Forms.CheckBox();
            this._jaCB = new System.Windows.Forms.CheckBox();
            this._confCB = new System.Windows.Forms.CheckBox();
            this._logCB = new System.Windows.Forms.CheckBox();
            this._oscCB = new System.Windows.Forms.CheckBox();
            this._saveDataButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this._oscInDeviceLabel = new System.Windows.Forms.Label();
            this._oscCountMTB = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._lastOscLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _sjCB
            // 
            this._sjCB.AutoSize = true;
            this._sjCB.Location = new System.Drawing.Point(12, 12);
            this._sjCB.Name = "_sjCB";
            this._sjCB.Size = new System.Drawing.Size(115, 17);
            this._sjCB.TabIndex = 0;
            this._sjCB.Text = "Журнал Системы";
            this._sjCB.UseVisualStyleBackColor = true;
            this._sjCB.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // _jaCB
            // 
            this._jaCB.AutoSize = true;
            this._jaCB.Location = new System.Drawing.Point(12, 35);
            this._jaCB.Name = "_jaCB";
            this._jaCB.Size = new System.Drawing.Size(106, 17);
            this._jaCB.TabIndex = 1;
            this._jaCB.Text = "Журнал Аварий";
            this._jaCB.UseVisualStyleBackColor = true;
            this._jaCB.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // _confCB
            // 
            this._confCB.AutoSize = true;
            this._confCB.Location = new System.Drawing.Point(12, 58);
            this._confCB.Name = "_confCB";
            this._confCB.Size = new System.Drawing.Size(99, 17);
            this._confCB.TabIndex = 2;
            this._confCB.Text = "Конфигурация";
            this._confCB.UseVisualStyleBackColor = true;
            this._confCB.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // _logCB
            // 
            this._logCB.AutoSize = true;
            this._logCB.Location = new System.Drawing.Point(12, 81);
            this._logCB.Name = "_logCB";
            this._logCB.Size = new System.Drawing.Size(171, 17);
            this._logCB.TabIndex = 3;
            this._logCB.Text = "Программирование (логика)";
            this._logCB.UseVisualStyleBackColor = true;
            this._logCB.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // _oscCB
            // 
            this._oscCB.AutoSize = true;
            this._oscCB.Location = new System.Drawing.Point(12, 104);
            this._oscCB.Name = "_oscCB";
            this._oscCB.Size = new System.Drawing.Size(111, 17);
            this._oscCB.TabIndex = 4;
            this._oscCB.Text = "Осциллограммы";
            this._oscCB.UseVisualStyleBackColor = true;
            this._oscCB.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // _saveDataButton
            // 
            this._saveDataButton.Location = new System.Drawing.Point(207, 54);
            this._saveDataButton.Name = "_saveDataButton";
            this._saveDataButton.Size = new System.Drawing.Size(150, 21);
            this._saveDataButton.TabIndex = 5;
            this._saveDataButton.Text = "Сохранить информацию";
            this._saveDataButton.UseVisualStyleBackColor = true;
            this._saveDataButton.Click += new System.EventHandler(this.saveData_Click);
            // 
            // _oscInDeviceLabel
            // 
            this._oscInDeviceLabel.AutoSize = true;
            this._oscInDeviceLabel.Location = new System.Drawing.Point(129, 105);
            this._oscInDeviceLabel.Name = "_oscInDeviceLabel";
            this._oscInDeviceLabel.Size = new System.Drawing.Size(0, 13);
            this._oscInDeviceLabel.TabIndex = 6;
            // 
            // _oscCountMTB
            // 
            this._oscCountMTB.Enabled = false;
            this._oscCountMTB.Location = new System.Drawing.Point(207, 130);
            this._oscCountMTB.Name = "_oscCountMTB";
            this._oscCountMTB.Size = new System.Drawing.Size(32, 20);
            this._oscCountMTB.TabIndex = 7;
            this._oscCountMTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._oscCountMTB.TextChanged += new System.EventHandler(this._oscCountMTB_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 26);
            this.label2.TabIndex = 8;
            this.label2.Text = "Кол-во осц. \r\nдля сохр.\r\n";
            // 
            // _lastOscLabel
            // 
            this._lastOscLabel.AutoSize = true;
            this._lastOscLabel.Location = new System.Drawing.Point(245, 133);
            this._lastOscLabel.Name = "_lastOscLabel";
            this._lastOscLabel.Size = new System.Drawing.Size(40, 13);
            this._lastOscLabel.TabIndex = 9;
            this._lastOscLabel.Text = "(посл.)";
            this._lastOscLabel.Visible = false;
            // 
            // AllInformationsDeviceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 152);
            this.Controls.Add(this._lastOscLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._oscCountMTB);
            this.Controls.Add(this._oscInDeviceLabel);
            this.Controls.Add(this._saveDataButton);
            this.Controls.Add(this._oscCB);
            this.Controls.Add(this._logCB);
            this.Controls.Add(this._confCB);
            this.Controls.Add(this._jaCB);
            this.Controls.Add(this._sjCB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AllInformationsDeviceForm";
            this.Text = "Вся информация с устройства";
            this.Load += new System.EventHandler(this.AllInformationsDeviceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _sjCB;
        private System.Windows.Forms.CheckBox _jaCB;
        private System.Windows.Forms.CheckBox _confCB;
        private System.Windows.Forms.CheckBox _logCB;
        private System.Windows.Forms.CheckBox _oscCB;
        private System.Windows.Forms.Button _saveDataButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label _oscInDeviceLabel;
        private System.Windows.Forms.MaskedTextBox _oscCountMTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label _lastOscLabel;
    }
}