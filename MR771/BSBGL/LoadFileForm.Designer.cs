﻿namespace BEMN.MR771.BSBGL
{
    partial class LoadFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._openBtn = new System.Windows.Forms.Button();
            this._cancelBtn = new System.Windows.Forms.Button();
            this._filesListView = new System.Windows.Forms.ListView();
            this._fileNameCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._typeCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._sizeCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._dateCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._timeCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._delBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _openBtn
            // 
            this._openBtn.Location = new System.Drawing.Point(388, 28);
            this._openBtn.Name = "_openBtn";
            this._openBtn.Size = new System.Drawing.Size(75, 23);
            this._openBtn.TabIndex = 0;
            this._openBtn.Text = "Открыть";
            this._openBtn.UseVisualStyleBackColor = true;
            this._openBtn.Click += new System.EventHandler(this._openBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelBtn.Location = new System.Drawing.Point(388, 186);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 0;
            this._cancelBtn.Text = "Отмена";
            this._cancelBtn.UseVisualStyleBackColor = true;
            this._cancelBtn.Click += new System.EventHandler(this._cancelBtn_Click);
            // 
            // _filesListView
            // 
            this._filesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._fileNameCol,
            this._typeCol,
            this._sizeCol,
            this._dateCol,
            this._timeCol});
            this._filesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._filesListView.FullRowSelect = true;
            this._filesListView.Location = new System.Drawing.Point(3, 16);
            this._filesListView.Name = "_filesListView";
            this._filesListView.Size = new System.Drawing.Size(364, 181);
            this._filesListView.TabIndex = 1;
            this._filesListView.UseCompatibleStateImageBehavior = false;
            this._filesListView.View = System.Windows.Forms.View.Details;
            this._filesListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this._filesListView_MouseDoubleClick);
            // 
            // _fileNameCol
            // 
            this._fileNameCol.Text = "Название архива";
            this._fileNameCol.Width = 120;
            // 
            // _typeCol
            // 
            this._typeCol.Text = "Тип файла";
            this._typeCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._typeCol.Width = 70;
            // 
            // _sizeCol
            // 
            this._sizeCol.Text = "Размер";
            this._sizeCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _dateCol
            // 
            this._dateCol.Text = "Дата";
            this._dateCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._dateCol.Width = 55;
            // 
            // _timeCol
            // 
            this._timeCol.Text = "Время";
            this._timeCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._timeCol.Width = 55;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._filesListView);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(370, 200);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Список архивов на диске";
            // 
            // _delBtn
            // 
            this._delBtn.Location = new System.Drawing.Point(388, 57);
            this._delBtn.Name = "_delBtn";
            this._delBtn.Size = new System.Drawing.Size(75, 23);
            this._delBtn.TabIndex = 0;
            this._delBtn.Text = "Удалить";
            this._delBtn.UseVisualStyleBackColor = true;
            this._delBtn.Click += new System.EventHandler(this._delBtn_Click);
            // 
            // LoadFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelBtn;
            this.ClientSize = new System.Drawing.Size(472, 224);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._cancelBtn);
            this.Controls.Add(this._delBtn);
            this.Controls.Add(this._openBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadFileForm";
            this.Text = "Открыть архив";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _openBtn;
        private System.Windows.Forms.Button _cancelBtn;
        private System.Windows.Forms.ListView _filesListView;
        private System.Windows.Forms.ColumnHeader _fileNameCol;
        private System.Windows.Forms.ColumnHeader _typeCol;
        private System.Windows.Forms.ColumnHeader _dateCol;
        private System.Windows.Forms.ColumnHeader _timeCol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _delBtn;
        private System.Windows.Forms.ColumnHeader _sizeCol;
    }
}