﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.MBServer;

namespace BEMN.MR771.BSBGL
{
    public partial class LoadFileForm : Form
    {
        private Dictionary<ushort,string> _typeDictionary;
        private string _selectedFile;
        private string _selectedFileSize;
        
        public string SelectedFile
        {
            get { return _selectedFile; }
        }

        public int SelectedFileSize
        {
            get { return int.Parse(_selectedFileSize); }
        }

        
        #region Constructor
        public LoadFileForm()
        {
            InitializeComponent();
        }

        public LoadFileForm(List<string[]> fileInfoList, string curDirectory)
        {
            InitializeComponent();
            groupBox1.Text += string.Format(" "+curDirectory+"\\");
            InitTypeList();
            foreach (var info in fileInfoList)
            {
                CreateItem(info);
            }
        }

        private void InitTypeList()
        {
            _typeDictionary = new Dictionary<ushort, string>
            {
                {0x01, "Read only"},
                {0x02, "Hidden"},
                {0x04, "System"},
                {0x08, "Volume label"},
                {0x0F, "LNF Entry"},
                {0x10, "Directory"},
                {0x20, "Archive"},
                {0x3F, "Mask of defined bits"}
            };
        }

        private void CreateItem(string[] info)
        {
            ListViewItem item = new ListViewItem(info[0]);
            item.SubItems.Add(GetType(info[1]));
            item.SubItems.Add(info[2]);
            item.SubItems.Add(GetDate(info[3]));
            item.SubItems.Add(GetTime(info[4]));
            _filesListView.Items.Add(item);
        }

        private void _cancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private string GetType(string strType)
        {
            ushort ind;
            if (ushort.TryParse(strType, out ind))
            {
                return _typeDictionary.ContainsKey(ind) ? _typeDictionary[ind] : strType;
            }
            else
            {
                return strType;
            }
        }

        private string GetDate(string strDate)
        {
            ushort value;
            if (ushort.TryParse(strDate, out value))
            {
                int day = Common.GetBits(value, 0, 1, 2, 3, 4);
                int month = Common.GetBits(value, 5, 6, 7, 8) >> 5;
                int year = Common.GetBits(value, 9, 10, 11, 12, 13, 14, 15) >> 9 - 20;
                return string.Format("{0:D2}.{1:D2}.{2:D2}", day, month, year);
            }
            else
            {
                return strDate;
            }
        }

        private string GetTime(string strTime)
        {
            ushort value;
            if (ushort.TryParse(strTime, out value))
            {
                int sec = Common.GetBits(value, 0, 1, 2, 3, 4)*2;
                int min = Common.GetBits(value, 5, 6, 7, 8, 9, 10) >> 5;
                int hour = Common.GetBits(value, 11, 12, 13, 14, 15) >> 11;
                return string.Format("{0:D2}:{1:D2}:{2:D2}", hour, min, sec);
            }
            else
            {
                return strTime;
            }
        }
        #endregion

        private bool FileExist()
        {
            var items = _filesListView.SelectedItems;
            if (items.Count == 0) return false;
            _selectedFile = items[0].Text;
            _selectedFileSize = items[0].SubItems[2].Text;
            return true;
        }

        private void _filesListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView lv = sender as ListView;
            if (lv == null) return;
            var items = lv.SelectedItems;
            if (items.Count == 0) return;
            _selectedFile = items[0].Text;
            _selectedFileSize = items[0].SubItems[2].Text;
            DialogResult = DialogResult.OK;
        }

        private void _openBtn_Click(object sender, EventArgs e)
        {
            if (!FileExist()) return;
            DialogResult = DialogResult.OK;
        }

        private void _delBtn_Click(object sender, EventArgs e)
        {
            if (!FileExist()) return;
            DialogResult = DialogResult.OK;
        }
    }
}
