﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR771.Configuration;

namespace BEMN.MR771.Emulation.Structures
{
    public class WriteStructEmul : StructBase
    {
        #region [Private Fiels]

        public const int ANALOG_COUNT = 5;


        [Layout(0)] private ushort _statusTime; // 
        [Layout(1)] private ushort _signal; // сигнал в БД
        [Layout(2, Count = ANALOG_COUNT)] private InputAnalogDataI[] _inputAnalogI; // входные аналогивые данные по току

        [Layout(3, Count = ANALOG_COUNT)] private InputAnalogDataU[] _inputAnalogU;
            // входные аналогивые данные по напряжению

        [Layout(4)] private DiscretsSignals _diskretInput; // дискретные данные (64 сигнала)

        #endregion

        /// <summary>
        /// необходимо для обнуления 
        /// </summary>
        [XmlElement(ElementName = "Для сброса уставок")]
        public bool IsRestart
        {
            get { return true; }
            set { this._statusTime = Common.SetBit(this._statusTime, 15, value); }
        }


        /// <summary>
        /// необходимо для сброса расчета временм
        /// </summary>
        [XmlIgnore]
        public bool StopTime
        {
            get { return true; }
            set
            {
                if (value)
                {
                    this._statusTime = Common.SetBits(this._statusTime, 0, 0, 1);
                }
               
            }
        }
        /// <summary>
        /// необходимо для недопущения старта рассчета времени повторно,так как после выставления первого бита он автоматически сбрасывается (костыль)
        /// </summary>
        [XmlIgnore]
        public bool WriteTimeOk
        {
            set
            {
                this._statusTime = Common.SetBits(this._statusTime, 2,0 );
            }
        }

   #region [Properties]

        /// <summary>
        /// Cтатус для расчета времени
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Cтатус для расчета времени")]
        public bool StatusTime
        {
            get
            {

                return Common.GetBit(this._statusTime, 0); 
            }
            set
            {
                if (value)
                {
                    this._statusTime = 0;
                    this._statusTime = Common.SetBit(this._statusTime, 0, true);
                }
                else
                {
                    return;
                }
            }
        }


        [BindingProperty(1)]
        [XmlElement(ElementName = "Сигнал остановки отсчета времени")]
        public string Signal
        {
            get { return Validator.Get(this._signal, StringsConfig.RelaySignals); }
            set { this._signal = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Аналоговый сигнал Ia")]
        public InputAnalogDataI Ia
        {
            get { return this._inputAnalogI[0]; }
            set { this._inputAnalogI[0] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Аналоговый сигнал Ib")]
        public InputAnalogDataI Ib
        {
            get { return this._inputAnalogI[1]; }
            set { this._inputAnalogI[1] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Аналоговый сигнал Ic")]
        public InputAnalogDataI Ic
        {
            get { return this._inputAnalogI[2]; }
            set { this._inputAnalogI[2] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Аналоговый сигнал In")]
        public InputAnalogDataI In
        {
            get { return this._inputAnalogI[3]; }
            set { this._inputAnalogI[3] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Аналоговый сигнал In1")]
        public InputAnalogDataI In1
        {
            get { return this._inputAnalogI[4]; }
            set { this._inputAnalogI[4] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Аналоговый сигнал Ua")]
        public InputAnalogDataU Ua
        {
            get { return this._inputAnalogU[0]; }
            set { this._inputAnalogU[0] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Аналоговый сигнал Ub")]
        public InputAnalogDataU Ub
        {
            get { return this._inputAnalogU[1]; }
            set { this._inputAnalogU[1] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Аналоговый сигнал Uc")]
        public InputAnalogDataU Uc
        {
            get { return this._inputAnalogU[2]; }
            set { this._inputAnalogU[2] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "Аналоговый сигнал Un")]
        public InputAnalogDataU Un
        {
            get { return this._inputAnalogU[3]; }
            set { this._inputAnalogU[3] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "Аналоговый сигнал Un1")]
        public InputAnalogDataU Un1
        {
            get { return this._inputAnalogU[4]; }
            set { this._inputAnalogU[4] = value; }
        }

        /// <summary>
        /// Список дискретных синалов
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Список дискретных синалов")]
        public DiscretsSignals DiscretInputs
        {

            get { return this._diskretInput; }

            set { this._diskretInput = value; }
        }

        
        #endregion
    }
}


