﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR771.Configuration.Structures.Emulation
{
    public class InputAnalogData : StructBase
    {
        #region [Private Fields]

        [Layout(0)] private ushort _a; // амплитуда
        [Layout(1)] private ushort _fi; // фаза (начальный сдвиг)
        [Layout(2)] private ushort _f; // частота
        [Layout(3)] private ushort _res; //резерв

        #endregion

        #region [Properties]
        /// <summary>
        /// Амлитуда
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Амлитуда")]
        public double Amplituda
        {
            get { return ValuesConverterCommon.GetAplituda(this._a); }
            set { this._a = ValuesConverterCommon.SetAplituda(value); }
        }

        /// <summary>
        /// Фаза
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фаза")]
        public double Faza
        {
            get { return this._fi; }
            set { if (value < 359) this._fi = (ushort)value; }
        }

        /// <summary>
        /// Частота
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Фаза")]
        public double F
        {
            get { return ValuesConverterCommon.GetUDecimal(this._f); }
            set { this._f = ValuesConverterCommon.SetUDecimal(value); }
        }

        #endregion
    }
}
