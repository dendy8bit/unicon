﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR771.Emulation.Structures
{
    public class DiscretsSignals : StructBase
    {
        #region[Constants]
         public const int LOGIC_COUNT = 4; 
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = LOGIC_COUNT)]private ushort[] _discretsSignals; 
        #endregion [Private fields]
        #region Public Fields
        [BindingProperty(0)]
        [XmlElement(ElementName = "Дискрет 1")]
        public bool D1
        {
            get { return Common.GetBit(this._discretsSignals[0], 0); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 0, value); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Дискрет 2")]
        public bool D2
        {
            get { return Common.GetBit(this._discretsSignals[0], 1); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 1, value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Дискрет 3")]
        public bool D3
        {
            get { return Common.GetBit(this._discretsSignals[0], 2); }
            set { this._discretsSignals[0]=Common.SetBit(this._discretsSignals[0], 2, value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Дискрет 4")]
        public bool D4
        {
            get { return Common.GetBit(this._discretsSignals[0], 3); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 3, value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Дискрет 5")]
        public bool D5
        {
            get { return Common.GetBit(this._discretsSignals[0], 4); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 4, value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Дискрет 6")]
        public bool D6
        {
            get { return Common.GetBit(this._discretsSignals[0], 5); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 5, value); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Дискрет 7")]
        public bool D7
        {
            get { return Common.GetBit(this._discretsSignals[0], 6); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 6, value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Дискрет 8")]
        public bool D8
        {
            get { return Common.GetBit(this._discretsSignals[0], 7); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 7, value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Дискрет 9")]
        public bool D9
        {
            get { return Common.GetBit(this._discretsSignals[0], 8); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 8, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Дискрет 10")]
        public bool D10
        {
            get { return Common.GetBit(this._discretsSignals[0], 9); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 9, value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Дискрет 11")]
        public bool D11
        {
            get { return Common.GetBit(this._discretsSignals[0], 10); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 10, value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Дискрет 12")]
        public bool D12
        {
            get { return Common.GetBit(this._discretsSignals[0], 11); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 11, value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Дискрет 13")]
        public bool D13
        {
            get { return Common.GetBit(this._discretsSignals[0], 12); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 12, value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Дискрет 14")]
        public bool D14
        {
            get { return Common.GetBit(this._discretsSignals[0], 13); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 13, value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Дискрет 15")]
        public bool D15
        {
            get { return Common.GetBit(this._discretsSignals[0], 14); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 14, value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Дискрет 16")]
        public bool D16
        {
            get { return Common.GetBit(this._discretsSignals[0], 15); }
            set { this._discretsSignals[0] = Common.SetBit(this._discretsSignals[0], 15, value); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "Дискрет 17")]
        public bool D17
        {
            get { return Common.GetBit(this._discretsSignals[1], 0); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 0, value); }
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "Дискрет 18")]
        public bool D18
        {
            get { return Common.GetBit(this._discretsSignals[1], 1); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 1, value); }
        }

        [BindingProperty(18)]
        [XmlElement(ElementName = "Дискрет 19")]
        public bool D19
        {
            get { return Common.GetBit(this._discretsSignals[1], 2); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 2, value); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "Дискрет 20")]
        public bool D20
        {
            get { return Common.GetBit(this._discretsSignals[1], 3); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 3, value); }
        }

        [BindingProperty(20)]
        [XmlElement(ElementName = "Дискрет 21")]
        public bool D21
        {
            get { return Common.GetBit(this._discretsSignals[1], 4); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 4, value); }
        }

        [BindingProperty(21)]
        [XmlElement(ElementName = "Дискрет 22")]
        public bool D22
        {
            get { return Common.GetBit(this._discretsSignals[1], 5); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 5, value); }
        }

        [BindingProperty(22)]
        [XmlElement(ElementName = "Дискрет 23")]
        public bool D23
        {
            get { return Common.GetBit(this._discretsSignals[1], 6); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 6, value); }
        }

        [BindingProperty(23)]
        [XmlElement(ElementName = "Дискрет 24")]
        public bool D24
        {
            get { return Common.GetBit(this._discretsSignals[1], 7); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1],7, value); }
        }

        [BindingProperty(24)]
        [XmlElement(ElementName = "Дискрет 25")]
        public bool D25
        {
            get { return Common.GetBit(this._discretsSignals[1], 8); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 8, value); }
        }

        [BindingProperty(25)]
        [XmlElement(ElementName = "Дискрет 26")]
        public bool D26
        {
            get { return Common.GetBit(this._discretsSignals[1], 9); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 9, value); }
        }

        [BindingProperty(26)]
        [XmlElement(ElementName = "Дискрет 27")]
        public bool D27
        {
            get { return Common.GetBit(this._discretsSignals[1], 10); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 10, value); }
        }

        [BindingProperty(27)]
        [XmlElement(ElementName = "Дискрет 28")]
        public bool D28
        {
            get { return Common.GetBit(this._discretsSignals[1], 11); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 11, value); }
        }

        [BindingProperty(28)]
        [XmlElement(ElementName = "Дискрет 29")]
        public bool D29
        {
            get { return Common.GetBit(this._discretsSignals[1], 12); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 12, value); }
        }

        [BindingProperty(29)]
        [XmlElement(ElementName = "Дискрет 30")]
        public bool D30
        {
            get { return Common.GetBit(this._discretsSignals[1], 13); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 13, value); }
        }
        [BindingProperty(30)]
        [XmlElement(ElementName = "Дискрет 31")]
        public bool D31
        {
            get { return Common.GetBit(this._discretsSignals[1], 14); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 14, value); }
        }

        [BindingProperty(31)]
        [XmlElement(ElementName = "Дискрет 32")]
        public bool D32
        {
            get { return Common.GetBit(this._discretsSignals[1], 15); }
            set { this._discretsSignals[1] = Common.SetBit(this._discretsSignals[1], 15, value); }
        }

        [BindingProperty(32)]
        [XmlElement(ElementName = "Дискрет 33")]
        public bool D33
        {
            get { return Common.GetBit(this._discretsSignals[2], 0); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 0, value); }
        }

        [BindingProperty(33)]
        [XmlElement(ElementName = "Дискрет 34")]
        public bool D34
        {
            get { return Common.GetBit(this._discretsSignals[2], 1); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 1, value); }
        }

        [BindingProperty(34)]
        [XmlElement(ElementName = "Дискрет 35")]
        public bool D35
        {
            get { return Common.GetBit(this._discretsSignals[2], 2); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 2, value); }
        }

        [BindingProperty(35)]
        [XmlElement(ElementName = "Дискрет 36")]
        public bool D36
        {
            get { return Common.GetBit(this._discretsSignals[2], 3); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 3, value); }
        }

        [BindingProperty(36)]
        [XmlElement(ElementName = "Дискрет 37")]
        public bool D37
        {
            get { return Common.GetBit(this._discretsSignals[2], 4); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 4, value); }
        }

        [BindingProperty(37)]
        [XmlElement(ElementName = "Дискрет 38")]
        public bool D38
        {
            get { return Common.GetBit(this._discretsSignals[2], 5); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 5, value); }
        }

        [BindingProperty(38)]
        [XmlElement(ElementName = "Дискрет 39")]
        public bool D39
        {
            get { return Common.GetBit(this._discretsSignals[2], 6); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 6, value); }
        }

        [BindingProperty(39)]
        [XmlElement(ElementName = "Дискрет 40")]
        public bool D40
        {
            get { return Common.GetBit(this._discretsSignals[2], 7); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 7, value); }
        }
        [BindingProperty(40)]
        [XmlElement(ElementName = "K1")]
        public bool K1
        {
            get { return Common.GetBit(this._discretsSignals[2], 8); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 8, value); }
        }
        [BindingProperty(41)]
        [XmlElement(ElementName = "K2")]
        public bool K2
        {
            get { return Common.GetBit(this._discretsSignals[2], 9); }
            set { this._discretsSignals[2] = Common.SetBit(this._discretsSignals[2], 9, value); }
        }
        #endregion
    }
}
