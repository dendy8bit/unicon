﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR771.Configuration;
using BEMN.MR771.Configuration.Structures.Emulation;

namespace BEMN.MR771.Emulation.Structers
{
    public class WriteStructEmul : StructBase
    {
        #region [Private Fiels]

        public const int ANALOG_COUNT = 10;

        [Layout(0)] private ushort _statusTime; // 
        [Layout(1)] private ushort _signal; // сигнал в БД
        [Layout(2, Count = ANALOG_COUNT)] private InputAnalogData[] _inputAnalog; // входные аналогивые данные
        [Layout(3)] private DiscretsSignals _diskretInput; // дискретные данные (64 сигнала)

        #endregion

        #region [Properties]
        /// <summary>
        /// Cтатус для расчета времени
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Cтатус для расчета времени")]
        public bool StatusTime
        {
            get { return Common.GetBit(_statusTime, 0); }
            set { _statusTime = Common.SetBit(_statusTime,0,value); }
        }


        [BindingProperty(1)]
        [XmlElement(ElementName = "Сигнал остановки отсчета времени")]
        public string Signal
        {
            get { return Validator.Get(this._signal, StringsConfig.RelaySignals); }
            set { this._signal = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Ia
        {
            get { return _inputAnalog[0]; }
            set { _inputAnalog[0] = value; }
        }
        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Ib
        {
            get { return _inputAnalog[1]; }
            set { _inputAnalog[1] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Ic
        {
            get { return _inputAnalog[2]; }
            set { _inputAnalog[2] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData In
        {
            get { return _inputAnalog[3]; }
            set { _inputAnalog[3] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData In1
        {
            get { return _inputAnalog[4]; }
            set { _inputAnalog[4] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Ua
        {
            get { return _inputAnalog[5]; }
            set { _inputAnalog[5] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Ub
        {
            get { return _inputAnalog[6]; }
            set { _inputAnalog[6] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Uc
        {
            get { return _inputAnalog[7]; }
            set { _inputAnalog[7] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Un
        {
            get { return _inputAnalog[8]; }
            set { _inputAnalog[8] = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData Un1
        {
            get { return _inputAnalog[9]; }
            set { _inputAnalog[9] = value; }
        }
        /// <summary>
        /// Список дискретных синалов
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Список дискретных синалов")]
        public DiscretsSignals DiscretInputs
        {

            get
            {
                return _diskretInput;
            }

            set { _diskretInput = value; }
        }
        }

        #endregion

    }


