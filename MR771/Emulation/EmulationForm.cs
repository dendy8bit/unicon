﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR771.Configuration;
using BEMN.MR771.Emulation.Structures;

namespace BEMN.MR771.Emulation
{
    public partial class EmulationForm : Form, IFormView
    {
        private const string READ_FAIL = "Не удалось прочитать эмуляцию";
        private const string WRITE_FAIL = "Не удалось записать эмуляцию";
        private const string INVALID_PORT = "Порт недоступен.";

        #region [Private fields]

        private readonly Mr771 _device;

        private bool IsStart = false;
        private bool IsSelect = false;
        private bool IsWrite = false;
        private bool IsChange = false;
        private bool IsKvit = false;
        private bool IsExit = false;
        private double _incriment = 0.0;
        private List<Button> _listButtons;
        private Button _currentButtton;
        private MaskedTextBox _currentAdduction;
        private MemoryEntity<WriteStructEmul> _memoryEntityStructEmulation;
        private MemoryEntity<WriteStructEmul> _memoryEntityNullStructEmulation;
        private WriteStructEmul _currentWriteStruct;
        private WriteStructEmul _tempWriteStruct;

        private NewStructValidator<WriteStructEmul> _validatorEmulationStruct;
        private MemoryEntity<ReadStructEmul> _readMemoryEntity;

        private NewStructValidator<DiscretsSignals> _discrets;
        private List<MaskedTextBox> _listFazaControls;
        private List<CheckBox> _listStartControls;
        private List<CheckBox> _listDiscretsControls;
        private List<MaskedTextBox> _listAllControls;




        private NewStructValidator<InputAnalogDataI> _Ia;
        private NewStructValidator<InputAnalogDataI> _Ib;
        private NewStructValidator<InputAnalogDataI> _Ic;
        private NewStructValidator<InputAnalogDataI> _In;
        private NewStructValidator<InputAnalogDataI> _In1;
        private NewStructValidator<InputAnalogDataU> _Ua;
        private NewStructValidator<InputAnalogDataU> _Ub;
        private NewStructValidator<InputAnalogDataU> _Uc;
        private NewStructValidator<InputAnalogDataU> _Un;
        private NewStructValidator<InputAnalogDataU> _Un1;



        #endregion

        public EmulationForm()
        {
            this.InitializeComponent();
        }

        public EmulationForm(Mr771 device)
        {
            this.InitializeComponent();
            this._device = device;

            StringsConfig.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
            StringsConfig.RelaySignals = StringsConfig.DefaultRelaySignals;

            this._memoryEntityStructEmulation = this._device.WriteStructEmulation;
            this._memoryEntityNullStructEmulation = this._device.WriteStructEmulationNull;
            this._readMemoryEntity = this._device.ReadStructEmulation;

            this._memoryEntityStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                this.EmulationReadOk);
            this._memoryEntityStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                this.EmulationWriteOk);
            this._memoryEntityStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._memoryEntityStructEmulation.RemoveStructQueries();
                this.EmulationWriteFail();
            });
            this._memoryEntityStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._memoryEntityStructEmulation.RemoveStructQueries();
                this.EmulationReadFail();
            });



            this._memoryEntityNullStructEmulation.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                this.NullEmulationReadOk);
            this._memoryEntityNullStructEmulation.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                this.EmulationWriteOk);
            this._memoryEntityNullStructEmulation.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._memoryEntityNullStructEmulation.RemoveStructQueries();
                this.EmulationWriteFail();
                this._kvit.Enabled = true;
            });
            this._memoryEntityNullStructEmulation.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._memoryEntityStructEmulation.RemoveStructQueries();
                this.EmulationReadFail();
                this._kvit.Enabled = true;
            });



            this._readMemoryEntity.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
             this.ReadOk);
            this._readMemoryEntity.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._status.Text = "";
                this.EmulationReadFail();
            });
            this._currentWriteStruct = new WriteStructEmul();


            this.Init();
        }



        private void Init()
        {
            this.kvitTooltip.SetToolTip(_kvit, "Применяется для сброса углов при изменении частоты");
            #region [ListCotrols]

            this._listDiscretsControls = new List<CheckBox>()
            {
                this._d1,this._d2,this._d3,this._d4,this._d5,this._d6,this._d7,this._d8,this._d9,this._d10,this._d11, this._d12, this._d13,this._d14, this._d15,this._d16,
                this._d17,this._d18,this._d19,this._d20,this._d21,this._d22,this._d23,this._d24,this._d25,this._d26,this._d27,this._d28,this._d29,
                this._d30,this._d31,this._d32,this._d33,this._d34,this._d35,this._d36,this._d37,this._d38,this._d39,this._d40,_k1,_k2
            };
            foreach (var control in this._listDiscretsControls)
            {
                control.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            }
            this._listFazaControls = new List<MaskedTextBox>()
           {
               this.fazaIa,this.fazaIb,this.fazaIc,this.fazaIn,this.fazan1,this.fazaUa,this.fazaUb,this.fazaUc,this.fazaUn,this.fazaUn1,

           };
            this._listStartControls = new List<CheckBox>()
            {
                this._startIa,this._startFazaA,this._startFa,
                this._startIb,this._startFazaB,this._startFb,
                this._startIc,this._startFazaC,this._startFc,
                this._startIn,this._startFazaN,this._startFn,
                new CheckBox(),new CheckBox(),new CheckBox(),
                this._startUa,this._startFazaUa,this._startFUa,
                this._startUb,this._startFazaUb,this._startFUb,
                this._startUc,this._startFazaUc,this._startFUc,
                this._startUn,this._startFazaUn,this._startFUn,
                this._startUn1,this._startFazaUn1,this._startFUn1,
                this.checkBox40,this.checkBox39,this.checkBox38,this.checkBox37,this.checkBox36,this.checkBox35,
                this.checkBox34,this.checkBox33,this.checkBox32,this.checkBox31,this.checkBox30,
                this.checkBox29,this.checkBox28,this.checkBox27,this.checkBox26,this.checkBox25,
                this.checkBox24,this.checkBox23,this.checkBox22,this.checkBox21,this.checkBox20,
                this.checkBox19,this.checkBox18,this.checkBox17,this.checkBox16,this.checkBox15,
                this.checkBox14,this.checkBox13,this.checkBox12,this.checkBox11,this.checkBox10,
                this.checkBox9,this.checkBox8,this.checkBox7,this.checkBox6,this.checkBox5,this.checkBox4,
                this.checkBox3,this.checkBox2,this.checkBox1,_startK1,_startK2
            };
            foreach (var control in this._listStartControls)
            {
                control.Enabled = false;
            }
            this._startTime.Enabled = false;
            this._listAllControls = new List<MaskedTextBox>()
            {
                this.Ia,this.fazaIa,this.Fa,
                this.Ib,this.fazaIb,this.Fb,
                this.Ic,this.fazaIc,this.Fc,
                this.In,this.fazaIn,this.Fn,
                this.In1,this.fazan1,this.Fn1,
                this.Ua,this.fazaUa,this.FUa,
                this.Ub,this.fazaUb,this.FUb,
                this.Uc,this.fazaUc,this.FUc,
                this.Un,this.fazaUn,this.FUn,
                this.Un1,this.fazaUn1,this.FUn1
            };
            foreach (var control in this._listAllControls)
            {
                control.MouseDown += new MouseEventHandler(this.MouseRightClick);

            }
            this._listButtons = new List<Button>()
            {
                this._IabutUp,this._IabutDown,this._fazaAbutUp,this._fazaAbutDown,this._FabutUp,this._FabutDown,
                this._IbbutUp,this._IbbutDown,this._fazaBbutUp,this._fazaBbutDown,this._FbbutUp,this._FbbutDown,
                this._IcbutUp,this._IcbutDown,this._fazaCbutUp,this._fazaCbutDown,this._FcbutUp,this._FcbutDown,
                this._InbutUp,this._InbutDown,this._fazaNbutUp,this._fazaNbutDown,this._FnbutUp,this._FnbutDown,
                this._In1butUp,this._In1butDown,this._FazaN1butUp,this._FazaN1butDown,this._Fn1butUp,this._Fn1butDown,
                this._UabutUp,this._UabutDown,this._fazaUabutUp,this._fazaUabutDown,this._FUabutUp,this._FUabutDown,
                this._UbbutUp,this._UbbutDown,this._fazaUbbutUp,this._fazaUbbutDown,this._FUbbutUp,this._FUbbutDown,
                this._UcbutUp,this._UcbutDown,this._fazaUcbutUp,this._fazaUcbutDown,this._FUcbutUp,this._FUcbutDown,
                this._UnbutUp,this._UnbutDown,this._fazaUnbutUp,this._fazaUnbutDown,this._FUnbutUp,this._FUnbutDown,
                this._Un1butUp,this._Un1butDown,this._fazaUn1butUp,this._fazaUn1butDown,this._FUn1butUp,this._FUn1butDown
            };
            #endregion


            #region [Init InputAnalogData]

            this._Ia = new NewStructValidator<InputAnalogDataI>
                (this.toolTip1,
                    new ControlInfoText(this.Ia, new CustomDoubleRule(0, 200)),
                    new ControlInfoText(this.fazaIa, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.Fa, new CustomDoubleRule(0, 256))
                );
            this._Ib = new NewStructValidator<InputAnalogDataI>
                (this.toolTip1,
                    new ControlInfoText(this.Ib, new CustomDoubleRule(0, 200)),
                    new ControlInfoText(this.fazaIb, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.Fb, new CustomDoubleRule(0, 256))
                );
            this._Ic = new NewStructValidator<InputAnalogDataI>
                (this.toolTip1,
                    new ControlInfoText(this.Ic, new CustomDoubleRule(0, 200)),
                    new ControlInfoText(this.fazaIc, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.Fc, new CustomDoubleRule(0, 256))

                );
            this._In = new NewStructValidator<InputAnalogDataI>
                (this.toolTip1,
                    new ControlInfoText(this.In, new CustomDoubleRule(0, 200)),
                    new ControlInfoText(this.fazaIn, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.Fn, new CustomDoubleRule(0, 256))
                );
            this._In1 = new NewStructValidator<InputAnalogDataI>
                (this.toolTip1,
                    new ControlInfoText(this.In1, new CustomDoubleRule(0, 200)),
                    new ControlInfoText(this.fazan1, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.Fn1, new CustomDoubleRule(0, 256))
                );
            this._Ua = new NewStructValidator<InputAnalogDataU>
                (this.toolTip1,
                    new ControlInfoText(this.Ua, new CustomDoubleRule(0, 256)),
                    new ControlInfoText(this.fazaUa, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.FUa, new CustomDoubleRule(0, 256))
                );
            this._Ub = new NewStructValidator<InputAnalogDataU>
                (this.toolTip1,
                    new ControlInfoText(this.Ub, new CustomDoubleRule(0, 256)),
                    new ControlInfoText(this.fazaUb, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.FUb, new CustomDoubleRule(0, 256))
                );
            this._Uc = new NewStructValidator<InputAnalogDataU>
                (this.toolTip1,
                    new ControlInfoText(this.Uc, new CustomDoubleRule(0, 256)),
                    new ControlInfoText(this.fazaUc, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.FUc, new CustomDoubleRule(0, 256))
                );
            this._Un = new NewStructValidator<InputAnalogDataU>
                (this.toolTip1,
                    new ControlInfoText(this.Un, new CustomDoubleRule(0, 256)),
                    new ControlInfoText(this.fazaUn, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.FUn, new CustomDoubleRule(0, 256))
                );
            this._Un1 = new NewStructValidator<InputAnalogDataU>
                (this.toolTip1,
                    new ControlInfoText(this.Un1, new CustomDoubleRule(0, 256)),
                    new ControlInfoText(this.fazaUn1, new CustomDoubleRule(0, 359)),
                    new ControlInfoText(this.FUn1, new CustomDoubleRule(0, 256))
                );

            #endregion


            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.07)
            {
                this._discrets = new NewStructValidator<DiscretsSignals>(this.toolTip1,
                    new ControlInfoCheck(this._d1),
                    new ControlInfoCheck(this._d2),
                    new ControlInfoCheck(this._d3),
                    new ControlInfoCheck(this._d4),
                    new ControlInfoCheck(this._d5),
                    new ControlInfoCheck(this._d6),
                    new ControlInfoCheck(this._d7),
                    new ControlInfoCheck(this._d8),
                    new ControlInfoCheck(this._d9),
                    new ControlInfoCheck(this._d10),
                    new ControlInfoCheck(this._d11),
                    new ControlInfoCheck(this._d12),
                    new ControlInfoCheck(this._d13),
                    new ControlInfoCheck(this._d14),
                    new ControlInfoCheck(this._d15),
                    new ControlInfoCheck(this._d16),
                    new ControlInfoCheck(this._d17),
                    new ControlInfoCheck(this._d18),
                    new ControlInfoCheck(this._d19),
                    new ControlInfoCheck(this._d20),
                    new ControlInfoCheck(this._d21),
                    new ControlInfoCheck(this._d22),
                    new ControlInfoCheck(this._d23),
                    new ControlInfoCheck(this._d24),
                    new ControlInfoCheck(this._d25),
                    new ControlInfoCheck(this._d26),
                    new ControlInfoCheck(this._d27),
                    new ControlInfoCheck(this._d28),
                    new ControlInfoCheck(this._d29),
                    new ControlInfoCheck(this._d30),
                    new ControlInfoCheck(this._d31),
                    new ControlInfoCheck(this._d32),
                    new ControlInfoCheck(this._d33),
                    new ControlInfoCheck(this._d34),
                    new ControlInfoCheck(this._d35),
                    new ControlInfoCheck(this._d36),
                    new ControlInfoCheck(this._d37),
                    new ControlInfoCheck(this._d38),
                    new ControlInfoCheck(this._d39),
                    new ControlInfoCheck(this._d40),
                    new ControlInfoCheck(this._k1),
                    new ControlInfoCheck(this._k2)
                    );
            }
            else
            {
                this._discrets = new NewStructValidator<DiscretsSignals>(this.toolTip1,
                    new ControlInfoCheck(this._d1),
                    new ControlInfoCheck(this._d2),
                    new ControlInfoCheck(this._d3),
                    new ControlInfoCheck(this._d4),
                    new ControlInfoCheck(this._d5),
                    new ControlInfoCheck(this._d6),
                    new ControlInfoCheck(this._d7),
                    new ControlInfoCheck(this._d8),
                    new ControlInfoCheck(this._d9),
                    new ControlInfoCheck(this._d10),
                    new ControlInfoCheck(this._d11),
                    new ControlInfoCheck(this._d12),
                    new ControlInfoCheck(this._d13),
                    new ControlInfoCheck(this._d14),
                    new ControlInfoCheck(this._d15),
                    new ControlInfoCheck(this._d16),
                    new ControlInfoCheck(this._d17),
                    new ControlInfoCheck(this._d18),
                    new ControlInfoCheck(this._d19),
                    new ControlInfoCheck(this._d20),
                    new ControlInfoCheck(this._d21),
                    new ControlInfoCheck(this._d22),
                    new ControlInfoCheck(this._d23),
                    new ControlInfoCheck(this._d24),
                    new ControlInfoCheck(this._d25),
                    new ControlInfoCheck(this._d26),
                    new ControlInfoCheck(this._d27),
                    new ControlInfoCheck(this._d28),
                    new ControlInfoCheck(this._d29),
                    new ControlInfoCheck(this._d30),
                    new ControlInfoCheck(this._d31),
                    new ControlInfoCheck(this._d32),
                    new ControlInfoCheck(this._d33),
                    new ControlInfoCheck(this._d34),
                    new ControlInfoCheck(this._d35),
                    new ControlInfoCheck(this._d36),
                    new ControlInfoCheck(this._d37),
                    new ControlInfoCheck(this._d38),
                    new ControlInfoCheck(this._d39),
                    new ControlInfoCheck(this._d40),
                    new ControlInfoCheck(this._k2),
                    new ControlInfoCheck(this._k1)
                    );
            }
            this._validatorEmulationStruct = new NewStructValidator<WriteStructEmul>
                (this.toolTip1,

                    new ControlInfoCheck(this._startTime),
                    new ControlInfoCombo(this._signal, StringsConfig.RelaySignals),
                    new ControlInfoValidator(this._Ia),
                    new ControlInfoValidator(this._Ib),
                    new ControlInfoValidator(this._Ic),
                    new ControlInfoValidator(this._In),
                    new ControlInfoValidator(this._In1),
                    new ControlInfoValidator(this._Ua),
                    new ControlInfoValidator(this._Ub),
                    new ControlInfoValidator(this._Uc),
                    new ControlInfoValidator(this._Un),
                    new ControlInfoValidator(this._Un1),
                    new ControlInfoValidator(this._discrets)
                );

            this.InitButton();
            this.InitStartControls();
            this.GotFocusInControl();
            this.FromXml();

        }
        /// <summary>
        /// Связывает кнопки с maskedtextbox
        /// </summary>
        private void InitButton()
        {
            int index = 0;
            for (int i = 0; i < this._listButtons.Count;)
            {
                this._listButtons[i].Tag = this._listAllControls[index];
                this._listButtons[i + 1].Tag = this._listAllControls[index];
                index++;
                i = i + 2;
            }

        }
        /// <summary>
        /// Связывает checkbox с m maskedtextbox
        /// </summary>
        private void InitStartControls()
        {
            int count = 0;
            for (int i = 0; i < this._listAllControls.Count; i++)
            {

                this._listAllControls[i].Tag = this._listStartControls[i];
                count = i;
            }
            count++;
            for (int i = 0; i < this._listDiscretsControls.Count; i++)
            {
                this._listDiscretsControls[i].Tag = this._listStartControls[count];
                count++;
            }
        }

        private void GotFocusInControl()
        {
            foreach (var control in this._listAllControls)
            {
                control.GotFocus += this.OnGotFocused;
                control.LostFocus += this.LostFocus;
                control.KeyDown += this.ControlKeyDown;
            }
        }
        #region [IFormView Members]

        public Type ClassType
        {
            get { return typeof(EmulationForm); }

        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.emulation.ToBitmap(); }

        }

        public string NodeName
        {
            get { return "Эмуляция"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public Type FormDevice
        {
            get { return typeof(Mr771); }
        }

        public bool Multishow { get; private set; }

        #endregion

        private void _kvit_Click(object sender, EventArgs e)
        {
            this._kvit.Enabled = false;
            this._memoryEntityNullStructEmulation.LoadStruct();


        }

        private void NullEmulationReadOk()
        {
            try
            {
                this.IsKvit = true;
                this._tempWriteStruct = this._memoryEntityNullStructEmulation.Value;
                WriteStructEmul tempStruct = new WriteStructEmul();
                tempStruct.IsRestart = true;
                this._memoryEntityNullStructEmulation.Value = tempStruct;
                this._memoryEntityNullStructEmulation.SaveStruct();
            }
            catch (Exception)
            {
                this.IsKvit = false;
            }


        }

        private void WriteEmulation()
        {
            if (this.IsKvit)
            {
                this.IsKvit = false;
                this._currentWriteStruct = this._tempWriteStruct;

            }
            else
            {
                this._currentWriteStruct = this._validatorEmulationStruct.Get();
            }

            if (this.IsSelect)
            {
                if (this.IsStart)
                {
                    this._currentWriteStruct.StatusTime = true;
                }

            }

            this._currentWriteStruct.IsRestart = false;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct(new TimeSpan(1000));



        }

        private void EmulationReadOk()
        {
            this._currentWriteStruct = this._memoryEntityStructEmulation.Value;
            this.ShowEmulation();
        }

        private void EmulationWriteOk()
        {
            if (this.IsKvit)
            {
                this._kvit.Enabled = true;
                this.WriteEmulation();
            }
            else
            {
                this._memoryEntityStructEmulation.Value.WriteTimeOk = true;
                this._memoryEntityStructEmulation.LoadStruct();
            }

        }

        private void ShowEmulation()
        {
            this._validatorEmulationStruct.Set(this._currentWriteStruct);

        }




        /// <summary>
        /// Ошибка записи эмуляции
        /// </summary>
        private void EmulationWriteFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = "Нет связи с устройством";
            MessageBox.Show(WRITE_FAIL);

        }

        private void EmulationReadFail()
        {
            this._statusLedControl.State = LedState.Off;
            this._labelStatus.Text = "Нет связи с устройством";


        }

        private void ReadOk()
        {
            this._timeSignal.Text = this._readMemoryEntity.Value.TimeSignal;
            this._time.Text = this._readMemoryEntity.Value.Time;
            this._status.Text = this._readMemoryEntity.Value.Status.ToString();
        }


        private void readEmulation_Click(object sender, EventArgs e)
        {
            this._memoryEntityStructEmulation.LoadStruct();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this._incriment = (double)this._step.Value;
        }


        private void OnGotFocused(Object sender, EventArgs e)
        {
            MaskedTextBox text = sender as MaskedTextBox;
            if (text != null)
            {
                text.SelectAll();
            }
            if (this._writeEmulButton.Checked)
            {
                this._currentAdduction = sender as MaskedTextBox;
                this._currentAdduction.TextChanged += this.OnTextChanged;
            }
        }

        private void ControlKeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                MaskedTextBox mt = sender as MaskedTextBox;
                if (mt.Text == String.Empty)
                {
                    mt.Text = "0";
                }
                int index = this._listAllControls.IndexOf(mt) + 1;
                if (index == this._listAllControls.Count)
                {
                    index = 0;
                }
                if (!_listAllControls[index].Visible)
                {
                    while (!_listAllControls[index].Visible)
                    {
                        index++;
                    }
                }


                this._listAllControls[index].Focus();
            }
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            this.IsChange = true;
        }

        private void LostFocus(object sender, EventArgs e)
        {
            if (!this._writeEmulButton.Checked) return;
            this._currentAdduction = sender as MaskedTextBox;

            if (this.IsChange)
            {
                var isstart = this._currentAdduction.Tag as CheckBox;
                this.IsChange = false;
                this._currentWriteStruct = this._validatorEmulationStruct.Get();
                if (this.IsSelect)
                {
                    if (isstart.Checked || this.IsStart)
                    {
                        this._currentWriteStruct.StatusTime = true;
                    }

                }

            }
            this._currentWriteStruct.IsRestart = false;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._currentAdduction.TextChanged -= this.OnTextChanged;
        }
        private void Up_Click(object sender, EventArgs e)
        {

            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            provider.NumberGroupSeparator = ".";
            Button but = sender as Button;
            MaskedTextBox mtx = but.Tag as MaskedTextBox;
            if (mtx.Text == "")
            {
                mtx.Text = "0";
            }
            if (mtx.Name.Contains("faza"))
            {
                if (mtx.Enabled == false)
                {
                    this._incriment = 0;
                }
                else
                {
                    this._incriment = 1;
                }
                //if (Convert.ToDouble(mtx.Text) <= 0)
                //{
                //    mtx.Text = (Convert.ToDouble(mtx.Text) + 360).ToString();
                //}
                if (Convert.ToDouble(mtx.Text) >= 359)
                {
                    mtx.Text = (Convert.ToDouble(mtx.Text) - 360).ToString();
                }
            }
            else
            {
                this._incriment = (double)this._step.Value;
            }

            double arg = Convert.ToDouble(mtx.Text, provider);
            mtx.Text = (arg + this._incriment).ToString();

            if (mtx.Name.Contains("I") && !mtx.Name.Contains("faza"))
            {
                if (Convert.ToDouble(mtx.Text) > 200)
                {
                    mtx.Text = 200.ToString();
                }
            }
            if (mtx.Name.Contains("U") && !mtx.Name.Contains("faza"))
            {
                if (Convert.ToDouble(mtx.Text) > 256)
                {
                    mtx.Text = 256.ToString();
                }
            }
            if (mtx.Name.Contains("F"))
            {
                if (Convert.ToDouble(mtx.Text) > 256)
                {
                    mtx.Text = 256.ToString();
                }
            }
            if (this.IsWrite)
            {
                this._currentWriteStruct = this._validatorEmulationStruct.Get();
                var cheak = mtx.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (cheak.Checked || this.IsStart)
                    {
                        this._currentWriteStruct.StatusTime = true;
                    }

                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }

        private void Down_Click(object sender, EventArgs e)
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ",";
            provider.NumberGroupSeparator = ".";
            Button but = sender as Button;
            MaskedTextBox mtx = but.Tag as MaskedTextBox;
            if (mtx.Text == "")
            {
                mtx.Text = "0";
            }
            if (mtx.Name.Contains("faza"))
            {
                if (mtx.Enabled == false)
                {
                    this._incriment = 0;
                }
                else
                {
                    this._incriment = 1;
                }
            }
            else
            {
                this._incriment = (double)this._step.Value;
            }

            double arg = Convert.ToDouble(mtx.Text, provider);
            mtx.Text = (arg - this._incriment).ToString();

            ValidationControl(mtx);

            if (this.IsWrite)
            {
                this._currentWriteStruct = this._validatorEmulationStruct.Get();
                var cheak = mtx.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (cheak.Checked || this.IsStart)
                    {
                        this._currentWriteStruct.StatusTime = true;
                    }


                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }


        private void ValidationControl(MaskedTextBox mtx)
        {
            if ((mtx.Name.Contains("I") && !mtx.Name.Contains("faza")) || (mtx.Name.Contains("U") && !mtx.Name.Contains("faza")) || (mtx.Name.Contains("F")))
            {
                if (Convert.ToDouble(mtx.Text) < 0)
                {
                    mtx.Text = 0.ToString();
                }
            }
            if (mtx.Name.Contains("faza"))
            {
                if (Convert.ToDouble(mtx.Text) < 0)
                {
                    mtx.Text = (Convert.ToDouble(mtx.Text) + 360).ToString();
                }
            }
        }
        private void EmulationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this._currentWriteStruct = this._validatorEmulationStruct.Get();
            this._currentWriteStruct.StopTime = true;
            this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
            this._memoryEntityStructEmulation.SaveStruct();
            this._readMemoryEntity.RemoveStructQueries();


        }

        private void EmulationForm_Load(object sender, EventArgs e)
        {
            this._readMemoryEntity.LoadStructCycle(new TimeSpan(100));
        }


        private void _writeEmulButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._writeEmulButton.Checked)
            {
                this._writeEmulButton.BackColor = Color.Black;
                this._writeEmulButton.ForeColor = Color.White;
                if (this.IsSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                    }
                    this._startTime.Enabled = true;
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = false;
                }
                if (!this._device.DeviceDlgInfo.IsConnectionMode) return;
                //if (this._device.MB.IsPortInvalid)

                //{
                //    MessageBox.Show(INVALID_PORT);
                //    return;
                //}
                this.IsWrite = true;
                this.WriteEmulation();
            }
            else
            {
                this._writeEmulButton.BackColor = Color.Empty;
                this._writeEmulButton.ForeColor = Color.Black;
                this.IsWrite = false;
                if (this.IsSelect)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                    }
                    this._startTime.Enabled = true;
                }
            }

        }


        private void _discret_CheckedChanged(object sender, EventArgs e)
        {
            if (this.IsWrite)
            {

                this._currentWriteStruct = this._validatorEmulationStruct.Get();
                var cheak = sender as CheckBox;
                var isstart = cheak.Tag as CheckBox;
                if (this.IsSelect)
                {
                    if (isstart.Checked || this.IsStart)
                    {
                        this._currentWriteStruct.StatusTime = true;
                    }


                }
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();
            }
        }

        private void NominalValue(MaskedTextBox tb)
        {
            if (tb.Name[0] == 'I')
            {
                tb.Text = "5";
            }
            if (tb.Name[0] == 'U')
            {
                double nom = 57.73;
                tb.Text = nom.ToString(CultureInfo.CurrentCulture);
            }
        }

        private void EqualizeAmplitudes(MaskedTextBox tb)
        {
            if (tb.Name.Contains("I") && (tb.Name.Contains("c") || tb.Name.Contains("a") || tb.Name.Contains("b")))
            {
                string val = tb.Text;
                this.Ia.Text = this.Ib.Text = this.Ic.Text = val;
            }
            if (tb.Name.Contains("U") && (tb.Name.Contains("c") || tb.Name.Contains("a") || tb.Name.Contains("b")))
            {
                string val = tb.Text;
                this.Ua.Text = this.Ub.Text = this.Uc.Text = val;
            }
        }
        /// <summary>
        /// Метод выполняет функции обратоного вращения и углы баланса (isDirect=false - обратное вращение)
        /// </summary>
        /// <param name="tb"></param>
        /// <param name="isDirect"></param>
        private void BalanceCorner(MaskedTextBox tb, bool isDirect)
        {
            int index = _listAllControls.IndexOf(tb);
            int currentCorner = Convert.ToInt32(tb.Text);
            
            if (isDirect)
            {
                if (index == 1)
                {
                    CalculatingCornerBalance(currentCorner, 4, 7);
                }

                if (index == 4)
                {
                    CalculatingCornerBalance(currentCorner, 1, 7);
                }

                if (index == 7)
                {
                    CalculatingCornerBalance(currentCorner, 1, 4);
                }

                if (index == 16)
                {
                    CalculatingCornerBalance(currentCorner, 19, 22);
                }

                if (index == 19)
                {
                    CalculatingCornerBalance(currentCorner, 16, 22);
                }

                if (index == 22)
                {
                    CalculatingCornerBalance(currentCorner, 16, 19);
                }

            }

            else
            {
                if (index == 1)
                {
                    CalculatingCornerBalance(currentCorner, 7, 4);
                }

                if (index == 4)
                {
                    CalculatingCornerBalance(currentCorner, 7, 1);
                }

                if (index == 7)
                {
                    CalculatingCornerBalance(currentCorner, 4, 1);
                }

                if (index == 16)
                {
                    CalculatingCornerBalance(currentCorner, 22, 19);
                }

                if (index == 19)
                {
                    CalculatingCornerBalance(currentCorner, 22, 16);
                }

                if (index == 22)
                {
                    CalculatingCornerBalance(currentCorner, 19, 16);
                }
            }
        }

        private void CalculatingCornerBalance(int currentCorner, int indexFor240, int indexFor120)
        {
            int cacheCornerValue240;
            int cacheCornerValue120;
            int cornerValue240;
            int cornerValue120;

            cacheCornerValue240 = currentCorner + 240;

            if (cacheCornerValue240 > 359)
            {
                cornerValue240 = cacheCornerValue240 - 360;
                this._listAllControls[indexFor240].Text = Convert.ToString((int) cornerValue240);
            }
            else
            {
                this._listAllControls[indexFor240].Text = Convert.ToString(cacheCornerValue240);
            }

            cacheCornerValue120 = currentCorner + 120;

            if (cacheCornerValue120 > 359)
            {
                cornerValue120 = cacheCornerValue120 - 360;
                this._listAllControls[indexFor120].Text = Convert.ToString((int) cornerValue120);
            }
            else
            {
                this._listAllControls[indexFor120].Text = Convert.ToString(cacheCornerValue120);
            }
        }

        private void MouseRightClick(object sender, MouseEventArgs e)
        {
            var tb = sender as MaskedTextBox;
            if (tb == null)
            {
                return;
            }
            if (e.Button == MouseButtons.Left)
            {
                tb.SelectAll();
            }
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu cm = new ContextMenu();
                cm.MenuItems.Add("Номинальное значение", (o, ea) =>
                {
                    this.NominalValue(tb);
                    if (this._writeEmulButton.Checked)
                    {
                        this.WriteEmulation();
                    }
                });
                cm.MenuItems.Add("Ноль", (o, ea) =>
                {
                    tb.Text = "0";
                    if (this._writeEmulButton.Checked)
                    {
                        this.WriteEmulation();
                    }
                });
                cm.MenuItems.Add("Уравнять амплитуды", (o, ea) =>
                {
                    this.EqualizeAmplitudes(tb);
                    if (this._writeEmulButton.Checked)
                    {
                        this.WriteEmulation();
                    }
                });
                if (tb.Name.Contains("faza"))
                {
                    cm.MenuItems.Clear();
                    cm.MenuItems.Add("Ноль", (o, ea) =>
                    {
                        tb.Text = "0";
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });

                    cm.MenuItems.Add("Углы баланса", (o, ea) =>
                    {
                        this.BalanceCorner(tb, true);
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });
                    cm.MenuItems.Add("Обратное вращение", (o, ea) =>
                    {
                        this.BalanceCorner(tb, false);
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });

                }

                if (tb.Name.Contains("F"))
                {
                    cm.MenuItems.Clear();
                    cm.MenuItems.Add("Номинальная частота", (o, ea) =>
                    {
                        tb.Text = "50";
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });

                    cm.MenuItems.Add("Уравнять частоты", (o, ea) =>
                    {
                        string val = tb.Text;
                        this.Fa.Text = val; this.Fb.Text = val; this.Fc.Text = val; this.Fn.Text = val;
                        this.FUa.Text = val; this.FUb.Text = val; this.FUc.Text = val; this.FUn.Text = val; this.FUn1.Text = val;
                        if (this._writeEmulButton.Checked)
                        {
                            this.WriteEmulation();
                        }
                    });

                }
                tb.ContextMenu = cm;
            }
        }

        private void _signal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._signal.SelectedIndex == 0)
            {
                this._currentWriteStruct = this._validatorEmulationStruct.Get();
                this._currentWriteStruct.StopTime = true;
                this._memoryEntityStructEmulation.Value = this._currentWriteStruct;
                this._memoryEntityStructEmulation.SaveStruct();

                this.IsSelect = false;
                this._startTime.Enabled = false;
                foreach (var control in this._listStartControls)
                {
                    control.Enabled = false;
                }
            }
            else
            {
                if (!this._writeEmulButton.Checked)
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = false;
                        this._startTime.Enabled = true;
                    }
                }
                else
                {
                    foreach (var control in this._listStartControls)
                    {
                        control.Enabled = true;
                        this._startTime.Enabled = true;
                    }
                }

                this.IsSelect = true;
            }


        }

        private void _status_TextChanged(object sender, EventArgs e)
        {
            switch (this._status.Text)
            {
                case "1":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 без блокировки выходных сигналов запущена";
                        break;
                    }
                case "2":
                    {
                        this._statusLedControl.State = LedState.Signaled;
                        this._labelStatus.Text = "Эмуляция 1 с блокировкой выходных сигналов запущена";
                        break;
                    }
                case "3":
                    {
                        this._statusLedControl.State = LedState.NoSignaled;
                        this._labelStatus.Text = "Эмуляция остановлена";
                        break;
                    }
                case "0":
                    {
                        this._statusLedControl.State = LedState.NoSignaled;
                        this._labelStatus.Text = "Эмуляция остановлена";
                        break;
                    }

            }
        }

        private void ToXML()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(WriteStructEmul));

                using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                {
                    serializer.Serialize(writer, _validatorEmulationStruct.Get());
                }

            }
        }

        private void FromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(WriteStructEmul));
            XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(Settings.Default.EmulationDefailtStruct));
            reader.Read();
            this._validatorEmulationStruct.Set(serializer.Deserialize(reader));
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.ToXML();
        }

        private void _startTime_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ch = sender as CheckBox;
            if (ch.Checked)
            {
                this.IsStart = true;
            }
            else
            {
                this.IsStart = false;
            }
        }

        private void _exitEmulation_CheckedChanged(object sender, EventArgs e)
        {
            if (_exitEmulation.Checked)
            {
                IsExit = true;
            }
            else
            {
                IsExit = false;
            }
        }

        private void _time_TextChanged(object sender, EventArgs e)
        {
            if (IsExit)
            {
                if (_time.Text.Contains("0:05"))
                {
                    WriteEmulation();

                }
            }
        }

        private void EmulationForm_Activated(object sender, EventArgs e)
        {
            StringsConfig.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }
    }
}
