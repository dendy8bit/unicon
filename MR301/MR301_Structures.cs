﻿using System;
using System.Runtime.InteropServices;

namespace BEMN.MR301
{
    //[StructLayout(LayoutKind.Sequential, Pack = 1)]
    //public struct SystemJournal
    //{
    //    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
    //    public Int16[] datatime;
    //}

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AlarmJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public Int16[] datatime; //8
        public Int16 difIndex;   
        public Int16 paramIndex; //10
        public Int16 paramVal;
        public Int16 ustGroup;
        public Int16 Ia;
        public Int16 Ib;
        public Int16 Ic;
        public Int16 Io;
        public Int16 Ig;
        public Int16 I0;
        public Int16 I1;
        public Int16 D1;        //20
    }
}
