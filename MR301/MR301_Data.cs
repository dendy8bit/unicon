using System;
using System.Collections.Generic;

namespace BEMN.MR301
{
    public class Strings
    {
        public const string MR301_FORMAT0_F2 = " = {0:F2}";
        public const string MR301_FORMAT_0_F2_SYS = "{0:F2}";
        public const string MEASURINGFORM_IA_PATTERN = "Ia = {0:F2} А";
        public const string MEASURINGFORM_IB_PATTERN = "Ib = {0:F2} А";
        public const string MEASURINGFORM_IC_PATTERN = "Ic = {0:F2} А";
        public const string MEASURINGFORM_IO_PATTERN = "I0 = {0:F2} А";
        public const string MEASURINGFORM_I1_PATTERN = "I1 = {0:F2} А";
        public const string MEASURINGFORM_I2_PATTERN = "I2 = {0:F2} А";
        public const string MEASURINGFORM_IG_PATTERN = "Ig = {0:F2} А";
        public const string MEASURINGFORM_I0_PATTERN = "Io = {0:F2} А";
        public const string MR301_A = "A";
        public const string MR301_B = "B";
        public const string MR301_C = "C";
        public const string QESTION = " ?";
        public const string MR301_I_1 = "I>";
        public const string MR301_I_2 = "I>>";
        public const string MR301_I_3 = "I>>>";
        public const string MR301_I_4 = "I>>>>";
        public const string MR301_I0_1 = "I0>";
        public const string MR301_I0_2 = "I0>>";
        public const string MR301_I2_1 = "I2>";
        public const string MR301_I2_2 = "I2>>";
        public const string MR301_IG = "Ig";
        public const string MR301_IO = "Io";
        public const string MR301_IA = "Ia";
        public const string MR301_IB = "Ib";
        public const string MR301_IC = "Ic";
        public const string MR301_I0 = "I0";
        public const string MR301_I1 = "I1";
        public const string MR301_I2 = "I2";
        public const string ALARMJOURNALFORM_IN = "In";
        public const string ALARMJOURNALFORM_F = "F";
        public const string ALARMJOURNALFORM_UAB = "Uab";
        public const string ALARMJOURNALFORM_UBC = "Ubc";
        public const string ALARMJOURNALFORM_UCA = "Uca";
        public const string ALARMJOURNALFORM_U0 = "U0";
        public const string ALARMJOURNALFORM_U1 = "U1";
        public const string ALARMJOURNALFORM_U2 = "U2";
        public const string ALARMJOURNALFORM_UN = "Un";

        public static List<string> OutputSignals
        {
            get
            {

                return _outputSignals;
            }
        }

        public static List<string> ExternalDefense
        {
            get
            {
                return _externalDefense;
            }
        }

        public static List<string> EngineMode
        {
            get
            {
                return new List<string>
                    {
                        "Работа",
                        "Пуск",
                        "XXXXX"
                    };

            }
        }

        public static List<string> FeatureI
        {
            get
            {
                return new List<string>
                    {
                        "Независимая",
                        "Зависимая",
                        "XXXXX"
                    };
            }
        }

        public static List<string> FeatureI02
        {
            get
            {
                return new List<string>
                    {
                        "Независимая",
                        "XXXXX"
                    };
            }
        }

        public static List<string> Blocking
        {
            get
            {
                return new List<string>
                    {
                        "Ненаправленная",
                        "Блокировка",
                        "XXXXX"
                    };
            }
        }

        public static List<string> TokSignalI
        {
            get
            {
                return new List<string>
                    {
                        "Одна фаза",
                        "Все фазы",
                        "XXXXX"
                    };
            }
        }

        public static List<string> TokSignalI2
        {
            get
            {
                return new List<string>
                    {
                        "Расчет",
                        "XXXXX"
                    };
            }
        }

        public static List<string> TokSignalI0
        {
            get
            {
                return new List<string>
                    {
                        "Измерение",
                        "Расчет",
                        "Гармоника",
                        "XXXXX"
                    };
            }
        }

        public static List<string> YesNo
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Да",
                        "XXXXX"
                    };
            }
        }

        public static List<string> Crat
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "1 крат",
                        "2 крат",
                        "XXXXX"
                    };
            }
        }

        public static List<string> Forbidden
        {
            get
            {
                return new List<string>
                    {
                        "Запрещено",
                        "Разрешено",
                        "XXXXX"
                    };
            }
        }

        public static List<string> Control
        {
            get
            {
                return new List<string>
                    {
                        "Разрешено",
                        "Контроль",
                        "XXXXX"
                    };
            }
        }

        public static List<string> All
        {
            get
            {
                return _all;
            }
        }

        public static List<String> Diskret
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Д1 Инв.",
                        "Д1",
                        "Д2 Инв.",
                        "Д2",
                        "Д3 Инв.",
                        "Д3",
                        "Д4 Инв.", //40
                        "Д4",
                        "Д5 Инв.",
                        "Д5",
                        "Д6 Инв.",
                        "Д6",
                        "Д7 Инв.",
                        "Д7",
                        "Д8 Инв.",
                        "Д8",
                        "XXXXX"
                    };
            }
        }

        public static List<string> SystemJournal
        {
            get { return _systemJournal; }
        }

        public static List<String> Output
        {
            get { return _output; }
        }

        public static List<string> Modes
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Контроль ИО",
                        "Срабатывание",
                        "Сигнализация",
                        "Авария",
                        "XXXXX"
                    };
            }
        }

        public static List<string> Modes2
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Срабатывание",
                        "Сигнализация",
                        "Авария",
                        "XXXXX"
                    };
            }
        }

        public static List<string> ModesLight
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Введено",
                        "XXXXX"
                    };
            }
        }

        public static List<String> Logic
        {
            get { return _logic; }
        }

        #region Lists of strings inicialization

        private static List<string> _outputSignals = new List<string>
            {
                "Д1",
                "Д2",
                "Д3",
                "Д4",
                "Д5",
                "Д6",
                "Д7",
                "Д8",
                "Л1",
                "Л2",
                "Л3",
                "Л4", //20
                "Л5",
                "Л6",
                "Л7",
                "Л8",
                "I> ИО",
                "I>",
                "I>> ИО",
                "I>>",
                "I>>> ИО",
                "I>>>", //30
                "I>>>> ИО",
                "I>>>>",
                "I0> ИО",
                "I0>",
                "I0>> ИО",
                "I0>>",
                "I2> ИО",
                "I2>",
                "I2>> ИО",
                "I2>>", //40
                "ВЗ 1",
                "ВЗ 2",
                "ВЗ 3",
                "ВЗ 4",
                "ВЗ 5",
                "ВЗ 6",
                "ВЗ 7",
                "ВЗ 8",
                "АЧР",
                "ЧАПВ", //50
                "АВР ВКЛ",
                "АВР ОТК",
                "АВР БЛ",
                "РЕЗЕРВ",
                "УСКОР.",
                "АПВ",
                "Откл. выкл-ля",
                "Вкл. вык-ля",
                "Неисправность",
                "Рез. уставки", //60
                "Сигнализация",
                "Авария",
                "Земля",
                "Работа УРОВ"
            };

        private static List<String> _externalDefense = new List<string>
            {
                "Нет",
                "Д1 Инв.",
                "Д1",
                "Д2 Инв.",
                "Д2",
                "Д3 Инв.",
                "Д3",
                "Д4 Инв.",
                "Д4",
                "Д5 Инв.",
                "Д5",
                "Д6 Инв.",
                "Д6",
                "Д7 Инв.",
                "Д7",
                "Д8 Инв.",
                "Д8",
                "Л1 Инв.",
                "Л1",
                "Л2 Инв.",
                "Л2",
                "Л3 Инв.",
                "Л3",
                "Л4 Инв.",
                "Л4",
                "Л5 Инв.",
                "Л5",
                "Л6 Инв.",
                "Л6",
                "Л7 Инв.",
                "Л7",
                "Л8 Инв.",
                "Л8",
                "I> ИО Инв.",
                "I> ИО",
                "I> Инв.",
                "I>",
                "I>> ИО Инв.",
                "I>> ИО",
                "I>> Инв.",
                "I>>",
                "I>>> ИО Инв.",
                "I>>> ИО",
                "I>>> Инв.",
                "I>>>",
                "I>>>> ИО Инв.",
                "I>>>> ИО",
                "I>>>> Инв.",
                "I>>>>",
                "I0> ИО Инв.",
                "I0> ИО",
                "I0> Инв.",
                "I0>",
                "I0>> ИО Инв.",
                "I0>> ИО",
                "I0>> Инв.",
                "I0>>",
                "I2> ИО Инв.",
                "I2> ИО",
                "I2> Инв.",
                "I2>",
                "I2>> ИО Инв.",
                "I2>> ИО",
                "I2>> Инв.",
                "I2>>",
                "XXXXX"
            };

        private static List<String> _all = new List<string>
            {
                "Нет",
                "Откл.выкл. <Инв.>",
                "Откл.выкл.",
                "Вкл.выкл. <Инв.>",
                "Вкл.выкл.",
                "Неиспр. <Инв>",
                "Неисправность",
                "Осн. уставки",
                "Рез. уставки",
                "Сигнал <Инв>",
                "Сигнализация",
                "Авария <Инв>",
                "Авария",
                "Земля  <Инв>",
                "Земля",
                "Работа уров <Инв>",
                "Работа уров",
                "Д1 <Инв>",
                "Д1",
                "Д2 <Инв>",
                "Д2",
                "Д3 <Инв>",
                "Д3",
                "Д4 <Инв>",
                "Д4",
                "Д5 <Инв>",
                "Д5",
                "Д6 <Инв>",
                "Д6",
                "Д7 <Инв>",
                "Д7",
                "Д8 <Инв>",
                "Д8",
                "Л1 <Инв>",
                "Л1",
                "Л2 <Инв>",
                "Л2",
                "Л3 <Инв>",
                "Л3",
                "Л4 <Инв>",
                "Л4",
                "Л5 <Инв>",
                "Л5",
                "Л6 <Инв>",
                "Л6",
                "Л7 <Инв>",
                "Л7",
                "Л8 <Инв>",
                "Л8",
                "I > ИО <Инв>",
                "I > ИО",
                "I > <Инв>",
                "I >",
                "I >> ИО <Инв>",
                "I >> ИО",
                "I >> <Инв>",
                "I >>",
                "I >>> ИО <Инв>",
                "I >>> ИО",
                "I >>> <Инв>",
                "I >>>",
                "I >>>> ИО <Инв>",
                "I >>>> ИО",
                "I >>>> <Инв>",
                "I >>>>",
                "I0> ИО <Инв>",
                "I0> ИО",
                "I0> <Инв>",
                "I0>",
                "I0>> ИО <Инв>",
                "I0>> ИО",
                "I0>> <Инв>",
                "I0>>",
                "I2> ИО <Инв>",
                "I2> ИО",
                "I2> <Инв>",
                "I2>",
                "I2>> ИО <Инв>",
                "I2>> ИО",
                "I2>> <Инв>",
                "I2>>",
                "ВЗ 1 <Инв.>",
                "ВЗ 1",
                "ВЗ 2  <Инв.>",
                "ВЗ 2",
                "ВЗ 3  <Инв.>",
                "ВЗ 3",
                "ВЗ 4  <Инв.>",
                "ВЗ 4",
                "ВЗ 5  <Инв.>",
                "ВЗ 5",
                "ВЗ 6  <Инв.>",
                "ВЗ 6",
                "ВЗ 7  <Инв.>",
                "ВЗ 7",
                "ВЗ 8  <Инв.>",
                "ВЗ 8",
                "АЧР <Инв.>",
                "АЧР",
                "ЧАПВ <Инв.>",
                "ЧАПВ",
                "АВР вкл. <Инв.>",
                "АВР вкл.",
                "АВР откл. <Инв.>",
                "АВР откл.",
                "АВР блок. <Инв.>",
                "АВР блок.",
                "Резерв  <Инв.>",
                "Резерв",
                "Ускорение  <Инв.>",
                "Ускорение",
                "АПВ <Инв.>",
                "АПВ",
                "ВЛС1 <Инв.>",
                "ВЛС1",
                "ВЛС2 <Инв.>",
                "ВЛС2",
                "ВЛС3 <Инв.>",
                "ВЛС3",
                "ВЛС4 <Инв.>",
                "ВЛС4",
                "ВЛС5 <Инв.>",
                "ВЛС5",
                "ВЛС6 <Инв.>",
                "ВЛС6",
                "ВЛС7 <Инв.>",
                "ВЛС7",
                "ВЛС8 <Инв.>",
                "ВЛС8",
                "XXXXX"
            };

        private static List<string> _systemJournal = new List<string>
            {
                "Журнал пуст", //0
                "Ошибка хранения данных",
                "Ошибка хранения данных",
                "Неисправность вн. шины",
                "Вн. шина исправна",
                "Температура выше нормы",
                "Температура в норме",
                "Температура ниже нормы",
                "Температура в норме",
                "МСА неисправен",
                "МСА исправен", //10
                "МРВ неисправен",
                "МРВ исправен",
                "МСД1 неисправен",
                "МСД1 исправен",
                "МСД2 неисправен",
                "МСД2 исправен",
                "Ошибка контрольной суммы уставок",
                "Ошибка контрольной суммы данных",
                "Ошибка контрольной суммы данных",
                "Ошибка журнала системы", //20
                "Ошибка журнала аварий",
                "Остановка часов",
                "Сообщения нет",
                "Сообщения нет",
                "МЕНЮ уставки изменены", //25
                "Пароль изменен",
                "Сброс журнала системы",
                "Сброс журнала аварий",
                "Сброс ресурса выключателя",
                "Сброс индикации", //30
                "Изменена группа уставок",
                "СДТУ - уставки изменены", //32
                "Ошибка задающего генератора",
                "Рестарт устройства",
                "Устройство выключено",
                "Устройство включено",
                "Сообщения нет",
                "МЕНЮ - сброс осциллографа",
                "СДТУ - сброс осциллографа",
                "Выключатель отключен", //40
                "Выключатель включен",
                "Блокировка выключателя",
                "Отказ выключателя",
                "Неисправность выключателя",
                "Внешняя неисправность выключателя",
                "Неисправность управления выключателя",
                "Работа УРОВ",
                "Защита отключить",
                "АПВ блокировано",
                "АПВ внешняя блокировка", //50
                "Запуск АПВ 1 крат",
                "Запуск АПВ 2 крат",
                "АПВ включить",
                "АЧР блокировано",
                "АЧР отключить",
                "Запуск ЧАПВ",
                "ЧАПВ блокировано",
                "ЧАПВ внешняя блокировка",
                "ЧАПВ включить",
                "АВР блокировано", //60
                "АВР внешняя блокировка",
                "Контроль АВР",
                "АВР отключить",
                "АВР включить",
                "АВР включить резерв",
                "АВР отключить резерв",
                "Кнопка отключить",
                "Кнопка включить",
                "Ключ отключить",
                "Ключ включить", //70	
                "Внешнее отключить",
                "Внешнее включить",
                "СДТУ отключить",
                "СДТУ включить",
                "Основные уставки",
                "Резервные уставки",
                "Внешние резервные уставки",
                "Сообщения нет",
                "Меню основные уставки",
                "Меню резервнвые уставки", //80
                "СДТУ основные уставки",
                "СДТУ резервнвые уставки",
                "Сообщения нет"
            };

        private static List<string> _output = new List<string>
            {
                "Д1",
                "Д2",
                "Д3",
                "Д4",
                "Д5",
                "Д6",
                "Д7",
                "Д8",
                "Л1",
                "Л2",
                "Л3",
                "Л4", //20
                "Л5",
                "Л6",
                "Л7",
                "Л8",
                "I> ИО",
                "I>",
                "I>> ИО",
                "I>>",
                "I>>> ИО",
                "I>>>", //30
                "I>>>> ИО",
                "I>>>>",
                "I0> ИО",
                "I0>",
                "I0>> ИО",
                "I0>>",
                "I2> ИО",
                "I2>",
                "I2>> ИО",
                "I2>>", //40
                "ВЗ 1",
                "ВЗ 2",
                "ВЗ 3",
                "ВЗ 4",
                "ВЗ 5",
                "ВЗ 6",
                "ВЗ 7",
                "ВЗ 8",
                "АЧР",
                "ЧАПВ", //50
                "АВР ВКЛ",
                "АВР ОТК",
                "АВР БЛ",
                "РЕЗЕРВ",
                "УСКОР.",
                "АПВ",
                "Откл. выкл-ля",
                "Вкл. вык-ля",
                "Неисправность",
                "Осн. уставки", //60
                "Сигнализация",
                "Авария",
                "Земля",
                "Работа УРОВ"
            };

        private static List<string> _logic = new List<string>
            {
                "Нет",
                "Д1 Инв.",
                "Д1",
                "Д2 Инв.",
                "Д2",
                "Д3 Инв.",
                "Д3",
                "Д4 Инв.", //40
                "Д4",
                "Д5 Инв.",
                "Д5",
                "Д6 Инв.",
                "Д6",
                "Д7 Инв.",
                "Д7",
                "Д8 Инв.",
                "Д8",
                "Л1 Инв.",
                "Л1",
                "Л2 Инв.",
                "Л2",
                "Л3 Инв.", //70
                "Л3",
                "Л4 Инв.",
                "Л4",
                "Л5 Инв.",
                "Л5",
                "Л6 Инв.",
                "Л6",
                "Л7 Инв.",
                "Л7",
                "Л8 Инв.", //80
                "Л8",
                "XXXXX"
            };

        #endregion Lists of strings inicialization

    }
}
