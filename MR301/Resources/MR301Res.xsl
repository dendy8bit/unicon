<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <xsl:variable name="vers" select="MR301/DeviceVersion"></xsl:variable>
    <h3>���������� <xsl:value-of select="MR301/DeviceType"/> ������ �� <xsl:value-of select="MR301/DeviceVersion"/></h3>
	  <p></p>
	    <h1>������� �������</h1>
		<h3>��������� ����</h3>
		<table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th>��������� ��� ��</th>
         <th>��������� ��� ����</th>
		 <th>����. ��� ��������</th>
      </tr>
      <tr  align="center">
         <td><xsl:value-of select="MR301/��"/></td>
         <td><xsl:value-of select="MR301/����"/></td>
         <td><xsl:value-of select="MR301/������������_���"/></td>
      </tr>
    </table>
			<h3>������� �������</h3>
		<table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th>���� ���������</th>
         <th>���� ��������</th>
		     <th>����. ���������</th>
		     <th>����. ��������</th>
         <th>����� ���������</th>
	     	 <th>������. �������</th>
         <xsl:if test="$vers = 2.16">
            <th>���������� ����</th>
         </xsl:if>
      </tr>
      <tr align="center">
         <td><xsl:value-of select="MR301/����_���������"/></td>
         <td><xsl:value-of select="MR301/����_��������"/></td>
         <td><xsl:value-of select="MR301/�������_���������"/></td>
		 <td><xsl:value-of select="MR301/�������_��������"/></td>
         <td><xsl:value-of select="MR301/�����_���������"/></td>
         <td><xsl:value-of select="MR301/������������_�������"/></td>
         <xsl:if test="$vers = 2.16">
            <td><xsl:value-of select="MR301/����������_����"/></td>
         </xsl:if>
      </tr>
    </table>
	<h3>�����������</h3>
		<table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th>��������� ���������</th>
         <th>��������� ��������</th>
		 <th>�������������</th>
		 <th>����������</th>
         <th>����� ����������</th>
		 <th>�������, ��</th>
      </tr>
      <tr align="center">
         <td><xsl:value-of select="MR301/�����������_���������"/></td>
         <td><xsl:value-of select="MR301/�����������_��������"/></td>
         <td><xsl:value-of select="MR301/�����������_������"/></td>
		 <td><xsl:value-of select="MR301/�����������_����������"/></td>
         <td><xsl:value-of select="MR301/�����������_�����"/></td>
         <td><xsl:value-of select="MR301/�����������_�������"/></td>
      </tr>
    </table>
		<h3>������� ����������</h3>
		<table border="1">
      <tr bgcolor="#c1ced5" align="center">
	     <th>�� ������</th>
         <th>�� �����</th>
         <th>�������</th>
		 <th>�� ����</th>
      </tr>
      <tr align="center">
         <td><xsl:value-of select="MR301/������_��_������"/></td>
         <td><xsl:value-of select="MR301/������_��_�����"/></td>
         <td><xsl:value-of select="MR301/������_��_��������"/></td>
		 <td><xsl:value-of select="MR301/������_��_����"/></td>
      </tr>
    </table>
	<h3>��������� �����</h3>
		<table border="1">
      <tr bgcolor="#c1ced5" align="center">
	     <th>����� �����</th>
         <th>�� ���������</th>
         <th>������������</th>
      </tr>
      <tr align="center">
         <td><xsl:value-of select="MR301/�����_�����_���������"/></td>
         <td><xsl:value-of select="MR301/���������_��_���������"/></td>
         <td><xsl:value-of select="MR301/������������_���������"/></td>
      </tr>
    </table>


    <h3>������� ���������� �������</h3>

      <div id="the whole thing" style="width:100%; overflow: hidden;">
        <div id="col1"  align="center" style="float:left; margin-right:20px;">

          <table border="1" cellspacing="0" align="center">
            <td align="center">
              <b>������ 1 �</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="#c1ced5" align="center">
                  <th>�����</th>
                  <th>��������</th>
                </tr>

                <xsl:for-each select="MR301/�������_����������_�������/�1/�������">
                  <xsl:if test="position() &lt; 9">
                    <tr align="center">
                      <td>
                        <xsl:value-of  select="format-number(position(), '#')"/>
                      </td>

                      <td>
                        <xsl:value-of select="current()"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>

            </td>
          </table>
        </div>



        <div id="col2"  align="center" style="float:left; margin-right:20px;">
          <table border="1" cellspacing="0" align="center">
            <td align="center">
              <b>������ 2 �</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="#c1ced5" align="center">
                  <th>�����</th>
                  <th>��������</th>
                </tr>

                <xsl:for-each select="MR301/�������_����������_�������/�2/�������">
                  <tr>
                    <tr align="center">
                      <xsl:if test="position() &lt; 9">

                        <td>
                          <xsl:value-of  select="format-number(position(), '#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>

                        </td>
                      </xsl:if>
                    </tr>

                  </tr>


                </xsl:for-each>

              </table>

            </td>
          </table>
        </div>
        <div id="col3" align="center" style="float:left; margin-right:20px;">
          <table border="1" cellspacing="0"  align="center">
            <td align="center">
              <b>������ 3 �</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="#c1ced5" align="center">
                  <th>�����</th>
                  <th>��������</th>
                </tr>

                <xsl:for-each select="MR301/�������_����������_�������/�3/�������">
                  <tr>
                    <tr align="center">
                      <xsl:if test="position() &lt; 9">

                        <td>
                          <xsl:value-of  select="format-number(position(), '#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>

                        </td>
                      </xsl:if>
                    </tr>

                  </tr>
                </xsl:for-each>
              </table>

            </td>
          </table>
        </div>
        <div id="col4" align="center" style="float:left; margin-right:20px;">
          <table border="1" cellspacing="0" align="center">
            <td align="center">
              <b>������ 4 �</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="#c1ced5" align="center">
                  <th>�����</th>
                  <th>��������</th>
                </tr>

                <xsl:for-each select="MR301/�������_����������_�������/�4/�������">
                  <tr>
                    <tr align="center">
                      <xsl:if test="position() &lt; 9">

                        <td>
                          <xsl:value-of  select="format-number(position(), '#')"/>
                        </td>

                        <td>
                          <xsl:value-of select="current()"/>

                        </td>
                      </xsl:if>
                    </tr>

                  </tr>
                </xsl:for-each>
              </table>

            </td>
          </table>
        </div>
      </div>

    <p></p>

    <div id="the whole thing" style="width:100%; overflow: hidden;">
      <div id="col1" align="center" style="float:left; margin-right:20px;">

        <table border="1" cellspacing="0" align="center">
          <td align="center">
            <b>������ 1 ���</b>
            <table border="1" cellspacing="0" align="center">

              <tr bgcolor="#c1ced5">
                <th>�����</th>
                <th>��������</th>
              </tr>

              <xsl:for-each select="MR301/�������_����������_�������/���1/�������">
                <tr>
                  <tr align="center">
                    <xsl:if test="position() &lt; 9">

                      <td>
                        <xsl:value-of  select="format-number(position(), '#')"/>
                      </td>

                      <td>
                        <xsl:value-of select="current()"/>

                      </td>
                    </xsl:if>
                  </tr>

                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>


      <div id="col2" align="center" style="float:left; margin-right:20px;">
        <table border="1" cellspacing="0" align="center">
          <td align="center">
            <b>������ 2 ���</b>
            <table border="1" cellspacing="0" align="center">

              <tr bgcolor="#c1ced5">
                <th>�����</th>
                <th>��������</th>
              </tr>

              <xsl:for-each select="MR301/�������_����������_�������/���2/�������">
                <tr>
                  <tr align="center">
                    <xsl:if test="position() &lt; 9">

                      <td>
                        <xsl:value-of  select="format-number(position(), '#')"/>
                      </td>

                      <td>
                        <xsl:value-of select="current()"/>

                      </td>
                    </xsl:if>
                  </tr>

                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col3" align="center" style="float:left; margin-right:20px;">
        <table border="1" cellspacing="0" align="center">
          <td align="center">
            <b>������ 3 ���</b>
            <table border="1" cellspacing="0" align="center">

              <tr bgcolor="#c1ced5">
                <th>�����</th>
                <th>��������</th>
              </tr>

              <xsl:for-each select="MR301/�������_����������_�������/���3/�������">
                <tr>
                  <tr align="center">
                    <xsl:if test="position() &lt; 9">

                      <td>
                        <xsl:value-of  select="format-number(position(), '#')"/>
                      </td>

                      <td>
                        <xsl:value-of select="current()"/>

                      </td>
                    </xsl:if>
                  </tr>

                </tr>
              </xsl:for-each>
            </table>

          </td>
        </table>
      </div>
      <div id="col4" align="center" style="float:left; margin-right:20px;">
        <table border="1" cellspacing="0">       
          <td align="center">
            <b>������ 4 ���</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="#c1ced5">
                <th>�����</th>
                <th>��������</th>
              </tr>
              <xsl:for-each select="MR301/�������_����������_�������/���4/�������">
                <tr>
                  <tr align="center">
                    <xsl:if test="position() &lt; 9">
                      <td>
                        <xsl:value-of  select="format-number(position(), '#')"/>
                      </td>
                      <td>
                        <xsl:value-of select="current()"/>
                      </td>
                    </xsl:if>
                  </tr>
                </tr>
              </xsl:for-each>
            </table>
          </td>
        </table>
      </div>

    </div>
    
    
    
	<h1>�������� �������</h1>
	<h3>�������� ����</h3>	
	<table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th>����� ����</th>
         <th>���</th>
         <th>������</th>
         <th>�������, ��</th>
      </tr>
      <xsl:for-each select="MR301/OutputRele/OutputReleItem">
      <tr align="center">
	  	  <xsl:if test="position() &lt; 6">
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="@�������"/></td>
		 		 </xsl:if>
      </tr>
    </xsl:for-each>
    </table>
		<div style="height:70px"> </div>
		<h3>����������</h3>	
	<table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>����� ����������</th>
         <th>���</th>
         <th>������</th>
         <th>����� �����. �� ���. ���.</th>
         <th>����� �����. �� ��</th>
         <th>����� �����. �� ��</th>
      </tr>
       <xsl:for-each select="MR301/OutputIndicator/OutputIndicatorItem">
        <tr align="center">
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <xsl:element name="td">
        <xsl:attribute name="id">indexing_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���������"/>,"indexing_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
      <xsl:element name="td">
        <xsl:attribute name="id">alarm_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@������"/>,"alarm_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
         <xsl:element name="td">
        <xsl:attribute name="id">sys_<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@�������"/>,"sys_<xsl:value-of select="position()"/>");</script>
      </xsl:element>
        </tr>
      </xsl:for-each>
    </table>
	<h3>�������������</h3>
<table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th>������� ����</th>
      </tr>
         <tr align="center">
         <td><xsl:value-of select="MR301/����_�������������_������������"/></td>
         </tr>
   </table>

    <h3>�������� ���������� �������</h3>
    <table border="1" cellspacing="0">
       <tr bgcolor="#c1ced5" align="center">

        <th>�����</th>
        <th>������������</th>
      </tr>
      <xsl:for-each select="MR301/VlsXml/ArrayOfString">
        <tr align="center">
          <td>
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:for-each select="string">
              <xsl:value-of select="current()"/>|
            </xsl:for-each>
          </td>
        </tr>
      </xsl:for-each>
    </table>

    <h3>���� �������������</h3>
    <table border="1" cellspacing="0">
      <tr bgcolor="#c1ced5" align="center">
        <th>����������</th>
        <th>�����������</th>
        <th>������</th>
        <th>������</th>
        <th>�����������</th>
        <th>�������������� ��������� ����-���������</th>
        <th>����� y�������� ������������</th>
        <th>����� �����������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:variable  name = "v1" select = "MR301/�������������[1]" />
          <xsl:if test="$v1 = 'false' " >��� </xsl:if>
          <xsl:if test="$v1 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v2" select= "MR301/�������������[2]" />
          <xsl:if test="$v2 = 'false' " >��� </xsl:if>
          <xsl:if test="$v2 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v3" select= "MR301/�������������[3]" />
          <xsl:if test="$v3 = 'false' " >��� </xsl:if>
          <xsl:if test="$v3 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v4" select= "MR301/�������������[4]" />
          <xsl:if test="$v4 = 'false' " >��� </xsl:if>
          <xsl:if test="$v4 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v5" select = "MR301/�������������[5]" />
          <xsl:if test="$v5 = 'false' " >��� </xsl:if>
          <xsl:if test="$v5 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v6" select= "MR301/�������������[6]" />
          <xsl:if test="$v6 = 'false' " >��� </xsl:if>
          <xsl:if test="$v6 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v7" select= "MR301/�������������[7]" />
          <xsl:if test="$v7 = 'false' " >��� </xsl:if>
          <xsl:if test="$v7 = 'true' " > ���� </xsl:if>
        </td>
        <td>
          <xsl:variable  name = "v8" select= "MR301/�������������[8]" />
          <xsl:if test="$v8 = 'false' " >��� </xsl:if>
          <xsl:if test="$v8 = 'true' " > ���� </xsl:if>
        </td>
      </tr>
    </table>

    <h1>����������</h1>
    <h2>�������� �������</h2>
    <h3>���</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>������������</th>
        <th>����������</th>
        <th>����� ����������</th>
        <th>����� ����������</th>
        <th>����� 1 �����</th>
        <th>����� 2 �����</th>
        <th>������ �� ��������������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_��������/������������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/�����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/�����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/�����_1_�����_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/�����_2_�����_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/������_���_��_��������������"/>
        </td>
      </tr>
    </table>
    <h3>���</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>����</th>
        <th>����������</th>
        <th>��������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_��������/����_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/��������_x0020_���"/>
        </td>
      </tr>
    </table>
    <h3>���</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>������������</th>
        <th>�������� �������</th>
        <th>����������</th>
        <th>�������</th>
        <th>�������� ��������</th>
        <th>�������� ����. ���.</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_��������/������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/�����_�������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/�������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/��������_��������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/��������_����������_���"/>
        </td>
      </tr>
    </table>
    <h3>����</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>����</th>
        <th>����������</th>
        <th>��������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_��������/����_����"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/����������_����"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_��������/��������_x0020_����"/>
        </td>
      </tr>
    </table>
    <div style="height:120px"> </div>
    <h2>��������� �������</h2>
    <h3>���</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>������������</th>
        <th>����������</th>
        <th>����� ����������</th>
        <th>����� ����������</th>
        <th>����� 1 �����</th>
        <th>����� 2 �����</th>
        <th>������ �� ��������������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_���������/������������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/�����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/�����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/�����_1_�����_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/�����_2_�����_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/������_���_��_��������������"/>
        </td>
      </tr>
    </table>
    <h3>���</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>����</th>
        <th>����������</th>
        <th>��������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_���������/����_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/��������_x0020_���"/>
        </td>
      </tr>
    </table>
    <h3>���</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>������������</th>
        <th>�������� �������</th>
        <th>����������</th>
        <th>�������</th>
        <th>�������� ��������</th>
        <th>�������� ����. ���.</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_���������/������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/�����_�������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/�������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/��������_��������_���"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/��������_����������_���"/>
        </td>
      </tr>
    </table>
    <h3>����</h3>
    <table border="1">
      <tr bgcolor="#c1ced5" align="center">
        <th>����</th>
        <th>����������</th>
        <th>��������</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="MR301/����������_���������/����_����"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/����������_����"/>
        </td>
        <td>
          <xsl:value-of select="MR301/����������_���������/��������_x0020_����"/>
        </td>
      </tr>
    </table>
 	<div style="height:150px"> </div> 
   <h1>������� ������</h1>
		<table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th>�����</th>
         <th>�����</th>
         <th>����������</th>
         <th>����</th>
		 <th>��������</th>
		 <th>���</th>
		 <th>����</th>
      </tr>
      <xsl:for-each select="MR301/ExternalDefensesMain/ExternalDefenseItem">
      <tr align="center">
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@����"/></td>
		 <td><xsl:value-of select="@������������_�����"/></td>
		 <xsl:element name="td">
        <xsl:attribute name="id">apv<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���"/>,"apv<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	  <xsl:element name="td">
        <xsl:attribute name="id">urov<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@����"/>,"urov<xsl:value-of select="position()"/>");</script>
      </xsl:element>
      </tr>
    </xsl:for-each>
    </table>
    
	 <h1>������� ������</h1>
    <h3>������ I></h3>
      <table border="1">
      <tr bgcolor="#c1ced5" align="center">
	     <th> </th>
         <th>�����</th>
         <th>����������</th>
         <th>��� �������</th>
		 <th>�������</th>
		 <th>����-��</th>
		 <th>t����, ��</th>
		 <th>�����.</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���.</th>
		 <th>���.���</th>
		 </tr>
	  <xsl:for-each select="MR301/TokDefensesMain/TokDefenseItem">
      <tr align="center">
	  <xsl:if test="position() &lt; 5">
         <td align="left"><xsl:if test="position() =1">I></xsl:if><xsl:if test="position() =2">I>></xsl:if><xsl:if test="position() =3">I>>></xsl:if><xsl:if test="position() =4">I>>>></xsl:if></td>
         <td><xsl:value-of select="@�����"/></td>
         <td><xsl:value-of select="@����������_�����"/></td>
         <td><xsl:value-of select="@���_x0020_�������"/></td>
         <td><xsl:value-of select="@������������_�������"/></td>
         <td><xsl:value-of select="@��������������"/></td>
         <td><xsl:value-of select="@��������"/></td>
         <td><xsl:value-of select="@�����������"/></td>
	    	 <xsl:element name="td">
       <xsl:attribute name="id">apv_1<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���"/>,"apv_1<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	  <xsl:element name="td">
        <xsl:attribute name="id">urov_1<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@����"/>,"urov_1<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	   <xsl:element name="td">
        <xsl:attribute name="id">usk_1<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���������"/>,"usk_1<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	  <td align="center"><xsl:value-of select="@���������_�����"/></td>
	   </xsl:if>
      </tr>
    </xsl:for-each>
	  </table>
	      
		  <h3>������ I0></h3>
		  <table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th> </th>
         <th>�����</th>
         <th>����������</th>
         <th>��� �������</th>
		 <th>�������</th>
		 <th>t����, ��</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���.</th>
		 <th>���.���</th>
      </tr>
	  <xsl:for-each select="MR301/TokDefensesMain/TokDefenseItem">
      <tr align="center">
	  <xsl:if test="position() &gt; 4">
		<xsl:if test="position() &lt; 7">
         <td align="left"><xsl:if test="position() =5">I0></xsl:if><xsl:if test="position() =6">I0>></xsl:if></td>
      <td><xsl:value-of select="@�����"/></td>
      <td><xsl:value-of select="@����������_�����"/></td>
      <td><xsl:value-of select="@���_x0020_�������"/></td>
		 
		 <xsl:variable name="ust" select="@������������_�������" />
		 <xsl:variable name="del" select="8" />
		 <xsl:if test=" (@���_x0020_������� = '���������') or (@���_x0020_������� = '���������')">
       <td><xsl:value-of select="$ust div $del"/></td>
		 </xsl:if>
		 
		 <xsl:if test=" @���_x0020_������� = '������'">
       <td><xsl:value-of select="@������������_�������"/></td>
		 </xsl:if>
      <td><xsl:value-of select="@��������"/></td>

		 <xsl:element name="td">
        <xsl:attribute name="id">apv_2<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���"/>,"apv_2<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	  <xsl:element name="td">
      <xsl:attribute name="id">urov_2<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@����"/>,"urov_2<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	   <xsl:element name="td">
        <xsl:attribute name="id">usk_2<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���������"/>,"usk_2<xsl:value-of select="position()"/>");</script>
      </xsl:element>
      <td><xsl:value-of select="@���������_�����"/></td>
	   </xsl:if>
	   </xsl:if>
      </tr>
    </xsl:for-each>
	  </table>
		  <h3>������ I2></h3>
		  <table border="1">
      <tr bgcolor="#c1ced5" align="center">
         <th> </th>
         <th>�����</th>
         <th>����������</th>
         <th>��� �������</th>
		 <th>�������</th>
		 <th>t����, ��</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���.</th>
		 <th>���.���</th>
      </tr>
	  <xsl:for-each select="MR301/TokDefensesMain/TokDefenseItem">
      <tr align="center">
	  <xsl:if test="position() &gt; 6">
		<xsl:if test="position() &lt; 9">
		<td align="left"><xsl:if test="position() =7">I2></xsl:if><xsl:if test="position() =8">I2>></xsl:if></td>
      <td><xsl:value-of select="@�����"/></td>
      <td><xsl:value-of select="@����������_�����"/></td>
      <td><xsl:value-of select="@���_x0020_�������"/></td>
      <td><xsl:value-of select="@������������_�������"/></td>
      <td><xsl:value-of select="@��������"/></td>
		 <xsl:element name="td">
        <xsl:attribute name="id">apv_3<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���"/>,"apv_3<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	  <xsl:element name="td">
        <xsl:attribute name="id">urov_3<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@����"/>,"urov_3<xsl:value-of select="position()"/>");</script>
      </xsl:element>
	   <xsl:element name="td">
        <xsl:attribute name="id">usk_3<xsl:value-of select="position()"/></xsl:attribute>            
        <script>translateBoolean(<xsl:value-of select="@���������"/>,"usk_3<xsl:value-of select="position()"/>");</script>
      </xsl:element>
      <td><xsl:value-of select="@���������_�����"/></td>
	   </xsl:if>
	   </xsl:if>
      </tr>
    </xsl:for-each>
	  </table>	  
	 </body>
  </html>
</xsl:template>
</xsl:stylesheet>
