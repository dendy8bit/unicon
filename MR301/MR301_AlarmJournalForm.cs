using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms.Export;
using BEMN.Interfaces;

namespace BEMN.MR301
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        private MR301 _device;

        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR301 device)
        {
            this.InitializeComponent();
            this._device = device;
            device.InputSignalsLoadOK += new Handler(this._device_InputSignalsLoadOK);
            this._device.InputSignalsLoadFail += new Handler(this._device_InputSignalsLoadFail);
            this._device.AlarmJournalSaveOK += new Handler(this._device_AlarmJournalSaveOK);
            this._device.AlarmJournalSaveFail += new Handler(this._device_AlarmJournalSaveFail);
            this._device.AlarmJournalLoadOK += new Handler(this._device_AlarmJournalLoadOK);
            this._device.AlarmJournalLoadFail += new Handler(this._device_AlarmJournalLoadFail);
        }

        

        private void OnAlarmJournalLoad()
        {
            this._readBut.Enabled = true;
            if (this._journalGrid.Rows.Count != 0)
                MessageBox.Show("������ ������ ��������");
            else
                MessageBox.Show("������ ������ ����");
        }

        private void OnAlarmJournalRecordLoadFail()
        {
            this._journalGrid.Rows.Add(new object[] { this._device.AlarmJournalPageIndex,
                                                 "������",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 ""
            });

            this._journalCntLabel.Text = this._device.AlarmJournalPageIndex.ToString();
            this._journalCntLabel.Invalidate();
        }

        private void OnAlarmJournalRecordLoadOk()
        {
            this._journalGrid.Rows.Add(new object[] { this._device.AlarmJournalPageIndex,
                                                 this._device.AlarmJournalRecTime,
                                                 this._device.AlarmJournalMSG,
                                                 this._device.AlarmJournalCODE,
                                                 this._device.AlarmJournalTYPE_VALUE,
                                                 this._device.AlarmJournalIA,
                                                 this._device.AlarmJournalIB,
                                                 this._device.AlarmJournalIC,
                                                 this._device.AlarmJournalIo,
                                                 this._device.AlarmJournalIG,
                                                 this._device.AlarmJournalI0,
                                                 this._device.AlarmJournalI1,
                                                 this._device.AlarmJournalI2,
                                                 this._device.AlarmJournalInSignals1
            });
            this._journalGrid.Invalidate();
            this._journalCntLabel.Text = this._device.AlarmJournalPageIndex.ToString();
            this._journalCntLabel.Invalidate();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR301); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��801_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig");
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.������� 8-1");
            table.Columns.Add("��.������� 9-16");


            List<object> row = new List<object>();

            for (int i = 0; i < this._journalGrid.Rows.Count; i++)
            {
                row.Clear();
                for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                {
                    row.Add(this._journalGrid[j, i].Value);
                }
                table.Rows.Add(row.ToArray());
            }
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }

        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��801_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
            table.Columns.Add("Ig");
            table.Columns.Add("In");
            table.Columns.Add("F");
            table.Columns.Add("Uab");
            table.Columns.Add("Ubc");
            table.Columns.Add("Uca");
            table.Columns.Add("U0");
            table.Columns.Add("U1");
            table.Columns.Add("U2");
            table.Columns.Add("Un");
            table.Columns.Add("��.������� 8-1");
            table.Columns.Add("��.������� 9-16");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._journalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            List<object> row = new List<object>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row.Clear();
                row.AddRange(table.Rows[i].ItemArray);
                this._journalGrid.Rows.Add(row.ToArray());

            }
        }

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.LoadInputSignals();
                this._readBut.Enabled = false;
                this._journalGrid.Rows.Clear();
            }
        }

        private void OnLoad() 
        {
            this._readBut.Enabled = true;
        }

        void _device_InputSignalsLoadFail(object sender)
        {
            this._device.InputSignalsLoadFail -= new Handler(this._device_InputSignalsLoadFail);
            MessageBox.Show("������ ������� �������� �� ���������.", "");
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoad));
            }
            catch { }
        }

        void _device_InputSignalsLoadOK(object sender)
        {
            this._device.InputSignalsLoadOK -= this._device_InputSignalsLoadOK;
            if (DialogResult.Yes == MessageBox.Show("������ ������� �������� ���������.\n\r������ ������ ������?", "", MessageBoxButtons.YesNo))
            {
                this._device.SaveAlarmJournalIndex();
            }
            else 
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnLoad));
                }
                catch { }
            }
        }

        void _device_AlarmJournalLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAlarmJournalRecordLoadFail));
            }
            catch { }

            this._device.LoadAlarmJournal();
        }

        void _device_AlarmJournalLoadOK(object sender)
        {
            if (this._device.AlarmJournalRecTime != "FAIL")
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnAlarmJournalRecordLoadOk));
                }
                catch { }

                this._device.LoadAlarmJournal();
            }
            else
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnAlarmJournalLoad));
                }
                catch { }
            }
        }

        void _device_AlarmJournalSaveFail(object sender)
        {
            MessageBox.Show("������ � ������ ������� ������", "");
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoad));
            }
            catch { }
        }

        void _device_AlarmJournalSaveOK(object sender)
        {
            this._device.LoadAlarmJournal();
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                DataTable table = new DataTable("��801_������_������");
                table.Columns.Add("�����");
                table.Columns.Add("�����");
                table.Columns.Add("���������");
                table.Columns.Add("���_�����������");
                table.Columns.Add("���_�����������");
                table.Columns.Add("Ia");
                table.Columns.Add("Ib");
                table.Columns.Add("Ic");
                table.Columns.Add("I0");
                table.Columns.Add("I1");
                table.Columns.Add("I2");
                table.Columns.Add("Ig");
                table.Columns.Add("In");
                table.Columns.Add("F");
                table.Columns.Add("Uab");
                table.Columns.Add("Ubc");
                table.Columns.Add("Uca");
                table.Columns.Add("U0");
                table.Columns.Add("U1");
                table.Columns.Add("U2");
                table.Columns.Add("Un");
                table.Columns.Add("��1");
                table.Columns.Add("��2");


                List<object> row = new List<object>();

                for (int i = 0; i < this._journalGrid.Rows.Count; i++)
                {
                    row.Clear();
                    for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                    {
                        row.Add(this._journalGrid[j, i].Value);
                    }
                    table.Rows.Add(row.ToArray());
                }

                HtmlExport.Export(table, this._saveJournalHtmlDialog.FileName, Resources.MR301AJ);
            }
        }
    }
}
