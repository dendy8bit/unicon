using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR301.SystemJournal;
using BEMN.MR301.SystemJournal.Structures;
using CategoryAttribute=System.ComponentModel.CategoryAttribute;

namespace BEMN.MR301
{
    public class MR301 : Device, IDeviceView, IDeviceVersion
    {
        private MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<DateTimeStruct> _dateTime;

        #region ������ ������

        public const int ALARMJOURNAL_RECORD_CNT = 32;
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];
        private CAlarmJournal _alarmJournalRecords;
        private bool _stopAlarmJournal = false;

        [Browsable(false)]
        public CAlarmJournal AlarmJournal
        {
            get { return _alarmJournalRecords; }
            set { _alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type_value;
            public string Ia;
            public string Ib;
            public string Ic;
            public string Io;
            public string Ig;
            public string I0;
            public string I1;
            public string I2;
            public string inSignals1;
            public string inSignals2;
        } ;

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _codeDictionary;
            private Dictionary<byte, string> _typeDictionary;
            private MR301 _device;

            public CAlarmJournal()
            {

            }

            public CAlarmJournal(MR301 device)
            {
                _device = device;
                _msgDictionary = new Dictionary<ushort, string>(6);
                _codeDictionary = new Dictionary<byte, string>(19);
                _typeDictionary = new Dictionary<byte, string>(9);

                _msgDictionary.Add(0, "������ ����");
                _msgDictionary.Add(1, "������������");
                _msgDictionary.Add(2, "����������");
                _msgDictionary.Add(3, "����������");
                _msgDictionary.Add(4, "���������� ���");
                _msgDictionary.Add(118, "������ ����");
                _msgDictionary.Add(147, "������ ����");
                _msgDictionary.Add(255, "��� ���������");
                _msgDictionary.Add(84, "��� ���������");

                _codeDictionary.Add(1, "��-1");
                _codeDictionary.Add(2, "��-2");
                _codeDictionary.Add(3, "��-3");
                _codeDictionary.Add(4, "��-4");
                _codeDictionary.Add(5, "��-5");
                _codeDictionary.Add(6, "��-6");
                _codeDictionary.Add(7, "��-7");
                _codeDictionary.Add(8, "��-8");
                _codeDictionary.Add(9, "I>");
                _codeDictionary.Add(10, "I>>");
                _codeDictionary.Add(11, "I>>>");
                _codeDictionary.Add(12, "I>>>>");
                _codeDictionary.Add(13, "I0>");
                _codeDictionary.Add(14, "I0>>");
                _codeDictionary.Add(15, "I2>");
                _codeDictionary.Add(16, "I2>>");
                _codeDictionary.Add(17, "���");
                _codeDictionary.Add(18, "���");


                _typeDictionary.Add(0, "��");
                _typeDictionary.Add(1, "Ig");
                _typeDictionary.Add(2, "Io");
                _typeDictionary.Add(3, "Ia");
                _typeDictionary.Add(4, "Ib");
                _typeDictionary.Add(5, "Ic");
                _typeDictionary.Add(6, "I0");
                _typeDictionary.Add(7, "I1");
                _typeDictionary.Add(8, "I2");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);

            public int Count
            {
                get { return _messages.Count; }
            }

            public AlarmRecord this[int i]
            {
                get { return _messages[i]; }
            }

            public bool IsMessageEmpty(int i)
            {
                return "������ ����" == _messages[i].msg;
            }

            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = CreateMessage(value);
                if ("�����" == msg.msg)
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    _messages.Add(msg);
                }
                return ret;
            }

            public string GetMessage(byte b)
            {
                try
                {
                    return _msgDictionary[b];
                }
                catch (KeyNotFoundException) 
                {
                    return _msgDictionary[0];
                }
                
            }

            public void d()
            {
                
            }

            public string GetDateTime(byte[] datetime)
            {
                return
                    datetime[4].ToString("d2") + "-" + datetime[2].ToString("d2") + "-" + datetime[0].ToString("d2") +
                    " " +
                    datetime[6].ToString("d2") + ":" + datetime[8].ToString("d2") + ":" + datetime[10].ToString("d2") +
                           ":" + datetime[12].ToString("d2");
            }

            public string GetDateTimeOSC(byte[] datetime)
            {
                return
                    datetime[2].ToString("d2") + "-" + datetime[1].ToString("d2") + "-" + datetime[0].ToString("d2") +
                    " " +
                    datetime[3].ToString("d2") + ":" + datetime[4].ToString("d2") + ":" + datetime[5].ToString("d2") +
                           ":" + datetime[6].ToString("d2");
            }

            public string GetDateTimeINT(int[] datetime)
            {
                return
                    datetime[2].ToString("d2") + "-" + datetime[1].ToString("d2") + "-" + datetime[0].ToString("d2") +
                    " " +
                    datetime[3].ToString("d2") + ":" + datetime[4].ToString("d2") + ":" + datetime[5].ToString("d2") +
                           ":" + datetime[6].ToString("d2");
            }

            public string GetCode(byte codeByte, byte phaseByte)
            {

                string group = (0 == (codeByte & 0x80)) ? "��������" : "������.";
                string phase = "";
                phase += (0 == (phaseByte & 0x08)) ? " " : "_";
                phase += (0 == (phaseByte & 0x01)) ? " " : "A";
                phase += (0 == (phaseByte & 0x02)) ? " " : "B";
                phase += (0 == (phaseByte & 0x04)) ? " " : "C";

                byte code = (byte)(codeByte & 0x7F);
                try
                {
                    return _codeDictionary[code] + " " + group + " " + phase;
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }

            }

            public string GetTypeValue(ushort damageWord, byte typeByte)
            {
                double damageValue = Measuring.GetI(damageWord, _device.TT, true);
                try
                {
                    return _typeDictionary[typeByte] + String.Format(" = {0:F2}", damageValue);
                }
                catch (KeyNotFoundException)
                {
                    return "";
                }
            }

            private AlarmRecord CreateMessage(byte[] buffer)
            {
                AlarmRecord rec = new AlarmRecord();
                rec.msg = GetMessage(buffer[0]);

                byte[] datetime = new byte[14];
                Array.ConstrainedCopy(buffer, 2, datetime, 0, 14);
                rec.time = GetDateTime(datetime);
                                
                rec.code = GetCode(buffer[16], buffer[18]);
                rec.type_value = GetTypeValue(Common.TOWORD(buffer[21], buffer[20]), buffer[19]);

                double Ia = Measuring.GetI(Common.TOWORD(buffer[23], buffer[22]), _device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[25], buffer[24]), _device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), _device.TT, true);
                double Io = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), _device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), _device.TTNP, false);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[33], buffer[32]), _device.TT, true);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[35], buffer[34]), _device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[37], buffer[36]), _device.TT, true);


                rec.Ia = String.Format("{0:F2}", Ia);
                rec.Ib = String.Format("{0:F2}", Ib);
                rec.Ic = String.Format("{0:F2}", Ic);
                rec.Io = String.Format("{0:F2}", Io);
                rec.Ig = String.Format("{0:F2}", Ig);
                rec.I0 = String.Format("{0:F2}", I0);
                rec.I1 = String.Format("{0:F2}", I1);
                rec.I2 = String.Format("{0:F2}", I2);
                
                

                byte[] b1 = new byte[] {buffer[38]};
                byte[] b2 = new byte[] {buffer[39]};
                rec.inSignals1 = Common.BitsToString(new BitArray(b1));
                rec.inSignals2 = Common.BitsToString(new BitArray(b2));
                return rec;
            }
        }

        #endregion

        #region ��������� ������ �����
        
        public MemoryEntity<OneWordStruct> RefreshSystemJournal
        {
            get { return this._refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }
        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }
        
        #endregion

        #region ������ ������ �����

        private Dictionary<ushort, string> _msgDictionary;
        private Dictionary<byte, string> _codeDictionary;
        private Dictionary<byte, string> _typeDictionary;
        byte[] _AlarmBuffer;

        public string GetMessage(byte b)
        {
            try
            {
                return _msgDictionary[b];
            }
            catch (KeyNotFoundException)
            {
                return _msgDictionary[0];
            }

        }

        public string GetCode(byte codeByte, byte phaseByte)
        {

            string group = (0 == (codeByte & 0x80)) ? "��������" : "������.";
            string phase = "";
            phase += (0 == (phaseByte & 0x08)) ? " " : "_";
            phase += (0 == (phaseByte & 0x01)) ? " " : "A";
            phase += (0 == (phaseByte & 0x02)) ? " " : "B";
            phase += (0 == (phaseByte & 0x04)) ? " " : "C";

            byte code = (byte)(codeByte & 0x7F);
            try
            {
                return _codeDictionary[code] + " " + group + " " + phase;
            }
            catch (KeyNotFoundException)
            {
                return "";
            }

        }

        public string GetTypeValue(ushort damageWord, byte typeByte)
        {
            double damageValue = Measuring.GetI(damageWord, this.TT, true);
            try
            {
                return _typeDictionary[typeByte] + String.Format(" = {0:F2}", damageValue);
            }
            catch (KeyNotFoundException)
            {
                return "";
            }
        }

        public string ConvertTime(string startTime)
        {
            string res = startTime;

            if (startTime.Length < 2)
            {
                res = "0" + startTime;
            }

            return res;
        }

        public int AlarmJournalPageIndex
        {
            get
            {
                return _alarmJournalRec;
            }
        }

        public string AlarmJournalRecTime
        {
            get
            {
                string _time = "FAIL";
                int timeVal = _alarmJournalRead.Value[1] + _alarmJournalRead.Value[2] + _alarmJournalRead.Value[3] + _alarmJournalRead.Value[4] + _alarmJournalRead.Value[5] +
                    _alarmJournalRead.Value[6] + _alarmJournalRead.Value[7];

                if (timeVal != 0)
                {
                    _time = ConvertTime(_alarmJournalRead.Value[3].ToString()) + "-" + ConvertTime(_alarmJournalRead.Value[2].ToString()) + "-" + ConvertTime(_alarmJournalRead.Value[1].ToString()) + ","
                           + ConvertTime(_alarmJournalRead.Value[4].ToString()) + ":" + ConvertTime(_alarmJournalRead.Value[5].ToString()) + ":" + ConvertTime(_alarmJournalRead.Value[6].ToString()) + "."
                           + ConvertTime(_alarmJournalRead.Value[7].ToString());
                }
                _AlarmBuffer = Common.TOBYTES(_alarmJournalRead.Value, false);
                return _time;
            }
        }

        public string AlarmJournalMSG
        {
            get
            {
                return GetMessage(_AlarmBuffer[0]);
            }
        }

        public string AlarmJournalCODE
        {
            get
            {
                return GetCode(_AlarmBuffer[16], _AlarmBuffer[18]);
            }
        }

        public string AlarmJournalTYPE_VALUE
        {
            get
            {
                return GetTypeValue(Common.TOWORD(_AlarmBuffer[21], _AlarmBuffer[20]), _AlarmBuffer[19]);
            }
        }

        public string AlarmJournalIA
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[23], _AlarmBuffer[22]);
                double Ia = Measuring.GetI(val , this.TT, true);
                return String.Format("{0:F2}", Ia);
            }
        }


        public string AlarmJournalIB
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[25], _AlarmBuffer[24]);
                double Ib = Measuring.GetI(val , this.TT, true);
                return String.Format("{0:F2}", Ib);
            }
        }

        public string AlarmJournalIC
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[27], _AlarmBuffer[26]);
                double Ic = Measuring.GetI(val , this.TT, true);
                return String.Format("{0:F2}", Ic);
            }
        }

        public string AlarmJournalIo
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[29], _AlarmBuffer[28]);
                double Io = Measuring.GetI(val , this.TTNP, false);
                return String.Format("{0:F2}", Io);
            }
        }

        public string AlarmJournalIG
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[31], _AlarmBuffer[30]);
                double Ig = Measuring.GetI(val , this.TTNP, false);
                return String.Format("{0:F2}", Ig);
            }
        }

        public string AlarmJournalI0
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[33], _AlarmBuffer[32]);
                double I0 = Measuring.GetI(val , this.TT, true);
                return String.Format("{0:F2}", I0);
            }
        }

        public string AlarmJournalI1
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[35], _AlarmBuffer[34]);
                double I1 = Measuring.GetI(val , this.TT, true);
                return String.Format("{0:F2}", I1);
            }
        }

        public string AlarmJournalI2
        {
            get
            {
                ushort val = Common.TOWORD(_AlarmBuffer[37], _AlarmBuffer[36]);
                double I2 = Measuring.GetI(val , this.TT, true);
                return String.Format("{0:F2}", I2);
            }
        }

        public string AlarmJournalInSignals1
        {
            get
            {
                byte[] b1 = new byte[] { _AlarmBuffer[38] };
                return Common.BitsToString(new BitArray(b1));
            }
        }

        public string AlarmJournalInSignals2
        {
            get
            {
                byte[] b2 = new byte[] { _AlarmBuffer[39] };
                return Common.BitsToString(new BitArray(b2));
            }
        }
        #endregion

        #region ������� �������

        private slot _inputSignals = new slot(0x1100, 0x1120);

        public void LoadInputSignals()
        {
            LoadSlot(DeviceNumber, _inputSignals, "LoadInputSignals" + DeviceNumber, this);
        }

        public void SaveInputSignals()
        {
            SaveSlot(DeviceNumber, _inputSignals, "SaveInputSignals" + DeviceNumber, this);
        }

        #region ��������� ���� � ����������

        [XmlElement("��")]
        [DisplayName("��")]
        [Description("��������� ��� ��������������")]
        [CategoryAttribute("��������� ���� � ����������")]
        public ushort TT
        {
            get { return _inputSignals.Value[0]; }
            set { _inputSignals.Value[0] = value; }
        }

        [XmlElement("����")]
        [DisplayName("����")]
        [Description("��������� ��� �������������� ������� ������������������")]
        [Category("��������� ���� � ����������")]
        public ushort TTNP
        {
            get { return _inputSignals.Value[1]; }
            set { _inputSignals.Value[1] = value; }
        }

        [XmlElement("������������_���")]
        [DisplayName("������������ ���")]
        [Description("������������ ��� ��������")]
        [Category("��������� ���� � ����������")]
        public double MaxTok
        {
            get { return Measuring.GetConstraint(_inputSignals.Value[3], ConstraintKoefficient.K_4000); }
            set { _inputSignals.Value[3] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        #endregion

        #region ��������� �����

        [XmlElement("�����_�����_���������")]
        [DisplayName("����� ����� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SpeedupNumber
        {
            get
            {
                ushort index = _inputSignals.Value[4];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }
                return Strings.Logic[index];
            }
            set { _inputSignals.Value[4] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("���������_��_���������")]
        [DisplayName("��������� �� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (ForbiddenTypeConverter))]
        public string SpeedupOn
        {
            get
            {
                ushort index = _inputSignals.Value[5];

                if (Strings.Forbidden.Count <= index)
                {
                    index = (ushort)(Strings.Forbidden.Count - 1);
                }
                return Strings.Forbidden[index];
            }
            set
            {
                _inputSignals.Value[5] = (ushort)(Strings.Forbidden.IndexOf(value));
            }
        }

        [XmlElement("������������_���������")]
        [DisplayName("������������ ���������")]
        [Description("������������ ��������� ������, ��")]
        [Category("������� �������")]
        [TypeConverter(typeof (ForbiddenTypeConverter))]
        public ulong SpeedupTime
        {
            get { return Measuring.GetTime(_inputSignals.Value[6]); }
            set { _inputSignals.Value[6] = Measuring.SetTime(value); }
        }

        #endregion

        #region ������� �������
        [XmlElement("����������_����")]
        [DisplayName("����������_����")]
        [Description("����� ����� ����� ��������")]
        [Category("������� �������")]
        [TypeConverter(typeof(LogicTypeConverter))]
        public string BlockStdu
        {
            get
            {
                ushort index = _inputSignals.Value[7];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort)(Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[7] = (ushort)(Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("����_���������")]
        [DisplayName("���� ���������")]
        [Description("����� ����� ����� ��������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string KeyOff
        {
            get
            {
                ushort index = _inputSignals.Value[8];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[8] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("����_��������")]
        [DisplayName("���� ��������")]
        [Description("����� ����� ����� ��������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string KeyOn
        {
            get
            {
                ushort index = _inputSignals.Value[9];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[9] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������_���������")]
        [DisplayName("������� ���������")]
        [Description("����� ����� ������� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ExternalOff
        {
            get
            {
                ushort index = _inputSignals.Value[10];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[10] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�������_��������")]
        [DisplayName("������� ��������")]
        [Description("����� ����� ������� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ExternalOn
        {
            get
            {
                ushort index = _inputSignals.Value[11];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[ index];
            }
            set { _inputSignals.Value[11] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("������������_�������")]
        [DisplayName("������������ �������")]
        [Description("������� ������ ������ �������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string ConstraintGroup
        {
            get
            {
                ushort index = _inputSignals.Value[12];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[12] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����_���������")]
        [DisplayName("����� ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string IndicationReset
        {
            get
            {
                ushort index = _inputSignals.Value[13];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _inputSignals.Value[13] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        #endregion

        #region ������� ����������

        [XmlElement("������_��_������")]
        [DisplayName("������ �� ������")]
        [Category("������� �������")]
        [Description("����������� ������ �� ������")]
        [TypeConverter(typeof (ForbiddenTypeConverter))]
        public string ManageSignalButton
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 0) ? Strings.Forbidden[1] : Strings.Forbidden[0];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? false : true;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 0, bit);
            }
        }

        [XmlElement("������_��_�����")]
        [DisplayName("������ �� �����")]
        [Category("������� �������")]
        [Description("����������� ������ �� �����")]
        [TypeConverter(typeof (ControlTypeConverter))]
        public string ManageSignalKey
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 1) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 1, bit);
            }
        }

        [XmlElement("������_��_��������")]
        [DisplayName("������ �� ��������")]
        [Category("������� �������")]
        [Description("����������� ������ �� �������� ����������")]
        [TypeConverter(typeof (ControlTypeConverter))]
        public string ManageSignalExternal
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 2) ? Strings.Control[0] : Strings.Control[1];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Control[0]) ? true : false;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 2, bit);
            }
        }

        [XmlElement("������_��_����")]
        [DisplayName("������ �� ����")]
        [Category("������� �������")]
        [Description("����������� ������ �� ����")]
        [TypeConverter(typeof (ForbiddenTypeConverter))]
        public string ManageSignalSDTU
        {
            get
            {
                string ret = Common.GetBit(_inputSignals.Value[15], 3) ? Strings.Forbidden[1] : Strings.Forbidden[0];
                return ret;
            }
            set
            {
                bool bit = (value == Strings.Forbidden[0]) ? false : true;
                _inputSignals.Value[15] = Common.SetBit(_inputSignals.Value[15], 3, bit);
            }
        }

        #endregion

        #region ������� ���������� �������

        public const int INPUT_LOGIC_OFFSET = 0x10;

        public class LogicSignalUnitDataTransfer
        {
            [XmlElement("�������")] public List<string> Value = new List<string>();

            public void FillValues(LogicState[] logicStates)
            {
                this.Value.Clear();
                foreach (var logicState in logicStates)
                {                   
                        this.Value.Add(logicState.ToString());                    
                }
            }

            public void FillOutValuesFromBitArray(BitArray bitArray)
            {
                this.Value.Clear();
                for (int bitNum = 0; bitNum < bitArray.Count; bitNum++)
                {                    
                    if (bitArray[bitNum])
                    {                        
                            this.Value.Add(Strings.OutputSignals[bitNum]);                        
                    }                    
                }
            }

        }

        public class InputLogicSignalDataTransfer
        {
            [XmlElement("�1")]
            public LogicSignalUnitDataTransfer And1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�2")]
            public LogicSignalUnitDataTransfer And2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�3")]
            public LogicSignalUnitDataTransfer And3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("�4")]
            public LogicSignalUnitDataTransfer And4Signal = new LogicSignalUnitDataTransfer();


            [XmlElement("���1")]
            public LogicSignalUnitDataTransfer Or1Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���2")]
            public LogicSignalUnitDataTransfer Or2Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���3")]
            public LogicSignalUnitDataTransfer Or3Signal = new LogicSignalUnitDataTransfer();
            [XmlElement("���4")]
            public LogicSignalUnitDataTransfer Or4Signal = new LogicSignalUnitDataTransfer();

        }

        [XmlElement("�������_����������_�������")]
        public InputLogicSignalDataTransfer InputLogicSignalStruct
        {
            get
            {
                InputLogicSignalDataTransfer toRet = new InputLogicSignalDataTransfer();
                toRet.And1Signal.FillValues(this.GetInputLogicSignals(0));
                toRet.And2Signal.FillValues(this.GetInputLogicSignals(1));
                toRet.And3Signal.FillValues(this.GetInputLogicSignals(2));
                toRet.And4Signal.FillValues(this.GetInputLogicSignals(3));
                toRet.Or1Signal.FillValues(this.GetInputLogicSignals(4));
                toRet.Or2Signal.FillValues(this.GetInputLogicSignals(5));
                toRet.Or3Signal.FillValues(this.GetInputLogicSignals(6));
                toRet.Or4Signal.FillValues(this.GetInputLogicSignals(7));
                return toRet;

            }
            set { }
        }

        public LogicState[] GetInputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            LogicState[] ret = new LogicState[16];
            SetLogicStates(_inputSignals.Value[INPUT_LOGIC_OFFSET + channel * 2], true, ref ret);
            SetLogicStates(_inputSignals.Value[INPUT_LOGIC_OFFSET + channel * 2 + 1], false, ref ret);
            return ret;
        }

        public void SetInputLogicSignals(int channel, LogicState[] logicSignals)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            ushort firstWord = 0;
            ushort secondWord = 0;

            for (int i = 0; i < logicSignals.Length; i++)
            {
                if (i < 8)
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        firstWord += (ushort)Math.Pow(2, i);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        firstWord += (ushort)Math.Pow(2, i);
                        firstWord += (ushort)Math.Pow(2, i + 8);
                    }
                }
                else
                {
                    if (LogicState.�� == logicSignals[i])
                    {
                        secondWord += (ushort)Math.Pow(2, i - 8);
                    }
                    if (LogicState.������ == logicSignals[i])
                    {
                        secondWord += (ushort)Math.Pow(2, i - 8);
                        secondWord += (ushort)Math.Pow(2, i);
                    }
                }
            }
            _inputSignals.Value[INPUT_LOGIC_OFFSET + channel * 2] = firstWord;
            _inputSignals.Value[INPUT_LOGIC_OFFSET + channel * 2 + 1] = secondWord;
        }

        private static void SetLogicStates(ushort value, bool firstWord, ref LogicState[] logicArray)
        {
            byte lowByte = Common.LOBYTE(value);
            byte hiByte = Common.HIBYTE(value);

            int start = firstWord ? 0 : 8;
            int end = firstWord ? 8 : 16;

            for (int i = start; i < end; i++)
            {
                int pow = firstWord ? i : i - 8;
                logicArray[i] = 0 == ((lowByte) & (byte)(Math.Pow(2, pow))) ? logicArray[i] : LogicState.��;
                logicArray[i] = IsLogicEquals(hiByte, pow) && (logicArray[i] == LogicState.��)
                                    ? LogicState.������
                                    : logicArray[i];
            }
        }

        private static bool IsLogicEquals(byte hiByte, int pow)
        {
            return (byte)(Math.Pow(2, pow)) == ((hiByte) & (byte)(Math.Pow(2, pow)));
        }

        #endregion

        #endregion

        #region �������� �������

        private slot _outputSignals = new slot(0x1120, 0x1140);
        private slot _outputLogic = new slot(0x1150, 0x1178);


         public string[][] VlsXml
        {
            get
            {
                List<bool[]> allVLS = new List<bool[]>();
                for (int i = 0; i < 8; i++)
                {
                    BitArray array = this.GetOutputLogicSignals(i);
                    allVLS.Add(array.Cast<bool>().ToArray());
                }
                string[][] result = new string[8][];
                for (int i = 0; i < 8; i++)
                {
                    result[i] = Strings.OutputSignals.Where((t, j) => allVLS[i][j]).ToArray();
                }
                return result;
            }
            set { }
        }

       


        public void LoadOutputSignals()
        {
            LoadSlot(DeviceNumber, _outputLogic, "LoadOutputLogic" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _outputSignals, "LoadOutputSignals" + DeviceNumber, this);
        }

        public void SaveOutputSignals()
        {
            Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, COutputRele.OFFSET, COutputRele.LENGTH);
            Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, COutputIndicator.OFFSET,
                                  COutputIndicator.LENGTH);

            SaveSlot(DeviceNumber, _outputSignals, "SaveOutputSignals" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _outputLogic, "SaveOutputLogicSignals" + DeviceNumber, this);
        }

        #region ���� �������������

        [XmlIgnore]
        [DisplayName("���� ������������� ������������")]
        [Category("�������� �������")]
        [Editor(typeof (BitsEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray DispepairReleConfiguration
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_outputSignals.Value[0])}); }
            set { _outputSignals.Value[0] = Common.BitsToUshort(value); }
        }

        [XmlElement("����_�������������_������������")]
        [DisplayName("���� ������������� - ������������")]
        [Category("������� �������")]
        public ulong DispepairReleDuration
        {
            get { return Measuring.GetTime(_outputSignals.Value[1]); }
            set { _outputSignals.Value[1] = Measuring.SetTime(value); }
        }

        #endregion

        #region �����������

        [XmlElement("�����������_���������")]
        [DisplayName("����������� - ����������")]
        [Description("����������� - ��������� ����������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherOff
        {
            get
            {
                ushort index = _outputSignals.Value[2];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[2] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_��������")]
        [DisplayName("����������� - ���������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherOn
        {
            get
            {
                ushort index = _outputSignals.Value[3];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[3] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_������")]
        [DisplayName("����������� - �������������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherError
        {
            get
            {
                ushort index = _outputSignals.Value[4];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[4] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_����������")]
        [DisplayName("����������� - ����������")]
        [Category("������� �������")]
        [TypeConverter(typeof (LogicTypeConverter))]
        public string SwitcherBlock
        {
            get
            {
                ushort index = _outputSignals.Value[5];

                if (Strings.Logic.Count <= index)
                {
                    index = (ushort) (Strings.Logic.Count - 1);
                }

                return Strings.Logic[index];
            }
            set { _outputSignals.Value[5] = (ushort) (Strings.Logic.IndexOf(value)); }
        }

        [XmlElement("�����������_�����")]
        [DisplayName("����������� - ����� ����������")]
        [Category("������� �������")]
        public ulong SwitcherTime
        {
            get { return Measuring.GetTime(_outputSignals.Value[6]); }
            set { _outputSignals.Value[6] = Measuring.SetTime(value); }
        }

        [XmlElement("�����������_�������")]
        [DisplayName("����������� - �������")]
        [Category("������� �������")]
        public ulong SwitcherImpulse
        {
            get { return Measuring.GetTime(_outputSignals.Value[7]); }
            set { _outputSignals.Value[7] = Measuring.SetTime(value); }
        }

        #endregion

        #region �������� ����

        public class COutputRele : ICollection
        {
            public const int LENGTH = 16;
            public const int COUNT = 8;
            public const int OFFSET = 16;

            public COutputRele()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("����������")]
            public int Count
            {
                get { return _releList.Count; }
            }

            public void SetBuffer(ushort[] values)
            {
                _releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", LENGTH,
                                                          "Output rele length must be 10");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _releList.Add(new OutputReleItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = _releList[i].Value[0];
                    ret[i*2 + 1] = _releList[i].Value[1];
                }
                return ret;
            }


            public OutputReleItem this[int i]
            {
                get { return _releList[i]; }
                set { _releList[i] = value; }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                _releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _releList.GetEnumerator();
            }

            #endregion
        } ;

        public class OutputReleItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ����";
            }

            //[XmlAttribute("��������")]
            //[DisplayName("��������")]
            //[Description("������ � ������� ����")]
            //[Category("������� ���")]
            //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Value
            {
                get { return new ushort[] {_hiWord, _loWord}; }
                set
                {
                    _hiWord = value[0];
                    _loWord = value[1];
                }
            }

            public OutputReleItem()
            {
            }

            public OutputReleItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get { return 0 != (_hiWord & 256); }
                set
                {
                    if (value)
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                    }
                    else
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                        _hiWord -= 256;
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlinkerTypeConverter))]
            public String Type
            {
                get
                {
                    //string ret = "";
                    //ret = IsBlinker ? "�������" : "�����������";
                    return IsBlinker ? "�������" : "�����������";
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte) Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Description("������������ �������� ����")]
            [Category("��������� ���")]
            public ulong ImpulseUlong
            {
                get { return Measuring.GetTime(_loWord); }
                set { _loWord = Measuring.SetTime(value); }
            }

            [XmlIgnore]
            [Browsable(false)]
            public ushort ImpulseUshort
            {
                get { return _loWord; }
                set { _loWord = value; }
            }
        } ;

     

        //[DisplayName("�������� ����")]
        //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        //[TypeConverter(typeof (RussianExpandableObjectConverter))]
        //[Category("�������� �������")]        
       // [XmlElement("��������_����")]
        public COutputRele OutputRele
        {
            get { return _outputRele; }
        }

        #endregion

        [XmlIgnore]
        public BitArray DispepairSignal
        {
            get { return new BitArray(new byte[] { Common.LOBYTE(_outputSignals.Value[0]) }); }
            set { _outputSignals.Value[0] = Common.BitsToUshort(value); }
        }

        [XmlElement("�������������")]
        public bool[] DispepairSignalXml
        {
            get { return this.DispepairSignal.Cast<bool>().ToArray(); }
            set { }
        }

        #region �������� ����������

        public class COutputIndicator : ICollection
        {
            #region ICollection Members

            public void Add(OutputIndicatorItem item)
            {
                _indList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _indList.GetEnumerator();
            }

            #endregion

            [DisplayName("����������")]
            public int Count
            {
                get { return _indList.Count; }
            }


            public const int LENGTH = 8;
            public const int COUNT = 4;
            public const int OFFSET = 8;

            private List<OutputIndicatorItem> _indList = new List<OutputIndicatorItem>(COUNT);

            public COutputIndicator()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public void SetBuffer(ushort[] values)
            {
                _indList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Output Indicator values",  LENGTH,
                                                          "Output indicator length must be 16");
                }
                for (int i = 0; i < LENGTH; i += 2)
                {
                    _indList.Add(new OutputIndicatorItem(values[i], values[i + 1]));
                }
            }

            public ushort[] ToUshort()
            {
                ushort[] ret = new ushort[LENGTH];
                for (int i = 0; i < COUNT; i++)
                {
                    ret[i*2] = _indList[i].Value[0];
                    ret[i*2 + 1] = _indList[i].Value[1];
                }
                return ret;
            }

            public OutputIndicatorItem this[int i]
            {
                get { return _indList[i]; }
                set { _indList[i] = value; }
            }
        } ;

        public class OutputIndicatorItem
        {
            private ushort _hiWord;
            private ushort _loWord;

            public override string ToString()
            {
                return "�������� ���������";
            }

            //[XmlAttribute("��������")]
            //[DisplayName("��������")]
            //[Description("������ � ������� ����")]
            //[Category("������� ���")]
            //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Value
            {
                get { return new ushort[] {_hiWord, _loWord}; }
            }

            public OutputIndicatorItem()
            {
            }

            public OutputIndicatorItem(ushort hiWord, ushort loWord)
            {
                SetItem(hiWord, loWord);
            }

            public void SetItem(ushort hiWord, ushort loWord)
            {
                _hiWord = hiWord;
                _loWord = loWord;
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsBlinker
            {
                get { return 0 != (_hiWord & 256); }
                set
                {
                    if (value)
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                    }
                    else
                    {
                        _hiWord = (ushort) (_hiWord | 256);
                        _hiWord -= 256;
                    }
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BlinkerTypeConverter))]
            public String Type
            {
                get
                {
                    //string ret = "";
                    //ret = IsBlinker ? "�������" : "�����������";
                    return IsBlinker ? "�������" : "�����������";
                }
                set
                {
                    if ("�������" == value)
                    {
                        IsBlinker = true;
                    }
                    else if ("�����������" == value)
                    {
                        IsBlinker = false;
                    }
                    else
                    {
                        throw new ArgumentException("Rele type undefined", "Type");
                    }
                }
            }

            [XmlIgnore]
            [Browsable(false)]
            public byte SignalByte
            {
                get { return Common.LOBYTE(_hiWord); }
                set { _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), value); }
            }

            [XmlAttribute("������")]
            [DisplayName("���")]
            [Description("��� ������� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (AllSignalsTypeConverter))]
            public string SignalString
            {
                get
                {
                    string ret = Strings.All[Strings.All.Count - 1];
                    if (Common.LOBYTE(_hiWord) <= Strings.All.Count)
                    {
                        ret = Strings.All[Common.LOBYTE(_hiWord)];
                    }
                    return ret;
                }
                set
                {
                    byte index = (byte) Strings.All.IndexOf(value);
                    _hiWord = Common.TOWORD(Common.HIBYTE(_hiWord), index);
                }
            }

            [XmlAttribute("���������")]
            [DisplayName("������ ���������")]
            [Description("������ '����� ���������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Indication
            {
                get { return 0 != (_loWord & 0x1); }
                set { _loWord = value ? (ushort) (_loWord | 0x1) : (ushort) (_loWord & ~0x1); }
            }

            [XmlAttribute("������")]
            [DisplayName("�. ������")]
            [Description("������ '����� ������� ������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Alarm
            {
                get { return 0 != (_loWord & 0x2); }
                set { _loWord = value ? (ushort) (_loWord | 0x2) : (ushort) (_loWord & ~0x2); }
            }

            [XmlAttribute("�������")]
            [DisplayName("�. �������")]
            [Description("������ '����� ������� �������'")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool System
            {
                get { return 0 != (_loWord & 0x4); }
                set { _loWord = value ? (ushort) (_loWord | 0x4) : (ushort) (_loWord & ~0x4); }
            }
        } ;

        private COutputIndicator _outputIndicator = new COutputIndicator();

        //[XmlElement("��������_����������")]
        //[DisplayName("�������� ����������")]
        //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        //[TypeConverter(typeof (RussianExpandableObjectConverter))]
        //[Category("�������� �������")]
        public COutputIndicator OutputIndicator
        {
            get { return _outputIndicator; }
        }

        #endregion



        #region ���������� �������

        public BitArray GetOutputLogicSignals(int channel)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }

            ushort[] logicBuffer = new ushort[5];
            Array.ConstrainedCopy(_outputLogic.Value, channel*5, logicBuffer, 0, 5);
            BitArray buffer = new BitArray(Common.TOBYTES(logicBuffer, false));
            BitArray ret = new BitArray(Strings.Output.Count);
            for (int i = 0; i < ret.Count; i++)
            {
                ret[i] = buffer[i];
            }
            return ret;
        }

        public void SetOutputLogicSignals(int channel, BitArray values)
        {
            if (channel < 0 || channel > 7)
            {
                throw new ArgumentOutOfRangeException("Channel");
            }
            if (104 != values.Count)
            {
                throw new ArgumentOutOfRangeException("OutputLogic bits count");
            }

            ushort[] logicBuffer = new ushort[5];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    if (i*16 + j < 104)
                    {
                        if (values[i*16 + j])
                        {
                            logicBuffer[i] += (ushort) Math.Pow(2, j);
                        }
                    }
                }
            }
            Array.ConstrainedCopy(logicBuffer, 0, _outputLogic.Value, channel*5, 5);
        }

        #endregion

        #endregion

        #region ����������

        //���� ����������
        private slot _automaticsPageMain = new slot(0x1000, 0x1018);
        private slot _automaticsPageReserve = new slot(0x1080, 0x1098);
        //private BitArray _bits = new BitArray(8);
        //������� �������� ����������
        public event Handler AutomaticsPageLoadOK;
        public event Handler AutomaticsPageLoadFail;
        public event Handler AutomaticsPageSaveOK;
        public event Handler AutomaticsPageSaveFail;

        private CAutomatics _cautomaticsMain = new CAutomatics();

        [XmlElement("����������_��������")]
        [DisplayName("���������� ��������")]
        [Description("���������� - �������� ������ �������")]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("����������")]
        public CAutomatics AutomaticsMain
        {
            get { return _cautomaticsMain; }
            set { _cautomaticsMain = value; }
        }

        private CAutomatics _cautomaticsReserve = new CAutomatics();

        [XmlElement("����������_���������")]
        [DisplayName("���������� _���������")]
        [Description("���������� - _��������� ������ �������")]
        [TypeConverter(typeof (RussianExpandableObjectConverter))]
        [Category("����������")]
        public CAutomatics AutomaticsReserve
        {
            get { return _cautomaticsReserve; }
            set { _cautomaticsReserve = value; }
        }


        public void LoadAutomaticsPage()
        {
            LoadSlot(DeviceNumber, _automaticsPageReserve, "LoadAutomaticsPageReserve" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _automaticsPageMain, "LoadAutomaticsPageMain" + DeviceNumber, this);
        }

        public void SaveAutomaticsPage()
        {
            SaveSlot(DeviceNumber, _automaticsPageReserve, "SaveAutomaticsPageReserve" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _automaticsPageMain, "SaveAutomaticsPageMain" + DeviceNumber, this);
        }

        public class CAutomatics
        {
            public const int LENGTH = 24;
            private ushort[] _buffer = new ushort[LENGTH];

            public void SetBuffer(ushort[] buffer)
            {
                _buffer = buffer;
            }

            #region ���

            [XmlElement("������������_���")]
            [DisplayName("������������ ���")]
            [Category("����������")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string APV_Cnf
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[0]);
                    if (index >= Strings.Crat.Count)
                    {
                        index = (byte) (Strings.Crat.Count - 1);
                    }
                    return Strings.Crat[index];
                }
                set { _buffer[0] = Common.TOWORD(Common.HIBYTE(_buffer[0]), (byte) Strings.Crat.IndexOf(value)); }
            }

            [XmlElement("����������_���")]
            [DisplayName("���������� ���")]
            [Description("����� ����� ���������� ���")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string APV_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[1]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[1] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�����_����������_���")]
            [DisplayName("����� ���")]
            [Description("����� ���������� ���")]
            [Category("����������")]
            public ulong APV_Time_Blocking
            {
                get { return Measuring.GetTime(_buffer[2]); }
                set { _buffer[2] = Measuring.SetTime(value); }
            }

            [XmlElement("�����_����������_���")]
            [DisplayName("���������� ���")]
            [Description("����� ���������� ���")]
            [Category("����������")]
            public ulong APV_Time_Ready
            {
                get { return Measuring.GetTime(_buffer[3]); }
                set { _buffer[3] = Measuring.SetTime(value); }
            }

            [XmlElement("�����_1_�����_���")]
            [DisplayName("���� 1 ���")]
            [Description("����� 1����� ���")]
            [Category("����������")]
            public ulong APV_Time_1Krat
            {
                get { return Measuring.GetTime(_buffer[4]); }
                set { _buffer[4] = Measuring.SetTime(value); }
            }

            [XmlElement("�����_2_�����_���")]
            [DisplayName("���� 2 ���")]
            [Description("����� 2����� ���")]
            [Category("����������")]
            public ulong APV_Time_2Krat
            {
                get { return Measuring.GetTime(_buffer[5]); }
                set { _buffer[5] = Measuring.SetTime(value); }
            }

            [XmlElement("������_���_��_��������������")]
            [DisplayName("�������������� ���")]
            [Description("������ ��� �� ��������������")]
            //[TypeConverter(typeof(YesNoTypeConverter))]
            [Category("����������")]
            public string APV_Start
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[6]);
                    if (index >= Strings.YesNo.Count)
                    {
                        index = (byte) (Strings.YesNo.Count - 1);
                    }
                    return Strings.YesNo[index];
                }
                set { _buffer[6] = (ushort) Strings.YesNo.IndexOf(value); }
            }

            #endregion

            #region ���

            [XmlElement("����_���")]
            [DisplayName("����� ����� ���")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string ACR_Entry
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[8]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[8] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("����������_���")]
            [DisplayName("���������� ���")]
            [Description("����� ����� ���������� ���")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string ACR_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[9]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[9] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�������� ���")]
            [DisplayName("�������� ���")]
            [Category("����������")]
            public ulong ACR_Time
            {
                get { return Measuring.GetTime(_buffer[10]); }
                set { _buffer[10] = Measuring.SetTime(value); }
            }

            #endregion

            #region ����

            [XmlElement("����_����")]
            [DisplayName("����� ����� ����")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string CAPV_Entry
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[11]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[11] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("����������_����")]
            [DisplayName("���������� ����")]
            [Description("����� ����� ���������� ����")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string CAPV_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[12]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[12] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�������� ����")]
            [DisplayName("�������� ����")]
            [Category("����������")]
            public ulong CAPV_Time
            {
                get { return Measuring.GetTime(_buffer[13]); }
                set { _buffer[13] = Measuring.SetTime(value); }
            }

            #endregion

            #region ���

            [XmlElement("������_���")]
            [DisplayName("������ ���")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string AVR_StartNumber
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[16]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[16] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("����������_���")]
            [DisplayName("���������� ���")]
            [Description("����� ����� ���������� ���")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string AVR_Blocking
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[18]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[18] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("�����_�������_���")]
            [DisplayName("�������� ������� ���")]
            [Category("����������")]
            public ulong AVR_StartTime
            {
                get { return Measuring.GetTime(_buffer[17]); }
                set { _buffer[17] = Measuring.SetTime(value); }
            }

            [XmlElement("�������_���")]
            [DisplayName("������� ���")]
            [Description("����� ����� �������� ���")]
            [Category("����������")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string AVR_ReturnNumber
            {
                get
                {
                    byte index = Common.LOBYTE(_buffer[19]);
                    if (index >= Strings.Logic.Count)
                    {
                        index = (byte) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[index];
                }
                set { _buffer[19] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlElement("��������_��������_���")]
            [DisplayName("�������� �������� ���")]
            [Category("����������")]
            public ulong AVR_ReturnTime
            {
                get { return Measuring.GetTime(_buffer[20]); }
                set { _buffer[20] = Measuring.SetTime(value); }
            }

            [XmlElement("��������_����������_���")]
            [DisplayName("�������� ���������� ���")]
            [Category("����������")]
            public ulong AVR_OffTime
            {
                get { return Measuring.GetTime(_buffer[21]); }
                set { _buffer[21] = Measuring.SetTime(value); }
            }

            #endregion
        } ;

        #endregion

        #region ����

        private slot _diagnostic = new slot(0x1900, 0x190C);
        private Oscilloscope _oscilloscope;

        private slot _analog = new slot(0x1800, 0x180B);


        private COutputRele _outputRele = new COutputRele();


        //������ ������
        private static ushort _writeAlarmJournalAdress = 0x2008;
        private static ushort _writeAlarmJournal = _writeAlarmJournalAdress;

        private static ushort _startAlarmJournalAdress = 0x2200;
        private static ushort _startAlarmJournal = _startAlarmJournalAdress;
        private static ushort _sizeAlarmJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AlarmJournal)) / 2);
        private static ushort _endAlarmJournal = (ushort)(_startAlarmJournal + _sizeAlarmJournal);
        private List<slot> CurrentAlarmJournal = new List<slot>();
        private slot _alarmJournalRead = new slot(_startAlarmJournal, _endAlarmJournal);
        //����� �������� ������� ������
        private slot _alarmJournalWrite = new slot(_writeAlarmJournal, (ushort)(_writeAlarmJournal + 1));

        //��������� ������
        //private static ushort _writeSysJournalAdress = 0x2000;
        //private static ushort _writeSysJournal = _writeSysJournalAdress;

        //private static ushort _startSysJournalAdress = 0x2100;
        //private static ushort _startSysJournal = _startSysJournalAdress;
       // private static ushort _sizeSysJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(SystemJournal)) / 2);
      //  private static ushort _endSysJournal = (ushort)(_startSysJournal + _sizeSysJournal);
        private List<slot> CurrentSystemJournal = new List<slot>();
       // private slot _systemJournalRead = new slot(_startSysJournal, _endSysJournal);
        //����� �������� ���������� �������
        //private slot _systemJournalWrite = new slot(_writeSysJournal, (ushort)(_writeSysJournal + 1));
        #endregion

        #region �������

        public event Handler DiagnosticLoadOk;
        public event Handler DiagnosticLoadFail;
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler InputSignalsLoadOK;
        public event Handler InputSignalsLoadFail;
        public event Handler InputSignalsSaveOK;
        public event Handler InputSignalsSaveFail;
        public event Handler ExternalDefensesLoadOK;
        public event Handler ExternalDefensesLoadFail;
        public event Handler ExternalDefensesSaveOK;
        public event Handler ExternalDefensesSaveFail;
        public event Handler TokDefensesLoadOK;
        public event Handler TokDefensesLoadFail;
        public event Handler TokDefensesSaveOK;
        public event Handler TokDefensesSaveFail;

        public event Handler AnalogSignalsLoadOK;
        public event Handler AnalogSignalsLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler OutputSignalsLoadOK;
        public event Handler OutputSignalsLoadFail;
        public event Handler OutputSignalsSaveOK;
        public event Handler OutputSignalsSaveFail;

        public event Handler AlarmJournalLoadOK;
        public event Handler AlarmJournalLoadFail;
        public event Handler AlarmJournalSaveOK;
        public event Handler AlarmJournalSaveFail;
        
        #endregion

        #region ������������ �������������

        public MR301()
        {
           HaveVersion = true;
        }

        public MR301(Modbus mb)
        {
            Init();
            MB = mb;
            InitAddr();
        }

        private void InitAddr()
        {
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("������ �������", this, 0x2100);
            this._refreshSystemJournal = new MemoryEntity<OneWordStruct>("���������� ������� �������", this, 0x2000);
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += new CompleteExchangeHandler(mb_CompleteExchange);
                }
            }
        }


        private void Init()
        {
            HaveVersion = true;
            _alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2800;
            ushort size = 20;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                _alarmJournal[i] = new slot(start, (ushort) (start + size));
                start += 0x40;
            }
            _oscilloscope = new Oscilloscope(this);
            _oscilloscope.Initialize();

            _msgDictionary = new Dictionary<ushort, string>(6);
            _codeDictionary = new Dictionary<byte, string>(19);
            _typeDictionary = new Dictionary<byte, string>(9);

            _msgDictionary.Add(0, "������ ����");
            _msgDictionary.Add(1, "������������");
            _msgDictionary.Add(2, "����������");
            _msgDictionary.Add(3, "����������");
            _msgDictionary.Add(4, "���������� ���");
            _msgDictionary.Add(118, "������ ����");
            _msgDictionary.Add(147, "������ ����");
            _msgDictionary.Add(255, "��� ���������");
            _msgDictionary.Add(84, "��� ���������");

            _codeDictionary.Add(1, "��-1");
            _codeDictionary.Add(2, "��-2");
            _codeDictionary.Add(3, "��-3");
            _codeDictionary.Add(4, "��-4");
            _codeDictionary.Add(5, "��-5");
            _codeDictionary.Add(6, "��-6");
            _codeDictionary.Add(7, "��-7");
            _codeDictionary.Add(8, "��-8");
            _codeDictionary.Add(9, "I>");
            _codeDictionary.Add(10, "I>>");
            _codeDictionary.Add(11, "I>>>");
            _codeDictionary.Add(12, "I>>>>");
            _codeDictionary.Add(13, "I0>");
            _codeDictionary.Add(14, "I0>>");
            _codeDictionary.Add(15, "I2>");
            _codeDictionary.Add(16, "I2>>");
            _codeDictionary.Add(17, "���");
            _codeDictionary.Add(18, "���");


            _typeDictionary.Add(0, "��");
            _typeDictionary.Add(1, "Ig");
            _typeDictionary.Add(2, "Io");
            _typeDictionary.Add(3, "Ia");
            _typeDictionary.Add(4, "Ib");
            _typeDictionary.Add(5, "Ic");
            _typeDictionary.Add(6, "I0");
            _typeDictionary.Add(7, "I1");
            _typeDictionary.Add(8, "I2");
        }

        #endregion

        #region ���������� �������

        [Category("���������� �������")]
        public double Ig
        {
            get { return Measuring.GetI(_analog.Value[0], TTNP, false); }
        }

        [Category("���������� �������")]
        public double Io
        {
            get { return Measuring.GetI(_analog.Value[1], TTNP, false); }
        }

        [Category("���������� �������")]
        public double Ia
        {
            get { return Measuring.GetI(_analog.Value[2], TT, true); }
        }

        [Category("���������� �������")]
        public double Ib
        {
            get { return Measuring.GetI(_analog.Value[3], TT, true); }
        }

        [Category("���������� �������")]
        public double Ic
        {
            get { return Measuring.GetI(_analog.Value[4], TT, true); }
        }

        [Category("���������� �������")]
        public double I0
        {
            get { return Measuring.GetI(_analog.Value[5], TT, true); }
        }

        [Category("���������� �������")]
        public double I1
        {
            get { return Measuring.GetI(_analog.Value[6], TT, false); }
        }

        [Category("���������� �������")]
        public double I2
        {
            get { return Measuring.GetI(_analog.Value[7], TT, false); }
        }

        #endregion

        #region ���������� �������

        private slot _constraintGroup = new slot(0x0400, 0x0401);

        public void SelectConstraintGroup(bool value)
        {
            _constraintGroup.Value[0] = value ? (ushort) 1 : (ushort) 0;
            SaveSlot(DeviceNumber, _constraintGroup, "��301�" + DeviceNumber + ": ����� ������ �������", this);
        }


        [XmlIgnore]
        [DisplayName("����������� �������")]
        [Description("����������� ������� ����")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray ManageSignals
        {
            get
            {
                BitArray ret = new BitArray(7);
                int j = 0;
                for (int i = 1; i < 8; i++)
                {
                    if (4 == i)
                    {
                        i++;
                    }
                    ret[j++] = Common.GetBit(_diagnostic.Value[0], i);
                }

                return ret;
            }
        }

        [XmlIgnore]
        [DisplayName("����������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Indicators
        {
            get
            {
                BitArray ret = new BitArray(new byte[] {Common.LOBYTE(_diagnostic.Value[6])});
                ret[4] = !ret[4];
                ret[5] = !ret[5];
                return ret;
            }
        }

        [XmlIgnore]
        [DisplayName("������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new byte[]
                                        {
                                            Common.HIBYTE(_diagnostic.Value[8]),
                                            Common.LOBYTE(_diagnostic.Value[9]),
                                        });
            }
        }

        [XmlIgnore]
        [DisplayName("����")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray Rele
        {
            get { return new BitArray(new byte[] {Common.HIBYTE(_diagnostic.Value[6])}); }
        }

        [XmlIgnore]
        [DisplayName("�������� �������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray OutputSignals
        {
            get { return new BitArray(new byte[] {
                Common.HIBYTE(_diagnostic.Value[11])
            }); }
        }

        [XmlIgnore]
        [DisplayName("������� ��������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new byte[]
                                        {
                                            Common.HIBYTE(_diagnostic.Value[9]),
                                            Common.LOBYTE(_diagnostic.Value[10]),
                                        });
            }
        }

        [XmlIgnore]
        [DisplayName("��������� �������������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray FaultState
        {
            get { return new BitArray(new byte[] {Common.LOBYTE(_diagnostic.Value[4])}); }
        }

        [XmlIgnore]
        [DisplayName("������� �������������")]
        [Category("���������� �������")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray FaultSignals
        {
            get
            {
                return
                    new BitArray(new byte[] {Common.HIBYTE(_diagnostic.Value[4]), Common.LOBYTE(_diagnostic.Value[5])});
            }
        }

        [XmlIgnore]
        [DisplayName("������� ��")]
        [Category("C������ ������� �����")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray ExternalSignals
        {
            get { return new BitArray(new byte[] {  Common.HIBYTE(_diagnostic.Value[10])
                                                    }); }
        }

        [XmlIgnore]
        [DisplayName("������� ����������")]
        [Category("C������ ������� �����")]
        [Editor(typeof (LedEditor), typeof (UITypeEditor))]
        [TypeConverter(typeof (RussianObjectConverter))]
        public BitArray AutomaticSignals
        {
            get { return new BitArray(new byte[] {
                Common.LOBYTE(_diagnostic.Value[11])
            }); 
            }
        }

        #endregion

        #region ������� ������

        private slot _externalDefensesMain = new slot(0x1020, 0x1040);
        private slot _externalDefensesReserve = new slot(0x10A0, 0x10C0);


        public void LoadExternalDefenses()
        {
            LoadSlot(DeviceNumber, _externalDefensesMain, "LoadExternalDefenses1" + DeviceNumber, this);
            LoadSlot(DeviceNumber, _externalDefensesReserve, "LoadExternalDefenses2" + DeviceNumber, this);
        }

        public void SaveExternalDefenses()
        {
            Array.ConstrainedCopy(ExternalDefensesMain.ToUshort(), 0, _externalDefensesMain.Value, 0,
                                  CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(ExternalDefensesReserve.ToUshort(), 0, _externalDefensesReserve.Value, 0,
                                  CExternalDefenses.LENGTH);

            SaveSlot(DeviceNumber, _externalDefensesMain, "SaveExternalDefenses1" + DeviceNumber, this);
            SaveSlot(DeviceNumber, _externalDefensesReserve, "SaveExternalDefenses2" + DeviceNumber, this);
        }

        public class CExternalDefenses : ICollection
        {
            public const int LENGTH = 0x20;
            public const int COUNT = 8;

            #region ICollection Members

            public void Add(ExternalDefenseItem item)
            {
                _defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _defenseList.GetEnumerator();
            }

            #endregion

            public CExternalDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            private List<ExternalDefenseItem> _defenseList = new List<ExternalDefenseItem>(COUNT);

            public void SetBuffer(ushort[] buffer)
            {
                _defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", LENGTH,
                                                          "External defense length must be 48");
                }
                for (int i = 0; i < LENGTH; i += ExternalDefenseItem.LENGTH)
                {
                    ushort[] temp = new ushort[ExternalDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, ExternalDefenseItem.LENGTH);
                    _defenseList.Add(new ExternalDefenseItem(temp));
                }
                for (int i = 0; i < COUNT; i++)
                {
                    this[i].Name = "�� - " + (i + 1);
                }
            }

            [DisplayName("����������")]
            public int Count
            {
                get { return _defenseList.Count; }
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];

                for (int i = 0; i < Count; i++)
                {
                    Array.ConstrainedCopy(_defenseList[i].Values, 0, buffer, i*ExternalDefenseItem.LENGTH,
                                          ExternalDefenseItem.LENGTH);
                }
                return buffer;
            }

            public ExternalDefenseItem this[int i]
            {
                get { return _defenseList[i]; }
                set { _defenseList[i] = value; }
            }
        } ;

        public class ExternalDefenseItem
        {
            public const int LENGTH = 4;
            private ushort[] _values = new ushort[LENGTH];

            public ExternalDefenseItem()
            {
            }

            public ExternalDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            public override string ToString()
            {
                return "������� ������";
            }

            private string _name;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }


            //[XmlAttribute("��������")]
            //[DisplayName("��������")]
            //[Description("������ � ������� ����")]
            //[Category("������� ���")]
            //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value",  LENGTH,
                                                          "External defense item length must be 6");
                }
                _values = buffer;
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte) (Common.LOBYTE(_values[0]) & 0x0F);
                    if (index >= Strings.Modes2.Count)
                    {
                        index = (byte) (Strings.Modes2.Count - 1);
                    }
                    return Strings.Modes2[index];
                }
                set { _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes2.IndexOf(value), 0, 1, 2, 3); }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("����� ����������")]
            [Description("����� ����� ���������� ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[2] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set { _values[2] = (ushort) Strings.Logic.IndexOf(value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Description("����� ����� ������������ ��")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ExternalDefensesTypeConverter))]
            public string Entry
            {
                get
                {
                    ushort value = (ushort) (_values[1] & 0xFF);
                    if (value >= Strings.ExternalDefense.Count)
                    {
                        value = (ushort) (Strings.ExternalDefense.Count - 1);
                    }
                    return Strings.ExternalDefense[value];
                }
                set { _values[1] = (ushort) Strings.ExternalDefense.IndexOf(value); }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������ ��")]
            [Category("��������� ���")]
            public ulong WorkingTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return Common.GetBit(_values[0], 14); }
                set { _values[0] = Common.SetBit(_values[0], 14, value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return Common.GetBit(_values[0], 15); }
                set { _values[0] = Common.SetBit(_values[0], 15, value); }
            }
        }

        private CExternalDefenses _cexternalDefensesMain = new CExternalDefenses();
        private CExternalDefenses _cexternalDefensesResereve = new CExternalDefenses();

        //[XmlElement("�������_������_��������")]
        //[DisplayName("������� ������ ��������")]
        //[Description("������� ������ - �������� ������ �������")]
        //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        //[TypeConverter(typeof (RussianExpandableObjectConverter))]
        //[Category("������� ������")]
        public CExternalDefenses ExternalDefensesMain
        {
            get { return _cexternalDefensesMain; }
            set { _cexternalDefensesMain = value; }
        }

        //[XmlElement("�������_������_���������")]
        //[DisplayName("������� ������ ���������")]
        //[Description("������� ������ - ��������� ������ �������")]
        //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        //[TypeConverter(typeof (RussianExpandableObjectConverter))]
        //[Category("������� ������")]
        public CExternalDefenses ExternalDefensesReserve
        {
            get { return _cexternalDefensesResereve; }
            set { _cexternalDefensesResereve = value; }
        }

        #endregion

        #region ���� - �����

        private slot _datetime = new slot(0x200, 0x207);

        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, _datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        [Browsable(false)]
        public byte[] DateTime
        {
            get { return Common.TOBYTES(_datetime.Value, true); }
            set { _datetime.Value = Common.TOWORDS(value, true); }
        }

        #endregion

        #region ������� ��������/����������

        public void LoadDiagnostic()
        {
            LoadBitSlot(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, this);
        }

        public void LoadDiagnosticCycle()
        {
            LoadSlotCycle(DeviceNumber, _diagnostic, "LoadDiagnostic" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void LoadAnalogSignals()
        {
            LoadSlot(DeviceNumber, _analog, "LoadAnalogSignals" + DeviceNumber, this);
        }

        public void RemoveDiagnostic()
        {
            MB.RemoveQuery("LoadDiagnostic" + DeviceNumber);
        }

        public void RemoveAnalogSignals()
        {
            MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticQuick()
        {
            MakeQueryQuick("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeDiagnosticSlow()
        {
            MakeQuerySlow("LoadDiagnostic" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void RemoveDateTime()
        {
            MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        //public void SuspendSystemJournal(bool suspend)
        //{
        //    List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
        //    for (int i = 0; i < q.Count; i++)
        //    {
        //        MB.SuspendQuery(q[i].name, suspend);
        //    }
        //}

        public void SuspendAlarmJournal(bool suspend)
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.SuspendQuery(q[i].name, suspend);
            }
        }

        public void RemoveAlarmJournal()
        {
            List<Query> q = MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            for (int i = 0; i < q.Count; i++)
            {
                MB.RemoveQuery(q[i].name);
            }
            _stopAlarmJournal = true;
        }

        //public void RemoveSystemJournal()
        //{
        //    List<Query> q = MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
        //    for (int i = 0; i < q.Count; i++)
        //    {
        //        MB.RemoveQuery(q[i].name);
        //    }
        //    _stopSystemJournal = true;
        //}

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, _datetime, "SaveDateTime" + DeviceNumber, this);
        }

        //public void LoadSystemJournal()
        //{
        //    LoadSlot(DeviceNumber, _systemJournal[0], "LoadSJRecord0_" + DeviceNumber);
        //    _stopSystemJournal = false;
        //}

        //public void LoadAlarmJournal()
        //{
        //    if (!_inputSignals.Loaded)
        //    {
        //        LoadInputSignals();
        //    }
        //    _stopAlarmJournal = false;
        //    LoadSlot(DeviceNumber, _alarmJournal[0], "LoadALRecord0_" + DeviceNumber);
        //}

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0, true, "��301 ������ �������������", this);
        }

        //������ ������ ���������� �������
        int _systemJournalRec = 0;
        //public void LoadSystemJournal()
        //{
        //    LoadSlot(DeviceNumber, _systemJournalRead, "LoadSystemJournalRecord" + _systemJournalRec + DeviceNumber, this);
        //}

        int _alarmJournalRec = 0;
        public void LoadAlarmJournal()
        {
            LoadSlot(DeviceNumber, _alarmJournalRead, "LoadAlarmJournalRecord" + _alarmJournalRec + DeviceNumber, this);
        }

        //public void SaveSystemJournalIndex()
        //{
        //    _systemJournalRec = 0;
        //    _systemJournalWrite.Value[0] = 0;
        //    SaveSlot6(DeviceNumber, _systemJournalWrite, "SaveSystemJournal" + DeviceNumber, this);
        //}

        public void SaveAlarmJournalIndex()
        {
            _alarmJournalRec = 0;
            _alarmJournalWrite.Value[0] = 0;
            SaveSlot6(DeviceNumber, _alarmJournalWrite, "SaveAlarmJournal" + DeviceNumber, this);
        }

        #endregion

        #region ������������

        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ��301 ���������", binFileName);
            }

            try
            {
                DeserializeSlot(doc, "/��301_�������/����_�����", _datetime);
                DeserializeSlot(doc, "/��301_�������/�������_�������", _inputSignals);
                DeserializeSlot(doc, "/��301_�������/��������_�������", _outputSignals);
                DeserializeSlot(doc, "/��301_�������/��������_����������_�������", _outputLogic);
                DeserializeSlot(doc, "/��301_�������/�����������", _diagnostic);
                DeserializeSlot(doc, "/��301_�������/������_�������_��������", _externalDefensesMain);
                DeserializeSlot(doc, "/��301_�������/������_�������_���������", _externalDefensesReserve);
                DeserializeSlot(doc, "/��301_�������/����������_��������", _automaticsPageMain);
                DeserializeSlot(doc, "/��301_�������/����������_���������", _automaticsPageReserve);
                DeserializeSlot(doc, "/��301_�������/�������_��������", _tokDefensesMain);
                DeserializeSlot(doc, "/��301_�������/�������_���������", _tokDefensesReserve);
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��301 ���������", binFileName);
            }

            ExternalDefensesMain.SetBuffer(_externalDefensesMain.Value);
            ExternalDefensesReserve.SetBuffer(_externalDefensesReserve.Value);
            AutomaticsMain.SetBuffer(_automaticsPageMain.Value);
            AutomaticsReserve.SetBuffer(_automaticsPageReserve.Value);
            TokDefensesMain.SetBuffer(_tokDefensesMain.Value);
            TokDefensesReserve.SetBuffer(_tokDefensesReserve.Value);

            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
            OutputRele.SetBuffer(releBuffer);
            ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
            Array.ConstrainedCopy(_outputSignals.Value, COutputIndicator.OFFSET, outputIndicator, 0,
                                  COutputIndicator.LENGTH);
            OutputIndicator.SetBuffer(outputIndicator);
        }

        private static void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void SerializeBin(string binFileName)
        {
           /* string xmlFileName = Path.ChangeExtension(binFileName, ".xml");

            XmlSerializer ser = new XmlSerializer(GetType());
            TextWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
            ser.Serialize(writer, this);
            writer.Close();*/


            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("��301_�������"));

            Array.ConstrainedCopy(ExternalDefensesMain.ToUshort(), 0, _externalDefensesMain.Value, 0,
                                  CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(ExternalDefensesReserve.ToUshort(), 0, _externalDefensesReserve.Value, 0,
                                  CExternalDefenses.LENGTH);
            Array.ConstrainedCopy(OutputRele.ToUshort(), 0, _outputSignals.Value, COutputRele.OFFSET, COutputRele.LENGTH);
            Array.ConstrainedCopy(OutputIndicator.ToUshort(), 0, _outputSignals.Value, COutputIndicator.OFFSET,
                                  COutputIndicator.LENGTH);
            Array.ConstrainedCopy(TokDefensesMain.ToUshort(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH);


            SerializeSlot(doc, "����_�����", _datetime);
            SerializeSlot(doc, "�������_�������", _inputSignals);
            SerializeSlot(doc, "��������_�������", _outputSignals);
            SerializeSlot(doc, "��������_����������_�������", _outputLogic);
            SerializeSlot(doc, "�����������", _diagnostic);
            SerializeSlot(doc, "������_�������_��������", _externalDefensesMain);
            SerializeSlot(doc, "������_�������_���������", _externalDefensesReserve);
            SerializeSlot(doc, "����������_��������", _automaticsPageMain);
            SerializeSlot(doc, "����������_���������", _automaticsPageReserve);
            SerializeSlot(doc, "�������_��������", _tokDefensesMain);
            SerializeSlot(doc, "�������_���������", _tokDefensesReserve);


            doc.Save(binFileName);
        }

        private static void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }

        #endregion

        #region ������� ������

        private slot _tokDefensesMain = new slot(0x1040, 0x1080);
        //private slot _tokDefenses2Main = new slot(0x1060, 0x1080);
        private slot _tokDefensesReserve = new slot(0x10C0, 0x1100);
        //private slot _tokDefenses2Reserve = new slot(0x10E0, 0x1100);

        public void LoadTokDefenses()
        {
            LoadSlot(DeviceNumber, _tokDefensesReserve, "LoadTokDefensesReserve" + DeviceNumber, this);
            //LoadSlot(DeviceNumber, _tokDefenses2Reserve, "LoadTokDefensesReserve2" + DeviceNumber);
            LoadSlot(DeviceNumber, _tokDefensesMain, "LoadTokDefensesMain" + DeviceNumber, this);
            //LoadSlot(DeviceNumber, _tokDefenses2Main, "LoadTokDefensesMain2" + DeviceNumber);
        }

        public void SaveTokDefenses()
        {
            Array.ConstrainedCopy(TokDefensesMain.ToUshort(), 0, _tokDefensesMain.Value, 0, CTokDefenses.LENGTH);
            Array.ConstrainedCopy(TokDefensesReserve.ToUshort(), 0, _tokDefensesReserve.Value, 0, CTokDefenses.LENGTH);

            SaveSlot(DeviceNumber, _tokDefensesReserve, "SaveTokDefensesReserve" + DeviceNumber,this);
            SaveSlot(DeviceNumber, _tokDefensesMain, "SaveTokDefensesMain" + DeviceNumber, this);
        }

        private CTokDefenses _ctokDefensesMain = new CTokDefenses();
        private CTokDefenses _ctokDefensesReserve = new CTokDefenses();

        //[XmlElement("�������_������_��������")]
        //[DisplayName("�������� ������")]
        //[Description("������� ������ - �������� ������ �������")]
        //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        //[TypeConverter(typeof (RussianExpandableObjectConverter))]
        //[Category("������� ������")]
        public CTokDefenses TokDefensesMain
        {
            get { return _ctokDefensesMain; }
        }

        //[XmlElement("�������_������_���������")]
        //[DisplayName("��������� ������")]
        //[Description("������� ������ - ��������� ������ �������")]
        //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
        //[TypeConverter(typeof (RussianExpandableObjectConverter))]
        //[Category("������� ������")]
        public CTokDefenses TokDefensesReserve
        {
            get { return _ctokDefensesReserve; }
        }


        public class CTokDefenses : ICollection
        {
            public const int LENGTH = 0x40;
            public const int COUNT = 8;

            #region ICollection Members

            [DisplayName("����������")]
            public int Count
            {
                get { return _items.Count; }
            }

            public void Add(TokDefenseItem item)
            {
                _items.Add(item);
            }

            public void CopyTo(Array array, int index)
            {
            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }

            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return _items.GetEnumerator();
            }

            #endregion

            private List<TokDefenseItem> _items = new List<TokDefenseItem>(12);

            public CTokDefenses()
            {
                SetBuffer(new ushort[LENGTH]);
            }

            public TokDefenseItem this[int i]
            {
                get { return _items[i]; }
                set { _items[i] = value; }
            }

            public void SetBuffer(ushort[] buffer)
            {
                _items.Clear();
                for (int i = 0; i < buffer.Length; i += TokDefenseItem.LENGTH)
                {
                    ushort[] item = new ushort[TokDefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, item, 0, TokDefenseItem.LENGTH);
                    _items.Add(new TokDefenseItem(item));
                }

                _items[0].Name = "I>";
                _items[0].IsI = true;
                _items[1].Name = "I>>";
                _items[1].IsI = true;
                _items[2].Name = "I>>>";
                _items[2].IsI = true;
                _items[3].Name = "I>>>>";
                _items[3].IsI = true;
                _items[4].Name = "I0>";
                _items[4].IsI0 = true;
                _items[5].Name = "I0>>";
                _items[5].IsI0 = true;
                _items[6].Name = "I2>";
                _items[6].IsI2 = true;
                _items[7].Name = "I2>>";
                _items[7].IsI2 = true;
            }

            public ushort[] ToUshort()
            {
                ushort[] buffer = new ushort[LENGTH];

                for (int i = 0; i < COUNT; i++)
                {
                    Array.ConstrainedCopy(_items[i].Values, 0, buffer, i*TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
                }
                return buffer;
            }

            //public ushort[] ToUshort2()
            //{
            //    ushort[] buffer = new ushort[LENGTH / 2];
            //    for (int i = (COUNT / 2); i < COUNT; i++)
            //    {
            //        Array.ConstrainedCopy(_items[i].Values, 0, buffer, i * TokDefenseItem.LENGTH, TokDefenseItem.LENGTH);
            //    }
            //    return buffer;
            //}
        } ;

        public class TokDefenseItem
        {
            public const int LENGTH = 8;
            private bool _isI = false;
            private bool _isI0 = false;
            private bool _isI2 = false;

            [XmlIgnore]
            [Browsable(false)]
            public bool IsI2
            {
                get { return _isI2; }
                set { _isI2 = value; }
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsI0
            {
                get { return _isI0; }
                set { _isI0 = value; }
            }

            [XmlIgnore]
            [Browsable(false)]
            public bool IsI
            {
                get { return _isI; }
                set { _isI = value; }
            }


            private String _name;
            private ushort[] _values = new ushort[LENGTH];

            public TokDefenseItem()
            {
            }

            public TokDefenseItem(ushort[] buffer)
            {
                SetItem(buffer);
            }

            [XmlIgnore]
            [Browsable(false)]
            public String Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public void SetItem(ushort[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Tok defense increase1 item value",  LENGTH,
                                                          "Tok defense increase1 item length must be 6");
                }
                _values = buffer;
            }

            //[XmlAttribute("��������")]
            //[DisplayName("��������")]
            //[Description("������ � ������� ����")]
            //[Category("������� ���")]
            //[Editor(typeof (RussianCollectionEditor), typeof (UITypeEditor))]
            public ushort[] Values
            {
                get { return _values; }
                set { SetItem(value); }
            }

            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte) (Common.LOBYTE(_values[0]) & 0xF);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte) (Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set 
                { 
                    _values[0] = Common.SetBits(_values[0], (ushort) Strings.Modes.IndexOf(value), 0, 1, 2, 3); 
                }
            }

            [XmlAttribute("����������_�����")]
            [DisplayName("���������� �����")]
            [Description("����� ����� ����������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (LogicTypeConverter))]
            public string BlockingNumber
            {
                get
                {
                    ushort value = (ushort) (_values[1] & 0xFF);
                    if (value >= Strings.Logic.Count)
                    {
                        value = (ushort) (Strings.Logic.Count - 1);
                    }
                    return Strings.Logic[value];
                }
                set 
                { 
                    _values[1] = (ushort) Strings.Logic.IndexOf(value); 
                }
            }

            private static string GetStringDataValue(int index, List<string> stringList)
            {
                if (index >= stringList.Count)
                {
                    index = stringList.Count - 1;
                }
                return stringList[index];
            }

            [XmlAttribute("��� �������")]
            [Browsable(false)]
            public string Signal
            {
                get
                {
                    string ret = "";
                    int index = Common.GetBits(_values[0], new int[] {4, 5}) >> 4;
                    if (IsI)
                    {
                        ret = GetStringDataValue(index, Strings.TokSignalI);
                    }
                    else if (IsI0)
                    {
                        ret = GetStringDataValue(index, Strings.TokSignalI0);
                    }
                    else if (IsI2)
                    {
                        ret = GetStringDataValue(index, Strings.TokSignalI2);
                    }
                    return ret;
                }
                set
                {
                    if (IsI)
                    {
                        _values[0] =
                            Common.SetBits(_values[0], (ushort) Strings.TokSignalI.IndexOf(value), new int[] {4, 5});
                    }
                    else if (IsI0)
                    {
                        _values[0] =
                            Common.SetBits(_values[0], (ushort) Strings.TokSignalI0.IndexOf(value), new int[] {4, 5});
                    }
                    else if (IsI2)
                    {
                        _values[0] =
                            Common.SetBits(_values[0], (ushort) Strings.TokSignalI2.IndexOf(value), new int[] {4, 5});
                    }
                }
            }

            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    return Measuring.GetConstraint(_values[2], ConstraintKoefficient.K_4001);
                }
                set
                {
                    _values[2] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
                }
            }

            [XmlAttribute("��������������")]
            [DisplayName("��������������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (FeatureTypeConverter))]
            public string Feature
            {
                get
                {
                    string ret;
                    if (IsI)
                    {
                        ret = Common.GetBit(_values[0], 12) ? Strings.FeatureI[1] : Strings.FeatureI[0];
                    }
                    else
                    {
                        ret = "�����������";
                    }

                    return ret;
                }
                set
                {
                    if (IsI)
                    {
                        _values[0] = Common.SetBits(_values[0], (ushort) Strings.FeatureI.IndexOf(value), 12);
                    }
                    else
                    {
                        _values[0] = Common.SetBits(_values[0], 0, 12);
                    }
                }
            }


            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Category("��������� ���")]
            public ulong WorkTime
            {
                get { return Measuring.GetTime(_values[3]); }
                set { _values[3] = Measuring.SetTime(value); }
            }

            [XmlAttribute("�����������")]
            [DisplayName("�����������")]
            [Category("��������� ���")]
            public ushort Koefficient
            {
                get { return _values[3]; }
                set { _values[3] = value; }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool APV
            {
                get { return Common.GetBit(_values[0], 14); }
                set { _values[0] = Common.SetBit(_values[0], 14, value); }
            }

            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool UROV
            {
                get { return Common.GetBit(_values[0], 15); }
                set { _values[0] = Common.SetBit(_values[0], 15, value); }
            }

            [XmlAttribute("���������")]
            [DisplayName("���������")]
            [Category("��������� ���")]
            [TypeConverter(typeof (BooleanTypeConverter))]
            public bool Speedup
            {
                get { return Common.GetBit(_values[0], 13); }
                set { _values[0] = Common.SetBit(_values[0], 13, value); }
            }

            [XmlAttribute("���������_�����")]
            [DisplayName("��������� �����")]
            [Description("�������� ������� ���������")]
            [Category("��������� ���")]
            public ulong SpeedupTime
            {
                get { return Measuring.GetTime(_values[4]); }
                set { _values[4] = Measuring.SetTime(value); }
            }
        } ;

        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (MR301); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr301; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��301"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        public const ulong TIMELIMIT = 3000000;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]";
        
        private new void mb_CompleteExchange(object sender, Query query)
        {
            _oscilloscope.OnMbCompleteExchange(sender,query);

            #region ������ ������

            if (IsIndexQuery(query.name, "LoadALRecord"))
            {
                int index = GetIndex(query.name, "LoadALRecord");
                if (0 == query.fail)
                {
                    _alarmJournalRecords.AddMessage(query.readBuffer);
                    if (_alarmJournalRecords.IsMessageEmpty(index))
                    {
                        if (null != AlarmJournalLoadOk)
                        {
                            AlarmJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != AlarmJournalRecordLoadOk)
                        {
                            AlarmJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < ALARMJOURNAL_RECORD_CNT && !_stopAlarmJournal)
                        {
                            LoadSlot(DeviceNumber, _alarmJournal[index], "LoadALRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (null != AlarmJournalLoadOk)
                            {
                                AlarmJournalLoadOk(this);
                            }
                        }
                    }
                }
                else
                {
                    if (null != AlarmJournalRecordLoadFail)
                    {
                        AlarmJournalRecordLoadFail(this, index);
                    }
                }
            }

            #endregion

            #region ��������� ������ �����
            //if ("LoadSystemJournalRecord" + _systemJournalRec + DeviceNumber == query.name)
            //{
            //    _systemJournalRec++;
            //    Raise(query, SysJournalLoadOK, SysJournalLoadFail, ref _systemJournalRead);
            //    CurrentSystemJournal.Add(_systemJournalRead);
            //}

            //if ("SaveSystemJournal" + DeviceNumber == query.name)
            //{
            //    Raise(query, SysJournalSaveOK, SysJournalSaveFail);
            //}
            #endregion

            #region ������ ������ �����
            if ("LoadAlarmJournalRecord" + _alarmJournalRec + DeviceNumber == query.name)
            {
                _alarmJournalRec++;
                Raise(query, AlarmJournalLoadOK, AlarmJournalLoadFail, ref _alarmJournalRead);
                CurrentAlarmJournal.Add(_alarmJournalRead);
                
            }

            if ("SaveAlarmJournal" + DeviceNumber == query.name)
            {
                Raise(query, AlarmJournalSaveOK, AlarmJournalSaveFail);
            }
            #endregion

            #region �������� �������

            if ("LoadOutputLogic" + DeviceNumber == query.name)
            {
                _outputLogic.Value = Common.TOWORDS(query.readBuffer, true);
            }
            if ("LoadOutputSignals" + DeviceNumber == query.name)
            {
                _outputSignals.Value = Common.TOWORDS(query.readBuffer, true);
                ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                Array.ConstrainedCopy(_outputSignals.Value, COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
                _outputRele.SetBuffer(releBuffer);

                ushort[] outputIndicator = new ushort[COutputIndicator.LENGTH];
                Array.ConstrainedCopy(_outputSignals.Value, COutputIndicator.OFFSET, outputIndicator, 0,
                                      COutputIndicator.LENGTH);
                _outputIndicator.SetBuffer(outputIndicator);

                if (0 == query.fail)
                {
                    if (null != OutputSignalsLoadOK)
                    {
                        OutputSignalsLoadOK(this);
                    }
                }
                else
                {
                    if (null != OutputSignalsLoadFail)
                    {
                        OutputSignalsLoadFail(this);
                    }
                }
            }
            if ("SaveOutputSignals" + DeviceNumber == query.name)
            {
                Raise(query, OutputSignalsSaveOK, OutputSignalsSaveFail);
            }

            #endregion

            #region ������� ������

            if ("LoadExternalDefenses1" + DeviceNumber == query.name)
            {
                _externalDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
            }
            if ("LoadExternalDefenses2" + DeviceNumber == query.name)
            {
                _externalDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);

                ExternalDefensesMain.SetBuffer(_externalDefensesMain.Value);
                ExternalDefensesReserve.SetBuffer(_externalDefensesReserve.Value);

                if (0 == query.fail)
                {
                    if (null != ExternalDefensesLoadOK)
                    {
                        ExternalDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != ExternalDefensesLoadFail)
                    {
                        ExternalDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveExternalDefenses1" + DeviceNumber == query.name)
            {
                Raise(query, ExternalDefensesSaveOK, ExternalDefensesSaveFail);
            }

            #endregion

            #region ������� �������

            if ("LoadInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, InputSignalsLoadOK, InputSignalsLoadFail, ref _inputSignals);
            }
            if ("SaveInputSignals" + DeviceNumber == query.name)
            {
                Raise(query, InputSignalsSaveOK, InputSignalsSaveFail);
            }

            #endregion

            #region ������� ������

            if ("LoadTokDefensesReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _tokDefensesReserve.Value = Common.TOWORDS(query.readBuffer, true);
                    _ctokDefensesReserve.SetBuffer(_tokDefensesReserve.Value);
                }
            }
            if ("LoadTokDefensesMain" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    _tokDefensesMain.Value = Common.TOWORDS(query.readBuffer, true);
                    _ctokDefensesMain.SetBuffer(_tokDefensesMain.Value);
                    if (null != TokDefensesLoadOK)
                    {
                        TokDefensesLoadOK(this);
                    }
                }
                else
                {
                    if (null != TokDefensesLoadFail)
                    {
                        TokDefensesLoadFail(this);
                    }
                }
            }
            if ("SaveTokDefensesMain" + DeviceNumber == query.name)
            {
                Raise(query, TokDefensesSaveOK, TokDefensesSaveFail);
            }

            #endregion

            #region ���������� 

            if ("LoadAutomaticsPageReserve" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    Array.ConstrainedCopy(Common.TOWORDS(query.readBuffer, true), 0, _automaticsPageReserve.Value, 0,
                                          CAutomatics.LENGTH);
                    AutomaticsReserve.SetBuffer(_automaticsPageReserve.Value);
                }
            }

            if ("LoadAutomaticsPageMain" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    Array.ConstrainedCopy(Common.TOWORDS(query.readBuffer, true), 0, _automaticsPageMain.Value, 0,
                                          CAutomatics.LENGTH);
                    AutomaticsMain.SetBuffer(_automaticsPageMain.Value);
                    if (null != AutomaticsPageLoadOK)
                    {
                        AutomaticsPageLoadOK(this);
                    }
                }
                else
                {
                    if (null != AutomaticsPageLoadFail)
                    {
                        AutomaticsPageLoadFail(this);
                    }
                }
            }
            if ("SaveAutomaticsPageMain" + DeviceNumber == query.name)
            {
                Raise(query, AutomaticsPageSaveOK, AutomaticsPageSaveFail);
            }

            #endregion

            if ("LoadDiagnostic" + DeviceNumber == query.name)
            {
                Raise(query, DiagnosticLoadOk, DiagnosticLoadFail, ref _diagnostic);
            }
            if ("LoadDateTime" + DeviceNumber == query.name)
            {
                Raise(query, DateTimeLoadOk, DateTimeLoadFail, ref _datetime);
            }


            if ("LoadAnalogSignals" + DeviceNumber == query.name)
            {
                if (!_inputSignals.Loaded)
                {
                    LoadInputSignals();
                }
                else
                {
                    Raise(query, AnalogSignalsLoadOK, AnalogSignalsLoadFail, ref _analog);
                }
            }

            if ("version" + DeviceNumber == query.name)
            {
                byte[] d = query.readBuffer;
            }

            base.mb_CompleteExchange(sender, query);
        }

        public Type[] Forms
        {
            get
            {
                if (DeviceVersion == "�� ������ 2.15")
                {
                    DeviceVersion = "2.15";
                }
                return new[]
                {
                    typeof (ConfigurationForm),
                    typeof (MeasuringForm),
                    typeof (AlarmJournalForm),
                    typeof (SystemJournalForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return  new List<string>
                {
                    "�� ������ 2.15",
                    "2.16"
                };
            }
        }
    }
}