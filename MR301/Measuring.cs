using System;

namespace BEMN.MR301
{
    public class Measuring
    {
        public static double GetU(ushort value, ConstraintKoefficient koeff)
        {
            double temp = (double)((double)value / (double)0x100 * (double)koeff) / 1000;

            return Math.Round(temp, 2);
        }

        public static double SetU(double value, ConstraintKoefficient koeff)
        {
            return (double)((double)value * 1000 / (double)koeff) * (double)0x100;
        }
        
        public static double GetI(ushort value, ushort KoeffNPTT, bool phaseTok)
        {
            int b;
            b = phaseTok ? 40 : 5;
            return (double) b *(double) value / 0x10000 * KoeffNPTT;
        }

        public static double GetU(ushort value, double Koeff)
        {
            return(double) value/(double) 0x100* Koeff;
        }

        public static double GetF(ushort value)
        {
            return (double) value/(double) 0x100;
        }

        public static ulong GetTime(ushort value)
        {
            return value < 32768 ? (ulong) value*10 : (ulong) ((value - 32768)*100);
        }

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff)
        {
            float temp = (float) value*(int) koeff/65535;
            return Math.Floor(temp + 0.5)/100;
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            if (value < 0)
            {
                throw new FormatException("������� �� ����� ���� ������ 0");
            }
            return (ushort) (value*65535*100/(int) koeff);
        }

        public static ushort SetTime(ulong value)
        {
            return value < 327680 ? (ushort) (value/10) : (ushort) (value/100 + 32768);
        }
    }
}