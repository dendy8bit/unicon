using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR301
{
    
    public partial class ConfigurationForm : Form, IFormView
    {
        private MR301 _device;
        private bool _validatingOk = true;
        private bool _connectingErrors;

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(MR301 device)
        {
            this._device = device;
            this.InitializeComponent();
            this._groupSelector = new RadioButtonSelector(this._mainRadioBtnGroup, this._reserveRadioBtnGroup, this._groupChangeBtn);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            this.Init();
        }

        private void Init()
        {
            if (Common.VersionConverter(this._device.DeviceVersion) <= 2.15)
            {
                this._blockSDTU_Label.Visible = this._blockStduCombo.Visible = false;
            }
            this._device.InputSignalsLoadOK += new Handler(this._device_InputSignalsLoadOK);
            this._device.InputSignalsLoadFail += new Handler(this._device_InputSignalsLoadFail);
            this._device.OutputSignalsLoadOK += new Handler(this._device_OutputSignalsLoadOK);
            this._device.OutputSignalsLoadFail += new Handler(this._device_OutputSignalsLoadFail);
            this._device.ExternalDefensesLoadOK += new Handler(this._device_ExternalDefensesLoadOK);
            this._device.ExternalDefensesLoadFail += new Handler(this._device_ExternalDefensesLoadFail);
            this._device.AutomaticsPageLoadOK += new Handler(this._device_AutomaticsPageLoadOK);
            this._device.AutomaticsPageLoadFail += new Handler(this._device_AutomaticsPageLoadFail);
            this._device.TokDefensesLoadOK += new Handler(this._device_TokDefensesMainLoadOK);
            this._device.TokDefensesLoadFail += new Handler(this._device_TokDefensesMainLoadFail);

            this.SubscriptOnSaveHandlers();

            this.PrepareInputSignals();
            this.PrepareOutputSignals();
            this.PrepareExternalDefenses();
            this.PrepareAutomaticsSignals();
            this.PrepareTokDefenses();
        }

        void _groupSelector_NeedCopyGroup()
        { 
            //�������� � ���������, ����� ��������
            if (this._groupSelector.SelectedGroup == 0)
            {
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.WriteAutomaticsPage(this._device.AutomaticsReserve);
                this.WriteExternalDefenses(this._device.ExternalDefensesReserve);
                     
            }
            else
            {
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.WriteAutomaticsPage(this._device.AutomaticsMain);
                this.WriteExternalDefenses(this._device.ExternalDefensesMain);  
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR301); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Misc
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg1.ShowDialog())
            {
                try
                {
                    this._device.Deserialize(this._openConfigurationDlg1.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    this._processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ��301 ��� ���������";
                    return;
                }
                this.ShowTokDefenses(this._mainRadioBtnGroup.Checked
                    ? this._device.TokDefensesMain
                    : this._device.TokDefensesReserve);
                this.ShowAutomatics(this._mainRadioBtnGroup.Checked
                    ? this._device.AutomaticsMain
                    : this._device.AutomaticsReserve);
                this.ShowExternalDefenses(this._mainRadioBtnGroup.Checked
                    ? this._device.ExternalDefensesMain
                    : this._device.ExternalDefensesReserve);

                this.ReadInputSignals();
                this.ReadOutputSignals();

                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = "���� " + this._openConfigurationDlg1.FileName + " ��������";
            }
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            this._validatingOk = true;
            if (this.ValidateAll())
            {
                this._saveConfigurationDlg.FileName = string.Format("��301_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._exchangeProgressBar.Value = 0;
                    this._device.SerializeBin(this._saveConfigurationDlg.FileName);
                    this._processLabel.Text = "���� " + this._openConfigurationDlg1.FileName + " ��������";
                }
            }
        }
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._connectingErrors = false;
            this._exchangeProgressBar.Value = 0;
            this._validatingOk = true;
            this.ValidateAll();
            if (this.ValidateAll())
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ ��301 �" + this._device.DeviceNumber + " ?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "���� ������";
                    this._device.SaveOutputSignals();
                    this._device.SaveInputSignals();
                    this._device.SaveExternalDefenses();
                    this._device.SaveTokDefenses();
                    this._device.SaveAutomaticsPage();
                    this._device.ConfirmConstraint();
                }
            }
        }
        private bool ValidateAll()
        {
            //�������� MaskedTextBox
            this.ValidateInputSignals();
            this.ValidateAutomatics();
            //�������� DataGrid
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteOutputSignals()
                                              && this.WriteInputSignals();
                if (this._mainRadioBtnGroup.Checked)
                {
                    this.WriteAutomaticsPage(this._device.AutomaticsMain);
                }
                else
                {
                    this.WriteAutomaticsPage(this._device.AutomaticsReserve);
                }
                this._validatingOk &= this._mainRadioBtnGroup.Checked ? this.WriteTokDefenses(this._device.TokDefensesMain) : this.WriteTokDefenses(this._device.TokDefensesReserve);
                this._validatingOk &= this._mainRadioBtnGroup.Checked ? this.WriteExternalDefenses(this._device.ExternalDefensesMain) : this.WriteExternalDefenses(this._device.ExternalDefensesReserve);
            }
            return this._validatingOk;
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {           
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������...";
            this._connectingErrors = false;
            this._device.LoadOutputSignals();
            this._device.LoadInputSignals();
            this._device.LoadExternalDefenses();
            this._device.LoadAutomaticsPage();
            this._device.LoadTokDefenses();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��301. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��301. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveOk()
        {
            this._exchangeProgressBar.PerformStep();
        }

        private void OnLoadComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";                
            }
            else
            {
                this._processLabel.Text = "������ ������� ���������";    
            }
        }

        private void OnSaveComplete()
        {            
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "������ ������� ���������";
            }
        }
        void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                //combo.SelectedIndex = 0;
                if (combo.Items[combo.Items.Count - 1].ToString() == "XXXXX")
                {
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartRead();
        }

        private void SubscriptOnSaveHandlers()
        {
            this._device.OutputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.OutputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.InputSignalsSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.InputSignalsSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.ExternalDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.ExternalDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.AutomaticsPageSaveOK += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.AutomaticsPageSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.TokDefensesSaveOK += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveOk));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.TokDefensesSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                }
                catch (InvalidOperationException)
                { }
            };
        }
                                                   
        void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    this.ShowToolTip(box);
                    
                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        this.ShowToolTip(box);
                    }
                }
                
            }
        }
        
        private void ShowToolTip(MaskedTextBox box)
        {
            if (this._validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    this._tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }
                this._toolTip.ShowAlways = true;
                this._toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                this._validatingOk = false;
            }
        }

        private void ShowToolTip(string msg, DataGridViewCell cell,TabPage page)
        {
            try
            {
                cell.OwningColumn.DataGridView.CurrentCell.Selected = false;
            }
            catch(Exception e)
            {

            }finally
            {
                if (this._validatingOk)
                {
                    Validator.ShowControl(cell.OwningColumn.DataGridView);
                    cell.OwningColumn.DataGridView.ShowCellToolTips = false;
                    var cellDisplayRect = cell.OwningColumn.DataGridView.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false);
                    this._toolTip.Show(msg,
                                  cell.OwningColumn.DataGridView,
                                  cellDisplayRect.X + cell.Size.Width / 2,
                                  cellDisplayRect.Y + cell.Size.Height / 2,
                                  2000);

                    cell.Selected = true;
                    this._validatingOk = false;
                }
            }

            
        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(this.Combo_DropDown);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }

        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                combos[i].SelectedIndex = 0;
            }
        }

        void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes,Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }
        #endregion

        #region �������� �������
        void _device_OutputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_OutputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadOutputSignals));
            }
            catch (InvalidOperationException)
            { }
        }

        private void PrepareOutputSignals()
        {
            this._outputReleGrid.Rows.Clear();
            this._releSignalCol.Items.Clear();
            this._releSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outIndSignalCol.Items.Clear();
            this._outIndSignalCol.Items.AddRange(Strings.All.ToArray());
            this._outputLogicCheckList.Items.Clear();
            this._outputLogicCheckList.Items.AddRange(Strings.OutputSignals.ToArray());
            this._outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < MR301.COutputRele.COUNT-3; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i +1, "�����������","���", 0 });
            }
            this._outputIndicatorsGrid.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputReleGrid.EditingControlShowing +=new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR301.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            "�����������",
                                                            "���",
                                                            false,
                                                            false,
                                                            false});
            }

            

            
        }
        
        private void ReadOutputSignals()
        {
            this._exchangeProgressBar.PerformStep();
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < MR301.COutputRele.COUNT-3; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i +1,this._device.OutputRele[i].Type,this._device.OutputRele[i].SignalString,this._device.OutputRele[i].ImpulseUlong});
            }
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < MR301.COutputIndicator.COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{i + 1,
                                                            this._device.OutputIndicator[i].Type,
                                                            this._device.OutputIndicator[i].SignalString,
                                                            this._device.OutputIndicator[i].Indication,
                                                            this._device.OutputIndicator[i].Alarm,
                                                            this._device.OutputIndicator[i].System});
            }
            this._outputLogicCombo.SelectedIndex = 0;
            for (int i = 0; i < this._device.DispepairReleConfiguration.Count; i++)
            {
                this._dispepairCheckList.SetItemChecked(i, this._device.DispepairReleConfiguration[i]);
            }
            this._dispepairReleDurationBox.Text = this._device.DispepairReleDuration.ToString();

            //�������������

            this.ShowOutputLogicSignals(0);
            
            
        }

        private void ShowOutputLogicSignals(int channel)
        {
            BitArray bits = this._device.GetOutputLogicSignals(channel);
            for (int i = 0; i < bits.Count; i++)
            {
                this._outputLogicCheckList.SetItemChecked(i, bits[i]);
            }
        }

        private bool WriteOutputSignals()
        {
            bool ret = true;
          
            for (int i = 0; i < this._outputReleGrid.Rows.Count; i++)
            {
                this._device.OutputRele[i].Type = this._outputReleGrid["_releTypeCol", i].Value.ToString();
                this._device.OutputRele[i].SignalString = this._outputReleGrid["_releSignalCol", i].Value.ToString();
                try
                {
                    ulong value = ulong.Parse(this._outputReleGrid["_releImpulseCol", i].Value.ToString());
                    if (value > 3000000)
                    {
                        this.ShowToolTip("������� ����� ����� < 3000000", this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                        ret = false;
                    }
                    else
                    {
                        this._device.OutputRele[i].ImpulseUlong = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR301.TIMELIMIT_ERROR_MSG, this._outputReleGrid["_releImpulseCol", i], this._outputSignalsPage);
                    ret = false;
                }
                
            }
                   
            for (int i = 0; i < this._outputIndicatorsGrid.Rows.Count; i++)
            {
                this._device.OutputIndicator[i].Type = this._outputIndicatorsGrid["_outIndTypeCol", i].Value.ToString();
                this._device.OutputIndicator[i].SignalString = this._outputIndicatorsGrid["_outIndSignalCol", i].Value.ToString();
                this._device.OutputIndicator[i].Indication = (bool)this._outputIndicatorsGrid["_outIndResetCol", i].Value;
                this._device.OutputIndicator[i].Alarm = (bool)this._outputIndicatorsGrid["_outIndAlarmCol", i].Value;
                this._device.OutputIndicator[i].System = (bool)this._outputIndicatorsGrid["_outIndSystemCol", i].Value;
            }
      
            CheckedListBoxManager checkManager = new CheckedListBoxManager();
            checkManager.CheckList = this._dispepairCheckList;
             this._device.DispepairReleConfiguration = checkManager.ToBitArray();
             this._device.DispepairReleDuration = ulong.Parse(this._dispepairReleDurationBox.Text);


            return ret;
        }

        private void _outputLogicCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowOutputLogicSignals(this._outputLogicCombo.SelectedIndex);
        }

        private void _outputLogicAcceptBut_Click(object sender, EventArgs e)
        {
            BitArray bits = new BitArray(104);
            for (int i = 0; i < this._outputLogicCheckList.Items.Count; i++)
            {
                bits[i] = this._outputLogicCheckList.GetItemChecked(i);
            }
            this._device.SetOutputLogicSignals(this._outputLogicCombo.SelectedIndex, bits);
        }
        #endregion

        #region ������� ������

        void _device_ExternalDefensesLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_ExternalDefensesLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnExternalDefensesLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        void OnExternalDefensesLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainRadioBtnGroup.Checked)
            {                
                this.ShowExternalDefenses(this._device.ExternalDefensesMain);
            }
            else
            {
                this.ShowExternalDefenses(this._device.ExternalDefensesReserve);
            }
        }

        //private void _externalDefensesMainRadio_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (_externalDefensesMainRadio.Checked)
        //    {
        //        WriteExternalDefenses(_device.ExternalDefensesReserve);
        //        ShowExternalDefenses(_device.ExternalDefensesMain);
        //    }
        //    else
        //    {
        //        WriteExternalDefenses(_device.ExternalDefensesMain);
        //        ShowExternalDefenses(_device.ExternalDefensesReserve);
        //    }
        //}
        private void PrepareExternalDefenses()
        {
            this._externalDefenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            //_exDefModeCol.Items.Clear();
            //_exDefBlockingCol.Items.Clear();
            //_exDefEntryCol.Items.Clear();
            if (this._exDefBlockingCol.Items.Count == 0)
            {
                this._exDefBlockingCol.Items.AddRange(Strings.Logic.ToArray());
            }
            if (this._exDefModeCol.Items.Count == 0)
            {
                this._exDefModeCol.Items.AddRange(Strings.Modes2.ToArray());
            }
            if (this._exDefEntryCol.Items.Count == 0)
            {
                this._exDefEntryCol.Items.AddRange(Strings.ExternalDefense.ToArray());
            }

            this.ShowExternalDefenses(this._device.ExternalDefensesMain);
            for (int i = 0; i < MR301.CExternalDefenses.COUNT; i++)
            {
                this.OnExternalDefenseGridModeChanged(i);
            }
            this._externalDefenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._externalDefenseGrid_CellStateChanged);
            
        }

        private bool OnExternalDefenseGridModeChanged(int row)
        {
            int[] columns = new int[this._externalDefenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(this._externalDefenseGrid, row, "��������", 1, columns);
        }

        void _externalDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (this._exDefModeCol == e.Cell.OwningColumn )
            {
                this.OnExternalDefenseGridModeChanged(e.Cell.RowIndex);
            }
        }

        private void ShowExternalDefenses(MR301.CExternalDefenses externalDefenses)
        {
            
            this._externalDefenseGrid.Rows.Clear();
            //_exDefModeCol.Items.Clear();
            //_exDefBlockingCol.Items.Clear();
            //_exDefEntryCol.Items.Clear();

            for (int i = 0; i < MR301.CExternalDefenses.COUNT; i++)
            {
                this._externalDefenseGrid.Rows.Add(new object[]{externalDefenses[i].Name,
                                                           externalDefenses[i].Mode,
                                                           externalDefenses[i].BlockingNumber,
                                                           externalDefenses[i].Entry,
                                                           externalDefenses[i].WorkingTime,
                                                           externalDefenses[i].APV,
                                                           externalDefenses[i].UROV
                                                           });
                this.OnExternalDefenseGridModeChanged(i);
            }
        }

        private bool WriteExternalDefenses(MR301.CExternalDefenses externalDefenses)
        {
            bool ret = true;
            if (externalDefenses.Count == MR301.CExternalDefenses.COUNT && this._externalDefenseGrid.Rows.Count <= MR301.CExternalDefenses.COUNT)
            {
                for (int i = 0; i < this._externalDefenseGrid.Rows.Count; i++)
                {
                   externalDefenses[i].Mode = this._externalDefenseGrid["_exDefModeCol", i].Value.ToString();
                   bool enabled = externalDefenses[i].Mode != "��������";
                   //if (enabled)
                   //{
                       externalDefenses[i].BlockingNumber = this._externalDefenseGrid["_exDefBlockingCol", i].Value.ToString();
                       externalDefenses[i].Entry = this._externalDefenseGrid["_exDefEntryCol", i].Value.ToString();
                       externalDefenses[i].UROV = (bool)this._externalDefenseGrid["_exDefUROVcol", i].Value;
                       externalDefenses[i].APV = (bool)this._externalDefenseGrid["_exDefAPVcol", i].Value;

                       try
                       {
                           ulong value = 0;
                           if (this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value != null)
                           {
                               value = ulong.Parse(this._externalDefenseGrid["_exDefWorkingTimeCol", i].Value.ToString());
                           }
                           if (value > 3000000)
                           {
                               this.ShowToolTip("������� ����� < 3000000", this._externalDefenseGrid["_exDefWorkingTimeCol", i], this._externalNewTabPage);
                               ret = false;
                           }
                           else
                           {
                               externalDefenses[i].WorkingTime = value;
                           }
                       }
                       catch (Exception)
                       {
                           this.ShowToolTip(MR301.TIMELIMIT_ERROR_MSG, this._externalDefenseGrid["_exDefWorkingTimeCol", i], this._externalNewTabPage);
                           ret = false;
                       }
                   //}
                }

            }
            return ret;
        }

        #endregion

        #region ������� �������

        private void ReadInputSignals()
        {
            this.ClearCombos(this._inputSignalsCombos);
            this.FillInputSignalsCombos();
            //SubscriptCombos(_inputSignalsCombos);
            this._exchangeProgressBar.PerformStep();

            //�������� ������� �������
            this._TT_Box.Text = this._device.TT.ToString();
            this._TTNP_Box.Text = this._device.TTNP.ToString();
            double res;
            if (this._device.MaxTok > 24)
            {
                res = this._device.MaxTok - 0.01;
            }else
            {
                res = this._device.MaxTok;
            }
            this._maxTok_Box.Text = res.ToString();
            //_maxTok_Box.Text = _device.MaxTok.ToString();
                       
            //������� �������                            
            this.ComboError(this._keyOffCombo, this._device.KeyOff);
            this.ComboError(this._keyOnCombo, this._device.KeyOn);
            this.ComboError(this._extOffCombo, this._device.ExternalOff);
            this.ComboError(this._extOnCombo, this._device.ExternalOn);
            this.ComboError(this._constraintGroupCombo, this._device.ConstraintGroup);
            this.ComboError(this._indicationCombo, this._device.IndicationReset);
            this.ComboError(this._blockStduCombo,this._device.BlockStdu);
            
            //����������� �������
            this.ComboError(this._manageSignalsSDTU_Combo, this._device.ManageSignalSDTU);
            this.ComboError(this._manageSignalsKeyCombo, this._device.ManageSignalKey);
            this.ComboError(this._manageSignalsExternalCombo, this._device.ManageSignalExternal);
            //_manageSignalsButtonCombo.SelectedItem = _device.ManageSignalButton;
            
            //��������� �����
            this.ComboError(this._speedupNumberCombo, this._device.SpeedupNumber);
           // if (_device.SpeedupOn == "���������")
          //{
          //    _speedupOnCombo.SelectedItem = "���������";
          //}
          //else
          //{
          //    _speedupOnCombo.SelectedItem = "���������";
          //}
            this.ComboError(this._speedupOnCombo, this._device.SpeedupOn);
            this._speedupTimeBox.Text = this._device.SpeedupTime.ToString();

            this.ComboError(this._switcherBlockCombo, this._device.SwitcherBlock);
            this.ComboError(this._switcherErrorCombo, this._device.SwitcherError);
            this.ComboError(this._switcherStateOffCombo, this._device.SwitcherOff);
            this.ComboError(this._switcherStateOnCombo, this._device.SwitcherOn);

            this._switcherImpulseBox.Text = this._device.SwitcherImpulse.ToString();
            this._switcherTimeBox.Text = this._device.SwitcherTime.ToString();

            //���������� �������        
            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ComboError(ComboBox combo, string item) 
        {
            try
            {
                combo.SelectedItem = item;
            }
            catch
            {
                if (combo.Items.Contains("XXXXX"))
                {
                    combo.SelectedItem = "XXXXX";
                }
                else
                {
                    combo.Items.Add("XXXXX");
                    combo.SelectedItem = "XXXXX";
                }
            }
        }

        void _device_InputSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_InputSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadInputSignals));
                //ReadInputSignals();
            }
            catch (InvalidOperationException fdf) 
            {
                //MessageBox.Show("��������� ������� �������.\n\n" + fdf.ToString(), "������ ������");
            }

        }

        private ComboBox[] _inputSignalsCombos;
        private MaskedTextBox[] _inputSignalsDoubleMaskBoxes;
        private MaskedTextBox[] _inputSignalsUlongMaskBoxes;
                       
        private void PrepareInputSignals()
        {
            this._inputSignalsCombos = new ComboBox[] { this._extOffCombo,this._extOnCombo,this._constraintGroupCombo,
                                                  this._keyOffCombo,this._keyOnCombo,this._indicationCombo,this._blockStduCombo,
                                                  this._switcherBlockCombo,this._switcherErrorCombo,this._switcherStateOffCombo,
                                                  this._switcherStateOnCombo,this._speedupOnCombo,this._speedupNumberCombo,
                                                  /*_manageSignalsButtonCombo,*/this._manageSignalsExternalCombo,this._manageSignalsSDTU_Combo,this._manageSignalsKeyCombo};
            this._inputSignalsDoubleMaskBoxes = new MaskedTextBox[] { this._maxTok_Box };
            this._inputSignalsUlongMaskBoxes = new MaskedTextBox[] { this._TTNP_Box, this._TT_Box, this._switcherTimeBox, this._switcherImpulseBox,this._speedupTimeBox,this._dispepairReleDurationBox };
            

            this.FillInputSignalsCombos();

            this._logicChannelsCombo.SelectedIndex = 0;
            this.SubscriptCombos(this._inputSignalsCombos);

            this.PrepareMaskedBoxes(this._inputSignalsDoubleMaskBoxes, typeof(double));
            this.PrepareMaskedBoxes(this._inputSignalsUlongMaskBoxes, typeof(ulong));
        }

        private void FillInputSignalsCombos()
        {
            this._extOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._extOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._constraintGroupCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._keyOnCombo.Items.AddRange(Strings.Logic.ToArray());
            this._indicationCombo.Items.AddRange(Strings.Logic.ToArray());
            this._blockStduCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherBlockCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherErrorCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOffCombo.Items.AddRange(Strings.Logic.ToArray());
            this._switcherStateOnCombo.Items.AddRange(Strings.Logic.ToArray());
            //_manageSignalsButtonCombo.Items.AddRange(Strings.Forbidden.ToArray());
            this._manageSignalsKeyCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsExternalCombo.Items.AddRange(Strings.Control.ToArray());
            this._manageSignalsSDTU_Combo.Items.AddRange(Strings.Forbidden.ToArray());
            this._speedupNumberCombo.Items.AddRange(Strings.Logic.ToArray());
            this._speedupOnCombo.Items.AddRange(Strings.Forbidden.ToArray());
        }
        
        private void _logicChannelsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowInputLogicSignals(this._logicChannelsCombo.SelectedIndex);
        }

        private void ShowInputLogicSignals(int channel)
        {
            this._logicSignalsDataGrid.Rows.Clear();
            LogicState[] logicSignals = this._device.GetInputLogicSignals(channel);
            for (int i = 0; i < 8; i++)
            {               
                this._logicSignalsDataGrid.Rows.Add(new object[] { i + 1, logicSignals[i].ToString() });
            }

        }

        private void ValidateInputSignals()
        {
            this.ValidateMaskedBoxes(this._inputSignalsUlongMaskBoxes);
            this.ValidateMaskedBoxes(this._inputSignalsDoubleMaskBoxes);
        }

        private bool WriteInputSignals()
        {
            this._device.TT = UInt16.Parse(this._TT_Box.Text);
            this._device.TTNP = UInt16.Parse(this._TTNP_Box.Text);
            if (Double.Parse(this._maxTok_Box.Text) > 24)
            {this._device.MaxTok = Double.Parse(this._maxTok_Box.Text) + 0.01; }else{this._device.MaxTok = Double.Parse(this._maxTok_Box.Text);}
            //_device.MaxTok = Double.Parse(_maxTok_Box.Text);
            this._device.SwitcherImpulse = UInt64.Parse(this._switcherImpulseBox.Text);
            this._device.SwitcherTime = UInt64.Parse(this._switcherTimeBox.Text);
                            
            this._device.ExternalOff = this._extOffCombo.SelectedItem.ToString();
            this._device.ExternalOn = this._extOnCombo.SelectedItem.ToString();
            this._device.ConstraintGroup = this._constraintGroupCombo.SelectedItem.ToString();
            this._device.KeyOff = this._keyOffCombo.SelectedItem.ToString();
            this._device.KeyOn = this._keyOnCombo.SelectedItem.ToString();
            this._device.IndicationReset = this._indicationCombo.SelectedItem.ToString();
            this._device.BlockStdu = this._blockStduCombo.SelectedItem.ToString();
                        
            //_device.ManageSignalButton = _manageSignalsButtonCombo.SelectedItem.ToString();
            this._device.ManageSignalExternal = this._manageSignalsExternalCombo.SelectedItem.ToString();
            this._device.ManageSignalKey = this._manageSignalsKeyCombo.SelectedItem.ToString();
            this._device.ManageSignalSDTU = this._manageSignalsSDTU_Combo.SelectedItem.ToString();

            this._device.SpeedupNumber = this._speedupNumberCombo.SelectedItem.ToString();
            this._device.SpeedupOn = this._speedupOnCombo.SelectedItem.ToString();
            this._device.SpeedupTime = ulong.Parse(this._speedupTimeBox.Text);

            this._device.SwitcherBlock = this._switcherBlockCombo.SelectedItem.ToString();
            this._device.SwitcherError = this._switcherErrorCombo.SelectedItem.ToString();
            this._device.SwitcherOff = this._switcherStateOffCombo.SelectedItem.ToString();
            this._device.SwitcherOn = this._switcherStateOnCombo.SelectedItem.ToString();

            return true;
                         
        }
        
        private void _applyLogicSignalsBut_Click(object sender, EventArgs e)
        {
            LogicState[] logicSignals = new LogicState[8];
            for (int i = 0; i < this._logicSignalsDataGrid.Rows.Count; i++)
            {
                object o = Enum.Parse(typeof(LogicState),this._logicSignalsDataGrid["_diskretValueCol", i].Value.ToString());
                logicSignals[i] = (LogicState)(o);
            }
            this._device.SetInputLogicSignals(this._logicChannelsCombo.SelectedIndex,logicSignals);
        }
        #endregion

        #region �������� ����������
        void _device_AutomaticsPageLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void _device_AutomaticsPageLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAutomaticsPageLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;
        private RadioButtonSelector _groupSelector;

        private void ValidateAutomatics()
        {
            this.ValidateMaskedBoxes(this._automaticsMaskedBoxes);
        }

        private void PrepareAutomaticsSignals()
        {
            this._APV_ConfigCombo.SelectedIndexChanged += new EventHandler(this._APV_ConfigCombo_SelectedIndexChanged);
            this._ACR_EntryCombo.SelectedIndexChanged += new EventHandler(this._ACR_EntryCombo_SelectedIndexChanged);
            this._CAPV_EntryCombo.SelectedIndexChanged += new EventHandler(this._CAPV_EntryCombo_SelectedIndexChanged);
            this._AVR_StartNumberCombo.SelectedIndexChanged += new EventHandler(this._AVR_StartNumberCombo_SelectedIndexChanged);

            this._automaticCombos = new ComboBox[] {this._APV_SelfresetCombo,this._APV_ConfigCombo,this._APV_BlockingCombo,
                                               this._ACR_BlockingCombo,this._ACR_EntryCombo,
                                               this._CAPV_BlockingCombo,this._CAPV_EntryCombo,
                                               this._AVR_BlockingCombo,this._AVR_ReturnNumberCombo,this._AVR_StartNumberCombo};
            this._automaticsMaskedBoxes = new MaskedTextBox[]{this._APV_Crat1TimeBox,this._APV_Crat2TimeBox,this._APV_BlockingTimeBox,this._APV_ReadyTimeBox,
                                                         this._ACR_TimeBox,
                                                         this._CAPV_TimeBox,
                                                         this._AVR_OffTimeBox,this._AVR_ReturnTimeBox,this._AVR_StartTimeBox};
            this.PrepareMaskedBoxes(this._automaticsMaskedBoxes, typeof(ulong));
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);

        }

        void _AVR_StartNumberCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._AVR_BlockingCombo.Enabled = this._AVR_OffTimeBox.Enabled = this._AVR_ReturnNumberCombo.Enabled =
            this._AVR_ReturnTimeBox.Enabled = this._AVR_StartTimeBox.Enabled = ("���" != this._AVR_StartNumberCombo.Text);
        }

        void _CAPV_EntryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._CAPV_BlockingCombo.Enabled = this._CAPV_TimeBox.Enabled = ("���" != this._CAPV_EntryCombo.Text);
        }

        void _ACR_EntryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._ACR_BlockingCombo.Enabled = this._ACR_TimeBox.Enabled = ("���" != this._ACR_EntryCombo.Text);
        }

        void _APV_ConfigCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._APV_ReadyTimeBox.Enabled = this._APV_BlockingCombo.Enabled = this._APV_BlockingTimeBox.Enabled =
            this._APV_Crat1TimeBox.Enabled = this._APV_Crat2TimeBox.Enabled = this._APV_ReadyTimeBox.Enabled = this._APV_SelfresetCombo.Enabled =
            ("��������" != this._APV_ConfigCombo.Text);
        }

        private void FillAutomaticCombo()
        {
            this._APV_BlockingCombo.Items.AddRange(Strings.Logic.ToArray());
            this._APV_ConfigCombo.Items.AddRange(Strings.Crat.ToArray());
            this._APV_SelfresetCombo.Items.AddRange(Strings.YesNo.ToArray());

            this._CAPV_BlockingCombo.Items.AddRange(Strings.Logic.ToArray());
            this._CAPV_EntryCombo.Items.AddRange(Strings.Logic.ToArray());

            this._ACR_BlockingCombo.Items.AddRange(Strings.Logic.ToArray());
            this._ACR_EntryCombo.Items.AddRange(Strings.Logic.ToArray());

            this._AVR_BlockingCombo.Items.AddRange(Strings.Logic.ToArray());
            this._AVR_ReturnNumberCombo.Items.AddRange(Strings.Logic.ToArray());
            this._AVR_StartNumberCombo.Items.AddRange(Strings.Logic.ToArray());
        }


        private void OnAutomaticsPageLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            this.ClearCombos(this._automaticCombos);
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);
            if (this._mainRadioBtnGroup.Checked)
            {             
                this.ShowAutomatics(this._device.AutomaticsMain);
            }
            else
            {
                this.ShowAutomatics(this._device.AutomaticsReserve);
            }

        }

        private void ShowAutomatics(MR301.CAutomatics automatics)
        {
            //���
            this._APV_ConfigCombo.Text = automatics.APV_Cnf;
            this._APV_BlockingCombo.Text = automatics.APV_Blocking;
            this._APV_SelfresetCombo.Text = automatics.APV_Start;

            this._APV_BlockingTimeBox.Text = automatics.APV_Time_Blocking.ToString();
            this._APV_ReadyTimeBox.Text = automatics.APV_Time_Ready.ToString();
            this._APV_Crat1TimeBox.Text = automatics.APV_Time_1Krat.ToString();
            this._APV_Crat2TimeBox.Text = automatics.APV_Time_2Krat.ToString();

            //����
            this._CAPV_BlockingCombo.Text = automatics.CAPV_Blocking;
            this._CAPV_EntryCombo.Text = automatics.CAPV_Entry;
            this._CAPV_TimeBox.Text = automatics.CAPV_Time.ToString();

            //���
            this._ACR_BlockingCombo.Text = automatics.ACR_Blocking;
            this._ACR_EntryCombo.Text = automatics.ACR_Entry;
            this._ACR_TimeBox.Text = automatics.ACR_Time.ToString();

            //���
            this._AVR_BlockingCombo.Text = automatics.AVR_Blocking;
            this._AVR_ReturnNumberCombo.Text = automatics.AVR_ReturnNumber;
            this._AVR_StartNumberCombo.Text = automatics.AVR_StartNumber;

            this._AVR_OffTimeBox.Text = automatics.AVR_OffTime.ToString();
            this._AVR_ReturnTimeBox.Text = automatics.AVR_ReturnTime.ToString();
            this._AVR_StartTimeBox.Text = automatics.AVR_StartTime.ToString();
        }

        private void WriteAutomaticsPage(MR301.CAutomatics automatics)
        {
            //����
            automatics.CAPV_Blocking = this._CAPV_BlockingCombo.Text;
            automatics.CAPV_Entry = this._CAPV_EntryCombo.Text;
            automatics.CAPV_Time = ulong.Parse(this._CAPV_TimeBox.Text);
            //���
            automatics.AVR_Blocking = this._AVR_BlockingCombo.Text;
            automatics.AVR_OffTime = ulong.Parse(this._AVR_OffTimeBox.Text);
            automatics.AVR_ReturnNumber = this._AVR_ReturnNumberCombo.Text;
            automatics.AVR_ReturnTime = ulong.Parse(this._AVR_ReturnTimeBox.Text);
            automatics.AVR_StartNumber = this._AVR_StartNumberCombo.Text;
            automatics.AVR_StartTime = ulong.Parse(this._AVR_StartTimeBox.Text);
            
            //���
            automatics.APV_Blocking = this._APV_BlockingCombo.Text;
            automatics.APV_Cnf = this._APV_ConfigCombo.Text;
            automatics.APV_Start = this._APV_SelfresetCombo.Text;
            automatics.APV_Time_1Krat = ulong.Parse(this._APV_Crat1TimeBox.Text);
            automatics.APV_Time_2Krat = ulong.Parse(this._APV_Crat2TimeBox.Text);
            automatics.APV_Time_Blocking = ulong.Parse(this._APV_BlockingTimeBox.Text);
            automatics.APV_Time_Ready = ulong.Parse(this._APV_ReadyTimeBox.Text);
            
            //���
            automatics.ACR_Blocking = this._ACR_BlockingCombo.Text;
            automatics.ACR_Entry = this._ACR_EntryCombo.Text;
            automatics.ACR_Time = ulong.Parse(this._ACR_TimeBox.Text);
        }
        

        //private void _automaticConstraintGroupRadio_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (_automaticConstraintGroupRadio.Checked)
        //    {
        //        ValidateAutomatics();
        //        if (_validatingOk)
        //        {
        //            WriteAutomaticsPage(_device.AutomaticsReserve);
        //        }
        //        ShowAutomatics(_device.AutomaticsMain);
               
        //    }
        //    else
        //    {
        //        ValidateAutomatics();
        //        if (_validatingOk)
        //        {
        //            WriteAutomaticsPage(_device.AutomaticsMain);
        //        }
        //        ShowAutomatics(_device.AutomaticsReserve);

                
        //    }
        //}
        #endregion

        #region ������� ������

        void ChangeTokDefenseCellDisabling(DataGridView grid, int row)
        {
            if (!this.OnTokDefenseGridModeChanged(grid, row))
            {                
                if (this._tokDefenseGrid1 == grid)
                {
                    GridManager.ChangeCellDisabling(grid, row, "�����������", 5, 7);
                    GridManager.ChangeCellDisabling(grid, row, "���������", 5, 6);
                    GridManager.ChangeCellDisabling(grid, row, false, 10, 11);
                }
                else
                {
                    GridManager.ChangeCellDisabling(grid, row, false, 9, 10);
                    GridManager.ChangeCellDisabling(grid, row, "�����������", 5, 5);
                } 
            }
        }
      

        void ShowTokDefenses(MR301.CTokDefenses tokDefenses)
        {
            this._tokDefenseGrid1.Rows.Clear();
            this._tokDefenseGrid2.Rows.Clear();
            this._tokDefenseGrid3.Rows.Clear();


            for (int i = 0; i < 4; i++)
            {
                this._tokDefenseGrid1.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].Signal,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].Koefficient,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime
                                                       });
                this.ChangeTokDefenseCellDisabling(this._tokDefenseGrid1, i);
            }
            for (int i = 4; i < 6; i++)
            {
                if (tokDefenses[i].Signal == "���������" || tokDefenses[i].Signal == "���������")
                {
                    this._tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].Signal,
                                                       tokDefenses[i].WorkConstraint/8,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime});
                }else
                {
                    this._tokDefenseGrid2.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].Signal,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime});
                }

                this.ChangeTokDefenseCellDisabling(this._tokDefenseGrid2, i - 4);
            }
            for (int i = 6; i < 8; i++)
            {

                this._tokDefenseGrid3.Rows.Add(new object[]{tokDefenses[i].Name,
                                                       tokDefenses[i].Mode,     
                                                       tokDefenses[i].BlockingNumber,
                                                       tokDefenses[i].Signal,
                                                       tokDefenses[i].WorkConstraint,
                                                       tokDefenses[i].Feature,
                                                       tokDefenses[i].WorkTime,
                                                       tokDefenses[i].APV,
                                                       tokDefenses[i].UROV,
                                                       tokDefenses[i].Speedup,
                                                       tokDefenses[i].SpeedupTime});
                this.ChangeTokDefenseCellDisabling(this._tokDefenseGrid3, i - 6);
            }
         
            for (int i = 0; i < this._tokDefenseGrid1.Rows.Count; i++)
            {
                this._tokDefenseGrid1[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid2.Rows.Count; i++)
            {
                this._tokDefenseGrid2[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            for (int i = 0; i < this._tokDefenseGrid3.Rows.Count; i++)
            {
                this._tokDefenseGrid3[0, i].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
        
        }

        void PrepareTokDefenses()
        {
            //_tokDefenseModeCol1.Items.Clear();
            this._tokDefenseModeCol1.Items.AddRange(Strings.Modes.ToArray());
            //_tokDefenseBlockingNumberCol1.Items.Clear();
            this._tokDefenseBlockingNumberCol1.Items.AddRange(Strings.Logic.ToArray());
            //_tokDefenseSignalCol1.Items.Clear();
            this._tokDefenseSignalCol1.Items.AddRange(Strings.TokSignalI.ToArray());
            //_tokDefenseFeatureCol1.Items.Clear();
            this._tokDefenseFeatureCol1.Items.AddRange(Strings.FeatureI.ToArray());

            //_tokDefenseModeCol2.Items.Clear();
            this._tokDefenseModeCol2.Items.AddRange(Strings.Modes.ToArray());
            //_tokDefenseBlockingNumberCol2.Items.Clear();
            this._tokDefenseBlockingNumberCol2.Items.AddRange(Strings.Logic.ToArray());
            //_tokDefenseSignalCol2.Items.Clear();
            this._tokDefenseSignalCol2.Items.AddRange(Strings.TokSignalI0.ToArray());
            //_tokDefenseFeatureCol2.Items.AddRange(Strings.FeatureI02.ToArray());

            //_tokDefenseModeCol3.Items.Clear();
            this._tokDefenseModeCol3.Items.AddRange(Strings.Modes.ToArray());
            //_tokDefenseBlockingCol3.Items.Clear();
            this._tokDefenseBlockingCol3.Items.AddRange(Strings.Logic.ToArray());
            //_tokDefenseSignalCol3.Items.Clear();
            this._tokDefenseSignalCol3.Items.AddRange(Strings.TokSignalI2.ToArray());
            //_tokDefenseFeatureCol3.Items.AddRange(Strings.FeatureI02.ToArray());

            this.ShowTokDefenses(this._device.TokDefensesMain);

            this._tokDefenseGrid1.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid2.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
            this._tokDefenseGrid3.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._tokDefenseGrid_CellStateChanged);
           
            this._tokDefenseGrid1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid2.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._tokDefenseGrid3.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
           
        }

        void _tokDefenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            if (this._tokDefenseModeCol1 == e.Cell.OwningColumn || 
                this._tokDefenseModeCol2 == e.Cell.OwningColumn || 
                this._tokDefenseModeCol3 == e.Cell.OwningColumn || 
                this._tokDefenseFeatureCol1 == e.Cell.OwningColumn || 
                this._tokDefenseSpeedupCol1 == e.Cell.OwningColumn || 
                this._tokDefenseSpeedupCol2 == e.Cell.OwningColumn || 
                this._tokDefenseSpeedupCol3 == e.Cell.OwningColumn 
                )
            {
                this.ChangeTokDefenseCellDisabling(sender as DataGridView, e.Cell.RowIndex);
            }
            
        }

        void _device_TokDefensesMainLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnTokDefenseLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            if (this._mainRadioBtnGroup.Checked)
            {
                this.ShowTokDefenses(this._device.TokDefensesMain);
            }
            else
            {
                this.ShowTokDefenses(this._device.TokDefensesReserve);
            }
         
        }
        void _device_TokDefensesMainLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnTokDefenseLoadOk));
            }
            catch (InvalidOperationException)
            { }
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        private bool OnTokDefenseGridModeChanged(DataGridView grid, int row)
        {
            int[] columns = new int[grid.Columns.Count - 2];
           
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(grid, row, "��������", 1, columns);
            
        }

        private bool WriteTokDefenseItem(MR301.CTokDefenses tokDefenses, DataGridView grid, int rowIndex, int itemIndex)
        {
             bool ret = true;
            tokDefenses[itemIndex].Mode = grid[1, rowIndex].Value.ToString();
            bool enabled = tokDefenses[itemIndex].Mode != "��������";
            //if (enabled)
            //{

                tokDefenses[itemIndex].BlockingNumber = grid[2, rowIndex].Value.ToString();
                tokDefenses[itemIndex].Signal = grid[3, rowIndex].Value.ToString();
                if (grid[3, rowIndex].Value.ToString() == "���������" || grid[3, rowIndex].Value.ToString() == "���������")
                {
                    try
                    {
                        float value = float.Parse(grid[4, rowIndex].Value.ToString());

                        if (value < 0 || value > 5)
                        {
                            this.ShowToolTip("������� ����� � ��������� [0 - 5.0]", grid[4, rowIndex], this._defendingTabPage);
                            ret = false;
                        }
                        else
                        {
                            tokDefenses[itemIndex].WorkConstraint = value * 8;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 5.0]", grid[4, rowIndex], this._defendingTabPage);
                        ret = false;
                    }
                }else
                {
                    try
                    {
                        float value = float.Parse(grid[4, rowIndex].Value.ToString());

                        if (value < 0 || value > 40)
                        {
                            this.ShowToolTip("������� ����� � ��������� [0 - 40.0]", grid[4, rowIndex], this._defendingTabPage);
                            ret = false;
                        }
                        else
                        {
                            tokDefenses[itemIndex].WorkConstraint = value;
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip("������� ����� � ��������� [0 - 40.0]", grid[4, rowIndex], this._defendingTabPage);
                        ret = false;
                    }
                }

                tokDefenses[itemIndex].Feature = grid[5, rowIndex].Value.ToString();
                try
                {
                    ulong value = ulong.Parse(grid[6, rowIndex].Value.ToString());
                    if (value > 3000000)
                    {
                        this.ShowToolTip("������� ����� < 3000000", grid[6, rowIndex], this._defendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].WorkTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip(MR301.TIMELIMIT_ERROR_MSG, grid[6, rowIndex], this._defendingTabPage);
                    ret = false;
                }
                int currentColumn = 7;
                if (this._tokDefenseGrid1 == grid)
                {
                    try
                    {
                        ushort value = ushort.Parse(grid[7, rowIndex].Value.ToString());
                        if ("���������" == tokDefenses[itemIndex].Feature) //��� ����������� ���-��  - ��������
                        {
                            if (value < 800 || value > 4000)
                            {
                                this.ShowToolTip("������� ����� � ��������� [800 - 4000]", grid[7, rowIndex], this._defendingTabPage);
                                ret = false;
                            }
                            else
                            {
                                tokDefenses[itemIndex].Koefficient = value;
                            }
                        }

                    }
                    catch (Exception)
                    {
                        this.ShowToolTip("������� ����� � �������� [800 - 4000]", grid[7, rowIndex], this._defendingTabPage);
                        ret = false;
                    }
                    currentColumn += 1;
                }

                int APV_Column = currentColumn;
                int UROV_Column = currentColumn + 1;
                int SpeedupColumn = currentColumn + 2;
                int SpeedupTimeColumn = currentColumn + 3;


                tokDefenses[itemIndex].APV = bool.Parse(grid[APV_Column, rowIndex].Value.ToString());
                tokDefenses[itemIndex].UROV = bool.Parse(grid[UROV_Column, rowIndex].Value.ToString());
                tokDefenses[itemIndex].Speedup = bool.Parse(grid[SpeedupColumn, rowIndex].Value.ToString());
                try
                {
                    ulong value = ulong.Parse(grid[SpeedupTimeColumn, rowIndex].Value.ToString());
                    if (value > 3000000)
                    {
                        this.ShowToolTip("������� ����� < 3000000", grid[SpeedupTimeColumn, rowIndex], this._defendingTabPage);
                        ret = false;
                    }
                    else
                    {
                        tokDefenses[itemIndex].SpeedupTime = value;
                    }

                }
                catch (Exception)
                {
                    this.ShowToolTip("������� ����� � �������� [0 - 3000000]", grid[SpeedupTimeColumn, rowIndex], this._defendingTabPage);
                    ret = false;
                }
            //}
            return ret;
        }

        private bool WriteTokDefenses(MR301.CTokDefenses tokDefenses)
        {
            bool ret = true;

            for (int i = 0; i < 8; i++)
            {
                if (i >= 0 && i < 4)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid1, i, i);
                }
                if (i >= 4 && i < 6)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid2, i - 4, i);
                }
                if (i >= 6 && i < 8)
                {
                    ret &= this.WriteTokDefenseItem(tokDefenses, this._tokDefenseGrid3, i - 6, i);
                }
            }

            return ret;
        }

        //private void _tokDefenseMainConstraintRadio_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (_tokDefenseMainConstraintRadio.Checked)
        //    {
        //        WriteTokDefenses(_device.TokDefensesReserve);
        //        ShowTokDefenses(_device.TokDefensesMain);
        //    }
        //    else
        //    {
        //        WriteTokDefenses(_device.TokDefensesMain);
        //        ShowTokDefenses(_device.TokDefensesReserve);
        //    }
        //}

  
      
        #endregion
        
        private void deSelAllOutLogic_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this._outputLogicCheckList.Items.Count; i++)
            {
                this._outputLogicCheckList.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        private void deSelAllNeispr_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this._dispepairCheckList.Items.Count; i++)
            {
                this._dispepairCheckList.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            if (this.ValidateAll())
            {
                this._processLabel.Text = HtmlExport.Export(Resources.MR301Main, Resources.MR301Res, this._device, "��301", this._device.DeviceVersion);
            }
        }

        private void _maintRadioBtnGroup_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioBtnGroup.Checked)
            {
                this.WriteTokDefenses(this._device.TokDefensesReserve);
                this.ShowTokDefenses(this._device.TokDefensesMain);
            }
            else
            {
                this.WriteTokDefenses(this._device.TokDefensesMain);
                this.ShowTokDefenses(this._device.TokDefensesReserve);
            }

            if (this._mainRadioBtnGroup.Checked)
            {
                this.ValidateAutomatics();
                if (this._validatingOk)
                {
                    this.WriteAutomaticsPage(this._device.AutomaticsReserve);
                }
                this.ShowAutomatics(this._device.AutomaticsMain);
            }
            else
            {
                this.ValidateAutomatics();
                if (this._validatingOk)
                {
                    this.WriteAutomaticsPage(this._device.AutomaticsMain);
                }
                this.ShowAutomatics(this._device.AutomaticsReserve);
            }
            if (this._mainRadioBtnGroup.Checked)
            {
                this.WriteExternalDefenses(this._device.ExternalDefensesReserve);
                this.ShowExternalDefenses(this._device.ExternalDefensesMain);
            }
            else
            {
                this.WriteExternalDefenses(this._device.ExternalDefensesMain);
                this.ShowExternalDefenses(this._device.ExternalDefensesReserve);
            }
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }

        private void Mr301ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}
