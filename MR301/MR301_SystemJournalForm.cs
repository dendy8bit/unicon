using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms.Export;
using BEMN.Interfaces;

namespace BEMN.MR301
{
    public partial class SystemJournalForm : Form, IFormView
    {
        private MR301 _device;
        
        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR301 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._device.SysJournalLoadOK += HandlerHelper.CreateHandler(this, this.SysJournalLoadOk);
            this._device.SysJournalLoadFail += HandlerHelper.CreateHandler(this, this.SysJournalLoadFail);
            this._device.SysJournalSaveOK += HandlerHelper.CreateHandler(this, this._device.LoadSystemJournal);
        }

        private void OnSysJournalIndexLoadFail()
        {
            this._journalCntLabel.Text = "������ ������ �������";
        }
        

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR301); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return AssemblyResources.Resources.js; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��301_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(this._sysJournalGrid["_indexCol", i].Value, this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value);
            }

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��301_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(table.Rows[i].ItemArray[0], table.Rows[i].ItemArray[1], table.Rows[i].ItemArray[2]);
            }
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.SaveSystemJournalIndex();
                this._readSysJournalBut.Enabled = false;
                this._sysJournalGrid.Rows.Clear();
            }
        }

        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void SysJournalLoadFail()
        {
            if (this._device.SysJournalMessage != "��������� ���������")
            {
                this.OnSysJournalIndexLoadFail();
                this._device.LoadSystemJournal();
            }
            else
            {
                this.SysJournalLoaded();
            }
        }

        private void SysJournalLoadOk()
        {
            try
            {
                if (this._device.SysJournalMessage != "������ ����")
                {
                    this._sysJournalGrid.Rows.Add(this._device.SysJournalMessageIndex.ToString(), this._device.SysJournalRecTime, this._device.SysJournalMessage);
                    this._sysJournalGrid.Invalidate();
                    this._journalCntLabel.Text = this._device.SysJournalMessageIndex.ToString();
                    this._journalCntLabel.Invalidate();
                    this._device.LoadSystemJournal();
                }
                else
                {
                    this.SysJournalLoaded();
                }
            }
            catch
            {
            }
        }

        private void SysJournalLoaded()
        {
            if (this._sysJournalGrid.Rows.Count != 0)
            {
                this._readSysJournalBut.Enabled = true;
                MessageBox.Show("��������� ������ ��������");
            }
            else
            {
                this._readSysJournalBut.Enabled = true;
                MessageBox.Show("��������� ������ ����");
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                DataTable table = new DataTable("��301_������_�������");
                table.Columns.Add("�����");
                table.Columns.Add("�����");
                table.Columns.Add("���������");
                for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
                {
                    table.Rows.Add(this._sysJournalGrid["_indexCol", i].Value, this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value);
                }
                HtmlExport.Export(table, this._saveJournalHtmlDialog.FileName, Resources.MR301SJ);
            }
        }
    }
}