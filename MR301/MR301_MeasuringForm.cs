using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR301
{
    public partial class MeasuringForm : Form , IFormView
    {
        private MR301 _device;
        private LedControl[] _manageLeds;
        private LedControl[] _autoLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _externalSignalsLeds;
        private Timer _timer = new Timer();

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(MR301 device)
        {
            this.InitializeComponent();
            this._device = device;
            this.Init();
            this._device.ConnectionModeChanged += this.StartStopRead;
        }

        private void Init()
        {
            this._dateTimeBox.MR700Flag = true;
            this._manageLeds = new LedControl[]{this._manageLed2,this._manageLed4,
                                           this._manageLed5,this._manageLed6,this._manageLed7,this._manageLed8};
            this._externalSignalsLeds = new LedControl[] {this._extDefenseLed1,this._extDefenseLed2,this._extDefenseLed3,this._extDefenseLed4,
                                                     this._extDefenseLed5,this._extDefenseLed6,this._extDefenseLed7,this._extDefenseLed8};
            this._indicatorLeds = new LedControl[]{this._indLed1,this._indLed2,this._indLed3,this._indLed4,
                                              this._indLed5,this._indLed6,this._indLed7,this._indLed8};
            this._inputLeds = new LedControl[] { this._inD_led1,this._inD_led2,this._inD_led3,this._inD_led4,
                                            this._inD_led5,this._inD_led6,this._inD_led7,this._inD_led8,
                                            this._inL_led1,this._inL_led2,this._inL_led3,this._inL_led4,
                                            this._inL_led5,this._inL_led6,this._inL_led7,this._inL_led8};
            this._outputLeds = new LedControl[]{this._outLed1,this._outLed2,this._outLed3,this._outLed4,
                                            this._outLed5,this._outLed6,this._outLed7,this._outLed8};
            this._releLeds = new LedControl[]{this._releLed1,this._releLed2,this._releLed3,this._releLed4,
                                         this._releLed5,this._releLed6,this._releLed7,this._releLed8};
            this._limitLeds = new LedControl[] { this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4,
                                            this._ImaxLed5,this._ImaxLed6,this._ImaxLed7,this._ImaxLed8,
                                            
                                            this._I0maxLed5,this._I0maxLed6,this._I0maxLed7,this._I0maxLed8,
                                            this._I0maxLed1,this._I0maxLed2,this._I0maxLed3,this._I0maxLed4};

            this._faultStateLeds = new LedControl[]{this._faultStateLed1,this._faultStateLed2,this._faultStateLed3,this._faultStateLed5,
                                               this._faultStateLed7,this._faultStateLed6,this._faultStateLed4,this._faultStateLed8};
            this._faultSignalsLeds = new LedControl[]{this._faultSignalLed1,this._faultSignalLed2,this._faultSignalLed3,this._faultSignalLed4,
                                                 this._faultSignalLed5,this._faultSignalLed6,this._faultSignalLed7,this._faultSignalLed8,
                                                 this._faultSignalLed9,this._faultSignalLed10,this._faultSignalLed11,this._faultSignalLed12,
                                                 this._faultSignalLed13,this._faultSignalLed14,this._faultSignalLed15,this._faultSignalLed16};
            this._autoLeds = new LedControl[]{this._autoLed1,this._autoLed2,this._autoLed3,this._autoLed4,
                                         this._autoLed5,this._autoLed6,this._autoLed7,this._autoLed8};

            this._device.DiagnosticLoadOk += new Handler(this._device_DiagnosticLoadOk);
            this._device.DiagnosticLoadFail += new Handler(this._device_DiagnosticLoadFail);
            this._device.DateTimeLoadOk += new Handler(this._device_DateTimeLoadOk);
            this._device.DateTimeLoadFail += new Handler(this._device_DateTimeLoadFail);
            this._device.AnalogSignalsLoadOK += new Handler(this._device_AnalogSignalsLoadOK);
            this._device.AnalogSignalsLoadFail += new Handler(this._device_AnalogSignalsLoadFail);

            this._timer.Interval = 100;
            this._timer.Tick += new EventHandler(this._timer_Tick);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR301); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticQuick();
            this._device.MakeDateTimeQuick();
            this._device.MakeAnalogSignalsQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._device.MakeDiagnosticSlow();
            this._device.MakeDateTimeSlow();
            this._device.MakeAnalogSignalsSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveDiagnostic();
            this._device.RemoveDateTime();
            this._device.RemoveAnalogSignals();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.LoadAnalogSignalsCycle();
                this._device.LoadDiagnosticCycle();
                this._device.LoadTimeCycle();
            }
            else
            {
                this._device.RemoveDiagnostic();
                this._device.RemoveDateTime();
                this._device.RemoveAnalogSignals();
                this.OnAnalogSignalsLoadFail();
                this.OnDiagnosticLoadFail();
                this.OnDateTimeLoadFail();
            }
        }

        void _device_AnalogSignalsLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAnalogSignalsLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnAnalogSignalsLoadFail()
        {
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;
            this._IoBox.Text = string.Empty;
        }

        void _device_AnalogSignalsLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnAnalogSignalsLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }
        
        private void OnAnalogSignalsLoadOk()
        {            
            this._IaBox.Text = string.Format("Ia = {0:F2} �", this._device.Ia);
            this._IbBox.Text = string.Format("Ib = {0:F2} �", this._device.Ib);
            this._IcBox.Text = string.Format("Ic = {0:F2} �", this._device.Ic);
            this._I0Box.Text = string.Format("I0 = {0:F2} �", this._device.I0);
            this._I1Box.Text = string.Format("I1 = {0:F2} �", this._device.I1);
            this._I2Box.Text = string.Format("I2 = {0:F2} �", this._device.I2);
            this._IgBox.Text = string.Format("Ig = {0:F2} �", this._device.Ig);
            this._IoBox.Text = string.Format("Io = {0:F2} �", this._device.I0);
        }

        void _device_DateTimeLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnDateTimeLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDateTimeLoadFail()
        {
            this._dateTimeBox.Text = "";
        }

        void _device_DateTimeLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnDateTimeLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDateTimeLoadOk()
        {
            this._dateTimeBox.DateTime = this._device.DateTime;
        }

        void _device_DiagnosticLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnDiagnosticLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._externalSignalsLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
            LedManager.TurnOffLeds(this._autoLeds);
        }

        void _device_DiagnosticLoadOk(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnDiagnosticLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnDiagnosticLoadOk()
        {
            if (this._device.ManageSignals[0])
            {
                this._swticherOnButton.Enabled = false;
                this._switcherOffButton.Enabled = true;
            }
            else 
            {
                this._swticherOnButton.Enabled = true;
                this._switcherOffButton.Enabled = false;
            }
            LedManager.SetLeds(this._manageLeds, this._device.ManageSignals);
            LedManager.SetLeds(this._externalSignalsLeds, this._device.ExternalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._device.Indicators);
            LedManager.SetLeds(this._inputLeds, this._device.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._device.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._device.Rele);
            LedManager.SetLeds(this._limitLeds, this._device.LimitSignals);
            LedManager.SetLeds(this._faultSignalsLeds, this._device.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._device.FaultState);
            LedManager.SetLeds(this._autoLeds, this._device.AutomaticSignals);
            

        }
        
        private void _readTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            this._device.SuspendDateTime(!this._readTimeCheck.Checked);
            if (this._readTimeCheck.Checked)
            {                
                this._systemTimeCheck.Checked = false;
            }
        }

        private void _writeTimeBut_Click(object sender, EventArgs e)
        {
            if ((this._dateTimeBox.DateTime[3] < 13 && this._dateTimeBox.DateTime[3] > 0) && (this._dateTimeBox.DateTime[5] < 32 && this._dateTimeBox.DateTime[5] > 0) && (this._dateTimeBox.DateTime[7] < 24 && this._dateTimeBox.DateTime[7] >= 0) &&
                (this._dateTimeBox.DateTime[9] < 60 && this._dateTimeBox.DateTime[9] >= 0) && (this._dateTimeBox.DateTime[11] < 60 && this._dateTimeBox.DateTime[11] >= 0))
            {
                this._device.DateTime = this._dateTimeBox.DateTime;
                this._device.SaveDateTime();
                this._readTimeCheck.Checked = this._systemTimeCheck.Checked = false;
            }
            else
            {
                MessageBox.Show("���������� ������� ����");
            }
        }

        private void _systemTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._systemTimeCheck.Checked)
            {
                this._readTimeCheck.Checked = false;
                this._timer.Start();
            }
            else
            {
                this._timer.Stop();
            }
            
        
        }
        
        void _timer_Tick(object sender, EventArgs e)
        {
            string[] dates = System.Text.RegularExpressions.Regex.Split(DateTime.Now.ToShortDateString(), "\\.");
            if (null != dates[2]) //Year
            {
                dates[2] = dates[2].Remove(0, 2);
            }

            string timeString = DateTime.Now.ToLongTimeString();
            if (DateTime.Now.Hour < 10)
            {
                timeString = "0" + timeString;
            }
            this._dateTimeBox.Text = string.Concat(dates[0], dates[1], dates[2]) + timeString + DateTime.Now.Millisecond.ToString();

        }
              
        private void ConfirmSDTU(ushort address,string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��301", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
               this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber,this._device);
            }
        }
             
        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "������������� ������� �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "������������� ������� ������");
        }

        private void _constraintToggleButton_Click(object sender, EventArgs e)
        {
            if (this._manageLed5.State == LedState.NoSignaled)
            {
                string msg = "����������� �� �������� ������ ?";
                if (DialogResult.Yes == MessageBox.Show(msg, "������-��301", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                {
                    this._device.SelectConstraintGroup(false);
                }
                
            }
            if (this._manageLed5.State == LedState.Signaled)
            {
                string msg = "����������� �� ��������� ������ ?";
                if (DialogResult.Yes == MessageBox.Show(msg, "������-��301", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                {
                    this._device.SelectConstraintGroup(true); 
                }
            }
        }
        
        private void _swticherOnButton_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void _switcherOffButton_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _indicationResetButton_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1802, "�������� ���������");
        }
    }
}