using System.ComponentModel;

namespace BEMN.MR301
{
    
    public class FeatureTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.FeatureI);
        }
    }

    public class AllSignalsTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.All);
        }
    }
    public class ControlTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Control);
        }
    }
    public class ForbiddenTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Forbidden);
        }
    }
    public class BlinkerTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(new string[] { "�����������", "�������" });
        }
    }
    public class ModeTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Modes);
        }
    }
    public class ModeLightTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ModesLight);
        }
    }
    public class ExternalDefensesTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ExternalDefense);
        }
    }
    public class LogicTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Logic);
        }
    }



  
    

}