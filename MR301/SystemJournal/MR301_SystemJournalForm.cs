using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MR301.SystemJournal.Structures;

namespace BEMN.MR301.SystemJournal
{
    public partial class SystemJournalForm : Form, IFormView
    {
        private const string RECORDS_IN_JOURNAL = "������� � ������� - {0}";
        private const string JOURNAL_SAVED = "������ ��������";
        private const string READ_FAIL = "���������� ��������� ������";
        private const string TABLE_NAME_SYS = "��301_������_�������";
        private const string NUMBER_SYS = "�����";
        private const string TIME_SYS = "�����";
        private const string MESSAGE_SYS = "���������";
        private const string SYSTEM_JOURNAL = "������ �������";


        private readonly MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private MR301 _device;
        
        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR301 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._systemJournal = device.SystemJournal;
            this._refreshSystemJournal = device.RefreshSystemJournal;

            this._refreshSystemJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._systemJournal.LoadStruct);
            this._refreshSystemJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);

            //this._device.SysJournalLoadOK += HandlerHelper.CreateHandler(this, this.SysJournalLoadOk);
            //this._device.SysJournalLoadFail += HandlerHelper.CreateHandler(this, this.SysJournalLoadFail);
            //this._device.SysJournalSaveOK += HandlerHelper.CreateHandler(this, this._device.LoadSystemJournal);
        }

        //private void OnSysJournalIndexLoadFail()
        //{
        //    this._journalCntLabel.Text = "������ ������ �������";
        //}

        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._journalCntLabel.Text = READ_FAIL;
            this.ButtonsEnabled = true;
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR301); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        private bool ButtonsEnabled
        {
            set
            {
                this._readSysJournalBut.Enabled = this._serializeSysJournalBut.Enabled =
                    this._deserializeSysJournalBut.Enabled = this._exportButton.Enabled = value;
            }
        }
        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveSysJournalDlg.FileName);
                this._journalCntLabel.Text = JOURNAL_SAVED;
            }
        }

        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void LoadJournalFromFile()
        {

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openSysJounralDlg.FileName);
            }
            this.RecordNumber = this._sysJournalGrid.Rows.Count;
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        private void StartReadJournal()
        {
            this.RecordNumber = 0;
            this.SaveNum();
        }

        private void SaveNum()
        {
            this._refreshSystemJournal.Value.Word = (ushort)this.RecordNumber;
            this._refreshSystemJournal.SaveStruct6();
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._sysJournalGrid.DataSource = this._dataTable;
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._refreshSystemJournal.SaveStruct6();
        }

        private void LoadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this.StartReadJournal();
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(new object[]
                {
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage
                });
                this.SaveNum();
            }
            else
            {
                this.ButtonsEnabled = true;
            }

        }

        /// <summary>
        /// ������� ���������
        /// </summary>
        public int RecordNumber
        {
            get
            {
                return this._recordNumber;
            }
            set
            {
                this._recordNumber = value;
                this._journalCntLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
        }

        //private void SysJournalLoadFail()
        //{
        //    if (this._device.SysJournalMessage != "��������� ���������")
        //    {
        //        this.OnSysJournalIndexLoadFail();
        //        this._device.LoadSystemJournal();
        //    }
        //    else
        //    {
        //        this.SysJournalLoaded();
        //    }
        //}

        //private void SysJournalLoadOk()
        //{
        //    try
        //    {
        //        if (this._device.SysJournalMessage != "������ ����")
        //        {
        //            this._sysJournalGrid.Rows.Add(this._device.SysJournalMessageIndex.ToString(), this._device.SysJournalRecTime, this._device.SysJournalMessage);
        //            this._sysJournalGrid.Invalidate();
        //            this._journalCntLabel.Text = this._device.SysJournalMessageIndex.ToString();
        //            this._journalCntLabel.Invalidate();
        //            this._device.LoadSystemJournal();
        //        }
        //        else
        //        {
        //            this.SysJournalLoaded();
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        //private void SysJournalLoaded()
        //{
        //    if (this._sysJournalGrid.Rows.Count != 0)
        //    {
        //        this._readSysJournalBut.Enabled = true;
        //        MessageBox.Show("��������� ������ ��������");
        //    }
        //    else
        //    {
        //        this._readSysJournalBut.Enabled = true;
        //        MessageBox.Show("��������� ������ ����");
        //    }
        //}

        private void _exportButton_Click(object sender, EventArgs e)
        {
            //if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            //{
            //    DataTable table = new DataTable("��301_������_�������");
            //    table.Columns.Add("�����");
            //    table.Columns.Add("�����");
            //    table.Columns.Add("���������");
            //    for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            //    {
            //        table.Rows.Add(this._sysJournalGrid["_indexCol", i].Value, this._sysJournalGrid["_timeCol", i].Value, this._sysJournalGrid["_msgCol", i].Value);
            //    }
            //    HtmlExport.Export(table, this._saveJournalHtmlDialog.FileName, Resources.MR301SJ);
            //}

            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR301SJ);
                this._journalCntLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}