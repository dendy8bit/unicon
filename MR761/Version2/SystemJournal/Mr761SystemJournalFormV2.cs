﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761.Version2.SystemJournal.Structures;

namespace BEMN.MR761.Version2.SystemJournal
{
    public partial class Mr761SystemJournalFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР761_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";

        #endregion [Constants]

        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private readonly MemoryEntity<SystemJournalStructV2> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private MR761 _device;
        #endregion [Private fields]

        public Mr761SystemJournalFormV2()
        {
            this.InitializeComponent();
        }

        public Mr761SystemJournalFormV2(MR761 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._systemJournal = device.Mr761DeviceV2.SystemJournal;
            double ver = Common.VersionConverter(device.DeviceVersion);
            if (ver >= 2.04 && ver < 3.0)
            {
                this._systemJournal.Value.Messages = StringsSj.MessageV204;
            }
            else
            {
                this._systemJournal.Value.Messages = StringsSj.Message;
            }
            this._refreshSystemJournal = device.Mr761DeviceV2.RefreshSystemJournal;
            this._refreshSystemJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._systemJournal.LoadStruct);
            this._refreshSystemJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
        }

        private bool IsProcess
        {
            set
            {
                this._readJournalButton.Enabled = !value;
                this._loadJournalButton.Enabled = !value;
                this._saveJournalButton.Enabled = !value;
                this._exportButton.Enabled = !value;
            }
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR761); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr761SystemJournalFormV2); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
        }
        #endregion [Properties]


        #region [Help members]
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
            this.IsProcess = false;
        }
        

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage
                );
                this.WriteJournalNumber();
            }
            else
            {
                if (this._dataTable.Rows.Count == 0)
                {
                    this._statusLabel.Text = "Журнал пуст";
                }
                this.IsProcess = false;
            }
        }

        private void GetJournalDataTable()
        {
            this._dataTable = new DataTable(TABLE_NAME_SYS);
            this._dataTable.Columns.Add(NUMBER_SYS);
            this._dataTable.Columns.Add(TIME_SYS);
            this._dataTable.Columns.Add(MESSAGE_SYS);
            
            this._systemJournalGrid.DataSource = this._dataTable;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._dataTable.Clear();
            this._recordNumber = 0;
            this.WriteJournalNumber();
        }

        private void WriteJournalNumber()
        {
            this._refreshSystemJournal.Value.Word = (ushort)this._recordNumber;
            this._refreshSystemJournal.SaveStruct();
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void Mr761SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.GetJournalDataTable();
            this.StartRead();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR731SJ);
               this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
