﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761.Version2.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурация по Ethernet
    /// </summary>
    public class ConfigEthernetStruct :StructBase// IStruct, IStructInit
    {
        [Layout(0)]
        ushort ip_lo;		//сетевой адрес устройства
        [Layout(1)]
        ushort ip_hi;
        [Layout(2)]
        ushort port;		//порт
        [Layout(3)]
        ushort rez;

    /*    #region [IStruct Members]
        public StructInfo GetStructInfo(int len)
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            ip_lo = StructHelper.GetUshort(array, ref index);
            ip_hi = StructHelper.GetUshort(array, ref index);
            port = StructHelper.GetUshort(array, ref index);
            rez = StructHelper.GetUshort(array, ref index);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    ip_lo,
                    ip_hi,
                    port,
                    rez
                };

        }

        #endregion [IStructInit Members]*/
    }
}
