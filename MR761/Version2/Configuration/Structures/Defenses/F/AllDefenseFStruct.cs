﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR761.Version2.Configuration.Structures.Defenses.F
{
    public class AllDefenseFStruct : StructBase, IDgvRowsContainer<DefenseFStruct>
    {
        [XmlArray(ElementName = "U")]
        [Layout(0, Count =8)]
        private DefenseFStruct[] _u; //мтз U>

        [XmlArray(ElementName = "Все")]
        public DefenseFStruct[] Rows
        {
            get { return _u; }
            set { _u = value; }
        }
    }
}
