﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR761.Version2.Configuration.Structures.Defenses.Istar
{
    public class AllDefenseStarStruct : StructBase, IDgvRowsContainer<DefenseStarStruct>
   {
       public const int DEF_COUNT = 6;
        
       [Layout(0, Count = DEF_COUNT)]
       private DefenseStarStruct[] _mtzmaini0; //мтз I*


[XmlArray(ElementName = "Все")]
        public DefenseStarStruct[] Rows
        {
            get { return _mtzmaini0; }
            set { _mtzmaini0 = value; }
        }
   }
}
