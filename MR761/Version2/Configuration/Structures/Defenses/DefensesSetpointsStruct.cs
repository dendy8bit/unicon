﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.Block;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.External;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.F;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.I;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.I2I1;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.Ig;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.Istar;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.Q;
using BEMN.MR761.Version2.Configuration.Structures.Defenses.U;

namespace BEMN.MR761.Version2.Configuration.Structures.Defenses
{
    /// <summary>
    /// список защит
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "защиты")]
    public class DefensesSetpointsStruct : StructBase
    {

        [Layout(0)] private CornerStruct _corner;
        [Layout(1)] private AllMtzMainStruct _mtzmain; //мтз основная
        [Layout(2)] private AllDefenseStarStruct _mtzmaini0; //мтз I*
        [Layout(3)] private DefenseI2I1Struct _mtzi2I1; //обрыв провода
        [Layout(4)] private DefenseIgStruct _mtzig; //гармоника
        [Layout(5)] private AllDefenceUStruct _uDefences;
        [Layout(6)] private AllDefenseFStruct _fDefenses;
        [Layout(7)] private AllDefenseQStruct _qDefenses; //мтз Q>
        [Layout(8)] private DefenseTermBlockStruct _termblock; //блокировка по тепловой модели
        [Layout(9)] private DefenseNBlockStruct _curblock; //блокировка пуска двигателя по числу пусков
        [Layout(10)] private AllDefenseExternalStruct _externalDefenses;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public CornerStruct Corner
        {
            get { return _corner; }
            set { _corner = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "I")]
        public AllMtzMainStruct Mtzmain
        {
            get { return _mtzmain; }
            set { _mtzmain = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "I_Со_звездой")]
        public AllDefenseStarStruct Mtzmaini0
        {
            get { return _mtzmaini0; }
            set { _mtzmaini0 = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "I2I1")]
        public DefenseI2I1Struct Mtzi2I1
        {
            get { return _mtzi2I1; }
            set { _mtzi2I1 = value; }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Ig")]
        public DefenseIgStruct Mtzig
        {
            get { return _mtzig; }
            set { _mtzig = value; }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "U")]
        public AllDefenceUStruct UDefences
        {
            get { return _uDefences; }
            set { _uDefences = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "F")]
        public AllDefenseFStruct FDefenses
        {
            get { return _fDefenses; }
            set { _fDefenses = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Q")]
        public AllDefenseQStruct QDefenses
        {
            get { return _qDefenses; }
            set { _qDefenses = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Тепловая")]
        public DefenseTermBlockStruct Termblock
        {
            get { return _termblock; }
            set { _termblock = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "По_числу_пусков")]
        public DefenseNBlockStruct Curblock
        {
            get { return _curblock; }
            set { _curblock = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Внешние")]
        public AllDefenseExternalStruct ExternalDefenses
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
    }
}
