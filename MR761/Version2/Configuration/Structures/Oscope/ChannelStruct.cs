﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761.Version2.Configuration.Structures.Oscope
{
    public class ChannelStruct : StructBase
    {
        [Layout(0)] private ushort _channel;

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string Channel
        {
            get { return Validator.Get(this._channel, StringsConfig.RelaySignals); }
            set { this._channel = Validator.Set(value, StringsConfig.RelaySignals); }
        }
    }
}
