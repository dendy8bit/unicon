﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761.Version2.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора I
    /// </summary>
    public class KanalITransStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)] private ushort _ittx;//конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)] private ushort _ittx1; //конфигурация - номинальный первичный ток In1, конфигурация - коэфициэнт Un1
        [Layout(3)] private ushort _polarityL; //резерв, вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX;//резерв, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
        [Layout(6)] private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)] private ushort _rez; //резерв 

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// тип ТТ
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "тип_ТТ")]
        public string ItypeXml
        {
            get { return Validator.Get(this._binding, StringsConfig.TtType); }
            set { this._binding =Validator.Set(value,StringsConfig.TtType) ; }
        }

        /// <summary>
        /// Iм
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Iм")]
        public double Im
        {
            get { return ValuesConverterCommon.GetIn(this._imax); }
            set { this._imax = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public ushort Ittl
        {
            get { return _ittl; }
            set { _ittl = value; }
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        public ushort Ittx
        {
            get { return _ittx; }
            set { _ittx = value; }
        }

        #endregion [Properties]
    }
}
