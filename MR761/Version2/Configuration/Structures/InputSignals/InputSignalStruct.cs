﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761.Version2.Configuration.Structures.InputSignals
{
    /// <summary>
    /// Конфигурациия входных сигналов
    /// </summary>
    public class InputSignalStruct :StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _groopUst; //вход аварийная группа уставок
        [Layout(1)]
        private ushort _clrInd; //вход сброс индикации 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// вход аварийная группа уставок
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "вход_аварийная_группа_уставок")]
public string CrashXml
        {
            get { return Validator.Get(this._groopUst,StringsConfig.SwitchSignals) ; }
            set { this._groopUst = Validator.Set(value,StringsConfig.SwitchSignals) ; }
        }
        
        /// <summary>
        /// вход сброс индикации
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "вход_сброс_индикации")]
        public string ResetIndicationXml
        {
            get { return Validator.Get(this._clrInd,StringsConfig.SwitchSignals); }
            set { this._clrInd = Validator.Set(value, StringsConfig.SwitchSignals); }
        } 
        #endregion [Properties]


     
    }
}
