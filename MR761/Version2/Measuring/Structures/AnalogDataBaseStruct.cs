﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR761.Version2.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR761.Version2.Measuring.Structures
{
    public class AnalogDataBaseStruct : StructBase
    {
        #region [Private fields]
        //ток
        [Layout(0)] private ushort _ia; // ток A
        [Layout(1)] private ushort _ib; // ток B
        [Layout(2)] private ushort _ic; // ток C
        [Layout(3)] private ushort _i0; // ток 0
        [Layout(4)] private ushort _i1; // ток 2
        [Layout(5)] private ushort _i2; // ток 2
        [Layout(6)] private ushort _in; // ток N
        [Layout(7)] private ushort _in1; // ток N1
        [Layout(8)] private ushort _ig; // ток Iг
        //ток вторая гармоника
        [Layout(9)] private ushort _i2A; // ток A
        [Layout(10)] private ushort _i2B; // ток B
        [Layout(11)] private ushort _i2C; // ток C
        //канал U
        [Layout(12)] private ushort _ua; // напряжение A
        [Layout(13)] private ushort _ub; // напряжение B
        [Layout(14)] private ushort _uc; // напряжение C
        [Layout(15)] private ushort _un; // напряжение N
        [Layout(16)] private ushort _un1; // напряжение N1
        //напряжения расчетные
        [Layout(17)] private ushort _uab;
        [Layout(18)] private ushort _ubc;
        [Layout(19)] private ushort _uca;
        [Layout(20)] private ushort _u0;
        [Layout(21)] private ushort _u1;
        [Layout(22)] private ushort _u2;
        //канал F
        [Layout(23)] private ushort _f; // частота
        [Layout(24)] private int _p; //дб выровнено на 2 слова	
        [Layout(25)] private int _q; //дб выровнено на 2 слова
        [Layout(26)] private ushort _fi; // частота
        [Layout(27)] private ushort _omp;
        //свободная логика(SIGNAL FREE LOGIC)
        [Layout(28)] private ushort _sfl1;		//
        [Layout(29)] private ushort _sfl2;		//
        [Layout(30)] private ushort _sfl3;		//
        [Layout(31)] private ushort _sfl4;		//
        [Layout(32)] private ushort _sfl5;		//
        [Layout(33)] private ushort _sfl6;		//
        [Layout(34)] private ushort _sfl7;		//
        [Layout(35)] private ushort _sfl8;		//
        //тепловая модель
        [Layout(36)] private ushort _qt;		//состояние тепловой модели
        [Layout(37)] private ushort _qtnum;	//число пусков
        [Layout(38)] private ushort _qtnumhot;	//число горячих пусков 
        #endregion [Private fields]

        #region [Public members]
        public string GetStarts()
        {
            return this._qtnum.ToString();
        }

        public string GetHotStarts()
        {
            return this._qtnumhot.ToString();
        }

        public string GetQt(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._qt);
            return ValuesConverterCommon.Analog.GetQt(value);
        }

        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            var count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        private int GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, int> func)
        {
            var count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Sum(func);
            return (int)(sum / (double)count);
        }
        
        public string GetCosF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._fi);
            return ValuesConverterCommon.Analog.GetCosF(value);
        }

        public string GetQ(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQ(value, measure.I1.Ittl * measure.U1.KthlValue);
        }

        public string GetP(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetP(value, measure.I1.Ittl * measure.U1.KthlValue);
        }

        public string GetF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF(value);
        }

        public string GetIa(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ia);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetIb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ib);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetIc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ic);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetI1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetI2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetI0(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._i0);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetIn(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._in);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittx * 40);
        }
        public string GetIg(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ig);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittx * 40);
        }

        public string GetUa(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUb(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUab(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUbc(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUca(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetU1(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetU2(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetU0(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._u0);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUn(List<AnalogDataBaseStruct> list, MeasureTransStruct measure)
        {
            var value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthxValue);
        }

        #endregion [Public members]
    }
}
