using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR761.Version2.AlarmJournal;
using BEMN.MR761.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR761.Version2.Osc.HelpClasses;
using BEMN.MR761.Version2.Osc.Loaders;
using BEMN.MR761.Version2.Osc.ShowOsc;
using BEMN.MR761.Version2.Osc.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;

namespace BEMN.MR761.Version2.Osc
{
    public partial class Mr761OscilloscopeFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoaderV2 _pageLoaderV2;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoaderV2 _oscJournalLoaderV2;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListV2 _countingListV2;
        private MR761 _device;
        private OscJournalStructV2 _journalStructV2;
        private readonly DataTable _table;
        private OscOptionsLoaderV2 _oscopeOptionsLoaderV2;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr761OscilloscopeFormV2()
        {
            InitializeComponent();
        }

        public Mr761OscilloscopeFormV2(MR761 device)
        {
            InitializeComponent();
            this._device = device;

            //��������� �������
            this._pageLoaderV2 = new OscPageLoaderV2(device.Mr761DeviceV2.SetOscStartPage, device.Mr761DeviceV2.OscPage);
            this._pageLoaderV2.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoaderV2.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, OscReadOk);
            this._pageLoaderV2.OscReadStopped += HandlerHelper.CreateActionHandler(this, ReadStop);
          
            //��������� �������
            this._oscJournalLoaderV2 = new OscJournalLoaderV2(device.Mr761DeviceV2.OscJournal, device.Mr761DeviceV2.RefreshOscJournal, device.Mr761DeviceV2.OscOptions);
            this._oscJournalLoaderV2.ReadRecordOk += HandlerHelper.CreateActionHandler(this, ReadRecord);
            this._oscJournalLoaderV2.ReadJournalFail += HandlerHelper.CreateActionHandler(this, FailReadOscJournal);
            this._oscJournalLoaderV2.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            //������������ ������� �����������
            this._oscopeOptionsLoaderV2 = device.Mr761DeviceV2.OscopeOptionsLoaderV2;
            this._oscopeOptionsLoaderV2.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscJournalLoaderV2.StartReadJournal);
            this._oscopeOptionsLoaderV2.LoadFail += HandlerHelper.CreateActionHandler(this, FailReadOscJournal);

            //��������� ������� �����
            this._currentOptionsLoader = device.Mr761DeviceV2.CurrentOptionsLoaderOsc;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscopeOptionsLoaderV2.StartRead);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, FailReadOscJournal);
            this._table = this.GetJournalDataTable();

        }
        #endregion [Ctor's]

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoaderV2.RecordNumber == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }

            this.EnableButtons = true;
        }

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            _stopReadOsc.Enabled = false;
            _oscProgressBar.Value = 0;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR761); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr761OscilloscopeFormV2); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return Mr761OscilloscopeFormV2.OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
          
            int number = this._oscJournalLoaderV2.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
                if (!this.CanSelectOsc)
                {
                    this.CanSelectOsc = true;
                }
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
                this._table.Rows.Add(this._oscJournalLoaderV2.GetRecord);
                this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingListV2 = new CountingListV2(this._pageLoaderV2.ResultArray, this._journalStructV2, 
                this._currentOptionsLoader.MeasureStruct, this._oscopeOptionsLoaderV2.OscOptions.OscopeAllChannels.ChannelsInWords, this._device.DeviceVersion);
            }
            
               catch (Exception e)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
                this.EnableButtons = true;
            }
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this.EnableButtons = true;
            this._oscReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            _oscProgressBar.Value = _oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListV2 CountingListV2
        {
            get { return _countingListV2; }
            set
            {
                this._countingListV2 = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("��761_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoaderV2.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._currentOptionsLoader.StartRead();
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingListV2 == null)
            {
                this.CountingListV2 = new CountingListV2(new ushort[12000],new OscJournalStructV2(), new MeasureTransStruct(), new ushort[24], this._device.DeviceVersion);
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingListV2.IsLoad)
                {
                    fileName = this._countingListV2.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��761 v{this._device.DeviceVersion} �������������");
                    this._countingListV2.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                Mr761OscilloscopeResultFormV2 resForm = new Mr761OscilloscopeResultFormV2(this.CountingListV2);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStructV2 = this._oscJournalLoaderV2.OscRecords[selectedOsc];
            this._pageLoaderV2.StartRead(this._journalStructV2, this._oscJournalLoaderV2.OscSizeOptions);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoaderV2.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this._saveOscilloscopeDlg.FileName = "������������� ��761";
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = this._saveOscilloscopeDlg.FileName.Replace(".hdr", $"[v{this._device.DeviceVersion}].hdr");
                this._countingListV2.Save(fileName);
                this._statusLabel.Text = "������������ ���������";
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                this.CountingListV2 = CountingListV2.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoaderV2.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            _oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            _oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
    }
}