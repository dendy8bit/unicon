﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using BEMN_XY_Chart;

namespace BEMN.MR761.Version2.Osc.ShowOsc.UI
{
    /// <summary>
    /// Предоставляет набор статических методов для работы с графиками
    /// </summary>
    public static class ChartHelper
    {

        #region [Constants]
        private const string MARKER_NAME_PATTERN = "Marker {0}";
        /// <summary>
        /// Ширина маркера
        /// </summary>
        private const int MARKER_LINE_WIDTH = 1; 
        #endregion [Constants]


        #region [Public members]
        /// <summary>
        /// Добавляет маркер на график
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="markerNumber">Номер маркера(0 или 1) / 2 - пуск осц.</param>
        /// <param name="x">Координата по X</param>
        public static void AddMarker(DAS_Net_XYChart chart, int markerNumber, int x)
        {
            var xCollection = new int[] {x, x};
            var yCollection = new List<double> { chart.CoordinateYMin, chart.CoordinateYMax };
            Color lineColor = Color.White;
            switch (markerNumber)
            {
                case 0:
                    lineColor = Color.Blue;
                    break;
                case 1:
                    lineColor = Color.DarkViolet;
                    break;
                case 2:
                    lineColor = Color.Aqua;
                    break;
            }
            var markerName = string.Format(MARKER_NAME_PATTERN, markerNumber);

            var markerCurve = ChartHelper.CreateLineCurve(markerName, lineColor, ChartHelper.MARKER_LINE_WIDTH, xCollection, yCollection);
            chart.AddCurve(markerName, markerCurve);
            chart.Update();

        }
        /// <summary>
        /// Удаляет маркер с графика
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="markerNumber">Номер маркера(0 или 1)</param>
        public static void RemoveMarker(DAS_Net_XYChart chart, int markerNumber)
        {
            var markerName = string.Format(MARKER_NAME_PATTERN, markerNumber);
            var curve = new DAS_Net_XYChart.DAS_XYCurveVariable { strCurveName = markerName };
            chart.UpdateCurveProperty(markerName, curve);
        }

        /// <summary>
        /// Добавляет кривую на график
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="name">Название(идентификатор) кривой</param>
        /// <param name="color">Цвет</param>
        /// <param name="lineWidth">Толщина линии</param>
        /// <param name="yCollection"></param>
        public static void AddLineCurves(DAS_Net_XYChart chart, string name, Color color, int lineWidth, ICollection yCollection)
        {
            ChartHelper.AddLineCurves(chart,name,color,lineWidth,yCollection,Enumerable.Range(0, yCollection.Count).ToList());
        }

        /// <summary>
        /// Добавляет кривую на график
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="name">Название(идентификатор) кривой</param>
        /// <param name="color">Цвет</param>
        /// <param name="lineWidth">Толщина линии</param>
        /// <param name="yCollection"></param>
        public static void AddLineCurves(DAS_Net_XYChart chart, string name, Color color, int lineWidth, ICollection yCollection, ICollection xCollection)
        {

            chart.AddCurve(name, ChartHelper.CreateLineCurve(name, color, lineWidth, xCollection, yCollection));
        }


        /// <summary>
        /// Удаляет кривую с графика
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="name">Имя кривой</param>
        public static void RemoveLineCurves(DAS_Net_XYChart chart, string name)
        {
            var curve = new DAS_Net_XYChart.DAS_XYCurveVariable { strCurveName = name };
            chart.UpdateCurveProperty(name, curve);
        }
 

        /// <summary>
        /// Добавляет на график дискретную функцию(значения 0/1)
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="groupName">Группа параметров</param>
        /// <param name="lineIndex">Номер параметра</param>
        /// <param name="values">Точки</param>
        public static void AddDiscretLine(DAS_Net_XYChart chart, string groupName, int lineIndex, ushort[] values)
        {
          /*  var lineNameMid = string.Format(DISCRET_MID_PATTERN, groupName, lineIndex);
            var lineNameTop = string.Format(DISCRET_TOP_PATTERN, groupName, lineIndex);
            var lineNameBot = string.Format(DISCRET_BOT_PATTERN, groupName, lineIndex);
            int y = (int) ((-1)*(lineIndex + 0.5)*DISCRET_LINE_INTERVAL);
            var yMidValues = new int[] { y, y };
           if (values.All(a => a == 0))
           {
               var xValues = new int[]{0,values.Length};
              
               var yValues = new int[] { y, y };

               ChartHelper.AddLineCurves(chart, lineNameMid, ChartHelper.DiscretLineColor, 1, yValues, xValues);
               return;
           }

           if (values.All(a => a == 1))
           {
               var xValues = new int[] { 0, values.Length };
         
               var yTop = y+1;
               var yBot = y-1;

               var yTopValues = new int[] { yTop, yTop };
               var yBotValues = new int[] { yBot, yBot };

               ChartHelper.AddLineCurves(chart, lineNameTop, ChartHelper.DiscretLineColor, 1, yTopValues, xValues);
               ChartHelper.AddLineCurves(chart, lineNameBot, ChartHelper.DiscretLineColor, 1, yBotValues, xValues);
               ChartHelper.AddLineCurves(chart, lineNameMid, ChartHelper.MiddleDiscretLineColor, (DISCRET_LINE_INTERVAL - 1) * 2, yMidValues, xValues);
               return;
           }


            var discretXList = new List<int> {0};
            var discretYList = new List<int> {values[0]};
            int oldValue = values[0];
            for (int i = 1; i < values.Length; i++)
            {
                if (values[i] != oldValue)
                {
                    discretXList.Add(i - 1);
                    discretYList.Add(oldValue);

                    discretXList.Add(i);
                    discretYList.Add(values[i]);
                    oldValue = values[i];
                }
            }
          if (discretXList.Last() != values.Length - 1)
          {
              discretXList.Add(values.Length - 1);
              discretYList.Add(values[values.Length - 1]);
          }

            var topYValues = discretYList.Select(a => y + a).ToList();
            var botYValues = discretYList.Select(a => y - a).ToList();

            var midXValues = new List<int[]>();
            for (int i = 0; i < discretYList.Count - 1; i++)
            {
              if ((discretYList[i] == 1) & (discretYList[i + 1] == 1))
              {
                  midXValues.Add(new int[] { discretXList[i], discretXList[i+1] });
              }
            }



            ChartHelper.AddLineCurves(chart, lineNameTop, ChartHelper.DiscretLineColor, 1, topYValues, discretXList);
            ChartHelper.AddLineCurves(chart, lineNameBot, ChartHelper.DiscretLineColor, 1, botYValues, discretXList);
            for (int i = 0; i < midXValues.Count; i++)
            {
                var midNumberName = string.Format("{0}_{1}", lineNameMid, i);
                ChartHelper.AddLineCurves(chart, midNumberName, ChartHelper.MiddleDiscretLineColor,
                                          (DISCRET_LINE_INTERVAL - 1)*2, yMidValues, midXValues[i]);
            }*/

        }

        /// <summary>
        /// Удаляет с графика дискретную функцию
        /// </summary>
        /// <param name="chart">График</param>
        /// <param name="groupName">Группа параметров</param>
        /// <param name="lineIndex">Номер параметра</param>
        public static void RemoveDiscretLine(DAS_Net_XYChart chart, string groupName, int lineIndex)
        {
         /*   var curveNameTop = string.Format(DISCRET_TOP_PATTERN, groupName, lineIndex);
            var curveNameBot = string.Format(DISCRET_BOT_PATTERN, groupName, lineIndex);
            var lineNameMid = string.Format(DISCRET_MID_PATTERN, groupName, lineIndex);
            var curve = new DAS_Net_XYChart.DAS_XYCurveVariable { strCurveName = curveNameTop };
            chart.UpdateCurveProperty(curveNameTop, curve);

            curve = new DAS_Net_XYChart.DAS_XYCurveVariable { strCurveName = curveNameBot };
            chart.UpdateCurveProperty(curveNameBot, curve);

            curve = new DAS_Net_XYChart.DAS_XYCurveVariable { strCurveName = lineNameMid };
            chart.UpdateCurveProperty(lineNameMid, curve);*/
        }
        #endregion [Public members]


        #region [Help members]
        private static DAS_Net_XYChart.DAS_XYCurveVariable CreateLineCurve(string name, Color curveColor, int lineWidth, ICollection xCollection, ICollection yCollection)
        {
            var curve = new DAS_Net_XYChart.DAS_XYCurveVariable();
            //цвет
            curve.cColor = curveColor;
            //название
            curve.strCurveName = name;
            //Показывать ли кривую (обычный visible)
            curve.bLineVisible = true;
            //Не используется
            curve.iPointNumber = 100;
            //Размер одной точки
            curve.iPointSize = 5;
            //Ширина линии
            curve.iLineWidth = lineWidth;
            //Внешний вид точек графика
            curve.ePointStyle = DAS_CurvePointStyle.BPTS_NONE;
            //Видимость
            curve.bVisible = true;

            curve.iCurPriority = 0;
            curve.dblMax = 1000.0;
            curve.dblMin = 0;
            curve.bYScaleVisible = true;
            curve.bYAtStart = true;
            curve.bAreaMode = false;
            curve.dblAreaBaseValue = 0.0;
            curve.ArrayX = new ArrayList(xCollection);
            curve.ArrayY = new ArrayList(yCollection);
            return curve;
        }  
        #endregion [Help members]
    }
}
