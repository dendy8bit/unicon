﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using BEMN_XY_Chart;

namespace BEMN.MR761.Version2.Osc.ShowOsc.UI
{
    /// <summary>
    /// Дискретныя линия на графике
    /// </summary>
    public class DiscretLine
    {
        #region [Constants]
        private const string DISCRET_TOP_PATTERN = "{0} {1} Top";
        private const string DISCRET_MID_PATTERN = "{0} {1} Mid";
        private const string DISCRET_BOT_PATTERN = "{0} {1} Bot";
        public const int DISCRET_LINE_INTERVAL = 5;
        private const int ONE_LINE_HEIGHT = 1; 
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Цвет внутренней части дискретной линии
        /// </summary>
        private static readonly Color MiddleDiscretLineColor = Color.SlateGray;
        /// <summary>
        /// Цвет внешней части дискретной линии
        /// </summary>
        private static readonly Color DiscretLineColor = Color.Black;
        private readonly DAS_Net_XYChart _chart;
        private LineType _lineType;
        private readonly string _lineNameMid;
        private readonly string _lineNameTop;
        private readonly string _lineNameBot;
        private int[] _xValues;
        private readonly int[] _yMidValues;
        private int[] _yTopValues;
        private int[] _yBotValues;
        private List<OneLine> _oneLines; 
        #endregion [Private fields]


        #region [Ctor's]
        public DiscretLine(DAS_Net_XYChart chart, string groupName, int lineIndex, ushort[] values)
        {
            this._chart = chart;
            this._lineNameMid = string.Format(DISCRET_MID_PATTERN, groupName, lineIndex);
            this._lineNameTop = string.Format(DISCRET_TOP_PATTERN, groupName, lineIndex);
            this._lineNameBot = string.Format(DISCRET_BOT_PATTERN, groupName, lineIndex);

            var y = (int)((-1) * (lineIndex + 0.5) * DISCRET_LINE_INTERVAL);
            this._yMidValues = new[] { y, y };
            this._xValues = new[] { 0, values.Length };


            if (values.All(a => a == 0))
            {
                this.CreateAllZeroLine();
                return;
            }

            if (values.All(a => a == 1))
            {
                this.CreateAllOneLine(y);
                return;
            }
            this.CreateZeroAndOneLine(values, y);

        } 
        #endregion [Ctor's]


        #region [Public members]
        /// <summary>
        /// Добавляет линию на график
        /// </summary>
        public void Add()
        {
            if (this._lineType == LineType.ALL_ZERO)
            {
                ChartHelper.AddLineCurves(this._chart, this._lineNameMid, DiscretLine.DiscretLineColor, 1,
                                          this._yMidValues, this._xValues);
            }

            if (this._lineType == LineType.ALL_ONE)
            {
                ChartHelper.AddLineCurves(this._chart, this._lineNameTop, DiscretLine.DiscretLineColor, 1,
                                          this._yTopValues, this._xValues);
                ChartHelper.AddLineCurves(this._chart, this._lineNameBot, DiscretLine.DiscretLineColor, 1,
                                          this._yBotValues, this._xValues);
                ChartHelper.AddLineCurves(this._chart, this._lineNameMid, DiscretLine.MiddleDiscretLineColor,
                                          (DISCRET_LINE_INTERVAL - 1) * 2, this._yMidValues, this._xValues);
            }

            if (this._lineType == LineType.ZERO_AND_ONE)
            {
                ChartHelper.AddLineCurves(this._chart, this._lineNameTop, DiscretLine.DiscretLineColor, 1, this._yTopValues, this._xValues);
                ChartHelper.AddLineCurves(this._chart, this._lineNameBot, DiscretLine.DiscretLineColor, 1, this._yBotValues, this._xValues);

                foreach (var line in this._oneLines)
                {
                    ChartHelper.AddLineCurves(this._chart, line.Name, DiscretLine.MiddleDiscretLineColor,
                                     (DISCRET_LINE_INTERVAL - 1) * 2, line.YList, line.XList);
                }
            }

        } 

        public void Remove()
        {
            if (this._lineType == LineType.ALL_ZERO)
            {
                ChartHelper.RemoveLineCurves(this._chart, this._lineNameMid);
            }

            if (this._lineType == LineType.ALL_ONE)
            {
                ChartHelper.RemoveLineCurves(this._chart, this._lineNameTop);
                ChartHelper.RemoveLineCurves(this._chart, this._lineNameBot);
                ChartHelper.RemoveLineCurves(this._chart, this._lineNameMid);
            }

            if (this._lineType == LineType.ZERO_AND_ONE)
            {
                ChartHelper.RemoveLineCurves(this._chart, this._lineNameTop);
                ChartHelper.RemoveLineCurves(this._chart, this._lineNameBot);

                foreach (var line in this._oneLines)
                {
                    ChartHelper.RemoveLineCurves(this._chart, line.Name);
                }
            }
        }
        #endregion [Public members]


        #region [Help members]

        /// <summary>
        /// Создаёт линию содержащую только нули
        /// </summary>
        private void CreateAllZeroLine()
        {
            this._lineType = LineType.ALL_ZERO;
            this.Add();
        }

        /// <summary>
        /// Создаёт линию содержащую только единицы
        /// </summary>
        /// <param name="y">Координата на графике для вывода</param>
        private void CreateAllOneLine(int y)
        {
            this._lineType = LineType.ALL_ONE;
            var yTop = y + ONE_LINE_HEIGHT;
            var yBot = y - ONE_LINE_HEIGHT;
            this._yTopValues = new[] { yTop, yTop };
            this._yBotValues = new[] { yBot, yBot };
            this.Add();
        }



        /// <summary>
        /// Создаёт линию содержащую как нули так и единицы
        /// </summary>
        /// <param name="values">Массив данных</param>
        /// <param name="y">Координата на графике для вывода</param>
        private void CreateZeroAndOneLine(ushort[] values, int y)
        {
            this._lineType = LineType.ZERO_AND_ONE;
            this._oneLines = new List<OneLine>();

            var discretXList = new List<int> { 0 };
            var discretYList = new List<int> { values[0] };
            int oldValue = values[0];
            for (int i = 1; i < values.Length; i++)
            {
                if (values[i] != oldValue)
                {
                    discretXList.Add(i - 1);
                    discretYList.Add(oldValue);

                    discretXList.Add(i);
                    discretYList.Add(values[i]);
                    oldValue = values[i];
                }
            }
            if (discretXList.Last() != values.Length - 1)
            {
                discretXList.Add(values.Length - 1);
                discretYList.Add(values[values.Length - 1]);
            }

            this._yTopValues = discretYList.Select(a => y + a).ToArray();
            this._yBotValues = discretYList.Select(a => y - a).ToArray();

            var midXValues = new List<int[]>();
            for (int i = 0; i < discretYList.Count - 1; i++)
            {
                if ((discretYList[i] == 1) & (discretYList[i + 1] == 1))
                {
                    midXValues.Add(new[] { discretXList[i], discretXList[i + 1] });
                }
            }
            this._xValues = discretXList.ToArray();

            for (int i = 0; i < midXValues.Count; i++)
            {
                var midNumberName = string.Format("{0}_{1}", this._lineNameMid, i);

                this._oneLines.Add(new OneLine(midNumberName, midXValues[i], this._yMidValues));
            }
            this.Add();
        }
        #endregion [Help members]


        #region [Nested types]
        private enum LineType
        {
            ALL_ZERO,
            ALL_ONE,
            ZERO_AND_ONE
        };

        private class OneLine
        {
            #region [Private fields]
            private readonly string _name;
            private readonly ArrayList _xList;
            private readonly ArrayList _yList;
            #endregion [Private fields]


            #region [Ctor's]
            public OneLine(string name, ICollection xList, ICollection yList)
            {
                this._name = name;
                this._xList = new ArrayList(xList);
                this._yList = new ArrayList(yList);
            }
            #endregion [Ctor's]


            #region [Properties]
            public string Name
            {
                get { return _name; }
            }

            public ArrayList XList
            {
                get { return _xList; }
            }

            public ArrayList YList
            {
                get { return _yList; }
            }
            #endregion [Properties]
        }
        #endregion [Nested types]
    }
}
