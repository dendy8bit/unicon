﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR761.Version2.Configuration;

namespace BEMN.MR761.Version204.Configuration.Structures.Oscope
{
    [XmlType(TypeName = "Один_канал")]
   public class ChannelStructV204 : StructBase
   {
       [Layout(0)] private ushort _channel;

       [BindingProperty(0)]
       [XmlAttribute(AttributeName = "Канал")]
       public string Channel
       {
           get { return Validator.Get(this._channel, StringsConfig.RelaySignalsV204); }
           set { this._channel = Validator.Set(value, StringsConfig.RelaySignalsV204); }
       }
   }
}
