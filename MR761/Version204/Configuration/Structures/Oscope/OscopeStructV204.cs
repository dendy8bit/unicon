﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761.Version2.Configuration.Structures.Oscope;

namespace BEMN.MR761.Version204.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStructV204 : StructBase 
    {
        private const int REZ_COUNT = 4;

        #region [Private fields]

        [Layout(0)] private OscopeConfigStruct _oscopeConfig;
        [Layout(1)] private OscopeAllChannelsStructV204 _oscopeAllChannels;
        [Layout(2)] private ChannelStructV204 _inputStartOsc;
        [Layout(3, Count = REZ_COUNT)] private ushort[] _rez;
        #endregion [Private fields]
        
        #region [Properties]
        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStruct OscopeConfig
        {
            get { return this._oscopeConfig; }
            set { this._oscopeConfig = value; }
        }
        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStructV204 OscopeAllChannels
        {
            get { return this._oscopeAllChannels; }
            set { this._oscopeAllChannels = value; }
        }
        /// <summary>
        /// Вход запуска осциллографа
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Вход_пуска_осциллографа")]
        public ChannelStructV204 InputStartOscope
        {
            get { return this._inputStartOsc; }
            set { this._inputStartOsc = value; }
        }
        #endregion [Properties]
    }
}
