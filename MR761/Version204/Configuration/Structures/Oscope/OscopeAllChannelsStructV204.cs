﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MR761.Version2.Configuration;

namespace BEMN.MR761.Version204.Configuration.Structures.Oscope
{
    public class OscopeAllChannelsStructV204 : StructBase, IDgvRowsContainer<ChannelStructV204>
    {
        #region [Constants]

        public const int KANAL_COUNT = 24;

        #endregion [Constants]

        [Layout(0, Count = KANAL_COUNT)]
        private ChannelStructV204[] _kanal; //конфигурация канала осциллографирования

        #region Properties
        [XmlIgnore]
        public ushort[] ChannelsInWords
        {
            get { return this._kanal.Select(o => (ushort)StringsConfig.RelaySignalsV204.IndexOf(o.Channel)).ToArray(); }
        }

        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelStructV204[] Rows
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }
        #endregion
    }
}
