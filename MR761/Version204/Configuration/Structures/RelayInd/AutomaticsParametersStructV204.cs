﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761.Version2.Configuration.Structures.RelayInd;

namespace BEMN.MR761.Version204.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры автоматики(Реле и индикаторы)
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "Реле_и_Индикаторы")]
    public class AutomaticsParametersStructV204 : StructBase
    {
        #region [Private fields]

        /// <summary>
        /// Реле
        /// </summary>
        [Layout(0)] private AllReleOutputStructV204 _relays;

        /// <summary>
        /// индикаторы
        /// </summary>
        [Layout(1)] private AllIndicatorsStructV204 _indicators;

        /// <summary>
        /// реле неисправность
        /// </summary>
        [Layout(2)] private FaultStruct _fault;

       

        #endregion [Private fields]

        /// <summary>
        /// Реле
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Реле")]
        public AllReleOutputStructV204 Relays
        {
            get { return this._relays; }
            set { this._relays = value; }
        }

        /// <summary>
        /// индикаторы
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Индикаторы")]
        public AllIndicatorsStructV204 Indicators
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }

        /// <summary>
        /// реле неисправность
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Реле_неисправности")]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }
    }
}
