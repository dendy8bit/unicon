﻿using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Framework;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;
using BEMN.MR761.BSBGL;
using BEMN.MR761.Version1.AlarmJournal;
using BEMN.MR761.Version1.AlarmJournal.Structures;
using BEMN.MR761.Version1.Configuration;
using BEMN.MR761.Version1.Configuration.Structures;
using BEMN.MR761.Version1.Measuring;
using BEMN.MR761.Version1.Measuring.Structures;
using BEMN.MR761.Version1.OldClasses;
using BEMN.MR761.Version1.Osc;
using BEMN.MR761.Version1.Osc.Loaders;
using BEMN.MR761.Version1.Osc.Structures;
using BEMN.MR761.Version1.SystemJournal;
using BEMN.MR761.Version1.SystemJournal.Structures;
using BEMN.MR761.Version2.AlarmJournal;
using BEMN.MR761.Version2.Configuration;
using BEMN.MR761.Version2.Measuring;
using BEMN.MR761.Version2.Osc;
using BEMN.MR761.Version2.SystemJournal;
using BEMN.MR761.Version201.Configuration;
using BEMN.MR761.Version201.Measuring;
using BEMN.MR761.Version204.Configuration;
using BEMN.MR761.Version300.AlarmJournal;
using BEMN.MR761.Version300.Configuration;
using BEMN.MR761.Version300.Configuration.Structures;
using BEMN.MR761.Version300.Emulation;
using BEMN.MR761.Version300.Emulation.Structers;
using BEMN.MR761.Version300.Measuring;
using BEMN.MR761.Version300.Oscilloscope;
using BEMN.MR761.Version300.SystemJournal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;
using BEMN.MR761.Version300.FilesSharing;
using CurrentOptionsLoader = BEMN.MR761.Version1.AlarmJournal.CurrentOptionsLoader;
using StringsConfig = BEMN.MR761.Version300.Configuration.StringsConfig;

namespace BEMN.MR761
{
    public class MR761 : Device, IDeviceView, IDeviceVersion
    {
        #region Константы
        public const int RELE_COUNT = 32;
        public const int INDICATOR_COUNT = 12;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int ALARM_JOURNAL_START_ADRESS = 0x700;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;

        private bool _isCheckVersion;
        #endregion

        #region Поля

        private Mr761DeviceV2 _mr761DeviceV2;
        private Dictionary<string, StObj> _memoryMap;
        private bool _devicesInitialised;

        private MemoryEntity<OscJournalStructV1> _oscJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscopeStruct> _oscopeStruct;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private OscOptionsLoader _oscOptionsLoader;

        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;
        #endregion

        #region Структуры
        private SWITCH _switch;
        private APV _apv;
        private AVR _avr;
        private LPB _lpb;
        private AUTOBLOWER _autoblower;
        private TERMAL _termall;
        private INPUTSIGNAL _inputsignal;
        private OSCOPE _osc = new OSCOPE(true);
        private MEASURETRANS _measuretrans;
        private INPSYGNAL _inpsignal = new INPSYGNAL(64);
        private ELSSYGNAL _elssignal = new ELSSYGNAL(64);
        private CURRENTPROTMAIN _protmain = new CURRENTPROTMAIN(64);
        private CURRENTPROTRESERVE _protreserve = new CURRENTPROTRESERVE(64);
        private PARAMAUTOMAT _paramautomat = new PARAMAUTOMAT(64);
        private CONFIGSYSTEM _configsystem;
        private CONFOMP _confOMP;
        private MTZMAIN _mtzMain;
        private MTZMAIN _mtzMainI0;
        private MTZMAIN _mtzMainI2I1;
        private MTZMAIN _mtzMainIr;
        private MTZUEXT _mtzExt;
        private MTZUEXT _mtzFMax;
        private MTZUEXT _mtzFMin;
        private MTZUEXT _mtzUMax;
        private MTZUEXT _mtzUMin;

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MemoryEntity<MeasureTransStruct> _measureTransOsc;
        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<JournalRefreshStruct> _refreshSystemJournal;
        private MemoryEntity<MeasureTransStruct> _connectionsAlarmJournal;
        private CurrentOptionsLoader _currentOptionsLoader;
        #endregion Структуры

        #region Свойства

        public Dictionary<string, StObj> MemoryMap
        {
            get { return this._memoryMap; }
            set { this._memoryMap = value; }
        }

        public Mr761DeviceV2 Mr761DeviceV2
        {
            get { return this._mr761DeviceV2; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return this._refreshAlarmJournal; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return this._alarmJournal; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return this._measureTrans; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTransOsc
        {
            get { return this._measureTransOsc; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshSystemJournal
        {
            get { return this._refreshSystemJournal; }
        }
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        public CurrentOptionsLoader CurrentOptionsLoader
        {
            get { return this._currentOptionsLoader; }
        }

        public MemoryEntity<OscJournalStructV1> OscJournal
        {
            get { return this._oscJournal; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return this._oscOptions; }
        }

        public OscOptionsLoader OscopeOptionsLoader
        {
            get { return this._oscOptionsLoader; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return this._setOscStartPage; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }

        public MTZMAIN sMtzMain
        {
            get { return this._mtzMain; }
            set { this._mtzMain = value; }
        }

        public MTZMAIN sMtzMainI0
        {
            get { return this._mtzMainI0; }
            set { this._mtzMainI0 = value; }
        }

        public MTZMAIN sMtzMainI2I1
        {
            get { return this._mtzMainI2I1; }
            set { this._mtzMainI2I1 = value; }
        }

        public MTZMAIN sMtzMainIr
        {
            get { return this._mtzMainIr; }
            set { this._mtzMainIr = value; }
        }

        public MTZUEXT sMtzExt
        {
            get { return this._mtzExt; }
            set { this._mtzExt = value; }
        }

        public MTZUEXT sMtzFMax
        {
            get { return this._mtzFMax; }
            set { this._mtzFMax = value; }
        }

        public MTZUEXT sMtzFMin
        {
            get { return this._mtzFMin; }
            set { this._mtzFMin = value; }
        }

        public MTZUEXT sMtzUMax
        {
            get { return this._mtzUMax; }
            set { this._mtzUMax = value; }
        }

        public MTZUEXT sMtzUMin
        {
            get { return this._mtzUMin; }
            set { this._mtzUMin = value; }
        }

        public SWITCH sSwitch
        {
            get { return this._switch; }
            set { this._switch = value; }
        }

        public APV sApv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }

        public AVR sAvr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }

        public LPB sLpb
        {
            get { return this._lpb; }
            set { this._lpb = value; }
        }

        public AUTOBLOWER sAutoblower
        {
            get { return this._autoblower; }
            set { this._autoblower = value; }
        }

        public TERMAL sTermall
        {
            get { return this._termall; }
            set { this._termall = value; }
        }

        public INPUTSIGNAL sInputsignal
        {
            get { return this._inputsignal; }
            set { this._inputsignal = value; }
        }

        public OSCOPE sOsc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }

        public MEASURETRANS sMeasuretrans
        {
            get { return this._measuretrans; }
            set { this._measuretrans = value; }
        }

        public INPSYGNAL sInpsignal
        {
            get { return this._inpsignal; }
            set { this._inpsignal = value; }
        }

        public ELSSYGNAL sElssignal
        {
            get { return this._elssignal; }
            set { this._elssignal = value; }
        }

        public CURRENTPROTMAIN sProtmain
        {
            get { return this._protmain; }
            set { this._protmain = value; }
        }

        public CURRENTPROTRESERVE sProtreserve
        {
            get { return this._protreserve; }
            set { this._protreserve = value; }
        }

        public PARAMAUTOMAT sParamautomat
        {
            get { return this._paramautomat; }
            set { this._paramautomat = value; }
        }

        public CONFIGSYSTEM sConfigsystem
        {
            get { return this._configsystem; }
            set { this._configsystem = value; }
        }

        public CONFOMP sConfOMP
        {
            get { return this._confOMP; }
            set { this._confOMP = value; }
        }
        #endregion Свойства

        #region Конструкторы и инициализация

        public MR761()
        {
            this.HaveVersion = true;
        }

        public MR761(Modbus mb)
        {
            this.MB = mb;
            this.HaveVersion = true;
            this.InitAddr();
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return this.mb; }
            set
            {
                this.mb = value;
                if (null != this.mb)
                {
                    this.mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        private void InitAddr()
        {
            this._memoryMap = new Dictionary<string, StObj>();

            this.InitStruct(new SWITCH(), "Выключатель", 0x1000);
            this.InitStruct(new APV(), "АПВ", "Выключатель");
            this.InitStruct(new AVR(), "АВР", "АПВ");
            this.InitStruct(new LPB(), "ЛПБ", "АВР");
            this.InitStruct(new AUTOBLOWER(), "Обдув", "ЛПБ");                          //резерв
            this.InitStruct(new TERMAL(), "Тепловая_Модель", "Обдув");                  //резерв
            this.InitStruct(new INPUTSIGNAL(), "Входные_Сигналы", "Тепловая_Модель");
            this.InitStruct(new OSCOPE(), "Осциллограмма", "Входные_Сигналы");
            this.InitStruct(new MEASURETRANS(), "Измерительный_Трансформатор", "Осциллограмма");
            this.InitStruct(new INPSYGNAL(64), "Входные_ЛС", "Измерительный_Трансформатор");
            this.InitStruct(new ELSSYGNAL(64), "Выходные_ЛС", "Входные_ЛС");
            this.InitStruct(new CURRENTPROTMAIN(), "Защиты_Основные", "Выходные_ЛС");
            this.InitStruct(new CURRENTPROTRESERVE(), "Защиты_Резервные", "Защиты_Основные");
            this.InitStruct(new PARAMAUTOMAT(64), "Автоматика", "Защиты_Резервные");    //резерв
            this.InitStruct(new CONFIGSYSTEM(), "Конфигурация_Системы", "Автоматика");
            this.InitStruct(new CONFOMP(), "ОМП", "Конфигурация_Системы");
            this.InitStruct(new SihronizmStruct(), "Синхронизм", "ОМП");

            this._refreshSystemJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала системы", this, 0x600);
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("ЖС", this, 0x600);
            this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", this, 0x700);
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, ANALOG_DATABASE_START_ADRESS);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, DISCRET_DATABASE_START_ADRESS);
            this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", this, ALARM_JOURNAL_START_ADRESS);
            this._connectionsAlarmJournal = new MemoryEntity<MeasureTransStruct>("Токи присоединений для ЖА", this, 0x105A);
            this._currentOptionsLoader = new CurrentOptionsLoader(this._connectionsAlarmJournal);
            this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений measuring", this, 0x105A);
            this._measureTransOsc = new MemoryEntity<MeasureTransStruct>("Параметры измерений osc", this, 0x105A);

            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x5A0);
            this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", this, OSC_JOURNAL_START_ADRESS);
            this._oscJournal = new MemoryEntity<OscJournalStructV1>("Журнал осциллографа", this, OSC_JOURNAL_START_ADRESS);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);

            this._oscopeStruct = new MemoryEntity<OscopeStruct>("Уставки осциллографа", this, 0x103a);
            this._oscOptionsLoader = new OscOptionsLoader(this._oscopeStruct);

            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
            this._programPageStruct = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);

            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);
            this._stateSpl = new MemoryEntity<OneWordStruct>("Состояние ошибок логики", this, 0x0D13);

            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись  нулевых аналоговых сигналов ", this, 0x5800);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение аналоговых сигналов", this, 0x582E);
        }

        #endregion

        #region Доп. функции

        private void InitStruct(object _struct, string _currentNameSTR, ushort _startAddr)
        {
            StObj _strObj = new StObj(_currentNameSTR, this.mb);
            object _slot = null;
            int slotArray = 0;
            _strObj.StructObj = _struct;
            StructInfo sInf = (_strObj.StructObj as IStruct).GetStructInfo(64);
            _strObj.StartAddr = _startAddr;
            _strObj.EndAddr = (ushort)(_strObj.StartAddr + sInf.FullSize);
            if (_strObj.EndAddr > _strObj.StartAddr)
            {
                _strObj.Values = new ushort[_strObj.EndAddr - _strObj.StartAddr];
            }
            _strObj.Slots = new List<slot>();
            _slot = (_strObj.StructObj as IStruct).GetSlots(_strObj.StartAddr, sInf.SlotsArray, 64);
            if (_slot != null)
            {
                if (_slot is Array)
                {
                    for (int i = 0; i < slotArray; i++)
                    {
                        _strObj.Slots.Add((_slot as slot[])[i]);
                    }
                }
                else
                {
                    _strObj.Slots.Add(_slot as slot);
                }
            }
            else
            {
                _strObj.Slots = null;
            }
            this._memoryMap.Add(_currentNameSTR, _strObj);
        }

        private void InitStruct(object _struct, string _currentNameSTR, string _lastNameSTR)
        {
            StObj strObj = new StObj(_currentNameSTR, this.mb);
            strObj.StructObj = _struct;
            StructInfo sInf = (strObj.StructObj as IStruct).GetStructInfo(64);
            strObj.StartAddr = this._memoryMap[_lastNameSTR].EndAddr;
            strObj.EndAddr = (ushort)(strObj.StartAddr + sInf.FullSize);
            if (strObj.EndAddr > strObj.StartAddr)
            {
                strObj.Values = new ushort[strObj.EndAddr - strObj.StartAddr];
            }
            strObj.Slots = new List<slot>();
            object slot = (strObj.StructObj as IStruct).GetSlots(strObj.StartAddr, sInf.SlotsArray, 64);
            if (slot != null)
            {
                if (slot is Array)
                {
                    slot[] slotArray = slot as slot[];
                    foreach (slot t in slotArray)
                    {
                        strObj.Slots.Add(t);
                    }
                }
                else
                {
                    strObj.Slots.Add(slot as slot);
                }
            }
            else
            {
                strObj.Slots = null;
            }
            this._memoryMap.Add(_currentNameSTR, strObj);
        }

        public void InitDeviceNumbers()
        {
            if (this._devicesInitialised || this._memoryMap == null) return;
            foreach (StObj item in this._memoryMap.Values)
            {
                item.DeviceNum = this.DeviceNumber;
            }
            this._devicesInitialised = true;
        }

        public void ConfirmConstraint()
        {
            this.SetBit(this.DeviceNumber, 0x0D00, true, "МР761 запрос подтверждения" + this.DeviceNumber, this);
        }

        #endregion

        #region События
        public event Action ConfigWriteOk;
        public event Action ConfigWriteFail;
        #endregion

        #region TT
        public string TTtype
        {
            get
            {
                return Strings.TT_Type[this._measuretrans.L1.Binding];
            }
            set
            {
                this._measuretrans.L1.Binding = (ushort)Strings.TT_Type.IndexOf(value);
            }
        }

        public double Imax
        {
            get
            {
                return Measuring.GetConstraintOnly(this._measuretrans.L1.Imax, ConstraintKoefficient.K_4000);
            }
            set
            {
                this._measuretrans.L1.Imax = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public ushort ITTL
        {
            get
            {
                return this._measuretrans.L1.Ittl;
            }
            set
            {
                this._measuretrans.L1.Ittl = value;
            }
        }

        public ushort ITTX
        {
            get
            {
                return this._measuretrans.L1.Ittx;
            }
            set
            {
                this._measuretrans.L1.Ittx = value;
            }
        }
        #endregion

        #region TT
        public string TNtype
        {
            get
            {
                return Strings.Uo_Type[this._measuretrans.U1.Binding];
            }
            set
            {
                this._measuretrans.U1.Binding = (ushort)Strings.Uo_Type.IndexOf(value);
            }
        }

        public double KTHL
        {
            get
            {
                ushort res = Common.GetBits(this._measuretrans.U1.Ittl, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                return Measuring.GetU(res, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._measuretrans.U1.Ittl = Common.SetBits(this._measuretrans.U1.Ittl, (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000), 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        public double KTHX
        {
            get
            {
                ushort res = Common.GetBits(this._measuretrans.U1.Ittx, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                return Measuring.GetU(res, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._measuretrans.U1.Ittx = Common.SetBits(this._measuretrans.U1.Ittx, (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000), 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        public string KTHLKoeff
        {
            get
            {
                return Strings.Measure_Koef[Common.GetBits(this._measuretrans.U1.Ittl, 15) >> 15];
            }
            set
            {
                this._measuretrans.U1.Ittl = Common.SetBits(this._measuretrans.U1.Ittl, (ushort)Strings.Measure_Koef.IndexOf(value), 15);
            }
        }

        public string KTHXKoeff
        {
            get
            {
                return Strings.Measure_Koef[Common.GetBits(this._measuretrans.U1.Ittx, 15) >> 15];
            }
            set
            {
                this._measuretrans.U1.Ittx = Common.SetBits(this._measuretrans.U1.Ittx, (ushort)Strings.Measure_Koef.IndexOf(value), 15);
            }
        }

        public string PolarityL
        {
            get
            {
                return Strings.SygnalInputSignals[this._measuretrans.U1.PolarityL];
            }
            set
            {
                this._measuretrans.U1.PolarityL = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string PolarityX
        {
            get
            {
                return Strings.SygnalInputSignals[this._measuretrans.U1.PolarityX];
            }
            set
            {
                this._measuretrans.U1.PolarityX = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }
        #endregion

        #region ОМП
        public string OMPtype
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._confOMP.config, 0)];
            }
            set
            {
                this._confOMP.config = Common.SetBits(this._confOMP.config, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public double Hud
        {
            get
            {
                return this._confOMP.Xyg / (double)1000;
            }
            set
            {
                this._confOMP.Xyg = (ushort)(value * 1000);
            }
        }
        #endregion

        #region Реле
        public ushort[] OutputReleValues
        {
            get
            {
                List<ushort> rele = new List<ushort>();
                for (int i = 0; i < this._paramautomat.releoutrom.Length; i++)
                {
                    rele.AddRange(this._paramautomat.releoutrom[i].GetValues());
                }
                return rele.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < this._paramautomat.releoutrom.Length; i++)
                {
                    StructInfo sInfo = this._paramautomat.releoutrom[i].GetStructInfo(64);
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    this._paramautomat.releoutrom[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }
        #endregion

        #region Индикаторы 
        public ushort[] OutputIndicatorValues
        {
            get
            {
                List<ushort> rele = new List<ushort>();
                for (int i = 0; i < this._paramautomat.indicatorrom.Length; i++)
                {
                    rele.AddRange(this._paramautomat.indicatorrom[i].GetValues());
                }
                return rele.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < this._paramautomat.indicatorrom.Length; i++)
                {
                    StructInfo sInfo = this._paramautomat.indicatorrom[i].GetStructInfo(64);
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    this._paramautomat.indicatorrom[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }
        #endregion

        #region Параметры неисправности
        public string OutputN1
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramautomat.disrepair, 0);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this._paramautomat.disrepair = Common.SetBit(this._paramautomat.disrepair, 0, rez);
            }
        }

        public string OutputN2
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramautomat.disrepair, 1);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this._paramautomat.disrepair = Common.SetBit(this._paramautomat.disrepair, 1, rez);
            }
        }

        public string OutputN3
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramautomat.disrepair, 2);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this._paramautomat.disrepair = Common.SetBit(this._paramautomat.disrepair, 2, rez);
            }
        }

        public string OutputN4
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramautomat.disrepair, 3);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                this._paramautomat.disrepair = Common.SetBit(this._paramautomat.disrepair, 3, rez);
            }
        }

        public int OutputImp
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._paramautomat.disrepairImp);
            }
            set
            {
                this._paramautomat.disrepairImp = ValuesConverterCommon.SetWaitTime(value);
            }
        }
        #endregion

        #region Выключатель
        public string SwitchOff
        {
            get { return Strings.SygnalInputSignals[this._switch.off]; }
            set { this._switch.off = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchOn
        {
            get { return Strings.SygnalInputSignals[this._switch.on]; }
            set { this._switch.on = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchError
        {
            get { return Strings.SygnalInputSignals[this._switch.err]; }
            set { this._switch.err = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchBlock
        {
            get { return Strings.SygnalInputSignals[this._switch.block]; }
            set { this._switch.block = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int SwitchTUrov
        {
            get { return ValuesConverterCommon.GetWaitTime(this._switch.timeUrov); }
            set { this._switch.timeUrov = ValuesConverterCommon.SetWaitTime(value); }
        }

        public double SwitchIUrov
        {
            get
            {
                return ValuesConverterCommon.GetIn(this._switch.urov);/* Measuring.GetConstraintOnly(_switch.urov, ConstraintKoefficient.K_4001);*/
            }
            set
            {
                this._switch.urov = ValuesConverterCommon.SetIn(value);
                /* Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);*/
            }
        }

        public int SwitchImpuls
        {
            get { return ValuesConverterCommon.GetWaitTime(this._switch.timeImp); }
            set { this._switch.timeImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int SwitchTUskor
        {
            get { return ValuesConverterCommon.GetWaitTime(this._switch.timeAcc); }
            set { this._switch.timeAcc = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string SwitchKontCep
        {
            get { return Strings.ModesLight[this._switch.ccde]; }
            set { this._switch.ccde = (ushort)Strings.ModesLight.IndexOf(value); }
        }
        #endregion

        #region АПВ
        public string APVModes
        {
            get
            {
                return Strings.APVMode[(Common.GetBits(this._apv.config, 0, 1, 2) >> 0)];
            }
            set
            {
                this._apv.config = Common.SetBits(this._apv.config, (ushort)Strings.APVMode.IndexOf(value), 0, 1, 2);
            }
        }

        public string APVBlocking
        {
            get { return Strings.SygnalInputSignals[this._apv.block]; }
            set { this._apv.block = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int APVTBlock
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.timeBlock); }
            set
            { this._apv.timeBlock = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APVTReady
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.ctrl); }
            set { this._apv.ctrl = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APV1Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.step1); }
            set { this._apv.step1 = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APV2Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.step2); }
            set
            {
                this._apv.step2 = ValuesConverterCommon.SetWaitTime(value);
            }

        }

        public int APV3Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.step3); }
            set
            {
                this._apv.step3 = ValuesConverterCommon.SetWaitTime(value);
            }

        }

        public int APV4Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.step4); }
            set
            {
                this._apv.step4 = ValuesConverterCommon.SetWaitTime(value);
            }

        }

        public string APVOff
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(this._apv.config, 8) >> 8)];
            }
            set
            {
                this._apv.config = Common.SetBits(this._apv.config, (ushort)Strings.YesNo.IndexOf(value), 8);
            }
        }
        #endregion

        #region АВР
        public string AVRBySignal
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(this._avr.config, 0) >> 0)];
            }
            set
            {
                this._avr.config = Common.SetBits(this._avr.config, (ushort)Strings.YesNo.IndexOf(value), 0);
            }
        }

        public string AVRByOff
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(this._avr.config, 2) >> 2)];
            }
            set
            {
                this._avr.config = Common.SetBits(this._avr.config, (ushort)Strings.YesNo.IndexOf(value), 2);
            }
        }

        public string AVRBySelfOff
        {
            get { return Strings.YesNo[(Common.GetBits(this._avr.config, 1) >> 1)]; }
            set { this._avr.config = Common.SetBits(this._avr.config, (ushort)Strings.YesNo.IndexOf(value), 1); }
        }

        public string AVRByDiff
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(this._avr.config, 3) >> 3)];
            }
            set
            {
                this._avr.config = Common.SetBits(this._avr.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string AVRSIGNOn
        {
            get { return Strings.SygnalInputSignals[this._avr.on]; }
            set { this._avr.on = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string AVRBlocking
        {
            get { return Strings.SygnalInputSignals[this._avr.block]; }
            set { this._avr.block = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string AVRBlockClear
        {
            get { return Strings.SygnalInputSignals[this._avr.clear]; }
            set { this._avr.clear = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string AVRResolve
        {
            get { return Strings.SygnalInputSignals[this._avr.start]; }
            set { this._avr.start = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int AVRTimeOn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._avr.timeOn); }
            set { this._avr.timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string AVRBack
        {
            get { return Strings.SygnalInputSignals[this._avr.off]; }
            set { this._avr.off = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int AVRTimeBack
        {
            get { return ValuesConverterCommon.GetWaitTime(this._avr.timeOff); }
            set { this._avr.timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int AVRTimeOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(this._avr.timeOtkl); }
            set { this._avr.timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string AVRClear
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._avr.config, 7))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._avr.config = Common.SetBit(this._avr.config, 7, res);
            }
        }

        #endregion

        #region ЛЗШ
        public string LZHMode
        {
            get
            {
                int index = (this._lpb.config < Strings.LZHModes.Count) ? this._lpb.config : 0;
                return Strings.LZHModes[index];
            }
            set
            {
                this._lpb.config = (ushort)Strings.LZHModes.IndexOf(value);
            }
        }

        public double LZHUstavka
        {
            get
            {
                return ValuesConverterCommon.GetIn(this._lpb.val);/* Measuring.GetConstraintOnly(_lpb.val, ConstraintKoefficient.K_4001);*/
            }
            set
            {
                this._lpb.val = ValuesConverterCommon.SetIn(value);/* Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);*/
            }
        }
        #endregion

        #region Управление
        public string SwitchKeyOn
        {
            get { return Strings.SygnalInputSignals[this._switch.keyOn]; }
            set { this._switch.keyOn = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchKeyOff
        {
            get { return Strings.SygnalInputSignals[this._switch.keyOff]; }
            set { this._switch.keyOff = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchVneshOn
        {
            get { return Strings.SygnalInputSignals[this._switch.extOn]; }
            set { this._switch.extOn = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchVneshOff
        {
            get { return Strings.SygnalInputSignals[this._switch.extOff]; }
            set { this._switch.extOff = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchButtons
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.config, 0))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.config = Common.SetBit(this._switch.config, 0, res);
            }
        }

        public string SwitchKey
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.config, 1))
                {
                    index = 0;
                }
                return Strings.Forbidden2[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden2.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.config = Common.SetBit(this._switch.config, 1, res);
            }
        }

        public string SwitchVnesh
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.config, 2))
                {
                    index = 0;
                }
                return Strings.Forbidden2[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden2.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.config = Common.SetBit(this._switch.config, 2, res);
            }
        }

        public string SwitchSDTU
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.config, 3))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.config = Common.SetBit(this._switch.config, 3, res);
            }
        }
        #endregion

        #region ВЛС

        public ushort[] VLS
        {
            get
            {
                ushort[] res = new ushort[this._elssignal.i.Length * this._elssignal.i[0].a.Length];
                int index = 0;
                for (int i = 0; i < this._elssignal.i.Length; i++)
                {
                    Array.ConstrainedCopy(this._elssignal.i[i].a, 0, res, index, this._elssignal.i[i].a.Length);
                    index += this._elssignal.i[i].a.Length;
                }

                return res;
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < this._elssignal.i.Length; i++)
                {
                    Array.ConstrainedCopy(res, index, this._elssignal.i[i].a, 0, this._elssignal.i[i].a.Length);
                    index += this._elssignal.i[i].a.Length;
                }
            }
        }

        #endregion

        #region Входные сигналы
        public ushort[] InpSygnals
        {
            get
            {
                List<ushort> res = new List<ushort>();
                for (int i = 0; i < this._inpsignal.i.Length; i++)
                {
                    res.AddRange(this._inpsignal.i[i].GetValues());
                }

                return res.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < this._inpsignal.i.Length; i++)
                {
                    StructInfo sInfo = this._inpsignal.i[i].GetStructInfo(64);
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    this._inpsignal.i[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }

        public string GrUst
        {
            get
            {
                return Strings.SygnalInputSignals[this._inputsignal.groopUst];
            }
            set
            {
                this._inputsignal.groopUst = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string SbInd
        {
            get
            {
                return Strings.SygnalInputSignals[this._inputsignal.clrInd];
            }
            set
            {
                this._inputsignal.clrInd = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }
        #endregion

        #region Защиты I
        public string I_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzMain.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double I_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzMain.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this._mtzMain.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public double I_UPUSK
        {
            get
            {
                return Measuring.GetU(this._mtzMain.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzMain.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string I_UPUSK_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMain.config, 3) >> 3];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string I_DIRECTION
        {
            get
            {
                return Strings.BusDirection[Common.GetBits(this._mtzMain.config, 6, 7) >> 6];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.BusDirection.IndexOf(value), 6, 7);
            }
        }

        public string I_UNDIRECTION
        {
            get
            {
                return Strings.UnDirection[Common.GetBits(this._mtzMain.config, 8) >> 8];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.UnDirection.IndexOf(value), 8);
            }
        }

        public string I_LOGIC
        {
            get
            {
                return Strings.TokParameter[Common.GetBits(this._mtzMain.config, 12) >> 12];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.TokParameter.IndexOf(value), 12);
            }
        }

        public string I_CHAR
        {
            get
            {
                return Strings.Characteristic[Common.GetBits(this._mtzMain.config, 4) >> 4];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.Characteristic.IndexOf(value), 4);
            }
        }

        public int I_T
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMain.time);
            }
            set
            {
                this._mtzMain.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public ushort I_K
        {
            get
            {
                return this._mtzMain.k;
            }
            set
            {
                this._mtzMain.k = value;
            }
        }

        public int I_TY
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMain.tu);
            }
            set
            {
                this._mtzMain.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string I_TY_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMain.config, 5) >> 5];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 5);
            }
        }

        public string I_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzMain.block];
            }
            set
            {
                this._mtzMain.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public double I_2r1r
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzMain.I21, ConstraintKoefficient.K_10000);
            }
            set
            {
                this._mtzMain.I21 = Measuring.SetConstraint(value, ConstraintKoefficient.K_10000);
            }
        }

        public string I_2r1r_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMain.config, 9) >> 9];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 9);
            }
        }

        public string I_BLOCK_DIRECT
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMain.config, 10) >> 10];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 10);
            }
        }

        public string I_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzMain.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzMain.config1 = Common.SetBits(this._mtzMain.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string I_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMain.config, 2) >> 2];
            }
            set
            {
                this._mtzMain.config = Common.SetBits(this._mtzMain.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string I_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMain.config1, 0) >> 0];
            }
            set
            {
                this._mtzMain.config1 = Common.SetBits(this._mtzMain.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string I_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMain.config1, 1) >> 1];
            }
            set
            {
                this._mtzMain.config1 = Common.SetBits(this._mtzMain.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты I*
        public string I0_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzMainI0.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double I0_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzMainI0.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this._mtzMainI0.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public double I0_UPUSK
        {
            get
            {
                return Measuring.GetU(this._mtzMainI0.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzMainI0.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string I0_UPUSK_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMainI0.config, 3) >> 3];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string I0_DIRECTION
        {
            get
            {
                return Strings.BusDirection[Common.GetBits(this._mtzMainI0.config, 6, 7) >> 6];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.BusDirection.IndexOf(value), 6, 7);
            }
        }

        public string I0_UNDIRECTION
        {
            get
            {
                return Strings.UnDirection[Common.GetBits(this._mtzMainI0.config, 8) >> 8];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.UnDirection.IndexOf(value), 8);
            }
        }

        public string I0_I0
        {
            get
            {
                return Strings.I0Modes[Common.GetBits(this._mtzMainI0.config, 12, 13) >> 12];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.I0Modes.IndexOf(value), 12, 13);
            }
        }

        public string I0_CHAR
        {
            get
            {
                return Strings.Characteristic[Common.GetBits(this._mtzMainI0.config, 4) >> 4];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.Characteristic.IndexOf(value), 4);
            }
        }

        public int I0_T
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMainI0.time);
            }
            set
            {
                this._mtzMainI0.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public ushort I0_K
        {
            get
            {
                return this._mtzMainI0.k;
            }
            set
            {
                this._mtzMainI0.k = value;
            }
        }

        public string I0_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzMainI0.block];
            }
            set
            {
                this._mtzMainI0.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string I0_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzMainI0.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzMainI0.config1 = Common.SetBits(this._mtzMainI0.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public int I0_TY
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMainI0.tu);
            }
            set
            {
                this._mtzMainI0.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string I0_TY_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMainI0.config, 5) >> 5];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.YesNo.IndexOf(value), 5);
            }
        }

        public string I0_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainI0.config, 2) >> 2];
            }
            set
            {
                this._mtzMainI0.config = Common.SetBits(this._mtzMainI0.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string I0_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainI0.config1, 0) >> 0];
            }
            set
            {
                this._mtzMainI0.config1 = Common.SetBits(this._mtzMainI0.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string I0_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainI0.config1, 1) >> 1];
            }
            set
            {
                this._mtzMainI0.config1 = Common.SetBits(this._mtzMainI0.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты I2I1
        public string I2I1_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzMainI2I1.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzMainI2I1.config = Common.SetBits(this._mtzMainI2I1.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string I2I1_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzMainI2I1.block];
            }
            set
            {
                this._mtzMainI2I1.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public double I2I1_I2I1
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzMainI2I1.ust, ConstraintKoefficient.K_10000);
            }
            set
            {
                this._mtzMainI2I1.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_10000);
            }
        }

        public int I2I1_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMainI2I1.time);
            }
            set
            {
                this._mtzMainI2I1.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string I2I1_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzMainI2I1.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzMainI2I1.config1 = Common.SetBits(this._mtzMainI2I1.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string I2I1_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainI2I1.config, 2) >> 2];
            }
            set
            {
                this._mtzMainI2I1.config = Common.SetBits(this._mtzMainI2I1.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string I2I1_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainI2I1.config1, 0) >> 0];
            }
            set
            {
                this._mtzMainI2I1.config1 = Common.SetBits(this._mtzMainI2I1.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string I2I1_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainI2I1.config1, 1) >> 1];
            }
            set
            {
                this._mtzMainI2I1.config1 = Common.SetBits(this._mtzMainI2I1.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты Ir
        public string Ir_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzMainIr.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzMainIr.config = Common.SetBits(this._mtzMainIr.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string Ir_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzMainIr.block];
            }
            set
            {
                this._mtzMainIr.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public double Ir_UPUSK
        {
            get
            {
                return Measuring.GetU(this._mtzMainIr.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzMainIr.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string Ir_UPUSK_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMainIr.config, 3) >> 3];
            }
            set
            {
                this._mtzMainIr.config = Common.SetBits(this._mtzMainIr.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public double Ir_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzMainIr.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                this._mtzMainIr.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public int Ir_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMainIr.time);
            }
            set
            {
                this._mtzMainIr.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }


        public int Ir_TY
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzMainIr.tu);
            }
            set
            {
                this._mtzMainIr.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string Ir_TY_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzMainIr.config, 5) >> 5];
            }
            set
            {
                this._mtzMainIr.config = Common.SetBits(this._mtzMainIr.config, (ushort)Strings.YesNo.IndexOf(value), 5);
            }
        }

        public string Ir_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzMainIr.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzMainIr.config1 = Common.SetBits(this._mtzMainIr.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string Ir_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainIr.config, 2) >> 2];
            }
            set
            {
                this._mtzMainIr.config = Common.SetBits(this._mtzMainIr.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string Ir_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainIr.config1, 0) >> 0];
            }
            set
            {
                this._mtzMainIr.config1 = Common.SetBits(this._mtzMainIr.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string Ir_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzMainIr.config1, 1) >> 1];
            }
            set
            {
                this._mtzMainIr.config1 = Common.SetBits(this._mtzMainIr.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты F>
        public string FB_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzFMax.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzFMax.config = Common.SetBits(this._mtzFMax.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double FB_FCP
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzFMax.ust, ConstraintKoefficient.K_25600);
            }
            set
            {
                this._mtzFMax.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public int FB_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzFMax.time);
            }
            set
            {
                this._mtzFMax.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double FB_FVZ
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzFMax.u, ConstraintKoefficient.K_25600);
            }
            set
            {
                this._mtzFMax.u = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public int FB_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzFMax.tu);
            }
            set
            {
                this._mtzFMax.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }
        /// <summary>
        /// //////////
        /// </summary>
        public string FB_FVZ_YN
        {
            get
            {

                return Strings.YesNo[Common.GetBits(this._mtzFMax.config, 3) >> 3];
            }
            set
            {
                this._mtzFMax.config = Common.SetBits(this._mtzFMax.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string FB_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzFMax.block];
            }
            set
            {
                this._mtzFMax.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string FB_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzFMax.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzFMax.config1 = Common.SetBits(this._mtzFMax.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string FB_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMax.config1, 2) >> 2];
            }
            set
            {
                this._mtzFMax.config1 = Common.SetBits(this._mtzFMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FB_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMax.config, 2) >> 2];
            }
            set
            {
                this._mtzFMax.config = Common.SetBits(this._mtzFMax.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FB_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMax.config1, 0) >> 0];
            }
            set
            {
                this._mtzFMax.config1 = Common.SetBits(this._mtzFMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string FB_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMax.config1, 1) >> 1];
            }
            set
            {
                this._mtzFMax.config1 = Common.SetBits(this._mtzFMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string FB_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzFMax.config, 15) >> 15];
            }
            set
            {
                this._mtzFMax.config = Common.SetBits(this._mtzFMax.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Защиты F<
        public string FM_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzFMin.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzFMin.config = Common.SetBits(this._mtzFMin.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double FM_FCP
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzFMin.ust, ConstraintKoefficient.K_25600);
            }
            set
            {
                this._mtzFMin.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public int FM_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzFMin.time);
            }
            set
            {
                this._mtzFMin.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }


        public int FM_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzFMin.tu);
            }
            set
            {
                this._mtzFMin.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double FM_FVZ
        {
            get
            {
                return Measuring.GetConstraintOnly(this._mtzFMin.u, ConstraintKoefficient.K_25600);
            }
            set
            {
                this._mtzFMin.u = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public string FM_FVZ_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzFMin.config, 3) >> 3];
            }
            set
            {
                this._mtzFMin.config = Common.SetBits(this._mtzFMin.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string FM_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzFMin.block];
            }
            set
            {
                this._mtzFMin.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string FM_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzFMin.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzFMin.config1 = Common.SetBits(this._mtzFMin.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string FM_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMin.config1, 2) >> 2];
            }
            set
            {
                this._mtzFMin.config1 = Common.SetBits(this._mtzFMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FM_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMin.config, 2) >> 2];
            }
            set
            {
                this._mtzFMin.config = Common.SetBits(this._mtzFMin.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FM_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMin.config1, 0) >> 0];
            }
            set
            {
                this._mtzFMin.config1 = Common.SetBits(this._mtzFMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string FM_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzFMin.config1, 1) >> 1];
            }
            set
            {
                this._mtzFMin.config1 = Common.SetBits(this._mtzFMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string FM_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzFMin.config, 15) >> 15];
            }
            set
            {
                this._mtzFMin.config = Common.SetBits(this._mtzFMin.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Защиты U>
        public string UB_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzUMax.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzUMax.config = Common.SetBits(this._mtzUMax.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string UB_TYPE
        {
            get
            {
                return Strings.UType[Common.GetBits(this._mtzUMax.config, 5, 6, 7) >> 5];
            }
            set
            {
                this._mtzUMax.config = Common.SetBits(this._mtzUMax.config, (ushort)Strings.UType.IndexOf(value), 5, 6, 7);
            }
        }

        public double UB_UCP
        {
            get
            {
                return Measuring.GetU(this._mtzUMax.ust, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzUMax.ust = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public int UB_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzUMax.time);
            }
            set
            {
                this._mtzUMax.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public int UB_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzUMax.tu);
            }
            set
            {
                this._mtzUMax.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double UB_UVZ
        {
            get
            {
                return Measuring.GetU(this._mtzUMax.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzUMax.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string UB_UVZ_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzUMax.config, 3) >> 3];
            }
            set
            {
                this._mtzUMax.config = Common.SetBits(this._mtzUMax.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string UB_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzUMax.block];
            }
            set
            {
                this._mtzUMax.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string UB_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzUMax.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzUMax.config1 = Common.SetBits(this._mtzUMax.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string UB_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMax.config1, 2) >> 2];
            }
            set
            {
                this._mtzUMax.config1 = Common.SetBits(this._mtzUMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UB_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMax.config, 2) >> 2];
            }
            set
            {
                this._mtzUMax.config = Common.SetBits(this._mtzUMax.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UB_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMax.config1, 0) >> 0];
            }
            set
            {
                this._mtzUMax.config1 = Common.SetBits(this._mtzUMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string UB_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMax.config1, 1) >> 1];
            }
            set
            {
                this._mtzUMax.config1 = Common.SetBits(this._mtzUMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string UB_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzUMax.config, 15) >> 15];
            }
            set
            {
                this._mtzUMax.config = Common.SetBits(this._mtzUMax.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Защиты U<
        public string UM_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzUMin.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzUMin.config = Common.SetBits(this._mtzUMin.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string UM_TYPE
        {
            get
            {
                return Strings.UType[Common.GetBits(this._mtzUMin.config, 5, 6, 7) >> 5];
            }
            set
            {
                this._mtzUMin.config = Common.SetBits(this._mtzUMin.config, (ushort)Strings.UType.IndexOf(value), 5, 6, 7);
            }
        }

        public double UM_UCP
        {
            get
            {
                return Measuring.GetU(this._mtzUMin.ust, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzUMin.ust = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public int UM_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzUMin.time);
            }
            set
            {
                this._mtzUMin.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public int UM_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzUMin.tu);
            }
            set
            {
                this._mtzUMin.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double UM_UVZ
        {
            get
            {
                return Measuring.GetU(this._mtzUMin.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                this._mtzUMin.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string UM_UVZ_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzUMin.config, 3) >> 3];
            }
            set
            {
                this._mtzUMin.config = Common.SetBits(this._mtzUMin.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string UM_BLOCK_M5
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzUMin.config, 4) >> 4];
            }
            set
            {
                this._mtzUMin.config = Common.SetBits(this._mtzUMin.config, (ushort)Strings.YesNo.IndexOf(value), 4);
            }
        }

        public string UM_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[this._mtzUMin.block];
            }
            set
            {
                this._mtzUMin.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string UM_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzUMin.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzUMin.config1 = Common.SetBits(this._mtzUMin.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string UM_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMin.config1, 2) >> 2];
            }
            set
            {
                this._mtzUMin.config1 = Common.SetBits(this._mtzUMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UM_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMin.config, 2) >> 2];
            }
            set
            {
                this._mtzUMin.config = Common.SetBits(this._mtzUMin.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UM_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMin.config1, 0) >> 0];
            }
            set
            {
                this._mtzUMin.config1 = Common.SetBits(this._mtzUMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string UM_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzUMin.config1, 1) >> 1];
            }
            set
            {
                this._mtzUMin.config1 = Common.SetBits(this._mtzUMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string UM_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzUMin.config, 15) >> 15];
            }
            set
            {
                this._mtzUMin.config = Common.SetBits(this._mtzUMin.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Внешние защиты
        public string EXTERNAL_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzExt.config, 0, 1) >> 0];
            }
            set
            {
                this._mtzExt.config = Common.SetBits(this._mtzExt.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string EXTERNAL_SRAB
        {
            get
            {
                return Strings.SygnalSrabExternal[this._mtzExt.ust];
            }
            set
            {
                this._mtzExt.ust = (ushort)Strings.SygnalSrabExternal.IndexOf(value);
            }
        }


        public int EXTERNAL_TSR
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzExt.time);
            }
            set
            {
                this._mtzExt.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public int EXTERNAL_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._mtzExt.tu);
            }
            set
            {
                this._mtzExt.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string EXTERNAL_VOZVR
        {
            get
            {
                return Strings.SygnalSrabExternal[this._mtzExt.u];
            }
            set
            {
                this._mtzExt.u = (ushort)Strings.SygnalSrabExternal.IndexOf(value);
            }
        }


        public string EXTERNAL_VOZVR_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzExt.config, 3) >> 3];
            }
            set
            {
                this._mtzExt.config = Common.SetBits(this._mtzExt.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string EXTERNAL_BLOCK
        {
            get
            {
                return Strings.SygnalSrabExternal[this._mtzExt.block];
            }
            set
            {
                this._mtzExt.block = (ushort)Strings.SygnalSrabExternal.IndexOf(value);
            }
        }

        public string EXTERNAL_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._mtzExt.config1, 4, 5) >> 4];
            }
            set
            {
                this._mtzExt.config1 = Common.SetBits(this._mtzExt.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string EXTERNAL_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzExt.config1, 2) >> 2];
            }
            set
            {
                this._mtzExt.config1 = Common.SetBits(this._mtzExt.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string EXTERNAL_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzExt.config, 2) >> 2];
            }
            set
            {
                this._mtzExt.config = Common.SetBits(this._mtzExt.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string EXTERNAL_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(this._mtzExt.config1, 0) >> 0];
            }
            set
            {
                this._mtzExt.config1 = Common.SetBits(this._mtzExt.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string EXTERNAL_AVR
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._mtzExt.config1, 1) >> 1];
            }
            set
            {
                this._mtzExt.config1 = Common.SetBits(this._mtzExt.config1, (ushort)Strings.ModesLightMode.IndexOf(value), 1);
            }
        }

        public string EXTERNAL_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(this._mtzExt.config, 15) >> 15];
            }
            set
            {
                this._mtzExt.config = Common.SetBits(this._mtzExt.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion 

        #region Конфигурация Осциллографа
        public string OSC_LENGTH
        {
            get
            {
                return Strings.OscLength[this._osc.size - 1];
            }
            set
            {
                this._osc.size = (ushort)(Strings.OscLength.IndexOf(value) + 1);
            }
        }

        public ushort OSC_W_LENGTH
        {
            get
            {
                return this._osc.percent;
            }
            set
            {
                this._osc.percent = value;
            }
        }

        public string OSC_FIX
        {
            get
            {
                return Strings.OscFix[this._osc.config];
            }
            set
            {
                this._osc.config = (ushort)Strings.OscFix.IndexOf(value);
            }
        }
        #endregion

        #region Emulation
        public MemoryEntity<WriteStructEmul> WriteStructEmulation
        {
            get { return this._writeSructEmulation; }
        }

        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull
        {
            get { return this._writeSructEmulationNull; }
        }
        public MemoryEntity<ReadStructEmul> ReadStructEmulation
        {
            get { return this._readSructEmulation; }
        }
        #endregion Emulation


        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;

        public MemoryEntity<OneWordStruct> StopSpl => this._stopSpl;

	    public MemoryEntity<OneWordStruct> StartSpl => this._startSpl;

	    public MemoryEntity<OneWordStruct> StateSpl => this._stateSpl;


	    private new void mb_CompleteExchange(object sender, Query query)
        {
            if (query.name == "version" + this.DeviceNumber)
            {
                this.SetStartConnectionState(!query.error);
                DeviceInfo info = DeviceInfo.GetVersion(Common.SwapArrayItems(query.readBuffer));
                this.Info = info;
                if (Common.VersionConverter(info.Version) >= 3.03 && !_isCheckVersion)
                {
                    _isCheckVersion = true;
                    this._infoSlot = new slot(0x500, 0x500 + 0x20);
                    this.MB.RemoveQuery("version" + this.DeviceNumber);
                    base.LoadVersion(this);
                    return;
                }
                this.LoadVersionOk();
                _isCheckVersion = false;
                return;
            }
            this.InitDeviceNumbers();
            if ("МР761 запрос подтверждения" + this.DeviceNumber == query.name)
            {
                if (query.error)
                {
                    if (this.ConfigWriteFail != null)
                    {
                        this.ConfigWriteFail.Invoke();
                    }
                }
                else
                {
                    if (this.ConfigWriteOk != null)
                    {
                        this.ConfigWriteOk.Invoke();
                    }
                }
            }
            if (query.name == "ConfirmConfig" + this.DeviceNumber)
            {
                if (query.error)
                {
                    MessageBox.Show("Не удалось записать бит подтверждения изменения конфигурации. Конфигурация в устройстве не изменена",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        private const int BAUDE_RATE_MAX = 921600;
        private const ushort START_ADDR_MEAS_TRANS = 0x1278;
        private ushort _groupUstSize;
        public ushort GetStartAddrMeasTrans(int group, string version)
        {
            if (string.IsNullOrEmpty(version)) return START_ADDR_MEAS_TRANS;
            return (ushort)(START_ADDR_MEAS_TRANS + this._groupUstSize * group);
        }

        #region IDeviceVersion

        public const string T4N5D42R19 = "T4N5D42R35";
        public const string T4N4D42R19 = "T4N4D42R35";
        
        private static readonly string[] _deviceApparatConfig = { T4N4D42R19, T4N5D42R19 };

        private static readonly string[] _deviceOutString = { T4N4D42R19, T4N5D42R19 };

        public Type[] Forms
        {
            get
            {
                if (this.DeviceVersion == "1.00 - 1.01")
                {
                    this.DeviceVersion = "1.01";
                }
                if (this.DeviceVersion == "1.02 - 1.05")
                {
                    this.DeviceVersion = "1.02";
                }

                double ver = Common.VersionConverter(this.DeviceVersion);
                this._groupUstSize = ver >= 3.04
                    ? new GroupSetpoint304().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize
                    : new GroupSetpointStructV303().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64)
                        .FullSize;
                this._mr761DeviceV2 = new Mr761DeviceV2(this);
                StringsConfig.CurrentVersion = ver;
                if (ver >= 3.03)
                {
                    //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                    //{
                    //    ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                    //    choice.ShowDialog();
                    //    this.DevicePlant = choice.DeviceType;
                    //}
                    return new[]
                    {
                        typeof (BSBGLEFV3),
                        typeof (Mr761ConfigurationFormV300),
                        typeof (Mr761MeasuringFormV300),
                        typeof (Mr761OscilloscopeFormV3),
                        typeof (Mr761SystemJournalFormV3),
                        typeof (Mr761AlarmJournalFormV3),
                        typeof (EmulationForm)
                        //typeof (DiagnosticForm)
                    };
                }
                if (ver >= 3.02)
                {
                    this._groupUstSize = new GroupSetpointStruct().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize;
                    return new[]
                    {
                        typeof (BSBGLEFV3),
                        typeof (Mr761ConfigurationFormV300),
                        typeof (Mr761MeasuringFormV300),
                        typeof (Mr761OscilloscopeFormV3),
                        typeof (Mr761SystemJournalFormV3),
                        typeof (Mr761AlarmJournalFormV3),
                        typeof (EmulationForm)
                        //typeof (DiagnosticForm)
                    };
                }
                if (ver >= 3.00 && ver < 3.02)
                {
                    return new[]
                    {
                        typeof (BSBGLEFV3),
                        typeof (Mr761ConfigurationFormV300),
                        typeof (Mr761MeasuringFormV300),
                        typeof (Mr761OscilloscopeFormV3),
                        typeof (Mr761SystemJournalFormV3),
                        typeof (Mr761AlarmJournalFormV3)

                    };
                }
                if (ver >= 2.04 && ver < 3.00)
                {
                    return new[]
                    {
                        typeof (Mr761AlarmJournalFormV2),
                        typeof (BSBGLEF),
                        typeof (Mr761ConfigurationFormV204),
                        typeof (Mr761MeasuringFormV201),
                        typeof (Mr761OscilloscopeFormV2),
                        typeof (Mr761SystemJournalFormV2)
                    };
                }
                if (ver >= 2.01 && ver < 2.04)
                {
                    return new[]
                    {
                        typeof (Mr761AlarmJournalFormV2),
                        typeof (BSBGLEF),
                        typeof (Mr761ConfigurationFormV201),
                        typeof (Mr761MeasuringFormV201),
                        typeof (Mr761OscilloscopeFormV2),
                        typeof (Mr761SystemJournalFormV2)
                    };
                }
                if (Math.Abs(ver - 2.00) < 0.001)
                {
                    return new[]
                    {
                        typeof (Mr761AlarmJournalFormV2),
                        typeof (BSBGLEF),
                        typeof (Mr761ConfigurationFormV2),
                        typeof (Mr761MeasuringFormV2),
                        typeof (Mr761OscilloscopeFormV2),
                        typeof (Mr761SystemJournalFormV2)
                    };
                }
                return new[]
                {
                    typeof (Mr761AlarmJournalFormV1),
                    typeof (BSBGLEF),
                    typeof (Mr761ConfigurationForm),
                    typeof (Mr761MeasuringForm),
                    typeof (Mr761OscilloscopeFormV1),
                    typeof (Mr761SystemJournalForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.00 - 1.01",
                    "1.02 - 1.05",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "3.00",
                    "3.01",
                    "3.02",
                    "3.03",
                    "3.04",
                    "3.05",
                    "3.06",
                    "3.07",
                    "3.08"
                };
            }
        }
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MR761); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mrBig; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР761"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
    }
}
