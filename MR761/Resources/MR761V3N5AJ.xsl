<?xml version="1.0" encoding="utf-8"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 
</head>
<body>
 <h3>Устройство МР761. Журнал аварий</h3>
	<p></p>
    <table border="1">
      <tr bgcolor="#c1ced5">
        <th>Номер</th>
        <th>Время</th>
        <th>Сообщение</th>
		   <th>Сработавшая защита</th>
		   <th>Параметр срабатывания</th>
		   <th>Значение параметра срабатывания</th>
		   <th>Группа уставок</th>
		   <th>Rab</th>
		   <th>Xab</th>
		   <th>Rbc</th>
		   <th>Xbc</th>
		   <th>Rca</th>
		   <th>Xca</th>
		   <th>Контур Ф-N</th>
        <th>Ra</th>
        <th>Xa</th>
        <th>Rb</th>
        <th>Xb</th>
        <th>Rc</th>
        <th>Xc</th>
		 <th>Ia</th>
		 <th>Ib</th>
		 <th>Ic</th>
		 <th>I1</th>
		 <th>I2</th>
		 <th>3I0</th>
		 <th>In</th>
		 <th>Iг</th>
		 <th>Ua</th>
		 <th>Ub</th>
		 <th>Uc</th>
		 <th>Uab</th>
		 <th>Ubc</th>
		 <th>Uca</th>
		 <th>U1</th>
		 <th>U2</th>
		 <th>3U0</th> 
     <th>Un</th>
        <th>Un1</th>
		 <th>F</th>
		 <th>Q</th>
		 <th>Вхсигналы 1-8</th>
		 <th>Вхсигналы 9-16</th>
		 <th>Вхсигналы 17-24</th>
		 <th>Вхсигналы 25-32</th>
		 <th>Вхсигналы 33-40</th>
      </tr>  
	  <xsl:for-each select="DocumentElement/МР761_журнал_аварий">
	  <tr align="center">
		  <td><xsl:value-of select="_indexCol"/></td>
		  <td><xsl:value-of select="_timeCol"/></td>
		  <td><xsl:value-of select="_msg1Col"/></td>
		  <td><xsl:value-of select="_msgCol"/></td>
		  <td><xsl:value-of select="_codeCol"/></td>
		  <td><xsl:value-of select="_typeCol"/></td>
		  <td><xsl:value-of select="_groupCol"/></td>
		  <td><xsl:value-of select="_rabCol"/></td>
		  <td><xsl:value-of select="_xabCol"/></td>
		  <td><xsl:value-of select="_rbcCol"/></td>
		  <td><xsl:value-of select="_xbcCol"/></td>
		  <td><xsl:value-of select="_rcaCol"/></td>
		  <td><xsl:value-of select="_xcaCol"/></td>
		  <td><xsl:value-of select="_stepColumn"/></td>
		  <td><xsl:value-of select="_ra1Col"/></td>
		  <td><xsl:value-of select="_xa1Col"/></td>
		  <td><xsl:value-of select="_rb1Col"/></td>
		  <td><xsl:value-of select="_xb1Col"/></td>
		  <td><xsl:value-of select="_rc1Col"/></td>
		  <td><xsl:value-of select="_xc1Col"/></td>
		  <td><xsl:value-of select="_IaCol"/></td>
		  <td><xsl:value-of select="_IbCol"/></td>
		  <td><xsl:value-of select="_IcCol"/></td>
		  <td><xsl:value-of select="_I1Col"/></td>
		  <td><xsl:value-of select="_I2Col"/></td>
		  <td><xsl:value-of select="_3I0Col"/></td>
		  <td><xsl:value-of select="_InCol"/></td>
		  <td><xsl:value-of select="_IgCol"/></td> 
		  <td><xsl:value-of select="_UaCol"/></td>
		  <td><xsl:value-of select="_UbCol"/></td>
		  <td><xsl:value-of select="_UcCol"/></td>
		  <td><xsl:value-of select="_UabCol"/></td>
		  <td><xsl:value-of select="_UbcCol"/></td>
		  <td><xsl:value-of select="_UcaCol"/></td>
		  <td><xsl:value-of select="_U1Col"/></td>
		  <td><xsl:value-of select="_U2Col"/></td>
		  <td><xsl:value-of select="_3U0Col"/></td>
      <td><xsl:value-of select="_UnCol"/></td>
      <td><xsl:value-of select="_Un1Col"/></td>
		  <td><xsl:value-of select="_FCol"/></td>
		  <td><xsl:value-of select="_QCol"/></td>
		  <td><xsl:value-of select="_D0Col"/></td>
		  <td><xsl:value-of select="_D1Col"/></td>
		  <td><xsl:value-of select="_D2Col"/></td>
		  <td><xsl:value-of select="_D3Col"/></td>
		  <td><xsl:value-of select="_D4Col"/></td>
	  </tr>
		 
    </xsl:for-each>
    </table>
</body>
  </html>
</xsl:template>
</xsl:stylesheet>
