﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761.Version2.Configuration.Structures.Apv;
using BEMN.MR761.Version2.Configuration.Structures.Avr;
using BEMN.MR761.Version2.Configuration.Structures.Defenses;
using BEMN.MR761.Version2.Configuration.Structures.Engine;
using BEMN.MR761.Version2.Configuration.Structures.InputSignals;
using BEMN.MR761.Version2.Configuration.Structures.Ls;
using BEMN.MR761.Version2.Configuration.Structures.Lzsh;
using BEMN.MR761.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR761.Version2.Configuration.Structures.Opm;
using BEMN.MR761.Version2.Configuration.Structures.Primary;
using BEMN.MR761.Version2.Configuration.Structures.RelayInd;
using BEMN.MR761.Version2.Configuration.Structures.Switch;
using BEMN.MR761.Version2.Configuration.Structures.Vls;
using BEMN.MR761.Version201.Configuration.Structures.Oscope;
using BEMN.MR761.Version201.Configuration.Structures.Sihronizm;

namespace BEMN.MR761.Version201.Configuration.Structures
{
    /// <summary>
    /// Конфигурация
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "МР76x")]
    public class ConfigurationStructV201 : StructBase
    {

        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType
        {
            get { return "МР761"; }
            set { }
        }

        #region [Private fields]

        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [Layout(0)] private SwitchStruct _sw;

        /// <summary>
        /// конфигурациия АПВ
        /// </summary>
        [Layout(1)] private ApvStruct _apv;

        /// <summary>
        /// конфигурациия АВР
        /// </summary>
        [Layout(2)] private AvrStruct _avr;

        /// <summary>
        /// конфигурациия ЛЗШ
        /// </summary>
        [Layout(3)] private LpbStruct _lpb;

        /// <summary>
        /// конфигурация тепловой модели
        /// </summary>
        [Layout(4)] private TermConfigStruct _termconfig;

        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [Layout(5)] private InputSignalStruct _impsg;

        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [Layout(6)] private OscopeStructV201 _osc;

        /// <summary>
        /// структура измерительного трансформатора
        /// </summary>
        [Layout(7)] private MeasureTransStruct _mt;

        /// <summary>
        /// структура входных логических сигналов
        /// </summary>
        [Layout(8)] private InputLogicSignalStruct _inp;

        /// <summary>
        /// структура выходных логических сигналов
        /// </summary>
        [Layout(9)] private OutputLogicSignalStruct _els;

        /// <summary>
        /// все защиты
        /// </summary>
        [Layout(10)] private AllDefensesSetpointsStruct _currentprotall;

        /// <summary>
        /// параметры автоматики(Реле и индикаторы)
        /// </summary>
        [Layout(11)] private AutomaticsParametersStruct _paramautomat;

        /// <summary>
        /// конфигурациия системы
        /// </summary>
        [Layout(12, Ignore = true)] private ConfigSystemStruct _cnfsys;

        /// <summary>
        /// конфигурациия омп
        /// </summary>
        [Layout(13)] private ConfigurationOpmStruct _ompdata;

        /// <summary>
        /// конфигурация синхронизма
        /// </summary>
        [Layout(14)] private SinhronizmStructV201 _sinhronizm;

        #endregion [Private fields]

        #region [Properties]

        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(0)]
        public SwitchStruct Sw
        {
            get { return this._sw; }
            set { this._sw = value; }
        }

        /// <summary>
        /// конфигурациия АПВ
        /// </summary>
        [XmlElement(ElementName = "АПВ")]
        [BindingProperty(1)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }

        /// <summary>
        /// конфигурациия АВР
        /// </summary>
        [XmlElement(ElementName = "АВР")]
        [BindingProperty(2)]
        public AvrStruct Avr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }

        /// <summary>
        /// конфигурациия ЛЗШ
        /// </summary>
        [XmlElement(ElementName = "ЛЗШ")]
        [BindingProperty(3)]
        public LpbStruct Lpb
        {
            get { return this._lpb; }
            set { this._lpb = value; }
        }

        /// <summary>
        /// конфигурация тепловой модели
        /// </summary>
        [XmlElement(ElementName = "Двигатель")]
        [BindingProperty(4)]
        public TermConfigStruct Termconfig
        {
            get { return this._termconfig; }
            set { this._termconfig = value; }
        }

        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(5)]
        public InputSignalStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }

        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(6)]
        public OscopeStructV201 Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }

        /// <summary>
        /// структура измерительного трансформатора
        /// </summary>
        [XmlElement(ElementName = "Измерительный_трансформатор")]
        [BindingProperty(7)]
        public MeasureTransStruct Mt
        {
            get { return this._mt; }
            set { this._mt = value; }
        }

        /// <summary>
        /// структура входных логических сигналов
        /// </summary>
        [XmlElement(ElementName = "Входные_логические_сигналы")]
        [BindingProperty(8)]
        public InputLogicSignalStruct Inp
        {
            get { return this._inp; }
            set { this._inp = value; }
        }

        /// <summary>
        /// структура выходных логических сигналов
        /// </summary>
        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(9)]
        public OutputLogicSignalStruct Els
        {
            get { return this._els; }
            set { this._els = value; }
        }

        /// <summary>
        /// все защиты
        /// </summary>
        [XmlElement(ElementName = "Защиты")]
        [BindingProperty(10)]
        public AllDefensesSetpointsStruct Currentprotall
        {
            get { return this._currentprotall; }
            set { this._currentprotall = value; }
        }

        /// <summary>
        /// параметры автоматики(Реле и индикаторы)
        /// </summary>
        [XmlElement(ElementName = "Реле_и_индикаторы")]
        [BindingProperty(11)]
        public AutomaticsParametersStruct Paramautomat
        {
            get { return this._paramautomat; }
            set { this._paramautomat = value; }
        }

        /// <summary>
        /// конфигурациия омп
        /// </summary>
        [XmlElement(ElementName = "ОМП")]
        [BindingProperty(12)]
        public ConfigurationOpmStruct Ompdata
        {
            get { return this._ompdata; }
            set { this._ompdata = value; }
        }

        /// <summary>
        /// конфигурация синхронизма
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_синхронизма")]
        [BindingProperty(13)]
        public SinhronizmStructV201 Sinhronizm
        {
            get { return this._sinhronizm; }
            set { this._sinhronizm = value; }
        }

        public int SystemCfgSize
        {
            get
            {
                if (this._cnfsys == null)
                {
                    this._cnfsys = new ConfigSystemStruct();
                }
                return this._cnfsys.GetSize();
            }
        }

        public int UnderSysSize
        {
            get { return this._ompdata.GetSize() + this._sinhronizm.GetSize(); }
        }

        #endregion [Properties]
    }
}
