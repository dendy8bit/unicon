﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR761.Version2.Configuration.Structures.Oscope;

namespace BEMN.MR761.Version201.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStructV201 : StructBase 
    {
        private const int REZ_COUNT = 4;
        
        [Layout(0)] private OscopeConfigStruct _oscopeConfig;
        [Layout(1)] private OscopeAllChannelsStructV201 _oscopeAllChannels;
        [Layout(2)] private ChannelStruct _inpOsc;
        [Layout(3, Count = REZ_COUNT)] private ushort[] _rez;

        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStruct OscopeConfig
        {
            get { return _oscopeConfig; }
            set { _oscopeConfig = value; }
        }
        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStructV201 OscopeAllChannels
        {
            get { return _oscopeAllChannels; }
            set { _oscopeAllChannels = value; }
        }
        /// <summary>
        /// Вход пуска осциллографа
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Вход_пуска_осциллографа")]
        public ChannelStruct InputOsc
        {
            get { return _inpOsc; }
            set { _inpOsc = value; }
        }
    }
}
