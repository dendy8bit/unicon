﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MR761.Version2.Configuration.Structures.Oscope;

namespace BEMN.MR761.Version201.Configuration.Structures.Oscope
{
    public class OscopeAllChannelsStructV201 : StructBase, IDgvRowsContainer<ChannelStruct>
    {
        public const int KANAL_COUNT = 24;

        [Layout(0, Count = KANAL_COUNT)]
        private ChannelStruct[] _kanal; //конфигурация канала осциллографирования
        
        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelStruct[] Rows
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }
    }
}
