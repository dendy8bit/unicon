﻿using System.Collections.Generic;
using BEMN.MR761.Version201.Configuration.Structures.Oscope;

namespace BEMN.MR761.Version201.Configuration
{
    public static class StringsConfigV201
    {
        /// <summary>
        /// Названия каналов
        /// </summary>
        public static List<string> ChannelNamesV201
        {
            get
            {
                var channelNames = new List<string>();

                for (int i = 0; i < OscopeAllChannelsStructV201.KANAL_COUNT; i++)
                {
                    channelNames.Add((i + 1).ToString());
                }
                return channelNames;
            }
        }
    }
}
