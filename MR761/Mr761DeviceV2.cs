﻿using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.MBServer;
using BEMN.MR761.Version2.AlarmJournal;
using BEMN.MR761.Version2.AlarmJournal.Structures;
using BEMN.MR761.Version2.Configuration.Structures;
using BEMN.MR761.Version2.Configuration.Structures.InputSignals;
using BEMN.MR761.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR761.Version2.Measuring.Structures;
using BEMN.MR761.Version2.Osc.Loaders;
using BEMN.MR761.Version2.Osc.Structures;
using BEMN.MR761.Version2.SystemJournal.Structures;
using BEMN.MR761.Version201.Configuration.Structures;
using BEMN.MR761.Version201.Measuring;
using BEMN.MR761.Version204.Configuration.Structures;
using BEMN.MR761.Version300.AlarmJournal.Structures;
using BEMN.MR761.Version300.Configuration;
using BEMN.MR761.Version300.Configuration.Structures;
using BEMN.MR761.Version300.Configuration.Structures.MeasuringTransformer;
using BEMN.MR761.Version300.Configuration.Structures.Oscope;
using BEMN.MR761.Version300.Configuration.Structures.Switch;
using BEMN.MR761.Version300.Diagnostic;
using BEMN.MR761.Version300.Measuring.Structures;
using BEMN.MR761.Version300.Oscilloscope;
using BEMN.MR761.Version300.SystemJournal.Structures;
using OscOptionsStruct = BEMN.MR761.Version2.Configuration.Structures.OscOptionsStruct;
using OscopeStruct = BEMN.MR761.Version2.Configuration.Structures.Oscope.OscopeStruct;

namespace BEMN.MR761
{
    public class Mr761DeviceV2
    {
        #region [Constants]

        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;

        #endregion [Constants]

        #region [Private fields]
        private MemoryEntity<OscJournalStructV2> _oscJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;

        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<ConfigurationStructV201> _configurationV2;
        private MemoryEntity<ConfigurationStructV204> _configurationV204;
        private MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStructV2> _systemJournal;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<AnalogDataBaseStructV201> _analogDataBaseV201;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<OscopeStruct> _oscopeStruct;
        private CurrentOptionsLoader _currentOptionsLoader;
        private CurrentOptionsLoader _currentOptionsLoaderOsc;
        private OscOptionsLoaderV2 _oscOptionsLoaderV2;
        
        private MemoryEntity<ProgramPageStruct> _programPageStructV3;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStructV3;
        private MemoryEntity<StartStruct> _programStartStructV3;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStructV3;

        private MemoryEntity<DiscretDataBaseStructV300> _discretsV300;
        private MemoryEntity<DiscretDataBaseStruct304> _discretsV304;
        private MemoryEntity<AnalogDataBaseStructV300> _analogV300;
        private MemoryEntity<DateTimeStruct> _dateTimeV300;
        private MemoryEntity<OneWordStruct> _iMin;
        private MemoryEntity<OneWordStruct> _groupSetpoint;
        private MemoryEntity<MeasureTransStructV3> _measuringTrans;

        private MemoryEntity<OneWordStruct> _setPageJa;
        private MemoryEntity<AlarmJournalRecordStructV3> _alarmJournalV3;
        private CurrentOptionsLoaderV300 _currentOptionsLoaderV3Ja;

        private MemoryEntity<OneWordStruct> _setPageOscJournal;
        private MemoryEntity<OscJournalStructV3> _oscJournalV3;
        private MemoryEntity<OscopeAllChannelsStruct> _oscopeChannelsStruct;
        private CurrentOptionsLoaderV300 _currentOptionsLoaderOscV300;

        private MemoryEntity<SystemJournalStructV3> _systemJournalV3;
        private MemoryEntity<OneWordStruct> _indexSystemJournal;
        
        private MemoryEntity<ConfigDiagnosticStruct> _configDiagnostic;
        private MemoryEntity<ConfigDiagnosticReadOnlyStruct> _configDiagnosticReadOnly;
        #endregion [Private fields]

        #region [Ctor's]
        
        public Mr761DeviceV2(MR761 device)
        {
            this.InitStructures(device);
        }

        private void InitStructures(MR761 device)
        {
            double version = Common.VersionConverter(device.DeviceVersion);
            if (version < 3.0)
            {
                this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", device, 0x1000);
                this._configurationV2 = new MemoryEntity<ConfigurationStructV201>("КонфигурацияV2", device, 0x1000);
                this._configurationV204 = new MemoryEntity<ConfigurationStructV204>("КонфигурацияV204", device, 0x1000);
                this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", device, 0x200);
                this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", device, 0x5A0);
                this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", device, DISCRET_DATABASE_START_ADRESS);
                this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", device, ANALOG_DATABASE_START_ADRESS);
                this._analogDataBaseV201 = new MemoryEntity<AnalogDataBaseStructV201>("Аналоговая БД V2.1", device, ANALOG_DATABASE_START_ADRESS);
                this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений", device, 0x104E);
                this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", device, 0x700);
                this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", device, 0x700);
                this._currentOptionsLoader = new CurrentOptionsLoader(new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖА", device, 0x104E));
                this._currentOptionsLoaderOsc = new CurrentOptionsLoader(new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖО", device, 0x104E));

                this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", device, OSC_JOURNAL_START_ADRESS);
                this._oscJournal = new MemoryEntity<OscJournalStructV2>("Журнал осциллографа", device, OSC_JOURNAL_START_ADRESS);
                this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", device, 0x900);
                this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", device, 0x900);
                this._oscopeStruct = new MemoryEntity<OscopeStruct>("Уставки осциллографа", device, 0x102E);
                this._oscOptionsLoaderV2 = new OscOptionsLoaderV2(this._oscopeStruct);

                this._systemJournal = new MemoryEntity<SystemJournalStructV2>("Журнал системы", device, SYSTEM_JOURNAL_START_ADRESS);
                this._refreshSystemJournal = new MemoryEntity<OneWordStruct>("Установка номера записи ЖС", device, SYSTEM_JOURNAL_START_ADRESS);
            }
            else
            {
                int slotLen = device.MB.BaudeRate == 921600 ? 1024 : 64;

                if (version >= 3.04)
                {
                    this.StateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики v3.xx", device, 0x0D17);
                    ushort[] values = new ushort[4];
                    this.StateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
                    this.StateSpl.Values = values;

                    this._oscopeChannelsStruct = new MemoryEntity<OscopeAllChannelsStruct>("Конфигурация каналов осциллографа V3", device, 0x2A14, slotLen);

                    this._discretsV304 = new MemoryEntity<DiscretDataBaseStruct304>("Дискретная БД v3.xx", device, DISCRET_DATABASE_START_ADRESS, slotLen);
                    if (version >= 3.07)
                    {
                        this.ConfigurationV307 = new MemoryEntity<ConfigurationStruct307>("Конфигурация V3.07", device, 0x1000, slotLen);
                    }
                    else
                    {
                        this.ConfigurationV304 = new MemoryEntity<ConfigurationStruct304>("Конфигурация V3.xx", device, 0x1000, slotLen);
                    }
                }
                else
                {
                    this._oscopeChannelsStruct = new MemoryEntity<OscopeAllChannelsStruct>("Конфигурация каналов осциллографа V3", device, 0x290C, slotLen);

                    this.StateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логикиV3", device, 0x0D14, slotLen);
                    ushort[] values = new ushort[4];
                    this.StateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D14);
                    this.StateSpl.Values = values;

                    this._discretsV300 = new MemoryEntity<DiscretDataBaseStructV300>("Дискретная БД v3.00", device, DISCRET_DATABASE_START_ADRESS, slotLen);
                    if (version == 3.03)
                    {
                        this.ConfigurationV303 = new MemoryEntity<ConfigurationStructV303>("Конфигурация V3.03", device, 0x1000, slotLen);
                    }
                    else
                    {
                        this.ConfigurationV3 = new MemoryEntity<ConfigurationStructV300>("КонфигурацияV3", device, 0x1000, slotLen);
                    }
                }

                this._sourceProgramStructV3 = new MemoryEntity<SourceProgramStruct>("SaveProgramV3", device, 0x4300, slotLen);
                this._programStartStructV3 = new MemoryEntity<StartStruct>("SaveProgramStartV3", device, 0x0E00, slotLen);
                this._programSignalsStructV3 = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_V3", device, 0x4100, slotLen);
                this._programPageStructV3 = new MemoryEntity<ProgramPageStruct>("SaveProgrammPageV3", device, 0x4000, slotLen);
                this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программыV3", device, 0x0D0C, slotLen);
                this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программыV3", device, 0x0D0D, slotLen);
                
                
                this._analogV300 = new MemoryEntity<AnalogDataBaseStructV300>("Аналоговая БД v3.00", device, ANALOG_DATABASE_START_ADRESS, slotLen);
                this._dateTimeV300 = new MemoryEntity<DateTimeStruct>("Дата и время v3", device, 0x200, slotLen);
                this._iMin = new MemoryEntity<OneWordStruct>("Минимально отображаемый ток V3", device, 0xFC05, slotLen);
                this._groupSetpoint = new MemoryEntity<OneWordStruct>("Группа уставок V3", device, 0x0400, slotLen);
                this._measuringTrans = new MemoryEntity<MeasureTransStructV3>("Измерительный трансформатор V3", device, 0x1278, slotLen);
                this.MeasuringN5 = new MemoryEntity<MeasureTransN5Struct>("Измерительный трансформатор V3.xx", device, 0x1278, slotLen);

                this._setPageJa = new MemoryEntity<OneWordStruct>("Страница ЖА V3", device, 0x700, slotLen);
                this._alarmJournalV3 = new MemoryEntity<AlarmJournalRecordStructV3>("ЖА V3", device, 0x700, slotLen);
                this._currentOptionsLoaderV3Ja = new CurrentOptionsLoaderV300(device);

                this._setPageOscJournal = new MemoryEntity<OneWordStruct>("Установка страницы ЖО V3", device, OSC_JOURNAL_START_ADRESS, slotLen);
                this._oscJournalV3 = new MemoryEntity<OscJournalStructV3>("ЖО V3", device, OSC_JOURNAL_START_ADRESS, slotLen);
                this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы V3", device, 0x900, slotLen);
               
                
                this._currentOptionsLoaderOscV300 = new CurrentOptionsLoaderV300(device);
                this._systemJournalV3 = new MemoryEntity<SystemJournalStructV3>("Журнал системы v3", device, SYSTEM_JOURNAL_START_ADRESS, slotLen);
                this._indexSystemJournal = new MemoryEntity<OneWordStruct>("Номер журнала системы v3", device, SYSTEM_JOURNAL_START_ADRESS, slotLen);
                this._oscPage = Common.VersionConverter(device.DeviceVersion) >= 3.02
                    ? new MemoryEntity<OscPage>("Страница осциллографа V3", device, 0x900, slotLen)
                    : new MemoryEntity<OscPage>("Страница осциллографа V3", device, 0x900);
                this._configDiagnostic = new MemoryEntity<ConfigDiagnosticStruct>("Перезаписываемая конфигурация диагностики", device, 0x5D00, slotLen);
                this._configDiagnosticReadOnly = new MemoryEntity<ConfigDiagnosticReadOnlyStruct>("Конфигурация диагностики только для чтения", device, 0x5D04, slotLen);
            }
        }
        #endregion [Ctor's]

        #region [Properties]

        public OscOptionsLoaderV2 OscopeOptionsLoaderV2
        {
            get { return this._oscOptionsLoaderV2; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return this._setOscStartPage; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        public MemoryEntity<OscJournalStructV2> OscJournal
        {
            get { return this._oscJournal; }
        }

        public CurrentOptionsLoader CurrentOptionsLoader
        {
            get { return this._currentOptionsLoader; }
        }

        public CurrentOptionsLoader CurrentOptionsLoaderOsc
        {
            get { return this._currentOptionsLoaderOsc; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return this._refreshAlarmJournal; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return this._alarmJournal; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<AnalogDataBaseStructV201> AnalogDataBaseV21
        {
            get { return this._analogDataBaseV201; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return this._configuration; }
        }

        public MemoryEntity<ConfigurationStructV201> ConfigurationV2
        {
            get { return this._configurationV2; }
        }
        public MemoryEntity<ConfigurationStructV204> ConfigurationV204
        {
            get { return this._configurationV204; }
        }

        public MemoryEntity<OneWordStruct> RefreshSystemJournal
        {
            get { return this._refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStructV2> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return this._oscOptions; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }
        
        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return this._measureTrans; }
        }

        public MemoryEntity<ConfigurationStructV300> ConfigurationV3 { get; private set; }
        public MemoryEntity<ConfigurationStructV303> ConfigurationV303 { get; private set; }
        
        public MemoryEntity<ConfigurationStruct304> ConfigurationV304 { get; private set; }
        public MemoryEntity<ConfigurationStruct307> ConfigurationV307 { get; private set; }

        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStructV3; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStructV3; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStructV3; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStructV3; }
        }
        
        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }

        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }

        public MemoryEntity<SomeStruct> StateSpl { get; private set; }

        public MemoryEntity<AnalogDataBaseStructV300> AnalogV3
        {
            get { return this._analogV300; }
        }

        public MemoryEntity<DiscretDataBaseStructV300> DiscretV3
        {
            get { return this._discretsV300; }
        }

        public MemoryEntity<DiscretDataBaseStruct304> DiscretV304
        {
            get { return this._discretsV304; }
        }

        public MemoryEntity<OneWordStruct> Imin
        {
            get { return this._iMin; }
        }

        public MemoryEntity<OneWordStruct> GroupSetpoint
        {
            get { return this._groupSetpoint; }
        }

        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTimeV300; }
        }

        public MemoryEntity<MeasureTransStructV3> MeasuringV3
        {
            get { return this._measuringTrans; }
        }

        public MemoryEntity<MeasureTransN5Struct> MeasuringN5 { get; private set; }

        public MemoryEntity<OneWordStruct> SetPageJa
        {
            get { return this._setPageJa; }
        }

        public MemoryEntity<AlarmJournalRecordStructV3> AlarmJournalV3
        {
            get { return this._alarmJournalV3; }
        }

        public CurrentOptionsLoaderV300 CurrentOptionsLoaderJa
        {
            get { return this._currentOptionsLoaderV3Ja; }
        }

        public MemoryEntity<SystemJournalStructV3> SystemJournalV3
        {
            get { return this._systemJournalV3; }
        }

        public MemoryEntity<OneWordStruct> IndexSystemJourna
        {
            get { return this._indexSystemJournal; }
        }
        
        public MemoryEntity<ConfigDiagnosticStruct> ConfigDiagnostic
        {
            get { return this._configDiagnostic; }
        }

        public MemoryEntity<ConfigDiagnosticReadOnlyStruct> ConfigDiagnosticReadOnly
        {
            get { return this._configDiagnosticReadOnly; }
        }
        public MemoryEntity<OneWordStruct> SetPageOscJournal
        {
            get { return this._setPageOscJournal; }
        }

        public MemoryEntity<OscJournalStructV3> OscJournalV3
        {
            get { return this._oscJournalV3; }
        }

        public MemoryEntity<OscopeAllChannelsStruct> OscChannels
        {
            get { return this._oscopeChannelsStruct; }
        }

        public CurrentOptionsLoaderV300 CurrentOptionsLoaderOscV300
        {
            get { return this._currentOptionsLoaderOscV300; }
        }
        #endregion [Properties]

        
    }
}
