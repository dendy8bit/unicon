﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR761.Version1.Configuration.Structures
{
    /// <summary>
    /// конфигурациия улавливания синхронизма (УС)
    /// </summary>
    public class SihronizmStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _config1;
        [Layout(2)] private ushort _ustUminno; //уставка порог отсутствия напряжения
        [Layout(3)] private ushort _ustUmin; //уставка min уровень напряжения
        [Layout(4)] private ushort _ustUmax; //уставка max уровень напряжения
        [Layout(5)] private ushort _twait; //время ожидания условий синхронизма
        [Layout(6)] private ushort _ton; //время включения выключателя (для несинхронного режима)
        [Layout(7)] private ushort _tdelay; //время задержки (для синхронного режима)
        [Layout(8)] private SihronizmMaddStruct _manual; //группа для ручного включения
        [Layout(9)] private SihronizmMaddStruct _automatic; //группа для автоматического включения 
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// U1
        /// </summary>
        public string U1
        {
            get
            {
                var index = Common.GetBits(this._config, 0, 1, 2) >> 0;
                return StringsConfig.Usinhr[index];
            }
            set
            {
                var index = (ushort)StringsConfig.Usinhr.IndexOf(value);
                this._config = Common.SetBits(this._config, index, 0, 1, 2);
            }
        }

        /// <summary>
        /// U2
        /// </summary>
        public string U2
        {
            get
            {
                var index = Common.GetBits(this._config, 3, 4, 5) >> 3;
                return StringsConfig.Usinhr[index];
            }
            set
            {
                var index = (ushort)StringsConfig.Usinhr.IndexOf(value);
                this._config = Common.SetBits(this._config, index, 3, 4, 5);
            }
        }

        /// <summary>
        /// t вкл, мс
        /// </summary>
        public ushort Ton
        {
            get { return this._ton; }
            set { this._ton = value; }
        }

        /// <summary>
        /// tсинхр, мс
        /// </summary>
        public int Tsinhr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tdelay); }
            set { this._tdelay = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// tож, мс
        /// </summary>
        public int Twait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._twait); }
            set { this._twait = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Umin. отс, В
        /// </summary>
        public double UminOts
        {
            get { return ValuesConverterCommon.GetU(this._ustUminno); }
            set { this._ustUminno = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// Umin. нал, В
        /// </summary>
        public double UminNal
        {
            get { return ValuesConverterCommon.GetU(this._ustUmin); }
            set { this._ustUmin = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// Umax. нал, В
        /// </summary>
        public double UmaxNal
        {
            get { return ValuesConverterCommon.GetU(this._ustUmax); }
            set { this._ustUmax = ValuesConverterCommon.SetU(value); }
        }

        public SihronizmMaddStruct Manual
        {
            get { return this._manual; }
            set { this._manual = value; }
        }

        public SihronizmMaddStruct Automatic
        {
            get { return this._automatic; }
            set { this._automatic = value; }
        }
        #endregion [Properties]
    }
}
