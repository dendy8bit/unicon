﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR761.Version1.Osc.Structures;

namespace BEMN.MR761.Version1.Osc.Loaders
{
    /// <summary>
    /// Загружает структуру уставок осц.
    /// </summary>
    public class OscOptionsLoader
    {
        #region [Private fields]
        private readonly MemoryEntity<OscopeStruct> _oscope; 
        #endregion [Private fields]

        public OscopeStruct OscOptions
        {
            get { return this._oscope.Value; }
        }

        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail; 
        #endregion [Events]


        #region [Ctor's]
        public OscOptionsLoader(MemoryEntity<OscopeStruct> oscope)
        {
            this._oscope = oscope;
            this._oscope.AllReadOk += this.OscopeAllReadOk;
            this._oscope.AllReadFail += this.OscopeAllReadFail;
        }
        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        void OscopeAllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        void OscopeAllReadOk(object sender)
        {
            if (this.LoadOk != null)
            {
                this.LoadOk.Invoke();
            }
        }  
        #endregion [Memory Entity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запускает загрузку уставок токов(Iтт)
        /// </summary>
        public void StartRead()
        {
           this._oscope.LoadStruct();
        } 
        #endregion [Public members]
    }
}
