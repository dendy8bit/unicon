﻿namespace BEMN.MR761.Version1.Osc.ShowOsc
{
    partial class Mr761OscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this._channelLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._channelsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._channel8Button = new System.Windows.Forms.Button();
            this._channel7Button = new System.Windows.Forms.Button();
            this._channel6Button = new System.Windows.Forms.Button();
            this._channel5Button = new System.Windows.Forms.Button();
            this._channel4Button = new System.Windows.Forms.Button();
            this._channel3Button = new System.Windows.Forms.Button();
            this._channel2Button = new System.Windows.Forms.Button();
            this._channel1Button = new System.Windows.Forms.Button();
            this._channel9Button = new System.Windows.Forms.Button();
            this._channel10Button = new System.Windows.Forms.Button();
            this._channel11Button = new System.Windows.Forms.Button();
            this._channel12Button = new System.Windows.Forms.Button();
            this._channel13Button = new System.Windows.Forms.Button();
            this._channel14Button = new System.Windows.Forms.Button();
            this._channel15Button = new System.Windows.Forms.Button();
            this._channel16Button = new System.Windows.Forms.Button();
            this._channel17Button = new System.Windows.Forms.Button();
            this._channel18Button = new System.Windows.Forms.Button();
            this._channel19Button = new System.Windows.Forms.Button();
            this._channel20Button = new System.Windows.Forms.Button();
            this._channel21Button = new System.Windows.Forms.Button();
            this._channel22Button = new System.Windows.Forms.Button();
            this._channel23Button = new System.Windows.Forms.Button();
            this._channel24Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this._discretsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._discrestsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this._discrete5Button = new System.Windows.Forms.Button();
            this._discrete24Button = new System.Windows.Forms.Button();
            this._discrete23Button = new System.Windows.Forms.Button();
            this._discrete22Button = new System.Windows.Forms.Button();
            this._discrete21Button = new System.Windows.Forms.Button();
            this._discrete20Button = new System.Windows.Forms.Button();
            this._discrete19Button = new System.Windows.Forms.Button();
            this._discrete18Button = new System.Windows.Forms.Button();
            this._discrete17Button = new System.Windows.Forms.Button();
            this._discrete16Button = new System.Windows.Forms.Button();
            this._discrete15Button = new System.Windows.Forms.Button();
            this._discrete14Button = new System.Windows.Forms.Button();
            this._discrete13Button = new System.Windows.Forms.Button();
            this._discrete12Button = new System.Windows.Forms.Button();
            this._discrete11Button = new System.Windows.Forms.Button();
            this._discrete10Button = new System.Windows.Forms.Button();
            this._discrete9Button = new System.Windows.Forms.Button();
            this._discrete8Button = new System.Windows.Forms.Button();
            this._discrete7Button = new System.Windows.Forms.Button();
            this._discrete6Button = new System.Windows.Forms.Button();
            this._discrete2Button = new System.Windows.Forms.Button();
            this._discrete3Button = new System.Windows.Forms.Button();
            this._discrete4Button = new System.Windows.Forms.Button();
            this._discrete1Button = new System.Windows.Forms.Button();
            this._discrete25Button = new System.Windows.Forms.Button();
            this._discrete26Button = new System.Windows.Forms.Button();
            this._discrete27Button = new System.Windows.Forms.Button();
            this._discrete28Button = new System.Windows.Forms.Button();
            this._discrete29Button = new System.Windows.Forms.Button();
            this._discrete30Button = new System.Windows.Forms.Button();
            this._discrete31Button = new System.Windows.Forms.Button();
            this._discrete32Button = new System.Windows.Forms.Button();
            this._discrete33Button = new System.Windows.Forms.Button();
            this._discrete34Button = new System.Windows.Forms.Button();
            this._discrete35Button = new System.Windows.Forms.Button();
            this._discrete36Button = new System.Windows.Forms.Button();
            this._discrete37Button = new System.Windows.Forms.Button();
            this._discrete38Button = new System.Windows.Forms.Button();
            this._discrete39Button = new System.Windows.Forms.Button();
            this._discrete40Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this._voltageLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._uScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._u3Button = new System.Windows.Forms.Button();
            this._u4Button = new System.Windows.Forms.Button();
            this._u1Button = new System.Windows.Forms.Button();
            this._u2Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._uChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._voltageChartDecreaseButton = new System.Windows.Forms.Button();
            this._voltageChartIncreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this._currentsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._iScroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this._i3Button = new System.Windows.Forms.Button();
            this._i4Button = new System.Windows.Forms.Button();
            this._i1Button = new System.Windows.Forms.Button();
            this._i2Button = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this._iChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this._currentChartDecreaseButton = new System.Windows.Forms.Button();
            this._currentChartIncreaseButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this._marker1TrackBar = new System.Windows.Forms.TrackBar();
            this._marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this._measuringChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._markerScrollPanel = new System.Windows.Forms.Panel();
            this._marker2Box = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._marker2K24 = new System.Windows.Forms.Label();
            this._marker2K23 = new System.Windows.Forms.Label();
            this._marker2K22 = new System.Windows.Forms.Label();
            this._marker2K21 = new System.Windows.Forms.Label();
            this._marker2K20 = new System.Windows.Forms.Label();
            this._marker2K19 = new System.Windows.Forms.Label();
            this._marker2K18 = new System.Windows.Forms.Label();
            this._marker2K17 = new System.Windows.Forms.Label();
            this._marker2K16 = new System.Windows.Forms.Label();
            this._marker2K15 = new System.Windows.Forms.Label();
            this._marker2K14 = new System.Windows.Forms.Label();
            this._marker2K13 = new System.Windows.Forms.Label();
            this._marker2K12 = new System.Windows.Forms.Label();
            this._marker2K11 = new System.Windows.Forms.Label();
            this._marker2K10 = new System.Windows.Forms.Label();
            this._marker2K9 = new System.Windows.Forms.Label();
            this._marker2K8 = new System.Windows.Forms.Label();
            this._marker2K7 = new System.Windows.Forms.Label();
            this._marker2K6 = new System.Windows.Forms.Label();
            this._marker2K5 = new System.Windows.Forms.Label();
            this._marker2K4 = new System.Windows.Forms.Label();
            this._marker2K3 = new System.Windows.Forms.Label();
            this._marker2K2 = new System.Windows.Forms.Label();
            this._marker2K1 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._marker2D40 = new System.Windows.Forms.Label();
            this._marker2D31 = new System.Windows.Forms.Label();
            this._marker2D30 = new System.Windows.Forms.Label();
            this._marker2D29 = new System.Windows.Forms.Label();
            this._marker2D39 = new System.Windows.Forms.Label();
            this._marker2D38 = new System.Windows.Forms.Label();
            this._marker2D37 = new System.Windows.Forms.Label();
            this._marker2D36 = new System.Windows.Forms.Label();
            this._marker2D35 = new System.Windows.Forms.Label();
            this._marker2D34 = new System.Windows.Forms.Label();
            this._marker2D33 = new System.Windows.Forms.Label();
            this._marker2D32 = new System.Windows.Forms.Label();
            this._marker2D28 = new System.Windows.Forms.Label();
            this._marker2D27 = new System.Windows.Forms.Label();
            this._marker2D26 = new System.Windows.Forms.Label();
            this._marker2D25 = new System.Windows.Forms.Label();
            this._marker2D24 = new System.Windows.Forms.Label();
            this._marker2D23 = new System.Windows.Forms.Label();
            this._marker2D22 = new System.Windows.Forms.Label();
            this._marker2D21 = new System.Windows.Forms.Label();
            this._marker2D20 = new System.Windows.Forms.Label();
            this._marker2D11 = new System.Windows.Forms.Label();
            this._marker2D10 = new System.Windows.Forms.Label();
            this._marker2D9 = new System.Windows.Forms.Label();
            this._marker2D19 = new System.Windows.Forms.Label();
            this._marker2D18 = new System.Windows.Forms.Label();
            this._marker2D17 = new System.Windows.Forms.Label();
            this._marker2D16 = new System.Windows.Forms.Label();
            this._marker2D15 = new System.Windows.Forms.Label();
            this._marker2D14 = new System.Windows.Forms.Label();
            this._marker2D13 = new System.Windows.Forms.Label();
            this._marker2D12 = new System.Windows.Forms.Label();
            this._marker2D8 = new System.Windows.Forms.Label();
            this._marker2D7 = new System.Windows.Forms.Label();
            this._marker2D6 = new System.Windows.Forms.Label();
            this._marker2D5 = new System.Windows.Forms.Label();
            this._marker2D4 = new System.Windows.Forms.Label();
            this._marker2D3 = new System.Windows.Forms.Label();
            this._marker2D2 = new System.Windows.Forms.Label();
            this._marker2D1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._marker2U4 = new System.Windows.Forms.Label();
            this._marker2U3 = new System.Windows.Forms.Label();
            this._marker2U2 = new System.Windows.Forms.Label();
            this._marker2U1 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._marker2I4 = new System.Windows.Forms.Label();
            this._marker2I3 = new System.Windows.Forms.Label();
            this._marker2I2 = new System.Windows.Forms.Label();
            this._marker2I1 = new System.Windows.Forms.Label();
            this._marker1Box = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._marker1U4 = new System.Windows.Forms.Label();
            this._marker1U3 = new System.Windows.Forms.Label();
            this._marker1U2 = new System.Windows.Forms.Label();
            this._marker1U1 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._marker1K24 = new System.Windows.Forms.Label();
            this._marker1K23 = new System.Windows.Forms.Label();
            this._marker1K22 = new System.Windows.Forms.Label();
            this._marker1K21 = new System.Windows.Forms.Label();
            this._marker1K20 = new System.Windows.Forms.Label();
            this._marker1K19 = new System.Windows.Forms.Label();
            this._marker1K18 = new System.Windows.Forms.Label();
            this._marker1K17 = new System.Windows.Forms.Label();
            this._marker1K16 = new System.Windows.Forms.Label();
            this._marker1K15 = new System.Windows.Forms.Label();
            this._marker1K14 = new System.Windows.Forms.Label();
            this._marker1K13 = new System.Windows.Forms.Label();
            this._marker1K12 = new System.Windows.Forms.Label();
            this._marker1K11 = new System.Windows.Forms.Label();
            this._marker1K10 = new System.Windows.Forms.Label();
            this._marker1K9 = new System.Windows.Forms.Label();
            this._marker1K8 = new System.Windows.Forms.Label();
            this._marker1K7 = new System.Windows.Forms.Label();
            this._marker1K6 = new System.Windows.Forms.Label();
            this._marker1K5 = new System.Windows.Forms.Label();
            this._marker1K4 = new System.Windows.Forms.Label();
            this._marker1K3 = new System.Windows.Forms.Label();
            this._marker1K2 = new System.Windows.Forms.Label();
            this._marker1K1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._marker1D40 = new System.Windows.Forms.Label();
            this._marker1D31 = new System.Windows.Forms.Label();
            this._marker1D30 = new System.Windows.Forms.Label();
            this._marker1D29 = new System.Windows.Forms.Label();
            this._marker1D39 = new System.Windows.Forms.Label();
            this._marker1D38 = new System.Windows.Forms.Label();
            this._marker1D37 = new System.Windows.Forms.Label();
            this._marker1D36 = new System.Windows.Forms.Label();
            this._marker1D35 = new System.Windows.Forms.Label();
            this._marker1D34 = new System.Windows.Forms.Label();
            this._marker1D33 = new System.Windows.Forms.Label();
            this._marker1D32 = new System.Windows.Forms.Label();
            this._marker1D28 = new System.Windows.Forms.Label();
            this._marker1D27 = new System.Windows.Forms.Label();
            this._marker1D26 = new System.Windows.Forms.Label();
            this._marker1D25 = new System.Windows.Forms.Label();
            this._marker1D24 = new System.Windows.Forms.Label();
            this._marker1D23 = new System.Windows.Forms.Label();
            this._marker1D22 = new System.Windows.Forms.Label();
            this._marker1D21 = new System.Windows.Forms.Label();
            this._marker1D20 = new System.Windows.Forms.Label();
            this._marker1D11 = new System.Windows.Forms.Label();
            this._marker1D10 = new System.Windows.Forms.Label();
            this._marker1D9 = new System.Windows.Forms.Label();
            this._marker1D19 = new System.Windows.Forms.Label();
            this._marker1D18 = new System.Windows.Forms.Label();
            this._marker1D17 = new System.Windows.Forms.Label();
            this._marker1D16 = new System.Windows.Forms.Label();
            this._marker1D15 = new System.Windows.Forms.Label();
            this._marker1D14 = new System.Windows.Forms.Label();
            this._marker1D13 = new System.Windows.Forms.Label();
            this._marker1D12 = new System.Windows.Forms.Label();
            this._marker1D8 = new System.Windows.Forms.Label();
            this._marker1D7 = new System.Windows.Forms.Label();
            this._marker1D6 = new System.Windows.Forms.Label();
            this._marker1D5 = new System.Windows.Forms.Label();
            this._marker1D4 = new System.Windows.Forms.Label();
            this._marker1D3 = new System.Windows.Forms.Label();
            this._marker1D2 = new System.Windows.Forms.Label();
            this._marker1D1 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._marker1I4 = new System.Windows.Forms.Label();
            this._marker1I3 = new System.Windows.Forms.Label();
            this._marker1I2 = new System.Windows.Forms.Label();
            this._marker1I1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._deltaTimeBox = new System.Windows.Forms.GroupBox();
            this._markerTimeDelta = new System.Windows.Forms.Label();
            this._marker2TimeBox = new System.Windows.Forms.GroupBox();
            this._marker2Time = new System.Windows.Forms.Label();
            this._marker1TimeBox = new System.Windows.Forms.GroupBox();
            this._marker1Time = new System.Windows.Forms.Label();
            this._xDecreaseButton = new System.Windows.Forms.Button();
            this._currentСheckBox = new System.Windows.Forms.CheckBox();
            this._discrestsСheckBox = new System.Windows.Forms.CheckBox();
            this._channelsСheckBox = new System.Windows.Forms.CheckBox();
            this._markCheckBox = new System.Windows.Forms.CheckBox();
            this._markerCheckBox = new System.Windows.Forms.CheckBox();
            this._newMarkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._xIncreaseButton = new System.Windows.Forms.Button();
            this._oscRunСheckBox = new System.Windows.Forms.CheckBox();
            this._voltageСheckBox = new System.Windows.Forms.CheckBox();
            this.MAINTABLE.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this._channelLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this._discretsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this._voltageLayoutPanel.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this._currentsLayoutPanel.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._markerScrollPanel.SuspendLayout();
            this._marker2Box.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this._marker1Box.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._deltaTimeBox.SuspendLayout();
            this._marker2TimeBox.SuspendLayout();
            this._marker1TimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1384, 753);
            this.MAINTABLE.TabIndex = 1;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 733);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1084, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1384, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(22, 20);
            this.toolStripMenuItem1.Text = "|";
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1078, 702);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 90);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 612);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.splitter5);
            this.panel2.Controls.Add(this._channelLayoutPanel);
            this.panel2.Controls.Add(this.splitter2);
            this.panel2.Controls.Add(this._discretsLayoutPanel);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this._voltageLayoutPanel);
            this.panel2.Controls.Add(this.splitter3);
            this.panel2.Controls.Add(this._currentsLayoutPanel);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1057, 2618);
            this.panel2.TabIndex = 0;
            // 
            // splitter5
            // 
            this.splitter5.BackColor = System.Drawing.Color.Black;
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 2570);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(1057, 3);
            this.splitter5.TabIndex = 36;
            this.splitter5.TabStop = false;
            this.splitter5.Visible = false;
            // 
            // _channelLayoutPanel
            // 
            this._channelLayoutPanel.ColumnCount = 3;
            this._channelLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._channelLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._channelLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._channelLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._channelLayoutPanel.Controls.Add(this._channelsChart, 1, 1);
            this._channelLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this._channelLayoutPanel.Controls.Add(this.label3, 1, 0);
            this._channelLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._channelLayoutPanel.Location = new System.Drawing.Point(0, 2003);
            this._channelLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._channelLayoutPanel.Name = "_channelLayoutPanel";
            this._channelLayoutPanel.RowCount = 2;
            this._channelLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._channelLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._channelLayoutPanel.Size = new System.Drawing.Size(1057, 567);
            this._channelLayoutPanel.TabIndex = 35;
            // 
            // _channelsChart
            // 
            this._channelsChart.BkGradient = false;
            this._channelsChart.BkGradientAngle = 90;
            this._channelsChart.BkGradientColor = System.Drawing.Color.White;
            this._channelsChart.BkGradientRate = 0.5F;
            this._channelsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._channelsChart.BkRestrictedToChartPanel = false;
            this._channelsChart.BkShinePosition = 1F;
            this._channelsChart.BkTransparency = 0F;
            this._channelsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._channelsChart.BorderExteriorLength = 0;
            this._channelsChart.BorderGradientAngle = 225;
            this._channelsChart.BorderGradientLightPos1 = 1F;
            this._channelsChart.BorderGradientLightPos2 = -1F;
            this._channelsChart.BorderGradientRate = 0.5F;
            this._channelsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._channelsChart.BorderLightIntermediateBrightness = 0F;
            this._channelsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._channelsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._channelsChart.ChartPanelBkTransparency = 0F;
            this._channelsChart.ControlShadow = false;
            this._channelsChart.CoordinateAxesVisible = true;
            this._channelsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._channelsChart.CoordinateXOrigin = 10D;
            this._channelsChart.CoordinateYMax = 90D;
            this._channelsChart.CoordinateYMin = 0D;
            this._channelsChart.CoordinateYOrigin = 0D;
            this._channelsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._channelsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._channelsChart.FooterColor = System.Drawing.Color.Black;
            this._channelsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._channelsChart.FooterVisible = false;
            this._channelsChart.GridColor = System.Drawing.Color.MistyRose;
            this._channelsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._channelsChart.GridVisible = true;
            this._channelsChart.GridXSubTicker = 0;
            this._channelsChart.GridXTicker = 10;
            this._channelsChart.GridYSubTicker = 0;
            this._channelsChart.GridYTicker = 2;
            this._channelsChart.HeaderColor = System.Drawing.Color.Black;
            this._channelsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._channelsChart.HeaderVisible = false;
            this._channelsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._channelsChart.InnerBorderLength = 0;
            this._channelsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._channelsChart.LegendBkColor = System.Drawing.Color.White;
            this._channelsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._channelsChart.LegendVisible = false;
            this._channelsChart.Location = new System.Drawing.Point(45, 26);
            this._channelsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._channelsChart.MiddleBorderLength = 0;
            this._channelsChart.Name = "_channelsChart";
            this._channelsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._channelsChart.OuterBorderLength = 0;
            this._channelsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._channelsChart.Precision = 0;
            this._channelsChart.RoundRadius = 10;
            this._channelsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._channelsChart.ShadowDepth = 8;
            this._channelsChart.ShadowRate = 0.5F;
            this._channelsChart.Size = new System.Drawing.Size(989, 538);
            this._channelsChart.TabIndex = 11;
            this._channelsChart.Text = "daS_Net_XYChart5";
            this._channelsChart.XMax = 100D;
            this._channelsChart.XMin = 0D;
            this._channelsChart.XScaleColor = System.Drawing.Color.Black;
            this._channelsChart.XScaleVisible = true;
            this._channelsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this._channel8Button, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this._channel7Button, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this._channel6Button, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this._channel5Button, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this._channel4Button, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._channel3Button, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._channel2Button, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._channel1Button, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._channel9Button, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this._channel10Button, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this._channel11Button, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this._channel12Button, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this._channel13Button, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this._channel14Button, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this._channel15Button, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this._channel16Button, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this._channel17Button, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this._channel18Button, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this._channel19Button, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this._channel20Button, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this._channel21Button, 0, 20);
            this.tableLayoutPanel1.Controls.Add(this._channel22Button, 0, 21);
            this.tableLayoutPanel1.Controls.Add(this._channel23Button, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this._channel24Button, 0, 23);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 25;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(36, 529);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // _channel8Button
            // 
            this._channel8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel8Button.AutoSize = true;
            this._channel8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel8Button.ForeColor = System.Drawing.Color.Black;
            this._channel8Button.Location = new System.Drawing.Point(0, 156);
            this._channel8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel8Button.Name = "_channel8Button";
            this._channel8Button.Size = new System.Drawing.Size(36, 22);
            this._channel8Button.TabIndex = 1;
            this._channel8Button.Text = "К8";
            this._channel8Button.UseVisualStyleBackColor = false;
            this._channel8Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel7Button
            // 
            this._channel7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel7Button.AutoSize = true;
            this._channel7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel7Button.ForeColor = System.Drawing.Color.Black;
            this._channel7Button.Location = new System.Drawing.Point(0, 134);
            this._channel7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel7Button.Name = "_channel7Button";
            this._channel7Button.Size = new System.Drawing.Size(36, 22);
            this._channel7Button.TabIndex = 2;
            this._channel7Button.Text = "К7";
            this._channel7Button.UseVisualStyleBackColor = false;
            this._channel7Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel6Button
            // 
            this._channel6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel6Button.AutoSize = true;
            this._channel6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel6Button.ForeColor = System.Drawing.Color.Black;
            this._channel6Button.Location = new System.Drawing.Point(0, 112);
            this._channel6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel6Button.Name = "_channel6Button";
            this._channel6Button.Size = new System.Drawing.Size(36, 22);
            this._channel6Button.TabIndex = 6;
            this._channel6Button.Text = "К6";
            this._channel6Button.UseVisualStyleBackColor = false;
            this._channel6Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel5Button
            // 
            this._channel5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel5Button.AutoSize = true;
            this._channel5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel5Button.ForeColor = System.Drawing.Color.Black;
            this._channel5Button.Location = new System.Drawing.Point(0, 90);
            this._channel5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel5Button.Name = "_channel5Button";
            this._channel5Button.Size = new System.Drawing.Size(36, 22);
            this._channel5Button.TabIndex = 10;
            this._channel5Button.Text = "К5";
            this._channel5Button.UseVisualStyleBackColor = false;
            this._channel5Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel4Button
            // 
            this._channel4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel4Button.AutoSize = true;
            this._channel4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel4Button.ForeColor = System.Drawing.Color.Black;
            this._channel4Button.Location = new System.Drawing.Point(0, 68);
            this._channel4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel4Button.Name = "_channel4Button";
            this._channel4Button.Size = new System.Drawing.Size(36, 22);
            this._channel4Button.TabIndex = 11;
            this._channel4Button.Text = "К4";
            this._channel4Button.UseVisualStyleBackColor = false;
            this._channel4Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel3Button
            // 
            this._channel3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel3Button.AutoSize = true;
            this._channel3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel3Button.ForeColor = System.Drawing.Color.Black;
            this._channel3Button.Location = new System.Drawing.Point(0, 46);
            this._channel3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel3Button.Name = "_channel3Button";
            this._channel3Button.Size = new System.Drawing.Size(36, 22);
            this._channel3Button.TabIndex = 9;
            this._channel3Button.Text = "К3";
            this._channel3Button.UseVisualStyleBackColor = false;
            this._channel3Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel2Button
            // 
            this._channel2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel2Button.AutoSize = true;
            this._channel2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel2Button.ForeColor = System.Drawing.Color.Black;
            this._channel2Button.Location = new System.Drawing.Point(0, 24);
            this._channel2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel2Button.Name = "_channel2Button";
            this._channel2Button.Size = new System.Drawing.Size(36, 22);
            this._channel2Button.TabIndex = 7;
            this._channel2Button.Text = "К2";
            this._channel2Button.UseVisualStyleBackColor = false;
            this._channel2Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel1Button
            // 
            this._channel1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel1Button.AutoSize = true;
            this._channel1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel1Button.ForeColor = System.Drawing.Color.Black;
            this._channel1Button.Location = new System.Drawing.Point(0, 1);
            this._channel1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel1Button.Name = "_channel1Button";
            this._channel1Button.Size = new System.Drawing.Size(36, 23);
            this._channel1Button.TabIndex = 8;
            this._channel1Button.Text = "К1";
            this._channel1Button.UseVisualStyleBackColor = false;
            this._channel1Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel9Button
            // 
            this._channel9Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel9Button.AutoSize = true;
            this._channel9Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel9Button.ForeColor = System.Drawing.Color.Black;
            this._channel9Button.Location = new System.Drawing.Point(0, 178);
            this._channel9Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel9Button.Name = "_channel9Button";
            this._channel9Button.Size = new System.Drawing.Size(36, 22);
            this._channel9Button.TabIndex = 12;
            this._channel9Button.Text = "К9";
            this._channel9Button.UseVisualStyleBackColor = false;
            this._channel9Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel10Button
            // 
            this._channel10Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel10Button.AutoSize = true;
            this._channel10Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel10Button.ForeColor = System.Drawing.Color.Black;
            this._channel10Button.Location = new System.Drawing.Point(0, 200);
            this._channel10Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel10Button.Name = "_channel10Button";
            this._channel10Button.Size = new System.Drawing.Size(36, 22);
            this._channel10Button.TabIndex = 13;
            this._channel10Button.Text = "К10";
            this._channel10Button.UseVisualStyleBackColor = false;
            this._channel10Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel11Button
            // 
            this._channel11Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel11Button.AutoSize = true;
            this._channel11Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel11Button.ForeColor = System.Drawing.Color.Black;
            this._channel11Button.Location = new System.Drawing.Point(0, 222);
            this._channel11Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel11Button.Name = "_channel11Button";
            this._channel11Button.Size = new System.Drawing.Size(36, 22);
            this._channel11Button.TabIndex = 14;
            this._channel11Button.Text = "К11";
            this._channel11Button.UseVisualStyleBackColor = false;
            this._channel11Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel12Button
            // 
            this._channel12Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel12Button.AutoSize = true;
            this._channel12Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel12Button.ForeColor = System.Drawing.Color.Black;
            this._channel12Button.Location = new System.Drawing.Point(0, 244);
            this._channel12Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel12Button.Name = "_channel12Button";
            this._channel12Button.Size = new System.Drawing.Size(36, 22);
            this._channel12Button.TabIndex = 15;
            this._channel12Button.Text = "К12";
            this._channel12Button.UseVisualStyleBackColor = false;
            this._channel12Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel13Button
            // 
            this._channel13Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel13Button.AutoSize = true;
            this._channel13Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel13Button.ForeColor = System.Drawing.Color.Black;
            this._channel13Button.Location = new System.Drawing.Point(0, 266);
            this._channel13Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel13Button.Name = "_channel13Button";
            this._channel13Button.Size = new System.Drawing.Size(36, 22);
            this._channel13Button.TabIndex = 16;
            this._channel13Button.Text = "К13";
            this._channel13Button.UseVisualStyleBackColor = false;
            this._channel13Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel14Button
            // 
            this._channel14Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel14Button.AutoSize = true;
            this._channel14Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel14Button.ForeColor = System.Drawing.Color.Black;
            this._channel14Button.Location = new System.Drawing.Point(0, 288);
            this._channel14Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel14Button.Name = "_channel14Button";
            this._channel14Button.Size = new System.Drawing.Size(36, 22);
            this._channel14Button.TabIndex = 17;
            this._channel14Button.Text = "К14";
            this._channel14Button.UseVisualStyleBackColor = false;
            this._channel14Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel15Button
            // 
            this._channel15Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel15Button.AutoSize = true;
            this._channel15Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel15Button.ForeColor = System.Drawing.Color.Black;
            this._channel15Button.Location = new System.Drawing.Point(0, 310);
            this._channel15Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel15Button.Name = "_channel15Button";
            this._channel15Button.Size = new System.Drawing.Size(36, 22);
            this._channel15Button.TabIndex = 18;
            this._channel15Button.Text = "К15";
            this._channel15Button.UseVisualStyleBackColor = false;
            this._channel15Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel16Button
            // 
            this._channel16Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel16Button.AutoSize = true;
            this._channel16Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel16Button.ForeColor = System.Drawing.Color.Black;
            this._channel16Button.Location = new System.Drawing.Point(0, 332);
            this._channel16Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel16Button.Name = "_channel16Button";
            this._channel16Button.Size = new System.Drawing.Size(36, 22);
            this._channel16Button.TabIndex = 19;
            this._channel16Button.Text = "К16";
            this._channel16Button.UseVisualStyleBackColor = false;
            this._channel16Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel17Button
            // 
            this._channel17Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel17Button.AutoSize = true;
            this._channel17Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel17Button.ForeColor = System.Drawing.Color.Black;
            this._channel17Button.Location = new System.Drawing.Point(0, 354);
            this._channel17Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel17Button.Name = "_channel17Button";
            this._channel17Button.Size = new System.Drawing.Size(36, 22);
            this._channel17Button.TabIndex = 20;
            this._channel17Button.Text = "К17";
            this._channel17Button.UseVisualStyleBackColor = false;
            this._channel17Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel18Button
            // 
            this._channel18Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel18Button.AutoSize = true;
            this._channel18Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel18Button.ForeColor = System.Drawing.Color.Black;
            this._channel18Button.Location = new System.Drawing.Point(0, 376);
            this._channel18Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel18Button.Name = "_channel18Button";
            this._channel18Button.Size = new System.Drawing.Size(36, 22);
            this._channel18Button.TabIndex = 21;
            this._channel18Button.Text = "К18";
            this._channel18Button.UseVisualStyleBackColor = false;
            this._channel18Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel19Button
            // 
            this._channel19Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel19Button.AutoSize = true;
            this._channel19Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel19Button.ForeColor = System.Drawing.Color.Black;
            this._channel19Button.Location = new System.Drawing.Point(0, 398);
            this._channel19Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel19Button.Name = "_channel19Button";
            this._channel19Button.Size = new System.Drawing.Size(36, 22);
            this._channel19Button.TabIndex = 22;
            this._channel19Button.Text = "К19";
            this._channel19Button.UseVisualStyleBackColor = false;
            this._channel19Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel20Button
            // 
            this._channel20Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel20Button.AutoSize = true;
            this._channel20Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel20Button.ForeColor = System.Drawing.Color.Black;
            this._channel20Button.Location = new System.Drawing.Point(0, 420);
            this._channel20Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel20Button.Name = "_channel20Button";
            this._channel20Button.Size = new System.Drawing.Size(36, 22);
            this._channel20Button.TabIndex = 23;
            this._channel20Button.Text = "К20";
            this._channel20Button.UseVisualStyleBackColor = false;
            this._channel20Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel21Button
            // 
            this._channel21Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel21Button.AutoSize = true;
            this._channel21Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel21Button.ForeColor = System.Drawing.Color.Black;
            this._channel21Button.Location = new System.Drawing.Point(0, 442);
            this._channel21Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel21Button.Name = "_channel21Button";
            this._channel21Button.Size = new System.Drawing.Size(36, 22);
            this._channel21Button.TabIndex = 24;
            this._channel21Button.Text = "К21";
            this._channel21Button.UseVisualStyleBackColor = false;
            this._channel21Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel22Button
            // 
            this._channel22Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel22Button.AutoSize = true;
            this._channel22Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel22Button.ForeColor = System.Drawing.Color.Black;
            this._channel22Button.Location = new System.Drawing.Point(0, 464);
            this._channel22Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel22Button.Name = "_channel22Button";
            this._channel22Button.Size = new System.Drawing.Size(36, 22);
            this._channel22Button.TabIndex = 25;
            this._channel22Button.Text = "К22";
            this._channel22Button.UseVisualStyleBackColor = false;
            this._channel22Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel23Button
            // 
            this._channel23Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel23Button.AutoSize = true;
            this._channel23Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel23Button.ForeColor = System.Drawing.Color.Black;
            this._channel23Button.Location = new System.Drawing.Point(0, 486);
            this._channel23Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel23Button.Name = "_channel23Button";
            this._channel23Button.Size = new System.Drawing.Size(36, 22);
            this._channel23Button.TabIndex = 26;
            this._channel23Button.Text = "К23";
            this._channel23Button.UseVisualStyleBackColor = false;
            this._channel23Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // _channel24Button
            // 
            this._channel24Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._channel24Button.AutoSize = true;
            this._channel24Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._channel24Button.ForeColor = System.Drawing.Color.Black;
            this._channel24Button.Location = new System.Drawing.Point(0, 508);
            this._channel24Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._channel24Button.Name = "_channel24Button";
            this._channel24Button.Size = new System.Drawing.Size(36, 22);
            this._channel24Button.TabIndex = 27;
            this._channel24Button.Text = "К24";
            this._channel24Button.UseVisualStyleBackColor = false;
            this._channel24Button.Click += new System.EventHandler(this.ChannelsButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Каналы (K1 - K24)";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Black;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Enabled = false;
            this.splitter2.Location = new System.Drawing.Point(0, 2000);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1057, 3);
            this.splitter2.TabIndex = 30;
            this.splitter2.TabStop = false;
            // 
            // _discretsLayoutPanel
            // 
            this._discretsLayoutPanel.ColumnCount = 3;
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._discretsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._discretsLayoutPanel.Controls.Add(this._discrestsChart, 1, 1);
            this._discretsLayoutPanel.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this._discretsLayoutPanel.Controls.Add(this.label2, 1, 0);
            this._discretsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._discretsLayoutPanel.Location = new System.Drawing.Point(0, 1082);
            this._discretsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._discretsLayoutPanel.Name = "_discretsLayoutPanel";
            this._discretsLayoutPanel.RowCount = 2;
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._discretsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._discretsLayoutPanel.Size = new System.Drawing.Size(1057, 918);
            this._discretsLayoutPanel.TabIndex = 29;
            // 
            // _discrestsChart
            // 
            this._discrestsChart.BkGradient = false;
            this._discrestsChart.BkGradientAngle = 90;
            this._discrestsChart.BkGradientColor = System.Drawing.Color.White;
            this._discrestsChart.BkGradientRate = 0.5F;
            this._discrestsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discrestsChart.BkRestrictedToChartPanel = false;
            this._discrestsChart.BkShinePosition = 1F;
            this._discrestsChart.BkTransparency = 0F;
            this._discrestsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discrestsChart.BorderExteriorLength = 0;
            this._discrestsChart.BorderGradientAngle = 225;
            this._discrestsChart.BorderGradientLightPos1 = 1F;
            this._discrestsChart.BorderGradientLightPos2 = -1F;
            this._discrestsChart.BorderGradientRate = 0.5F;
            this._discrestsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discrestsChart.BorderLightIntermediateBrightness = 0F;
            this._discrestsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discrestsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discrestsChart.ChartPanelBkTransparency = 0F;
            this._discrestsChart.ControlShadow = false;
            this._discrestsChart.CoordinateAxesVisible = true;
            this._discrestsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._discrestsChart.CoordinateXOrigin = 0D;
            this._discrestsChart.CoordinateYMax = 100D;
            this._discrestsChart.CoordinateYMin = -100D;
            this._discrestsChart.CoordinateYOrigin = 0D;
            this._discrestsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrestsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discrestsChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._discrestsChart.FooterColor = System.Drawing.Color.Black;
            this._discrestsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discrestsChart.FooterVisible = false;
            this._discrestsChart.GridColor = System.Drawing.Color.MistyRose;
            this._discrestsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discrestsChart.GridVisible = true;
            this._discrestsChart.GridXSubTicker = 0;
            this._discrestsChart.GridXTicker = 10;
            this._discrestsChart.GridYSubTicker = 0;
            this._discrestsChart.GridYTicker = 2;
            this._discrestsChart.HeaderColor = System.Drawing.Color.Black;
            this._discrestsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discrestsChart.HeaderVisible = false;
            this._discrestsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.InnerBorderLength = 0;
            this._discrestsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.LegendBkColor = System.Drawing.Color.White;
            this._discrestsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discrestsChart.LegendVisible = false;
            this._discrestsChart.Location = new System.Drawing.Point(43, 26);
            this._discrestsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discrestsChart.MiddleBorderLength = 0;
            this._discrestsChart.Name = "_discrestsChart";
            this._discrestsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.OuterBorderLength = 0;
            this._discrestsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.Precision = 0;
            this._discrestsChart.RoundRadius = 10;
            this._discrestsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discrestsChart.ShadowDepth = 8;
            this._discrestsChart.ShadowRate = 0.5F;
            this._discrestsChart.Size = new System.Drawing.Size(991, 889);
            this._discrestsChart.TabIndex = 29;
            this._discrestsChart.Text = "daS_Net_XYChart2";
            this._discrestsChart.XMax = 100D;
            this._discrestsChart.XMin = 0D;
            this._discrestsChart.XScaleColor = System.Drawing.Color.White;
            this._discrestsChart.XScaleVisible = false;
            this._discrestsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this._discrete5Button, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this._discrete24Button, 0, 23);
            this.tableLayoutPanel5.Controls.Add(this._discrete23Button, 0, 22);
            this.tableLayoutPanel5.Controls.Add(this._discrete22Button, 0, 21);
            this.tableLayoutPanel5.Controls.Add(this._discrete21Button, 0, 20);
            this.tableLayoutPanel5.Controls.Add(this._discrete20Button, 0, 19);
            this.tableLayoutPanel5.Controls.Add(this._discrete19Button, 0, 18);
            this.tableLayoutPanel5.Controls.Add(this._discrete18Button, 0, 17);
            this.tableLayoutPanel5.Controls.Add(this._discrete17Button, 0, 16);
            this.tableLayoutPanel5.Controls.Add(this._discrete16Button, 0, 15);
            this.tableLayoutPanel5.Controls.Add(this._discrete15Button, 0, 14);
            this.tableLayoutPanel5.Controls.Add(this._discrete14Button, 0, 13);
            this.tableLayoutPanel5.Controls.Add(this._discrete13Button, 0, 12);
            this.tableLayoutPanel5.Controls.Add(this._discrete12Button, 0, 11);
            this.tableLayoutPanel5.Controls.Add(this._discrete11Button, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this._discrete10Button, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this._discrete9Button, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this._discrete8Button, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this._discrete7Button, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this._discrete6Button, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this._discrete2Button, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this._discrete3Button, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this._discrete4Button, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this._discrete1Button, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this._discrete25Button, 0, 24);
            this.tableLayoutPanel5.Controls.Add(this._discrete26Button, 0, 25);
            this.tableLayoutPanel5.Controls.Add(this._discrete27Button, 0, 26);
            this.tableLayoutPanel5.Controls.Add(this._discrete28Button, 0, 27);
            this.tableLayoutPanel5.Controls.Add(this._discrete29Button, 0, 28);
            this.tableLayoutPanel5.Controls.Add(this._discrete30Button, 0, 29);
            this.tableLayoutPanel5.Controls.Add(this._discrete31Button, 0, 30);
            this.tableLayoutPanel5.Controls.Add(this._discrete32Button, 0, 31);
            this.tableLayoutPanel5.Controls.Add(this._discrete33Button, 0, 32);
            this.tableLayoutPanel5.Controls.Add(this._discrete34Button, 0, 33);
            this.tableLayoutPanel5.Controls.Add(this._discrete35Button, 0, 34);
            this.tableLayoutPanel5.Controls.Add(this._discrete36Button, 0, 35);
            this.tableLayoutPanel5.Controls.Add(this._discrete37Button, 0, 36);
            this.tableLayoutPanel5.Controls.Add(this._discrete38Button, 0, 37);
            this.tableLayoutPanel5.Controls.Add(this._discrete39Button, 0, 38);
            this.tableLayoutPanel5.Controls.Add(this._discrete40Button, 0, 39);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 41;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(34, 882);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // _discrete5Button
            // 
            this._discrete5Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete5Button.AutoSize = true;
            this._discrete5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete5Button.ForeColor = System.Drawing.Color.Black;
            this._discrete5Button.Location = new System.Drawing.Point(0, 88);
            this._discrete5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete5Button.Name = "_discrete5Button";
            this._discrete5Button.Size = new System.Drawing.Size(41, 22);
            this._discrete5Button.TabIndex = 16;
            this._discrete5Button.Text = "Д5";
            this._discrete5Button.UseVisualStyleBackColor = false;
            this._discrete5Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete24Button
            // 
            this._discrete24Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete24Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete24Button.ForeColor = System.Drawing.Color.Black;
            this._discrete24Button.Location = new System.Drawing.Point(0, 506);
            this._discrete24Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete24Button.Name = "_discrete24Button";
            this._discrete24Button.Size = new System.Drawing.Size(41, 22);
            this._discrete24Button.TabIndex = 35;
            this._discrete24Button.Text = "Д24";
            this._discrete24Button.UseVisualStyleBackColor = false;
            this._discrete24Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete23Button
            // 
            this._discrete23Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete23Button.AutoSize = true;
            this._discrete23Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete23Button.ForeColor = System.Drawing.Color.Black;
            this._discrete23Button.Location = new System.Drawing.Point(0, 484);
            this._discrete23Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete23Button.Name = "_discrete23Button";
            this._discrete23Button.Size = new System.Drawing.Size(41, 22);
            this._discrete23Button.TabIndex = 34;
            this._discrete23Button.Text = "Д23";
            this._discrete23Button.UseVisualStyleBackColor = false;
            this._discrete23Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete22Button
            // 
            this._discrete22Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete22Button.AutoSize = true;
            this._discrete22Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete22Button.ForeColor = System.Drawing.Color.Black;
            this._discrete22Button.Location = new System.Drawing.Point(0, 462);
            this._discrete22Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete22Button.Name = "_discrete22Button";
            this._discrete22Button.Size = new System.Drawing.Size(41, 22);
            this._discrete22Button.TabIndex = 33;
            this._discrete22Button.Text = "Д22";
            this._discrete22Button.UseVisualStyleBackColor = false;
            this._discrete22Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete21Button
            // 
            this._discrete21Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete21Button.AutoSize = true;
            this._discrete21Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete21Button.ForeColor = System.Drawing.Color.Black;
            this._discrete21Button.Location = new System.Drawing.Point(0, 440);
            this._discrete21Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete21Button.Name = "_discrete21Button";
            this._discrete21Button.Size = new System.Drawing.Size(41, 22);
            this._discrete21Button.TabIndex = 32;
            this._discrete21Button.Text = "Д21";
            this._discrete21Button.UseVisualStyleBackColor = false;
            this._discrete21Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete20Button
            // 
            this._discrete20Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete20Button.AutoSize = true;
            this._discrete20Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete20Button.ForeColor = System.Drawing.Color.Black;
            this._discrete20Button.Location = new System.Drawing.Point(0, 418);
            this._discrete20Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete20Button.Name = "_discrete20Button";
            this._discrete20Button.Size = new System.Drawing.Size(41, 22);
            this._discrete20Button.TabIndex = 31;
            this._discrete20Button.Text = "Д20";
            this._discrete20Button.UseVisualStyleBackColor = false;
            this._discrete20Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete19Button
            // 
            this._discrete19Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete19Button.AutoSize = true;
            this._discrete19Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete19Button.ForeColor = System.Drawing.Color.Black;
            this._discrete19Button.Location = new System.Drawing.Point(0, 396);
            this._discrete19Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete19Button.Name = "_discrete19Button";
            this._discrete19Button.Size = new System.Drawing.Size(41, 22);
            this._discrete19Button.TabIndex = 30;
            this._discrete19Button.Text = "Д19";
            this._discrete19Button.UseVisualStyleBackColor = false;
            this._discrete19Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete18Button
            // 
            this._discrete18Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete18Button.AutoSize = true;
            this._discrete18Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete18Button.ForeColor = System.Drawing.Color.Black;
            this._discrete18Button.Location = new System.Drawing.Point(0, 374);
            this._discrete18Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete18Button.Name = "_discrete18Button";
            this._discrete18Button.Size = new System.Drawing.Size(41, 22);
            this._discrete18Button.TabIndex = 29;
            this._discrete18Button.Text = "Д18";
            this._discrete18Button.UseVisualStyleBackColor = false;
            this._discrete18Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete17Button
            // 
            this._discrete17Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete17Button.AutoSize = true;
            this._discrete17Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete17Button.ForeColor = System.Drawing.Color.Black;
            this._discrete17Button.Location = new System.Drawing.Point(0, 352);
            this._discrete17Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete17Button.Name = "_discrete17Button";
            this._discrete17Button.Size = new System.Drawing.Size(41, 22);
            this._discrete17Button.TabIndex = 28;
            this._discrete17Button.Text = "Д17";
            this._discrete17Button.UseVisualStyleBackColor = false;
            this._discrete17Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete16Button
            // 
            this._discrete16Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete16Button.AutoSize = true;
            this._discrete16Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete16Button.ForeColor = System.Drawing.Color.Black;
            this._discrete16Button.Location = new System.Drawing.Point(0, 330);
            this._discrete16Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete16Button.Name = "_discrete16Button";
            this._discrete16Button.Size = new System.Drawing.Size(41, 22);
            this._discrete16Button.TabIndex = 27;
            this._discrete16Button.Text = "Д16";
            this._discrete16Button.UseVisualStyleBackColor = false;
            this._discrete16Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete15Button
            // 
            this._discrete15Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete15Button.AutoSize = true;
            this._discrete15Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete15Button.ForeColor = System.Drawing.Color.Black;
            this._discrete15Button.Location = new System.Drawing.Point(0, 308);
            this._discrete15Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete15Button.Name = "_discrete15Button";
            this._discrete15Button.Size = new System.Drawing.Size(41, 22);
            this._discrete15Button.TabIndex = 26;
            this._discrete15Button.Text = "Д15";
            this._discrete15Button.UseVisualStyleBackColor = false;
            this._discrete15Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete14Button
            // 
            this._discrete14Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete14Button.AutoSize = true;
            this._discrete14Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete14Button.ForeColor = System.Drawing.Color.Black;
            this._discrete14Button.Location = new System.Drawing.Point(0, 286);
            this._discrete14Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete14Button.Name = "_discrete14Button";
            this._discrete14Button.Size = new System.Drawing.Size(41, 22);
            this._discrete14Button.TabIndex = 25;
            this._discrete14Button.Text = "Д14";
            this._discrete14Button.UseVisualStyleBackColor = false;
            this._discrete14Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete13Button
            // 
            this._discrete13Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete13Button.AutoSize = true;
            this._discrete13Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete13Button.ForeColor = System.Drawing.Color.Black;
            this._discrete13Button.Location = new System.Drawing.Point(0, 264);
            this._discrete13Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete13Button.Name = "_discrete13Button";
            this._discrete13Button.Size = new System.Drawing.Size(41, 22);
            this._discrete13Button.TabIndex = 24;
            this._discrete13Button.Text = "Д13";
            this._discrete13Button.UseVisualStyleBackColor = false;
            this._discrete13Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete12Button
            // 
            this._discrete12Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete12Button.AutoSize = true;
            this._discrete12Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete12Button.ForeColor = System.Drawing.Color.Black;
            this._discrete12Button.Location = new System.Drawing.Point(0, 242);
            this._discrete12Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete12Button.Name = "_discrete12Button";
            this._discrete12Button.Size = new System.Drawing.Size(41, 22);
            this._discrete12Button.TabIndex = 23;
            this._discrete12Button.Text = "Д12";
            this._discrete12Button.UseVisualStyleBackColor = false;
            this._discrete12Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete11Button
            // 
            this._discrete11Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete11Button.AutoSize = true;
            this._discrete11Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete11Button.ForeColor = System.Drawing.Color.Black;
            this._discrete11Button.Location = new System.Drawing.Point(0, 220);
            this._discrete11Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete11Button.Name = "_discrete11Button";
            this._discrete11Button.Size = new System.Drawing.Size(41, 22);
            this._discrete11Button.TabIndex = 22;
            this._discrete11Button.Text = "Д11";
            this._discrete11Button.UseVisualStyleBackColor = false;
            this._discrete11Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete10Button
            // 
            this._discrete10Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete10Button.AutoSize = true;
            this._discrete10Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete10Button.ForeColor = System.Drawing.Color.Black;
            this._discrete10Button.Location = new System.Drawing.Point(0, 198);
            this._discrete10Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete10Button.Name = "_discrete10Button";
            this._discrete10Button.Size = new System.Drawing.Size(41, 22);
            this._discrete10Button.TabIndex = 21;
            this._discrete10Button.Text = "Д10";
            this._discrete10Button.UseVisualStyleBackColor = false;
            this._discrete10Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete9Button
            // 
            this._discrete9Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete9Button.AutoSize = true;
            this._discrete9Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete9Button.ForeColor = System.Drawing.Color.Black;
            this._discrete9Button.Location = new System.Drawing.Point(0, 176);
            this._discrete9Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete9Button.Name = "_discrete9Button";
            this._discrete9Button.Size = new System.Drawing.Size(41, 22);
            this._discrete9Button.TabIndex = 20;
            this._discrete9Button.Text = "Д9";
            this._discrete9Button.UseVisualStyleBackColor = false;
            this._discrete9Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete8Button
            // 
            this._discrete8Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete8Button.AutoSize = true;
            this._discrete8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete8Button.ForeColor = System.Drawing.Color.Black;
            this._discrete8Button.Location = new System.Drawing.Point(0, 154);
            this._discrete8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete8Button.Name = "_discrete8Button";
            this._discrete8Button.Size = new System.Drawing.Size(41, 22);
            this._discrete8Button.TabIndex = 19;
            this._discrete8Button.Text = "Д8";
            this._discrete8Button.UseVisualStyleBackColor = false;
            this._discrete8Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete7Button
            // 
            this._discrete7Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete7Button.AutoSize = true;
            this._discrete7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete7Button.ForeColor = System.Drawing.Color.Black;
            this._discrete7Button.Location = new System.Drawing.Point(0, 132);
            this._discrete7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete7Button.Name = "_discrete7Button";
            this._discrete7Button.Size = new System.Drawing.Size(41, 22);
            this._discrete7Button.TabIndex = 18;
            this._discrete7Button.Text = "Д7";
            this._discrete7Button.UseVisualStyleBackColor = false;
            this._discrete7Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete6Button
            // 
            this._discrete6Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete6Button.AutoSize = true;
            this._discrete6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete6Button.ForeColor = System.Drawing.Color.Black;
            this._discrete6Button.Location = new System.Drawing.Point(0, 110);
            this._discrete6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete6Button.Name = "_discrete6Button";
            this._discrete6Button.Size = new System.Drawing.Size(41, 22);
            this._discrete6Button.TabIndex = 17;
            this._discrete6Button.Text = "Д6";
            this._discrete6Button.UseVisualStyleBackColor = false;
            this._discrete6Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete2Button
            // 
            this._discrete2Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete2Button.AutoSize = true;
            this._discrete2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete2Button.ForeColor = System.Drawing.Color.Black;
            this._discrete2Button.Location = new System.Drawing.Point(0, 22);
            this._discrete2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete2Button.Name = "_discrete2Button";
            this._discrete2Button.Size = new System.Drawing.Size(41, 22);
            this._discrete2Button.TabIndex = 37;
            this._discrete2Button.Text = "Д2";
            this._discrete2Button.UseVisualStyleBackColor = false;
            this._discrete2Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete3Button
            // 
            this._discrete3Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete3Button.AutoSize = true;
            this._discrete3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete3Button.ForeColor = System.Drawing.Color.Black;
            this._discrete3Button.Location = new System.Drawing.Point(0, 44);
            this._discrete3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete3Button.Name = "_discrete3Button";
            this._discrete3Button.Size = new System.Drawing.Size(41, 22);
            this._discrete3Button.TabIndex = 38;
            this._discrete3Button.Text = "Д3";
            this._discrete3Button.UseVisualStyleBackColor = false;
            this._discrete3Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete4Button
            // 
            this._discrete4Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete4Button.AutoSize = true;
            this._discrete4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete4Button.ForeColor = System.Drawing.Color.Black;
            this._discrete4Button.Location = new System.Drawing.Point(0, 66);
            this._discrete4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete4Button.Name = "_discrete4Button";
            this._discrete4Button.Size = new System.Drawing.Size(41, 22);
            this._discrete4Button.TabIndex = 39;
            this._discrete4Button.Text = "Д4";
            this._discrete4Button.UseVisualStyleBackColor = false;
            this._discrete4Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete1Button
            // 
            this._discrete1Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete1Button.AutoSize = true;
            this._discrete1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete1Button.ForeColor = System.Drawing.Color.Black;
            this._discrete1Button.Location = new System.Drawing.Point(0, 0);
            this._discrete1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete1Button.Name = "_discrete1Button";
            this._discrete1Button.Size = new System.Drawing.Size(41, 22);
            this._discrete1Button.TabIndex = 36;
            this._discrete1Button.Text = "Д1";
            this._discrete1Button.UseVisualStyleBackColor = false;
            this._discrete1Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete25Button
            // 
            this._discrete25Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete25Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete25Button.ForeColor = System.Drawing.Color.Black;
            this._discrete25Button.Location = new System.Drawing.Point(0, 528);
            this._discrete25Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete25Button.Name = "_discrete25Button";
            this._discrete25Button.Size = new System.Drawing.Size(41, 22);
            this._discrete25Button.TabIndex = 40;
            this._discrete25Button.Text = "Д25";
            this._discrete25Button.UseVisualStyleBackColor = false;
            this._discrete25Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete26Button
            // 
            this._discrete26Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete26Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete26Button.ForeColor = System.Drawing.Color.Black;
            this._discrete26Button.Location = new System.Drawing.Point(0, 550);
            this._discrete26Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete26Button.Name = "_discrete26Button";
            this._discrete26Button.Size = new System.Drawing.Size(41, 22);
            this._discrete26Button.TabIndex = 41;
            this._discrete26Button.Text = "Д26";
            this._discrete26Button.UseVisualStyleBackColor = false;
            this._discrete26Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete27Button
            // 
            this._discrete27Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete27Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete27Button.ForeColor = System.Drawing.Color.Black;
            this._discrete27Button.Location = new System.Drawing.Point(0, 572);
            this._discrete27Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete27Button.Name = "_discrete27Button";
            this._discrete27Button.Size = new System.Drawing.Size(41, 22);
            this._discrete27Button.TabIndex = 42;
            this._discrete27Button.Text = "Д27";
            this._discrete27Button.UseVisualStyleBackColor = false;
            this._discrete27Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete28Button
            // 
            this._discrete28Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete28Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete28Button.ForeColor = System.Drawing.Color.Black;
            this._discrete28Button.Location = new System.Drawing.Point(0, 594);
            this._discrete28Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete28Button.Name = "_discrete28Button";
            this._discrete28Button.Size = new System.Drawing.Size(41, 22);
            this._discrete28Button.TabIndex = 43;
            this._discrete28Button.Text = "Д28";
            this._discrete28Button.UseVisualStyleBackColor = false;
            this._discrete28Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete29Button
            // 
            this._discrete29Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete29Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete29Button.ForeColor = System.Drawing.Color.Black;
            this._discrete29Button.Location = new System.Drawing.Point(0, 616);
            this._discrete29Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete29Button.Name = "_discrete29Button";
            this._discrete29Button.Size = new System.Drawing.Size(41, 22);
            this._discrete29Button.TabIndex = 44;
            this._discrete29Button.Text = "Д29";
            this._discrete29Button.UseVisualStyleBackColor = false;
            this._discrete29Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete30Button
            // 
            this._discrete30Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete30Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete30Button.ForeColor = System.Drawing.Color.Black;
            this._discrete30Button.Location = new System.Drawing.Point(0, 638);
            this._discrete30Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete30Button.Name = "_discrete30Button";
            this._discrete30Button.Size = new System.Drawing.Size(41, 22);
            this._discrete30Button.TabIndex = 45;
            this._discrete30Button.Text = "Д30";
            this._discrete30Button.UseVisualStyleBackColor = false;
            this._discrete30Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete31Button
            // 
            this._discrete31Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete31Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete31Button.ForeColor = System.Drawing.Color.Black;
            this._discrete31Button.Location = new System.Drawing.Point(0, 660);
            this._discrete31Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete31Button.Name = "_discrete31Button";
            this._discrete31Button.Size = new System.Drawing.Size(41, 22);
            this._discrete31Button.TabIndex = 46;
            this._discrete31Button.Text = "Д31";
            this._discrete31Button.UseVisualStyleBackColor = false;
            this._discrete31Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete32Button
            // 
            this._discrete32Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete32Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete32Button.ForeColor = System.Drawing.Color.Black;
            this._discrete32Button.Location = new System.Drawing.Point(0, 682);
            this._discrete32Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete32Button.Name = "_discrete32Button";
            this._discrete32Button.Size = new System.Drawing.Size(41, 22);
            this._discrete32Button.TabIndex = 47;
            this._discrete32Button.Text = "Д32";
            this._discrete32Button.UseVisualStyleBackColor = false;
            this._discrete32Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete33Button
            // 
            this._discrete33Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete33Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete33Button.ForeColor = System.Drawing.Color.Black;
            this._discrete33Button.Location = new System.Drawing.Point(0, 704);
            this._discrete33Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete33Button.Name = "_discrete33Button";
            this._discrete33Button.Size = new System.Drawing.Size(41, 22);
            this._discrete33Button.TabIndex = 48;
            this._discrete33Button.Text = "Д33";
            this._discrete33Button.UseVisualStyleBackColor = false;
            this._discrete33Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete34Button
            // 
            this._discrete34Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete34Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete34Button.ForeColor = System.Drawing.Color.Black;
            this._discrete34Button.Location = new System.Drawing.Point(0, 726);
            this._discrete34Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete34Button.Name = "_discrete34Button";
            this._discrete34Button.Size = new System.Drawing.Size(41, 22);
            this._discrete34Button.TabIndex = 49;
            this._discrete34Button.Text = "Д34";
            this._discrete34Button.UseVisualStyleBackColor = false;
            this._discrete34Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete35Button
            // 
            this._discrete35Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete35Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete35Button.ForeColor = System.Drawing.Color.Black;
            this._discrete35Button.Location = new System.Drawing.Point(0, 748);
            this._discrete35Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete35Button.Name = "_discrete35Button";
            this._discrete35Button.Size = new System.Drawing.Size(41, 22);
            this._discrete35Button.TabIndex = 50;
            this._discrete35Button.Text = "Д35";
            this._discrete35Button.UseVisualStyleBackColor = false;
            this._discrete35Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete36Button
            // 
            this._discrete36Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete36Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete36Button.ForeColor = System.Drawing.Color.Black;
            this._discrete36Button.Location = new System.Drawing.Point(0, 770);
            this._discrete36Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete36Button.Name = "_discrete36Button";
            this._discrete36Button.Size = new System.Drawing.Size(41, 22);
            this._discrete36Button.TabIndex = 51;
            this._discrete36Button.Text = "Д36";
            this._discrete36Button.UseVisualStyleBackColor = false;
            this._discrete36Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete37Button
            // 
            this._discrete37Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete37Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete37Button.ForeColor = System.Drawing.Color.Black;
            this._discrete37Button.Location = new System.Drawing.Point(0, 792);
            this._discrete37Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete37Button.Name = "_discrete37Button";
            this._discrete37Button.Size = new System.Drawing.Size(41, 22);
            this._discrete37Button.TabIndex = 52;
            this._discrete37Button.Text = "Д37";
            this._discrete37Button.UseVisualStyleBackColor = false;
            this._discrete37Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete38Button
            // 
            this._discrete38Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete38Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete38Button.ForeColor = System.Drawing.Color.Black;
            this._discrete38Button.Location = new System.Drawing.Point(0, 814);
            this._discrete38Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete38Button.Name = "_discrete38Button";
            this._discrete38Button.Size = new System.Drawing.Size(41, 22);
            this._discrete38Button.TabIndex = 53;
            this._discrete38Button.Text = "Д38";
            this._discrete38Button.UseVisualStyleBackColor = false;
            this._discrete38Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete39Button
            // 
            this._discrete39Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete39Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete39Button.ForeColor = System.Drawing.Color.Black;
            this._discrete39Button.Location = new System.Drawing.Point(0, 836);
            this._discrete39Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete39Button.Name = "_discrete39Button";
            this._discrete39Button.Size = new System.Drawing.Size(41, 22);
            this._discrete39Button.TabIndex = 54;
            this._discrete39Button.Text = "Д39";
            this._discrete39Button.UseVisualStyleBackColor = false;
            this._discrete39Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete40Button
            // 
            this._discrete40Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._discrete40Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete40Button.ForeColor = System.Drawing.Color.Black;
            this._discrete40Button.Location = new System.Drawing.Point(0, 858);
            this._discrete40Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete40Button.Name = "_discrete40Button";
            this._discrete40Button.Size = new System.Drawing.Size(41, 22);
            this._discrete40Button.TabIndex = 55;
            this._discrete40Button.Text = "Д40";
            this._discrete40Button.UseVisualStyleBackColor = false;
            this._discrete40Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Дискреты";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point(0, 1079);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1057, 3);
            this.splitter1.TabIndex = 28;
            this.splitter1.TabStop = false;
            // 
            // _voltageLayoutPanel
            // 
            this._voltageLayoutPanel.ColumnCount = 3;
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.Controls.Add(this._uScroll, 2, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this._voltageLayoutPanel.Controls.Add(this._uChart, 1, 1);
            this._voltageLayoutPanel.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this._voltageLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._voltageLayoutPanel.Location = new System.Drawing.Point(0, 541);
            this._voltageLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._voltageLayoutPanel.Name = "_voltageLayoutPanel";
            this._voltageLayoutPanel.RowCount = 2;
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._voltageLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._voltageLayoutPanel.Size = new System.Drawing.Size(1057, 538);
            this._voltageLayoutPanel.TabIndex = 27;
            // 
            // _uScroll
            // 
            this._uScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._uScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._uScroll.LargeChange = 1;
            this._uScroll.Location = new System.Drawing.Point(1039, 35);
            this._uScroll.Minimum = 100;
            this._uScroll.Name = "_uScroll";
            this._uScroll.Size = new System.Drawing.Size(15, 503);
            this._uScroll.TabIndex = 32;
            this._uScroll.Value = 100;
            this._uScroll.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this._voltageLayoutPanel.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 666F));
            this.tableLayoutPanel3.Controls.Add(this._u3Button, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this._u4Button, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this._u1Button, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this._u2Button, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // _u3Button
            // 
            this._u3Button.BackColor = System.Drawing.Color.Red;
            this._u3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u3Button.Location = new System.Drawing.Point(213, 3);
            this._u3Button.Name = "_u3Button";
            this._u3Button.Size = new System.Drawing.Size(49, 23);
            this._u3Button.TabIndex = 13;
            this._u3Button.Text = "Uc";
            this._u3Button.UseVisualStyleBackColor = false;
            this._u3Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u4Button
            // 
            this._u4Button.BackColor = System.Drawing.Color.Indigo;
            this._u4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u4Button.Location = new System.Drawing.Point(268, 3);
            this._u4Button.Name = "_u4Button";
            this._u4Button.Size = new System.Drawing.Size(49, 23);
            this._u4Button.TabIndex = 11;
            this._u4Button.Text = "Un";
            this._u4Button.UseVisualStyleBackColor = false;
            this._u4Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u1Button
            // 
            this._u1Button.BackColor = System.Drawing.Color.Yellow;
            this._u1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u1Button.Location = new System.Drawing.Point(103, 3);
            this._u1Button.Name = "_u1Button";
            this._u1Button.Size = new System.Drawing.Size(49, 23);
            this._u1Button.TabIndex = 10;
            this._u1Button.Text = "Ua";
            this._u1Button.UseVisualStyleBackColor = false;
            this._u1Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // _u2Button
            // 
            this._u2Button.BackColor = System.Drawing.Color.Green;
            this._u2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._u2Button.Location = new System.Drawing.Point(158, 3);
            this._u2Button.Name = "_u2Button";
            this._u2Button.Size = new System.Drawing.Size(49, 23);
            this._u2Button.TabIndex = 9;
            this._u2Button.Text = "Ub";
            this._u2Button.UseVisualStyleBackColor = false;
            this._u2Button.Click += new System.EventHandler(this.VoltageButton_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Напряжения";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _uChart
            // 
            this._uChart.BkGradient = false;
            this._uChart.BkGradientAngle = 90;
            this._uChart.BkGradientColor = System.Drawing.Color.White;
            this._uChart.BkGradientRate = 0.5F;
            this._uChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._uChart.BkRestrictedToChartPanel = false;
            this._uChart.BkShinePosition = 1F;
            this._uChart.BkTransparency = 0F;
            this._uChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._uChart.BorderExteriorLength = 0;
            this._uChart.BorderGradientAngle = 225;
            this._uChart.BorderGradientLightPos1 = 1F;
            this._uChart.BorderGradientLightPos2 = -1F;
            this._uChart.BorderGradientRate = 0.5F;
            this._uChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._uChart.BorderLightIntermediateBrightness = 0F;
            this._uChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._uChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._uChart.ChartPanelBkTransparency = 0F;
            this._uChart.ControlShadow = false;
            this._uChart.CoordinateAxesVisible = true;
            this._uChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._uChart.CoordinateXOrigin = 0D;
            this._uChart.CoordinateYMax = 100D;
            this._uChart.CoordinateYMin = -100D;
            this._uChart.CoordinateYOrigin = 0D;
            this._uChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._uChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._uChart.FooterColor = System.Drawing.Color.Black;
            this._uChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._uChart.FooterVisible = false;
            this._uChart.GridColor = System.Drawing.Color.MistyRose;
            this._uChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._uChart.GridVisible = true;
            this._uChart.GridXSubTicker = 0;
            this._uChart.GridXTicker = 10;
            this._uChart.GridYSubTicker = 0;
            this._uChart.GridYTicker = 10;
            this._uChart.HeaderColor = System.Drawing.Color.Black;
            this._uChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._uChart.HeaderVisible = false;
            this._uChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.InnerBorderLength = 0;
            this._uChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._uChart.LegendBkColor = System.Drawing.Color.White;
            this._uChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._uChart.LegendVisible = false;
            this._uChart.Location = new System.Drawing.Point(43, 38);
            this._uChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._uChart.MiddleBorderLength = 0;
            this._uChart.Name = "_uChart";
            this._uChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._uChart.OuterBorderLength = 0;
            this._uChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._uChart.Precision = 0;
            this._uChart.RoundRadius = 10;
            this._uChart.ShadowColor = System.Drawing.Color.DimGray;
            this._uChart.ShadowDepth = 8;
            this._uChart.ShadowRate = 0.5F;
            this._uChart.Size = new System.Drawing.Size(991, 497);
            this._uChart.TabIndex = 33;
            this._uChart.Text = "daS_Net_XYChart1";
            this._uChart.XMax = 100D;
            this._uChart.XMin = 0D;
            this._uChart.XScaleColor = System.Drawing.Color.Black;
            this._uChart.XScaleVisible = true;
            this._uChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this._voltageChartDecreaseButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._voltageChartIncreaseButton, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(34, 497);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // _voltageChartDecreaseButton
            // 
            this._voltageChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartDecreaseButton.Enabled = false;
            this._voltageChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartDecreaseButton.Location = new System.Drawing.Point(3, 251);
            this._voltageChartDecreaseButton.Name = "_voltageChartDecreaseButton";
            this._voltageChartDecreaseButton.Size = new System.Drawing.Size(28, 24);
            this._voltageChartDecreaseButton.TabIndex = 1;
            this._voltageChartDecreaseButton.Text = "-";
            this._voltageChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _voltageChartIncreaseButton
            // 
            this._voltageChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._voltageChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._voltageChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._voltageChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._voltageChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._voltageChartIncreaseButton.Location = new System.Drawing.Point(3, 221);
            this._voltageChartIncreaseButton.Name = "_voltageChartIncreaseButton";
            this._voltageChartIncreaseButton.Size = new System.Drawing.Size(28, 24);
            this._voltageChartIncreaseButton.TabIndex = 0;
            this._voltageChartIncreaseButton.Text = "+";
            this._voltageChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(9, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Black;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Enabled = false;
            this.splitter3.Location = new System.Drawing.Point(0, 538);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1057, 3);
            this.splitter3.TabIndex = 37;
            this.splitter3.TabStop = false;
            // 
            // _currentsLayoutPanel
            // 
            this._currentsLayoutPanel.ColumnCount = 3;
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._currentsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._currentsLayoutPanel.Controls.Add(this._iScroll, 2, 1);
            this._currentsLayoutPanel.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this._currentsLayoutPanel.Controls.Add(this._iChart, 1, 1);
            this._currentsLayoutPanel.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this._currentsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._currentsLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._currentsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this._currentsLayoutPanel.Name = "_currentsLayoutPanel";
            this._currentsLayoutPanel.RowCount = 2;
            this._currentsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this._currentsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._currentsLayoutPanel.Size = new System.Drawing.Size(1057, 538);
            this._currentsLayoutPanel.TabIndex = 38;
            // 
            // _iScroll
            // 
            this._iScroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._iScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._iScroll.LargeChange = 1;
            this._iScroll.Location = new System.Drawing.Point(1039, 35);
            this._iScroll.Minimum = 100;
            this._iScroll.Name = "_iScroll";
            this._iScroll.Size = new System.Drawing.Size(15, 503);
            this._iScroll.TabIndex = 32;
            this._iScroll.Value = 100;
            this._iScroll.Visible = false;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this._currentsLayoutPanel.SetColumnSpan(this.tableLayoutPanel7, 3);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 666F));
            this.tableLayoutPanel7.Controls.Add(this._i3Button, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this._i4Button, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this._i1Button, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this._i2Button, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel7.TabIndex = 23;
            // 
            // _i3Button
            // 
            this._i3Button.BackColor = System.Drawing.Color.Red;
            this._i3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i3Button.Location = new System.Drawing.Point(213, 3);
            this._i3Button.Name = "_i3Button";
            this._i3Button.Size = new System.Drawing.Size(49, 23);
            this._i3Button.TabIndex = 13;
            this._i3Button.Text = "Ic";
            this._i3Button.UseVisualStyleBackColor = false;
            this._i3Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // _i4Button
            // 
            this._i4Button.BackColor = System.Drawing.Color.Indigo;
            this._i4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i4Button.Location = new System.Drawing.Point(268, 3);
            this._i4Button.Name = "_i4Button";
            this._i4Button.Size = new System.Drawing.Size(49, 23);
            this._i4Button.TabIndex = 11;
            this._i4Button.Text = "In";
            this._i4Button.UseVisualStyleBackColor = false;
            this._i4Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // _i1Button
            // 
            this._i1Button.BackColor = System.Drawing.Color.Yellow;
            this._i1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i1Button.Location = new System.Drawing.Point(103, 3);
            this._i1Button.Name = "_i1Button";
            this._i1Button.Size = new System.Drawing.Size(49, 23);
            this._i1Button.TabIndex = 10;
            this._i1Button.Text = "Ia";
            this._i1Button.UseVisualStyleBackColor = false;
            this._i1Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // _i2Button
            // 
            this._i2Button.BackColor = System.Drawing.Color.Green;
            this._i2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i2Button.Location = new System.Drawing.Point(158, 3);
            this._i2Button.Name = "_i2Button";
            this._i2Button.Size = new System.Drawing.Size(49, 23);
            this._i2Button.TabIndex = 9;
            this._i2Button.Text = "Ib";
            this._i2Button.UseVisualStyleBackColor = false;
            this._i2Button.Click += new System.EventHandler(this.CurrentButton_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Токи";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _iChart
            // 
            this._iChart.BkGradient = false;
            this._iChart.BkGradientAngle = 90;
            this._iChart.BkGradientColor = System.Drawing.Color.White;
            this._iChart.BkGradientRate = 0.5F;
            this._iChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._iChart.BkRestrictedToChartPanel = false;
            this._iChart.BkShinePosition = 1F;
            this._iChart.BkTransparency = 0F;
            this._iChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._iChart.BorderExteriorLength = 0;
            this._iChart.BorderGradientAngle = 225;
            this._iChart.BorderGradientLightPos1 = 1F;
            this._iChart.BorderGradientLightPos2 = -1F;
            this._iChart.BorderGradientRate = 0.5F;
            this._iChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._iChart.BorderLightIntermediateBrightness = 0F;
            this._iChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._iChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._iChart.ChartPanelBkTransparency = 0F;
            this._iChart.ControlShadow = false;
            this._iChart.CoordinateAxesVisible = true;
            this._iChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._iChart.CoordinateXOrigin = 0D;
            this._iChart.CoordinateYMax = 100D;
            this._iChart.CoordinateYMin = -100D;
            this._iChart.CoordinateYOrigin = 0D;
            this._iChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._iChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._iChart.FooterColor = System.Drawing.Color.Black;
            this._iChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._iChart.FooterVisible = false;
            this._iChart.GridColor = System.Drawing.Color.MistyRose;
            this._iChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._iChart.GridVisible = true;
            this._iChart.GridXSubTicker = 0;
            this._iChart.GridXTicker = 10;
            this._iChart.GridYSubTicker = 0;
            this._iChart.GridYTicker = 10;
            this._iChart.HeaderColor = System.Drawing.Color.Black;
            this._iChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._iChart.HeaderVisible = false;
            this._iChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._iChart.InnerBorderLength = 0;
            this._iChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._iChart.LegendBkColor = System.Drawing.Color.White;
            this._iChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._iChart.LegendVisible = false;
            this._iChart.Location = new System.Drawing.Point(43, 38);
            this._iChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._iChart.MiddleBorderLength = 0;
            this._iChart.Name = "_iChart";
            this._iChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._iChart.OuterBorderLength = 0;
            this._iChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._iChart.Precision = 0;
            this._iChart.RoundRadius = 10;
            this._iChart.ShadowColor = System.Drawing.Color.DimGray;
            this._iChart.ShadowDepth = 8;
            this._iChart.ShadowRate = 0.5F;
            this._iChart.Size = new System.Drawing.Size(991, 497);
            this._iChart.TabIndex = 33;
            this._iChart.Text = "daS_Net_XYChart1";
            this._iChart.XMax = 100D;
            this._iChart.XMin = 0D;
            this._iChart.XScaleColor = System.Drawing.Color.Black;
            this._iChart.XScaleVisible = true;
            this._iChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this._currentChartDecreaseButton, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this._currentChartIncreaseButton, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 4;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(34, 497);
            this.tableLayoutPanel9.TabIndex = 31;
            // 
            // _currentChartDecreaseButton
            // 
            this._currentChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentChartDecreaseButton.Enabled = false;
            this._currentChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentChartDecreaseButton.Location = new System.Drawing.Point(3, 251);
            this._currentChartDecreaseButton.Name = "_currentChartDecreaseButton";
            this._currentChartDecreaseButton.Size = new System.Drawing.Size(28, 24);
            this._currentChartDecreaseButton.TabIndex = 1;
            this._currentChartDecreaseButton.Text = "-";
            this._currentChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentChartIncreaseButton
            // 
            this._currentChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentChartIncreaseButton.Location = new System.Drawing.Point(3, 221);
            this._currentChartIncreaseButton.Name = "_currentChartIncreaseButton";
            this._currentChartIncreaseButton.Size = new System.Drawing.Size(28, 24);
            this._currentChartIncreaseButton.TabIndex = 0;
            this._currentChartIncreaseButton.Text = "+";
            this._currentChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.Location = new System.Drawing.Point(9, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "Y";
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 3;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.MarkersTable.Controls.Add(this._marker1TrackBar, 1, 0);
            this.MarkersTable.Controls.Add(this._marker2TrackBar, 1, 1);
            this.MarkersTable.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.MarkersTable.Controls.Add(this.panel4, 0, 2);
            this.MarkersTable.Controls.Add(this.label6, 2, 2);
            this.MarkersTable.Controls.Add(this.label9, 0, 0);
            this.MarkersTable.Controls.Add(this.label10, 0, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 3;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.Size = new System.Drawing.Size(1078, 90);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // _marker1TrackBar
            // 
            this._marker1TrackBar.BackColor = System.Drawing.Color.LightSkyBlue;
            this._marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker1TrackBar.Location = new System.Drawing.Point(66, 4);
            this._marker1TrackBar.Name = "_marker1TrackBar";
            this._marker1TrackBar.Size = new System.Drawing.Size(961, 24);
            this._marker1TrackBar.TabIndex = 2;
            this._marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker1TrackBar.Scroll += new System.EventHandler(this._marker1TrackBar_Scroll);
            // 
            // _marker2TrackBar
            // 
            this._marker2TrackBar.BackColor = System.Drawing.Color.Violet;
            this._marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker2TrackBar.LargeChange = 0;
            this._marker2TrackBar.Location = new System.Drawing.Point(66, 35);
            this._marker2TrackBar.Maximum = 3400;
            this._marker2TrackBar.Name = "_marker2TrackBar";
            this._marker2TrackBar.Size = new System.Drawing.Size(961, 24);
            this._marker2TrackBar.TabIndex = 3;
            this._marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker2TrackBar.Scroll += new System.EventHandler(this._marker2TrackBar_Scroll);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel4.Controls.Add(this._measuringChart, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(63, 63);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(967, 30);
            this.tableLayoutPanel4.TabIndex = 35;
            // 
            // _measuringChart
            // 
            this._measuringChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._measuringChart.BkGradient = false;
            this._measuringChart.BkGradientAngle = 90;
            this._measuringChart.BkGradientColor = System.Drawing.Color.White;
            this._measuringChart.BkGradientRate = 0.5F;
            this._measuringChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._measuringChart.BkRestrictedToChartPanel = false;
            this._measuringChart.BkShinePosition = 1F;
            this._measuringChart.BkTransparency = 0F;
            this._measuringChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._measuringChart.BorderExteriorLength = 0;
            this._measuringChart.BorderGradientAngle = 225;
            this._measuringChart.BorderGradientLightPos1 = 1F;
            this._measuringChart.BorderGradientLightPos2 = -1F;
            this._measuringChart.BorderGradientRate = 0.5F;
            this._measuringChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._measuringChart.BorderLightIntermediateBrightness = 0F;
            this._measuringChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._measuringChart.ChartPanelBackColor = System.Drawing.Color.White;
            this._measuringChart.ChartPanelBkTransparency = 0F;
            this._measuringChart.ControlShadow = false;
            this._measuringChart.CoordinateAxesVisible = true;
            this._measuringChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._measuringChart.CoordinateXOrigin = 0D;
            this._measuringChart.CoordinateYMax = 1D;
            this._measuringChart.CoordinateYMin = -1D;
            this._measuringChart.CoordinateYOrigin = 0D;
            this._measuringChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._measuringChart.FooterColor = System.Drawing.Color.Black;
            this._measuringChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._measuringChart.FooterVisible = false;
            this._measuringChart.GridColor = System.Drawing.Color.Gray;
            this._measuringChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._measuringChart.GridVisible = true;
            this._measuringChart.GridXSubTicker = 0;
            this._measuringChart.GridXTicker = 10;
            this._measuringChart.GridYSubTicker = 0;
            this._measuringChart.GridYTicker = 2;
            this._measuringChart.HeaderColor = System.Drawing.Color.Black;
            this._measuringChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._measuringChart.HeaderVisible = false;
            this._measuringChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._measuringChart.InnerBorderLength = 0;
            this._measuringChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._measuringChart.LegendBkColor = System.Drawing.Color.White;
            this._measuringChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._measuringChart.LegendVisible = false;
            this._measuringChart.Location = new System.Drawing.Point(16, 0);
            this._measuringChart.Margin = new System.Windows.Forms.Padding(0);
            this._measuringChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._measuringChart.MiddleBorderLength = 0;
            this._measuringChart.Name = "_measuringChart";
            this._measuringChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._measuringChart.OuterBorderLength = 0;
            this._measuringChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._measuringChart.Precision = 0;
            this._measuringChart.RoundRadius = 10;
            this._measuringChart.ShadowColor = System.Drawing.Color.DimGray;
            this._measuringChart.ShadowDepth = 8;
            this._measuringChart.ShadowRate = 0.5F;
            this._measuringChart.Size = new System.Drawing.Size(934, 30);
            this._measuringChart.TabIndex = 34;
            this._measuringChart.Text = "daS_Net_XYChart1";
            this._measuringChart.XMax = 100D;
            this._measuringChart.XMin = 0D;
            this._measuringChart.XScaleColor = System.Drawing.Color.Black;
            this._measuringChart.XScaleVisible = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(4, 66);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(44, 24);
            this.panel4.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Время";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1034, 69);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "мс";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 30);
            this.label9.TabIndex = 37;
            this.label9.Text = "Маркер 1";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 30);
            this.label10.TabIndex = 38;
            this.label10.Text = "Маркер 2";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1087, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(294, 722);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this._markerScrollPanel);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(293, 720);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мгновенные значения";
            // 
            // _markerScrollPanel
            // 
            this._markerScrollPanel.AutoScroll = true;
            this._markerScrollPanel.Controls.Add(this._marker2Box);
            this._markerScrollPanel.Controls.Add(this._marker1Box);
            this._markerScrollPanel.Controls.Add(this.groupBox4);
            this._markerScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._markerScrollPanel.Location = new System.Drawing.Point(0, 13);
            this._markerScrollPanel.Name = "_markerScrollPanel";
            this._markerScrollPanel.Size = new System.Drawing.Size(293, 707);
            this._markerScrollPanel.TabIndex = 3;
            // 
            // _marker2Box
            // 
            this._marker2Box.Controls.Add(this.groupBox6);
            this._marker2Box.Controls.Add(this.groupBox5);
            this._marker2Box.Controls.Add(this.groupBox3);
            this._marker2Box.Controls.Add(this.groupBox12);
            this._marker2Box.Location = new System.Drawing.Point(142, 3);
            this._marker2Box.Name = "_marker2Box";
            this._marker2Box.Size = new System.Drawing.Size(133, 912);
            this._marker2Box.TabIndex = 3;
            this._marker2Box.TabStop = false;
            this._marker2Box.Text = "Маркер 2";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._marker2K24);
            this.groupBox6.Controls.Add(this._marker2K23);
            this.groupBox6.Controls.Add(this._marker2K22);
            this.groupBox6.Controls.Add(this._marker2K21);
            this.groupBox6.Controls.Add(this._marker2K20);
            this.groupBox6.Controls.Add(this._marker2K19);
            this.groupBox6.Controls.Add(this._marker2K18);
            this.groupBox6.Controls.Add(this._marker2K17);
            this.groupBox6.Controls.Add(this._marker2K16);
            this.groupBox6.Controls.Add(this._marker2K15);
            this.groupBox6.Controls.Add(this._marker2K14);
            this.groupBox6.Controls.Add(this._marker2K13);
            this.groupBox6.Controls.Add(this._marker2K12);
            this.groupBox6.Controls.Add(this._marker2K11);
            this.groupBox6.Controls.Add(this._marker2K10);
            this.groupBox6.Controls.Add(this._marker2K9);
            this.groupBox6.Controls.Add(this._marker2K8);
            this.groupBox6.Controls.Add(this._marker2K7);
            this.groupBox6.Controls.Add(this._marker2K6);
            this.groupBox6.Controls.Add(this._marker2K5);
            this.groupBox6.Controls.Add(this._marker2K4);
            this.groupBox6.Controls.Add(this._marker2K3);
            this.groupBox6.Controls.Add(this._marker2K2);
            this.groupBox6.Controls.Add(this._marker2K1);
            this.groupBox6.Location = new System.Drawing.Point(5, 641);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(119, 263);
            this.groupBox6.TabIndex = 47;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Каналы";
            // 
            // _marker2K24
            // 
            this._marker2K24.AutoSize = true;
            this._marker2K24.Location = new System.Drawing.Point(63, 236);
            this._marker2K24.Name = "_marker2K24";
            this._marker2K24.Size = new System.Drawing.Size(38, 13);
            this._marker2K24.TabIndex = 52;
            this._marker2K24.Text = "К24 = ";
            // 
            // _marker2K23
            // 
            this._marker2K23.AutoSize = true;
            this._marker2K23.Location = new System.Drawing.Point(63, 216);
            this._marker2K23.Name = "_marker2K23";
            this._marker2K23.Size = new System.Drawing.Size(38, 13);
            this._marker2K23.TabIndex = 51;
            this._marker2K23.Text = "К23 = ";
            // 
            // _marker2K22
            // 
            this._marker2K22.AutoSize = true;
            this._marker2K22.Location = new System.Drawing.Point(63, 196);
            this._marker2K22.Name = "_marker2K22";
            this._marker2K22.Size = new System.Drawing.Size(38, 13);
            this._marker2K22.TabIndex = 50;
            this._marker2K22.Text = "К22 = ";
            // 
            // _marker2K21
            // 
            this._marker2K21.AutoSize = true;
            this._marker2K21.Location = new System.Drawing.Point(63, 176);
            this._marker2K21.Name = "_marker2K21";
            this._marker2K21.Size = new System.Drawing.Size(38, 13);
            this._marker2K21.TabIndex = 49;
            this._marker2K21.Text = "К21 = ";
            // 
            // _marker2K20
            // 
            this._marker2K20.AutoSize = true;
            this._marker2K20.Location = new System.Drawing.Point(63, 156);
            this._marker2K20.Name = "_marker2K20";
            this._marker2K20.Size = new System.Drawing.Size(38, 13);
            this._marker2K20.TabIndex = 48;
            this._marker2K20.Text = "К20 = ";
            // 
            // _marker2K19
            // 
            this._marker2K19.AutoSize = true;
            this._marker2K19.Location = new System.Drawing.Point(63, 136);
            this._marker2K19.Name = "_marker2K19";
            this._marker2K19.Size = new System.Drawing.Size(38, 13);
            this._marker2K19.TabIndex = 47;
            this._marker2K19.Text = "К19 = ";
            // 
            // _marker2K18
            // 
            this._marker2K18.AutoSize = true;
            this._marker2K18.Location = new System.Drawing.Point(63, 116);
            this._marker2K18.Name = "_marker2K18";
            this._marker2K18.Size = new System.Drawing.Size(38, 13);
            this._marker2K18.TabIndex = 46;
            this._marker2K18.Text = "К18 = ";
            // 
            // _marker2K17
            // 
            this._marker2K17.AutoSize = true;
            this._marker2K17.Location = new System.Drawing.Point(63, 96);
            this._marker2K17.Name = "_marker2K17";
            this._marker2K17.Size = new System.Drawing.Size(38, 13);
            this._marker2K17.TabIndex = 45;
            this._marker2K17.Text = "К17 = ";
            // 
            // _marker2K16
            // 
            this._marker2K16.AutoSize = true;
            this._marker2K16.Location = new System.Drawing.Point(62, 76);
            this._marker2K16.Name = "_marker2K16";
            this._marker2K16.Size = new System.Drawing.Size(38, 13);
            this._marker2K16.TabIndex = 44;
            this._marker2K16.Text = "К16 = ";
            // 
            // _marker2K15
            // 
            this._marker2K15.AutoSize = true;
            this._marker2K15.Location = new System.Drawing.Point(62, 56);
            this._marker2K15.Name = "_marker2K15";
            this._marker2K15.Size = new System.Drawing.Size(38, 13);
            this._marker2K15.TabIndex = 43;
            this._marker2K15.Text = "К15 = ";
            // 
            // _marker2K14
            // 
            this._marker2K14.AutoSize = true;
            this._marker2K14.Location = new System.Drawing.Point(62, 36);
            this._marker2K14.Name = "_marker2K14";
            this._marker2K14.Size = new System.Drawing.Size(38, 13);
            this._marker2K14.TabIndex = 42;
            this._marker2K14.Text = "К14 = ";
            // 
            // _marker2K13
            // 
            this._marker2K13.AutoSize = true;
            this._marker2K13.Location = new System.Drawing.Point(62, 16);
            this._marker2K13.Name = "_marker2K13";
            this._marker2K13.Size = new System.Drawing.Size(38, 13);
            this._marker2K13.TabIndex = 41;
            this._marker2K13.Text = "К13 = ";
            // 
            // _marker2K12
            // 
            this._marker2K12.AutoSize = true;
            this._marker2K12.Location = new System.Drawing.Point(7, 236);
            this._marker2K12.Name = "_marker2K12";
            this._marker2K12.Size = new System.Drawing.Size(38, 13);
            this._marker2K12.TabIndex = 40;
            this._marker2K12.Text = "К12 = ";
            // 
            // _marker2K11
            // 
            this._marker2K11.AutoSize = true;
            this._marker2K11.Location = new System.Drawing.Point(7, 216);
            this._marker2K11.Name = "_marker2K11";
            this._marker2K11.Size = new System.Drawing.Size(38, 13);
            this._marker2K11.TabIndex = 39;
            this._marker2K11.Text = "К11 = ";
            // 
            // _marker2K10
            // 
            this._marker2K10.AutoSize = true;
            this._marker2K10.Location = new System.Drawing.Point(7, 196);
            this._marker2K10.Name = "_marker2K10";
            this._marker2K10.Size = new System.Drawing.Size(38, 13);
            this._marker2K10.TabIndex = 38;
            this._marker2K10.Text = "К10 = ";
            // 
            // _marker2K9
            // 
            this._marker2K9.AutoSize = true;
            this._marker2K9.Location = new System.Drawing.Point(7, 176);
            this._marker2K9.Name = "_marker2K9";
            this._marker2K9.Size = new System.Drawing.Size(32, 13);
            this._marker2K9.TabIndex = 37;
            this._marker2K9.Text = "К9 = ";
            // 
            // _marker2K8
            // 
            this._marker2K8.AutoSize = true;
            this._marker2K8.Location = new System.Drawing.Point(7, 156);
            this._marker2K8.Name = "_marker2K8";
            this._marker2K8.Size = new System.Drawing.Size(32, 13);
            this._marker2K8.TabIndex = 36;
            this._marker2K8.Text = "К8 = ";
            // 
            // _marker2K7
            // 
            this._marker2K7.AutoSize = true;
            this._marker2K7.Location = new System.Drawing.Point(7, 136);
            this._marker2K7.Name = "_marker2K7";
            this._marker2K7.Size = new System.Drawing.Size(32, 13);
            this._marker2K7.TabIndex = 35;
            this._marker2K7.Text = "К7 = ";
            // 
            // _marker2K6
            // 
            this._marker2K6.AutoSize = true;
            this._marker2K6.Location = new System.Drawing.Point(7, 116);
            this._marker2K6.Name = "_marker2K6";
            this._marker2K6.Size = new System.Drawing.Size(32, 13);
            this._marker2K6.TabIndex = 34;
            this._marker2K6.Text = "К6 = ";
            // 
            // _marker2K5
            // 
            this._marker2K5.AutoSize = true;
            this._marker2K5.Location = new System.Drawing.Point(7, 96);
            this._marker2K5.Name = "_marker2K5";
            this._marker2K5.Size = new System.Drawing.Size(32, 13);
            this._marker2K5.TabIndex = 33;
            this._marker2K5.Text = "К5 = ";
            // 
            // _marker2K4
            // 
            this._marker2K4.AutoSize = true;
            this._marker2K4.Location = new System.Drawing.Point(6, 76);
            this._marker2K4.Name = "_marker2K4";
            this._marker2K4.Size = new System.Drawing.Size(32, 13);
            this._marker2K4.TabIndex = 32;
            this._marker2K4.Text = "К4 = ";
            // 
            // _marker2K3
            // 
            this._marker2K3.AutoSize = true;
            this._marker2K3.Location = new System.Drawing.Point(6, 56);
            this._marker2K3.Name = "_marker2K3";
            this._marker2K3.Size = new System.Drawing.Size(32, 13);
            this._marker2K3.TabIndex = 31;
            this._marker2K3.Text = "К3 = ";
            // 
            // _marker2K2
            // 
            this._marker2K2.AutoSize = true;
            this._marker2K2.Location = new System.Drawing.Point(6, 36);
            this._marker2K2.Name = "_marker2K2";
            this._marker2K2.Size = new System.Drawing.Size(32, 13);
            this._marker2K2.TabIndex = 30;
            this._marker2K2.Text = "К2 = ";
            // 
            // _marker2K1
            // 
            this._marker2K1.AutoSize = true;
            this._marker2K1.Location = new System.Drawing.Point(6, 16);
            this._marker2K1.Name = "_marker2K1";
            this._marker2K1.Size = new System.Drawing.Size(32, 13);
            this._marker2K1.TabIndex = 29;
            this._marker2K1.Text = "К1 = ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._marker2D40);
            this.groupBox5.Controls.Add(this._marker2D31);
            this.groupBox5.Controls.Add(this._marker2D30);
            this.groupBox5.Controls.Add(this._marker2D29);
            this.groupBox5.Controls.Add(this._marker2D39);
            this.groupBox5.Controls.Add(this._marker2D38);
            this.groupBox5.Controls.Add(this._marker2D37);
            this.groupBox5.Controls.Add(this._marker2D36);
            this.groupBox5.Controls.Add(this._marker2D35);
            this.groupBox5.Controls.Add(this._marker2D34);
            this.groupBox5.Controls.Add(this._marker2D33);
            this.groupBox5.Controls.Add(this._marker2D32);
            this.groupBox5.Controls.Add(this._marker2D28);
            this.groupBox5.Controls.Add(this._marker2D27);
            this.groupBox5.Controls.Add(this._marker2D26);
            this.groupBox5.Controls.Add(this._marker2D25);
            this.groupBox5.Controls.Add(this._marker2D24);
            this.groupBox5.Controls.Add(this._marker2D23);
            this.groupBox5.Controls.Add(this._marker2D22);
            this.groupBox5.Controls.Add(this._marker2D21);
            this.groupBox5.Controls.Add(this._marker2D20);
            this.groupBox5.Controls.Add(this._marker2D11);
            this.groupBox5.Controls.Add(this._marker2D10);
            this.groupBox5.Controls.Add(this._marker2D9);
            this.groupBox5.Controls.Add(this._marker2D19);
            this.groupBox5.Controls.Add(this._marker2D18);
            this.groupBox5.Controls.Add(this._marker2D17);
            this.groupBox5.Controls.Add(this._marker2D16);
            this.groupBox5.Controls.Add(this._marker2D15);
            this.groupBox5.Controls.Add(this._marker2D14);
            this.groupBox5.Controls.Add(this._marker2D13);
            this.groupBox5.Controls.Add(this._marker2D12);
            this.groupBox5.Controls.Add(this._marker2D8);
            this.groupBox5.Controls.Add(this._marker2D7);
            this.groupBox5.Controls.Add(this._marker2D6);
            this.groupBox5.Controls.Add(this._marker2D5);
            this.groupBox5.Controls.Add(this._marker2D4);
            this.groupBox5.Controls.Add(this._marker2D3);
            this.groupBox5.Controls.Add(this._marker2D2);
            this.groupBox5.Controls.Add(this._marker2D1);
            this.groupBox5.Location = new System.Drawing.Point(5, 214);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(119, 421);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Дискреты";
            // 
            // _marker2D40
            // 
            this._marker2D40.AutoSize = true;
            this._marker2D40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D40.Location = new System.Drawing.Point(62, 396);
            this._marker2D40.Name = "_marker2D40";
            this._marker2D40.Size = new System.Drawing.Size(40, 13);
            this._marker2D40.TabIndex = 55;
            this._marker2D40.Text = "Д40 = ";
            // 
            // _marker2D31
            // 
            this._marker2D31.AutoSize = true;
            this._marker2D31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D31.Location = new System.Drawing.Point(63, 216);
            this._marker2D31.Name = "_marker2D31";
            this._marker2D31.Size = new System.Drawing.Size(40, 13);
            this._marker2D31.TabIndex = 54;
            this._marker2D31.Text = "Д31 = ";
            // 
            // _marker2D30
            // 
            this._marker2D30.AutoSize = true;
            this._marker2D30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D30.Location = new System.Drawing.Point(63, 196);
            this._marker2D30.Name = "_marker2D30";
            this._marker2D30.Size = new System.Drawing.Size(40, 13);
            this._marker2D30.TabIndex = 53;
            this._marker2D30.Text = "Д30 = ";
            // 
            // _marker2D29
            // 
            this._marker2D29.AutoSize = true;
            this._marker2D29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D29.Location = new System.Drawing.Point(63, 176);
            this._marker2D29.Name = "_marker2D29";
            this._marker2D29.Size = new System.Drawing.Size(40, 13);
            this._marker2D29.TabIndex = 52;
            this._marker2D29.Text = "Д29 = ";
            // 
            // _marker2D39
            // 
            this._marker2D39.AutoSize = true;
            this._marker2D39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D39.Location = new System.Drawing.Point(62, 376);
            this._marker2D39.Name = "_marker2D39";
            this._marker2D39.Size = new System.Drawing.Size(40, 13);
            this._marker2D39.TabIndex = 51;
            this._marker2D39.Text = "Д39 = ";
            // 
            // _marker2D38
            // 
            this._marker2D38.AutoSize = true;
            this._marker2D38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D38.Location = new System.Drawing.Point(62, 356);
            this._marker2D38.Name = "_marker2D38";
            this._marker2D38.Size = new System.Drawing.Size(40, 13);
            this._marker2D38.TabIndex = 50;
            this._marker2D38.Text = "Д38 = ";
            // 
            // _marker2D37
            // 
            this._marker2D37.AutoSize = true;
            this._marker2D37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D37.Location = new System.Drawing.Point(62, 336);
            this._marker2D37.Name = "_marker2D37";
            this._marker2D37.Size = new System.Drawing.Size(40, 13);
            this._marker2D37.TabIndex = 49;
            this._marker2D37.Text = "Д37 = ";
            // 
            // _marker2D36
            // 
            this._marker2D36.AutoSize = true;
            this._marker2D36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D36.Location = new System.Drawing.Point(62, 316);
            this._marker2D36.Name = "_marker2D36";
            this._marker2D36.Size = new System.Drawing.Size(40, 13);
            this._marker2D36.TabIndex = 48;
            this._marker2D36.Text = "Д36 = ";
            // 
            // _marker2D35
            // 
            this._marker2D35.AutoSize = true;
            this._marker2D35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D35.Location = new System.Drawing.Point(62, 296);
            this._marker2D35.Name = "_marker2D35";
            this._marker2D35.Size = new System.Drawing.Size(40, 13);
            this._marker2D35.TabIndex = 47;
            this._marker2D35.Text = "Д35 = ";
            // 
            // _marker2D34
            // 
            this._marker2D34.AutoSize = true;
            this._marker2D34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D34.Location = new System.Drawing.Point(62, 276);
            this._marker2D34.Name = "_marker2D34";
            this._marker2D34.Size = new System.Drawing.Size(40, 13);
            this._marker2D34.TabIndex = 46;
            this._marker2D34.Text = "Д34 = ";
            // 
            // _marker2D33
            // 
            this._marker2D33.AutoSize = true;
            this._marker2D33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D33.Location = new System.Drawing.Point(62, 256);
            this._marker2D33.Name = "_marker2D33";
            this._marker2D33.Size = new System.Drawing.Size(40, 13);
            this._marker2D33.TabIndex = 45;
            this._marker2D33.Text = "Д33 = ";
            // 
            // _marker2D32
            // 
            this._marker2D32.AutoSize = true;
            this._marker2D32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D32.Location = new System.Drawing.Point(62, 236);
            this._marker2D32.Name = "_marker2D32";
            this._marker2D32.Size = new System.Drawing.Size(40, 13);
            this._marker2D32.TabIndex = 44;
            this._marker2D32.Text = "Д32 = ";
            // 
            // _marker2D28
            // 
            this._marker2D28.AutoSize = true;
            this._marker2D28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D28.Location = new System.Drawing.Point(63, 156);
            this._marker2D28.Name = "_marker2D28";
            this._marker2D28.Size = new System.Drawing.Size(40, 13);
            this._marker2D28.TabIndex = 43;
            this._marker2D28.Text = "Д28 = ";
            // 
            // _marker2D27
            // 
            this._marker2D27.AutoSize = true;
            this._marker2D27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D27.Location = new System.Drawing.Point(63, 136);
            this._marker2D27.Name = "_marker2D27";
            this._marker2D27.Size = new System.Drawing.Size(40, 13);
            this._marker2D27.TabIndex = 42;
            this._marker2D27.Text = "Д27 = ";
            // 
            // _marker2D26
            // 
            this._marker2D26.AutoSize = true;
            this._marker2D26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D26.Location = new System.Drawing.Point(63, 116);
            this._marker2D26.Name = "_marker2D26";
            this._marker2D26.Size = new System.Drawing.Size(40, 13);
            this._marker2D26.TabIndex = 41;
            this._marker2D26.Text = "Д26 = ";
            // 
            // _marker2D25
            // 
            this._marker2D25.AutoSize = true;
            this._marker2D25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D25.Location = new System.Drawing.Point(63, 96);
            this._marker2D25.Name = "_marker2D25";
            this._marker2D25.Size = new System.Drawing.Size(40, 13);
            this._marker2D25.TabIndex = 40;
            this._marker2D25.Text = "Д25 = ";
            // 
            // _marker2D24
            // 
            this._marker2D24.AutoSize = true;
            this._marker2D24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D24.Location = new System.Drawing.Point(63, 76);
            this._marker2D24.Name = "_marker2D24";
            this._marker2D24.Size = new System.Drawing.Size(40, 13);
            this._marker2D24.TabIndex = 39;
            this._marker2D24.Text = "Д24 = ";
            // 
            // _marker2D23
            // 
            this._marker2D23.AutoSize = true;
            this._marker2D23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D23.Location = new System.Drawing.Point(63, 56);
            this._marker2D23.Name = "_marker2D23";
            this._marker2D23.Size = new System.Drawing.Size(40, 13);
            this._marker2D23.TabIndex = 38;
            this._marker2D23.Text = "Д23 = ";
            // 
            // _marker2D22
            // 
            this._marker2D22.AutoSize = true;
            this._marker2D22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D22.Location = new System.Drawing.Point(62, 36);
            this._marker2D22.Name = "_marker2D22";
            this._marker2D22.Size = new System.Drawing.Size(40, 13);
            this._marker2D22.TabIndex = 37;
            this._marker2D22.Text = "Д22 = ";
            // 
            // _marker2D21
            // 
            this._marker2D21.AutoSize = true;
            this._marker2D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D21.Location = new System.Drawing.Point(62, 16);
            this._marker2D21.Name = "_marker2D21";
            this._marker2D21.Size = new System.Drawing.Size(40, 13);
            this._marker2D21.TabIndex = 36;
            this._marker2D21.Text = "Д21 = ";
            // 
            // _marker2D20
            // 
            this._marker2D20.AutoSize = true;
            this._marker2D20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D20.Location = new System.Drawing.Point(5, 396);
            this._marker2D20.Name = "_marker2D20";
            this._marker2D20.Size = new System.Drawing.Size(40, 13);
            this._marker2D20.TabIndex = 35;
            this._marker2D20.Text = "Д20 = ";
            // 
            // _marker2D11
            // 
            this._marker2D11.AutoSize = true;
            this._marker2D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D11.Location = new System.Drawing.Point(6, 216);
            this._marker2D11.Name = "_marker2D11";
            this._marker2D11.Size = new System.Drawing.Size(40, 13);
            this._marker2D11.TabIndex = 34;
            this._marker2D11.Text = "Д11 = ";
            // 
            // _marker2D10
            // 
            this._marker2D10.AutoSize = true;
            this._marker2D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D10.Location = new System.Drawing.Point(6, 196);
            this._marker2D10.Name = "_marker2D10";
            this._marker2D10.Size = new System.Drawing.Size(40, 13);
            this._marker2D10.TabIndex = 33;
            this._marker2D10.Text = "Д10 = ";
            // 
            // _marker2D9
            // 
            this._marker2D9.AutoSize = true;
            this._marker2D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D9.Location = new System.Drawing.Point(6, 176);
            this._marker2D9.Name = "_marker2D9";
            this._marker2D9.Size = new System.Drawing.Size(34, 13);
            this._marker2D9.TabIndex = 32;
            this._marker2D9.Text = "Д9 = ";
            // 
            // _marker2D19
            // 
            this._marker2D19.AutoSize = true;
            this._marker2D19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D19.Location = new System.Drawing.Point(5, 376);
            this._marker2D19.Name = "_marker2D19";
            this._marker2D19.Size = new System.Drawing.Size(40, 13);
            this._marker2D19.TabIndex = 31;
            this._marker2D19.Text = "Д19 = ";
            // 
            // _marker2D18
            // 
            this._marker2D18.AutoSize = true;
            this._marker2D18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D18.Location = new System.Drawing.Point(5, 356);
            this._marker2D18.Name = "_marker2D18";
            this._marker2D18.Size = new System.Drawing.Size(40, 13);
            this._marker2D18.TabIndex = 30;
            this._marker2D18.Text = "Д18 = ";
            // 
            // _marker2D17
            // 
            this._marker2D17.AutoSize = true;
            this._marker2D17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D17.Location = new System.Drawing.Point(5, 336);
            this._marker2D17.Name = "_marker2D17";
            this._marker2D17.Size = new System.Drawing.Size(40, 13);
            this._marker2D17.TabIndex = 29;
            this._marker2D17.Text = "Д17 = ";
            // 
            // _marker2D16
            // 
            this._marker2D16.AutoSize = true;
            this._marker2D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D16.Location = new System.Drawing.Point(5, 316);
            this._marker2D16.Name = "_marker2D16";
            this._marker2D16.Size = new System.Drawing.Size(40, 13);
            this._marker2D16.TabIndex = 28;
            this._marker2D16.Text = "Д16 = ";
            // 
            // _marker2D15
            // 
            this._marker2D15.AutoSize = true;
            this._marker2D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D15.Location = new System.Drawing.Point(5, 296);
            this._marker2D15.Name = "_marker2D15";
            this._marker2D15.Size = new System.Drawing.Size(40, 13);
            this._marker2D15.TabIndex = 27;
            this._marker2D15.Text = "Д15 = ";
            // 
            // _marker2D14
            // 
            this._marker2D14.AutoSize = true;
            this._marker2D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D14.Location = new System.Drawing.Point(5, 276);
            this._marker2D14.Name = "_marker2D14";
            this._marker2D14.Size = new System.Drawing.Size(40, 13);
            this._marker2D14.TabIndex = 26;
            this._marker2D14.Text = "Д14 = ";
            // 
            // _marker2D13
            // 
            this._marker2D13.AutoSize = true;
            this._marker2D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D13.Location = new System.Drawing.Point(5, 256);
            this._marker2D13.Name = "_marker2D13";
            this._marker2D13.Size = new System.Drawing.Size(40, 13);
            this._marker2D13.TabIndex = 25;
            this._marker2D13.Text = "Д13 = ";
            // 
            // _marker2D12
            // 
            this._marker2D12.AutoSize = true;
            this._marker2D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D12.Location = new System.Drawing.Point(5, 236);
            this._marker2D12.Name = "_marker2D12";
            this._marker2D12.Size = new System.Drawing.Size(40, 13);
            this._marker2D12.TabIndex = 24;
            this._marker2D12.Text = "Д12 = ";
            // 
            // _marker2D8
            // 
            this._marker2D8.AutoSize = true;
            this._marker2D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D8.Location = new System.Drawing.Point(6, 156);
            this._marker2D8.Name = "_marker2D8";
            this._marker2D8.Size = new System.Drawing.Size(34, 13);
            this._marker2D8.TabIndex = 23;
            this._marker2D8.Text = "Д8 = ";
            // 
            // _marker2D7
            // 
            this._marker2D7.AutoSize = true;
            this._marker2D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D7.Location = new System.Drawing.Point(6, 136);
            this._marker2D7.Name = "_marker2D7";
            this._marker2D7.Size = new System.Drawing.Size(34, 13);
            this._marker2D7.TabIndex = 22;
            this._marker2D7.Text = "Д7 = ";
            // 
            // _marker2D6
            // 
            this._marker2D6.AutoSize = true;
            this._marker2D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D6.Location = new System.Drawing.Point(6, 116);
            this._marker2D6.Name = "_marker2D6";
            this._marker2D6.Size = new System.Drawing.Size(34, 13);
            this._marker2D6.TabIndex = 21;
            this._marker2D6.Text = "Д6 = ";
            // 
            // _marker2D5
            // 
            this._marker2D5.AutoSize = true;
            this._marker2D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D5.Location = new System.Drawing.Point(6, 96);
            this._marker2D5.Name = "_marker2D5";
            this._marker2D5.Size = new System.Drawing.Size(34, 13);
            this._marker2D5.TabIndex = 20;
            this._marker2D5.Text = "Д5 = ";
            // 
            // _marker2D4
            // 
            this._marker2D4.AutoSize = true;
            this._marker2D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D4.Location = new System.Drawing.Point(6, 76);
            this._marker2D4.Name = "_marker2D4";
            this._marker2D4.Size = new System.Drawing.Size(34, 13);
            this._marker2D4.TabIndex = 19;
            this._marker2D4.Text = "Д4 = ";
            // 
            // _marker2D3
            // 
            this._marker2D3.AutoSize = true;
            this._marker2D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D3.Location = new System.Drawing.Point(6, 56);
            this._marker2D3.Name = "_marker2D3";
            this._marker2D3.Size = new System.Drawing.Size(34, 13);
            this._marker2D3.TabIndex = 18;
            this._marker2D3.Text = "Д3 = ";
            // 
            // _marker2D2
            // 
            this._marker2D2.AutoSize = true;
            this._marker2D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D2.Location = new System.Drawing.Point(6, 36);
            this._marker2D2.Name = "_marker2D2";
            this._marker2D2.Size = new System.Drawing.Size(34, 13);
            this._marker2D2.TabIndex = 17;
            this._marker2D2.Text = "Д2 = ";
            // 
            // _marker2D1
            // 
            this._marker2D1.AutoSize = true;
            this._marker2D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D1.Location = new System.Drawing.Point(6, 16);
            this._marker2D1.Name = "_marker2D1";
            this._marker2D1.Size = new System.Drawing.Size(34, 13);
            this._marker2D1.TabIndex = 16;
            this._marker2D1.Text = "Д1 = ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._marker2U4);
            this.groupBox3.Controls.Add(this._marker2U3);
            this.groupBox3.Controls.Add(this._marker2U2);
            this.groupBox3.Controls.Add(this._marker2U1);
            this.groupBox3.Location = new System.Drawing.Point(5, 113);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(119, 95);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Напряжения";
            // 
            // _marker2U4
            // 
            this._marker2U4.AutoSize = true;
            this._marker2U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U4.Location = new System.Drawing.Point(6, 78);
            this._marker2U4.Name = "_marker2U4";
            this._marker2U4.Size = new System.Drawing.Size(28, 13);
            this._marker2U4.TabIndex = 3;
            this._marker2U4.Text = "In = ";
            // 
            // _marker2U3
            // 
            this._marker2U3.AutoSize = true;
            this._marker2U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U3.Location = new System.Drawing.Point(6, 58);
            this._marker2U3.Name = "_marker2U3";
            this._marker2U3.Size = new System.Drawing.Size(28, 13);
            this._marker2U3.TabIndex = 2;
            this._marker2U3.Text = "Ic = ";
            // 
            // _marker2U2
            // 
            this._marker2U2.AutoSize = true;
            this._marker2U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U2.Location = new System.Drawing.Point(6, 38);
            this._marker2U2.Name = "_marker2U2";
            this._marker2U2.Size = new System.Drawing.Size(28, 13);
            this._marker2U2.TabIndex = 1;
            this._marker2U2.Text = "Ib = ";
            // 
            // _marker2U1
            // 
            this._marker2U1.AutoSize = true;
            this._marker2U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2U1.Location = new System.Drawing.Point(6, 18);
            this._marker2U1.Name = "_marker2U1";
            this._marker2U1.Size = new System.Drawing.Size(28, 13);
            this._marker2U1.TabIndex = 0;
            this._marker2U1.Text = "Ia = ";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._marker2I4);
            this.groupBox12.Controls.Add(this._marker2I3);
            this.groupBox12.Controls.Add(this._marker2I2);
            this.groupBox12.Controls.Add(this._marker2I1);
            this.groupBox12.Location = new System.Drawing.Point(5, 12);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(119, 95);
            this.groupBox12.TabIndex = 40;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Токи";
            // 
            // _marker2I4
            // 
            this._marker2I4.AutoSize = true;
            this._marker2I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I4.Location = new System.Drawing.Point(6, 78);
            this._marker2I4.Name = "_marker2I4";
            this._marker2I4.Size = new System.Drawing.Size(25, 13);
            this._marker2I4.TabIndex = 3;
            this._marker2I4.Text = "In =";
            // 
            // _marker2I3
            // 
            this._marker2I3.AutoSize = true;
            this._marker2I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I3.Location = new System.Drawing.Point(6, 58);
            this._marker2I3.Name = "_marker2I3";
            this._marker2I3.Size = new System.Drawing.Size(25, 13);
            this._marker2I3.TabIndex = 2;
            this._marker2I3.Text = "Ic =";
            // 
            // _marker2I2
            // 
            this._marker2I2.AutoSize = true;
            this._marker2I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I2.Location = new System.Drawing.Point(6, 38);
            this._marker2I2.Name = "_marker2I2";
            this._marker2I2.Size = new System.Drawing.Size(25, 13);
            this._marker2I2.TabIndex = 1;
            this._marker2I2.Text = "Ib =";
            // 
            // _marker2I1
            // 
            this._marker2I1.AutoSize = true;
            this._marker2I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I1.Location = new System.Drawing.Point(6, 18);
            this._marker2I1.Name = "_marker2I1";
            this._marker2I1.Size = new System.Drawing.Size(25, 13);
            this._marker2I1.TabIndex = 0;
            this._marker2I1.Text = "Ia =";
            // 
            // _marker1Box
            // 
            this._marker1Box.Controls.Add(this.groupBox2);
            this._marker1Box.Controls.Add(this.groupBox16);
            this._marker1Box.Controls.Add(this.groupBox11);
            this._marker1Box.Controls.Add(this.groupBox8);
            this._marker1Box.Location = new System.Drawing.Point(3, 3);
            this._marker1Box.Name = "_marker1Box";
            this._marker1Box.Size = new System.Drawing.Size(133, 912);
            this._marker1Box.TabIndex = 0;
            this._marker1Box.TabStop = false;
            this._marker1Box.Text = "Маркер 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._marker1U4);
            this.groupBox2.Controls.Add(this._marker1U3);
            this.groupBox2.Controls.Add(this._marker1U2);
            this.groupBox2.Controls.Add(this._marker1U1);
            this.groupBox2.Location = new System.Drawing.Point(5, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(119, 95);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Напряжения";
            // 
            // _marker1U4
            // 
            this._marker1U4.AutoSize = true;
            this._marker1U4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U4.Location = new System.Drawing.Point(6, 78);
            this._marker1U4.Name = "_marker1U4";
            this._marker1U4.Size = new System.Drawing.Size(28, 13);
            this._marker1U4.TabIndex = 3;
            this._marker1U4.Text = "In = ";
            // 
            // _marker1U3
            // 
            this._marker1U3.AutoSize = true;
            this._marker1U3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U3.Location = new System.Drawing.Point(6, 58);
            this._marker1U3.Name = "_marker1U3";
            this._marker1U3.Size = new System.Drawing.Size(28, 13);
            this._marker1U3.TabIndex = 2;
            this._marker1U3.Text = "Ic = ";
            // 
            // _marker1U2
            // 
            this._marker1U2.AutoSize = true;
            this._marker1U2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U2.Location = new System.Drawing.Point(6, 38);
            this._marker1U2.Name = "_marker1U2";
            this._marker1U2.Size = new System.Drawing.Size(28, 13);
            this._marker1U2.TabIndex = 1;
            this._marker1U2.Text = "Ib = ";
            // 
            // _marker1U1
            // 
            this._marker1U1.AutoSize = true;
            this._marker1U1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1U1.Location = new System.Drawing.Point(6, 18);
            this._marker1U1.Name = "_marker1U1";
            this._marker1U1.Size = new System.Drawing.Size(28, 13);
            this._marker1U1.TabIndex = 0;
            this._marker1U1.Text = "Ia = ";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._marker1K24);
            this.groupBox16.Controls.Add(this._marker1K23);
            this.groupBox16.Controls.Add(this._marker1K22);
            this.groupBox16.Controls.Add(this._marker1K21);
            this.groupBox16.Controls.Add(this._marker1K20);
            this.groupBox16.Controls.Add(this._marker1K19);
            this.groupBox16.Controls.Add(this._marker1K18);
            this.groupBox16.Controls.Add(this._marker1K17);
            this.groupBox16.Controls.Add(this._marker1K16);
            this.groupBox16.Controls.Add(this._marker1K15);
            this.groupBox16.Controls.Add(this._marker1K14);
            this.groupBox16.Controls.Add(this._marker1K13);
            this.groupBox16.Controls.Add(this._marker1K12);
            this.groupBox16.Controls.Add(this._marker1K11);
            this.groupBox16.Controls.Add(this._marker1K10);
            this.groupBox16.Controls.Add(this._marker1K9);
            this.groupBox16.Controls.Add(this._marker1K8);
            this.groupBox16.Controls.Add(this._marker1K7);
            this.groupBox16.Controls.Add(this._marker1K6);
            this.groupBox16.Controls.Add(this._marker1K5);
            this.groupBox16.Controls.Add(this._marker1K4);
            this.groupBox16.Controls.Add(this._marker1K3);
            this.groupBox16.Controls.Add(this._marker1K2);
            this.groupBox16.Controls.Add(this._marker1K1);
            this.groupBox16.Location = new System.Drawing.Point(5, 641);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(119, 263);
            this.groupBox16.TabIndex = 44;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Каналы";
            // 
            // _marker1K24
            // 
            this._marker1K24.AutoSize = true;
            this._marker1K24.Location = new System.Drawing.Point(63, 236);
            this._marker1K24.Name = "_marker1K24";
            this._marker1K24.Size = new System.Drawing.Size(38, 13);
            this._marker1K24.TabIndex = 52;
            this._marker1K24.Text = "К24 = ";
            // 
            // _marker1K23
            // 
            this._marker1K23.AutoSize = true;
            this._marker1K23.Location = new System.Drawing.Point(63, 216);
            this._marker1K23.Name = "_marker1K23";
            this._marker1K23.Size = new System.Drawing.Size(38, 13);
            this._marker1K23.TabIndex = 51;
            this._marker1K23.Text = "К23 = ";
            // 
            // _marker1K22
            // 
            this._marker1K22.AutoSize = true;
            this._marker1K22.Location = new System.Drawing.Point(63, 196);
            this._marker1K22.Name = "_marker1K22";
            this._marker1K22.Size = new System.Drawing.Size(38, 13);
            this._marker1K22.TabIndex = 50;
            this._marker1K22.Text = "К22 = ";
            // 
            // _marker1K21
            // 
            this._marker1K21.AutoSize = true;
            this._marker1K21.Location = new System.Drawing.Point(63, 176);
            this._marker1K21.Name = "_marker1K21";
            this._marker1K21.Size = new System.Drawing.Size(38, 13);
            this._marker1K21.TabIndex = 49;
            this._marker1K21.Text = "К21 = ";
            // 
            // _marker1K20
            // 
            this._marker1K20.AutoSize = true;
            this._marker1K20.Location = new System.Drawing.Point(63, 156);
            this._marker1K20.Name = "_marker1K20";
            this._marker1K20.Size = new System.Drawing.Size(38, 13);
            this._marker1K20.TabIndex = 48;
            this._marker1K20.Text = "К20 = ";
            // 
            // _marker1K19
            // 
            this._marker1K19.AutoSize = true;
            this._marker1K19.Location = new System.Drawing.Point(63, 136);
            this._marker1K19.Name = "_marker1K19";
            this._marker1K19.Size = new System.Drawing.Size(38, 13);
            this._marker1K19.TabIndex = 47;
            this._marker1K19.Text = "К19 = ";
            // 
            // _marker1K18
            // 
            this._marker1K18.AutoSize = true;
            this._marker1K18.Location = new System.Drawing.Point(63, 116);
            this._marker1K18.Name = "_marker1K18";
            this._marker1K18.Size = new System.Drawing.Size(38, 13);
            this._marker1K18.TabIndex = 46;
            this._marker1K18.Text = "К18 = ";
            // 
            // _marker1K17
            // 
            this._marker1K17.AutoSize = true;
            this._marker1K17.Location = new System.Drawing.Point(63, 96);
            this._marker1K17.Name = "_marker1K17";
            this._marker1K17.Size = new System.Drawing.Size(38, 13);
            this._marker1K17.TabIndex = 45;
            this._marker1K17.Text = "К17 = ";
            // 
            // _marker1K16
            // 
            this._marker1K16.AutoSize = true;
            this._marker1K16.Location = new System.Drawing.Point(62, 76);
            this._marker1K16.Name = "_marker1K16";
            this._marker1K16.Size = new System.Drawing.Size(38, 13);
            this._marker1K16.TabIndex = 44;
            this._marker1K16.Text = "К16 = ";
            // 
            // _marker1K15
            // 
            this._marker1K15.AutoSize = true;
            this._marker1K15.Location = new System.Drawing.Point(62, 56);
            this._marker1K15.Name = "_marker1K15";
            this._marker1K15.Size = new System.Drawing.Size(38, 13);
            this._marker1K15.TabIndex = 43;
            this._marker1K15.Text = "К15 = ";
            // 
            // _marker1K14
            // 
            this._marker1K14.AutoSize = true;
            this._marker1K14.Location = new System.Drawing.Point(62, 36);
            this._marker1K14.Name = "_marker1K14";
            this._marker1K14.Size = new System.Drawing.Size(38, 13);
            this._marker1K14.TabIndex = 42;
            this._marker1K14.Text = "К14 = ";
            // 
            // _marker1K13
            // 
            this._marker1K13.AutoSize = true;
            this._marker1K13.Location = new System.Drawing.Point(62, 16);
            this._marker1K13.Name = "_marker1K13";
            this._marker1K13.Size = new System.Drawing.Size(38, 13);
            this._marker1K13.TabIndex = 41;
            this._marker1K13.Text = "К13 = ";
            // 
            // _marker1K12
            // 
            this._marker1K12.AutoSize = true;
            this._marker1K12.Location = new System.Drawing.Point(7, 236);
            this._marker1K12.Name = "_marker1K12";
            this._marker1K12.Size = new System.Drawing.Size(38, 13);
            this._marker1K12.TabIndex = 40;
            this._marker1K12.Text = "К12 = ";
            // 
            // _marker1K11
            // 
            this._marker1K11.AutoSize = true;
            this._marker1K11.Location = new System.Drawing.Point(7, 216);
            this._marker1K11.Name = "_marker1K11";
            this._marker1K11.Size = new System.Drawing.Size(38, 13);
            this._marker1K11.TabIndex = 39;
            this._marker1K11.Text = "К11 = ";
            // 
            // _marker1K10
            // 
            this._marker1K10.AutoSize = true;
            this._marker1K10.Location = new System.Drawing.Point(7, 196);
            this._marker1K10.Name = "_marker1K10";
            this._marker1K10.Size = new System.Drawing.Size(38, 13);
            this._marker1K10.TabIndex = 38;
            this._marker1K10.Text = "К10 = ";
            // 
            // _marker1K9
            // 
            this._marker1K9.AutoSize = true;
            this._marker1K9.Location = new System.Drawing.Point(7, 176);
            this._marker1K9.Name = "_marker1K9";
            this._marker1K9.Size = new System.Drawing.Size(32, 13);
            this._marker1K9.TabIndex = 37;
            this._marker1K9.Text = "К9 = ";
            // 
            // _marker1K8
            // 
            this._marker1K8.AutoSize = true;
            this._marker1K8.Location = new System.Drawing.Point(7, 156);
            this._marker1K8.Name = "_marker1K8";
            this._marker1K8.Size = new System.Drawing.Size(32, 13);
            this._marker1K8.TabIndex = 36;
            this._marker1K8.Text = "К8 = ";
            // 
            // _marker1K7
            // 
            this._marker1K7.AutoSize = true;
            this._marker1K7.Location = new System.Drawing.Point(7, 136);
            this._marker1K7.Name = "_marker1K7";
            this._marker1K7.Size = new System.Drawing.Size(32, 13);
            this._marker1K7.TabIndex = 35;
            this._marker1K7.Text = "К7 = ";
            // 
            // _marker1K6
            // 
            this._marker1K6.AutoSize = true;
            this._marker1K6.Location = new System.Drawing.Point(7, 116);
            this._marker1K6.Name = "_marker1K6";
            this._marker1K6.Size = new System.Drawing.Size(32, 13);
            this._marker1K6.TabIndex = 34;
            this._marker1K6.Text = "К6 = ";
            // 
            // _marker1K5
            // 
            this._marker1K5.AutoSize = true;
            this._marker1K5.Location = new System.Drawing.Point(7, 96);
            this._marker1K5.Name = "_marker1K5";
            this._marker1K5.Size = new System.Drawing.Size(32, 13);
            this._marker1K5.TabIndex = 33;
            this._marker1K5.Text = "К5 = ";
            // 
            // _marker1K4
            // 
            this._marker1K4.AutoSize = true;
            this._marker1K4.Location = new System.Drawing.Point(6, 76);
            this._marker1K4.Name = "_marker1K4";
            this._marker1K4.Size = new System.Drawing.Size(32, 13);
            this._marker1K4.TabIndex = 32;
            this._marker1K4.Text = "К4 = ";
            // 
            // _marker1K3
            // 
            this._marker1K3.AutoSize = true;
            this._marker1K3.Location = new System.Drawing.Point(6, 56);
            this._marker1K3.Name = "_marker1K3";
            this._marker1K3.Size = new System.Drawing.Size(32, 13);
            this._marker1K3.TabIndex = 31;
            this._marker1K3.Text = "К3 = ";
            // 
            // _marker1K2
            // 
            this._marker1K2.AutoSize = true;
            this._marker1K2.Location = new System.Drawing.Point(6, 36);
            this._marker1K2.Name = "_marker1K2";
            this._marker1K2.Size = new System.Drawing.Size(32, 13);
            this._marker1K2.TabIndex = 30;
            this._marker1K2.Text = "К2 = ";
            // 
            // _marker1K1
            // 
            this._marker1K1.AutoSize = true;
            this._marker1K1.Location = new System.Drawing.Point(6, 16);
            this._marker1K1.Name = "_marker1K1";
            this._marker1K1.Size = new System.Drawing.Size(32, 13);
            this._marker1K1.TabIndex = 29;
            this._marker1K1.Text = "К1 = ";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._marker1D40);
            this.groupBox11.Controls.Add(this._marker1D31);
            this.groupBox11.Controls.Add(this._marker1D30);
            this.groupBox11.Controls.Add(this._marker1D29);
            this.groupBox11.Controls.Add(this._marker1D39);
            this.groupBox11.Controls.Add(this._marker1D38);
            this.groupBox11.Controls.Add(this._marker1D37);
            this.groupBox11.Controls.Add(this._marker1D36);
            this.groupBox11.Controls.Add(this._marker1D35);
            this.groupBox11.Controls.Add(this._marker1D34);
            this.groupBox11.Controls.Add(this._marker1D33);
            this.groupBox11.Controls.Add(this._marker1D32);
            this.groupBox11.Controls.Add(this._marker1D28);
            this.groupBox11.Controls.Add(this._marker1D27);
            this.groupBox11.Controls.Add(this._marker1D26);
            this.groupBox11.Controls.Add(this._marker1D25);
            this.groupBox11.Controls.Add(this._marker1D24);
            this.groupBox11.Controls.Add(this._marker1D23);
            this.groupBox11.Controls.Add(this._marker1D22);
            this.groupBox11.Controls.Add(this._marker1D21);
            this.groupBox11.Controls.Add(this._marker1D20);
            this.groupBox11.Controls.Add(this._marker1D11);
            this.groupBox11.Controls.Add(this._marker1D10);
            this.groupBox11.Controls.Add(this._marker1D9);
            this.groupBox11.Controls.Add(this._marker1D19);
            this.groupBox11.Controls.Add(this._marker1D18);
            this.groupBox11.Controls.Add(this._marker1D17);
            this.groupBox11.Controls.Add(this._marker1D16);
            this.groupBox11.Controls.Add(this._marker1D15);
            this.groupBox11.Controls.Add(this._marker1D14);
            this.groupBox11.Controls.Add(this._marker1D13);
            this.groupBox11.Controls.Add(this._marker1D12);
            this.groupBox11.Controls.Add(this._marker1D8);
            this.groupBox11.Controls.Add(this._marker1D7);
            this.groupBox11.Controls.Add(this._marker1D6);
            this.groupBox11.Controls.Add(this._marker1D5);
            this.groupBox11.Controls.Add(this._marker1D4);
            this.groupBox11.Controls.Add(this._marker1D3);
            this.groupBox11.Controls.Add(this._marker1D2);
            this.groupBox11.Controls.Add(this._marker1D1);
            this.groupBox11.Location = new System.Drawing.Point(5, 214);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(119, 421);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дискреты";
            // 
            // _marker1D40
            // 
            this._marker1D40.AutoSize = true;
            this._marker1D40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D40.Location = new System.Drawing.Point(62, 396);
            this._marker1D40.Name = "_marker1D40";
            this._marker1D40.Size = new System.Drawing.Size(40, 13);
            this._marker1D40.TabIndex = 55;
            this._marker1D40.Text = "Д40 = ";
            // 
            // _marker1D31
            // 
            this._marker1D31.AutoSize = true;
            this._marker1D31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D31.Location = new System.Drawing.Point(63, 216);
            this._marker1D31.Name = "_marker1D31";
            this._marker1D31.Size = new System.Drawing.Size(40, 13);
            this._marker1D31.TabIndex = 54;
            this._marker1D31.Text = "Д31 = ";
            // 
            // _marker1D30
            // 
            this._marker1D30.AutoSize = true;
            this._marker1D30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D30.Location = new System.Drawing.Point(63, 196);
            this._marker1D30.Name = "_marker1D30";
            this._marker1D30.Size = new System.Drawing.Size(40, 13);
            this._marker1D30.TabIndex = 53;
            this._marker1D30.Text = "Д30 = ";
            // 
            // _marker1D29
            // 
            this._marker1D29.AutoSize = true;
            this._marker1D29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D29.Location = new System.Drawing.Point(63, 176);
            this._marker1D29.Name = "_marker1D29";
            this._marker1D29.Size = new System.Drawing.Size(40, 13);
            this._marker1D29.TabIndex = 52;
            this._marker1D29.Text = "Д29 = ";
            // 
            // _marker1D39
            // 
            this._marker1D39.AutoSize = true;
            this._marker1D39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D39.Location = new System.Drawing.Point(62, 376);
            this._marker1D39.Name = "_marker1D39";
            this._marker1D39.Size = new System.Drawing.Size(40, 13);
            this._marker1D39.TabIndex = 51;
            this._marker1D39.Text = "Д39 = ";
            // 
            // _marker1D38
            // 
            this._marker1D38.AutoSize = true;
            this._marker1D38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D38.Location = new System.Drawing.Point(62, 356);
            this._marker1D38.Name = "_marker1D38";
            this._marker1D38.Size = new System.Drawing.Size(40, 13);
            this._marker1D38.TabIndex = 50;
            this._marker1D38.Text = "Д38 = ";
            // 
            // _marker1D37
            // 
            this._marker1D37.AutoSize = true;
            this._marker1D37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D37.Location = new System.Drawing.Point(62, 336);
            this._marker1D37.Name = "_marker1D37";
            this._marker1D37.Size = new System.Drawing.Size(40, 13);
            this._marker1D37.TabIndex = 49;
            this._marker1D37.Text = "Д37 = ";
            // 
            // _marker1D36
            // 
            this._marker1D36.AutoSize = true;
            this._marker1D36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D36.Location = new System.Drawing.Point(62, 316);
            this._marker1D36.Name = "_marker1D36";
            this._marker1D36.Size = new System.Drawing.Size(40, 13);
            this._marker1D36.TabIndex = 48;
            this._marker1D36.Text = "Д36 = ";
            // 
            // _marker1D35
            // 
            this._marker1D35.AutoSize = true;
            this._marker1D35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D35.Location = new System.Drawing.Point(62, 296);
            this._marker1D35.Name = "_marker1D35";
            this._marker1D35.Size = new System.Drawing.Size(40, 13);
            this._marker1D35.TabIndex = 47;
            this._marker1D35.Text = "Д35 = ";
            // 
            // _marker1D34
            // 
            this._marker1D34.AutoSize = true;
            this._marker1D34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D34.Location = new System.Drawing.Point(62, 276);
            this._marker1D34.Name = "_marker1D34";
            this._marker1D34.Size = new System.Drawing.Size(40, 13);
            this._marker1D34.TabIndex = 46;
            this._marker1D34.Text = "Д34 = ";
            // 
            // _marker1D33
            // 
            this._marker1D33.AutoSize = true;
            this._marker1D33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D33.Location = new System.Drawing.Point(62, 256);
            this._marker1D33.Name = "_marker1D33";
            this._marker1D33.Size = new System.Drawing.Size(40, 13);
            this._marker1D33.TabIndex = 45;
            this._marker1D33.Text = "Д33 = ";
            // 
            // _marker1D32
            // 
            this._marker1D32.AutoSize = true;
            this._marker1D32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D32.Location = new System.Drawing.Point(62, 236);
            this._marker1D32.Name = "_marker1D32";
            this._marker1D32.Size = new System.Drawing.Size(40, 13);
            this._marker1D32.TabIndex = 44;
            this._marker1D32.Text = "Д32 = ";
            // 
            // _marker1D28
            // 
            this._marker1D28.AutoSize = true;
            this._marker1D28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D28.Location = new System.Drawing.Point(63, 156);
            this._marker1D28.Name = "_marker1D28";
            this._marker1D28.Size = new System.Drawing.Size(40, 13);
            this._marker1D28.TabIndex = 43;
            this._marker1D28.Text = "Д28 = ";
            // 
            // _marker1D27
            // 
            this._marker1D27.AutoSize = true;
            this._marker1D27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D27.Location = new System.Drawing.Point(63, 136);
            this._marker1D27.Name = "_marker1D27";
            this._marker1D27.Size = new System.Drawing.Size(40, 13);
            this._marker1D27.TabIndex = 42;
            this._marker1D27.Text = "Д27 = ";
            // 
            // _marker1D26
            // 
            this._marker1D26.AutoSize = true;
            this._marker1D26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D26.Location = new System.Drawing.Point(63, 116);
            this._marker1D26.Name = "_marker1D26";
            this._marker1D26.Size = new System.Drawing.Size(40, 13);
            this._marker1D26.TabIndex = 41;
            this._marker1D26.Text = "Д26 = ";
            // 
            // _marker1D25
            // 
            this._marker1D25.AutoSize = true;
            this._marker1D25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D25.Location = new System.Drawing.Point(63, 96);
            this._marker1D25.Name = "_marker1D25";
            this._marker1D25.Size = new System.Drawing.Size(40, 13);
            this._marker1D25.TabIndex = 40;
            this._marker1D25.Text = "Д25 = ";
            // 
            // _marker1D24
            // 
            this._marker1D24.AutoSize = true;
            this._marker1D24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D24.Location = new System.Drawing.Point(63, 76);
            this._marker1D24.Name = "_marker1D24";
            this._marker1D24.Size = new System.Drawing.Size(40, 13);
            this._marker1D24.TabIndex = 39;
            this._marker1D24.Text = "Д24 = ";
            // 
            // _marker1D23
            // 
            this._marker1D23.AutoSize = true;
            this._marker1D23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D23.Location = new System.Drawing.Point(63, 56);
            this._marker1D23.Name = "_marker1D23";
            this._marker1D23.Size = new System.Drawing.Size(40, 13);
            this._marker1D23.TabIndex = 38;
            this._marker1D23.Text = "Д23 = ";
            // 
            // _marker1D22
            // 
            this._marker1D22.AutoSize = true;
            this._marker1D22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D22.Location = new System.Drawing.Point(62, 36);
            this._marker1D22.Name = "_marker1D22";
            this._marker1D22.Size = new System.Drawing.Size(40, 13);
            this._marker1D22.TabIndex = 37;
            this._marker1D22.Text = "Д22 = ";
            // 
            // _marker1D21
            // 
            this._marker1D21.AutoSize = true;
            this._marker1D21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D21.Location = new System.Drawing.Point(62, 16);
            this._marker1D21.Name = "_marker1D21";
            this._marker1D21.Size = new System.Drawing.Size(40, 13);
            this._marker1D21.TabIndex = 36;
            this._marker1D21.Text = "Д21 = ";
            // 
            // _marker1D20
            // 
            this._marker1D20.AutoSize = true;
            this._marker1D20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D20.Location = new System.Drawing.Point(5, 396);
            this._marker1D20.Name = "_marker1D20";
            this._marker1D20.Size = new System.Drawing.Size(40, 13);
            this._marker1D20.TabIndex = 35;
            this._marker1D20.Text = "Д20 = ";
            // 
            // _marker1D11
            // 
            this._marker1D11.AutoSize = true;
            this._marker1D11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D11.Location = new System.Drawing.Point(6, 216);
            this._marker1D11.Name = "_marker1D11";
            this._marker1D11.Size = new System.Drawing.Size(40, 13);
            this._marker1D11.TabIndex = 34;
            this._marker1D11.Text = "Д11 = ";
            // 
            // _marker1D10
            // 
            this._marker1D10.AutoSize = true;
            this._marker1D10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D10.Location = new System.Drawing.Point(6, 196);
            this._marker1D10.Name = "_marker1D10";
            this._marker1D10.Size = new System.Drawing.Size(40, 13);
            this._marker1D10.TabIndex = 33;
            this._marker1D10.Text = "Д10 = ";
            // 
            // _marker1D9
            // 
            this._marker1D9.AutoSize = true;
            this._marker1D9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D9.Location = new System.Drawing.Point(6, 176);
            this._marker1D9.Name = "_marker1D9";
            this._marker1D9.Size = new System.Drawing.Size(34, 13);
            this._marker1D9.TabIndex = 32;
            this._marker1D9.Text = "Д9 = ";
            // 
            // _marker1D19
            // 
            this._marker1D19.AutoSize = true;
            this._marker1D19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D19.Location = new System.Drawing.Point(5, 376);
            this._marker1D19.Name = "_marker1D19";
            this._marker1D19.Size = new System.Drawing.Size(40, 13);
            this._marker1D19.TabIndex = 31;
            this._marker1D19.Text = "Д19 = ";
            // 
            // _marker1D18
            // 
            this._marker1D18.AutoSize = true;
            this._marker1D18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D18.Location = new System.Drawing.Point(5, 356);
            this._marker1D18.Name = "_marker1D18";
            this._marker1D18.Size = new System.Drawing.Size(40, 13);
            this._marker1D18.TabIndex = 30;
            this._marker1D18.Text = "Д18 = ";
            // 
            // _marker1D17
            // 
            this._marker1D17.AutoSize = true;
            this._marker1D17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D17.Location = new System.Drawing.Point(5, 336);
            this._marker1D17.Name = "_marker1D17";
            this._marker1D17.Size = new System.Drawing.Size(40, 13);
            this._marker1D17.TabIndex = 29;
            this._marker1D17.Text = "Д17 = ";
            // 
            // _marker1D16
            // 
            this._marker1D16.AutoSize = true;
            this._marker1D16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D16.Location = new System.Drawing.Point(5, 316);
            this._marker1D16.Name = "_marker1D16";
            this._marker1D16.Size = new System.Drawing.Size(40, 13);
            this._marker1D16.TabIndex = 28;
            this._marker1D16.Text = "Д16 = ";
            // 
            // _marker1D15
            // 
            this._marker1D15.AutoSize = true;
            this._marker1D15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D15.Location = new System.Drawing.Point(5, 296);
            this._marker1D15.Name = "_marker1D15";
            this._marker1D15.Size = new System.Drawing.Size(40, 13);
            this._marker1D15.TabIndex = 27;
            this._marker1D15.Text = "Д15 = ";
            // 
            // _marker1D14
            // 
            this._marker1D14.AutoSize = true;
            this._marker1D14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D14.Location = new System.Drawing.Point(5, 276);
            this._marker1D14.Name = "_marker1D14";
            this._marker1D14.Size = new System.Drawing.Size(40, 13);
            this._marker1D14.TabIndex = 26;
            this._marker1D14.Text = "Д14 = ";
            // 
            // _marker1D13
            // 
            this._marker1D13.AutoSize = true;
            this._marker1D13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D13.Location = new System.Drawing.Point(5, 256);
            this._marker1D13.Name = "_marker1D13";
            this._marker1D13.Size = new System.Drawing.Size(40, 13);
            this._marker1D13.TabIndex = 25;
            this._marker1D13.Text = "Д13 = ";
            // 
            // _marker1D12
            // 
            this._marker1D12.AutoSize = true;
            this._marker1D12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D12.Location = new System.Drawing.Point(5, 236);
            this._marker1D12.Name = "_marker1D12";
            this._marker1D12.Size = new System.Drawing.Size(40, 13);
            this._marker1D12.TabIndex = 24;
            this._marker1D12.Text = "Д12 = ";
            // 
            // _marker1D8
            // 
            this._marker1D8.AutoSize = true;
            this._marker1D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D8.Location = new System.Drawing.Point(6, 156);
            this._marker1D8.Name = "_marker1D8";
            this._marker1D8.Size = new System.Drawing.Size(34, 13);
            this._marker1D8.TabIndex = 23;
            this._marker1D8.Text = "Д8 = ";
            // 
            // _marker1D7
            // 
            this._marker1D7.AutoSize = true;
            this._marker1D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D7.Location = new System.Drawing.Point(6, 136);
            this._marker1D7.Name = "_marker1D7";
            this._marker1D7.Size = new System.Drawing.Size(34, 13);
            this._marker1D7.TabIndex = 22;
            this._marker1D7.Text = "Д7 = ";
            // 
            // _marker1D6
            // 
            this._marker1D6.AutoSize = true;
            this._marker1D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D6.Location = new System.Drawing.Point(6, 116);
            this._marker1D6.Name = "_marker1D6";
            this._marker1D6.Size = new System.Drawing.Size(34, 13);
            this._marker1D6.TabIndex = 21;
            this._marker1D6.Text = "Д6 = ";
            // 
            // _marker1D5
            // 
            this._marker1D5.AutoSize = true;
            this._marker1D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D5.Location = new System.Drawing.Point(6, 96);
            this._marker1D5.Name = "_marker1D5";
            this._marker1D5.Size = new System.Drawing.Size(34, 13);
            this._marker1D5.TabIndex = 20;
            this._marker1D5.Text = "Д5 = ";
            // 
            // _marker1D4
            // 
            this._marker1D4.AutoSize = true;
            this._marker1D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D4.Location = new System.Drawing.Point(6, 76);
            this._marker1D4.Name = "_marker1D4";
            this._marker1D4.Size = new System.Drawing.Size(34, 13);
            this._marker1D4.TabIndex = 19;
            this._marker1D4.Text = "Д4 = ";
            // 
            // _marker1D3
            // 
            this._marker1D3.AutoSize = true;
            this._marker1D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D3.Location = new System.Drawing.Point(6, 56);
            this._marker1D3.Name = "_marker1D3";
            this._marker1D3.Size = new System.Drawing.Size(34, 13);
            this._marker1D3.TabIndex = 18;
            this._marker1D3.Text = "Д3 = ";
            // 
            // _marker1D2
            // 
            this._marker1D2.AutoSize = true;
            this._marker1D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D2.Location = new System.Drawing.Point(6, 36);
            this._marker1D2.Name = "_marker1D2";
            this._marker1D2.Size = new System.Drawing.Size(34, 13);
            this._marker1D2.TabIndex = 17;
            this._marker1D2.Text = "Д2 = ";
            // 
            // _marker1D1
            // 
            this._marker1D1.AutoSize = true;
            this._marker1D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D1.Location = new System.Drawing.Point(6, 16);
            this._marker1D1.Name = "_marker1D1";
            this._marker1D1.Size = new System.Drawing.Size(34, 13);
            this._marker1D1.TabIndex = 16;
            this._marker1D1.Text = "Д1 = ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._marker1I4);
            this.groupBox8.Controls.Add(this._marker1I3);
            this.groupBox8.Controls.Add(this._marker1I2);
            this.groupBox8.Controls.Add(this._marker1I1);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(119, 95);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Токи";
            // 
            // _marker1I4
            // 
            this._marker1I4.AutoSize = true;
            this._marker1I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I4.Location = new System.Drawing.Point(6, 78);
            this._marker1I4.Name = "_marker1I4";
            this._marker1I4.Size = new System.Drawing.Size(28, 13);
            this._marker1I4.TabIndex = 3;
            this._marker1I4.Text = "In = ";
            // 
            // _marker1I3
            // 
            this._marker1I3.AutoSize = true;
            this._marker1I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I3.Location = new System.Drawing.Point(6, 58);
            this._marker1I3.Name = "_marker1I3";
            this._marker1I3.Size = new System.Drawing.Size(28, 13);
            this._marker1I3.TabIndex = 2;
            this._marker1I3.Text = "Ic = ";
            // 
            // _marker1I2
            // 
            this._marker1I2.AutoSize = true;
            this._marker1I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I2.Location = new System.Drawing.Point(6, 38);
            this._marker1I2.Name = "_marker1I2";
            this._marker1I2.Size = new System.Drawing.Size(28, 13);
            this._marker1I2.TabIndex = 1;
            this._marker1I2.Text = "Ib = ";
            // 
            // _marker1I1
            // 
            this._marker1I1.AutoSize = true;
            this._marker1I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I1.Location = new System.Drawing.Point(6, 18);
            this._marker1I1.Name = "_marker1I1";
            this._marker1I1.Size = new System.Drawing.Size(28, 13);
            this._marker1I1.TabIndex = 0;
            this._marker1I1.Text = "Ia = ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._deltaTimeBox);
            this.groupBox4.Controls.Add(this._marker2TimeBox);
            this.groupBox4.Controls.Add(this._marker1TimeBox);
            this.groupBox4.Location = new System.Drawing.Point(3, 921);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(814, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Время";
            // 
            // _deltaTimeBox
            // 
            this._deltaTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._deltaTimeBox.Controls.Add(this._markerTimeDelta);
            this._deltaTimeBox.Location = new System.Drawing.Point(6, 46);
            this._deltaTimeBox.Name = "_deltaTimeBox";
            this._deltaTimeBox.Size = new System.Drawing.Size(266, 33);
            this._deltaTimeBox.TabIndex = 2;
            this._deltaTimeBox.TabStop = false;
            this._deltaTimeBox.Text = "Дельта";
            // 
            // _markerTimeDelta
            // 
            this._markerTimeDelta.AutoSize = true;
            this._markerTimeDelta.Location = new System.Drawing.Point(118, 16);
            this._markerTimeDelta.Name = "_markerTimeDelta";
            this._markerTimeDelta.Size = new System.Drawing.Size(30, 13);
            this._markerTimeDelta.TabIndex = 2;
            this._markerTimeDelta.Text = "0 мс";
            // 
            // _marker2TimeBox
            // 
            this._marker2TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker2TimeBox.Controls.Add(this._marker2Time);
            this._marker2TimeBox.Location = new System.Drawing.Point(144, 13);
            this._marker2TimeBox.Name = "_marker2TimeBox";
            this._marker2TimeBox.Size = new System.Drawing.Size(119, 33);
            this._marker2TimeBox.TabIndex = 1;
            this._marker2TimeBox.TabStop = false;
            this._marker2TimeBox.Text = "Маркер 2";
            // 
            // _marker2Time
            // 
            this._marker2Time.AutoSize = true;
            this._marker2Time.Location = new System.Drawing.Point(41, 16);
            this._marker2Time.Name = "_marker2Time";
            this._marker2Time.Size = new System.Drawing.Size(30, 13);
            this._marker2Time.TabIndex = 1;
            this._marker2Time.Text = "0 мс";
            // 
            // _marker1TimeBox
            // 
            this._marker1TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker1TimeBox.Controls.Add(this._marker1Time);
            this._marker1TimeBox.Location = new System.Drawing.Point(6, 13);
            this._marker1TimeBox.Name = "_marker1TimeBox";
            this._marker1TimeBox.Size = new System.Drawing.Size(118, 33);
            this._marker1TimeBox.TabIndex = 0;
            this._marker1TimeBox.TabStop = false;
            this._marker1TimeBox.Text = "Маркер 1";
            // 
            // _marker1Time
            // 
            this._marker1Time.AutoSize = true;
            this._marker1Time.Location = new System.Drawing.Point(39, 16);
            this._marker1Time.Name = "_marker1Time";
            this._marker1Time.Size = new System.Drawing.Size(30, 13);
            this._marker1Time.TabIndex = 0;
            this._marker1Time.Text = "0 мс";
            // 
            // _xDecreaseButton
            // 
            this._xDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xDecreaseButton.Location = new System.Drawing.Point(148, 3);
            this._xDecreaseButton.Name = "_xDecreaseButton";
            this._xDecreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xDecreaseButton.TabIndex = 3;
            this._xDecreaseButton.Text = "X -";
            this._xDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentСheckBox
            // 
            this._currentСheckBox.AutoSize = true;
            this._currentСheckBox.BackColor = System.Drawing.Color.Silver;
            this._currentСheckBox.Checked = true;
            this._currentСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._currentСheckBox.Location = new System.Drawing.Point(195, 3);
            this._currentСheckBox.Name = "_currentСheckBox";
            this._currentСheckBox.Size = new System.Drawing.Size(51, 17);
            this._currentСheckBox.TabIndex = 4;
            this._currentСheckBox.Text = "Токи";
            this._currentСheckBox.UseVisualStyleBackColor = false;
            this._currentСheckBox.CheckedChanged += new System.EventHandler(this._currentСonnectionsСheckBox_CheckedChanged);
            // 
            // _discrestsСheckBox
            // 
            this._discrestsСheckBox.AutoSize = true;
            this._discrestsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._discrestsСheckBox.Checked = true;
            this._discrestsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._discrestsСheckBox.Location = new System.Drawing.Point(361, 3);
            this._discrestsСheckBox.Name = "_discrestsСheckBox";
            this._discrestsСheckBox.Size = new System.Drawing.Size(78, 17);
            this._discrestsСheckBox.TabIndex = 5;
            this._discrestsСheckBox.Text = "Дискреты";
            this._discrestsСheckBox.UseVisualStyleBackColor = false;
            this._discrestsСheckBox.CheckedChanged += new System.EventHandler(this._discrestsСheckBox_CheckedChanged);
            // 
            // _channelsСheckBox
            // 
            this._channelsСheckBox.AutoSize = true;
            this._channelsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._channelsСheckBox.Checked = true;
            this._channelsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._channelsСheckBox.Location = new System.Drawing.Point(451, 3);
            this._channelsСheckBox.Name = "_channelsСheckBox";
            this._channelsСheckBox.Size = new System.Drawing.Size(65, 17);
            this._channelsСheckBox.TabIndex = 6;
            this._channelsСheckBox.Text = "Каналы";
            this._channelsСheckBox.UseVisualStyleBackColor = false;
            this._channelsСheckBox.CheckedChanged += new System.EventHandler(this._channelsСheckBox_CheckedChanged);
            // 
            // _markCheckBox
            // 
            this._markCheckBox.AutoSize = true;
            this._markCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markCheckBox.Location = new System.Drawing.Point(691, 3);
            this._markCheckBox.Name = "_markCheckBox";
            this._markCheckBox.Size = new System.Drawing.Size(58, 17);
            this._markCheckBox.TabIndex = 7;
            this._markCheckBox.Text = "Метки";
            this._markCheckBox.UseVisualStyleBackColor = false;
            // 
            // _markerCheckBox
            // 
            this._markerCheckBox.AutoSize = true;
            this._markerCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markerCheckBox.Location = new System.Drawing.Point(761, 3);
            this._markerCheckBox.Name = "_markerCheckBox";
            this._markerCheckBox.Size = new System.Drawing.Size(73, 17);
            this._markerCheckBox.TabIndex = 8;
            this._markerCheckBox.Text = "Маркеры";
            this._markerCheckBox.UseVisualStyleBackColor = false;
            this._markerCheckBox.CheckedChanged += new System.EventHandler(this._markerCheckBox_CheckedChanged);
            // 
            // _xIncreaseButton
            // 
            this._xIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xIncreaseButton.Location = new System.Drawing.Point(108, 3);
            this._xIncreaseButton.Name = "_xIncreaseButton";
            this._xIncreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xIncreaseButton.TabIndex = 2;
            this._xIncreaseButton.Text = "X +";
            this._xIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // _oscRunСheckBox
            // 
            this._oscRunСheckBox.AutoSize = true;
            this._oscRunСheckBox.BackColor = System.Drawing.Color.Silver;
            this._oscRunСheckBox.Location = new System.Drawing.Point(521, 3);
            this._oscRunСheckBox.Name = "_oscRunСheckBox";
            this._oscRunСheckBox.Size = new System.Drawing.Size(127, 17);
            this._oscRunСheckBox.TabIndex = 9;
            this._oscRunСheckBox.Text = "Пуск осциллографа";
            this._oscRunСheckBox.UseVisualStyleBackColor = false;
            this._oscRunСheckBox.CheckedChanged += new System.EventHandler(this._oscRunСheckBox_CheckedChanged);
            // 
            // _voltageСheckBox
            // 
            this._voltageСheckBox.AutoSize = true;
            this._voltageСheckBox.BackColor = System.Drawing.Color.Silver;
            this._voltageСheckBox.Checked = true;
            this._voltageСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._voltageСheckBox.Location = new System.Drawing.Point(256, 3);
            this._voltageСheckBox.Name = "_voltageСheckBox";
            this._voltageСheckBox.Size = new System.Drawing.Size(90, 17);
            this._voltageСheckBox.TabIndex = 10;
            this._voltageСheckBox.Text = "Напряжения";
            this._voltageСheckBox.UseVisualStyleBackColor = false;
            this._voltageСheckBox.CheckedChanged += new System.EventHandler(this._voltageСheckBox_CheckedChanged);
            // 
            // Mr731OscilloscopeResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 753);
            this.Controls.Add(this._voltageСheckBox);
            this.Controls.Add(this._oscRunСheckBox);
            this.Controls.Add(this._markerCheckBox);
            this.Controls.Add(this._markCheckBox);
            this.Controls.Add(this._channelsСheckBox);
            this.Controls.Add(this._discrestsСheckBox);
            this.Controls.Add(this._currentСheckBox);
            this.Controls.Add(this._xDecreaseButton);
            this.Controls.Add(this._xIncreaseButton);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "Mr761OscilloscopeResultForm";
            this.Text = "Mr901OscilloscopeResultForm";
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this._channelLayoutPanel.ResumeLayout(false);
            this._channelLayoutPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this._discretsLayoutPanel.ResumeLayout(false);
            this._discretsLayoutPanel.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this._voltageLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this._currentsLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._markerScrollPanel.ResumeLayout(false);
            this._marker2Box.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this._marker1Box.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this._deltaTimeBox.ResumeLayout(false);
            this._deltaTimeBox.PerformLayout();
            this._marker2TimeBox.ResumeLayout(false);
            this._marker2TimeBox.PerformLayout();
            this._marker1TimeBox.ResumeLayout(false);
            this._marker1TimeBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.TableLayoutPanel _channelLayoutPanel;
        private System.Windows.Forms.Button _channel1Button;
        private System.Windows.Forms.Button _channel2Button;
        private System.Windows.Forms.Button _channel3Button;
        private System.Windows.Forms.Button _channel4Button;
        private System.Windows.Forms.Button _channel5Button;
        private System.Windows.Forms.Button _channel6Button;
        private System.Windows.Forms.Button _channel7Button;
        private System.Windows.Forms.Button _channel8Button;
        private BEMN_XY_Chart.DAS_Net_XYChart _channelsChart;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TableLayoutPanel _discretsLayoutPanel;
        private BEMN_XY_Chart.DAS_Net_XYChart _discrestsChart;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TableLayoutPanel _voltageLayoutPanel;
        private System.Windows.Forms.VScrollBar _uScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _voltageChartDecreaseButton;
        private System.Windows.Forms.Button _voltageChartIncreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button _u3Button;
        private System.Windows.Forms.Button _u4Button;
        private System.Windows.Forms.Button _u1Button;
        private System.Windows.Forms.Button _u2Button;
        private System.Windows.Forms.Label label5;
        private BEMN_XY_Chart.DAS_Net_XYChart _uChart;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.TrackBar _marker1TrackBar;
        private System.Windows.Forms.TrackBar _marker2TrackBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox _deltaTimeBox;
        private System.Windows.Forms.GroupBox _marker2TimeBox;
        private System.Windows.Forms.GroupBox _marker1TimeBox;
        private System.Windows.Forms.GroupBox _marker1Box;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button _discrete5Button;
        private System.Windows.Forms.Button _discrete6Button;
        private System.Windows.Forms.Button _discrete7Button;
        private System.Windows.Forms.Button _discrete8Button;
        private System.Windows.Forms.Button _discrete9Button;
        private System.Windows.Forms.Button _discrete10Button;
        private System.Windows.Forms.Button _discrete11Button;
        private System.Windows.Forms.Button _discrete12Button;
        private System.Windows.Forms.Button _discrete13Button;
        private System.Windows.Forms.Button _discrete14Button;
        private System.Windows.Forms.Button _discrete15Button;
        private System.Windows.Forms.Button _discrete16Button;
        private System.Windows.Forms.Button _discrete17Button;
        private System.Windows.Forms.Button _discrete18Button;
        private System.Windows.Forms.Button _discrete19Button;
        private System.Windows.Forms.Button _discrete20Button;
        private System.Windows.Forms.Button _discrete21Button;
        private System.Windows.Forms.Button _discrete22Button;
        private System.Windows.Forms.Button _discrete23Button;
        private System.Windows.Forms.Button _discrete24Button;
        private System.Windows.Forms.Button _discrete1Button;
        private System.Windows.Forms.Button _discrete2Button;
        private System.Windows.Forms.Button _discrete3Button;
        private System.Windows.Forms.Button _discrete4Button;
        private System.Windows.Forms.Button _xDecreaseButton;
        private System.Windows.Forms.CheckBox _currentСheckBox;
        private System.Windows.Forms.CheckBox _discrestsСheckBox;
        private System.Windows.Forms.CheckBox _channelsСheckBox;
        private System.Windows.Forms.CheckBox _markCheckBox;
        private System.Windows.Forms.CheckBox _markerCheckBox;
        private System.Windows.Forms.ToolTip _newMarkToolTip;
        private System.Windows.Forms.Label _marker1I4;
        private System.Windows.Forms.Label _marker1I3;
        private System.Windows.Forms.Label _marker1I2;
        private System.Windows.Forms.Label _marker1I1;
        private System.Windows.Forms.Label _marker1D22;
        private System.Windows.Forms.Label _marker1D21;
        private System.Windows.Forms.Label _marker1D20;
        private System.Windows.Forms.Label _marker1D11;
        private System.Windows.Forms.Label _marker1D10;
        private System.Windows.Forms.Label _marker1D9;
        private System.Windows.Forms.Label _marker1D19;
        private System.Windows.Forms.Label _marker1D18;
        private System.Windows.Forms.Label _marker1D17;
        private System.Windows.Forms.Label _marker1D16;
        private System.Windows.Forms.Label _marker1D15;
        private System.Windows.Forms.Label _marker1D14;
        private System.Windows.Forms.Label _marker1D13;
        private System.Windows.Forms.Label _marker1D12;
        private System.Windows.Forms.Label _marker1D8;
        private System.Windows.Forms.Label _marker1D7;
        private System.Windows.Forms.Label _marker1D6;
        private System.Windows.Forms.Label _marker1D5;
        private System.Windows.Forms.Label _marker1D4;
        private System.Windows.Forms.Label _marker1D3;
        private System.Windows.Forms.Label _marker1D2;
        private System.Windows.Forms.Label _marker1D1;
        private System.Windows.Forms.Label _marker1D24;
        private System.Windows.Forms.Label _marker1D23;
        private System.Windows.Forms.Label _marker1K8;
        private System.Windows.Forms.Label _marker1K7;
        private System.Windows.Forms.Label _marker1K6;
        private System.Windows.Forms.Label _marker1K5;
        private System.Windows.Forms.Label _marker1K4;
        private System.Windows.Forms.Label _marker1K3;
        private System.Windows.Forms.Label _marker1K2;
        private System.Windows.Forms.Label _marker1K1;
        private System.Windows.Forms.Panel _markerScrollPanel;
        private System.Windows.Forms.GroupBox _marker2Box;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label _marker2I4;
        private System.Windows.Forms.Label _marker2I3;
        private System.Windows.Forms.Label _marker2I2;
        private System.Windows.Forms.Label _marker2I1;
        private System.Windows.Forms.Button _xIncreaseButton;
        private System.Windows.Forms.Label _markerTimeDelta;
        private System.Windows.Forms.Label _marker2Time;
        private System.Windows.Forms.Label _marker1Time;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private BEMN_XY_Chart.DAS_Net_XYChart _measuringChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.CheckBox _oscRunСheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.TableLayoutPanel _currentsLayoutPanel;
        private System.Windows.Forms.VScrollBar _iScroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button _i3Button;
        private System.Windows.Forms.Button _i4Button;
        private System.Windows.Forms.Button _i1Button;
        private System.Windows.Forms.Button _i2Button;
        private System.Windows.Forms.Label label7;
        private BEMN_XY_Chart.DAS_Net_XYChart _iChart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button _currentChartDecreaseButton;
        private System.Windows.Forms.Button _currentChartIncreaseButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button _discrete25Button;
        private System.Windows.Forms.Button _discrete26Button;
        private System.Windows.Forms.Button _discrete27Button;
        private System.Windows.Forms.Button _discrete28Button;
        private System.Windows.Forms.Button _discrete29Button;
        private System.Windows.Forms.Button _discrete30Button;
        private System.Windows.Forms.Button _discrete31Button;
        private System.Windows.Forms.Button _discrete32Button;
        private System.Windows.Forms.Button _discrete33Button;
        private System.Windows.Forms.Button _discrete34Button;
        private System.Windows.Forms.Button _discrete35Button;
        private System.Windows.Forms.Button _discrete36Button;
        private System.Windows.Forms.Button _discrete37Button;
        private System.Windows.Forms.Button _discrete38Button;
        private System.Windows.Forms.Button _discrete39Button;
        private System.Windows.Forms.Button _discrete40Button;
        private System.Windows.Forms.Button _channel9Button;
        private System.Windows.Forms.Button _channel10Button;
        private System.Windows.Forms.Button _channel11Button;
        private System.Windows.Forms.Button _channel12Button;
        private System.Windows.Forms.Button _channel13Button;
        private System.Windows.Forms.Button _channel14Button;
        private System.Windows.Forms.Button _channel15Button;
        private System.Windows.Forms.Button _channel16Button;
        private System.Windows.Forms.Button _channel17Button;
        private System.Windows.Forms.Button _channel18Button;
        private System.Windows.Forms.Button _channel19Button;
        private System.Windows.Forms.Button _channel20Button;
        private System.Windows.Forms.Button _channel21Button;
        private System.Windows.Forms.Button _channel22Button;
        private System.Windows.Forms.Button _channel23Button;
        private System.Windows.Forms.Button _channel24Button;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label _marker2D40;
        private System.Windows.Forms.Label _marker2D31;
        private System.Windows.Forms.Label _marker2D30;
        private System.Windows.Forms.Label _marker2D29;
        private System.Windows.Forms.Label _marker2D39;
        private System.Windows.Forms.Label _marker2D38;
        private System.Windows.Forms.Label _marker2D37;
        private System.Windows.Forms.Label _marker2D36;
        private System.Windows.Forms.Label _marker2D35;
        private System.Windows.Forms.Label _marker2D34;
        private System.Windows.Forms.Label _marker2D33;
        private System.Windows.Forms.Label _marker2D32;
        private System.Windows.Forms.Label _marker2D28;
        private System.Windows.Forms.Label _marker2D27;
        private System.Windows.Forms.Label _marker2D26;
        private System.Windows.Forms.Label _marker2D25;
        private System.Windows.Forms.Label _marker2D24;
        private System.Windows.Forms.Label _marker2D23;
        private System.Windows.Forms.Label _marker2D22;
        private System.Windows.Forms.Label _marker2D21;
        private System.Windows.Forms.Label _marker2D20;
        private System.Windows.Forms.Label _marker2D11;
        private System.Windows.Forms.Label _marker2D10;
        private System.Windows.Forms.Label _marker2D9;
        private System.Windows.Forms.Label _marker2D19;
        private System.Windows.Forms.Label _marker2D18;
        private System.Windows.Forms.Label _marker2D17;
        private System.Windows.Forms.Label _marker2D16;
        private System.Windows.Forms.Label _marker2D15;
        private System.Windows.Forms.Label _marker2D14;
        private System.Windows.Forms.Label _marker2D13;
        private System.Windows.Forms.Label _marker2D12;
        private System.Windows.Forms.Label _marker2D8;
        private System.Windows.Forms.Label _marker2D7;
        private System.Windows.Forms.Label _marker2D6;
        private System.Windows.Forms.Label _marker2D5;
        private System.Windows.Forms.Label _marker2D4;
        private System.Windows.Forms.Label _marker2D3;
        private System.Windows.Forms.Label _marker2D2;
        private System.Windows.Forms.Label _marker2D1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label _marker2U4;
        private System.Windows.Forms.Label _marker2U3;
        private System.Windows.Forms.Label _marker2U2;
        private System.Windows.Forms.Label _marker2U1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label _marker1U4;
        private System.Windows.Forms.Label _marker1U3;
        private System.Windows.Forms.Label _marker1U2;
        private System.Windows.Forms.Label _marker1U1;
        private System.Windows.Forms.Label _marker1D40;
        private System.Windows.Forms.Label _marker1D31;
        private System.Windows.Forms.Label _marker1D30;
        private System.Windows.Forms.Label _marker1D29;
        private System.Windows.Forms.Label _marker1D39;
        private System.Windows.Forms.Label _marker1D38;
        private System.Windows.Forms.Label _marker1D37;
        private System.Windows.Forms.Label _marker1D36;
        private System.Windows.Forms.Label _marker1D35;
        private System.Windows.Forms.Label _marker1D34;
        private System.Windows.Forms.Label _marker1D33;
        private System.Windows.Forms.Label _marker1D32;
        private System.Windows.Forms.Label _marker1D28;
        private System.Windows.Forms.Label _marker1D27;
        private System.Windows.Forms.Label _marker1D26;
        private System.Windows.Forms.Label _marker1D25;
        private System.Windows.Forms.Label _marker1K24;
        private System.Windows.Forms.Label _marker1K23;
        private System.Windows.Forms.Label _marker1K22;
        private System.Windows.Forms.Label _marker1K21;
        private System.Windows.Forms.Label _marker1K20;
        private System.Windows.Forms.Label _marker1K19;
        private System.Windows.Forms.Label _marker1K18;
        private System.Windows.Forms.Label _marker1K17;
        private System.Windows.Forms.Label _marker1K16;
        private System.Windows.Forms.Label _marker1K15;
        private System.Windows.Forms.Label _marker1K14;
        private System.Windows.Forms.Label _marker1K13;
        private System.Windows.Forms.Label _marker1K12;
        private System.Windows.Forms.Label _marker1K11;
        private System.Windows.Forms.Label _marker1K10;
        private System.Windows.Forms.Label _marker1K9;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label _marker2K24;
        private System.Windows.Forms.Label _marker2K23;
        private System.Windows.Forms.Label _marker2K22;
        private System.Windows.Forms.Label _marker2K21;
        private System.Windows.Forms.Label _marker2K20;
        private System.Windows.Forms.Label _marker2K19;
        private System.Windows.Forms.Label _marker2K18;
        private System.Windows.Forms.Label _marker2K17;
        private System.Windows.Forms.Label _marker2K16;
        private System.Windows.Forms.Label _marker2K15;
        private System.Windows.Forms.Label _marker2K14;
        private System.Windows.Forms.Label _marker2K13;
        private System.Windows.Forms.Label _marker2K12;
        private System.Windows.Forms.Label _marker2K11;
        private System.Windows.Forms.Label _marker2K10;
        private System.Windows.Forms.Label _marker2K9;
        private System.Windows.Forms.Label _marker2K8;
        private System.Windows.Forms.Label _marker2K7;
        private System.Windows.Forms.Label _marker2K6;
        private System.Windows.Forms.Label _marker2K5;
        private System.Windows.Forms.Label _marker2K4;
        private System.Windows.Forms.Label _marker2K3;
        private System.Windows.Forms.Label _marker2K2;
        private System.Windows.Forms.Label _marker2K1;
        private System.Windows.Forms.CheckBox _voltageСheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;



    }
}