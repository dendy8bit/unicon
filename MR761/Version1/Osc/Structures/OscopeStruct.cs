﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761.Version1.Osc.Structures
{
    /// <summary>
    /// Конфигурациия осцилографа
    /// </summary>
    public class OscopeStruct : StructBase
    {
        public const int KANAL_COUNT = 24;

        [Layout(0)] private ushort _config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort _size; //размер осциллограмы
        [Layout(2)] private ushort _percent; //процент от размера осциллограммы
        [Layout(3, Count = KANAL_COUNT)] private ushort[] _kanal;//конфигурация канала осциллографирования
        
        public ushort[] ChannelsInWords
        {
            get {return this._kanal; }
        }
    }
}
