﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR761.Version1.Measuring.Structures
{
    /// <summary>
    /// Конфигурациия измерительного трансформатора
    /// </summary>
    public class KanalTransStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)] private ushort _ittx;//конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)] private ushort _ittx1; //конфигурация - номинальный первичный ток In1, конфигурация - коэфициэнт Un1
        [Layout(3)] private ushort _polarityL; //резерв, вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX;//резерв, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
        [Layout(6)] private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)] private ushort _rez; //резерв 
        #endregion [Private fields]

        #region [Properties]
        
        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        public ushort Ittl
        {
            get { return _ittl; }
            set { _ittl = value; }
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        public ushort Ittx
        {
            get { return _ittx; }
            set { _ittx = value; }
        }
        
        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        public double KthlValue
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this.Ittl);
                return Common.GetBit(this.Ittl, 15)
                           ? value * 1000
                           : value;
            }
        }
        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        public double KthxValue
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this.Ittx);
                return Common.GetBit(this.Ittx, 15)
                           ? value * 1000
                           : value;
            }
        }
        #endregion [Properties]
    }
}
