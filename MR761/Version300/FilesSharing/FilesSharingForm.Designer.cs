﻿namespace BEMN.MR761.Version300.FilesSharing
{
    partial class FilesSharingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileSharingControl = new BEMN.Forms.FileSharingControl();
            this.SuspendLayout();
            // 
            // fileSharingControl
            // 
            this.fileSharingControl.Location = new System.Drawing.Point(4, 3);
            this.fileSharingControl.Name = "fileSharingControl";
            this.fileSharingControl.Size = new System.Drawing.Size(454, 330);
            this.fileSharingControl.TabIndex = 0;
            // 
            // FilesSharingForm
            // 
            this.ClientSize = new System.Drawing.Size(462, 337);
            this.Controls.Add(this.fileSharingControl);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(478, 376);
            this.MinimumSize = new System.Drawing.Size(478, 376);
            this.Name = "FilesSharingForm";
            this.Text = "Работа с файлами";
            this.ResumeLayout(false);

        }

        #endregion

        private Forms.FileSharingControl fileSharingControl;
    }
}