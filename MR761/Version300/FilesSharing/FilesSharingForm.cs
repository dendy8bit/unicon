﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR761.Version300.FilesSharing
{
    public partial class FilesSharingForm : Form, IFormView
    {
        private MR761 _device;

        public FilesSharingForm()
        {
            this.InitializeComponent();
        }

        public FilesSharingForm(MR761 device)
        {
            this.InitializeComponent();

            _device = device;
        }

        #region IFormView

        public Type ClassType => typeof(FilesSharingForm);
        public bool ForceShow => false;
        public Image NodeImage => Resources.filecervice;
        public string NodeName => "Работа с файлами";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;
        public Type FormDevice => typeof(MR761);
        public bool Multishow { get; private set; }

        #endregion
    }
}
