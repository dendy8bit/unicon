﻿namespace BEMN.MR761.Version300.SystemJournal
{
    partial class Mr761SystemJournalFormV3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._systemJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._readJournalButton = new System.Windows.Forms.Button();
            this._loadJournalButton = new System.Windows.Forms.Button();
            this._saveJournalButton = new System.Windows.Forms.Button();
            this._saveJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this._openJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._exportButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _systemJournalGrid
            // 
            this._systemJournalGrid.AllowUserToAddRows = false;
            this._systemJournalGrid.AllowUserToDeleteRows = false;
            this._systemJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._systemJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._systemJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._systemJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol});
            this._systemJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._systemJournalGrid.Name = "_systemJournalGrid";
            this._systemJournalGrid.ReadOnly = true;
            this._systemJournalGrid.RowHeadersVisible = false;
            this._systemJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._systemJournalGrid.Size = new System.Drawing.Size(684, 476);
            this._systemJournalGrid.TabIndex = 14;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "Номер";
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 30;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Время";
            this._timeCol.HeaderText = "Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._msgCol.DataPropertyName = "Сообщение";
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _readJournalButton
            // 
            this._readJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readJournalButton.Location = new System.Drawing.Point(12, 482);
            this._readJournalButton.Name = "_readJournalButton";
            this._readJournalButton.Size = new System.Drawing.Size(117, 23);
            this._readJournalButton.TabIndex = 19;
            this._readJournalButton.Text = "Прочитать журнал";
            this._readJournalButton.UseVisualStyleBackColor = true;
            this._readJournalButton.Click += new System.EventHandler(this._readJournalButton_Click);
            // 
            // _loadJournalButton
            // 
            this._loadJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadJournalButton.Location = new System.Drawing.Point(298, 482);
            this._loadJournalButton.Name = "_loadJournalButton";
            this._loadJournalButton.Size = new System.Drawing.Size(128, 23);
            this._loadJournalButton.TabIndex = 21;
            this._loadJournalButton.Text = "Загрузить из файла";
            this._loadJournalButton.UseVisualStyleBackColor = true;
            this._loadJournalButton.Click += new System.EventHandler(this._loadJournalButton_Click);
            // 
            // _saveJournalButton
            // 
            this._saveJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveJournalButton.Location = new System.Drawing.Point(432, 482);
            this._saveJournalButton.Name = "_saveJournalButton";
            this._saveJournalButton.Size = new System.Drawing.Size(117, 23);
            this._saveJournalButton.TabIndex = 20;
            this._saveJournalButton.Text = "Сохранить в файл";
            this._saveJournalButton.UseVisualStyleBackColor = true;
            this._saveJournalButton.Click += new System.EventHandler(this._saveJournalButton_Click);
            // 
            // _saveJournalDialog
            // 
            this._saveJournalDialog.DefaultExt = "xml";
            this._saveJournalDialog.FileName = "МР761 Журнал Системы";
            this._saveJournalDialog.Filter = "МР761 Журнал Системы | *.xml";
            this._saveJournalDialog.Title = "Сохранить  журнал системы для МР761";
            // 
            // _openJournalDialog
            // 
            this._openJournalDialog.DefaultExt = "xml";
            this._openJournalDialog.FileName = "МР761 Журнал Системы";
            this._openJournalDialog.Filter = "МР761 Журнал системы(*.xml)|*.xml|МР761 Журнал системы(*.bin)|*.bin";
            this._openJournalDialog.RestoreDirectory = true;
            this._openJournalDialog.Title = "Открыть журнал системы для МР761";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(555, 482);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(117, 23);
            this._exportButton.TabIndex = 22;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 508);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(684, 22);
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "МР761 Журнал Системы";
            this._saveJournalHtmlDialog.Filter = "МР761 Журнал Системы | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал системы для МР761";
            // 
            // Mr761SystemJournalFormV3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 530);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this._readJournalButton);
            this.Controls.Add(this._loadJournalButton);
            this.Controls.Add(this._saveJournalButton);
            this.Controls.Add(this._systemJournalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(700, 568);
            this.Name = "Mr761SystemJournalFormV3";
            this.Text = "SystemJournalForm";
            this.Load += new System.EventHandler(this.SystemJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._systemJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _systemJournalGrid;
        private System.Windows.Forms.Button _readJournalButton;
        private System.Windows.Forms.Button _loadJournalButton;
        private System.Windows.Forms.Button _saveJournalButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalDialog;
        private System.Windows.Forms.OpenFileDialog _openJournalDialog;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
    }
}