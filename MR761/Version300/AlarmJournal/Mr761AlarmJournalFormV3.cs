﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761.Version300.AlarmJournal.Structures;
using BEMN.MR761.Version300.Configuration;
using BEMN.MR761.Version300.Configuration.Structures.MeasuringTransformer;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR761.Version300.AlarmJournal
{
    public partial class Mr761AlarmJournalFormV3 : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР761_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string DEVICE_NAME = "MR76X";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        #endregion [Constants]
        
        #region [Private fields]

        private readonly MemoryEntity<OneWordStruct> _setPage;  
        private readonly MemoryEntity<AlarmJournalRecordStructV3> _alarmJournal;
        private readonly CurrentOptionsLoaderV300 _currentOptionsLoader;
        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private MR761 _device;
        private List<string> _messages;
        private FileDriver _fileDriver;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr761AlarmJournalFormV3()
        {
            this.InitializeComponent();
        }

        public Mr761AlarmJournalFormV3(MR761 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._alarmJournal = device.Mr761DeviceV2.AlarmJournalV3;
            this._setPage = device.Mr761DeviceV2.SetPageJa;
            this._currentOptionsLoader = device.Mr761DeviceV2.CurrentOptionsLoaderJa;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this.CurrenOptionLoaded);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);

            this._setPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._alarmJournal.LoadStruct);
            this._setPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._device.DevicePlant == MR761.T4N5D42R19)
                {
                    this.ReadRecord(this._alarmJournal.Value,
                        this._currentOptionsLoader.ConnectionsN5[this._alarmJournal.Value.GroupOfSetpoints].Value);
                }
                else
                {
                    this.ReadRecord(this._alarmJournal.Value,
                        this._currentOptionsLoader.Connections[this._alarmJournal.Value.GroupOfSetpoints].Value);
                }
            });
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._failCounter < 80)
                {
                    this._failCounter++;
                }
                else
                {
                    this._alarmJournal.RemoveStructQueries();
                    this._failCounter = 0;
                    this.ButtonsEnabled = true;
                }
            });

            if (this._device.DevicePlant != MR761.T4N5D42R19)
            {
                this._alarmJournalGrid.Columns.Remove(this._Un1Col);
            }
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;

            this._fileDriver = new FileDriver(device, this);
        } 
        #endregion [Ctor's]


        #region [Help members]

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof (ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals) serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._messages = PropFormsStrings.GetNewAlarmList();
                }
            }
            catch
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }
            this.StartReadOption();
        }

        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }
        
        private void CurrenOptionLoaded()
        {
            this._recordNumber = this._failCounter = 0;
            this._table.Clear();
            this.SavePageNumber();
        }

        private void SavePageNumber()
        {
            this._setPage.Value.Word = (ushort)this._recordNumber;
            this._setPage.SaveStruct6();
        }

        private void ReadRecord(AlarmJournalRecordStructV3 record, MeasureTransStructV3 measure)
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStringsV3.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStringsV3.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetResist((short) record.Rab, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xab, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rbc, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xbc, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rca, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xca, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            this.GetStep(record.NumberOfTriggeredParametr),
                            ValuesConverterCommon.Analog.GetResist((short) record.Ra1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xa1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rb1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xb1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Rc1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short) record.Xc1, measure.ChannelI.Ittl,measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl*40), // 3I0
                            ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx*40),
                            ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl*40),
                            ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                            ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                            ValuesConverterCommon.Analog.GetF(record.F),
                            ValuesConverterCommon.Analog.GetQ(record.Q),
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();
                    this.SavePageNumber();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch(Exception e)
            {
                this._table.Rows.Add
                    (
                        this._recordNumber, "", "Ошибка ("+e.Message+")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", ""
                    );
                this.SavePageNumber();
            }
        }

        private void ReadRecord(AlarmJournalRecordStructV3 record, MeasureTransN5Struct measure)
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStringsV3.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStringsV3.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            this.GetStep(record.NumberOfTriggeredParametr),
                            ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),
                            ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),
                            ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue),
                            ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                            ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                            ValuesConverterCommon.Analog.GetF(record.F),
                            ValuesConverterCommon.Analog.GetQ(record.Q),
                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();
                    this.SavePageNumber();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                    (
                        this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", ""
                    );
                this.SavePageNumber();
            }
        }

        private void ReadRecordFromBinFile(MeasureTransStructV3 measure)
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    AlarmJournalRecordStructV3 record = this._alarmJournal.Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    this._table.Rows.Add
                    (
                        this._recordNumber,
                        record.GetTime,
                        AjStringsV3.Message[record.Message],
                        triggeredDef,
                        parameter,
                        parametrValue,
                        AjStringsV3.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        this.GetStep(record.NumberOfTriggeredParametr),
                        ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40), // 3I0
                        ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetQ(record.Q),
                        record.D1To8,
                        record.D9To16,
                        record.D17To24,
                        record.D25To32,
                        record.D33To40
                    );
                    //this._alarmJournalGrid.DataSource = this._table;
                    //this._alarmJournalGrid.Update();
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                (
                    this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", ""
                );
            }
        }

        private void ReadRecordFromBinFile(MeasureTransN5Struct measure)
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    AlarmJournalRecordStructV3 record = this._alarmJournal.Value;
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    this._table.Rows.Add
                    (
                        this._recordNumber,
                        record.GetTime,
                        AjStringsV3.Message[record.Message],
                        triggeredDef,
                        parameter,
                        parametrValue,
                        AjStringsV3.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        this.GetStep(record.NumberOfTriggeredParametr),
                        ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.ChannelI.Ittl, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.ChannelI.Ittl * 40), // 3I0
                        ValuesConverterCommon.Analog.GetI(record.In, measure.ChannelI.Ittx * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.ChannelI.Ittl * 40),
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.ChannelU.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.ChannelU.KthlValue), //3U0
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.ChannelU.KthxValue),
                        ValuesConverterCommon.Analog.GetU(record.Un1, measure.ChannelU.Kthx1Value),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetQ(record.Q),
                        record.D1To8,
                        record.D9To16,
                        record.D17To24,
                        record.D25To32,
                        record.D33To40
                    );
                    //this._alarmJournalGrid.DataSource = this._table;
                    //this._alarmJournalGrid.Update();
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                (
                    this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","", "", ""
                );
            }
        }

        private string GetTriggeredDefence(AlarmJournalRecordStructV3 record)
        {
            return record.TriggeredDefense == 61 
                ? this._messages[record.ValueOfTriggeredParametr] 
                : AjStringsV3.TriggeredDefense[record.TriggeredDefense];
        }

        private string GetParameter(AlarmJournalRecordStructV3 record)
        {
            if (record.TriggeredDefense >= 44 && record.TriggeredDefense <= 61)
            {
                return string.Empty;
            }
            return AjStringsV3.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalRecordStructV3 record, MeasureTransStructV3 measure)
        {
            int parameter = record.NumberOfTriggeredParametr;
            ushort value = record.ValueOfTriggeredParametr;
            if ((parameter>=36 && parameter <= 40) || parameter == 42 || parameter == 65)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
            }
            if (parameter == 41 || parameter == 43)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittx * 40);
            }
            if ((parameter >= 0) && (parameter <= 35))
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short) value, (short) value1, measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
            }
            if ((parameter >= 44) && (parameter <= 52))
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
            }
            if (parameter == 53)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
            }
            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }
            if (parameter == AjStringsV3.OmpInd)
            {
                return ValuesConverterCommon.Analog.GetOmp(value);
            }
            if (parameter == 61 || (parameter >= 56 && parameter <= 58))
            {
                return string.Empty;
            }
            if (parameter == 60)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }
            if (parameter == 64)
            {
                return ValuesConverterCommon.Analog.GetSignF((short) value);
            }
            return value.ToString();
        }

        private string GetParametrValue(AlarmJournalRecordStructV3 record, MeasureTransN5Struct measure)
        {
            int parameter = record.NumberOfTriggeredParametr;
            ushort value = record.ValueOfTriggeredParametr;
            if ((parameter >= 36 && parameter <= 40) || parameter == 42 || parameter == 65)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
            }
            if (parameter == 41 || parameter == 43)
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittx * 40);
            }
            if ((parameter >= 0) && (parameter <= 35))
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short)value, (short)value1,
                    measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
            }
            if ((parameter >= 44) && (parameter <= 52))
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
            }
            if (parameter == 53)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
            }
            if (parameter == 54 || parameter == 69)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.Kthx1Value);
            }
            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }
            if (parameter == AjStringsV3.OmpInd)
            {
                return ValuesConverterCommon.Analog.GetOmp(value);
            }
            if (parameter == 61 || (parameter >= 56 && parameter <= 58))
            {
                return string.Empty;
            }
            if (parameter == 64)
            {
                return ValuesConverterCommon.Analog.GetSignF((short)value);
            }
            return value.ToString();
        }

        private string GetStep(int param)
        {
            if ((param >= 0 && param <= 11) || param >= 36) return string.Format("Контур Ф-N{0}", 1);// по умолчанию 1я ступень
            if (param > 11 && param <= 17) return string.Format("Контур Ф-N{0}", 2);
            if (param > 17 && param <= 23) return string.Format("Контур Ф-N{0}", 3);
            if (param > 23 && param <= 29) return string.Format("Контур Ф-N{0}", 4);
            if (param > 29 && param <= 35) return string.Format("Контур Ф-N{0}", 5);
            return string.Format("Контур Ф-N{0}", 1); // по умолчанию 1я ступень
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
            }
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void _readAlarmJournalButtonClick(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void StartReadOption()
        {
            this._recordNumber = this._failCounter = 0;
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            this._table.Clear();
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                this._messages = PropFormsStrings.GetNewAlarmList();
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalRecordStructV3 journal = new AlarmJournalRecordStructV3();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int countRecord = journalBytes.Count / size;
                int j = 0;
                this._recordNumber = 0;
                for (int i = 0; j < countRecord - 2; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i,size).ToArray());
                    this._alarmJournal.Value = journal;
                    if (this._device.DevicePlant == MR761.T4N5D42R19)
                    {
                        this.ReadRecordFromBinFile(this._currentOptionsLoader.ConnectionsN5[journal.GroupOfSetpoints].Value);
                    }
                    else
                    {
                        this.ReadRecordFromBinFile(this._currentOptionsLoader.Connections[journal.GroupOfSetpoints].Value);
                    }
                    j++;
                }
            }
            else
            {
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName,
                    this._device.DevicePlant == MR761.T4N5D42R19 ? Resources.MR761V3N5AJ : Resources.MR761V3AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
        
        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._alarmJournal.RemoveStructQueries();
        }
        #endregion [Event Handlers]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR761); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr761AlarmJournalFormV3); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]
    }
}
