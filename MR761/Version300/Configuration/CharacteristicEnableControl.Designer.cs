﻿namespace BEMN.MR761.Version300.Configuration
{
    partial class CharacteristicEnableControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelZ1 = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // labelZ1
            // 
            this.labelZ1.AutoSize = true;
            this.labelZ1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelZ1.Location = new System.Drawing.Point(26, 5);
            this.labelZ1.Name = "labelZ1";
            this.labelZ1.Size = new System.Drawing.Size(0, 13);
            this.labelZ1.TabIndex = 3;
            this.labelZ1.Click += new System.EventHandler(this.labelZ1_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.Transparent;
            this.panel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel.Location = new System.Drawing.Point(3, 3);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(16, 16);
            this.panel.TabIndex = 2;
            this.panel.Click += new System.EventHandler(this.panel_Click);
            // 
            // CharacteristicEnableControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelZ1);
            this.Controls.Add(this.panel);
            this.Name = "CharacteristicEnableControl";
            this.Size = new System.Drawing.Size(179, 22);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelZ1;
        private System.Windows.Forms.Panel panel;
    }
}
