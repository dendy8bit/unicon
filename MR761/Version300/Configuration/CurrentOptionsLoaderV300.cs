﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR761.Version300.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR761.Version300.Configuration
{
    public class CurrentOptionsLoaderV300
    {
        #region [Private fields]
        private const int COUNT_GROUPS = 6;
        private MR761 _device;

        public MemoryEntity<MeasureTransStructV3>[] Connections { get; }
        public MemoryEntity<MeasureTransN5Struct>[] ConnectionsN5 { get; }

        private int _numberOfGroup;
        #endregion [Private fields]
        
        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        #endregion [Events]

        #region [Ctor's]

        public CurrentOptionsLoaderV300(MR761 device)
        {
            this._device = device;
            this._numberOfGroup = 0;
            if (this._device.DevicePlant == MR761.T4N5D42R19)
            {
                this.ConnectionsN5 = new MemoryEntity<MeasureTransN5Struct>[COUNT_GROUPS];
                for (int i = 0; i < this.ConnectionsN5.Length; i++)
                {
                    this.ConnectionsN5[i] =
                        new MemoryEntity<MeasureTransN5Struct>(string.Format("Параметры измерительного трансформатора гр{0} V3.xx", i + 1),
                            this._device, this._device.GetStartAddrMeasTrans(i, this._device.DeviceVersion));
                    this.ConnectionsN5[i].AllReadOk += this._connections_AllReadOk;
                    this.ConnectionsN5[i].AllReadFail += this._connections_AllReadFail;
                }
            }
            else
            {
                this.Connections = new MemoryEntity<MeasureTransStructV3>[COUNT_GROUPS];
                for (int i = 0; i < this.Connections.Length; i++)
                {
                    this.Connections[i] =
                        new MemoryEntity<MeasureTransStructV3>(
                            string.Format("Параметры измерительного трансформатора гр{0} V3", i + 1),
                            this._device, this._device.GetStartAddrMeasTrans(i, this._device.DeviceVersion));
                    this.Connections[i].AllReadOk += this._connections_AllReadOk;
                    this.Connections[i].AllReadFail += this._connections_AllReadFail;
                }
            }
        }

        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        private void _connections_AllReadFail(object sender)
        {
            this.LoadFail?.Invoke();
        }

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        private void _connections_AllReadOk(object sender)
        {
            this._numberOfGroup++;
            if (this._numberOfGroup < COUNT_GROUPS)
            {
                this.Connections?[this._numberOfGroup].LoadStruct();
                this.ConnectionsN5?[this._numberOfGroup].LoadStruct();
            }
            else
            {
                this.LoadOk?.Invoke();
            }
        }

        #endregion [Memory Entity Events Handlers]


        #region [Methods]

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this.Connections?[0].LoadStruct();
            this.ConnectionsN5?[0].LoadStruct();
        }
        
        #endregion [Methods]
    }
}
