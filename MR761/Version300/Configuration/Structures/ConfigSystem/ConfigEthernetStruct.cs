﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761.Version300.Configuration.Structures.ConfigSystem
{
    public class ConfigEthernetStruct : StructBase
    {
        [Layout(0)] private ushort _ipLo;
        [Layout(1)] private ushort _ipHi;
        [Layout(2)] private ushort _port;
        [Layout(3)] private ushort _prop;
        [Layout(4)] private ushort _macLo1;
        [Layout(5)] private ushort _macHi1;
        [Layout(6)] private ushort _macLo2;
        [Layout(7)] private ushort _macHi2;
    }
}
