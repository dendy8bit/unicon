﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761.Version300.Configuration.Structures.ConfigSystem
{
    public class ConfigSystemStruct : StructBase
    {
        [Layout(0)] private ConfigNetStruct _configNet;
        [Layout(1)] private ConfigEthernetStruct _configEthernet;
    }
}
