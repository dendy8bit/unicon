﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR761.Version300.Configuration.Structures.ConfigSystem
{
    public class ConfigIPAddress : StructBase
    {
        [Layout(0)] private ushort _ipLo;
        [Layout(1)] private ushort _ipHi;
        [Layout(2)] private ushort sntpip_lo;  // IP адрес сервера реального времени для синхронизации
        [Layout(3)] private ushort sntpip_hi;
        [Layout(4)] private ushort _config;     //тестирование, резервирование, свойства MAC адреса
        //[Layout(5)] private ushort mac_lo;
        //[Layout(6)] private ushort mac_md;
        //[Layout(7)] private ushort mac_hi;
        //[Layout(8)] private ushort period;     //период обновления времени 0-9999 мин
        //[Layout(9)] private ushort timezone;   //часовой пояс

        [BindingProperty(0)]
        public ushort IpLo1 
        {
            get { return Common.GetBits(this._ipLo, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._ipLo = Common.SetBits(this._ipLo, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }
        [BindingProperty(1)]
        public ushort IpLo2
        {
            get { return (ushort) (Common.GetBits(this._ipLo, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._ipLo = Common.SetBits(this._ipLo, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        [BindingProperty(2)]
        public ushort IpHi1
        {
            get { return Common.GetBits(this._ipHi, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._ipHi = Common.SetBits(this._ipHi, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        [BindingProperty(3)]
        public ushort IpHi2
        {
            get { return (ushort)(Common.GetBits(this._ipHi, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._ipHi = Common.SetBits(this._ipHi, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        [BindingProperty(4)]
        public string Reserve
        {
            get { return Validator.Get(this._config, StringsConfig.Reserve, 2, 3); }
            set { this._config = Validator.Set(value, StringsConfig.Reserve, this._config, 2, 3); }
        }
    }
}
