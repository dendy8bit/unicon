﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR761.Version300.Configuration.Structures.AcCountLoad
{
    public class AcCountLoadStruct : StructBase
    {
        [Layout(0)] private ushort _r1Faza ;
        [Layout(1)] private ushort _r2Faza;
        [Layout(2)] private ushort _cornerFaza;
        [Layout(3)] private ushort _res1;
        [Layout(4)] private ushort _r1Line;
        [Layout(5)] private ushort _r2Line;
        [Layout(6)] private ushort _cornerLine;
        [Layout(7)] private ushort _res2;

        [BindingProperty(0)]
        public double R1Faza
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r1Faza); }
            set { this._r1Faza = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(1)]
        public double R2Faza
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r2Faza); }
            set { this._r2Faza = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(2)]
        public ushort CornerFaza
        {
            get { return this._cornerFaza; }
            set { this._cornerFaza = value; }
        }
        [BindingProperty(3)]
        public double R1Line
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r1Line); }
            set { this._r1Line = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(4)]
        public double R2Line
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r2Line); }
            set { this._r2Line = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(5)]
        public ushort CornerLine
        {
            get { return this._cornerLine; }
            set { this._cornerLine = value; }
        }
    }
}
