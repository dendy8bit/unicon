﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR761.Version300.Configuration.Structures.AcCountLoad
{
    public class AcCountLoadStructNew : StructBase
    {
        [Layout(0)] private ushort _config; // 0 - нет 1 - да
        [Layout(1)] private ushort _imax;
        [Layout(2)] private ushort _umin;
        [Layout(3)] private ushort _res1;
        [Layout(4)] private ushort _r1Line;
        [Layout(5)] private ushort _r2Line;
        [Layout(6)] private ushort _cornerLine;
        [Layout(7)] private ushort _res2;

        [BindingProperty(0)]
        public bool Config
        {
            get { return Common.GetBit(this._config, 0); }
            set { this._config = Common.SetBit(this._config, 0, value); }
        }
        [BindingProperty(1)]
        public double Umin
        {
            get { return ValuesConverterCommon.GetUstavka256(this._umin); }
            set { this._umin = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(2)]
        public double Imax
        {
            get { return ValuesConverterCommon.GetIn(this._imax); }
            set { this._imax = ValuesConverterCommon.SetIn(value); }
        }
        [BindingProperty(3)]
        public double R1Line
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r1Line); }
            set { this._r1Line = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(4)]
        public double R2Line
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r2Line); }
            set { this._r2Line = ValuesConverterCommon.SetUstavka256(value); }
        }
        [BindingProperty(5)]
        public ushort CornerLine
        {
            get { return this._cornerLine; }
            set { this._cornerLine = value; }
        }
    }
}
