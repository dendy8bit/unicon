﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR761.Version300.Configuration.Structures
{
    public class AllGroupSetpointStruct307 : StructBase, ISetpointContainer<GroupSetpoint307>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpoint307[] _groupSetpoints;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint307[] Setpoints
        {
            get
            {
                GroupSetpoint307[] res = new GroupSetpoint307[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpoint307>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
