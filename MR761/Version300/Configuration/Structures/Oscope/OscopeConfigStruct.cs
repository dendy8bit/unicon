﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761.Version300.Configuration.Structures.Oscope
{
    public class OscopeConfigStruct : StructBase
    {
        [Layout(0)] private ushort _config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort _size; //размер осциллограмы
        [Layout(2)] private ushort _percent; //процент от размера осциллограммы

        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return this._percent; }
            set { this._percent = value; }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string FixationXml
        {
            get { return Validator.Get(this._config, StringsConfig.OscFixation); }
            set { this._config = Validator.Set(value, StringsConfig.OscFixation); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string SizeXml
        {
            get { return this._size.ToString(); }
            set { this._size = ushort.Parse(value); }
        }

        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize
        {
            get
            {
                return 1024;
            }
        }
    }
}
