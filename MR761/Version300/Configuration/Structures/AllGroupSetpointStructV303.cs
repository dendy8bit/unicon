﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR761.Version300.Configuration.Structures
{
    public class AllGroupSetpointStructV303 : StructBase, ISetpointContainer<GroupSetpointStructV303>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpointStructV303[] _groupSetpoints;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpointStructV303[] Setpoints
        {
            get
            {
                GroupSetpointStructV303[] res = new GroupSetpointStructV303[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpointStructV303>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
