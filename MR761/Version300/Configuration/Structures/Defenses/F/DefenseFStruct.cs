﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR761.Version300.Configuration.Structures.Defenses.F
{
    /// <summary>
    /// конфигурациия основной ступени Umax, Umin
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "конфигурациия_защиты_F")]
    public class DefenseFStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1; //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block; //вход блокировки
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _u; //уставка возврата
        [Layout(6)] private ushort _tu; //время возврата
        [Layout(7)] private ushort _u1; //U1, если выбран тип dF/dt

        #endregion [Private fields]


        #region Properties

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Тип")]
        public string Type
        {
            get { return Validator.Get(this._config, StringsConfig.FreqDefType, 5); }
            set { this._config = Validator.Set(value, StringsConfig.FreqDefType, this._config, 5);}
        }
        
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ust); }
            set { this._ust = ValuesConverterCommon.SetUstavka256FromDfDf(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "U1")]
        public double U1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u1); }
            set { this._u1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "ty")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "U_пуск")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Uпуск")]
        public bool UStartBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "АПВ")]
        public string Apv
        {
            get { return Validator.Get(this._config1, StringsConfig.AutomaticMode, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.AutomaticMode, this._config1, 0); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "АВР")]
        public string Avr
        {
            get { return Validator.Get(this._config1, StringsConfig.AutomaticMode, 1); }
            set { this._config1 = Validator.Set(value, StringsConfig.AutomaticMode, this._config1, 1); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Апв_возврат")]
        public bool ApvBack
        {
            get { return Common.GetBit(this._config1, 2); }
            set { this._config1 = Common.SetBit(this._config1, 2, value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }

        #endregion
    }
}
