﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR761.Version300.Configuration.Structures.Defenses.Q
{
    /// <summary>
    /// блокировка по тепловой модели
    /// </summary>

    [Serializable]
    public class DefenseTermBlockStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] ushort _config;				//конфигурация
        [Layout(1)] ushort _val;				//уставка срабатывания
        [Layout(2)] ushort _wait;				//время блокировки
        //паспортные данные
        [Layout(3)] ushort _cosf;
        // защита по числу пусков
        [Layout(4)] ushort _numhot;
        [Layout(5)] ushort _numcold;
        [Layout(6)] ushort _waitblk;
        //паспортные данные
        [Layout(7)] ushort _kpd;
        #endregion [Private fields]


        #region [Properties]
     
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn); }
        }
        /// <summary>
        /// уставка срабатывания
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "уставка_срабатывания")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetUstavka256(this._val); }
            set { this._val = ValuesConverterCommon.SetUstavka256(value); }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Время_срабатывания")]
        public ushort Time
        {
            get { return this._wait; }
            set { this._wait = value; }
        }

        /// <summary>
        /// Число горячих пусков
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Число_горячих_пусков")]
        public ushort Hot
        {
            get { return this._numhot; }
            set { this._numhot = value; }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Число_холодных_пусков")]
        public ushort Cold
        {
            get { return this._numcold; }
            set { this._numcold = value; }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Время_блокировки")]
        public ushort TimeBlk
        {
            get { return this._waitblk; }
            set { this._waitblk = value; }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Cosf")]
        public double CosF
        {
            get
            {
                double val = _cosf / 100.00;
                return Math.Round(val,2);
            }
            set
            {
                _cosf = (ushort) (value * 100);
            }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "КПД")]
        public ushort Kpd
        {
            get { return _kpd;}
            set { _kpd = value; }
        }
        #endregion [Properties]
    }
}
