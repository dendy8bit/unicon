﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761.Version300.Configuration.Structures.Defenses.Z
{
    public class AllResistanceDefensesStructNew : StructBase
    {
        public const int RESIST_DEF_COUNT = 6;
        [Layout(0, Count = RESIST_DEF_COUNT)] private ResistanceDefensesStructNew[] _resistanceDefenses;
        [Layout(1)] private ResistanceDefensesStructNew _reserve; //РЕЗЕРВ!!!

        [BindingProperty(0)]
        public ResistanceDefensesStructNew this[int index]
        {
            get { return this._resistanceDefenses[index]; }
            set { this._resistanceDefenses[index] = value; }
        }
        [XmlElement(ElementName = "Все_дистанционные_защиты")]
        public ResistanceDefensesStructNew[] ResistanceDefenses
        {
            get { return this._resistanceDefenses; }
            set { this._resistanceDefenses = value; }
        }
    }
}
