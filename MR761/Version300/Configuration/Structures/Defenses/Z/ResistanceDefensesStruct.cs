﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR761.Version300.Configuration.Structures.Defenses.Z
{
    [XmlRoot(ElementName = "конфигурациия_дистанционной_защиты")]
    public class ResistanceDefensesStruct : StructBase
    {
        #region [Fields]
        [Layout(0)] private ushort _config;  // конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1; // конфигурация дополнительная (АПВ - выведено/введено, осц, переход в ненаправленную)
        [Layout(2)] private ushort _block;   // вход блокировки
        [Layout(3)] private ushort _x1;      // уставка срабатывания
        [Layout(4)] private ushort _time;    // время срабатывания
        [Layout(5)] private ushort _imax;    // уставка пуска по току
        [Layout(6)] private ushort _u;       // уставка пуска по напряжению
        [Layout(7)] private ushort _tu;      // время ускорения
        [Layout(8)] private ushort _r1;      // уставка 
        [Layout(9)] private ushort _cornRad;  // угол/радиус
        [Layout(10)] private ushort _inUsk;
        [Layout(11)] private ushort _rez;
        #endregion

        #region [Prop]
        [BindingProperty(0)]

        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        public string Type
        {
            get { return Validator.Get(this._config, StringsConfig.ResistDefType, 4); }
            set { this._config = Validator.Set(value, StringsConfig.ResistDefType, this._config, 4); }
        }

        [BindingProperty(2)]
        public string BlocInp
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        public bool SignR
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }

        [BindingProperty(3)]
        public double UstR
        {
            get
            {
                var ret = ValuesConverterCommon.GetUstavka256(this._r1);
                if (this.SignR && Common.GetBit(this._config, 4)) ret = -ret;
                return ret;
            }
            set
            {
                var val = value;
                this.SignR = val < 0;
                this._r1 = ValuesConverterCommon.SetUstavka256(Math.Abs(val));
            }
        }

        public bool SignX
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }

        [BindingProperty(4)]
        public double UstX
        {
            get
            {
                var ret = ValuesConverterCommon.GetUstavka256(this._x1);
                if (this.SignX && Common.GetBit(this._config, 4)) ret = -ret;
                return ret;
            }
            set
            {
                var val = value;
                this.SignX = val < 0;
                this._x1 = ValuesConverterCommon.SetUstavka256(Math.Abs(val));
            }
        }

        [BindingProperty(5)]
        public double Ustr
        {
            get 
            {
                return Common.GetBit(this._config, 4) ? ValuesConverterCommon.GetUstavka256(this._cornRad) : this._cornRad;
            }
            set
            {
                this._cornRad = Common.GetBit(this._config, 4) ? ValuesConverterCommon.SetUstavka256(value) : (ushort)value;
            }
        }

        [BindingProperty(6)]
        public string Direction
        {
            get { return Validator.Get(this._config, StringsConfig.ResistDefDirection, 5, 6); }
            set { this._config = Validator.Set(value, StringsConfig.ResistDefDirection, this._config, 5, 6); }
        }

        public int DirectionInd
        {
            get { return Common.GetBits(this._config, 5, 6) >> 5; }
            set{}
        }

  
        [BindingProperty(7)]
        public bool StartOnU
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        [BindingProperty(8)]
        public double Ustart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(9)]
        public double Isr
        {
            get { return ValuesConverterCommon.GetUstavka40(this._imax); }
            set { this._imax = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(10)]
        public int Tsr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value);}
        }

        [BindingProperty(11)]
        public string Acceleration
        {
            get { return Validator.Get(this._inUsk, StringsConfig.RelaySignals); }
            set { this._inUsk = Validator.Set(value, StringsConfig.RelaySignals);}
        }
        
        [BindingProperty(12)]
        public int Tu
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value);}
        }
        

        [BindingProperty(13)]
        public string Conture
        {
            get { return Validator.Get(this._config, StringsConfig.ContureV2, 8, 9, 10); }
            set { this._config = Validator.Set(value, StringsConfig.ContureV2, this._config, 8, 9, 10); }
        }

        public int ContureInd
        {
            get { return Common.GetBits(this._config, 8, 9, 10) >> 8; }
            set { }
        }

        [BindingProperty(14)]
        public string BlockFromTn
        {
            get { return Validator.Get(this._config1, StringsConfig.BlockTn, 9, 10); }
            set { this._config1 = Validator.Set(value, StringsConfig.BlockTn, this._config1, 9, 10); }
        }

        [BindingProperty(15)]
        public bool BlockFromLoad
        {
            get { return Common.GetBit(this._config, 11); }
            set { this._config = Common.SetBit(this._config, 11, value); }
        }

        [BindingProperty(16)]
        public bool BlockFromSwing
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }
        [BindingProperty(17)]
        public bool DamageFaza
        {
            get { return Common.GetBit(this._config1, 7); }
            set { this._config1 = Common.SetBit(this._config1, 7, value); }
        }
        [BindingProperty(18)]
        public bool ResetStep
        {
            get { return Common.GetBit(this._config1, 8); }
            set { this._config1 = Common.SetBit(this._config1, 8, value); }
        }

        [BindingProperty(19)]
        public string Oscilloscope
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }
        
        [BindingProperty(20)]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        [BindingProperty(21)]
        public string Apv
        {
            get { return Validator.Get(this._config1, StringsConfig.AutomaticMode, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.AutomaticMode, this._config1, 0); }
        }

        [BindingProperty(22)]
        public string Avr
        {
            get { return Validator.Get(this._config1, StringsConfig.AutomaticMode, 1); }
            set { this._config1 = Validator.Set(value, StringsConfig.AutomaticMode, this._config1, 1); }
        }

        [BindingProperty(23)]
        public bool SteeredModeAcceler
        {
            get { return Common.GetBit(this._config1, 6); }
            set { this._config1 = Common.SetBit(this._config1, 6, value); }
        }
        #endregion
    }
}
