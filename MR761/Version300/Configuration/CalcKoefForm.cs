﻿using System;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR761.Version300.Configuration.Structures.Defenses.Z;

namespace BEMN.MR761.Version300.Configuration
{
    public partial class CalcKoefForm : Form
    {
        private Complex N1Z0;
        private Complex N1Z1;

        private Complex N2Z0;
        private Complex N2Z1;

        private Complex N3Z0;
        private Complex N3Z1;

        private Complex N4Z0;
        private Complex N4Z1;

        private Complex N5Z0;
        private Complex N5Z1;

        public CalcKoefForm(ResistanceStruct resist)
        {
            this.InitializeComponent();
            this.N1Z0 = new Complex(resist.R0Step1, resist.X0Step1);
            this.N1Z1 = new Complex(resist.R1Step1, resist.X1Step1);

            this.N2Z0 = new Complex(resist.R0Step2, resist.X0Step2);
            this.N2Z1 = new Complex(resist.R1Step2, resist.X1Step2);

            this.N3Z0 = new Complex(resist.R0Step3, resist.X0Step3);
            this.N3Z1 = new Complex(resist.R1Step3, resist.X1Step3);

            this.N4Z0 = new Complex(resist.R0Step4, resist.X0Step4);
            this.N4Z1 = new Complex(resist.R1Step4, resist.X1Step4);

            this.N5Z0 = new Complex(resist.R0Step5, resist.X0Step5);
            this.N5Z1 = new Complex(resist.R1Step5, resist.X1Step5);
        }

        private void CloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void CalcKoefForm_Load(object sender, EventArgs e)
        {
            this.Ko1Amp.Text = ((this.N1Z0 - this.N1Z1)/(this.N1Z1*3)).Mod;
            this.Ko1Angle.Text =  ((this.N1Z0 - this.N1Z1)/(this.N1Z1*3)).Argument;
            this.Z0Z1Amp1.Text = (this.N1Z0/this.N1Z1).Mod;
            this.Z0Z1Angle1.Text = (this.N1Z0/this.N1Z1).Argument;

            this.Ko2Amp.Text = ((this.N2Z0 - this.N2Z1)/(this.N2Z1*3)).Mod;
            this.Ko2Angle.Text =  ((this.N2Z0 - this.N2Z1)/(this.N2Z1*3)).Argument;
            this.Z0Z1Amp2.Text = (this.N2Z0/this.N2Z1).Mod;
            this.Z0Z1Angle2.Text = (this.N2Z0/this.N2Z1).Argument;

            this.Ko3Amp.Text = ((this.N3Z0 - this.N3Z1)/(this.N3Z1*3)).Mod;
            this.Ko3Angle.Text =  ((this.N3Z0 - this.N3Z1)/(this.N3Z1*3)).Argument;
            this.Z0Z1Amp3.Text = (this.N3Z0/this.N3Z1).Mod;
            this.Z0Z1Angle3.Text = (this.N3Z0/this.N3Z1).Argument;

            this.Ko4Amp.Text = ((this.N4Z0 - this.N4Z1)/(this.N4Z1*3)).Mod;
            this.Ko4Angle.Text =  ((this.N4Z0 - this.N4Z1)/(this.N4Z1*3)).Argument;
            this.Z0Z1Amp4.Text = (this.N4Z0/this.N4Z1).Mod;
            this.Z0Z1Angle4.Text = (this.N4Z0/this.N4Z1).Argument;

            this.Ko5Amp.Text = ((this.N5Z0 - this.N5Z1)/(this.N5Z1*3)).Mod;
            this.Ko5Angle.Text =  ((this.N5Z0 - this.N5Z1)/(this.N5Z1*3)).Argument;
            this.Z0Z1Amp5.Text = (this.N5Z0/this.N5Z1).Mod;
            this.Z0Z1Angle5.Text = (this.N5Z0/this.N5Z1).Argument;
        }
    }
}
