﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR761.Version300.Diagnostic
{
    public class ConfigDiagnosticReadOnlyStruct : StructBase
    {
        [Layout(0)] private ushort _size;                   //размер структуры неисправности
        [Layout(1)] private ushort _pos;                    //текущая пустая позиция
        [Layout(2)] private ushort _max;                    //максимальное число сохраняемых неисправностей
        [Layout(3)] private ushort _res;                    
        [Layout(4)] private uint _err;                      //общее число ошибок модулей
        [Layout(5)] private uint _tim;                      //время в мс от вкл. устройства
        [Layout(6, Count = 8)] private ushort[] _dateTime;  //дата и время

        public uint Tim
        {
            get { return this._tim; }
            set { this._tim = value; }
        }

        public DateTime StartTime
        {
            get
            {
                return new DateTime(this._dateTime[0], this._dateTime[1], this._dateTime[2], this._dateTime[3],
                    this._dateTime[4], this._dateTime[5], this._dateTime[6]);
            }
        }

        public string GetDataTime(uint tim)
        {
            double delta = tim - this._tim;
            if (delta < 0) return string.Empty;
            DateTime currentErrorTime = this.StartTime.AddMilliseconds(delta);

            return string.Format("{0:D2}.{1:D2}.{2:D2} {3:D2}:{4:D2}:{5:D2}.{6:D3}", currentErrorTime.Day, currentErrorTime.Month,
                currentErrorTime.Year, currentErrorTime.Hour, currentErrorTime.Minute, currentErrorTime.Second,
                currentErrorTime.Millisecond);
        } 
    }
}
