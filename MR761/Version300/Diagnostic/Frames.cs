﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.MBServer;

namespace BEMN.MR761.Version300.Diagnostic
{
    [Serializable]
    public class Frames
    {
        public Frames()
        {
            this.FramesList = new List<Frame>();
        }
        [XmlElement("Frame")]
        public List<Frame> FramesList { get; private set; }

        public void AddFrames(AllDiagnosticDataStruct allDiagnostic, ConfigDiagnosticReadOnlyStruct config)
        {
            Frame last = this.FramesList.Count > 0 ? this.FramesList.Last() : new Frame();
            foreach (DiagnosticDataStruct diagnostic in
                allDiagnostic.AllDiagnostic.Where(diagnostic => diagnostic.Tim >= config.Tim && (diagnostic.Error > last.Err || diagnostic.Error == 0)))
            {
                this.FramesList.Add(new Frame(diagnostic, config.GetDataTime(diagnostic.Tim)));
            }
        }
    }

    [Serializable]
    public class Frame
    {
        public Frame()
        {
            this.Write = this.Read = this.TypeFault = this.Pos = this.DateTime = string.Empty;
            this.Err = this.LenPac = 0;
        }

        public Frame(DiagnosticDataStruct diagnostic, string dateTime)
        {
            this.Write = diagnostic.Write.Aggregate(string.Empty,
                (current, w) =>
                    current + Common.LOBYTE(w).ToString("X").PadLeft(2, '0') + " " +
                    Common.HIBYTE(w).ToString("X").PadLeft(2, '0') + " ");
            this.Read = diagnostic.Read.Aggregate(string.Empty,
                (current, w) =>
                    current + Common.LOBYTE(w).ToString("X").PadLeft(2, '0') + " " +
                    Common.HIBYTE(w).ToString("X").PadLeft(2, '0') + " ");

            this.DateTime = dateTime;
            this.Err = diagnostic.Error;
            this.TypeFault = Convert.ToString(diagnostic.TypeFault, 2).PadLeft(16, '0');
            switch (diagnostic.Pos)
            {
                case 0x1:
                    this.Pos = "Модуль 5";
                    break;
                case 0x2:
                    this.Pos = "Модуль 4";
                    break;
                case 0x4:
                    this.Pos = "Модуль 3";
                    break;
                case 0x8:
                    this.Pos = "Модуль 2";
                    break;
                case 0x10:
                    this.Pos = "Модуль 1";
                    break;
                default:
                    this.Pos = "Неизвестное значение";
                    break;
            }
            this.LenPac = diagnostic.LenPac;
        }

        [XmlElement]
        public string Write { get; set; }
        [XmlElement]
        public string Read { get; set; }
        [XmlElement]
        public string DateTime { get; set; }
        [XmlElement]
        public uint Err { get; set; }
        [XmlElement]
        public string TypeFault { get; set; }
        [XmlElement]
        public string Pos { get; set; }
        [XmlElement]
        public ushort LenPac { get; set; }
    }
}
