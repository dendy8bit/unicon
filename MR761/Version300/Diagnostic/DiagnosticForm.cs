﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR761.Version300.Diagnostic
{
    public partial class DiagnosticForm : Form, IFormView
    {
        private MR761 _device;
        private CheckBox[] _faults;
        private CheckBox[] _modules;
        private bool _runDiagnostic;
        private XmlTextWriter _writer;
        private XmlSerializer _serializer;
        private Frames _frames;
        private Thread _th;

        public DiagnosticForm()
        {
            InitializeComponent();
        }

        public DiagnosticForm(MR761 device)
        {
            InitializeComponent();

            this._device = device;
            this._device.Mr761DeviceV2.ConfigDiagnostic.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._device.Mr761DeviceV2.ConfigDiagnosticReadOnly.LoadStruct);
            this._device.Mr761DeviceV2.ConfigDiagnostic.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigReadFail);
            this._device.Mr761DeviceV2.ConfigDiagnostic.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigWriteOk);
            this._device.Mr761DeviceV2.ConfigDiagnostic.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigWriteFail);
            this._device.Mr761DeviceV2.ConfigDiagnosticReadOnly.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ShowConfig);
            this._device.Mr761DeviceV2.ConfigDiagnosticReadOnly.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigReadFail);

            this._faults = new [] {this.err1, this.err2, this.err3, this.err4, this.err5, this.err6, this.err7, this.err8};
            this._modules = new[] {this.module1, this.module2, this.module3, this.module4, this.module5};
        }

        private bool ConfigLoaded
        {
            set
            {
                this.SaveConfigDiagnosticBtn.Enabled = value;
                this.StartDiagnosticBtn.Enabled = value;
                this.LoadLogListBtn.Enabled = value;
            }
        }

        private void ConfigWriteFail()
        {
            MessageBox.Show("Невозможно записать конфигурацию диагностики", "Внимание", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            this.ConfigLoaded = false;
        }

        private void ConfigWriteOk()
        {
            MessageBox.Show("Конфигурация диагностики записана успешно", "Внимание", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            this._device.Mr761DeviceV2.ConfigDiagnostic.LoadStruct();
        }

        private void ConfigReadFail()
        {
            MessageBox.Show("Невозможно прочитать конфигурацию диагностики", "Внимание", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            foreach (CheckBox checkBox in this._faults)
            {
                checkBox.Checked = false;
            }
            foreach (CheckBox checkBox in this._modules)
            {
                checkBox.Checked = false;
            }
            this.workLed.State = LedState.Off;
            this.Len.Text = "0";
            this.ConfigLoaded = false;
        }

        private void ShowConfig()
        {
            for (int i = 0; i < this._faults.Length; i++)
            {
                this._faults[i].Checked = i < this._faults.Length - 1 
                    ? this._device.Mr761DeviceV2.ConfigDiagnostic.Value.FaultsConfig[i] 
                    : this._device.Mr761DeviceV2.ConfigDiagnostic.Value.FaultsConfig[15];
            }

            for (int i = 0; i < this._modules.Length; i++)
            {
                this._modules[this._modules.Length-i-1].Checked = this._device.Mr761DeviceV2.ConfigDiagnostic.Value.ModulesConfig[i];
            }
            this.workLed.State = this._device.Mr761DeviceV2.ConfigDiagnostic.Value.Status ? LedState.NoSignaled : LedState.Signaled;
            this.Len.Text = this._device.Mr761DeviceV2.ConfigDiagnostic.Value.CountFrames.ToString();
            this.ConfigLoaded = true;
        }

        private void LoadConfigDiagnosticBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Mr761DeviceV2.ConfigDiagnostic.LoadStruct();
        }

        private void SaveConfigDiagnosticBtn_Click(object sender, EventArgs e)
        {
            ConfigDiagnosticStruct config = this._device.Mr761DeviceV2.ConfigDiagnostic.Value;
            config.CountFrames = Convert.ToUInt16(this.Len.Text);
            List<bool> faultsList = this._faults.Select(checkBox => checkBox.Checked).ToList();
            config.FaultsConfig = new BitArray(faultsList.ToArray());
            List<bool> modulesList = this._modules.Select(checkBox => checkBox.Checked).Reverse().ToList();
            config.ModulesConfig = new BitArray(modulesList.ToArray());
            this._device.Mr761DeviceV2.ConfigDiagnostic.Value = config;
            this._device.Mr761DeviceV2.ConfigDiagnostic.SaveStruct();
        }

        private void StartStopDiagnosticBtn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if(btn == null) return;
            DialogForm.Result result = this.CheckDiagnosticState();
            switch (result)
            {
                case DialogForm.Result.RESET:
                    this.WriteStatus(Convert.ToBoolean(btn.Tag));
                    return;
                case DialogForm.Result.CONTINUE:
                    this.StartReadDiagnosticProcess();
                    break;
                case DialogForm.Result.CANCEL:
                    break;
                case DialogForm.Result.BREAK:
                    this.WriteStopStatus();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void WriteStatus(bool btnTag)
        {
            if (btnTag)
            {
                this.WriteStartStatus();
            }
            else
            {
                this.WriteStopStatus();
            }
        }

        private void WriteStartStatus()
        {
            byte[] request = new byte[8];
            //запись старта диагностики
            request[0] = this._device.DeviceNumber;
            request[1] = 6;
            request[2] = 0x5D;
            request[3] = 0x00;
            request[4] = 0x00;
            request[5] = 0x01;
            ushort crc = CRC16.CalcCrcFast(request, 6);
            request[6] = (byte) (crc >> 8);
            request[7] = (byte) (crc & 0xFF);
            byte[] answer = this._device.MB.Port.Opened ? this._device.MB.Port.WriteRead(request, 256) as byte[] : null;
            if (answer == null || answer.Length < 8)
            {
                MessageBox.Show("Невозможно запустить чтение диагностики", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.workLed.State = LedState.Off;
                return;
            }
            //чтение значения TIM в заголовке
            request[0] = this._device.DeviceNumber;
            request[1] = 4;
            request[2] = 0x5D;
            request[3] = 0x0A;
            request[4] = 0x00;
            request[5] = 0x02;
            crc = CRC16.CalcCrcFast(request, 6);
            request[6] = (byte) (crc >> 8);
            request[7] = (byte) (crc & 0xFF);
            answer = this._device.MB.Port.Opened ? this._device.MB.Port.WriteRead(request, 256) as byte[] : null;
            if (answer == null || answer.Length < 8)
            {
                MessageBox.Show("Невозможно запустить чтение диагностики", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.workLed.State = LedState.Off;
                return;
            }
            this._device.Mr761DeviceV2.ConfigDiagnosticReadOnly.Value.Tim = Common.ToWord32(answer.Skip(3).Take(4).ToArray());
            this.StartReadDiagnosticProcess();
        }

        private void WriteStopStatus()
        {
            byte[] request = new byte[8];
            this._runDiagnostic = false;
            request[0] = this._device.DeviceNumber;
            request[1] = 6;
            request[2] = 0x5D;
            request[3] = 0x00;
            request[4] = 0x00;
            request[5] = 0x00;
            ushort crc = CRC16.CalcCrcFast(request, 6);
            request[6] = (byte) (crc >> 8);
            request[7] = (byte) (crc & 0xFF);
            byte[] answer = this._device.MB.Port.Opened ? this._device.MB.Port.WriteRead(request, 256) as byte[] : null;
            if (answer == null || answer.Length < 8)
            {
                MessageBox.Show("Невозможно записать статус чтения диагностики", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.workLed.State = LedState.Off;
                return;
            }
            this.workLed.State = LedState.Signaled;
            this._device.Mr761DeviceV2.ConfigDiagnostic.Value.Status = false;
            this.StartDiagnosticBtn.Enabled = true;
            this.StopDiagnosticBtn.Enabled = false;
        }

        private DialogForm.Result CheckDiagnosticState()
        {
            if (this._device.Mr761DeviceV2.ConfigDiagnostic.Value.Status && !this._runDiagnostic)
            {
                DialogForm dialogForm = new DialogForm();
                return dialogForm.ShowDialog();
            }
            return DialogForm.Result.RESET;
        }

        private void StartReadDiagnosticProcess()
        {
            this._counter.Text = 0.ToString();
            this.workLed.State = LedState.NoSignaled;
            this._runDiagnostic = true;
            if (this._th != null && this._th.IsAlive)
            {
                this._th.Abort();
            }
            this._th = new Thread(this.ReadDiagnostic);
            this._th.Start();
            this.StartDiagnosticBtn.Enabled = false;
            this.StopDiagnosticBtn.Enabled = true;
        }

        private void ReadDiagnostic()
        {
            using (this._writer = new XmlTextWriter(Path.GetDirectoryName(Application.ExecutablePath) + "\\MR761\\DiagnosticLog.xml", Encoding.Default))
            {
                this._frames = new Frames();
                this._serializer = new XmlSerializer(typeof (Frames));

                this._writer.Formatting = Formatting.Indented;
                AllDiagnosticDataStruct allDiagnosticData = new AllDiagnosticDataStruct();
                byte[] request = new byte[8];
                request[0] = this._device.DeviceNumber;
                request[1] = 4;
                request[2] = 0x5E;
                request[3] = 0x00;
                request[4] = 0x04;
                request[5] = 0x00;
                ushort crc = CRC16.CalcCrcFast(request, 6);
                request[6] = (byte) (crc >> 8);
                request[7] = (byte) (crc & 0xFF);
                int i = 0;
                Action<int> action = this.IncrementCounter;
                int errQuery = 0;
                while (this._runDiagnostic)
                {
                    byte[] answer = this._device.MB.Port.Opened ? this._device.MB.Port.WriteRead(request, 0x806) as byte[] : null;
                    if (answer == null || Common.TOWORD(answer[2], answer[3]) != (answer.Length - 6)/2)
                    {
                        if (errQuery > 4)
                        {
                            MessageBox.Show("Невозможно прочитать диагностику каналов. Чтение будет остановлено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        errQuery++;
                        continue;
                    }
                    errQuery = 0;
                    Invoke(action, ++i);
                    byte[] buff = Common.SwapArrayItems(answer.Skip(4).Take(0x800).ToArray());
                    allDiagnosticData.InitStruct(buff);
                    allDiagnosticData.SortByError();
                    this._frames.AddFrames(allDiagnosticData, this._device.Mr761DeviceV2.ConfigDiagnosticReadOnly.Value);
                }
                this._serializer.Serialize(this._writer, this._frames);
            }
            this._writer = null;
            this._serializer = null;
        }

        private void IncrementCounter(int i)
        {
            this._counter.Text = i.ToString();
        }

        private void DiagnosticForm_Load(object sender, EventArgs e)
        {
            this.ConfigLoaded = false;
            if (!Directory.Exists(Path.GetDirectoryName(Application.ExecutablePath) + "\\MR761"))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(Application.ExecutablePath) + "\\MR761");
            }
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (this._writer != null && this._serializer != null)
            {
                this._serializer.Serialize(this._writer, this._frames);
                if (this._th.IsAlive)
                {
                    this._th.Abort();
                }
                this._runDiagnostic = false;
                //this._writer.Close();
            }
        }

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (MR761); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public Type ClassType
        {
            get { return typeof (DiagnosticForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Диагностика"; }
        }

        #endregion [IFormView Members]
    }
}
