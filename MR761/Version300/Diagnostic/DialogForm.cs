﻿using System;
using System.Windows.Forms;

namespace BEMN.MR761.Version300.Diagnostic
{
    public partial class DialogForm : Form
    {
        public Result DialogResult { get; private set; }
        public DialogForm()
        {
            InitializeComponent();
            this.DialogResult = Result.CANCEL;
        }

        private void OnButtonClick(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if(btn == null) return;

            switch (Convert.ToInt32(btn.Tag))
            {
                case 1:
                    this.DialogResult = Result.CONTINUE;
                    break;
                case 2:
                    this.DialogResult = Result.RESET;
                    break;
                case 3:
                    this.DialogResult = Result.BREAK;
                    break;
                case 4:
                    this.DialogResult = Result.CANCEL;
                    break;
                default:
                    this.DialogResult = Result.CANCEL;
                    break;
            }
            Close();
        }

        public new Result ShowDialog()
        {
            base.ShowDialog();
            return this.DialogResult;
        }

        public enum Result
        {
            CANCEL,CONTINUE,RESET,BREAK
        }
    }
}
