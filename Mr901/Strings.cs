﻿
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BEMN.MR901.Big.Configuration.Structures;
using BEMN.MR901.Big.Configuration.Structures.Defenses.External;
using BEMN.MR901.Big.Configuration.Structures.Ls;
using BEMN.MR901.Big.Configuration.Structures.Osc;
using BEMN.MR901.Big.Configuration.Structures.RelayIndicator;
using BEMN.MR901.Big.Configuration.Structures.Urov;
using BEMN.MR901.Old.Configuration.Structures.Defenses.External;
using BEMN.MR901.Old.Configuration.Structures.RelayInd;
using BEMN.MR901.Old.Configuration.Structures.Urov;

namespace BEMN.MR901
{
    public class Strings
    {
        public static double Version = 0;
        public static string DeviceType;

        /// <summary>
        /// Коэффициенты Kthl(x)
        /// </summary>
        public static List<string> KthKoefs
        {
            get
            {
                return new List<string>
                {
                    "1",
                    "1000"
                };
            }
        }
        public static List<string> UBtypes => new List<string>
        {
            "Одна фаза", "Все фазы", "Одно лин.", "Все лин.","3U0","U2", "Un"
        };

        public static List<string> UMtypes => new List<string>
        {
            "Одна фаза", "Все фазы", "Одно лин.", "Все лин.", "Un"
        };

        public static List<string> Unames => new List<string>
        {
            "U> 1","U> 2", "U< 1","U< 2"
        };

        public static List<string> ConfigIgssh => new List<string>
        {
            "Iд2СШ2","Iд3ПО"
        };

        /// <summary>
        /// Названия индикаторов
        /// </summary>
        public static List<string> DefNames
        {
            get
            {
                return new List<string>
                    {
                        "Iд1 СШ1",
                        "Iд2 СШ2",
                        "Iд3 ПО",
                        "Iд1м СШ1",
                        "Iд2м СШ2",
                        "Iд3м ПО"
                    };
            }
        }

        public static List<string> ChannelsNames
        {
            get
            {
                return new List<string>
                    {
                       "1",
                       "2",
                       "3",
                       "4",
                       "5",
                       "6",
                       "7",
                       "8"
                    };
            }
        }

        public static List<string> ChannelsNamesBig
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= ChannelBigStruct.COUNT; i++)
                {
                    ret.Add(i.ToString("D"));
                }
                return ret;
            }
        }

        

        public static List<string> ChannelsNamesBig213
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 33; i <= ChannelBigStruct.COUNT; i++)
                {
                    ret.Add(i.ToString("D"));
                }
                return ret;
            }
        }

        /// <summary>
        /// Названия каналов c базами
        /// </summary>
        public static List<string> OscChannelWithBaseNames
        {
            get
            {
                List<string> channelNames = new List<string>();
                for (int i = 0; i < AllChannelsWithBase.KANAL_COUNT; i++)
                {
                    channelNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                return channelNames;
            }
        }

        /// <summary>
        /// Названия индикаторов
        /// </summary>
        public static List<string> IndNames
        {
            get
            {
                var indNames = new List<string>();

                for (int i = 0; i < AllIndicatorsStruct.COUNT; i++)
                {
                    indNames.Add((i + 1).ToString());
                }
                return indNames;
            }
        }

        /// <summary>
        /// Названия присоединений
        /// </summary>
        public static List<string> ConnectionNames
        {
            get
            {
                var indNames = new List<string>();


                for (int i = 0; i < AllUrovConnectionStruct.COUNT; i++)
                {
                    indNames.Add("Присоединение " + (i + 1));
                }
                return indNames;
            }
        }

        /// <summary>
        /// Названия присоединений
        /// </summary>
        public static List<string> ConnectionNamesBig
        {
            get
            {
                List<string> indNames = new List<string>();
                for (int i = 1; i <= AllUrovConnectionBigStruct.CurrentUrovConnectionCount; i++)
                {
                    indNames.Add("Присоединение " + i);
                }
                return indNames;
            }
        }

        public static List<string> ExternalnNames
        {
            get
            {
                var indNames = new List<string>();


                for (int i = 0; i < AllExternalDefenseStruct.COUNT; i++)
                {
                    indNames.Add("ВЗ-" + (i + 1));
                }
                return indNames;
            }
        }

        public static List<string> ExternalnNamesBig
        {
            get
            {
                var indNames = new List<string>();


                for (int i = 0; i < AllExternalDefenseBigStruct.CurrentCount; i++)
                {
                    indNames.Add("ВЗ-" + (i + 1));
                }
                return indNames;
            }
        }
        /// <summary>
        /// Названия реле
        /// </summary>
        public static List<string> RelayNames
        {
            get
            {
                var relayNames = new List<string>();

                for (int i = 0; i < AllReleOutputStruct.RELAY_COUNT; i++)
                {
                    relayNames.Add((i + 1).ToString());
                }
                return relayNames;
            }
        }

        /// <summary>
        /// Названия реле
        /// </summary>
        public static List<string> RelayNamesNew
        {
            get
            {
                var relayNames = new List<string>();

                for (int i = 0; i < AllReleBigStruct.CurrentCount; i++)
                {
                    relayNames.Add((i + 1).ToString());
                }
                return relayNames;
            }
        }
        
        /// <summary>
        /// Журнал аварий. Сработавший параметр для внешних защит
        /// </summary>
        public static List<string> AlarmJournalExternalDefenseTriggedOption
        {
            get
            {
                return new List<string>
                    {
                        "СШ 1",
                        "СШ 2",
                        "ПО",
                        "Пр.1",
                        "Пр.2",
                        "Пр.3",
                        "Пр.4",
                        "Пр.5",
                        "Пр.6",
                        "Пр.7",
                        "Пр.8",
                        "Пр.9",
                        "Пр.10",
                        "Пр.11",
                        "Пр.12",
                        "Пр.13",
                        "Пр.14",
                        "Пр.15",
                        "Пр.16",
                        "Пр.17",
                        "Пр.18",
                        "Пр.19",
                        "Пр.20",
                        "Пр.21",
                        "Пр.22",
                        "Пр.23",
                        "Пр.24"
                    };
            }
        }


        /// <summary>
        /// Журнал аварий. Сработавший параметр
        /// </summary>
        public static List<string> AlarmJournalTriggedOption
        {
            get
            {
                return new List<string>
                    {
                        "Iд",       //0
                        "Iдb",      //1
                        "Iдc",      //2
                        "Iд",       //3
                        "Iдb",      //4
                        "Iдc",      //5
                        "Iд",       //6
                        "Iдb",      //7
                        "Iдc",      //8
                        "Iт",       //9
                        "Iтb",      //10
                        "Iтc",      //11
                        "Iт",       //12
                        "Iтb",      //13
                        "Iтc",      //14
                        "Iт",       //15
                        "Iтb",      //16
                        "Iтc",      //17
                        "Iприс.1",  //18
                        "Iприс.2",  //19
                        "Iприс.3",  //20
                        "Iприс.4",  //21
                        "Iприс.5",  //22
                        "Iприс.6",  //23
                        "Iприс.7",  //24
                        "Iприс.8",  //25
                        "Iприс.9",  //26
                        "Iприс.10", //27
                        "Iприс.11", //28
                        "Iприс.12", //29
                        "Iприс.13", //30
                        "Iприс.14", //31
                        "Iприс.15", //32
                        "Iприс.16", //33
                        "",         //34
                        "",         //35
                        ""          //36 СПЛ
                    };
            }
        }
        /// <summary>
        /// Журнал аварий. Сработавший параметр
        /// </summary>
        public static List<string> AlarmJournalTriggedOptionBig
        {
            get
            {
                List<string> ret = new List<string>
                    {
                        "Iд",       //0
                        "Iдb",      //1
                        "Iдc",      //2
                        "Iд",       //3
                        "Iдb",      //4
                        "Iдc",      //5
                        "Iд",       //6
                        "Iдb",      //7
                        "Iдc",      //8
                        "Iт",       //9
                        "Iтb",      //10
                        "Iтc",      //11
                        "Iт",       //12
                        "Iтb",      //13
                        "Iтc",      //14
                        "Iт",       //15
                        "Iтb",      //16
                        "Iтc",      //17
                        "Iприс.1",  //18
                        "Iприс.2",  //19
                        "Iприс.3",  //20
                        "Iприс.4",  //21
                        "Iприс.5",  //22
                        "Iприс.6",  //23
                        "Iприс.7",  //24
                        "Iприс.8",  //25
                        "Iприс.9",  //26
                        "Iприс.10", //27
                        "Iприс.11", //28
                        "Iприс.12", //29
                        "Iприс.13", //30
                        "Iприс.14", //31
                        "Iприс.15", //32
                        "Iприс.16", //33
                        "Iприс.17", //34
                        "Iприс.18", //35
                        "Iприс.19", //36
                        "Iприс.20", //37
                        "Iприс.21", //38
                        "Iприс.22", //39
                        "Iприс.23", //40
                        "Iприс.24", //41
                        "",         //42
                        "",         //43
                        ""          //44 СПЛ
                    };

                if (ConfigurationStructBigV210.DeviceModelType == "A5")
                {
                    int ind = ret.IndexOf("Iприс.21");
                    ret.RemoveAt(ind);
                    ret.RemoveAt(ind);
                    ret.RemoveAt(ind);
                    ret.RemoveAt(ind);
                    ret.Insert(ind, "Un");
                    ret.Insert(ind, "Uc");
                    ret.Insert(ind, "Ub");
                    ret.Insert(ind, "Ua");
                    ret.Add("res1");
                    ret.Add("res2");
                    ret.Add("res3");
                    ret.Add("");//СПЛ
                    ret.Add("Uab");
                    ret.Add("Ubс");
                    ret.Add("Uca");
                    ret.Add("3U0");
                    ret.Add("U1");
                    ret.Add("U2");
                }

                return ret;
            }
        }

        /// <summary>
        /// Ступень (Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalStage 
        {
            get
            {
                return new List<string>
                {
                    "Iд1м СШ1 ",
                    "Iд1м СШ1*",
                    "Iд2м СШ2 ",
                    "Iд2м СШ2*",
                    "Iд3м ПО  ",
                    "Iд3м ПО *",
                    "Iд1  СШ1 ",
                    "Iд1  СШ1*",
                    "Iд2  СШ2 ",
                    "Iд2  СШ2*",
                    "Iд3  ПО  ",
                    "Iд3  ПО *",
                    "I> 1     ",
                    "I> 2     ",
                    "I> 3     ",
                    "I> 4     ",
                    "I> 5     ",
                    "I> 6     ",
                    "I> 7     ",
                    "I> 8     ",
                    "I> 9     ",
                    "I> 10    ",
                    "I> 11    ",
                    "I> 12    ",
                    "I> 13    ",
                    "I> 14    ",
                    "I> 15    ",
                    "I> 16    ",
                    "I> 17    ",
                    "I> 18    ",
                    "I> 19    ",
                    "I> 20    ",
                    "I> 21    ",
                    "I> 22    ",
                    "I> 23    ",
                    "I> 24    ",
                    "I> 25    ",
                    "I> 26    ",
                    "I> 27    ",
                    "I> 28    ",
                    "I> 29    ",
                    "I> 30    ",
                    "I> 31    ",
                    "I> 32    ",
                    "ВЗ-1     ",
                    "ВЗ-2     ",
                    "ВЗ-3     ",
                    "ВЗ-4     ",
                    "ВЗ-5     ",
                    "ВЗ-6     ",
                    "ВЗ-7     ",
                    "ВЗ-8     ",
                    "ВЗ-9     ",
                    "ВЗ-10    ",
                    "ВЗ-11    ",
                    "ВЗ-12    ",
                    "ВЗ-13    ",
                    "ВЗ-14    ",
                    "ВЗ-15    ",
                    "ВЗ-16    ",
                    "СШ1      ",
                    "СШ2      ",
                    "ПО       ",
                    "Прис.1   ",
                    "Прис.2   ",
                    "Прис.3   ",
                    "Прис.4   ",
                    "Прис.5   ",
                    "Прис.6   ",
                    "Прис.7   ",
                    "Прис.8   ",
                    "Прис.9   ",
                    "Прис.10  ",
                    "Прис.11  ",
                    "Прис.12  ",
                    "Прис.13  ",
                    "Прис.14  ",
                    "Прис.15  ",
                    "Прис.16  ",
                    "внеш.СШ1 ",
                    "внеш.СШ2 ",
                    "внеш.ПО  ",
                    "ЖА СПЛ   ",
                    "Пуск от дискрета",
                    "Пуск от меню",
                    "Пуск от СДТУ"
                };
            }
        }
        
        /// <summary>
        /// Ступень (Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalStageBig
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Iд1м СШ1 ",
                    "Iд1м СШ1*",
                    "Iд2м СШ2 ",
                    "Iд2м СШ2*",
                    "Iд3м ПО  ",
                    "Iд3м ПО *",
                    "Iд1  СШ1 ",
                    "Iд1  СШ1*",
                    "Iд2  СШ2 ",
                    "Iд2  СШ2*",
                    "Iд3  ПО  ",
                    "Iд3  ПО *",
                    "I> 1     ",
                    "I> 2     ",
                    "I> 3     ",
                    "I> 4     ",
                    "I> 5     ",
                    "I> 6     ",
                    "I> 7     ",
                    "I> 8     ",
                    "I> 9     ",
                    "I> 10    ",
                    "I> 11    ",
                    "I> 12    ",
                    "I> 13    ",
                    "I> 14    ",
                    "I> 15    ",
                    "I> 16    ",
                    "I> 17    ",
                    "I> 18    ",
                    "I> 19    ",
                    "I> 20    ",
                    "I> 21    ",
                    "I> 22    ",
                    "I> 23    ",
                    "I> 24    ",
                    "I> 25    ",
                    "I> 26    ",
                    "I> 27    ",
                    "I> 28    ",
                    "I> 29    ",
                    "I> 30    ",
                    "I> 31    ",
                    "I> 32    ",
                    "ВЗ-1     ",
                    "ВЗ-2     ",
                    "ВЗ-3     ",
                    "ВЗ-4     ",
                    "ВЗ-5     ",
                    "ВЗ-6     ",
                    "ВЗ-7     ",
                    "ВЗ-8     ",
                    "ВЗ-9     ",
                    "ВЗ-10    ",
                    "ВЗ-11    ",
                    "ВЗ-12    ",
                    "ВЗ-13    ",
                    "ВЗ-14    ",
                    "ВЗ-15    ",
                    "ВЗ-16    ",
                    "ВЗ-17    ",
                    "ВЗ-18    ",
                    "ВЗ-19    ",
                    "ВЗ-20    ",
                    "ВЗ-21    ",
                    "ВЗ-22    ",
                    "ВЗ-23    ",
                    "ВЗ-24    ",
                    "СШ1      ",
                    "СШ2      ",
                    "ПО       ",
                    "Прис.1   ",
                    "Прис.2   ",
                    "Прис.3   ",
                    "Прис.4   ",
                    "Прис.5   ",
                    "Прис.6   ",
                    "Прис.7   ",
                    "Прис.8   ",
                    "Прис.9   ",
                    "Прис.10  ",
                    "Прис.11  ",
                    "Прис.12  ",
                    "Прис.13  ",
                    "Прис.14  ",
                    "Прис.15  ",
                    "Прис.16  ",
                    "Прис.17  ",
                    "Прис.18  ",
                    "Прис.19  ",
                    "Прис.20  ",
                    "Прис.21  ",
                    "Прис.22  ",
                    "Прис.23  ",
                    "Прис.24  ",
                    "внеш.СШ1 ",
                    "внеш.СШ2 ",
                    "внеш.ПО  ",
                    "ЖА СПЛ   ",
                    "Пуск от дискрета",
                    "Пуск от меню",
                    "Пуск от СДТУ"
                };

                if (ConfigurationStructBigV210.DeviceModelType == "A5")
                {
                    int index = ret.IndexOf("ВЗ-21    ");
                    for (int i = index; i < index + 4; i++)
                    {
                        ret.RemoveAt(index);
                    }
                    ret.Insert(index, "U<<");
                    ret.Insert(index, "U<");
                    ret.Insert(index, "U>>");
                    ret.Insert(index, "U>");
                }

                return ret;
            }
        }

        /// <summary>
        ///Сообщения (Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalMessage
        {
            get
            {
                return new List<string>
                    {
                        "АВАРИЯ",
                        "СИГНАЛ-ЦИЯ",
                        "РАБОТА",
                        "ОТКЛЮЧЕНИЕ",
                        "УРОВ 1",
                        "УРОВ 2",
                        "УРОВ 3",
                        "УРОВ",
                        "АВАРИЯ",
                        "ЛОГИКА",
                        "СООБЩЕНИЕ"
                    };
            }
        }
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Основная",
                        "Резервная"
                    };
            }
        }
        
        public static List<string> InputSignals
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Д1 ",
                        "Д1 Инв",
                        "Д2 ",
                        "Д2 Инв",
                        "Д3 ",
                        "Д3 Инв",
                        "Д4 ",
                        "Д4 Инв",
                        "Д5 ",
                        "Д5 Инв",
                        "Д6 ",
                        "Д6 Инв",
                        "Д7 ",
                        "Д7 Инв",
                        "Д8 ",
                        "Д8 Инв",
                        "Д9 ",
                        "Д9 Инв",
                        "Д10 ",
                        "Д10 Инв",
                        "Д11 ",
                        "Д11 Инв",
                        "Д12 ",
                        "Д12 Инв",
                        "Д13",
                        "Д13 Инв",
                        "Д14 ",
                        "Д14 Инв",
                        "Д15 ",
                        "Д15 Инв",
                        "Д16 ",
                        "Д16 Инв",
                        "Д17 ",
                        "Д17 Инв",
                        "Д18 ",
                        "Д18 Инв",
                        "Д19 ",
                        "Д19 Инв",
                        "Д20 ",
                        "Д20 Инв",
                        "Д21 ",
                        "Д21 Инв",
                        "Д22 ",
                        "Д22 Инв",
                        "Д23 ",
                        "Д23 Инв",
                        "Д24 ",
                        "Д24 Инв",
                        "ЛС1 ",
                        "ЛС1 Инв",
                        "ЛС2 ",
                        "ЛС2 Инв",
                        "ЛС3 ",
                        "ЛС3 Инв",
                        "ЛС4 ",
                        "ЛС4 Инв",
                        "ЛС5 ",
                        "ЛС5 Инв",
                        "ЛС6 ",
                        "ЛС6 Инв",
                        "ЛС7 ",
                        "ЛС7 Инв",
                        "ЛС8 ",
                        "ЛС8 Инв",
                        "ЛС9 ",
                        "ЛС9 Инв",
                        "ЛС10 ",
                        "ЛС10 Инв",
                        "ЛС11 ",
                        "ЛС11 Инв",
                        "ЛС12 ",
                        "ЛС12 Инв",
                        "ЛС13",
                        "ЛС13 Инв",
                        "ЛС14 ",
                        "ЛС14 Инв",
                        "ЛС15 ",
                        "ЛС15 Инв",
                        "ЛС16 ",
                        "ЛС16 Инв",
                        "ВЛС1 ",
                        "ВЛС1 Инв",
                        "ВЛС2 ",
                        "ВЛС2 Инв",
                        "ВЛС3 ",
                        "ВЛС3 Инв",
                        "ВЛС4 ",
                        "ВЛС4 Инв",
                        "ВЛС5 ",
                        "ВЛС5 Инв",
                        "ВЛС6 ",
                        "ВЛС6 Инв",
                        "ВЛС7 ",
                        "ВЛС7 Инв",
                        "ВЛС8 ",
                        "ВЛС8 Инв",
                        "ВЛС9 ",
                        "ВЛС9 Инв",
                        "ВЛС10 ",
                        "ВЛС10 Инв",
                        "ВЛС11 ",
                        "ВЛС11 Инв",
                        "ВЛС12 ",
                        "ВЛС12 Инв",
                        "ВЛС13",
                        "ВЛС13 Инв",
                        "ВЛС14 ",
                        "ВЛС14 Инв",
                        "ВЛС15 ",
                        "ВЛС15 Инв",
                        "ВЛС16 ",
                        "ВЛС16 Инв"
                    };
            }
        }


        public static List<string> ModesLight
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Введено"
                    };
            }
        }

        public static List<string> TtFault
        {
            get
            {
                return new List<string>
                    {
                        "Выведен",
                        "Неисправность",
                        "Блок+Неисправность"
                    };
            }
        }
        
        public static List<string> ResetTT
        {
            get
            {
                return new List<string>
                    {
                        "Ручной",
                        "Автомат."
                    };
            }
        }

        public static List<string> KONTR
        {
            get
            {
                return new List<string>
                    {
                        "По току",
                        "Блок-Кон+Ток"
                    };
            }
        }

        public static List<string> Forbidden
        {
            get
            {
                return new List<string>
                    {
                        "Запрещено",
                        "Разрешено"
                    };
            }
        }

        public static List<string> YesNo
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Есть"
                    };
            }
        }


        public static List<string> Mode
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Введено",
                        "Сигнализация",
                        "Отключение"
                    };
            }
        }


        public static List<string> ModesLightOsc
        {
            get
            {
                return new List<string>
                    {
                        "Выведено",
                        "Пуск по ИО",
                        "Пуск по защите"
                    };
            }
        }
        
        public static List<string> Characteristic
        {
            get
            {
                return new List<string>
                    {
                        "Независимая",
                        "Зависимая"
                    };
            }
        }

        public static List<string> SignalSrab
        {
            get
            {
                List<string> retList = new List<string>
                {
                    "Нет",
                    "Д1",
                    "Д1 Инв",
                    "Д2",
                    "Д2 Инв",
                    "Д3",
                    "Д3 Инв",
                    "Д4",
                    "Д4 Инв",
                    "Д5",
                    "Д5 Инв",
                    "Д6",
                    "Д6 Инв",
                    "Д7",
                    "Д7 Инв",
                    "Д8",
                    "Д8 Инв",
                    "Д9",
                    "Д9 Инв",
                    "Д10",
                    "Д10 Инв",
                    "Д11",
                    "Д11 Инв",
                    "Д12",
                    "Д12 Инв",
                    "Д13",
                    "Д13 Инв",
                    "Д14",
                    "Д14 Инв",
                    "Д15",
                    "Д15 Инв",
                    "Д16",
                    "Д16 Инв",
                    "Д17",
                    "Д17 Инв",
                    "Д18",
                    "Д18 Инв",
                    "Д19",
                    "Д19 Инв",
                    "Д20",
                    "Д20 Инв",
                    "Д21",
                    "Д21 Инв",
                    "Д22",
                    "Д22 Инв",
                    "Д23",
                    "Д23 Инв",
                    "Д24",
                    "Д24 Инв",
                    "ЛС1",
                    "ЛС1 Инв",
                    "ЛС2",
                    "ЛС2 Инв",
                    "ЛС3",
                    "ЛС3 Инв",
                    "ЛС4",
                    "ЛС4 Инв",
                    "ЛС5",
                    "ЛС5 Инв",
                    "ЛС6",
                    "ЛС6 Инв",
                    "ЛС7",
                    "ЛС7 Инв",
                    "ЛС8",
                    "ЛС8 Инв",
                    "ЛС9",
                    "ЛС9 Инв",
                    "ЛС10",
                    "ЛС10 Инв",
                    "ЛС11",
                    "ЛС11 Инв",
                    "ЛС12",
                    "ЛС12 Инв",
                    "ЛС13",
                    "ЛС13 Инв",
                    "ЛС14",
                    "ЛС14 Инв",
                    "ЛС15",
                    "ЛС15 Инв",
                    "ЛС16",
                    "ЛС16 Инв",
                    "ВЛС1",
                    "ВЛС1 Инв",
                    "ВЛС2",
                    "ВЛС2 Инв",
                    "ВЛС3",
                    "ВЛС3 Инв",
                    "ВЛС4",
                    "ВЛС4 Инв",
                    "ВЛС5",
                    "ВЛС5 Инв",
                    "ВЛС6",
                    "ВЛС6 Инв",
                    "ВЛС7",
                    "ВЛС7 Инв",
                    "ВЛС8",
                    "ВЛС8 Инв",
                    "ВЛС9",
                    "ВЛС9 Инв",
                    "ВЛС10",
                    "ВЛС10 Инв",
                    "ВЛС11",
                    "ВЛС11 Инв",
                    "ВЛС12",
                    "ВЛС12 Инв",
                    "ВЛС13",
                    "ВЛС13 Инв",
                    "ВЛС14",
                    "ВЛС14 Инв",
                    "ВЛС15",
                    "ВЛС15 Инв",
                    "ВЛС16",
                    "ВЛС16 Инв",
                    "Iд1м СШ1 *",
                    "Iд1м СШ1 * Инв",
                    "Iд1м СШ1",
                    "Iд1м СШ1 Инв",
                    "Iд2м СШ2 *",
                    "Iд2м СШ2 * Инв",
                    "Iд2м СШ2",
                    "Iд2м СШ2 Инв",
                    "Iд3м ПО *",
                    "Iд3м ПО * Инв",
                    "Iд3м ПО",
                    "Iд3м ПО Инв",
                    "Iд1 СШ1 ИО",
                    "Iд1 СШ1 ИО Инв",
                    "Iд1 СШ1 *",
                    "Iд1 СШ1 * Инв",
                    "Iд1 СШ1",
                    "Iд1 СШ1 Инв",
                    "Iд2 СШ2 ИО",
                    "Iд2 СШ2 ИО Инв",
                    "Iд2 СШ2 *",
                    "Iд2 СШ2 * Инв",
                    "Iд2 СШ2",
                    "Iд2 СШ2 Инв",
                    "Iд3 ПО ИО",
                    "Iд3 ПО ИО Инв",
                    "Iд3 ПО *",
                    "Iд3 ПО * Инв",
                    "Iд3 ПО",
                    "Iд3 ПО Инв",
                    "I1> ИО",
                    "I1> ИО Инв",
                    "I1>",
                    "I1> Инв",
                    "I2> ИО",
                    "I2> ИО Инв",
                    "I2>",
                    "I2> Инв",
                    "I3> ИО",
                    "I3> ИО Инв",
                    "I3>",
                    "I3> Инв",
                    "I4> ИО",
                    "I4> ИО Инв",
                    "I4>",
                    "I4> Инв",
                    "I5> ИО",
                    "I5> ИО Инв",
                    "I5>",
                    "I5> Инв",
                    "I6> ИО",
                    "I6> ИО Инв",
                    "I6>",
                    "I6> Инв",
                    "I7> ИО",
                    "I7> ИО Инв",
                    "I7>",
                    "I7> Инв",
                    "I8> ИО",
                    "I8> ИО Инв",
                    "I8>",
                    "I8> Инв",
                    "I9> ИО",
                    "I9> ИО Инв",
                    "I9>",
                    "I9> Инв",
                    "I10> ИО",
                    "I10> ИО Инв",
                    "I10>",
                    "I10> Инв",
                    "I11> ИО",
                    "I11> ИО Инв",
                    "I11>",
                    "I11> Инв",
                    "I12> ИО",
                    "I12> ИО Инв",
                    "I12>",
                    "I12> Инв",
                    "I13> ИО",
                    "I13> ИО Инв",
                    "I13>",
                    "I13> Инв",
                    "I14> ИО",
                    "I14> ИО Инв",
                    "I14>",
                    "I14> Инв",
                    "I15> ИО",
                    "I15> ИО Инв",
                    "I15>",
                    "I15> Инв",
                    "I16> ИО",
                    "I16> ИО Инв",
                    "I16>",
                    "I16> Инв",
                    "I17> ИО",
                    "I17> ИО Инв",
                    "I17>",
                    "I17> Инв",
                    "I18> ИО",
                    "I18> ИО Инв",
                    "I18>",
                    "I18> Инв",
                    "I19> ИО",
                    "I19> ИО Инв",
                    "I19>",
                    "I19> Инв",
                    "I20> ИО",
                    "I20> ИО Инв",
                    "I20>",
                    "I20> Инв",
                    "I21> ИО",
                    "I21> ИО Инв",
                    "I21>",
                    "I21> Инв",
                    "I22> ИО",
                    "I22> Инв",
                    "I22>",
                    "I22> Инв",
                    "I23> ИО",
                    "I23> ИО Инв",
                    "I23>",
                    "I23> Инв",
                    "I24> ИО",
                    "I24> ИО Инв",
                    "I24>",
                    "I24> Инв",
                    "I25> ИО",
                    "I25> ИО Инв",
                    "I25>",
                    "I25> Инв",
                    "I26> ИО",
                    "I26> ИО Инв",
                    "I26>",
                    "I26> Инв",
                    "I27> ИО",
                    "I27> ИО Инв",
                    "I27>",
                    "I27> Инв",
                    "I28> ИО",
                    "I28> ИО Инв",
                    "I28>",
                    "I28> Инв",
                    "I29> ИО",
                    "I29> ИО Инв",
                    "I29>",
                    "I29> Инв",
                    "I30> ИО",
                    "I30> ИО Инв",
                    "I30>",
                    "I30> Инв",
                    "I31> ИО",
                    "I31> ИО Инв",
                    "I31>",
                    "I31> Инв",
                    "I32> ИО",
                    "I32> ИО Инв",
                    "I32>",
                    "I32> Инв",
                    "ВНЕШ. 1",
                    "ВНЕШ. 1 Инв",
                    "ВНЕШ. 2",
                    "ВНЕШ. 2 Инв",
                    "ВНЕШ. 3",
                    "ВНЕШ. 3 Инв",
                    "ВНЕШ. 4",
                    "ВНЕШ. 4 Инв",
                    "ВНЕШ. 5",
                    "ВНЕШ. 5 Инв",
                    "ВНЕШ. 6",
                    "ВНЕШ. 6 Инв",
                    "ВНЕШ. 7",
                    "ВНЕШ. 7 Инв",
                    "ВНЕШ. 8",
                    "ВНЕШ. 8 Инв",
                    "ВНЕШ. 9",
                    "ВНЕШ. 9 Инв",
                    "ВНЕШ. 10",
                    "ВНЕШ. 10 Инв",
                    "ВНЕШ. 11",
                    "ВНЕШ. 11 Инв",
                    "ВНЕШ. 12",
                    "ВНЕШ. 12 Инв",
                    "ВНЕШ. 13",
                    "ВНЕШ. 13 Инв",
                    "ВНЕШ. 14",
                    "ВНЕШ. 14 Инв",
                    "ВНЕШ. 15",
                    "ВНЕШ. 15 Инв",
                    "ВНЕШ. 16",
                    "ВНЕШ. 16 Инв",
                    "ССЛ1",
                    "ССЛ1. Инв",
                    "ССЛ2",
                    "ССЛ2. Инв",
                    "ССЛ3",
                    "ССЛ3. Инв",
                    "ССЛ4",
                    "ССЛ4. Инв",
                    "ССЛ5",
                    "ССЛ5. Инв",
                    "ССЛ6",
                    "ССЛ6. Инв",
                    "ССЛ7",
                    "ССЛ7. Инв",
                    "ССЛ8",
                    "ССЛ8. Инв",
                    "ССЛ9",
                    "ССЛ9. Инв",
                    "ССЛ10",
                    "ССЛ10. Инв",
                    "ССЛ11",
                    "ССЛ11. Инв",
                    "ССЛ12",
                    "ССЛ12. Инв",
                    "ССЛ13",
                    "ССЛ13. Инв",
                    "ССЛ14",
                    "ССЛ14. Инв",
                    "ССЛ15",
                    "ССЛ15. Инв",
                    "ССЛ16",
                    "ССЛ16. Инв",
                    "ССЛ17",
                    "ССЛ17. Инв",
                    "ССЛ18",
                    "ССЛ18. Инв",
                    "ССЛ19",
                    "ССЛ19. Инв",
                    "ССЛ20",
                    "ССЛ20. Инв",
                    "ССЛ21",
                    "ССЛ21. Инв",
                    "ССЛ22",
                    "ССЛ22. Инв",
                    "ССЛ23",
                    "ССЛ23. Инв",
                    "ССЛ24",
                    "ССЛ24. Инв",
                    "ССЛ25",
                    "ССЛ25. Инв",
                    "ССЛ26",
                    "ССЛ26. Инв",
                    "ССЛ27",
                    "ССЛ27. Инв",
                    "ССЛ28",
                    "ССЛ28. Инв",
                    "ССЛ29",
                    "ССЛ29. Инв",
                    "ССЛ30",
                    "ССЛ30. Инв",
                    "ССЛ31",
                    "ССЛ31. Инв",
                    "ССЛ32",
                    "ССЛ32. Инв",
                    "УРОВ СШ1",
                    "УРОВ СШ1 Инв",
                    "УРОВ СШ2",
                    "УРОВ СШ2 Инв",
                    "УРОВ ПО",
                    "УРОВ ПО Инв",
                    "УРОВ Пр1",
                    "УРОВ Пр1 Инв",
                    "УРОВ Пр2",
                    "УРОВ Пр2 Инв",
                    "УРОВ Пр3",
                    "УРОВ Пр3 Инв",
                    "УРОВ Пр4",
                    "УРОВ Пр4 Инв",
                    "УРОВ Пр5",
                    "УРОВ Пр5 Инв",
                    "УРОВ Пр6",
                    "УРОВ Пр6 Инв",
                    "УРОВ Пр7",
                    "УРОВ Пр7 Инв",
                    "УРОВ Пр8",
                    "УРОВ Пр8 Инв",
                    "УРОВ Пр9",
                    "УРОВ Пр9 Инв",
                    "УРОВ Пр10",
                    "УРОВ Пр10 Инв",
                    "УРОВ Пр11",
                    "УРОВ Пр11 Инв",
                    "УРОВ Пр12",
                    "УРОВ Пр12 Инв",
                    "УРОВ Пр13",
                    "УРОВ Пр13 Инв",
                    "УРОВ Пр14",
                    "УРОВ Пр14 Инв",
                    "УРОВ Пр15",
                    "УРОВ Пр15 Инв",
                    "УРОВ Пр16",
                    "УРОВ Пр16 Инв",
                    "Откл.СШ1",
                    "Откл.СШ1 Инв",
                    "Откл.СШ2",
                    "Откл.СШ2 Инв",
                    "Откл.ПО",
                    "Откл.ПО Инв",
                    "Откл.Пр1",
                    "Откл.Пр1 Инв",
                    "Откл.Пр2",
                    "Откл.Пр2 Инв",
                    "Откл.Пр3",
                    "Откл.Пр3 Инв",
                    "Откл.Пр4",
                    "Откл.Пр4 Инв",
                    "Откл.Пр5",
                    "Откл.Пр5 Инв",
                    "Откл.Пр6",
                    "Откл.Пр6 Инв",
                    "Откл.Пр7",
                    "Откл.Пр7 Инв",
                    "Откл.Пр8",
                    "Откл.Пр8 Инв",
                    "Откл.Пр9",
                    "Откл.Пр9 Инв",
                    "Откл.Пр10",
                    "Откл.Пр10 Инв",
                    "Откл.Пр11",
                    "Откл.Пр11 Инв",
                    "Откл.Пр12",
                    "Откл.Пр12 Инв",
                    "Откл.Пр13",
                    "Откл.Пр13 Инв",
                    "Откл.Пр14",
                    "Откл.Пр14 Инв",
                    "Откл.Пр15",
                    "Откл.Пр15 Инв",
                    "Откл.Пр16",
                    "Откл.Пр16 Инв",
                    "НЕИСПР.",
                    "НЕИСПР Инв",
                    "ОСН.ГРУППА",
                    "ОСН.ГРУППА Инв",
                    "РЕЗ.ГРУППА",
                    "РЕЗ.ГРУППА Инв",
                    "АВАРИЯ",
                    "АВАРИЯ Инв",
                    "СИГНАЛ-ЦИЯ",
                    "СИГНАЛ-ЦИЯ Инв"
                };
                if (Version >= 2.03)
                {
                    retList.AddRange(new[]
                    {
                        "НЕИСПР. ТТ ОБЩ.",
                        "НЕИСПР. ТТ ОБЩ. Инв",
                        "НЕИСПР. ТТ СШ1",
                        "НЕИСПР. ТТ СШ1 Инв",
                        "НЕИСПР. ТТ СШ2",
                        "НЕИСПР. ТТ СШ2 Инв"
                    });
                }
                return retList;
            }
        }

        public static List<string> SignalSrabExternal
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Д1",
                        "Д1 Инв",
                        "Д2",
                        "Д2 Инв",
                        "Д3",
                        "Д3 Инв",
                        "Д4",
                        "Д4 Инв",
                        "Д5",
                        "Д5 Инв",
                        "Д6",
                        "Д6 Инв",
                        "Д7",
                        "Д7 Инв",
                        "Д8",
                        "Д8 Инв",
                        "Д9",
                        "Д9 Инв",
                        "Д10",
                        "Д10 Инв",
                        "Д11",
                        "Д11 Инв",
                        "Д12",
                        "Д12 Инв",
                        "Д13",
                        "Д13 Инв",
                        "Д14",
                        "Д14 Инв",
                        "Д15",
                        "Д15 Инв",
                        "Д16",
                        "Д16 Инв",
                        "Д17",
                        "Д17 Инв",
                        "Д18",
                        "Д18 Инв",
                        "Д19",
                        "Д19 Инв",
                        "Д20",
                        "Д20 Инв",
                        "Д21",
                        "Д21 Инв",
                        "Д22",
                        "Д22 Инв",
                        "Д23",
                        "Д23 Инв",
                        "Д24",
                        "Д24 Инв",
                        "ЛС1",
                        "ЛС1 Инв",
                        "ЛС2",
                        "ЛС2 Инв",
                        "ЛС3",
                        "ЛС3 Инв",
                        "ЛС4",
                        "ЛС4 Инв",
                        "ЛС5",
                        "ЛС5 Инв",
                        "ЛС6",
                        "ЛС6 Инв",
                        "ЛС7",
                        "ЛС7 Инв",
                        "ЛС8",
                        "ЛС8 Инв",
                        "ЛС9",
                        "ЛС9 Инв",
                        "ЛС10",
                        "ЛС10 Инв",
                        "ЛС11",
                        "ЛС11 Инв",
                        "ЛС12",
                        "ЛС12 Инв",
                        "ЛС13",
                        "ЛС13 Инв",
                        "ЛС14",
                        "ЛС14 Инв",
                        "ЛС15",
                        "ЛС15 Инв",
                        "ЛС16",
                        "ЛС16 Инв",
                        "ВЛС1",
                        "ВЛС1 Инв",
                        "ВЛС2",
                        "ВЛС2 Инв",
                        "ВЛС3",
                        "ВЛС3 Инв",
                        "ВЛС4",
                        "ВЛС4 Инв",
                        "ВЛС5",
                        "ВЛС5 Инв",
                        "ВЛС6",
                        "ВЛС6 Инв",
                        "ВЛС7",
                        "ВЛС7 Инв",
                        "ВЛС8",
                        "ВЛС8 Инв",
                        "ВЛС9",
                        "ВЛС9 Инв",
                        "ВЛС10",
                        "ВЛС10 Инв",
                        "ВЛС11",
                        "ВЛС11 Инв",
                        "ВЛС12",
                        "ВЛС12 Инв",
                        "ВЛС13",
                        "ВЛС13 Инв",
                        "ВЛС14",
                        "ВЛС14 Инв",
                        "ВЛС15",
                        "ВЛС15 Инв",
                        "ВЛС16",
                        "ВЛС16 Инв",
                        "Iд1м СШ1 *",
                        "Iд1м СШ1 * Инв",
                        "Iд1м СШ1",
                        "Iд1м СШ1 Инв",
                        "Iд2м СШ2 *",
                        "Iд2м СШ2 * Инв",
                        "Iд2м СШ2",
                        "Iд2м СШ2 Инв",
                        "Iд3м ПО *",
                        "Iд3м ПО * Инв",
                        "Iд3м ПО",
                        "Iд3м ПО Инв",
                        "Iд1 СШ1 ИО",
                        "Iд1 СШ1 ИО Инв",
                        "Iд1 СШ1 *",
                        "Iд1 СШ1 * Инв",
                        "Iд1 СШ1",
                        "Iд1 СШ1 Инв",
                        "Iд2 СШ2 ИО",
                        "Iд2 СШ2 ИО Инв",
                        "Iд2 СШ2 *",
                        "Iд2 СШ2 * Инв",
                        "Iд2 СШ2",
                        "Iд2 СШ2 Инв",
                        "Iд3 ПО ИО",
                        "Iд3 ПО ИО Инв",
                        "Iд3 ПО *",
                        "Iд3 ПО * Инв",
                        "Iд3 ПО",
                        "Iд3 ПО Инв",
                        "I1> ИО",
                        "I1> ИО Инв",
                        "I1>",
                        "I1> Инв",
                        "I2> ИО",
                        "I2> ИО Инв",
                        "I2>",
                        "I2> Инв",
                        "I3> ИО",
                        "I3> ИО Инв",
                        "I3>",
                        "I3> Инв",
                        "I4> ИО",
                        "I4> ИО Инв",
                        "I4>",
                        "I4> Инв",
                        "I5> ИО",
                        "I5> ИО Инв",
                        "I5>",
                        "I5> Инв",
                        "I6> ИО",
                        "I6> ИО Инв",
                        "I6>",
                        "I6> Инв",
                        "I7> ИО",
                        "I7> ИО Инв",
                        "I7>",
                        "I7> Инв",
                        "I8> ИО",
                        "I8> ИО Инв",
                        "I8>",
                        "I8> Инв",
                        "I9> ИО",
                        "I9> ИО Инв",
                        "I9>",
                        "I9> Инв",
                        "I10> ИО",
                        "I10> ИО Инв",
                        "I10>",
                        "I10> Инв",
                        "I11> ИО",
                        "I11> ИО Инв",
                        "I11>",
                        "I11> Инв",
                        "I12> ИО",
                        "I12> ИО Инв",
                        "I12>",
                        "I12> Инв",
                        "I13> ИО",
                        "I13> ИО Инв",
                        "I13>",
                        "I13> Инв",
                        "I14> ИО",
                        "I14> ИО Инв",
                        "I14>",
                        "I14> Инв",
                        "I15> ИО",
                        "I15> ИО Инв",
                        "I15>",
                        "I15> Инв",
                        "I16> ИО",
                        "I16> ИО Инв",
                        "I16>",
                        "I16> Инв",
                        "I17> ИО",
                        "I17> ИО Инв",
                        "I17>",
                        "I17> Инв",
                        "I18> ИО",
                        "I18> ИО Инв",
                        "I18>",
                        "I18> Инв",
                        "I19> ИО",
                        "I19> ИО Инв",
                        "I19>",
                        "I19> Инв",
                        "I20> ИО",
                        "I20> ИО Инв",
                        "I20>",
                        "I20> Инв",
                        "I21> ИО",
                        "I21> ИО Инв",
                        "I21>",
                        "I21> Инв",
                        "I22> ИО",
                        "I22> Инв",
                        "I22>",
                        "I22> Инв",
                        "I23> ИО",
                        "I23> ИО Инв",
                        "I23>",
                        "I23> Инв",
                        "I24> ИО",
                        "I24> ИО Инв",
                        "I24>",
                        "I24> Инв",
                        "I25> ИО",
                        "I25> ИО Инв",
                        "I25>",
                        "I25> Инв",
                        "I26> ИО",
                        "I26> ИО Инв",
                        "I26>",
                        "I26> Инв",
                        "I27> ИО",
                        "I27> ИО Инв",
                        "I27>",
                        "I27> Инв",
                        "I28> ИО",
                        "I28> ИО Инв",
                        "I28>",
                        "I28> Инв",
                        "I29> ИО",
                        "I29> ИО Инв",
                        "I29>",
                        "I29> Инв",
                        "I30> ИО",
                        "I30> ИО Инв",
                        "I30>",
                        "I30> Инв",
                        "I31> ИО",
                        "I31> ИО Инв",
                        "I31>",
                        "I31> Инв",
                        "I32> ИО",
                        "I32> ИО Инв",
                        "I32>",
                        "I32> Инв"
                    };
            }
        }

        public static List<string> SignalType
        {
            get
            {
                return new List<string>
                    {
                        "Повторитель",
                        "Блинкер"
                    };
            }
        }

        public static List<string> VLSSignals
        {
            get
            {
                List<string> retList = new List<string>
                {
                    "Д1",
                    "Д2",
                    "Д3",
                    "Д4",
                    "Д5",
                    "Д6",
                    "Д7",
                    "Д8",
                    "Д9",
                    "Д10",
                    "Д11",
                    "Д12",
                    "Д13",
                    "Д14",
                    "Д15",
                    "Д16",
                    "Д17",
                    "Д18",
                    "Д19",
                    "Д20",
                    "Д21",
                    "Д22",
                    "Д23",
                    "Д24",
                    "ЛС1",
                    "ЛС2",
                    "ЛС3",
                    "ЛС4",
                    "ЛС5",
                    "ЛС6",
                    "ЛС7",
                    "ЛС8",
                    "ЛС9",
                    "ЛС10",
                    "ЛС11",
                    "ЛС12",
                    "ЛС13",
                    "ЛС14",
                    "ЛС15",
                    "ЛС16",
                    "Iд1м СШ1 *",
                    "Iд1м СШ1",
                    "Iд2м СШ2 *",
                    "Iд2м СШ2",
                    "Iд3м ПО *",
                    "Iд3м ПО",
                    "Iд1 СШ1 ИО",
                    "Iд1 СШ1 *",
                    "Iд1 СШ1",
                    "Iд2 СШ2 ИО",
                    "Iд2 СШ2 *",
                    "Iд2 СШ2",
                    "Iд3 ПО ИО",
                    "Iд3 ПО *",
                    "Iд3 ПО",
                    "I1> ИО",
                    "I1>",
                    "I2> ИО",
                    "I2>",
                    "I3> ИО",
                    "I3>",
                    "I4> ИО",
                    "I4>",
                    "I5> ИО",
                    "I5>",
                    "I6> ИО",
                    "I6>",
                    "I7> ИО",
                    "I7>",
                    "I8> ИО",
                    "I8>",
                    "I9> ИО",
                    "I9>",
                    "I10> ИО",
                    "I10>",
                    "I11> ИО",
                    "I11>",
                    "I12> ИО",
                    "I12>",
                    "I13> ИО",
                    "I13>",
                    "I14> ИО",
                    "I14>",
                    "I15> ИО",
                    "I15>",
                    "I16> ИО",
                    "I16>",
                    "I17> ИО",
                    "I17>",
                    "I18> ИО",
                    "I18>",
                    "I19> ИО",
                    "I19>",
                    "I20> ИО",
                    "I20>",
                    "I21> ИО",
                    "I21>",
                    "I22> ИО",
                    "I22>",
                    "I23> ИО",
                    "I23>",
                    "I24> ИО",
                    "I24>",
                    "I25> ИО",
                    "I25>",
                    "I26> ИО",
                    "I26>",
                    "I27> ИО",
                    "I27>",
                    "I28> ИО",
                    "I28>",
                    "I29> ИО",
                    "I29>",
                    "I30> ИО",
                    "I30>",
                    "I31> ИО",
                    "I31>",
                    "I32> ИО",
                    "I32>",
                    "ВНЕШ. 1",
                    "ВНЕШ. 2",
                    "ВНЕШ. 3",
                    "ВНЕШ. 4",
                    "ВНЕШ. 5",
                    "ВНЕШ. 6",
                    "ВНЕШ. 7",
                    "ВНЕШ. 8",
                    "ВНЕШ. 9",
                    "ВНЕШ. 10",
                    "ВНЕШ. 11",
                    "ВНЕШ. 12",
                    "ВНЕШ. 13",
                    "ВНЕШ. 14",
                    "ВНЕШ. 15",
                    "ВНЕШ. 16",
                    "ССЛ1",
                    "ССЛ2",
                    "ССЛ3",
                    "ССЛ4",
                    "ССЛ5",
                    "ССЛ6",
                    "ССЛ7",
                    "ССЛ8",
                    "ССЛ9",
                    "ССЛ10",
                    "ССЛ11",
                    "ССЛ12",
                    "ССЛ13",
                    "ССЛ14",
                    "ССЛ15",
                    "ССЛ16",
                    "ССЛ17",
                    "ССЛ18",
                    "ССЛ19",
                    "ССЛ20",
                    "ССЛ21",
                    "ССЛ22",
                    "ССЛ23",
                    "ССЛ24",
                    "ССЛ25",
                    "ССЛ26",
                    "ССЛ27",
                    "ССЛ28",
                    "ССЛ29",
                    "ССЛ30",
                    "ССЛ31",
                    "ССЛ32",
                    "УРОВ СШ1",
                    "УРОВ СШ2",
                    "УРОВ ПО",
                    "УРОВ Пр1",
                    "УРОВ Пр2",
                    "УРОВ Пр3",
                    "УРОВ Пр4",
                    "УРОВ Пр5",
                    "УРОВ Пр6",
                    "УРОВ Пр7",
                    "УРОВ Пр8",
                    "УРОВ Пр9",
                    "УРОВ Пр10",
                    "УРОВ Пр11",
                    "УРОВ Пр12",
                    "УРОВ Пр13",
                    "УРОВ Пр14",
                    "УРОВ Пр15",
                    "УРОВ Пр16",
                    "Откл.СШ1",
                    "Откл.СШ2",
                    "Откл.ПО",
                    "Откл.Пр1",
                    "Откл.Пр2",
                    "Откл.Пр3",
                    "Откл.Пр4",
                    "Откл.Пр5",
                    "Откл.Пр6",
                    "Откл.Пр7",
                    "Откл.Пр8",
                    "Откл.Пр9",
                    "Откл.Пр10",
                    "Откл.Пр11",
                    "Откл.Пр12",
                    "Откл.Пр13",
                    "Откл.Пр14",
                    "Откл.Пр15",
                    "Откл.Пр16",
                    "НЕИСПР.",
                    "ОСН.ГРУППА",
                    "РЕЗ.ГРУППА",
                    "АВАРИЯ",
                    "СИГНАЛ-ЦИЯ"
                };
                if (Version >= 2.03)
                {
                    retList.AddRange(new[]
                    {
                        "НЕИСПР. ТТ ОБЩ.",
                        "НЕИСПР. ТТ СШ1",
                        "НЕИСПР. ТТ СШ2",
                    });
                }
                return retList;
            }
        }

        public static List<string> LogicSignalNames
        {
            get
            {
                List<string> ret = new List<string>();
                for (int i = 1; i <= InputLogicBigStruct.DiscrestCount; i++)
                {
                    ret.Add("Д" + i);
                }
                return ret;
            }
        }
        /// <summary>
        /// Сигналы ЛС
        /// </summary>
        public static List<string> LsSignals
        {
            get
            {
                return new List<string>
                    {
                        "Д1",
                        "Д2",
                        "Д3",
                        "Д4",
                        "Д5",
                        "Д6",
                        "Д7",
                        "Д8",
                        "Д9",
                        "Д10",
                        "Д11",
                        "Д12",
                        "Д13",
                        "Д14",
                        "Д15",
                        "Д16",
                        "Д17",
                        "Д18",
                        "Д19",
                        "Д20",
                        "Д21",
                        "Д22",
                        "Д23",
                        "Д24"
                    };
            }
        }

        public static List<string> LogicValues
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "Да",
                        "Инверс"
                    };
            }
        }
        
        public static List<string> Join
        {
            get
            {
                return new List<string>
                    {
                        "Нет",
                        "СШ1",
                        "СШ2",
                        "СВ+СШ1",
                        "СВ+СШ2",
                        "СВ1",
                        "СВ2",
                        "От входа"
                    };
            }
        }
         
        public static List<string> MtzNames
        {
            get
            {
                var res = new List<string>();
                for (int i = 0; i < 32; i++)
                {
                    res.Add("I> " + (i + 1));
                }

                return res;
            }
        }

        public static List<string> TtNames
        {
            get
            {
                return new List<string>
                    {
                        "СШ1",
                        "СШ2",
                        "ПО"
                    };
            }
        }

        public static List<string> Otkl
        {
            get
            {
                return new List<string>
                    {
                        "СШ1",
                        "СШ2",
                        "ПО",
                        "Прис. 1",
                        "Прис. 2",
                        "Прис. 3",
                        "Прис. 4",
                        "Прис. 5",
                        "Прис. 6",
                        "Прис. 7",
                        "Прис. 8",
                        "Прис. 9",
                        "Прис. 10",
                        "Прис. 11",
                        "Прис. 12",
                        "Прис. 13",
                        "Прис. 14",
                        "Прис. 15",
                        "Прис. 16"
                    };
            }
        }

        public static List<string> OtklBig
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "СШ1",
                    "СШ2",
                    "ПО",
                    "Прис. 1",
                    "Прис. 2",
                    "Прис. 3",
                    "Прис. 4",
                    "Прис. 5",
                    "Прис. 6",
                    "Прис. 7",
                    "Прис. 8",
                    "Прис. 9",
                    "Прис. 10",
                    "Прис. 11",
                    "Прис. 12",
                    "Прис. 13",
                    "Прис. 14",
                    "Прис. 15",
                    "Прис. 16",
                    "Прис. 17",
                    "Прис. 18",
                    "Прис. 19",
                    "Прис. 20",
                    "Прис. 21",
                    "Прис. 22",
                    "Прис. 23",
                    "Прис. 24"
                };
                if (ConfigurationStructBigV210.DeviceModelType == "A1" ||
                    ConfigurationStructBigV210.DeviceModelType == string.Empty)
                {
                    for (int i = 19; i < 27; i++)
                    {
                        ret.RemoveAt(19);
                    }
                }
                else if (ConfigurationStructBigV210.DeviceModelType == "A5")
                {
                    for (int i = 23; i < 27; i++)
                    {
                        ret.RemoveAt(23);
                    }
                }
                return ret;
            }
        }

        public static List<string> MTZJoin
        {
            get
            {
                return new List<string>
                {
                    "Прис. 1",
                    "Прис. 2",
                    "Прис. 3",
                    "Прис. 4",
                    "Прис. 5",
                    "Прис. 6",
                    "Прис. 7",
                    "Прис. 8",
                    "Прис. 9",
                    "Прис. 10",
                    "Прис. 11",
                    "Прис. 12",
                    "Прис. 13",
                    "Прис. 14",
                    "Прис. 15",
                    "Прис. 16"
                };
            }
        }

        public static List<string> MTZJoinBig
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Прис. 1",
                    "Прис. 2",
                    "Прис. 3",
                    "Прис. 4",
                    "Прис. 5",
                    "Прис. 6",
                    "Прис. 7",
                    "Прис. 8",
                    "Прис. 9",
                    "Прис. 10",
                    "Прис. 11",
                    "Прис. 12",
                    "Прис. 13",
                    "Прис. 14",
                    "Прис. 15",
                    "Прис. 16",
                    "Прис. 17",
                    "Прис. 18",
                    "Прис. 19",
                    "Прис. 20",
                    "Прис. 21",
                    "Прис. 22",
                    "Прис. 23",
                    "Прис. 24"
                };
                if (DeviceType == "A1" ||
                    DeviceType == string.Empty)
                {
                    for (int i = 16; i < 24; i++)
                    {
                        ret.RemoveAt(16);
                    }
                }
                else if (DeviceType == "A5")
                {
                    for (int i = 20; i < 24; i++)
                    {
                        ret.RemoveAt(20);
                    }
                }
                return ret;
            }
        }
        public static List<string> OscBases
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Б1", "Б2", "Б3"
                };
                return ret;
            }
        }

        public static List<Dictionary<ushort,string>> OscChannelSignals
        {
            get
            {
                List<Dictionary<ushort, string>> ret = new List<Dictionary<ushort, string>>
                {
                    RelaySignalsBig213,
                    Mr901Neispr,
                    Mr901Params
                };
                return ret;
            }
        }
        

        /// <summary>
        /// Сигналы неисправностей
        /// </summary>
        public static Dictionary<ushort, string> Mr901Neispr
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Нет",
                    "Аппаратная неиспр.",
                    "Аппаратная неиспр. Инв.",
                    "Программн. неиспр.",
                    "Программн. неиспр. Инв.",
                    "Измерения ТТ",
                    "Измерения ТТ Инв.",
                    "Уров",
                    "Уров Инв.",
                    "Неисправность мод.1",
                    "Неисправность мод.1 Инв.",
                    "Неисправность мод.2",
                    "Неисправность мод.2 Инв",
                    "Неисправность мод.3",
                    "Неисправность мод.3 Инв.",
                    "Неисправность мод.4",
                    "Неисправность мод.4 Инв.",
                    "Неисправность мод.5",
                    "Неисправность мод.5 Инв.",
                    "Неисправность мод.6",
                    "Неисправность мод.6 Инв.",
                    "Неисправность уставок",
                    "Неисправность уставок Инв.",
                    "Неисправность группы уст.",
                    "Неисправность группы уст. Инв.",
                    "Неисправность пароля",
                    "Неисправность пароля Инв.",
                    "Неисправность ЖС",
                    "Неисправность ЖС Инв.",
                    "Неисправность ЖА",
                    "Неисправность ЖА Инв.",
                    "Неисправность осциллографа",
                    "Неисправность осциллографа Инв.",
                    "CRC - константы",
                    "CRC - константы Инв.",
                    "CRC - разрешение",
                    "CRC - разрешение Инв.",
                    "CRC - программы",
                    "CRC - программы Инв.",
                    "CRC - меню",
                    "CRC - меню Инв.",
                    "Ошибка логики - выполнение",
                    "Ошибка логики - выполнение Инв.",
                    "Неисправность ТТ 1 (СШ1)",
                    "Неисправность ТТ 1 (СШ1) Инв.",
                    "Неисправность ТТ 2 (СШ2)",
                    "Неисправность ТТ 2 (СШ2) Инв.",
                    "Неисправность ТТ 3 (ПО)",
                    "Неисправность ТТ 3 (ПО) Инв.",
                    "Неисправность УРОВ (Работа )",
                    "Неисправность УРОВ (Работа ) Инв.",
                    "Неисправность УРОВ (Блок-конт)",
                    "Неисправность УРОВ (Блок-конт) Инв.",
                    "Резерв1",
                    "Резерв2",
                    "Неисправность TH",
                    "Неисправность TH Инв.",
                    "Неисправность TH: dU",
                    "Неисправность TH: dU Инв.",
                    "Неисправность TH: несим.",
                    "Неисправность TH: несим. Инв.",
                    "Неисправность внеш. Uabc",
                    "Неисправность внеш. Uabc Инв.",
                    "Неисправность Uabc < 5В",
                    "Неисправность Uabc < 5В Инв.",
                    "Неисправность внеш. Un",
                    "Неисправность внеш. Un Инв.",
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                ushort index = ret.First(r => r.Value == "Резерв1").Key;

                for (ushort i = index; i < index + 2; i++)
                {
                    ret.Remove(i);
                }

                return ret.ToDictionary(r=>r.Key, r=>r.Value);
            }
        }

        /// <summary>
        /// Сигналы параметров
        /// </summary>
        public static Dictionary<ushort, string> Mr901Params 
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Нет",
                    "Нас. мгн. СШ1",
                    "Нас. мгн. СШ1 Инв.",
                    "Нас. мгн. СШ2",
                    "Нас. мгн. СШ2 Инв.",
                    "Нас. мгн. ПО",
                    "Нас. мгн. ПО Инв.",
                    "Резерв1",
                    "Резерв2",
                    "Резерв3",
                    "Резерв4",
                    "Резерв5",
                    "Резерв6",
                    "Резерв7",
                    "Резерв8",
                    "Резерв9",
                    "Резерв10",
                    "Резерв11",
                    "Резерв12",
                    "Нас. действ. СШ1",
                    "Нас. действ. СШ1 Инв.",
                    "Нас. действ. СШ2",
                    "Нас. действ. СШ2 Инв.",
                    "Нас. действ. ПО",
                    "Нас. действ. ПО Ин."
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                ushort index = ret.First(r => r.Value == "Резерв1").Key;

                for (ushort i = index; i < index + 12; i++)
                {
                    ret.Remove(i);
                }

                return ret.ToDictionary(r => r.Key, r => r.Value);

            }
        }


        public static List<string> OscLength
        {
            get
            {
                return new List<string>
                {
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "17",
                    "18",
                    "19",
                    "20",
                    "21",
                    "22",
                    "23",
                    "24",
                    "25",
                    "26",
                    "27",
                    "28",
                    "29",
                    "30",
                    "31",
                    "32",
                    "33",
                    "34",
                    "35",
                    "36",
                    "37",
                    "38",
                    "39",
                    "40"
                };
            }
        }

        public static List<string> OscFix
        {
            get
            {
                return new List<string>
                    {
                        "Первой",
                        "Посл."
                    };
            }
        }

        /// <summary>
        /// Названия каналов c базами
        /// </summary>
        public static List<string> OscChannelNames
        {
            get
            {
                List<string> channelNames = new List<string>();
                for (int i = AllChannelsWithBase.KANAL_COUNT; i < ChannelBigStruct213.COUNT + AllChannelsWithBase.KANAL_COUNT; i++)
                {
                    channelNames.Add((i + 1).ToString(CultureInfo.InvariantCulture));
                }
                return channelNames;
            }
        }

        #region New Signals

        public static Dictionary<int, string> VlsSignalsBig
        {
            get
            {
                SortedDictionary<int, string> ret = new SortedDictionary<int, string>
                {
                    {0, "Д1"},
                    {1, "Д2"},
                    {2, "Д3"},
                    {3, "Д4"},
                    {4, "Д5"},
                    {5, "Д6"},
                    {6, "Д7"},
                    {7, "Д8"},
                    {8, "Д9"},
                    {9, "Д10"},
                    {10, "Д11"},
                    {11, "Д12"},
                    {12, "Д13"},
                    {13, "Д14"},
                    {14, "Д15"},
                    {15, "Д16"},
                    {16, "Д17"},
                    {17, "Д18"},
                    {18, "Д19"},
                    {19, "Д20"},
                    {20, "Д21"},
                    {21, "Д22"},
                    {22, "Д23"},
                    {23, "Д24"},
                    {24, "Д25"},
                    {25, "Д26"},
                    {26, "Д27"},
                    {27, "Д28"},
                    {28, "Д29"},
                    {29, "Д30"},
                    {30, "Д31"},
                    {31, "Д32"},
                    {32, "Д33"},
                    {33, "Д34"},
                    {34, "Д35"},
                    {35, "Д36"},
                    {36, "Д37"},
                    {37, "Д38"},
                    {38, "Д39"},
                    {39, "Д40"},
                    {40, "Д41"},
                    {41, "Д42"},
                    {42, "Д43"},
                    {43, "Д44"},
                    {44, "Д45"},
                    {45, "Д46"},
                    {46, "Д47"},
                    {47, "Д48"},
                    {48, "Д49"},
                    {49, "Д50"},
                    {50, "Д51"},
                    {51, "Д52"},
                    {52, "Д53"},
                    {53, "Д54"},
                    {54, "Д55"},
                    {55, "Д56"},
                    {56, "Д57"},
                    {57, "Д58"},
                    {58, "Д59"},
                    {59, "Д60"},
                    {60, "Д61"},
                    {61, "Д62"},
                    {62, "Д63"},
                    {63, "Д64"},
                    {64, "Д65"},
                    {65, "Д66"},
                    {66, "Д67"},
                    {67, "Д68"},
                    {68, "Д69"},
                    {69, "Д70"},
                    {70, "Д71"},
                    {71, "Д72"},
                    {72, "Д73"},
                    {73, "Д74"},
                    {74, "Д75"},
                    {75, "Д76"},
                    {76, "Д77"},
                    {77, "Д78"},
                    {78, "Д79"},
                    {79, "Д80"},
                    {80, "Д81"},
                    {81, "Д82"},
                    {82, "Д83"},
                    {83, "Д84"},
                    {84, "Д85"},
                    {85, "Д86"},
                    {86, "Д87"},
                    {87, "Д88"},
                    {88, "Д89"},
                    {89, "Д90"},
                    {90, "Д91"},
                    {91, "Д92"},
                    {92, "Д93"},
                    {93, "Д94"},
                    {94, "Д95"},
                    {95, "Д96"},
                    {96, "ЛС1"},
                    {97, "ЛС2"},
                    {98, "ЛС3"},
                    {99, "ЛС4"},
                    {100, "ЛС5"},
                    {101, "ЛС6"},
                    {102, "ЛС7"},
                    {103, "ЛС8"},
                    {104, "ЛС9"},
                    {105, "ЛС10"},
                    {106, "ЛС11"},
                    {107, "ЛС12"},
                    {108, "ЛС13"},
                    {109, "ЛС14"},
                    {110, "ЛС15"},
                    {111, "ЛС16"},
                    {112, "БГ1"},
                    {113, "БГ2"},
                    {114, "БГ3"},
                    {115, "БГ4"},
                    {116, "БГ5"},
                    {117, "БГ6"},
                    {118, "БГ7"},
                    {119, "БГ8"},
                    {120, "БГ9"},
                    {121, "БГ10"},
                    {122, "БГ11"},
                    {123, "БГ12"},
                    {124, "БГ13"},
                    {125, "БГ14"},
                    {126, "БГ15"},
                    {127, "БГ16"},
                    {128, "Iд1м СШ1 *"},
                    {129, "Iд1м СШ1"},
                    {130, "Iд2м СШ2 *"},
                    {131, "Iд2м СШ2"},
                    {132, "Iд3м ПО *"},
                    {133, "Iд3м ПО"},
                    {134, "Iд1 СШ1 ИО"},
                    {135, "Iд1 СШ1 *"},
                    {136, "Iд1 СШ1"},
                    {137, "Iд2 СШ2 ИО"},
                    {138, "Iд2 СШ2 *"},
                    {139, "Iд2 СШ2"},
                    {140, "Iд3 ПО ИО"},
                    {141, "Iд3 ПО *"},
                    {142, "Iд3 ПО"},
                    {143, "I1> ИО"},
                    {144, "I1>"},
                    {145, "I2> ИО"},
                    {146, "I2>"},
                    {147, "I3> ИО"},
                    {148, "I3>"},
                    {149, "I4> ИО"},
                    {150, "I4>"},
                    {151, "I5> ИО"},
                    {152, "I5>"},
                    {153, "I6> ИО"},
                    {154, "I6>"},
                    {155, "I7> ИО"},
                    {156, "I7>"},
                    {157, "I8> ИО"},
                    {158, "I8>"},
                    {159, "I9> ИО"},
                    {160, "I9>"},
                    {161, "I10> ИО"},
                    {162, "I10>"},
                    {163, "I11> ИО"},
                    {164, "I11>"},
                    {165, "I12> ИО"},
                    {166, "I12>"},
                    {167, "I13> ИО"},
                    {168, "I13>"},
                    {169, "I14> ИО"},
                    {170, "I14>"},
                    {171, "I15> ИО"},
                    {172, "I15>"},
                    {173, "I16> ИО"},
                    {174, "I16>"},
                    {175, "I17> ИО"},
                    {176, "I17>"},
                    {177, "I18> ИО"},
                    {178, "I18>"},
                    {179, "I19> ИО"},
                    {180, "I19>"},
                    {181, "I20> ИО"},
                    {182, "I20>"},
                    {183, "I21> ИО"},
                    {184, "I21>"},
                    {185, "I22> ИО"},
                    {186, "I22>"},
                    {187, "I23> ИО"},
                    {188, "I23>"},
                    {189, "I24> ИО"},
                    {190, "I24>"},
                    {191, "I25> ИО"},
                    {192, "I25>"},
                    {193, "I26> ИО"},
                    {194, "I26>"},
                    {195, "I27> ИО"},
                    {196, "I27>"},
                    {197, "I28> ИО"},
                    {198, "I28>"},
                    {199, "I29> ИО"},
                    {200, "I29>"},
                    {201, "I30> ИО"},
                    {202, "I30>"},
                    {203, "I31> ИО"},
                    {204, "I31>"},
                    {205, "I32> ИО"},
                    {206, "I32>"},
                    {207, "ВНЕШ. 1"},
                    {208, "ВНЕШ. 2"},
                    {209, "ВНЕШ. 3"},
                    {210, "ВНЕШ. 4"},
                    {211, "ВНЕШ. 5"},
                    {212, "ВНЕШ. 6"},
                    {213, "ВНЕШ. 7"},
                    {214, "ВНЕШ. 8"},
                    {215, "ВНЕШ. 9"},
                    {216, "ВНЕШ. 10"},
                    {217, "ВНЕШ. 11"},
                    {218, "ВНЕШ. 12"},
                    {219, "ВНЕШ. 13"},
                    {220, "ВНЕШ. 14"},
                    {221, "ВНЕШ. 15"},
                    {222, "ВНЕШ. 16"},
                    {223, "ВНЕШ. 17"},
                    {224, "ВНЕШ. 18"},
                    {225, "ВНЕШ. 19"},
                    {226, "ВНЕШ. 20"},
                    {227, "ВНЕШ. 21"},
                    {228, "ВНЕШ. 22"},
                    {229, "ВНЕШ. 23"},
                    {230, "ВНЕШ. 24"},
                    {231, "ССЛ1"},
                    {232, "ССЛ2"},
                    {233, "ССЛ3"},
                    {234, "ССЛ4"},
                    {235, "ССЛ5"},
                    {236, "ССЛ6"},
                    {237, "ССЛ7"},
                    {238, "ССЛ8"},
                    {239, "ССЛ9"},
                    {240, "ССЛ10"},
                    {241, "ССЛ11"},
                    {242, "ССЛ12"},
                    {243, "ССЛ13"},
                    {244, "ССЛ14"},
                    {245, "ССЛ15"},
                    {246, "ССЛ16"},
                    {247, "ССЛ17"},
                    {248, "ССЛ18"},
                    {249, "ССЛ19"},
                    {250, "ССЛ20"},
                    {251, "ССЛ21"},
                    {252, "ССЛ22"},
                    {253, "ССЛ23"},
                    {254, "ССЛ24"},
                    {255, "ССЛ25"},
                    {256, "ССЛ26"},
                    {257, "ССЛ27"},
                    {258, "ССЛ28"},
                    {259, "ССЛ29"},
                    {260, "ССЛ30"},
                    {261, "ССЛ31"},
                    {262, "ССЛ32"},
                    {263, "ССЛ33"},
                    {264, "ССЛ34"},
                    {265, "ССЛ35"},
                    {266, "ССЛ36"},
                    {267, "ССЛ37"},
                    {268, "ССЛ38"},
                    {269, "ССЛ39"},
                    {270, "ССЛ40"},
                    {271, "ССЛ41"},
                    {272, "ССЛ42"},
                    {273, "ССЛ43"},
                    {274, "ССЛ44"},
                    {275, "ССЛ45"},
                    {276, "ССЛ46"},
                    {277, "ССЛ47"},
                    {278, "ССЛ48"},
                    {279, "УРОВ СШ1"},
                    {280, "УРОВ СШ2"},
                    {281, "УРОВ ПО"},
                    {282, "УРОВ Пр1"},
                    {283, "УРОВ Пр2"},
                    {284, "УРОВ Пр3"},
                    {285, "УРОВ Пр4"},
                    {286, "УРОВ Пр5"},
                    {287, "УРОВ Пр6"},
                    {288, "УРОВ Пр7"},
                    {289, "УРОВ Пр8"},
                    {290, "УРОВ Пр9"},
                    {291, "УРОВ Пр10"},
                    {292, "УРОВ Пр11"},
                    {293, "УРОВ Пр12"},
                    {294, "УРОВ Пр13"},
                    {295, "УРОВ Пр14"},
                    {296, "УРОВ Пр15"},
                    {297, "УРОВ Пр16"},
                    {298, "УРОВ Пр17"},
                    {299, "УРОВ Пр18"},
                    {300, "УРОВ Пр19"},
                    {301, "УРОВ Пр20"},
                    {302, "УРОВ Пр21"},
                    {303, "УРОВ Пр22"},
                    {304, "УРОВ Пр23"},
                    {305, "УРОВ Пр24"},
                    {306, "Откл.СШ1"},
                    {307, "Откл.СШ2"},
                    {308, "Откл.ПО"},
                    {309, "Откл.Пр1"},
                    {310, "Откл.Пр2"},
                    {311, "Откл.Пр3"},
                    {312, "Откл.Пр4"},
                    {313, "Откл.Пр5"},
                    {314, "Откл.Пр6"},
                    {315, "Откл.Пр7"},
                    {316, "Откл.Пр8"},
                    {317, "Откл.Пр9"},
                    {318, "Откл.Пр10"},
                    {319, "Откл.Пр11"},
                    {320, "Откл.Пр12"},
                    {321, "Откл.Пр13"},
                    {322, "Откл.Пр14"},
                    {323, "Откл.Пр15"},
                    {324, "Откл.Пр16"},
                    {325, "Откл.Пр17"},
                    {326, "Откл.Пр18"},
                    {327, "Откл.Пр19"},
                    {328, "Откл.Пр20"},
                    {329, "Откл.Пр21"},
                    {330, "Откл.Пр22"},
                    {331, "Откл.Пр23"},
                    {332, "Откл.Пр24"},
                    {333, "НЕИСПР."},
                    {334, "ОСН.ГРУППА"},
                    {335, "РЕЗ.ГРУППА"},
                    {336, "АВАРИЯ"},
                    {337, "СИГНАЛ-ЦИЯ"},
                    {338, "НЕИСПР. ТТ ОБЩ."},
                    {339, "НЕИСПР. ТТ СШ1"},
                    {340, "НЕИСПР. ТТ СШ2"}
                };
                switch (ConfigurationStructBigV210.DeviceModelType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (int i = 64; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                            //присоединения УРОВ
                            for (int i = 298; i < 306; i++)
                            {
                                ret.Remove(i);
                            }
                            //присоединения
                            for (int i = 325; i < 333; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A2":
                        {
                            //дискреты
                            for (int i = 40; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A3":
                        {
                            //дискреты
                            for (int i = 24; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A4":
                        {
                            //дискреты
                            for (int i = 32; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A5":
                        {
                            //дискреты
                            for (int i = 40; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                            ushort key = 227;
                            ret[key] = "U> ИО";
                            ret[++key] = "U>";
                            ret[++key] = "U>> ИО";
                            ret[++key] = "U>>";
                            //УРОВ Прис.
                            key = 302;
                            ret[key] = "U< ИО";
                            ret[++key] = "U<";
                            ret[++key] = "U<< ИО";
                            ret[++key] = "U<<";
                            //Прис.
                            key = 329;
                            ret[key] = "Неиспр. ТН";
                            ret[++key] = "Неиспр. 3U0";
                            ret[++key] = "Неиспр. кНС";
                        }
                        break;
                    case "A6":
                    {
                            //дискреты
                            for (int i = 24; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                            ushort key = 227;
                            ret[key] = "U> ИО";
                            ret[++key] = "U>";
                            ret[++key] = "U>> ИО";
                            ret[++key] = "U>>";
                            //УРОВ Прис.
                            key = 302;
                            ret[key] = "U< ИО";
                            ret[++key] = "U<";
                            ret[++key] = "U<< ИО";
                            ret[++key] = "U<<";
                            //Прис.
                            key = 329;
                            ret[key] = "Неиспр. ТН";
                            ret[++key] = "Неиспр. 3U0";
                            ret[++key] = "Неиспр. кНС";
                            }
                        break;
                    case "A7":
                        {
                            //дискреты
                            for (int i = 32; i < 96; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (int i = 112; i < 128; i++)
                            {
                                ret.Remove(i);
                            }
                            ushort key = 227;
                            ret[key] = "U> ИО";
                            ret[++key] = "U>";
                            ret[++key] = "U>> ИО";
                            ret[++key] = "U>>";
                            //УРОВ Прис.
                            key = 302;
                            ret[key] = "U< ИО";
                            ret[++key] = "U<";
                            ret[++key] = "U<< ИО";
                            ret[++key] = "U<<";
                            //Прис.
                            key = 329;
                            ret[key] = "Неиспр. ТН";
                            ret[++key] = "Неиспр. 3U0";
                            ret[++key] = "Неиспр. кНС";
                        }
                        break;
                    default:
                        //дискреты
                        for (int i = 24; i < 96; i++)
                        {
                            ret.Remove(i);
                        }
                        //быстрые гусы
                        for (int i = 112; i < 128; i++)
                        {
                            ret.Remove(i);
                        }
                        //присоединения УРОВ
                        for (int i = 298; i < 306; i++)
                        {
                            ret.Remove(i);
                        }
                        //присоединения
                        for (int i = 325; i < 334; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }
                return ret.ToDictionary(r=>r.Key, r=>r.Value);
            }
        }

        public static Dictionary<ushort, string> RelaySignalsBig
        {
            get
            {
                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>
                {
                    {0, "Нет" },
                    {1, "Д1"},
                    {2, "Д1 Инв."},
                    {3, "Д2"},
                    {4, "Д2 Инв."},
                    {5, "Д3"},
                    {6, "Д3 Инв."},
                    {7, "Д4"},
                    {8, "Д4 Инв."},
                    {9, "Д5"},
                    {10, "Д5 Инв."},
                    {11, "Д6"},
                    {12, "Д6 Инв."},
                    {13, "Д7"},
                    {14, "Д7 Инв."},
                    {15, "Д8"},
                    {16, "Д8 Инв."},
                    {17, "Д9"},
                    {18, "Д9 Инв."},
                    {19, "Д10"},
                    {20, "Д10 Инв."},
                    {21, "Д11"},
                    {22, "Д11 Инв."},
                    {23, "Д12"},
                    {24, "Д12 Инв."},
                    {25, "Д13"},
                    {26, "Д13 Инв."},
                    {27, "Д14"},
                    {28, "Д14 Инв."},
                    {29, "Д15"},
                    {30, "Д15 Инв."},
                    {31, "Д16"},
                    {32, "Д16 Инв."},
                    {33, "Д17"},
                    {34, "Д17 Инв."},
                    {35, "Д18"},
                    {36, "Д18 Инв."},
                    {37, "Д19"},
                    {38, "Д19 Инв."},
                    {39, "Д20"},
                    {40, "Д20 Инв."},
                    {41, "Д21"},
                    {42, "Д21 Инв."},
                    {43, "Д22"},
                    {44, "Д22 Инв."},
                    {45, "Д23"},
                    {46, "Д23 Инв."},
                    {47, "Д24"},
                    {48, "Д24 Инв."},
                    {49, "Д25"},
                    {50, "Д25 Инв."},
                    {51, "Д26"},
                    {52, "Д26 Инв."},
                    {53, "Д27"},
                    {54, "Д27 Инв."},
                    {55, "Д28"},
                    {56, "Д28 Инв."},
                    {57, "Д29"},
                    {58, "Д29 Инв."},
                    {59, "Д30"},
                    {60, "Д30 Инв."},
                    {61, "Д31"},
                    {62, "Д31 Инв."},
                    {63, "Д32"},
                    {64, "Д32 Инв."},
                    {65, "Д33"},
                    {66, "Д33 Инв."},
                    {67, "Д34"},
                    {68, "Д34 Инв."},
                    {69, "Д35"},
                    {70, "Д35 Инв."},
                    {71, "Д36"},
                    {72, "Д36 Инв."},
                    {73, "Д37"},
                    {74, "Д37 Инв."},
                    {75, "Д38"},
                    {76, "Д38 Инв."},
                    {77, "Д39"},
                    {78, "Д39 Инв."},
                    {79, "Д40"},
                    {80, "Д40 Инв."},
                    {81, "Д41"},
                    {82, "Д41 Инв."},
                    {83, "Д42"},
                    {84, "Д42 Инв."},
                    {85, "Д43"},
                    {86, "Д43 Инв."},
                    {87, "Д44"},
                    {88, "Д44 Инв."},
                    {89, "Д45"},
                    {90, "Д45 Инв."},
                    {91, "Д46"},
                    {92, "Д46 Инв."},
                    {93, "Д47"},
                    {94, "Д47 Инв."},
                    {95, "Д48"},
                    {96, "Д48 Инв."},
                    {97, "Д49"},
                    {98, "Д49 Инв."},
                    {99, "Д50"},
                    {100, "Д50 Инв."},
                    {101, "Д51"},
                    {102, "Д51 Инв."},
                    {103, "Д52"},
                    {104, "Д52 Инв."},
                    {105, "Д53"},
                    {106, "Д53 Инв."},
                    {107, "Д54"},
                    {108, "Д54 Инв."},
                    {109, "Д55"},
                    {110, "Д55 Инв."},
                    {111, "Д56"},
                    {112, "Д56 Инв."},
                    {113, "Д57"},
                    {114, "Д57 Инв."},
                    {115, "Д58"},
                    {116, "Д58 Инв."},
                    {117, "Д59"},
                    {118, "Д59 Инв."},
                    {119, "Д60"},
                    {120, "Д60 Инв."},
                    {121, "Д61"},
                    {122, "Д61 Инв."},
                    {123, "Д62"},
                    {124, "Д62 Инв."},
                    {125, "Д63"},
                    {126, "Д63 Инв."},
                    {127, "Д64"},
                    {128, "Д64 Инв."},
                    {129, "Д65"},
                    {130, "Д65 Инв."},
                    {131, "Д66"},
                    {132, "Д66 Инв."},
                    {133, "Д67"},
                    {134, "Д67 Инв."},
                    {135, "Д68"},
                    {136, "Д68 Инв."},
                    {137, "Д69"},
                    {138, "Д69 Инв."},
                    {139, "Д70"},
                    {140, "Д70 Инв."},
                    {141, "Д71"},
                    {142, "Д71 Инв."},
                    {143, "Д72"},
                    {144, "Д72 Инв."},
                    {145, "Д73"},
                    {146, "Д73 Инв."},
                    {147, "Д74"},
                    {148, "Д74 Инв."},
                    {149, "Д75"},
                    {150, "Д75 Инв."},
                    {151, "Д76"},
                    {152, "Д76 Инв."},
                    {153, "Д77"},
                    {154, "Д77 Инв."},
                    {155, "Д78"},
                    {156, "Д78 Инв."},
                    {157, "Д79"},
                    {158, "Д79 Инв."},
                    {159, "Д80"},
                    {160, "Д80 Инв."},
                    {161, "Д81"},
                    {162, "Д81 Инв."},
                    {163, "Д82"},
                    {164, "Д82 Инв."},
                    {165, "Д83"},
                    {166, "Д83 Инв."},
                    {167, "Д84"},
                    {168, "Д84 Инв."},
                    {169, "Д85"},
                    {170, "Д85 Инв."},
                    {171, "Д86"},
                    {172, "Д86 Инв."},
                    {173, "Д87"},
                    {174, "Д87 Инв."},
                    {175, "Д88"},
                    {176, "Д88 Инв."},
                    {177, "Д89"},
                    {178, "Д89 Инв."},
                    {179, "Д90"},
                    {180, "Д90 Инв."},
                    {181, "Д91"},
                    {182, "Д91 Инв."},
                    {183, "Д92"},
                    {184, "Д92 Инв."},
                    {185, "Д93"},
                    {186, "Д93 Инв."},
                    {187, "Д94"},
                    {188, "Д94 Инв."},
                    {189, "Д95"},
                    {190, "Д95 Инв."},
                    {191, "Д96"},
                    {192, "Д96 Инв."},
                    {193, "ЛС1"},
                    {194, "ЛС1 Инв."},
                    {195, "ЛС2"},
                    {196, "ЛС2 Инв."},
                    {197, "ЛС3"},
                    {198, "ЛС3 Инв."},
                    {199, "ЛС4"},
                    {200, "ЛС4 Инв."},
                    {201, "ЛС5"},
                    {202, "ЛС5 Инв."},
                    {203, "ЛС6"},
                    {204, "ЛС6 Инв."},
                    {205, "ЛС7"},
                    {206, "ЛС7 Инв."},
                    {207, "ЛС8"},
                    {208, "ЛС8 Инв."},
                    {209, "ЛС9"},
                    {210, "ЛС9 Инв."},
                    {211, "ЛС10"},
                    {212, "ЛС10 Инв."},
                    {213, "ЛС11"},
                    {214, "ЛС11 Инв."},
                    {215, "ЛС12"},
                    {216, "ЛС12 Инв."},
                    {217, "ЛС13"},
                    {218, "ЛС13 Инв."},
                    {219, "ЛС14"},
                    {220, "ЛС14 Инв."},
                    {221, "ЛС15"},
                    {222, "ЛС15 Инв."},
                    {223, "ЛС16"},
                    {224, "ЛС16 Инв."},
                    {225, "БГ1"},
                    {226, "БГ1 Инв."},
                    {227, "БГ2"},
                    {228, "БГ2 Инв."},
                    {229, "БГ3"},
                    {230, "БГ3 Инв."},
                    {231, "БГ4"},
                    {232, "БГ4 Инв."},
                    {233, "БГ5"},
                    {234, "БГ5 Инв."},
                    {235, "БГ6"},
                    {236, "БГ6 Инв."},
                    {237, "БГ7"},
                    {238, "БГ7 Инв."},
                    {239, "БГ8"},
                    {240, "БГ8 Инв."},
                    {241, "БГ9"},
                    {242, "БГ9 Инв."},
                    {243, "БГ10"},
                    {244, "БГ10 Инв."},
                    {245, "БГ11"},
                    {246, "БГ11 Инв."},
                    {247, "БГ12"},
                    {248, "БГ12 Инв."},
                    {249, "БГ13"},
                    {250, "БГ13 Инв."},
                    {251, "БГ14"},
                    {252, "БГ14 Инв."},
                    {253, "БГ15"},
                    {254, "БГ15 Инв."},
                    {255, "БГ16"},
                    {256, "БГ16 Инв."},
                    {257, "ВЛС1"},
                    {258, "ВЛС1 Инв."},
                    {259, "ВЛС2"},
                    {260, "ВЛС2 Инв."},
                    {261, "ВЛС3"},
                    {262, "ВЛС3 Инв."},
                    {263, "ВЛС4"},
                    {264, "ВЛС4 Инв."},
                    {265, "ВЛС5"},
                    {266, "ВЛС5 Инв."},
                    {267, "ВЛС6"},
                    {268, "ВЛС6 Инв."},
                    {269, "ВЛС7"},
                    {270, "ВЛС7 Инв."},
                    {271, "ВЛС8"},
                    {272, "ВЛС8 Инв."},
                    {273, "ВЛС9"},
                    {274, "ВЛС9 Инв."},
                    {275, "ВЛС10"},
                    {276, "ВЛС10 Инв."},
                    {277, "ВЛС11"},
                    {278, "ВЛС11 Инв."},
                    {279, "ВЛС12"},
                    {280, "ВЛС12 Инв."},
                    {281, "ВЛС13"},
                    {282, "ВЛС13 Инв."},
                    {283, "ВЛС14"},
                    {284, "ВЛС14 Инв."},
                    {285, "ВЛС15"},
                    {286, "ВЛС15 Инв."},
                    {287, "ВЛС16"},
                    {288, "ВЛС16 Инв."},
                    {289, "Iд1м СШ1 *"},
                    {290, "Iд1м СШ1* Инв."},
                    {291, "Iд1м СШ1"},
                    {292, "Iд1м СШ1 Инв."},
                    {293, "Iд2м СШ2 *"},
                    {294, "Iд2м СШ2* Инв."},
                    {295, "Iд2м СШ2"},
                    {296, "Iд2м СШ2 Инв."},
                    {297, "Iд3м ПО *"},
                    {298, "Iд3м ПО* Инв."},
                    {299, "Iд3м ПО"},
                    {300, "Iд3м ПО Инв."},
                    {301, "Iд1 СШ1 ИО"},
                    {302, "Iд1 СШ1 ИО Инв."},
                    {303, "Iд1 СШ1 *"},
                    {304, "Iд1 СШ1* Инв."},
                    {305, "Iд1 СШ1"},
                    {306, "Iд1 СШ1 Инв."},
                    {307, "Iд2 СШ2 ИО"},
                    {308, "Iд2 СШ2 ИО Инв."},
                    {309, "Iд2 СШ2 *"},
                    {310, "Iд2 СШ2* Инв."},
                    {311, "Iд2 СШ2"},
                    {312, "Iд2 СШ2 Инв."},
                    {313, "Iд3 ПО ИО"},
                    {314, "Iд3 ПО ИО Инв."},
                    {315, "Iд3 ПО *"},
                    {316, "Iд3 ПО* Инв."},
                    {317, "Iд3 ПО"},
                    {318, "Iд3 ПО Инв."},

                    {319, "I1> ИО"},
                    {320, "I1> ИО Инв."},
                    {321, "I1>"},
                    {322, "I1> Инв."},
                    {323, "I2> ИО"},
                    {324, "I2> ИО Инв."},
                    {325, "I2>"},
                    {326, "I2> Инв."},
                    {327, "I3> ИО"},
                    {328, "I3> ИО Инв."},
                    {329, "I3>"},
                    {330, "I3> Инв."},
                    {331, "I4> ИО"},
                    {332, "I4> ИО Инв."},
                    {333, "I4>"},
                    {334, "I4> Инв."},
                    {335, "I5> ИО"},
                    {336, "I5> ИО Инв."},
                    {337, "I5>"},
                    {338, "I5> Инв."},
                    {339, "I6> ИО"},
                    {340, "I6> ИО Инв."},
                    {341, "I6>"},
                    {342, "I6> Инв."},
                    {343, "I7> ИО"},
                    {344, "I7> ИО Инв."},
                    {345, "I7>"},
                    {346, "I7> Инв."},
                    {347, "I8> ИО"},
                    {348, "I8> ИО Инв."},
                    {349, "I8>"},
                    {350, "I8> Инв."},
                    {351, "I9> ИО"},
                    {352, "I9> ИО Инв."},
                    {353, "I9>"},
                    {354, "I9> Инв."},
                    {355, "I10> ИО"},
                    {356, "I10> ИО Инв."},
                    {357, "I10>"},
                    {358, "I10> Инв."},
                    {359, "I11> ИО"},
                    {360, "I11> ИО Инв."},
                    {361, "I11>"},
                    {362, "I11> Инв."},
                    {363, "I12> ИО"},
                    {364, "I12> ИО Инв."},
                    {365, "I12>"},
                    {366, "I12> Инв."},
                    {367, "I13> ИО"},
                    {368, "I13> ИО Инв."},
                    {369, "I13>"},
                    {370, "I13> Инв."},
                    {371, "I14> ИО"},
                    {372, "I14> ИО Инв."},
                    {373, "I14>"},
                    {374, "I14> Инв."},
                    {375, "I15> ИО"},
                    {376, "I15> ИО Инв."},
                    {377, "I15>"},
                    {378, "I15> Инв."},
                    {379, "I16> ИО"},
                    {380, "I16> ИО Инв."},
                    {381, "I16>"},
                    {382, "I16> Инв."},
                    {383, "I17> ИО"},
                    {384, "I17> ИО Инв."},
                    {385, "I17>"},
                    {386, "I17> Инв."},
                    {387, "I18> ИО"},
                    {388, "I18> ИО Инв."},
                    {389, "I18>"},
                    {390, "I18> Инв."},
                    {391, "I19> ИО"},
                    {392, "I19> ИО Инв."},
                    {393, "I19>"},
                    {394, "I19> Инв."},
                    {395, "I20> ИО"},
                    {396, "I20> ИО Инв."},
                    {397, "I20>"},
                    {398, "I20> Инв."},
                    {399, "I21> ИО"},
                    {400, "I21> ИО Инв."},
                    {401, "I21>"},
                    {402, "I21> Инв."},
                    {403, "I22> ИО"},
                    {404, "I22> ИО Инв."},
                    {405, "I22>"},
                    {406, "I22> Инв."},
                    {407, "I23> ИО"},
                    {408, "I23> ИО Инв."},
                    {409, "I23>"},
                    {410, "I23> Инв."},
                    {411, "I24> ИО"},
                    {412, "I24> ИО Инв."},
                    {413, "I24>"},
                    {414, "I24> Инв."},
                    {415, "I25> ИО"},
                    {416, "I25> ИО Инв."},
                    {417, "I25>"},
                    {418, "I25> Инв."},
                    {419, "I26> ИО"},
                    {420, "I26> ИО Инв."},
                    {421, "I26>"},
                    {422, "I26> Инв."},
                    {423, "I27> ИО"},
                    {424, "I27> ИО Инв."},
                    {425, "I27>"},
                    {426, "I27> Инв."},
                    {427, "I28> ИО"},
                    {428, "I28> ИО Инв."},
                    {429, "I28>"},
                    {430, "I28> Инв."},
                    {431, "I29> ИО"},
                    {432, "I29> ИО Инв."},
                    {433, "I29>"},
                    {434, "I29> Инв."},
                    {435, "I30> ИО"},
                    {436, "I30> ИО Инв."},
                    {437, "I30>"},
                    {438, "I30> Инв."},
                    {439, "I31> ИО"},
                    {440, "I31> ИО Инв."},
                    {441, "I31>"},
                    {442, "I31> Инв."},
                    {443, "I32> ИО"},
                    {444, "I32> ИО Инв."},
                    {445, "I32>"},
                    {446, "I32> Инв."},

                    {447, "ВНЕШ. 1"},
                    {448, "ВНЕШ. 1 Инв."},
                    {449, "ВНЕШ. 2"},
                    {450, "ВНЕШ. 2 Инв."},
                    {451, "ВНЕШ. 3"},
                    {452, "ВНЕШ. 3 Инв."},
                    {453, "ВНЕШ. 4"},
                    {454, "ВНЕШ. 4 Инв."},
                    {455, "ВНЕШ. 5"},
                    {456, "ВНЕШ. 5 Инв."},
                    {457, "ВНЕШ. 6"},
                    {458, "ВНЕШ. 6 Инв."},
                    {459, "ВНЕШ. 7"},
                    {460, "ВНЕШ. 7 Инв."},
                    {461, "ВНЕШ. 8"},
                    {462, "ВНЕШ. 8 Инв."},
                    {463, "ВНЕШ. 9"},
                    {464, "ВНЕШ. 9 Инв."},
                    {465, "ВНЕШ. 10"},
                    {466, "ВНЕШ. 10 Инв."},
                    {467, "ВНЕШ. 11"},
                    {468, "ВНЕШ. 11 Инв."},
                    {469, "ВНЕШ. 12"},
                    {470, "ВНЕШ. 12 Инв."},
                    {471, "ВНЕШ. 13"},
                    {472, "ВНЕШ. 13 Инв."},
                    {473, "ВНЕШ. 14"},
                    {474, "ВНЕШ. 14 Инв."},
                    {475, "ВНЕШ. 15"},
                    {476, "ВНЕШ. 15 Инв."},
                    {477, "ВНЕШ. 16"},
                    {478, "ВНЕШ. 16 Инв."},
                    {479, "ВНЕШ. 17"},
                    {480, "ВНЕШ. 17 Инв."},
                    {481, "ВНЕШ. 18"},
                    {482, "ВНЕШ. 18 Инв."},
                    {483, "ВНЕШ. 19"},
                    {484, "ВНЕШ. 19 Инв."},
                    {485, "ВНЕШ. 20"},
                    {486, "ВНЕШ. 20 Инв."},
                    {487, "ВНЕШ. 21"},
                    {488, "ВНЕШ. 21 Инв."},
                    {489, "ВНЕШ. 22"},
                    {490, "ВНЕШ. 22 Инв."},
                    {491, "ВНЕШ. 23"},
                    {492, "ВНЕШ. 23 Инв."},
                    {493, "ВНЕШ. 24"},
                    {494, "ВНЕШ. 24 Инв."},

                    {495, "ССЛ1"},
                    {496, "ССЛ1 Инв."},
                    {497, "ССЛ2"},
                    {498, "ССЛ2 Инв."},
                    {499, "ССЛ3"},
                    {500, "ССЛ3 Инв."},
                    {501, "ССЛ4"},
                    {502, "ССЛ4 Инв."},
                    {503, "ССЛ5"},
                    {504, "ССЛ5 Инв."},
                    {505, "ССЛ6"},
                    {506, "ССЛ6 Инв."},
                    {507, "ССЛ7"},
                    {508, "ССЛ7 Инв."},
                    {509, "ССЛ8"},
                    {510, "ССЛ8 Инв."},
                    {511, "ССЛ9"},
                    {512, "ССЛ9 Инв."},
                    {513, "ССЛ10"},
                    {514, "ССЛ10 Инв."},
                    {515, "ССЛ11"},
                    {516, "ССЛ11 Инв."},
                    {517, "ССЛ12"},
                    {518, "ССЛ12 Инв."},
                    {519, "ССЛ13"},
                    {520, "ССЛ13 Инв."},
                    {521, "ССЛ14"},
                    {522, "ССЛ14 Инв."},
                    {523, "ССЛ15"},
                    {524, "ССЛ15 Инв."},
                    {525, "ССЛ16"},
                    {526, "ССЛ16 Инв."},
                    {527, "ССЛ17"},
                    {528, "ССЛ17 Инв."},
                    {529, "ССЛ18"},
                    {530, "ССЛ18 Инв."},
                    {531, "ССЛ19"},
                    {532, "ССЛ19 Инв."},
                    {533, "ССЛ20"},
                    {534, "ССЛ20 Инв."},
                    {535, "ССЛ21"},
                    {536, "ССЛ21 Инв."},
                    {537, "ССЛ22"},
                    {538, "ССЛ22 Инв."},
                    {539, "ССЛ23"},
                    {540, "ССЛ23 Инв."},
                    {541, "ССЛ24"},
                    {542, "ССЛ24 Инв."},
                    {543, "ССЛ25"},
                    {544, "ССЛ25 Инв."},
                    {545, "ССЛ26"},
                    {546, "ССЛ26 Инв."},
                    {547, "ССЛ27"},
                    {548, "ССЛ27 Инв."},
                    {549, "ССЛ28"},
                    {550, "ССЛ28 Инв."},
                    {551, "ССЛ29"},
                    {552, "ССЛ29 Инв."},
                    {553, "ССЛ30"},
                    {554, "ССЛ30 Инв."},
                    {555, "ССЛ31"},
                    {556, "ССЛ31 Инв."},
                    {557, "ССЛ32"},
                    {558, "ССЛ32 Инв."},
                    {559, "ССЛ33"},
                    {560, "ССЛ33 Инв."},
                    {561, "ССЛ34"},
                    {562, "ССЛ34 Инв."},
                    {563, "ССЛ35"},
                    {564, "ССЛ35 Инв."},
                    {565, "ССЛ36"},
                    {566, "ССЛ36 Инв."},
                    {567, "ССЛ37"},
                    {568, "ССЛ37 Инв."},
                    {569, "ССЛ38"},
                    {570, "ССЛ38 Инв."},
                    {571, "ССЛ39"},
                    {572, "ССЛ39 Инв."},
                    {573, "ССЛ40"},
                    {574, "ССЛ40 Инв."},
                    {575, "ССЛ41"},
                    {576, "ССЛ41 Инв."},
                    {577, "ССЛ42"},
                    {578, "ССЛ42 Инв."},
                    {579, "ССЛ43"},
                    {580, "ССЛ43 Инв."},
                    {581, "ССЛ44"},
                    {582, "ССЛ44 Инв."},
                    {583, "ССЛ45"},
                    {584, "ССЛ45 Инв."},
                    {585, "ССЛ46"},
                    {586, "ССЛ46 Инв."},
                    {587, "ССЛ47"},
                    {588, "ССЛ47 Инв."},
                    {589, "ССЛ48"},
                    {590, "ССЛ48 Инв."},

                    {591, "УРОВ СШ1"},
                    {592, "УРОВ СШ1 Инв."},
                    {593, "УРОВ СШ2"},
                    {594, "УРОВ СШ2 Инв."},
                    {595, "УРОВ ПО"},
                    {596, "УРОВ ПО Инв."},

                    {597, "УРОВ Пр1"},
                    {598, "УРОВ Пр1 Инв."},
                    {599, "УРОВ Пр2"},
                    {600, "УРОВ Пр2 Инв."},
                    {601, "УРОВ Пр3"},
                    {602, "УРОВ Пр3 Инв."},
                    {603, "УРОВ Пр4"},
                    {604, "УРОВ Пр4 Инв."},
                    {605, "УРОВ Пр5"},
                    {606, "УРОВ Пр5 Инв."},
                    {607, "УРОВ Пр6"},
                    {608, "УРОВ Пр6 Инв."},
                    {609, "УРОВ Пр7"},
                    {610, "УРОВ Пр7 Инв."},
                    {611, "УРОВ Пр8"},
                    {612, "УРОВ Пр8 Инв."},
                    {613, "УРОВ Пр9"},
                    {614, "УРОВ Пр9 Инв."},
                    {615, "УРОВ Пр10"},
                    {616, "УРОВ Пр10 Инв."},
                    {617, "УРОВ Пр11"},
                    {618, "УРОВ Пр11 Инв."},
                    {619, "УРОВ Пр12"},
                    {620, "УРОВ Пр12 Инв."},
                    {621, "УРОВ Пр13"},
                    {622, "УРОВ Пр13 Инв."},
                    {623, "УРОВ Пр14"},
                    {624, "УРОВ Пр14 Инв."},
                    {625, "УРОВ Пр15"},
                    {626, "УРОВ Пр15 Инв."},
                    {627, "УРОВ Пр16"},
                    {628, "УРОВ Пр16 Инв."},
                    {629, "УРОВ Пр17"},
                    {630, "УРОВ Пр17 Инв."},
                    {631, "УРОВ Пр18"},
                    {632, "УРОВ Пр18 Инв."},
                    {633, "УРОВ Пр19"},
                    {634, "УРОВ Пр19 Инв."},
                    {635, "УРОВ Пр20"},
                    {636, "УРОВ Пр20 Инв."},
                    {637, "УРОВ Пр21"},
                    {638, "УРОВ Пр21 Инв."},
                    {639, "УРОВ Пр22"},
                    {640, "УРОВ Пр22 Инв."},
                    {641, "УРОВ Пр23"},
                    {642, "УРОВ Пр23 Инв."},
                    {643, "УРОВ Пр24"},
                    {644, "УРОВ Пр24 Инв."},

                    {645, "Откл.СШ1"},
                    {646, "Откл.СШ1 Инв."},
                    {647, "Откл.СШ2"},
                    {648, "Откл.СШ2 Инв."},
                    {649, "Откл.ПО"},
                    {650, "Откл.ПО Инв."},

                    {651, "Откл.Пр1"},
                    {652, "Откл.Пр1 Инв."},
                    {653, "Откл.Пр2"},
                    {654, "Откл.Пр2 Инв."},
                    {655, "Откл.Пр3"},
                    {656, "Откл.Пр3 Инв."},
                    {657, "Откл.Пр4"},
                    {658, "Откл.Пр4 Инв."},
                    {659, "Откл.Пр5"},
                    {660, "Откл.Пр5 Инв."},
                    {661, "Откл.Пр6"},
                    {662, "Откл.Пр6 Инв."},
                    {663, "Откл.Пр7"},
                    {664, "Откл.Пр7 Инв."},
                    {665, "Откл.Пр8"},
                    {666, "Откл.Пр8 Инв."},
                    {667, "Откл.Пр9"},
                    {668, "Откл.Пр9 Инв."},
                    {669, "Откл.Пр10"},
                    {670, "Откл.Пр10 Инв."},
                    {671, "Откл.Пр11"},
                    {672, "Откл.Пр11 Инв."},
                    {673, "Откл.Пр12"},
                    {674, "Откл.Пр12 Инв."},
                    {675, "Откл.Пр13"},
                    {676, "Откл.Пр13 Инв."},
                    {677, "Откл.Пр14"},
                    {678, "Откл.Пр14 Инв."},
                    {679, "Откл.Пр15"},
                    {680, "Откл.Пр15 Инв."},
                    {681, "Откл.Пр16"},
                    {682, "Откл.Пр16 Инв."},
                    {683, "Откл.Пр17"},
                    {684, "Откл.Пр17 Инв."},
                    {685, "Откл.Пр18"},
                    {686, "Откл.Пр18 Инв."},
                    {687, "Откл.Пр19"},
                    {688, "Откл.Пр19 Инв."},
                    {689, "Откл.Пр20"},
                    {690, "Откл.Пр20 Инв."},
                    {691, "Откл.Пр21"},
                    {692, "Откл.Пр21 Инв."},
                    {693, "Откл.Пр22"},
                    {694, "Откл.Пр22 Инв."},
                    {695, "Откл.Пр23"},
                    {696, "Откл.Пр23 Инв."},
                    {697, "Откл.Пр24"},
                    {698, "Откл.Пр24 Инв."},
                    
                    {699, "НЕИСПР."},
                    {700, "НЕИСПР. Инв."},
                    {701, "ОСН.ГРУППА"},
                    {702, "ОСН.ГРУППА Инв."},
                    {703, "РЕЗ.ГРУППА"},
                    {704, "РЕЗ.ГРУППА Инв."},
                    {705, "АВАРИЯ"},
                    {706, "АВАРИЯ Инв."},
                    {707, "СИГНАЛ-ЦИЯ"},
                    {708, "СИГНАЛ-ЦИЯ Инв."},
                    {709, "НЕИСПР. ТТ ОБЩ."},
                    {710, "НЕИСПР. ТТ ОБЩ. Инв."},
                    {711, "НЕИСПР. ТТ СШ1"},
                    {712, "НЕИСПР. ТТ СШ1 Инв."},
                    {713, "НЕИСПР. ТТ СШ2"},
                    {714, "НЕИСПР. ТТ СШ2 Инв."}
                };
                switch (DeviceType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (ushort i = 129; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 258; i++)
                            {
                                ret.Remove(i);
                            }
                            //присоединения УРОВ
                            for (ushort i = 629; i < 645; i++)
                            {
                                ret.Remove(i);
                            }
                            //присоединения
                            for (ushort i = 683; i < 699; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A2":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A3":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A4":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A5":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВЗ
                            ushort key = 487;
                            ret[key] = "U> ИО";
                            ret[++key] = "U> ИО Инв.";
                            ret[++key] = "U>";
                            ret[++key] = "U> Инв.";
                            ret[++key] = "U>> ИО";
                            ret[++key] = "U>> ИО Инв.";
                            ret[++key] = "U>>";
                            ret[++key] = "U>> Инв.";
                            //Прис. УРОВ
                            key = 637;
                            ret[key] = "U< ИО";
                            ret[++key] = "U< ИО Инв.";
                            ret[++key] = "U<";
                            ret[++key] = "U< Инв.";
                            ret[++key] = "U<< ИО";
                            ret[++key] = "U<< ИО Инв.";
                            ret[++key] = "U<<";
                            ret[++key] = "U<< Инв.";
                            //Прис.
                            for (ushort i = 691; i < 699; i++)
                            {
                                ret.Remove(i);
                            }
                            key = 691;
                            ret[key] = "Неиспр. ТН";
                            ret[++key] = "Неиспр. ТН Инв.";
                            ret[++key] = "Неиспр. 3U0";
                            ret[++key] = "Неиспр. 3U0 Инв.";
                            ret[++key] = "Неиспр. кНС";
                            ret[++key] = "Неиспр. кНс Инв.";
                        }
                        break;
                    case "A6":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВЗ
                            ushort key = 487;
                            ret[key] = "U> ИО";
                            ret[++key] = "U> ИО Инв.";
                            ret[++key] = "U>";
                            ret[++key] = "U> Инв.";
                            ret[++key] = "U>> ИО";
                            ret[++key] = "U>> ИО Инв.";
                            ret[++key] = "U>>";
                            ret[++key] = "U>> Инв.";
                            //Прис. УРОВ
                            key = 637;
                            ret[key] = "U< ИО";
                            ret[++key] = "U< ИО Инв.";
                            ret[++key] = "U<";
                            ret[++key] = "U< Инв.";
                            ret[++key] = "U<< ИО";
                            ret[++key] = "U<< ИО Инв.";
                            ret[++key] = "U<<";
                            ret[++key] = "U<< Инв.";
                            //Прис.
                            for (ushort i = 691; i < 699; i++)
                            {
                                ret.Remove(i);
                            }
                            key = 691;
                            ret[key] = "Неиспр. ТН";
                            ret[++key] = "Неиспр. ТН Инв.";
                            ret[++key] = "Неиспр. 3U0";
                            ret[++key] = "Неиспр. 3U0 Инв.";
                            ret[++key] = "Неиспр. кНС";
                            ret[++key] = "Неиспр. кНс Инв.";
                        }
                        break;
                    case "A7":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВЗ
                            ushort key = 487;
                            ret[key] = "U> ИО";
                            ret[++key] = "U> ИО Инв.";
                            ret[++key] = "U>";
                            ret[++key] = "U> Инв.";
                            ret[++key] = "U>> ИО";
                            ret[++key] = "U>> ИО Инв.";
                            ret[++key] = "U>>";
                            ret[++key] = "U>> Инв.";
                            //Прис. УРОВ
                            key = 637;
                            ret[key] = "U< ИО";
                            ret[++key] = "U< ИО Инв.";
                            ret[++key] = "U<";
                            ret[++key] = "U< Инв.";
                            ret[++key] = "U<< ИО";
                            ret[++key] = "U<< ИО Инв.";
                            ret[++key] = "U<<";
                            ret[++key] = "U<< Инв.";
                            //Прис.
                            for (ushort i = 691; i < 699; i++)
                            {
                                ret.Remove(i);
                            }
                            key = 691;
                            ret[key] = "Неиспр. ТН";
                            ret[++key] = "Неиспр. ТН Инв.";
                            ret[++key] = "Неиспр. 3U0";
                            ret[++key] = "Неиспр. 3U0 Инв.";
                            ret[++key] = "Неиспр. кНС";
                            ret[++key] = "Неиспр. кНс Инв.";
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }
                        //присоединения УРОВ
                        for (ushort i = 629; i < 645; i++)
                        {
                            ret.Remove(i);
                        }
                        //присоединения
                        for (ushort i = 683; i < 699; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }
                return ret.ToDictionary(r=>r.Key, r=>r.Value);
            }
        }

        public static Dictionary<ushort, string> RelaySignalsBig213
        {
            get
            {
                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>
                {
                    {0, "Нет"},
                    {1, "Д1"},
                    {2, "Д1 Инв."},
                    {3, "Д2"},
                    {4, "Д2 Инв."},
                    {5, "Д3"},
                    {6, "Д3 Инв."},
                    {7, "Д4"},
                    {8, "Д4 Инв."},
                    {9, "Д5"},
                    {10, "Д5 Инв."},
                    {11, "Д6"},
                    {12, "Д6 Инв."},
                    {13, "Д7"},
                    {14, "Д7 Инв."},
                    {15, "Д8"},
                    {16, "Д8 Инв."},
                    {17, "Д9"},
                    {18, "Д9 Инв."},
                    {19, "Д10"},
                    {20, "Д10 Инв."},
                    {21, "Д11"},
                    {22, "Д11 Инв."},
                    {23, "Д12"},
                    {24, "Д12 Инв."},
                    {25, "Д13"},
                    {26, "Д13 Инв."},
                    {27, "Д14"},
                    {28, "Д14 Инв."},
                    {29, "Д15"},
                    {30, "Д15 Инв."},
                    {31, "Д16"},
                    {32, "Д16 Инв."},
                    {33, "Д17"},
                    {34, "Д17 Инв."},
                    {35, "Д18"},
                    {36, "Д18 Инв."},
                    {37, "Д19"},
                    {38, "Д19 Инв."},
                    {39, "Д20"},
                    {40, "Д20 Инв."},
                    {41, "Д21"},
                    {42, "Д21 Инв."},
                    {43, "Д22"},
                    {44, "Д22 Инв."},
                    {45, "Д23"},
                    {46, "Д23 Инв."},
                    {47, "Д24"},
                    {48, "Д24 Инв."},
                    {49, "Д25"},
                    {50, "Д25 Инв."},
                    {51, "Д26"},
                    {52, "Д26 Инв."},
                    {53, "Д27"},
                    {54, "Д27 Инв."},
                    {55, "Д28"},
                    {56, "Д28 Инв."},
                    {57, "Д29"},
                    {58, "Д29 Инв."},
                    {59, "Д30"},
                    {60, "Д30 Инв."},
                    {61, "Д31"},
                    {62, "Д31 Инв."},
                    {63, "Д32"},
                    {64, "Д32 Инв."},
                    {65, "Д33"},
                    {66, "Д33 Инв."},
                    {67, "Д34"},
                    {68, "Д34 Инв."},
                    {69, "Д35"},
                    {70, "Д35 Инв."},
                    {71, "Д36"},
                    {72, "Д36 Инв."},
                    {73, "Д37"},
                    {74, "Д37 Инв."},
                    {75, "Д38"},
                    {76, "Д38 Инв."},
                    {77, "Д39"},
                    {78, "Д39 Инв."},
                    {79, "Д40"},
                    {80, "Д40 Инв."},
                    {81, "Д41"},
                    {82, "Д41 Инв."},
                    {83, "Д42"},
                    {84, "Д42 Инв."},
                    {85, "Д43"},
                    {86, "Д43 Инв."},
                    {87, "Д44"},
                    {88, "Д44 Инв."},
                    {89, "Д45"},
                    {90, "Д45 Инв."},
                    {91, "Д46"},
                    {92, "Д46 Инв."},
                    {93, "Д47"},
                    {94, "Д47 Инв."},
                    {95, "Д48"},
                    {96, "Д48 Инв."},
                    {97, "Д49"},
                    {98, "Д49 Инв."},
                    {99, "Д50"},
                    {100, "Д50 Инв."},
                    {101, "Д51"},
                    {102, "Д51 Инв."},
                    {103, "Д52"},
                    {104, "Д52 Инв."},
                    {105, "Д53"},
                    {106, "Д53 Инв."},
                    {107, "Д54"},
                    {108, "Д54 Инв."},
                    {109, "Д55"},
                    {110, "Д55 Инв."},
                    {111, "Д56"},
                    {112, "Д56 Инв."},
                    {113, "Д57"},
                    {114, "Д57 Инв."},
                    {115, "Д58"},
                    {116, "Д58 Инв."},
                    {117, "Д59"},
                    {118, "Д59 Инв."},
                    {119, "Д60"},
                    {120, "Д60 Инв."},
                    {121, "Д61"},
                    {122, "Д61 Инв."},
                    {123, "Д62"},
                    {124, "Д62 Инв."},
                    {125, "Д63"},
                    {126, "Д63 Инв."},
                    {127, "Д64"},
                    {128, "Д64 Инв."},
                    {129, "Д65"},
                    {130, "Д65 Инв."},
                    {131, "Д66"},
                    {132, "Д66 Инв."},
                    {133, "Д67"},
                    {134, "Д67 Инв."},
                    {135, "Д68"},
                    {136, "Д68 Инв."},
                    {137, "Д69"},
                    {138, "Д69 Инв."},
                    {139, "Д70"},
                    {140, "Д70 Инв."},
                    {141, "Д71"},
                    {142, "Д71 Инв."},
                    {143, "Д72"},
                    {144, "Д72 Инв."},
                    {145, "Д73"},
                    {146, "Д73 Инв."},
                    {147, "Д74"},
                    {148, "Д74 Инв."},
                    {149, "Д75"},
                    {150, "Д75 Инв."},
                    {151, "Д76"},
                    {152, "Д76 Инв."},
                    {153, "Д77"},
                    {154, "Д77 Инв."},
                    {155, "Д78"},
                    {156, "Д78 Инв."},
                    {157, "Д79"},
                    {158, "Д79 Инв."},
                    {159, "Д80"},
                    {160, "Д80 Инв."},
                    {161, "Д81"},
                    {162, "Д81 Инв."},
                    {163, "Д82"},
                    {164, "Д82 Инв."},
                    {165, "Д83"},
                    {166, "Д83 Инв."},
                    {167, "Д84"},
                    {168, "Д84 Инв."},
                    {169, "Д85"},
                    {170, "Д85 Инв."},
                    {171, "Д86"},
                    {172, "Д86 Инв."},
                    {173, "Д87"},
                    {174, "Д87 Инв."},
                    {175, "Д88"},
                    {176, "Д88 Инв."},
                    {177, "Д89"},
                    {178, "Д89 Инв."},
                    {179, "Д90"},
                    {180, "Д90 Инв."},
                    {181, "Д91"},
                    {182, "Д91 Инв."},
                    {183, "Д92"},
                    {184, "Д92 Инв."},
                    {185, "Д93"},
                    {186, "Д93 Инв."},
                    {187, "Д94"},
                    {188, "Д94 Инв."},
                    {189, "Д95"},
                    {190, "Д95 Инв."},
                    {191, "Д96"},
                    {192, "Д96 Инв."},
                    {193, "ЛС1"},
                    {194, "ЛС1 Инв."},
                    {195, "ЛС2"},
                    {196, "ЛС2 Инв."},
                    {197, "ЛС3"},
                    {198, "ЛС3 Инв."},
                    {199, "ЛС4"},
                    {200, "ЛС4 Инв."},
                    {201, "ЛС5"},
                    {202, "ЛС5 Инв."},
                    {203, "ЛС6"},
                    {204, "ЛС6 Инв."},
                    {205, "ЛС7"},
                    {206, "ЛС7 Инв."},
                    {207, "ЛС8"},
                    {208, "ЛС8 Инв."},
                    {209, "ЛС9"},
                    {210, "ЛС9 Инв."},
                    {211, "ЛС10"},
                    {212, "ЛС10 Инв."},
                    {213, "ЛС11"},
                    {214, "ЛС11 Инв."},
                    {215, "ЛС12"},
                    {216, "ЛС12 Инв."},
                    {217, "ЛС13"},
                    {218, "ЛС13 Инв."},
                    {219, "ЛС14"},
                    {220, "ЛС14 Инв."},
                    {221, "ЛС15"},
                    {222, "ЛС15 Инв."},
                    {223, "ЛС16"},
                    {224, "ЛС16 Инв."},
                    {225, "БГ1"},
                    {226, "БГ1 Инв."},
                    {227, "БГ2"},
                    {228, "БГ2 Инв."},
                    {229, "БГ3"},
                    {230, "БГ3 Инв."},
                    {231, "БГ4"},
                    {232, "БГ4 Инв."},
                    {233, "БГ5"},
                    {234, "БГ5 Инв."},
                    {235, "БГ6"},
                    {236, "БГ6 Инв."},
                    {237, "БГ7"},
                    {238, "БГ7 Инв."},
                    {239, "БГ8"},
                    {240, "БГ8 Инв."},
                    {241, "БГ9"},
                    {242, "БГ9 Инв."},
                    {243, "БГ10"},
                    {244, "БГ10 Инв."},
                    {245, "БГ11"},
                    {246, "БГ11 Инв."},
                    {247, "БГ12"},
                    {248, "БГ12 Инв."},
                    {249, "БГ13"},
                    {250, "БГ13 Инв."},
                    {251, "БГ14"},
                    {252, "БГ14 Инв."},
                    {253, "БГ15"},
                    {254, "БГ15 Инв."},
                    {255, "БГ16"},
                    {256, "БГ16 Инв."},
                    {257, "ВЛС1"},
                    {258, "ВЛС1 Инв."},
                    {259, "ВЛС2"},
                    {260, "ВЛС2 Инв."},
                    {261, "ВЛС3"},
                    {262, "ВЛС3 Инв."},
                    {263, "ВЛС4"},
                    {264, "ВЛС4 Инв."},
                    {265, "ВЛС5"},
                    {266, "ВЛС5 Инв."},
                    {267, "ВЛС6"},
                    {268, "ВЛС6 Инв."},
                    {269, "ВЛС7"},
                    {270, "ВЛС7 Инв."},
                    {271, "ВЛС8"},
                    {272, "ВЛС8 Инв."},
                    {273, "ВЛС9"},
                    {274, "ВЛС9 Инв."},
                    {275, "ВЛС10"},
                    {276, "ВЛС10 Инв."},
                    {277, "ВЛС11"},
                    {278, "ВЛС11 Инв."},
                    {279, "ВЛС12"},
                    {280, "ВЛС12 Инв."},
                    {281, "ВЛС13"},
                    {282, "ВЛС13 Инв."},
                    {283, "ВЛС14"},
                    {284, "ВЛС14 Инв."},
                    {285, "ВЛС15"},
                    {286, "ВЛС15 Инв."},
                    {287, "ВЛС16"},
                    {288, "ВЛС16 Инв."},
                    {289, "Iд1м СШ1 *"},
                    {290, "Iд1м СШ1* Инв."},
                    {291, "Iд1м СШ1"},
                    {292, "Iд1м СШ1 Инв."},
                    {293, "Iд2м СШ2 *"},
                    {294, "Iд2м СШ2* Инв."},
                    {295, "Iд2м СШ2"},
                    {296, "Iд2м СШ2 Инв."},
                    {297, "Iд3м ПО *"},
                    {298, "Iд3м ПО* Инв."},
                    {299, "Iд3м ПО"},
                    {300, "Iд3м ПО Инв."},
                    {301, "Iд1 СШ1 ИО"},
                    {302, "Iд1 СШ1 ИО Инв."},
                    {303, "Iд1 СШ1 *"},
                    {304, "Iд1 СШ1* Инв."},
                    {305, "Iд1 СШ1"},
                    {306, "Iд1 СШ1 Инв."},
                    {307, "Iд2 СШ2 ИО"},
                    {308, "Iд2 СШ2 ИО Инв."},
                    {309, "Iд2 СШ2 *"},
                    {310, "Iд2 СШ2* Инв."},
                    {311, "Iд2 СШ2"},
                    {312, "Iд2 СШ2 Инв."},
                    {313, "Iд3 ПО ИО"},
                    {314, "Iд3 ПО ИО Инв."},
                    {315, "Iд3 ПО *"},
                    {316, "Iд3 ПО* Инв."},
                    {317, "Iд3 ПО"},
                    {318, "Iд3 ПО Инв."},

                    {319, "I1> ИО"},
                    {320, "I1> ИО Инв."},
                    {321, "I1>"},
                    {322, "I1> Инв."},
                    {323, "I2> ИО"},
                    {324, "I2> ИО Инв."},
                    {325, "I2>"},
                    {326, "I2> Инв."},
                    {327, "I3> ИО"},
                    {328, "I3> ИО Инв."},
                    {329, "I3>"},
                    {330, "I3> Инв."},
                    {331, "I4> ИО"},
                    {332, "I4> ИО Инв."},
                    {333, "I4>"},
                    {334, "I4> Инв."},
                    {335, "I5> ИО"},
                    {336, "I5> ИО Инв."},
                    {337, "I5>"},
                    {338, "I5> Инв."},
                    {339, "I6> ИО"},
                    {340, "I6> ИО Инв."},
                    {341, "I6>"},
                    {342, "I6> Инв."},
                    {343, "I7> ИО"},
                    {344, "I7> ИО Инв."},
                    {345, "I7>"},
                    {346, "I7> Инв."},
                    {347, "I8> ИО"},
                    {348, "I8> ИО Инв."},
                    {349, "I8>"},
                    {350, "I8> Инв."},
                    {351, "I9> ИО"},
                    {352, "I9> ИО Инв."},
                    {353, "I9>"},
                    {354, "I9> Инв."},
                    {355, "I10> ИО"},
                    {356, "I10> ИО Инв."},
                    {357, "I10>"},
                    {358, "I10> Инв."},
                    {359, "I11> ИО"},
                    {360, "I11> ИО Инв."},
                    {361, "I11>"},
                    {362, "I11> Инв."},
                    {363, "I12> ИО"},
                    {364, "I12> ИО Инв."},
                    {365, "I12>"},
                    {366, "I12> Инв."},
                    {367, "I13> ИО"},
                    {368, "I13> ИО Инв."},
                    {369, "I13>"},
                    {370, "I13> Инв."},
                    {371, "I14> ИО"},
                    {372, "I14> ИО Инв."},
                    {373, "I14>"},
                    {374, "I14> Инв."},
                    {375, "I15> ИО"},
                    {376, "I15> ИО Инв."},
                    {377, "I15>"},
                    {378, "I15> Инв."},
                    {379, "I16> ИО"},
                    {380, "I16> ИО Инв."},
                    {381, "I16>"},
                    {382, "I16> Инв."},
                    {383, "I17> ИО"},
                    {384, "I17> ИО Инв."},
                    {385, "I17>"},
                    {386, "I17> Инв."},
                    {387, "I18> ИО"},
                    {388, "I18> ИО Инв."},
                    {389, "I18>"},
                    {390, "I18> Инв."},
                    {391, "I19> ИО"},
                    {392, "I19> ИО Инв."},
                    {393, "I19>"},
                    {394, "I19> Инв."},
                    {395, "I20> ИО"},
                    {396, "I20> ИО Инв."},
                    {397, "I20>"},
                    {398, "I20> Инв."},
                    {399, "I21> ИО"},
                    {400, "I21> ИО Инв."},
                    {401, "I21>"},
                    {402, "I21> Инв."},
                    {403, "I22> ИО"},
                    {404, "I22> ИО Инв."},
                    {405, "I22>"},
                    {406, "I22> Инв."},
                    {407, "I23> ИО"},
                    {408, "I23> ИО Инв."},
                    {409, "I23>"},
                    {410, "I23> Инв."},
                    {411, "I24> ИО"},
                    {412, "I24> ИО Инв."},
                    {413, "I24>"},
                    {414, "I24> Инв."},
                    {415, "I25> ИО"},
                    {416, "I25> ИО Инв."},
                    {417, "I25>"},
                    {418, "I25> Инв."},
                    {419, "I26> ИО"},
                    {420, "I26> ИО Инв."},
                    {421, "I26>"},
                    {422, "I26> Инв."},
                    {423, "I27> ИО"},
                    {424, "I27> ИО Инв."},
                    {425, "I27>"},
                    {426, "I27> Инв."},
                    {427, "I28> ИО"},
                    {428, "I28> ИО Инв."},
                    {429, "I28>"},
                    {430, "I28> Инв."},
                    {431, "I29> ИО"},
                    {432, "I29> ИО Инв."},
                    {433, "I29>"},
                    {434, "I29> Инв."},
                    {435, "I30> ИО"},
                    {436, "I30> ИО Инв."},
                    {437, "I30>"},
                    {438, "I30> Инв."},
                    {439, "I31> ИО"},
                    {440, "I31> ИО Инв."},
                    {441, "I31>"},
                    {442, "I31> Инв."},
                    {443, "I32> ИО"},
                    {444, "I32> ИО Инв."},
                    {445, "I32>"},
                    {446, "I32> Инв."},

                    {447, "ВНЕШ. 1"},
                    {448, "ВНЕШ. 1 Инв."},
                    {449, "ВНЕШ. 2"},
                    {450, "ВНЕШ. 2 Инв."},
                    {451, "ВНЕШ. 3"},
                    {452, "ВНЕШ. 3 Инв."},
                    {453, "ВНЕШ. 4"},
                    {454, "ВНЕШ. 4 Инв."},
                    {455, "ВНЕШ. 5"},
                    {456, "ВНЕШ. 5 Инв."},
                    {457, "ВНЕШ. 6"},
                    {458, "ВНЕШ. 6 Инв."},
                    {459, "ВНЕШ. 7"},
                    {460, "ВНЕШ. 7 Инв."},
                    {461, "ВНЕШ. 8"},
                    {462, "ВНЕШ. 8 Инв."},
                    {463, "ВНЕШ. 9"},
                    {464, "ВНЕШ. 9 Инв."},
                    {465, "ВНЕШ. 10"},
                    {466, "ВНЕШ. 10 Инв."},
                    {467, "ВНЕШ. 11"},
                    {468, "ВНЕШ. 11 Инв."},
                    {469, "ВНЕШ. 12"},
                    {470, "ВНЕШ. 12 Инв."},
                    {471, "ВНЕШ. 13"},
                    {472, "ВНЕШ. 13 Инв."},
                    {473, "ВНЕШ. 14"},
                    {474, "ВНЕШ. 14 Инв."},
                    {475, "ВНЕШ. 15"},
                    {476, "ВНЕШ. 15 Инв."},
                    {477, "ВНЕШ. 16"},
                    {478, "ВНЕШ. 16 Инв."},
                    {479, "ВНЕШ. 17"},
                    {480, "ВНЕШ. 17 Инв."},
                    {481, "ВНЕШ. 18"},
                    {482, "ВНЕШ. 18 Инв."},
                    {483, "ВНЕШ. 19"},
                    {484, "ВНЕШ. 19 Инв."},
                    {485, "ВНЕШ. 20"},
                    {486, "ВНЕШ. 20 Инв."},
                    {487, "ВНЕШ. 21"},
                    {488, "ВНЕШ. 21 Инв."},
                    {489, "ВНЕШ. 22"},
                    {490, "ВНЕШ. 22 Инв."},
                    {491, "ВНЕШ. 23"},
                    {492, "ВНЕШ. 23 Инв."},
                    {493, "ВНЕШ. 24"},
                    {494, "ВНЕШ. 24 Инв."},

                    {495, "ССЛ1"},
                    {496, "ССЛ1 Инв."},
                    {497, "ССЛ2"},
                    {498, "ССЛ2 Инв."},
                    {499, "ССЛ3"},
                    {500, "ССЛ3 Инв."},
                    {501, "ССЛ4"},
                    {502, "ССЛ4 Инв."},
                    {503, "ССЛ5"},
                    {504, "ССЛ5 Инв."},
                    {505, "ССЛ6"},
                    {506, "ССЛ6 Инв."},
                    {507, "ССЛ7"},
                    {508, "ССЛ7 Инв."},
                    {509, "ССЛ8"},
                    {510, "ССЛ8 Инв."},
                    {511, "ССЛ9"},
                    {512, "ССЛ9 Инв."},
                    {513, "ССЛ10"},
                    {514, "ССЛ10 Инв."},
                    {515, "ССЛ11"},
                    {516, "ССЛ11 Инв."},
                    {517, "ССЛ12"},
                    {518, "ССЛ12 Инв."},
                    {519, "ССЛ13"},
                    {520, "ССЛ13 Инв."},
                    {521, "ССЛ14"},
                    {522, "ССЛ14 Инв."},
                    {523, "ССЛ15"},
                    {524, "ССЛ15 Инв."},
                    {525, "ССЛ16"},
                    {526, "ССЛ16 Инв."},
                    {527, "ССЛ17"},
                    {528, "ССЛ17 Инв."},
                    {529, "ССЛ18"},
                    {530, "ССЛ18 Инв."},
                    {531, "ССЛ19"},
                    {532, "ССЛ19 Инв."},
                    {533, "ССЛ20"},
                    {534, "ССЛ20 Инв."},
                    {535, "ССЛ21"},
                    {536, "ССЛ21 Инв."},
                    {537, "ССЛ22"},
                    {538, "ССЛ22 Инв."},
                    {539, "ССЛ23"},
                    {540, "ССЛ23 Инв."},
                    {541, "ССЛ24"},
                    {542, "ССЛ24 Инв."},
                    {543, "ССЛ25"},
                    {544, "ССЛ25 Инв."},
                    {545, "ССЛ26"},
                    {546, "ССЛ26 Инв."},
                    {547, "ССЛ27"},
                    {548, "ССЛ27 Инв."},
                    {549, "ССЛ28"},
                    {550, "ССЛ28 Инв."},
                    {551, "ССЛ29"},
                    {552, "ССЛ29 Инв."},
                    {553, "ССЛ30"},
                    {554, "ССЛ30 Инв."},
                    {555, "ССЛ31"},
                    {556, "ССЛ31 Инв."},
                    {557, "ССЛ32"},
                    {558, "ССЛ32 Инв."},
                    {559, "ССЛ33"},
                    {560, "ССЛ33 Инв."},
                    {561, "ССЛ34"},
                    {562, "ССЛ34 Инв."},
                    {563, "ССЛ35"},
                    {564, "ССЛ35 Инв."},
                    {565, "ССЛ36"},
                    {566, "ССЛ36 Инв."},
                    {567, "ССЛ37"},
                    {568, "ССЛ37 Инв."},
                    {569, "ССЛ38"},
                    {570, "ССЛ38 Инв."},
                    {571, "ССЛ39"},
                    {572, "ССЛ39 Инв."},
                    {573, "ССЛ40"},
                    {574, "ССЛ40 Инв."},
                    {575, "ССЛ41"},
                    {576, "ССЛ41 Инв."},
                    {577, "ССЛ42"},
                    {578, "ССЛ42 Инв."},
                    {579, "ССЛ43"},
                    {580, "ССЛ43 Инв."},
                    {581, "ССЛ44"},
                    {582, "ССЛ44 Инв."},
                    {583, "ССЛ45"},
                    {584, "ССЛ45 Инв."},
                    {585, "ССЛ46"},
                    {586, "ССЛ46 Инв."},
                    {587, "ССЛ47"},
                    {588, "ССЛ47 Инв."},
                    {589, "ССЛ48"},
                    {590, "ССЛ48 Инв."},

                    {591, "УРОВ СШ1"},
                    {592, "УРОВ СШ1 Инв."},
                    {593, "УРОВ СШ2"},
                    {594, "УРОВ СШ2 Инв."},
                    {595, "УРОВ ПО"},
                    {596, "УРОВ ПО Инв."},

                    {597, "УРОВ Пр1"},
                    {598, "УРОВ Пр1 Инв."},
                    {599, "УРОВ Пр2"},
                    {600, "УРОВ Пр2 Инв."},
                    {601, "УРОВ Пр3"},
                    {602, "УРОВ Пр3 Инв."},
                    {603, "УРОВ Пр4"},
                    {604, "УРОВ Пр4 Инв."},
                    {605, "УРОВ Пр5"},
                    {606, "УРОВ Пр5 Инв."},
                    {607, "УРОВ Пр6"},
                    {608, "УРОВ Пр6 Инв."},
                    {609, "УРОВ Пр7"},
                    {610, "УРОВ Пр7 Инв."},
                    {611, "УРОВ Пр8"},
                    {612, "УРОВ Пр8 Инв."},
                    {613, "УРОВ Пр9"},
                    {614, "УРОВ Пр9 Инв."},
                    {615, "УРОВ Пр10"},
                    {616, "УРОВ Пр10 Инв."},
                    {617, "УРОВ Пр11"},
                    {618, "УРОВ Пр11 Инв."},
                    {619, "УРОВ Пр12"},
                    {620, "УРОВ Пр12 Инв."},
                    {621, "УРОВ Пр13"},
                    {622, "УРОВ Пр13 Инв."},
                    {623, "УРОВ Пр14"},
                    {624, "УРОВ Пр14 Инв."},
                    {625, "УРОВ Пр15"},
                    {626, "УРОВ Пр15 Инв."},
                    {627, "УРОВ Пр16"},
                    {628, "УРОВ Пр16 Инв."},
                    {629, "УРОВ Пр17"},
                    {630, "УРОВ Пр17 Инв."},
                    {631, "УРОВ Пр18"},
                    {632, "УРОВ Пр18 Инв."},
                    {633, "УРОВ Пр19"},
                    {634, "УРОВ Пр19 Инв."},
                    {635, "УРОВ Пр20"},
                    {636, "УРОВ Пр20 Инв."},
                    {637, "УРОВ Пр21"},
                    {638, "УРОВ Пр21 Инв."},
                    {639, "УРОВ Пр22"},
                    {640, "УРОВ Пр22 Инв."},
                    {641, "УРОВ Пр23"},
                    {642, "УРОВ Пр23 Инв."},
                    {643, "УРОВ Пр24"},
                    {644, "УРОВ Пр24 Инв."},

                    {645, "Откл.СШ1"},
                    {646, "Откл.СШ1 Инв."},
                    {647, "Откл.СШ2"},
                    {648, "Откл.СШ2 Инв."},
                    {649, "Откл.ПО"},
                    {650, "Откл.ПО Инв."},

                    {651, "Откл.Пр1"},
                    {652, "Откл.Пр1 Инв."},
                    {653, "Откл.Пр2"},
                    {654, "Откл.Пр2 Инв."},
                    {655, "Откл.Пр3"},
                    {656, "Откл.Пр3 Инв."},
                    {657, "Откл.Пр4"},
                    {658, "Откл.Пр4 Инв."},
                    {659, "Откл.Пр5"},
                    {660, "Откл.Пр5 Инв."},
                    {661, "Откл.Пр6"},
                    {662, "Откл.Пр6 Инв."},
                    {663, "Откл.Пр7"},
                    {664, "Откл.Пр7 Инв."},
                    {665, "Откл.Пр8"},
                    {666, "Откл.Пр8 Инв."},
                    {667, "Откл.Пр9"},
                    {668, "Откл.Пр9 Инв."},
                    {669, "Откл.Пр10"},
                    {670, "Откл.Пр10 Инв."},
                    {671, "Откл.Пр11"},
                    {672, "Откл.Пр11 Инв."},
                    {673, "Откл.Пр12"},
                    {674, "Откл.Пр12 Инв."},
                    {675, "Откл.Пр13"},
                    {676, "Откл.Пр13 Инв."},
                    {677, "Откл.Пр14"},
                    {678, "Откл.Пр14 Инв."},
                    {679, "Откл.Пр15"},
                    {680, "Откл.Пр15 Инв."},
                    {681, "Откл.Пр16"},
                    {682, "Откл.Пр16 Инв."},
                    {683, "Откл.Пр17"},
                    {684, "Откл.Пр17 Инв."},
                    {685, "Откл.Пр18"},
                    {686, "Откл.Пр18 Инв."},
                    {687, "Откл.Пр19"},
                    {688, "Откл.Пр19 Инв."},
                    {689, "Откл.Пр20"},
                    {690, "Откл.Пр20 Инв."},
                    {691, "Откл.Пр21"},
                    {692, "Откл.Пр21 Инв."},
                    {693, "Откл.Пр22"},
                    {694, "Откл.Пр22 Инв."},
                    {695, "Откл.Пр23"},
                    {696, "Откл.Пр23 Инв."},
                    {697, "Откл.Пр24"},
                    {698, "Откл.Пр24 Инв."},

                    {699, "НЕИСПР."},
                    {700, "НЕИСПР. Инв."},
                    {701, "ОСН.ГРУППА"},
                    {702, "ОСН.ГРУППА Инв."},
                    {703, "РЕЗ.ГРУППА"},
                    {704, "РЕЗ.ГРУППА Инв."},
                    {705, "АВАРИЯ"},
                    {706, "АВАРИЯ Инв."},
                    {707, "СИГНАЛ-ЦИЯ"},
                    {708, "СИГНАЛ-ЦИЯ Инв."},
                    {709, "НЕИСПР. ТТ ОБЩ."},
                    {710, "НЕИСПР. ТТ ОБЩ. Инв."},
                    {711, "НЕИСПР. ТТ СШ1"},
                    {712, "НЕИСПР. ТТ СШ1 Инв."},
                    {713, "НЕИСПР. ТТ СШ2"},
                    {714, "НЕИСПР. ТТ СШ2 Инв."}
                };
                switch (ConfigurationStructBigV210.DeviceModelType)
                {
                    case "A1":
                    {
                        //дискреты
                        for (ushort i = 129; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 258; i++)
                        {
                            ret.Remove(i);
                        }

                        //присоединения УРОВ
                        for (ushort i = 629; i < 645; i++)
                        {
                            ret.Remove(i);
                        }

                        //присоединения
                        for (ushort i = 683; i < 699; i++)
                        {
                            ret.Remove(i);
                        }
                    }
                        break;
                    case "A2":
                    {
                        //дискреты
                        for (ushort i = 81; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }
                    }
                        break;
                    case "A3":
                    {
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }
                    }
                        break;
                    case "A4":
                    {
                        //дискреты
                        for (ushort i = 65; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }
                    }
                        break;
                    case "A5":
                    {
                        //дискреты
                        for (ushort i = 81; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }

                        //ВЗ
                        ushort key = 487;
                        ret[key] = "U> ИО";
                        ret[++key] = "U> ИО Инв.";
                        ret[++key] = "U>";
                        ret[++key] = "U> Инв.";
                        ret[++key] = "U>> ИО";
                        ret[++key] = "U>> ИО Инв.";
                        ret[++key] = "U>>";
                        ret[++key] = "U>> Инв.";
                        //Прис. УРОВ
                        key = 637;
                        ret[key] = "U< ИО";
                        ret[++key] = "U< ИО Инв.";
                        ret[++key] = "U<";
                        ret[++key] = "U< Инв.";
                        ret[++key] = "U<< ИО";
                        ret[++key] = "U<< ИО Инв.";
                        ret[++key] = "U<<";
                        ret[++key] = "U<< Инв.";
                        //Прис.
                        for (ushort i = 691; i < 699; i++)
                        {
                            ret.Remove(i);
                        }

                        key = 691;
                        ret[key] = "Неиспр. ТН";
                        ret[++key] = "Неиспр. ТН Инв.";
                        ret[++key] = "Неиспр. 3U0";
                        ret[++key] = "Неиспр. 3U0 Инв.";
                        ret[++key] = "Неиспр. кНС";
                        ret[++key] = "Неиспр. кНс Инв.";
                    }
                        break;
                    case "A6":
                    {
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }

                        //ВЗ
                        ushort key = 487;
                        ret[key] = "U> ИО";
                        ret[++key] = "U> ИО Инв.";
                        ret[++key] = "U>";
                        ret[++key] = "U> Инв.";
                        ret[++key] = "U>> ИО";
                        ret[++key] = "U>> ИО Инв.";
                        ret[++key] = "U>>";
                        ret[++key] = "U>> Инв.";
                        //Прис. УРОВ
                        key = 637;
                        ret[key] = "U< ИО";
                        ret[++key] = "U< ИО Инв.";
                        ret[++key] = "U<";
                        ret[++key] = "U< Инв.";
                        ret[++key] = "U<< ИО";
                        ret[++key] = "U<< ИО Инв.";
                        ret[++key] = "U<<";
                        ret[++key] = "U<< Инв.";
                        //Прис.
                        for (ushort i = 691; i < 699; i++)
                        {
                            ret.Remove(i);
                        }

                        key = 691;
                        ret[key] = "Неиспр. ТН";
                        ret[++key] = "Неиспр. ТН Инв.";
                        ret[++key] = "Неиспр. 3U0";
                        ret[++key] = "Неиспр. 3U0 Инв.";
                        ret[++key] = "Неиспр. кНС";
                        ret[++key] = "Неиспр. кНс Инв.";
                    }
                        break;
                    case "A7":
                    {
                        //дискреты
                        for (ushort i = 65; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }

                        //ВЗ
                        ushort key = 487;
                        ret[key] = "U> ИО";
                        ret[++key] = "U> ИО Инв.";
                        ret[++key] = "U>";
                        ret[++key] = "U> Инв.";
                        ret[++key] = "U>> ИО";
                        ret[++key] = "U>> ИО Инв.";
                        ret[++key] = "U>>";
                        ret[++key] = "U>> Инв.";
                        //Прис. УРОВ
                        key = 637;
                        ret[key] = "U< ИО";
                        ret[++key] = "U< ИО Инв.";
                        ret[++key] = "U<";
                        ret[++key] = "U< Инв.";
                        ret[++key] = "U<< ИО";
                        ret[++key] = "U<< ИО Инв.";
                        ret[++key] = "U<<";
                        ret[++key] = "U<< Инв.";
                        //Прис.
                        for (ushort i = 691; i < 699; i++)
                        {
                            ret.Remove(i);
                        }

                        key = 691;
                        ret[key] = "Неиспр. ТН";
                        ret[++key] = "Неиспр. ТН Инв.";
                        ret[++key] = "Неиспр. 3U0";
                        ret[++key] = "Неиспр. 3U0 Инв.";
                        ret[++key] = "Неиспр. кНС";
                        ret[++key] = "Неиспр. кНс Инв.";
                    }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }

                        //присоединения УРОВ
                        for (ushort i = 629; i < 645; i++)
                        {
                            ret.Remove(i);
                        }

                        //присоединения
                        for (ushort i = 683; i < 699; i++)
                        {
                            ret.Remove(i);
                        }

                        break;
                }
                
                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }
        

        public static List<string> RelaySignalsBig213List
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Нет",
                    "Д1",
                    "Д1 Инв.",
                    "Д2",
                    "Д2 Инв.",
                    "Д3",
                    "Д3 Инв.",
                    "Д4",
                    "Д4 Инв.",
                    "Д5",
                    "Д5 Инв.",
                    "Д6",
                    "Д6 Инв.",
                    "Д7",
                    "Д7 Инв.",
                    "Д8",
                    "Д8 Инв.",
                    "Д9",
                    "Д9 Инв.",
                    "Д10",
                    "Д10 Инв.",
                    "Д11",
                    "Д11 Инв.",
                    "Д12",
                    "Д12 Инв.",
                    "Д13",
                    "Д13 Инв.",
                    "Д14",
                    "Д14 Инв.",
                    "Д15",
                    "Д15 Инв.",
                    "Д16",
                    "Д16 Инв.",
                    "Д17",
                    "Д17 Инв.",
                    "Д18",
                    "Д18 Инв.",
                    "Д19",
                    "Д19 Инв.",
                    "Д20",
                    "Д20 Инв.",
                    "Д21",
                    "Д21 Инв.",
                    "Д22",
                    "Д22 Инв.",
                    "Д23",
                    "Д23 Инв.",
                    "Д24",
                    "Д24 Инв.",
                    "Д25",
                    "Д25 Инв.",
                    "Д26",
                    "Д26 Инв.",
                    "Д27",
                    "Д27 Инв.",
                    "Д28",
                    "Д28 Инв.",
                    "Д29",
                    "Д29 Инв.",
                    "Д30",
                    "Д30 Инв.",
                    "Д31",
                    "Д31 Инв.",
                    "Д32",
                    "Д32 Инв.",
                    "Д33",
                    "Д33 Инв.",
                    "Д34",
                    "Д34 Инв.",
                    "Д35",
                    "Д35 Инв.",
                    "Д36",
                    "Д36 Инв.",
                    "Д37",
                    "Д37 Инв.",
                    "Д38",
                    "Д38 Инв.",
                    "Д39",
                    "Д39 Инв.",
                    "Д40",
                    "Д40 Инв.",
                    "Д41",
                    "Д41 Инв.",
                    "Д42",
                    "Д42 Инв.",
                    "Д43",
                    "Д43 Инв.",
                    "Д44",
                    "Д44 Инв.",
                    "Д45",
                    "Д45 Инв.",
                    "Д46",
                    "Д46 Инв.",
                    "Д47",
                    "Д47 Инв.",
                    "Д48",
                    "Д48 Инв.",
                    "Д49",
                    "Д49 Инв.",
                    "Д50",
                    "Д50 Инв.",
                    "Д51",
                    "Д51 Инв.",
                    "Д52",
                    "Д52 Инв.",
                    "Д53",
                    "Д53 Инв.",
                    "Д54",
                    "Д54 Инв.",
                    "Д55",
                    "Д55 Инв.",
                    "Д56",
                    "Д56 Инв.",
                    "Д57",
                    "Д57 Инв.",
                    "Д58",
                    "Д58 Инв.",
                    "Д59",
                    "Д59 Инв.",
                    "Д60",
                    "Д60 Инв.",
                    "Д61",
                    "Д61 Инв.",
                    "Д62",
                    "Д62 Инв.",
                    "Д63",
                    "Д63 Инв.",
                    "Д64",
                    "Д64 Инв.",
                    "Д65",
                    "Д65 Инв.",
                    "Д66",
                    "Д66 Инв.",
                    "Д67",
                    "Д67 Инв.",
                    "Д68",
                    "Д68 Инв.",
                    "Д69",
                    "Д69 Инв.",
                    "Д70",
                    "Д70 Инв.",
                    "Д71",
                    "Д71 Инв.",
                    "Д72",
                    "Д72 Инв.",
                    "Д73",
                    "Д73 Инв.",
                    "Д74",
                    "Д74 Инв.",
                    "Д75",
                    "Д75 Инв.",
                    "Д76",
                    "Д76 Инв.",
                    "Д77",
                    "Д77 Инв.",
                    "Д78",
                    "Д78 Инв.",
                    "Д79",
                    "Д79 Инв.",
                    "Д80",
                    "Д80 Инв.",
                    "Д81",
                    "Д81 Инв.",
                    "Д82",
                    "Д82 Инв.",
                    "Д83",
                    "Д83 Инв.",
                    "Д84",
                    "Д84 Инв.",
                    "Д85",
                    "Д85 Инв.",
                    "Д86",
                    "Д86 Инв.",
                    "Д87",
                    "Д87 Инв.",
                    "Д88",
                    "Д88 Инв.",
                    "Д89",
                    "Д89 Инв.",
                    "Д90",
                    "Д90 Инв.",
                    "Д91",
                    "Д91 Инв.",
                    "Д92",
                    "Д92 Инв.",
                    "Д93",
                    "Д93 Инв.",
                    "Д94",
                    "Д94 Инв.",
                    "Д95",
                    "Д95 Инв.",
                    "Д96",
                    "Д96 Инв.",
                    "ЛС1",
                    "ЛС1 Инв.",
                    "ЛС2",
                    "ЛС2 Инв.",
                    "ЛС3",
                    "ЛС3 Инв.",
                    "ЛС4",
                    "ЛС4 Инв.",
                    "ЛС5",
                    "ЛС5 Инв.",
                    "ЛС6",
                    "ЛС6 Инв.",
                    "ЛС7",
                    "ЛС7 Инв.",
                    "ЛС8",
                    "ЛС8 Инв.",
                    "ЛС9",
                    "ЛС9 Инв.",
                    "ЛС10",
                    "ЛС10 Инв.",
                    "ЛС11",
                    "ЛС11 Инв.",
                    "ЛС12",
                    "ЛС12 Инв.",
                    "ЛС13",
                    "ЛС13 Инв.",
                    "ЛС14",
                    "ЛС14 Инв.",
                    "ЛС15",
                    "ЛС15 Инв.",
                    "ЛС16",
                    "ЛС16 Инв.",
                    "БГ1",
                    "БГ1 Инв.",
                    "БГ2",
                    "БГ2 Инв.",
                    "БГ3",
                    "БГ3 Инв.",
                    "БГ4",
                    "БГ4 Инв.",
                    "БГ5",
                    "БГ5 Инв.",
                    "БГ6",
                    "БГ6 Инв.",
                    "БГ7",
                    "БГ7 Инв.",
                    "БГ8",
                    "БГ8 Инв.",
                    "БГ9",
                    "БГ9 Инв.",
                    "БГ10",
                    "БГ10 Инв.",
                    "БГ11",
                    "БГ11 Инв.",
                    "БГ12",
                    "БГ12 Инв.",
                    "БГ13",
                    "БГ13 Инв.",
                    "БГ14",
                    "БГ14 Инв.",
                    "БГ15",
                    "БГ15 Инв.",
                    "БГ16",
                    "БГ16 Инв.",
                    "ВЛС1",
                    "ВЛС1 Инв.",
                    "ВЛС2",
                    "ВЛС2 Инв.",
                    "ВЛС3",
                    "ВЛС3 Инв.",
                    "ВЛС4",
                    "ВЛС4 Инв.",
                    "ВЛС5",
                    "ВЛС5 Инв.",
                    "ВЛС6",
                    "ВЛС6 Инв.",
                    "ВЛС7",
                    "ВЛС7 Инв.",
                    "ВЛС8",
                    "ВЛС8 Инв.",
                    "ВЛС9",
                    "ВЛС9 Инв.",
                    "ВЛС10",
                    "ВЛС10 Инв.",
                    "ВЛС11",
                    "ВЛС11 Инв.",
                    "ВЛС12",
                    "ВЛС12 Инв.",
                    "ВЛС13",
                    "ВЛС13 Инв.",
                    "ВЛС14",
                    "ВЛС14 Инв.",
                    "ВЛС15",
                    "ВЛС15 Инв.",
                    "ВЛС16",
                    "ВЛС16 Инв.",
                    "Iд1м СШ1 *",
                    "Iд1м СШ1* Инв.",
                    "Iд1м СШ1",
                    "Iд1м СШ1 Инв.",
                    "Iд2м СШ2 *",
                    "Iд2м СШ2* Инв.",
                    "Iд2м СШ2",
                    "Iд2м СШ2 Инв.",
                    "Iд3м ПО *",
                    "Iд3м ПО* Инв.",
                    "Iд3м ПО",
                    "Iд3м ПО Инв.",
                    "Iд1 СШ1 ИО",
                    "Iд1 СШ1 ИО Инв.",
                    "Iд1 СШ1 *",
                    "Iд1 СШ1* Инв.",
                    "Iд1 СШ1",
                    "Iд1 СШ1 Инв.",
                    "Iд2 СШ2 ИО",
                    "Iд2 СШ2 ИО Инв.",
                    "Iд2 СШ2 *",
                    "Iд2 СШ2* Инв.",
                    "Iд2 СШ2",
                    "Iд2 СШ2 Инв.",
                    "Iд3 ПО ИО",
                    "Iд3 ПО ИО Инв.",
                    "Iд3 ПО *",
                    "Iд3 ПО* Инв.",
                    "Iд3 ПО",
                    "Iд3 ПО Инв.",

                    "I1> ИО",
                    "I1> ИО Инв.",
                    "I1>",
                    "I1> Инв.",
                    "I2> ИО",
                    "I2> ИО Инв.",
                    "I2>",
                    "I2> Инв.",
                    "I3> ИО",
                    "I3> ИО Инв.",
                    "I3>",
                    "I3> Инв.",
                    "I4> ИО",
                    "I4> ИО Инв.",
                    "I4>",
                    "I4> Инв.",
                    "I5> ИО",
                    "I5> ИО Инв.",
                    "I5>",
                    "I5> Инв.",
                    "I6> ИО",
                    "I6> ИО Инв.",
                    "I6>",
                    "I6> Инв.",
                    "I7> ИО",
                    "I7> ИО Инв.",
                    "I7>",
                    "I7> Инв.",
                    "I8> ИО",
                    "I8> ИО Инв.",
                    "I8>",
                    "I8> Инв.",
                    "I9> ИО",
                    "I9> ИО Инв.",
                    "I9>",
                    "I9> Инв.",
                    "I10> ИО",
                    "I10> ИО Инв.",
                    "I10>",
                    "I10> Инв.",
                    "I11> ИО",
                    "I11> ИО Инв.",
                    "I11>",
                    "I11> Инв.",
                    "I12> ИО",
                    "I12> ИО Инв.",
                    "I12>",
                    "I12> Инв.",
                    "I13> ИО",
                    "I13> ИО Инв.",
                    "I13>",
                    "I13> Инв.",
                    "I14> ИО",
                    "I14> ИО Инв.",
                    "I14>",
                    "I14> Инв.",
                    "I15> ИО",
                    "I15> ИО Инв.",
                    "I15>",
                    "I15> Инв.",
                    "I16> ИО",
                    "I16> ИО Инв.",
                    "I16>",
                    "I16> Инв.",
                    "I17> ИО",
                    "I17> ИО Инв.",
                    "I17>",
                    "I17> Инв.",
                    "I18> ИО",
                    "I18> ИО Инв.",
                    "I18>",
                    "I18> Инв.",
                    "I19> ИО",
                    "I19> ИО Инв.",
                    "I19>",
                    "I19> Инв.",
                    "I20> ИО",
                    "I20> ИО Инв.",
                    "I20>",
                    "I20> Инв.",
                    "I21> ИО",
                    "I21> ИО Инв.",
                    "I21>",
                    "I21> Инв.",
                    "I22> ИО",
                    "I22> ИО Инв.",
                    "I22>",
                    "I22> Инв.",
                    "I23> ИО",
                    "I23> ИО Инв.",
                    "I23>",
                    "I23> Инв.",
                    "I24> ИО",
                    "I24> ИО Инв.",
                    "I24>",
                    "I24> Инв.",
                    "I25> ИО",
                    "I25> ИО Инв.",
                    "I25>",
                    "I25> Инв.",
                    "I26> ИО",
                    "I26> ИО Инв.",
                    "I26>",
                    "I26> Инв.",
                    "I27> ИО",
                    "I27> ИО Инв.",
                    "I27>",
                    "I27> Инв.",
                    "I28> ИО",
                    "I28> ИО Инв.",
                    "I28>",
                    "I28> Инв.",
                    "I29> ИО",
                    "I29> ИО Инв.",
                    "I29>",
                    "I29> Инв.",
                    "I30> ИО",
                    "I30> ИО Инв.",
                    "I30>",
                    "I30> Инв.",
                    "I31> ИО",
                    "I31> ИО Инв.",
                    "I31>",
                    "I31> Инв.",
                    "I32> ИО",
                    "I32> ИО Инв.",
                    "I32>",
                    "I32> Инв.",

                    "ВНЕШ. 1",
                    "ВНЕШ. 1 Инв.",
                    "ВНЕШ. 2",
                    "ВНЕШ. 2 Инв.",
                    "ВНЕШ. 3",
                    "ВНЕШ. 3 Инв.",
                    "ВНЕШ. 4",
                    "ВНЕШ. 4 Инв.",
                    "ВНЕШ. 5",
                    "ВНЕШ. 5 Инв.",
                    "ВНЕШ. 6",
                    "ВНЕШ. 6 Инв.",
                    "ВНЕШ. 7",
                    "ВНЕШ. 7 Инв.",
                    "ВНЕШ. 8",
                    "ВНЕШ. 8 Инв.",
                    "ВНЕШ. 9",
                    "ВНЕШ. 9 Инв.",
                    "ВНЕШ. 10",
                    "ВНЕШ. 10 Инв.",
                    "ВНЕШ. 11",
                    "ВНЕШ. 11 Инв.",
                    "ВНЕШ. 12",
                    "ВНЕШ. 12 Инв.",
                    "ВНЕШ. 13",
                    "ВНЕШ. 13 Инв.",
                    "ВНЕШ. 14",
                    "ВНЕШ. 14 Инв.",
                    "ВНЕШ. 15",
                    "ВНЕШ. 15 Инв.",
                    "ВНЕШ. 16",
                    "ВНЕШ. 16 Инв.",
                    "ВНЕШ. 17",
                    "ВНЕШ. 17 Инв.",
                    "ВНЕШ. 18",
                    "ВНЕШ. 18 Инв.",
                    "ВНЕШ. 19",
                    "ВНЕШ. 19 Инв.",
                    "ВНЕШ. 20",
                    "ВНЕШ. 20 Инв.",
                    "ВНЕШ. 21",
                    "ВНЕШ. 21 Инв.",
                    "ВНЕШ. 22",
                    "ВНЕШ. 22 Инв.",
                    "ВНЕШ. 23",
                    "ВНЕШ. 23 Инв.",
                    "ВНЕШ. 24",
                    "ВНЕШ. 24 Инв.",

                    "ССЛ1",
                    "ССЛ1 Инв.",
                    "ССЛ2",
                    "ССЛ2 Инв.",
                    "ССЛ3",
                    "ССЛ3 Инв.",
                    "ССЛ4",
                    "ССЛ4 Инв.",
                    "ССЛ5",
                    "ССЛ5 Инв.",
                    "ССЛ6",
                    "ССЛ6 Инв.",
                    "ССЛ7",
                    "ССЛ7 Инв.",
                    "ССЛ8",
                    "ССЛ8 Инв.",
                    "ССЛ9",
                    "ССЛ9 Инв.",
                    "ССЛ10",
                    "ССЛ10 Инв.",
                    "ССЛ11",
                    "ССЛ11 Инв.",
                    "ССЛ12",
                    "ССЛ12 Инв.",
                    "ССЛ13",
                    "ССЛ13 Инв.",
                    "ССЛ14",
                    "ССЛ14 Инв.",
                    "ССЛ15",
                    "ССЛ15 Инв.",
                    "ССЛ16",
                    "ССЛ16 Инв.",
                    "ССЛ17",
                    "ССЛ17 Инв.",
                    "ССЛ18",
                    "ССЛ18 Инв.",
                    "ССЛ19",
                    "ССЛ19 Инв.",
                    "ССЛ20",
                    "ССЛ20 Инв.",
                    "ССЛ21",
                    "ССЛ21 Инв.",
                    "ССЛ22",
                    "ССЛ22 Инв.",
                    "ССЛ23",
                    "ССЛ23 Инв.",
                    "ССЛ24",
                    "ССЛ24 Инв.",
                    "ССЛ25",
                    "ССЛ25 Инв.",
                    "ССЛ26",
                    "ССЛ26 Инв.",
                    "ССЛ27",
                    "ССЛ27 Инв.",
                    "ССЛ28",
                    "ССЛ28 Инв.",
                    "ССЛ29",
                    "ССЛ29 Инв.",
                    "ССЛ30",
                    "ССЛ30 Инв.",
                    "ССЛ31",
                    "ССЛ31 Инв.",
                    "ССЛ32",
                    "ССЛ32 Инв.",
                    "ССЛ33",
                    "ССЛ33 Инв.",
                    "ССЛ34",
                    "ССЛ34 Инв.",
                    "ССЛ35",
                    "ССЛ35 Инв.",
                    "ССЛ36",
                    "ССЛ36 Инв.",
                    "ССЛ37",
                    "ССЛ37 Инв.",
                    "ССЛ38",
                    "ССЛ38 Инв.",
                    "ССЛ39",
                    "ССЛ39 Инв.",
                    "ССЛ40",
                    "ССЛ40 Инв.",
                    "ССЛ41",
                    "ССЛ41 Инв.",
                    "ССЛ42",
                    "ССЛ42 Инв.",
                    "ССЛ43",
                    "ССЛ43 Инв.",
                    "ССЛ44",
                    "ССЛ44 Инв.",
                    "ССЛ45",
                    "ССЛ45 Инв.",
                    "ССЛ46",
                    "ССЛ46 Инв.",
                    "ССЛ47",
                    "ССЛ47 Инв.",
                    "ССЛ48",
                    "ССЛ48 Инв.",

                    "УРОВ СШ1",
                    "УРОВ СШ1 Инв.",
                    "УРОВ СШ2",
                    "УРОВ СШ2 Инв.",
                    "УРОВ ПО",
                    "УРОВ ПО Инв.",

                    "УРОВ Пр1",
                    "УРОВ Пр1 Инв.",
                    "УРОВ Пр2",
                    "УРОВ Пр2 Инв.",
                    "УРОВ Пр3",
                    "УРОВ Пр3 Инв.",
                    "УРОВ Пр4",
                    "УРОВ Пр4 Инв.",
                    "УРОВ Пр5",
                    "УРОВ Пр5 Инв.",
                    "УРОВ Пр6",
                    "УРОВ Пр6 Инв.",
                    "УРОВ Пр7",
                    "УРОВ Пр7 Инв.",
                    "УРОВ Пр8",
                    "УРОВ Пр8 Инв.",
                    "УРОВ Пр9",
                    "УРОВ Пр9 Инв.",
                    "УРОВ Пр10",
                    "УРОВ Пр10 Инв.",
                    "УРОВ Пр11",
                    "УРОВ Пр11 Инв.",
                    "УРОВ Пр12",
                    "УРОВ Пр12 Инв.",
                    "УРОВ Пр13",
                    "УРОВ Пр13 Инв.",
                    "УРОВ Пр14",
                    "УРОВ Пр14 Инв.",
                    "УРОВ Пр15",
                    "УРОВ Пр15 Инв.",
                    "УРОВ Пр16",
                    "УРОВ Пр16 Инв.",
                    "УРОВ Пр17",
                    "УРОВ Пр17 Инв.",
                    "УРОВ Пр18",
                    "УРОВ Пр18 Инв.",
                    "УРОВ Пр19",
                    "УРОВ Пр19 Инв.",
                    "УРОВ Пр20",
                    "УРОВ Пр20 Инв.",
                    "УРОВ Пр21",
                    "УРОВ Пр21 Инв.",
                    "УРОВ Пр22",
                    "УРОВ Пр22 Инв.",
                    "УРОВ Пр23",
                    "УРОВ Пр23 Инв.",
                    "УРОВ Пр24",
                    "УРОВ Пр24 Инв.",

                    "Откл.СШ1",
                    "Откл.СШ1 Инв.",
                    "Откл.СШ2",
                    "Откл.СШ2 Инв.",
                    "Откл.ПО",
                    "Откл.ПО Инв.",

                    "Откл.Пр1",
                    "Откл.Пр1 Инв.",
                    "Откл.Пр2",
                    "Откл.Пр2 Инв.",
                    "Откл.Пр3",
                    "Откл.Пр3 Инв.",
                    "Откл.Пр4",
                    "Откл.Пр4 Инв.",
                    "Откл.Пр5",
                    "Откл.Пр5 Инв.",
                    "Откл.Пр6",
                    "Откл.Пр6 Инв.",
                    "Откл.Пр7",
                    "Откл.Пр7 Инв.",
                    "Откл.Пр8",
                    "Откл.Пр8 Инв.",
                    "Откл.Пр9",
                    "Откл.Пр9 Инв.",
                    "Откл.Пр10",
                    "Откл.Пр10 Инв.",
                    "Откл.Пр11",
                    "Откл.Пр11 Инв.",
                    "Откл.Пр12",
                    "Откл.Пр12 Инв.",
                    "Откл.Пр13",
                    "Откл.Пр13 Инв.",
                    "Откл.Пр14",
                    "Откл.Пр14 Инв.",
                    "Откл.Пр15",
                    "Откл.Пр15 Инв.",
                    "Откл.Пр16",
                    "Откл.Пр16 Инв.",
                    "Откл.Пр17",
                    "Откл.Пр17 Инв.",
                    "Откл.Пр18",
                    "Откл.Пр18 Инв.",
                    "Откл.Пр19",
                    "Откл.Пр19 Инв.",
                    "Откл.Пр20",
                    "Откл.Пр20 Инв.",
                    "Откл.Пр21",
                    "Откл.Пр21 Инв.",
                    "Откл.Пр22",
                    "Откл.Пр22 Инв.",
                    "Откл.Пр23",
                    "Откл.Пр23 Инв.",
                    "Откл.Пр24",
                    "Откл.Пр24 Инв.",

                    "НЕИСПР.",
                    "НЕИСПР. Инв.",
                    "ОСН.ГРУППА",
                    "ОСН.ГРУППА Инв.",
                    "РЕЗ.ГРУППА",
                    "РЕЗ.ГРУППА Инв.",
                    "АВАРИЯ",
                    "АВАРИЯ Инв.",
                    "СИГНАЛ-ЦИЯ",
                    "СИГНАЛ-ЦИЯ Инв.",
                    "НЕИСПР. ТТ ОБЩ.",
                    "НЕИСПР. ТТ ОБЩ. Инв.",
                    "НЕИСПР. ТТ СШ1",
                    "НЕИСПР. ТТ СШ1 Инв.",
                    "НЕИСПР. ТТ СШ2",
                    "НЕИСПР. ТТ СШ2 Инв."
                };
                switch (ConfigurationStructBigV213.DeviceModelType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (ushort i = 129; i < 193; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //быстрые гусы
                            for (ushort i = 225; i < 258; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //присоединения УРОВ
                            for (ushort i = 629; i < 645; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //присоединения
                            for (ushort i = 683; i < 699; i++)
                            {
                                ret.RemoveAt(i);
                            }
                        }
                        break;
                    case "A2":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.RemoveAt(i);
                            }
                        }
                        break;
                    case "A3":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.RemoveAt(i);
                            }
                        }
                        break;
                    case "A4":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.RemoveAt(i);
                            }
                        }
                        break;
                    case "A5":
                        {
                            //дискреты
                            for (ushort j = 81; j < 193; j++)
                            {
                                ret.RemoveAt(81);
                            }

                            //быстрые гусы
                            for (ushort j = 225; j < 257; j++)
                            {
                                ret.RemoveAt(225);
                            }

                            int i = ret.FindIndex(s => s == "ВНЕШ. 21");
                            for (int j = 0; j < 8; j++)
                            {
                                ret.RemoveAt(i);
                            }
                            
                            ret.Insert(i, "U> ИО");
                            ret.Insert(i + 1, "U> ИО Инв.");
                            ret.Insert(i + 2, "U>");
                            ret.Insert(i + 3, "U> Инв.");
                            ret.Insert(i + 4, "U>> ИО");
                            ret.Insert(i + 5, "U>> ИО Инв.");
                            ret.Insert(i + 6, "U>>");
                            ret.Insert(i + 7, "U>> Инв.");

                            i = ret.FindIndex(s => s == "УРОВ Пр21");
                            for (int j = 0; j < 8; j++)
                            {
                                ret.RemoveAt(i);
                            }

                            ret.Insert(i, "U< ИО");
                            ret.Insert(i + 1, "U< ИО Инв.");
                            ret.Insert(i + 2, "U<");
                            ret.Insert(i + 3, "U< Инв.");
                            ret.Insert(i + 4, "U<< ИО");
                            ret.Insert(i + 5, "U<< ИО Инв.");
                            ret.Insert(i + 6, "U<<");
                            ret.Insert(i + 7, "U<< Инв.");

                            i = ret.FindIndex(s => s == "Откл.Пр21");
                            for (ushort j = 0; j < 8; j++)
                            {
                                ret.RemoveAt(i);
                            }

                            ret.Insert(i, "Неиспр. ТН");
                            ret.Insert(i + 1, "Неиспр. ТН Инв.");
                            ret.Insert(i + 2, "Неиспр. 3U0");
                            ret.Insert(i + 3, "Неиспр. 3U0 Инв.");
                            ret.Insert(i + 4, "Неиспр. кНС");
                            ret.Insert(i + 5, "Неиспр. кНс Инв.");
                        }
                        break;
                    case "A6":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            ////ВЗ
                            //ushort key = 487;
                            //ret[key] = "U> ИО";
                            //ret[++key] = "U> ИО Инв.";
                            //ret[++key] = "U>";
                            //ret[++key] = "U> Инв.";
                            //ret[++key] = "U>> ИО";
                            //ret[++key] = "U>> ИО Инв.";
                            //ret[++key] = "U>>";
                            //ret[++key] = "U>> Инв.";
                            ////Прис. УРОВ
                            //key = 637;
                            //ret[key] = "U< ИО";
                            //ret[++key] = "U< ИО Инв.";
                            //ret[++key] = "U<";
                            //ret[++key] = "U< Инв.";
                            //ret[++key] = "U<< ИО";
                            //ret[++key] = "U<< ИО Инв.";
                            //ret[++key] = "U<<";
                            //ret[++key] = "U<< Инв.";
                            ////Прис.
                            //for (ushort i = 691; i < 699; i++)
                            //{
                            //    ret.RemoveAt(i);
                            //}

                            //key = 691;
                            //ret[key] = "Неиспр. ТН";
                            //ret[++key] = "Неиспр. ТН Инв.";
                            //ret[++key] = "Неиспр. 3U0";
                            //ret[++key] = "Неиспр. 3U0 Инв.";
                            //ret[++key] = "Неиспр. кНС";
                            //ret[++key] = "Неиспр. кНс Инв.";
                        }
                        break;
                    case "A7":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.RemoveAt(i);
                            }

                            ////ВЗ
                            //ushort key = 487;
                            //ret[key] = "U> ИО";
                            //ret[++key] = "U> ИО Инв.";
                            //ret[++key] = "U>";
                            //ret[++key] = "U> Инв.";
                            //ret[++key] = "U>> ИО";
                            //ret[++key] = "U>> ИО Инв.";
                            //ret[++key] = "U>>";
                            //ret[++key] = "U>> Инв.";
                            ////Прис. УРОВ
                            //key = 637;
                            //ret[key] = "U< ИО";
                            //ret[++key] = "U< ИО Инв.";
                            //ret[++key] = "U<";
                            //ret[++key] = "U< Инв.";
                            //ret[++key] = "U<< ИО";
                            //ret[++key] = "U<< ИО Инв.";
                            //ret[++key] = "U<<";
                            //ret[++key] = "U<< Инв.";
                            ////Прис.
                            //for (ushort i = 691; i < 699; i++)
                            //{
                            //    ret.RemoveAt(i);
                            //}

                            //key = 691;
                            //ret[key] = "Неиспр. ТН";
                            //ret[++key] = "Неиспр. ТН Инв.";
                            //ret[++key] = "Неиспр. 3U0";
                            //ret[++key] = "Неиспр. 3U0 Инв.";
                            //ret[++key] = "Неиспр. кНС";
                            //ret[++key] = "Неиспр. кНс Инв.";
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.RemoveAt(i);
                        }

                        ////присоединения УРОВ
                        //for (ushort i = 629; i < 645; i++)
                        //{
                        //    ret.RemoveAt(i);
                        //}

                        ////присоединения
                        //for (ushort i = 683; i < 699; i++)
                        //{
                        //    ret.RemoveAt(i);
                        //}

                        break;
                }

                return ret;
            }
        }

        public static List<string> ExtSignalsBig213
        {
            get
            {
                List<string> ret = new List<string>
                {
                    "Нет",
                    "Д1",
                    "Д1 Инв.",
                    "Д2",
                    "Д2 Инв.",
                    "Д3",
                    "Д3 Инв.",
                    "Д4",
                    "Д4 Инв.",
                    "Д5",
                    "Д5 Инв.",
                    "Д6",
                    "Д6 Инв.",
                    "Д7",
                    "Д7 Инв.",
                    "Д8",
                    "Д8 Инв.",
                    "Д9",
                    "Д9 Инв.",
                    "Д10",
                    "Д10 Инв.",
                    "Д11",
                    "Д11 Инв.",
                    "Д12",
                    "Д12 Инв.",
                    "Д13",
                    "Д13 Инв.",
                    "Д14",
                    "Д14 Инв.",
                    "Д15",
                    "Д15 Инв.",
                    "Д16",
                    "Д16 Инв.",
                    "Д17",
                    "Д17 Инв.",
                    "Д18",
                    "Д18 Инв.",
                    "Д19",
                    "Д19 Инв.",
                    "Д20",
                    "Д20 Инв.",
                    "Д21",
                    "Д21 Инв.",
                    "Д22",
                    "Д22 Инв.",
                    "Д23",
                    "Д23 Инв.",
                    "Д24",
                    "Д24 Инв.",
                    "Д25",
                    "Д25 Инв.",
                    "Д26",
                    "Д26 Инв.",
                    "Д27",
                    "Д27 Инв.",
                    "Д28",
                    "Д28 Инв.",
                    "Д29",
                    "Д29 Инв.",
                    "Д30",
                    "Д30 Инв.",
                    "Д31",
                    "Д31 Инв.",
                    "Д32",
                    "Д32 Инв.",
                    "Д33",
                    "Д33 Инв.",
                    "Д34",
                    "Д34 Инв.",
                    "Д35",
                    "Д35 Инв.",
                    "Д36",
                    "Д36 Инв.",
                    "Д37",
                    "Д37 Инв.",
                    "Д38",
                    "Д38 Инв.",
                    "Д39",
                    "Д39 Инв.",
                    "Д40",
                    "Д40 Инв.",
                    "Д41",
                    "Д41 Инв.",
                    "Д42",
                    "Д42 Инв.",
                    "Д43",
                    "Д43 Инв.",
                    "Д44",
                    "Д44 Инв.",
                    "Д45",
                    "Д45 Инв.",
                    "Д46",
                    "Д46 Инв.",
                    "Д47",
                    "Д47 Инв.",
                    "Д48",
                    "Д48 Инв.",
                    "Д49",
                    "Д49 Инв.",
                    "Д50",
                    "Д50 Инв.",
                    "Д51",
                    "Д51 Инв.",
                    "Д52",
                    "Д52 Инв.",
                    "Д53",
                    "Д53 Инв.",
                    "Д54",
                    "Д54 Инв.",
                    "Д55",
                    "Д55 Инв.",
                    "Д56",
                    "Д56 Инв.",
                    "Д57",
                    "Д57 Инв.",
                    "Д58",
                    "Д58 Инв.",
                    "Д59",
                    "Д59 Инв.",
                    "Д60",
                    "Д60 Инв.",
                    "Д61",
                    "Д61 Инв.",
                    "Д62",
                    "Д62 Инв.",
                    "Д63",
                    "Д63 Инв.",
                    "Д64",
                    "Д64 Инв.",
                    "Д65",
                    "Д65 Инв.",
                    "Д66",
                    "Д66 Инв.",
                    "Д67",
                    "Д67 Инв.",
                    "Д68",
                    "Д68 Инв.",
                    "Д69",
                    "Д69 Инв.",
                    "Д70",
                    "Д70 Инв.",
                    "Д71",
                    "Д71 Инв.",
                    "Д72",
                    "Д72 Инв.",
                    "Д73",
                    "Д73 Инв.",
                    "Д74",
                    "Д74 Инв.",
                    "Д75",
                    "Д75 Инв.",
                    "Д76",
                    "Д76 Инв.",
                    "Д77",
                    "Д77 Инв.",
                    "Д78",
                    "Д78 Инв.",
                    "Д79",
                    "Д79 Инв.",
                    "Д80",
                    "Д80 Инв.",
                    "Д81",
                    "Д81 Инв.",
                    "Д82",
                    "Д82 Инв.",
                    "Д83",
                    "Д83 Инв.",
                    "Д84",
                    "Д84 Инв.",
                    "Д85",
                    "Д85 Инв.",
                    "Д86",
                    "Д86 Инв.",
                    "Д87",
                    "Д87 Инв.",
                    "Д88",
                    "Д88 Инв.",
                    "Д89",
                    "Д89 Инв.",
                    "Д90",
                    "Д90 Инв.",
                    "Д91",
                    "Д91 Инв.",
                    "Д92",
                    "Д92 Инв.",
                    "Д93",
                    "Д93 Инв.",
                    "Д94",
                    "Д94 Инв.",
                    "Д95",
                    "Д95 Инв.",
                    "Д96",
                    "Д96 Инв.",
                    "ЛС1",
                    "ЛС1 Инв.",
                    "ЛС2",
                    "ЛС2 Инв.",
                    "ЛС3",
                    "ЛС3 Инв.",
                    "ЛС4",
                    "ЛС4 Инв.",
                    "ЛС5",
                    "ЛС5 Инв.",
                    "ЛС6",
                    "ЛС6 Инв.",
                    "ЛС7",
                    "ЛС7 Инв.",
                    "ЛС8",
                    "ЛС8 Инв.",
                    "ЛС9",
                    "ЛС9 Инв.",
                    "ЛС10",
                    "ЛС10 Инв.",
                    "ЛС11",
                    "ЛС11 Инв.",
                    "ЛС12",
                    "ЛС12 Инв.",
                    "ЛС13",
                    "ЛС13 Инв.",
                    "ЛС14",
                    "ЛС14 Инв.",
                    "ЛС15",
                    "ЛС15 Инв.",
                    "ЛС16",
                    "ЛС16 Инв.",
                    "БГ1",
                    "БГ1 Инв.",
                    "БГ2",
                    "БГ2 Инв.",
                    "БГ3",
                    "БГ3 Инв.",
                    "БГ4",
                    "БГ4 Инв.",
                    "БГ5",
                    "БГ5 Инв.",
                    "БГ6",
                    "БГ6 Инв.",
                    "БГ7",
                    "БГ7 Инв.",
                    "БГ8",
                    "БГ8 Инв.",
                    "БГ9",
                    "БГ9 Инв.",
                    "БГ10",
                    "БГ10 Инв.",
                    "БГ11",
                    "БГ11 Инв.",
                    "БГ12",
                    "БГ12 Инв.",
                    "БГ13",
                    "БГ13 Инв.",
                    "БГ14",
                    "БГ14 Инв.",
                    "БГ15",
                    "БГ15 Инв.",
                    "БГ16",
                    "БГ16 Инв.",
                    "ВЛС1",
                    "ВЛС1 Инв.",
                    "ВЛС2",
                    "ВЛС2 Инв.",
                    "ВЛС3",
                    "ВЛС3 Инв.",
                    "ВЛС4",
                    "ВЛС4 Инв.",
                    "ВЛС5",
                    "ВЛС5 Инв.",
                    "ВЛС6",
                    "ВЛС6 Инв.",
                    "ВЛС7",
                    "ВЛС7 Инв.",
                    "ВЛС8",
                    "ВЛС8 Инв.",
                    "ВЛС9",
                    "ВЛС9 Инв.",
                    "ВЛС10",
                    "ВЛС10 Инв.",
                    "ВЛС11",
                    "ВЛС11 Инв.",
                    "ВЛС12",
                    "ВЛС12 Инв.",
                    "ВЛС13",
                    "ВЛС13 Инв.",
                    "ВЛС14",
                    "ВЛС14 Инв.",
                    "ВЛС15",
                    "ВЛС15 Инв.",
                    "ВЛС16",
                    "ВЛС16 Инв.",
                    "Iд1м СШ1 *",
                    "Iд1м СШ1* Инв.",
                    "Iд1м СШ1",
                    "Iд1м СШ1 Инв.",
                    "Iд2м СШ2 *",
                    "Iд2м СШ2* Инв.",
                    "Iд2м СШ2",
                    "Iд2м СШ2 Инв.",
                    "Iд3м ПО *",
                    "Iд3м ПО* Инв.",
                    "Iд3м ПО",
                    "Iд3м ПО Инв.",
                    "Iд1 СШ1 ИО",
                    "Iд1 СШ1 ИО Инв.",
                    "Iд1 СШ1 *",
                    "Iд1 СШ1* Инв.",
                    "Iд1 СШ1",
                    "Iд1 СШ1 Инв.",
                    "Iд2 СШ2 ИО",
                    "Iд2 СШ2 ИО Инв.",
                    "Iд2 СШ2 *",
                    "Iд2 СШ2* Инв.",
                    "Iд2 СШ2",
                    "Iд2 СШ2 Инв.",
                    "Iд3 ПО ИО",
                    "Iд3 ПО ИО Инв.",
                    "Iд3 ПО *",
                    "Iд3 ПО* Инв.",
                    "Iд3 ПО",
                    "Iд3 ПО Инв.",

                    "I1> ИО",
                    "I1> ИО Инв.",
                    "I1>",
                    "I1> Инв.",
                    "I2> ИО",
                    "I2> ИО Инв.",
                    "I2>",
                    "I2> Инв.",
                    "I3> ИО",
                    "I3> ИО Инв.",
                    "I3>",
                    "I3> Инв.",
                    "I4> ИО",
                    "I4> ИО Инв.",
                    "I4>",
                    "I4> Инв.",
                    "I5> ИО",
                    "I5> ИО Инв.",
                    "I5>",
                    "I5> Инв.",
                    "I6> ИО",
                    "I6> ИО Инв.",
                    "I6>",
                    "I6> Инв.",
                    "I7> ИО",
                    "I7> ИО Инв.",
                    "I7>",
                    "I7> Инв.",
                    "I8> ИО",
                    "I8> ИО Инв.",
                    "I8>",
                    "I8> Инв.",
                    "I9> ИО",
                    "I9> ИО Инв.",
                    "I9>",
                    "I9> Инв.",
                    "I10> ИО",
                    "I10> ИО Инв.",
                    "I10>",
                    "I10> Инв.",
                    "I11> ИО",
                    "I11> ИО Инв.",
                    "I11>",
                    "I11> Инв.",
                    "I12> ИО",
                    "I12> ИО Инв.",
                    "I12>",
                    "I12> Инв.",
                    "I13> ИО",
                    "I13> ИО Инв.",
                    "I13>",
                    "I13> Инв.",
                    "I14> ИО",
                    "I14> ИО Инв.",
                    "I14>",
                    "I14> Инв.",
                    "I15> ИО",
                    "I15> ИО Инв.",
                    "I15>",
                    "I15> Инв.",
                    "I16> ИО",
                    "I16> ИО Инв.",
                    "I16>",
                    "I16> Инв.",
                    "I17> ИО",
                    "I17> ИО Инв.",
                    "I17>",
                    "I17> Инв.",
                    "I18> ИО",
                    "I18> ИО Инв.",
                    "I18>",
                    "I18> Инв.",
                    "I19> ИО",
                    "I19> ИО Инв.",
                    "I19>",
                    "I19> Инв.",
                    "I20> ИО",
                    "I20> ИО Инв.",
                    "I20>",
                    "I20> Инв.",
                    "I21> ИО",
                    "I21> ИО Инв.",
                    "I21>",
                    "I21> Инв.",
                    "I22> ИО",
                    "I22> ИО Инв.",
                    "I22>",
                    "I22> Инв.",
                    "I23> ИО",
                    "I23> ИО Инв.",
                    "I23>",
                    "I23> Инв.",
                    "I24> ИО",
                    "I24> ИО Инв.",
                    "I24>",
                    "I24> Инв.",
                    "I25> ИО",
                    "I25> ИО Инв.",
                    "I25>",
                    "I25> Инв.",
                    "I26> ИО",
                    "I26> ИО Инв.",
                    "I26>",
                    "I26> Инв.",
                    "I27> ИО",
                    "I27> ИО Инв.",
                    "I27>",
                    "I27> Инв.",
                    "I28> ИО",
                    "I28> ИО Инв.",
                    "I28>",
                    "I28> Инв.",
                    "I29> ИО",
                    "I29> ИО Инв.",
                    "I29>",
                    "I29> Инв.",
                    "I30> ИО",
                    "I30> ИО Инв.",
                    "I30>",
                    "I30> Инв.",
                    "I31> ИО",
                    "I31> ИО Инв.",
                    "I31>",
                    "I31> Инв.",
                    "I32> ИО",
                    "I32> ИО Инв.",
                    "I32>",
                    "I32> Инв.",
                    
                    "ССЛ1",
                    "ССЛ1 Инв.",
                    "ССЛ2",
                    "ССЛ2 Инв.",
                    "ССЛ3",
                    "ССЛ3 Инв.",
                    "ССЛ4",
                    "ССЛ4 Инв.",
                    "ССЛ5",
                    "ССЛ5 Инв.",
                    "ССЛ6",
                    "ССЛ6 Инв.",
                    "ССЛ7",
                    "ССЛ7 Инв.",
                    "ССЛ8",
                    "ССЛ8 Инв.",
                    "ССЛ9",
                    "ССЛ9 Инв.",
                    "ССЛ10",
                    "ССЛ10 Инв.",
                    "ССЛ11",
                    "ССЛ11 Инв.",
                    "ССЛ12",
                    "ССЛ12 Инв.",
                    "ССЛ13",
                    "ССЛ13 Инв.",
                    "ССЛ14",
                    "ССЛ14 Инв.",
                    "ССЛ15",
                    "ССЛ15 Инв.",
                    "ССЛ16",
                    "ССЛ16 Инв.",
                    "ССЛ17",
                    "ССЛ17 Инв.",
                    "ССЛ18",
                    "ССЛ18 Инв.",
                    "ССЛ19",
                    "ССЛ19 Инв.",
                    "ССЛ20",
                    "ССЛ20 Инв.",
                    "ССЛ21",
                    "ССЛ21 Инв.",
                    "ССЛ22",
                    "ССЛ22 Инв.",
                    "ССЛ23",
                    "ССЛ23 Инв.",
                    "ССЛ24",
                    "ССЛ24 Инв.",
                    "ССЛ25",
                    "ССЛ25 Инв.",
                    "ССЛ26",
                    "ССЛ26 Инв.",
                    "ССЛ27",
                    "ССЛ27 Инв.",
                    "ССЛ28",
                    "ССЛ28 Инв.",
                    "ССЛ29",
                    "ССЛ29 Инв.",
                    "ССЛ30",
                    "ССЛ30 Инв.",
                    "ССЛ31",
                    "ССЛ31 Инв.",
                    "ССЛ32",
                    "ССЛ32 Инв.",
                    "ССЛ33",
                    "ССЛ33 Инв.",
                    "ССЛ34",
                    "ССЛ34 Инв.",
                    "ССЛ35",
                    "ССЛ35 Инв.",
                    "ССЛ36",
                    "ССЛ36 Инв.",
                    "ССЛ37",
                    "ССЛ37 Инв.",
                    "ССЛ38",
                    "ССЛ38 Инв.",
                    "ССЛ39",
                    "ССЛ39 Инв.",
                    "ССЛ40",
                    "ССЛ40 Инв.",
                    "ССЛ41",
                    "ССЛ41 Инв.",
                    "ССЛ42",
                    "ССЛ42 Инв.",
                    "ССЛ43",
                    "ССЛ43 Инв.",
                    "ССЛ44",
                    "ССЛ44 Инв.",
                    "ССЛ45",
                    "ССЛ45 Инв.",
                    "ССЛ46",
                    "ССЛ46 Инв.",
                    "ССЛ47",
                    "ССЛ47 Инв.",
                    "ССЛ48",
                    "ССЛ48 Инв.",

                    "УРОВ СШ1",
                    "УРОВ СШ1 Инв.",
                    "УРОВ СШ2",
                    "УРОВ СШ2 Инв.",
                    "УРОВ ПО",
                    "УРОВ ПО Инв.",

                    "УРОВ Пр1",
                    "УРОВ Пр1 Инв.",
                    "УРОВ Пр2",
                    "УРОВ Пр2 Инв.",
                    "УРОВ Пр3",
                    "УРОВ Пр3 Инв.",
                    "УРОВ Пр4",
                    "УРОВ Пр4 Инв.",
                    "УРОВ Пр5",
                    "УРОВ Пр5 Инв.",
                    "УРОВ Пр6",
                    "УРОВ Пр6 Инв.",
                    "УРОВ Пр7",
                    "УРОВ Пр7 Инв.",
                    "УРОВ Пр8",
                    "УРОВ Пр8 Инв.",
                    "УРОВ Пр9",
                    "УРОВ Пр9 Инв.",
                    "УРОВ Пр10",
                    "УРОВ Пр10 Инв.",
                    "УРОВ Пр11",
                    "УРОВ Пр11 Инв.",
                    "УРОВ Пр12",
                    "УРОВ Пр12 Инв.",
                    "УРОВ Пр13",
                    "УРОВ Пр13 Инв.",
                    "УРОВ Пр14",
                    "УРОВ Пр14 Инв.",
                    "УРОВ Пр15",
                    "УРОВ Пр15 Инв.",
                    "УРОВ Пр16",
                    "УРОВ Пр16 Инв.",
                    "УРОВ Пр17",
                    "УРОВ Пр17 Инв.",
                    "УРОВ Пр18",
                    "УРОВ Пр18 Инв.",
                    "УРОВ Пр19",
                    "УРОВ Пр19 Инв.",
                    "УРОВ Пр20",
                    "УРОВ Пр20 Инв.",
                    "УРОВ Пр21",
                    "УРОВ Пр21 Инв.",
                    "УРОВ Пр22",
                    "УРОВ Пр22 Инв.",
                    "УРОВ Пр23",
                    "УРОВ Пр23 Инв.",
                    "УРОВ Пр24",
                    "УРОВ Пр24 Инв.",
                    "УРОВ 1",


                };
                //switch (ConfigurationStructBigV213.DeviceModelType)
                //{
                //    case "A1":
                //        {
                //            //дискреты
                //            for (ushort i = 129; i < 193; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            //быстрые гусы
                //            for (ushort i = 225; i < 258; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //        }
                //        break;
                //    case "A2":
                //        {
                //            //дискреты
                //            for (ushort i = 81; i < 193; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            //быстрые гусы
                //            for (ushort i = 225; i < 257; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }
                //        }
                //        break;
                //    case "A3":
                //        {
                //            //дискреты
                //            for (ushort i = 49; i < 193; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            //быстрые гусы
                //            for (ushort i = 225; i < 257; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }
                //        }
                //        break;
                //    case "A4":
                //        {
                //            //дискреты
                //            for (ushort i = 65; i < 193; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            //быстрые гусы
                //            for (ushort i = 225; i < 257; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }
                //        }
                //        break;
                //    case "A5":
                //        {
                //            //дискреты
                //            for (ushort j = 81; j < 193; j++)
                //            {
                //                ret.RemoveAt(81);
                //            }

                //            //быстрые гусы
                //            for (ushort j = 225; j < 257; j++)
                //            {
                //                ret.RemoveAt(225);
                //            }

                //            int i = ret.FindIndex(s => s == "ВНЕШ. 21");
                //            for (int j = 0; j < 8; j++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            ret.Insert(i, "U> ИО");
                //            ret.Insert(i + 1, "U> ИО Инв.");
                //            ret.Insert(i + 2, "U>");
                //            ret.Insert(i + 3, "U> Инв.");
                //            ret.Insert(i + 4, "U>> ИО");
                //            ret.Insert(i + 5, "U>> ИО Инв.");
                //            ret.Insert(i + 6, "U>>");
                //            ret.Insert(i + 7, "U>> Инв.");

                //            i = ret.FindIndex(s => s == "УРОВ Пр21");
                //            for (int j = 0; j < 8; j++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            ret.Insert(i, "U< ИО");
                //            ret.Insert(i + 1, "U< ИО Инв.");
                //            ret.Insert(i + 2, "U<");
                //            ret.Insert(i + 3, "U< Инв.");
                //            ret.Insert(i + 4, "U<< ИО");
                //            ret.Insert(i + 5, "U<< ИО Инв.");
                //            ret.Insert(i + 6, "U<<");
                //            ret.Insert(i + 7, "U<< Инв.");

                //            i = ret.FindIndex(s => s == "Откл.Пр21");
                //            for (ushort j = 0; j < 8; j++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            ret.Insert(i, "Неиспр. ТН");
                //            ret.Insert(i + 1, "Неиспр. ТН Инв.");
                //            ret.Insert(i + 2, "Неиспр. 3U0");
                //            ret.Insert(i + 3, "Неиспр. 3U0 Инв.");
                //            ret.Insert(i + 4, "Неиспр. кНС");
                //            ret.Insert(i + 5, "Неиспр. кНс Инв.");
                //        }
                //        break;
                //    case "A6":
                //        {
                //            //дискреты
                //            for (ushort i = 49; i < 193; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            //быстрые гусы
                //            for (ushort i = 225; i < 257; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            ////ВЗ
                //            //ushort key = 487;
                //            //ret[key] = "U> ИО";
                //            //ret[++key] = "U> ИО Инв.";
                //            //ret[++key] = "U>";
                //            //ret[++key] = "U> Инв.";
                //            //ret[++key] = "U>> ИО";
                //            //ret[++key] = "U>> ИО Инв.";
                //            //ret[++key] = "U>>";
                //            //ret[++key] = "U>> Инв.";
                //            ////Прис. УРОВ
                //            //key = 637;
                //            //ret[key] = "U< ИО";
                //            //ret[++key] = "U< ИО Инв.";
                //            //ret[++key] = "U<";
                //            //ret[++key] = "U< Инв.";
                //            //ret[++key] = "U<< ИО";
                //            //ret[++key] = "U<< ИО Инв.";
                //            //ret[++key] = "U<<";
                //            //ret[++key] = "U<< Инв.";
                //            ////Прис.
                //            //for (ushort i = 691; i < 699; i++)
                //            //{
                //            //    ret.RemoveAt(i);
                //            //}

                //            //key = 691;
                //            //ret[key] = "Неиспр. ТН";
                //            //ret[++key] = "Неиспр. ТН Инв.";
                //            //ret[++key] = "Неиспр. 3U0";
                //            //ret[++key] = "Неиспр. 3U0 Инв.";
                //            //ret[++key] = "Неиспр. кНС";
                //            //ret[++key] = "Неиспр. кНс Инв.";
                //        }
                //        break;
                //    case "A7":
                //        {
                //            //дискреты
                //            for (ushort i = 65; i < 193; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            //быстрые гусы
                //            for (ushort i = 225; i < 257; i++)
                //            {
                //                ret.RemoveAt(i);
                //            }

                //            ////ВЗ
                //            //ushort key = 487;
                //            //ret[key] = "U> ИО";
                //            //ret[++key] = "U> ИО Инв.";
                //            //ret[++key] = "U>";
                //            //ret[++key] = "U> Инв.";
                //            //ret[++key] = "U>> ИО";
                //            //ret[++key] = "U>> ИО Инв.";
                //            //ret[++key] = "U>>";
                //            //ret[++key] = "U>> Инв.";
                //            ////Прис. УРОВ
                //            //key = 637;
                //            //ret[key] = "U< ИО";
                //            //ret[++key] = "U< ИО Инв.";
                //            //ret[++key] = "U<";
                //            //ret[++key] = "U< Инв.";
                //            //ret[++key] = "U<< ИО";
                //            //ret[++key] = "U<< ИО Инв.";
                //            //ret[++key] = "U<<";
                //            //ret[++key] = "U<< Инв.";
                //            ////Прис.
                //            //for (ushort i = 691; i < 699; i++)
                //            //{
                //            //    ret.RemoveAt(i);
                //            //}

                //            //key = 691;
                //            //ret[key] = "Неиспр. ТН";
                //            //ret[++key] = "Неиспр. ТН Инв.";
                //            //ret[++key] = "Неиспр. 3U0";
                //            //ret[++key] = "Неиспр. 3U0 Инв.";
                //            //ret[++key] = "Неиспр. кНС";
                //            //ret[++key] = "Неиспр. кНс Инв.";
                //        }
                //        break;
                //    default:
                //        //дискреты
                //        for (ushort i = 49; i < 193; i++)
                //        {
                //            ret.RemoveAt(i);
                //        }

                //        ////присоединения УРОВ
                //        //for (ushort i = 629; i < 645; i++)
                //        //{
                //        //    ret.RemoveAt(i);
                //        //}

                //        ////присоединения
                //        //for (ushort i = 683; i < 699; i++)
                //        //{
                //        //    ret.RemoveAt(i);
                //        //}

                //        break;
                //}

                return ret;
            }
        }

        public static Dictionary<ushort, string> InputSignalsBig
        {
            get
            {
                Dictionary<ushort, string> ret = new Dictionary<ushort, string>
                {
                    {0, "Нет" },
                    {1, "Д1"},
                    {2, "Д1 Инв."},
                    {3, "Д2"},
                    {4, "Д2 Инв."},
                    {5, "Д3"},
                    {6, "Д3 Инв."},
                    {7, "Д4"},
                    {8, "Д4 Инв."},
                    {9, "Д5"},
                    {10, "Д5 Инв."},
                    {11, "Д6"},
                    {12, "Д6 Инв."},
                    {13, "Д7"},
                    {14, "Д7 Инв."},
                    {15, "Д8"},
                    {16, "Д8 Инв."},
                    {17, "Д9"},
                    {18, "Д9 Инв."},
                    {19, "Д10"},
                    {20, "Д10 Инв."},
                    {21, "Д11"},
                    {22, "Д11 Инв."},
                    {23, "Д12"},
                    {24, "Д12 Инв."},
                    {25, "Д13"},
                    {26, "Д13 Инв."},
                    {27, "Д14"},
                    {28, "Д14 Инв."},
                    {29, "Д15"},
                    {30, "Д15 Инв."},
                    {31, "Д16"},
                    {32, "Д16 Инв."},
                    {33, "Д17"},
                    {34, "Д17 Инв."},
                    {35, "Д18"},
                    {36, "Д18 Инв."},
                    {37, "Д19"},
                    {38, "Д19 Инв."},
                    {39, "Д20"},
                    {40, "Д20 Инв."},
                    {41, "Д21"},
                    {42, "Д21 Инв."},
                    {43, "Д22"},
                    {44, "Д22 Инв."},
                    {45, "Д23"},
                    {46, "Д23 Инв."},
                    {47, "Д24"},
                    {48, "Д24 Инв."},
                    {49, "Д25"},
                    {50, "Д25 Инв."},
                    {51, "Д26"},
                    {52, "Д26 Инв."},
                    {53, "Д27"},
                    {54, "Д27 Инв."},
                    {55, "Д28"},
                    {56, "Д28 Инв."},
                    {57, "Д29"},
                    {58, "Д29 Инв."},
                    {59, "Д30"},
                    {60, "Д30 Инв."},
                    {61, "Д31"},
                    {62, "Д31 Инв."},
                    {63, "Д32"},
                    {64, "Д32 Инв."},
                    {65, "Д33"},
                    {66, "Д33 Инв."},
                    {67, "Д34"},
                    {68, "Д34 Инв."},
                    {69, "Д35"},
                    {70, "Д35 Инв."},
                    {71, "Д36"},
                    {72, "Д36 Инв."},
                    {73, "Д37"},
                    {74, "Д37 Инв."},
                    {75, "Д38"},
                    {76, "Д38 Инв."},
                    {77, "Д39"},
                    {78, "Д39 Инв."},
                    {79, "Д40"},
                    {80, "Д40 Инв."},
                    {81, "Д41"},
                    {82, "Д41 Инв."},
                    {83, "Д42"},
                    {84, "Д42 Инв."},
                    {85, "Д43"},
                    {86, "Д43 Инв."},
                    {87, "Д44"},
                    {88, "Д44 Инв."},
                    {89, "Д45"},
                    {90, "Д45 Инв."},
                    {91, "Д46"},
                    {92, "Д46 Инв."},
                    {93, "Д47"},
                    {94, "Д47 Инв."},
                    {95, "Д48"},
                    {96, "Д48 Инв."},
                    {97, "Д49"},
                    {98, "Д49 Инв."},
                    {99, "Д50"},
                    {100, "Д50 Инв."},
                    {101, "Д51"},
                    {102, "Д51 Инв."},
                    {103, "Д52"},
                    {104, "Д52 Инв."},
                    {105, "Д53"},
                    {106, "Д53 Инв."},
                    {107, "Д54"},
                    {108, "Д54 Инв."},
                    {109, "Д55"},
                    {110, "Д55 Инв."},
                    {111, "Д56"},
                    {112, "Д56 Инв."},
                    {113, "Д57"},
                    {114, "Д57 Инв."},
                    {115, "Д58"},
                    {116, "Д58 Инв."},
                    {117, "Д59"},
                    {118, "Д59 Инв."},
                    {119, "Д60"},
                    {120, "Д60 Инв."},
                    {121, "Д61"},
                    {122, "Д61 Инв."},
                    {123, "Д62"},
                    {124, "Д62 Инв."},
                    {125, "Д63"},
                    {126, "Д63 Инв."},
                    {127, "Д64"},
                    {128, "Д64 Инв."},
                    {129, "Д65"},
                    {130, "Д65 Инв."},
                    {131, "Д66"},
                    {132, "Д66 Инв."},
                    {133, "Д67"},
                    {134, "Д67 Инв."},
                    {135, "Д68"},
                    {136, "Д68 Инв."},
                    {137, "Д69"},
                    {138, "Д69 Инв."},
                    {139, "Д70"},
                    {140, "Д70 Инв."},
                    {141, "Д71"},
                    {142, "Д71 Инв."},
                    {143, "Д72"},
                    {144, "Д72 Инв."},
                    {145, "Д73"},
                    {146, "Д73 Инв."},
                    {147, "Д74"},
                    {148, "Д74 Инв."},
                    {149, "Д75"},
                    {150, "Д75 Инв."},
                    {151, "Д76"},
                    {152, "Д76 Инв."},
                    {153, "Д77"},
                    {154, "Д77 Инв."},
                    {155, "Д78"},
                    {156, "Д78 Инв."},
                    {157, "Д79"},
                    {158, "Д79 Инв."},
                    {159, "Д80"},
                    {160, "Д80 Инв."},
                    {161, "Д81"},
                    {162, "Д81 Инв."},
                    {163, "Д82"},
                    {164, "Д82 Инв."},
                    {165, "Д83"},
                    {166, "Д83 Инв."},
                    {167, "Д84"},
                    {168, "Д84 Инв."},
                    {169, "Д85"},
                    {170, "Д85 Инв."},
                    {171, "Д86"},
                    {172, "Д86 Инв."},
                    {173, "Д87"},
                    {174, "Д87 Инв."},
                    {175, "Д88"},
                    {176, "Д88 Инв."},
                    {177, "Д89"},
                    {178, "Д89 Инв."},
                    {179, "Д90"},
                    {180, "Д90 Инв."},
                    {181, "Д91"},
                    {182, "Д91 Инв."},
                    {183, "Д92"},
                    {184, "Д92 Инв."},
                    {185, "Д93"},
                    {186, "Д93 Инв."},
                    {187, "Д94"},
                    {188, "Д94 Инв."},
                    {189, "Д95"},
                    {190, "Д95 Инв."},
                    {191, "Д96"},
                    {192, "Д96 Инв."},
                    {193, "ЛС1"},
                    {194, "ЛС1 Инв."},
                    {195, "ЛС2"},
                    {196, "ЛС2 Инв."},
                    {197, "ЛС3"},
                    {198, "ЛС3 Инв."},
                    {199, "ЛС4"},
                    {200, "ЛС4 Инв."},
                    {201, "ЛС5"},
                    {202, "ЛС5 Инв."},
                    {203, "ЛС6"},
                    {204, "ЛС6 Инв."},
                    {205, "ЛС7"},
                    {206, "ЛС7 Инв."},
                    {207, "ЛС8"},
                    {208, "ЛС8 Инв."},
                    {209, "ЛС9"},
                    {210, "ЛС9 Инв."},
                    {211, "ЛС10"},
                    {212, "ЛС10 Инв."},
                    {213, "ЛС11"},
                    {214, "ЛС11 Инв."},
                    {215, "ЛС12"},
                    {216, "ЛС12 Инв."},
                    {217, "ЛС13"},
                    {218, "ЛС13 Инв."},
                    {219, "ЛС14"},
                    {220, "ЛС14 Инв."},
                    {221, "ЛС15"},
                    {222, "ЛС15 Инв."},
                    {223, "ЛС16"},
                    {224, "ЛС16 Инв."},
                    {225, "БГ1"},
                    {226, "БГ1 Инв."},
                    {227, "БГ2"},
                    {228, "БГ2 Инв."},
                    {229, "БГ3"},
                    {230, "БГ3 Инв."},
                    {231, "БГ4"},
                    {232, "БГ4 Инв."},
                    {233, "БГ5"},
                    {234, "БГ5 Инв."},
                    {235, "БГ6"},
                    {236, "БГ6 Инв."},
                    {237, "БГ7"},
                    {238, "БГ7 Инв."},
                    {239, "БГ8"},
                    {240, "БГ8 Инв."},
                    {241, "БГ9"},
                    {242, "БГ9 Инв."},
                    {243, "БГ10"},
                    {244, "БГ10 Инв."},
                    {245, "БГ11"},
                    {246, "БГ11 Инв."},
                    {247, "БГ12"},
                    {248, "БГ12 Инв."},
                    {249, "БГ13"},
                    {250, "БГ13 Инв."},
                    {251, "БГ14"},
                    {252, "БГ14 Инв."},
                    {253, "БГ15"},
                    {254, "БГ15 Инв."},
                    {255, "БГ16"},
                    {256, "БГ16 Инв."},
                    {257, "ВЛС1"},
                    {258, "ВЛС1 Инв."},
                    {259, "ВЛС2"},
                    {260, "ВЛС2 Инв."},
                    {261, "ВЛС3"},
                    {262, "ВЛС3 Инв."},
                    {263, "ВЛС4"},
                    {264, "ВЛС4 Инв."},
                    {265, "ВЛС5"},
                    {266, "ВЛС5 Инв."},
                    {267, "ВЛС6"},
                    {268, "ВЛС6 Инв."},
                    {269, "ВЛС7"},
                    {270, "ВЛС7 Инв."},
                    {271, "ВЛС8"},
                    {272, "ВЛС8 Инв."},
                    {273, "ВЛС9"},
                    {274, "ВЛС9 Инв."},
                    {275, "ВЛС10"},
                    {276, "ВЛС10 Инв."},
                    {277, "ВЛС11"},
                    {278, "ВЛС11 Инв."},
                    {279, "ВЛС12"},
                    {280, "ВЛС12 Инв."},
                    {281, "ВЛС13"},
                    {282, "ВЛС13 Инв."},
                    {283, "ВЛС14"},
                    {284, "ВЛС14 Инв."},
                    {285, "ВЛС15"},
                    {286, "ВЛС15 Инв."},
                    {287, "ВЛС16"},
                    {288, "ВЛС16 Инв."}
                };
                switch (ConfigurationStructBigV210.DeviceModelType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (ushort i = 129; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A2":
                    case "A5":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A3":
                    case "A6":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A4":
                    case "A7":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }
                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }
                
                return ret;
            }
        }

        public static Dictionary<ushort, string> ExtDefSignalsBig213
        {
            get
            {
                Dictionary<ushort, string> ret = new Dictionary<ushort, string>
                {
                   {0, "Нет"},
                    {1, "Д1"},
                    {2, "Д1 Инв."},
                    {3, "Д2"},
                    {4, "Д2 Инв."},
                    {5, "Д3"},
                    {6, "Д3 Инв."},
                    {7, "Д4"},
                    {8, "Д4 Инв."},
                    {9, "Д5"},
                    {10, "Д5 Инв."},
                    {11, "Д6"},
                    {12, "Д6 Инв."},
                    {13, "Д7"},
                    {14, "Д7 Инв."},
                    {15, "Д8"},
                    {16, "Д8 Инв."},
                    {17, "Д9"},
                    {18, "Д9 Инв."},
                    {19, "Д10"},
                    {20, "Д10 Инв."},
                    {21, "Д11"},
                    {22, "Д11 Инв."},
                    {23, "Д12"},
                    {24, "Д12 Инв."},
                    {25, "Д13"},
                    {26, "Д13 Инв."},
                    {27, "Д14"},
                    {28, "Д14 Инв."},
                    {29, "Д15"},
                    {30, "Д15 Инв."},
                    {31, "Д16"},
                    {32, "Д16 Инв."},
                    {33, "Д17"},
                    {34, "Д17 Инв."},
                    {35, "Д18"},
                    {36, "Д18 Инв."},
                    {37, "Д19"},
                    {38, "Д19 Инв."},
                    {39, "Д20"},
                    {40, "Д20 Инв."},
                    {41, "Д21"},
                    {42, "Д21 Инв."},
                    {43, "Д22"},
                    {44, "Д22 Инв."},
                    {45, "Д23"},
                    {46, "Д23 Инв."},
                    {47, "Д24"},
                    {48, "Д24 Инв."},
                    {49, "Д25"},
                    {50, "Д25 Инв."},
                    {51, "Д26"},
                    {52, "Д26 Инв."},
                    {53, "Д27"},
                    {54, "Д27 Инв."},
                    {55, "Д28"},
                    {56, "Д28 Инв."},
                    {57, "Д29"},
                    {58, "Д29 Инв."},
                    {59, "Д30"},
                    {60, "Д30 Инв."},
                    {61, "Д31"},
                    {62, "Д31 Инв."},
                    {63, "Д32"},
                    {64, "Д32 Инв."},
                    {65, "Д33"},
                    {66, "Д33 Инв."},
                    {67, "Д34"},
                    {68, "Д34 Инв."},
                    {69, "Д35"},
                    {70, "Д35 Инв."},
                    {71, "Д36"},
                    {72, "Д36 Инв."},
                    {73, "Д37"},
                    {74, "Д37 Инв."},
                    {75, "Д38"},
                    {76, "Д38 Инв."},
                    {77, "Д39"},
                    {78, "Д39 Инв."},
                    {79, "Д40"},
                    {80, "Д40 Инв."},
                    {81, "Д41"},
                    {82, "Д41 Инв."},
                    {83, "Д42"},
                    {84, "Д42 Инв."},
                    {85, "Д43"},
                    {86, "Д43 Инв."},
                    {87, "Д44"},
                    {88, "Д44 Инв."},
                    {89, "Д45"},
                    {90, "Д45 Инв."},
                    {91, "Д46"},
                    {92, "Д46 Инв."},
                    {93, "Д47"},
                    {94, "Д47 Инв."},
                    {95, "Д48"},
                    {96, "Д48 Инв."},
                    {97, "Д49"},
                    {98, "Д49 Инв."},
                    {99, "Д50"},
                    {100, "Д50 Инв."},
                    {101, "Д51"},
                    {102, "Д51 Инв."},
                    {103, "Д52"},
                    {104, "Д52 Инв."},
                    {105, "Д53"},
                    {106, "Д53 Инв."},
                    {107, "Д54"},
                    {108, "Д54 Инв."},
                    {109, "Д55"},
                    {110, "Д55 Инв."},
                    {111, "Д56"},
                    {112, "Д56 Инв."},
                    {113, "Д57"},
                    {114, "Д57 Инв."},
                    {115, "Д58"},
                    {116, "Д58 Инв."},
                    {117, "Д59"},
                    {118, "Д59 Инв."},
                    {119, "Д60"},
                    {120, "Д60 Инв."},
                    {121, "Д61"},
                    {122, "Д61 Инв."},
                    {123, "Д62"},
                    {124, "Д62 Инв."},
                    {125, "Д63"},
                    {126, "Д63 Инв."},
                    {127, "Д64"},
                    {128, "Д64 Инв."},
                    {129, "Д65"},
                    {130, "Д65 Инв."},
                    {131, "Д66"},
                    {132, "Д66 Инв."},
                    {133, "Д67"},
                    {134, "Д67 Инв."},
                    {135, "Д68"},
                    {136, "Д68 Инв."},
                    {137, "Д69"},
                    {138, "Д69 Инв."},
                    {139, "Д70"},
                    {140, "Д70 Инв."},
                    {141, "Д71"},
                    {142, "Д71 Инв."},
                    {143, "Д72"},
                    {144, "Д72 Инв."},
                    {145, "Д73"},
                    {146, "Д73 Инв."},
                    {147, "Д74"},
                    {148, "Д74 Инв."},
                    {149, "Д75"},
                    {150, "Д75 Инв."},
                    {151, "Д76"},
                    {152, "Д76 Инв."},
                    {153, "Д77"},
                    {154, "Д77 Инв."},
                    {155, "Д78"},
                    {156, "Д78 Инв."},
                    {157, "Д79"},
                    {158, "Д79 Инв."},
                    {159, "Д80"},
                    {160, "Д80 Инв."},
                    {161, "Д81"},
                    {162, "Д81 Инв."},
                    {163, "Д82"},
                    {164, "Д82 Инв."},
                    {165, "Д83"},
                    {166, "Д83 Инв."},
                    {167, "Д84"},
                    {168, "Д84 Инв."},
                    {169, "Д85"},
                    {170, "Д85 Инв."},
                    {171, "Д86"},
                    {172, "Д86 Инв."},
                    {173, "Д87"},
                    {174, "Д87 Инв."},
                    {175, "Д88"},
                    {176, "Д88 Инв."},
                    {177, "Д89"},
                    {178, "Д89 Инв."},
                    {179, "Д90"},
                    {180, "Д90 Инв."},
                    {181, "Д91"},
                    {182, "Д91 Инв."},
                    {183, "Д92"},
                    {184, "Д92 Инв."},
                    {185, "Д93"},
                    {186, "Д93 Инв."},
                    {187, "Д94"},
                    {188, "Д94 Инв."},
                    {189, "Д95"},
                    {190, "Д95 Инв."},
                    {191, "Д96"},
                    {192, "Д96 Инв."},
                    {193, "ЛС1"},
                    {194, "ЛС1 Инв."},
                    {195, "ЛС2"},
                    {196, "ЛС2 Инв."},
                    {197, "ЛС3"},
                    {198, "ЛС3 Инв."},
                    {199, "ЛС4"},
                    {200, "ЛС4 Инв."},
                    {201, "ЛС5"},
                    {202, "ЛС5 Инв."},
                    {203, "ЛС6"},
                    {204, "ЛС6 Инв."},
                    {205, "ЛС7"},
                    {206, "ЛС7 Инв."},
                    {207, "ЛС8"},
                    {208, "ЛС8 Инв."},
                    {209, "ЛС9"},
                    {210, "ЛС9 Инв."},
                    {211, "ЛС10"},
                    {212, "ЛС10 Инв."},
                    {213, "ЛС11"},
                    {214, "ЛС11 Инв."},
                    {215, "ЛС12"},
                    {216, "ЛС12 Инв."},
                    {217, "ЛС13"},
                    {218, "ЛС13 Инв."},
                    {219, "ЛС14"},
                    {220, "ЛС14 Инв."},
                    {221, "ЛС15"},
                    {222, "ЛС15 Инв."},
                    {223, "ЛС16"},
                    {224, "ЛС16 Инв."},
                    {225, "БГ1"},
                    {226, "БГ1 Инв."},
                    {227, "БГ2"},
                    {228, "БГ2 Инв."},
                    {229, "БГ3"},
                    {230, "БГ3 Инв."},
                    {231, "БГ4"},
                    {232, "БГ4 Инв."},
                    {233, "БГ5"},
                    {234, "БГ5 Инв."},
                    {235, "БГ6"},
                    {236, "БГ6 Инв."},
                    {237, "БГ7"},
                    {238, "БГ7 Инв."},
                    {239, "БГ8"},
                    {240, "БГ8 Инв."},
                    {241, "БГ9"},
                    {242, "БГ9 Инв."},
                    {243, "БГ10"},
                    {244, "БГ10 Инв."},
                    {245, "БГ11"},
                    {246, "БГ11 Инв."},
                    {247, "БГ12"},
                    {248, "БГ12 Инв."},
                    {249, "БГ13"},
                    {250, "БГ13 Инв."},
                    {251, "БГ14"},
                    {252, "БГ14 Инв."},
                    {253, "БГ15"},
                    {254, "БГ15 Инв."},
                    {255, "БГ16"},
                    {256, "БГ16 Инв."},
                    {257, "ВЛС1"},
                    {258, "ВЛС1 Инв."},
                    {259, "ВЛС2"},
                    {260, "ВЛС2 Инв."},
                    {261, "ВЛС3"},
                    {262, "ВЛС3 Инв."},
                    {263, "ВЛС4"},
                    {264, "ВЛС4 Инв."},
                    {265, "ВЛС5"},
                    {266, "ВЛС5 Инв."},
                    {267, "ВЛС6"},
                    {268, "ВЛС6 Инв."},
                    {269, "ВЛС7"},
                    {270, "ВЛС7 Инв."},
                    {271, "ВЛС8"},
                    {272, "ВЛС8 Инв."},
                    {273, "ВЛС9"},
                    {274, "ВЛС9 Инв."},
                    {275, "ВЛС10"},
                    {276, "ВЛС10 Инв."},
                    {277, "ВЛС11"},
                    {278, "ВЛС11 Инв."},
                    {279, "ВЛС12"},
                    {280, "ВЛС12 Инв."},
                    {281, "ВЛС13"},
                    {282, "ВЛС13 Инв."},
                    {283, "ВЛС14"},
                    {284, "ВЛС14 Инв."},
                    {285, "ВЛС15"},
                    {286, "ВЛС15 Инв."},
                    {287, "ВЛС16"},
                    {288, "ВЛС16 Инв."},
                    {289, "Iд1м СШ1 *"},
                    {290, "Iд1м СШ1* Инв."},
                    {291, "Iд1м СШ1"},
                    {292, "Iд1м СШ1 Инв."},
                    {293, "Iд2м СШ2 *"},
                    {294, "Iд2м СШ2* Инв."},
                    {295, "Iд2м СШ2"},
                    {296, "Iд2м СШ2 Инв."},
                    {297, "Iд3м ПО *"},
                    {298, "Iд3м ПО* Инв."},
                    {299, "Iд3м ПО"},
                    {300, "Iд3м ПО Инв."},
                    {301, "Iд1 СШ1 ИО"},
                    {302, "Iд1 СШ1 ИО Инв."},
                    {303, "Iд1 СШ1 *"},
                    {304, "Iд1 СШ1* Инв."},
                    {305, "Iд1 СШ1"},
                    {306, "Iд1 СШ1 Инв."},
                    {307, "Iд2 СШ2 ИО"},
                    {308, "Iд2 СШ2 ИО Инв."},
                    {309, "Iд2 СШ2 *"},
                    {310, "Iд2 СШ2* Инв."},
                    {311, "Iд2 СШ2"},
                    {312, "Iд2 СШ2 Инв."},
                    {313, "Iд3 ПО ИО"},
                    {314, "Iд3 ПО ИО Инв."},
                    {315, "Iд3 ПО *"},
                    {316, "Iд3 ПО* Инв."},
                    {317, "Iд3 ПО"},
                    {318, "Iд3 ПО Инв."},

                    {319, "I1> ИО"},
                    {320, "I1> ИО Инв."},
                    {321, "I1>"},
                    {322, "I1> Инв."},
                    {323, "I2> ИО"},
                    {324, "I2> ИО Инв."},
                    {325, "I2>"},
                    {326, "I2> Инв."},
                    {327, "I3> ИО"},
                    {328, "I3> ИО Инв."},
                    {329, "I3>"},
                    {330, "I3> Инв."},
                    {331, "I4> ИО"},
                    {332, "I4> ИО Инв."},
                    {333, "I4>"},
                    {334, "I4> Инв."},
                    {335, "I5> ИО"},
                    {336, "I5> ИО Инв."},
                    {337, "I5>"},
                    {338, "I5> Инв."},
                    {339, "I6> ИО"},
                    {340, "I6> ИО Инв."},
                    {341, "I6>"},
                    {342, "I6> Инв."},
                    {343, "I7> ИО"},
                    {344, "I7> ИО Инв."},
                    {345, "I7>"},
                    {346, "I7> Инв."},
                    {347, "I8> ИО"},
                    {348, "I8> ИО Инв."},
                    {349, "I8>"},
                    {350, "I8> Инв."},
                    {351, "I9> ИО"},
                    {352, "I9> ИО Инв."},
                    {353, "I9>"},
                    {354, "I9> Инв."},
                    {355, "I10> ИО"},
                    {356, "I10> ИО Инв."},
                    {357, "I10>"},
                    {358, "I10> Инв."},
                    {359, "I11> ИО"},
                    {360, "I11> ИО Инв."},
                    {361, "I11>"},
                    {362, "I11> Инв."},
                    {363, "I12> ИО"},
                    {364, "I12> ИО Инв."},
                    {365, "I12>"},
                    {366, "I12> Инв."},
                    {367, "I13> ИО"},
                    {368, "I13> ИО Инв."},
                    {369, "I13>"},
                    {370, "I13> Инв."},
                    {371, "I14> ИО"},
                    {372, "I14> ИО Инв."},
                    {373, "I14>"},
                    {374, "I14> Инв."},
                    {375, "I15> ИО"},
                    {376, "I15> ИО Инв."},
                    {377, "I15>"},
                    {378, "I15> Инв."},
                    {379, "I16> ИО"},
                    {380, "I16> ИО Инв."},
                    {381, "I16>"},
                    {382, "I16> Инв."},
                    {383, "I17> ИО"},
                    {384, "I17> ИО Инв."},
                    {385, "I17>"},
                    {386, "I17> Инв."},
                    {387, "I18> ИО"},
                    {388, "I18> ИО Инв."},
                    {389, "I18>"},
                    {390, "I18> Инв."},
                    {391, "I19> ИО"},
                    {392, "I19> ИО Инв."},
                    {393, "I19>"},
                    {394, "I19> Инв."},
                    {395, "I20> ИО"},
                    {396, "I20> ИО Инв."},
                    {397, "I20>"},
                    {398, "I20> Инв."},
                    {399, "I21> ИО"},
                    {400, "I21> ИО Инв."},
                    {401, "I21>"},
                    {402, "I21> Инв."},
                    {403, "I22> ИО"},
                    {404, "I22> ИО Инв."},
                    {405, "I22>"},
                    {406, "I22> Инв."},
                    {407, "I23> ИО"},
                    {408, "I23> ИО Инв."},
                    {409, "I23>"},
                    {410, "I23> Инв."},
                    {411, "I24> ИО"},
                    {412, "I24> ИО Инв."},
                    {413, "I24>"},
                    {414, "I24> Инв."},
                    {415, "I25> ИО"},
                    {416, "I25> ИО Инв."},
                    {417, "I25>"},
                    {418, "I25> Инв."},
                    {419, "I26> ИО"},
                    {420, "I26> ИО Инв."},
                    {421, "I26>"},
                    {422, "I26> Инв."},
                    {423, "I27> ИО"},
                    {424, "I27> ИО Инв."},
                    {425, "I27>"},
                    {426, "I27> Инв."},
                    {427, "I28> ИО"},
                    {428, "I28> ИО Инв."},
                    {429, "I28>"},
                    {430, "I28> Инв."},
                    {431, "I29> ИО"},
                    {432, "I29> ИО Инв."},
                    {433, "I29>"},
                    {434, "I29> Инв."},
                    {435, "I30> ИО"},
                    {436, "I30> ИО Инв."},
                    {437, "I30>"},
                    {438, "I30> Инв."},
                    {439, "I31> ИО"},
                    {440, "I31> ИО Инв."},
                    {441, "I31>"},
                    {442, "I31> Инв."},
                    {443, "I32> ИО"},
                    {444, "I32> ИО Инв."},
                    {445, "I32>"},
                    {446, "I32> Инв."},
                    
                    {447, "ВНЕШ. 1"},
                    {448, "ВНЕШ. 1 Инв."},
                    {449, "ВНЕШ. 2"},
                    {450, "ВНЕШ. 2 Инв."},
                    {451, "ВНЕШ. 3"},
                    {452, "ВНЕШ. 3 Инв."},
                    {453, "ВНЕШ. 4"},
                    {454, "ВНЕШ. 4 Инв."},
                    {455, "ВНЕШ. 5"},
                    {456, "ВНЕШ. 5 Инв."},
                    {457, "ВНЕШ. 6"},
                    {458, "ВНЕШ. 6 Инв."},
                    {459, "ВНЕШ. 7"},
                    {460, "ВНЕШ. 7 Инв."},
                    {461, "ВНЕШ. 8"},
                    {462, "ВНЕШ. 8 Инв."},
                    {463, "ВНЕШ. 9"},
                    {464, "ВНЕШ. 9 Инв."},
                    {465, "ВНЕШ. 10"},
                    {466, "ВНЕШ. 10 Инв."},
                    {467, "ВНЕШ. 11"},
                    {468, "ВНЕШ. 11 Инв."},
                    {469, "ВНЕШ. 12"},
                    {470, "ВНЕШ. 12 Инв."},
                    {471, "ВНЕШ. 13"},
                    {472, "ВНЕШ. 13 Инв."},
                    {473, "ВНЕШ. 14"},
                    {474, "ВНЕШ. 14 Инв."},
                    {475, "ВНЕШ. 15"},
                    {476, "ВНЕШ. 15 Инв."},
                    {477, "ВНЕШ. 16"},
                    {478, "ВНЕШ. 16 Инв."},
                    {479, "ВНЕШ. 17"},
                    {480, "ВНЕШ. 17 Инв."},
                    {481, "ВНЕШ. 18"},
                    {482, "ВНЕШ. 18 Инв."},
                    {483, "ВНЕШ. 19"},
                    {484, "ВНЕШ. 19 Инв."},
                    {485, "ВНЕШ. 20"},
                    {486, "ВНЕШ. 20 Инв."},
                    {487, "ВНЕШ. 21"},
                    {488, "ВНЕШ. 21 Инв."},
                    {489, "ВНЕШ. 22"},
                    {490, "ВНЕШ. 22 Инв."},
                    {491, "ВНЕШ. 23"},
                    {492, "ВНЕШ. 23 Инв."},
                    {493, "ВНЕШ. 24"},
                    {494, "ВНЕШ. 24 Инв."},

                    {495, "ССЛ1"},
                    {496, "ССЛ1 Инв."},
                    {497, "ССЛ2"},
                    {498, "ССЛ2 Инв."},
                    {499, "ССЛ3"},
                    {500, "ССЛ3 Инв."},
                    {501, "ССЛ4"},
                    {502, "ССЛ4 Инв."},
                    {503, "ССЛ5"},
                    {504, "ССЛ5 Инв."},
                    {505, "ССЛ6"},
                    {506, "ССЛ6 Инв."},
                    {507, "ССЛ7"},
                    {508, "ССЛ7 Инв."},
                    {509, "ССЛ8"},
                    {510, "ССЛ8 Инв."},
                    {511, "ССЛ9"},
                    {512, "ССЛ9 Инв."},
                    {513, "ССЛ10"},
                    {514, "ССЛ10 Инв."},
                    {515, "ССЛ11"},
                    {516, "ССЛ11 Инв."},
                    {517, "ССЛ12"},
                    {518, "ССЛ12 Инв."},
                    {519, "ССЛ13"},
                    {520, "ССЛ13 Инв."},
                    {521, "ССЛ14"},
                    {522, "ССЛ14 Инв."},
                    {523, "ССЛ15"},
                    {524, "ССЛ15 Инв."},
                    {525, "ССЛ16"},
                    {526, "ССЛ16 Инв."},
                    {527, "ССЛ17"},
                    {528, "ССЛ17 Инв."},
                    {529, "ССЛ18"},
                    {530, "ССЛ18 Инв."},
                    {531, "ССЛ19"},
                    {532, "ССЛ19 Инв."},
                    {533, "ССЛ20"},
                    {534, "ССЛ20 Инв."},
                    {535, "ССЛ21"},
                    {536, "ССЛ21 Инв."},
                    {537, "ССЛ22"},
                    {538, "ССЛ22 Инв."},
                    {539, "ССЛ23"},
                    {540, "ССЛ23 Инв."},
                    {541, "ССЛ24"},
                    {542, "ССЛ24 Инв."},
                    {543, "ССЛ25"},
                    {544, "ССЛ25 Инв."},
                    {545, "ССЛ26"},
                    {546, "ССЛ26 Инв."},
                    {547, "ССЛ27"},
                    {548, "ССЛ27 Инв."},
                    {549, "ССЛ28"},
                    {550, "ССЛ28 Инв."},
                    {551, "ССЛ29"},
                    {552, "ССЛ29 Инв."},
                    {553, "ССЛ30"},
                    {554, "ССЛ30 Инв."},
                    {555, "ССЛ31"},
                    {556, "ССЛ31 Инв."},
                    {557, "ССЛ32"},
                    {558, "ССЛ32 Инв."},
                    {559, "ССЛ33"},
                    {560, "ССЛ33 Инв."},
                    {561, "ССЛ34"},
                    {562, "ССЛ34 Инв."},
                    {563, "ССЛ35"},
                    {564, "ССЛ35 Инв."},
                    {565, "ССЛ36"},
                    {566, "ССЛ36 Инв."},
                    {567, "ССЛ37"},
                    {568, "ССЛ37 Инв."},
                    {569, "ССЛ38"},
                    {570, "ССЛ38 Инв."},
                    {571, "ССЛ39"},
                    {572, "ССЛ39 Инв."},
                    {573, "ССЛ40"},
                    {574, "ССЛ40 Инв."},
                    {575, "ССЛ41"},
                    {576, "ССЛ41 Инв."},
                    {577, "ССЛ42"},
                    {578, "ССЛ42 Инв."},
                    {579, "ССЛ43"},
                    {580, "ССЛ43 Инв."},
                    {581, "ССЛ44"},
                    {582, "ССЛ44 Инв."},
                    {583, "ССЛ45"},
                    {584, "ССЛ45 Инв."},
                    {585, "ССЛ46"},
                    {586, "ССЛ46 Инв."},
                    {587, "ССЛ47"},
                    {588, "ССЛ47 Инв."},
                    {589, "ССЛ48"},
                    {590, "ССЛ48 Инв."},

                    {591, "УРОВ СШ1"},
                    {592, "УРОВ СШ1 Инв."},
                    {593, "УРОВ СШ2"},
                    {594, "УРОВ СШ2 Инв."},
                    {595, "УРОВ ПО"},
                    {596, "УРОВ ПО Инв."},

                    {597, "УРОВ Пр1"},
                    {598, "УРОВ Пр1 Инв."},
                    {599, "УРОВ Пр2"},
                    {600, "УРОВ Пр2 Инв."},
                    {601, "УРОВ Пр3"},
                    {602, "УРОВ Пр3 Инв."},
                    {603, "УРОВ Пр4"},
                    {604, "УРОВ Пр4 Инв."},
                    {605, "УРОВ Пр5"},
                    {606, "УРОВ Пр5 Инв."},
                    {607, "УРОВ Пр6"},
                    {608, "УРОВ Пр6 Инв."},
                    {609, "УРОВ Пр7"},
                    {610, "УРОВ Пр7 Инв."},
                    {611, "УРОВ Пр8"},
                    {612, "УРОВ Пр8 Инв."},
                    {613, "УРОВ Пр9"},
                    {614, "УРОВ Пр9 Инв."},
                    {615, "УРОВ Пр10"},
                    {616, "УРОВ Пр10 Инв."},
                    {617, "УРОВ Пр11"},
                    {618, "УРОВ Пр11 Инв."},
                    {619, "УРОВ Пр12"},
                    {620, "УРОВ Пр12 Инв."},
                    {621, "УРОВ Пр13"},
                    {622, "УРОВ Пр13 Инв."},
                    {623, "УРОВ Пр14"},
                    {624, "УРОВ Пр14 Инв."},
                    {625, "УРОВ Пр15"},
                    {626, "УРОВ Пр15 Инв."},
                    {627, "УРОВ Пр16"},
                    {628, "УРОВ Пр16 Инв."},
                    {629, "УРОВ Пр17"},
                    {630, "УРОВ Пр17 Инв."},
                    {631, "УРОВ Пр18"},
                    {632, "УРОВ Пр18 Инв."},
                    {633, "УРОВ Пр19"},
                    {634, "УРОВ Пр19 Инв."},
                    {635, "УРОВ Пр20"},
                    {636, "УРОВ Пр20 Инв."},
                    {637, "УРОВ Пр21"},
                    {638, "УРОВ Пр21 Инв."},
                    {639, "УРОВ Пр22"},
                    {640, "УРОВ Пр22 Инв."},
                    {641, "УРОВ Пр23"},
                    {642, "УРОВ Пр23 Инв."},
                    {643, "УРОВ Пр24"},
                    {644, "УРОВ Пр24 Инв."},
                };
                switch (ConfigurationStructBigV213.DeviceModelType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (ushort i = 129; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 224; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВНЕШНИЕ РЕЗЕРВ
                            for (ushort i = 447; i < 495; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A2":
                    case "A5":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВНЕШНИЕ РЕЗЕРВ
                            for (ushort i = 447; i < 495; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A3":
                    case "A6":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВНЕШНИЕ РЕЗЕРВ
                            for (ushort i = 447; i < 495; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A4":
                    case "A7":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                            //ВНЕШНИЕ РЕЗЕРВ
                            for (ushort i = 447; i < 495; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }
                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }
                        //ВНЕШНИЕ РЕЗЕРВ
                        for (ushort i = 447; i < 495; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }
                return ret;
            }
        }

        public static Dictionary<ushort, string> ExtDefSignals213
        {
            get
            {
                List<string> list = new List<string>
                {
                    "Нет",
                    "Д1",
                    "Д1 Инв.",
                    "Д2",
                    "Д2 Инв.",
                    "Д3",
                    "Д3 Инв.",
                    "Д4",
                    "Д4 Инв.",
                    "Д5",
                    "Д5 Инв.",
                    "Д6",
                    "Д6 Инв.",
                    "Д7",
                    "Д7 Инв.",
                    "Д8",
                    "Д8 Инв.",
                    "Д9",
                    "Д9 Инв.",
                    "Д10",
                    "Д10 Инв.",
                    "Д11",
                    "Д11 Инв.",
                    "Д12",
                    "Д12 Инв.",
                    "Д13",
                    "Д13 Инв.",
                    "Д14",
                    "Д14 Инв.",
                    "Д15",
                    "Д15 Инв.",
                    "Д16",
                    "Д16 Инв.",
                    "Д17",
                    "Д17 Инв.",
                    "Д18",
                    "Д18 Инв.",
                    "Д19",
                    "Д19 Инв.",
                    "Д20",
                    "Д20 Инв.",
                    "Д21",
                    "Д21 Инв.",
                    "Д22",
                    "Д22 Инв.",
                    "Д23",
                    "Д23 Инв.",
                    "Д24",
                    "Д24 Инв.",
                    "Д25",
                    "Д25 Инв.",
                    "Д26",
                    "Д26 Инв.",
                    "Д27",
                    "Д27 Инв.",
                    "Д28",
                    "Д28 Инв.",
                    "Д29",
                    "Д29 Инв.",
                    "Д30",
                    "Д30 Инв.",
                    "Д31",
                    "Д31 Инв.",
                    "Д32",
                    "Д32 Инв.",
                    "Д33",
                    "Д33 Инв.",
                    "Д34",
                    "Д34 Инв.",
                    "Д35",
                    "Д35 Инв.",
                    "Д36",
                    "Д36 Инв.",
                    "Д37",
                    "Д37 Инв.",
                    "Д38",
                    "Д38 Инв.",
                    "Д39",
                    "Д39 Инв.",
                    "Д40",
                    "Д40 Инв.",
                    "Д41",
                    "Д41 Инв.",
                    "Д42",
                    "Д42 Инв.",
                    "Д43",
                    "Д43 Инв.",
                    "Д44",
                    "Д44 Инв.",
                    "Д45",
                    "Д45 Инв.",
                    "Д46",
                    "Д46 Инв.",
                    "Д47",
                    "Д47 Инв.",
                    "Д48",
                    "Д48 Инв.",
                    "Д49",
                    "Д49 Инв.",
                    "Д50",
                    "Д50 Инв.",
                    "Д51",
                    "Д51 Инв.",
                    "Д52",
                    "Д52 Инв.",
                    "Д53",
                    "Д53 Инв.",
                    "Д54",
                    "Д54 Инв.",
                    "Д55",
                    "Д55 Инв.",
                    "Д56",
                    "Д56 Инв.",
                    "Д57",
                    "Д57 Инв.",
                    "Д58",
                    "Д58 Инв.",
                    "Д59",
                    "Д59 Инв.",
                    "Д60",
                    "Д60 Инв.",
                    "Д61",
                    "Д61 Инв.",
                    "Д62",
                    "Д62 Инв.",
                    "Д63",
                    "Д63 Инв.",
                    "Д64",
                    "Д64 Инв.",
                    "Д65",
                    "Д65 Инв.",
                    "Д66",
                    "Д66 Инв.",
                    "Д67",
                    "Д67 Инв.",
                    "Д68",
                    "Д68 Инв.",
                    "Д69",
                    "Д69 Инв.",
                    "Д70",
                    "Д70 Инв.",
                    "Д71",
                    "Д71 Инв.",
                    "Д72",
                    "Д72 Инв.",
                    "Д73",
                    "Д73 Инв.",
                    "Д74",
                    "Д74 Инв.",
                    "Д75",
                    "Д75 Инв.",
                    "Д76",
                    "Д76 Инв.",
                    "Д77",
                    "Д77 Инв.",
                    "Д78",
                    "Д78 Инв.",
                    "Д79",
                    "Д79 Инв.",
                    "Д80",
                    "Д80 Инв.",
                    "Д81",
                    "Д81 Инв.",
                    "Д82",
                    "Д82 Инв.",
                    "Д83",
                    "Д83 Инв.",
                    "Д84",
                    "Д84 Инв.",
                    "Д85",
                    "Д85 Инв.",
                    "Д86",
                    "Д86 Инв.",
                    "Д87",
                    "Д87 Инв.",
                    "Д88",
                    "Д88 Инв.",
                    "Д89",
                    "Д89 Инв.",
                    "Д90",
                    "Д90 Инв.",
                    "Д91",
                    "Д91 Инв.",
                    "Д92",
                    "Д92 Инв.",
                    "Д93",
                    "Д93 Инв.",
                    "Д94",
                    "Д94 Инв.",
                    "Д95",
                    "Д95 Инв.",
                    "Д96",
                    "Д96 Инв.",
                    
                    "ЛС1",
                    "ЛС1 Инв.",
                    "ЛС2",
                    "ЛС2 Инв.",
                    "ЛС3",
                    "ЛС3 Инв.",
                    "ЛС4",
                    "ЛС4 Инв.",
                    "ЛС5",
                    "ЛС5 Инв.",
                    "ЛС6",
                    "ЛС6 Инв.",
                    "ЛС7",
                    "ЛС7 Инв.",
                    "ЛС8",
                    "ЛС8 Инв.",
                    "ЛС9",
                    "ЛС9 Инв.",
                    "ЛС10",
                    "ЛС10 Инв.",
                    "ЛС11",
                    "ЛС11 Инв.",
                    "ЛС12",
                    "ЛС12 Инв.",
                    "ЛС13",
                    "ЛС13 Инв.",
                    "ЛС14",
                    "ЛС14 Инв.",
                    "ЛС15",
                    "ЛС15 Инв.",
                    "ЛС16",
                    "ЛС16 Инв.",
                    "БГС1",
                    "БГС1 Инв.",
                    "БГС2",
                    "БГС2 Инв.",
                    "БГС3",
                    "БГС3 Инв.",
                    "БГС4",
                    "БГС4 Инв.",
                    "БГС5",
                    "БГС5 Инв.",
                    "БГС6",
                    "БГС6 Инв.",
                    "БГС7",
                    "БГС7 Инв.",
                    "БГС8",
                    "БГС8 Инв.",
                    "БГС9",
                    "БГС9 Инв.",
                    "БГС10",
                    "БГС10 Инв.",
                    "БГС11",
                    "БГС11 Инв.",
                    "БГС12",
                    "БГС12 Инв.",
                    "БГС13",
                    "БГС13 Инв.",
                    "БГС14",
                    "БГС14 Инв.",
                    "БГС15",
                    "БГС15 Инв.",
                    "БГС16",
                    "БГС16 Инв.",
                    "ВЛС1",
                    "ВЛС1 Инв.",
                    "ВЛС2",
                    "ВЛС2 Инв.",
                    "ВЛС3",
                    "ВЛС3 Инв.",
                    "ВЛС4",
                    "ВЛС4 Инв.",
                    "ВЛС5",
                    "ВЛС5 Инв.",
                    "ВЛС6",
                    "ВЛС6 Инв.",
                    "ВЛС7",
                    "ВЛС7 Инв.",
                    "ВЛС8",
                    "ВЛС8 Инв.",
                    "ВЛС9",
                    "ВЛС9 Инв.",
                    "ВЛС10",
                    "ВЛС10 Инв.",
                    "ВЛС11",
                    "ВЛС11 Инв.",
                    "ВЛС12",
                    "ВЛС12 Инв.",
                    "ВЛС13",
                    "ВЛС13 Инв.",
                    "ВЛС14",
                    "ВЛС14 Инв.",
                    "ВЛС15",
                    "ВЛС15 Инв.",
                    "ВЛС16",
                    "ВЛС16 Инв.",
                    "Iд1м СШ1 *",
                    "Iд1м СШ1* Инв.",
                    "Iд1м СШ1",
                    "Iд1м СШ1 Инв.",
                    "Iд2м СШ2 *",
                    "Iд2м СШ2* Инв.",
                    "Iд2м СШ2",
                    "Iд2м СШ2 Инв.",
                    "Iд3м ПО *",
                    "Iд3м ПО* Инв.",
                    "Iд3м ПО",
                    "Iд3м ПО Инв.",
                    "Iд1 СШ1 ИО",
                    "Iд1 СШ1 ИО Инв.",
                    "Iд1 СШ1 *",
                    "Iд1 СШ1* Инв.",
                    "Iд1 СШ1",
                    "Iд1 СШ1 Инв.",
                    "Iд2 СШ2 ИО",
                    "Iд2 СШ2 ИО Инв.",
                    "Iд2 СШ2 *",
                    "Iд2 СШ2* Инв.",
                    "Iд2 СШ2",
                    "Iд2 СШ2 Инв.",
                    "Iд3 ПО ИО",
                    "Iд3 ПО ИО Инв.",
                    "Iд3 ПО *",
                    "Iд3 ПО* Инв.",
                    "Iд3 ПО",
                    "Iд3 ПО Инв.",

                    "I1> ИО",
                    "I1> ИО Инв.",
                    "I1>",
                    "I1> Инв.",
                    "I2> ИО",
                    "I2> ИО Инв.",
                    "I2>",
                    "I2> Инв.",
                    "I3> ИО",
                    "I3> ИО Инв.",
                    "I3>",
                    "I3> Инв.",
                    "I4> ИО",
                    "I4> ИО Инв.",
                    "I4>",
                    "I4> Инв.",
                    "I5> ИО",
                    "I5> ИО Инв.",
                    "I5>",
                    "I5> Инв.",
                    "I6> ИО",
                    "I6> ИО Инв.",
                    "I6>",
                    "I6> Инв.",
                    "I7> ИО",
                    "I7> ИО Инв.",
                    "I7>",
                    "I7> Инв.",
                    "I8> ИО",
                    "I8> ИО Инв.",
                    "I8>",
                    "I8> Инв.",
                    "I9> ИО",
                    "I9> ИО Инв.",
                    "I9>",
                    "I9> Инв.",
                    "I10> ИО",
                    "I10> ИО Инв.",
                    "I10>",
                    "I10> Инв.",
                    "I11> ИО",
                    "I11> ИО Инв.",
                    "I11>",
                    "I11> Инв.",
                    "I12> ИО",
                    "I12> ИО Инв.",
                    "I12>",
                    "I12> Инв.",
                    "I13> ИО",
                    "I13> ИО Инв.",
                    "I13>",
                    "I13> Инв.",
                    "I14> ИО",
                    "I14> ИО Инв.",
                    "I14>",
                    "I14> Инв.",
                    "I15> ИО",
                    "I15> ИО Инв.",
                    "I15>",
                    "I15> Инв.",
                    "I16> ИО",
                    "I16> ИО Инв.",
                    "I16>",
                    "I16> Инв.",
                    "I17> ИО",
                    "I17> ИО Инв.",
                    "I17>",
                    "I17> Инв.",
                    "I18> ИО",
                    "I18> ИО Инв.",
                    "I18>",
                    "I18> Инв.",
                    "I19> ИО",
                    "I19> ИО Инв.",
                    "I19>",
                    "I19> Инв.",
                    "I20> ИО",
                    "I20> ИО Инв.",
                    "I20>",
                    "I20> Инв.",
                    "I21> ИО",
                    "I21> ИО Инв.",
                    "I21>",
                    "I21> Инв.",
                    "I22> ИО",
                    "I22> ИО Инв.",
                    "I22>",
                    "I22> Инв.",
                    "I23> ИО",
                    "I23> ИО Инв.",
                    "I23>",
                    "I23> Инв.",
                    "I24> ИО",
                    "I24> ИО Инв.",
                    "I24>",
                    "I24> Инв.",
                    "I25> ИО",
                    "I25> ИО Инв.",
                    "I25>",
                    "I25> Инв.",
                    "I26> ИО",
                    "I26> ИО Инв.",
                    "I26>",
                    "I26> Инв.",
                    "I27> ИО",
                    "I27> ИО Инв.",
                    "I27>",
                    "I27> Инв.",
                    "I28> ИО",
                    "I28> ИО Инв.",
                    "I28>",
                    "I28> Инв.",
                    "I29> ИО",
                    "I29> ИО Инв.",
                    "I29>",
                    "I29> Инв.",
                    "I30> ИО",
                    "I30> ИО Инв.",
                    "I30>",
                    "I30> Инв.",
                    "I31> ИО",
                    "I31> ИО Инв.",
                    "I31>",
                    "I31> Инв.",
                    "I32> ИО",
                    "I32> ИО Инв.",
                    "I32>",
                    "I32> Инв.",

                    "Резерв1",
                    "Резерв2",
                    "Резерв3",
                    "Резерв4",
                    "Резерв5",
                    "Резерв6",
                    "Резерв7",
                    "Резерв8",
                    "Резерв9",
                    "Резерв10",
                    "Резерв11",
                    "Резерв12",
                    "Резерв13",
                    "Резерв14",
                    "Резерв15",
                    "Резерв16",
                    "Резерв17",
                    "Резерв18",
                    "Резерв19",
                    "Резерв20",
                    "Резерв21",
                    "Резерв22",
                    "Резерв23",
                    "Резерв24",
                    "Резерв25",
                    "Резерв26",
                    "Резерв27",
                    "Резерв28",
                    "Резерв29",
                    "Резерв30",
                    "Резерв31",
                    "Резерв32",
                    "Резерв33",
                    "Резерв34",
                    "Резерв35",
                    "Резерв36",
                    "Резерв37",
                    "Резерв48",
                    "Резерв49",
                    "Резерв40",

                    "U> ИО",
                    "U> ИО Инв.",
                    "U>",
                    "U> Инв.",
                    "U>> ИО",
                    "U>> ИО Инв.",
                    "U>>",
                    "U>> Инв.",

                    "ССЛ1",
                    "ССЛ1 Инв.",
                    "ССЛ2",
                    "ССЛ2 Инв.",
                    "ССЛ3",
                    "ССЛ3 Инв.",
                    "ССЛ4",
                    "ССЛ4 Инв.",
                    "ССЛ5",
                    "ССЛ5 Инв.",
                    "ССЛ6",
                    "ССЛ6 Инв.",
                    "ССЛ7",
                    "ССЛ7 Инв.",
                    "ССЛ8",
                    "ССЛ8 Инв.",
                    "ССЛ9",
                    "ССЛ9 Инв.",
                    "ССЛ10",
                    "ССЛ10 Инв.",
                    "ССЛ11",
                    "ССЛ11 Инв.",
                    "ССЛ12",
                    "ССЛ12 Инв.",
                    "ССЛ13",
                    "ССЛ13 Инв.",
                    "ССЛ14",
                    "ССЛ14 Инв.",
                    "ССЛ15",
                    "ССЛ15 Инв.",
                    "ССЛ16",
                    "ССЛ16 Инв.",
                    "ССЛ17",
                    "ССЛ17 Инв.",
                    "ССЛ18",
                    "ССЛ18 Инв.",
                    "ССЛ19",
                    "ССЛ19 Инв.",
                    "ССЛ20",
                    "ССЛ20 Инв.",
                    "ССЛ21",
                    "ССЛ21 Инв.",
                    "ССЛ22",
                    "ССЛ22 Инв.",
                    "ССЛ23",
                    "ССЛ23 Инв.",
                    "ССЛ24",
                    "ССЛ24 Инв.",
                    "ССЛ25",
                    "ССЛ25 Инв.",
                    "ССЛ26",
                    "ССЛ26 Инв.",
                    "ССЛ27",
                    "ССЛ27 Инв.",
                    "ССЛ28",
                    "ССЛ28 Инв.",
                    "ССЛ29",
                    "ССЛ29 Инв.",
                    "ССЛ30",
                    "ССЛ30 Инв.",
                    "ССЛ31",
                    "ССЛ31 Инв.",
                    "ССЛ32",
                    "ССЛ32 Инв.",
                    "ССЛ33",
                    "ССЛ33 Инв.",
                    "ССЛ34",
                    "ССЛ34 Инв.",
                    "ССЛ35",
                    "ССЛ35 Инв.",
                    "ССЛ36",
                    "ССЛ36 Инв.",
                    "ССЛ37",
                    "ССЛ37 Инв.",
                    "ССЛ38",
                    "ССЛ38 Инв.",
                    "ССЛ39",
                    "ССЛ39 Инв.",
                    "ССЛ40",
                    "ССЛ40 Инв.",
                    "ССЛ41",
                    "ССЛ41 Инв.",
                    "ССЛ42",
                    "ССЛ42 Инв.",
                    "ССЛ43",
                    "ССЛ43 Инв.",
                    "ССЛ44",
                    "ССЛ44 Инв.",
                    "ССЛ45",
                    "ССЛ45 Инв.",
                    "ССЛ46",
                    "ССЛ46 Инв.",
                    "ССЛ47",
                    "ССЛ47 Инв.",
                    "ССЛ48",
                    "ССЛ48 Инв.",

                    "УРОВ СШ1",
                    "УРОВ СШ1 Инв.",
                    "УРОВ СШ2",
                    "УРОВ СШ2 Инв.",
                    "УРОВ ПО",
                    "УРОВ ПО Инв.",

                    "УРОВ Пр1",
                    "УРОВ Пр1 Инв.",
                    "УРОВ Пр2",
                    "УРОВ Пр2 Инв.",
                    "УРОВ Пр3",
                    "УРОВ Пр3 Инв.",
                    "УРОВ Пр4",
                    "УРОВ Пр4 Инв.",
                    "УРОВ Пр5",
                    "УРОВ Пр5 Инв.",
                    "УРОВ Пр6",
                    "УРОВ Пр6 Инв.",
                    "УРОВ Пр7",
                    "УРОВ Пр7 Инв.",
                    "УРОВ Пр8",
                    "УРОВ Пр8 Инв.",
                    "УРОВ Пр9",
                    "УРОВ Пр9 Инв.",
                    "УРОВ Пр10",
                    "УРОВ Пр10 Инв.",
                    "УРОВ Пр11",
                    "УРОВ Пр11 Инв.",
                    "УРОВ Пр12",
                    "УРОВ Пр12 Инв.",
                    "УРОВ Пр13",
                    "УРОВ Пр13 Инв.",
                    "УРОВ Пр14",
                    "УРОВ Пр14 Инв.",
                    "УРОВ Пр15",
                    "УРОВ Пр15 Инв.",
                    "УРОВ Пр16",
                    "УРОВ Пр16 Инв.",
                    "УРОВ Пр17",
                    "УРОВ Пр17 Инв.",
                    "УРОВ Пр18",
                    "УРОВ Пр18 Инв.",
                    "УРОВ Пр19",
                    "УРОВ Пр19 Инв.",
                    "УРОВ Пр20",
                    "УРОВ Пр20 Инв.",
                    "УРОВ Пр21",
                    "УРОВ Пр21 Инв.",
                    "УРОВ Пр22",
                    "УРОВ Пр22 Инв.",
                    "УРОВ Пр23",
                    "УРОВ Пр23 Инв.",
                    "УРОВ Пр24",
                    "УРОВ Пр24 Инв.",
                };

                SortedDictionary<ushort, string> ret = new SortedDictionary<ushort, string>();
                for (ushort i = 0; i < list.Count; i++)
                {
                    ret.Add(i, list[i]);
                }

                switch (ConfigurationStructBigV213.DeviceModelType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (ushort i = 129; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index = ret.First(r => r.Value == "УРОВ Пр17").Key;

                            //присоединения УРОВ
                            for (ushort i = index; i < index + 16; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index1 = ret.First(r => r.Value == "U> ИО").Key;
                            for (ushort i = index1; i < index1 + 8; i++)
                            {
                                ret.Remove(i);
                            }
                            
                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }

                        }
                        break;
                    case "A2":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index1 = ret.First(r => r.Value == "U> ИО").Key;
                            for (ushort i = index1; i < index1 + 8; i++)
                            {
                                ret.Remove(i);
                            }
                            
                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A3":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index = ret.First(r => r.Value == "U> ИО").Key;
                            for (ushort i = index; i < index + 8; i++)
                            {
                                ret.Remove(i);
                            }
                            
                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A4":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }


                            ushort index = ret.First(r => r.Value == "U> ИО").Key;
                            for (ushort i = index; i < index + 8; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A5":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index = ret.First(r => r.Value == "УРОВ Пр21").Key;

                            //присоединения УРОВ
                            for (ushort i = index; i < index + 8; i++)
                            {
                                ret.Remove(i);
                            }

                            ret.Add(index, "U< ИО");
                            ret.Add((ushort) (index + 1), "U< ИО Инв.");
                            ret.Add((ushort) (index + 2), "U<");
                            ret.Add((ushort) (index + 3), "U< Инв.");
                            ret.Add((ushort) (index + 4), "U<< ИО");
                            ret.Add((ushort) (index + 5), "U<< ИО Инв.");
                            ret.Add((ushort) (index + 6), "U<<");
                            ret.Add((ushort) (index + 7), "U<< Инв.");

                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A6":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index = ret.First(r => r.Value == "УРОВ Пр21").Key;
                            //присоединения УРОВ
                            for (ushort i = index; i < index + 8; i++)
                            {
                                ret.Remove(i);
                            }

                            ret.Add(index, "U< ИО");
                            ret.Add((ushort)(index + 1), "U< ИО Инв.");
                            ret.Add((ushort)(index + 2), "U<");
                            ret.Add((ushort)(index + 3), "U< Инв.");
                            ret.Add((ushort)(index + 4), "U<< ИО");
                            ret.Add((ushort)(index + 5), "U<< ИО Инв.");
                            ret.Add((ushort)(index + 6), "U<<");
                            ret.Add((ushort)(index + 7), "U<< Инв.");

                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A7":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index = ret.First(r => r.Value == "УРОВ Пр21").Key;

                            //присоединения УРОВ
                            for (ushort i = index; i < index + 8; i++)
                            {
                                ret.Remove(i);
                            }

                            ret.Add(index, "U< ИО");
                            ret.Add((ushort)(index + 1), "U< ИО Инв.");
                            ret.Add((ushort)(index + 2), "U<");
                            ret.Add((ushort)(index + 3), "U< Инв.");
                            ret.Add((ushort)(index + 4), "U<< ИО");
                            ret.Add((ushort)(index + 5), "U<< ИО Инв.");
                            ret.Add((ushort)(index + 6), "U<<");
                            ret.Add((ushort)(index + 7), "U<< Инв.");

                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    default:
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index = ret.First(r => r.Value == "УРОВ Пр17").Key;

                            //присоединения УРОВ
                            for (ushort i = index; i < index + 16; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index1 = ret.First(r => r.Value == "U> ИО").Key;
                            for (ushort i = index1; i < index1 + 8; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index3 = ret.First(r => r.Value == "Резерв1").Key;
                            for (ushort i = index3; i < index3 + 40; i++)
                            {
                                ret.Remove(i);
                            }

                            ushort index4 = ret.First(r => r.Value == "БГС1").Key;
                            for (ushort i = index4; i < index4 + 32; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                }

                return ret.ToDictionary(r => r.Key, r => r.Value);
            }
        }

        public static Dictionary<ushort, string> ExtDefSignalsBig
        {
            get
            {
                Dictionary<ushort, string> ret = new Dictionary<ushort, string>
                {
                    {0, "Нет" },
                    {1, "Д1"},
                    {2, "Д1 Инв."},
                    {3, "Д2"},
                    {4, "Д2 Инв."},
                    {5, "Д3"},
                    {6, "Д3 Инв."},
                    {7, "Д4"},
                    {8, "Д4 Инв."},
                    {9, "Д5"},
                    {10, "Д5 Инв."},
                    {11, "Д6"},
                    {12, "Д6 Инв."},
                    {13, "Д7"},
                    {14, "Д7 Инв."},
                    {15, "Д8"},
                    {16, "Д8 Инв."},
                    {17, "Д9"},
                    {18, "Д9 Инв."},
                    {19, "Д10"},
                    {20, "Д10 Инв."},
                    {21, "Д11"},
                    {22, "Д11 Инв."},
                    {23, "Д12"},
                    {24, "Д12 Инв."},
                    {25, "Д13"},
                    {26, "Д13 Инв."},
                    {27, "Д14"},
                    {28, "Д14 Инв."},
                    {29, "Д15"},
                    {30, "Д15 Инв."},
                    {31, "Д16"},
                    {32, "Д16 Инв."},
                    {33, "Д17"},
                    {34, "Д17 Инв."},
                    {35, "Д18"},
                    {36, "Д18 Инв."},
                    {37, "Д19"},
                    {38, "Д19 Инв."},
                    {39, "Д20"},
                    {40, "Д20 Инв."},
                    {41, "Д21"},
                    {42, "Д21 Инв."},
                    {43, "Д22"},
                    {44, "Д22 Инв."},
                    {45, "Д23"},
                    {46, "Д23 Инв."},
                    {47, "Д24"},
                    {48, "Д24 Инв."},
                    {49, "Д25"},
                    {50, "Д25 Инв."},
                    {51, "Д26"},
                    {52, "Д26 Инв."},
                    {53, "Д27"},
                    {54, "Д27 Инв."},
                    {55, "Д28"},
                    {56, "Д28 Инв."},
                    {57, "Д29"},
                    {58, "Д29 Инв."},
                    {59, "Д30"},
                    {60, "Д30 Инв."},
                    {61, "Д31"},
                    {62, "Д31 Инв."},
                    {63, "Д32"},
                    {64, "Д32 Инв."},
                    {65, "Д33"},
                    {66, "Д33 Инв."},
                    {67, "Д34"},
                    {68, "Д34 Инв."},
                    {69, "Д35"},
                    {70, "Д35 Инв."},
                    {71, "Д36"},
                    {72, "Д36 Инв."},
                    {73, "Д37"},
                    {74, "Д37 Инв."},
                    {75, "Д38"},
                    {76, "Д38 Инв."},
                    {77, "Д39"},
                    {78, "Д39 Инв."},
                    {79, "Д40"},
                    {80, "Д40 Инв."},
                    {81, "Д41"},
                    {82, "Д41 Инв."},
                    {83, "Д42"},
                    {84, "Д42 Инв."},
                    {85, "Д43"},
                    {86, "Д43 Инв."},
                    {87, "Д44"},
                    {88, "Д44 Инв."},
                    {89, "Д45"},
                    {90, "Д45 Инв."},
                    {91, "Д46"},
                    {92, "Д46 Инв."},
                    {93, "Д47"},
                    {94, "Д47 Инв."},
                    {95, "Д48"},
                    {96, "Д48 Инв."},
                    {97, "Д49"},
                    {98, "Д49 Инв."},
                    {99, "Д50"},
                    {100, "Д50 Инв."},
                    {101, "Д51"},
                    {102, "Д51 Инв."},
                    {103, "Д52"},
                    {104, "Д52 Инв."},
                    {105, "Д53"},
                    {106, "Д53 Инв."},
                    {107, "Д54"},
                    {108, "Д54 Инв."},
                    {109, "Д55"},
                    {110, "Д55 Инв."},
                    {111, "Д56"},
                    {112, "Д56 Инв."},
                    {113, "Д57"},
                    {114, "Д57 Инв."},
                    {115, "Д58"},
                    {116, "Д58 Инв."},
                    {117, "Д59"},
                    {118, "Д59 Инв."},
                    {119, "Д60"},
                    {120, "Д60 Инв."},
                    {121, "Д61"},
                    {122, "Д61 Инв."},
                    {123, "Д62"},
                    {124, "Д62 Инв."},
                    {125, "Д63"},
                    {126, "Д63 Инв."},
                    {127, "Д64"},
                    {128, "Д64 Инв."},
                    {129, "Д65"},
                    {130, "Д65 Инв."},
                    {131, "Д66"},
                    {132, "Д66 Инв."},
                    {133, "Д67"},
                    {134, "Д67 Инв."},
                    {135, "Д68"},
                    {136, "Д68 Инв."},
                    {137, "Д69"},
                    {138, "Д69 Инв."},
                    {139, "Д70"},
                    {140, "Д70 Инв."},
                    {141, "Д71"},
                    {142, "Д71 Инв."},
                    {143, "Д72"},
                    {144, "Д72 Инв."},
                    {145, "Д73"},
                    {146, "Д73 Инв."},
                    {147, "Д74"},
                    {148, "Д74 Инв."},
                    {149, "Д75"},
                    {150, "Д75 Инв."},
                    {151, "Д76"},
                    {152, "Д76 Инв."},
                    {153, "Д77"},
                    {154, "Д77 Инв."},
                    {155, "Д78"},
                    {156, "Д78 Инв."},
                    {157, "Д79"},
                    {158, "Д79 Инв."},
                    {159, "Д80"},
                    {160, "Д80 Инв."},
                    {161, "Д81"},
                    {162, "Д81 Инв."},
                    {163, "Д82"},
                    {164, "Д82 Инв."},
                    {165, "Д83"},
                    {166, "Д83 Инв."},
                    {167, "Д84"},
                    {168, "Д84 Инв."},
                    {169, "Д85"},
                    {170, "Д85 Инв."},
                    {171, "Д86"},
                    {172, "Д86 Инв."},
                    {173, "Д87"},
                    {174, "Д87 Инв."},
                    {175, "Д88"},
                    {176, "Д88 Инв."},
                    {177, "Д89"},
                    {178, "Д89 Инв."},
                    {179, "Д90"},
                    {180, "Д90 Инв."},
                    {181, "Д91"},
                    {182, "Д91 Инв."},
                    {183, "Д92"},
                    {184, "Д92 Инв."},
                    {185, "Д93"},
                    {186, "Д93 Инв."},
                    {187, "Д94"},
                    {188, "Д94 Инв."},
                    {189, "Д95"},
                    {190, "Д95 Инв."},
                    {191, "Д96"},
                    {192, "Д96 Инв."},
                    {193, "ЛС1"},
                    {194, "ЛС1 Инв."},
                    {195, "ЛС2"},
                    {196, "ЛС2 Инв."},
                    {197, "ЛС3"},
                    {198, "ЛС3 Инв."},
                    {199, "ЛС4"},
                    {200, "ЛС4 Инв."},
                    {201, "ЛС5"},
                    {202, "ЛС5 Инв."},
                    {203, "ЛС6"},
                    {204, "ЛС6 Инв."},
                    {205, "ЛС7"},
                    {206, "ЛС7 Инв."},
                    {207, "ЛС8"},
                    {208, "ЛС8 Инв."},
                    {209, "ЛС9"},
                    {210, "ЛС9 Инв."},
                    {211, "ЛС10"},
                    {212, "ЛС10 Инв."},
                    {213, "ЛС11"},
                    {214, "ЛС11 Инв."},
                    {215, "ЛС12"},
                    {216, "ЛС12 Инв."},
                    {217, "ЛС13"},
                    {218, "ЛС13 Инв."},
                    {219, "ЛС14"},
                    {220, "ЛС14 Инв."},
                    {221, "ЛС15"},
                    {222, "ЛС15 Инв."},
                    {223, "ЛС16"},
                    {224, "ЛС16 Инв."},
                    {225, "БГ1"},
                    {226, "БГ1 Инв."},
                    {227, "БГ2"},
                    {228, "БГ2 Инв."},
                    {229, "БГ3"},
                    {230, "БГ3 Инв."},
                    {231, "БГ4"},
                    {232, "БГ4 Инв."},
                    {233, "БГ5"},
                    {234, "БГ5 Инв."},
                    {235, "БГ6"},
                    {236, "БГ6 Инв."},
                    {237, "БГ7"},
                    {238, "БГ7 Инв."},
                    {239, "БГ8"},
                    {240, "БГ8 Инв."},
                    {241, "БГ9"},
                    {242, "БГ9 Инв."},
                    {243, "БГ10"},
                    {244, "БГ10 Инв."},
                    {245, "БГ11"},
                    {246, "БГ11 Инв."},
                    {247, "БГ12"},
                    {248, "БГ12 Инв."},
                    {249, "БГ13"},
                    {250, "БГ13 Инв."},
                    {251, "БГ14"},
                    {252, "БГ14 Инв."},
                    {253, "БГ15"},
                    {254, "БГ15 Инв."},
                    {255, "БГ16"},
                    {256, "БГ16 Инв."},
                    {257, "ВЛС1"},
                    {258, "ВЛС1 Инв."},
                    {259, "ВЛС2"},
                    {260, "ВЛС2 Инв."},
                    {261, "ВЛС3"},
                    {262, "ВЛС3 Инв."},
                    {263, "ВЛС4"},
                    {264, "ВЛС4 Инв."},
                    {265, "ВЛС5"},
                    {266, "ВЛС5 Инв."},
                    {267, "ВЛС6"},
                    {268, "ВЛС6 Инв."},
                    {269, "ВЛС7"},
                    {270, "ВЛС7 Инв."},
                    {271, "ВЛС8"},
                    {272, "ВЛС8 Инв."},
                    {273, "ВЛС9"},
                    {274, "ВЛС9 Инв."},
                    {275, "ВЛС10"},
                    {276, "ВЛС10 Инв."},
                    {277, "ВЛС11"},
                    {278, "ВЛС11 Инв."},
                    {279, "ВЛС12"},
                    {280, "ВЛС12 Инв."},
                    {281, "ВЛС13"},
                    {282, "ВЛС13 Инв."},
                    {283, "ВЛС14"},
                    {284, "ВЛС14 Инв."},
                    {285, "ВЛС15"},
                    {286, "ВЛС15 Инв."},
                    {287, "ВЛС16"},
                    {288, "ВЛС16 Инв."},
                    {289, "Iд1м СШ1 *"},
                    {290, "Iд1м СШ1* Инв."},
                    {291, "Iд1м СШ1"},
                    {292, "Iд1м СШ1 Инв."},
                    {293, "Iд2м СШ2 *"},
                    {294, "Iд2м СШ2* Инв."},
                    {295, "Iд2м СШ2"},
                    {296, "Iд2м СШ2 Инв."},
                    {297, "Iд3м ПО *"},
                    {298, "Iд3м ПО* Инв."},
                    {299, "Iд3м ПО"},
                    {300, "Iд3м ПО Инв."},
                    {301, "Iд1 СШ1 ИО"},
                    {302, "Iд1 СШ1 ИО Инв."},
                    {303, "Iд1 СШ1 *"},
                    {304, "Iд1 СШ1* Инв."},
                    {305, "Iд1 СШ1"},
                    {306, "Iд1 СШ1 Инв."},
                    {307, "Iд2 СШ2 ИО"},
                    {308, "Iд2 СШ2 ИО Инв."},
                    {309, "Iд2 СШ2 *"},
                    {310, "Iд2 СШ2* Инв."},
                    {311, "Iд2 СШ2"},
                    {312, "Iд2 СШ2 Инв."},
                    {313, "Iд3 ПО ИО"},
                    {314, "Iд3 ПО ИО Инв."},
                    {315, "Iд3 ПО *"},
                    {316, "Iд3 ПО* Инв."},
                    {317, "Iд3 ПО"},
                    {318, "Iд3 ПО Инв."},

                    {319, "I1> ИО"},
                    {320, "I1> ИО Инв."},
                    {321, "I1>"},
                    {322, "I1> Инв."},
                    {323, "I2> ИО"},
                    {324, "I2> ИО Инв."},
                    {325, "I2>"},
                    {326, "I2> Инв."},
                    {327, "I3> ИО"},
                    {328, "I3> ИО Инв."},
                    {329, "I3>"},
                    {330, "I3> Инв."},
                    {331, "I4> ИО"},
                    {332, "I4> ИО Инв."},
                    {333, "I4>"},
                    {334, "I4> Инв."},
                    {335, "I5> ИО"},
                    {336, "I5> ИО Инв."},
                    {337, "I5>"},
                    {338, "I5> Инв."},
                    {339, "I6> ИО"},
                    {340, "I6> ИО Инв."},
                    {341, "I6>"},
                    {342, "I6> Инв."},
                    {343, "I7> ИО"},
                    {344, "I7> ИО Инв."},
                    {345, "I7>"},
                    {346, "I7> Инв."},
                    {347, "I8> ИО"},
                    {348, "I8> ИО Инв."},
                    {349, "I8>"},
                    {350, "I8> Инв."},
                    {351, "I9> ИО"},
                    {352, "I9> ИО Инв."},
                    {353, "I9>"},
                    {354, "I9> Инв."},
                    {355, "I10> ИО"},
                    {356, "I10> ИО Инв."},
                    {357, "I10>"},
                    {358, "I10> Инв."},
                    {359, "I11> ИО"},
                    {360, "I11> ИО Инв."},
                    {361, "I11>"},
                    {362, "I11> Инв."},
                    {363, "I12> ИО"},
                    {364, "I12> ИО Инв."},
                    {365, "I12>"},
                    {366, "I12> Инв."},
                    {367, "I13> ИО"},
                    {368, "I13> ИО Инв."},
                    {369, "I13>"},
                    {370, "I13> Инв."},
                    {371, "I14> ИО"},
                    {372, "I14> ИО Инв."},
                    {373, "I14>"},
                    {374, "I14> Инв."},
                    {375, "I15> ИО"},
                    {376, "I15> ИО Инв."},
                    {377, "I15>"},
                    {378, "I15> Инв."},
                    {379, "I16> ИО"},
                    {380, "I16> ИО Инв."},
                    {381, "I16>"},
                    {382, "I16> Инв."},
                    {383, "I17> ИО"},
                    {384, "I17> ИО Инв."},
                    {385, "I17>"},
                    {386, "I17> Инв."},
                    {387, "I18> ИО"},
                    {388, "I18> ИО Инв."},
                    {389, "I18>"},
                    {390, "I18> Инв."},
                    {391, "I19> ИО"},
                    {392, "I19> ИО Инв."},
                    {393, "I19>"},
                    {394, "I19> Инв."},
                    {395, "I20> ИО"},
                    {396, "I20> ИО Инв."},
                    {397, "I20>"},
                    {398, "I20> Инв."},
                    {399, "I21> ИО"},
                    {400, "I21> ИО Инв."},
                    {401, "I21>"},
                    {402, "I21> Инв."},
                    {403, "I22> ИО"},
                    {404, "I22> ИО Инв."},
                    {405, "I22>"},
                    {406, "I22> Инв."},
                    {407, "I23> ИО"},
                    {408, "I23> ИО Инв."},
                    {409, "I23>"},
                    {410, "I23> Инв."},
                    {411, "I24> ИО"},
                    {412, "I24> ИО Инв."},
                    {413, "I24>"},
                    {414, "I24> Инв."},
                    {415, "I25> ИО"},
                    {416, "I25> ИО Инв."},
                    {417, "I25>"},
                    {418, "I25> Инв."},
                    {419, "I26> ИО"},
                    {420, "I26> ИО Инв."},
                    {421, "I26>"},
                    {422, "I26> Инв."},
                    {423, "I27> ИО"},
                    {424, "I27> ИО Инв."},
                    {425, "I27>"},
                    {426, "I27> Инв."},
                    {427, "I28> ИО"},
                    {428, "I28> ИО Инв."},
                    {429, "I28>"},
                    {430, "I28> Инв."},
                    {431, "I29> ИО"},
                    {432, "I29> ИО Инв."},
                    {433, "I29>"},
                    {434, "I29> Инв."},
                    {435, "I30> ИО"},
                    {436, "I30> ИО Инв."},
                    {437, "I30>"},
                    {438, "I30> Инв."},
                    {439, "I31> ИО"},
                    {440, "I31> ИО Инв."},
                    {441, "I31>"},
                    {442, "I31> Инв."},
                    {443, "I32> ИО"},
                    {444, "I32> ИО Инв."},
                    {445, "I32>"},
                    {446, "I32> Инв."}
                };
                switch (ConfigurationStructBigV210.DeviceModelType)
                {
                    case "A1":
                        {
                            //дискреты
                            for (ushort i = 129; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 224; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A2":
                    case "A5":
                        {
                            //дискреты
                            for (ushort i = 81; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A3":
                    case "A6":
                        {
                            //дискреты
                            for (ushort i = 49; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    case "A4":
                    case "A7":
                        {
                            //дискреты
                            for (ushort i = 65; i < 193; i++)
                            {
                                ret.Remove(i);
                            }
                            //быстрые гусы
                            for (ushort i = 225; i < 257; i++)
                            {
                                ret.Remove(i);
                            }
                        }
                        break;
                    default:
                        //дискреты
                        for (ushort i = 49; i < 193; i++)
                        {
                            ret.Remove(i);
                        }
                        //быстрые гусы
                        for (ushort i = 225; i < 257; i++)
                        {
                            ret.Remove(i);
                        }
                        break;
                }
                return ret;
            }
        }
        #endregion
    }
}
