using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.MBServer;
using BEMN.MR901.Big.Configuration.Structures.Connections;
using BEMN.MR901.Big.Configuration.Structures.Ls;
using BEMN.MR901.Big.Configuration.Structures.Osc;
using BEMN.MR901.Big.Osc.Structures;
namespace BEMN.MR901.Big.Osc
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingListBig
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        private const int COUNTING_SIZE = 32;

        private const int CURRENTS_MAX = 24;
        public int CurrentsCount { get; private set; }
        public int DiscretsCount { get; }
        public int ChannelsCount { get; }

        public int VoltageCount { get; private set; } = 0;

        #endregion [Constants]

        #region [Private fields]

        private string _devicePlant;
        private OscJournalStructNew _oscJournalStruct;
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ �������
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;
        /// <summary>
        /// ������������
        /// </summary>
        private List<double> _factors;
        /// <summary>
        /// ���� ������������� �� �������������
        /// </summary>
        private double[][] _currents;
        private short[][] _baseCurrents;
        
        /// <summary>
        /// ���������� ������������� �� �������������
        /// </summary>
        private double[][] _voltages;
        private short[][] _baseVoltages;
        private string[] _uNames;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private List<double> _factorsV;
        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;

        private string _stage;
        private string _dateTime;

        private ChannelWithBase[] _channelsWithBase;

        private Mr901Device _device;
        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        private ushort[][] _channels;

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }
        
        #region [Ctor's]
        public CountingListBig(string devicePlant)
        {
            
            this.DiscretsCount = 16;
            this.ChannelsCount = 96;
            this.GetConst(devicePlant);
            
        }

        private void GetConst(string devicePlant)
        {
            this._devicePlant = devicePlant;
            switch (devicePlant)
            {
                case "A1":
                    this.CurrentsCount = 16;
                    break;
                case "A2":
                case "A3":
                case "A4":
                    this.CurrentsCount = 24;
                    break;
                case "A5":
                case "A6":
                case "A7":
                    this.VoltageCount = 4;
                    this.CurrentsCount = 20;
                    this._uNames = new[] { "Ua", "Ub", "Uc", "Un" };
                    break;
                default:
                    this.CurrentsCount = 16;
                    break;
            }
        }

        public CountingListBig(Mr901Device device, ushort[] pageValue, OscJournalStructNew oscJournalStruct, ConnectionsAndTransformer cnt, string devicePlant) : this(devicePlant)
        {
            _device = device;
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = this._oscJournalStruct.GetDate + " " + this._oscJournalStruct.GetTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];

            if (Strings.Version >= 2.13 ) _channelsWithBase = _device.AllChannels.Value.Channel.ForOsc;

            //����� ���������� ��������
            this._count = pageValue.Length / COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            try
            {
                foreach (ushort value in pageValue)
                {
                    this._countingArray[n][m] = value;
                    n++;
                    if (n == COUNTING_SIZE)
                    {
                        m++;
                        n = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            //��������
            List<ushort[]> discrets = new List<ushort[]>();
            discrets.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX]));
            this._discrets = discrets.ToArray();
            
            //������ 
            List<ushort[]> channels = new List<ushort[]>();
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX + 1]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX + 2]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX + 3]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX + 4]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX + 5]));
            channels.AddRange(this.DiscretArrayInit(this._countingArray[CURRENTS_MAX + 6]));
            this._channels = channels.ToArray();
            
            List<ushort> factorsBase = cnt.Connections.AllItt.Skip(1).ToList();
            this._factors = new List<double>();
            this._currents = new double[this.CurrentsCount][];
            this._baseCurrents = new short[this.CurrentsCount][];
            this._maxI = double.MinValue;
            this._minI = double.MaxValue;

            for (int i = 0; i < this.CurrentsCount; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)a).ToArray();
                this._factors.Add(Math.Sqrt(2) * factorsBase[i] * 40 / 32767);
                this._currents[i] = this._baseCurrents[i].Select(a => this._factors[i] * a).ToArray();
                this._maxI = Math.Max(this._maxI, this._currents[i].Max());
                this._minI = Math.Min(this._minI, this._currents[i].Min());
            }
            
            if (this.VoltageCount != 0)
            {
                this.SetCountingVoltage(cnt.TN);
            }
        }

        #endregion [Ctor's]

        #region [Help members]
        
        private void SetCountingVoltage(ParametersNTStruct measure)
        {
            this._voltages = new double[this.VoltageCount][];
            this._baseVoltages = new short[this.VoltageCount][];
            this._factorsV = new List<double>();
            this._maxU = double.MinValue;
            this._minU = double.MaxValue;
            for (int i = 0; i < this.VoltageCount; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + this.CurrentsCount].Select(a => (short)a).ToArray();

                double koef = i == this.VoltageCount - 1 ? measure.KthxValue : measure.KthlValue;

                this._factorsV.Add(koef * 2 * Math.Sqrt(2) / 256.0);
                this._voltages[i] = this._baseVoltages[i].Select(a => this._factorsV[i] * a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
        }

        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        #endregion [Help members]

        public void Save(string filePath, OscilloscopeSettingsBigStruct oscSettings)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("�� 901 {0} ������� - {1}", this._dateTime, this._stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine("DevicePlant = {0}", this._devicePlant);
                for (int i = 0; i < oscSettings.Kanal.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}", i + 1, oscSettings.Kanal[i]);
                }
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);
            }

            NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP901,1,1991");
                cgfFile.WriteLine("{0},{1}A,{2}D", this.CurrentsCount + this.VoltageCount + this.DiscretsCount + this.ChannelsCount,
                    this.CurrentsCount + this.VoltageCount, this.DiscretsCount + this.ChannelsCount);

                int index = 1;

                for (int i = 0; i < this.CurrentsCount; i++)
                {
                    cgfFile.WriteLine("{0},I{1},,,A,{2},0,0,-32768,32767,1,1,P", index, i + 1, this._factors[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.VoltageCount; i++)
                {
                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, this._uNames[i], this._factorsV[i].ToString(format));
                    index++;
                }
                for (int i = 0; i < this._discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }

                if (Strings.Version >= 2.13)
                {
                    for (int i = 0; i < this._channels.Length; i++)
                    {

                        if (i < 32)
                        {
                            cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, this._channelsWithBase[i].ChannelStr);
                            index++;
                        }
                        else
                        {
                            cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, Strings.RelaySignalsBig[oscSettings.Kanal[i]]);
                            index++;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < this._channels.Length; i++)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, Strings.RelaySignalsBig[oscSettings.Kanal[i]]);
                        index++;
                    }
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeStartOsc(this._alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    for (int j = 0; j < this.VoltageCount; j++)
                    {
                        datFile.Write(",{0}", this._baseVoltages[j][i]);
                    }
                    
                    foreach (ushort[] discret in this._discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this._channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public CountingListBig Load(string filePath, out OscilloscopeSettingsBigStruct oscSettings)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);

            string devicePlant = hdrStrings[3].Replace("DevicePlant = ", string.Empty);
            CountingListBig result = new CountingListBig(devicePlant);
            this.VoltageCount = result.VoltageCount;
            this.CurrentsCount = result.CurrentsCount;

            string[] buff = hdrStrings[0].Split(' ');
            result._dateTime = $"{buff[2]} {buff[3]}";
            result._alarm = int.Parse(hdrStrings[2].Replace("Alarm = ", string.Empty));

            oscSettings = new OscilloscopeSettingsBigStruct();
            oscSettings.Kanal = new ushort[this.ChannelsCount];
            for (int i = 0; i < this.ChannelsCount; i++)
            {
                oscSettings.Kanal[i] = ushort.Parse(hdrStrings[4 + i].Replace(string.Format("K{0} = ", i + 1), string.Empty));
            }
            result._stage = hdrStrings[3 + this.ChannelsCount];

            double[] factors = new double[this.CurrentsCount];
            double[] factorsV = new double[this.VoltageCount];

            Regex factorRegex = new Regex(@"\d+\,[IU]?(\d+)?(\w+)\,\,\,[AV]\,(?<value>[0-9\.]+)");
            NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

            for (int i = 2; i < 2 + this.CurrentsCount; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                factors[i - 2] = double.Parse(res, format);
            }

            for (int i = 2 + this.CurrentsCount; i < 2 + this.CurrentsCount + this.VoltageCount; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                factorsV[i - (2 + this.CurrentsCount)] = double.Parse(res, format);
            }

            result._count = int.Parse(cfgStrings[2 + CURRENTS_MAX + this.DiscretsCount + this.ChannelsCount + 2].Replace("1000,", string.Empty));

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);
            double[][] currents = new double[this.CurrentsCount][];
            double[][] voltages = new double[this.VoltageCount][];
            ushort[][] discrets = new ushort[this.DiscretsCount][];
            ushort[][] channels = new ushort[this.ChannelsCount][];

            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[result._count];
            }
            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[result._count];
            }
            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[result._count];
            }
            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[result._count];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] line = datStrings[i].Split(',');
                int index = 2;

                for (int j = 0; j < this.CurrentsCount; j++)
                {
                    currents[j][i] = double.Parse(line[index]) * factors[j];
                    index++;
                }

                for (int j = 0; j < this.VoltageCount; j++)
                {
                    voltages[j][i] = double.Parse(line[index]) * factorsV[j];
                    index++;
                }

                for (int j = 0; j < this.DiscretsCount; j++)
                {
                    discrets[j][i] = ushort.Parse(line[index]);
                    index++;
                }

                for (int j = 0; j < this.ChannelsCount; j++)
                {
                    channels[j][i] = ushort.Parse(line[index]);
                    index++;
                }
            }
            result._maxI = double.MinValue;
            result._minI = double.MaxValue;
            for (int i = 0; i < this.CurrentsCount; i++)
            {
                result._maxI = Math.Max(result._maxI, currents[i].Max());
                result._minI = Math.Min(result._minI, currents[i].Min());
            }
            result._maxU = double.MinValue;
            result._minU = double.MaxValue;
            for (int i = 0; i < this.VoltageCount; i++)
            {
                result._maxU = Math.Max(result._maxU, voltages[i].Max());
                result._minU = Math.Min(result._minU, voltages[i].Min());
            }
            result._currents = currents;
            result._channels = channels;
            result._discrets = discrets;
            result._factors = new List<double>(factors);
            result._factorsV = new List<double>(factorsV);
            result.FilePath = filePath;
            result.IsLoad = true;
            return result;
        }
    }
}
