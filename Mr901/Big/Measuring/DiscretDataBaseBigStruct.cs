﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Measuring
{
    public class DiscretDataBaseBigStruct : StructBase
    {
        private const int BASE = 32;
        private const int ALARM = 16;
        private const int PARAMS = 16;

        [Layout(0, Count = BASE)] private ushort[] _base;
        [Layout(1, Count = ALARM)] private ushort[] _alarm;
        [Layout(2, Count = PARAMS)] private ushort[] _params;

        public bool[] Discrets
        {
            get { return Common.GetBitsArray(this._base, 0, 95); }
        }

        public bool[] LogicSignals
        {
            get { return Common.GetBitsArray(this._base, 96, 111); }
        }

        public bool[] ExtLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 128, 143); }
        }

        public bool[] DiffCurrents
        {
            get { return Common.GetBitsArray(this._base, 144, 158); }
        }

        public bool[] Currents
        {
            get { return Common.GetBitsArray(this._base, 159, 222); }
        }

        public bool[] ExternalDefenses
        {
            get { return Common.GetBitsArray(this._base, 223, 246); }
        }

        public bool[] Ssl
        {
            get { return Common.GetBitsArray(this._base, 247, 294); }
        }

        public bool[] Urov
        {
            get { return Common.GetBitsArray(this._base, 295, 321); }
        }

        public bool[] State
        {
            get { return Common.GetBitsArray(this._base, 349, 353); }
        }

        public bool[] Relays
        {
            get { return Common.GetBitsArray(this._base, 357, 462); }
        }
        public bool[] Indicators
        {
            get { return Common.GetBitsArray(this._base, 463, 486); }
        }

        public bool[] Control
        {
            get { return Common.GetBitsArray(this._base, 489, 496); }
        }

        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults
        {
            get
            {
                return new[]
                    {
                        Common.GetBit(this._alarm[0], 0), //аппаратная
                        Common.GetBit(this._alarm[0], 1), //программная
                        Common.GetBit(this._alarm[0], 2), //измерения ТТ
                        Common.GetBit(this._alarm[0], 3), //уров
                        Common.GetBit(this._alarm[0], 4), //модуль 1
                        Common.GetBit(this._alarm[0], 5), //модуль 2
                        Common.GetBit(this._alarm[0], 6), //модуль 3
                        Common.GetBit(this._alarm[0], 7), //модуль 4

                        Common.GetBit(this._alarm[0], 8), //модуль 5
                        Common.GetBit(this._alarm[0], 9), //модуль 6
                        Common.GetBit(this._alarm[0], 10), //уставок
                        Common.GetBit(this._alarm[0], 11), //группы уставок
                        Common.GetBit(this._alarm[0], 13), //журнала системы
                        Common.GetBit(this._alarm[0], 14), //журнала аварий
                        Common.GetBit(this._alarm[0], 15), //осциллографа

                        this.FaultLogic,
                        Common.GetBit(this._alarm[1], 5), //неиспр. СШ1
                        Common.GetBit(this._alarm[1], 6), //неиспр. СШ2
                        Common.GetBit(this._alarm[1], 7)  //неиспр. ПО
                    };
            }
        }
        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool FaultLogic
        {
            get
            {
                return Common.GetBit(this._alarm[1], 0) | //ошибка CRC констант программы логики
                       Common.GetBit(this._alarm[1], 1) | //ошибка CRC разрешения программы логики
                       Common.GetBit(this._alarm[1], 2) | //ошибка CRC программы логики
                       Common.GetBit(this._alarm[1], 3) | //ошибка CRC меню логики
                       Common.GetBit(this._alarm[1], 4); //ошибка в ходе выполнения программы логики
            }
        }

        public bool[] NeisprTN
        {
            get { return Common.GetBitsArray(this._alarm, 27, 32); }
        }
    }
}
