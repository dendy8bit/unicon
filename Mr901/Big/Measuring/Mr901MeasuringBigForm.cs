﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901.Big.Configuration.Structures;
using BEMN.MR901.Big.Configuration.Structures.Connections;

namespace BEMN.MR901.Big.Measuring
{
    public partial class Mr901MeasuringBigForm : Form, IFormView
    {
        #region [Constants]
        private const string RESET_INDICATION = "Сброс индикации";
        private const string RESET_FAULT_SYSTEM_JOURNAL = "Сбросить наличие неисправности по ЖС";
        private const string RESET_OSC_JOURNAL = "Сбросить журнал осциллографа";
        private const string RESET_ALARM_JOURNAL = "Сбросить журнал аварий";
        private const string RESET_SYSTEM_JOURNAL = "Сбросить журнал системы";
        private const string SET_MAIN_GROUP = "Переключить на основную группу";
        private const string SET_RESERVED_GROUP = "Переключить на резервную группу";
        private const string RESET_TT = "Сбросить неисправности ТТ";
        private const string START_OSC = "Запустить осциллограф";
        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<AnalogDataBaseBigStruct> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseBigStruct> _discretDataBase;
        private readonly MemoryEntity<ConnectionsAndTransformer> _connections;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly AveragerTime<AnalogDataBaseBigStruct> _averagerTime;
        private readonly Mr901Device _device;
        private int[] _factors;
        private string[] _bindings;
        private double _kthl;
        private double _kthx;
        /// <summary>
        /// Аналоговые токи
        /// </summary>
        private TextBox[] _analogCurrentBoxes;

        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Дествующий ток
        /// </summary>
        private LedControl[] _operatingCurrent;

        /// <summary>
        /// Максимальный ток
        /// </summary>
        private LedControl[] _maximumCurrent;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;
        /// <summary>
        /// Защиты U
        /// </summary>
        private LedControl[] _voltageDiscrets;

        private List<bool> _voltageBoolList; 
        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// УРОВ
        /// </summary>
        private LedControl[] _urov;

        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _state;
        
        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faults;

        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;

        private LedControl[] _neisprTNLeds;

        #endregion [Private fields]


        #region [Ctor's]

        public Mr901MeasuringBigForm()
        {
            this.InitializeComponent();
        }
        
        public Mr901MeasuringBigForm(Mr901Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;

            this._averagerTime = new AveragerTime<AnalogDataBaseBigStruct>(this, 500);
            this._averagerTime.Tick += this.AveragerTimeTick;

            this._analogDataBase = new MemoryEntity<AnalogDataBaseBigStruct>("Аналоговая БД", device, 0x0E00);
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._discretDataBase = new MemoryEntity<DiscretDataBaseBigStruct>("Дискретная БД", device, 0x0D00);
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);

            this._connections = new MemoryEntity<ConnectionsAndTransformer>("Присоединения (big)", device, 0x106A);
            this._connections.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                Strings.Version = Common.VersionConverter(this._device.DeviceVersion);
                ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
                this._factors = this._connections.Value.Connections.AllIttJoin.Select(val => val*40).ToArray();

                if (voltageGroup.Visible)
                {
                    this._kthl = this._connections.Value.TN.KthlValue;
                    this._kthx = this._connections.Value.TN.KthxValue;
                }
                this._analogDataBase.LoadStructCycle();
            });

            this._dateTime = new MemoryEntity<DateTimeStruct>("Дата/время", device, 0x200);
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);
            
            device.ConfigurationBigV210.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._analogDataBase.RemoveStructQueries();
                this._connections.LoadStruct();
            });

            this.Init();
            
        }

        private void Init()
        {
            this._freeLogic = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32,
                this._ssl33, this._ssl34, this._ssl35, this._ssl36, this._ssl37, this._ssl38, this._ssl39, this._ssl40,
                this._ssl41, this._ssl42, this._ssl43, this._ssl44, this._ssl45, this._ssl46, this._ssl47, this._ssl48
            };

            this._maximumCurrent = new[]
            {
                this._i1Io, this._i1, this._i2Io, this._i2, this._i3Io, this._i3, this._i4Io, this._i4, this._i5Io,
                this._i5,
                this._i6Io, this._i6, this._i7Io, this._i7, this._i8Io, this._i8, this._i9Io, this._i9, this._i10Io,
                this._i10,
                this._i11Io, this._i11, this._i12Io, this._i12, this._i13Io, this._i13, this._i14Io, this._i14,
                this._i15Io,
                this._i15, this._i16Io, this._i16, this._i17Io, this._i17, this._i18Io, this._i18, this._i19Io,
                this._i19,
                this._i20Io, this._i20, this._i21Io, this._i21, this._i22Io, this._i22, this._i23Io, this._i23,
                this._i24Io,
                this._i24, this._i25Io, this._i25, this._i26Io, this._i26, this._i27Io, this._i27, this._i28Io,
                this._i28,
                this._i29Io, this._i29, this._i30Io, this._i30, this._i31Io, this._i31, this._i32Io, this._i32
            };

            this._operatingCurrent = new[]
            {
                this._difDefenceInstantaneousChtoSh1, this._difDefenceInstantaneousSrabSh1,
                this._difDefenceInstantaneousChtoSh2,
                this._difDefenceInstantaneousSrabSh2, this._difDefenceInstantaneousChtoPo,
                this._difDefenceInstantaneousSrabPo,
                this._difDefenceActualIoSh1, this._difDefenceActualChtoSh1, this._difDefenceActualSrabSh1,
                this._difDefenceActualIoSh2, this._difDefenceActualChtoSh2, this._difDefenceActualSrabSh2,
                this._difDefenceActualIoPo, this._difDefenceActualChtoPo, this._difDefenceActualSrabPo
            };
            this._urov = new[]
            {
                this._urovSH1, this._urovSH2, this._urovPO,
                this._urovPr1, this._urovPr2, this._urovPr3, this._urovPr4, this._urovPr5, this._urovPr6,
                this._urovPr7, this._urovPr8, this._urovPr9, this._urovPr10, this._urovPr11, this._urovPr12,
                this._urovPr13, this._urovPr14, this._urovPr15, this._urovPr16, this._urovPr17, this._urovPr18,
                this._urovPr19,this._urovPr20,this._urovPr21,this._urovPr22,this._urovPr23,this._urovPr24
            };

            this._state = new[]
            {
                this._fault,this._mainGroupOfSetpoints,this._reservedGroupOfSetpoints,this._alarm,this._signalization
            };

            this._indicators = new[]
            {
                this._indicator1, this._indicator2, this._indicator3, this._indicator4, this._indicator5,
                this._indicator6,this._indicator7, this._indicator8, this._indicator9, this._indicator10,
                this._indicator11, this._indicator12
            };

            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16,
                this._vz17, this._vz18, this._vz19, this._vz20, this._vz21, this._vz22, this._vz23, this._vz24
            };

            this._voltageDiscrets = new[]
            {
                this._u1bIO, this._u1b, this._u2bIO, this._u2b, this._u1lIO, this._u1l, this._u2lIO, this._u2l
            };

            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };
            this._analogCurrentBoxes = new[]
            {
                this._i1TextBox, this._i2TextBox, this._i3TextBox, this._i4TextBox, this._i5TextBox, this._i6TextBox,
                this._i7TextBox, this._i8TextBox, this._i9TextBox, this._i10TextBox, this._i11TextBox, this._i12TextBox,
                this._i13TextBox, this._i14TextBox, this._i15TextBox, this._i16TextBox, this._i17TextBox, this._i18TextBox,
                this._i19TextBox, this._i20TextBox, this._i21TextBox,this._i22TextBox, this._i23TextBox, this._i24TextBox,

                this._id1TextBox,this._id2TextBox,this._id3TextBox, this._it1TextBox, this._it2TextBox, this._it3TextBox
            };

            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal, this._availabilityFaultSystemJournal
            };

            this._faults = new[]
            {
                this._faultHardware, this._faultSoftware, this._faultMeasuring, this._faultUrov,
                this._faultModule1, this._faultModule2, this._faultModule3, this._faultModule4, this._faultModule5,
                this._faultModule6,this._faultSetpoints, this._faultGroupsOfSetpoints, this._faultSystemJournal,
                this._faultAlarmJournal, this._faultOsc, this._faultLogic, this._faultTt1, this._faultTt2, this._faultTt3
            };

            this.CreateDeviceType();
        }

        private void CreateDeviceType()
        {
            Label[] currentsLabel =
            {
                this._i17Label, this._i18Label, this._i19Label, this._i20Label, this._i21Label, this._i22Label, this._i23Label, this._i24Label
            };

            TextBox[] currents =
            {
                this._i17TextBox, this._i18TextBox, this._i19TextBox, this._i20TextBox, this._i21TextBox,
                this._i22TextBox, this._i23TextBox, this._i24TextBox
            };

            Label[] urovLabels =
            {
                this._urovPrLabel17, this._urovPrLabel18, this._urovPrLabel19, this._urovPrLabel20, this._urovPrLabel21,
                this._urovPrLabel22, this._urovPrLabel23, this._urovPrLabel24
            };

            LedControl[] urov =
            {
                this._urovPr17, this._urovPr18, this._urovPr19, this._urovPr20, this._urovPr21, this._urovPr22,
                this._urovPr23, this._urovPr24
            };

            switch (this._device.DevicePlant)
            {
                case "A1":
                    foreach (TextBox box in currents)
                    {
                        box.Visible = false;
                    }
                    foreach (Label label in currentsLabel)
                    {
                        label.Visible = false;
                    }
                    foreach (Label urovLabel in urovLabels)
                    {
                        urovLabel.Visible = false;
                    }
                    foreach (LedControl control in urov)
                    {
                        control.Visible = false;
                    }
                    this.connectionCurrentsGB.Size = new Size(230, 239);
                    this.CreateModules(this.relayGroupBox, new [] { 10, 16, 16}, "Р", out this._relays);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16, 16, 16 }, "Д", out this._discretInputs);
                    break;
                case "A2":
                    this.CreateModules(this.relayGroupBox, new[] { 10, 8, 16 }, "Р", out this._relays);
                    this.CreateModules(this.discretsGroupBox, new[] {8, 16, 16 }, "Д", out this._discretInputs);
                    break;
                case "A3":
                    this.CreateModules(this.relayGroupBox, new[] { 10, 8, 16, 16 }, "Р", out this._relays);
                    this.CreateModules(this.discretsGroupBox, new[] { 8, 16 }, "Д", out this._discretInputs);
                    break;
                case "A4":
                    this.CreateModules(this.relayGroupBox, new[] { 10, 16, 16 }, "Р", out this._relays);
                    this.CreateModules(this.discretsGroupBox, new[] { 16, 16 }, "Д", out this._discretInputs);

                    break;
                case "A5":
                    this.CreateModules(this.relayGroupBox, new[] { 10, 8, 16 }, "Р", out this._relays);
                    this.CreateModules(this.discretsGroupBox, new[] { 8, 16, 16 }, "Д", out this._discretInputs);
                    this.voltageGroup.Visible = true;
                    this.voltageDiscretsGroup.Visible = true;
                    this._voltageBoolList = new List<bool>();
                    for (int i = 4; i < currents.Length; i++)
                    {
                        currents[i].Visible = false;
                        currentsLabel[i].Visible = false;
                    }

                    this._vz24label.Visible = this._vz23label.Visible = this._vz22label.Visible = this._vz21label.Visible = false;
                    this._vz21.Visible = this._vz22.Visible = this._vz23.Visible = this._vz24.Visible = false;

                    this._urovPr21.Visible = this._urovPr22.Visible = this._urovPr23.Visible = this._urovPr24.Visible = false;
                    this._urovPrLabel21.Visible = this._urovPrLabel22.Visible = this._urovPrLabel23.Visible = this._urovPrLabel24.Visible = false;

                    this._neisprTNGroup.Visible = true;
                    this._neisprTN.Visible = true;
                    this._neisprTNLabel.Visible = true;
                    this._neisprTNLeds = new[]
                    {
                        this._neisprTN, this._neisprTN3U0, this._neisprTNns, this._neisprTNUabc, new LedControl(),  this._neisprTNUn
                    };

                    break;
                default:
                    foreach (TextBox box in currents)
                    {
                        box.Visible = false;
                    }
                    foreach (Label label in currentsLabel)
                    {
                        label.Visible = false;
                    }
                    foreach (Label urovLabel in urovLabels)
                    {
                        urovLabel.Visible = false;
                    }
                    foreach (LedControl control in urov)
                    {
                        control.Visible = false;
                    }
                    this.connectionCurrentsGB.Size = new Size(230, 239);
                    this.CreateModules(this.relayGroupBox, new[] { 10, 8 }, "Р", out this._relays);
                    this.CreateModules(this.discretsGroupBox, new[] { 8, 16 }, "Д", out this._discretInputs);
                    break;
            }
        }

        private void CreateModules(GroupBox mainContainer, int[] counts, string character, out LedControl[] mass)
        {
            List<LedControl> list = new List<LedControl>();
            int counter = 1;
            for (int module = 0; module < counts.Length; module++)
            {
                GroupBox moduleGroup = new GroupBox
                {
                    Size = new Size(68, 262),
                    Location = new Point(6 + 74*module, 19),
                    Text = string.Empty
                };
                for (int relay = 0; relay < counts[module]; relay++)
                {
                    LedControl relayLed = new LedControl { Location = new Point(6, 19 + 15 * relay) };
                    list.Add(relayLed);
                    Label label = new Label { Location = new Point(25, 19 + 15 * relay), Text = character + counter++, AutoSize = true};
                    moduleGroup.Controls.Add(relayLed);
                    moduleGroup.Controls.Add(label);
                }
                mainContainer.Controls.Add(moduleGroup);
            }
            mass = list.ToArray();
        }

        private void AveragerTimeTick()
        {
            this._id1TextBox.Text = this._analogDataBase.Value.GetIda1(this._averagerTime.ValueList, this._factors);
            this._it1TextBox.Text = this._analogDataBase.Value.GetIba1(this._averagerTime.ValueList, this._factors);
            this._id2TextBox.Text = this._analogDataBase.Value.GetIda2(this._averagerTime.ValueList, this._factors);
            this._it2TextBox.Text = this._analogDataBase.Value.GetIba2(this._averagerTime.ValueList, this._factors);
            this._id3TextBox.Text = this._analogDataBase.Value.GetIda3(this._averagerTime.ValueList, this._factors);
            this._it3TextBox.Text = this._analogDataBase.Value.GetIba3(this._averagerTime.ValueList, this._factors);
            this._i1TextBox.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._factors);
            this._i2TextBox.Text = this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._factors);
            this._i3TextBox.Text = this._analogDataBase.Value.GetI3(this._averagerTime.ValueList, this._factors);
            this._i4TextBox.Text = this._analogDataBase.Value.GetI4(this._averagerTime.ValueList, this._factors);
            this._i5TextBox.Text = this._analogDataBase.Value.GetI5(this._averagerTime.ValueList, this._factors);
            this._i6TextBox.Text = this._analogDataBase.Value.GetI6(this._averagerTime.ValueList, this._factors);
            this._i7TextBox.Text = this._analogDataBase.Value.GetI7(this._averagerTime.ValueList, this._factors);
            this._i8TextBox.Text = this._analogDataBase.Value.GetI8(this._averagerTime.ValueList, this._factors);
            this._i9TextBox.Text = this._analogDataBase.Value.GetI9(this._averagerTime.ValueList, this._factors);
            this._i10TextBox.Text = this._analogDataBase.Value.GetI10(this._averagerTime.ValueList, this._factors);
            this._i11TextBox.Text = this._analogDataBase.Value.GetI11(this._averagerTime.ValueList, this._factors);
            this._i12TextBox.Text = this._analogDataBase.Value.GetI12(this._averagerTime.ValueList, this._factors);
            this._i13TextBox.Text = this._analogDataBase.Value.GetI13(this._averagerTime.ValueList, this._factors);
            this._i14TextBox.Text = this._analogDataBase.Value.GetI14(this._averagerTime.ValueList, this._factors);
            this._i15TextBox.Text = this._analogDataBase.Value.GetI15(this._averagerTime.ValueList, this._factors);
            this._i16TextBox.Text = this._analogDataBase.Value.GetI16(this._averagerTime.ValueList, this._factors);
            if (this._i17TextBox.Visible)
            {
                this._i17TextBox.Text = this._analogDataBase.Value.GetI17(this._averagerTime.ValueList, this._factors);
                this._i18TextBox.Text = this._analogDataBase.Value.GetI18(this._averagerTime.ValueList, this._factors);
                this._i19TextBox.Text = this._analogDataBase.Value.GetI19(this._averagerTime.ValueList, this._factors);
                this._i20TextBox.Text = this._analogDataBase.Value.GetI20(this._averagerTime.ValueList, this._factors);
                if (this._i21TextBox.Visible) // конфигурация А5-А7
                {
                    this._i21TextBox.Text = this._analogDataBase.Value.GetI21(this._averagerTime.ValueList, this._factors);
                    this._i22TextBox.Text = this._analogDataBase.Value.GetI22(this._averagerTime.ValueList, this._factors);
                    this._i23TextBox.Text = this._analogDataBase.Value.GetI23(this._averagerTime.ValueList, this._factors);
                    this._i24TextBox.Text = this._analogDataBase.Value.GetI24(this._averagerTime.ValueList, this._factors);
                }
            }
            if (this.voltageGroup.Visible)
            {
                this._ua.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, this._kthl);
                this._ub.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, this._kthl);
                this._uc.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, this._kthl);
                this._un.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, this._kthx);
                this._uab.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._kthl);
                this._ubc.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._kthl);
                this._uca.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._kthl);
                this._u1.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._kthl);
                this._u2.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._kthl);
                this._u30.Text = this._analogDataBase.Value.Get3U0(this._averagerTime.ValueList, this._kthl);
            }
        }

        #endregion [Ctor's]


        #region [Help members]

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);
            LedManager.TurnOffLeds(this._operatingCurrent);
            LedManager.TurnOffLeds(this._maximumCurrent);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._urov);
            LedManager.TurnOffLeds(this._state);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._indicators);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._faults);
            LedManager.TurnOffLeds(this._voltageDiscrets);
            if (this._neisprTNLeds != null) LedManager.TurnOffLeds(_neisprTNLeds);
            this._logicState.State = LedState.Off;
            this._mainGroup.State = LedState.Off;
            this._reservedGroup.State = LedState.Off;
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, this._discretDataBase.Value.Discrets);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, this._discretDataBase.Value.LogicSignals);
            //Выходные ЛС
            LedManager.SetLeds(this._outputLogicSignals, this._discretDataBase.Value.ExtLogicSignals);
            //Дествующий ток
            LedManager.SetLeds(this._operatingCurrent, this._discretDataBase.Value.DiffCurrents);
            //Максимальный ток
            LedManager.SetLeds(this._maximumCurrent, this._discretDataBase.Value.Currents);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, this._discretDataBase.Value.ExternalDefenses);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, this._discretDataBase.Value.Ssl);
            //УРОВ
            LedManager.SetLeds(this._urov, this._discretDataBase.Value.Urov);
            //Состояния
            LedManager.SetLeds(this._state, this._discretDataBase.Value.State);
            this._mainGroup.State = this._mainGroupOfSetpoints.State;
            this._reservedGroup.State = this._reservedGroupOfSetpoints.State;
            //Реле
            LedManager.SetLeds(this._relays, this._discretDataBase.Value.Relays);
            //Индикаторы
            LedManager.SetLeds(this._indicators, this._discretDataBase.Value.Indicators);
            //Индикаторы
            LedManager.SetLeds(this._controlSignals, this._discretDataBase.Value.Control);
            bool res = this._discretDataBase.Value.Control[7] && !this._discretDataBase.Value.FaultLogic;
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;
            //Неисправности
            LedManager.SetLeds(this._faults, this._discretDataBase.Value.Faults);
            if (this._voltageBoolList != null)
            {
                this._voltageBoolList.Clear();
                this._voltageBoolList.AddRange(this._discretDataBase.Value.ExternalDefenses.Skip(20));
                this._voltageBoolList.AddRange(this._discretDataBase.Value.Urov.Skip(23));
                LedManager.SetLeds(this._voltageDiscrets, this._voltageBoolList.ToArray());
            }
            if (this._neisprTNGroup.Visible)
            {
                LedManager.SetLeds(this._neisprTNLeds, this._discretDataBase.Value.NeisprTN);
            }
        }

        /// <summary>
        /// Ошибка чтения аналоговой базы данных
        /// </summary>
        private void AnalogBdReadFail()
        {
            foreach (TextBox t in this._analogCurrentBoxes)
            {
                t.Text = string.Empty;
            }
        }

        /// <summary>
        /// Прочитана аналоговая база данных
        /// </summary>
        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }
        
        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._connections.LoadStruct();
                this._discretDataBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._analogDataBase.RemoveStructQueries();
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        #endregion [Help members]


        #region [Events Handlers]
        private void Mr901MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void Mr901MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._analogDataBase.RemoveStructQueries();
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void SetBitByAdress(ushort adress, string requestName)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(string.Format("{0}?", requestName), string.Empty, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                this._device.SetBit(this._device.DeviceNumber, adress, true, requestName, this);
            }   
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D01, RESET_SYSTEM_JOURNAL);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D02, RESET_ALARM_JOURNAL);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D03, RESET_OSC_JOURNAL);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D04, RESET_FAULT_SYSTEM_JOURNAL);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D05, RESET_INDICATION);
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D06, SET_MAIN_GROUP);
        }

        private void _reserveGroupButton_Click(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D07, SET_RESERVED_GROUP);
        }

        private void ResetTtClick(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D09, RESET_TT);
        }
        
        private void StartOscClick(object sender, EventArgs e)
        {
            this.SetBitByAdress(0x0D0A, START_OSC);
        }
        
        private void dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }


        private void startLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D08, true, "Запуск СПЛ", this._device);
        }

        private void stopLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0C, true, "Останов СПЛ", this._device);
        }
        #endregion [Events Handlers]

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr901Device); }
        }

        public Type ClassType
        {
            get { return typeof(Mr901MeasuringBigForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool Multishow { get; private set; }

        #endregion [INodeView Members]
    }
}
