﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR901.Big.Measuring
{
    /// <summary>
    /// МР 901 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseBigStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _i1; // ток 1
        [Layout(1)] private ushort _i2; // ток 2
        [Layout(2)] private ushort _i3; // ток 3
        [Layout(3)] private ushort _i4; // ток 4
        [Layout(4)] private ushort _i5; // ток 5
        [Layout(5)] private ushort _i6; // ток 6
        [Layout(6)] private ushort _i7; // ток 7
        [Layout(7)] private ushort _i8; // ток 8
        [Layout(8)] private ushort _i9; // ток 9
        [Layout(9)] private ushort _i10; // ток 10
        [Layout(10)] private ushort _i11; // ток 11
        [Layout(11)] private ushort _i12; // ток 12
        [Layout(12)] private ushort _i13; // ток 13
        [Layout(13)] private ushort _i14; // ток 14
        [Layout(14)] private ushort _i15; // ток 15
        [Layout(15)] private ushort _i16; // ток 16
        [Layout(16)] private ushort _i17; // ток 9
        [Layout(17)] private ushort _i18; // ток 10
        [Layout(18)] private ushort _i19; // ток 11
        [Layout(19)] private ushort _i20; // ток 12
        [Layout(20)] private ushort _i21; // ток 13
        [Layout(21)] private ushort _i22; // ток 14
        [Layout(22)] private ushort _i23; // ток 15
        [Layout(23)] private ushort _i24; // ток 16
        //ток
        [Layout(24)] private ushort _ida1; // ток СШ1
        [Layout(25)] private ushort _ida2; // ток СШ2
        [Layout(26)] private ushort _ida3; // ток ПО
        //ток
        [Layout(27)] private ushort _iba1; // ток СШ1
        [Layout(28)] private ushort _iba2; // ток СШ2
        [Layout(29)] private ushort _iba3; // ток ПО
        //ток
        [Layout(30)] private ushort _idb1; // ток СШ1
        [Layout(31)] private ushort _idb2; // ток СШ2
        [Layout(32)] private ushort _idb3; // ток ПО
        //ток
        [Layout(33)] private ushort _ibb1; // ток СШ1
        [Layout(34)] private ushort _ibb2; // ток СШ2
        [Layout(35)] private ushort _ibb3; // ток ПО
        //ток
        [Layout(36)] private ushort _ibc1; // ток СШ1
        [Layout(37)] private ushort _ibc2; // ток СШ2
        [Layout(38)] private ushort _ibc3; // ток ПО
        //res
        [Layout(39)] private ushort _res1; // ток СШ1
        [Layout(40)] private ushort _res2; // ток СШ2
        [Layout(41)] private ushort _res3; // ток ПО
        //напряжения
        [Layout(42)] private ushort _ua;
        [Layout(43)] private ushort _ub;
        [Layout(44)] private ushort _uc;
        [Layout(45)] private ushort _un;
        [Layout(46)] private ushort _uab;
        [Layout(47)] private ushort _ubc;
        [Layout(48)] private ushort _uca;
        [Layout(49)] private ushort _u0;
        [Layout(50)] private ushort _u1;
        [Layout(51)] private ushort _u2;
        [Layout(52)] private ushort _u30;
        [Layout(53)] private ushort _res4;
        #endregion [Private fields]


        #region [Properties]
        public string GetI1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetI2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetI3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i3);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetI4(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i4);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetI5(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i5);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetI6(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i6);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }

        public string GetI7(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i7);
            return ValuesConverterCommon.Analog.GetI(value, factors[7]);
        }

        public string GetI8(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i8);
            return ValuesConverterCommon.Analog.GetI(value, factors[8]);
        }

        public string GetI9(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i9);
            return ValuesConverterCommon.Analog.GetI(value, factors[9]);
        }

        public string GetI10(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i10);
            return ValuesConverterCommon.Analog.GetI(value, factors[10]);
        }

        public string GetI11(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i11);
            return ValuesConverterCommon.Analog.GetI(value, factors[11]);
        }

        public string GetI12(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i12);
            return ValuesConverterCommon.Analog.GetI(value, factors[12]);
        }

        public string GetI13(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i13);
            return ValuesConverterCommon.Analog.GetI(value, factors[13]);
        }

        public string GetI14(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i14);
            return ValuesConverterCommon.Analog.GetI(value, factors[14]);
        }

        public string GetI15(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i15);
            return ValuesConverterCommon.Analog.GetI(value, factors[15]);
        }

        public string GetI16(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i16);
            return ValuesConverterCommon.Analog.GetI(value, factors[16]);
        }

        public string GetI17(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i17);
            return ValuesConverterCommon.Analog.GetI(value, factors[17]);
        }

        public string GetI18(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i18);
            return ValuesConverterCommon.Analog.GetI(value, factors[18]);
        }

        public string GetI19(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i19);
            return ValuesConverterCommon.Analog.GetI(value, factors[19]);
        }

        public string GetI20(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i20);
            return ValuesConverterCommon.Analog.GetI(value, factors[20]);
        }

        public string GetI21(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i21);
            return ValuesConverterCommon.Analog.GetI(value, factors[21]);
        }

        public string GetI22(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i22);
            return ValuesConverterCommon.Analog.GetI(value, factors[22]);
        }

        public string GetI23(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i23);
            return ValuesConverterCommon.Analog.GetI(value, factors[23]);
        }

        public string GetI24(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i24);
            return ValuesConverterCommon.Analog.GetI(value, factors[24]);
        }

        public string GetIda1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIba1(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._iba1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIba2(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._iba2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIba3(List<AnalogDataBaseBigStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._iba3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        private ushort GetMean(List<AnalogDataBaseBigStruct> list, Func<AnalogDataBaseBigStruct, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        public string GetUa(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetUb(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetUc(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetUab(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetUbc(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetUca(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetU1(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetU2(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string Get3U0(List<AnalogDataBaseBigStruct> list, double kthl)
        {
            var value = this.GetMean(list, o => o._u30);
            return ValuesConverterCommon.Analog.GetU(value, kthl);
        }

        public string GetUn(List<AnalogDataBaseBigStruct> list, double kthx)
        {
            var value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU(value, kthx);
        }
        #endregion [Properties]
    }
}
