﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901.Big.Configuration.Structures;
using BEMN.MR901.Big.Configuration.Structures.ConfigSystem;
using BEMN.MR901.Big.Configuration.Structures.Connections;
using BEMN.MR901.Big.Configuration.Structures.Defenses;
using BEMN.MR901.Big.Configuration.Structures.Defenses.Differential;
using BEMN.MR901.Big.Configuration.Structures.Defenses.External;
using BEMN.MR901.Big.Configuration.Structures.Defenses.Mtz;
using BEMN.MR901.Big.Configuration.Structures.Defenses.U;
using BEMN.MR901.Big.Configuration.Structures.Ls;
using BEMN.MR901.Big.Configuration.Structures.Osc;
using BEMN.MR901.Big.Configuration.Structures.RelayIndicator;
using BEMN.MR901.Big.Configuration.Structures.Tt;
using BEMN.MR901.Big.Configuration.Structures.Urov;
using BEMN.MR901.Big.Configuration.Structures.Vls;
using BEMN.MR901.Big.Configuration213.Structures.ConfigSystem;

namespace BEMN.MR901.Big.Configuration
{
    public partial class Mr901BigConfigurationForm : Form, IFormView
    {
        #region [Constants]

        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR901_{0}";
        private const string XML_FILE_NAME = "МР901_Уставки_версия_{0}_{1}.bin";
        private const string MR901_BASE_CONFIG_PATH = "\\MR901\\MR901_BaseConfig_v{0}_{1}.bin";

        private const string ERROR_SETPOINTS_VALUE =
            "Обнаружены некорректные уставки. Конфигурация не может быть записана.";

        private const string INVALID_PORT = "Порт недоступен.";
        private const string READ_OK = "Конфигурация успешно прочитана";
        private const string READ_FAIL = "Не удалось прочитать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";
        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;

        #endregion [Constants]

        #region Поля


        private Mr901Device _device;
        private MemoryEntity<ConfigurationStructBigV210> _configuration;
        private MemoryEntity<ConfigurationStructBigV213> _configuration213;
        private ConfigurationStructBigV210 _currentSetpointsStruct;
        private ConfigurationStructBigV213 _currentSetpointsStruct213;

        private StructUnion<SetpointNewStruct> _setpointValidator;
        private StructUnion<ConfigurationStructBigV210> _configurationValidator;

        private StructUnion<SetpointNewStruct> _setpointValidator213;
        private StructUnion<ConfigurationStructBigV213> _configurationValidator213;

        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        #endregion


        #region Конструкторы

        public Mr901BigConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr901BigConfigurationForm(Mr901Device device)
        {
            this.InitializeComponent();
            this._device = device;
            Strings.Version = Common.VersionConverter(this._device.DeviceVersion);

            if (Strings.Version < 2.13)
            {
                ConfigurationStructBigV210.DeviceModelType = Strings.DeviceType;
                this._device.ConfigWriteOk += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteOk);
                this._device.ConfigWriteFail += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteFail);


                this._configuration = device.ConfigurationBigV210;
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration.AllWriteOk +=
                    HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
                this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this.Init();
                this._confIndGB.Visible = false;
                this.groupBox27.Visible = false;
            }
            else
            {
                ConfigurationStructBigV213.DeviceModelType = Strings.DeviceType;
                this._device.ConfigWriteOk += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteOk);
                this._device.ConfigWriteFail += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteFail);


                this._configuration213 = device.ConfigurationBigV213;
                this._configuration213.AllReadOk +=
                    HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration213.AllWriteOk +=
                    HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
                this._configuration213.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration213.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._configuration213.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration213.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration213.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this.Init213();
            }
        }

        private void Init213()
        {
            this._progressBar.Maximum = this._configuration213.Slots.Count;
            this._currentSetpointsStruct213 = new ConfigurationStructBigV213();
            if (this._device.DevicePlant == "A1" || this._device.DevicePlant == string.Empty)
            {
                this.urovGroup.Size = new Size(265, 426);
            }

            //if (this._device.DevicePlant == "A5")
            //{
            //    this._controlGroupBox.Text = "Контроль исправности цепей ТН";
            //    this._controlGroupBox.Size = new Size(203, 88);
            //    this._divControl.Location = new Point(182, 0);
            //}

            #region [Реле и Индикаторы]

            NewDgwValidatior<AllReleBigStruct, ReleOutputBigStruct> relayValidator =
                new NewDgwValidatior<AllReleBigStruct, ReleOutputBigStruct>
                (
                    this._outputReleGrid,
                    AllReleBigStruct.CurrentCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.RelayNamesNew, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.SignalType),
                    new ColumnInfoCombo(Strings.RelaySignalsBig),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllIndicatorsBigStruct, IndicatorsBigStruct> indicatorValidator =
                new NewDgwValidatior<AllIndicatorsBigStruct, IndicatorsBigStruct>
                (
                    this._outputIndicatorsGrid,
                    AllIndicatorsBigStruct.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.IndNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.SignalType),
                    new ColumnInfoCombo(Strings.RelaySignalsBig),
                    new ColumnInfoColor()
                );

            NewStructValidator<FaultBigStruct> faultValidator = new NewStructValidator<FaultBigStruct>
            (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
            );

            #endregion [Реле и Индикаторы]

            #region [ВЛС]

            allVlsCheckedListBoxs.Add(this.VLScheckedListBox1);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox2);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox3);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox4);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox5);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox6);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox7);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox8);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox9);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox10);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox11);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox12);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox13);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox14);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox15);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox16);

            CheckedListBox[] vlsBoxes =
            {
                this.VLScheckedListBox1,
                this.VLScheckedListBox2,
                this.VLScheckedListBox3,
                this.VLScheckedListBox4,
                this.VLScheckedListBox5,
                this.VLScheckedListBox6,
                this.VLScheckedListBox7,
                this.VLScheckedListBox8,
                this.VLScheckedListBox9,
                this.VLScheckedListBox10,
                this.VLScheckedListBox11,
                this.VLScheckedListBox12,
                this.VLScheckedListBox13,
                this.VLScheckedListBox14,
                this.VLScheckedListBox15,
                this.VLScheckedListBox16
            };
            NewCheckedListBoxValidator<OutputLogicBigStruct>[] vlsValidator =
                new NewCheckedListBoxValidator<OutputLogicBigStruct>[AllOutputLogicSignalBigStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalBigStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] =
                    new NewCheckedListBoxValidator<OutputLogicBigStruct>(vlsBoxes[i], Strings.VlsSignalsBig);
            }
            StructUnion<AllOutputLogicSignalBigStruct> vlsUnion =
                new StructUnion<AllOutputLogicSignalBigStruct>(vlsValidator);

            #endregion [ВЛС]

            #region [ЛС]

            DataGridView[] lsBoxes =
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12,
                this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16
            };

            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }

            NewDgwValidatior<InputLogicBigStruct>[] inputLogicValidator =
                new NewDgwValidatior<InputLogicBigStruct>[AllInputLogicSignalBigStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicSignalBigStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicBigStruct>
                (
                    lsBoxes[i],
                    InputLogicBigStruct.DiscrestCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LogicSignalNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.LogicValues)
                );
            }
            StructUnion<AllInputLogicSignalBigStruct> inputLogicUnion =
                new StructUnion<AllInputLogicSignalBigStruct>(inputLogicValidator);

            #endregion [ЛС]

            NewDgwValidatior<AllControlTtBigStruct, ControlTtBigStruct> ttValidator =
                new NewDgwValidatior<AllControlTtBigStruct, ControlTtBigStruct>
                (
                    this._configTtDgv,
                    AllControlTtBigStruct.TT_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.TtNames, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.TtFault),
                    new ColumnInfoCombo(Strings.ResetTT)
                );

            NewStructValidator<ControlTtUnionStruct> newTtValidator = new NewStructValidator<ControlTtUnionStruct>
            (
                this._toolTip,
                new ControlInfoValidator(ttValidator),
                new ControlInfoCombo(this._inpResetTtcomboBox, Strings.InputSignalsBig)
            );

            NewStructValidator<UrovNewStruct> urovValidator = new NewStructValidator<UrovNewStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._DZHModes, Strings.ModesLight),
                new ControlInfoCombo(this._DZHKontr, Strings.KONTR),
                new ControlInfoCombo(this._DZHDiff, Strings.Forbidden),
                new ControlInfoCombo(this._DZHSelf, Strings.Forbidden),
                new ControlInfoCombo(this._DZHSign, Strings.Forbidden),
                new ControlInfoText(this._DZHTUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov3, RulesContainer.TimeRule),
                new ControlInfoCombo(this._DZHSH1, Strings.InputSignalsBig),
                new ControlInfoCombo(this._DZHSH2, Strings.InputSignalsBig),
                new ControlInfoCombo(this._DZHPO, Strings.InputSignalsBig)
            );

            NewStructValidator<InputSignalBigStruct> inputSignalValidator = new NewStructValidator<InputSignalBigStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, Strings.InputSignalsBig),
                new ControlInfoCombo(this._indComboBox, Strings.InputSignalsBig)
            );

            #region [Осц]

            Func<string, Dictionary<ushort, string>> func = str =>
            {
                if (string.IsNullOrEmpty(str)) return new Dictionary<ushort, string>();
                int index = Strings.OscBases.IndexOf(str);
                return index != -1 ? Strings.OscChannelSignals[(ushort) index] : new Dictionary<ushort, string>();
            };

            NewDgwValidatior<AllChannelsWithBase, ChannelWithBase> allChannelsWithBase =
                new DgvValidatorWithDepend<AllChannelsWithBase, ChannelWithBase>
                (
                    _oscChannelsWithBaseGrid,
                    AllChannelsWithBase.KANAL_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.OscChannelWithBaseNames, ColumnsType.NAME),
                    new ColumnInfoComboControl(Strings.OscBases, 2),
                    new ColumnInfoDictionaryComboDependent(func, 1)
                );

            NewDgwValidatior<ChannelBigStruct213, ChannelStruct> allChannels =
                new DgvValidatorWithDepend<ChannelBigStruct213, ChannelStruct>
                (
                    _oscChannels,
                    ChannelBigStruct213.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.OscChannelNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.RelaySignalsBig213)
                );

            NewStructValidator<OscilloscopeSettingsBigStruct213> allChannelsValidator =
                new NewStructValidator<OscilloscopeSettingsBigStruct213>
                (
                    this._toolTip,
                    new ControlInfoCombo(_oscLength, Strings.OscLength),
                    new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                    new ControlInfoCombo(this._oscFix, Strings.OscFix),
                    new ControlInfoCombo(this._inpOscComboBox, Strings.RelaySignalsBig213),
                    new ControlInfoValidator(allChannelsWithBase),
                    new ControlInfoValidator(allChannels)
                );

            #endregion [Осц]

            NewDgwValidatior<AllUrovConnectionBigStruct, UrovConnectionBigStruct> urovConnectionValidator =
                new NewDgwValidatior<AllUrovConnectionBigStruct, UrovConnectionBigStruct>
                (
                    this._UROVJoinData,
                    AllUrovConnectionBigStruct.CurrentUrovConnectionCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ConnectionNamesBig, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllConnectionBigStruct, ConnectionBigStruct> connectionValidator =
                new NewDgwValidatior<AllConnectionBigStruct, ConnectionBigStruct>
                (
                    this._joinData,
                    AllConnectionBigStruct.ConnectionsCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ConnectionNamesBig, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.UshortTo65534),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.Join),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );
            StructUnion<ConnectionsAndTransformer> connectionAndTransformer;

            if (ConfigurationStructBigV213.DeviceModelType == "A5" ||
                ConfigurationStructBigV210.DeviceModelType == "A6" ||
                ConfigurationStructBigV210.DeviceModelType == "A7")
            {
                NewStructValidator<ParametersNTStruct> paramTN = new NewStructValidator<ParametersNTStruct>
                (
                    this._toolTip,
                    new ControlInfoText(this._KTHL, RulesContainer.Ustavka128),
                    new ControlInfoText(this._KTHX, RulesContainer.Ustavka128),
                    new ControlInfoCombo(this._KTHLkoef, Strings.KthKoefs),
                    new ControlInfoCombo(this._KTHXkoef, Strings.KthKoefs),
                    new ControlInfoCombo(this._neisprTNf, Strings.InputSignalsBig),
                    new ControlInfoCombo(this._neisprTNn, Strings.InputSignalsBig),
                    new ControlInfoCheck(this._deltaUcontrol),
                    new ControlInfoText(this._deltaU, RulesContainer.Ustavka256),
                    new ControlInfoText(this._timeDeltaU, RulesContainer.TimeRule),
                    new ControlInfoCombo(this._deltaUblock, Strings.ExtDefSignalsBig),
                    new ControlInfoCheck(this._divControl),
                    new ControlInfoText(this._divU, RulesContainer.UshortTo100),
                    new ControlInfoText(this._divTime, RulesContainer.TimeRule),
                    new ControlInfoCombo(this._divBlock, Strings.ExtDefSignalsBig)
                );
                connectionAndTransformer = new StructUnion<ConnectionsAndTransformer>(connectionValidator, paramTN);
            }
            else
            {
                this._configurationTabControl.TabPages.Remove(this._parameterTNpage);
                connectionAndTransformer = new StructUnion<ConnectionsAndTransformer>(connectionValidator);
            }

            NewDgwValidatior<AllDifferentialCurrentBigStruct, DifferentialCurrentBigStruct> differentialCurrentValidator
                =
                new NewDgwValidatior<AllDifferentialCurrentBigStruct, DifferentialCurrentBigStruct>
                (
                    new[] {this._difDDataGrid, this._difMDataGrid},
                    new[] {3, 3},
                    this._toolTip,
                    new ColumnInfoCombo(Strings.DefNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule, true, false),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(new CustomUshortRule(0, 45)),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._difDDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        ),
                        new TurnOffDgv
                        (
                            this._difMDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        )
                    },
                    Disabled = new Point[]
                    {
                        new Point(3, 2)
                    }
                };

            NewDgwValidatior<AllMtzBigStruct, MtzBigStruct> mtzValidator213 =
                new NewDgwValidatior<AllMtzBigStruct, MtzBigStruct>
                (
                    this._MTZDifensesDataGrid,
                    AllMtzBigStruct.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.MtzNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.ExtDefSignals213),
                    new ColumnInfoCombo(Strings.MTZJoinBig),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoCombo(Strings.Characteristic),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.Ushort100To4000),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._MTZDifensesDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9)
                        )
                    }
                };
            
            NewDgwValidatior<AllExternalDefenseBigStruct, ExternalDefenseBigStruct> externalDefenseValidator =
                new NewDgwValidatior<AllExternalDefenseBigStruct, ExternalDefenseBigStruct>
                (
                    this._externalDifensesDataGrid,
                    AllExternalDefenseBigStruct.CurrentCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ExternalnNamesBig, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.OtklBig),
                    new ColumnInfoCombo(Strings.ExtDefSignals213),
                    new ColumnInfoCombo(Strings.ExtDefSignals213),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.ExtDefSignals213),
                    new ColumnInfoCheck(),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._externalDifensesDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                    }
                };

            if (ConfigurationStructBigV213.DeviceModelType == "A5" ||
                ConfigurationStructBigV213.DeviceModelType == "A6" ||
                ConfigurationStructBigV213.DeviceModelType == "A7")
            {
                NewDgwValidatior<AllVoltageDefenseBigStruct, VoltageDefenseBigStruct> voltageDef =
                    new NewDgwValidatior<AllVoltageDefenseBigStruct, VoltageDefenseBigStruct>
                    (
                        new[] {this._defenseUBgrid, this._defenseUMgrid},
                        new[] {2, 2},
                        this._toolTip,
                        new ColumnInfoCombo(Strings.Unames, ColumnsType.NAME),
                        new ColumnInfoCombo(Strings.Mode),
                        new ColumnInfoCombo(Strings.OtklBig),
                        new ColumnInfoCombo(Strings.ExtDefSignals213),
                        new ColumnInfoCheck(),
                        new ColumnInfoCheck(),
                        new ColumnInfoCheck(),
                        new ColumnInfoCombo(Strings.UBtypes, ColumnsType.COMBO, true, false),
                        new ColumnInfoCombo(Strings.UMtypes, ColumnsType.COMBO, false, true),
                        new ColumnInfoText(RulesContainer.Ustavka256),
                        new ColumnInfoText(RulesContainer.TimeRule),
                        new ColumnInfoCheck(),
                        new ColumnInfoText(RulesContainer.Ustavka256),
                        new ColumnInfoText(RulesContainer.TimeRule),
                        new ColumnInfoCombo(Strings.ModesLightOsc),
                        new ColumnInfoCheck()
                    )
                    {
                        TurnOff = new[]
                        {
                            new TurnOffDgv
                            (
                                this._defenseUBgrid,
                                new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                                    15)
                            ),
                            new TurnOffDgv
                            (
                                this._defenseUMgrid,
                                new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                                    15)
                            )
                        }
                    };

                this._setpointValidator213 = new StructUnion<SetpointNewStruct>
                (
                    differentialCurrentValidator,
                    mtzValidator213,
                    externalDefenseValidator,
                    voltageDef
                );
            }
            else
            {
                this._fault5CheckBox.Visible = false;
                this._fault5Label.Visible = false;
                this._difensesTC.TabPages.Remove(this._defUtab);
                this._setpointValidator213 = new StructUnion<SetpointNewStruct>
                (
                    differentialCurrentValidator,
                    mtzValidator213,
                    externalDefenseValidator
                );
            }

            NewStructValidator<ConfigIgSsh> igSshValidator = new NewStructValidator<ConfigIgSsh>(
                this._toolTip,
                new ControlInfoCombo(this._configIgSshComboBox, Strings.ConfigIgssh));

            NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                this._toolTip,
                new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));

            RadioButtonSelector groupSelector =
                new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton, this._groupChangeButton);
            SetpointsValidator<AllDefensesSetpointsNewStruct, SetpointNewStruct> defensesValidator =
                new SetpointsValidator<AllDefensesSetpointsNewStruct, SetpointNewStruct>
                (
                    groupSelector,
                    this._setpointValidator213
                );

            this._configurationValidator213 = new StructUnion<ConfigurationStructBigV213>
            (
                urovValidator,
                urovConnectionValidator,
                connectionAndTransformer,
                inputSignalValidator,
                allChannelsValidator,
                newTtValidator,
                inputLogicUnion,
                vlsUnion,
                defensesValidator,
                relayValidator,
                indicatorValidator,
                faultValidator,
                ethernetValidator,
                igSshValidator
            );
        }

        private void Init()
        {
            this._progressBar.Maximum = this._configuration.Slots.Count;
            this._currentSetpointsStruct = new ConfigurationStructBigV210();
            if (this._device.DevicePlant == "A1" || this._device.DevicePlant == string.Empty)
            {
                this.urovGroup.Size = new Size(265, 426);
            }

            //if (this._device.DevicePlant == "A5")
            //{
            //    this._controlGroupBox.Text = "Контроль исправности цепей ТН";
            //    this._controlGroupBox.Size = new Size(203, 88);
            //    this._divControl.Location = new Point(182, 0);
            //}

            #region [Реле и Индикаторы]

            NewDgwValidatior<AllReleBigStruct, ReleOutputBigStruct> relayValidator =
                new NewDgwValidatior<AllReleBigStruct, ReleOutputBigStruct>
                (
                    this._outputReleGrid,
                    AllReleBigStruct.CurrentCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.RelayNamesNew, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.SignalType),
                    new ColumnInfoCombo(Strings.RelaySignalsBig),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllIndicatorsBigStruct, IndicatorsBigStruct> indicatorValidator =
                new NewDgwValidatior<AllIndicatorsBigStruct, IndicatorsBigStruct>
                (
                    this._outputIndicatorsGrid,
                    AllIndicatorsBigStruct.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.IndNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.SignalType),
                    new ColumnInfoCombo(Strings.RelaySignalsBig),
                    new ColumnInfoColor()
                );

            NewStructValidator<FaultBigStruct> faultValidator = new NewStructValidator<FaultBigStruct>
            (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
            );

            #endregion [Реле и Индикаторы]

            #region [ВЛС]

            allVlsCheckedListBoxs.Add(this.VLScheckedListBox1);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox2);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox3);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox4);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox5);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox6);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox7);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox8);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox9);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox10);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox11);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox12);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox13);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox14);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox15);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox16);

            CheckedListBox[] vlsBoxes =
            {
                this.VLScheckedListBox1,
                this.VLScheckedListBox2,
                this.VLScheckedListBox3,
                this.VLScheckedListBox4,
                this.VLScheckedListBox5,
                this.VLScheckedListBox6,
                this.VLScheckedListBox7,
                this.VLScheckedListBox8,
                this.VLScheckedListBox9,
                this.VLScheckedListBox10,
                this.VLScheckedListBox11,
                this.VLScheckedListBox12,
                this.VLScheckedListBox13,
                this.VLScheckedListBox14,
                this.VLScheckedListBox15,
                this.VLScheckedListBox16
            };
            NewCheckedListBoxValidator<OutputLogicBigStruct>[] vlsValidator =
                new NewCheckedListBoxValidator<OutputLogicBigStruct>[AllOutputLogicSignalBigStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalBigStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] =
                    new NewCheckedListBoxValidator<OutputLogicBigStruct>(vlsBoxes[i], Strings.VlsSignalsBig);
            }
            StructUnion<AllOutputLogicSignalBigStruct> vlsUnion =
                new StructUnion<AllOutputLogicSignalBigStruct>(vlsValidator);

            #endregion [ВЛС]

            #region [ЛС]

            DataGridView[] lsBoxes =
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12,
                this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16
            };

            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }

            NewDgwValidatior<InputLogicBigStruct>[] inputLogicValidator =
                new NewDgwValidatior<InputLogicBigStruct>[AllInputLogicSignalBigStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicSignalBigStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicBigStruct>
                (
                    lsBoxes[i],
                    InputLogicBigStruct.DiscrestCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LogicSignalNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.LogicValues)
                );
            }
            StructUnion<AllInputLogicSignalBigStruct> inputLogicUnion =
                new StructUnion<AllInputLogicSignalBigStruct>(inputLogicValidator);

            #endregion [ЛС]

            NewDgwValidatior<AllControlTtBigStruct, ControlTtBigStruct> ttValidator =
                new NewDgwValidatior<AllControlTtBigStruct, ControlTtBigStruct>
                (
                    this._configTtDgv,
                    AllControlTtBigStruct.TT_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.TtNames, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.TtFault),
                    new ColumnInfoCombo(Strings.ResetTT)
                );

            NewStructValidator<ControlTtUnionStruct> newTtValidator = new NewStructValidator<ControlTtUnionStruct>
            (
                this._toolTip,
                new ControlInfoValidator(ttValidator),
                new ControlInfoCombo(this._inpResetTtcomboBox, Strings.InputSignalsBig)
            );

            NewStructValidator<UrovNewStruct> urovValidator = new NewStructValidator<UrovNewStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._DZHModes, Strings.ModesLight),
                new ControlInfoCombo(this._DZHKontr, Strings.KONTR),
                new ControlInfoCombo(this._DZHDiff, Strings.Forbidden),
                new ControlInfoCombo(this._DZHSelf, Strings.Forbidden),
                new ControlInfoCombo(this._DZHSign, Strings.Forbidden),
                new ControlInfoText(this._DZHTUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov3, RulesContainer.TimeRule),
                new ControlInfoCombo(this._DZHSH1, Strings.InputSignalsBig),
                new ControlInfoCombo(this._DZHSH2, Strings.InputSignalsBig),
                new ControlInfoCombo(this._DZHPO, Strings.InputSignalsBig)
            );

            NewStructValidator<InputSignalBigStruct> inputSignalValidator = new NewStructValidator<InputSignalBigStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, Strings.InputSignalsBig),
                new ControlInfoCombo(this._indComboBox, Strings.InputSignalsBig)
            );

            NewDgwValidatior<ChannelBigStruct> channelValidator = new NewDgwValidatior<ChannelBigStruct>
            (
                this._oscChannels,
                ChannelBigStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.ChannelsNamesBig, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.RelaySignalsBig)
            );
            this._oscChannels.CellBeginEdit += this.GridOnCellBeginEdit;

            NewStructValidator<OscilloscopeSettingsBigStruct> oscValidator =
                new NewStructValidator<OscilloscopeSettingsBigStruct>
                (
                    this._toolTip,
                    new ControlInfoCombo(this._oscLength, Strings.OscLength),
                    new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                    new ControlInfoCombo(this._oscFix, Strings.OscFix),
                    new ControlInfoValidator(channelValidator),
                    new ControlInfoCombo(this._inpOscComboBox, Strings.RelaySignalsBig)
                );

            NewDgwValidatior<AllUrovConnectionBigStruct, UrovConnectionBigStruct> urovConnectionValidator =
                new NewDgwValidatior<AllUrovConnectionBigStruct, UrovConnectionBigStruct>
                (
                    this._UROVJoinData,
                    AllUrovConnectionBigStruct.CurrentUrovConnectionCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ConnectionNamesBig, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllConnectionBigStruct, ConnectionBigStruct> connectionValidator =
                new NewDgwValidatior<AllConnectionBigStruct, ConnectionBigStruct>
                (
                    this._joinData,
                    AllConnectionBigStruct.ConnectionsCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ConnectionNamesBig, ColumnsType.NAME),
                    new ColumnInfoText(RulesContainer.UshortTo65534),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.Join),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.TimeRule)
                );
            StructUnion<ConnectionsAndTransformer> connectionAndTransformer;
            if (ConfigurationStructBigV210.DeviceModelType == "A5" ||
                ConfigurationStructBigV210.DeviceModelType == "A6" ||
                ConfigurationStructBigV210.DeviceModelType == "A7")
            {
                NewStructValidator<ParametersNTStruct> paramTN = new NewStructValidator<ParametersNTStruct>
                (
                    this._toolTip,
                    new ControlInfoText(this._KTHL, RulesContainer.Ustavka128),
                    new ControlInfoText(this._KTHX, RulesContainer.Ustavka128),
                    new ControlInfoCombo(this._KTHLkoef, Strings.KthKoefs),
                    new ControlInfoCombo(this._KTHXkoef, Strings.KthKoefs),
                    new ControlInfoCombo(this._neisprTNf, Strings.InputSignalsBig),
                    new ControlInfoCombo(this._neisprTNn, Strings.InputSignalsBig),
                    new ControlInfoCheck(this._deltaUcontrol),
                    new ControlInfoText(this._deltaU, RulesContainer.Ustavka256),
                    new ControlInfoText(this._timeDeltaU, RulesContainer.TimeRule),
                    new ControlInfoCombo(this._deltaUblock, Strings.ExtDefSignalsBig),
                    new ControlInfoCheck(this._divControl),
                    new ControlInfoText(this._divU, RulesContainer.UshortTo100),
                    new ControlInfoText(this._divTime, RulesContainer.TimeRule),
                    new ControlInfoCombo(this._divBlock, Strings.ExtDefSignalsBig)
                );
                connectionAndTransformer = new StructUnion<ConnectionsAndTransformer>(connectionValidator, paramTN);
            }
            else
            {
                this._configurationTabControl.TabPages.Remove(this._parameterTNpage);
                connectionAndTransformer = new StructUnion<ConnectionsAndTransformer>(connectionValidator);
            }

            NewDgwValidatior<AllDifferentialCurrentBigStruct, DifferentialCurrentBigStruct> differentialCurrentValidator
                =
                new NewDgwValidatior<AllDifferentialCurrentBigStruct, DifferentialCurrentBigStruct>
                (
                    new[] {this._difDDataGrid, this._difMDataGrid},
                    new[] {3, 3},
                    this._toolTip,
                    new ColumnInfoCombo(Strings.DefNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule, true, false),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(new CustomUshortRule(0, 45)),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                    new ColumnInfoCheck(true, false),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._difDDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        ),
                        new TurnOffDgv
                        (
                            this._difMDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        )
                    },
                    Disabled = new Point[]
                    {
                        new Point(3, 2)
                    }
                };
            NewDgwValidatior<AllMtzBigStruct, MtzBigStruct> mtzValidator =
                new NewDgwValidatior<AllMtzBigStruct, MtzBigStruct>
                (
                    this._MTZDifensesDataGrid,
                    AllMtzBigStruct.COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.MtzNames, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.InputSignalsBig),
                    new ColumnInfoCombo(Strings.MTZJoinBig),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoCombo(Strings.Characteristic),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.Ushort100To4000),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._MTZDifensesDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9)
                        )
                    }
                };

            NewDgwValidatior<AllExternalDefenseBigStruct, ExternalDefenseBigStruct> externalDefenseValidator =
                new NewDgwValidatior<AllExternalDefenseBigStruct, ExternalDefenseBigStruct>
                (
                    this._externalDifensesDataGrid,
                    AllExternalDefenseBigStruct.CurrentCount,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.ExternalnNamesBig, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.Mode),
                    new ColumnInfoCombo(Strings.OtklBig),
                    new ColumnInfoCombo(Strings.ExtDefSignalsBig),
                    new ColumnInfoCombo(Strings.ExtDefSignalsBig),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.ExtDefSignalsBig),
                    new ColumnInfoCheck(),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._externalDifensesDataGrid,
                            new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                    }
                };

            if (ConfigurationStructBigV210.DeviceModelType == "A5" || 
                ConfigurationStructBigV210.DeviceModelType == "A6" ||
                ConfigurationStructBigV210.DeviceModelType == "A7")
            {
                NewDgwValidatior<AllVoltageDefenseBigStruct, VoltageDefenseBigStruct> voltageDef =
                    new NewDgwValidatior<AllVoltageDefenseBigStruct, VoltageDefenseBigStruct>
                    (
                        new[] {this._defenseUBgrid, this._defenseUMgrid},
                        new[] {2, 2},
                        this._toolTip,
                        new ColumnInfoCombo(Strings.Unames, ColumnsType.NAME),
                        new ColumnInfoCombo(Strings.Mode),
                        new ColumnInfoCombo(Strings.OtklBig),
                        new ColumnInfoCombo(Strings.ExtDefSignalsBig),
                        new ColumnInfoCheck(),
                        new ColumnInfoCheck(),
                        new ColumnInfoCheck(),
                        new ColumnInfoCombo(Strings.UBtypes, ColumnsType.COMBO, true, false),
                        new ColumnInfoCombo(Strings.UMtypes, ColumnsType.COMBO, false, true),
                        new ColumnInfoText(RulesContainer.Ustavka256),
                        new ColumnInfoText(RulesContainer.TimeRule),
                        new ColumnInfoCheck(),
                        new ColumnInfoText(RulesContainer.Ustavka256),
                        new ColumnInfoText(RulesContainer.TimeRule),
                        new ColumnInfoCombo(Strings.ModesLightOsc),
                        new ColumnInfoCheck()
                    )
                    {
                        TurnOff = new[]
                        {
                            new TurnOffDgv
                            (
                                this._defenseUBgrid,
                                new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                                    15)
                            ),
                            new TurnOffDgv
                            (
                                this._defenseUMgrid,
                                new TurnOffRule(1, Strings.Mode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                                    15)
                            )
                        }
                    };

                this._setpointValidator = new StructUnion<SetpointNewStruct>
                (
                    differentialCurrentValidator,
                    mtzValidator,
                    externalDefenseValidator,
                    voltageDef
                );
            }
            else
            {
                this._fault5CheckBox.Visible = false;
                this._fault5Label.Visible = false;
                this._difensesTC.TabPages.Remove(this._defUtab);
                this._setpointValidator = new StructUnion<SetpointNewStruct>
                (
                    differentialCurrentValidator,
                    mtzValidator,
                    externalDefenseValidator
                );
            }

            NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                this._toolTip,
                new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));

            RadioButtonSelector groupSelector =
                new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton, this._groupChangeButton);
            SetpointsValidator<AllDefensesSetpointsNewStruct, SetpointNewStruct> defensesValidator =
                new SetpointsValidator<AllDefensesSetpointsNewStruct, SetpointNewStruct>
                (
                    groupSelector,
                    this._setpointValidator
                );

            this._configurationValidator = new StructUnion<ConfigurationStructBigV210>
            (
                urovValidator,
                urovConnectionValidator,
                connectionAndTransformer,
                inputSignalValidator,
                oscValidator,
                newTtValidator,
                inputLogicUnion,
                vlsUnion,
                defensesValidator,
                relayValidator,
                indicatorValidator,
                faultValidator,
                ethernetValidator
            );
        }

        #endregion

        #region Дополнительные функции

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi2Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        public void ConfigurationReadOk()
        {
            this._statusLabel.Text = READ_OK;

            if (Strings.Version < 2.13)
            {
                this._currentSetpointsStruct = this._configuration.Value;
            }
            if (Strings.Version >= 2.13)
            {
                this._currentSetpointsStruct213 = this._configuration213.Value;
            }
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
            this.IsProcess = false;
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this._statusLabel.Text = WRITE_OK;
            MessageBox.Show(WRITE_OK);
            this.IsProcess = false;
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            if (Strings.Version < 2.13)
            {
                this._configurationValidator.Set(this._currentSetpointsStruct);
            }

            if (Strings.Version >= 2.13)
            {
                this._configurationValidator213.Set(this._currentSetpointsStruct213);
            }
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._progressBar.Value = 0;
            if (Strings.Version < 2.13)
            {
                this._configuration.LoadStruct();
            }
            if (Strings.Version >= 2.13)
            {
                this._configuration213.LoadStruct();
            }
            this._statusLabel.Text = "Идет чтение";
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (Strings.Version < 2.13)
            {
                if (this._configurationValidator.Check(out message, true))
                {
                    this._currentSetpointsStruct = this._configurationValidator.Get();
                }

                return true;
            }
            else if (Strings.Version >= 2.13)
            {
                if (this._configurationValidator213.Check(out message, true))
                {
                    this._currentSetpointsStruct213 = this._configurationValidator213.Get();
                }
                return true;
            }
            else
            {
                MessageBox.Show(ERROR_SETPOINTS_VALUE);
                return false;
            }

        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a;

                if (Common.VersionConverter(this._device.DeviceVersion) >= 2.10)
                {
                    string config = Mr901Device.GetConfigDevString(this._device.DevicePlant);
                    a = doc.FirstChild.FirstChild;
                    if (a.Name != string.Format(XML_HEAD, config))
                    {
                        MessageBox.Show(
                            $"Невозможно загрузить уставки {a.Name}. Текущее устройство {string.Format(XML_HEAD, config)}.",
                            "ВНимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    a = doc.FirstChild.SelectSingleNode("MR901_SET_POINTS");
                    if (a == null)
                    {
                        throw new Exception();
                    }
                }

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (Strings.Version < 2.13)
                {
                    this._currentSetpointsStruct?.InitStruct(values);
                }
                if (Strings.Version >= 2.13)
                {
                    this._currentSetpointsStruct213?.InitStruct(values);
                }
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }
        }

        private void SaveInFile()
        {
            string config = Mr901Device.GetConfigDevString(this._device.DevicePlant);
            this._saveConfigurationDlg.FileName = string.Format(XML_FILE_NAME, this._device.DeviceVersion, config);
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.WriteConfiguration();
                this.Serialize(this._saveConfigurationDlg.FileName, config);
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName, string config)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR901"));

                ushort[] values = new ushort[] { };

                if (Strings.Version < 2.13)
                {
                    values = this._currentSetpointsStruct.GetValues();
                }

                if (Strings.Version >= 2.13)
                {
                    values = this._currentSetpointsStruct213.GetValues();
                }

                XmlElement element = doc.CreateElement(string.Format(XML_HEAD, config));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        #endregion

        #region Обработчики событий

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]
            {
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.resetSetpointsItem,
                this.clearSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem
            });
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.StartRead();
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int osclen = 31360 * 2;
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = $"{osclen / (index + 2)} мс";
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}
            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 901 №{0}? " +
                                                                "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                    this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                "Запись", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идёт запись конфигурации";
                    this.IsProcess = true;
                    this._progressBar.Value = 0;
                    if (Strings.Version < 2.13)
                    {
                        this._configuration.Value = this._currentSetpointsStruct;
                        this._configuration.SaveStruct();
                    }
                    if (Strings.Version >= 2.13)
                    {
                        this._configuration213.Value = this._currentSetpointsStruct213;
                        this._configuration213.SaveStruct();
                    }

                }
            }
        }

        private void _clearSetpointsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            if (Strings.Version < 2.13)
            {
                this._configurationValidator.Reset();
            }
            if (Strings.Version >= 2.13)
            {
                this._configurationValidator213.Reset();
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void
            ResetSetpoints(bool isDialog) //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {
                string config = Mr901Device.GetConfigDevString(this._device.DevicePlant);

                XmlDocument doc = new XmlDocument();
                string path = Path.GetDirectoryName(Application.ExecutablePath) +
                              string.Format(MR901_BASE_CONFIG_PATH, this._device.DeviceVersion, config);
                doc.Load(path);

                XmlNode a = doc.FirstChild.SelectSingleNode(string.Format(XML_HEAD, config));
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (Strings.Version < 2.13)
                {
                    this._currentSetpointsStruct.InitStruct(values);
                }
                if (Strings.Version >= 2.13)
                {
                    this._currentSetpointsStruct213.InitStruct(values);
                }
                this.ShowConfiguration();
                this._statusLabel.Text = "Базовые уставки успешно загружены";
            }
            catch (Exception ex)
            {
                if (Validator.DEBUG)
                {
                    MessageBox.Show(ex.Message);
                }
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                            "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }

                if (Strings.Version < 2.13)
                {
                    this._configurationValidator.Reset();
                }
                if (Strings.Version >= 2.13)
                {
                    this._configurationValidator213.Reset();
                }
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    if (Strings.Version < 2.13)
                    {
                        this._currentSetpointsStruct.DeviceVersion =
                            Common.VersionConverter(this._device.DeviceVersion);
                        this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct.DeviceModelTypeXml = this._device.DevicePlant;
                        this._currentSetpointsStruct.OscCount = this._oscSizeTextBox.Text;

                        this._statusLabel.Text = HtmlExport.Export(Resources.MR901Main,
                            Resources.MR901Res, this._currentSetpointsStruct,
                            this._currentSetpointsStruct.DeviceType, this._device.DeviceVersion);
                    }

                    if (Strings.Version >= 2.13)
                    {
                        this._currentSetpointsStruct213.DeviceVersion =
                            Common.VersionConverter(this._device.DeviceVersion);
                        this._currentSetpointsStruct213.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct213.DeviceModelTypeXml = this._device.DevicePlant;
                        this._currentSetpointsStruct213.OscCount = this._oscSizeTextBox.Text;

                        this._statusLabel.Text = HtmlExport.Export(Resources.MR901Main,
                            Resources.MR901Res, this._currentSetpointsStruct213,
                            this._currentSetpointsStruct213.DeviceType, this._device.DeviceVersion);
                    }

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        private void Mr901ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                    //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                    //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                    //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                    //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip) sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
                if (Strings.Version < 2.13)
                {
                    this._configurationValidator.Reset();
                }
                if (Strings.Version >= 2.13)
                {
                    this._configurationValidator213.Reset();
                }
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            }
            if (e.ClickedItem == this.resetSetpointsItem)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                //TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                //TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void Mr901ConfigurationForm_Activated(object sender, EventArgs e)
        {
            Strings.Version = Common.VersionConverter(this._device.DeviceVersion);
            ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
        }

        #endregion


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr901Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr901BigConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void VLScheckedListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _wrapBtn_Click(object sender, EventArgs e)
        {
            if (this._wrapBtn.Text == "Свернуть")
            {
                this.treeViewForVLS.CollapseAll();
                this._wrapBtn.Text = "Развернуть";
            }
            else
            {
                this.treeViewForVLS.ExpandAll();
                this._wrapBtn.Text = "Свернуть";
            }
        }

        private void _DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }        
    }
}
