﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Configuration.Structures.Osc
{
    public class ChannelBigStruct213 : StructBase, IDgvRowsContainer<ChannelStruct>
    {
        public const int COUNT = 64;

        public ushort[] Kanal //конфигурация канала осциллографирования
        {
            get { return this.Rows.Select(r => r.ChannelUshort).ToArray(); }
            set
            {
                for (int i = 0; i < COUNT; i++)
                {
                    this.Rows[i].InitStruct(Common.TOBYTE(value[i], false));
                }
            }
        }

        private ChannelStruct[] _rows;

        [XmlElement(ElementName = "Каналы")]
        public ChannelStruct[] Rows
        {
            get
            {
                if (this._rows == null || this._rows.Length < COUNT)
                {
                    this._rows = new ChannelStruct[COUNT];
                    for (int i = 0; i < COUNT; i++)
                    {
                        this._rows[i] = new ChannelStruct();
                    }
                }
                return this._rows;
            }
            set { this._rows = value; }
        }
    }
}
