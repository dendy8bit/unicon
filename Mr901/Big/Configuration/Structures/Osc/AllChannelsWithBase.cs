﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Configuration.Structures.Osc
{
    public class AllChannelsWithBase : StructBase, IDgvRowsContainer<ChannelWithBase>
    {
        public const int KANAL_COUNT = 32;
        public const int BASE_COUNT = 4;

        public ushort[] OscChannels { get; set; }    //конфигурация канала осциллографирования
        public ushort[] CfgOscChannels { get; set; } //конфигурация каналов, выборка списков сигналов из БД

        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelWithBase[] Rows
        {
            get { return this.GetChannelsWithBase(); }
            set { this.SetChannelsWithBase(value); }
        }

        public ChannelWithBase[] ForOsc
        {
            get { return this.GetChannelsWithBase(); }
            set { this.SetChannelsWithBase(value); }
        }

        public ChannelWithBase[] GetChannelsWithBase()
        {
            this.OscChannels = this.OscChannels ?? new ushort[KANAL_COUNT];
            this.CfgOscChannels = this.CfgOscChannels ?? new ushort[BASE_COUNT];

            BitArray array = new BitArray(Common.TOBYTES(this.CfgOscChannels, false));
            byte[] bases = new byte[KANAL_COUNT];
            for (int i = 0; i < KANAL_COUNT; i++)
            {
                bases[i] = (byte) (1*(array[i*2] ? 1 : 0) + 2*(array[i*2+1] ? 1 : 0));
            }
            List<ChannelWithBase> channelsList = new List<ChannelWithBase>();
            for (int i = 0; i < KANAL_COUNT; i++)
            {
                channelsList.Add(new ChannelWithBase { Base = bases[i], Channel = this.OscChannels[i] });
            }
            return channelsList.ToArray();
        }

        public void SetChannelsWithBase(ChannelWithBase[] value)
        {
            List<bool> boolList = new List<bool>();
            foreach (bool[] values in value.Select(channel => new[] { Common.GetBit(channel.Base, 0), Common.GetBit(channel.Base, 1) }))
            {
                boolList.AddRange(values);
            }
            do
            {
                boolList.Add(false);
            } while (boolList.Count < BASE_COUNT * 16); // заполнение 0 битами до размера 8 ushort
            List<ushort> retUshorts = new List<ushort>();
            for (int i = 0; i < BASE_COUNT; i++)
            {
                ushort cfg = 0;
                for (int j = 0; j < 16; j++)
                {
                    cfg += boolList[j+i*16] ? (ushort)Math.Pow(2, j) : (ushort)0;
                }
                retUshorts.Add(cfg);
            }
            this.CfgOscChannels = retUshorts.ToArray();
            for (int i = 0; i < value.Length; i++)
            {
                this.OscChannels[i] = value[i].Channel;
            }
        }
    }
}
