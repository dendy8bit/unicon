﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Big.Configuration.Structures.Osc
{
    public class ChannelWithBase : StructBase
    {
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "База")]
        public string BaseStr
        {
            get { return Validator.Get(this.Base, Strings.OscBases); }
            set { this.Base = (byte)Validator.Set(value, Strings.OscBases); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Канал")]
        public string ChannelStr
        {
            get
            {
                Dictionary<ushort,string> list = Strings.OscChannelSignals[this.Base];
                return Validator.Get(this.Channel, list);
            }
            set
            {
                Dictionary<ushort, string> list = Strings.OscChannelSignals[this.Base];
                this.Channel = Validator.Set(value, list);
            }
        }

        public byte Base { get; set; }

        public ushort Channel { get; set; }
    }
}
