﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Big.Configuration.Structures.Osc
{
    public class ChannelBigStruct : StructBase, IXmlSerializable
    {
        public const int COUNT = 96;
        [Layout(0, Count = COUNT)] private ushort[] _kanal; //конфигурация канала осциллографирования
        
        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get { return Validator.Get(this._kanal[ls], Strings.RelaySignalsBig); }
            set { this._kanal[ls] = Validator.Set(value, Strings.RelaySignalsBig); }
        }

        public ushort[] Kanal 
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < COUNT; i++)
            {
                writer.WriteElementString("Канал", this[i]);
            }
        }
    }
}
