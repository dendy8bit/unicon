using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// ��� ����������
    /// </summary>
    public class AllIndicatorsBigStruct : StructBase, IDgvRowsContainer<IndicatorsBigStruct>
    {
        public const int COUNT = 12;

        /// <summary>
        /// ����������
        /// </summary>
        [Layout(0, Count = COUNT)] private IndicatorsBigStruct[] _indicators;

        /// <summary>
        /// ����������
        /// </summary>
        [XmlArray(ElementName = "���_����������")]
        public IndicatorsBigStruct[] Rows
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }
    }
}