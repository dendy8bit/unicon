﻿using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Configuration.Structures.RelayIndicator
{
    /// <summary>
    /// параметры индикаторов
    /// </summary>
    [XmlType(TypeName = "Один_индикатор")]
    public class IndicatorsBigStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _signal1;
        [Layout(2)] private ushort _type;
        [Layout(3)] private ushort _reserve;
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
         [BindingProperty(0)]
         [XmlAttribute(AttributeName = "Тип")]
         public string Type
        {
            get { return Validator.Get(this._type, Strings.SignalType, 0); }
            set { this._type = Validator.Set(value, Strings.SignalType, this._type, 0); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string Signal
        {
            get { return Validator.Get(this._signal, Strings.RelaySignalsBig); }
            set { this._signal = Validator.Set(value, Strings.RelaySignalsBig); }
        }

        /// <summary>
        /// Цвет
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(Type = typeof(XmlColor), ElementName =  "Цвет_индикатора")]
        public Color Color
        {
            get { return Common.GetBit(this._type, 8) ? Color.Green : Color.Red; }
            set
            {
                var bit = value == Color.Green;
                this._type = Common.SetBit(this._type, 8, bit);
            }
        }
        #endregion [Properties]
    }
}
