﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Big.Configuration.Structures
{
    public class InputSignalBigStruct : StructBase
    {
        [Layout(0, Count = 2, Ignore = true)] private ushort[] _reserve;
        [Layout(1)] private ushort _groopUst; //вход аварийная группа уставок
        [Layout(2)] private ushort _clrInd; //вход сброс индикации

        #region Входные сигналы
        [XmlElement(ElementName = "аварийная_группа")]
        [BindingProperty(0)]
        public string GrUst
        {
            get { return Validator.Get(this._groopUst, Strings.InputSignalsBig); }
            set { this._groopUst = Validator.Set(value, Strings.InputSignalsBig); }
        }
        [XmlElement(ElementName = "сброс_индикации")]
        [BindingProperty(1)]
        public string SbInd
        {
            get { return Validator.Get(this._clrInd, Strings.InputSignalsBig); }
            set { this._clrInd = Validator.Set(value, Strings.InputSignalsBig); }
        }

        #endregion
    }
}