﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR901.Big.Configuration.Structures.Defenses.Differential;
using BEMN.MR901.Big.Configuration.Structures.Defenses.External;
using BEMN.MR901.Big.Configuration.Structures.Defenses.Mtz;
using BEMN.MR901.Big.Configuration.Structures.Defenses.U;

namespace BEMN.MR901.Big.Configuration.Structures.Defenses
{
    public class SetpointNewStruct : StructBase
    {
        [Layout(0)] private AllDifferentialCurrentBigStruct _allDifferentialCurrent;
        [Layout(1)] private AllMtzBigStruct _allMtz;
        [Layout(2)] private AllExternalDefenseBigStruct _allExternalDefense;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Диф")]
        public AllDifferentialCurrentBigStruct AllDifferentialCurrent
        {
            get { return this._allDifferentialCurrent; }
            set { this._allDifferentialCurrent = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "МТЗ")]
        public AllMtzBigStruct AllMtz
        {
            get { return this._allMtz; }
            set { this._allMtz = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefenseBigStruct AllExternalDefense
        {
            get { return this._allExternalDefense; }
            set { this._allExternalDefense = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Напряжения")]
        public AllVoltageDefenseBigStruct AllVoltageDefense
        {
            get
            {
                if (AllExternalDefenseBigStruct.CurrentCount > 20) return null;

                AllVoltageDefenseBigStruct ret = new AllVoltageDefenseBigStruct();
                List<ushort> values = new List<ushort>();
                for (int i = 0; i < AllVoltageDefenseBigStruct.COUNT; i++)
                {
                    values.AddRange(this._allExternalDefense.Rows[AllExternalDefenseBigStruct.CurrentCount + i].GetValues());
                }
                ret.InitStruct(Common.TOBYTES(values, false));
                return ret;
            }
            set
            {
                for (int i = 0; i < AllVoltageDefenseBigStruct.COUNT; i++)
                {
                    byte[] values = Common.TOBYTES(value.Rows[i].GetValues(), false);
                    this._allExternalDefense.Rows[AllExternalDefenseBigStruct.CurrentCount + i].InitStruct(values);
                }
            }
        }
    }
}
