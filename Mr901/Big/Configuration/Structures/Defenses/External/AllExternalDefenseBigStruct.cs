﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.Defenses.External
{
    public class AllExternalDefenseBigStruct : StructBase, IDgvRowsContainer<ExternalDefenseBigStruct>
    {
        private const int COUNT = 24;

        private static int _currentCount;
        public static int CurrentCount => _currentCount;

        [Layout(0, Count = COUNT)] private ExternalDefenseBigStruct[] _externalDefenses;

        public ExternalDefenseBigStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }

        public static void SetDeviceExternalDefType(string type)
        {
            switch (type)
            {
                case "A1":
                case "A2":
                case "A3":
                case "A4":
                    _currentCount = 24;
                    break;
                case "A5":
                    _currentCount = 20;
                    break;
                default:
                    _currentCount = 24;
                    break;
            }
        }
    }
}
