﻿using System.Runtime.Remoting.Messaging;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Configuration.Structures.Defenses.Mtz
{
    public class MtzBigStruct : StructBase
    {
        [Layout(0)] private ushort config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort config1;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort block; //вход блокировки
        [Layout(3)] private ushort ust; //уставка срабатывания_
        [Layout(4)] private ushort time; //время срабатывания_ тср
        [Layout(5)] private ushort k; //коэфиц. зависимой хар-ки
        [Layout(6)] private ushort u; //уставка пуска по напряжению Упуск
        [Layout(7)] private ushort tu; //время ускорения_

        #region Защиты I

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string MtzMode
        {
            get { return Validator.Get(this.config, Strings.Mode, 0, 1); }
            set { this.config = Validator.Set(value, Strings.Mode, this.config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string MtzBlock
        {
            get
            {
                return Validator.Get(this.block, Strings.Version < 2.13 ? Strings.InputSignalsBig : Strings.ExtDefSignals213);
            }
            set
            {
                this.block = Validator.Set(value, Strings.Version < 2.13 ? Strings.InputSignalsBig : Strings.ExtDefSignals213);
            }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Измерение")]
        public string MtzMeasure
        {
            get { return Validator.Get(this.config, Strings.MTZJoinBig, 6, 7, 8, 9, 10); }
            set { this.config = Validator.Set(value, Strings.MTZJoinBig, this.config, 6, 7, 8, 9, 10); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка")]
        public double MtzIcp
        {
            get { return ValuesConverterCommon.GetIn(this.ust); }
            set { this.ust = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Характеристика")]
        public string MtzChar
        {
            get { return Validator.Get(this.config, Strings.Characteristic, 3); }
            set { this.config = Validator.Set(value, Strings.Characteristic, this.config, 3); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "tср")]
        public int MtzT
        {
            get { return ValuesConverterCommon.GetWaitTime(this.time); }
            set { this.time = ValuesConverterCommon.SetWaitTime(value); }

        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "К")]
        public ushort MtzK
        {
            get
            {
                if (k == 0) return k = 100;
                return this.k;
            }
            set { this.k = value; }
        }


        [BindingProperty(7)]
        [XmlElement(ElementName = "Осц")]
        public string MtzOsc
        {
            get { return Validator.Get(this.config, Strings.ModesLightOsc, 14, 15); }
            set { this.config = Validator.Set(value, Strings.ModesLightOsc, this.config, 14, 15); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Уров")]
        public bool MtzUrov
        {
            get { return Common.GetBit(this.config, 2); }
            set { this.config = Common.SetBit(this.config, 2, value); }
        }

        #endregion
    }
}