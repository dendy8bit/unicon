﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.Defenses.U
{
    public class AllVoltageDefenseBigStruct : StructBase, IDgvRowsContainer<VoltageDefenseBigStruct>
    {
        public const int COUNT = 4;

        [Layout(0, Count = COUNT)] private VoltageDefenseBigStruct[] _externalDefenses;

        public VoltageDefenseBigStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
