﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.Defenses.Differential
{
  public  class AllDifferentialCurrentBigStruct:StructBase, IDgvRowsContainer<DifferentialCurrentBigStruct>
    {
      #region [Private fields]

      [Layout(0, Count = 6)] private DifferentialCurrentBigStruct[] _differentialCurrents;

      #endregion [Private fields]

      public DifferentialCurrentBigStruct[] Rows
      {
          get { return this._differentialCurrents; }
          set { this._differentialCurrents = value; }
      }
    }
}
