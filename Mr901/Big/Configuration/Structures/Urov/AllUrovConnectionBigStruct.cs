﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.Urov
{

    public class AllUrovConnectionBigStruct : StructBase, IDgvRowsContainer<UrovConnectionBigStruct>
    {
        private const int COUNT_MEMORY = 24;
        public static int CurrentUrovConnectionCount { get; private set; }

        [Layout(0, Count = COUNT_MEMORY)] private UrovConnectionBigStruct[] ust;
        
        public UrovConnectionBigStruct[] Rows
        {
            get { return this.ust; }
            set { this.ust = value; }
        }

        public static void SetDeviceConnectionsType(string type)
        {
            switch (type)
            {
                case "A1":
                    CurrentUrovConnectionCount = 16;
                    break;
                case "A2":
                case "A3":
                case "A4":
                    CurrentUrovConnectionCount = 24;
                    break;
                case "A5":
                case "A6":
                case "A7":
                    CurrentUrovConnectionCount = 20;
                    break;
                default:
                    CurrentUrovConnectionCount = 16;
                    break;
            }
        }
    }
}