﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR901.Big.Configuration.Structures.Tt
{
    public class ControlTtUnionStruct : StructBase
    {
        [Layout(0)] private AllControlTtBigStruct _allControlTtStruct;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Цепи_ТТ_все")]
        public AllControlTtBigStruct AllControlTtStructV203
        {
            get { return this._allControlTtStruct; }
            set { this._allControlTtStruct = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Сброс_неисправности_ТТ")]
        public string InpResertFaultTt
        {
            get { return this._allControlTtStruct.InpResertFaultTt; }
            set { this._allControlTtStruct.InpResertFaultTt = value; }
        }
    }
}
