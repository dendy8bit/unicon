﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.Tt
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>

    public class AllControlTtBigStruct : StructBase, IDgvRowsContainer<ControlTtBigStruct>
    {
        public const int TT_COUNT = 3;
        [Layout(0, Count = TT_COUNT)] public ControlTtBigStruct[] TtStructChannels;
        
        public ControlTtBigStruct[] Rows
        {
            get { return this.TtStructChannels; }
            set { this.TtStructChannels = value; }
        }

        public string InpResertFaultTt
        {
            get { return this.TtStructChannels[0].InpResertFaultTt; }
            set { this.TtStructChannels[0].InpResertFaultTt = value; }
        }
    }
}
