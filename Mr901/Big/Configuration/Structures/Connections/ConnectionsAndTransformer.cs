﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Configuration.Structures.Connections
{
    public class ConnectionsAndTransformer : StructBase
    {
        [Layout(0)] private AllConnectionBigStruct _connections;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Присоединения")]
        public AllConnectionBigStruct Connections
        {
            get { return this._connections; }
            set { this._connections = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Параметры_ТН")]
        public ParametersNTStruct TN
        {
            get
            {
                if(AllConnectionBigStruct.ConnectionsCount > 20) return null;

                ParametersNTStruct ret = new ParametersNTStruct();
                List<ushort> values = new List<ushort>();
                //Только 2 из 4 присоединений выделены под параметры ТН, остальное резерв
                values.AddRange(this._connections.Rows[AllConnectionBigStruct.ConnectionsCount].GetValues());
                values.AddRange(this._connections.Rows[AllConnectionBigStruct.ConnectionsCount + 1].GetValues());
                ret.InitStruct(Common.TOBYTES(values, false));
                return ret;
            }
            set
            {
                byte[] values = Common.TOBYTES(value.GetValues(), false);
                this._connections.Rows[AllConnectionBigStruct.ConnectionsCount].InitStruct(values.Take(12).ToArray());
                this._connections.Rows[AllConnectionBigStruct.ConnectionsCount + 1].InitStruct(values.Skip(12).ToArray());
            }
        }
    }
}
