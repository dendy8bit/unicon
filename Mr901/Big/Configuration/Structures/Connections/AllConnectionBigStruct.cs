﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Big.Configuration.Structures.Connections
{
    /// <summary>
    /// Структура. Все присоединения
    /// </summary>
    public class AllConnectionBigStruct : StructBase, IDgvRowsContainer<ConnectionBigStruct>
    {
        #region [Constants]

        private const int CONNECTIONS_MEMORY_COUNT = 24;
        public static int ConnectionsCount { get; private set; }

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = CONNECTIONS_MEMORY_COUNT)] private ConnectionBigStruct[] _connectionStructs;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Токи присоединений
        /// </summary>
        [XmlIgnore]
        public List<ushort> AllItt
        {
            get
            {
                List<ushort> result = new List<ushort> {this._connectionStructs.Take(ConnectionsCount).Max(o => o.Inom)};
                result.AddRange(this._connectionStructs.Take(ConnectionsCount).Select(oneStruct => oneStruct.Inom));
                return result;
            }
            set
            {
                if(value.Count != ConnectionsCount + 1) throw new Exception("Количество коэффициентов не совпадает с количеством присоединений");

                for (int i = 0; i < ConnectionsCount; i++)
                {
                    this._connectionStructs[i].Inom = value[i + 1];
                }
            }
        }

        /// <summary>
        /// Токи присоединений
        /// </summary>
        [XmlIgnore]
        public List<ushort> AllIttJoin
        {
            get
            {
                try
                {
                    ushort ttMax = _connectionStructs.Take(ConnectionsCount).Where(t => t.JoinJoin != Strings.Join[0])
                        .Max(t => t.Inom);
                    List<ushort> result = new List<ushort> {ttMax};
                    result.AddRange(this._connectionStructs.Take(ConnectionsCount).Select(oneStruct => oneStruct.Inom));
                    return result;
                }
                catch
                {
                    List<ushort> result = new List<ushort> { this._connectionStructs.Take(ConnectionsCount).Max(o => o.Inom) };
                    result.AddRange(this._connectionStructs.Take(ConnectionsCount).Select(oneStruct => oneStruct.Inom));
                    return result;
                }
            }
        }

        #endregion [Properties]

        public ConnectionBigStruct[] Rows
        {
            get { return this._connectionStructs; }
            set { this._connectionStructs = value; }
        }

        public static void SetDeviceConnectionsType(string type)
        {
            switch (type)
            {
                case "A1":
                    ConnectionsCount = 16;
                    break;
                case "A2":
                    ConnectionsCount = 24;
                    break;
                case "A3":
                    ConnectionsCount = 24;
                    break;
                case "A4":
                    ConnectionsCount = 24;
                    break;
                case "A5":
                case "A6":
                case "A7":
                    ConnectionsCount = 20;
                    break;
                default:
                    ConnectionsCount = 16;
                    break;
            }
        }
    }
}