﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Big.Configuration.Structures.ConfigSystem
{
    public class ConfigLogicIndicator : StructBase
    {
        [Layout(0)] private ushort _config;
	    [Layout(1)] private ushort _rez1;

		[BindingProperty(0)]
        public string IndConf
        {
			get { return Validator.Get(this._config, Strings.ConfigInd, 0, 1); }
	        set { this._config = Validator.Set(value, Strings.ConfigInd, this._config, 0, 1); }
		}
        
    }
}
