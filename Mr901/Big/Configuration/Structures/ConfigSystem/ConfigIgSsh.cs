﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901.Big.Configuration213.Structures.ConfigSystem
{
    public class ConfigIgSsh : StructBase
    {
        [Layout(0)] private ushort _config;

        [BindingProperty(0)]
        public string IndConf
        {
            get { return Validator.Get(this._config, Strings.ConfigIgssh, 0, 1); }
            set { this._config = Validator.Set(value, Strings.ConfigIgssh, this._config, 0, 1); }
        }

    }
}
