﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901.Big.AlarmJournal.Structures;
using BEMN.MR901.Big.Configuration.Structures;
using BEMN.MR901.Big.Configuration.Structures.Connections;

namespace BEMN.MR901.Big.AlarmJournal
{
    public partial class Mr901AlarmJournalBigForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР901_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _refreshAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordBigStruct> _alarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordBigUStruct> _alarmJournalU;
        private readonly MemoryEntity<ConnectionsAndTransformer> _currentConnectionsMemory;
        private DataTable _table;
        private int _recordNumber;
        private Mr901Device _device;
        private ConnectionsAndTransformer _currentConnectionsAndVoltage;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr901AlarmJournalBigForm()
        {
            this.InitializeComponent();
        }

        public Mr901AlarmJournalBigForm(Mr901Device device)
        {
            this.InitializeComponent();
            this._device = device;

            this._refreshAlarmJournal = new MemoryEntity<OneWordStruct>("Номер записи ЖА", device, 0x700);
            this._refreshAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournalRec);
            this._refreshAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;

            if (ConfigurationStructBigV210.DeviceModelType == "A5"
                || ConfigurationStructBigV210.DeviceModelType == "A6"
                || ConfigurationStructBigV210.DeviceModelType == "A7")
            {
                this._alarmJournalU = new MemoryEntity<AlarmJournalRecordBigUStruct>("Журнал аварий МР901", device, 0x700);
                this._alarmJournalU.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
                this._alarmJournalU.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            }
            else
            {
                this._alarmJournal = new MemoryEntity<AlarmJournalRecordBigStruct>("Журнал аварий МР901", device, 0x700);
                this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
                this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            }

            this._currentConnectionsMemory = new MemoryEntity<ConnectionsAndTransformer>("Присоединения (ЖА)", device, 0x106A);
            this._currentConnectionsMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                Strings.Version = Common.VersionConverter(this._device.DeviceVersion);
                ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
                this._currentConnectionsAndVoltage = this._currentConnectionsMemory.Value;
                this._recordNumber = 0;
                this.SaveRecNumber();
            });
            this._currentConnectionsMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
        } 
        #endregion [Ctor's]


        #region [Help members]
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            if (this._recordNumber == 0)
            {
                this._alarmJournal?.RemoveStructQueries();
                this._alarmJournalU?.RemoveStructQueries();
            }
        }

        private void SaveRecNumber()
        {
            this._refreshAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._refreshAlarmJournal.SaveStruct();
        }

        private void StartReadJournalRec()
        {
            if (this._alarmJournalU != null)
            {
                this._alarmJournalU.LoadStruct();
            }
            else
            {
                this._alarmJournal.LoadStruct();
            }
        }

        private void ReadRecord(StructBase structBase)
        {
            if (structBase is AlarmJournalRecordBigUStruct)
            {
                AlarmJournalRecordBigUStruct record = (AlarmJournalRecordBigUStruct) structBase;
                if (!record.IsEmpty)
                {
                    this._recordNumber++;
                    this._table.Rows.Add(record.GetRecord(this._recordNumber, this._currentConnectionsAndVoltage, this._device.DevicePlant));
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.SaveRecNumber();
                }
                else
                {
                    if (this._recordNumber == 0)
                    {
                        this._statusLabel.Text = "Журнал пуст";
                    }
                }
            }
            else
            {
                AlarmJournalRecordBigStruct record = (AlarmJournalRecordBigStruct) structBase;
                if (!record.IsEmpty)
                {
                    this._recordNumber++;
                    this._table.Rows.Add(record.GetRecord(this._recordNumber, this._currentConnectionsAndVoltage.Connections.AllItt, this._device.DevicePlant));
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.SaveRecNumber();
                }
                else
                {
                    if (this._recordNumber == 0)
                    {
                        this._statusLabel.Text = "Журнал пуст";
                    }
                }
            }
        }

        private void ReadRecord()
        {
            if (this._alarmJournalU != null)
            {
                this.ReadRecord(this._alarmJournalU.Value);
            }
            else
            {
                this.ReadRecord(this._alarmJournal.Value);
            }
        }

        private DataTable GetJournalDataTable()
        {
            switch (this._device.DevicePlant)
            {
                case "A1":
                    this._alarmJournalGrid.Columns.Remove(this._I17Col);
                    this._alarmJournalGrid.Columns.Remove(this._I18Col);
                    this._alarmJournalGrid.Columns.Remove(this._I19Col);
                    this._alarmJournalGrid.Columns.Remove(this._I20Col);
                    this._alarmJournalGrid.Columns.Remove(this._I21Col);
                    this._alarmJournalGrid.Columns.Remove(this._I22Col);
                    this._alarmJournalGrid.Columns.Remove(this._I23Col);
                    this._alarmJournalGrid.Columns.Remove(this._I24Col);
                    this._alarmJournalGrid.Columns.Remove(this._ua);
                    this._alarmJournalGrid.Columns.Remove(this._ub);
                    this._alarmJournalGrid.Columns.Remove(this._uc);
                    this._alarmJournalGrid.Columns.Remove(this._un);
                    this._alarmJournalGrid.Columns.Remove(this._uab);
                    this._alarmJournalGrid.Columns.Remove(this._ubc);
                    this._alarmJournalGrid.Columns.Remove(this._uca);
                    this._alarmJournalGrid.Columns.Remove(this._u0);
                    this._alarmJournalGrid.Columns.Remove(this._u2);
                    break;
                case "A2":
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    this._alarmJournalGrid.Columns.Remove(this._ua);
                    this._alarmJournalGrid.Columns.Remove(this._ub);
                    this._alarmJournalGrid.Columns.Remove(this._uc);
                    this._alarmJournalGrid.Columns.Remove(this._un);
                    this._alarmJournalGrid.Columns.Remove(this._uab);
                    this._alarmJournalGrid.Columns.Remove(this._ubc);
                    this._alarmJournalGrid.Columns.Remove(this._uca);
                    this._alarmJournalGrid.Columns.Remove(this._u0);
                    this._alarmJournalGrid.Columns.Remove(this._u2);
                    break;
                case "A3":
                    this._alarmJournalGrid.Columns.Remove(this._D3Col);
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    this._alarmJournalGrid.Columns.Remove(this._ua);
                    this._alarmJournalGrid.Columns.Remove(this._ub);
                    this._alarmJournalGrid.Columns.Remove(this._uc);
                    this._alarmJournalGrid.Columns.Remove(this._un);
                    this._alarmJournalGrid.Columns.Remove(this._uab);
                    this._alarmJournalGrid.Columns.Remove(this._ubc);
                    this._alarmJournalGrid.Columns.Remove(this._uca);
                    this._alarmJournalGrid.Columns.Remove(this._u0);
                    this._alarmJournalGrid.Columns.Remove(this._u2);
                    break;
                case "A4":
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    this._alarmJournalGrid.Columns.Remove(this._ua);
                    this._alarmJournalGrid.Columns.Remove(this._ub);
                    this._alarmJournalGrid.Columns.Remove(this._uc);
                    this._alarmJournalGrid.Columns.Remove(this._un);
                    this._alarmJournalGrid.Columns.Remove(this._uab);
                    this._alarmJournalGrid.Columns.Remove(this._ubc);
                    this._alarmJournalGrid.Columns.Remove(this._uca);
                    this._alarmJournalGrid.Columns.Remove(this._u0);
                    this._alarmJournalGrid.Columns.Remove(this._u2);
                    break;
                case "A5":
                    this._alarmJournalGrid.Columns.Remove(this._I21Col);
                    this._alarmJournalGrid.Columns.Remove(this._I22Col);
                    this._alarmJournalGrid.Columns.Remove(this._I23Col);
                    this._alarmJournalGrid.Columns.Remove(this._I24Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    break;
                default:
                    this._alarmJournalGrid.Columns.Remove(this._I17Col);
                    this._alarmJournalGrid.Columns.Remove(this._I18Col);
                    this._alarmJournalGrid.Columns.Remove(this._I19Col);
                    this._alarmJournalGrid.Columns.Remove(this._I20Col);
                    this._alarmJournalGrid.Columns.Remove(this._I21Col);
                    this._alarmJournalGrid.Columns.Remove(this._I22Col);
                    this._alarmJournalGrid.Columns.Remove(this._I23Col);
                    this._alarmJournalGrid.Columns.Remove(this._I24Col);
                    this._alarmJournalGrid.Columns.Remove(this._D3Col);
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._D5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    this._alarmJournalGrid.Columns.Remove(this._D7Col);
                    break;
            }

            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].HeaderText);
            }
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void Mr901AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }
        private void Mr901AlarmJournalBigForm_Activated(object sender, EventArgs e)
        {
            ConfigurationStructBigV210.DeviceModelType = this._device.DevicePlant;
        }

        private void StartReadBtn_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._statusLabel.Text = READ_AJ;
            this._currentConnectionsMemory.LoadStruct();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
                {
                    this._table.Clear();
                    StructBase journalRec;
                    if (ConfigurationStructBigV210.DeviceModelType == "A5"
                        || ConfigurationStructBigV210.DeviceModelType == "A6"
                        || ConfigurationStructBigV210.DeviceModelType == "A7")
                    {
                        journalRec = new AlarmJournalRecordBigUStruct();
                    }
                    else
                    {
                        journalRec = new AlarmJournalRecordBigStruct();
                    }
                    if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
                    {
                        byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                        int size = journalRec.GetSize();
                        int connectionSize = (file.Length - 16)%size == 0 ? 16 : 24;
                        ushort[] buff = Common.TOWORDS(file.Skip(file.Length - connectionSize).ToArray(), true);
                        List<ushort> factors = new List<ushort>();
                        factors.AddRange(buff.Take(AllConnectionBigStruct.ConnectionsCount));
                        factors.Insert(0, buff.Take(AllConnectionBigStruct.ConnectionsCount).Max());
                        
                        this._currentConnectionsAndVoltage = this._currentConnectionsAndVoltage ??
                                                             new ConnectionsAndTransformer();

                        this._currentConnectionsAndVoltage.Connections.AllItt = factors;

                        if (journalRec is AlarmJournalRecordBigUStruct)
                        {
                            this._currentConnectionsAndVoltage.TN.SetKthl(buff[buff.Length-2]);
                            this._currentConnectionsAndVoltage.TN.SetKthx(buff[buff.Length-1]);
                        }

                        int index = 0;
                        this._recordNumber = 0;
                        while (index < file.Length - connectionSize)
                        {
                            List<byte> journalBytes = new List<byte>();
                            journalBytes.AddRange(Common.SwapListItems(file.Skip(index).Take(size)));
                            journalRec.InitStruct(journalBytes.ToArray());
                            this.ReadRecord(journalRec);
                            index += size;
                        }
                    }
                    else
                    {
                        this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                        this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
                        this._alarmJournalGrid.Refresh();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Невозможно открыть файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _saveJournaloToHTML(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmHTMLdialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveAlarmHTMLdialog.FileName, "МР901. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        #endregion [Event Handlers]



        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr901Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr901AlarmJournalBigForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]

        
    }
}
