﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR901.Big.Configuration.Structures.Connections;

namespace BEMN.MR901.Big.AlarmJournal.Structures
{
    class AlarmJournalRecordBigUStruct: StructBase
    {
        #region [Constants]
        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        private const string SPL_PATTERN = "СПЛ {0}"; 
        #endregion [Constants]


        #region [Static fields]
        /// <summary>
        /// Номер сообщения
        /// </summary>
        public static int Number = 0; 
        #endregion [Static fields]


        #region [Private fields]
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _stage;
        [Layout(9)] private ushort _numberOfTriggeredParametr;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _groupOfSetpoints;

        [Layout(12)] private ushort _ida1;
        [Layout(13)] private ushort _idb1;
        [Layout(14)] private ushort _idc1;

        [Layout(15)] private ushort _ida2;
        [Layout(16)] private ushort _idb2;
        [Layout(17)] private ushort _idc2;

        [Layout(18)] private ushort _ida3;
        [Layout(19)] private ushort _idb3;
        [Layout(20)] private ushort _idc3;

        [Layout(21)] private ushort _ita1;
        [Layout(22)] private ushort _itb1;
        [Layout(23)] private ushort _itc1;

        [Layout(24)] private ushort _ita2;
        [Layout(25)] private ushort _itb2;
        [Layout(26)] private ushort _itc2;

        [Layout(27)] private ushort _ita3;
        [Layout(28)] private ushort _itb3;
        [Layout(29)] private ushort _itc3;

        [Layout(30)] private ushort _i1;
        [Layout(31)] private ushort _i2;
        [Layout(32)] private ushort _i3;
        [Layout(33)] private ushort _i4;
        [Layout(34)] private ushort _i5;
        [Layout(35)] private ushort _i6;
        [Layout(36)] private ushort _i7;
        [Layout(37)] private ushort _i8;
        [Layout(38)] private ushort _i9;
        [Layout(39)] private ushort _i10;
        [Layout(40)] private ushort _i11;
        [Layout(41)] private ushort _i12;
        [Layout(42)] private ushort _i13;
        [Layout(43)] private ushort _i14;
        [Layout(44)] private ushort _i15;
        [Layout(45)] private ushort _i16;
        [Layout(46)] private ushort _i17;
        [Layout(47)] private ushort _i18;
        [Layout(48)] private ushort _i19;
        [Layout(49)] private ushort _i20;
        [Layout(50)] private ushort _ua;
        [Layout(51)] private ushort _ub;
        [Layout(52)] private ushort _uc;
        [Layout(53)] private ushort _un;
        [Layout(54)] private ushort _d1;
        [Layout(55)] private ushort _d2;
        [Layout(56)] private ushort _d3;
        [Layout(57)] private ushort _d4;
        [Layout(58)] private ushort _d5;
        [Layout(59)] private ushort _d6;
        [Layout(60)] private ushort _spl;
        [Layout(61)] private ushort _uab;
        [Layout(62)] private ushort _ubc;
        [Layout(63)] private ushort _uca;
        [Layout(64)] private ushort _3u0;
        [Layout(65)] private ushort _u2;
        [Layout(66)] private ushort _u1Reserve;
        #endregion [Private fields]
        //для того, чтобы вычитывать одним запросом всю запись, потому что в устройстве есть автоинкремент
        public override object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            slotLen = GetSize()/2;
            return base.GetSlots(start, slotArray, slotLen);
        }

        #region [Properties]
        /// <summary>
        /// Сработанный параметр
        /// </summary>
        private string GetTriggedOption
        {
            get { return Strings.AlarmJournalTriggedOptionBig[this._numberOfTriggeredParametr]; }
        }

        /// <summary>
        /// //Значение сработанного параметра
        /// </summary>
        private string GetValueTriggedOption(ConnectionsAndTransformer cnt)
        {
            if (this._numberOfTriggeredParametr < 18)
                return ValuesConverterCommon.Analog.GetI(this._valueOfTriggeredParametr, cnt.Connections.AllItt[0]*40);

            if (this._numberOfTriggeredParametr < 38)
            {
                int parametr = this._numberOfTriggeredParametr - 18;
                return ValuesConverterCommon.Analog.GetI(this._valueOfTriggeredParametr, cnt.Connections.AllItt[parametr + 1]*40);
            }

            if (this._numberOfTriggeredParametr >= 38 && this._numberOfTriggeredParametr < 41)
            {
                return ValuesConverterCommon.Analog.GetU(this._valueOfTriggeredParametr, cnt.TN.KthlValue);
            }
            if (this._numberOfTriggeredParametr == 41)
            {
                return ValuesConverterCommon.Analog.GetU(this._valueOfTriggeredParametr, cnt.TN.KthxValue);
            }

            if (this._numberOfTriggeredParametr < 44)
                return Strings.AlarmJournalExternalDefenseTriggedOption[this._valueOfTriggeredParametr];
            if (this._numberOfTriggeredParametr == 44)
                return string.Format(SPL_PATTERN, this._valueOfTriggeredParametr);
            // Uab, Ubc..
            if (this._numberOfTriggeredParametr > 45 && this._numberOfTriggeredParametr < 48)
            {
                return ValuesConverterCommon.Analog.GetU(this._valueOfTriggeredParametr, cnt.TN.KthlValue);
            }
            if (this._numberOfTriggeredParametr == 48)
            {
                return string.Format("ЖА СПЛ {0}", this._valueOfTriggeredParametr);
            }
            return string.Empty;
        }

        /// <summary>
        /// Группа уставок
        /// </summary>
        private string GetGroupOfSetpoints
        {
            get { return Strings.AlarmJournalSetpointsGroup[this._groupOfSetpoints]; }
        }
        /// <summary>
        /// Запись аварии
        /// </summary>
        public object[] GetRecord(int number, ConnectionsAndTransformer cnt, string type)
        {
            List<int> factors = cnt.Connections.AllItt.Select(c=>c*40).ToList();

            List<object> ret = new List<object>
            {
                number, //Номер сообщения
                this.GetTime, //Время сообщения
                Strings.AlarmJournalMessage[this._message], //Сообщение
                Strings.AlarmJournalStageBig[this._stage], //Ступень
                this.GetTriggedOption, //Сработанный параметр
                this.GetValueTriggedOption(cnt), //Значение сработанного параметра
                this.GetGroupOfSetpoints, //Группа уставок
                ValuesConverterCommon.Analog.GetI(this._ida1, factors[0]), //Ид1
                ValuesConverterCommon.Analog.GetI(this._ita1, factors[0]), //Ит1
                ValuesConverterCommon.Analog.GetI(this._ida2, factors[0]), //Ид2
                ValuesConverterCommon.Analog.GetI(this._ita2, factors[0]), //Ит2
                ValuesConverterCommon.Analog.GetI(this._ida3, factors[0]), //Ид3
                ValuesConverterCommon.Analog.GetI(this._ita3, factors[0]), //Ит3
                ValuesConverterCommon.Analog.GetI(this._i1, factors[1]), //И1
                ValuesConverterCommon.Analog.GetI(this._i2, factors[2]), //И2
                ValuesConverterCommon.Analog.GetI(this._i3, factors[3]), //И3
                ValuesConverterCommon.Analog.GetI(this._i4, factors[4]), //И4
                ValuesConverterCommon.Analog.GetI(this._i5, factors[5]), //И5
                ValuesConverterCommon.Analog.GetI(this._i6, factors[6]), //И6
                ValuesConverterCommon.Analog.GetI(this._i7, factors[7]), //И7
                ValuesConverterCommon.Analog.GetI(this._i8, factors[8]), //И8
                ValuesConverterCommon.Analog.GetI(this._i9, factors[9]), //И9
                ValuesConverterCommon.Analog.GetI(this._i10, factors[10]), //И10
                ValuesConverterCommon.Analog.GetI(this._i11, factors[11]), //И11
                ValuesConverterCommon.Analog.GetI(this._i12, factors[12]), //И12
                ValuesConverterCommon.Analog.GetI(this._i13, factors[13]), //И13
                ValuesConverterCommon.Analog.GetI(this._i14, factors[14]), //И14
                ValuesConverterCommon.Analog.GetI(this._i15, factors[15]), //И15
                ValuesConverterCommon.Analog.GetI(this._i16, factors[16]), //И16
            };
            switch (type)
            {
                case "A5":
                {
                    var trans = cnt.TN;
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i17, factors[17])); //И17
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i18, factors[18])); //И18
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i19, factors[19])); //И19
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i20, factors[20])); //И20
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ua, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ub, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uc, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._un, trans.KthxValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uab, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ubc, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uca, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._3u0, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._u2, trans.KthlValue));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d1)));
                    ret.Add(this.GetMask(Common.HIBYTE(this._d1)));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d2)));
                    ret.Add(this.GetMask(Common.HIBYTE(this._d2)));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d3)));
                    break;
                }
                case "A6":
                {
                    var trans = cnt.TN;
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i17, factors[17])); //И17
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i18, factors[18])); //И18
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i19, factors[19])); //И19
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i20, factors[20])); //И20
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ua, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ub, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uc, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._un, trans.KthxValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uab, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ubc, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uca, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._3u0, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._u2, trans.KthlValue));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d1)));
                    ret.Add(this.GetMask(Common.HIBYTE(this._d1)));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d2)));
                    break;
                }
                case "A7":
                {
                    var trans = cnt.TN;
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i17, factors[17])); //И17
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i18, factors[18])); //И18
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i19, factors[19])); //И19
                    ret.Add(ValuesConverterCommon.Analog.GetI(this._i20, factors[20])); //И20
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ua, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ub, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uc, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._un, trans.KthxValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uab, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._ubc, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._uca, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._3u0, trans.KthlValue));
                    ret.Add(ValuesConverterCommon.Analog.GetU(this._u2, trans.KthlValue));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d1)));
                    ret.Add(this.GetMask(Common.HIBYTE(this._d1)));
                    ret.Add(this.GetMask(Common.LOBYTE(this._d2)));
                    ret.Add(this.GetMask(Common.HIBYTE(this._d2)));
                    break;
                }
            }
            return ret.ToArray();
        }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this._year +
                          this._month +
                          this._date +
                          this._hour +
                          this._minute +
                          this._second +
                          this._millisecond;
                return sum == 0;
            }
        }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        private string GetTime
        {
            get
            {
                return string.Format
                   (
                       DATE_TIME_PATTERN,
                       this._date,
                       this._month,
                       this._year,
                       this._hour,
                       this._minute,
                       this._second,
                       this._millisecond
                   );
            }
        }
        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Ивертирует двоичное представление числа
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Инвертированое двоичное представление</returns>
        private string GetMask(ushort value)
        {
            var chars = Convert.ToString(value, 2).PadLeft(8, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        } 
        #endregion [Help members]
    }
}
