﻿using System;
using BEMN.Devices;
using BEMN.MBServer;
using System.Xml.Serialization;
using System.ComponentModel;
using BEMN.MBServer.Queries;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MR901.Big.AlarmJournal;
using BEMN.MR901.Big.Configuration;
using BEMN.MR901.Big.Configuration.Structures;
using BEMN.MR901.Big.Configuration.Structures.Osc;
using BEMN.MR901.Big.Measuring;
using BEMN.MR901.Big.Osc;
using BEMN.MR901.Old.AlarmJournal;
using BEMN.MR901.Old.AlarmJournal.Structures;
using BEMN.MR901.Old.BSBGL;
using BEMN.MR901.Old.Configuration;
using BEMN.MR901.Old.Configuration.Structures;
using BEMN.MR901.Old.Configuration.Structures.Connections;
using BEMN.MR901.Old.Configuration.Structures.Osc;
using BEMN.MR901.Old.ConfigurationV203;
using BEMN.MR901.Old.ConfigurationV203.Structures;
using BEMN.MR901.Old.HelpClasses;
using BEMN.MR901.Old.Measuring;
using BEMN.MR901.Old.Measuring.Structures;
using BEMN.MR901.Old.Osc;
using BEMN.MR901.Old.Osc.Structures;
using BEMN.MR901.Old.SystemJournal;
using BEMN.Framework;

namespace BEMN.MR901
{
    public class Mr901Device : Device, IDeviceView, IDeviceVersion
    {
        #region Константы

        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;
        private const int ALARM_JOURNAL_START_ADRESS = 0x700;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int CURRENT_CONNECTIONS_START_ADRESS = 0x1028;
        private const int OSC_OPTIONS_START_ADRESS = 0x5a0;

        private const string LOW_VERSION = "1.00 - 1.03";
        private const string HIGH_VERSION = "2.00 - 2.02";

        #endregion

        #region События
        public event Action ConfigWriteOk;
        public event Action ConfigWriteFail;
        #endregion

        #region Поля
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        private CurrentOptionsLoader _currentOptionsLoader;

        private MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<AllConnectionStruct> _connectionsAlarmJournal;
        private MemoryEntity<AllConnectionStruct> _connectionsMeasuring;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscilloscopeSettingsStruct> _oscilloscopeSettings;
        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<ConfigurationStructV203> _configurationV203;
        private MemoryEntity<DateTimeStruct> _dateTime;
        #endregion

        #region Свойства
        
        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return this._oscJournal; }
        }

        public MemoryEntity<AllConnectionStruct> ConnectionsMeasuring
        {
            get { return this._connectionsMeasuring; }
        }

        public MemoryEntity<OneWordStruct> RefreshSystemJournal
        {
            get { return this._refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return this._refreshAlarmJournal; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return this._alarmJournal; }
        }

        public MemoryEntity<AllConnectionStruct> ConnectionsAlarmJournal
        {
            get { return this._connectionsAlarmJournal; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return this._oscOptions; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return this._setOscStartPage; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        public CurrentOptionsLoader CurrentOptionsLoader
        {
            get { return this._currentOptionsLoader; }
        }

        public MemoryEntity<OscilloscopeSettingsBigStruct213> AllChannels { get; private set; }

        public MemoryEntity<OscilloscopeSettingsStruct> OscilloscopeSettings
        {
            get { return this._oscilloscopeSettings; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return this._configuration; }
        }

        public MemoryEntity<ConfigurationStructV203> ConfigurationV203
        {
            get { return this._configurationV203; }
        }

        public MemoryEntity<ConfigurationStructBigV213> ConfigurationBigV213 { get; private set; }
        public MemoryEntity<ConfigurationStructBigV210> ConfigurationBigV210 { get; private set; }

        #endregion

        #region Programming

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;


        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }
        
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }
        #endregion

        #region Конструкторы и инициализация

        public Mr901Device()
        {
            HaveVersion = true;
        }

        public Mr901Device(Modbus mb)
        {
            this.MB = mb;
            this.InitAddr();
            HaveVersion = true;
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null != mb)
                {    
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        private void InitAddr()
        {
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы", this, SYSTEM_JOURNAL_START_ADRESS);
            this._refreshSystemJournal = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);
            this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", this, ALARM_JOURNAL_START_ADRESS);
            this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", this, ALARM_JOURNAL_START_ADRESS);
            this._connectionsAlarmJournal = new MemoryEntity<AllConnectionStruct>("Токи присоединений для ЖА", this, CURRENT_CONNECTIONS_START_ADRESS);
            this.AllChannels = new MemoryEntity<OscilloscopeSettingsBigStruct213>("Все каналы осциллографа", this, 0x10FE);

            this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000);
            this._configurationV203 = new MemoryEntity<ConfigurationStructV203>("Конфигурация v2.03 и выше", this, 0x1000);
            this._connectionsMeasuring = new MemoryEntity<AllConnectionStruct>("Токи присоединений для измерений", this, CURRENT_CONNECTIONS_START_ADRESS);
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, ANALOG_DATABASE_START_ADRESS);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, DISCRET_DATABASE_START_ADRESS);
            this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", this, OSC_JOURNAL_START_ADRESS);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, OSC_JOURNAL_START_ADRESS);
            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Настройки осциллографа", this, OSC_OPTIONS_START_ADRESS);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._oscilloscopeSettings = new MemoryEntity<OscilloscopeSettingsStruct>("Уставки осциллографа", this, 0x108C);

            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
            this._programPageStruct = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);

            this._currentOptionsLoader = new CurrentOptionsLoader(this._connectionsAlarmJournal);
        }

        private new void mb_CompleteExchange(object sender, Query query)
        {
            if ("MR901ConfirmConfig" + DeviceNumber == query.name)
            {
                if (query.fail == 0)
                {
                    this.ConfigWriteOk?.Invoke();
                }
                else
                {
                    this.ConfigWriteFail?.Invoke();
                }
            }
            base.mb_CompleteExchange(sender, query);
        }
        #endregion

        #region IDeviceVersion Members

        private static string[] _deviceApparatConfig =
        {
            "T16N0D64R43",
            "T24N0D40R35",
            "T24N0D24R51",
            "T24N0D32R43",
            "T20N4D40R35",
            "T16N0D24R19"
        };

        private static string[] _deviceOutString = {"A1", "A2", "A3", "A4", "A5", string.Empty};

        public static string GetConfigDevString(string deviceOutString)
        {
            int ind = _deviceOutString.ToList().IndexOf(deviceOutString);
            return _deviceApparatConfig[ind];
        }

        public Type[] Forms
        {
            get
            {
                if (DeviceVersion == LOW_VERSION)
                {
                    DeviceVersion = "1.03";
                }
                if (DeviceVersion == HIGH_VERSION)
                {
                    DeviceVersion = "2.00";
                }

                double vers = Common.VersionConverter(DeviceVersion);
                Strings.Version = vers;

                if (vers >= 2.10)
                {
                    //if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                    //{
                    //    ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                    //    choice.ShowDialog();
                    //    DevicePlant = choice.DeviceType;
                    //    DeviceType = "MR901";
                    //}

                    Strings.DeviceType = DevicePlant;

                    if (vers <= 2.13)
                    {
                        this.ConfigurationBigV210 = new MemoryEntity<ConfigurationStructBigV210>("Конфигурация от вер. 2.10", this, 0x1000);
                    }
                    if (vers >= 2.13)
                    {
                        this.ConfigurationBigV213 = new MemoryEntity<ConfigurationStructBigV213>("Конфигурация от вер. 2.13", this, 0x1000);
                    }
                    return new[]
                    {
                        typeof(BSBGLEF),
                        typeof(Mr901AlarmJournalBigForm),
                        typeof(Mr901BigConfigurationForm),
                        typeof(Mr901MeasuringBigForm),
                        typeof(OscilloscopeNewForm),
                        typeof(Mr901SystemJournalForm)
                    };
                }
                if (vers >= 2.03 && vers < 2.10)
                {
                    return new[]
                    {
                        typeof(BSBGLEF),
                        typeof(Mr901AlarmJournalForm),
                        typeof(Mr901ConfigurationFormV203),
                        typeof(Mr901MeasuringForm),
                        typeof(Mr901OscilloscopeForm),
                        typeof(Mr901SystemJournalForm)
                    };
                }
                return new[]
                {
                    typeof(BSBGLEF),
                    typeof(Mr901AlarmJournalForm),
                    typeof(Mr901ConfigurationForm),
                    typeof(Mr901MeasuringForm),
                    typeof(Mr901OscilloscopeForm),
                    typeof(Mr901SystemJournalForm)
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                    {
                        LOW_VERSION,
                        HIGH_VERSION,
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "2.10",
                        "2.11",
                        "2.12",
						"2.13"
                    };
            }
        }
        #endregion
        
        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Mr901Device); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.mrBig;
        

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР901 (до в.2.13)"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0x0D00, true, "MR901ConfirmConfig" + DeviceNumber, this);
        }
    }
}
