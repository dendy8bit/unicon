﻿using System.Collections.Generic;
using BEMN.Forms.SystemJournal;

namespace BEMN.MR901.Old.SystemJournal
{
    public class SystemJournalStruct : SystemJournalAbstract
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d3}";
        private const string MESSAGE_WITH_CODE_PATTERN = "{0} ({1})";
        private const string MESSAGE_SPL_PATTERN = "СООБЩЕНИЕ ЖС СПЛ N{0}";

        #endregion [Constants]


        #region [Properties]
        public static double CurrentVersion { get; set; }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public override string GetRecordTime => string.Format
            (
                DATE_TIME_PATTERN,
                _date,
                _month,
                _year,
                _hour,
                _minute,
                _second,
                _millisecond
            );

        /// <summary>
        /// Сообщения журнала событий
        /// </summary>
        public List<string> SystemJournalMessages => CurrentVersion >= 2.10 ? this.List2 : this.List1;

        private List<string> List1 => new List<string>
        {
            "ОШИБОЧНОЕ СООБЩЕНИЕ",
            "УСТРОЙСТВО ВЫКЛЮЧЕНО",
            "УСТРОЙСТВО ВКЛЮЧЕНО",
            "УСТАВКИ ИЗМЕНЕНЫ",
            "СБРОС ЖУРНАЛА СИСТЕМЫ",
            "СБРОС ЖУРНАЛА АВАРИЙ ",
            "СБРОС ОСЦИЛЛОГРАФА ",
            "ОШИБКА МОДУЛЯ 1 ",
            "НОРМА МОДУЛЯ 1",
            "ОШИБКА МОДУЛЯ 2",
            "НОРМА МОДУЛЯ 2",
            "ОШИБКА МОДУЛЯ 3",
            "НОРМА МОДУЛЯ 3",
            "ОШИБКА МОДУЛЯ 4",
            "НОРМА МОДУЛЯ 4",
            "ОШИБКА МОДУЛЯ 5",
            "НОРМА МОДУЛЯ 5",
            "ОШИБКА ШИНЫ SPI",
            "НОРМА ШИНЫ SPI",
            "ОШИБКА ШИНЫ MCBSP",
            "НОРМА ШИНЫ MCBSP",
            "ОШИБКА УСТАВОК",
            "ОШИБКА ГРУППЫ УСТАВОК",
            "ОШИБКА ПАРОЛЯ",
            "ОШИБКА ЖУРНАЛА АВАРИЙ",
            "ОШИБКА ЖУРНАЛА СИСТЕМЫ",
            "ОШИБКА ОСЦИЛЛОГРАФА",
            "МЕНЮ: УСТАВКИ ИЗМЕНЕНЫ",
            "СДТУ: УСТАВКИ ИЗМЕНЕНЫ",
            "НЕИСПРАВНОСТЬ ЦЕПЕЙ ТТ СШ1",
            "НОРМА ЦЕПЕЙ ТТ СШ1",
            "НЕИСПРАВНОСТЬ ЦЕПЕЙ ТТ СШ2",
            "НОРМА ЦЕПЕЙ ТТ СШ2",
            "НЕИСПРАВНОСТЬ ЦЕПЕЙ ТТ ПО",
            "НОРМА ЦЕПЕЙ ТТ ПО",
            "ОШИБКА ЧАСТОТЫ",
            "НОРМА  ЧАСТОТЫ",
            "МЕНЮ: ОСНОВНАЯ ГРУППА",
            "МЕНЮ: РЕЗЕРВНАЯ ГРУППА",
            "СДТУ: ОСНОВНАЯ ГРУППА",
            "СДТУ: РЕЗЕРВНАЯ ГРУППА",
            "ВНЕШНЯЯ РЕЗЕРВНАЯ ГРУППА УСТАВОК",
            "СБРОС ВНЕШНЕЙ РЕЗЕРВНОЙ ГРУППЫ",
            "ГРУППА УСТАВОК ИЗМЕНЕНА",
            "ПАРОЛЬ ИЗМЕНЕН",
            "МЕНЮ: СБРОС ИНДИКАЦИИ",
            "СДТУ: СБРОС ИНДИКАЦИИ",
            "ДИСКРЕТ: СБРОС ИНДИКАЦИИ",
            "УРОВ ОТКЛЮЧЕНИЕ СШ1",
            "УРОВ ОТКЛЮЧЕНИЕ СШ2",
            "УРОВ ОТКЛЮЧЕНИЕ ПО",
            "УРОВ ПРИСОЕДИНЕНИЕ 1",
            "УРОВ ПРИСОЕДИНЕНИЕ 2",
            "УРОВ ПРИСОЕДИНЕНИЕ 3",
            "УРОВ ПРИСОЕДИНЕНИЕ 4",
            "УРОВ ПРИСОЕДИНЕНИЕ 5",
            "УРОВ ПРИСОЕДИНЕНИЕ 6",
            "УРОВ ПРИСОЕДИНЕНИЕ 7",
            "УРОВ ПРИСОЕДИНЕНИЕ 8",
            "УРОВ ПРИСОЕДИНЕНИЕ 9",
            "УРОВ ПРИСОЕДИНЕНИЕ 10",
            "УРОВ ПРИСОЕДИНЕНИЕ 11",
            "УРОВ ПРИСОЕДИНЕНИЕ 12",
            "УРОВ ПРИСОЕДИНЕНИЕ 13",
            "УРОВ ПРИСОЕДИНЕНИЕ 14",
            "УРОВ ПРИСОЕДИНЕНИЕ 15",
            "УРОВ ПРИСОЕДИНЕНИЕ 16",
            "СДТУ: ЛОГИКА ИЗМЕНЕНА",
            "СДТУ: КОНСТАНТЫ ЛОГИКИ ИЗМЕНЕНЫ ",
            "МЕНЮ: КОНСТАНТЫ  ЛОГИКИ ИЗМЕНЕНЫ ",
            "СДТУ: МЕНЮ ЛОГИКИ ИЗМЕНЕНА",
            "МЕНЮ: ЗАПУСК ЛОГИКИ",
            "СДТУ: ЗАПУСК ЛОГИКИ",
            "МЕНЮ: ОСТАНОВ ЛОГИКИ",
            "СДТУ: ОСТАНОВ ЛОГИКИ",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:ПРОГ.",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:ПАРОЛЬ",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:РАЗРЕШ.",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:КОНФИГ.",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:МЕНЮ",
            "ОШИБКА ЛОГИКИ: ТАЙМ АУТ",
            "ОШИБКА ЛОГИКИ: РАЗМЕР",
            "ОШИБКА ЛОГИКИ: КОМАНДА",
            "ОШИБКА ЛОГИКИ: АРГУМЕНТ",
            "МЕНЮ: СБРОС КОНФИГУРАЦИИ",
            "МЕНЮ: СБРОС СП-ЛОГИКИ",
            "УРОВ: Неисправность блок-конт. прис.1",
            "УРОВ: Неисправность блок-конт. прис.2",
            "УРОВ: Неисправность блок-конт. прис.3",
            "УРОВ: Неисправность блок-конт. прис.4",
            "УРОВ: Неисправность блок-конт. прис.5",
            "УРОВ: Неисправность блок-конт. прис.6",
            "УРОВ: Неисправность блок-конт. прис.7",
            "УРОВ: Неисправность блок-конт. прис.8",
            "УРОВ: Неисправность блок-конт. прис.9",
            "УРОВ: Неисправность блок-конт. прис.10",
            "УРОВ: Неисправность блок-конт. прис.11",
            "УРОВ: Неисправность блок-конт. прис.12",
            "УРОВ: Неисправность блок-конт. прис.13",
            "УРОВ: Неисправность блок-конт. прис.14",
            "УРОВ: Неисправность блок-конт. прис.15",
            "УРОВ: Неисправность блок-конт. прис.16",
            "ВНЕШНИЙ СБРОС НЕИСПРАВНОСТИ ЦЕПЕЙ ТТ",
            "МЕНЮ: СБРОС НЕИСПРАВНОСТИ ЦЕПЕЙ ТТ",
            "СДТУ: СБРОС НЕИСПРАВНОСТИ ЦЕПЕЙ ТТ",
            "ПУСК ОСЦИЛЛОГРАФА ОТ ДИСКРЕТНОГО СИГНАЛА",
            "МЕНЮ: ПУСК ОСЦИЛЛОГРАФА",
            "СДТУ: ПУСК ОСЦИЛЛОГРАФА"
        };

        private List<string> List2 => new List<string>
        {
            "ОШИБОЧНОЕ СООБЩЕНИЕ",
            "УСТРОЙСТВО ВЫКЛЮЧЕНО",
            "УСТРОЙСТВО ВКЛЮЧЕНО",
            "УСТАВКИ ИЗМЕНЕНЫ",
            "СБРОС ЖУРНАЛА СИСТЕМЫ",
            "СБРОС ЖУРНАЛА АВАРИЙ ",
            "СБРОС ОСЦИЛЛОГРАФА ",
            "ОШИБКА МОДУЛЯ 1 ",
            "НОРМА МОДУЛЯ 1",
            "ОШИБКА МОДУЛЯ 2",
            "НОРМА МОДУЛЯ 2",
            "ОШИБКА МОДУЛЯ 3",
            "НОРМА МОДУЛЯ 3",
            "ОШИБКА МОДУЛЯ 4",
            "НОРМА МОДУЛЯ 4",
            "ОШИБКА МОДУЛЯ 5",
            "НОРМА МОДУЛЯ 5",
            "ОШИБКА ШИНЫ SPI",
            "НОРМА ШИНЫ SPI",
            "ОШИБКА ШИНЫ MCBSP",
            "НОРМА ШИНЫ MCBSP",
            "ОШИБКА УСТАВОК",
            "ОШИБКА ГРУППЫ УСТАВОК",
            "ОШИБКА ПАРОЛЯ",
            "ОШИБКА ЖУРНАЛА АВАРИЙ",
            "ОШИБКА ЖУРНАЛА СИСТЕМЫ",
            "ОШИБКА ОСЦИЛЛОГРАФА",
            "МЕНЮ: УСТАВКИ ИЗМЕНЕНЫ",
            "СДТУ: УСТАВКИ ИЗМЕНЕНЫ",
            "НЕИСПРАВНОСТЬ ЦЕПЕЙ ТТ СШ1",
            "НОРМА ЦЕПЕЙ ТТ СШ1",
            "НЕИСПРАВНОСТЬ ЦЕПЕЙ ТТ СШ2",
            "НОРМА ЦЕПЕЙ ТТ СШ2",
            "НЕИСПРАВНОСТЬ ЦЕПЕЙ ТТ ПО",
            "НОРМА ЦЕПЕЙ ТТ ПО",
            "ОШИБКА ЧАСТОТЫ",
            "НОРМА  ЧАСТОТЫ",
            "МЕНЮ: ОСНОВНАЯ ГРУППА",
            "МЕНЮ: РЕЗЕРВНАЯ ГРУППА",
            "СДТУ: ОСНОВНАЯ ГРУППА",
            "СДТУ: РЕЗЕРВНАЯ ГРУППА",
            "ВНЕШНЯЯ РЕЗЕРВНАЯ ГРУППА УСТАВОК",
            "СБРОС ВНЕШНЕЙ РЕЗЕРВНОЙ ГРУППЫ",
            "ГРУППА УСТАВОК ИЗМЕНЕНА",
            "ПАРОЛЬ ИЗМЕНЕН",
            "МЕНЮ: СБРОС ИНДИКАЦИИ",
            "СДТУ: СБРОС ИНДИКАЦИИ",
            "ДИСКРЕТ: СБРОС ИНДИКАЦИИ",
            "УРОВ ОТКЛЮЧЕНИЕ СШ1",
            "УРОВ ОТКЛЮЧЕНИЕ СШ2",
            "УРОВ ОТКЛЮЧЕНИЕ ПО",
            "УРОВ ПРИСОЕДИНЕНИЕ 1",
            "УРОВ ПРИСОЕДИНЕНИЕ 2",
            "УРОВ ПРИСОЕДИНЕНИЕ 3",
            "УРОВ ПРИСОЕДИНЕНИЕ 4",
            "УРОВ ПРИСОЕДИНЕНИЕ 5",
            "УРОВ ПРИСОЕДИНЕНИЕ 6",
            "УРОВ ПРИСОЕДИНЕНИЕ 7",
            "УРОВ ПРИСОЕДИНЕНИЕ 8",
            "УРОВ ПРИСОЕДИНЕНИЕ 9",
            "УРОВ ПРИСОЕДИНЕНИЕ 10",
            "УРОВ ПРИСОЕДИНЕНИЕ 11",
            "УРОВ ПРИСОЕДИНЕНИЕ 12",
            "УРОВ ПРИСОЕДИНЕНИЕ 13",
            "УРОВ ПРИСОЕДИНЕНИЕ 14",
            "УРОВ ПРИСОЕДИНЕНИЕ 15",
            "УРОВ ПРИСОЕДИНЕНИЕ 16",
            "УРОВ ПРИСОЕДИНЕНИЕ 17",
            "УРОВ ПРИСОЕДИНЕНИЕ 18",
            "УРОВ ПРИСОЕДИНЕНИЕ 19",
            "УРОВ ПРИСОЕДИНЕНИЕ 20",
            "УРОВ ПРИСОЕДИНЕНИЕ 21",
            "УРОВ ПРИСОЕДИНЕНИЕ 22",
            "УРОВ ПРИСОЕДИНЕНИЕ 23",
            "УРОВ ПРИСОЕДИНЕНИЕ 24",
            "СДТУ: ЛОГИКА ИЗМЕНЕНА",
            "СДТУ: КОНСТАНТЫ ЛОГИКИ ИЗМЕНЕНЫ ",
            "МЕНЮ: КОНСТАНТЫ  ЛОГИКИ ИЗМЕНЕНЫ ",
            "СДТУ: МЕНЮ ЛОГИКИ ИЗМЕНЕНА",
            "МЕНЮ: ЗАПУСК ЛОГИКИ",
            "СДТУ: ЗАПУСК ЛОГИКИ",
            "МЕНЮ: ОСТАНОВ ЛОГИКИ",
            "СДТУ: ОСТАНОВ ЛОГИКИ",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:ПРОГ.",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:ПАРОЛЬ",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:РАЗРЕШ.",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:КОНФИГ.",
            "ОШИБКА ЛОГИКИ ПО СТАРТУ:МЕНЮ",
            "ОШИБКА ЛОГИКИ: ТАЙМ АУТ",
            "ОШИБКА ЛОГИКИ: РАЗМЕР",
            "ОШИБКА ЛОГИКИ: КОМАНДА",
            "ОШИБКА ЛОГИКИ: АРГУМЕНТ",
            "МЕНЮ: СБРОС КОНФИГУРАЦИИ",
            "МЕНЮ: СБРОС СП-ЛОГИКИ",
            "УРОВ: Неисправность блок-конт. прис.1",
            "УРОВ: Неисправность блок-конт. прис.2",
            "УРОВ: Неисправность блок-конт. прис.3",
            "УРОВ: Неисправность блок-конт. прис.4",
            "УРОВ: Неисправность блок-конт. прис.5",
            "УРОВ: Неисправность блок-конт. прис.6",
            "УРОВ: Неисправность блок-конт. прис.7",
            "УРОВ: Неисправность блок-конт. прис.8",
            "УРОВ: Неисправность блок-конт. прис.9",
            "УРОВ: Неисправность блок-конт. прис.10",
            "УРОВ: Неисправность блок-конт. прис.11",
            "УРОВ: Неисправность блок-конт. прис.12",
            "УРОВ: Неисправность блок-конт. прис.13",
            "УРОВ: Неисправность блок-конт. прис.14",
            "УРОВ: Неисправность блок-конт. прис.15",
            "УРОВ: Неисправность блок-конт. прис.16",
            "УРОВ: Неисправность блок-конт. прис.17",
            "УРОВ: Неисправность блок-конт. прис.18",
            "УРОВ: Неисправность блок-конт. прис.19",
            "УРОВ: Неисправность блок-конт. прис.20",
            "УРОВ: Неисправность блок-конт. прис.21",
            "УРОВ: Неисправность блок-конт. прис.22",
            "УРОВ: Неисправность блок-конт. прис.23",
            "УРОВ: Неисправность блок-конт. прис.24",
            "ВНЕШНИЙ СБРОС НЕИСПРАВНОСТИ ЦЕПЕЙ ТТ",
            "МЕНЮ: СБРОС НЕИСПРАВНОСТИ ЦЕПЕЙ ТТ",
            "СДТУ: СБРОС НЕИСПРАВНОСТИ ЦЕПЕЙ ТТ",
            "ПУСК ОСЦИЛЛОГРАФА ОТ ДИСКРЕТНОГО СИГНАЛА",
            "МЕНЮ: ПУСК ОСЦИЛЛОГРАФА",
            "СДТУ: ПУСК ОСЦИЛЛОГРАФА",
            "МЕНЮ: СБРОС ТЕХНОЛОГИЧЕСКИХ НАСТРОЕК",
            "ОШИБКА МОДУЛЯ 6",
            "НОРМА МОДУЛЯ 6",
            "НЕИСПРАВНОСТЬ ТН",
            "НОРМА ТН",
            "Uabc < 5В",
            "Uф > 5В",
            "НЕИСПРАВНОСТЬ ТНn",
            "НОРМА ТНn",
            "НЕИСПРАВНОСТЬ ТН ПО КОНТРОЛЮ 3U0-Un",
            "НЕИСПРАВНОСТЬ ТН ПО НЕСИММЕТРИИ Uabc"
        };

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public override string GetRecordMessage
        {
            get
            {
                if ((_message == 7) | (_message == 9) | (_message == 11) | (_message == 13) |
                    (_message == 15))
                    return string.Format(MESSAGE_WITH_CODE_PATTERN, this.SystemJournalMessages[_message],
                                         _moduleErrorCode);
                if (_message >= 500)
                    return string.Format(MESSAGE_SPL_PATTERN, _message - 499);
                return this.SystemJournalMessages.Count > _message
                    ? this.SystemJournalMessages[_message]
                    : _message.ToString();

            }
        } 
        #endregion [Properties]
    }
}
