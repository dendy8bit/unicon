﻿using System;
using System.IO;
using System.Windows.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.SystemJournal;
using BEMN.MBServer;

namespace BEMN.MR901.Old.SystemJournal
{
    public class Mr901SystemJournalForm : Mr900SystemJournalBaseForm<SystemJournalStruct>
    {
        public Mr901SystemJournalForm() { }

        public Mr901SystemJournalForm(Mr901Device device):base(device, device.RefreshSystemJournal,device.SystemJournal)
        {
            Activated += this.ActivatedForm;
            if (Common.VersionConverter(_device.DeviceVersion) >= 2.03)
            {
                _saveToHtmlBtn.Visible = true;
                _saveToHtmlBtn.Click += SaveToHtmlBtnOnClick;
            }
        }

        public override Type FormDevice
        {
            get { return typeof (Mr901Device); }
        }

        public override Type ClassType
        {
            get { return this.GetType(); }
        }

        private void ActivatedForm(object sender, EventArgs e)
        {
            SystemJournalStruct.CurrentVersion = Common.VersionConverter(_device.DeviceVersion);
        }
        
        private void SaveToHtmlBtnOnClick(object sender, EventArgs eventArgs)
        {
            if (_dataTable.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            if (_saveHtmlDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        _dataTable.WriteXml(writer);
                        xml = writer.ToString();
                    }
                    HtmlExport.Export(_saveHtmlDialog.FileName, "МР901. Журнал системы", xml);
                    _statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
    }
}
