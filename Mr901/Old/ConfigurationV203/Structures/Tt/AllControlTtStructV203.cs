﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Old.ConfigurationV203.Structures.Tt
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>

    public class AllControlTtStructV203 : StructBase, IDgvRowsContainer<ControlTtStructV203>
    {
        public const int TT_COUNT = 3;
        [Layout(0, Count = TT_COUNT)] public ControlTtStructV203[] TtStructChannels;
        
        public ControlTtStructV203[] Rows
        {
            get { return this.TtStructChannels; }
            set { this.TtStructChannels = value; }
        }

        public string InpResertFaultTt
        {
            get { return this.TtStructChannels[0].InpResertFaultTt; }
            set { this.TtStructChannels[0].InpResertFaultTt = value; }
        }
    }
}
