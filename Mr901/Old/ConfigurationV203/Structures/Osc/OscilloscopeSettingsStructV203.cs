﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Old.ConfigurationV203.Structures.Osc
{
    /// <summary>
    /// Уставки осциллографа
    /// </summary>
    public class OscilloscopeSettingsStructV203 : StructBase
    {
        [Layout(0)] private ushort config; //0 - фиксация по первой аварии 1 - фиксация по последней аварии
        [Layout(1)] private ushort size; //размер осциллограмы
        [Layout(2)] private ushort percent; //процент от размера осциллограммы
        [Layout(3)] private ChannelStructV203 _kanal; //конфигурация канала осциллографирования
        [Layout(4)] private ushort _inputOsc;
        [Layout(5, Count = 4)] private ushort[] rez;

        [XmlIgnore]
        public ushort[] Kanal
        {
            get { return this._kanal.Kanal; }
            set { this._kanal.Kanal = value; }
        }

        #region Конфигурация Осциллографа
        [BindingProperty(0)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string OscLength
        {
            get { return Validator.Get((ushort) (this.size - 1), Strings.OscLength); }
            set { this.size = (ushort) (Validator.Set(value, Strings.OscLength) + 1); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort OscWLength
        {
            get { return this.percent; }
            set { this.percent = value; }
        }
        [XmlElement(ElementName = "Фиксация")]
        [BindingProperty(2)]
        public string OscFix
        {
            get { return Validator.Get(this.config, Strings.OscFix); }
            set { this.config = Validator.Set(value, Strings.OscFix); }
        }
        [XmlElement(ElementName = "Конфигурация_каналов")]
        [BindingProperty(3)]
        public ChannelStructV203 Channel
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }

        [XmlElement(ElementName = "Вход_пуска_осциллографа")]
        [BindingProperty(4)]
        public string InputOsc
        {
            get { return Validator.Get(this._inputOsc, Strings.SignalSrab); }
            set { this._inputOsc = Validator.Set(value, Strings.SignalSrab); }
        }
        #endregion
    }
}