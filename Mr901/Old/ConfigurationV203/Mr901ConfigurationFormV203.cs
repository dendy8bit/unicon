﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901.Old.Configuration.Structures;
using BEMN.MR901.Old.Configuration.Structures.Connections;
using BEMN.MR901.Old.Configuration.Structures.Defenses;
using BEMN.MR901.Old.Configuration.Structures.Defenses.Differential;
using BEMN.MR901.Old.Configuration.Structures.Defenses.External;
using BEMN.MR901.Old.Configuration.Structures.Defenses.Mtz;
using BEMN.MR901.Old.Configuration.Structures.Ls;
using BEMN.MR901.Old.Configuration.Structures.RelayInd;
using BEMN.MR901.Old.Configuration.Structures.Urov;
using BEMN.MR901.Old.Configuration.Structures.Vls;
using BEMN.MR901.Old.ConfigurationV203.Structures;
using BEMN.MR901.Old.ConfigurationV203.Structures.Osc;
using BEMN.MR901.Old.ConfigurationV203.Structures.Tt;

namespace BEMN.MR901.Old.ConfigurationV203
{
    public partial class Mr901ConfigurationFormV203 : Form, IFormView
    {
        #region [Constants]
        
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR901_SET_POINTS";
        private const string ERROR_SETPOINTS_VALUE = "В конфигурации заданы некорректные значения. Проверьте конфигурацию.";
        private const string INVALID_PORT = "Порт недоступен.";

        private const string READ_OK = "Конфигурация успешно прочитана";
        private const string READ_FAIL = "Не удалось прочитать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";

        #endregion [Constants]

        #region Поля

        private RadioButtonSelector _groupSelector;
        private Mr901Device _device;
        private MemoryEntity<ConfigurationStructV203> _configuration;
        private ConfigurationStructV203 _currentSetpointsStruct;
        #region [Реле и индикаторы]
        private NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> _releyValidator;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<FaultStruct> _faultValidator;
        #endregion [Реле и индикаторы]

        #region [ВЛС]
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<AllOutputLogicSignalStruct> _vlsUnion;
        /// <summary>
        /// Массив контролов для ВЛС
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        #endregion [ВЛС]

        #region [Входные сигналы]
        /// <summary>
        /// Массив контролов для ЛС
        /// </summary>
        private DataGridView[] _lsBoxes;
        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<AllInputLogicSignalStruct> _inputLogicUnion;
        #endregion [Входные сигналы]

        private NewDgwValidatior<AllControlTtStructV203, ControlTtStructV203> _ttValidator;
        private NewStructValidator<UrovStruct> _urovValidator;
        private NewStructValidator<InputSignalStruct> _inputSignalValidator;
        private NewStructValidator<NewControlTtStruct> _newTtValidator;
        private NewStructValidator<OscilloscopeSettingsStructV203> _oscValidator;
        private NewDgwValidatior<AllUrovConnectionStruct, UrovConnectionStruct> _urovConnectionValidator;
        private NewDgwValidatior<AllConnectionStruct, ConnectionStruct> _connectionValidator;

        #region [Защиты]
        private NewDgwValidatior<AllDifferentialCurrentStruct, DifferentialCurrentStruct> _differentialCurrentValidator;
        private NewDgwValidatior<AllMtzStruct, MtzStruct> _mtzValidator;
        private NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct> _externalDefenseValidator;
        private StructUnion<SetpointStruct> _setpointValidator;
        private SetpointsValidator<AllDefensesSetpointsStruct, SetpointStruct> _defensesValidator; 
        #endregion [Защиты]

        private StructUnion<ConfigurationStructV203> _configurationValidator; 

        #endregion
        
        #region Конструкторы

        public Mr901ConfigurationFormV203()
        {
            this.InitializeComponent();
        }

        public Mr901ConfigurationFormV203(Mr901Device device)
        {
            this.InitializeComponent();
            this._device = device;
            Strings.Version = Common.VersionConverter(this._device.DeviceVersion);
            this._device.ConfigWriteOk += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteOk);
            this._device.ConfigWriteFail += HandlerHelper.CreateActionHandler(this, this.ConfigurationWriteFail);

            this._configuration = device.ConfigurationV203;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.ConfirmConstraint);
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationWriteFail();
            });
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationReadFail();
            });

            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._progressBar.Maximum = this._configuration.Slots.Count;
            this.Init();
        }


        private void Init()
        {
            this._currentSetpointsStruct = new ConfigurationStructV203();

            #region [Реле и Индикаторы]

            this._releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                this._outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.SignalType),
                new ColumnInfoCombo(Strings.SignalSrab),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.SignalType),
                new ColumnInfoCombo(Strings.SignalSrab),
                new ColumnInfoColor()
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            #endregion [Реле и Индикаторы]

            #region [ВЛС]

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1,
                this.VLScheckedListBox2,
                this.VLScheckedListBox3,
                this.VLScheckedListBox4,
                this.VLScheckedListBox5,
                this.VLScheckedListBox6,
                this.VLScheckedListBox7,
                this.VLScheckedListBox8,
                this.VLScheckedListBox9,
                this.VLScheckedListBox10,
                this.VLScheckedListBox11,
                this.VLScheckedListBox12,
                this.VLScheckedListBox13,
                this.VLScheckedListBox14,
                this.VLScheckedListBox15,
                this.VLScheckedListBox16
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i],
                    Strings.VLSSignals);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalStruct>(this._vlsValidator);

            #endregion [ВЛС]

            #region [ЛС]

            this._lsBoxes = new[]
            {
                this._inputSignals1,
                this._inputSignals2,
                this._inputSignals3,
                this._inputSignals4,
                this._inputSignals5,
                this._inputSignals6,
                this._inputSignals7,
                this._inputSignals8,
                this._inputSignals9,
                this._inputSignals10,
                this._inputSignals11,
                this._inputSignals12,
                this._inputSignals13,
                this._inputSignals14,
                this._inputSignals15,
                this._inputSignals16
            };
            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[AllInputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    this._lsBoxes[i],
                    InputLogicStruct.DISCRETS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(Strings.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.LogicValues)
                    );
            }
            this._inputLogicUnion = new StructUnion<AllInputLogicSignalStruct>(this._inputLogicValidator);

            #endregion [ЛС]

            this._ttValidator = new NewDgwValidatior<AllControlTtStructV203, ControlTtStructV203>
                (
                this._configTtDgv,
                AllControlTtStructV203.TT_COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.TtNames, ColumnsType.NAME),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(Strings.TtFault),
                new ColumnInfoCombo(Strings.ResetTT)
                );

            this._newTtValidator = new NewStructValidator<NewControlTtStruct>
                (
                this._toolTip,
                new ControlInfoValidator(this._ttValidator),
                new ControlInfoCombo(this._inpResetTtcomboBox, Strings.InputSignals)
                );

            this._urovValidator = new NewStructValidator<UrovStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._DZHModes, Strings.ModesLight),
                new ControlInfoCombo(this._DZHKontr, Strings.KONTR),
                new ControlInfoCombo(this._DZHDiff, Strings.Forbidden),
                new ControlInfoCombo(this._DZHSelf, Strings.Forbidden),
                new ControlInfoCombo(this._DZHSign, Strings.Forbidden),
                new ControlInfoText(this._DZHTUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._DZHTUrov3, RulesContainer.TimeRule),
                new ControlInfoCombo(this._DZHSH1, Strings.InputSignals),
                new ControlInfoCombo(this._DZHSH2, Strings.InputSignals),
                new ControlInfoCombo(this._DZHPO, Strings.InputSignals)
                );

            this._inputSignalValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, Strings.InputSignals),
                new ControlInfoCombo(this._indComboBox, Strings.InputSignals)
                );

            NewDgwValidatior<ChannelStructV203> _channelValidator = new NewDgwValidatior<ChannelStructV203>
                (
                this._oscChannels,
                ChannelStructV203.COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.ChannelsNames, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.SignalSrab)
                );

            this._oscValidator = new NewStructValidator<OscilloscopeSettingsStructV203>
                (
                this._toolTip,
                new ControlInfoCombo(this._oscLength, Strings.OscLength),
                new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                new ControlInfoCombo(this._oscFix, Strings.OscFix),
                new ControlInfoValidator(_channelValidator),
                new ControlInfoCombo(this._inpOscComboBox, Strings.SignalSrab)
                );

            this._urovConnectionValidator = new NewDgwValidatior
                <AllUrovConnectionStruct, UrovConnectionStruct>
                (
                this._UROVJoinData,
                AllUrovConnectionStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.ConnectionNames, ColumnsType.NAME),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule)
                );
            this._connectionValidator = new NewDgwValidatior
                <AllConnectionStruct, ConnectionStruct>
                (
                this._joinData,
                AllConnectionStruct.CONNECTIONS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.ConnectionNames, ColumnsType.NAME),
                new ColumnInfoText(RulesContainer.UshortTo65534),
                new ColumnInfoCombo(Strings.InputSignals),
                new ColumnInfoCombo(Strings.InputSignals),
                new ColumnInfoCombo(Strings.Join),
                new ColumnInfoCombo(Strings.InputSignals),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._differentialCurrentValidator = new NewDgwValidatior
                <AllDifferentialCurrentStruct, DifferentialCurrentStruct>
                (
                new[] {this._difDDataGrid, this._difMDataGrid},
                new[] {3, 3},
                this._toolTip,
                new ColumnInfoCombo(Strings.DefNames, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.Mode),
                new ColumnInfoCombo(Strings.InputSignals),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule, true, false),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(new CustomUshortRule(0, 45)),
                new ColumnInfoCheck(true, false),
                new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                new ColumnInfoCheck(true, false),
                new ColumnInfoText(RulesContainer.UshortTo100, true, false),
                new ColumnInfoCheck(true, false),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(Strings.InputSignals),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difDDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        ),
                    new TurnOffDgv
                        (
                        this._difMDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        )
                },
                Disabled = new Point[]
                {
                    new Point(3, 2)
                }
            };
            this._mtzValidator = new NewDgwValidatior<AllMtzStruct, MtzStruct>
                (
                this._MTZDifensesDataGrid,
                AllMtzStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.MtzNames, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.Mode),
                new ColumnInfoCombo(Strings.InputSignals),
                new ColumnInfoCombo(Strings.MTZJoin),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(Strings.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._MTZDifensesDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9)
                        )
                }
            };
            this._externalDefenseValidator = new NewDgwValidatior<AllExternalDefenseStruct, ExternalDefenseStruct>
                (
                this._externalDifensesDataGrid,
                AllExternalDefenseStruct.COUNT,
                this._toolTip,
                new ColumnInfoCombo(Strings.ExternalnNames, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.Mode),
                new ColumnInfoCombo(Strings.Otkl),
                new ColumnInfoCombo(Strings.SignalSrabExternal),
                new ColumnInfoCombo(Strings.SignalSrabExternal),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(Strings.SignalSrabExternal),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDifensesDataGrid,
                        new TurnOffRule(1, Strings.Mode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10)
                        )
                }
            };

            this._setpointValidator = new StructUnion<SetpointStruct>
                (
                this._differentialCurrentValidator,
                this._mtzValidator,
                this._externalDefenseValidator
                );
            

            _groupSelector = new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton, _groupChangeButton);

            this._defensesValidator = new SetpointsValidator<AllDefensesSetpointsStruct, SetpointStruct>(_groupSelector, this._setpointValidator);
            
            this._configurationValidator = new StructUnion<ConfigurationStructV203>
                (
                this._urovValidator,
                this._urovConnectionValidator,
                this._connectionValidator,
                this._inputSignalValidator,
                this._oscValidator,
                this._newTtValidator,
                this._inputLogicUnion,
                this._vlsUnion,
                this._defensesValidator,
                this._releyValidator,
                this._indicatorValidator,
                this._faultValidator
                );
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (Mr901Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr901ConfigurationFormV203); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Дополнительные функции
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        public void ConfigurationReadOk()
        {
            this._statusLabel.Text = READ_OK;
            this._currentSetpointsStruct = this._configuration.Value;
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
            this.IsProcess = false;
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = WRITE_OK;
            MessageBox.Show(WRITE_OK);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        } 
        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            this._configurationValidator.Set(this._currentSetpointsStruct);
        }
        
        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._progressBar.Value = 0;
            this._configuration.LoadStruct();
            this._statusLabel.Text = "Идет чтение";
        }
        #endregion

        #region Обработчики событий
        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
        }
        
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int osclen = Common.VersionConverter(this._device.DeviceVersion) < 2.0 ? 26168*2 : 55751*2;
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = ((int)(osclen / (index + 2))).ToString();
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}
            DialogResult result = MessageBox.Show(
                string.Format("Записать конфигурацию МР901 №{0}?", this._device.DeviceNumber), "Запись",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идёт запись конфигурации";
                    this.IsProcess = true;
                    this._progressBar.Value = 0;
                    this._configuration.Value = this._currentSetpointsStruct;
                    this._configuration.SaveStruct();
                }
                else
                {
                    MessageBox.Show(ERROR_SETPOINTS_VALUE);
                }
            }
        }
        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.ResetSetPoints();
        }

        private void ResetSetPoints()
        {
            this._configurationValidator.Reset();
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            } 
        }
        #endregion

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (this._configurationValidator.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationValidator.Get();
                return true;
            }
            else
            {
                MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
                return false;
            }
        }
        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                
                byte[] values = doc.DocumentElement.Name == "МР901_уставки" 
                    ? this.LoadOldConfig(doc, doc.DocumentElement.Name) 
                    : Convert.FromBase64String(a.InnerText);

                if (values.Length != this._currentSetpointsStruct.GetSize() - this._currentSetpointsStruct.IgnoredAddresses.Count)
                {
                    List<byte> buf = new List<byte>(values);
                    foreach (int ignoredAddress in this._currentSetpointsStruct.IgnoredAddresses)
                    {
                        buf.RemoveAt(ignoredAddress);
                    }
                    values = buf.ToArray();
                }

                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }


        private byte[] LoadOldConfig(XmlDocument doc, string docElement)
        {
            List<byte> result = new List<byte>();
            docElement = string.Format("/{0}/", docElement);
            result.AddRange(this.DeserializeSlot(doc, docElement + "UROV"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "UROVCONN"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "CONNMAIN"));
            result.AddRange(new byte[]{0,0,0,0});
            result.AddRange(this.DeserializeSlot(doc, docElement + "INPUTSIGNAL"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "OSCOPE"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "CONFIGTT"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "INPSYGNAL"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "ELSSYGNAL"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "CURRENTPROTMAIN"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "CURRENTPROTRESERVE"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "PARAMAUTOMAT"));
            
            byte[] res = result.ToArray();
            Common.SwapArrayItems(ref res);
            return res;
        }

        private byte[] DeserializeSlot(XmlDocument doc, string nodePath)
        {
            return Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР901_Уставки_версия {0:F1}.bin", this._device.DeviceVersion);
            if (this.WriteConfiguration() && DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.Serialize(this._saveConfigurationDlg.FileName);
            } 
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR901"));
                
                ushort[] values = this._currentSetpointsStruct.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._currentSetpointsStruct.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._statusLabel.Text = HtmlExport.Export(Resources.MR901Main,
                        Resources.MR901Res, this._currentSetpointsStruct,
                        this._currentSetpointsStruct.DeviceType, this._device.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        private void Mr901ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.resetSetpoints)
            {
                this.ResetSetPoints();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void Mr901ConfigurationFormV203_Activated(object sender, EventArgs e)
        {
            Strings.Version = Common.VersionConverter(this._device.DeviceVersion);
        }
    }
}
