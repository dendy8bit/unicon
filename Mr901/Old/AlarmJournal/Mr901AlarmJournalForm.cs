﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR901.Old.AlarmJournal.Structures;
using BEMN.MR901.Old.Configuration.Structures.Connections;
using BEMN.MR901.Old.HelpClasses;

namespace BEMN.MR901.Old.AlarmJournal
{
    public partial class Mr901AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР901_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly MemoryEntity<AllConnectionStruct> _currentConnections;
        private DataTable _table;
        private int _recordNumber;
        private Mr901Device _device;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr901AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr901AlarmJournalForm(Mr901Device device)
        {
            this.InitializeComponent();
            this._device = device;

            this._alarmJournal = device.AlarmJournal;
            this._refreshAlarmJournal = device.RefreshAlarmJournal;
            this._currentConnections = device.ConnectionsAlarmJournal;
            this._currentConnections.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                CurrentConverter.Factors = this._currentConnections.Value.GetAllItt;
                this._refreshAlarmJournal.SaveStruct();
            });
            this._currentConnections.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._refreshAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._refreshAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
        } 
        #endregion [Ctor's]


        #region [Help members]
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
        }

        private void StartReadJournal()
        {
            this._recordNumber = 0;
            this._table.Clear();
            this._alarmJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {
            if (!this._alarmJournal.Value.IsEmpty)
            {
                this._recordNumber++;
                AlarmJournalRecordStruct.Number = this._recordNumber;
                this._table.Rows.Add(this._alarmJournal.Value.GetRecord);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            }
            else
            {
                if (this._recordNumber == 0)
                {
                    this._statusLabel.Text = "Журнал пуст";
                }
            }
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
            }
            return table;
        }

        #endregion [Help members]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr901Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr901AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Event Handlers]
        private void Mr901AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._statusLabel.Text = READ_AJ;
            this._currentConnections.LoadStruct();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
                {
                    this._table.Clear();
                    if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
                    {
                        byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                        int connectionSize = AllConnectionStruct.CONNECTIONS_COUNT * 2;
                        CurrentConverter.Factors = Common.TOWORDS(file.Skip(file.Length - connectionSize).ToArray(), true);
                        AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
                        int size = journal.GetSize();
                        int index = 0;
                        this._recordNumber = 0;
                        while (index < file.Length - connectionSize)
                        {
                            List<byte> journalBytes = new List<byte>();
                            journalBytes.AddRange(Common.SwapListItems(file.Skip(index).Take(size)));
                            journal.InitStruct(journalBytes.ToArray());
                            this._alarmJournal.Value = journal;
                            this.ReadRecord();
                            index += size;
                        }
                    }
                    else
                    {
                        this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                        this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
                        this._alarmJournalGrid.Refresh();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Невозможно открыть файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        #endregion [Event Handlers]

        private void _saveToHTMLBtn_Click(object sender, EventArgs e)
        {

            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmHTMLdialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    HtmlExport.Export(this._table, this._saveAlarmHTMLdialog.FileName, Resources.MR901AJ);
                    this._statusLabel.Text = "Журнал успешно сохранен.";
                    /*
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveAlarmHTMLdialog.FileName, "МР901. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";
                    */
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }
    }
}
