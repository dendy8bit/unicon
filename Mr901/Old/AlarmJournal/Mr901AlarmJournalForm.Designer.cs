﻿namespace BEMN.MR901.Old.AlarmJournal
{
    partial class Mr901AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._saveToHTMLBtn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveAlarmHTMLdialog = new System.Windows.Forms.SaveFileDialog();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ida3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ita3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I7Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I8Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I9Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I10Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I11Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I12Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I13Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I14Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I15Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I16Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Location = new System.Drawing.Point(3, 3);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._IaCol,
            this._Ida1Col,
            this._Ita1Col,
            this._Ida2Col,
            this._Ita2Col,
            this._Ida3Col,
            this._Ita3Col,
            this._I1Col,
            this._I2Col,
            this._I3Col,
            this._I4Col,
            this._I5Col,
            this._I6Col,
            this._I7Col,
            this._I8Col,
            this._I9Col,
            this._I10Col,
            this._I11Col,
            this._I12Col,
            this._I13Col,
            this._I14Col,
            this._I15Col,
            this._I16Col,
            this._D0Col,
            this._D1Col,
            this._D2Col});
            this._alarmJournalGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 486);
            this._alarmJournalGrid.TabIndex = 19;
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(152, 3);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(301, 3);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(143, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._openAlarmJournalDialog.Filter = "ЖА МР901 (.xml)|*.xml|ЖА МР901 (.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР901";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР901";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР901) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._alarmJournalGrid);
            this.splitContainer1.Panel1MinSize = 100;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._saveToHTMLBtn);
            this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
            this.splitContainer1.Panel2.Controls.Add(this._loadAlarmJournalButton);
            this.splitContainer1.Panel2.Controls.Add(this._readAlarmJournalButton);
            this.splitContainer1.Panel2.Controls.Add(this._saveAlarmJournalButton);
            this.splitContainer1.Panel2MinSize = 50;
            this.splitContainer1.Size = new System.Drawing.Size(788, 540);
            this.splitContainer1.SplitterDistance = 486;
            this.splitContainer1.TabIndex = 23;
            // 
            // _saveToHTMLBtn
            // 
            this._saveToHTMLBtn.Location = new System.Drawing.Point(450, 3);
            this._saveToHTMLBtn.Name = "_saveToHTMLBtn";
            this._saveToHTMLBtn.Size = new System.Drawing.Size(143, 23);
            this._saveToHTMLBtn.TabIndex = 23;
            this._saveToHTMLBtn.Text = "Сохранить в HTML";
            this._saveToHTMLBtn.UseVisualStyleBackColor = true;
            this._saveToHTMLBtn.Click += new System.EventHandler(this._saveToHTMLBtn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 28);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _saveAlarmHTMLdialog
            // 
            this._saveAlarmHTMLdialog.DefaultExt = "xml";
            this._saveAlarmHTMLdialog.FileName = "Журнал аварий МР901 HTML";
            this._saveAlarmHTMLdialog.Filter = "(Журнал аварий МР901) | *.html";
            this._saveAlarmHTMLdialog.Title = "Сохранить  журнал аварий для МР901";
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "_indexCol";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "_timeCol";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "_msg1Col";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle1;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "_msgCol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._msgCol.HeaderText = "Ступень";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 120;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "_codeCol";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "_typeCol";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IaCol
            // 
            this._IaCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._IaCol.DataPropertyName = "_IaCol";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._IaCol.HeaderText = "Группа уставок";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _Ida1Col
            // 
            this._Ida1Col.DataPropertyName = "_Ida1Col";
            this._Ida1Col.HeaderText = "Iд1 СШ1";
            this._Ida1Col.Name = "_Ida1Col";
            this._Ida1Col.ReadOnly = true;
            this._Ida1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida1Col.Width = 48;
            // 
            // _Ita1Col
            // 
            this._Ita1Col.DataPropertyName = "_Ita1Col";
            this._Ita1Col.HeaderText = "Iт1 СШ1";
            this._Ita1Col.Name = "_Ita1Col";
            this._Ita1Col.ReadOnly = true;
            this._Ita1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita1Col.Width = 47;
            // 
            // _Ida2Col
            // 
            this._Ida2Col.DataPropertyName = "_Ida2Col";
            this._Ida2Col.HeaderText = "Iд2 СШ2";
            this._Ida2Col.Name = "_Ida2Col";
            this._Ida2Col.ReadOnly = true;
            this._Ida2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida2Col.Width = 48;
            // 
            // _Ita2Col
            // 
            this._Ita2Col.DataPropertyName = "_Ita2Col";
            this._Ita2Col.HeaderText = "Iт2 СШ2";
            this._Ita2Col.Name = "_Ita2Col";
            this._Ita2Col.ReadOnly = true;
            this._Ita2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita2Col.Width = 47;
            // 
            // _Ida3Col
            // 
            this._Ida3Col.DataPropertyName = "_Ida3Col";
            this._Ida3Col.HeaderText = "Iд3 ПО";
            this._Ida3Col.Name = "_Ida3Col";
            this._Ida3Col.ReadOnly = true;
            this._Ida3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ida3Col.Width = 42;
            // 
            // _Ita3Col
            // 
            this._Ita3Col.DataPropertyName = "_Ita3Col";
            this._Ita3Col.HeaderText = "Iт3 ПО";
            this._Ita3Col.Name = "_Ita3Col";
            this._Ita3Col.ReadOnly = true;
            this._Ita3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Ita3Col.Width = 41;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "_I1Col";
            this._I1Col.HeaderText = "I1";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 22;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "_I2Col";
            this._I2Col.HeaderText = "I2";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 22;
            // 
            // _I3Col
            // 
            this._I3Col.DataPropertyName = "_I3Col";
            this._I3Col.HeaderText = "I3";
            this._I3Col.Name = "_I3Col";
            this._I3Col.ReadOnly = true;
            this._I3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I3Col.Width = 22;
            // 
            // _I4Col
            // 
            this._I4Col.DataPropertyName = "_I4Col";
            this._I4Col.HeaderText = "I4";
            this._I4Col.Name = "_I4Col";
            this._I4Col.ReadOnly = true;
            this._I4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I4Col.Width = 22;
            // 
            // _I5Col
            // 
            this._I5Col.DataPropertyName = "_I5Col";
            this._I5Col.HeaderText = "I5";
            this._I5Col.Name = "_I5Col";
            this._I5Col.ReadOnly = true;
            this._I5Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I5Col.Width = 22;
            // 
            // _I6Col
            // 
            this._I6Col.DataPropertyName = "_I6Col";
            this._I6Col.HeaderText = "I6";
            this._I6Col.Name = "_I6Col";
            this._I6Col.ReadOnly = true;
            this._I6Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I6Col.Width = 22;
            // 
            // _I7Col
            // 
            this._I7Col.DataPropertyName = "_I7Col";
            this._I7Col.HeaderText = "I7";
            this._I7Col.Name = "_I7Col";
            this._I7Col.ReadOnly = true;
            this._I7Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I7Col.Width = 22;
            // 
            // _I8Col
            // 
            this._I8Col.DataPropertyName = "_I8Col";
            this._I8Col.HeaderText = "I8";
            this._I8Col.Name = "_I8Col";
            this._I8Col.ReadOnly = true;
            this._I8Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I8Col.Width = 22;
            // 
            // _I9Col
            // 
            this._I9Col.DataPropertyName = "_I9Col";
            this._I9Col.HeaderText = "I9";
            this._I9Col.Name = "_I9Col";
            this._I9Col.ReadOnly = true;
            this._I9Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I9Col.Width = 22;
            // 
            // _I10Col
            // 
            this._I10Col.DataPropertyName = "_I10Col";
            this._I10Col.HeaderText = "I10";
            this._I10Col.Name = "_I10Col";
            this._I10Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I10Col.Width = 28;
            // 
            // _I11Col
            // 
            this._I11Col.DataPropertyName = "_I11Col";
            this._I11Col.HeaderText = "I11";
            this._I11Col.Name = "_I11Col";
            this._I11Col.ReadOnly = true;
            this._I11Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I11Col.Width = 28;
            // 
            // _I12Col
            // 
            this._I12Col.DataPropertyName = "_I12Col";
            this._I12Col.HeaderText = "I12";
            this._I12Col.Name = "_I12Col";
            this._I12Col.ReadOnly = true;
            this._I12Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I12Col.Width = 28;
            // 
            // _I13Col
            // 
            this._I13Col.DataPropertyName = "_I13Col";
            this._I13Col.HeaderText = "I13";
            this._I13Col.Name = "_I13Col";
            this._I13Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I13Col.Width = 28;
            // 
            // _I14Col
            // 
            this._I14Col.DataPropertyName = "_I14Col";
            this._I14Col.HeaderText = "I14";
            this._I14Col.Name = "_I14Col";
            this._I14Col.ReadOnly = true;
            this._I14Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I14Col.Width = 28;
            // 
            // _I15Col
            // 
            this._I15Col.DataPropertyName = "_I15Col";
            this._I15Col.HeaderText = "I15";
            this._I15Col.Name = "_I15Col";
            this._I15Col.ReadOnly = true;
            this._I15Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I15Col.Width = 28;
            // 
            // _I16Col
            // 
            this._I16Col.DataPropertyName = "_I16Col";
            this._I16Col.HeaderText = "I16";
            this._I16Col.Name = "_I16Col";
            this._I16Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I16Col.Width = 28;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "_D0Col";
            this._D0Col.HeaderText = "D[1-8]";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 42;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "_D1Col";
            this._D1Col.HeaderText = "D [9-16]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 46;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "_D2Col";
            this._D2Col.HeaderText = "D [17-24]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 51;
            // 
            // Mr901AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 540);
            this.Controls.Add(this.splitContainer1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Mr901AlarmJournalForm";
            this.Text = "Mr901AlarmJournalForm";
            this.Load += new System.EventHandler(this.Mr901AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _saveToHTMLBtn;
        private System.Windows.Forms.SaveFileDialog _saveAlarmHTMLdialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ida3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ita3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I7Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I8Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I9Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I10Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I12Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I13Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I14Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I15Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I16Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
    }
}