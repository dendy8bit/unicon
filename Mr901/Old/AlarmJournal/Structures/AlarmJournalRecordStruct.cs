﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR901.Old.HelpClasses;

namespace BEMN.MR901.Old.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]
        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        private const string SPL_PATTERN = "СПЛ {0}"; 
        #endregion [Constants]


        #region [Static fields]
        /// <summary>
        /// Номер сообщения
        /// </summary>
        public static int Number = 0; 
        #endregion [Static fields]


        #region [Private fields]
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _stage;
        [Layout(9)] private ushort _numberOfTriggeredParametr;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _groupOfSetpoints;

        [Layout(12)] private ushort _ida1;
        [Layout(13)] private ushort _idb1Reserv;
        [Layout(14)] private ushort _idc1Reserv;

        [Layout(15)] private ushort _ida2;
        [Layout(16)] private ushort _idb2Reserv;
        [Layout(17)] private ushort _idc2Reserv;

        [Layout(18)] private ushort _ida3;
        [Layout(19)] private ushort _idb3Reserv;
        [Layout(20)] private ushort _idc3Reserv;

        [Layout(21)] private ushort _ita1;
        [Layout(22)] private ushort _itb1Reserv;
        [Layout(23)] private ushort _itc1Reserv;

        [Layout(24)] private ushort _ita2;
        [Layout(25)] private ushort _itb2Reserv;
        [Layout(26)] private ushort _itc2Reserv;

        [Layout(27)] private ushort _ita3;
        [Layout(28)] private ushort _itb3Reserv;
        [Layout(29)] private ushort _itc3Reserv;

        [Layout(30)] private ushort _i1;
        [Layout(31)] private ushort _i2;
        [Layout(32)] private ushort _i3;
        [Layout(33)] private ushort _i4;
        [Layout(34)] private ushort _i5;
        [Layout(35)] private ushort _i6;
        [Layout(36)] private ushort _i7;
        [Layout(37)] private ushort _i8;
        [Layout(38)] private ushort _i9;
        [Layout(39)] private ushort _i10;
        [Layout(40)] private ushort _i11;
        [Layout(41)] private ushort _i12;
        [Layout(42)] private ushort _i13;
        [Layout(43)] private ushort _i14;
        [Layout(44)] private ushort _i15;
        [Layout(45)] private ushort _i16;
        [Layout(46)] private ushort _d1;
        [Layout(47)] private ushort _d2;
        [Layout(48, Count = 4)] private ushort[] _reserve;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Сработанный параметр
        /// </summary>
        private string GetTriggedOption
        {
            get { return Strings.AlarmJournalTriggedOption[this._numberOfTriggeredParametr]; }
        }
        /// <summary>
        /// //Значение сработанного параметра
        /// </summary>
        private string GetValueTriggedOption
        {
            get
            {
                if (this._numberOfTriggeredParametr < 18)
                    return CurrentConverter.GetAmperage(this._valueOfTriggeredParametr, 0);
                if (this._numberOfTriggeredParametr < 34)
                {
                    int parametr = this._numberOfTriggeredParametr - 18;
                    return CurrentConverter.GetAmperage(this._valueOfTriggeredParametr, (ushort)(parametr + 1));
                }
                if (this._numberOfTriggeredParametr < 36)
                    return Strings.AlarmJournalExternalDefenseTriggedOption[this._valueOfTriggeredParametr];
                if (this._numberOfTriggeredParametr == 36)
                    return string.Format(SPL_PATTERN, this._valueOfTriggeredParametr);
                return string.Empty;
            }
        }
        /// <summary>
        /// Группа уставок
        /// </summary>
        private string GetGroupOfSetpoints
        {
            get { return Strings.AlarmJournalSetpointsGroup[this._groupOfSetpoints]; }
        }
        /// <summary>
        /// Запись аварии
        /// </summary>
        public object[] GetRecord
        {
            get
            { 
                return new object[]
                    {
                      
                        AlarmJournalRecordStruct.Number, //Номер сообщения
                        this.GetTime, //Время сообщения
                        Strings.AlarmJournalMessage[this._message], //Сообщение
                        Strings.AlarmJournalStage[this._stage], //Ступень
                        this.GetTriggedOption, //Сработанный параметр
                        this.GetValueTriggedOption, //Значение сработанного параметра
                        this.GetGroupOfSetpoints, //Группа уставок
                        CurrentConverter.GetAmperage(this._ida1, 0), //Ид1
                        CurrentConverter.GetAmperage(this._ita1, 0), //Ит1
                        CurrentConverter.GetAmperage(this._ida2, 0), //Ид2
                        CurrentConverter.GetAmperage(this._ita2, 0), //Ит2
                        CurrentConverter.GetAmperage(this._ida3, 0), //Ид3
                        CurrentConverter.GetAmperage(this._ita3, 0), //Ит3
                        CurrentConverter.GetAmperage(this._i1, 1), //И1
                        CurrentConverter.GetAmperage(this._i2, 2), //И2
                        CurrentConverter.GetAmperage(this._i3, 3), //И3
                        CurrentConverter.GetAmperage(this._i4, 4), //И4
                        CurrentConverter.GetAmperage(this._i5, 5), //И5
                        CurrentConverter.GetAmperage(this._i6, 6), //И6
                        CurrentConverter.GetAmperage(this._i7, 7), //И7
                        CurrentConverter.GetAmperage(this._i8, 8), //И8
                        CurrentConverter.GetAmperage(this._i9, 9), //И9
                        CurrentConverter.GetAmperage(this._i10, 10), //И10
                        CurrentConverter.GetAmperage(this._i11, 11), //И11
                        CurrentConverter.GetAmperage(this._i12, 12), //И12
                        CurrentConverter.GetAmperage(this._i13, 13), //И13
                        CurrentConverter.GetAmperage(this._i14, 14), //И14
                        CurrentConverter.GetAmperage(this._i15, 15), //И15
                        CurrentConverter.GetAmperage(this._i16, 16), //И16
                        this.GetMask(Common.LOBYTE(this._d1)), //Д[1-8]
                        this.GetMask(Common.HIBYTE(this._d1)),//Д[9-16]
                        this.GetMask(Common.LOBYTE(this._d2))////Д[17-24]
                    };
            }
        }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this._year +
                          this._month +
                          this._date +
                          this._hour +
                          this._minute +
                          this._second +
                          this._millisecond;
                return sum == 0;
            }
        }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        private string GetTime
        {
            get
            {
                return string.Format
                   (
                       DATE_TIME_PATTERN,
                       this._date,
                       this._month,
                       this._year,
                       this._hour,
                       this._minute,
                       this._second,
                       this._millisecond
                   );
            }
        }
        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Ивертирует двоичное представление числа
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Инвертированое двоичное представление</returns>
        private string GetMask(ushort value)
        {
            var chars = Convert.ToString(value, 2).PadLeft(8, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        } 
        #endregion [Help members]
    }
}
