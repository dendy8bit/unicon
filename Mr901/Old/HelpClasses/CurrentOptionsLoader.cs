﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR901.Old.Configuration.Structures.Connections;

namespace BEMN.MR901.Old.HelpClasses
{
    /// <summary>
    /// Загружает уставки токов(Iтт)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Private fields]
        private readonly MemoryEntity<AllConnectionStruct> _connections; 
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail; 
        #endregion [Events]


        #region [Ctor's]
        public CurrentOptionsLoader(MemoryEntity<AllConnectionStruct> connections)
        {
            this._connections = connections;
            this._connections.AllReadOk += this._connections_AllReadOk;
            this._connections.AllReadFail += this._connections_AllReadFail;
        }
        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        void _connections_AllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        void _connections_AllReadOk(object sender)
        {
            CurrentConverter.Factors = this._connections.Value.GetAllItt;
            if (this.LoadOk != null)
            {
                this.LoadOk.Invoke();
            }
        }  
        #endregion [Memory Entity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запускает загрузку уставок токов(Iтт)
        /// </summary>
        public void StartRead()
        {
            this._connections.LoadStruct();
        } 
        #endregion [Public members]
    }
}
