﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR901.Old.Measuring.Structures
{
    /// <summary>
    /// МР 901 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _i1; // ток 1
        [Layout(1)] private ushort _i2; // ток 2
        [Layout(2)] private ushort _i3; // ток 3
        [Layout(3)] private ushort _i4; // ток 4
        [Layout(4)] private ushort _i5; // ток 5
        [Layout(5)] private ushort _i6; // ток 6
        [Layout(6)] private ushort _i7; // ток 7
        [Layout(7)] private ushort _i8; // ток 8
        [Layout(8)] private ushort _i9; // ток 9
        [Layout(9)] private ushort _i10; // ток 10
        [Layout(10)] private ushort _i11; // ток 11
        [Layout(11)] private ushort _i12; // ток 12
        [Layout(12)] private ushort _i13; // ток 13
        [Layout(13)] private ushort _i14; // ток 14
        [Layout(14)] private ushort _i15; // ток 15
        [Layout(15)] private ushort _i16; // ток 16
        //ток
        [Layout(16)] private ushort _ida1; // ток СШ1
        [Layout(17)] private ushort _ida2; // ток СШ2
        [Layout(18)] private ushort _ida3; // ток ПО
        //ток
        [Layout(19)] private ushort _iba1; // ток СШ1
        [Layout(20)] private ushort _iba2; // ток СШ2
        [Layout(21)] private ushort _iba3; // ток ПО
        //ток
        [Layout(22)] private ushort _idb1; // ток СШ1
        [Layout(23)] private ushort _idb2; // ток СШ2
        [Layout(24)] private ushort _idb3; // ток ПО
        //ток
        [Layout(25)] private ushort _ibb1; // ток СШ1
        [Layout(26)] private ushort _ibb2; // ток СШ2
        [Layout(27)] private ushort _ibb3; // ток ПО
        //ток
        [Layout(28)] private ushort _ibc1; // ток СШ1
        [Layout(29)] private ushort _ibc2; // ток СШ2
        [Layout(30)] private ushort _ibc3; // ток ПО

        //свободная логика(SIGNAL FREE LOGIC)
        [Layout(31)] private ushort _sfl1; //
        [Layout(32)] private ushort _sfl2; //
        [Layout(33)] private ushort _sfl3; //
        [Layout(34)] private ushort _sfl4; //
        [Layout(35)] private ushort _sfl5; //
        [Layout(36)] private ushort _sfl6; //
        [Layout(37)] private ushort _sfl7; // 
        #endregion [Private fields]


        #region [Properties]
        public string GetI1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, factors[1]);
        }

        public string GetI2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, factors[2]);
        }

        public string GetI3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i3);
            return ValuesConverterCommon.Analog.GetI(value, factors[3]);
        }

        public string GetI4(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i4);
            return ValuesConverterCommon.Analog.GetI(value, factors[4]);
        }

        public string GetI5(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i5);
            return ValuesConverterCommon.Analog.GetI(value, factors[5]);
        }

        public string GetI6(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i6);
            return ValuesConverterCommon.Analog.GetI(value, factors[6]);
        }

        public string GetI7(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i7);
            return ValuesConverterCommon.Analog.GetI(value, factors[7]);
        }

        public string GetI8(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i8);
            return ValuesConverterCommon.Analog.GetI(value, factors[8]);
        }

        public string GetI9(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i9);
            return ValuesConverterCommon.Analog.GetI(value, factors[9]);
        }

        public string GetI10(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i10);
            return ValuesConverterCommon.Analog.GetI(value, factors[10]);
        }

        public string GetI11(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i11);
            return ValuesConverterCommon.Analog.GetI(value, factors[11]);
        }

        public string GetI12(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i12);
            return ValuesConverterCommon.Analog.GetI(value, factors[12]);
        }

        public string GetI13(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i13);
            return ValuesConverterCommon.Analog.GetI(value, factors[13]);
        }

        public string GetI14(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i14);
            return ValuesConverterCommon.Analog.GetI(value, factors[14]);
        }

        public string GetI15(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i15);
            return ValuesConverterCommon.Analog.GetI(value, factors[15]);
        }

        public string GetI16(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._i16);
            return ValuesConverterCommon.Analog.GetI(value, factors[16]);
        }

        public string GetIda1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIda3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._ida3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIba1(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._iba1);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIba2(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._iba2);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        public string GetIba3(List<AnalogDataBaseStruct> list, int[] factors)
        {
            ushort value = this.GetMean(list, o => o._iba3);
            return ValuesConverterCommon.Analog.GetI(value, factors[0]);
        }

        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }
        #endregion [Properties]
    }
}
