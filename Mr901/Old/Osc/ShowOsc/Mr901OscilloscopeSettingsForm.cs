﻿using System;
using System.Globalization;
using System.Windows.Forms;
using BEMN_XY_Chart;

namespace BEMN.MR901.Old.Osc.ShowOsc
{
    public partial class Mr901OscilloscopeSettingsForm : Form
    {
        #region [Constants]
        private const string INVALID_VALUE_ERROR = "Неверное значение. Проверьте ввод."; 
        #endregion [Constants]


        #region [Private fields]
        private readonly DAS_Net_XYChart _yChart;
        private readonly DAS_Net_XYChart[] _xCharts;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr901OscilloscopeSettingsForm()
        {
            this.InitializeComponent();
        }

        public Mr901OscilloscopeSettingsForm(DAS_Net_XYChart[] xCharts,DAS_Net_XYChart yChart)
        {
            this.InitializeComponent();
            this._yChart = yChart;
            this._xCharts = xCharts;
        } 
        #endregion [Ctor's]


        #region [Event Handlers]
        private void Mr901OscilloscopeSettingsForm_Load(object sender, EventArgs e)
        {
            this._xTickerTextBox.Text = this._yChart.GridXTicker.ToString(CultureInfo.InvariantCulture);
            this._yTickerTextBox.Text = this._yChart.GridYTicker.ToString(CultureInfo.InvariantCulture);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var x = Convert.ToInt32(this._xTickerTextBox.Text);
                var y = Convert.ToInt32(this._yTickerTextBox.Text);
                this._yChart.GridXTicker = x;
                this._yChart.GridYTicker = y;
                foreach (var xChart in this._xCharts)
                {
                    xChart.GridXTicker = x;
                    xChart.GridYTicker = y;
                }
                this.Close();
            }
            catch (Exception)
            {

                MessageBox.Show(INVALID_VALUE_ERROR);
            }

        }

        private void _xTickerTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsDigit(e.KeyChar) | e.KeyChar == '\b');
        } 
        #endregion [Event Handlers]
    }
}
