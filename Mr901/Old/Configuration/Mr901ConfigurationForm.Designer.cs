﻿namespace BEMN.MR901.Old.Configuration
{
    partial class Mr901ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndColorCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VLScheckedListBox16 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox13 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox15 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox14 = new System.Windows.Forms.CheckedListBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.VLScheckedListBox12 = new System.Windows.Forms.CheckedListBox();
            this.VLScheckedListBox11 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox9 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox10 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this._allDefensesPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._groupChangeButton = new System.Windows.Forms.Button();
            this._mainRadioButton = new System.Windows.Forms.RadioButton();
            this._reserveRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._difensesTC = new System.Windows.Forms.TabControl();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._difDDataGrid = new System.Windows.Forms.DataGridView();
            this._difDStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difDBlockColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDIcpColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDIdoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDtcpColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDIbColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDfColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDBlockGColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDI2gColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDBlock5GColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDI5gColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDOprNasColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDOchColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difDIochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDtochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difDEnterOchColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difDOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difDUrovColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._difMDataGrid = new System.Windows.Forms.DataGridView();
            this._difMStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difMBlockColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difMIcpColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMIdoColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMIbColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMfColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difMOchColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._difMIochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMtochColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._difMEnterOchColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difMOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._difMUrovColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._MTZDifensesDataGrid = new System.Windows.Forms.DataGridView();
            this._mtzStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzMeasureColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzICPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzCharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzTColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzkColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._mtzOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._mtzUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._externalDifensesDataGrid = new System.Windows.Forms.DataGridView();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._inputSygnalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._grUstComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals9 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals10 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this._inputSignals13 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this._inputSignals14 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this._inputSignals15 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this._inputSignals16 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._inputSignals5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this._inputSignals6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this._inputSignals7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this._inputSignals8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._joinPage = new System.Windows.Forms.TabPage();
            this._joinData = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._joinITT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._joinSwitchOFF = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinSwitchOn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinJoin = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinEnter = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._joinResetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._timeResetJoinColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscChannels = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscSygnal = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this._urovPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._UROVJoinData = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._JoinIUROV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._JoinTUROV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._DZHTUrov3 = new System.Windows.Forms.MaskedTextBox();
            this._DZHTUrov2 = new System.Windows.Forms.MaskedTextBox();
            this._DZHTUrov1 = new System.Windows.Forms.MaskedTextBox();
            this._DZHPO = new System.Windows.Forms.ComboBox();
            this._DZHSH2 = new System.Windows.Forms.ComboBox();
            this._DZHSH1 = new System.Windows.Forms.ComboBox();
            this._DZHSign = new System.Windows.Forms.ComboBox();
            this._DZHSelf = new System.Windows.Forms.ComboBox();
            this._DZHDiff = new System.Windows.Forms.ComboBox();
            this._DZHKontr = new System.Windows.Forms.ComboBox();
            this._DZHModes = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this._configTtDgv = new System.Windows.Forms.DataGridView();
            this._nameTtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._idTtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeSrabTtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._faultTtColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0DataGreed = new System.Windows.Forms.DataGridView();
            this._dif0AVRColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0APVColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0UrovColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0OscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0Intg2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Ib2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Intg1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Ib1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0TdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0InColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0ModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this._modeDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._stepOnInstantValuesDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._UROVDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._oscDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this._blockingDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this._constraintDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this._timeEnduranceDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label128 = new System.Windows.Forms.Label();
            this._APVDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label129 = new System.Windows.Forms.Label();
            this._AVRDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this._modeDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._Ib1BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this._K1AngleOfSlopeTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label66 = new System.Windows.Forms.Label();
            this._Ib2BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label69 = new System.Windows.Forms.Label();
            this._K2TangensTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this._constraintDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this._I2I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._perBlockI2I1 = new System.Windows.Forms.ComboBox();
            this._timeEnduranceDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this._blockingDTZComboBox = new System.Windows.Forms.ComboBox();
            this._UROVDTZComboBox = new System.Windows.Forms.ComboBox();
            this._oscDTZComboBox = new System.Windows.Forms.ComboBox();
            this._modeI2I1CB = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this._I5I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._perBlockI5I1 = new System.Windows.Forms.ComboBox();
            this._modeI5I1CB = new System.Windows.Forms.ComboBox();
            this.label130 = new System.Windows.Forms.Label();
            this._APVDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label131 = new System.Windows.Forms.Label();
            this._AVRDTZComboBox = new System.Windows.Forms.ComboBox();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this.miniToolStrip = new System.Windows.Forms.StatusStrip();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifOtklColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this._allDefensesPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this._difensesTC.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difDDataGrid)).BeginInit();
            this.tabPage18.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difMDataGrid)).BeginInit();
            this.tabPage22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._MTZDifensesDataGrid)).BeginInit();
            this.tabPage19.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).BeginInit();
            this.groupBox13.SuspendLayout();
            this._inputSygnalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).BeginInit();
            this._configurationTabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._joinPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._joinData)).BeginInit();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this._systemPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannels)).BeginInit();
            this._urovPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._UROVJoinData)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._configTtDgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dif0DataGreed)).BeginInit();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox11.Location = new System.Drawing.Point(8, 198);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(393, 190);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndColorCol});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(9, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(376, 167);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndColorCol
            // 
            this._outIndColorCol.HeaderText = "Цвет";
            this._outIndColorCol.Name = "_outIndColorCol";
            this._outIndColorCol.ReadOnly = true;
            this._outIndColorCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndColorCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndColorCol.Width = 70;
            // 
            // VLScheckedListBox16
            // 
            this.VLScheckedListBox16.CheckOnClick = true;
            this.VLScheckedListBox16.FormattingEnabled = true;
            this.VLScheckedListBox16.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox16.Name = "VLScheckedListBox16";
            this.VLScheckedListBox16.ScrollAlwaysVisible = true;
            this.VLScheckedListBox16.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox16.TabIndex = 6;
            // 
            // VLScheckedListBox13
            // 
            this.VLScheckedListBox13.CheckOnClick = true;
            this.VLScheckedListBox13.FormattingEnabled = true;
            this.VLScheckedListBox13.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox13.Name = "VLScheckedListBox13";
            this.VLScheckedListBox13.ScrollAlwaysVisible = true;
            this.VLScheckedListBox13.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox13.TabIndex = 6;
            // 
            // VLScheckedListBox15
            // 
            this.VLScheckedListBox15.CheckOnClick = true;
            this.VLScheckedListBox15.FormattingEnabled = true;
            this.VLScheckedListBox15.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox15.Name = "VLScheckedListBox15";
            this.VLScheckedListBox15.ScrollAlwaysVisible = true;
            this.VLScheckedListBox15.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox15.TabIndex = 6;
            // 
            // VLScheckedListBox14
            // 
            this.VLScheckedListBox14.CheckOnClick = true;
            this.VLScheckedListBox14.FormattingEnabled = true;
            this.VLScheckedListBox14.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox14.Name = "VLScheckedListBox14";
            this.VLScheckedListBox14.ScrollAlwaysVisible = true;
            this.VLScheckedListBox14.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox14.TabIndex = 6;
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(376, 167);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releWaitCol.Width = 70;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._outputReleGrid);
            this.groupBox12.Location = new System.Drawing.Point(8, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(393, 189);
            this.groupBox12.TabIndex = 3;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Выходные реле";
            // 
            // VLScheckedListBox12
            // 
            this.VLScheckedListBox12.CheckOnClick = true;
            this.VLScheckedListBox12.FormattingEnabled = true;
            this.VLScheckedListBox12.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox12.Name = "VLScheckedListBox12";
            this.VLScheckedListBox12.ScrollAlwaysVisible = true;
            this.VLScheckedListBox12.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox12.TabIndex = 6;
            // 
            // VLScheckedListBox11
            // 
            this.VLScheckedListBox11.CheckOnClick = true;
            this.VLScheckedListBox11.FormattingEnabled = true;
            this.VLScheckedListBox11.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox11.Name = "VLScheckedListBox11";
            this.VLScheckedListBox11.ScrollAlwaysVisible = true;
            this.VLScheckedListBox11.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox11.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(388, 468);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox2.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(388, 468);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox3.TabIndex = 6;
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Controls.Add(this.VLS9);
            this.VLSTabControl.Controls.Add(this.VLS10);
            this.VLSTabControl.Controls.Add(this.VLS11);
            this.VLSTabControl.Controls.Add(this.VLS12);
            this.VLSTabControl.Controls.Add(this.VLS13);
            this.VLSTabControl.Controls.Add(this.VLS14);
            this.VLSTabControl.Controls.Add(this.VLS15);
            this.VLSTabControl.Controls.Add(this.VLS16);
            this.VLSTabControl.Location = new System.Drawing.Point(6, 19);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(396, 521);
            this.VLSTabControl.TabIndex = 0;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(388, 468);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox1.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(388, 468);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox4.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(388, 468);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox5.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(388, 468);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox6.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(388, 468);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox7.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(388, 468);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox8.TabIndex = 6;
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLScheckedListBox9);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(388, 468);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox9
            // 
            this.VLScheckedListBox9.CheckOnClick = true;
            this.VLScheckedListBox9.FormattingEnabled = true;
            this.VLScheckedListBox9.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox9.Name = "VLScheckedListBox9";
            this.VLScheckedListBox9.ScrollAlwaysVisible = true;
            this.VLScheckedListBox9.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox9.TabIndex = 6;
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLScheckedListBox10);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(388, 468);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox10
            // 
            this.VLScheckedListBox10.CheckOnClick = true;
            this.VLScheckedListBox10.FormattingEnabled = true;
            this.VLScheckedListBox10.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox10.Name = "VLScheckedListBox10";
            this.VLScheckedListBox10.ScrollAlwaysVisible = true;
            this.VLScheckedListBox10.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox10.TabIndex = 6;
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLScheckedListBox11);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(388, 468);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLScheckedListBox12);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(388, 468);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLScheckedListBox13);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(388, 468);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLScheckedListBox14);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(388, 468);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLScheckedListBox15);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(388, 468);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLScheckedListBox16);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(388, 468);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // _allDefensesPage
            // 
            this._allDefensesPage.Controls.Add(this.groupBox5);
            this._allDefensesPage.Controls.Add(this.groupBox10);
            this._allDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._allDefensesPage.Name = "_allDefensesPage";
            this._allDefensesPage.Size = new System.Drawing.Size(906, 553);
            this._allDefensesPage.TabIndex = 4;
            this._allDefensesPage.Text = "Защиты";
            this._allDefensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._groupChangeButton);
            this.groupBox5.Controls.Add(this._mainRadioButton);
            this.groupBox5.Controls.Add(this._reserveRadioButton);
            this.groupBox5.Location = new System.Drawing.Point(8, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(360, 35);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Группа уставок";
            // 
            // _groupChangeButton
            // 
            this._groupChangeButton.Location = new System.Drawing.Point(184, 11);
            this._groupChangeButton.Name = "_groupChangeButton";
            this._groupChangeButton.Size = new System.Drawing.Size(170, 23);
            this._groupChangeButton.TabIndex = 33;
            this._groupChangeButton.Text = "Основные -> Резервные";
            this._toolTip.SetToolTip(this._groupChangeButton, "Копировать уставки");
            this._groupChangeButton.UseVisualStyleBackColor = true;
            // 
            // _mainRadioButton
            // 
            this._mainRadioButton.AutoSize = true;
            this._mainRadioButton.Checked = true;
            this._mainRadioButton.Location = new System.Drawing.Point(17, 13);
            this._mainRadioButton.Name = "_mainRadioButton";
            this._mainRadioButton.Size = new System.Drawing.Size(75, 17);
            this._mainRadioButton.TabIndex = 6;
            this._mainRadioButton.TabStop = true;
            this._mainRadioButton.Text = "Основная";
            this._mainRadioButton.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioButton
            // 
            this._reserveRadioButton.AutoSize = true;
            this._reserveRadioButton.Location = new System.Drawing.Point(98, 14);
            this._reserveRadioButton.Name = "_reserveRadioButton";
            this._reserveRadioButton.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioButton.TabIndex = 7;
            this._reserveRadioButton.Text = "Резервная";
            this._reserveRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this._difensesTC);
            this.groupBox10.Location = new System.Drawing.Point(8, 40);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(890, 510);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Защиты";
            // 
            // _difensesTC
            // 
            this._difensesTC.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._difensesTC.Controls.Add(this.tabPage21);
            this._difensesTC.Controls.Add(this.tabPage22);
            this._difensesTC.Controls.Add(this.tabPage19);
            this._difensesTC.Dock = System.Windows.Forms.DockStyle.Top;
            this._difensesTC.Location = new System.Drawing.Point(3, 16);
            this._difensesTC.Name = "_difensesTC";
            this._difensesTC.SelectedIndex = 0;
            this._difensesTC.Size = new System.Drawing.Size(884, 494);
            this._difensesTC.TabIndex = 0;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.tabControl3);
            this.tabPage21.Location = new System.Drawing.Point(4, 25);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Size = new System.Drawing.Size(876, 465);
            this.tabPage21.TabIndex = 4;
            this.tabPage21.Text = "Дифференциальные";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl3.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl3.Controls.Add(this.tabPage17);
            this.tabControl3.Controls.Add(this.tabPage18);
            this.tabControl3.Location = new System.Drawing.Point(3, 3);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(870, 459);
            this.tabControl3.TabIndex = 0;
            // 
            // tabPage17
            // 
            this.tabPage17.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage17.Controls.Add(this.groupBox20);
            this.tabPage17.Location = new System.Drawing.Point(4, 25);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(862, 430);
            this.tabPage17.TabIndex = 0;
            this.tabPage17.Text = "По действующим значениям";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox20.Controls.Add(this._difDDataGrid);
            this.groupBox20.Location = new System.Drawing.Point(3, 6);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(853, 142);
            this.groupBox20.TabIndex = 5;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Дифференциальные защиты по действующим значениям";
            // 
            // _difDDataGrid
            // 
            this._difDDataGrid.AllowUserToAddRows = false;
            this._difDDataGrid.AllowUserToDeleteRows = false;
            this._difDDataGrid.AllowUserToResizeColumns = false;
            this._difDDataGrid.AllowUserToResizeRows = false;
            this._difDDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difDDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difDDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this._difDDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difDDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._difDStageColumn,
            this._difDModesColumn,
            this._difDBlockColumn,
            this.Column1,
            this._difDIcpColumn,
            this._difDIdoColumn,
            this._difDtcpColumn,
            this._difDIbColumn,
            this._difDfColumn,
            this._difDBlockGColumn,
            this._difDI2gColumn,
            this._difDBlock5GColumn,
            this._difDI5gColumn,
            this._difDOprNasColumn,
            this._difDOchColumn,
            this._difDIochColumn,
            this._difDtochColumn,
            this._difDEnterOchColumn,
            this._difDOscColumn,
            this._difDUrovColumn});
            this._difDDataGrid.Location = new System.Drawing.Point(6, 19);
            this._difDDataGrid.MultiSelect = false;
            this._difDDataGrid.Name = "_difDDataGrid";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difDDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this._difDDataGrid.RowHeadersVisible = false;
            this._difDDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difDDataGrid.RowTemplate.Height = 24;
            this._difDDataGrid.ShowCellErrors = false;
            this._difDDataGrid.ShowRowErrors = false;
            this._difDDataGrid.Size = new System.Drawing.Size(841, 113);
            this._difDDataGrid.TabIndex = 3;
            // 
            // _difDStageColumn
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            this._difDStageColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this._difDStageColumn.HeaderText = "Ступень";
            this._difDStageColumn.Name = "_difDStageColumn";
            this._difDStageColumn.ReadOnly = true;
            this._difDStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difDModesColumn
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDModesColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this._difDModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._difDModesColumn.HeaderText = "Режим";
            this._difDModesColumn.Name = "_difDModesColumn";
            // 
            // _difDBlockColumn
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDBlockColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this._difDBlockColumn.HeaderText = "Блокировка";
            this._difDBlockColumn.Name = "_difDBlockColumn";
            this._difDBlockColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDBlockColumn.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Пуск от Iд3 ПО";
            this.Column1.Name = "Column1";
            // 
            // _difDIcpColumn
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIcpColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this._difDIcpColumn.HeaderText = "Iд>, Iн";
            this._difDIcpColumn.Name = "_difDIcpColumn";
            this._difDIcpColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDIcpColumn.Width = 65;
            // 
            // _difDIdoColumn
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIdoColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this._difDIdoColumn.HeaderText = "Iд>>, Iн";
            this._difDIdoColumn.Name = "_difDIdoColumn";
            this._difDIdoColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difDtcpColumn
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDtcpColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this._difDtcpColumn.HeaderText = "tср, мс";
            this._difDtcpColumn.Name = "_difDtcpColumn";
            this._difDtcpColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDtcpColumn.Width = 110;
            // 
            // _difDIbColumn
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIbColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this._difDIbColumn.HeaderText = "Iб, Iн";
            this._difDIbColumn.Name = "_difDIbColumn";
            this._difDIbColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDIbColumn.Width = 65;
            // 
            // _difDfColumn
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDfColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this._difDfColumn.HeaderText = "f";
            this._difDfColumn.Name = "_difDfColumn";
            this._difDfColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difDBlockGColumn
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.NullValue = false;
            this._difDBlockGColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this._difDBlockGColumn.HeaderText = "Блок. гармон. 2";
            this._difDBlockGColumn.Name = "_difDBlockGColumn";
            this._difDBlockGColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _difDI2gColumn
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDI2gColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this._difDI2gColumn.HeaderText = "I2г, %";
            this._difDI2gColumn.Name = "_difDI2gColumn";
            this._difDI2gColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difDI2gColumn.Width = 65;
            // 
            // _difDBlock5GColumn
            // 
            this._difDBlock5GColumn.HeaderText = "Блок. гармон. 5";
            this._difDBlock5GColumn.Name = "_difDBlock5GColumn";
            this._difDBlock5GColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _difDI5gColumn
            // 
            this._difDI5gColumn.HeaderText = "I5г, %";
            this._difDI5gColumn.Name = "_difDI5gColumn";
            this._difDI5gColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difDOprNasColumn
            // 
            this._difDOprNasColumn.HeaderText = "Опред. насыщ.";
            this._difDOprNasColumn.Name = "_difDOprNasColumn";
            this._difDOprNasColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _difDOchColumn
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.NullValue = false;
            this._difDOchColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this._difDOchColumn.HeaderText = "Очувствление *";
            this._difDOchColumn.Name = "_difDOchColumn";
            this._difDOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDOchColumn.Width = 90;
            // 
            // _difDIochColumn
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDIochColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this._difDIochColumn.HeaderText = "Iд*, Iн";
            this._difDIochColumn.Name = "_difDIochColumn";
            this._difDIochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difDtochColumn
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDtochColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this._difDtochColumn.HeaderText = "tоч, мс";
            this._difDtochColumn.Name = "_difDtochColumn";
            this._difDtochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difDEnterOchColumn
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDEnterOchColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this._difDEnterOchColumn.HeaderText = "Вход очувств.";
            this._difDEnterOchColumn.Name = "_difDEnterOchColumn";
            this._difDEnterOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDEnterOchColumn.Width = 110;
            // 
            // _difDOscColumn
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difDOscColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this._difDOscColumn.HeaderText = "Осц.";
            this._difDOscColumn.Name = "_difDOscColumn";
            this._difDOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _difDUrovColumn
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle19.NullValue = false;
            this._difDUrovColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this._difDUrovColumn.HeaderText = "УРОВ";
            this._difDUrovColumn.Name = "_difDUrovColumn";
            this._difDUrovColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difDUrovColumn.Width = 90;
            // 
            // tabPage18
            // 
            this.tabPage18.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage18.Controls.Add(this.groupBox1);
            this.tabPage18.Location = new System.Drawing.Point(4, 25);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(862, 430);
            this.tabPage18.TabIndex = 1;
            this.tabPage18.Text = "По мгновенным значениям";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._difMDataGrid);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(853, 142);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дифференциальные защиты по мгновенным значениям";
            // 
            // _difMDataGrid
            // 
            this._difMDataGrid.AllowUserToAddRows = false;
            this._difMDataGrid.AllowUserToDeleteRows = false;
            this._difMDataGrid.AllowUserToResizeColumns = false;
            this._difMDataGrid.AllowUserToResizeRows = false;
            this._difMDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difMDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difMDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this._difMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._difMStageColumn,
            this._difMModesColumn,
            this._difMBlockColumn,
            this.Column2,
            this._difMIcpColumn,
            this._difMIdoColumn,
            this.Column3,
            this._difMIbColumn,
            this._difMfColumn,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this._difMOchColumn,
            this._difMIochColumn,
            this._difMtochColumn,
            this._difMEnterOchColumn,
            this._difMOscColumn,
            this._difMUrovColumn});
            this._difMDataGrid.Location = new System.Drawing.Point(6, 19);
            this._difMDataGrid.MultiSelect = false;
            this._difMDataGrid.Name = "_difMDataGrid";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._difMDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this._difMDataGrid.RowHeadersVisible = false;
            this._difMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difMDataGrid.RowTemplate.Height = 24;
            this._difMDataGrid.ShowCellErrors = false;
            this._difMDataGrid.ShowRowErrors = false;
            this._difMDataGrid.Size = new System.Drawing.Size(841, 113);
            this._difMDataGrid.TabIndex = 4;
            // 
            // _difMStageColumn
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.White;
            this._difMStageColumn.DefaultCellStyle = dataGridViewCellStyle22;
            this._difMStageColumn.Frozen = true;
            this._difMStageColumn.HeaderText = "Ступень";
            this._difMStageColumn.Name = "_difMStageColumn";
            this._difMStageColumn.ReadOnly = true;
            this._difMStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difMModesColumn
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMModesColumn.DefaultCellStyle = dataGridViewCellStyle23;
            this._difMModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._difMModesColumn.HeaderText = "Режим";
            this._difMModesColumn.Name = "_difMModesColumn";
            // 
            // _difMBlockColumn
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMBlockColumn.DefaultCellStyle = dataGridViewCellStyle24;
            this._difMBlockColumn.HeaderText = "Блокировка";
            this._difMBlockColumn.Name = "_difMBlockColumn";
            this._difMBlockColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMBlockColumn.Width = 90;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Пуск от Iд3м ПО";
            this.Column2.Name = "Column2";
            // 
            // _difMIcpColumn
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIcpColumn.DefaultCellStyle = dataGridViewCellStyle25;
            this._difMIcpColumn.HeaderText = "Iд>, Iн";
            this._difMIcpColumn.Name = "_difMIcpColumn";
            this._difMIcpColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMIcpColumn.Width = 65;
            // 
            // _difMIdoColumn
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIdoColumn.DefaultCellStyle = dataGridViewCellStyle26;
            this._difMIdoColumn.HeaderText = "Iд>>, Iн";
            this._difMIdoColumn.Name = "_difMIdoColumn";
            this._difMIdoColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // _difMIbColumn
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIbColumn.DefaultCellStyle = dataGridViewCellStyle27;
            this._difMIbColumn.HeaderText = "Iб1, Iн";
            this._difMIbColumn.Name = "_difMIbColumn";
            this._difMIbColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._difMIbColumn.Width = 65;
            // 
            // _difMfColumn
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMfColumn.DefaultCellStyle = dataGridViewCellStyle28;
            this._difMfColumn.HeaderText = "f";
            this._difMfColumn.Name = "_difMfColumn";
            this._difMfColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.Visible = false;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.Name = "Column8";
            this.Column8.Visible = false;
            // 
            // _difMOchColumn
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle29.NullValue = false;
            this._difMOchColumn.DefaultCellStyle = dataGridViewCellStyle29;
            this._difMOchColumn.HeaderText = "Очувствление *";
            this._difMOchColumn.Name = "_difMOchColumn";
            this._difMOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMOchColumn.Width = 90;
            // 
            // _difMIochColumn
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMIochColumn.DefaultCellStyle = dataGridViewCellStyle30;
            this._difMIochColumn.HeaderText = "Iд*, Iн";
            this._difMIochColumn.Name = "_difMIochColumn";
            this._difMIochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difMtochColumn
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMtochColumn.DefaultCellStyle = dataGridViewCellStyle31;
            this._difMtochColumn.HeaderText = "tоч, мс";
            this._difMtochColumn.Name = "_difMtochColumn";
            this._difMtochColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _difMEnterOchColumn
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMEnterOchColumn.DefaultCellStyle = dataGridViewCellStyle32;
            this._difMEnterOchColumn.HeaderText = "Вход очувств.";
            this._difMEnterOchColumn.Name = "_difMEnterOchColumn";
            this._difMEnterOchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMEnterOchColumn.Width = 90;
            // 
            // _difMOscColumn
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._difMOscColumn.DefaultCellStyle = dataGridViewCellStyle33;
            this._difMOscColumn.HeaderText = "Осц.";
            this._difMOscColumn.Name = "_difMOscColumn";
            this._difMOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _difMUrovColumn
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle34.NullValue = false;
            this._difMUrovColumn.DefaultCellStyle = dataGridViewCellStyle34;
            this._difMUrovColumn.HeaderText = "УРОВ";
            this._difMUrovColumn.Name = "_difMUrovColumn";
            this._difMUrovColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._difMUrovColumn.Width = 90;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.groupBox21);
            this.tabPage22.Location = new System.Drawing.Point(4, 25);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Size = new System.Drawing.Size(876, 465);
            this.tabPage22.TabIndex = 5;
            this.tabPage22.Text = "МТЗ";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox21.Controls.Add(this._MTZDifensesDataGrid);
            this.groupBox21.Location = new System.Drawing.Point(3, 3);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(857, 459);
            this.groupBox21.TabIndex = 4;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "МТЗ";
            // 
            // _MTZDifensesDataGrid
            // 
            this._MTZDifensesDataGrid.AllowUserToAddRows = false;
            this._MTZDifensesDataGrid.AllowUserToDeleteRows = false;
            this._MTZDifensesDataGrid.AllowUserToResizeColumns = false;
            this._MTZDifensesDataGrid.AllowUserToResizeRows = false;
            this._MTZDifensesDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._MTZDifensesDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._MTZDifensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._MTZDifensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._mtzStageColumn,
            this._mtzModesColumn,
            this._mtzBlockingColumn,
            this._mtzMeasureColumn,
            this._mtzICPColumn,
            this._mtzCharColumn,
            this._mtzTColumn,
            this._mtzkColumn,
            this._mtzOscColumn,
            this._mtzUROVColumn});
            this._MTZDifensesDataGrid.Location = new System.Drawing.Point(6, 21);
            this._MTZDifensesDataGrid.MultiSelect = false;
            this._MTZDifensesDataGrid.Name = "_MTZDifensesDataGrid";
            this._MTZDifensesDataGrid.RowHeadersVisible = false;
            this._MTZDifensesDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._MTZDifensesDataGrid.RowTemplate.Height = 24;
            this._MTZDifensesDataGrid.ShowCellErrors = false;
            this._MTZDifensesDataGrid.ShowRowErrors = false;
            this._MTZDifensesDataGrid.Size = new System.Drawing.Size(845, 432);
            this._MTZDifensesDataGrid.TabIndex = 3;
            // 
            // _mtzStageColumn
            // 
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.White;
            this._mtzStageColumn.DefaultCellStyle = dataGridViewCellStyle36;
            this._mtzStageColumn.Frozen = true;
            this._mtzStageColumn.HeaderText = "Ступень";
            this._mtzStageColumn.Name = "_mtzStageColumn";
            this._mtzStageColumn.ReadOnly = true;
            this._mtzStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _mtzModesColumn
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzModesColumn.DefaultCellStyle = dataGridViewCellStyle37;
            this._mtzModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._mtzModesColumn.HeaderText = "Режим";
            this._mtzModesColumn.Name = "_mtzModesColumn";
            this._mtzModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzModesColumn.Width = 80;
            // 
            // _mtzBlockingColumn
            // 
            this._mtzBlockingColumn.HeaderText = "Блокировка";
            this._mtzBlockingColumn.Name = "_mtzBlockingColumn";
            this._mtzBlockingColumn.Width = 90;
            // 
            // _mtzMeasureColumn
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzMeasureColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this._mtzMeasureColumn.HeaderText = "Измерение";
            this._mtzMeasureColumn.Name = "_mtzMeasureColumn";
            this._mtzMeasureColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzMeasureColumn.Width = 90;
            // 
            // _mtzICPColumn
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzICPColumn.DefaultCellStyle = dataGridViewCellStyle39;
            this._mtzICPColumn.HeaderText = "Iср, Iн";
            this._mtzICPColumn.Name = "_mtzICPColumn";
            this._mtzICPColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzICPColumn.Width = 65;
            // 
            // _mtzCharColumn
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzCharColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this._mtzCharColumn.HeaderText = "Характ-ка";
            this._mtzCharColumn.Name = "_mtzCharColumn";
            this._mtzCharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._mtzCharColumn.Width = 90;
            // 
            // _mtzTColumn
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzTColumn.DefaultCellStyle = dataGridViewCellStyle41;
            this._mtzTColumn.HeaderText = "t, мс";
            this._mtzTColumn.Name = "_mtzTColumn";
            this._mtzTColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzTColumn.Width = 65;
            // 
            // _mtzkColumn
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzkColumn.DefaultCellStyle = dataGridViewCellStyle42;
            this._mtzkColumn.HeaderText = "k завис. хар-ки";
            this._mtzkColumn.Name = "_mtzkColumn";
            this._mtzkColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._mtzkColumn.Width = 110;
            // 
            // _mtzOscColumn
            // 
            this._mtzOscColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._mtzOscColumn.DefaultCellStyle = dataGridViewCellStyle43;
            this._mtzOscColumn.HeaderText = "Осциллограф";
            this._mtzOscColumn.Name = "_mtzOscColumn";
            this._mtzOscColumn.Width = 82;
            // 
            // _mtzUROVColumn
            // 
            this._mtzUROVColumn.HeaderText = "УРОВ";
            this._mtzUROVColumn.Name = "_mtzUROVColumn";
            this._mtzUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.groupBox24);
            this.tabPage19.Location = new System.Drawing.Point(4, 25);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Size = new System.Drawing.Size(876, 465);
            this.tabPage19.TabIndex = 2;
            this.tabPage19.Text = "Внешние";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox24.Controls.Add(this._externalDifensesDataGrid);
            this.groupBox24.Location = new System.Drawing.Point(6, 7);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(864, 450);
            this.groupBox24.TabIndex = 5;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Внешние защиты";
            // 
            // _externalDifensesDataGrid
            // 
            this._externalDifensesDataGrid.AllowUserToAddRows = false;
            this._externalDifensesDataGrid.AllowUserToDeleteRows = false;
            this._externalDifensesDataGrid.AllowUserToResizeColumns = false;
            this._externalDifensesDataGrid.AllowUserToResizeRows = false;
            this._externalDifensesDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDifensesDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._externalDifensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDifensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._externalDifStageColumn,
            this._externalDifModesColumn,
            this._externalDifOtklColumn,
            this._externalDifBlockingColumn,
            this._externalDifSrabColumn,
            this._externalDifTsrColumn,
            this._externalDifTvzColumn,
            this._externalDifVozvrColumn,
            this._externalDifVozvrYNColumn,
            this._externalDifOscColumn,
            this._externalDifUROVColumn});
            this._externalDifensesDataGrid.Location = new System.Drawing.Point(6, 21);
            this._externalDifensesDataGrid.MultiSelect = false;
            this._externalDifensesDataGrid.Name = "_externalDifensesDataGrid";
            this._externalDifensesDataGrid.RowHeadersVisible = false;
            this._externalDifensesDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDifensesDataGrid.RowTemplate.Height = 24;
            this._externalDifensesDataGrid.ShowCellErrors = false;
            this._externalDifensesDataGrid.ShowRowErrors = false;
            this._externalDifensesDataGrid.Size = new System.Drawing.Size(852, 425);
            this._externalDifensesDataGrid.TabIndex = 3;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.VLSTabControl);
            this.groupBox13.Location = new System.Drawing.Point(407, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(409, 546);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "ВЛС";
            // 
            // _inputSygnalsPage
            // 
            this._inputSygnalsPage.Controls.Add(this.groupBox18);
            this._inputSygnalsPage.Controls.Add(this.groupBox15);
            this._inputSygnalsPage.Controls.Add(this.groupBox17);
            this._inputSygnalsPage.Controls.Add(this.groupBox14);
            this._inputSygnalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSygnalsPage.Name = "_inputSygnalsPage";
            this._inputSygnalsPage.Size = new System.Drawing.Size(906, 553);
            this._inputSygnalsPage.TabIndex = 7;
            this._inputSygnalsPage.Text = "Входные сигналы";
            this._inputSygnalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(406, 61);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(179, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс индикации";
            // 
            // _indComboBox
            // 
            this._indComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(6, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._grUstComboBox);
            this.groupBox15.Location = new System.Drawing.Point(406, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(179, 52);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Аварийная группа уставок";
            // 
            // _grUstComboBox
            // 
            this._grUstComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._grUstComboBox.FormattingEnabled = true;
            this._grUstComboBox.Location = new System.Drawing.Point(6, 19);
            this._grUstComboBox.Name = "_grUstComboBox";
            this._grUstComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUstComboBox.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tabControl2);
            this.groupBox17.Location = new System.Drawing.Point(207, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(193, 547);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Controls.Add(this.tabPage16);
            this.tabControl2.Location = new System.Drawing.Point(6, 18);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(181, 528);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals9);
            this.tabPage9.Location = new System.Drawing.Point(4, 49);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(173, 475);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "ЛС9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals9
            // 
            this._inputSignals9.AllowUserToAddRows = false;
            this._inputSignals9.AllowUserToDeleteRows = false;
            this._inputSignals9.AllowUserToResizeColumns = false;
            this._inputSignals9.AllowUserToResizeRows = false;
            this._inputSignals9.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals9.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals9.Location = new System.Drawing.Point(3, 3);
            this._inputSignals9.MultiSelect = false;
            this._inputSignals9.Name = "_inputSignals9";
            this._inputSignals9.RowHeadersVisible = false;
            this._inputSignals9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals9.RowsDefaultCellStyle = dataGridViewCellStyle53;
            this._inputSignals9.RowTemplate.Height = 20;
            this._inputSignals9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals9.ShowCellErrors = false;
            this._inputSignals9.ShowCellToolTips = false;
            this._inputSignals9.ShowEditingIcon = false;
            this._inputSignals9.ShowRowErrors = false;
            this._inputSignals9.Size = new System.Drawing.Size(167, 469);
            this._inputSignals9.TabIndex = 2;
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "№";
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "Значение";
            this._signalValueColILI.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals10);
            this.tabPage10.Location = new System.Drawing.Point(4, 49);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(173, 475);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "ЛС10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals10
            // 
            this._inputSignals10.AllowUserToAddRows = false;
            this._inputSignals10.AllowUserToDeleteRows = false;
            this._inputSignals10.AllowUserToResizeColumns = false;
            this._inputSignals10.AllowUserToResizeRows = false;
            this._inputSignals10.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals10.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals10.Location = new System.Drawing.Point(3, 3);
            this._inputSignals10.MultiSelect = false;
            this._inputSignals10.Name = "_inputSignals10";
            this._inputSignals10.RowHeadersVisible = false;
            this._inputSignals10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals10.RowsDefaultCellStyle = dataGridViewCellStyle54;
            this._inputSignals10.RowTemplate.Height = 20;
            this._inputSignals10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals10.ShowCellErrors = false;
            this._inputSignals10.ShowCellToolTips = false;
            this._inputSignals10.ShowEditingIcon = false;
            this._inputSignals10.ShowRowErrors = false;
            this._inputSignals10.Size = new System.Drawing.Size(167, 469);
            this._inputSignals10.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "№";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals11);
            this.tabPage11.Location = new System.Drawing.Point(4, 49);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(173, 475);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "ЛС11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals11
            // 
            this._inputSignals11.AllowUserToAddRows = false;
            this._inputSignals11.AllowUserToDeleteRows = false;
            this._inputSignals11.AllowUserToResizeColumns = false;
            this._inputSignals11.AllowUserToResizeRows = false;
            this._inputSignals11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals11.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals11.Location = new System.Drawing.Point(3, 3);
            this._inputSignals11.MultiSelect = false;
            this._inputSignals11.Name = "_inputSignals11";
            this._inputSignals11.RowHeadersVisible = false;
            this._inputSignals11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals11.RowsDefaultCellStyle = dataGridViewCellStyle55;
            this._inputSignals11.RowTemplate.Height = 20;
            this._inputSignals11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals11.ShowCellErrors = false;
            this._inputSignals11.ShowCellToolTips = false;
            this._inputSignals11.ShowEditingIcon = false;
            this._inputSignals11.ShowRowErrors = false;
            this._inputSignals11.Size = new System.Drawing.Size(167, 469);
            this._inputSignals11.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "№";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals12);
            this.tabPage12.Location = new System.Drawing.Point(4, 49);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(173, 475);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "ЛС12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals12
            // 
            this._inputSignals12.AllowUserToAddRows = false;
            this._inputSignals12.AllowUserToDeleteRows = false;
            this._inputSignals12.AllowUserToResizeColumns = false;
            this._inputSignals12.AllowUserToResizeRows = false;
            this._inputSignals12.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals12.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals12.Location = new System.Drawing.Point(3, 3);
            this._inputSignals12.MultiSelect = false;
            this._inputSignals12.Name = "_inputSignals12";
            this._inputSignals12.RowHeadersVisible = false;
            this._inputSignals12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals12.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this._inputSignals12.RowTemplate.Height = 20;
            this._inputSignals12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals12.ShowCellErrors = false;
            this._inputSignals12.ShowCellToolTips = false;
            this._inputSignals12.ShowEditingIcon = false;
            this._inputSignals12.ShowRowErrors = false;
            this._inputSignals12.Size = new System.Drawing.Size(167, 469);
            this._inputSignals12.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "№";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this._inputSignals13);
            this.tabPage13.Location = new System.Drawing.Point(4, 49);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(173, 475);
            this.tabPage13.TabIndex = 4;
            this.tabPage13.Text = "ЛС13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // _inputSignals13
            // 
            this._inputSignals13.AllowUserToAddRows = false;
            this._inputSignals13.AllowUserToDeleteRows = false;
            this._inputSignals13.AllowUserToResizeColumns = false;
            this._inputSignals13.AllowUserToResizeRows = false;
            this._inputSignals13.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals13.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals13.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewComboBoxColumn11});
            this._inputSignals13.Location = new System.Drawing.Point(3, 3);
            this._inputSignals13.MultiSelect = false;
            this._inputSignals13.Name = "_inputSignals13";
            this._inputSignals13.RowHeadersVisible = false;
            this._inputSignals13.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals13.RowsDefaultCellStyle = dataGridViewCellStyle57;
            this._inputSignals13.RowTemplate.Height = 20;
            this._inputSignals13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals13.ShowCellErrors = false;
            this._inputSignals13.ShowCellToolTips = false;
            this._inputSignals13.ShowEditingIcon = false;
            this._inputSignals13.ShowRowErrors = false;
            this._inputSignals13.Size = new System.Drawing.Size(167, 469);
            this._inputSignals13.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "№";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 24;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn11.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this._inputSignals14);
            this.tabPage14.Location = new System.Drawing.Point(4, 49);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(173, 475);
            this.tabPage14.TabIndex = 5;
            this.tabPage14.Text = "ЛС14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // _inputSignals14
            // 
            this._inputSignals14.AllowUserToAddRows = false;
            this._inputSignals14.AllowUserToDeleteRows = false;
            this._inputSignals14.AllowUserToResizeColumns = false;
            this._inputSignals14.AllowUserToResizeRows = false;
            this._inputSignals14.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals14.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals14.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewComboBoxColumn12});
            this._inputSignals14.Location = new System.Drawing.Point(3, 3);
            this._inputSignals14.MultiSelect = false;
            this._inputSignals14.Name = "_inputSignals14";
            this._inputSignals14.RowHeadersVisible = false;
            this._inputSignals14.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals14.RowsDefaultCellStyle = dataGridViewCellStyle58;
            this._inputSignals14.RowTemplate.Height = 20;
            this._inputSignals14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals14.ShowCellErrors = false;
            this._inputSignals14.ShowCellToolTips = false;
            this._inputSignals14.ShowEditingIcon = false;
            this._inputSignals14.ShowRowErrors = false;
            this._inputSignals14.Size = new System.Drawing.Size(167, 469);
            this._inputSignals14.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "№";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 24;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn12.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this._inputSignals15);
            this.tabPage15.Location = new System.Drawing.Point(4, 49);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(173, 475);
            this.tabPage15.TabIndex = 6;
            this.tabPage15.Text = "ЛС15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // _inputSignals15
            // 
            this._inputSignals15.AllowUserToAddRows = false;
            this._inputSignals15.AllowUserToDeleteRows = false;
            this._inputSignals15.AllowUserToResizeColumns = false;
            this._inputSignals15.AllowUserToResizeRows = false;
            this._inputSignals15.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals15.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals15.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals15.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewComboBoxColumn13});
            this._inputSignals15.Location = new System.Drawing.Point(3, 3);
            this._inputSignals15.MultiSelect = false;
            this._inputSignals15.Name = "_inputSignals15";
            this._inputSignals15.RowHeadersVisible = false;
            this._inputSignals15.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals15.RowsDefaultCellStyle = dataGridViewCellStyle59;
            this._inputSignals15.RowTemplate.Height = 20;
            this._inputSignals15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals15.ShowCellErrors = false;
            this._inputSignals15.ShowCellToolTips = false;
            this._inputSignals15.ShowEditingIcon = false;
            this._inputSignals15.ShowRowErrors = false;
            this._inputSignals15.Size = new System.Drawing.Size(167, 469);
            this._inputSignals15.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "№";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 24;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn13.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn13.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn13.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this._inputSignals16);
            this.tabPage16.Location = new System.Drawing.Point(4, 49);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(173, 475);
            this.tabPage16.TabIndex = 7;
            this.tabPage16.Text = "ЛС16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // _inputSignals16
            // 
            this._inputSignals16.AllowUserToAddRows = false;
            this._inputSignals16.AllowUserToDeleteRows = false;
            this._inputSignals16.AllowUserToResizeColumns = false;
            this._inputSignals16.AllowUserToResizeRows = false;
            this._inputSignals16.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals16.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals16.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn14});
            this._inputSignals16.Location = new System.Drawing.Point(3, 3);
            this._inputSignals16.MultiSelect = false;
            this._inputSignals16.Name = "_inputSignals16";
            this._inputSignals16.RowHeadersVisible = false;
            this._inputSignals16.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals16.RowsDefaultCellStyle = dataGridViewCellStyle60;
            this._inputSignals16.RowTemplate.Height = 20;
            this._inputSignals16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals16.ShowCellErrors = false;
            this._inputSignals16.ShowCellToolTips = false;
            this._inputSignals16.ShowEditingIcon = false;
            this._inputSignals16.ShowRowErrors = false;
            this._inputSignals16.Size = new System.Drawing.Size(167, 469);
            this._inputSignals16.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "№";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 24;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn14.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn14.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn14.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tabControl1);
            this.groupBox14.Location = new System.Drawing.Point(8, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(193, 547);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Логические сигналы И";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(181, 528);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 49);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(173, 475);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ЛС1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(3, 3);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle61;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(167, 469);
            this._inputSignals1.TabIndex = 2;
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "№";
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "Значение";
            this._signalValueCol.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 49);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(173, 475);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ЛС2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(3, 3);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(167, 469);
            this._inputSignals2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 49);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(173, 475);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ЛС3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Location = new System.Drawing.Point(3, 3);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle63;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(167, 469);
            this._inputSignals3.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 49);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(173, 475);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ЛС4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Location = new System.Drawing.Point(3, 3);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle64;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(167, 469);
            this._inputSignals4.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "№";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._inputSignals5);
            this.tabPage5.Location = new System.Drawing.Point(4, 49);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(173, 475);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ЛС5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _inputSignals5
            // 
            this._inputSignals5.AllowUserToAddRows = false;
            this._inputSignals5.AllowUserToDeleteRows = false;
            this._inputSignals5.AllowUserToResizeColumns = false;
            this._inputSignals5.AllowUserToResizeRows = false;
            this._inputSignals5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals5.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn4});
            this._inputSignals5.Location = new System.Drawing.Point(3, 3);
            this._inputSignals5.MultiSelect = false;
            this._inputSignals5.Name = "_inputSignals5";
            this._inputSignals5.RowHeadersVisible = false;
            this._inputSignals5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals5.RowsDefaultCellStyle = dataGridViewCellStyle65;
            this._inputSignals5.RowTemplate.Height = 20;
            this._inputSignals5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals5.ShowCellErrors = false;
            this._inputSignals5.ShowCellToolTips = false;
            this._inputSignals5.ShowEditingIcon = false;
            this._inputSignals5.ShowRowErrors = false;
            this._inputSignals5.Size = new System.Drawing.Size(167, 469);
            this._inputSignals5.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "№";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this._inputSignals6);
            this.tabPage6.Location = new System.Drawing.Point(4, 49);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(173, 475);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "ЛС6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // _inputSignals6
            // 
            this._inputSignals6.AllowUserToAddRows = false;
            this._inputSignals6.AllowUserToDeleteRows = false;
            this._inputSignals6.AllowUserToResizeColumns = false;
            this._inputSignals6.AllowUserToResizeRows = false;
            this._inputSignals6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals6.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn5});
            this._inputSignals6.Location = new System.Drawing.Point(3, 3);
            this._inputSignals6.MultiSelect = false;
            this._inputSignals6.Name = "_inputSignals6";
            this._inputSignals6.RowHeadersVisible = false;
            this._inputSignals6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals6.RowsDefaultCellStyle = dataGridViewCellStyle66;
            this._inputSignals6.RowTemplate.Height = 20;
            this._inputSignals6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals6.ShowCellErrors = false;
            this._inputSignals6.ShowCellToolTips = false;
            this._inputSignals6.ShowEditingIcon = false;
            this._inputSignals6.ShowRowErrors = false;
            this._inputSignals6.Size = new System.Drawing.Size(167, 469);
            this._inputSignals6.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "№";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 24;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this._inputSignals7);
            this.tabPage7.Location = new System.Drawing.Point(4, 49);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(173, 475);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "ЛС7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // _inputSignals7
            // 
            this._inputSignals7.AllowUserToAddRows = false;
            this._inputSignals7.AllowUserToDeleteRows = false;
            this._inputSignals7.AllowUserToResizeColumns = false;
            this._inputSignals7.AllowUserToResizeRows = false;
            this._inputSignals7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals7.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn6});
            this._inputSignals7.Location = new System.Drawing.Point(3, 3);
            this._inputSignals7.MultiSelect = false;
            this._inputSignals7.Name = "_inputSignals7";
            this._inputSignals7.RowHeadersVisible = false;
            this._inputSignals7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals7.RowsDefaultCellStyle = dataGridViewCellStyle67;
            this._inputSignals7.RowTemplate.Height = 20;
            this._inputSignals7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals7.ShowCellErrors = false;
            this._inputSignals7.ShowCellToolTips = false;
            this._inputSignals7.ShowEditingIcon = false;
            this._inputSignals7.ShowRowErrors = false;
            this._inputSignals7.Size = new System.Drawing.Size(167, 469);
            this._inputSignals7.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "№";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 24;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this._inputSignals8);
            this.tabPage8.Location = new System.Drawing.Point(4, 49);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(173, 475);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "ЛС8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // _inputSignals8
            // 
            this._inputSignals8.AllowUserToAddRows = false;
            this._inputSignals8.AllowUserToDeleteRows = false;
            this._inputSignals8.AllowUserToResizeColumns = false;
            this._inputSignals8.AllowUserToResizeRows = false;
            this._inputSignals8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals8.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn7});
            this._inputSignals8.Location = new System.Drawing.Point(3, 3);
            this._inputSignals8.MultiSelect = false;
            this._inputSignals8.Name = "_inputSignals8";
            this._inputSignals8.RowHeadersVisible = false;
            this._inputSignals8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals8.RowsDefaultCellStyle = dataGridViewCellStyle68;
            this._inputSignals8.RowTemplate.Height = 20;
            this._inputSignals8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals8.ShowCellErrors = false;
            this._inputSignals8.ShowCellToolTips = false;
            this._inputSignals8.ShowEditingIcon = false;
            this._inputSignals8.ShowRowErrors = false;
            this._inputSignals8.Size = new System.Drawing.Size(167, 469);
            this._inputSignals8.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "№";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 24;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._joinPage);
            this._configurationTabControl.Controls.Add(this._inputSygnalsPage);
            this._configurationTabControl.Controls.Add(this._allDefensesPage);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this._urovPage);
            this._configurationTabControl.Controls.Add(this.tabPage20);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(914, 579);
            this._configurationTabControl.TabIndex = 30;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 136);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(212, 22);
            this.clearSetpointsItem.Text = "Обнулить уставки";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // _joinPage
            // 
            this._joinPage.Controls.Add(this._joinData);
            this._joinPage.Location = new System.Drawing.Point(4, 22);
            this._joinPage.Name = "_joinPage";
            this._joinPage.Size = new System.Drawing.Size(906, 553);
            this._joinPage.TabIndex = 2;
            this._joinPage.Text = "Присоединения";
            this._joinPage.UseVisualStyleBackColor = true;
            // 
            // _joinData
            // 
            this._joinData.AllowUserToAddRows = false;
            this._joinData.AllowUserToDeleteRows = false;
            this._joinData.AllowUserToResizeColumns = false;
            this._joinData.AllowUserToResizeRows = false;
            this._joinData.BackgroundColor = System.Drawing.Color.White;
            this._joinData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._joinData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this._joinITT,
            this._joinSwitchOFF,
            this._joinSwitchOn,
            this._joinJoin,
            this._joinEnter,
            this._joinResetColumn,
            this._timeResetJoinColumn});
            this._joinData.Location = new System.Drawing.Point(8, 13);
            this._joinData.MultiSelect = false;
            this._joinData.Name = "_joinData";
            this._joinData.RowHeadersVisible = false;
            this._joinData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._joinData.RowTemplate.Height = 24;
            this._joinData.ShowCellErrors = false;
            this._joinData.ShowRowErrors = false;
            this._joinData.Size = new System.Drawing.Size(811, 421);
            this._joinData.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Stage";
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle69.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle69.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle69.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle69.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewTextBoxColumn15.Frozen = true;
            this.dataGridViewTextBoxColumn15.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 54;
            // 
            // _joinITT
            // 
            this._joinITT.DataPropertyName = "Inom";
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinITT.DefaultCellStyle = dataGridViewCellStyle70;
            this._joinITT.Frozen = true;
            this._joinITT.HeaderText = "Iтт, A";
            this._joinITT.Name = "_joinITT";
            this._joinITT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._joinITT.Width = 70;
            // 
            // _joinSwitchOFF
            // 
            this._joinSwitchOFF.DataPropertyName = "JoinSwitchoff";
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinSwitchOFF.DefaultCellStyle = dataGridViewCellStyle71;
            this._joinSwitchOFF.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinSwitchOFF.Frozen = true;
            this._joinSwitchOFF.HeaderText = "Отключ.";
            this._joinSwitchOFF.Name = "_joinSwitchOFF";
            this._joinSwitchOFF.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._joinSwitchOFF.Width = 110;
            // 
            // _joinSwitchOn
            // 
            this._joinSwitchOn.DataPropertyName = "JoinSwitchon";
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinSwitchOn.DefaultCellStyle = dataGridViewCellStyle72;
            this._joinSwitchOn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinSwitchOn.Frozen = true;
            this._joinSwitchOn.HeaderText = "Включ.";
            this._joinSwitchOn.Name = "_joinSwitchOn";
            this._joinSwitchOn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._joinSwitchOn.Width = 110;
            // 
            // _joinJoin
            // 
            this._joinJoin.DataPropertyName = "JoinJoin";
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinJoin.DefaultCellStyle = dataGridViewCellStyle73;
            this._joinJoin.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinJoin.Frozen = true;
            this._joinJoin.HeaderText = "Привязка";
            this._joinJoin.Name = "_joinJoin";
            this._joinJoin.Width = 110;
            // 
            // _joinEnter
            // 
            this._joinEnter.DataPropertyName = "JoinEnter";
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._joinEnter.DefaultCellStyle = dataGridViewCellStyle74;
            this._joinEnter.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._joinEnter.Frozen = true;
            this._joinEnter.HeaderText = "Вход";
            this._joinEnter.Name = "_joinEnter";
            // 
            // _joinResetColumn
            // 
            this._joinResetColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._joinResetColumn.DataPropertyName = "ResetJoin";
            this._joinResetColumn.Frozen = true;
            this._joinResetColumn.HeaderText = "Обнуление";
            this._joinResetColumn.Name = "_joinResetColumn";
            this._joinResetColumn.Width = 68;
            // 
            // _timeResetJoinColumn
            // 
            this._timeResetJoinColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._timeResetJoinColumn.DataPropertyName = "ResetDeley";
            this._timeResetJoinColumn.Frozen = true;
            this._timeResetJoinColumn.HeaderText = "tобнул, задержка обнуления, мс ";
            this._timeResetJoinColumn.Name = "_timeResetJoinColumn";
            this._timeResetJoinColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeResetJoinColumn.Width = 144;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox26);
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Controls.Add(this.groupBox11);
            this._outputSignalsPage.Controls.Add(this.groupBox12);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Size = new System.Drawing.Size(906, 553);
            this._outputSignalsPage.TabIndex = 6;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._fault4CheckBox);
            this.groupBox26.Controls.Add(this._fault3CheckBox);
            this.groupBox26.Controls.Add(this._fault2CheckBox);
            this.groupBox26.Controls.Add(this._fault1CheckBox);
            this.groupBox26.Controls.Add(this.label1);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Controls.Add(this._impTB);
            this.groupBox26.Location = new System.Drawing.Point(8, 394);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(393, 150);
            this.groupBox26.TabIndex = 6;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Реле неисправность";
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(182, 94);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 22;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(182, 70);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 21;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(182, 46);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 20;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(182, 22);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 19;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "4. Отказ выключателя (УРОВ)";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 118);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(64, 13);
            this.label84.TabIndex = 14;
            this.label84.Text = "Твозвр, мс";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 70);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(148, 13);
            this.label83.TabIndex = 13;
            this.label83.Text = "3. Неисправность цепей ТТ";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 46);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(170, 13);
            this.label82.TabIndex = 12;
            this.label82.Text = "2. Программная неисправность";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 22);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(159, 13);
            this.label81.TabIndex = 11;
            this.label81.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(182, 115);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(121, 20);
            this._impTB.TabIndex = 7;
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.label16);
            this._systemPage.Controls.Add(this.groupBox6);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(906, 553);
            this._systemPage.TabIndex = 10;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(272, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "мс";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._oscSizeTextBox);
            this.groupBox6.Controls.Add(this._oscChannels);
            this.groupBox6.Controls.Add(this._oscWriteLength);
            this.groupBox6.Controls.Add(this._oscFix);
            this.groupBox6.Controls.Add(this._oscLength);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Location = new System.Drawing.Point(8, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(295, 319);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Осцилограф";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(187, 24);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 30;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscChannels
            // 
            this._oscChannels.AllowUserToAddRows = false;
            this._oscChannels.AllowUserToDeleteRows = false;
            this._oscChannels.AllowUserToResizeColumns = false;
            this._oscChannels.AllowUserToResizeRows = false;
            this._oscChannels.BackgroundColor = System.Drawing.Color.White;
            this._oscChannels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this._oscSygnal});
            this._oscChannels.Location = new System.Drawing.Point(21, 91);
            this._oscChannels.Name = "_oscChannels";
            this._oscChannels.RowHeadersVisible = false;
            this._oscChannels.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannels.RowTemplate.Height = 24;
            this._oscChannels.ShowCellErrors = false;
            this._oscChannels.ShowRowErrors = false;
            this._oscChannels.Size = new System.Drawing.Size(205, 219);
            this._oscChannels.TabIndex = 27;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle75.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle75.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle75.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle75;
            this.dataGridViewTextBoxColumn17.Frozen = true;
            this.dataGridViewTextBoxColumn17.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 50;
            // 
            // _oscSygnal
            // 
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscSygnal.DefaultCellStyle = dataGridViewCellStyle76;
            this._oscSygnal.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._oscSygnal.HeaderText = "Сигнал";
            this._oscSygnal.Name = "_oscSygnal";
            this._oscSygnal.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._oscSygnal.Width = 150;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(137, 44);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(137, 64);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(137, 23);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 67);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(66, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Фиксац. по";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(18, 46);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(113, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Длит. предзаписи, %";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(18, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Количество";
            // 
            // _urovPage
            // 
            this._urovPage.Controls.Add(this.groupBox3);
            this._urovPage.Controls.Add(this.groupBox2);
            this._urovPage.Location = new System.Drawing.Point(4, 22);
            this._urovPage.Name = "_urovPage";
            this._urovPage.Size = new System.Drawing.Size(906, 553);
            this._urovPage.TabIndex = 9;
            this._urovPage.Text = "УРОВ";
            this._urovPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._UROVJoinData);
            this.groupBox3.Location = new System.Drawing.Point(277, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(339, 434);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Присоединений";
            // 
            // _UROVJoinData
            // 
            this._UROVJoinData.AllowUserToAddRows = false;
            this._UROVJoinData.AllowUserToDeleteRows = false;
            this._UROVJoinData.AllowUserToResizeColumns = false;
            this._UROVJoinData.AllowUserToResizeRows = false;
            this._UROVJoinData.BackgroundColor = System.Drawing.Color.White;
            this._UROVJoinData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._UROVJoinData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this._JoinIUROV,
            this._JoinTUROV});
            this._UROVJoinData.Location = new System.Drawing.Point(6, 19);
            this._UROVJoinData.MultiSelect = false;
            this._UROVJoinData.Name = "_UROVJoinData";
            this._UROVJoinData.RowHeadersVisible = false;
            this._UROVJoinData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._UROVJoinData.RowTemplate.Height = 24;
            this._UROVJoinData.ShowCellErrors = false;
            this._UROVJoinData.ShowRowErrors = false;
            this._UROVJoinData.Size = new System.Drawing.Size(326, 409);
            this._UROVJoinData.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle77.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle77.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle77;
            this.dataGridViewTextBoxColumn16.Frozen = true;
            this.dataGridViewTextBoxColumn16.HeaderText = "Ступень";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // _JoinIUROV
            // 
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._JoinIUROV.DefaultCellStyle = dataGridViewCellStyle78;
            this._JoinIUROV.HeaderText = "Iуров, Iн";
            this._JoinIUROV.Name = "_JoinIUROV";
            this._JoinIUROV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._JoinIUROV.Width = 70;
            // 
            // _JoinTUROV
            // 
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._JoinTUROV.DefaultCellStyle = dataGridViewCellStyle79;
            this._JoinTUROV.HeaderText = "tуров, мс";
            this._JoinTUROV.Name = "_JoinTUROV";
            this._JoinTUROV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._DZHTUrov3);
            this.groupBox2.Controls.Add(this._DZHTUrov2);
            this.groupBox2.Controls.Add(this._DZHTUrov1);
            this.groupBox2.Controls.Add(this._DZHPO);
            this.groupBox2.Controls.Add(this._DZHSH2);
            this.groupBox2.Controls.Add(this._DZHSH1);
            this.groupBox2.Controls.Add(this._DZHSign);
            this.groupBox2.Controls.Add(this._DZHSelf);
            this.groupBox2.Controls.Add(this._DZHDiff);
            this.groupBox2.Controls.Add(this._DZHKontr);
            this.groupBox2.Controls.Add(this._DZHModes);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(8, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(263, 259);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ДЗШ";
            // 
            // _DZHTUrov3
            // 
            this._DZHTUrov3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._DZHTUrov3.Location = new System.Drawing.Point(101, 147);
            this._DZHTUrov3.Name = "_DZHTUrov3";
            this._DZHTUrov3.Size = new System.Drawing.Size(121, 20);
            this._DZHTUrov3.TabIndex = 24;
            this._DZHTUrov3.Tag = "3000000";
            this._DZHTUrov3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _DZHTUrov2
            // 
            this._DZHTUrov2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._DZHTUrov2.Location = new System.Drawing.Point(101, 127);
            this._DZHTUrov2.Name = "_DZHTUrov2";
            this._DZHTUrov2.Size = new System.Drawing.Size(121, 20);
            this._DZHTUrov2.TabIndex = 23;
            this._DZHTUrov2.Tag = "3000000";
            this._DZHTUrov2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _DZHTUrov1
            // 
            this._DZHTUrov1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._DZHTUrov1.Location = new System.Drawing.Point(101, 107);
            this._DZHTUrov1.Name = "_DZHTUrov1";
            this._DZHTUrov1.Size = new System.Drawing.Size(121, 20);
            this._DZHTUrov1.TabIndex = 22;
            this._DZHTUrov1.Tag = "3000000";
            this._DZHTUrov1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _DZHPO
            // 
            this._DZHPO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHPO.FormattingEnabled = true;
            this._DZHPO.Location = new System.Drawing.Point(101, 230);
            this._DZHPO.Name = "_DZHPO";
            this._DZHPO.Size = new System.Drawing.Size(121, 21);
            this._DZHPO.TabIndex = 21;
            // 
            // _DZHSH2
            // 
            this._DZHSH2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSH2.FormattingEnabled = true;
            this._DZHSH2.Location = new System.Drawing.Point(101, 209);
            this._DZHSH2.Name = "_DZHSH2";
            this._DZHSH2.Size = new System.Drawing.Size(121, 21);
            this._DZHSH2.TabIndex = 20;
            // 
            // _DZHSH1
            // 
            this._DZHSH1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSH1.FormattingEnabled = true;
            this._DZHSH1.Location = new System.Drawing.Point(101, 188);
            this._DZHSH1.Name = "_DZHSH1";
            this._DZHSH1.Size = new System.Drawing.Size(121, 21);
            this._DZHSH1.TabIndex = 19;
            // 
            // _DZHSign
            // 
            this._DZHSign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSign.FormattingEnabled = true;
            this._DZHSign.Location = new System.Drawing.Point(101, 167);
            this._DZHSign.Name = "_DZHSign";
            this._DZHSign.Size = new System.Drawing.Size(121, 21);
            this._DZHSign.TabIndex = 18;
            // 
            // _DZHSelf
            // 
            this._DZHSelf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHSelf.FormattingEnabled = true;
            this._DZHSelf.Location = new System.Drawing.Point(101, 86);
            this._DZHSelf.Name = "_DZHSelf";
            this._DZHSelf.Size = new System.Drawing.Size(121, 21);
            this._DZHSelf.TabIndex = 14;
            // 
            // _DZHDiff
            // 
            this._DZHDiff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHDiff.FormattingEnabled = true;
            this._DZHDiff.Location = new System.Drawing.Point(101, 65);
            this._DZHDiff.Name = "_DZHDiff";
            this._DZHDiff.Size = new System.Drawing.Size(121, 21);
            this._DZHDiff.TabIndex = 13;
            // 
            // _DZHKontr
            // 
            this._DZHKontr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHKontr.FormattingEnabled = true;
            this._DZHKontr.Location = new System.Drawing.Point(101, 44);
            this._DZHKontr.Name = "_DZHKontr";
            this._DZHKontr.Size = new System.Drawing.Size(121, 21);
            this._DZHKontr.TabIndex = 12;
            // 
            // _DZHModes
            // 
            this._DZHModes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._DZHModes.FormattingEnabled = true;
            this._DZHModes.Location = new System.Drawing.Point(101, 23);
            this._DZHModes.Name = "_DZHModes";
            this._DZHModes.Size = new System.Drawing.Size(121, 21);
            this._DZHModes.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 233);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "ПО";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 212);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "СШ2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "СШ1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "От сигн";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "tуров3, мс";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "tуров2, мс";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "tуров1, мс";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "На себя";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "От защит";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Контр";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Режим";
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this._configTtDgv);
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(906, 553);
            this.tabPage20.TabIndex = 11;
            this.tabPage20.Text = "Контроль цепей ТТ";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // _configTtDgv
            // 
            this._configTtDgv.AllowUserToAddRows = false;
            this._configTtDgv.AllowUserToDeleteRows = false;
            this._configTtDgv.AllowUserToResizeColumns = false;
            this._configTtDgv.AllowUserToResizeRows = false;
            this._configTtDgv.BackgroundColor = System.Drawing.Color.White;
            this._configTtDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._configTtDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._nameTtColumn,
            this._idTtColumn,
            this._timeSrabTtColumn,
            this._faultTtColumn});
            this._configTtDgv.Location = new System.Drawing.Point(8, 6);
            this._configTtDgv.MultiSelect = false;
            this._configTtDgv.Name = "_configTtDgv";
            this._configTtDgv.RowHeadersVisible = false;
            this._configTtDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._configTtDgv.RowTemplate.Height = 24;
            this._configTtDgv.ShowCellErrors = false;
            this._configTtDgv.ShowRowErrors = false;
            this._configTtDgv.Size = new System.Drawing.Size(460, 99);
            this._configTtDgv.TabIndex = 6;
            // 
            // _nameTtColumn
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle80.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle80.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle80.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle80.SelectionForeColor = System.Drawing.Color.White;
            this._nameTtColumn.DefaultCellStyle = dataGridViewCellStyle80;
            this._nameTtColumn.Frozen = true;
            this._nameTtColumn.HeaderText = "Контроль цепей ТТ";
            this._nameTtColumn.Name = "_nameTtColumn";
            this._nameTtColumn.ReadOnly = true;
            this._nameTtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._nameTtColumn.Width = 120;
            // 
            // _idTtColumn
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._idTtColumn.DefaultCellStyle = dataGridViewCellStyle81;
            this._idTtColumn.HeaderText = "Iдmin, Iн";
            this._idTtColumn.Name = "_idTtColumn";
            this._idTtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _timeSrabTtColumn
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._timeSrabTtColumn.DefaultCellStyle = dataGridViewCellStyle82;
            this._timeSrabTtColumn.HeaderText = "Tср, мс";
            this._timeSrabTtColumn.Name = "_timeSrabTtColumn";
            this._timeSrabTtColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _faultTtColumn
            // 
            this._faultTtColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._faultTtColumn.HeaderText = "Неисправность";
            this._faultTtColumn.Name = "_faultTtColumn";
            this._faultTtColumn.Width = 135;
            // 
            // _dif0DataGreed
            // 
            this._dif0DataGreed.AllowUserToAddRows = false;
            this._dif0DataGreed.AllowUserToDeleteRows = false;
            this._dif0DataGreed.AllowUserToResizeColumns = false;
            this._dif0DataGreed.AllowUserToResizeRows = false;
            this._dif0DataGreed.BackgroundColor = System.Drawing.Color.White;
            this._dif0DataGreed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dif0DataGreed.Location = new System.Drawing.Point(6, 21);
            this._dif0DataGreed.Name = "_dif0DataGreed";
            this._dif0DataGreed.RowHeadersVisible = false;
            this._dif0DataGreed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._dif0DataGreed.RowTemplate.Height = 24;
            this._dif0DataGreed.ShowCellErrors = false;
            this._dif0DataGreed.ShowRowErrors = false;
            this._dif0DataGreed.Size = new System.Drawing.Size(751, 112);
            this._dif0DataGreed.TabIndex = 2;
            // 
            // _dif0AVRColumn
            // 
            this._dif0AVRColumn.HeaderText = "АВР";
            this._dif0AVRColumn.Name = "_dif0AVRColumn";
            this._dif0AVRColumn.Visible = false;
            // 
            // _dif0APVColumn
            // 
            this._dif0APVColumn.HeaderText = "АПВ";
            this._dif0APVColumn.Name = "_dif0APVColumn";
            this._dif0APVColumn.Visible = false;
            // 
            // _dif0UrovColumn
            // 
            this._dif0UrovColumn.HeaderText = "Уров";
            this._dif0UrovColumn.Name = "_dif0UrovColumn";
            this._dif0UrovColumn.Visible = false;
            // 
            // _dif0OscColumn
            // 
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0OscColumn.DefaultCellStyle = dataGridViewCellStyle83;
            this._dif0OscColumn.HeaderText = "Осциллограф";
            this._dif0OscColumn.MaxDropDownItems = 15;
            this._dif0OscColumn.Name = "_dif0OscColumn";
            this._dif0OscColumn.Width = 90;
            // 
            // _dif0Intg2Column
            // 
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Intg2Column.DefaultCellStyle = dataGridViewCellStyle84;
            this._dif0Intg2Column.HeaderText = "Угол f2";
            this._dif0Intg2Column.Name = "_dif0Intg2Column";
            this._dif0Intg2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Intg2Column.Width = 55;
            // 
            // _dif0Ib2Column
            // 
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Ib2Column.DefaultCellStyle = dataGridViewCellStyle85;
            this._dif0Ib2Column.HeaderText = "Iб2 [Iн стороны]";
            this._dif0Ib2Column.Name = "_dif0Ib2Column";
            this._dif0Ib2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Ib2Column.Width = 115;
            // 
            // _dif0Intg1Column
            // 
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Intg1Column.DefaultCellStyle = dataGridViewCellStyle86;
            this._dif0Intg1Column.HeaderText = "Угол f1";
            this._dif0Intg1Column.Name = "_dif0Intg1Column";
            this._dif0Intg1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Intg1Column.Width = 55;
            // 
            // _dif0Ib1Column
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Ib1Column.DefaultCellStyle = dataGridViewCellStyle87;
            this._dif0Ib1Column.HeaderText = "Iб1 [Iн стороны]";
            this._dif0Ib1Column.Name = "_dif0Ib1Column";
            this._dif0Ib1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Ib1Column.Width = 115;
            // 
            // _dif0TdColumn
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0TdColumn.DefaultCellStyle = dataGridViewCellStyle88;
            this._dif0TdColumn.HeaderText = "tд [мс]";
            this._dif0TdColumn.Name = "_dif0TdColumn";
            this._dif0TdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0TdColumn.Width = 50;
            // 
            // _dif0InColumn
            // 
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0InColumn.DefaultCellStyle = dataGridViewCellStyle89;
            this._dif0InColumn.HeaderText = "Сторона";
            this._dif0InColumn.MaxDropDownItems = 15;
            this._dif0InColumn.Name = "_dif0InColumn";
            this._dif0InColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0InColumn.Width = 85;
            // 
            // _dif0IdColumn
            // 
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0IdColumn.DefaultCellStyle = dataGridViewCellStyle90;
            this._dif0IdColumn.HeaderText = "Iд [Iн стороны]";
            this._dif0IdColumn.Name = "_dif0IdColumn";
            this._dif0IdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0IdColumn.Width = 110;
            // 
            // _dif0BlockingColumn
            // 
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0BlockingColumn.DefaultCellStyle = dataGridViewCellStyle91;
            this._dif0BlockingColumn.HeaderText = "Блокировка";
            this._dif0BlockingColumn.MaxDropDownItems = 15;
            this._dif0BlockingColumn.Name = "_dif0BlockingColumn";
            this._dif0BlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0BlockingColumn.Width = 85;
            // 
            // _dif0ModeColumn
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0ModeColumn.DefaultCellStyle = dataGridViewCellStyle92;
            this._dif0ModeColumn.HeaderText = "Состояние";
            this._dif0ModeColumn.MaxDropDownItems = 15;
            this._dif0ModeColumn.Name = "_dif0ModeColumn";
            this._dif0ModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0ModeColumn.Width = 90;
            // 
            // _dif0StageColumn
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle93.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle93.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle93.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle93.SelectionForeColor = System.Drawing.Color.White;
            this._dif0StageColumn.DefaultCellStyle = dataGridViewCellStyle93;
            this._dif0StageColumn.Frozen = true;
            this._dif0StageColumn.HeaderText = "Ступень";
            this._dif0StageColumn.Name = "_dif0StageColumn";
            this._dif0StageColumn.ReadOnly = true;
            this._dif0StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0StageColumn.Width = 78;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 18);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(61, 13);
            this.label47.TabIndex = 0;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 97);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(189, 13);
            this.label48.TabIndex = 1;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 58);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(74, 13);
            this.label49.TabIndex = 2;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 77);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(135, 13);
            this.label50.TabIndex = 3;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 117);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(37, 13);
            this.label52.TabIndex = 5;
            this.label52.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 137);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(76, 13);
            this.label53.TabIndex = 6;
            // 
            // _modeDTOBTComboBox
            // 
            this._modeDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTOBTComboBox.FormattingEnabled = true;
            this._modeDTOBTComboBox.Location = new System.Drawing.Point(196, 15);
            this._modeDTOBTComboBox.Name = "_modeDTOBTComboBox";
            this._modeDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTOBTComboBox.TabIndex = 7;
            // 
            // _stepOnInstantValuesDTOBTComboBox
            // 
            this._stepOnInstantValuesDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._stepOnInstantValuesDTOBTComboBox.FormattingEnabled = true;
            this._stepOnInstantValuesDTOBTComboBox.Location = new System.Drawing.Point(196, 94);
            this._stepOnInstantValuesDTOBTComboBox.Name = "_stepOnInstantValuesDTOBTComboBox";
            this._stepOnInstantValuesDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._stepOnInstantValuesDTOBTComboBox.TabIndex = 8;
            // 
            // _UROVDTOBTComboBox
            // 
            this._UROVDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UROVDTOBTComboBox.FormattingEnabled = true;
            this._UROVDTOBTComboBox.Location = new System.Drawing.Point(196, 114);
            this._UROVDTOBTComboBox.Name = "_UROVDTOBTComboBox";
            this._UROVDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._UROVDTOBTComboBox.TabIndex = 12;
            this._UROVDTOBTComboBox.Visible = false;
            // 
            // _oscDTOBTComboBox
            // 
            this._oscDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTOBTComboBox.FormattingEnabled = true;
            this._oscDTOBTComboBox.Location = new System.Drawing.Point(196, 134);
            this._oscDTOBTComboBox.Name = "_oscDTOBTComboBox";
            this._oscDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTOBTComboBox.TabIndex = 13;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.ForeColor = System.Drawing.Color.Red;
            this.label54.Location = new System.Drawing.Point(287, 58);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(22, 13);
            this.label54.TabIndex = 14;
            // 
            // _blockingDTOBTComboBox
            // 
            this._blockingDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTOBTComboBox.FormattingEnabled = true;
            this._blockingDTOBTComboBox.Location = new System.Drawing.Point(196, 35);
            this._blockingDTOBTComboBox.Name = "_blockingDTOBTComboBox";
            this._blockingDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._blockingDTOBTComboBox.TabIndex = 11;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 38);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(68, 13);
            this.label51.TabIndex = 4;
            // 
            // _constraintDTOBTTextBox
            // 
            this._constraintDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTOBTTextBox.Location = new System.Drawing.Point(196, 55);
            this._constraintDTOBTTextBox.Name = "_constraintDTOBTTextBox";
            this._constraintDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTOBTTextBox.TabIndex = 15;
            this._constraintDTOBTTextBox.Tag = "40";
            this._constraintDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.Color.Red;
            this.label55.Location = new System.Drawing.Point(287, 77);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 13);
            this.label55.TabIndex = 15;
            // 
            // _timeEnduranceDTOBTTextBox
            // 
            this._timeEnduranceDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTOBTTextBox.Location = new System.Drawing.Point(196, 74);
            this._timeEnduranceDTOBTTextBox.Name = "_timeEnduranceDTOBTTextBox";
            this._timeEnduranceDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTOBTTextBox.TabIndex = 16;
            this._timeEnduranceDTOBTTextBox.Tag = "3276700";
            this._timeEnduranceDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(6, 157);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(29, 13);
            this.label128.TabIndex = 17;
            this.label128.Visible = false;
            // 
            // _APVDTOBTComboBox
            // 
            this._APVDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APVDTOBTComboBox.FormattingEnabled = true;
            this._APVDTOBTComboBox.Location = new System.Drawing.Point(196, 154);
            this._APVDTOBTComboBox.Name = "_APVDTOBTComboBox";
            this._APVDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._APVDTOBTComboBox.TabIndex = 18;
            this._APVDTOBTComboBox.Visible = false;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(6, 177);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(28, 13);
            this.label129.TabIndex = 19;
            this.label129.Visible = false;
            // 
            // _AVRDTOBTComboBox
            // 
            this._AVRDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVRDTOBTComboBox.FormattingEnabled = true;
            this._AVRDTOBTComboBox.Location = new System.Drawing.Point(196, 174);
            this._AVRDTOBTComboBox.Name = "_AVRDTOBTComboBox";
            this._AVRDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._AVRDTOBTComboBox.TabIndex = 20;
            this._AVRDTOBTComboBox.Visible = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 16);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(61, 13);
            this.label56.TabIndex = 8;
            // 
            // _modeDTZComboBox
            // 
            this._modeDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTZComboBox.FormattingEnabled = true;
            this._modeDTZComboBox.Location = new System.Drawing.Point(148, 13);
            this._modeDTZComboBox.Name = "_modeDTZComboBox";
            this._modeDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTZComboBox.TabIndex = 9;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 57);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 13);
            this.label61.TabIndex = 16;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 76);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(129, 13);
            this.label59.TabIndex = 17;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.ForeColor = System.Drawing.Color.Red;
            this.label58.Location = new System.Drawing.Point(239, 57);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(22, 13);
            this.label58.TabIndex = 20;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Location = new System.Drawing.Point(239, 76);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(27, 13);
            this.label57.TabIndex = 21;
            // 
            // groupBox8
            // 
            this.groupBox8.Location = new System.Drawing.Point(6, 264);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(261, 107);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 24);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(66, 13);
            this.label62.TabIndex = 2;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 43);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(92, 13);
            this.label63.TabIndex = 3;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 62);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(66, 13);
            this.label64.TabIndex = 4;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 81);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(92, 13);
            this.label65.TabIndex = 5;
            // 
            // _Ib1BeginTextBox
            // 
            this._Ib1BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib1BeginTextBox.Location = new System.Drawing.Point(141, 21);
            this._Ib1BeginTextBox.Name = "_Ib1BeginTextBox";
            this._Ib1BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib1BeginTextBox.TabIndex = 15;
            this._Ib1BeginTextBox.Tag = "40";
            this._Ib1BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _K1AngleOfSlopeTextBox
            // 
            this._K1AngleOfSlopeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K1AngleOfSlopeTextBox.Location = new System.Drawing.Point(141, 40);
            this._K1AngleOfSlopeTextBox.Name = "_K1AngleOfSlopeTextBox";
            this._K1AngleOfSlopeTextBox.Size = new System.Drawing.Size(90, 20);
            this._K1AngleOfSlopeTextBox.TabIndex = 16;
            this._K1AngleOfSlopeTextBox.Tag = "89";
            this._K1AngleOfSlopeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(232, 24);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(22, 13);
            this.label66.TabIndex = 21;
            // 
            // _Ib2BeginTextBox
            // 
            this._Ib2BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib2BeginTextBox.Location = new System.Drawing.Point(141, 59);
            this._Ib2BeginTextBox.Name = "_Ib2BeginTextBox";
            this._Ib2BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib2BeginTextBox.TabIndex = 17;
            this._Ib2BeginTextBox.Tag = "40";
            this._Ib2BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(232, 62);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(22, 13);
            this.label69.TabIndex = 22;
            // 
            // _K2TangensTextBox
            // 
            this._K2TangensTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K2TangensTextBox.Location = new System.Drawing.Point(141, 78);
            this._K2TangensTextBox.Name = "_K2TangensTextBox";
            this._K2TangensTextBox.Size = new System.Drawing.Size(90, 20);
            this._K2TangensTextBox.TabIndex = 18;
            this._K2TangensTextBox.Tag = "89";
            this._K2TangensTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(232, 43);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(11, 13);
            this.label70.TabIndex = 23;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Location = new System.Drawing.Point(232, 81);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(11, 13);
            this.label71.TabIndex = 24;
            // 
            // _constraintDTZTextBox
            // 
            this._constraintDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTZTextBox.Location = new System.Drawing.Point(148, 53);
            this._constraintDTZTextBox.Name = "_constraintDTZTextBox";
            this._constraintDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTZTextBox.TabIndex = 13;
            this._constraintDTZTextBox.Tag = "40";
            this._constraintDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox9
            // 
            this.groupBox9.Location = new System.Drawing.Point(6, 104);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(261, 73);
            this.groupBox9.TabIndex = 23;
            this.groupBox9.TabStop = false;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 49);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(123, 13);
            this.label73.TabIndex = 22;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(232, 49);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(15, 13);
            this.label72.TabIndex = 24;
            // 
            // _I2I1TextBox
            // 
            this._I2I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2I1TextBox.Location = new System.Drawing.Point(141, 46);
            this._I2I1TextBox.Name = "_I2I1TextBox";
            this._I2I1TextBox.Size = new System.Drawing.Size(90, 20);
            this._I2I1TextBox.TabIndex = 19;
            this._I2I1TextBox.Tag = "100";
            this._I2I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 25);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 30;
            // 
            // _perBlockI2I1
            // 
            this._perBlockI2I1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._perBlockI2I1.FormattingEnabled = true;
            this._perBlockI2I1.Location = new System.Drawing.Point(141, 19);
            this._perBlockI2I1.Name = "_perBlockI2I1";
            this._perBlockI2I1.Size = new System.Drawing.Size(90, 21);
            this._perBlockI2I1.TabIndex = 36;
            // 
            // _timeEnduranceDTZTextBox
            // 
            this._timeEnduranceDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTZTextBox.Location = new System.Drawing.Point(148, 72);
            this._timeEnduranceDTZTextBox.Name = "_timeEnduranceDTZTextBox";
            this._timeEnduranceDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTZTextBox.TabIndex = 14;
            this._timeEnduranceDTZTextBox.Tag = "3276700";
            this._timeEnduranceDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 36);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(68, 13);
            this.label78.TabIndex = 24;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 376);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(37, 13);
            this.label77.TabIndex = 25;
            this.label77.Visible = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 396);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(76, 13);
            this.label76.TabIndex = 26;
            // 
            // _blockingDTZComboBox
            // 
            this._blockingDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTZComboBox.FormattingEnabled = true;
            this._blockingDTZComboBox.Location = new System.Drawing.Point(148, 33);
            this._blockingDTZComboBox.Name = "_blockingDTZComboBox";
            this._blockingDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._blockingDTZComboBox.TabIndex = 27;
            // 
            // _UROVDTZComboBox
            // 
            this._UROVDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UROVDTZComboBox.FormattingEnabled = true;
            this._UROVDTZComboBox.Location = new System.Drawing.Point(148, 373);
            this._UROVDTZComboBox.Name = "_UROVDTZComboBox";
            this._UROVDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._UROVDTZComboBox.TabIndex = 28;
            this._UROVDTZComboBox.Visible = false;
            // 
            // _oscDTZComboBox
            // 
            this._oscDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTZComboBox.FormattingEnabled = true;
            this._oscDTZComboBox.Location = new System.Drawing.Point(148, 393);
            this._oscDTZComboBox.Name = "_oscDTZComboBox";
            this._oscDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTZComboBox.TabIndex = 29;
            // 
            // _modeI2I1CB
            // 
            this._modeI2I1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeI2I1CB.FormattingEnabled = true;
            this._modeI2I1CB.Location = new System.Drawing.Point(125, 99);
            this._modeI2I1CB.Name = "_modeI2I1CB";
            this._modeI2I1CB.Size = new System.Drawing.Size(90, 21);
            this._modeI2I1CB.TabIndex = 40;
            // 
            // groupBox25
            // 
            this.groupBox25.Location = new System.Drawing.Point(6, 185);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(261, 73);
            this.groupBox25.TabIndex = 41;
            this.groupBox25.TabStop = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(6, 49);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(97, 13);
            this.label75.TabIndex = 38;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(233, 49);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(15, 13);
            this.label74.TabIndex = 39;
            // 
            // _I5I1TextBox
            // 
            this._I5I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I5I1TextBox.Location = new System.Drawing.Point(141, 46);
            this._I5I1TextBox.Name = "_I5I1TextBox";
            this._I5I1TextBox.Size = new System.Drawing.Size(90, 20);
            this._I5I1TextBox.TabIndex = 37;
            this._I5I1TextBox.Tag = "100";
            this._I5I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 13);
            this.label24.TabIndex = 41;
            // 
            // _perBlockI5I1
            // 
            this._perBlockI5I1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._perBlockI5I1.FormattingEnabled = true;
            this._perBlockI5I1.Location = new System.Drawing.Point(141, 21);
            this._perBlockI5I1.Name = "_perBlockI5I1";
            this._perBlockI5I1.Size = new System.Drawing.Size(90, 21);
            this._perBlockI5I1.TabIndex = 42;
            // 
            // _modeI5I1CB
            // 
            this._modeI5I1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeI5I1CB.FormattingEnabled = true;
            this._modeI5I1CB.Location = new System.Drawing.Point(125, 181);
            this._modeI5I1CB.Name = "_modeI5I1CB";
            this._modeI5I1CB.Size = new System.Drawing.Size(90, 21);
            this._modeI5I1CB.TabIndex = 42;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(6, 416);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(29, 13);
            this.label130.TabIndex = 43;
            this.label130.Visible = false;
            // 
            // _APVDTZComboBox
            // 
            this._APVDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APVDTZComboBox.FormattingEnabled = true;
            this._APVDTZComboBox.Location = new System.Drawing.Point(148, 413);
            this._APVDTZComboBox.Name = "_APVDTZComboBox";
            this._APVDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._APVDTZComboBox.TabIndex = 44;
            this._APVDTZComboBox.Visible = false;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(6, 436);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(28, 13);
            this.label131.TabIndex = 45;
            this.label131.Visible = false;
            // 
            // _AVRDTZComboBox
            // 
            this._AVRDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVRDTZComboBox.FormattingEnabled = true;
            this._AVRDTZComboBox.Location = new System.Drawing.Point(148, 433);
            this._AVRDTZComboBox.Name = "_AVRDTZComboBox";
            this._AVRDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._AVRDTZComboBox.TabIndex = 46;
            this._AVRDTZComboBox.Visible = false;
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР901";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "МР901_уставки";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР901";
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.Location = new System.Drawing.Point(103, 1);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.miniToolStrip.Size = new System.Drawing.Size(830, 22);
            this.miniToolStrip.TabIndex = 26;
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 110;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 620);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(914, 22);
            this._statusStrip.TabIndex = 26;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(766, 594);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 37;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(314, 594);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(149, 23);
            this._resetSetpointsButton.TabIndex = 36;
            this._resetSetpointsButton.Text = "Обнулить уставки";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(159, 594);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 33;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(4, 594);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(151, 23);
            this._readConfigBut.TabIndex = 32;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(624, 594);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(136, 23);
            this._saveConfigBut.TabIndex = 35;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(469, 594);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(149, 23);
            this._loadConfigBut.TabIndex = 34;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.xml) | *.xml";
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.White;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle44;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "Ступень";
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.ReadOnly = true;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle45;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Режим";
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifModesColumn.Width = 95;
            // 
            // _externalDifOtklColumn
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOtklColumn.DefaultCellStyle = dataGridViewCellStyle46;
            this._externalDifOtklColumn.HeaderText = "Отключение";
            this._externalDifOtklColumn.Name = "_externalDifOtklColumn";
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle47;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifBlockingColumn.Width = 120;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle48;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle49;
            this._externalDifTsrColumn.HeaderText = "tср, мс";
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 70;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle50;
            this._externalDifTvzColumn.HeaderText = "tвз, мс";
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 70;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle51;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврата";
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifVozvrColumn.Width = 110;
            // 
            // _externalDifVozvrYNColumn
            // 
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifVozvrYNColumn.Width = 60;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle52;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifUROVColumn
            // 
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifUROVColumn.Width = 60;
            // 
            // Mr901ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 642);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._resetSetpointsButton);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._configurationTabControl);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Mr901ConfigurationForm";
            this.Text = "Конфигурация";
            this.Activated += new System.EventHandler(this.Mr901ConfigurationForm_Activated);
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr901ConfigurationForm_KeyUp);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this._allDefensesPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this._difensesTC.ResumeLayout(false);
            this.tabPage21.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage17.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difDDataGrid)).EndInit();
            this.tabPage18.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difMDataGrid)).EndInit();
            this.tabPage22.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._MTZDifensesDataGrid)).EndInit();
            this.tabPage19.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this._inputSygnalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).EndInit();
            this._configurationTabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._joinPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._joinData)).EndInit();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this._systemPage.ResumeLayout(false);
            this._systemPage.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannels)).EndInit();
            this._urovPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._UROVJoinData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._configTtDgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dif0DataGreed)).EndInit();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox16;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox13;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox15;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox14;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox12;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox11;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox9;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox10;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.TabPage _allDefensesPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton _mainRadioButton;
        private System.Windows.Forms.RadioButton _reserveRadioButton;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TabPage _inputSygnalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox _grUstComboBox;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView _inputSignals9;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView _inputSignals10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView _inputSignals11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView _inputSignals12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.DataGridView _inputSignals13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.DataGridView _inputSignals14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.DataGridView _inputSignals15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.DataGridView _inputSignals16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView _inputSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView _inputSignals2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView _inputSignals3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView _inputSignals4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView _inputSignals5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView _inputSignals6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView _inputSignals7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.DataGridView _inputSignals8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _joinPage;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.TabControl _difensesTC;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.DataGridView _MTZDifensesDataGrid;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.DataGridView _dif0DataGreed;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0AVRColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0APVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0UrovColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0OscColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Intg2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Ib2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Intg1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Ib1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0TdColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0InColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0IdColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0BlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0ModeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0StageColumn;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox _modeDTOBTComboBox;
        private System.Windows.Forms.ComboBox _stepOnInstantValuesDTOBTComboBox;
        private System.Windows.Forms.ComboBox _UROVDTOBTComboBox;
        private System.Windows.Forms.ComboBox _oscDTOBTComboBox;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox _blockingDTOBTComboBox;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox _constraintDTOBTTextBox;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTOBTTextBox;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.ComboBox _APVDTOBTComboBox;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.ComboBox _AVRDTOBTComboBox;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox _modeDTZComboBox;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.MaskedTextBox _Ib1BeginTextBox;
        private System.Windows.Forms.MaskedTextBox _K1AngleOfSlopeTextBox;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.MaskedTextBox _Ib2BeginTextBox;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.MaskedTextBox _K2TangensTextBox;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.MaskedTextBox _constraintDTZTextBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.MaskedTextBox _I2I1TextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox _perBlockI2I1;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTZTextBox;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox _blockingDTZComboBox;
        private System.Windows.Forms.ComboBox _UROVDTZComboBox;
        private System.Windows.Forms.ComboBox _oscDTZComboBox;
        private System.Windows.Forms.ComboBox _modeI2I1CB;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.MaskedTextBox _I5I1TextBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox _perBlockI5I1;
        private System.Windows.Forms.ComboBox _modeI5I1CB;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.ComboBox _APVDTZComboBox;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.ComboBox _AVRDTZComboBox;
        private System.Windows.Forms.TabPage _urovPage;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.DataGridView _externalDifensesDataGrid;
        private System.Windows.Forms.DataGridView _joinData;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.DataGridView _difDDataGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView _difMDataGrid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _DZHSH2;
        private System.Windows.Forms.ComboBox _DZHSH1;
        private System.Windows.Forms.ComboBox _DZHSign;
        private System.Windows.Forms.ComboBox _DZHSelf;
        private System.Windows.Forms.ComboBox _DZHDiff;
        private System.Windows.Forms.ComboBox _DZHKontr;
        private System.Windows.Forms.ComboBox _DZHModes;
        private System.Windows.Forms.MaskedTextBox _DZHTUrov3;
        private System.Windows.Forms.MaskedTextBox _DZHTUrov2;
        private System.Windows.Forms.MaskedTextBox _DZHTUrov1;
        private System.Windows.Forms.ComboBox _DZHPO;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView _UROVJoinData;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _oscFix;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.DataGridView _oscChannels;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.DataGridView _configTtDgv;
        private System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn _JoinIUROV;
        private System.Windows.Forms.DataGridViewTextBoxColumn _JoinTUROV;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.StatusStrip miniToolStrip;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndColorCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn _oscSygnal;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDBlockColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIcpColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIdoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDtcpColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIbColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDfColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDBlockGColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDI2gColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDBlock5GColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDI5gColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDOprNasColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDOchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDIochColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difDtochColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDEnterOchColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difDOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difDUrovColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMBlockColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIcpColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIdoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIbColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMfColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difMOchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMIochColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _difMtochColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMEnterOchColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _difMOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _difMUrovColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzMeasureColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzICPColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzCharColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzTColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _mtzkColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _mtzOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _mtzUROVColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _nameTtColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _idTtColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeSrabTtColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _faultTtColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn _joinITT;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinSwitchOFF;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinSwitchOn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinJoin;
        private System.Windows.Forms.DataGridViewComboBoxColumn _joinEnter;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _joinResetColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeResetJoinColumn;
        private System.Windows.Forms.Button _groupChangeButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOtklColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
    }
}