﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR901.Old.Configuration.Structures.Defenses
{
    /// <summary>
    /// защиты по двум группам уставок
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Все_защиты")]
    public class AllDefensesSetpointsStruct : StructBase, ISetpointContainer<SetpointStruct>
    {
        #region [Public fields]
        [Layout(0)] private SetpointStruct _mainSetpoints;
        [Layout(1)] private SetpointStruct _reserveSetpoints;
        #endregion [Public fields]

        [XmlIgnore]
        public SetpointStruct[] Setpoints
        {
            get
            {
                return new[]
                    {
                        this._mainSetpoints.Clone<SetpointStruct>(),
                        this._reserveSetpoints.Clone<SetpointStruct>()
                    };
            }
            set
            {
                this._mainSetpoints = value[0].Clone<SetpointStruct>();
                this._reserveSetpoints = value[1].Clone<SetpointStruct>();
            }
        }

        /// <summary>
        /// Основная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Основная_группа_уставок")]
        public SetpointStruct MainSetpoints
        {
            get { return _mainSetpoints; }
            set { _mainSetpoints = value; }
        }

        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Резервная_группа_уставок")]
        public SetpointStruct ReserveSetpoints
        {
            get { return _reserveSetpoints; }
            set { _reserveSetpoints = value; }
        }
    }
}
