﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Old.Configuration.Structures.Defenses.External
{
    public class AllExternalDefenseStruct : StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        public const int COUNT = 16;
        [Layout(0, Count = COUNT)]
        private ExternalDefenseStruct[] _externalDefenses;

        public ExternalDefenseStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
