﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Old.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры выходных реле
    /// </summary>
    [XmlType(TypeName = "Одно_реле")]
    public class ReleOutputStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _type;
        [Layout(2)] private ushort _wait;
        [Layout(3)] private ushort _rez;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeXml
        {
            get { return Validator.Get(this._type,Strings.SignalType); }
            set { this._type = Validator.Set(value, Strings.SignalType); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string SignalXml
        {
            get { return Validator.Get(this._signal,Strings.SignalSrab); }
            set { this._signal = Validator.Set(value, Strings.SignalSrab); }
        }

        /// <summary>
        /// Время
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Время")]
        public int Wait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._wait); }
            set { this._wait = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion [Properties]
    }
}
