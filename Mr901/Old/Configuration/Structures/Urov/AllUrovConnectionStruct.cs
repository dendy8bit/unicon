﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Old.Configuration.Structures.Urov
{

    public class AllUrovConnectionStruct : StructBase, IDgvRowsContainer<UrovConnectionStruct>
    {
        public const int COUNT = 16;

        [Layout(0, Count = COUNT)]
        private UrovConnectionStruct[] ust;



        public UrovConnectionStruct[] Rows
        {
            get { return this.ust; }
            set { this.ust = value; }
        }
    }
}