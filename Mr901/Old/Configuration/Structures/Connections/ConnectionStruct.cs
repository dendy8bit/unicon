﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR901.Old.Configuration.Structures.Connections
{
    /// <summary>
    /// Присоединение
    /// </summary>
    public class ConnectionStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;					    //  конфигурация присоединения
        [Layout(1)] private ushort _inom;					    //	номинальный ток измерительного ТТ
        [Layout(2)] private ushort _off;						//	определение отключенного положения выключателя
        [Layout(3)] private ushort _on;						    //	определение включенного положения выключателя
        [Layout(4)] private ushort _inp;						//	вход, если конфигурация присоединения == "От входа"
        [Layout(5)] private ushort _tclr;                       //  задержка обнуления 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Iтт
        /// </summary>
        [XmlElement(ElementName = "Iтт")]
        [BindingProperty(0)]
        public ushort Inom
        {
            get { return this._inom; }
            set { this._inom = value; }
        }
        /// <summary>
        /// Отключение
        /// </summary>
        [XmlElement(ElementName = "Отключение")]
        [BindingProperty(1)]
        public string JoinSwitchoff
        {
            get
            {
                return Validator.Get(this._off,Strings.InputSignals);
            }
            set { this._off = Validator.Set(value, Strings.InputSignals); }
        }
        /// <summary>
        /// Включение
        /// </summary>
        [XmlElement(ElementName = "Включение")]
        [BindingProperty(2)]
        public string JoinSwitchon
        {
            get { return Validator.Get(this._on, Strings.InputSignals); }
            set { this._on = Validator.Set(value, Strings.InputSignals); }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        [XmlElement(ElementName = "Привязка")]
        [BindingProperty(3)]
        public string JoinJoin
        {
            get { return Validator.Get(this._config, Strings.Join, 0, 1, 2); }
            set { this._config = Validator.Set(value, Strings.Join, this._config, 0, 1, 2); }
        }

        /// <summary>
        /// Вход
        /// </summary>
        [XmlElement(ElementName = "Вход")]
        [BindingProperty(4)]
        public string JoinEnter
        {
            get { return Validator.Get(this._inp, Strings.InputSignals); }
            set
            {
                this._inp = Validator.Set(value, Strings.InputSignals);
            }
        }
        /// <summary>
        /// Обнуление
        /// </summary>
        [XmlElement(ElementName = "Обнуление")]
        [BindingProperty(5)]
        public bool ResetJoin
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
        /// <summary>
        /// Задержка обнуления
        /// </summary>
        [XmlElement(ElementName = "Задержка_обнуления")]
        [BindingProperty(6)]
        public int ResetDeley
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tclr); }
            set { this._tclr = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion [Properties]
    }
}