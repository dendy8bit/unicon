﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR901.Old.Configuration.Structures.Connections
{
    /// <summary>
    /// Структура. Все присоединения
    /// </summary>
    public class AllConnectionStruct : StructBase, IDgvRowsContainer<ConnectionStruct>
    {
        #region [Constants]

        public const int CONNECTIONS_COUNT = 16;

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = CONNECTIONS_COUNT)] private ConnectionStruct[] _connectionStructs;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Токи присоединений
        /// </summary>
        [XmlIgnore]
        public ushort[] GetAllIttNew
        {
            get
            {
                List<ushort> result = new List<ushort> {this._connectionStructs.Max(o => o.Inom)};
                result.AddRange(this._connectionStructs.Select(oneStruct => oneStruct.Inom));
                return result.ToArray();
            }
        }

        /// <summary>
        /// Токи присоединений
        /// </summary>
        [XmlIgnore]
        public ushort[] GetAllItt
        {
            get
            {
                ushort[] result = new ushort[this._connectionStructs.Length];
                for (int i = 0; i < this._connectionStructs.Length; i++)
                {
                    result[i] = this._connectionStructs[i].Inom;
                }
                return result;
            }
        }
        
        #endregion [Properties]

        public ConnectionStruct[] Rows
        {
            get { return this._connectionStructs; }
            set { this._connectionStructs = value; }
        }
    }
}