﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR901.Old.Configuration.Structures.Connections;
using BEMN.MR901.Old.Configuration.Structures.Defenses;
using BEMN.MR901.Old.Configuration.Structures.Ls;
using BEMN.MR901.Old.Configuration.Structures.Osc;
using BEMN.MR901.Old.Configuration.Structures.RelayInd;
using BEMN.MR901.Old.Configuration.Structures.Tt;
using BEMN.MR901.Old.Configuration.Structures.Urov;
using BEMN.MR901.Old.Configuration.Structures.Vls;

namespace BEMN.MR901.Old.Configuration.Structures
{
    [XmlRoot(ElementName = "МР90x")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР901"; } set { } }


        [Layout(0)] private UrovStruct _urov;
        [Layout(1)] private AllUrovConnectionStruct _allUrovConnection;
        [Layout(2)] private AllConnectionStruct _allConnection;
        [Layout(3)] private InputSignalStruct _inputSignal;
        [Layout(4)] private OscilloscopeSettingsStruct _oscilloscopeSettings;
        [Layout(5)] private AllControlTtStruct _allControlTt;
        [Layout(6)] private AllInputLogicSignalStruct _allInputLogicSignal;
        [Layout(7)] private AllOutputLogicSignalStruct _allOutputLogicSignal;
        [Layout(8)] private AllDefensesSetpointsStruct _allDefensesSetpoints;
        [Layout(9)] private AllReleOutputStruct _allRele;
        [Layout(10)] private AllIndicatorsStruct _allIndicators;
        [Layout(11)] private FaultStruct _fault;

        [XmlElement(ElementName = "Уров")]
        [BindingProperty(0)]
        public UrovStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }
        [XmlElement(ElementName = "Уров_Присоединения")]
        [BindingProperty(1)]
        public AllUrovConnectionStruct AllUrovConnection
        {
            get { return this._allUrovConnection; }
            set { this._allUrovConnection = value; }
        }
         [XmlElement(ElementName = "Присоединения")]
        [BindingProperty(2)]
        public AllConnectionStruct AllConnection
        {
            get { return this._allConnection; }
            set { this._allConnection = value; }
        }
        [XmlElement(ElementName = "Входные_сигналы")]
        [BindingProperty(3)]
        public InputSignalStruct InputSignal
        {
            get { return this._inputSignal; }
            set { this._inputSignal = value; }
        }
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(4)]
        public OscilloscopeSettingsStruct OscilloscopeSettings
        {
            get { return this._oscilloscopeSettings; }
            set { this._oscilloscopeSettings = value; }
        }
        [XmlElement(ElementName = "Цепи_ТТ")]
        [BindingProperty(5)]
        public AllControlTtStruct AllControlTt
        {
            get { return this._allControlTt; }
            set { this._allControlTt = value; }
        }
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(6)]
        public AllInputLogicSignalStruct AllInputLogicSignal
        {
            get { return this._allInputLogicSignal; }
            set { this._allInputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(7)]
        public AllOutputLogicSignalStruct AllOutputLogicSignal
        {
            get { return this._allOutputLogicSignal; }
            set { this._allOutputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Защиты")]
        [BindingProperty(8)]
        public AllDefensesSetpointsStruct AllDefensesSetpoints
        {
            get { return this._allDefensesSetpoints; }
            set { this._allDefensesSetpoints = value; }
        }
        [XmlElement(ElementName = "Реле")]
        [BindingProperty(9)]
        public AllReleOutputStruct AllRele
        {
            get { return this._allRele; }
            set { this._allRele = value; }
        }
        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(10)]
        public AllIndicatorsStruct AllIndicators
        {
            get { return this._allIndicators; }
            set { this._allIndicators = value; }
        }
        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(11)]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }
    }
}
