﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR901.Old.Configuration.Structures.Tt
{
    /// <summary>
    /// конфигурация цепей ТТ
    /// </summary>
    public class ControlTtStruct : StructBase
    {
        [Layout(0)]
        private ushort _config; //	выведена, неисправность, неисправность + блокировка
        [Layout(1)]
        private ushort _ust; //	уставка минимального дифференциального тока
        [Layout(2)]
        private ushort _time; //	выдержка времени
        [Layout(3)]
        private ushort _rez; //ack;					//	резерв

        /// <summary>
        /// Iдmin
        /// </summary>
        [XmlElement(ElementName = "Iдmin")]
        [BindingProperty(0)]
        public double Idmin
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Tср
        /// </summary>
        [XmlElement(ElementName = "Tср")]
        [BindingProperty(1)]
        public int Tsrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Неисправность
        /// </summary>
        [XmlElement(ElementName = "Неисправность")]
        [BindingProperty(2)]
        public string Fault
        {
            get { return Validator.Get(this._config,Strings.TtFault) ; }
            set { this._config =Validator.Set(value,Strings.TtFault) ; }
        }
    }
}
