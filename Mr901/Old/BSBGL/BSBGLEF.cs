using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Compressor;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BMTCD.HelperClasses;
using BMTCD.UDZT;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using Crownwood.Magic.Menus;
using SchemeEditorSystem;

namespace BEMN.MR901.Old.BSBGL
{
    public partial class BSBGLEF : Form, IFormView
    {
        #region Variables
        private const int COUNT_EXCHANGES_PROGRAM = 64; //��� ���������� �������� �����
        private MemoryEntity<StartStruct> _currentStartProgramStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<ProgramSignalsStruct> _currentSignalsStruct;
        private MemoryEntity<ProgramPageStruct> _currentProgramPage;
        private MessageBoxForm _formCheck;
		private Mr901Device _device;
        private NewSchematicForm _newForm;
        private DockingManager _manager;
        private Mr801Compiler _compiller;
        private bool _isOnSimulateMode;
	    private bool _isCompileScheme;
		private OutputWindow _outputWindow;
        private LibraryBox _libraryWindow;
        private ToolStrip _mainToolStrip;
        private Content _outputContent;
        private Content _libraryContent;
        private bool _isSaveLogic;
        private ushort[] _binFile;
        private ushort[] _binFile1;
        private ushort _pageIndex;
        private VisualStyle _style;
        private StatusBar _statusBar;
        private Crownwood.Magic.Controls.TabControl _filler;
        private ToolStrip MainToolStrip;
        private ToolStripButton newToolStripButton;
        private ToolStripButton openToolStripButton;
        private ToolStripButton openProjToolStripButton;
        private ToolStripButton saveToolStripButton;
        private ToolStripButton saveProjToolStripButton;
        private ToolStripButton printToolStripButton;
        private ToolStripButton undoToolStripButton;
        private ToolStripButton redoToolStripButton;
        private ToolStripButton cutToolStripButton;
        private ToolStripButton copyToolStripButton;
        private ToolStripButton pasteToolStripButton;
        private ToolStripButton CompilToolStripButton;
        private ToolStripButton StopToolStripButton;
        private ToolStripButton openFromDeviceToolStripButton;
        private ToolStripButton drawConnectionBtn;
        private ToolStripButton CompilOfflineToolStripButton;

        MemoryEntity<OneWordStruct> _stopSpl;
        MemoryEntity<OneWordStruct> _startSpl;
        MemoryEntity<SomeStruct> _stateSpl;
        #endregion
        
        #region Constructor
        public BSBGLEF()
        {
            this.InitForm();
        }
        public BSBGLEF(Mr901Device device)
        {
            this._device = device;
            this.InitForm();
            double vers = Common.VersionConverter(this._device.DeviceVersion);
            this._compiller = new Mr801Compiler(this._device.DeviceType, vers);
            this._currentSourceProgramStruct = this._device.SourceProgramStruct;
            this._currentStartProgramStruct = this._device.ProgramStartStruct;
            this._currentSignalsStruct = this._device.ProgramSignalsStruct;
            this._currentProgramPage = this._device.ProgramPage;
            //�������� ���������
            this._device.ProgramPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveOk);
            this._device.ProgramPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveFail);
            //���������
            this._device.SourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteOk);
            this._device.SourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteFail);
            this._device.SourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.SourceProgramStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructReadOk);
            this._device.SourceProgramStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructReadFail);
            this._device.SourceProgramStruct.ReadOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.SourceProgramStruct.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.SourceProgramStruct.RemoveStructQueries();
                this.ProgramStructReadFail();
            });
            this._device.SourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.SourceProgramStruct.RemoveStructQueries();
                this.ProgramStructWriteFail();
            });
            //����� ���������
            this._device.ProgramStartStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveOk);
            this._device.ProgramStartStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveFail);

            this._stopSpl = new MemoryEntity<OneWordStruct>("������� ���������� ���������", device, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("����� ���������� ���������", device, 0x0D08);

            ushort addr = vers < 2.10 ? (ushort)0x0D12 : (ushort)0x0D1F;
            this._stateSpl = new MemoryEntity<SomeStruct>("��������� ������ ������", device, addr);
            ushort[] values = new ushort[3];
            this._stateSpl.Slots = HelperFunctions.SetSlots(values, addr);
            this._stateSpl.Values = values;
            
            this._stopSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            MessageBox.Show("���������� �������� ��������������� ������ �����������", "������� ���", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._stopSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� �������", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._startSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._stateSpl.LoadStruct);
            this._startSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� �������", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._stateSpl.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                SomeStruct fails = this._stateSpl.Value;
                bool res = vers < 2.10
                    ? Common.GetBit(fails.Values[0], 15) || Common.GetBit(fails.Values[1], 0)
                      || Common.GetBit(fails.Values[1], 1) || Common.GetBit(fails.Values[1], 2)
                      || Common.GetBit(fails.Values[1], 3)
                    : !Common.GetBit(fails.Values[0], 0) || Common.GetBit(fails.Values[2], 0)
                      || Common.GetBit(fails.Values[2], 1) || Common.GetBit(fails.Values[2], 2)
                      || Common.GetBit(fails.Values[2], 3) || Common.GetBit(fails.Values[2], 4);
                if (res)
                {
                    MessageBox.Show("��������� ������: ������", "������ ������", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("��������� ������: ��������", "������ ������", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            });
            this._stateSpl.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� ��������� ������", "������ ������", MessageBoxButtons.OK, MessageBoxIcon.Error));

            //�������� ��������
            this._device.ProgramSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignalsLoadOk);
            this._device.ProgramSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSignalsLoadFail);
        }
        #endregion

        void InitForm()
        {
            this.InitializeComponent();
            this._style = VisualStyle.IDE;
            this._manager = new DockingManager(this, this._style);
            this._newForm = new NewSchematicForm();
            this._formCheck = new MessageBoxForm();
        }

        #region DeviceEventHandlers
        private void ProgramPageSaveFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSaveFail));
            }
            catch (Exception)
            {
            }
        }
        private void ProgramPageSaveOk()
        {
            try
            {
                if (this._pageIndex >= 4) return;
                if (this._isSaveLogic)
                {
                    SourceProgramStruct ps = new SourceProgramStruct();
                    ushort[] program = new ushort[1024];
                    Array.ConstrainedCopy(this._binFile, this._pageIndex * 1024, program, 0, 1024);
                    ps.InitStruct(Common.TOBYTES(program, false));
                    this._currentSourceProgramStruct.Value = ps;
                    this._currentSourceProgramStruct.SaveStruct();
                }
                else
                {
                    this._currentSourceProgramStruct.LoadStruct();
                }
            }
            catch
            { }
        }
        private void ExchangeOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(() => this._formCheck.ProgramExchangeOk()));
            }
            catch (InvalidOperationException)
            { }
        }
        
        private void ProgramStructWriteFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSaveFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ProgramSaveFail()
        {
            if (this._isSaveLogic)
            {
                this.OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
                this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
                this.OnStop();
            }
            else
            {
                this.OutMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
                this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
            }
        }

        private void ProgramStructReadFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramReadFail));
            }
            catch (InvalidOperationException)
            { }
        }
        private void ProgramReadFail()
        {
            this.OutMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
        }

        private void ProgramStructWriteOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.WriteOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void WriteOk()
        {
            this._pageIndex++;
            if (this._pageIndex < 4)
            {
                ushort[] values = new ushort[1];
                values[0] = this._pageIndex;
                ProgramPageStruct pp = new ProgramPageStruct();
                pp.InitStruct(Common.TOBYTES(values, false));
                this._currentProgramPage.Value = pp;
                this._currentProgramPage.SaveStruct();
            }
            else
            {
                this._formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
                var values = new ushort[1];
                values[0] = 0x00FF;
                StartStruct ss = new StartStruct();
                ss.InitStruct(Common.TOBYTES(values, false));
                this._currentStartProgramStruct.Value = ss;
                this._currentStartProgramStruct.SaveStruct5();
            }
        }
        private void ProgramStructReadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadOk));
            }
            catch (InvalidOperationException)
            { }
        }


        private void StartLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._startSpl.Value.Word = 0x00FF;
            this._startSpl.SaveStruct5();
        }

        private void StopLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._stopSpl.Value.Word = 0x00FF;
            this._stopSpl.SaveStruct5();
        }

        private void ReadOk()
        {
            try
            {
                ushort[] program = this._currentSourceProgramStruct.Value.ProgramStorage;
                Array.ConstrainedCopy(program, 0, this._binFile1, this._pageIndex * 1024, 1024);
                this._pageIndex++;
                if (this._pageIndex < 4)
                {
                    ushort[] values = new ushort[1];
                    values[0] = this._pageIndex;
                    ProgramPageStruct pp = new ProgramPageStruct();
                    pp.InitStruct(Common.TOBYTES(values, false));
                    this._currentProgramPage.Value = pp;
                    this._currentProgramPage.SaveStruct();
                }
                else
                {
                    BSBGLTab tab = new BSBGLTab();
                    this._compiller.ResetCompiller();
                    tab.InitializeBSBGLSheet("MR901Logic", SheetFormat.A0_P, this._device, this._manager);
                    this._compiller.SetSchemeOnBin(tab.Schematic, this._binFile1, this._device.DevicePlant);
                    this._compiller.SaveLogicInFile(Environment.CurrentDirectory +
                        string.Format("\\LogicProg_{0}_v{1}.asm", this._device.DeviceType, this._device.DeviceVersion));
                    this._filler.TabPages.Add(tab);
                    this._formCheck.ShowResultMessage("���������� ��������� ��������� �������");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "������ ������������������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._formCheck.ShowResultMessage("���������� ��������� �� ����� ���� ���������");
            }
        }

        private void SignalsLoadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSignalsLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }
        private void ProgramSignalsLoadOk()
        {
            if (this._isOnSimulateMode)
            {
                this._compiller.UpdateVol(this._currentSignalsStruct.Values);
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    src.Schematic.RedrawSchematic();
                    src.Schematic.RedrawBack();
                }
                this.OutMessage(InformationMessages.VARIABLES_UPDATED);
            }
        }
        
        private void ProgramStartSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_PROGRAM_START);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            this.OnStop();
        }
        private void ProgramSignalsLoadFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.SignalsLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }
        void SignalsLoadFail()
        {
            //������������� ���������
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
            if (this._isOnSimulateMode)
            {
                this.OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
            }
        }
        private void ProgramStartSaveOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.StartSaveOk));
            }
            catch (InvalidOperationException) { }
        }

        private void StartSaveOk()
        {
            this.OutMessage(InformationMessages.PROGRAM_START_OK);
            ProgramSignalsStruct ps = new ProgramSignalsStruct(this._compiller.GetRamRequired());
            ushort[] values = new ushort[this._compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0x4100);
            this._currentSignalsStruct.LoadStructCycle();
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_SAVE_OK_EMULATOR_RUNNING);
        }

        private void OnFileOpenFromDevice(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� ���������� ��������� �� ����������?", "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) != DialogResult.OK) return;
            this._isSaveLogic = false;
            this._pageIndex = 0;
            this._binFile1 = new ushort[4 * 1024];
            ushort[] values = new ushort[1];
            values[0] = this._pageIndex;
            ProgramPageStruct pp = new ProgramPageStruct();
            pp.InitStruct(Common.TOBYTES(values, false));
            this._currentProgramPage.Value = pp;
            this._currentProgramPage.SaveStruct();
            this._formCheck = new MessageBoxForm();
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            this._formCheck.ShowDialog("�������� ���������� ��������� �� ����������");
        }

        private void OnEditDrawLines(object sender, EventArgs e)
        {
            if (!(this._filler.SelectedTab is BSBGLTab)) return;
            BSBGLTab sTab = (BSBGLTab) this._filler.SelectedTab;
            sTab.Schematic.DrawLines();
        }
        #endregion

        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            this._filler = new Crownwood.Magic.Controls.TabControl
            {
                Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument,
                Dock = DockStyle.Fill,
                Style = this._style,
                IDEPixelBorder = true
            };
            Controls.Add(this._filler);
            this._filler.ClosePressed += this.OnFileClose;
            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // Create the object that manages the docking state
            if(this._manager == null)
                this._manager = new DockingManager(this, this._style);
            // Ensure that the RichTextBox is always the innermost control
            this._manager.InnerControl = this._filler;
            // Create and setup the StatusBar object
            this._statusBar = new StatusBar();
            this._statusBar.Dock = DockStyle.Bottom;
            this._statusBar.ShowPanels = true;
            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel();
            statusBarPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            this._statusBar.Panels.Add(statusBarPanel);
            Controls.Add(this._statusBar);
            this._mainToolStrip = this.CreateToolStrip();
            Controls.Add(this._mainToolStrip);
            this.CreateMenus();
            // Ensure that docking occurs after the menu control and status bar controls
            this._manager.OuterControl = this._statusBar;
            this.CreateOutputWindow();
            this.CreateLibraryWindow();
            Width = 800;
            Height = 600;
        }

        private void OutMessage(string str)
        {
            this._outputWindow.AddMessage(str + "\r\n");
        }

        private void DefineContentState(Content c)
        {
            c.CaptionBar = true;
            c.CloseButton = true;
        }

        #region Docking Forms Code
        private void CreateOutputWindow()
        {
            this._outputWindow = new OutputWindow();
            this._outputContent = this._manager.Contents.Add(this._outputWindow, "���������");
            this.DefineContentState(this._outputContent);
            this._manager.AddContentWithState(this._outputContent, State.DockBottom);
        }
        private void CreateLibraryWindow()
        {
            this._libraryWindow = new LibraryBox(Resources.BlockLib);
            this._libraryContent = this._manager.Contents.Add(this._libraryWindow, "����������");
            this.DefineContentState(this._libraryContent);
            this._manager.AddContentWithState(this._libraryContent, State.DockRight);
        }
        #endregion

        #region Toolbox
        private ToolStrip CreateToolStrip()
        {
            System.ComponentModel.ComponentResourceManager resources = 
                new System.ComponentModel.ComponentResourceManager(typeof(BSBGLEF));
            List<ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 9; i++)
            {
                ToolStripSeparator tls = new ToolStripSeparator();
                if (i >= 6) tls.Margin = new Padding(20, 0, 0, 0);
                toolStripSepList.Add(tls);
                tls.Name = "toolStripSeparator" + i;
                tls.Size = new Size(6, 25);
            }
            this.MainToolStrip = new ToolStrip();
            this.newToolStripButton = new ToolStripButton();
            this.openToolStripButton = new ToolStripButton();
            this.openProjToolStripButton = new ToolStripButton();
            this.openFromDeviceToolStripButton = new ToolStripButton();
            this.saveToolStripButton = new ToolStripButton();
            this.saveProjToolStripButton = new ToolStripButton();
            this.printToolStripButton = new ToolStripButton();

            this.undoToolStripButton = new ToolStripButton();
            this.redoToolStripButton = new ToolStripButton();
            this.cutToolStripButton = new ToolStripButton();
            this.copyToolStripButton = new ToolStripButton();
            this.pasteToolStripButton = new ToolStripButton();

            this.CompilToolStripButton = new ToolStripButton();
            this.StopToolStripButton = new ToolStripButton();
            this.drawConnectionBtn = new ToolStripButton();

            this.CompilOfflineToolStripButton = new ToolStripButton();

            ToolStripButton startLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.startSpl,
                ImageTransparentColor = Color.Magenta,
                Size = new Size(23, 22),
                Text = "��������� ��� � ����������",
            };
            startLogicBtn.Click += this.StartLogicProgram;

            ToolStripButton stopLogicBtn = new ToolStripButton
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.stopSpl,
                ImageTransparentColor = Color.Magenta,
                Size = new Size(23, 22),
                Text = "���������� ���������� ��� � ����������",
            };
            stopLogicBtn.Click += this.StopLogicProgram;

            this.MainToolStrip.Items.Add(this.newToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[0]);
            this.MainToolStrip.Items.Add(this.openToolStripButton);
            this.MainToolStrip.Items.Add(this.openProjToolStripButton);
            this.MainToolStrip.Items.Add(this.openFromDeviceToolStripButton);
            this.MainToolStrip.Items.Add(this.drawConnectionBtn);
            this.MainToolStrip.Items.Add(toolStripSepList[1]);
            this.MainToolStrip.Items.Add(this.saveToolStripButton);
            this.MainToolStrip.Items.Add(this.saveProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[2]);
            this.MainToolStrip.Items.Add(this.printToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[3]);
            this.MainToolStrip.Items.Add(this.undoToolStripButton);
            this.MainToolStrip.Items.Add(this.redoToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[4]);
            this.MainToolStrip.Items.Add(this.cutToolStripButton);
            this.MainToolStrip.Items.Add(this.copyToolStripButton);
            this.MainToolStrip.Items.Add(this.pasteToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[5]);
            this.MainToolStrip.Items.Add(this.CompilToolStripButton);
            this.MainToolStrip.Items.Add(this.openFromDeviceToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[6]);
            this.MainToolStrip.Items.Add(this.StopToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[7]);
            this.MainToolStrip.Items.Add(startLogicBtn);
            this.MainToolStrip.Items.Add(stopLogicBtn);
            this.MainToolStrip.Items.Add(toolStripSepList[8]);
            this.MainToolStrip.Items.Add(this.CompilOfflineToolStripButton);

            this.MainToolStrip.Location = new Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new Size(816, 25);
            this.MainToolStrip.TabIndex = 0;
            this.MainToolStrip.Text = "toolStrip1";

            this.newToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = Resources.filenew;
            this.newToolStripButton.ImageTransparentColor = Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new Size(23, 22);
            this.newToolStripButton.Text = "����� �����";
            this.newToolStripButton.Click += new EventHandler(this.OnFileNew);

            this.openToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = Resources.fileopen;
            this.openToolStripButton.ImageTransparentColor = Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new Size(23, 22);
            this.openToolStripButton.Text = "������� ��������";
            this.openToolStripButton.Click += new EventHandler(this.OnFileOpen);

            this.openProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openProjToolStripButton.Image = Resources.fileopenproject;
            this.openProjToolStripButton.ImageTransparentColor = Color.Magenta;
            this.openProjToolStripButton.Name = "openProjToolStripButton";
            this.openProjToolStripButton.Size = new Size(23, 22);
            this.openProjToolStripButton.Text = "������� ������";
            this.openProjToolStripButton.Click += new EventHandler(this.OnFileOpenProject);

            this.openFromDeviceToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openFromDeviceToolStripButton.Image = Resources.openfromdevice;
            this.openFromDeviceToolStripButton.ImageTransparentColor = Color.Magenta;
            this.openFromDeviceToolStripButton.Name = "openFromDeviceToolStripButton";
            this.openFromDeviceToolStripButton.Size = new Size(23, 22);
            this.openFromDeviceToolStripButton.Text = "��������� ���������� ��������� �� ����������";
            this.openFromDeviceToolStripButton.Click += new EventHandler(this.OnFileOpenFromDevice);

            this.drawConnectionBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.drawConnectionBtn.Image = Resources.Connect;
            this.drawConnectionBtn.ImageTransparentColor = Color.Magenta;
            this.drawConnectionBtn.Name = "drawConnectionBtn";
            this.drawConnectionBtn.Size = new Size(23, 22);
            this.drawConnectionBtn.Text = "���������� �����";
            this.drawConnectionBtn.Click += new EventHandler(this.OnEditDrawLines);

            this.saveToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = Resources.filesave;
            this.saveToolStripButton.ImageTransparentColor = Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new Size(23, 22);
            this.saveToolStripButton.Text = "��������� ��������";
            this.saveToolStripButton.Click += new EventHandler(this.OnFileSave);

            this.saveProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.saveProjToolStripButton.Image = Resources.filesaveproject;
            this.saveProjToolStripButton.ImageTransparentColor = Color.Magenta;
            this.saveProjToolStripButton.Name = "saveProjToolStripButton";
            this.saveProjToolStripButton.Size = new Size(23, 22);
            this.saveProjToolStripButton.Text = "��������� ������";
            this.saveProjToolStripButton.Click += new EventHandler(this.OnFileSaveProject);

            this.printToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = Resources.print;
            this.printToolStripButton.ImageTransparentColor = Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new Size(23, 22);
            this.printToolStripButton.Text = "������";
            this.printToolStripButton.Click += new EventHandler(this.printToolStripButton_Click);

            this.undoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = Resources.undo;
            this.undoToolStripButton.ImageTransparentColor = Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new Size(23, 22);
            this.undoToolStripButton.Text = "������";
            this.undoToolStripButton.Click += new EventHandler(this.OnUndo);

            this.redoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = Resources.redo;
            this.redoToolStripButton.ImageTransparentColor = Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new Size(23, 22);
            this.redoToolStripButton.Text = "������������ ���������";
            this.redoToolStripButton.Click += new EventHandler(this.OnRedo);

            this.cutToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = Resources.cut;
            this.cutToolStripButton.ImageTransparentColor = Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new Size(23, 22);
            this.cutToolStripButton.Text = "��������";
            this.cutToolStripButton.Click += new EventHandler(this.OnEditCut);

            this.copyToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = Resources.copy;
            this.copyToolStripButton.ImageTransparentColor = Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new Size(23, 22);
            this.copyToolStripButton.Text = "����������";
            this.copyToolStripButton.Click += new EventHandler(this.OnEditCopy);

            this.pasteToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = Resources.paste;
            this.pasteToolStripButton.ImageTransparentColor = Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new Size(23, 22);
            this.pasteToolStripButton.Text = "��������";
            this.pasteToolStripButton.Click += new EventHandler(this.OnEditPaste);
            
            this.CompilToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.CompilToolStripButton.Image = Resources.go;
            this.CompilToolStripButton.ImageTransparentColor = Color.Magenta;
            this.CompilToolStripButton.Name = "CompilToolStripButton";
            this.CompilToolStripButton.Size = new Size(23, 22);
            this.CompilToolStripButton.Text = "��������� ���������� ��������� � ����������, ��������� ��������";
            this.CompilToolStripButton.Click += new EventHandler(this.OnCompile);

            this.StopToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.StopToolStripButton.Image = Resources.stop;
            this.StopToolStripButton.ImageTransparentColor = Color.Magenta;
            this.StopToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.StopToolStripButton.Size = new Size(23, 22);
            this.StopToolStripButton.Text = "��������� ��������";
            this.StopToolStripButton.Click += new EventHandler(this.OnStop);

            this.CompilOfflineToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.CompilOfflineToolStripButton.Image = Resources.calculator.ToBitmap();
            this.CompilOfflineToolStripButton.ImageTransparentColor = Color.Magenta;
            this.CompilOfflineToolStripButton.Name = "CompilOfflineToolStripButton";
            this.CompilOfflineToolStripButton.Size = new Size(23, 22);
            this.CompilOfflineToolStripButton.Text = "�������� ���������� �����";
            this.CompilOfflineToolStripButton.Click += new EventHandler(this.OnCompileOffline);

            return this.MainToolStrip;
        }
        
        #endregion

        #region MAIN MENU
        private void CreateMenus()
        {
            MenuControl topMenu = new MenuControl();
            topMenu.Style = this._style;
            topMenu.MultiLine = false;
            MenuCommand topFile = new MenuCommand("&����");
            MenuCommand topEdit = new MenuCommand("������");
            MenuCommand topView = new MenuCommand("���");

            topMenu.MenuCommands.AddRange(new MenuCommand[] { topFile, topEdit, topView });
            //File
            MenuCommand topFileNew = new MenuCommand("�����", new EventHandler(this.OnFileNew));
            MenuCommand topFileOpen = new MenuCommand("������� ��������", new EventHandler(this.OnFileOpen));
            MenuCommand topFileOpenProject = new MenuCommand("������� ������", new EventHandler(this.OnFileOpenProject));
            MenuCommand topFileClose = new MenuCommand("������� ��������", new EventHandler(this.OnFileClose));
            MenuCommand topFileCloseProject = new MenuCommand("������� ������", new EventHandler(this.OnFileCloseProject));
            MenuCommand topFileSave = new MenuCommand("��������� ��������", new EventHandler(this.OnFileSave));
            MenuCommand topFileSaveProject = new MenuCommand("��������� ������", new EventHandler(this.OnFileSaveProject));
            topFile.MenuCommands.AddRange(new MenuCommand[] { topFileNew, topFileOpen, topFileOpenProject,
                                                              topFileClose,topFileCloseProject,
                                                              topFileSave, topFileSaveProject });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("����������", new EventHandler(this.OnEditCopy));
            MenuCommand topEditPaste = new MenuCommand("��������", new EventHandler(this.OnEditPaste));
            topEdit.MenuCommands.AddRange(new MenuCommand[] { topEditCopy, topEditPaste });
            //View
            MenuCommand topViewOutputWindow = new MenuCommand("���������", new EventHandler(this.OnViewOutputWindow));
            MenuCommand topViewToolWindow = new MenuCommand("����������", new EventHandler(this.OnViewToolWindow));
            topView.MenuCommands.AddRange(new MenuCommand[] { topViewOutputWindow,topViewToolWindow });
            topMenu.Dock = DockStyle.Top;
            Controls.Add(topMenu);
        }

        private void OnFileNew(object sender, EventArgs e)
        {
            this._newForm.ShowDialog();
            if (DialogResult.OK == this._newForm.DialogResult)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this._newForm.NameOfSchema, this._newForm.sheetFormat, this._device, this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
                this.OutMessage("������ ����� ���� �����: " + this._newForm.NameOfSchema);
                myTab.Schematic.Focus();
            }
        }

        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            this.pageSetupDialog.PageSettings = new System.Drawing.Printing.PageSettings();
            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            this.pageSetupDialog.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
            //Do not show the network in the printer dialog.
            this.pageSetupDialog.ShowNetwork = false;
            //Show the dialog storing the result.
            DialogResult result = this.pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.printDocument.DefaultPageSettings = this.pageSetupDialog.PageSettings;
                this.printPreviewDialog.Document = this.printDocument;
                // Call the ShowDialog method. This will trigger the document's
                //  PrintPage event.
                this.printPreviewDialog.ShowDialog();
                DialogResult result1 = this.printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    this.printDocument.Print();
                }
            }
        }
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                float scale = e.PageBounds.Width / (float)_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics, scale);
            }
        }

        private void OnUndo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        private void OnRedo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        private void OnEditCut(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }
        }

        private void OnEditCopy(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        private void OnEditPaste(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }
        }

        private void OnFileOpen(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this.openFileDialog.FileName, SheetFormat.A0_L, this._device, this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                myTab.Schematic.ReadXml(reader);
                myTab.UpdateTitle();
                reader.Close();
                this.OutMessage("�������� ���� �����.");
                myTab.Schematic.Focus();
            }
        }
        private void OnFileOpenProject(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {

                if (this._filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("��������� ������� ������ �� ����� ?",
                        "�������� �������", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (this.SaveProjectDoc())
                            {
                                this._filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No:
                            this._filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }
                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                while (reader.Read())
                {
                    if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                    {
                        BSBGLTab myTab = new BSBGLTab();
                        myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, this._device, this._manager);
                        myTab.Selected = true;
                        this._filler.TabPages.Add(myTab);
                        myTab.Schematic.ReadXml(reader);
                        myTab.UpdateTitle();
                    }
                }
                this.OutMessage("�������� ������.");
                reader.Close();
            }
        }

        private void OnFileClose(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("��������� �������� �� ����� ?", 
                "�������� ���������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                {
                    if (this.SaveActiveDoc())
                    {
                        BSBGLTab tab = (BSBGLTab) this._filler.SelectedTab;
                        this._filler.TabPages.Remove(tab);
                        tab.Dispose();
                    }
                    break;
                }
                case DialogResult.No:
                {
                    BSBGLTab tab = (BSBGLTab) this._filler.SelectedTab;
                    this._filler.TabPages.Remove(tab);
                    tab.Dispose();
                    break;
                }
                case DialogResult.Cancel:
                    break;
            }
        }
        private void OnFileCloseProject(object sender, EventArgs e)
        {

            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("��������� ������ �� ����� ?", "�������� �������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    this.SaveProjectDoc();                    
                    this._filler.TabPages.Clear();                    
                    break;
                case DialogResult.No:
                    this._filler.TabPages.Clear();
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        private void OnFileSave(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveActiveDoc();
        }
        private void OnFileSaveProject(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveProjectDoc();
        }
        private void OnViewToolWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._libraryContent);
        }
        private void OnViewOutputWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._outputContent);
        }
        
        private void CompileProject()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._filler.TabPages.Count == 0) return;
            this._compiller.ResetCompiller();
            try
            {
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    this._compiller.AddSource(src.TabName, src.Schematic);
                    this.OutMessage("���������� �����: " + src.TabName);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return;
            }

	        this._binFile = this._compiller.Make();

			double percent = (float)this._compiller.BinarySize * 100 / 2047;
	        string outMessage = "�p����� ���������� �����: " + myRound(percent, 1) + "%.";
	        this.OutMessage("�p����� ���������� �����: " + myRound(percent, 1) + "%.");
			
            if (percent > 95)
            {
				DialogResult dialogResult = MessageBox.Show(outMessage + "\n�������� ����������� ����� ������ (95%)!\n ������ �������� ������ � ����������?" , "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				
	            if (dialogResult == DialogResult.Yes)
                {
                    this.CompileProjectDone();
                }
                else if (dialogResult == DialogResult.No)
                {
                    this.OnStop();
                    return;
                }
                return;
            }

	        if (MessageBox.Show(outMessage + "\n������ �������� ������ � ����������?", "�������� ���������� ��������� � ����������", MessageBoxButtons.OKCancel,
		            MessageBoxIcon.Question) != DialogResult.OK) return;

			this.CompileProjectDone();
        }

        private void CompileProjectDone()
        {
            if (this._binFile.Length == 0)
            {
                this.OnStop();
                return;
            }

            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }

            this._pageIndex = 0;
            ushort[] values = new ushort[1];
            values[0] = this._pageIndex;
            ProgramPageStruct pp = new ProgramPageStruct();
            pp.InitStruct(Common.TOBYTES(values, false));
            this._currentProgramPage.Value = pp;
            this._currentProgramPage.SaveStruct();
            this._isOnSimulateMode = true;
            this._isSaveLogic = true;
            this._formCheck = new MessageBoxForm();
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            this._formCheck.ShowDialog(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
        }

        static double myRound(double x, int precision)
        {
            return ((int)(x * Math.Pow(10.0, precision)) / Math.Pow(10.0, precision));
        }

        private void CompileProjectOffline()
        {
            if (this._filler.TabPages.Count == 0) return;
            this._compiller.ResetCompiller();
            try
            {
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    this._compiller.AddSource(src.TabName, src.Schematic);
                    this.OutMessage("���������� �����: " + src.TabName);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OnStop();
                return;
            }

			this._binFile = this._compiller.Make();

			double percent = (float) this._compiller.BinarySize * 100 / 2047;
            string outMessage = "�p����� ���������� �����: " + myRound(percent, 1) + "%.";
	        this.OutMessage("�p����� ���������� �����: " + myRound(percent, 1) + "%.");
			
            if (/*this._compiller.BinarySize > 2047*/ percent > 95)
            {
                MessageBox.Show(outMessage + "\n�������� ����������� ����� ������ (95%)!", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.OnStop();
                return;
            }

			MessageBox.Show(outMessage, "��������� ���������� �����", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void OnCompileOffline(object sender, EventArgs e)
        {
            this.CompileProjectOffline();
        }
        private void OnCompile(object sender, EventArgs e)
        {
            this.CompileProject();
        }
        void OnStop(object sender, EventArgs e)
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        void OnStop()
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }

        private bool SaveActiveDoc()
        {
            BSBGLTab tabPage = (BSBGLTab) this._filler.SelectedTab;
            if (tabPage.Schematic.LinesIsLoaded && !tabPage.Schematic.IsDrawnLoadedLines)
            {
                string msg =
                    string.Format("�� ����������� ����� \"{0}\" �� ������������ �����. ����������� ����� � ����������?",
                        tabPage.TabName);
                DialogResult dr = MessageBox.Show(msg, "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    tabPage.Schematic.DrawLines();
                }
                else
                {
                    return false;
                }
            }
            ZIPCompressor compr = new ZIPCompressor();
            DialogResult SaveFileRzult;
            this.saveFileDialog = new SaveFileDialog();
            this.saveFileDialog.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            SaveFileRzult = this.saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter memwriter = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                memwriter.Formatting = Formatting.Indented;
                memwriter.WriteStartDocument();
                tabPage.Schematic.WriteXml(memwriter);
                memwriter.WriteEndDocument();
                memwriter.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this.saveFileDialog.FileName,
                    FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this.saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }

        private bool SaveProjectDoc()
        {
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                if (tabPage.Schematic.LinesIsLoaded && !tabPage.Schematic.IsDrawnLoadedLines)
                {
                    string msg =
                        string.Format("�� ����������� ����� \"{0}\" �� ������������ �����. ����������� ����� � ����������?",
                        tabPage.TabName);
                    DialogResult dr = MessageBox.Show(msg, "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dr == DialogResult.Yes)
                    {
                        tabPage.Schematic.DrawLines();
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            ZIPCompressor compr = new ZIPCompressor();
            DialogResult SaveFileRzult;
            this.saveFileDialog = new SaveFileDialog();
            this.saveFileDialog.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            SaveFileRzult = this.saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_ProjectFile");
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("name", src.TabName);
                    src.Schematic.WriteXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this.saveFileDialog.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();
                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this.saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr901Device); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.programming.ToBitmap();//(Image)BEMN.UDZT.Properties.Resources.programming1.ToBitmap(); 
            }
        }

        public string NodeName
        {
            get { return "����������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void BSBGLEF_FormClosed(object sender, FormClosedEventArgs e)
        {
            this._currentSignalsStruct.RemoveStructQueries();
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("��������� ������� ������ �� ����� ?"
                                            , "�������� �������", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    if (this.SaveProjectDoc())
                    {
                        this._filler.TabPages.Clear();
                    }
                    break;
            }
        }
    }
}
