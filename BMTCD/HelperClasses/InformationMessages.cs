﻿namespace BMTCD.HelperClasses
{
    public static class InformationMessages
    {
        public const string PROGRAM_SAVE_OK = "Логическая программа сохранена успешно.";
        public const string PROGRAM_ARCHIVE_SAVE_OK_START_PROGRAM = "Логическая программа загружена в устройство.\r\nАрхив логической программы загружен в устройство\r\nЭмулятор запущен.\r\nНажмите Ок для продолжения.";
        public const string ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE = "Ошибка загрузки архива логической программы из устройства.";
        public const string ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE = "Ошибка! Архив логической программы не был сохранен в устройство.";
        public const string PROGRAM_START_OK = "Программа стартовала.";
        public const string LOADING_ARCHIVE_IN_DEVICE = "Сохранение архива логической программы в устройство.\r\n";
        public const string PROGRAM_SAVE_OK_EMULATOR_RUNNING = "Логическая программа записана. Эмулятор запущен.";
        public const string ERROR_PROGRAM_START = "Ошибка запуска программы.";
        public const string VARIABLES_UPDATED = "Переменные обновлены.";
        public const string ERROR_VARIABLES_UPDATED = "Ошибка загрузки переменных.";
        public const string ERROR_LOADING_PROGRAM_IN_DEVICE = "Ошибка записи логической программы в устройство.";
        public const string ERROR_UPLOADING_PROGRAM_IN_DEVICE = "Ошибка загрузки логической программы из устройства.";
        public const string CREATED_NEW_SCHEMATIC_SHEET = "Создан новый лист схемы: ";
        public const string SHEET_SCHEMATIC_LOADED = "Загружен лист схемы.";
        public const string DOWNLOADING_ARCHIVE_OF_DEVICE = "Идет загрузка архива логической программы из устройства";
        public const string PROJECT_IS_LOADED = "Проект загружен";
        public const string COMPILING = "Компиляция схемы: ";
        public const string PERCENTAGE_OF_FILLING_SCHEME = "Процент заполнения схемы:";
        public const string PROJECT_LOADED_OK_OF_DEVICE = "Проект успешно загружен из устройства";
        public const string PROJWCT_STORAGE_IS_EMPTY = "В устройстве отсутствует архив логической программы.";
        public const string LOADING_PROGRAM_IN_DEVICE = "Сохранение логической программы в устройство.\n\r";
    }
}
