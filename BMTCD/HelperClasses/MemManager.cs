﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BMTCD.HelperClasses
{
    enum MEMTYPE
    {
        DATA,
        CODE
    }
    class MemManager
    {
        private List<MemObject> memObjects = new List<MemObject>();
        private const int MAX_ADDR = 65535;

        public void Clear()
        {
            memObjects.Clear();
        }

        public MemObject Allocate(MEMTYPE mt, int size, String name)
        {
            for (int addr = 0; addr < MAX_ADDR; addr++)
            {
                bool isAllocYes = this.memObjects.All(mo => !mo.IsOccupied(mt, addr, size));
                if (isAllocYes)
                {
                    MemObject newMo = new MemObject(mt, addr, size, name);
                    memObjects.Add(newMo);
                    return newMo;
                }
            }
            return null;
        }
        public int GetMaxRamAddress()
        {
            int max = 0;
            foreach (MemObject mo in memObjects)
            {
                if (mo.MemType == MEMTYPE.DATA)
                {
                    if (mo.Addr > max) { max = mo.Addr; }
                }
            }
            return max;
        }

        public MemObject FindObject(MEMTYPE mt, int addr)
        {
            return this.memObjects.FirstOrDefault(mo => mo.IsOccupied(mt, addr, 1));
        }

        public MemObject FindObject(string str)
        {
            return memObjects.FirstOrDefault(mo => mo.Name == str && mo.MemType == MEMTYPE.DATA);
        }
        
        public void DiskretUpdateVol(ushort[] buff)
        {
            foreach (MemObject mo in memObjects)
            {
                if (mo.Addr < buff.Length)
                {
                    mo.Value = buff[mo.Addr];
                }
            }
        }

        public void UpdateVol(ushort[] buff)
        {
            foreach (MemObject mo in memObjects)
            {
                if (mo.Addr < buff.Length && mo.MemType == MEMTYPE.DATA)
                {
                    mo.Value = buff[mo.Addr];
                }
            }
        }
    }

    class MemObject
    {
        public MEMTYPE MemType { get; set; }
        public int Addr { get; set; }
        public int Size { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

        public MemObject(MEMTYPE mt, int addr, int size, String name)
        {
            this.MemType = mt;
            this.Addr = addr;
            this.Size = size;
            this.Name = name;
        }

        public bool IsOccupied(MEMTYPE mt, int addr, int size)
        {
            if (mt != this.MemType) return false;
            if ((addr + size) <= this.Addr) return false;
            if ((this.Addr + this.Size) <= addr) return false;
            return true;
        }
    }
}

