﻿using BEMN.MBServer;
using BMTCD.HelperClasses;

namespace BMTCD.BMTCDMr600
{
    public class MR600CD
    {
        private ProjectInstruction project;
        private MemManager _memManager;
        /// <summary>
        /// Размер программы в байтах
        /// </summary>
        public int Binarysize { get; set; }

        public MR600CD()
        {
            project = new ProjectInstruction("ROOTSEG");
            _memManager = new MemManager();
            project.InstructionInit("MR600 BSBGL PROJECT");
        }
        public ushort[] Make()
        {
            project.Locate(ref _memManager);
            ushort[] programmBinary = new ushort[1024];
            ushort[] tmpBinary = project.GenerateBin(_memManager);
            this.Binarysize = project.GetBinSize();
            if (tmpBinary.Length > programmBinary.Length)
            {
                return new ushort[0];
            }
            tmpBinary.CopyTo(programmBinary, 0);

            byte[] tbuff = new byte[2048];
            if (programmBinary.Length > 0)
            {
                int i;
                ushort crc;
                for (i = 0; i < programmBinary.Length*2; i += 2)
                {
                    tbuff[i] = (byte) programmBinary[i/2];
                    tbuff[i + 1] = (byte) (programmBinary[i/2] >> 8);
                }
                crc = CRC16.CalcCrcFast(tbuff, tbuff.Length - 2);
                tbuff[tbuff.Length - 1] = (byte) (crc & 0xFF);
                tbuff[tbuff.Length - 2] = (byte) (crc >> 8);
                programmBinary[programmBinary.Length - 1] = (ushort) (
                    (ushort) ((ushort) (crc >> (ushort) 8) & (ushort) 0x00ff) +
                    (ushort) ((ushort) (crc << (ushort) 8) & (ushort) 0xff00));
            }
            return programmBinary;
        }

        /// <summary>
        /// Метод добавления инструкций относительно МР600
        /// </summary>
        /// <param name="name">Имя схемы</param>
        /// <param name="doc">Объект схемы</param>
        public void AddSource(string name, SchematicSystem doc)
        {
            project.AddInstruction(name, doc);
        }
        public void ResetCompiller()
        {
            project.Clear();
            _memManager.Clear();
        }
        public int GetRamRequired()
        {
            return _memManager.GetMaxRamAddress() + 1;
        }
        public int GetRomRequired()
        {
            return project.GetBinSize();
        }
        public void UpdateVol(ushort[] buff)
        {
            //Для дискретных сигналов,заменяем считанные 128 на 1
            for (int i = 0; i < buff.Length; i++)
            {
                if (buff[i] == 128)
                {
                    buff[i] = 1;
                }
            }
            _memManager.UpdateVol(buff);
            project.UpdateValue();

        }

    }
}
