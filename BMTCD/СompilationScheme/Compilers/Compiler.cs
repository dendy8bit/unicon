﻿using BEMN.MBServer;
using BMTCD.HelperClasses;
using BMTCD.СompilationScheme.Instructions;

namespace BMTCD.СompilationScheme.Compilers
{
    public class Compiler
    {
        private DiskretProjectInstruction project;
        private DiskretProjectInstructionV2 projectV2;
        private MemManager _memManager;

        public int Binarysize { get; set; }

        public Compiler(string deviceName)
        {
            if (deviceName == "MR5")
            {
                this.projectV2 = new DiskretProjectInstructionV2(deviceName);
                this.projectV2.InstructionInit(string.Format("{0} BSBGL PROJECT", deviceName));
            }
            else
            {
                this.project = new DiskretProjectInstruction("ROOTSEG");
                this.project.InstructionInit(string.Format("{0} BSBGL PROJECT", deviceName));
            }
            this._memManager = new MemManager();
            
        }
        
        public ushort[] Make()
        {
            ushort[] programmBinary = new ushort[1024];
            ushort[] tmpBinary;
            if (this.project != null)
            {
                this.project.Locate(ref this._memManager);
                tmpBinary = this.project.GenerateBin(this._memManager);
                this.Binarysize = this.project.GetBinSize();
            }
            else if (this.projectV2 != null)
            {
                this.projectV2.Locate(ref this._memManager);
                tmpBinary = this.projectV2.GenerateBin(this._memManager);
                this.Binarysize = this.projectV2.GetBinSize();
            }
            else
            {
                return new ushort[0];
            }
            if (tmpBinary.Length > programmBinary.Length)
            {
                return new ushort[0];
            }
            tmpBinary.CopyTo(programmBinary, 0);
            byte[] tbuff = new byte[1024 * 2];
            if (programmBinary.Length > 0)
            {
                int i;
                ushort crc;
                for (i = 0; i < programmBinary.Length*2; i += 2)
                {
                    tbuff[i] = (byte) programmBinary[i/2];
                    tbuff[i + 1] = (byte) (programmBinary[i/2] >> 8);
                }
                crc = CRC16.CalcCrcFast(tbuff, tbuff.Length - 2);
                tbuff[tbuff.Length - 1] = (byte) (crc & 0xFF);
                tbuff[tbuff.Length - 2] = (byte) (crc >> 8);
                programmBinary[programmBinary.Length - 1] = (ushort) (
                    (ushort) ((ushort) (crc >> 8) & 0x00ff) +
                    (ushort) ((ushort) (crc << 8) & 0xff00));
            }
            project?.SaveLogicInFile(tbuff, _memManager);
            projectV2?.SaveLogicInFile(tbuff, _memManager);
            return programmBinary;
        }


        public void AddSource(string name, SchematicSystem doc)
        {
            if (this.projectV2 != null)
            {
                this.projectV2.AddInstruction(name, doc);
            }
            else
            {
                this.project.AddInstruction(name, doc);
            }
        }

        public void ResetCompiller()
        {
            if (this.projectV2 != null)
            {
                this.projectV2.Clear();
            }
            else
            {
                this.project.Clear();
            }
            this._memManager.Clear();
        }

        public int GetRamRequired()
        {
            return this._memManager.GetMaxRamAddress() + 1;
        }

        public void DiskretUpdateVol(ushort[] buff)
        {
            //Для дискретных сигналов
            for (int i = 0; i < buff.Length; i++)
            {
                if (buff[i] == 128)
                {
                    buff[i] = 1;
                }
            }
            this._memManager.DiskretUpdateVol(buff);
            if (this.projectV2 != null)
            {
                this.projectV2.UpdateValue();
            }
            else
            {
                this.project.UpdateValue();
            }
        }
    }
}
