﻿using System;
using System.IO;
using System.Xml;
using BMTCD.HelperClasses;

namespace BMTCD.СompilationScheme.Instructions
{
    class DiskretProjectInstructionV2 : InstructionsBase
    {
        private int _segmentConuter;

        public DiskretProjectInstructionV2(string sName): base(sName)
        {
            this._segmentConuter = 0;
        }

        public override void Clear()
        {
            base.Clear();
            this._segmentConuter = 0;
        }

        public void AddInstruction(string sourceName, SchematicSystem schem)
        {
            SourceInstructionV2 sourceInstruction = new SourceInstructionV2(sourceName + this._segmentConuter, schem);
            sourceInstruction.InstructionInit(sourceName);
            subInstructions.Add(sourceInstruction);
            _segmentConuter++;
        }
    }

    class SourceInstructionV2 : SourceInstruction
    {
        public SourceInstructionV2(string sName, SchematicSystem schem): base(sName, schem)
        {
        }

        public override void InstructionInit(string nm)
        {
            name = nm;
            XmlDocument doc = new XmlDocument();
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmltextWriter = new XmlTextWriter(stringWriter);
            _schematicBSBGL.GetCompileData(xmltextWriter);
            xmltextWriter.Flush();
            xmltextWriter.Close();
            stringWriter.Flush();
            doc.LoadXml(stringWriter.ToString());
            XmlNode rootNode = doc.DocumentElement;
            XmlNodeList varNodeList = rootNode.SelectNodes("VariableList");
            XmlNodeList blockNodeList = rootNode.SelectNodes("BlockList");

            foreach (XmlNode varNode in varNodeList)
            {
                XmlNodeList varList = varNode.SelectNodes("variable");
                foreach (XmlNode var in varList)
                {
                    VarInstruction varInstruction = new VarInstruction(_segmentName, _schematicBSBGL);
                    varInstruction.InstructionInit(var);
                    subInstructions.Add(varInstruction);
                }
            }
            foreach (XmlNode blockNode in blockNodeList)
            {
                XmlNodeList blockList = blockNode.SelectNodes("block");
                foreach (XmlNode block in blockList)
                {
                    switch (block.Attributes[0].Value)
                    {
                        case "&":
                            AndInstruction newAndBlock = new AndInstruction(_segmentName, _schematicBSBGL);
                            newAndBlock.InstructionInit(block);
                            subInstructions.Add(newAndBlock);
                            break;
                        case "|":
                            OrInstruction newOrBlock = new OrInstruction(_segmentName, _schematicBSBGL);
                            newOrBlock.InstructionInit(block);
                            subInstructions.Add(newOrBlock);
                            break;
                        case "^":
                            XorInstruction newXorBlock = new XorInstruction(_segmentName, _schematicBSBGL);
                            newXorBlock.InstructionInit(block);
                            subInstructions.Add(newXorBlock);
                            break;
                        case "in":
                            InInstructionV2 newInBlock = new InInstructionV2(_segmentName, _schematicBSBGL);
                            newInBlock.InstructionInit(block);
                            subInstructions.Add(newInBlock);
                            break;
                        case "out":
                            OutInstruction newOutBlock = new OutInstruction(_segmentName, _schematicBSBGL);
                            newOutBlock.InstructionInit(block);
                            subInstructions.Add(newOutBlock);
                            break;
                        case "journal":
                            JournalInstruction newJournalBlock = new JournalInstruction(_segmentName, _schematicBSBGL);
                            newJournalBlock.InstructionInit(block);
                            subInstructions.Add(newJournalBlock);
                            break;

                        case "~":
                            NotInstruction newNotBlock = new NotInstruction(_segmentName, _schematicBSBGL);
                            newNotBlock.InstructionInit(block);
                            subInstructions.Add(newNotBlock);
                            break;
                        case "T":
                            TimerInstruction newTimerBlock = new TimerInstruction(_segmentName, _schematicBSBGL);
                            newTimerBlock.InstructionInit(block);
                            subInstructions.Add(newTimerBlock);
                            break;
                        case "RST":
                            RstInstruction newRSTBlock = new RstInstruction(_segmentName, _schematicBSBGL);
                            newRSTBlock.InstructionInit(block);
                            subInstructions.Add(newRSTBlock);
                            break;
                        case "SRT":
                            SrtInstruction newSRTBlock = new SrtInstruction(_segmentName, _schematicBSBGL);
                            newSRTBlock.InstructionInit(block);
                            subInstructions.Add(newSRTBlock);
                            break;
                        case "MS":
                            MuxInstruction newMuxBlock = new MuxInstruction(_segmentName, _schematicBSBGL);
                            newMuxBlock.InstructionInit(block);
                            subInstructions.Add(newMuxBlock);
                            break;
                    }
                }
            }
        }
    }

    class InInstructionV2 : BlockInstruction
    {
        public InInstructionV2(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            switch (Convert.ToInt32(_userData["baseNum"]))
            {
                case 0:
                    {
                        bindata[0] = 2;
                        break;
                    }
                case 1:
                    {
                        bindata[0] = 3;
                        break;
                    }
            }
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[2] = addr;
            return bindata;
        }
    }
}
