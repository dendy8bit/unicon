﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BMTCD.HelperClasses;

namespace BMTCD.СompilationScheme.Instructions
{
    class InstructionsBase
    {
        protected List<InstructionsBase> subInstructions = new List<InstructionsBase>();
        protected string name;
        protected string _segmentName;
        protected MemObject _memObject;

        public InstructionsBase(string sName)
        {
            _segmentName = sName;
        }

        public virtual void Clear()
        {
            subInstructions.Clear();
        }

        public virtual void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, 0, name);
            foreach (InstructionsBase i in subInstructions)
            {
                i.Locate(ref mm);
            }
        }

        public virtual void UpdateValue()
        {
            foreach (InstructionsBase i in subInstructions)
            {
                i.UpdateValue();
            }
        }
        // Добавление новых инструкций
        public virtual void InstructionInit(string nm)
        {
            name = nm;
        }

        public virtual int GetBinSize()
        {
            int size = 0;
            foreach (InstructionsBase i in subInstructions)
            {
                size += i.GetBinSize();
            }
            return size;
        }
        
        public virtual ushort[] GenerateBin(MemManager mm)
        {
            int index = 0;
            int binsize = this.subInstructions.Sum(i => i.GetBinSize());
            ushort[] bindata = new ushort[binsize];
            foreach (InstructionsBase i in subInstructions)
            {
                ushort[] tmp = i.GenerateBin(mm);
                tmp.CopyTo(bindata, index);
                index += tmp.Length;
            }
            return bindata;
        }

        public virtual string GenerateAsm(MemManager mm)
        {
            string asmData = ";----- " + name + " -----;\n";
            asmData += "; SEGMENT :" + _segmentName + "\n"; ;
            asmData += "; Memory Usage    : 0x" + GetBinSize().ToString("X4") + "\n"; ;

            foreach (InstructionsBase i in subInstructions)
            {
                asmData += i.GenerateAsm(mm);
            }
            asmData += ";----- END OF " + name + " -----;\n";
            return asmData;
        }

        public virtual void SaveLogicInFile(byte[] tbuff, MemManager memManager)
        {
            string filePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\logic.bin";
            try
            {
                //string str = GenerateAsm(memManager);
                //using (StreamWriter sw = new StreamWriter(filePath))
                //{
                //    sw.Write(str);
                //}
                //filePath = filePath.Replace(".asm", ".bin");
                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                {
                    fs.Write(tbuff, 0, tbuff.Length);
                }
            }
            catch (Exception e)
            {
                throw new InvalidDataException(e.Message);
            }
        }
    }

    #region  USER INSTRUCTIONS

    class DiskretProjectInstruction : InstructionsBase
    {
        private int _segmentConuter;

        public DiskretProjectInstruction(string sName) : base(sName)
        {
            this._segmentConuter = 0;
        }

        public override void Clear()
        {
            base.Clear();
            this._segmentConuter = 0;
        }

        public void AddInstruction(string sourceName, SchematicSystem schem)
        {
            SourceInstruction sourceInstruction = new SourceInstruction(sourceName + this._segmentConuter, schem);
            sourceInstruction.InstructionInit(sourceName);
            subInstructions.Add(sourceInstruction);
            _segmentConuter++;
        }
    }

    class SourceInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;

        public SourceInstruction(string sName, SchematicSystem schem): base(sName)
        {
            _schematicBSBGL = schem;
        }

        public override void InstructionInit(string nm)
        {
            base.InstructionInit(nm);
            XmlDocument doc = new XmlDocument();
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmltextWriter = new XmlTextWriter(stringWriter);
            _schematicBSBGL.GetCompileData(xmltextWriter);
            xmltextWriter.Flush();
            xmltextWriter.Close();
            stringWriter.Flush();
            doc.LoadXml(stringWriter.ToString());
            XmlNode rootNode = doc.DocumentElement;
            XmlNodeList varNodeList = rootNode.SelectNodes("VariableList");
            XmlNodeList blockNodeList = rootNode.SelectNodes("BlockList");

            foreach (XmlNode varNode in varNodeList)
            {
                XmlNodeList varList = varNode.SelectNodes("variable");
                foreach (XmlNode var in varList)
                {
                    VarInstruction varInstruction = new VarInstruction(_segmentName, _schematicBSBGL);
                    varInstruction.InstructionInit(var);
                    subInstructions.Add(varInstruction);
                }
            }
            foreach (XmlNode blockNode in blockNodeList)
            {
                XmlNodeList blockList = blockNode.SelectNodes("block");
                foreach (XmlNode block in blockList)
                {
                    switch (block.Attributes[0].Value)
                    {
                        case "&":
                            AndInstruction newAndBlock = new AndInstruction(_segmentName, _schematicBSBGL);
                            newAndBlock.InstructionInit(block);
                            subInstructions.Add(newAndBlock);
                            break;
                        case "|":
                            OrInstruction newOrBlock = new OrInstruction(_segmentName, _schematicBSBGL);
                            newOrBlock.InstructionInit(block);
                            subInstructions.Add(newOrBlock);
                            break;
                        case "^":
                            XorInstruction newXorBlock = new XorInstruction(_segmentName, _schematicBSBGL);
                            newXorBlock.InstructionInit(block);
                            subInstructions.Add(newXorBlock);
                            break;
                        case "in":
                            InInstruction newInBlock = new InInstruction(_segmentName, _schematicBSBGL);
                            newInBlock.InstructionInit(block);
                            subInstructions.Add(newInBlock);
                            break;
                        case "out":
                            OutInstruction newOutBlock = new OutInstruction(_segmentName, _schematicBSBGL);
                            newOutBlock.InstructionInit(block);
                            subInstructions.Add(newOutBlock);
                            break;
                        case "journal":
                            JournalInstruction newJournalBlock = new JournalInstruction(_segmentName, _schematicBSBGL);
                            newJournalBlock.InstructionInit(block);
                            subInstructions.Add(newJournalBlock);
                            break;

                        case "~":
                            NotInstruction newNotBlock = new NotInstruction(_segmentName, _schematicBSBGL);
                            newNotBlock.InstructionInit(block);
                            subInstructions.Add(newNotBlock);
                            break;
                        case "T":
                            TimerInstruction newTimerBlock = new TimerInstruction(_segmentName, _schematicBSBGL);
                            newTimerBlock.InstructionInit(block);
                            subInstructions.Add(newTimerBlock);
                            break;
                        case "RST":
                            RstInstruction newRSTBlock = new RstInstruction(_segmentName, _schematicBSBGL);
                            newRSTBlock.InstructionInit(block);
                            subInstructions.Add(newRSTBlock);
                            break;
                        case "SRT":
                            SrtInstruction newSRTBlock = new SrtInstruction(_segmentName, _schematicBSBGL);
                            newSRTBlock.InstructionInit(block);
                            subInstructions.Add(newSRTBlock);
                            break;
                        case "MS":
                            MuxInstruction newMuxBlock = new MuxInstruction(_segmentName, _schematicBSBGL);
                            newMuxBlock.InstructionInit(block);
                            subInstructions.Add(newMuxBlock);
                            break;
                    }
                }
            }
        }

        public override void UpdateValue()
        {
            foreach (InstructionsBase i in subInstructions)
            {
                i.UpdateValue();
            }
        }
    }

    class PinData
    {
        public PinData(string name, string dir)
        {
            this.Name = name;
            this.Option = dir;

        }

        public string Name { get; set; }
        public string Option { get; set; }
    }

    class VarInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;

        public VarInstruction(string sName, SchematicSystem schem): base(sName)
        {
            _schematicBSBGL = schem;
        }

        public void InstructionInit(XmlNode varNode)
        {
            name = varNode.Attributes[0].Value;
        }

        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.DATA, 1, _segmentName + "_" + name);
        }

        public override void UpdateValue()
        {
            _schematicBSBGL.SetVarableValue(this.name, _memObject.Value);
        }
    }

    #region BLOCKS Instructions
    class BlockInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;
        protected int _blockSize;
        protected Dictionary<int, PinData> inputs = new Dictionary<int, PinData>();
        protected Dictionary<int, PinData> outputs = new Dictionary<int, PinData>();
        protected Dictionary<string, string> _userData = new Dictionary<string, string>();

        public BlockInstruction(string sName, SchematicSystem schem): base(sName)
        {
            _blockSize = 0;
            _schematicBSBGL = schem;
        }
        
        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, _blockSize, _segmentName + "_" + name);
        }

        public override string GenerateAsm(MemManager mm)
        {
            return ("DATA 0x" + _memObject.Addr.ToString("X4") + "  Variable - " + name + ";\n");
        }

        public virtual void InstructionInit(XmlNode blockNode)
        {
            XmlNodeList pinDataList = blockNode.SelectNodes("PinData");
            foreach (XmlNode pinData in pinDataList)
            {
                XmlNodeList pinList = pinData.SelectNodes("Pin");
                foreach (XmlNode pin in pinList)
                {
                    if (pin.Attributes[2].Value == "left")
                    {
                        inputs.Add(Convert.ToInt32(pin.Attributes[1].Value),
                            new PinData(pin.Attributes[0].Value, pin.Attributes[3].Value));
                    }
                    else
                    {
                        outputs.Add(Convert.ToInt32(pin.Attributes[1].Value),
                            new PinData(pin.Attributes[0].Value, pin.Attributes[3].Value));
                    }
                }
            }

            XmlNodeList userDataList = blockNode.SelectNodes("UserData");
            foreach (XmlNode userData in userDataList)
            {
                XmlNodeList propList = userData.SelectNodes("Prop");
                foreach (XmlNode prop in propList)
                {
                    _userData.Add(prop.Attributes[0].Value, prop.Attributes[1].Value);
                }
            }
        }
        public override int GetBinSize()
        {
            return _memObject.Size;
        }
    }

    #region Connectors
    class InInstruction : BlockInstruction
    {
        public InInstruction(string sName, SchematicSystem schem) : base(sName, schem)
        {
            _blockSize = 3;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            if (Convert.ToUInt16(_userData["inOutType"]) < 96)
            {
                bindata[0] = 0x0002;
                bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            }
            else
            {
                bindata[0] = 0x0003;
                bindata[1] = (ushort)(Convert.ToUInt16(_userData["inOutType"]) - 96);
            }
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ?(ushort)0xFFFF
                :(ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[2] = addr;
            return bindata;
        }
    }

    class OutInstruction : BlockInstruction
    {
        public OutInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 3;
        }
        
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 0x0004;
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            ushort addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[2] = addr;
            return bindata;
        }
    }

    class JournalInstruction : BlockInstruction
    {
        public JournalInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 4;
        }
        
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 0x000F;
            ushort addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[1] = addr;
            bindata[2] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[3] = 0x0080;
            return bindata;
        }
    }
    #endregion

    #region Simle logic (And Or Xor)
    class SimpleLogicInstruction : BlockInstruction
    {
        protected string _asmCode = "";
        protected ushort _binCode;

        public SimpleLogicInstruction(string sName, SchematicSystem schem) : base(sName, schem)
        {
            _blockSize = 0;
        }

        public override void InstructionInit(XmlNode blockNode)
        {
            base.InstructionInit(blockNode);
            _blockSize = inputs.Count + 2;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(_binCode + inputs.Count * 0x0100);
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[1] = addr;// Out
            if (outputs[1].Option != "direct") { bindata[1] |= 0x8000; }
            for (int i = 1; i <= inputs.Count; i++)
            {
                addr = mm.FindObject(_segmentName + "_" + inputs[i].Name) == null
                    ? (ushort)0xFFFF
                    : (ushort)mm.FindObject(_segmentName + "_" + inputs[i].Name).Addr;
                bindata[1 + i] = addr;// Out
                if (inputs[i].Option != "direct") { bindata[1 + i] |= 0x8000; }
            }
            return bindata;
        }
    }

    class AndInstruction : SimpleLogicInstruction
    {
        public AndInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _asmCode = "And";
            _binCode = 0x0005;
        }
    }

    class OrInstruction : SimpleLogicInstruction
    {
        public OrInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _asmCode = "Or";
            _binCode = 0x0006;
        }
    }

    class XorInstruction : SimpleLogicInstruction
    {
        public XorInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _asmCode = "Xor";
            _binCode = 0x0007;
        }
    }

    #endregion

    //Not
    class NotInstruction : BlockInstruction
    {
        public NotInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 3;
        }
        
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)0x0008; //TYPE HERE!!!!
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[1] = addr;// Out
            bindata[1] |= 0x8000;
            addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[2] = addr;// Out
            return bindata;
        }
    }

    //Timer
    class TimerInstruction : BlockInstruction
    {
        public TimerInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 6;
        }
        
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(0x000B + Convert.ToUInt16(_userData["timerType"]) * 0x0100);
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
               ? (ushort)0xFFFF
               : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[1] = addr;// Out
            if (outputs[1].Option != "direct") { bindata[1] |= 0x8000; }
            addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[2] = addr;// IN
            if (inputs[1].Option != "direct") { bindata[2] |= 0x8000; }
            bindata[3] = (ushort)(((float)Convert.ToDouble(_userData["time"])) * 100);
            bindata[4] = 0;
            bindata[5] = 0;
            return bindata;
        }
    }

    //Trigger
    class RstInstruction : BlockInstruction
    {
        public RstInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 5;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(0x000A);
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[1] = addr;// Out
            if (outputs[1].Option != "direct") { bindata[1] |= 0x8000; }

            addr = mm.FindObject(_segmentName + "_" + inputs[2].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[2].Name).Addr;
            bindata[2] = addr;// IN1
            if (inputs[2].Option != "direct") { bindata[2] |= 0x8000; }

            addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[3] = addr;// IN2
            if (inputs[1].Option != "direct") { bindata[3] |= 0x8000; }
            bindata[4] = 0;
            return bindata;
        }
    }

    class SrtInstruction : BlockInstruction
    {
        public SrtInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 5;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(0x000A + 0x0100);
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[1] = addr;// Out
            if (outputs[1].Option != "direct") { bindata[1] |= 0x8000; }
            addr = mm.FindObject(_segmentName + "_" + inputs[2].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[2].Name).Addr;
            bindata[2] = addr;// IN1
            if (inputs[2].Option != "direct") { bindata[2] |= 0x8000; }
            addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[3] = addr;// IN2
            if (inputs[1].Option != "direct") { bindata[3] |= 0x8000; }
            bindata[4] = 0;
            return bindata;
        }
    }

    ////Mux
    class MuxInstruction : BlockInstruction
    {
        public MuxInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 5;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)0x000C;
            ushort addr = mm.FindObject(_segmentName + "_" + outputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + outputs[1].Name).Addr;
            bindata[1] = addr;// Out
            if (outputs[1].Option != "direct") { bindata[1] |= 0x8000; }

            addr = mm.FindObject(_segmentName + "_" + inputs[3].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[3].Name).Addr;
            bindata[2] = addr;// IN3
            if (inputs[3].Option != "direct") { bindata[2] |= 0x8000; }

            addr = mm.FindObject(_segmentName + "_" + inputs[1].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[1].Name).Addr;
            bindata[3] = addr;// IN2
            if (inputs[1].Option != "direct") { bindata[3] |= 0x8000; }

            addr = mm.FindObject(_segmentName + "_" + inputs[2].Name) == null
                ? (ushort)0xFFFF
                : (ushort)mm.FindObject(_segmentName + "_" + inputs[2].Name).Addr;
            bindata[4] = addr;// IN2
            if (inputs[2].Option != "direct") { bindata[4] |= 0x8000; }
            return bindata;
        }
    }

    #endregion
    #endregion
}


