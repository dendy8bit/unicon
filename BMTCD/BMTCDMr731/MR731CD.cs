﻿using BEMN.MBServer;
using BMTCD.HelperClasses;

namespace BMTCD.BMTCDMr731
{
    public class MR731CD
    {
        ProjectInstruction project;
        MemManager _memManager;
        public int binarysize;
        public MR731CD()
        {
            project = new ProjectInstruction("ROOTSEG");
            _memManager = new MemManager();
            project.InstructionInit("MR731 BSBGL PROJECT");
        }
        public ushort[] Make(double version)
        {
            this.project.AddHeaderAndExitInstruction(version);
            project.Locate(ref _memManager);
            ushort[] programmBinary = new ushort[4096];
            ushort[] tmpBinary = project.GenerateBin(_memManager);
            binarysize = project.GetBinSize();
            if (tmpBinary.Length > programmBinary.Length)
            {
                return new ushort[0];
            }
            tmpBinary.CopyTo(programmBinary, 0);
            byte[] tbuff = new byte[8192]; //РАЗМЕР ДЛЯ UDZT!!!
            if (programmBinary.Length > 0)
            {
                int i;
                ushort crc;
                for (i = 0; i < programmBinary.Length*2; i += 2)
                {
                    tbuff[i] = (byte) programmBinary[i/2];
                    tbuff[i + 1] = (byte) (programmBinary[i/2] >> 8);
                }
                crc = CRC16.CalcCrcFast(tbuff, tbuff.Length - 2);
                tbuff[tbuff.Length - 1] = (byte) (crc & 0xFF);
                tbuff[tbuff.Length - 2] = (byte) (crc >> 8);
                programmBinary[programmBinary.Length - 1] = (ushort) (
                    (ushort) ((ushort) (crc >> (ushort) 8) & (ushort) 0x00ff) +
                    (ushort) ((ushort) (crc << (ushort) 8) & (ushort) 0xff00));
            }
            return programmBinary;
        }
        public void AddSource(string name, SchematicSystem doc)
        {
            project.AddInstruction(name, doc);
        }
        public void ResetCompiller()
        {
            project.Clear();
            _memManager.Clear();
        }
        public int GetRamRequired()
        {
            return _memManager.GetMaxRamAddress() + 1;
        }
        public int GetRomRequired()
        {
            return project.GetBinSize();
        }
        public void UpdateVol(ushort[] buff)
        {
            _memManager.UpdateVol(buff);
            project.UpdateValue();
        }

    }
}
