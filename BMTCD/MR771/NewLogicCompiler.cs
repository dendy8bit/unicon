﻿using System;
using System.IO;
using System.Windows.Forms;
using BEMN.MBServer;
using BMTCD.HelperClasses;

namespace BMTCD.MR771
{
    /// <summary>
    /// Компилятор логики. Переводит нарисованную схему в массив байт-инструкций для процессора
    /// </summary>
    public class NewLogicCompiler
    {
        private ProjectInstruction _project;
        private MemManager _memManager;
        private int _binarysize;
        /// <summary>
        /// Конструктор компилятора
        /// </summary>
        /// <param name="devName">Название устройства</param>
        public NewLogicCompiler(string devName)
        {
            _project = new ProjectInstruction("ROOTSEG");
            _memManager = new MemManager();
            _project.InstructionInit(string.Format("{0}PRJ", devName));
        }

        public int Binarysize
        {
            get { return _binarysize; }
        }

        /// <summary>
        /// Компилирует инструкции схемы в бинарный файл
        /// </summary>
        /// <param name="devName">Название устройства</param>
        /// <param name="version">Версия устройства</param>
        /// <returns></returns>
        public ushort[] Make(string devName, double version)
        {
            this._project.AddHeaderAndExitInstruction(devName, version);
            this._project.Locate(ref _memManager);
            ushort[] programmBinary = new ushort[4096];
            ushort[] tmpBinary = _project.GenerateBin(_memManager);
            _binarysize = _project.GetBinSize();
            if (tmpBinary.Length > programmBinary.Length)
            {
                return new ushort[0];
            }
            tmpBinary.CopyTo(programmBinary, 0);
            byte[] tbuff = this.CalcCrc(programmBinary);
            this.SaveLogicInFile(tbuff);
            return programmBinary;
        }

        private void SaveLogicInFile(byte[] tbuff)
        {
            string filePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\logic.asm";
            try
            {
                string str = this._project.GenerateAsm(_memManager);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.Write(str);
                }
                filePath = filePath.Replace(".asm", ".bin");
                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                {
                    fs.Write(tbuff, 0, tbuff.Length);
                }
            }
            catch (Exception e)
            {
                throw new InvalidDataException(e.Message);
            }
        }
        private byte[] CalcCrc(ushort[] programmBinary)
        {
            if (programmBinary.Length <= 0) return null;
            byte[] tbuff = Common.TOBYTES(programmBinary, false);
            ushort crc = CRC16.CalcCrcFast(tbuff, tbuff.Length - 2);
            tbuff[tbuff.Length - 1] = (byte)(crc & 0xFF);
            tbuff[tbuff.Length - 2] = (byte)(crc >> 8);
            programmBinary[programmBinary.Length - 1] = (ushort)(
                (ushort)((ushort)(crc >> (ushort)8) & (ushort)0x00ff) +
                (ushort)((ushort)(crc << (ushort)8) & (ushort)0xff00));
            return tbuff;
        }

        public void AddSource(string name, SchematicSystem doc)
        {
            this._project.AddInstruction(name, doc);
        }

        public void ResetCompiller()
        {
            this._project.Clear();
            this._memManager.Clear();
        }

        public int GetRamRequired()
        {
            return this._memManager.GetMaxRamAddress() + 1;
        }

        public void UpdateVol(ushort[] buff)
        {
            this._memManager.UpdateVol(buff);
            this._project.UpdateValue();
        }
    }
}
