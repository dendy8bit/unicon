﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using BEMN.MBServer;
using BMTCD.HelperClasses;

namespace BMTCD.MR771
{
    class InstructionsBase
    {
        protected List<InstructionsBase> _subInstructions = new List<InstructionsBase>();
        protected string name;
        protected string _segmentName;
        protected MemObject _memObject;

        public InstructionsBase(string sName)
        {
            _segmentName = sName;
        }
        public virtual void Clear()
        {
            _subInstructions.Clear();
        }
        public virtual void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, 0, name);
            foreach (InstructionsBase i in _subInstructions)
            {
                i.Locate(ref mm);
            }
        }
        public virtual void UpdateValue()
        {
            foreach (InstructionsBase i in _subInstructions)
            {
                i.UpdateValue();
            }
        }
        // Добавление новых инструкций
        public virtual void InstructionInit(string nm)
        {
            name = nm;
        }

        public virtual int GetBinSize()
        {
            int size = 0;
            foreach (InstructionsBase i in _subInstructions)
            {
                size += i.GetBinSize();
            }
            return size;
        }
        
        public virtual string GenerateAsm(MemManager mm)
        {
            string asmData = ";----- " + name + " -----;\n";
            asmData += "; SEGMENT :" + _segmentName + "\n"; ;
            asmData += "; Memory Usage    : 0x" + GetBinSize().ToString("X4") + "\n"; ;
            foreach (InstructionsBase i in _subInstructions)
            {
                asmData += i.GenerateAsm(mm);
            }
            asmData += ";----- END OF " + name + " -----;\n";
            return asmData;
        }
        public virtual ushort[] GenerateBin(MemManager mm)
        {

            int index = 0;
            int binsize = 0;

            foreach (InstructionsBase i in _subInstructions)
            {
                binsize += i.GetBinSize();
            }
            ushort[] bindata = new ushort[binsize];
            foreach (InstructionsBase i in _subInstructions)
            {
                ushort[] tmp = i.GenerateBin(mm);
                tmp.CopyTo(bindata, index);
                index += tmp.Length;
            }
            return bindata;
        }
    }

    #region  USER INSTRUCTIONS

    class ProjectInstruction : InstructionsBase
    {
        private int _segment;

        public ProjectInstruction(string sName) : base(sName)
        {
            this._segment = 0;
        }

        public override void Clear()
        {
            this._segment = 0;
            base.Clear();
        }

        public void AddInstruction(string sourceName, SchematicSystem schem)
        {
            SourceInstruction sourceInstruction = new SourceInstruction(sourceName + this._segment, schem);
            sourceInstruction.InstructionInit(sourceName + this._segment);
            _subInstructions.Add(sourceInstruction);
            this._segment++;
        }

        public void AddHeaderAndExitInstruction(string devName, double version)
        {
            HeaderInstruction newHeaderBlock = new HeaderInstruction(_segmentName, devName, version);
            _subInstructions.Insert(0, newHeaderBlock);
            ExitInstruction exitBlock = new ExitInstruction(_segmentName);
            _subInstructions.Add(exitBlock);
        }
    }

    class SourceInstruction : InstructionsBase
    {
        private SchematicSystem _schematicBSBGL;

        public SourceInstruction(string sName, SchematicSystem schem) : base(sName)
        {
            _schematicBSBGL = schem;
        }

        public override void InstructionInit(string nm)
        {
            base.InstructionInit(nm);
            XmlDocument doc = new XmlDocument();
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmltextWriter = new XmlTextWriter(stringWriter);
            _schematicBSBGL.GetCompileData(xmltextWriter);
            xmltextWriter.Flush();
            xmltextWriter.Close();
            stringWriter.Flush();
            doc.LoadXml(stringWriter.ToString());
            XmlNode rootNode = doc.DocumentElement;
            XmlNodeList varNodeList = rootNode.SelectNodes("VariableList");
            XmlNodeList blockNodeList = rootNode.SelectNodes("BlockList");

            foreach (XmlNode varNode in varNodeList)
            {
                XmlNodeList varList = varNode.SelectNodes("variable");
                foreach (XmlNode var in varList)
                {
                    VarInstruction varInstruction = new VarInstruction(_segmentName, _schematicBSBGL);
                    varInstruction.InstructionInit(var);
                    _subInstructions.Add(varInstruction);
                }
            }
            foreach (XmlNode blockNode in blockNodeList)
            {
                XmlNodeList blockList = blockNode.SelectNodes("block");
                foreach (XmlNode block in blockList)
                {
                    switch (block.Attributes[0].Value)
                    {
                        case "&":
                            AndInstruction newAndBlock = new AndInstruction(_segmentName, _schematicBSBGL);
                            newAndBlock.InstructionInit(block);
                            _subInstructions.Add(newAndBlock);
                            break;
                        case "|":
                            OrInstruction newOrBlock = new OrInstruction(_segmentName, _schematicBSBGL);
                            newOrBlock.InstructionInit(block);
                            _subInstructions.Add(newOrBlock);
                            break;
                        case "^":
                            XorInstruction newXorBlock = new XorInstruction(_segmentName, _schematicBSBGL);
                            newXorBlock.InstructionInit(block);
                            _subInstructions.Add(newXorBlock);
                            break;
                        case "in":
                            InInstruction newInBlock = new InInstruction(_segmentName, _schematicBSBGL);
                            newInBlock.InstructionInit(block);
                            _subInstructions.Add(newInBlock);
                            break;
                        case "out":
                            OutInstruction newOutBlock = new OutInstruction(_segmentName, _schematicBSBGL);
                            newOutBlock.InstructionInit(block);
                            _subInstructions.Add(newOutBlock);
                            break;
                        case "in16":
                            In16Instruction newIn16Block = new In16Instruction(_segmentName, _schematicBSBGL);
                            newIn16Block.InstructionInit(block);
                            _subInstructions.Add(newIn16Block);
                            break;
                        case "out16":
                            Out16Instruction newOut16Block = new Out16Instruction(_segmentName, _schematicBSBGL);
                            newOut16Block.InstructionInit(block);
                            _subInstructions.Add(newOut16Block);
                            break;
                        case "journal":
                            JournalInstruction newJournalBlock = new JournalInstruction(_segmentName, _schematicBSBGL);
                            newJournalBlock.InstructionInit(block);
                            _subInstructions.Add(newJournalBlock);
                            break;
                        case "journalA":
                            AvaryJournalInstruction newAvaryJournalBlock = new AvaryJournalInstruction(_segmentName, _schematicBSBGL);
                            newAvaryJournalBlock.InstructionInit(block);
                            _subInstructions.Add(newAvaryJournalBlock);
                            break;
                        case "~":
                            NotInstruction newNotBlock = new NotInstruction(_segmentName, _schematicBSBGL);
                            newNotBlock.InstructionInit(block);
                            _subInstructions.Add(newNotBlock);
                            break;
                        case "T":
                            TimerInstruction newTimerBlock = new TimerInstruction(_segmentName, _schematicBSBGL);
                            newTimerBlock.InstructionInit(block);
                            _subInstructions.Add(newTimerBlock);
                            break;
                        case "RST":
                            RstInstruction newRSTBlock = new RstInstruction(_segmentName, _schematicBSBGL);
                            newRSTBlock.InstructionInit(block);
                            _subInstructions.Add(newRSTBlock);
                            break;
                        case "SRT":
                            SrtInstruction newSRTBlock = new SrtInstruction(_segmentName, _schematicBSBGL);
                            newSRTBlock.InstructionInit(block);
                            _subInstructions.Add(newSRTBlock);
                            break;
                        case "MS":
                            MuxInstruction newMuxBlock = new MuxInstruction(_segmentName, _schematicBSBGL);
                            newMuxBlock.InstructionInit(block);
                            _subInstructions.Add(newMuxBlock);
                            break;
                        case "max":
                            MaxInstruction newMaxBlock = new MaxInstruction(_segmentName, _schematicBSBGL);
                            newMaxBlock.InstructionInit(block);
                            _subInstructions.Add(newMaxBlock);
                            break;
                        case "min":
                            MinInstruction newMinBlock = new MinInstruction(_segmentName, _schematicBSBGL);
                            newMinBlock.InstructionInit(block);
                            _subInstructions.Add(newMinBlock);
                            break;
                        case "+":
                            PlusInstruction newPlusBlock = new PlusInstruction(_segmentName, _schematicBSBGL);
                            newPlusBlock.InstructionInit(block);
                            _subInstructions.Add(newPlusBlock);
                            break;
                        case "-":
                            MinusInstruction newMinusBlock = new MinusInstruction(_segmentName, _schematicBSBGL);
                            newMinusBlock.InstructionInit(block);
                            _subInstructions.Add(newMinusBlock);
                            break;
                        case "MUL":
                            MulInstruction newMulBlock = new MulInstruction(_segmentName, _schematicBSBGL);
                            newMulBlock.InstructionInit(block);
                            _subInstructions.Add(newMulBlock);
                            break;
                        case "DIV":
                            DivInstruction newDivBlock = new DivInstruction(_segmentName, _schematicBSBGL);
                            newDivBlock.InstructionInit(block);
                            _subInstructions.Add(newDivBlock);
                            break;
                        case ">":
                            MoreInstruction newMoreBlock = new MoreInstruction(_segmentName, _schematicBSBGL);
                            newMoreBlock.InstructionInit(block);
                            _subInstructions.Add(newMoreBlock);
                            break;
                        case "<":
                            LessInstruction newLessBlock = new LessInstruction(_segmentName, _schematicBSBGL);
                            newLessBlock.InstructionInit(block);
                            _subInstructions.Add(newLessBlock);
                            break;
                        case "MS16":
                            Mux16Instruction newMux16Block = new Mux16Instruction(_segmentName, _schematicBSBGL);
                            newMux16Block.InstructionInit(block);
                            newMux16Block.RevreshConst();
                            _subInstructions.Add(newMux16Block);
                            break;
                        case "D":
                            DeshInstruction newDeshBlock = new DeshInstruction(_segmentName, _schematicBSBGL);
                            newDeshBlock.InstructionInit(block);
                            newDeshBlock.RevreshConst();
                            _subInstructions.Add(newDeshBlock);
                            break;
                    }
                }
            }
        }

        public override void UpdateValue()
        {
            foreach (InstructionsBase i in _subInstructions)
            {
                i.UpdateValue();
            }
        }
    }

    class PinData
    {
        public PinData(string name, string dir)
        {
            _name = name;
            _option = dir;
        }
        public string _name;
        public string _option;
    }

    class VarInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;

        public VarInstruction(string sName, SchematicSystem schem): base(sName)
        {
            _schematicBSBGL = schem;
        }
        public void InstructionInit(XmlNode varNode)
        {
            name = varNode.Attributes[0].Value;
        }
        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.DATA, 1, _segmentName + "_" + name);
        }
        public override string GenerateAsm(MemManager mm)
        {
            return ("DATA 0x" + _memObject.Addr.ToString("X4") + "  Variable - " + name + ";\n");
        }
        public override void UpdateValue()
        {
            _schematicBSBGL.SetVarableValue(name, _memObject.Value);
        }
    }

    #region BLOCKS Instructions
    class BlockInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;
        protected int _blockSize;
        protected Dictionary<int, PinData> inputs = new Dictionary<int, PinData>();
        protected Dictionary<int, PinData> outputs = new Dictionary<int, PinData>();
        protected Dictionary<string, string> _userData = new Dictionary<string, string>();

        public BlockInstruction(string sName, SchematicSystem schem): base(sName)
        {
            _blockSize = 0;
            _schematicBSBGL = schem;
        }

        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, _blockSize, _segmentName + "_" + name);
        }

        public virtual void InstructionInit(XmlNode blockNode)
        {
            XmlNodeList pinDataList = blockNode.SelectNodes("PinData");
            foreach (XmlNode pinData in pinDataList)
            {
                XmlNodeList pinList = pinData.SelectNodes("Pin");
                foreach (XmlNode pin in pinList)
                {
                    if (pin.Attributes[2].Value == "left")
                    {
                        inputs.Add(Convert.ToInt32(pin.Attributes[1].Value),
                            new PinData(pin.Attributes[0].Value, pin.Attributes[3].Value));
                    }
                    else
                    {
                        outputs.Add(Convert.ToInt32(pin.Attributes[1].Value),
                            new PinData(pin.Attributes[0].Value, pin.Attributes[3].Value));
                    }
                }
            }

            XmlNodeList userDataList = blockNode.SelectNodes("UserData");
            foreach (XmlNode userData in userDataList)
            {
                XmlNodeList propList = userData.SelectNodes("Prop");
                foreach (XmlNode prop in propList)
                {
                    _userData.Add(prop.Attributes[0].Value, prop.Attributes[1].Value);
                }
            }
        }

        protected ushort GetAddress(MemManager mm, PinData pinData)
        {
            MemObject mo = mm.FindObject(_segmentName + "_" + pinData._name);
            return (ushort)(mo == null ? 0xFFFF : mo.Addr);
        }

        public override int GetBinSize()
        {
            return _memObject.Size;
        }
    }

    class HeaderInstruction : BlockInstruction
    {
        private const string MR771_HDR1 = "MR771 LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        private const string MR771_HDR2 = "MR771 LOGIKA PROG VER. 00001 SUBVER. 00002  ";
        private const string MR761OBR_HDR = "MR761OBR LOG PROG VER. 00001 SUBVER. 00001  ";
        private const string MR761_HDR = "MR76X LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        private const string MR801_HDR = "MR801 LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        private const string MR901_HDR = "MR901 LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        private const string MR761OBRNEW_HDR = "MR761OBR LOGIKA PROG VER. 00001 SUBVER 00002  ";

        private readonly double _version;
        private string _devName;

        public HeaderInstruction(string sName, string devName, double version): base(sName, null)
        {
            this._devName = devName;
            this._version = version;
            _blockSize = 10 + (ushort)(Marshal.SizeOf(typeof(AouthdrStruct)) / 2);
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 1234;// Магическое число
            bindata[1] = 0;// Количество секций
            bindata[2] = 0;// Время и дата создания
            bindata[3] = 0;// Время и дата создания
            bindata[4] = 0;//* Указатель в файле на таблицу имен
            bindata[5] = 0;//* Указатель в файле на таблицу имен
            bindata[6] = 0;//* Число элементов в таблице имен
            bindata[7] = 0;//* Число элементов в таблице имен
            bindata[8] = (ushort)(Marshal.SizeOf(typeof(AouthdrStruct)));//* Размер вспомогательного заголовка
            bindata[9] = 0;//* Флаги
            string aouthdr = MR771_HDR1;
            switch (this._devName)
            {
                case "MR771":
                    if (this._version >= 1.02)
                        aouthdr = MR771_HDR2;
                    break;
                case "MR761OBR":
                    if (this._version < 3.00)
                    {
                        aouthdr = MR761OBR_HDR;
                    }
                    else
                    {
                        aouthdr = MR761OBRNEW_HDR;
                    }
                    break;
                case "MR761":
                case "MR762":
                case "MR763":
                    aouthdr = MR761_HDR;
                    break;
                case "MR801":
                    aouthdr = MR801_HDR;
                    break;
                case "MR901":
                    aouthdr = MR901_HDR;
                    break;
                case "MR902":
                    aouthdr = MR901_HDR;
                    break;
            }
             
            byte[] aouthdrByte = Encoding.ASCII.GetBytes(aouthdr);
            for (int i = 0; i < (bindata[8] / 2); i++)
            {
                bindata[10 + i] = Common.TOWORD(aouthdrByte[2 * i + 1], aouthdrByte[2 * i]);
            }
            return bindata;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct AouthdrStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 44)]
        byte[] version;
    }

    #region Connectors
    class InInstruction : BlockInstruction
    {
        public InInstruction(string sName, SchematicSystem schem)
            : base(sName, schem) { _blockSize = 3; }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": LDIN(";
            for (int i = 1; i <= outputs.Count; i++)
            {
                asm += outputs[i]._name + ":0x" + GetAddress(mm, outputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return asm + ");" + "\n";
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            switch (Convert.ToInt32(_userData["baseNum"]))
            {
                case 0:
                {
                    bindata[0] = 2;
                    break;
                }
                case 1:
                {
                    bindata[0] = 3;
                    break;
                }
                case 2:
                {
                    bindata[0] = 4;
                    break;
                }
                case 3:
                {
                    bindata[0] = 33;
                    break;
                }
                case 4:
                {
                    bindata[0] = 34;
                    break;
                }
            }
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[2] = GetAddress(mm, outputs[1]);
            return bindata;
        }
    }

    class OutInstruction : BlockInstruction
    {
        public OutInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
        }
        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": SVOUT(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 5;
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[2] = GetAddress(mm, inputs[1]);
            return bindata;
        }
    }
    
    class JournalInstruction : BlockInstruction
    {
        public JournalInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 4;
        }
        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": SYSJOURNAL(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 16;
            bindata[1] = GetAddress(mm, inputs[1]);
            bindata[2] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[3] = 0x0001;
            return bindata;
        }
    }

    class AvaryJournalInstruction : BlockInstruction
    {
        public AvaryJournalInstruction(string sName, SchematicSystem schem) : base(sName, schem)
        {
            _blockSize = 4;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": ALMJOURNAL(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 17;
            bindata[1] = GetAddress(mm, inputs[1]);
            bindata[2] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[3] = 0x0001;
            return bindata;
        }

    }

    class In16Instruction : BlockInstruction
    {
        public In16Instruction(string sName, SchematicSystem schem) : base(sName, schem)
        {
            _blockSize = 3;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm =
                string.Format("CODE 0x{0}: LDIN{1}16(", _memObject.Addr.ToString("X4"), _userData["baseNum"]);
            for (int i = 1; i <= outputs.Count; i++)
            {
                asm += outputs[i]._name + ":0x" + GetAddress(mm, outputs[i]).ToString("X4");
                asm += Convert.ToUInt16(_userData["baseNum"]) == 2
                    ? "," + _userData["const"]
                    : "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            switch (Convert.ToUInt16(_userData["baseNum"]))
            {
                case 0:
                {
                    bindata[0] = 19;
                    bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
                    break;
                }
                case 1:
                {
                    bindata[0] = 20;
                    bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
                    break;
                }
                case 2:
                {
                    try
                    {
                        bindata[0] = 21;
                        bindata[1] = Convert.ToUInt16(_userData["const"]);
                    }
                    catch
                    {
                        bindata[1] = 0;
                    }
                    break;
                }
            }
            bindata[2] = GetAddress(mm, outputs[1]);
            return bindata;
        }
    }

    class Out16Instruction : BlockInstruction
    {
        public Out16Instruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 3;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": SVOUT16(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 22;
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[2] = GetAddress(mm, inputs[1]);
            return bindata;
        }
    }

    #endregion

    #region Simle logic
    class SimpleLogicInstruction : BlockInstruction
    {
        protected string AsmCode = "";
        protected ushort BinCode;

        public SimpleLogicInstruction(string sName, SchematicSystem schem) : base(sName, schem)
        {
            _blockSize = 0;
        }
        public override void InstructionInit(XmlNode blockNode)
        {
            base.InstructionInit(blockNode);
            _blockSize = inputs.Count + 2;
        }
        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + this.AsmCode +
                                    "(" + inputs.Count;

            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct") { asm += "!"; }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            for (int i = 1; i <= outputs.Count; i++)
            {
                asm += ",";
                if (outputs[i]._option != "direct") { asm += "!"; }
                asm += outputs[i]._name;
                asm += ":0x" + GetAddress(mm, outputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(this.BinCode + inputs.Count * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]);// Out
                if (inputs[i]._option != "direct") { bindata[1 + i] |= 0x8000; }
            }
            return bindata;
        }
    }
    //и
    class AndInstruction : SimpleLogicInstruction
    {
        public AndInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "AND";
            BinCode = 6;
        }
    }

    //или
    class OrInstruction : SimpleLogicInstruction
    {
        public OrInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "OR";
            BinCode = 7;
        }
    }
    //исключающее или
    class XorInstruction : SimpleLogicInstruction
    {
        public XorInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "XOR";
            BinCode = 8;
        }
    }

    //Not
    class NotInstruction : BlockInstruction
    {
        public NotInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 3;
        }
        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "MOV(";
            asm += inputs[1]._name;
            asm += ":0x" + GetAddress(mm, inputs[1]).ToString("X4");
            asm += ",";
            asm += "!";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            //TIME AND TYPE ADD HERE !!!
            asm += ");\n";
            return asm;
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 9; //TYPE HERE!!!!
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            bindata[1] |= 0x8000;
            bindata[2] = GetAddress(mm, inputs[1]);// Out
            return bindata;
        }
    }
    //функция выхода
    class ExitInstruction : SimpleLogicInstruction
    {
        public ExitInstruction(string sName): base(sName, null)
        {
            AsmCode = "EXIT";
            BinCode = 0x0001;
            _blockSize = 1;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "EXIT();\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = BinCode;
            return bindata;
        }
    }
    #endregion

    #region Extended logic
    //TODO для всех блоков определить метод GenerateAsm(...), у которых он не реализован

    class MaxInstruction : SimpleLogicInstruction
    {
        public MaxInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "MAX";
            BinCode = 27;
        }
    }

    class MinInstruction : SimpleLogicInstruction
    {
        public MinInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "MIN";
            BinCode = 28;
        }
    }

    class PlusInstruction : SimpleLogicInstruction
    {
        public PlusInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "ADD";
            BinCode = 23;
        }
    }

    class MinusInstruction : SimpleLogicInstruction
    {
        public MinusInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "SUB";
            BinCode = 24;
        }
    }

    class MulInstruction : SimpleLogicInstruction
    {
        public MulInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "MUL";
            BinCode = 25;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(BinCode + Convert.ToUInt16(_userData["shiftR"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]);// Out
                if (inputs[i]._option != "direct") { bindata[1 + i] |= 0x8000; }
            }
            return bindata;
        }
    }

    class DivInstruction : SimpleLogicInstruction
    {
        public DivInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            AsmCode = "DIV";
            BinCode = 26;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = BinCode;
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]);// Out
                if (inputs[i]._option != "direct") { bindata[1 + i] |= 0x8000; }
            }
            return bindata;
        }
    }
    
    class MoreInstruction : BlockInstruction
    {
        public MoreInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 6;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {

            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(29 + Convert.ToUInt16(_userData["shiftR"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]);// Out
                if (inputs[i]._option != "direct") { bindata[1 + i] |= 0x8000; }
            }
            bindata[_blockSize - 2] = Convert.ToUInt16(_userData["ustavkaVozvr"]);
            bindata[_blockSize - 1] = 0;
            return bindata;
        }

        //public override string GenerateAsm(MemManager mm)
        //{
        //    String asm = "CODE 0x" + _memObject._addr.ToString("X4") + ": " + "MOV(";
        //    asm += inputs[1]._name;
        //    asm += ":0x" + GetAddress(mm, inputs[1]).ToString("X4");
        //    asm += ",";
        //    asm += "!";
        //    asm += outputs[1]._name;
        //    asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
        //    //TIME AND TYPE ADD HERE !!!
        //    asm += ");\n";
        //    return asm;
        //}
    }

    class LessInstruction : BlockInstruction
    {
        public LessInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 6;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {

            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(30 + Convert.ToUInt16(_userData["shiftR"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]);// Out
                if (inputs[i]._option != "direct") { bindata[1 + i] |= 0x8000; }
            }
            bindata[_blockSize - 2] = Convert.ToUInt16(_userData["ustavkaVozvr"]);
            bindata[_blockSize - 1] = 0;
            return bindata;
        }
    }

    //Timer
    class TimerInstruction : BlockInstruction
    {
        public TimerInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 6;
        }
        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "Timer(";
            asm += inputs[1]._name;
            asm += ":0x" + GetAddress(mm, inputs[1]).ToString("X4");
            asm += ",";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            asm += ",Type:" + _userData["timerType"]; 
            asm += ",Time:" + _userData["time"]; 
            //TIME AND TYPE ADD HERE !!!
            asm += ");\n";
            return asm;
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(12 + Convert.ToUInt16(_userData["timerType"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }
            bindata[2] = GetAddress(mm, inputs[1]);// Out
            if (inputs[1]._option != "direct") { bindata[2] |= 0x8000; }
            bindata[3] = (ushort)(Convert.ToDouble(_userData["time"], CultureInfo.CurrentCulture) * 100);
            bindata[4] = 0;
            bindata[5] = 0;
            return bindata;
        }
    }
    //Trigger
    class RstInstruction : BlockInstruction
    {
        public RstInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 5;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "Rst(";
            if (outputs[1]._option != "direct") { asm += "!"; }
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct") { asm += "!"; }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 11;
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }

            bindata[2] = GetAddress(mm, inputs[2]);// Out
            if (inputs[2]._option != "direct") { bindata[2] |= 0x8000; }
            bindata[3] = GetAddress(mm, inputs[1]);// Out
            if (inputs[1]._option != "direct") { bindata[3] |= 0x8000; }
            bindata[4] = 0;
            return bindata;
        }
    }

    class SrtInstruction : BlockInstruction
    {
        public SrtInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 5;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "Srt(";
            if (outputs[1]._option != "direct") { asm += "!"; }
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct") { asm += "!"; }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 11 + 0x0100;
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }

            bindata[2] = GetAddress(mm, inputs[2]);// Out
            if (inputs[2]._option != "direct") { bindata[2] |= 0x8000; }
            bindata[3] = GetAddress(mm, inputs[1]);// Out
            if (inputs[1]._option != "direct") { bindata[3] |= 0x8000; }
            bindata[4] = 0;
            return bindata;
        }
    }

    ////Mux
    class MuxInstruction : BlockInstruction
    {
        public MuxInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = 5;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "Mux(";
            if (outputs[1]._option != "direct") { asm += "!"; }
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            foreach (KeyValuePair<int, PinData> input in inputs)
            {
                asm += ",";
                if (input.Value._option != "direct") { asm += "!"; }
                asm += input.Value._name;
                asm += ":0x" + GetAddress(mm, input.Value).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }
        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 13;
            bindata[1] = GetAddress(mm, outputs[1]);// Out
            if (outputs[1]._option != "direct") { bindata[1] |= 0x8000; }

            bindata[2] = GetAddress(mm, inputs[3]);// Out
            if (inputs[3]._option != "direct") { bindata[2] |= 0x8000; }

            bindata[3] = GetAddress(mm, inputs[1]);// Out
            if (inputs[1]._option != "direct") { bindata[3] |= 0x8000; }

            bindata[4] = GetAddress(mm, inputs[2]);// Out
            if (inputs[2]._option != "direct") { bindata[4] |= 0x8000; }
            return bindata;
        }
    }

    class Mux16Instruction : BlockInstruction
    {
        public Mux16Instruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = inputs.Count + 2;
        }

        public void RevreshConst()
        {
            _blockSize = inputs.Count + 2;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] =
                (ushort) (31 + 0x0100*Convert.ToUInt16(_userData["mask"]) + 0x1000*Convert.ToUInt16(_userData["addr"]));

            List<int> keys = new List<int>();
            foreach (var item in inputs)
            {
                keys.Add(item.Key);
            }

            foreach (var item in inputs)
            {
                if (item.Key == keys[keys.Count - 1])
                {
                    bindata[1] = GetAddress(mm, item.Value);
                }
            }

            bindata[2] = GetAddress(mm, outputs[1]);// Out

            int index = 3;
            foreach (var item in inputs)
            {
                if (item.Key != keys[keys.Count - 1])
                {
                    bindata[index] = GetAddress(mm, item.Value);
                    index++;
                }
            }
            return bindata;
        }
    }

    class DeshInstruction : BlockInstruction
    {
        public DeshInstruction(string sName, SchematicSystem schem): base(sName, schem)
        {
            _blockSize = outputs.Count + 2;
        }

        public void RevreshConst()
        {
            _blockSize = outputs.Count + 2;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(32 + 0x0100 * Convert.ToUInt16(_userData["mask"]) + 0x1000 * Convert.ToUInt16(_userData["addr"]));
            bindata[1] = GetAddress(mm, inputs[1]);// Out
            int index = 2;
            foreach (var item in outputs)
            {
                bindata[index] = GetAddress(mm, item.Value);
                if (item.Value._option != "direct")
                {
                    bindata[index] |= 0x8000; 
                }
                index++;
            }
            return bindata;
        }
    }
    #endregion Extended logic

    #endregion

    #endregion
}


