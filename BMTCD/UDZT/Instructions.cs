using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using BEMN.MBServer;
using BMTCD.HelperClasses;
using SchemeEditorSystem.descriptors;
using SchemeEditorSystem.ResourceLibs.PropForms;

namespace BMTCD.UDZT
{
    internal class InstructionsBase
    {
        protected List<InstructionsBase> SubInstructions;
        protected string instructionName;
        protected string segmentName;
        protected MemObject _memObject;
        protected string deviceName;
        protected double deviceVersion;

        public InstructionsBase() { this.SubInstructions = new List<InstructionsBase>(); }

        public InstructionsBase(string sName)
        {
            segmentName = sName;
            this.SubInstructions = new List<InstructionsBase>();
        }

        public virtual void Clear()
        {
            this.SubInstructions.Clear();
        }

        public virtual void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, 0, instructionName);
            foreach (InstructionsBase i in this.SubInstructions)
            {
                i.Locate(ref mm);
            }
        }

        public virtual void UpdateValue()
        {
            foreach (InstructionsBase i in this.SubInstructions)
            {
                i.UpdateValue();
            }
        }

        // ���������� ����� ����������
        public virtual void InstructionInit(string name)
        {
            instructionName = name;
        }

        public void InstructionInit(string device, string proj)
        {
            deviceName = device;
            instructionName = device + proj;
        }

        public virtual int GetBinSize()
        {
            int size = 0;
            foreach (InstructionsBase i in this.SubInstructions)
            {
                size += i.GetBinSize();
            }
            return size;
        }

        public virtual int GetAsmSize()
        {
            int size = 0;
            foreach (InstructionsBase i in this.SubInstructions)
            {
                size += i.GetAsmSize();
            }
            return size;
        }

        public virtual string GenerateAsm(MemManager mm)
        {
            string asmData = ";----- " + instructionName + " -----;\n";
            asmData += "; SEGMENT :" + segmentName + "\n";

            //asmData += "; Memory Location : 0x" + _memObject._addr.ToString("X4") + "\n"; ;
            asmData += "; Memory Usage    : 0x" + GetBinSize().ToString("X4") + "\n";

            foreach (InstructionsBase i in this.SubInstructions)
            {
                asmData += i.GenerateAsm(mm);
            }
            asmData += ";----- END OF " + instructionName + " -----;\n";
            return asmData;
        }

        public virtual ushort[] GenerateBin(MemManager mm)
        {
            int index = 0;
            int binsize = 0;

            foreach (InstructionsBase i in this.SubInstructions)
            {
                binsize += i.GetBinSize();
            }
            ushort[] bindata = new ushort[binsize];
            foreach (InstructionsBase i in this.SubInstructions)
            {
                ushort[] tmp = i.GenerateBin(mm);
                tmp.CopyTo(bindata, index);
                index += tmp.Length;
            }
            return bindata;
        }
    }

    #region  USER INSTRUCTIONS

    internal class ProjectInstruction : InstructionsBase
    {
        private int _segment;

        public ProjectInstruction(string sName) : base(sName) { }

        public override void Clear()
        {
            base.Clear();
            this._segment = 0;
        }

        public void AddInstruction(string sourceName, SchematicSystem schem)
        {
            SourceInstruction sourceInstruction = new SourceInstruction(sourceName + this._segment, schem);
            sourceInstruction.InstructionInit(sourceName + this._segment);
            SubInstructions.Add(sourceInstruction);
            this._segment++;
        }

        public void AddHeaderAndExitInstruction(string device, double version)
        {
            HeaderInstruction newHeaderBlock = new HeaderInstruction(segmentName, device, version);
            SubInstructions.Insert(0, newHeaderBlock);
            ExitInstruction exitBlock = new ExitInstruction(segmentName);
            SubInstructions.Add(exitBlock);
        }

        public void AddInstruction(SchematicSystem scheme, ushort[] bin, string device, double vers, string deviceConfig = "")
        {
            string sourceName = device + "LogicProg";
            SourceInstruction sourceInstruction = new SourceInstruction(sourceName + _segment, scheme);
            sourceInstruction.InstructionInitFromBin(device, vers, bin, scheme, deviceConfig);
            SubInstructions.Add(sourceInstruction);
            _segment++;
        }
    }

    internal class SourceInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;
        private List<BlockDesc> _blockList;
        public SourceInstruction(string sName, SchematicSystem schem)
            : base(sName)
        {
            _schematicBSBGL = schem;
            _blockList = new List<BlockDesc>();
        }

        public void InstructionInit(string nm)
        {
            base.InstructionInit(nm);
            XmlDocument doc = new XmlDocument();
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmltextWriter = new XmlTextWriter(stringWriter);
            _schematicBSBGL.GetCompileData(xmltextWriter);
            xmltextWriter.Flush();
            xmltextWriter.Close();
            stringWriter.Flush();
            doc.LoadXml(stringWriter.ToString());
            XmlNode rootNode = doc.DocumentElement;
            XmlNodeList varNodeList = rootNode.SelectNodes("VariableList");
            XmlNodeList blockNodeList = rootNode.SelectNodes("BlockList");
            
            foreach (XmlNode varNode in varNodeList)
            {
                XmlNodeList varList = varNode.SelectNodes("variable");
                foreach (XmlNode var in varList)
                {
                    VarInstruction varInstruction = new VarInstruction(segmentName, _schematicBSBGL);
                    varInstruction.InstructionInit(var);
                    SubInstructions.Add(varInstruction);
                }
            }
            foreach (XmlNode blockNode in blockNodeList)
            {
                XmlNodeList blockList = blockNode.SelectNodes("block");
                foreach (XmlNode block in blockList)
                {
                    switch (block.Attributes[0].Value)
                    {
                        case "&":
                            AndInstruction newAndBlock = new AndInstruction(segmentName, _schematicBSBGL);
                            newAndBlock.InstructionInit(block);
                            SubInstructions.Add(newAndBlock);
                            break;
                        case "|":
                            OrInstruction newOrBlock = new OrInstruction(segmentName, _schematicBSBGL);
                            newOrBlock.InstructionInit(block);
                            SubInstructions.Add(newOrBlock);
                            break;
                        case "^":
                            XorInstruction newXorBlock = new XorInstruction(segmentName, _schematicBSBGL);
                            newXorBlock.InstructionInit(block);
                            SubInstructions.Add(newXorBlock);
                            break;
                        case "in":
                            InInstruction newInBlock = new InInstruction(segmentName, _schematicBSBGL);
                            newInBlock.InstructionInit(block);
                            SubInstructions.Add(newInBlock);
                            break;
                        case "out":
                            OutInstruction newOutBlock = new OutInstruction(segmentName, _schematicBSBGL);
                            newOutBlock.InstructionInit(block);
                            SubInstructions.Add(newOutBlock);
                            break;
                        case "in16":
                            In16Instruction newIn16Block = new In16Instruction(segmentName, _schematicBSBGL);
                            newIn16Block.InstructionInit(block);
                            SubInstructions.Add(newIn16Block);
                            break;
                        case "out16":
                            Out16Instruction newOut16Block = new Out16Instruction(segmentName, _schematicBSBGL);
                            newOut16Block.InstructionInit(block);
                            SubInstructions.Add(newOut16Block);
                            break;
                        case "journal":
                            SysJournalInstruction newJournalBlock = new SysJournalInstruction(segmentName,
                                _schematicBSBGL);
                            newJournalBlock.InstructionInit(block);
                            SubInstructions.Add(newJournalBlock);
                            break;
                        case "journalA":
                            AlmJournalInstruction newAvaryJournalBlock = new AlmJournalInstruction(segmentName,
                                _schematicBSBGL);
                            newAvaryJournalBlock.InstructionInit(block);
                            SubInstructions.Add(newAvaryJournalBlock);
                            break;
                        case "~":
                            NotInstruction newNotBlock = new NotInstruction(segmentName, _schematicBSBGL);
                            newNotBlock.InstructionInit(block);
                            SubInstructions.Add(newNotBlock);
                            break;
                        case "T":
                            TimerInstruction newTimerBlock = new TimerInstruction(segmentName, _schematicBSBGL);
                            newTimerBlock.InstructionInit(block);
                            SubInstructions.Add(newTimerBlock);
                            break;
                        case "RST":
                            RstInstruction newRSTBlock = new RstInstruction(segmentName, _schematicBSBGL);
                            newRSTBlock.InstructionInit(block);
                            SubInstructions.Add(newRSTBlock);
                            break;
                        case "SRT":
                            SrtInstruction newSRTBlock = new SrtInstruction(segmentName, _schematicBSBGL);
                            newSRTBlock.InstructionInit(block);
                            SubInstructions.Add(newSRTBlock);
                            break;
                        case "MS":
                            MuxInstruction newMuxBlock = new MuxInstruction(segmentName, _schematicBSBGL);
                            newMuxBlock.InstructionInit(block);
                            SubInstructions.Add(newMuxBlock);
                            break;
                        case "max":
                            MaxInstruction newMaxBlock = new MaxInstruction(segmentName, _schematicBSBGL);
                            newMaxBlock.InstructionInit(block);
                            SubInstructions.Add(newMaxBlock);
                            break;
                        case "min":
                            MinInstruction newMinBlock = new MinInstruction(segmentName, _schematicBSBGL);
                            newMinBlock.InstructionInit(block);
                            SubInstructions.Add(newMinBlock);
                            break;
                        case "+":
                            PlusInstruction newPlusBlock = new PlusInstruction(segmentName, _schematicBSBGL);
                            newPlusBlock.InstructionInit(block);
                            SubInstructions.Add(newPlusBlock);
                            break;
                        case "-":
                            MinusInstruction newMinusBlock = new MinusInstruction(segmentName, _schematicBSBGL);
                            newMinusBlock.InstructionInit(block);
                            SubInstructions.Add(newMinusBlock);
                            break;
                        case "MUL":
                            MulInstruction newMulBlock = new MulInstruction(segmentName, _schematicBSBGL);
                            newMulBlock.InstructionInit(block);
                            SubInstructions.Add(newMulBlock);
                            break;
                        case "DIV":
                            DivInstruction newDivBlock = new DivInstruction(segmentName, _schematicBSBGL);
                            newDivBlock.InstructionInit(block);
                            SubInstructions.Add(newDivBlock);
                            break;
                        case ">":
                            MoreInstruction newMoreBlock = new MoreInstruction(segmentName, _schematicBSBGL);
                            newMoreBlock.InstructionInit(block);
                            SubInstructions.Add(newMoreBlock);
                            break;
                        case "<":
                            LessInstruction newLessBlock = new LessInstruction(segmentName, _schematicBSBGL);
                            newLessBlock.InstructionInit(block);
                            SubInstructions.Add(newLessBlock);
                            break;
                        case "MS16":
                            Mux16Instruction newMux16Block = new Mux16Instruction(segmentName, _schematicBSBGL);
                            newMux16Block.InstructionInit(block);
                            newMux16Block.RevreshConst();
                            SubInstructions.Add(newMux16Block);
                            break;
                        case "D":
                            Decod16Instruction newDeshBlock = new Decod16Instruction(segmentName, _schematicBSBGL);
                            newDeshBlock.InstructionInit(block);
                            newDeshBlock.RevreshConst();
                            SubInstructions.Add(newDeshBlock);
                            break;
                    }
                }
            }
        }

        public void InstructionInitFromBin(string device, double vers, ushort[] bin, SchematicSystem schem, string deviceConfig)
        {
            BlockInstruction.SetDefaultSize();
            instructionName = device + "LogicProg";
            int index = 0;
            HeaderInstruction header = new HeaderInstruction(segmentName, device, vers);
            index += header.CheckHeader(bin);
            SubInstructions.Add(header);
            List<InstructionsBase> buffInstructions = new List<InstructionsBase>();
            while (true)
            {
                if (bin[index] == 0x0001)
                {
                    break;
                }
                var instr = GetSubinstruction(bin, index, schem);
                index += instr.GetBlockSize();
                buffInstructions.Add(instr);
            }
            SortedDictionary<ushort, string> sortedDicctionary =
                new SortedDictionary<ushort, string>(BlockInstruction.VarNames);
            foreach (var varName in sortedDicctionary)
            {
                VarInstruction var = new VarInstruction(segmentName, schem);
                var.InstructionInit(varName.Value);
                SubInstructions.Add(var);
            }
            SubInstructions.AddRange(buffInstructions);
            ExitInstruction exit = new ExitInstruction(segmentName);
            SubInstructions.Add(exit);

            SetBlockList(device, vers, deviceConfig);
            AddBlocksOnScheme();
        }

        private BlockInstruction GetSubinstruction(ushort[] bin, int index, SchematicSystem schem)
        {
            ushort blockInfo = bin[index];
            byte blockNum = Common.LOBYTE(blockInfo);
            switch (blockNum)
            {
                case 2:
                case 3:
                case 4:
                    InInstruction inInstr = new InInstruction(segmentName, schem);
                    InitInstructionFromData(inInstr, bin, index);
                    return inInstr;
                case 5:
                    OutInstruction outInstr = new OutInstruction(segmentName, schem);
                    InitInstructionFromData(outInstr, bin, index);
                    return outInstr;
                case 6:
                    AndInstruction andInstr = new AndInstruction(segmentName, schem);
                    andInstr.InitBlockSize(Common.HIBYTE(blockInfo));
                    InitInstructionFromData(andInstr, bin, index);
                    return andInstr;
                case 7:
                    OrInstruction orInstr = new OrInstruction(segmentName, schem);
                    orInstr.InitBlockSize(Common.HIBYTE(blockInfo));
                    InitInstructionFromData(orInstr, bin, index);
                    return orInstr;
                case 8:
                    XorInstruction xorInstr = new XorInstruction(segmentName, schem);
                    xorInstr.InitBlockSize(Common.HIBYTE(blockInfo));
                    InitInstructionFromData(xorInstr, bin, index);
                    return xorInstr;
                case 9:
                    NotInstruction notInstr = new NotInstruction(segmentName, schem);
                    InitInstructionFromData(notInstr, bin, index);
                    return notInstr;
                case 11:
                    BlockInstruction trigInstruction;
                    if (Common.HIBYTE(blockInfo) == 0x01)
                    {
                        trigInstruction = new SrtInstruction(segmentName, schem);
                    }
                    else
                    {
                        trigInstruction = new RstInstruction(segmentName, schem);
                    }
                    InitInstructionFromData(trigInstruction, bin, index);
                    return trigInstruction;
                case 12:
                    TimerInstruction timerInstr = new TimerInstruction(segmentName, schem);
                    InitInstructionFromData(timerInstr, bin, index);
                    return timerInstr;
                case 13:
                    MuxInstruction mux = new MuxInstruction(segmentName, schem);
                    InitInstructionFromData(mux, bin, index);
                    return mux;
                case 16:
                    SysJournalInstruction sysJournalInstr = new SysJournalInstruction(segmentName, schem);
                    InitInstructionFromData(sysJournalInstr, bin, index);
                    return sysJournalInstr;
                case 17:
                    AlmJournalInstruction almJournalInstr = new AlmJournalInstruction(segmentName, schem);
                    InitInstructionFromData(almJournalInstr, bin, index);
                    return almJournalInstr;
                case 19:
                case 20:
                case 21:
                    In16Instruction in16Instruction = new In16Instruction(segmentName, schem);
                    InitInstructionFromData(in16Instruction, bin, index);
                    return in16Instruction;
                case 22:
                    Out16Instruction out16Instr = new Out16Instruction(segmentName, schem);
                    InitInstructionFromData(out16Instr, bin, index);
                    return out16Instr;
                case 23:
                    PlusInstruction plusInstr = new PlusInstruction(segmentName, schem);
                    plusInstr.InitBlockSize(Common.HIBYTE(blockInfo));
                    InitInstructionFromData(plusInstr, bin, index);
                    return plusInstr;
                case 24:
                    MinusInstruction minusInstr = new MinusInstruction(segmentName, schem);
                    minusInstr.InitBlockSize(Common.HIBYTE(blockInfo));
                    InitInstructionFromData(minusInstr, bin, index);
                    return minusInstr;
                case 25:
                    MulInstruction mulInstr = new MulInstruction(segmentName, schem);
                    mulInstr.InitBlockSize(2);
                    InitInstructionFromData(mulInstr, bin, index);
                    return mulInstr;
                case 26:
                    DivInstruction divInstr = new DivInstruction(segmentName, schem);
                    divInstr.InitBlockSize(2);
                    InitInstructionFromData(divInstr, bin, index);
                    return divInstr;
                case 27:
                    MaxInstruction maxInstr = new MaxInstruction(segmentName, schem);
                    maxInstr.InitBlockSize((Common.HIBYTE(blockInfo)));
                    InitInstructionFromData(maxInstr, bin, index);
                    return maxInstr;
                case 28:
                    MinInstruction minInstr = new MinInstruction(segmentName, schem);
                    minInstr.InitBlockSize((Common.HIBYTE(blockInfo)));
                    InitInstructionFromData(minInstr, bin, index);
                    return minInstr;
                case 29:
                    MoreInstruction moreInstr = new MoreInstruction(segmentName, schem);
                    InitInstructionFromData(moreInstr, bin, index);
                    return moreInstr;
                case 30:
                    LessInstruction lessInstr = new LessInstruction(segmentName, schem);
                    InitInstructionFromData(lessInstr, bin, index);
                    return lessInstr;
                case 31:
                    Mux16Instruction mux16Instr = new Mux16Instruction(segmentName, schem);
                    mux16Instr.InitBlockSize(bin[index]);
                    InitInstructionFromData(mux16Instr, bin, index);
                    return mux16Instr;
                case 32:
                    Decod16Instruction decod16Instr = new Decod16Instruction(segmentName, schem);
                    decod16Instr.InitBlockSize(bin[index]);
                    InitInstructionFromData(decod16Instr, bin, index);
                    return decod16Instr;
            }
            return null;
        }

        private void InitInstructionFromData(BlockInstruction instruction, ushort[] bin, int index)
        {
            ushort[] data = new ushort[instruction.GetBlockSize()];
            Array.ConstrainedCopy(bin, index, data, 0, data.Length);
            instruction.InstructionInitFromBin(data);
        }

        private void SetBlockList(string device, double vers, string deviceConfig)
        {
            if (this.SubInstructions == null) return;
            foreach (var blockInstr in SubInstructions.OfType<BlockInstruction>())
            {
                _blockList.Add(blockInstr.GetBlockDescOnInstruction(device, vers, deviceConfig));
            }
        }

        private void AddBlocksOnScheme()
        {
            _schematicBSBGL.AddBlockDescList(_blockList, BlockInstruction.GetVarNamesList());
        }

        public override void UpdateValue()
        {
            foreach (InstructionsBase i in SubInstructions)
            {
                i.UpdateValue();
            }
        }
    }

    internal class PinData
    {
        public static Dictionary<bool, string> Option
        {
            get
            {
                return new Dictionary<bool, string>
                {
                    {false, "direct"},
                    {true, "inverse"}
                };
            }
        }

        public PinData(string name, string dir)
        {
            _name = name;
            _option = dir;
        }

        public string _name;
        public string _option;
    }

    internal class VarInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;

        public VarInstruction(string sName, SchematicSystem schem)
            : base(sName)
        {
            _schematicBSBGL = schem;
        }

        public override void InstructionInit(string name)
        {
            instructionName = name;
        }

        public void InstructionInit(XmlNode varNode)
        {
            instructionName = varNode.Attributes[0].Value;
        }

        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.DATA, 1, segmentName + "_" + instructionName);
        }

        public override string GenerateAsm(MemManager mm)
        {
            return ("DATA 0x" + _memObject.Addr.ToString("X4") + "  Variable - " + instructionName + ";\n");
        }

        public override void UpdateValue()
        {
            _schematicBSBGL.SetVarableValue(this.instructionName, _memObject.Value);
        }
    }

    internal class HeaderInstruction : InstructionsBase
    {
        public const string HeaderMr801 = "MR801 LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        public const string HeaderMr76X = "MR76X LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        public const string HeaderMr90X = "MR90X LOGIKA PROG VER. 00001 SUBVER. 00001  ";
        public const string HeaderMr801Dvg = "MR801DVG LOG PROG VER. 00001 SUBVER. 00001  ";

        private const ushort MagicNumber = 1234;
        private byte[] _aouthdrByte;
        private int _blockSize;

        private string AoutHeader
        {
            get
            {
                if (deviceName.ToUpper() == "MR801")
                {
                    return HeaderMr801;
                }
                if (deviceName.ToUpper() == "MR901" || deviceName.ToUpper() == "MR902")
                {
                    return HeaderMr90X;
                }
                if (deviceName.ToUpper() == "MR762" || deviceName.ToUpper() == "MR763")
                {
                    return HeaderMr76X;
                }
                if (deviceName.ToUpper() == "MR761")
                {
                    return deviceVersion >= 2.01 ? HeaderMr76X.Replace("SUBVER. 00001", "SUBVER. 00002") : HeaderMr76X;
                }
                if (deviceName.ToUpper() == "MR801DVG")
                {
                    return HeaderMr801Dvg;
                }
                return string.Empty;
            }
        }

        public HeaderInstruction(string sName, string device, double vers) : base(sName)
        {
            deviceName = device;
            deviceVersion = vers;
            _aouthdrByte = Encoding.ASCII.GetBytes(AoutHeader);
            _blockSize = 10 + (ushort)(_aouthdrByte.Length / 2);
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = MagicNumber; // ���������� �����
            bindata[1] = 0; // ���������� ������
            bindata[2] = 0; // ����� � ���� ��������
            bindata[3] = 0; // ����� � ���� ��������
            bindata[4] = 0; //* ��������� � ����� �� ������� ����
            bindata[5] = 0; //* ��������� � ����� �� ������� ����
            bindata[6] = 0; //* ����� ��������� � ������� ����
            bindata[7] = 0; //* ����� ��������� � ������� ����
            bindata[8] = (ushort)(_aouthdrByte.Length); //* ������ ���������������� ���������
            bindata[9] = 0; //* �����
            for (int i = 0; i < (bindata[8] / 2); i++)
            {
                bindata[10 + i] = Common.TOWORD(_aouthdrByte[2 * i + 1], _aouthdrByte[2 * i]);
            }
            return bindata;
        }

        /// <summary>
        /// ��������� ��������� � ���������� ��� ������ � ���������� �� ������
        /// </summary>
        /// <param name="bin">������ ���� ���������</param>
        /// <returns>������ ���������� � ������</returns>
        public int CheckHeader(ushort[] bin)
        {
            if (bin[0] != MagicNumber)
                throw new InvalidDataException("� ���������� ���������� ���������� ���������");
            ushort[] data = new ushort[bin[8] / 2];
            Array.ConstrainedCopy(bin, 10, data, 0, data.Length);
            byte[] aouthdrByte = Common.TOBYTES(data, false);
            string aouthdr = Encoding.ASCII.GetString(aouthdrByte);
            if (aouthdr != AoutHeader)
                throw new InvalidDataException("������������ ��������� ����� ���������� ���������");
            return _blockSize;
        }

        public override int GetBinSize()
        {
            return _memObject.Size;
        }

        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, _blockSize, segmentName + "_" + instructionName);
        }
    }

    internal class ExitInstruction : InstructionsBase
    {
        private const ushort BinCode = 0x0001;
        private int _blockSize;
        public ExitInstruction(string sName)
            : base(sName)
        {
            _blockSize = 1;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "EXIT();\n";

            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = BinCode;
            return bindata;
        }

        public override int GetBinSize()
        {
            return _memObject.Size;
        }

        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, _blockSize, segmentName + "_" + instructionName);
        }
    }

    #region BLOCKS Instructions

    internal class BlockInstruction : InstructionsBase
    {
        protected SchematicSystem _schematicBSBGL;
        protected Dictionary<int, PinData> inputs = new Dictionary<int, PinData>();
        protected Dictionary<int, PinData> outputs = new Dictionary<int, PinData>();
        protected Dictionary<string, string> _userData = new Dictionary<string, string>();
        protected string BlockType;
        protected string BlockGroup;
        protected string BlockDescription;
        protected string BlockPropType;
        protected string BlockName;
        protected int _blockSize;
        protected static int MaxY;
        protected static int _inputY;
        protected static int _inputX;
        protected static int _blockY;
        protected static int _blockX;
        protected static int _outputY;
        protected static int _outputX;
        private static Dictionary<ushort, string> _varNames = new Dictionary<ushort, string>();

        public static int InputY
        {
            get { return _inputY; }
        }
        public static int BlockY
        {
            get { return _blockY; }
        }

        public static int OutputY
        {
            get { return _outputY; }
        }

        /// <summary>
        /// ������� �� ������� � ���� ���������� ������ ����� �������
        /// </summary>
        public static Dictionary<ushort, string> VarNames
        {
            get { return _varNames; }
        }


        public BlockInstruction(string sName, SchematicSystem schem)
            : base(sName)
        {
            _blockSize = 0;
            _schematicBSBGL = schem;
            if (_schematicBSBGL == null) return;
            MaxY = schem.SizeY;
            _blockX = schem.SizeX/3+40;
            _outputX = schem.SizeX/4*3+30;
        }

        public static void SetDefaultSize()
        {
            _inputX = 20;
            _inputY = _outputY =10;
            _outputX = 240;
            _blockX = 150;
            _blockY = 25;
        }

        public string GetVarName(ushort address)
        {
            if (_varNames.ContainsKey(address)) return _varNames[address];
            if (address == 0xFFFF) return string.Empty;
            _varNames.Add(address, string.Format("{0}", address));
            return _varNames[address];
        }

        public override void Locate(ref MemManager mm)
        {
            _memObject = mm.Allocate(MEMTYPE.CODE, _blockSize, segmentName + "_" + instructionName);
        }

        public virtual void InstructionInit(XmlNode blockNode)
        {
            XmlNodeList pinDataList = blockNode.SelectNodes("PinData");
            foreach (XmlNode pinData in pinDataList)
            {
                XmlNodeList pinList = pinData.SelectNodes("Pin");
                foreach (XmlNode pin in pinList)
                {
                    if (pin.Attributes[2].Value == "left")
                    {
                        inputs.Add(Convert.ToInt32(pin.Attributes[1].Value),
                            new PinData(pin.Attributes[0].Value, pin.Attributes[3].Value));
                    }
                    else
                    {
                        outputs.Add(Convert.ToInt32(pin.Attributes[1].Value),
                            new PinData(pin.Attributes[0].Value, pin.Attributes[3].Value));
                    }
                }
            }

            XmlNodeList userDataList = blockNode.SelectNodes("UserData");
            foreach (XmlNode userData in userDataList)
            {
                XmlNodeList propList = userData.SelectNodes("Prop");
                foreach (XmlNode prop in propList)
                {
                    _userData.Add(prop.Attributes[0].Value, prop.Attributes[1].Value);
                }
            }
        }

        public override int GetBinSize()
        {
            return _memObject.Size;
        }

        public int GetBlockSize()
        {
            return _blockSize;
        }

        public static List<string> GetVarNamesList()
        {
            return _varNames.Select(vName => vName.Value).ToList();
        }

        public virtual void InstructionInitFromBin(ushort[] data) { }

        public virtual BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = new BlockDesc();
            desc.userData = _userData;
            foreach (var outp in outputs)
            {
                PinType pType = outp.Value._option == PinData.Option[false] ? PinType.PT_DIRECT : PinType.PT_INVERSE;
                desc.AddPin(outp.Value._name, pType, PinOrientation.PO_RIGHT, outp.Key);
            }
            if (this is RstInstruction || this is SrtInstruction)
            {
                desc.AddPin(inputs[1]._name + " S", PinType.PT_DIRECT, PinOrientation.PO_LEFT, 1);
                desc.AddPin(inputs[2]._name + " R", PinType.PT_DIRECT, PinOrientation.PO_LEFT, 2);
            }
            else
            {
                foreach (var inp in inputs)
                {
                    PinType pType = inp.Value._option == PinData.Option[false] ? PinType.PT_DIRECT : PinType.PT_INVERSE;
                    desc.AddPin(inp.Value._name, pType, PinOrientation.PO_LEFT, inp.Key);
                }
                if (this is MuxInstruction || this is Mux16Instruction)
                    desc.listPinDesc[1].PinName += " Y";
            }
            desc.group = BlockGroup;
            desc.TypeElem = BlockType;
            desc.propType = BlockPropType;
            desc.description = BlockDescription;
            desc.name = BlockName;
            return desc;
        }

        public static void IncreaseInputY()
        {
            _inputY += 15;
            if (MaxY > _inputY + 20) return;
            _inputY = 10;
            _inputX += 35;
        }
        /// <summary>
        /// ������� ����������� ������� ����
        /// </summary>
        /// <param name="outs">���������� ������� ��������</param>
        /// <param name="inp">���������� ������</param>
        public static void IncreaseBlockY(int outs, int inp)
        {
            _blockY += 20 + 5 * Math.Max(outs, inp);
            if (MaxY > _blockY + 20) return;
            _blockY = 10;
            _blockX += 50;
        }
        public static void IncreaseOutputY()
        {
            _outputY += 15;
            if (MaxY > _outputY + 20) return;
            _outputY = 10;
            _outputX += 35;
        }

        protected ushort GetAddress(MemManager mm, PinData pinData)
        {
            MemObject mo = mm.FindObject(segmentName + "_" + pinData._name);
            return (ushort)(mo == null ? 0xFFFF : mo.Addr);
        }
    }

    #region Connectors

    internal class InInstruction : BlockInstruction
    {
        public InInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
            BlockType = "in";
            BlockGroup = "�������";
            BlockDescription = "����";
            BlockPropType = "InOutElements";
            BlockName = "Input";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string func = string.Format("LDIN{0}", _userData["baseNum"]);
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + func + "(";
            for (int i = 1; i <= outputs.Count; i++)
            {
                asm += _userData["inOutType"];
                asm += "," + outputs[i]._name + ":0x" + GetAddress(mm, outputs[i]).ToString("X4");
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            int baseNum = Convert.ToInt32(_userData["baseNum"]);
            if (baseNum >= 0 && baseNum <= 2)
            {
                bindata[0] = (ushort) (baseNum + 2);
            }
            else
            {
                bindata[0] = 33;
            }
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[2] = GetAddress(mm, outputs[1]);
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            if (data[0] >= 2 && data[0] <= 4)
            {
                _userData.Add("baseNum", (data[0] - 2).ToString());
            }
            else
            {
                _userData.Add("baseNum", 3.ToString());
            }
            _userData.Add("inOutName", GetVarName(data[2]));
            _userData.Add("inOutType", data[1].ToString());
            outputs.Add(1, new PinData(GetVarName(data[2]), "direct"));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _inputX;
            desc.posY = BlockInstruction.InputY;
            BlockInstruction.IncreaseInputY();
            int baseNum = int.Parse(_userData["baseNum"]);
            int inOutType = int.Parse(_userData["inOutType"]);
            if (vers >= 2.10 && baseNum == 0)
            {
                desc.name = InOutProps.InitDictionary(device, deviceConfig)[inOutType];
            }
            else
            {
                desc.name = InOutProps.InitInList(device, baseNum, vers, deviceConfig)[inOutType];
            }
            return desc;
        }
    }

    internal class OutInstruction : BlockInstruction
    {
        public OutInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
            BlockType = "out";
            BlockGroup = "�������";
            BlockDescription = "�����";
            BlockPropType = "InOutElements";
            BlockName = "Output";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": SVOUT(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += _userData["inOutType"];
                asm += "," + inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)5;
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[2] = GetAddress(mm, inputs[1]);
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            _userData.Add("inOutName", GetVarName(data[2]));
            _userData.Add("inOutType", data[1].ToString());
            inputs.Add(1, new PinData(GetVarName(data[2]), "direct"));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers, deviceConfig);
            desc.posX = _outputX;
            desc.posY = BlockInstruction.OutputY;
            BlockInstruction.IncreaseOutputY();
            int inOutType = int.Parse(_userData["inOutType"]);
            desc.name = InOutProps.InitOutList(device, vers)[inOutType];
            return desc;
        }
    }

    internal class In16Instruction : BlockInstruction
    {
        public In16Instruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
            BlockType = "in16";
            BlockGroup = "�������";
            BlockDescription = "���� 16-���������";
            BlockPropType = "InOut16Elements";
            BlockName = "Input16";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm =
                string.Format("CODE 0x{0}: LDIN{1}16(", _memObject.Addr.ToString("X4"), _userData["baseNum"]);
            for (int i = 1; i <= outputs.Count; i++)
            {
                asm += outputs[i]._name + ":0x" + GetAddress(mm, outputs[i]).ToString("X4");
                asm += Convert.ToUInt16(_userData["baseNum"]) == 2
                    ? "," + _userData["const"]
                    : "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            switch (Convert.ToUInt16(_userData["baseNum"]))
            {
                case 0:
                    {
                        bindata[0] = 19;
                        bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
                        break;
                    }
                case 1:
                    {
                        bindata[0] = 20;
                        bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
                        break;
                    }
                case 2:
                    {
                        try
                        {
                            bindata[0] = 21;
                            bindata[1] = Convert.ToUInt16(_userData["const"]);
                        }
                        catch
                        {
                            bindata[1] = 0;
                        }
                        break;
                    }
            }
            bindata[2] = GetAddress(mm, outputs[1]);
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            if (Common.LOBYTE(data[0]) == 19)
            {
                _userData.Add("inOutType", data[1].ToString());
                _userData.Add("baseNum", 0.ToString());
                _userData.Add("const", "");
            }
            else if (Common.LOBYTE(data[0]) == 20)
            {
                _userData.Add("inOutType", data[1].ToString());
                _userData.Add("baseNum", 1.ToString());
                _userData.Add("const", "");
            }
            else if (Common.LOBYTE(data[0]) == 21)
            {
                _userData.Add("inOutType", "0");
                _userData.Add("baseNum", 2.ToString());
                _userData.Add("const", data[1].ToString());
            }
            _userData.Add("inOutName", "16 " + GetVarName(data[2]));
            outputs.Add(1, new PinData(GetVarName(data[2]), "direct"));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _inputX;
            desc.posY = BlockInstruction.InputY;
            BlockInstruction.IncreaseInputY();
            int baseNum = int.Parse(_userData["baseNum"]);
            if (baseNum != 2)
            {
                int inOutType = int.Parse(_userData["inOutType"]);
                desc.name = InOut16Props.InitInList(device, vers, baseNum, deviceConfig)[inOutType];
            }
            else
            {
                desc.name = "Const = " + _userData["const"];
            }
            return desc;
        }
    }

    internal class Out16Instruction : BlockInstruction
    {
        public Out16Instruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
            BlockType = "out16";
            BlockGroup = "�������";
            BlockDescription = "����� 16-���������";
            BlockPropType = "InOut16Elements";
            BlockName = "Output16";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": SVOUT16(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 22;
            bindata[1] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[2] = GetAddress(mm, inputs[1]);
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            inputs.Add(1, new PinData(GetVarName(data[2]), PinData.Option[false]));
            _userData.Add("inOutName", "16 " + GetVarName(data[2]));
            _userData.Add("inOutType", data[1].ToString("D"));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _outputX;
            desc.posY = BlockInstruction.OutputY;
            BlockInstruction.IncreaseOutputY();
            int inOutType = int.Parse(_userData["inOutType"]);
            desc.name = InOut16Props.InitOutList()[inOutType];
            return desc;
        }
    }

    internal class SysJournalInstruction : BlockInstruction
    {
        public SysJournalInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 4;
            BlockType = "journal";
            BlockGroup = "�������";
            BlockDescription = "������ � ��������� ������";
            BlockPropType = "InOutElements";
            BlockName = "SysJournal";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": SYSJOURNAL(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 16;
            bindata[1] = GetAddress(mm, inputs[1]);
            bindata[2] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[3] = 0x8000;
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            inputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));
            _userData.Add("inOutType", data[2].ToString());
            _userData.Add("inOutName", GetVarName(data[1]));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _outputX;
            desc.posY = BlockInstruction.OutputY;
            BlockInstruction.IncreaseOutputY();
            int inOutType = int.Parse(_userData["inOutType"]);
            desc.name = InOutProps.InitJournalList("��", 100)[inOutType];
            return desc;
        }
    }

    internal class AlmJournalInstruction : BlockInstruction
    {
        public AlmJournalInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 4;
            BlockType = "journalA";
            BlockGroup = "�������";
            BlockDescription = "������ � ������ ������";
            BlockPropType = "InOutElements";
            BlockName = "AlmJournal";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": ALMJOURNAL(";
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += inputs[i]._name + ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
                asm += "," + _userData["inOutType"];
            }
            return (asm + ");" + "\n");
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 17;
            bindata[1] = GetAddress(mm, inputs[1]);
            bindata[2] = Convert.ToUInt16(_userData["inOutType"]);
            bindata[3] = 0x8000;
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            inputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));
            _userData.Add("inOutType", data[2].ToString());
            _userData.Add("inOutName", GetVarName(data[1]));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _outputX;
            desc.posY = BlockInstruction.OutputY;
            BlockInstruction.IncreaseOutputY();
            int inOutType = int.Parse(_userData["inOutType"]);
            desc.name = InOutProps.InitJournalList("��", 100)[inOutType];
            return desc;
        }
    }

    #endregion

    #region Simle logic (And Or Xor)

    internal class SimpleLogicInstruction : BlockInstruction
    {
        protected string _asmCode = "";
        protected ushort _binCode = 0;

        public SimpleLogicInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 0;
        }

        public override void InstructionInit(XmlNode blockNode)
        {
            base.InstructionInit(blockNode);
            _blockSize = inputs.Count + 2;
        }

        public void InitBlockSize(byte pinCount)
        {
            _blockSize = pinCount + 2;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + _asmCode + "(";

            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");

            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct")
                {
                    asm += "!";
                }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(_binCode + inputs.Count * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            if (outputs[1]._option != "direct")
            {
                bindata[1] |= 0x8000;
            }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]); // In
                if (inputs[i]._option != "direct")
                {
                    bindata[1 + i] |= 0x8000;
                }
            }
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            int inpCount = Common.HIBYTE(data[0]);
            ushort addr = Common.LOBYTE(data[1]);
            bool option = (Common.HIBYTE(data[1]) >> 7) == 1;
            outputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            for (int i = 0; i < inpCount; i++)
            {
                addr = Common.LOBYTE(data[2 + i]);
                option = (Common.HIBYTE(data[2 + i]) >> 7) == 1;
                inputs.Add(1 + i, new PinData(GetVarName(addr), PinData.Option[option]));
            }
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    internal class AndInstruction : SimpleLogicInstruction
    {
        public AndInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "AND";
            _binCode = (ushort)6;
            BlockType = "&";
            BlockGroup = "������� ������";
            BlockDescription = "���������� ������� �";
            BlockPropType = "MultInp2Out";
            BlockName = "And";
        }
    }

    internal class OrInstruction : SimpleLogicInstruction
    {
        public OrInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "OR";
            _binCode = (ushort)7;
            BlockType = "|";
            BlockGroup = "������� ������";
            BlockDescription = "���������� ������� ���";
            BlockPropType = "MultInp2Out";
            BlockName = "Or";
        }
    }

    internal class XorInstruction : SimpleLogicInstruction
    {
        public XorInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "XOR";
            _binCode = (ushort)8;
            BlockType = "^";
            BlockGroup = "������� ������";
            BlockDescription = "���������� ������� ����. ���";
            BlockPropType = "MultInp2Out";
            BlockName = "Xor";
        }
    }

    internal class MaxInstruction : SimpleLogicInstruction
    {
        public MaxInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "MAX16";
            _binCode = (ushort)27;
            BlockType = "max";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� MAX";
            BlockPropType = "MultInp2Out";
            BlockName = "Max16";
        }
    }

    internal class MinInstruction : SimpleLogicInstruction
    {
        public MinInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "MIN16";
            _binCode = (ushort)28;
            BlockType = "min";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� MIN";
            BlockPropType = "MultInp2Out";
            BlockName = "Min16";
        }
    }

    internal class PlusInstruction : SimpleLogicInstruction
    {
        public PlusInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "ADD16";
            _binCode = (ushort)23;
            BlockType = "+";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� [+]";
            BlockPropType = "MultInp2Out";
            BlockName = "Add";
        }
    }

    internal class MinusInstruction : SimpleLogicInstruction
    {
        public MinusInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "SUB16";
            _binCode = (ushort)24;
            BlockType = "-";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� [-]";
            BlockPropType = "MultInp2Out";
            BlockName = "Sub";
        }

    }

    internal class MulInstruction : SimpleLogicInstruction
    {
        public MulInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "MUL16";
            _binCode = 25;
            BlockType = "MUL";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� [*]";
            BlockPropType = "MulDiv";
            BlockName = "Mul16";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + _asmCode + "(" + inputs.Count.ToString();
            asm += _userData["shiftR"] + ",";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct")
                {
                    asm += "!";
                }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(_binCode + Convert.ToUInt16(_userData["shiftR"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]); // IN
                if (inputs[i]._option != "direct")
                {
                    bindata[1 + i] |= 0x8000;
                }
            }
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            byte shiftr = Common.HIBYTE(data[0]);
            _userData.Add("shiftR", shiftr.ToString());
            outputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));
            ushort addr = Common.LOBYTE(data[2]);
            bool option = (Common.HIBYTE(data[2]) >> 7) == 1;
            inputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[3]);
            option = (Common.HIBYTE(data[3]) >> 7) == 1;
            inputs.Add(2, new PinData(GetVarName(addr), PinData.Option[option]));
        }
    }

    internal class DivInstruction : SimpleLogicInstruction
    {
        public DivInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _asmCode = "DIV16";
            _binCode = (ushort)26;
            BlockType = "DIV";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� [//]";
            BlockPropType = "Fixed";
            BlockName = "Div16";
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = _binCode;
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            if (outputs[1]._option != "direct")
            {
                bindata[1] |= 0x8000;
            }
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]); // In
                if (inputs[i]._option != "direct")
                {
                    bindata[1 + i] |= 0x8000;
                }
            }
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            ushort addr = Common.LOBYTE(data[1]);
            bool option = (Common.HIBYTE(data[1]) >> 7) == 1;
            outputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[2]);
            option = (Common.HIBYTE(data[2]) >> 7) == 1;
            inputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[3]);
            option = (Common.HIBYTE(data[3]) >> 7) == 1;
            inputs.Add(2, new PinData(GetVarName(addr), PinData.Option[option]));
        }
    }

    internal class MoreInstruction : BlockInstruction
    {
        public MoreInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 6;
            BlockType = ">";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� [>]";
            BlockPropType = "MoreLes";
            BlockName = "More";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "MORE16(";
            asm += _userData["shiftR"] + ",";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct")
                {
                    asm += "!";
                }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += "," + _userData["ustavkaVozvr"] + ",0);\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)((ushort)29 + Convert.ToUInt16(_userData["shiftR"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]); // Out

            bindata[2] = GetAddress(mm, inputs[1]); // In
            if (inputs[1]._option != "direct")
            {
                bindata[2] |= 0x8000;
            }
            bindata[3] = GetAddress(mm, inputs[2]); // In
            if (inputs[2]._option != "direct")
            {
                bindata[3] |= 0x8000;
            }

            bindata[4] = Convert.ToUInt16(_userData["ustavkaVozvr"]);
            bindata[5] = 0;
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            outputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));

            ushort addr = Common.LOBYTE(data[2]);
            bool option = (Common.HIBYTE(data[2]) >> 7) == 1;
            inputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[3]);
            option = (Common.HIBYTE(data[3]) >> 7) == 1;
            inputs.Add(2, new PinData(GetVarName(addr), PinData.Option[option]));

            int shiftR = Common.HIBYTE(data[0]);
            double koef = data[4] * Math.Pow(2, 16 - shiftR) / 65536;
            _userData.Add("ustavkaVozvr", data[4].ToString("D"));
            _userData.Add("koeff", koef.ToString("F1")); // ����� � ��������� ������������
            _userData.Add("shiftR", shiftR.ToString("D"));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    internal class LessInstruction : BlockInstruction
    {
        public LessInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 6;
            BlockType = "<";
            BlockGroup = "����������� ������";
            BlockDescription = "���������� ������� [<]";
            BlockPropType = "MoreLes";
            BlockName = "Less";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "LESS16(";
            asm += _userData["shiftR"] + ",";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = 1; i <= inputs.Count; i++)
            {
                asm += ",";
                if (inputs[i]._option != "direct")
                {
                    asm += "!";
                }
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += "," + _userData["ustavkaVozvr"] + ",0);\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)((ushort)30 + Convert.ToUInt16(_userData["shiftR"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            for (int i = 1; i <= inputs.Count; i++)
            {
                bindata[1 + i] = GetAddress(mm, inputs[i]); // In
                if (inputs[i]._option != "direct")
                {
                    bindata[1 + i] |= 0x8000;
                }
            }
            bindata[_blockSize - 2] = Convert.ToUInt16(_userData["ustavkaVozvr"]);
            bindata[_blockSize - 1] = 0;
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            outputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));

            ushort addr = Common.LOBYTE(data[2]);
            bool option = (Common.HIBYTE(data[2]) >> 7) == 1;
            inputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[3]);
            option = (Common.HIBYTE(data[3]) >> 7) == 1;
            inputs.Add(2, new PinData(GetVarName(addr), PinData.Option[option]));

            int shiftR = Common.HIBYTE(data[0]);
            double koef = data[4] * Math.Pow(2, 16 - shiftR) / 65536;
            _userData.Add("ustavkaVozvr", data[4].ToString("D"));
            _userData.Add("koeff", koef.ToString("F1")); // ����� � ��������� ������������
            _userData.Add("shiftR", shiftR.ToString("D"));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    #endregion

    //Not
    internal class NotInstruction : BlockInstruction
    {
        public NotInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 3;
            BlockType = "~";
            BlockGroup = "������� ������";
            BlockDescription = "���������� ������� ��";
            BlockPropType = "Fixed";
            BlockName = "Not";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "MOV(";
            asm += inputs[1]._name;
            asm += ":0x" + GetAddress(mm, inputs[1]).ToString("X4");
            asm += ",";
            asm += "!";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            //TIME AND TYPE ADD HERE !!!
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = 9; //TYPE IS HERE!!!!
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            bindata[1] |= 0x8000;
            bindata[2] = GetAddress(mm, inputs[1]); // In
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            ushort addr = Common.LOBYTE(data[1]);
            outputs.Add(1, new PinData(GetVarName(addr), PinData.Option[true]));
            inputs.Add(1, new PinData(GetVarName(data[2]), PinData.Option[false]));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    //Timer
    internal class TimerInstruction : BlockInstruction
    {
        public TimerInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 6;
            BlockType = "T";
            BlockGroup = "����������� ������";
            BlockDescription = "������";
            BlockPropType = "TimeInverseInOut";
            BlockName = "Timer";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "TIMER(";
            if (outputs[1]._option != "direct")
            {
                asm += "!";
            }
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            asm += ",";
            if (inputs[1]._option != "direct")
            {
                asm += "!";
            }
            asm += inputs[1]._name;
            asm += ":0x" + GetAddress(mm, inputs[1]).ToString("X4");
            asm += ",Time:" + _userData["time"].Replace(",", ".");
            asm += ",Type:" + _userData["timerType"];
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(12 + Convert.ToUInt16(_userData["timerType"]) * 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            if (outputs[1]._option != "direct")
            {
                bindata[1] |= 0x8000;
            }
            bindata[2] = GetAddress(mm, inputs[1]); // Out
            if (inputs[1]._option != "direct")
            {
                bindata[2] |= 0x8000;
            }
            bindata[3] = (ushort)((Convert.ToDouble(_userData["time"])) * 100);
            bindata[4] = 0;
            bindata[5] = 0;

            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            ushort timerType = Common.HIBYTE(data[0]);
            _userData.Add("timerType", timerType.ToString("D"));
            ushort addr = Common.LOBYTE(data[1]);
            bool option = (Common.HIBYTE(data[1]) >> 7) == 1;
            outputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));

            addr = Common.LOBYTE(data[2]);
            option = (Common.HIBYTE(data[2]) >> 7) == 1;
            inputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));

            string time = ((double)(data[3]) / 100).ToString();
            _userData.Add("time", time);
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            int type = int.Parse(_userData["timerType"]);
            desc.name = string.Format("[{0};{1} c]",TimerProps.InitList()[type],_userData["time"]);
            return desc;
        }
    }

    //Trigger
    internal class RstInstruction : BlockInstruction
    {
        public RstInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 5;
            BlockType = "RST";
            BlockGroup = "����������� ������";
            BlockDescription = "RS �������";
            BlockPropType = "Fixed";
            BlockName = "RST";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "RST(";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = inputs.Count; i > 0; i--)
            {
                asm += ",";
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(11);
            bindata[1] = GetAddress(mm, outputs[1]); // Out

            bindata[2] = GetAddress(mm, inputs[2]); // in R
            bindata[3] = GetAddress(mm, inputs[1]); // in S
            bindata[4] = 0;
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            outputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));

            inputs.Add(1, new PinData(GetVarName(data[3]), PinData.Option[false]));
            inputs.Add(2, new PinData(GetVarName(data[2]), PinData.Option[false]));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    internal class SrtInstruction : BlockInstruction
    {
        public SrtInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 5;
            BlockType = "SRT";
            BlockGroup = "����������� ������";
            BlockDescription = "SR �������";
            BlockPropType = "Fixed";
            BlockName = "SRT";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "SRT(";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            for (int i = inputs.Count; i > 0; i--)
            {
                asm += ",";
                asm += inputs[i]._name;
                asm += ":0x" + GetAddress(mm, inputs[i]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(11 + 0x0100);
            bindata[1] = GetAddress(mm, outputs[1]); // Out

            bindata[2] = GetAddress(mm, inputs[2]); // inR
            bindata[3] = GetAddress(mm, inputs[1]); // inS
            bindata[4] = 0;
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            outputs.Add(1, new PinData(GetVarName(data[1]), PinData.Option[false]));

            inputs.Add(1, new PinData(GetVarName(data[3]), PinData.Option[false]));
            inputs.Add(2, new PinData(GetVarName(data[2]), PinData.Option[false]));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    //Mux
    internal class MuxInstruction : BlockInstruction
    {
        public MuxInstruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = 5;
            BlockType = "MS";
            BlockGroup = "����������� ������";
            BlockDescription = "�������������";
            BlockPropType = "MS";
            BlockName = "Mux";
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "MUX(";
            if (outputs[1]._option != "direct")
            {
                asm += "!";
            }
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");
            int[] ind = new int[] { 3, 1, 2 };
            for (int i = 0; i < inputs.Count; i++)
            {
                asm += ",";
                if (inputs[ind[i]]._option != "direct")
                {
                    asm += "!";
                }
                asm += inputs[ind[i]]._name;
                asm += ":0x" + GetAddress(mm, inputs[ind[i]]).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)13;
            bindata[1] = GetAddress(mm, outputs[1]); // Out
            if (outputs[1]._option != "direct")
            {
                bindata[1] |= 0x8000;
            }

            bindata[2] = GetAddress(mm, inputs[3]); // In3
            if (inputs[3]._option != "direct")
            {
                bindata[2] |= 0x8000;
            }

            bindata[3] = GetAddress(mm, inputs[1]); // In1
            if (inputs[1]._option != "direct")
            {
                bindata[3] |= 0x8000;
            }

            bindata[4] = GetAddress(mm, inputs[2]); // In2
            if (inputs[2]._option != "direct")
            {
                bindata[4] |= 0x8000;
            }
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            ushort addr = Common.LOBYTE(data[1]);
            bool option = Common.HIBYTE(data[1]) >> 7 == 1;
            outputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));

            addr = Common.LOBYTE(data[2]);
            option = Common.HIBYTE(data[2]) >> 7 == 1;
            inputs.Add(3, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[3]);
            option = Common.HIBYTE(data[3]) >> 7 == 1;
            inputs.Add(1, new PinData(GetVarName(addr), PinData.Option[option]));
            addr = Common.LOBYTE(data[4]);
            option = Common.HIBYTE(data[4]) >> 7 == 1;
            inputs.Add(2, new PinData(GetVarName(addr), PinData.Option[option]));
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    internal class Mux16Instruction : BlockInstruction
    {
        public Mux16Instruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = inputs.Count + 2;
            BlockType = "MS16";
            BlockGroup = "����������� ������";
            BlockDescription = "������������� 16-�����������";
            BlockPropType = "MS16";
            BlockName = "Mux16";
        }

        public void RevreshConst()
        {
            _blockSize = inputs.Count + 2;
        }

        public void InitBlockSize(ushort data)
        {
            int mask = (data >> 8) & 0xF;
            _blockSize = mask + 4;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "MUX16(";
            asm += _userData["addr"] + ",";
            asm += outputs[1]._name;
            asm += ":0x" + GetAddress(mm, outputs[1]).ToString("X4");

            int controlInpKey = inputs.Keys.Max();
            asm += "," + inputs[controlInpKey]._name + ":0x" + GetAddress(mm, inputs[controlInpKey]).ToString("X4");
            foreach (var input in inputs)
            {
                if (input.Key == controlInpKey) continue;
                asm += ",";
                asm += input.Value._name;
                asm += ":0x" + GetAddress(mm, input.Value).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] = (ushort)(31 + 0x0100 * Convert.ToUInt16(_userData["mask"]) +
                     0x1000 * Convert.ToUInt16(_userData["addr"]));

            bindata[1] = GetAddress(mm, outputs[1]); // Out
            bindata[2] = GetAddress(mm, inputs.Last().Value); //InControl
            for (int i = 0; i < inputs.Count - 1; i++)
            {
                bindata[3 + i] = GetAddress(mm, inputs[i+1]);
            }
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            int mask = (data[0] >> 8) & 0xF;
            int addr = (data[0] >> 12) & 0xF;
            _userData.Add("mask", mask.ToString("D"));
            _userData.Add("addr", addr.ToString("D"));
            _userData.Add("count", (mask + 1).ToString("D"));
            outputs.Add(1, new PinData(GetVarName(Common.LOBYTE(data[1])), PinData.Option[false]));

            int inpLen = data.Length - 3;
            inputs.Add(inpLen + 2, new PinData(GetVarName(Common.LOBYTE(data[2])), PinData.Option[false]));
            for (int i = 0; i < inpLen; i++)
            {
                inputs.Add(i + 1, new PinData(GetVarName(Common.LOBYTE(data[3 + i])), PinData.Option[false]));
            }
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    internal class Decod16Instruction : BlockInstruction
    {
        public Decod16Instruction(string sName, SchematicSystem schem)
            : base(sName, schem)
        {
            _blockSize = outputs.Count + 2;
            BlockType = "D";
            BlockGroup = "����������� ������";
            BlockDescription = "����������";
            BlockPropType = "D";
            BlockName = "Decod";
        }

        public void RevreshConst()
        {
            _blockSize = outputs.Count + 2;
        }

        public void InitBlockSize(ushort data)
        {
            int mask = (data >> 8) & 0xF;
            _blockSize = mask + 3;
        }

        public override string GenerateAsm(MemManager mm)
        {
            string asm = "CODE 0x" + _memObject.Addr.ToString("X4") + ": " + "DECOD16(";
            asm += _userData["addr"] + ",";
            asm += inputs[1]._name;
            asm += ":0x" + GetAddress(mm, inputs[1]).ToString("X4");
            foreach (var output in outputs)
            {
                asm += ",";
                asm += output.Value._name;
                asm += ":0x" + GetAddress(mm, output.Value).ToString("X4");
            }
            asm += ");\n";
            return asm;
        }

        public override ushort[] GenerateBin(MemManager mm)
        {
            ushort[] bindata = new ushort[_blockSize];
            bindata[0] =
                (ushort)(32 + 0x0100 * Convert.ToUInt16(_userData["mask"]) + 0x1000 * Convert.ToUInt16(_userData["addr"]));
            bindata[1] = GetAddress(mm, inputs[1]); // In
            int index = 2;
            foreach (var item in outputs)
            {
                bindata[index] = GetAddress(mm, item.Value);
                index++;
            }
            return bindata;
        }

        public override void InstructionInitFromBin(ushort[] data)
        {
            int mask = (data[0] >> 8) & 0xF;
            int addr = (data[0] >> 12) & 0xF;
            _userData.Add("mask", mask.ToString("D"));
            _userData.Add("addr", addr.ToString("D"));
            _userData.Add("count", (mask + 1).ToString("D"));
            inputs.Add(1, new PinData(GetVarName(Common.LOBYTE(data[1])), PinData.Option[false]));
            int outLen = data.Length - 2;
            for (int i = 0; i < outLen; i++)
            {
                outputs.Add(i + 1, new PinData(GetVarName(Common.LOBYTE(data[2 + i])), PinData.Option[false]));
            }
        }

        public override BlockDesc GetBlockDescOnInstruction(string device, double vers, string deviceConfig = "")
        {
            BlockDesc desc = base.GetBlockDescOnInstruction(device, vers);
            desc.posX = _blockX;
            desc.posY = BlockInstruction.BlockY;
            BlockInstruction.IncreaseBlockY(outputs.Count, inputs.Count);
            return desc;
        }
    }

    #endregion

    #endregion
}


