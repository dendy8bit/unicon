using System;
using System.IO;
using BEMN.Forms;
using BEMN.MBServer;
using BMTCD.HelperClasses;

namespace BMTCD.UDZT
{
    /// <summary>
    /// ����� ��� ���������� � ���� ���������� ���������
    /// </summary>
    public class Mr801Compiler
    {
        private ProjectInstruction _project;
        private MemManager _memManager;
	    private MessageBoxForm _formCheck;
		private int _binarysize;
        private string _device;
        private double _vers;
        /// <summary>
        /// ������ ��������� �����
        /// </summary>
        public int BinarySize
        {
            get { return _binarysize; }
        }
        /// <summary>
        /// ����� ��� ���������� � ���� ���������� ���������
        /// </summary>
        public Mr801Compiler(string device, double vers)
        {
            _project = new ProjectInstruction("ROOTSEG");
            _memManager = new MemManager();
            _device = device;
            _vers = vers;
            _project.InstructionInit(device, " BSBGL PROJECT");
        }
        /// <summary>
        /// ���������� � ���� ���������
        /// </summary>
        /// <returns>������ ���� ����� ��������� ������</returns>
        public ushort[] Make()
        {
			this._project.AddHeaderAndExitInstruction(this._device, this._vers);
            this._project.Locate(ref _memManager);
            ushort[] programmBinary = new ushort[4096];
            ushort[] tmpBinary = _project.GenerateBin(_memManager);
            if (tmpBinary.Length > programmBinary.Length)
            {
                return new ushort[0];
            }
            tmpBinary.CopyTo(programmBinary,0);
            _binarysize = _project.GetBinSize();
            CalcCrc(programmBinary);
            return programmBinary;
        }

        /// <summary>
        /// ���������� ����� �� ��������� �����
        /// </summary>
        /// <param name="scheme">�����</param>
        /// <param name="bin">������ ����, ����������� ������</param>
        /// <param name="deviceConfig">���� ���� ��������� ������������, �� ����������� ���������� ������������ ����������</param>
        public void SetSchemeOnBin(SchematicSystem scheme, ushort[] bin, string deviceConfig = "")
        {
            _project.AddInstruction(scheme, bin, _device, _vers, deviceConfig);
        }
        /// <summary>
        /// �������������� ���������� ��� �������� ��������� ����� ������
        /// </summary>
        /// <param name="name">�������� �����</param>
        /// <param name="doc">�����</param>
        /// <exception cref="Exception">������������ �����</exception>
        public void AddSource(string name, SchematicSystem doc)
        {
            _project.AddInstruction(name, doc);
        }
        public void ResetCompiller()
        {
            _project.Clear();
            _memManager.Clear();
        }   
        public int GetRamRequired()
        {
            return _memManager.GetMaxRamAddress()+1;
        }
        public void UpdateVol(ushort[] buff)
        {
            _memManager.UpdateVol(buff);
            _project.UpdateValue();
        }

        /// <summary>
        /// ���������� � �������� � � .asm ����� ��������� �� ���������� ������
        /// </summary>
        /// <param name="filePath">���� ���������� �����</param>
        public void SaveLogicInFile(string filePath)
        {
            try
            {
                _project.Locate(ref _memManager);
                string str = _project.GenerateAsm(_memManager);
                using (var sw = new StreamWriter(filePath))
                {
                    sw.Write(str);
                }
                ushort[] programmBinary = new ushort[4096];
                ushort[] tmpBinary = _project.GenerateBin(_memManager);
                if (tmpBinary.Length > programmBinary.Length)
                {
                    return;
                }
                tmpBinary.CopyTo(programmBinary, 0);
                byte[] byteBin = CalcCrc(programmBinary);
                filePath = filePath.Replace(".asm", ".bin");
                using (var fs = new FileStream(filePath, FileMode.Create))
                {
                    fs.Write(byteBin,0, byteBin.Length);
                }
            }
            catch (Exception e)
            {
                throw new InvalidDataException(e.Message);
            }
        }

        private byte[] CalcCrc(ushort[] programmBinary)
        {
            if (programmBinary.Length <= 0) return null;
            byte[] tbuff = Common.TOBYTES(programmBinary, false);
            ushort crc = CRC16.CalcCrcFast(tbuff, tbuff.Length - 2);
            tbuff[tbuff.Length - 1] = (byte)(crc & 0xFF);
            tbuff[tbuff.Length - 2] = (byte)(crc >> 8);
            programmBinary[programmBinary.Length - 1] = (ushort)(
                (ushort)((ushort)(crc >> (ushort)8) & (ushort)0x00ff) +
                (ushort)((ushort)(crc << (ushort)8) & (ushort)0xff00));
            return tbuff;
        }
    }
}
