<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	  
<xsl:template match="/">
  <html>
<head>

</head>
	<body>
    <xsl:variable name="vers" select="МР851/Версия"></xsl:variable>
		 <h2>Устройство <xsl:value-of select="МР851/Тип_устройства"/>. Версия ПО <xsl:value-of select="МР851/Версия"/>. Номер <xsl:value-of select="МР851/Номер_устройства"/> </h2>
		 <h3><b>Секция 1</b></h3>
		 <table border="1" cellspacing="0">
		 <td>
				 <b>Параметры напряжений</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="FFFFCC">
				 <th>Измерение</th>
				 <th>ТН коэфф.</th>
				 <th>Kтн K*</th>
				 <th>ТННП коэфф.</th>
				 <th>Kтннп K*</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Секция1/Измерение"/></td>
				<td><xsl:value-of select="МР851/Секция1/ТН"/></td>
				<td><xsl:value-of select="МР851/Секция1/ТН_коэфф"/></td>
				<td><xsl:value-of select="МР851/Секция1/ТННП"/></td>
				<td><xsl:value-of select="МР851/Секция1/ТННП_коэфф"/></td>
				</tr>
				</table>
				<p></p>
				<b>Номинальный ток ТТ</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="FFFFCC">
				 <th>ВВ, А</th>
				 <th>СВ, А</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Секция1/BB"/></td>
				<td><xsl:value-of select="МР851/Секция1/CB"/></td>
				</tr>
				</table>
				<p></p>
				<b>Параметры регулируемой секции</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="FFFFCC">
				 <th>Uп, В</th>
				 <th>Uп1, В</th>
				 <th>Uп2, В</th>
				 <th>Uп3, В</th>
				 <th>dU, В</th>
				 <th>Umin, В</th>
         <xsl:if test="$vers &gt; 2.00">
          <th>Umin(р+д), В</th>
         </xsl:if>
				 <th>Umax, В</th>
				 <th>U2(Un), В</th>
				 <th>Imax, In</th>
				 <th>Uk, В</th>
				 <th>Uk m, В</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Секция1/Up"/></td>
				<td><xsl:value-of select="МР851/Секция1/Up1"/></td>
				<td><xsl:value-of select="МР851/Секция1/Up2"/></td>
				<td><xsl:value-of select="МР851/Секция1/Up3"/></td>
				<td><xsl:value-of select="МР851/Секция1/dU"/></td>
				<td><xsl:value-of select="МР851/Секция1/Umin"/></td>
        <xsl:if test="$vers &gt; 2.00">
        <td><xsl:value-of select="МР851/Секция1/Umin_x0028_р_x002B_д_x0029_"/></td>
        </xsl:if>
				<td><xsl:value-of select="МР851/Секция1/Umax"/></td>
				<td><xsl:value-of select="МР851/Секция1/Un"/></td>
				<td><xsl:value-of select="МР851/Секция1/Imax"/></td>
				<td><xsl:value-of select="МР851/Секция1/Uk"/></td>
				<td><xsl:value-of select="МР851/Секция1/Ukm"/></td>
				</tr>
				</table>
		 </td>
		</table>
				<p></p>
		 		 <h3><b>Секция 2</b></h3>
		 <table border="1" cellspacing="0">
		 <td>
				 <b>Параметры напряжений</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="FFFFCC">
				 <th>Измерение</th>
				 <th>ТН коэфф.</th>
				 <th>Kтн K*</th>
				 <th>ТННП коэфф.</th>
				 <th>Kтннп K*</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Секция2/Измерение"/></td>
				<td><xsl:value-of select="МР851/Секция2/ТН"/></td>
				<td><xsl:value-of select="МР851/Секция2/ТН_коэфф"/></td>
				<td><xsl:value-of select="МР851/Секция2/ТННП"/></td>
				<td><xsl:value-of select="МР851/Секция2/ТННП_коэфф"/></td>
				</tr>
				</table>
				<p></p>
				<b>Номинальный ток ТТ</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="FFFFCC">
				 <th>ВВ, А</th>
				 <th>СВ, А</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Секция2/BB"/></td>
				<td><xsl:value-of select="МР851/Секция2/CB"/></td>
				</tr>
				</table>
				<p></p>
				<b>Параметры регулируемой секции</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="FFFFCC">
				 <th>Uп, В</th>
				 <th>Uп1, В</th>
				 <th>Uп2, В</th>
				 <th>Uп3, В</th>
				 <th>dU, В</th>
				 <th>Umin, В</th>
         <xsl:if test="$vers &gt; 2.00">
          <th>Umin(р+д), В</th>
         </xsl:if>
				 <th>Umax, В</th>
				 <th>Un, В</th>
				 <th>Imax, In</th>
				 <th>Uk, В</th>
				 <th>Uk m, В</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Секция2/Up"/></td>
				<td><xsl:value-of select="МР851/Секция2/Up1"/></td>
				<td><xsl:value-of select="МР851/Секция2/Up2"/></td>
				<td><xsl:value-of select="МР851/Секция2/Up3"/></td>
				<td><xsl:value-of select="МР851/Секция2/dU"/></td>
				<td><xsl:value-of select="МР851/Секция2/Umin"/></td>
          <xsl:if test="$vers &gt; 2.00">
            <td><xsl:value-of select="МР851/Секция2/Umin_x0028_р_x002B_д_x0029_"/></td>
          </xsl:if>
				<td><xsl:value-of select="МР851/Секция2/Umax"/></td>
				<td><xsl:value-of select="МР851/Секция2/Un"/></td>
				<td><xsl:value-of select="МР851/Секция2/Imax"/></td>
				<td><xsl:value-of select="МР851/Секция2/Uk"/></td>
				<td><xsl:value-of select="МР851/Секция2/Ukm"/></td>
				</tr>
				</table>
		 </td>
		</table>
				<p></p>	
<!--|||||||||||||||||||||||||входные сигналы|||||||||||||||||||||||||||||||||||||||||-->
		<h3><b>Входные сигналы</b></h3>
	
		<table border="1" cellspacing="0">
		 <tr bgcolor="CCFF99">
         <th>Блок. по току</th>
         <th>Блок по темп.</th>
		 <th>Uп3</th>
		 <th>Сброс сигнализ.</th>
		 <th>Контр. пит. привод.</th>
		 <th>Сброс блокиров.</th>
       <xsl:if test="$vers &gt; 2.00">
        <th>Блокировка авт. и дист. режимов</th>
       </xsl:if>
		</tr>
 
		<tr align="center">	
         <td><xsl:value-of select="МР851/Входные_сигналы/Блок_по_току"/></td>
		 <td><xsl:value-of select="МР851/Входные_сигналы/Блок_по_темп"/></td>
		 <td><xsl:value-of select="МР851/Входные_сигналы/Uп3"/></td>
		 <td><xsl:value-of select="МР851/Входные_сигналы/Сброс_сигнализ"/></td>
		 <td><xsl:value-of select="МР851/Входные_сигналы/Контр_пит_привод"/></td>
		 <td><xsl:value-of select="МР851/Входные_сигналы/Сброс_блокиров"/></td>
      <xsl:if test="$vers &gt; 2.00">
          <td><xsl:value-of select="МР851/Входные_сигналы/Блокировка_автомат._и_дист._режимов"/></td>
      </xsl:if>
		</tr>
		</table>
<!--|||||||||||||||||||||||||выходные сигналы|||||||||||||||||||||||||||||||||||||||||-->
		<h3><b>Выходные сигналы</b></h3>
		<table border="1" cellspacing="0">
		<tr bgcolor="#c1ced5">
			<th>Номер реле</th>
			<th>Тип</th>
			<th>Сигнал</th>
			<th>Импульс, мс</th>
		</tr>
      <xsl:for-each select="МР851/Реле/Rows/RelayStruct">
	  <xsl:if test="position() &lt; 13">
		<tr align="center">
			<td><xsl:value-of select="position()"/></td>
			<td><xsl:value-of select="@Тип"/></td>
			<td><xsl:value-of select="@Сигнал"/></td>
			<td><xsl:value-of select="@Время"/></td>
		</tr>
		</xsl:if>
	  </xsl:for-each>
		</table>
  <p></p>	
		<h3><b>Сигнал неисправность</b></h3>
		<table border="1" cellspacing="0">
		<tr bgcolor="#c1ced5">
			<th>Аппаратная неисправность</th>
			<th>Программная неисправность</th>
			<th>Неисправность привода</th>
			<th>Возврат, мс</th>
		</tr>
  
		<tr align="center">

		 <td><xsl:variable  name = "v1" select = "МР851/Реле_неисправности/@Неисправность1" />
		 <xsl:if test="$v1 = 'false' " >Нет </xsl:if> <xsl:if test="$v1 = 'true' " > Есть </xsl:if></td>
		 
		 <td><xsl:variable  name = "v2" select = "МР851/Реле_неисправности/@Неисправность2" />
		 <xsl:if test="$v2 = 'false' " >Нет </xsl:if> <xsl:if test="$v2 = 'true' " > Есть </xsl:if></td>
		 
		 <td><xsl:variable  name = "v3" select = "МР851/Реле_неисправности/@Неисправность3" />
		 <xsl:if test="$v3 = 'false' " >Нет </xsl:if> <xsl:if test="$v3 = 'true' " > Есть </xsl:if></td>
		 
		 <td><xsl:value-of select="МР851/Реле_неисправности/@Импульс_реле_неисправность"/></td>
		</tr>

		</table>
<p></p>	

<!--|||||||||||||||||||||||||параметры регулирования|||||||||||||||||||||||||||||||||||||||||-->
				<h3><b>Параметры регулирования</b></h3>
    
				 <b>Параметры режима регулирования</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="CC6666">
				 <th>Режим регулиров.</th>
				 <th>Переход в автон. режим</th>
				 <th>Управл.: ключ</th>
				 <th>Управл.: пульт</th>
				 <th>Ручной режим</th>
         <xsl:if test="$vers &gt; 2.00">
           <th>Дист. режим</th>
         </xsl:if>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Параметры_регулирования/Режим_регулиров"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Переход_в_автом_режим"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Управл_ключ"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Управл_пульт"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Ручной_режим"/></td>
          <xsl:if test="$vers &gt; 2.00">
            <td>
              <xsl:value-of select="МР851/Параметры_регулирования/Управление_по_СДТУ"/>
            </td>
          </xsl:if>
				</tr>
				</table>

					 <p></p>	
				 <b>Задержки команд управления</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="CC6666">
				 <th>Задержка первой команды Тк1, мс</th>
				 <th>Задержка повторной команды Тк2, мс</th>
				 <th>Задержка команды перенапряжения Ткп, мс</th>
         <th>Задержка контроля перегрузки по току Тср, мс</th>
         <th>Задержка отключения питания привода Тот, мс</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Параметры_регулирования/Задержка_1_ком"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Задержка_повторной_ком"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Задержка_ком_перенапряжения"/></td>
        <td><xsl:value-of select="МР851/Параметры_регулирования/Задержка_перегрузки_по_току"/></td>
        <td><xsl:value-of select="МР851/Параметры_регулирования/Задержка_отключения_питания_привода"/></td>
				</tr>
				</table>
					 <p></p>	
				<b>Импульсы режима управления</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="CC6666">
				 <th>Время реакции Тр, мс</th>
				 <th>Время проверки завершения перекл. Тп, мс</th>
				 <th>Задержка снятия сигнала управления Тз, мс</th>
				 </tr>
     
				<tr align="center">
				<td><xsl:value-of select="МР851/Параметры_регулирования/Время_реакции"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Время_завершения"/></td>
				<td><xsl:value-of select="МР851/Параметры_регулирования/Задержка_снятия_сигнала_управления"/></td>
				</tr>
				</table>
				<p></p>
				<b>Счетчик переключений</b>
				 <table border="1" cellspacing="0">
				 <tr bgcolor="CC6666">
				 <th>Логометр</th>
				 <th>Текущая ступень</th>
				 <th>Выраб. ресурс</th>
         <th>Направл. счета</th>
				 <th>Кол-во ступеней</th>
				 <th>Rступени, Ом</th>
         <th>Начальная ступень</th>
				 <th>Конечная ступень</th>
				 </tr>
     
				<tr align="center">
        <td>
          <xsl:variable  name = "v1" select = "МР851/Параметры_регулирования/Логометр" />
          <xsl:if test="$v1 = 'false' " >Нет </xsl:if>
          <xsl:if test="$v1 = 'true' " > Есть </xsl:if>
        </td>
				<td><xsl:value-of select="МР851/Текущая_ступень"/></td>
				<td><xsl:value-of select="МР851/Выраб_ресурс"/></td>
        <td><xsl:value-of select="МР851/Параметры_регулирования/Направл_счета"/></td>
        <td><xsl:value-of select="МР851/Секция1/Количество_ступеней"/></td>
        <td><xsl:value-of select="МР851/Параметры_регулирования/Rступени"/></td>
        <td><xsl:value-of select="МР851/Секция1/Начальная_ступень"/></td>
        <td><xsl:value-of select="МР851/Секция1/Конечная_ступень"/></td> 
				</tr>
				</table>
    
				 <p></p>	
		<b>Неисправности привода</b>
    <table border="1" cellspacing="0">
      <td>
        <b>Фиксация блокировки управления</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="#c1ced5">
            <th>Привод не пошел</th>
            <th>Привод не застрял</th>
            <th>Привод побежал</th>
          </tr>

          <tr align="center">
            <td>
              <xsl:variable  name = "v1" select = "МР851/Входные_сигналы/Фиксация_x0020_блокировки_x0020_привод_x0020_не_x0020_пошел" />
              <xsl:if test="$v1 = 'false' " >Нет </xsl:if>
              <xsl:if test="$v1 = 'true' " > Есть </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v2" select = "МР851/Входные_сигналы/Фиксация_x0020_блокировки_x0020_привод_x0020_застрял" />
              <xsl:if test="$v2 = 'false' " >Нет </xsl:if>
              <xsl:if test="$v2 = 'true' " > Есть </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v3" select = "МР851/Входные_сигналы/Фиксация_x0020_блокировки_x0020_привод_x0020_побежал" />
              <xsl:if test="$v3 = 'false' " >Нет </xsl:if>
              <xsl:if test="$v3 = 'true' " > Есть </xsl:if>
            </td>
          </tr>
        </table>
        <p></p>

        <b>Действие на отключение питания привода</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="#c1ced5">
            <th>Привод не пошел</th>
            <th>Привод не застрял</th>
            <th>Привод побежал</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:variable  name = "v1" select = "МР851/Входные_сигналы/Фиксация_x0020_блокировки_x0020_привод_x0020_не_x0020_пошел" />
              <xsl:if test="$v1 = 'false' " >Нет </xsl:if>
              <xsl:if test="$v1 = 'true' " > Есть </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v2" select = "МР851/Входные_сигналы/Фиксация_x0020_блокировки_x0020_привод_x0020_застрял" />
              <xsl:if test="$v2 = 'false' " >Нет </xsl:if>
              <xsl:if test="$v2 = 'true' " > Есть </xsl:if>
            </td>
            <td>
              <xsl:variable  name = "v3" select = "МР851/Входные_сигналы/Фиксация_x0020_блокировки_x0020_привод_x0020_побежал" />
              <xsl:if test="$v3 = 'false' " >Нет </xsl:if>
              <xsl:if test="$v3 = 'true' " > Есть </xsl:if>
            </td>
          </tr>
        </table>
      </td>
    </table>
	</body>
  </html>
</xsl:template>






</xsl:stylesheet>