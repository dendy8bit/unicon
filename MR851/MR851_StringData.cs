﻿using System.Collections.Generic;

namespace BEMN.MR851
{
    public class Strings
    {
        public static double DevVersion = 0;

        public static List<string> MEASURE
        {
            get
            {
                return new List<string>(new string[] {  "Uab+Un",
                                                        "Uab+Ubc"
                                                     });
            }
        }

        public static List<string> KOEFF
        {
            get
            {
                return new List<string>(new string[] {  "1",
                                                        "1000"
                                                     });
            }
        }

        public static List<string> E_SIGNALS
        {
            get
            {
                return new List<string>(new string[] {  "Нет",
                                                        "Д1 ИНВ.",
                                                        "Д1",
                                                        "Д2 ИНВ.",
                                                        "Д2",
                                                        "Д3 ИНВ.",
                                                        "Д3",
                                                        "Д4 ИНВ.",
                                                        "Д4",
                                                     });
            }
        }

        public static List<string> RELE_TYPE
        {
            get
            {
                return new List<string>(new string[] {  "Повторитель",
                                                        "Блинкер"
                                                     });
            }
        }

        public static List<string> RelayNames
        {
            get
            {
                return new List<string>(new string[] {  "1",
                                                        "2",
                                                        "3",
                                                        "4",
                                                        "5",
                                                        "6",
                                                        "7",
                                                        "8",
                                                        "9",
                                                        "10",
                                                        "11",
                                                        "12"
                                                     });
            }
        }

        public static List<string> RELE_SIGNALS
        {
            get
            {
                return new List<string>(new string[] {  "Нет",
                                                        "ПРИБАВИТЬ  <ИНВ>",
    	                                                "ПРИБАВИТЬ",
    	                                                "УБАВИТЬ    <ИНВ>",
    	                                                "УБАВИТЬ",
    	                                                "НЕИСПРАВ.  <ИНВ>",
    	                                                "НЕИСПРАВНОСТЬ",
    	                                                "БЛОКИРОВКА <ИНВ>",
    	                                                "БЛОКИРОВКА",
                                                        "РУЧНОЙ РЕЖИМ",
                                                        "АВТО.  РЕЖИМ",
    	                                                "РЕЗЕРВ     <ИНВ>",
    	                                                "РЕЗЕРВ",
    	                                                "РЕЗЕРВ     <ИНВ>",
    	                                                "РЕЗЕРВ",
    	                                                "СИГНАЛИЗ.  <ИНВ>",
    	                                                "СИГНАЛИЗАЦИЯ",
    	                                                "ВХ.ПРИБАВ. <ИНВ>",
    	                                                "ВХ.ПРИБАВИТЬ",
    	                                                "ВХ.УБАВИТЬ <ИНВ>",
    	                                                "ВХ.УБАВИТЬ",
    	                                                "ВХ.ВЕРХ.ПОЛ<ИНВ>",
    	                                                "ВХ.ВЕРХ.ПОЛ.",
                                                        "ВХ.НИЖ.ПОЛ.<ИНВ>",
                                                        "ВХ.НИЖ.ПОЛ.",
                                                        "ВХ.ПЕРЕКЛ. <ИНВ>",
                                                        "ВХ.ПЕРЕКЛЮЧЕННИЕ",
                                                        "ВХ.РУЧ.РЕЖИМ",
                                                        "ВХ.АВТО.РЕЖИМ",
                                                        "ВХ.СЕКЦИЯ 1<ИНВ>",
                                                        "ВХ.СЕКЦИЯ 1",
                                                        "ВХ.СЕКЦИЯ 2<ИНВ>",
                                                        "ВХ.СЕКЦИЯ 2",
                                                        "ВХ.КОНТР.2С<ИНВ>",
                                                        "ВХ.КОНТР.2С.",
                                                        "ВХ.ВН.БЛОК.<ИНВ>",
                                                        "ВХ.ВН.БЛОК.",
                                                        "ВХ.Uп1     <ИНВ>",
                                                        "ВХ.Uп1",
                                                        "ВХ.Uп2     <ИНВ>",
                                                        "ВХ.Uп2",
                                                        "Д1         <ИНВ>",
                                                        "Д1",
                                                        "Д2         <ИНВ>",
                                                        "Д2",
                                                        "Д3         <ИНВ>",
                                                        "Д3",
                                                        "Д4         <ИНВ>",
                                                        "Д4",
                                                        "БЛОК. ПО I <ИНВ>",
                                                        "БЛОК. ПО I",
                                                        "БЛОК. ПО Т <ИНВ>",
                                                        "БЛОК. ПО Т",
                                                        "БЛОК. ПО Un<ИНВ>",
                                                        "БЛОК. ПО Un",
                                                        "БЛОК. ПО U2<ИНВ>",
                                                        "БЛОК. ПО U2",
                                                        "БЛ. ПО Umin<ИНВ>",
                                                        "БЛ. ПО Umin",
                                                        "БЛ. ПО Umax<ИНВ>",
                                                        "БЛ. ПО Umax",
                                                        "ВЫБОР СЕК.1<ИНВ>",
                                                        "ВЫБОР СЕКЦИИ 1  ",
                                                        "ВЫБОР СЕК.2<ИНВ>",
                                                        "ВЫБОР СЕКЦИИ 2  ",
                                                        "U  > НОРМЫ <ИНВ>",
                                                        "U  > НОРМЫ",
                                                        "U  < НОРМЫ <ИНВ>",
                                                        "U  < НОРМЫ",
                                                        "ПЕРЕГРУЗКА <ИНВ>",
                                                        "ПЕРЕГРУЗКА",
                                                        "РАБ.ПО Uп  <ИНВ>",
                                                        "РАБ.ПО Uп",
                                                        "РАБ.ПО Uп1 <ИНВ>",
                                                        "РАБ.ПО Uп1",
                                                        "РАБ.ПО Uп2 <ИНВ>",
                                                        "РАБ.ПО Uп2",
                                                        "РАБ.ПО Uп3 <ИНВ>",
                                                        "РАБ.ПО Uп3",
                                                        "НЕТ ПИТ.ПР.<ИНВ>",
                                                        "НЕТ ПИТАНИЯ ПР. ",
                                                        "НЕИСПРАВ.ПР<ИНВ>",
                                                        "НЕИСПРАВ.ПРИВОДА",
                                                        "ПР.НЕ ПОШЕЛ<ИНВ>",
                                                        "ПР.НЕ ПОШЕЛ",
                                                        "ПР. ЗАСТРЯЛ<ИНВ>",
                                                        "ПР. ЗАСТРЯЛ",
                                                        "ПР. ПОБЕЖАЛ<ИНВ>",
                                                        "ПР. ПОБЕЖАЛ",
                                                        "ОТК.ПИТАНИЯ 1<ИНВ>",
                                                        "ОТК.ПИТАНИЯ 1",
                                                        "ОТК.ПИТАНИЯ 2<ИНВ>",
                                                        "ОТК.ПИТАНИЯ 2",
                                                        "РЕЗЕРВ     <ИНВ>",
                                                        "РЕЗЕРВ",
                                                        "РЕЗЕРВ     <ИНВ>",
                                                        "РЕЗЕРВ"
                                                     });
            }
        }


        public static List<string> YES_NO
        {
            get
            {
                return new List<string>(new string[] {  "Запрещено",
                                                        "Разрешено"
                                                     });
            }
        }

        public static List<string> REG_MODE
        {
            get
            {
                return new List<string>(new string[] {  "Непрерывный",
                                                        "Импульсный"
                                                     });
            }
        }

        public static List<string> AUTO_MODE
        {
            get
            {
                return new List<string>(new string[] {  "Ключ",
                                                        "Пульт"
                                                     });
            }
        }

        public static List<string> HAND_MODE
        {
            get
            {
                return new List<string>(new string[] {  "Контроль",
                                                        "Управление"
                                                     });
            }
        }

        public static List<string> COUNTER
        {
            get
            {
                return new List<string>(new string[] {  "Прямое",
                                                        "Обратное"
                                                     });
            }
        }


        public static List<string> JOURNAL
        {
            get
            {
                return new List<string>(new string[] {  
                                                        "ЖУРНАЛ ПУСТ",                  //0
                                                        "ОШИБКА ХРАНЕНИЯ ДАННЫХ",
                                                        "ОШИБКА ХРАНЕНИЯ ДАННЫХ",
                                                        "НЕИСПРАВНОСТЬ ВН. ШИНЫ",
                                                        "ВН. ШИНА ИСПРАВНА",
                                                        "ТЕМПЕРАТУРА ВЫШЕ НОРМЫ",
                                                        "ТЕМПЕРАТУРА В НОРМЕ",
                                                        "ВХОДА I НЕИСПРАВНЫ",
                                                        "ВХОДА I ИСПРАВНЫ",
                                                        "ВХОДА U НЕИСПРАВНЫ",
                                                        "ВХОДА U ИСПРАВНЫ",             //10
                                                        "МРВ НЕИСПРАВЕН",
                                                        "МРВ ИСПРАВЕН",
                                                        "Д.ВХОДЫ 1-8 НЕИСПРАВНЫ",
                                                        "Д.ВХОДЫ 1-8 ИСПРАВНЫ",
                                                        "Д.ВХОДЫ 9-16 НЕИСПРАВНЫ",
                                                        "Д.ВХОДЫ 9-16 ИСПРАВНЫ",
                                                        "ОШИБКА К.СУММЫ УСТАВОК",
                                                        "ОШИБКА К.СУММЫ ДАННЫХ",
                                                        "ОШИБКА К.СУММЫ ДАННЫХ",
                                                        "ОШИБКА ЖУРНАЛА СИСТЕМЫ",       //20
                                                        "ОШИБКА ЖУРНАЛА АВАРИЙ",
                                                        "ОСТАНОВКА ЧАСОВ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "МЕНЮ:УСТАВКИ ИЗМЕНЕНЫ",
                                                        "ПАРОЛЬ ИЗМЕНЕН",
                                                        "СБРОС ЖУРНАЛА СИСТЕМЫ",
                                                        "СБРОС ЖУРНАЛА АВАРИЙ",
                                                        "СБРОС РЕСУРСА ВЫКЛЮЧАТЕЛЯ",
                                                        "СБРОС ИНДИКАЦИИ",              //30
                                                        "ИЗМЕНЕНА ГРУППА УСТАВОК",
                                                        "СДТУ:УСТАВКИ ИЗМЕНЕНЫ",
                                                        "ОШИБКА ЗАДАЮЩЕГО ГЕНЕРАТОРА",
                                                        "РЕСТАРТ УСТРОЙСТВА",
                                                        "УСТРОЙСТВО ВЫКЛЮЧЕНО",
                                                        "УСТРОЙСТВО ВКЛЮЧЕНО",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "КРИТИЧЕСКАЯ ОШИБКА УСТРОЙСТВА",//40
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "НЕИСПР. ЦЕПЕЙ ВКЛЮЧЕНИЯ",
                                                        "НЕИСПР. ЦЕПЕЙ ОТКЛЮЧЕНИЯ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "ВЕРХНЕЕ ПОЛОЖЕНИЕ",            
                                                        "НИЖНЕЕ ПОЛОЖЕНИЕ",             //50
                                                        "КОНТРОЛЬ ОДНОЙ СЕКЦИИ",
                                                        "ПЕРЕГРУЗКА ПО ТОКУ",
                                                        "ВХОД СЕКЦИЯ 1",
                                                        "ВХОД СЕКЦИЯ 2",
                                                        "ВЫБОР СЕКЦИИ 1",
                                                        "ВЫБОР СЕКЦИИ 2",
                                                        "ОТКАЗ ПРИВОДА",
                                                        "РАБОТА ИО РПН ПО U >",
                                                        "РАБОТА ИО РПН ПО U <",         
                                                        "БЛОКИРОВКА ПО I",              //60
                                                        "БЛОКИРОВКА ПО T",
                                                        "БЛОКИРОВКА ВНЕШНЯЯ",
                                                        "КНОПКА ПРИБАВИТЬ",
                                                        "ВХОД ПРИБАВИТЬ",
                                                        "КНОПКА УБАВИТЬ",
                                                        "ВХОД УБАВИТЬ",
                                                        "БЛОКИРОВКА ПО Un",
                                                        "БЛОКИРОВКА ПО U2",
                                                        "БЛОКИРОВКА ПО Umin",           
                                                        "БЛОКИРОВКА ПО Umax",           //70
                                                        "НАПРЯЖЕНИЕ В НОРМЕ",
                                                        "БЛОКИРОВКA: ПР.НЕ ПОШЕЛ",
                                                        "БЛОКИРОВКA: ПР.ЗАСТРЯЛ",
                                                        "СООБЩЕНИЯ НЕТ",
                                                        "КОНТРОЛЬ ДВУХ СЕКЦИЙ",
                                                        "РАБОТА  ПО Uп",
                                                        "РАБОТА  ПО Uп1",
                                                        "РАБОТА  ПО Uп2",
                                                        "РАБОТА  ПО Uп3",               
                                                        "РУЧНОЙ РЕЖИМ ПО КЛЮЧУ",        //80
                                                        "АВТ.   РЕЖИМ ПО КЛЮЧУ",
                                                        "РУЧНОЙ РЕЖИМ ПО КНОПКЕ",
                                                        "АВТ.   РЕЖИМ ПО КНОПКЕ",
                                                        "НАЧАЛО ПЕРЕКЛЮЧЕНИЯ",
                                                        "ПЕРЕКЛЮЧЕНИЕ ЗАВЕРШЕНО",
                                                        "ВЫСОКОЕ НАПРЯЖЕНИЕ",
                                                        "НИЗКОЕ НАПРЯЖЕНИЕ",
                                                        "ВХОД:СБРОС БЛОКИРОВКИ",
                                                        "ПУЛЬТ:СБРОС БЛОКИРОВКИ",       
                                                        "СДТУ:СБРОС БЛОКИРОВКИ",        //90
                                                        "ПРИВОД ПОБЕЖАЛ",
                                                        "НЕТ ПИТАНИЯ ПРИВОДА",
                                                        "ЕСТЬ ПИТАНИЕ ПРИВОДА",
                                                        "РЕЗЕРВ",
                                                        "БЛОКИРОВКА СБРОШЕНА",
                                                        "ВХОД:СБРОС СИГНАЛИЗАЦИИ",
                                                        "ПУЛЬТ:СБРОС СИГНАЛИЗАЦИИ",
                                                        "СДТУ:СБРОС СИГНАЛИЗАЦИИ",
                                                        "СООБЩЕНИЯ НЕТ",                
                                                        "СООБЩЕНИЯ НЕТ",                //100
                                                        "СДТУ: УСТ. ДИСТ. РЕЖИМА",
                                                        "СДТУ: СБРОС ДИСТ. РЕЖИМА",
                                                        "СИГНАЛ: АВТ. РЕЖИМ",
                                                        "СИГНАЛ: ДИСТ. РЕЖИМ",                 //104
                                                        "СДТУ: ПРИБАВИТЬ",
                                                        "СДТУ: УБАВИТЬ",
                                                        "ВХОД: БЛОК-КА АВТ. И ДИСТ. РЕЖИМОВ",
                                                        "БЛОК-КА ПО Umin (р+д)",
                                                        "РАБОТА ИО РПН ПО Umax",
                                                     });
            }
        }
    }
}
