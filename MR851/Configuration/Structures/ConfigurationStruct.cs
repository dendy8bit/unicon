﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    [XmlRoot(ElementName = "МР851")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР851"; } set {} }

        [Layout(0)] private SectionStruct _section1;
        [Layout(1)] private SectionStruct _section2;
        [Layout(2)] private AllRelayStruct _relays;
        [Layout(3)] private DisrepairStruct _disrepair;
        [Layout(4)] private RegParamsStruct _regParams;
        [Layout(5)] private InputSignalsStruct _inputSignals;


        [BindingProperty(0)]
        [XmlElement(ElementName = "Секция1")]
        public SectionStruct Section1
        {
            get { return _section1; }
            set { _section1 = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Секция2")]
        public SectionStruct Section2
        {
            get { return _section2; }
            set { _section2 = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Реле")]
        public AllRelayStruct Relays
        {
            get { return _relays; }
            set { _relays = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Реле_неисправности")]
        public DisrepairStruct Disrepair
        {
            get { return _disrepair; }
            set { _disrepair = value; }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Параметры_регулирования")]
        public RegParamsStruct RegParams
        {
            get { return _regParams; }
            set { _regParams = value; }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Входные_сигналы")]
        public InputSignalsStruct InputSignals
        {
            get { return _inputSignals; }
            set { _inputSignals = value; }
        }

        [XmlElement(ElementName = "Текущая_ступень")]
        public string CurrentStage { get; set; }

        [XmlElement(ElementName = "Выраб_ресурс")]
        public string Resourse { get; set; }
    }
}
