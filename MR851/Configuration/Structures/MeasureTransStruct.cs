using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    public class MeasureTransStruct : StructBase
    {
        [Layout(0)] public ushort RomConfigI; //�� ������������
        [Layout(1)] public ushort RomNominalI; //����
        [Layout(2)] public ushort RomNominalIo; //����
        [Layout(3)] public ushort RomRes1; //�� ������������
        [Layout(4)] public ushort RomRes2; //�� ������������
        [Layout(5)] public ushort RomRes3; //�� ������������
        [Layout(6)] public ushort RomRes4; //�� ������������
        [Layout(7)] public ushort RomRes5; //�� ������������
        [Layout(8)] public ushort RomConfigU; //���������  
        [Layout(9)] public ushort RomFactorU; //�� 
        [Layout(10)] public ushort RomErrorsU; //�� ������������
        [Layout(11)] public ushort RomFactorUo; //����
        [Layout(12)] public ushort RomErrorsUo; //�� ������������
        [Layout(13)] public ushort CountStep; //���������� ��������
        [Layout(14)] public ushort StartStep; //��������� �������
        [Layout(15)] public ushort EndStep; //�������� �������
    }
}