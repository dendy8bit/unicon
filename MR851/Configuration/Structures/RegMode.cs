﻿using System;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR851.Configuration.Structures
{
    public class RegMode : StructBase
    {
        [Layout(0)] public UInt16 ROM_CONTROL_MODE;
        [Layout(1)] public UInt16 ROM_STEP_BEG;
        [Layout(2)] public UInt16 ROM_STEP_END;
        [Layout(3)] public UInt16 reserve;
    }
}