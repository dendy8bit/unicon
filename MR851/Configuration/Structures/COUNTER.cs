﻿using System;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR851.Configuration.Structures
{
    public class Counter : StructBase
    {
        [Layout(0)] public UInt16 ROM_COUNT_DIST;
    }
}