﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR851.Configuration.Structures
{
    public class InputSignalsStruct : StructBase
    {
        [Layout(0)] private ushort _romInBlokI;
        [Layout(1)] private ushort _romInBlokT;
        [Layout(2)] private ushort _romInUp3;
        [Layout(3)] private ushort _romInSignalR;
        [Layout(4)] private ushort _romInContrPow;
        [Layout(5)] private ushort _romInBlokR;
        [Layout(6)] private ushort _romFaultDrive;
        [Layout(7)] private ushort _blockAutoSyp;

        #region Входные сигналы

        [BindingProperty(0)]
        [XmlElement(ElementName = "Блок_по_току")]
        public string BlockI
        {
            get { return Validator.Get(this._romInBlokI,Strings.E_SIGNALS); }
            set { this._romInBlokI = Validator.Set(value, Strings.E_SIGNALS); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блок_по_темп")]
        public string BlockT
        {
            get { return Validator.Get(this._romInBlokT,Strings.E_SIGNALS); }
            set { this._romInBlokT = Validator.Set(value, Strings.E_SIGNALS); }
        }
        
        [BindingProperty(2)]
        [XmlElement(ElementName = "Uп3")]
        public string Up3
        {
            get { return Validator.Get(this._romInUp3, Strings.E_SIGNALS);  }
            set { this._romInUp3 = Validator.Set(value, Strings.E_SIGNALS); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Сброс_сигнализ")]
        public string DropSygn
        {
            get { return Validator.Get(this._romInSignalR, Strings.E_SIGNALS);  }
            set { this._romInSignalR = Validator.Set(value, Strings.E_SIGNALS); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Контр_пит_привод")]
        public string KontrPitPrivod
        {
            get { return Validator.Get(this._romInContrPow, Strings.E_SIGNALS);  }
            set { this._romInContrPow = Validator.Set(value, Strings.E_SIGNALS); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Сброс_блокиров")]
        public string DropBlock
        {
            get { return Validator.Get(this._romInBlokR, Strings.E_SIGNALS);  }
            set { this._romInBlokR = Validator.Set(value, Strings.E_SIGNALS); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Фиксация блокировки привод не пошел")]
        public bool FixNotGo
        {
            get { return Common.GetBit(_romFaultDrive, 0); }
            set { this._romFaultDrive = Common.SetBit(_romFaultDrive, 0, value); }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Фиксация блокировки привод застрял")]
        public bool FixStick
        {
            get { return Common.GetBit(_romFaultDrive, 1); }
            set { this._romFaultDrive = Common.SetBit(_romFaultDrive, 1, value); }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Фиксация блокировки привод побежал")]
        public bool FixRun
        {
            get { return Common.GetBit(_romFaultDrive, 2); }
            set { this._romFaultDrive = Common.SetBit(_romFaultDrive, 2, value); }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "Действие на отключение привод не пошел")]
        public bool OffNotGo
        {
            get { return Common.GetBit(_romFaultDrive, 3); }
            set { this._romFaultDrive = Common.SetBit(_romFaultDrive, 3, value); }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Действие на отключение привод застрял")]
        public bool OffStick
        {
            get { return Common.GetBit(_romFaultDrive, 4); }
            set { this._romFaultDrive = Common.SetBit(_romFaultDrive, 4, value); }
        }
        [BindingProperty(11)]
        [XmlElement(ElementName = "Действие на отключение привод побежал")]
        public bool OffRun
        {
            get { return Common.GetBit(_romFaultDrive, 5); }
            set { this._romFaultDrive = Common.SetBit(_romFaultDrive, 5, value); }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "Блокировка_автомат._и_дист._режимов")]
        public string BlockAutoSyp
        {
            get { return Validator.Get(this._blockAutoSyp, Strings.E_SIGNALS); }
            set { this._blockAutoSyp = Validator.Set(value, Strings.E_SIGNALS); }
        }
        #endregion
    }
}