﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    public class RegModeStruct : StructBase
    {
        [Layout(0)] public ushort RomControlMode;
        [Layout(1)] public ushort ResistStep;
        [Layout(2)] public ushort RomStepBeg;
        [Layout(3)] public ushort RomStepEnd;
    }
}