﻿using System;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR851.Configuration.Structures
{
    public class ImpMode : StructBase
    {
        [Layout(0)]
        public UInt16 ROM_CONTROL_Tr;
        [Layout(1)]
        public UInt16 ROM_CONTROL_Tp;
        [Layout(2)]
        public UInt16 ROM_CONTROL_Tz;
    }
}