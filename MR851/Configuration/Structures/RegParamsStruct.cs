﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR851.Configuration.Structures
{
    public class RegParamsStruct : StructBase
    {
        [Layout(0)] private RegModeStruct _regModeStruct;
        [Layout(1)] private CommandsStruct _commandsStruct;
        [Layout(2)] private ImpModeStruct _impModeStruct;
        [Layout(3)] private ushort _counter;



        #region Параметры режима регулирования

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим_регулиров")]
        public string RegMode
        {
            get { return Validator.Get(this._regModeStruct.RomControlMode, Strings.REG_MODE, 0); }
            set
            {
                this._regModeStruct.RomControlMode = Validator.Set(value, Strings.REG_MODE,this._regModeStruct.RomControlMode, 0);
            }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Переход_в_автом_режим")]
        public string AutoMode
        {
            get { return Validator.Get(this._regModeStruct.RomControlMode, Strings.AUTO_MODE, 1); }
            set
            {
                this._regModeStruct.RomControlMode = Validator.Set(value, Strings.AUTO_MODE,
                                                                  this._regModeStruct.RomControlMode, 1);
            }

        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Управл_ключ")]
        public string Key
        {
            get { return Validator.Get(this._regModeStruct.RomControlMode, Strings.YES_NO, 2); }
            set
            {
                this._regModeStruct.RomControlMode = Validator.Set(value, Strings.YES_NO,
                                                                  this._regModeStruct.RomControlMode, 2);
            }

        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Управл_пульт")]
        public string Pult
        {
            get { return Validator.Get(this._regModeStruct.RomControlMode, Strings.YES_NO, 3); }
            set
            {
                this._regModeStruct.RomControlMode = Validator.Set(value, Strings.YES_NO,
                                                                  this._regModeStruct.RomControlMode, 3);
            }

        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Ручной_режим")]
        public string HandMode
        {
            get { return Validator.Get(this._regModeStruct.RomControlMode, Strings.HAND_MODE, 4); }
            set
            {
                this._regModeStruct.RomControlMode = Validator.Set(value, Strings.HAND_MODE,
                                                                  this._regModeStruct.RomControlMode, 4);
            }
        }
        
        [BindingProperty(5)]
        [XmlElement(ElementName = "Управление_по_СДТУ")]
        public string ControlSdtu
        {
            get { return Validator.Get(this._regModeStruct.RomControlMode, Strings.YES_NO, 5); }
            set
            {
                this._regModeStruct.RomControlMode = Validator.Set(value, Strings.YES_NO,
                    this._regModeStruct.RomControlMode, 5);
            }
        }
        #endregion

        #region Задержки команд управления
        [BindingProperty(6)]
        [XmlElement(ElementName = "Задержка_перегрузки_по_току")]
        public int Tcp
        {
            get { return ValuesConverterCommon.GetWaitTime(this._regModeStruct.RomStepBeg); }
            set { this._regModeStruct.RomStepBeg = ValuesConverterCommon.SetWaitTime(value); }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Задержка_отключения_питания_привода")]
        public int Tot
        {
            get { return ValuesConverterCommon.GetWaitTime(this._regModeStruct.RomStepEnd); }
            set { this._regModeStruct.RomStepEnd = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Задержка_1_ком")]
        public int Tk1
        {
            get { return ValuesConverterCommon.GetWaitTime(this._commandsStruct.RomControlT1) ; }
            set { this._commandsStruct.RomControlT1 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Задержка_повторной_ком")]
        public int Tk2
        {
            get { return ValuesConverterCommon.GetWaitTime(this._commandsStruct.RomControlT2); }
            set { this._commandsStruct.RomControlT2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Задержка_ком_перенапряжения")]
        public int Tkp
        {
            get { return ValuesConverterCommon.GetWaitTime(this._commandsStruct.RomControlT3); ; }
            set { this._commandsStruct.RomControlT3 = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion

        #region Имп режима управления

        [BindingProperty(11)]
        [XmlElement(ElementName = "Время_реакции")]
        public int Tr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._impModeStruct.RomControlTr); }
            set { this._impModeStruct.RomControlTr = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Время_завершения")]
        public int Tp
        {
            get { return ValuesConverterCommon.GetWaitTime(this._impModeStruct.RomControlTp); }
            set { this._impModeStruct.RomControlTp = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Задержка_снятия_сигнала_управления")]
        public int Tz
        {
            get { return ValuesConverterCommon.GetWaitTime(this._impModeStruct.RomControlTz); }
            set { this._impModeStruct.RomControlTz = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion

        #region Счетчик переключений

        [BindingProperty(14)]
        [XmlElement(ElementName = "Направл_счета")]
        public string Counter
        {
            get { return Validator.Get(_counter, Strings.COUNTER,0); }
            set { _counter = Validator.Set(value, Strings.COUNTER, _counter, 0); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Логометр")]
        public bool Log
        {
            get { return Common.GetBit(_counter, 1); }
            set { _counter = Common.SetBit(_counter, 1, value); }
        }

        [BindingProperty(16), XmlElement(ElementName = "Rступени")]
        public ushort Rstep
        {
            get { return _regModeStruct.ResistStep; }
            set { _regModeStruct.ResistStep = value; }
        }
        #endregion
        
    }
}