﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR851.Configuration.Structures
{
    public class AllRelayStruct : StructBase, IDgvRowsContainer<RelayStruct>
    {
        [Layout(0, Count = 13)]
        private RelayStruct[] Relays;

        public RelayStruct[] Rows
        {
            get { return Relays; }
            set { Relays = value; }
        }
    }
}
