﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    public class CommandsStruct : StructBase
    {
        [Layout(0)] public ushort RomControlT1;
        [Layout(1)] public ushort RomControlT2;
        [Layout(2)] public ushort RomControlT3;
    }
}