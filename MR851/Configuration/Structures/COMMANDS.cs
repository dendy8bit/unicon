﻿using System;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR851.Configuration.Structures
{
    public class Commands : StructBase
    {
        [Layout(0)]
        public UInt16 ROM_CONTROL_T1;
        [Layout(1)]
        public UInt16 ROM_CONTROL_T2;
        [Layout(2)]
        public UInt16 ROM_CONTROL_T3;
    }
}