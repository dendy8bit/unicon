﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR851.Configuration.Structures
{
    public class RelayStruct : StructBase
    {
        [Layout(0)]
        private ushort Signal;
        [Layout(1)]
        private ushort Wait;

        #region Реле
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string ReleType
        {
            get { return Validator.Get(this.Signal, Strings.RELE_TYPE, 15); }
            set { this.Signal = Validator.Set(value, Strings.RELE_TYPE, this.Signal, 15); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string ReleSygnal
        {
            get { return Validator.Get(this.Signal, Strings.RELE_SIGNALS, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this.Signal = Validator.Set(value, Strings.RELE_SIGNALS, this.Signal, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        

        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Время")]
        public int ReleWait
        {
            get { return ValuesConverterCommon.GetWaitTime(this.Wait); }
            set { this.Wait = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion
    }
}