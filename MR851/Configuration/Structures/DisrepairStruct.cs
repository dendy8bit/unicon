﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR851.Configuration.Structures
{
    public class DisrepairStruct : StructBase
    {
        [Layout(0)] private ushort Disrepair; //реле неисправность
        [Layout(1)] private ushort DisrepairImp; //импульс реле неисправность
        [Layout(2, Count = 4)] private ushort[] Reserve1;


        #region Неисправности

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Неисправность1")]
        public bool OutputN1
        {
            get { return Common.GetBit(this.Disrepair, 0); }
            set { this.Disrepair = Common.SetBit(this.Disrepair, 0, value); }
        }

        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Неисправность2")]
        public bool OutputN2
        {
            get { return Common.GetBit(this.Disrepair, 1); }
            set { this.Disrepair = Common.SetBit(this.Disrepair, 1, value); }
        }

        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Неисправность3")]
        public bool OutputN3
        {
            get { return Common.GetBit(this.Disrepair, 2); }
            set { this.Disrepair = Common.SetBit(this.Disrepair, 2, value); }
        }

        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "Импульс_реле_неисправность")]
        public int OutputImp
        {
            get { return ValuesConverterCommon.GetWaitTime(this.DisrepairImp); }
            set { this.DisrepairImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        #endregion
    }
}
