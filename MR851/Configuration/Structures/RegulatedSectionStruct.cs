using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    public class RegulatedSectionStruct : StructBase
    {
        [Layout(0)] public ushort RomUp; //���������� �����������	2 �����	
        [Layout(1)] public ushort RomUp1; //���������� �����������1	2 �����	
        [Layout(2)] public ushort RomUp2; //���������� �����������2	2 �����	
        [Layout(3)] public ushort RomUp3; //���������� �����������3	2 �����	
        [Layout(4)] public ushort Rom_DU; //���������� ���� ����������.	2 �����	
        [Layout(5)] public ushort RomUmin; //����������� ���������� 	2 �����	
        [Layout(6)] public ushort RomUmax; //������������ ���������� 	2 �����	
        [Layout(7)] public ushort RomUn; //���������� ���������� 	2 �����	
        [Layout(8)] public ushort RomImax; //������������ ���	 	2 �����	
        [Layout(9)] public ushort RomUk; //�����. ������� �����������	2 �����		
        [Layout(10)] public ushort RomUkmax; //����������� ������� �������.	2 �����	
        [Layout(11)] public ushort RomUminrd; //����������� ���������� ������� � �������������� ����������	2 �����	
    }
}