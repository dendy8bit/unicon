﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    public class ImpModeStruct : StructBase
    {
        [Layout(0)] public ushort RomControlTr;
        [Layout(1)] public ushort RomControlTp;
        [Layout(2)] public ushort RomControlTz;
    }
}