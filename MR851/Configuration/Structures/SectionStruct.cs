﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR851.Configuration.Structures
{
    public class SectionStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private  MeasureTransStruct _romIzmer;
        [Layout(1)] private RegulatedSectionStruct _romRegul;
        [Layout(2, Count = 4)] private ushort[] _reserve; 
        #endregion [Private fields]


        #region Properties

        [BindingProperty(0)]
        [XmlElement(ElementName = "Измерение")]
        public string Measure
        {
            get { return Validator.Get(this._romIzmer.RomConfigU, Strings.MEASURE); }
            set { this._romIzmer.RomConfigU = Validator.Set(value, Strings.MEASURE); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ТН")]
        public double Tn
        {
            get { return ValuesConverterCommon.GetKth(this._romIzmer.RomFactorU); }
            set { this._romIzmer.RomFactorU = ValuesConverterCommon.SetKth(value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "ТН_коэфф")]
        public string TnKoeff
        {
            get { return Validator.Get(this._romIzmer.RomFactorU, Strings.KOEFF, 15); }
            set { this._romIzmer.RomFactorU = Validator.Set(value, Strings.KOEFF, this._romIzmer.RomFactorU, 15); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "ТННП")]
        public double Tnnp
        {
            get { return ValuesConverterCommon.GetKth(this._romIzmer.RomFactorUo); }
            set { this._romIzmer.RomFactorUo = ValuesConverterCommon.SetKth(value); }

        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "ТННП_коэфф")]
        public string TnnpKoeff
        {
            get { return Validator.Get(this._romIzmer.RomFactorUo, Strings.KOEFF, 15); }
            set { this._romIzmer.RomFactorUo = Validator.Set(value, Strings.KOEFF, this._romIzmer.RomFactorUo, 15); }

        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "BB")]
        public ushort Ttbb
        {
            get { return this._romIzmer.RomNominalI; }
            set { this._romIzmer.RomNominalI = value; }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "CB")]
        public ushort Ttcb
        {
            get { return this._romIzmer.RomNominalIo; }
            set { this._romIzmer.RomNominalIo = value; }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Up")]
        public double Up
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUp); }
            set { this._romRegul.RomUp = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Up1")]
        public double Up1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUp1); }
            set { this._romRegul.RomUp1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Up2")]
        public double Up2
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUp2); }
            set { this._romRegul.RomUp2 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Up3")]
        public double Up3
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUp3); }
            set { this._romRegul.RomUp3 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "dU")]
        public double Du
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.Rom_DU); }
            set { this._romRegul.Rom_DU = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Umin")]
        public double Umin
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUmin); }
            set { this._romRegul.RomUmin = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "Umax")]
        public double Umax
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUmax); }
            set { this._romRegul.RomUmax = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Un")]
        public double Un
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUn); }
            set { this._romRegul.RomUn = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Imax")]
        public double Imax
        {
            get { return ValuesConverterCommon.GetUstavka5(this._romRegul.RomImax); }
            set { this._romRegul.RomImax = ValuesConverterCommon.SetUstavka5(value); }
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "Uk")]
        public double Uk
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUk); }
            set { this._romRegul.RomUk = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "Ukm")]
        public double Ukm
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUkmax); }
            set { this._romRegul.RomUkmax = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(18)]
        [XmlElement(ElementName = "Umin(р+д)")]
        public double Uminrd
        {
            get { return ValuesConverterCommon.GetUstavka256(this._romRegul.RomUminrd); }
            set { this._romRegul.RomUminrd = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "Количество_ступеней")]
        public ushort CountStep
        {
            get { return this._romIzmer.CountStep; }
            set { this._romIzmer.CountStep = value; }
        }

        [BindingProperty(20)]
        [XmlElement(ElementName = "Начальная_ступень")]
        public ushort StartStep
        {
            get { return this._romIzmer.StartStep; }
            set { this._romIzmer.StartStep = value; }
        }
        [BindingProperty(21)]
        [XmlElement(ElementName = "Конечная_ступень")]
        public ushort EndStep
        {
            get { return this._romIzmer.EndStep; }
            set { this._romIzmer.EndStep = value; }
        }

        public MeasureTransStruct RomIzmer
        {
            get { return this._romIzmer; }
        }

        #endregion

    }
}