﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Configuration.Structures
{
    public class OutputSignalsStruct : StructBase
    {
        [Layout(0)] public AllRelayStruct RomRelay;
    }
}