﻿namespace BEMN.MR851.Configuration
{
    partial class Mr851ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mr851ConfigurationForm));
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._S1Umax = new System.Windows.Forms.MaskedTextBox();
            this._S1Ukm = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._S1Uk = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this._S1Imax = new System.Windows.Forms.MaskedTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this._S1Un = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._uMinrdS1 = new System.Windows.Forms.MaskedTextBox();
            this._uMinrdLabelS1 = new System.Windows.Forms.Label();
            this._S1Umin = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._S1dU = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._S1Up3 = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._S1Up2 = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this._S1Up1 = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._S1Up = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._S1TTBB = new System.Windows.Forms.MaskedTextBox();
            this._S1TTCB = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._S1Ktnnp = new System.Windows.Forms.ComboBox();
            this._S1TNNP = new System.Windows.Forms.MaskedTextBox();
            this._S1Ktn = new System.Windows.Forms.ComboBox();
            this._S1MeasureCB = new System.Windows.Forms.ComboBox();
            this._S1TN = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._S2Umax = new System.Windows.Forms.MaskedTextBox();
            this._S2Ukm = new System.Windows.Forms.MaskedTextBox();
            this.label42 = new System.Windows.Forms.Label();
            this._S2Uk = new System.Windows.Forms.MaskedTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this._S2Imax = new System.Windows.Forms.MaskedTextBox();
            this.label45 = new System.Windows.Forms.Label();
            this._S2Un = new System.Windows.Forms.MaskedTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this._uMinrdS2 = new System.Windows.Forms.MaskedTextBox();
            this._uMinrdLabelS2 = new System.Windows.Forms.Label();
            this._S2Umin = new System.Windows.Forms.MaskedTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this._S2dU = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this._S2Up3 = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this._S2Up2 = new System.Windows.Forms.MaskedTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this._S2Up1 = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this._S2Up = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._S2TTBB = new System.Windows.Forms.MaskedTextBox();
            this._S2TTCB = new System.Windows.Forms.MaskedTextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this._S2Ktnnp = new System.Windows.Forms.ComboBox();
            this._S2TNNP = new System.Windows.Forms.MaskedTextBox();
            this._S2Ktn = new System.Windows.Forms.ComboBox();
            this._S2MeasureCB = new System.Windows.Forms.ComboBox();
            this._S2TN = new System.Windows.Forms.MaskedTextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._blockAutoSyp = new System.Windows.Forms.ComboBox();
            this._blockAutoSypLabel2 = new System.Windows.Forms.Label();
            this._blockAutoSypLabel1 = new System.Windows.Forms.Label();
            this._dropBlock = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this._kontrPitPrivod = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this._dropSign = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this._Up3 = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this._blockT = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this._blockI = new System.Windows.Forms.ComboBox();
            this.label63 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._neispr3CB = new System.Windows.Forms.CheckBox();
            this._neispr2CB = new System.Windows.Forms.CheckBox();
            this._neispr1CB = new System.Windows.Forms.CheckBox();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._faultsGrBox = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._fixStickCheck = new System.Windows.Forms.CheckBox();
            this._fixRunCheck = new System.Windows.Forms.CheckBox();
            this._fixNotGoCheck = new System.Windows.Forms.CheckBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._offStickCheck = new System.Windows.Forms.CheckBox();
            this._offRunCheck = new System.Windows.Forms.CheckBox();
            this._offNotGoCheck = new System.Windows.Forms.CheckBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._logCheck = new System.Windows.Forms.CheckBox();
            this._writeResource = new System.Windows.Forms.Button();
            this._writeStupen = new System.Windows.Forms.Button();
            this._resource = new System.Windows.Forms.MaskedTextBox();
            this.label71 = new System.Windows.Forms.Label();
            this._startStep = new System.Windows.Forms.MaskedTextBox();
            this._rStep = new System.Windows.Forms.MaskedTextBox();
            this._countSteps = new System.Windows.Forms.MaskedTextBox();
            this._endStep = new System.Windows.Forms.MaskedTextBox();
            this._stupen = new System.Windows.Forms.MaskedTextBox();
            this._counter = new System.Windows.Forms.ComboBox();
            this.endStepLabel = new System.Windows.Forms.Label();
            this.startStepLabel = new System.Windows.Forms.Label();
            this.rStepLabel = new System.Windows.Forms.Label();
            this.countStepLabel = new System.Windows.Forms.Label();
            this.logLabel = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label96 = new System.Windows.Forms.Label();
            this._timeSnSig = new System.Windows.Forms.MaskedTextBox();
            this.label74 = new System.Windows.Forms.Label();
            this._timeEnd = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this._timeStart = new System.Windows.Forms.MaskedTextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.timeLabel4 = new System.Windows.Forms.Label();
            this.timeLabel2 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this._time2Com = new System.Windows.Forms.MaskedTextBox();
            this._time1Com = new System.Windows.Forms.MaskedTextBox();
            this._time5Com = new System.Windows.Forms.MaskedTextBox();
            this.timeLabel3 = new System.Windows.Forms.Label();
            this.timeLabel1 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this._time4Com = new System.Windows.Forms.MaskedTextBox();
            this.label87 = new System.Windows.Forms.Label();
            this._time3Com = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label97 = new System.Windows.Forms.Label();
            this._controlSdtu = new System.Windows.Forms.ComboBox();
            this._controlSdtuLabel = new System.Windows.Forms.Label();
            this._handMode = new System.Windows.Forms.ComboBox();
            this.label93 = new System.Windows.Forms.Label();
            this._pilt = new System.Windows.Forms.ComboBox();
            this.label92 = new System.Windows.Forms.Label();
            this._key = new System.Windows.Forms.ComboBox();
            this.label91 = new System.Windows.Forms.Label();
            this._autoMode = new System.Windows.Forms.ComboBox();
            this.label90 = new System.Windows.Forms.Label();
            this._regMode = new System.Windows.Forms.ComboBox();
            this.label89 = new System.Windows.Forms.Label();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this.tabPage5.SuspendLayout();
            this._faultsGrBox.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _toolTip
            // 
            this._toolTip.ShowAlways = true;
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.ContextMenuStrip = this.contextMenu;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(639, 438);
            this.tabControl1.TabIndex = 4;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(631, 412);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Секция 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this._S1Umax);
            this.groupBox2.Controls.Add(this._S1Ukm);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this._S1Uk);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this._S1Imax);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this._S1Un);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this._uMinrdS1);
            this.groupBox2.Controls.Add(this._uMinrdLabelS1);
            this.groupBox2.Controls.Add(this._S1Umin);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this._S1dU);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this._S1Up3);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this._S1Up2);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this._S1Up1);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this._S1Up);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(6, 198);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(408, 204);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры регулируемой секции";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 185);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(240, 13);
            this.label23.TabIndex = 32;
            this.label23.Text = "Un при конфигурации  \"Измерение Uab + Un\"";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 170);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(253, 13);
            this.label24.TabIndex = 31;
            this.label24.Text = "* U2 при конфигурации \"Измерение Uab + Ubc\",";
            // 
            // _S1Umax
            // 
            this._S1Umax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Umax.Location = new System.Drawing.Point(294, 19);
            this._S1Umax.Name = "_S1Umax";
            this._S1Umax.Size = new System.Drawing.Size(88, 20);
            this._S1Umax.TabIndex = 28;
            this._S1Umax.Tag = "65535";
            this._S1Umax.Text = "0";
            this._S1Umax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S1Ukm
            // 
            this._S1Ukm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Ukm.Location = new System.Drawing.Point(294, 99);
            this._S1Ukm.Name = "_S1Ukm";
            this._S1Ukm.Size = new System.Drawing.Size(88, 20);
            this._S1Ukm.TabIndex = 27;
            this._S1Ukm.Tag = "65535";
            this._S1Ukm.Text = "0";
            this._S1Ukm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(233, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Uk m, B";
            // 
            // _S1Uk
            // 
            this._S1Uk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Uk.Location = new System.Drawing.Point(294, 79);
            this._S1Uk.Name = "_S1Uk";
            this._S1Uk.Size = new System.Drawing.Size(88, 20);
            this._S1Uk.TabIndex = 25;
            this._S1Uk.Tag = "65535";
            this._S1Uk.Text = "0";
            this._S1Uk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(233, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Uk, B";
            // 
            // _S1Imax
            // 
            this._S1Imax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Imax.Location = new System.Drawing.Point(294, 59);
            this._S1Imax.Name = "_S1Imax";
            this._S1Imax.Size = new System.Drawing.Size(88, 20);
            this._S1Imax.TabIndex = 23;
            this._S1Imax.Tag = "65535";
            this._S1Imax.Text = "0";
            this._S1Imax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(233, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Imax, Iн";
            // 
            // _S1Un
            // 
            this._S1Un.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Un.Location = new System.Drawing.Point(294, 39);
            this._S1Un.Name = "_S1Un";
            this._S1Un.Size = new System.Drawing.Size(88, 20);
            this._S1Un.TabIndex = 21;
            this._S1Un.Tag = "65535";
            this._S1Un.Text = "0";
            this._S1Un.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(233, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "U2 (Un)*, B";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(233, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Umax, B";
            // 
            // _uMinrdS1
            // 
            this._uMinrdS1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._uMinrdS1.Location = new System.Drawing.Point(86, 139);
            this._uMinrdS1.Name = "_uMinrdS1";
            this._uMinrdS1.Size = new System.Drawing.Size(88, 20);
            this._uMinrdS1.TabIndex = 17;
            this._uMinrdS1.Tag = "65535";
            this._uMinrdS1.Text = "0";
            this._uMinrdS1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._uMinrdS1.Visible = false;
            // 
            // _uMinrdLabelS1
            // 
            this._uMinrdLabelS1.AutoSize = true;
            this._uMinrdLabelS1.Location = new System.Drawing.Point(6, 141);
            this._uMinrdLabelS1.Name = "_uMinrdLabelS1";
            this._uMinrdLabelS1.Size = new System.Drawing.Size(71, 13);
            this._uMinrdLabelS1.TabIndex = 16;
            this._uMinrdLabelS1.Text = "Umin (р+д), B";
            this._uMinrdLabelS1.Visible = false;
            // 
            // _S1Umin
            // 
            this._S1Umin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Umin.Location = new System.Drawing.Point(86, 119);
            this._S1Umin.Name = "_S1Umin";
            this._S1Umin.Size = new System.Drawing.Size(88, 20);
            this._S1Umin.TabIndex = 17;
            this._S1Umin.Tag = "65535";
            this._S1Umin.Text = "0";
            this._S1Umin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 121);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Umin, B";
            // 
            // _S1dU
            // 
            this._S1dU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1dU.Location = new System.Drawing.Point(86, 99);
            this._S1dU.Name = "_S1dU";
            this._S1dU.Size = new System.Drawing.Size(88, 20);
            this._S1dU.TabIndex = 15;
            this._S1dU.Tag = "65535";
            this._S1dU.Text = "0";
            this._S1dU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "dU, B";
            // 
            // _S1Up3
            // 
            this._S1Up3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Up3.Location = new System.Drawing.Point(86, 79);
            this._S1Up3.Name = "_S1Up3";
            this._S1Up3.Size = new System.Drawing.Size(88, 20);
            this._S1Up3.TabIndex = 13;
            this._S1Up3.Tag = "65535";
            this._S1Up3.Text = "0";
            this._S1Up3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Uп3, B";
            // 
            // _S1Up2
            // 
            this._S1Up2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Up2.Location = new System.Drawing.Point(86, 59);
            this._S1Up2.Name = "_S1Up2";
            this._S1Up2.Size = new System.Drawing.Size(88, 20);
            this._S1Up2.TabIndex = 11;
            this._S1Up2.Tag = "65535";
            this._S1Up2.Text = "0";
            this._S1Up2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Uп2, B";
            // 
            // _S1Up1
            // 
            this._S1Up1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Up1.Location = new System.Drawing.Point(86, 39);
            this._S1Up1.Name = "_S1Up1";
            this._S1Up1.Size = new System.Drawing.Size(88, 20);
            this._S1Up1.TabIndex = 9;
            this._S1Up1.Tag = "65535";
            this._S1Up1.Text = "0";
            this._S1Up1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Uп1, B";
            // 
            // _S1Up
            // 
            this._S1Up.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1Up.Location = new System.Drawing.Point(86, 19);
            this._S1Up.Name = "_S1Up";
            this._S1Up.Size = new System.Drawing.Size(88, 20);
            this._S1Up.TabIndex = 7;
            this._S1Up.Tag = "65535";
            this._S1Up.Text = "0";
            this._S1Up.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Uп, B";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(7, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 186);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры измерений";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._S1TTBB);
            this.groupBox4.Controls.Add(this._S1TTCB);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(235, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(162, 86);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Номинальный ток ТТ";
            // 
            // _S1TTBB
            // 
            this._S1TTBB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1TTBB.Location = new System.Drawing.Point(59, 22);
            this._S1TTBB.Name = "_S1TTBB";
            this._S1TTBB.Size = new System.Drawing.Size(88, 20);
            this._S1TTBB.TabIndex = 7;
            this._S1TTBB.Tag = "65535";
            this._S1TTBB.Text = "0";
            this._S1TTBB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S1TTCB
            // 
            this._S1TTCB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1TTCB.Location = new System.Drawing.Point(59, 45);
            this._S1TTCB.Name = "_S1TTCB";
            this._S1TTCB.Size = new System.Drawing.Size(88, 20);
            this._S1TTCB.TabIndex = 6;
            this._S1TTCB.Tag = "65535";
            this._S1TTCB.Text = "0";
            this._S1TTCB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "CВ, A";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "ВВ, A";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this._S1Ktnnp);
            this.groupBox3.Controls.Add(this._S1TNNP);
            this.groupBox3.Controls.Add(this._S1Ktn);
            this.groupBox3.Controls.Add(this._S1MeasureCB);
            this.groupBox3.Controls.Add(this._S1TN);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(223, 161);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры напряжения";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(98, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "К * ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(98, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "К * ";
            // 
            // _S1Ktnnp
            // 
            this._S1Ktnnp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._S1Ktnnp.FormattingEnabled = true;
            this._S1Ktnnp.Location = new System.Drawing.Point(128, 106);
            this._S1Ktnnp.Name = "_S1Ktnnp";
            this._S1Ktnnp.Size = new System.Drawing.Size(89, 21);
            this._S1Ktnnp.TabIndex = 23;
            // 
            // _S1TNNP
            // 
            this._S1TNNP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1TNNP.Location = new System.Drawing.Point(101, 86);
            this._S1TNNP.Name = "_S1TNNP";
            this._S1TNNP.Size = new System.Drawing.Size(116, 20);
            this._S1TNNP.TabIndex = 22;
            this._S1TNNP.Tag = "65535";
            this._S1TNNP.Text = "0";
            this._S1TNNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S1Ktn
            // 
            this._S1Ktn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._S1Ktn.FormattingEnabled = true;
            this._S1Ktn.Location = new System.Drawing.Point(128, 65);
            this._S1Ktn.Name = "_S1Ktn";
            this._S1Ktn.Size = new System.Drawing.Size(89, 21);
            this._S1Ktn.TabIndex = 21;
            // 
            // _S1MeasureCB
            // 
            this._S1MeasureCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._S1MeasureCB.FormattingEnabled = true;
            this._S1MeasureCB.Location = new System.Drawing.Point(101, 24);
            this._S1MeasureCB.Name = "_S1MeasureCB";
            this._S1MeasureCB.Size = new System.Drawing.Size(116, 21);
            this._S1MeasureCB.TabIndex = 20;
            this._S1MeasureCB.SelectedValueChanged += new System.EventHandler(this._S1MeasureCB_SelectedValueChanged);
            // 
            // _S1TN
            // 
            this._S1TN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S1TN.Location = new System.Drawing.Point(101, 45);
            this._S1TN.Name = "_S1TN";
            this._S1TN.Size = new System.Drawing.Size(116, 20);
            this._S1TN.TabIndex = 5;
            this._S1TN.Tag = "65535";
            this._S1TN.Text = "0";
            this._S1TN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ктннп";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "ТННП коэфф.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "ТН коэфф.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ктн";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Измерение";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(631, 412);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Секция 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._S2Umax);
            this.groupBox5.Controls.Add(this._S2Ukm);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this._S2Uk);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this._S2Imax);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this._S2Un);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this._uMinrdS2);
            this.groupBox5.Controls.Add(this._uMinrdLabelS2);
            this.groupBox5.Controls.Add(this._S2Umin);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this._S2dU);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this._S2Up3);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this._S2Up2);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this._S2Up1);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this._S2Up);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Location = new System.Drawing.Point(6, 198);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(408, 186);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Параметры регулируемой секции";
            // 
            // _S2Umax
            // 
            this._S2Umax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Umax.Location = new System.Drawing.Point(284, 19);
            this._S2Umax.Name = "_S2Umax";
            this._S2Umax.Size = new System.Drawing.Size(88, 20);
            this._S2Umax.TabIndex = 28;
            this._S2Umax.Tag = "65535";
            this._S2Umax.Text = "0";
            this._S2Umax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S2Ukm
            // 
            this._S2Ukm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Ukm.Location = new System.Drawing.Point(284, 99);
            this._S2Ukm.Name = "_S2Ukm";
            this._S2Ukm.Size = new System.Drawing.Size(88, 20);
            this._S2Ukm.TabIndex = 27;
            this._S2Ukm.Tag = "65535";
            this._S2Ukm.Text = "0";
            this._S2Ukm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(233, 101);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(45, 13);
            this.label42.TabIndex = 26;
            this.label42.Text = "Uk m, B";
            // 
            // _S2Uk
            // 
            this._S2Uk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Uk.Location = new System.Drawing.Point(284, 79);
            this._S2Uk.Name = "_S2Uk";
            this._S2Uk.Size = new System.Drawing.Size(88, 20);
            this._S2Uk.TabIndex = 25;
            this._S2Uk.Tag = "65535";
            this._S2Uk.Text = "0";
            this._S2Uk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(233, 81);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(34, 13);
            this.label43.TabIndex = 24;
            this.label43.Text = "Uk, B";
            // 
            // _S2Imax
            // 
            this._S2Imax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Imax.Location = new System.Drawing.Point(284, 59);
            this._S2Imax.Name = "_S2Imax";
            this._S2Imax.Size = new System.Drawing.Size(88, 20);
            this._S2Imax.TabIndex = 23;
            this._S2Imax.Tag = "65535";
            this._S2Imax.Text = "0";
            this._S2Imax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(233, 61);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(44, 13);
            this.label45.TabIndex = 22;
            this.label45.Text = "Imax, Iн";
            // 
            // _S2Un
            // 
            this._S2Un.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Un.Location = new System.Drawing.Point(284, 39);
            this._S2Un.Name = "_S2Un";
            this._S2Un.Size = new System.Drawing.Size(88, 20);
            this._S2Un.TabIndex = 21;
            this._S2Un.Tag = "65535";
            this._S2Un.Text = "0";
            this._S2Un.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(233, 41);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(34, 13);
            this.label46.TabIndex = 20;
            this.label46.Text = "Un, B";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(233, 21);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(47, 13);
            this.label47.TabIndex = 18;
            this.label47.Text = "Umax, B";
            // 
            // _uMinrdS2
            // 
            this._uMinrdS2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._uMinrdS2.Location = new System.Drawing.Point(86, 139);
            this._uMinrdS2.Name = "_uMinrdS2";
            this._uMinrdS2.Size = new System.Drawing.Size(88, 20);
            this._uMinrdS2.TabIndex = 17;
            this._uMinrdS2.Tag = "65535";
            this._uMinrdS2.Text = "0";
            this._uMinrdS2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._uMinrdS2.Visible = false;
            // 
            // _uMinrdLabelS2
            // 
            this._uMinrdLabelS2.AutoSize = true;
            this._uMinrdLabelS2.Location = new System.Drawing.Point(6, 141);
            this._uMinrdLabelS2.Name = "_uMinrdLabelS2";
            this._uMinrdLabelS2.Size = new System.Drawing.Size(71, 13);
            this._uMinrdLabelS2.TabIndex = 16;
            this._uMinrdLabelS2.Text = "Umin (р+д), B";
            this._uMinrdLabelS2.Visible = false;
            // 
            // _S2Umin
            // 
            this._S2Umin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Umin.Location = new System.Drawing.Point(86, 119);
            this._S2Umin.Name = "_S2Umin";
            this._S2Umin.Size = new System.Drawing.Size(88, 20);
            this._S2Umin.TabIndex = 17;
            this._S2Umin.Tag = "65535";
            this._S2Umin.Text = "0";
            this._S2Umin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 121);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(44, 13);
            this.label48.TabIndex = 16;
            this.label48.Text = "Umin, B";
            // 
            // _S2dU
            // 
            this._S2dU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2dU.Location = new System.Drawing.Point(86, 99);
            this._S2dU.Name = "_S2dU";
            this._S2dU.Size = new System.Drawing.Size(88, 20);
            this._S2dU.TabIndex = 15;
            this._S2dU.Tag = "65535";
            this._S2dU.Text = "0";
            this._S2dU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 101);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 14;
            this.label49.Text = "dU, B";
            // 
            // _S2Up3
            // 
            this._S2Up3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Up3.Location = new System.Drawing.Point(86, 79);
            this._S2Up3.Name = "_S2Up3";
            this._S2Up3.Size = new System.Drawing.Size(88, 20);
            this._S2Up3.TabIndex = 13;
            this._S2Up3.Tag = "65535";
            this._S2Up3.Text = "0";
            this._S2Up3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 81);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(40, 13);
            this.label50.TabIndex = 12;
            this.label50.Text = "Uп3, B";
            // 
            // _S2Up2
            // 
            this._S2Up2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Up2.Location = new System.Drawing.Point(86, 59);
            this._S2Up2.Name = "_S2Up2";
            this._S2Up2.Size = new System.Drawing.Size(88, 20);
            this._S2Up2.TabIndex = 11;
            this._S2Up2.Tag = "65535";
            this._S2Up2.Text = "0";
            this._S2Up2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 61);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(40, 13);
            this.label51.TabIndex = 10;
            this.label51.Text = "Uп2, B";
            // 
            // _S2Up1
            // 
            this._S2Up1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Up1.Location = new System.Drawing.Point(86, 39);
            this._S2Up1.Name = "_S2Up1";
            this._S2Up1.Size = new System.Drawing.Size(88, 20);
            this._S2Up1.TabIndex = 9;
            this._S2Up1.Tag = "65535";
            this._S2Up1.Text = "0";
            this._S2Up1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 41);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(40, 13);
            this.label52.TabIndex = 8;
            this.label52.Text = "Uп1, B";
            // 
            // _S2Up
            // 
            this._S2Up.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2Up.Location = new System.Drawing.Point(86, 19);
            this._S2Up.Name = "_S2Up";
            this._S2Up.Size = new System.Drawing.Size(88, 20);
            this._S2Up.TabIndex = 7;
            this._S2Up.Tag = "65535";
            this._S2Up.Text = "0";
            this._S2Up.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 21);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 6;
            this.label53.Text = "Uп, B";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Location = new System.Drawing.Point(7, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(407, 186);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Параметры измерений";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._S2TTBB);
            this.groupBox7.Controls.Add(this._S2TTCB);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.label55);
            this.groupBox7.Location = new System.Drawing.Point(235, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(162, 86);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Номинальный ток ТТ";
            // 
            // _S2TTBB
            // 
            this._S2TTBB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2TTBB.Location = new System.Drawing.Point(59, 22);
            this._S2TTBB.Name = "_S2TTBB";
            this._S2TTBB.Size = new System.Drawing.Size(88, 20);
            this._S2TTBB.TabIndex = 7;
            this._S2TTBB.Tag = "65535";
            this._S2TTBB.Text = "0";
            this._S2TTBB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S2TTCB
            // 
            this._S2TTCB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2TTCB.Location = new System.Drawing.Point(59, 45);
            this._S2TTCB.Name = "_S2TTCB";
            this._S2TTCB.Size = new System.Drawing.Size(88, 20);
            this._S2TTCB.TabIndex = 6;
            this._S2TTCB.Tag = "65535";
            this._S2TTCB.Text = "0";
            this._S2TTCB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 50);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 13);
            this.label54.TabIndex = 2;
            this.label54.Text = "CВ, A";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 27);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 13);
            this.label55.TabIndex = 1;
            this.label55.Text = "ВВ, A";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label56);
            this.groupBox8.Controls.Add(this.label57);
            this.groupBox8.Controls.Add(this._S2Ktnnp);
            this.groupBox8.Controls.Add(this._S2TNNP);
            this.groupBox8.Controls.Add(this._S2Ktn);
            this.groupBox8.Controls.Add(this._S2MeasureCB);
            this.groupBox8.Controls.Add(this._S2TN);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this.label59);
            this.groupBox8.Controls.Add(this.label60);
            this.groupBox8.Controls.Add(this.label61);
            this.groupBox8.Controls.Add(this.label62);
            this.groupBox8.Location = new System.Drawing.Point(6, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(223, 161);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Параметры напряжения";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(98, 69);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(24, 13);
            this.label56.TabIndex = 25;
            this.label56.Text = "К * ";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(98, 110);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(24, 13);
            this.label57.TabIndex = 24;
            this.label57.Text = "К * ";
            // 
            // _S2Ktnnp
            // 
            this._S2Ktnnp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._S2Ktnnp.FormattingEnabled = true;
            this._S2Ktnnp.Location = new System.Drawing.Point(128, 106);
            this._S2Ktnnp.Name = "_S2Ktnnp";
            this._S2Ktnnp.Size = new System.Drawing.Size(89, 21);
            this._S2Ktnnp.TabIndex = 23;
            // 
            // _S2TNNP
            // 
            this._S2TNNP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2TNNP.Location = new System.Drawing.Point(101, 86);
            this._S2TNNP.Name = "_S2TNNP";
            this._S2TNNP.Size = new System.Drawing.Size(116, 20);
            this._S2TNNP.TabIndex = 22;
            this._S2TNNP.Tag = "65535";
            this._S2TNNP.Text = "0";
            this._S2TNNP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _S2Ktn
            // 
            this._S2Ktn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._S2Ktn.FormattingEnabled = true;
            this._S2Ktn.Location = new System.Drawing.Point(128, 65);
            this._S2Ktn.Name = "_S2Ktn";
            this._S2Ktn.Size = new System.Drawing.Size(89, 21);
            this._S2Ktn.TabIndex = 21;
            // 
            // _S2MeasureCB
            // 
            this._S2MeasureCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._S2MeasureCB.FormattingEnabled = true;
            this._S2MeasureCB.Location = new System.Drawing.Point(101, 24);
            this._S2MeasureCB.Name = "_S2MeasureCB";
            this._S2MeasureCB.Size = new System.Drawing.Size(116, 21);
            this._S2MeasureCB.TabIndex = 20;
            this._S2MeasureCB.SelectedValueChanged += new System.EventHandler(this._S2MeasureCB_SelectedValueChanged);
            // 
            // _S2TN
            // 
            this._S2TN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._S2TN.Location = new System.Drawing.Point(101, 45);
            this._S2TN.Name = "_S2TN";
            this._S2TN.Size = new System.Drawing.Size(116, 20);
            this._S2TN.TabIndex = 5;
            this._S2TN.Tag = "65535";
            this._S2TN.Text = "0";
            this._S2TN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(7, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(37, 13);
            this.label58.TabIndex = 4;
            this.label58.Text = "Ктннп";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(7, 88);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(78, 13);
            this.label59.TabIndex = 3;
            this.label59.Text = "ТННП коэфф.";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 47);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(62, 13);
            this.label60.TabIndex = 2;
            this.label60.Text = "ТН коэфф.";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(7, 68);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(25, 13);
            this.label61.TabIndex = 1;
            this.label61.Text = "Ктн";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(7, 27);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(65, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "Измерение";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(631, 412);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Входные сигналы";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._blockAutoSyp);
            this.groupBox9.Controls.Add(this._blockAutoSypLabel2);
            this.groupBox9.Controls.Add(this._blockAutoSypLabel1);
            this.groupBox9.Controls.Add(this._dropBlock);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this._kontrPitPrivod);
            this.groupBox9.Controls.Add(this.label67);
            this.groupBox9.Controls.Add(this._dropSign);
            this.groupBox9.Controls.Add(this.label66);
            this.groupBox9.Controls.Add(this._Up3);
            this.groupBox9.Controls.Add(this.label65);
            this.groupBox9.Controls.Add(this._blockT);
            this.groupBox9.Controls.Add(this.label64);
            this.groupBox9.Controls.Add(this._blockI);
            this.groupBox9.Controls.Add(this.label63);
            this.groupBox9.Location = new System.Drawing.Point(7, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(236, 227);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные сигналы";
            // 
            // _blockAutoSyp
            // 
            this._blockAutoSyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockAutoSyp.FormattingEnabled = true;
            this._blockAutoSyp.Location = new System.Drawing.Point(111, 189);
            this._blockAutoSyp.Name = "_blockAutoSyp";
            this._blockAutoSyp.Size = new System.Drawing.Size(116, 21);
            this._blockAutoSyp.TabIndex = 32;
            this._blockAutoSyp.Visible = false;
            // 
            // _blockAutoSypLabel2
            // 
            this._blockAutoSypLabel2.AutoSize = true;
            this._blockAutoSypLabel2.Location = new System.Drawing.Point(6, 201);
            this._blockAutoSypLabel2.Name = "_blockAutoSypLabel2";
            this._blockAutoSypLabel2.Size = new System.Drawing.Size(91, 13);
            this._blockAutoSypLabel2.TabIndex = 31;
            this._blockAutoSypLabel2.Text = "и дист. режимов";
            this._blockAutoSypLabel2.Visible = false;
            // 
            // _blockAutoSypLabel1
            // 
            this._blockAutoSypLabel1.AutoSize = true;
            this._blockAutoSypLabel1.Location = new System.Drawing.Point(6, 188);
            this._blockAutoSypLabel1.Name = "_blockAutoSypLabel1";
            this._blockAutoSypLabel1.Size = new System.Drawing.Size(94, 13);
            this._blockAutoSypLabel1.TabIndex = 31;
            this._blockAutoSypLabel1.Text = "Блокировка авт. ";
            this._blockAutoSypLabel1.Visible = false;
            // 
            // _dropBlock
            // 
            this._dropBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._dropBlock.FormattingEnabled = true;
            this._dropBlock.Location = new System.Drawing.Point(111, 162);
            this._dropBlock.Name = "_dropBlock";
            this._dropBlock.Size = new System.Drawing.Size(116, 21);
            this._dropBlock.TabIndex = 32;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 165);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(92, 13);
            this.label68.TabIndex = 31;
            this.label68.Text = "Сброс блокиров.";
            // 
            // _kontrPitPrivod
            // 
            this._kontrPitPrivod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._kontrPitPrivod.FormattingEnabled = true;
            this._kontrPitPrivod.Location = new System.Drawing.Point(111, 135);
            this._kontrPitPrivod.Name = "_kontrPitPrivod";
            this._kontrPitPrivod.Size = new System.Drawing.Size(116, 21);
            this._kontrPitPrivod.TabIndex = 30;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 138);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(105, 13);
            this.label67.TabIndex = 29;
            this.label67.Text = "Контр. пит. привод.";
            // 
            // _dropSign
            // 
            this._dropSign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._dropSign.FormattingEnabled = true;
            this._dropSign.Location = new System.Drawing.Point(111, 108);
            this._dropSign.Name = "_dropSign";
            this._dropSign.Size = new System.Drawing.Size(116, 21);
            this._dropSign.TabIndex = 28;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 111);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(91, 13);
            this.label66.TabIndex = 27;
            this.label66.Text = "Сброс сигнализ.";
            // 
            // _Up3
            // 
            this._Up3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Up3.FormattingEnabled = true;
            this._Up3.Location = new System.Drawing.Point(111, 81);
            this._Up3.Name = "_Up3";
            this._Up3.Size = new System.Drawing.Size(116, 21);
            this._Up3.TabIndex = 26;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 84);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(27, 13);
            this.label65.TabIndex = 25;
            this.label65.Text = "Uп3";
            // 
            // _blockT
            // 
            this._blockT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockT.FormattingEnabled = true;
            this._blockT.Location = new System.Drawing.Point(111, 54);
            this._blockT.Name = "_blockT";
            this._blockT.Size = new System.Drawing.Size(116, 21);
            this._blockT.TabIndex = 24;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 57);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(81, 13);
            this.label64.TabIndex = 23;
            this.label64.Text = "Блок. по темп.";
            // 
            // _blockI
            // 
            this._blockI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockI.FormattingEnabled = true;
            this._blockI.Location = new System.Drawing.Point(111, 27);
            this._blockI.Name = "_blockI";
            this._blockI.Size = new System.Drawing.Size(116, 21);
            this._blockI.TabIndex = 22;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 30);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(75, 13);
            this.label63.TabIndex = 21;
            this.label63.Text = "Блок. по току";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox26);
            this.tabPage4.Controls.Add(this.groupBox12);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(631, 412);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Выходные сигналы";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._neispr3CB);
            this.groupBox26.Controls.Add(this._neispr2CB);
            this.groupBox26.Controls.Add(this._neispr1CB);
            this.groupBox26.Controls.Add(this._impTB);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Location = new System.Drawing.Point(377, 6);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(216, 150);
            this.groupBox26.TabIndex = 7;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Сигнал неисправность";
            // 
            // _neispr3CB
            // 
            this._neispr3CB.AutoSize = true;
            this._neispr3CB.Location = new System.Drawing.Point(188, 87);
            this._neispr3CB.Name = "_neispr3CB";
            this._neispr3CB.Size = new System.Drawing.Size(15, 14);
            this._neispr3CB.TabIndex = 11;
            this._neispr3CB.UseVisualStyleBackColor = true;
            // 
            // _neispr2CB
            // 
            this._neispr2CB.AutoSize = true;
            this._neispr2CB.Location = new System.Drawing.Point(188, 56);
            this._neispr2CB.Name = "_neispr2CB";
            this._neispr2CB.Size = new System.Drawing.Size(15, 14);
            this._neispr2CB.TabIndex = 10;
            this._neispr2CB.UseVisualStyleBackColor = true;
            // 
            // _neispr1CB
            // 
            this._neispr1CB.AutoSize = true;
            this._neispr1CB.Location = new System.Drawing.Point(188, 25);
            this._neispr1CB.Name = "_neispr1CB";
            this._neispr1CB.Size = new System.Drawing.Size(15, 14);
            this._neispr1CB.TabIndex = 9;
            this._neispr1CB.UseVisualStyleBackColor = true;
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(101, 115);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(102, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(23, 117);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(69, 13);
            this.label84.TabIndex = 3;
            this.label84.Text = "Возврат, мс";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(23, 87);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(131, 13);
            this.label83.TabIndex = 2;
            this.label83.Text = "Неисправность привода";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(23, 56);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(158, 13);
            this.label82.TabIndex = 1;
            this.label82.Text = "Программная неисправность";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(23, 25);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(147, 13);
            this.label81.TabIndex = 0;
            this.label81.Text = "Аппаратная неисправность";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._outputReleGrid);
            this.groupBox12.Location = new System.Drawing.Point(7, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(364, 348);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputReleGrid.Location = new System.Drawing.Point(3, 16);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(358, 329);
            this._outputReleGrid.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._faultsGrBox);
            this.tabPage5.Controls.Add(this.groupBox14);
            this.tabPage5.Controls.Add(this.groupBox13);
            this.tabPage5.Controls.Add(this.groupBox11);
            this.tabPage5.Controls.Add(this.groupBox10);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(631, 412);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Параметры регулирования";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _faultsGrBox
            // 
            this._faultsGrBox.Controls.Add(this.label25);
            this._faultsGrBox.Controls.Add(this.groupBox15);
            this._faultsGrBox.Controls.Add(this.groupBox16);
            this._faultsGrBox.Location = new System.Drawing.Point(470, 6);
            this._faultsGrBox.Name = "_faultsGrBox";
            this._faultsGrBox.Size = new System.Drawing.Size(154, 391);
            this._faultsGrBox.TabIndex = 5;
            this._faultsGrBox.TabStop = false;
            this._faultsGrBox.Text = "Неисправности привода";
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(6, 238);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(142, 145);
            this.label25.TabIndex = 10;
            this.label25.Text = resources.GetString("label25.Text");
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._fixStickCheck);
            this.groupBox15.Controls.Add(this._fixRunCheck);
            this.groupBox15.Controls.Add(this._fixNotGoCheck);
            this.groupBox15.Location = new System.Drawing.Point(6, 19);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(141, 100);
            this.groupBox15.TabIndex = 4;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Фиксация блокировки управления";
            // 
            // _fixStickCheck
            // 
            this._fixStickCheck.AutoSize = true;
            this._fixStickCheck.Location = new System.Drawing.Point(6, 52);
            this._fixStickCheck.Name = "_fixStickCheck";
            this._fixStickCheck.Size = new System.Drawing.Size(108, 17);
            this._fixStickCheck.TabIndex = 0;
            this._fixStickCheck.Text = "Привод застрял";
            this._fixStickCheck.UseVisualStyleBackColor = true;
            // 
            // _fixRunCheck
            // 
            this._fixRunCheck.AutoSize = true;
            this._fixRunCheck.Location = new System.Drawing.Point(6, 75);
            this._fixRunCheck.Name = "_fixRunCheck";
            this._fixRunCheck.Size = new System.Drawing.Size(111, 17);
            this._fixRunCheck.TabIndex = 0;
            this._fixRunCheck.Text = "Привод побежал";
            this._fixRunCheck.UseVisualStyleBackColor = true;
            // 
            // _fixNotGoCheck
            // 
            this._fixNotGoCheck.AutoSize = true;
            this._fixNotGoCheck.Location = new System.Drawing.Point(6, 29);
            this._fixNotGoCheck.Name = "_fixNotGoCheck";
            this._fixNotGoCheck.Size = new System.Drawing.Size(121, 17);
            this._fixNotGoCheck.TabIndex = 0;
            this._fixNotGoCheck.Text = "Привод не пошел *";
            this._fixNotGoCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._offStickCheck);
            this.groupBox16.Controls.Add(this._offRunCheck);
            this.groupBox16.Controls.Add(this._offNotGoCheck);
            this.groupBox16.Location = new System.Drawing.Point(6, 120);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(141, 115);
            this.groupBox16.TabIndex = 4;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Действие на отключение питания привода";
            // 
            // _offStickCheck
            // 
            this._offStickCheck.AutoSize = true;
            this._offStickCheck.Location = new System.Drawing.Point(6, 70);
            this._offStickCheck.Name = "_offStickCheck";
            this._offStickCheck.Size = new System.Drawing.Size(108, 17);
            this._offStickCheck.TabIndex = 0;
            this._offStickCheck.Text = "Привод застрял";
            this._offStickCheck.UseVisualStyleBackColor = true;
            // 
            // _offRunCheck
            // 
            this._offRunCheck.AutoSize = true;
            this._offRunCheck.Location = new System.Drawing.Point(6, 93);
            this._offRunCheck.Name = "_offRunCheck";
            this._offRunCheck.Size = new System.Drawing.Size(111, 17);
            this._offRunCheck.TabIndex = 0;
            this._offRunCheck.Text = "Привод побежал";
            this._offRunCheck.UseVisualStyleBackColor = true;
            // 
            // _offNotGoCheck
            // 
            this._offNotGoCheck.AutoSize = true;
            this._offNotGoCheck.Location = new System.Drawing.Point(6, 47);
            this._offNotGoCheck.Name = "_offNotGoCheck";
            this._offNotGoCheck.Size = new System.Drawing.Size(114, 17);
            this._offNotGoCheck.TabIndex = 0;
            this._offNotGoCheck.Text = "Привод не пошел";
            this._offNotGoCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._logCheck);
            this.groupBox14.Controls.Add(this._writeResource);
            this.groupBox14.Controls.Add(this._writeStupen);
            this.groupBox14.Controls.Add(this._resource);
            this.groupBox14.Controls.Add(this.label71);
            this.groupBox14.Controls.Add(this._startStep);
            this.groupBox14.Controls.Add(this._rStep);
            this.groupBox14.Controls.Add(this._countSteps);
            this.groupBox14.Controls.Add(this._endStep);
            this.groupBox14.Controls.Add(this._stupen);
            this.groupBox14.Controls.Add(this._counter);
            this.groupBox14.Controls.Add(this.endStepLabel);
            this.groupBox14.Controls.Add(this.startStepLabel);
            this.groupBox14.Controls.Add(this.rStepLabel);
            this.groupBox14.Controls.Add(this.countStepLabel);
            this.groupBox14.Controls.Add(this.logLabel);
            this.groupBox14.Controls.Add(this.label69);
            this.groupBox14.Controls.Add(this.label70);
            this.groupBox14.Location = new System.Drawing.Point(230, 208);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(234, 189);
            this.groupBox14.TabIndex = 3;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Счетчик переключений";
            // 
            // _logCheck
            // 
            this._logCheck.AutoSize = true;
            this._logCheck.Location = new System.Drawing.Point(110, 19);
            this._logCheck.Name = "_logCheck";
            this._logCheck.Size = new System.Drawing.Size(15, 14);
            this._logCheck.TabIndex = 30;
            this._logCheck.UseVisualStyleBackColor = true;
            this._logCheck.Visible = false;
            this._logCheck.CheckedChanged += new System.EventHandler(this._logCheck_CheckedChanged);
            // 
            // _writeResource
            // 
            this._writeResource.AutoSize = true;
            this._writeResource.Location = new System.Drawing.Point(162, 57);
            this._writeResource.Name = "_writeResource";
            this._writeResource.Size = new System.Drawing.Size(65, 23);
            this._writeResource.TabIndex = 29;
            this._writeResource.Text = "Записать";
            this._writeResource.UseVisualStyleBackColor = true;
            this._writeResource.Click += new System.EventHandler(this._writeResource_Click);
            // 
            // _writeStupen
            // 
            this._writeStupen.AutoSize = true;
            this._writeStupen.Location = new System.Drawing.Point(162, 36);
            this._writeStupen.Name = "_writeStupen";
            this._writeStupen.Size = new System.Drawing.Size(65, 23);
            this._writeStupen.TabIndex = 28;
            this._writeStupen.Text = "Записать";
            this._writeStupen.UseVisualStyleBackColor = true;
            this._writeStupen.Click += new System.EventHandler(this._writeStupen_Click);
            // 
            // _resource
            // 
            this._resource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._resource.Location = new System.Drawing.Point(110, 58);
            this._resource.Name = "_resource";
            this._resource.Size = new System.Drawing.Size(46, 20);
            this._resource.TabIndex = 13;
            this._resource.Tag = "65000";
            this._resource.Text = "0";
            this._resource.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(5, 60);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(78, 13);
            this.label71.TabIndex = 12;
            this.label71.Text = "Выраб ресурс";
            // 
            // _startStep
            // 
            this._startStep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._startStep.Enabled = false;
            this._startStep.Location = new System.Drawing.Point(110, 142);
            this._startStep.Name = "_startStep";
            this._startStep.Size = new System.Drawing.Size(46, 20);
            this._startStep.TabIndex = 11;
            this._startStep.Tag = "40";
            this._startStep.Text = "0";
            this._startStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._startStep.Visible = false;
            // 
            // _rStep
            // 
            this._rStep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rStep.Enabled = false;
            this._rStep.Location = new System.Drawing.Point(110, 123);
            this._rStep.Name = "_rStep";
            this._rStep.Size = new System.Drawing.Size(46, 20);
            this._rStep.TabIndex = 11;
            this._rStep.Tag = "40";
            this._rStep.Text = "0";
            this._rStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._rStep.Visible = false;
            // 
            // _countSteps
            // 
            this._countSteps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._countSteps.Enabled = false;
            this._countSteps.Location = new System.Drawing.Point(110, 104);
            this._countSteps.Name = "_countSteps";
            this._countSteps.Size = new System.Drawing.Size(46, 20);
            this._countSteps.TabIndex = 11;
            this._countSteps.Tag = "40";
            this._countSteps.Text = "0";
            this._countSteps.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._countSteps.Visible = false;
            // 
            // _endStep
            // 
            this._endStep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._endStep.Enabled = false;
            this._endStep.Location = new System.Drawing.Point(110, 161);
            this._endStep.Name = "_endStep";
            this._endStep.Size = new System.Drawing.Size(46, 20);
            this._endStep.TabIndex = 11;
            this._endStep.Tag = "40";
            this._endStep.Text = "0";
            this._endStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._endStep.Visible = false;
            // 
            // _stupen
            // 
            this._stupen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._stupen.Location = new System.Drawing.Point(110, 39);
            this._stupen.Name = "_stupen";
            this._stupen.Size = new System.Drawing.Size(46, 20);
            this._stupen.TabIndex = 11;
            this._stupen.Tag = "40";
            this._stupen.Text = "0";
            this._stupen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _counter
            // 
            this._counter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._counter.FormattingEnabled = true;
            this._counter.Location = new System.Drawing.Point(110, 81);
            this._counter.Name = "_counter";
            this._counter.Size = new System.Drawing.Size(117, 21);
            this._counter.TabIndex = 10;
            // 
            // endStepLabel
            // 
            this.endStepLabel.AutoSize = true;
            this.endStepLabel.Location = new System.Drawing.Point(5, 163);
            this.endStepLabel.Name = "endStepLabel";
            this.endStepLabel.Size = new System.Drawing.Size(98, 13);
            this.endStepLabel.TabIndex = 9;
            this.endStepLabel.Text = "Конечная ступень";
            this.endStepLabel.Visible = false;
            // 
            // startStepLabel
            // 
            this.startStepLabel.AutoSize = true;
            this.startStepLabel.Location = new System.Drawing.Point(5, 144);
            this.startStepLabel.Name = "startStepLabel";
            this.startStepLabel.Size = new System.Drawing.Size(105, 13);
            this.startStepLabel.TabIndex = 9;
            this.startStepLabel.Text = "Начальная ступень";
            this.startStepLabel.Visible = false;
            // 
            // rStepLabel
            // 
            this.rStepLabel.AutoSize = true;
            this.rStepLabel.Location = new System.Drawing.Point(5, 125);
            this.rStepLabel.Name = "rStepLabel";
            this.rStepLabel.Size = new System.Drawing.Size(77, 13);
            this.rStepLabel.TabIndex = 9;
            this.rStepLabel.Text = "Rступени, Ом";
            this.rStepLabel.Visible = false;
            // 
            // countStepLabel
            // 
            this.countStepLabel.AutoSize = true;
            this.countStepLabel.Location = new System.Drawing.Point(5, 106);
            this.countStepLabel.Name = "countStepLabel";
            this.countStepLabel.Size = new System.Drawing.Size(90, 13);
            this.countStepLabel.TabIndex = 9;
            this.countStepLabel.Text = "Кол-во ступеней";
            this.countStepLabel.Visible = false;
            // 
            // logLabel
            // 
            this.logLabel.AutoSize = true;
            this.logLabel.Location = new System.Drawing.Point(5, 19);
            this.logLabel.Name = "logLabel";
            this.logLabel.Size = new System.Drawing.Size(57, 13);
            this.logLabel.TabIndex = 9;
            this.logLabel.Text = "Логометр";
            this.logLabel.Visible = false;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(5, 41);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(95, 13);
            this.label69.TabIndex = 9;
            this.label69.Text = "Текущая ступень";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(5, 84);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(85, 13);
            this.label70.TabIndex = 8;
            this.label70.Text = "Направл. счета";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label96);
            this.groupBox13.Controls.Add(this._timeSnSig);
            this.groupBox13.Controls.Add(this.label74);
            this.groupBox13.Controls.Add(this._timeEnd);
            this.groupBox13.Controls.Add(this.label22);
            this.groupBox13.Controls.Add(this.label72);
            this.groupBox13.Controls.Add(this._timeStart);
            this.groupBox13.Controls.Add(this.label73);
            this.groupBox13.Location = new System.Drawing.Point(7, 208);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(217, 189);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Импульсы режима управления";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(7, 110);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(102, 13);
            this.label96.TabIndex = 23;
            this.label96.Text = "управления Тз, мс";
            // 
            // _timeSnSig
            // 
            this._timeSnSig.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeSnSig.Location = new System.Drawing.Point(152, 99);
            this._timeSnSig.Name = "_timeSnSig";
            this._timeSnSig.Size = new System.Drawing.Size(59, 20);
            this._timeSnSig.TabIndex = 19;
            this._timeSnSig.Tag = "3000000";
            this._timeSnSig.Text = "0";
            this._timeSnSig.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(6, 94);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(140, 13);
            this.label74.TabIndex = 18;
            this.label74.Text = "Задержка снятия сигнала";
            // 
            // _timeEnd
            // 
            this._timeEnd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnd.Location = new System.Drawing.Point(152, 60);
            this._timeEnd.Name = "_timeEnd";
            this._timeEnd.Size = new System.Drawing.Size(59, 20);
            this._timeEnd.TabIndex = 17;
            this._timeEnd.Tag = "3000000";
            this._timeEnd.Text = "0";
            this._timeEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 13);
            this.label22.TabIndex = 16;
            this.label22.Text = "переключения Тп, мс";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(6, 56);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(141, 13);
            this.label72.TabIndex = 16;
            this.label72.Text = "Время проверки заверш-я";
            // 
            // _timeStart
            // 
            this._timeStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeStart.Location = new System.Drawing.Point(152, 22);
            this._timeStart.Name = "_timeStart";
            this._timeStart.Size = new System.Drawing.Size(59, 20);
            this._timeStart.TabIndex = 15;
            this._timeStart.Tag = "3000000";
            this._timeStart.Text = "0";
            this._timeStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 24);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(121, 13);
            this.label73.TabIndex = 14;
            this.label73.Text = "Время реакции Тр, мс";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.timeLabel4);
            this.groupBox11.Controls.Add(this.timeLabel2);
            this.groupBox11.Controls.Add(this.label95);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Controls.Add(this._time2Com);
            this.groupBox11.Controls.Add(this._time1Com);
            this.groupBox11.Controls.Add(this._time5Com);
            this.groupBox11.Controls.Add(this.timeLabel3);
            this.groupBox11.Controls.Add(this.timeLabel1);
            this.groupBox11.Controls.Add(this.label86);
            this.groupBox11.Controls.Add(this._time4Com);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._time3Com);
            this.groupBox11.Controls.Add(this.label21);
            this.groupBox11.Controls.Add(this.label88);
            this.groupBox11.Location = new System.Drawing.Point(230, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(221, 190);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Задержки команд управления";
            // 
            // timeLabel4
            // 
            this.timeLabel4.AutoSize = true;
            this.timeLabel4.Location = new System.Drawing.Point(7, 171);
            this.timeLabel4.Name = "timeLabel4";
            this.timeLabel4.Size = new System.Drawing.Size(134, 13);
            this.timeLabel4.TabIndex = 33;
            this.timeLabel4.Text = "питания привода Тот, мс";
            this.timeLabel4.Visible = false;
            // 
            // timeLabel2
            // 
            this.timeLabel2.AutoSize = true;
            this.timeLabel2.Location = new System.Drawing.Point(7, 137);
            this.timeLabel2.Name = "timeLabel2";
            this.timeLabel2.Size = new System.Drawing.Size(147, 13);
            this.timeLabel2.TabIndex = 33;
            this.timeLabel2.Text = "перегрузки по току Тср, мс";
            this.timeLabel2.Visible = false;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(8, 100);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(135, 13);
            this.label95.TabIndex = 33;
            this.label95.Text = "перенапряжения Ткп, мс";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(8, 64);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(95, 13);
            this.label94.TabIndex = 32;
            this.label94.Text = "команды Тк2, мс";
            // 
            // _time2Com
            // 
            this._time2Com.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._time2Com.Location = new System.Drawing.Point(160, 156);
            this._time2Com.Name = "_time2Com";
            this._time2Com.Size = new System.Drawing.Size(55, 20);
            this._time2Com.TabIndex = 28;
            this._time2Com.Tag = "3000000";
            this._time2Com.Text = "0";
            this._time2Com.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._time2Com.Visible = false;
            // 
            // _time1Com
            // 
            this._time1Com.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._time1Com.Location = new System.Drawing.Point(160, 121);
            this._time1Com.Name = "_time1Com";
            this._time1Com.Size = new System.Drawing.Size(55, 20);
            this._time1Com.TabIndex = 28;
            this._time1Com.Tag = "3000000";
            this._time1Com.Text = "0";
            this._time1Com.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._time1Com.Visible = false;
            // 
            // _time5Com
            // 
            this._time5Com.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._time5Com.Location = new System.Drawing.Point(161, 88);
            this._time5Com.Name = "_time5Com";
            this._time5Com.Size = new System.Drawing.Size(54, 20);
            this._time5Com.TabIndex = 28;
            this._time5Com.Tag = "3000000";
            this._time5Com.Text = "0";
            this._time5Com.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // timeLabel3
            // 
            this.timeLabel3.AutoSize = true;
            this.timeLabel3.Location = new System.Drawing.Point(6, 157);
            this.timeLabel3.Name = "timeLabel3";
            this.timeLabel3.Size = new System.Drawing.Size(121, 13);
            this.timeLabel3.TabIndex = 27;
            this.timeLabel3.Text = "Задержка отключения";
            this.timeLabel3.Visible = false;
            // 
            // timeLabel1
            // 
            this.timeLabel1.AutoSize = true;
            this.timeLabel1.Location = new System.Drawing.Point(6, 121);
            this.timeLabel1.Name = "timeLabel1";
            this.timeLabel1.Size = new System.Drawing.Size(108, 13);
            this.timeLabel1.TabIndex = 27;
            this.timeLabel1.Text = "Задержка контроля";
            this.timeLabel1.Visible = false;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(7, 85);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(107, 13);
            this.label86.TabIndex = 27;
            this.label86.Text = "Задержка команды";
            // 
            // _time4Com
            // 
            this._time4Com.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._time4Com.Location = new System.Drawing.Point(161, 54);
            this._time4Com.Name = "_time4Com";
            this._time4Com.Size = new System.Drawing.Size(54, 20);
            this._time4Com.TabIndex = 26;
            this._time4Com.Tag = "3000000";
            this._time4Com.Text = "0";
            this._time4Com.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(7, 49);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(114, 13);
            this.label87.TabIndex = 25;
            this.label87.Text = "Задержка повторной";
            // 
            // _time3Com
            // 
            this._time3Com.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._time3Com.Location = new System.Drawing.Point(161, 21);
            this._time3Com.Name = "_time3Com";
            this._time3Com.Size = new System.Drawing.Size(54, 20);
            this._time3Com.TabIndex = 24;
            this._time3Com.Tag = "3000000";
            this._time3Com.Text = "0";
            this._time3Com.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "команды Тк1, мс";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(7, 16);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(97, 13);
            this.label88.TabIndex = 23;
            this.label88.Text = "Задержка первой";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label97);
            this.groupBox10.Controls.Add(this._controlSdtu);
            this.groupBox10.Controls.Add(this._controlSdtuLabel);
            this.groupBox10.Controls.Add(this._handMode);
            this.groupBox10.Controls.Add(this.label93);
            this.groupBox10.Controls.Add(this._pilt);
            this.groupBox10.Controls.Add(this.label92);
            this.groupBox10.Controls.Add(this._key);
            this.groupBox10.Controls.Add(this.label91);
            this.groupBox10.Controls.Add(this._autoMode);
            this.groupBox10.Controls.Add(this.label90);
            this.groupBox10.Controls.Add(this._regMode);
            this.groupBox10.Controls.Add(this.label89);
            this.groupBox10.Location = new System.Drawing.Point(7, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(217, 190);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Параметры режима регулирования";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 57);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(78, 13);
            this.label97.TabIndex = 21;
            this.label97.Text = "автом. режим";
            // 
            // _controlSdtu
            // 
            this._controlSdtu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._controlSdtu.FormattingEnabled = true;
            this._controlSdtu.Location = new System.Drawing.Point(107, 139);
            this._controlSdtu.Name = "_controlSdtu";
            this._controlSdtu.Size = new System.Drawing.Size(104, 21);
            this._controlSdtu.TabIndex = 20;
            this._controlSdtu.Visible = false;
            // 
            // _controlSdtuLabel
            // 
            this._controlSdtuLabel.AutoSize = true;
            this._controlSdtuLabel.Location = new System.Drawing.Point(6, 142);
            this._controlSdtuLabel.Name = "_controlSdtuLabel";
            this._controlSdtuLabel.Size = new System.Drawing.Size(73, 13);
            this._controlSdtuLabel.TabIndex = 19;
            this._controlSdtuLabel.Text = "Дист. режим";
            this._controlSdtuLabel.Visible = false;
            // 
            // _handMode
            // 
            this._handMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._handMode.FormattingEnabled = true;
            this._handMode.Location = new System.Drawing.Point(107, 116);
            this._handMode.Name = "_handMode";
            this._handMode.Size = new System.Drawing.Size(104, 21);
            this._handMode.TabIndex = 20;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 119);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(79, 13);
            this.label93.TabIndex = 19;
            this.label93.Text = "Ручной режим";
            // 
            // _pilt
            // 
            this._pilt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._pilt.FormattingEnabled = true;
            this._pilt.Location = new System.Drawing.Point(107, 93);
            this._pilt.Name = "_pilt";
            this._pilt.Size = new System.Drawing.Size(104, 21);
            this._pilt.TabIndex = 18;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(6, 96);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(85, 13);
            this.label92.TabIndex = 17;
            this.label92.Text = "Управл. : пульт";
            // 
            // _key
            // 
            this._key.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._key.FormattingEnabled = true;
            this._key.Location = new System.Drawing.Point(107, 70);
            this._key.Name = "_key";
            this._key.Size = new System.Drawing.Size(104, 21);
            this._key.TabIndex = 16;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(6, 73);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(82, 13);
            this.label91.TabIndex = 15;
            this.label91.Text = "Управл. : ключ";
            // 
            // _autoMode
            // 
            this._autoMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._autoMode.FormattingEnabled = true;
            this._autoMode.Location = new System.Drawing.Point(107, 47);
            this._autoMode.Name = "_autoMode";
            this._autoMode.Size = new System.Drawing.Size(104, 21);
            this._autoMode.TabIndex = 14;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 43);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(62, 13);
            this.label90.TabIndex = 13;
            this.label90.Text = "Переход в ";
            // 
            // _regMode
            // 
            this._regMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._regMode.FormattingEnabled = true;
            this._regMode.Location = new System.Drawing.Point(107, 23);
            this._regMode.Name = "_regMode";
            this._regMode.Size = new System.Drawing.Size(104, 21);
            this._regMode.TabIndex = 12;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 26);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(100, 13);
            this.label89.TabIndex = 11;
            this.label89.Text = "Режим регулиров.";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР851";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "МР851_уставки";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР851";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.xml) | *.xml";
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(518, 444);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(114, 23);
            this._saveToXmlButton.TabIndex = 39;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(151, 444);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(134, 23);
            this._writeConfigBut.TabIndex = 36;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 469);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(642, 22);
            this._statusStrip.TabIndex = 34;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Maximum = 90;
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            this._configProgressBar.Step = 1;
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(5, 444);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 35;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readButton_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(408, 444);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(108, 23);
            this._saveConfigBut.TabIndex = 38;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(287, 444);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(119, 23);
            this._loadConfigBut.TabIndex = 37;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 120;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.Width = 90;
            // 
            // Mr851ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 491);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Mr851ConfigurationForm";
            this.Text = "MDO_Configuration";
            this.Load += new System.EventHandler(this.MDO_Configuration_Load);
            this.Shown += new System.EventHandler(this.Mr851ConfigurationForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr851ConfigurationForm_KeyUp);
            this.tabControl1.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this._faultsGrBox.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox _S1TN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox _S1Ktnnp;
        private System.Windows.Forms.MaskedTextBox _S1TNNP;
        private System.Windows.Forms.ComboBox _S1Ktn;
        private System.Windows.Forms.ComboBox _S1MeasureCB;
        private System.Windows.Forms.MaskedTextBox _S1Up3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _S1Up2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox _S1Up1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _S1Up;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox _S1TTBB;
        private System.Windows.Forms.MaskedTextBox _S1TTCB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox _S1Umax;
        private System.Windows.Forms.MaskedTextBox _S1Ukm;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox _S1Uk;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox _S1Imax;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _S1Un;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox _S1Umin;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox _S1dU;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.MaskedTextBox _S2Umax;
        private System.Windows.Forms.MaskedTextBox _S2Ukm;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.MaskedTextBox _S2Uk;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.MaskedTextBox _S2Imax;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.MaskedTextBox _S2Un;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.MaskedTextBox _S2Umin;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.MaskedTextBox _S2dU;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.MaskedTextBox _S2Up3;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.MaskedTextBox _S2Up2;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox _S2Up1;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox _S2Up;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.MaskedTextBox _S2TTBB;
        private System.Windows.Forms.MaskedTextBox _S2TTCB;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox _S2Ktnnp;
        private System.Windows.Forms.MaskedTextBox _S2TNNP;
        private System.Windows.Forms.ComboBox _S2Ktn;
        private System.Windows.Forms.ComboBox _S2MeasureCB;
        private System.Windows.Forms.MaskedTextBox _S2TN;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox _dropBlock;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.ComboBox _kontrPitPrivod;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox _dropSign;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox _Up3;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox _blockT;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox _blockI;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.MaskedTextBox _stupen;
        private System.Windows.Forms.ComboBox _counter;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.MaskedTextBox _resource;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.MaskedTextBox _timeSnSig;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.MaskedTextBox _timeEnd;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.MaskedTextBox _timeStart;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.MaskedTextBox _time5Com;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.MaskedTextBox _time4Com;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.MaskedTextBox _time3Com;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.ComboBox _autoMode;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.ComboBox _regMode;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.ComboBox _handMode;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.ComboBox _pilt;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.ComboBox _key;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Button _writeResource;
        private System.Windows.Forms.Button _writeStupen;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.CheckBox _neispr3CB;
        private System.Windows.Forms.CheckBox _neispr2CB;
        private System.Windows.Forms.CheckBox _neispr1CB;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.Label timeLabel4;
        private System.Windows.Forms.Label timeLabel2;
        private System.Windows.Forms.MaskedTextBox _time2Com;
        private System.Windows.Forms.MaskedTextBox _time1Com;
        private System.Windows.Forms.Label timeLabel3;
        private System.Windows.Forms.Label timeLabel1;
        private System.Windows.Forms.CheckBox _logCheck;
        private System.Windows.Forms.Label logLabel;
        private System.Windows.Forms.MaskedTextBox _countSteps;
        private System.Windows.Forms.Label countStepLabel;
        private System.Windows.Forms.MaskedTextBox _startStep;
        private System.Windows.Forms.MaskedTextBox _rStep;
        private System.Windows.Forms.Label startStepLabel;
        private System.Windows.Forms.Label rStepLabel;
        private System.Windows.Forms.MaskedTextBox _endStep;
        private System.Windows.Forms.Label endStepLabel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.CheckBox _offStickCheck;
        private System.Windows.Forms.CheckBox _offRunCheck;
        private System.Windows.Forms.CheckBox _offNotGoCheck;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.CheckBox _fixStickCheck;
        private System.Windows.Forms.CheckBox _fixRunCheck;
        private System.Windows.Forms.CheckBox _fixNotGoCheck;
        private System.Windows.Forms.GroupBox _faultsGrBox;
        private System.Windows.Forms.MaskedTextBox _uMinrdS1;
        private System.Windows.Forms.Label _uMinrdLabelS1;
        private System.Windows.Forms.MaskedTextBox _uMinrdS2;
        private System.Windows.Forms.Label _uMinrdLabelS2;
        private System.Windows.Forms.ComboBox _blockAutoSyp;
        private System.Windows.Forms.Label _blockAutoSypLabel1;
        private System.Windows.Forms.ComboBox _controlSdtu;
        private System.Windows.Forms.Label _controlSdtuLabel;
        private System.Windows.Forms.Label _blockAutoSypLabel2;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
    }
}