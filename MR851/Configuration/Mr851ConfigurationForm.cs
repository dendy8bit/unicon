﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MR851.Configuration.Structures;
using BEMN.MBServer;
using BEMN.MR851.Measuring.Structures;
using BEMN.MR851.Properties;

namespace BEMN.MR851.Configuration
{
    public partial class Mr851ConfigurationForm : Form, IFormView
    {
        #region [Constants]
        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";
        private const string READ_COUNT_FAIL = "Конфигурация счетчика не может быть прочитана";
        private const string WRITE_OK = "Конфигурация записана";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string WRITE_COUNT_FAIL = "Конфигурация счетчика не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "{0}_SET_POINTS";
        private const string ERROR_SETPOINTS_VALUE = "В конфигурации заданы некорректные значения. Проверьте конфигурацию.";
        private const string INVALID_PORT = "Порт недоступен.";
        private const string MR851 = "MR851";
        #endregion [Constants]

        #region Fields
        private Mr851Device _device;
        private MemoryEntity<ConfigurationStruct> _configuration;
        private ConfigurationStruct _currentSetpointsStruct;
        private CounterMeasStruct _counterMeasStruct;
        private NewStructValidator<SectionStruct> _section1Validator;
        private NewStructValidator<SectionStruct> _section2Validator;
        private NewDgwValidatior<AllRelayStruct, RelayStruct> _relayValidator;
        private NewStructValidator<DisrepairStruct> _disrepairValidator;
        private NewStructValidator<RegParamsStruct> _regValidator;
        private NewStructValidator<InputSignalsStruct> _inputSignalsValidator;
        private NewStructValidator<CounterMeasStruct> _counterValidator;
        private StructUnion<ConfigurationStruct> _configurationValidator;
        private MemoryEntity<CounterMeasStruct> _counterAndResource;
        
        private enum WriteCounterResource
        {
            STEP,RESOURCE,ALL
        }

        private WriteCounterResource _currentOperation;
        #endregion

        #region Property
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }
        #endregion

        #region C'tor
        public Mr851ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr851ConfigurationForm(Mr851Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this.InitValidators();

            this._counterAndResource = this._device.CounterConfig;
            this._counterAndResource.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,()=>
            {
                this._counterValidator.Set(this._counterAndResource.Value);
            });
            this._counterAndResource.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._currentOperation == WriteCounterResource.STEP)
                {
                    MessageBox.Show("Текущая ступень записана успешно", "Внимание", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else if (this._currentOperation == WriteCounterResource.RESOURCE)
                {
                    MessageBox.Show("Выработанный ресурс записан успешно", "Внимание", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            });
            this._counterAndResource.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show(READ_COUNT_FAIL));
            this._counterAndResource.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show(WRITE_COUNT_FAIL));
            this._counterAndResource.ReadOk += HandlerHelper.CreateHandler(this, () => this._configProgressBar.PerformStep());

            this._configuration = device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadFail);
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteFail);

            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._configProgressBar.PerformStep());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._configProgressBar.PerformStep());

            this._configProgressBar.Maximum = this._configuration.Slots.Count + this._counterAndResource.Slots.Count;
        }

        private void InitValidators()
        {
            #region Validators

            this._currentSetpointsStruct = new ConfigurationStruct();
            this._counterValidator = new NewStructValidator<CounterMeasStruct>(this._toolTip, new ControlInfoText(this._stupen, new CustomUshortRule(1, 40)), new ControlInfoText(this._resource, new CustomUshortRule(0, 65535)));

            var validFunc1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (Common.VersionConverter(this._device.DeviceVersion) >= 1.02)
                    {
                        return new CustomUshortRule(1, 40);
                    }
                    return new CustomUshortRule(ushort.MinValue, ushort.MaxValue);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(ushort.MinValue, ushort.MaxValue);
                }
            });
            var validFunc2 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (Common.VersionConverter(this._device.DeviceVersion) >= 1.02)
                    {
                        return RulesContainer.IntTo3M;
                    }
                    return new CustomUshortRule(ushort.MinValue, ushort.MaxValue);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(ushort.MinValue, ushort.MaxValue);
                }
            });
            var validFunc3 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (Common.VersionConverter(this._device.DeviceVersion) >= 1.02)
                    {
                        return new CustomUshortRule(3, 12);
                    }
                    return new CustomUshortRule(ushort.MinValue, ushort.MaxValue);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(ushort.MinValue, ushort.MaxValue);
                }
            });
            this._section1Validator = new NewStructValidator<SectionStruct>(this._toolTip, new ControlInfoCombo(this._S1MeasureCB, Strings.MEASURE), new ControlInfoText(this._S1TN, RulesContainer.Ustavka128), new ControlInfoCombo(this._S1Ktn, Strings.KOEFF), new ControlInfoText(this._S1TNNP, RulesContainer.Ustavka128), new ControlInfoCombo(this._S1Ktnnp, Strings.KOEFF), new ControlInfoText(this._S1TTBB, new CustomUshortRule(0, 5000)), new ControlInfoText(this._S1TTCB, new CustomUshortRule(0, 5000)), new ControlInfoText(this._S1Up, RulesContainer.Ustavka150), new ControlInfoText(this._S1Up1, RulesContainer.Ustavka150), new ControlInfoText(this._S1Up2, RulesContainer.Ustavka150), new ControlInfoText(this._S1Up3, RulesContainer.Ustavka150), new ControlInfoText(this._S1dU, new CustomDoubleRule(0, 30)), new ControlInfoText(this._S1Umin, RulesContainer.Ustavka150), new ControlInfoText(this._S1Umax, RulesContainer.Ustavka150), new ControlInfoText(this._S1Un, new CustomDoubleRule(0, 60)), new ControlInfoText(this._S1Imax, new CustomDoubleRule(0, 3)), new ControlInfoText(this._S1Uk, new CustomDoubleRule(0, 30)), new ControlInfoText(this._S1Ukm, new CustomDoubleRule(0, 30)), new ControlInfoText(this._uMinrdS1, RulesContainer.Ustavka150), new ControlInfoTextDependent(this._countSteps, validFunc1), new ControlInfoTextDependent(this._startStep, validFunc1), new ControlInfoTextDependent(this._endStep, validFunc1));

            this._section2Validator = new NewStructValidator<SectionStruct>(this._toolTip, new ControlInfoCombo(this._S2MeasureCB, Strings.MEASURE), new ControlInfoText(this._S2TN, RulesContainer.Ustavka128), new ControlInfoCombo(this._S2Ktn, Strings.KOEFF), new ControlInfoText(this._S2TNNP, RulesContainer.Ustavka128), new ControlInfoCombo(this._S2Ktnnp, Strings.KOEFF), new ControlInfoText(this._S2TTBB, new CustomUshortRule(0, 5000)), new ControlInfoText(this._S2TTCB, new CustomUshortRule(0, 5000)), new ControlInfoText(this._S2Up, RulesContainer.Ustavka150), new ControlInfoText(this._S2Up1, RulesContainer.Ustavka150), new ControlInfoText(this._S2Up2, RulesContainer.Ustavka150), new ControlInfoText(this._S2Up3, RulesContainer.Ustavka150), new ControlInfoText(this._S2dU, new CustomDoubleRule(0, 30)), new ControlInfoText(this._S2Umin, RulesContainer.Ustavka150), new ControlInfoText(this._S2Umax, RulesContainer.Ustavka150), new ControlInfoText(this._S2Un, new CustomDoubleRule(0, 60)), new ControlInfoText(this._S2Imax, new CustomDoubleRule(0, 3)), new ControlInfoText(this._S2Uk, new CustomDoubleRule(0, 30)), new ControlInfoText(this._S2Ukm, new CustomDoubleRule(0, 30)), new ControlInfoText(this._uMinrdS2, RulesContainer.Ustavka150), new ControlInfoText(new MaskedTextBox(), new CustomUshortRule(ushort.MinValue, ushort.MaxValue)), new ControlInfoText(new MaskedTextBox(), new CustomUshortRule(ushort.MinValue, ushort.MaxValue)), new ControlInfoText(new MaskedTextBox(), new CustomUshortRule(ushort.MinValue, ushort.MaxValue)));

            this._relayValidator = new NewDgwValidatior<AllRelayStruct, RelayStruct>(this._outputReleGrid, 12, this._toolTip, new ColumnInfoCombo(Strings.RelayNames, ColumnsType.NAME), new ColumnInfoCombo(Strings.RELE_TYPE), new ColumnInfoCombo(Strings.RELE_SIGNALS), new ColumnInfoText(RulesContainer.IntTo3M));

            this._disrepairValidator = new NewStructValidator<DisrepairStruct>(this._toolTip, new ControlInfoCheck(this._neispr1CB), new ControlInfoCheck(this._neispr2CB), new ControlInfoCheck(this._neispr3CB), new ControlInfoText(this._impTB, RulesContainer.IntTo3M));

            this._regValidator = new NewStructValidator<RegParamsStruct>(this._toolTip, new ControlInfoCombo(this._regMode, Strings.REG_MODE), new ControlInfoCombo(this._autoMode, Strings.AUTO_MODE), new ControlInfoCombo(this._key, Strings.YES_NO), new ControlInfoCombo(this._pilt, Strings.YES_NO), new ControlInfoCombo(this._handMode, Strings.HAND_MODE), new ControlInfoCombo(this._controlSdtu, Strings.YES_NO), new ControlInfoTextDependent(this._time1Com, validFunc2), new ControlInfoTextDependent(this._time2Com, validFunc2), new ControlInfoText(this._time3Com, RulesContainer.IntTo3M), new ControlInfoText(this._time4Com, RulesContainer.IntTo3M), new ControlInfoText(this._time5Com, RulesContainer.IntTo3M), new ControlInfoText(this._timeStart, new CustomUshortRule(0, 10000)), new ControlInfoText(this._timeEnd, new CustomUshortRule(0, 60000)), new ControlInfoText(this._timeSnSig, new CustomUshortRule(0, 2000)), new ControlInfoCombo(this._counter, Strings.COUNTER), new ControlInfoCheck(this._logCheck), new ControlInfoTextDependent(this._rStep, validFunc3));

            this._inputSignalsValidator = new NewStructValidator<InputSignalsStruct>(this._toolTip, new ControlInfoCombo(this._blockI, Strings.E_SIGNALS), new ControlInfoCombo(this._blockT, Strings.E_SIGNALS), new ControlInfoCombo(this._Up3, Strings.E_SIGNALS), new ControlInfoCombo(this._dropSign, Strings.E_SIGNALS), new ControlInfoCombo(this._kontrPitPrivod, Strings.E_SIGNALS), new ControlInfoCombo(this._dropBlock, Strings.E_SIGNALS), new ControlInfoCheck(this._fixNotGoCheck), new ControlInfoCheck(this._fixStickCheck), new ControlInfoCheck(this._fixRunCheck), new ControlInfoCheck(this._offNotGoCheck), new ControlInfoCheck(this._offStickCheck), new ControlInfoCheck(this._offRunCheck), new ControlInfoCombo(this._blockAutoSyp, Strings.E_SIGNALS));

            this._configurationValidator = new StructUnion<ConfigurationStruct>(this._section1Validator, this._section2Validator, this._relayValidator, this._disrepairValidator, this._regValidator, this._inputSignalsValidator);

            #endregion

            double vers = Common.VersionConverter(this._device.DeviceVersion);
            if (vers >= 1.02)
            {
                this._logCheck.Visible = this.logLabel.Visible = this.countStepLabel.Visible = this._countSteps.Visible = this._rStep.Visible = this.rStepLabel.Visible = this._startStep.Visible = this.startStepLabel.Visible = this._endStep.Visible = this.endStepLabel.Visible = true;
                this.timeLabel1.Visible = this.timeLabel2.Visible = this.timeLabel3.Visible = this.timeLabel4.Visible = this._time1Com.Visible = this._time2Com.Visible = true;
            }
            else
            {
                this._logCheck.Checked = false;
            }
            this._faultsGrBox.Visible = vers >= 1.04;
            if (vers >= 1.06 && vers < 2.0 || vers >= 2.01)
            {
                this._uMinrdLabelS1.Visible = this._uMinrdLabelS2.Visible = this._uMinrdS1.Visible = this._uMinrdS2.Visible = true;
                this._controlSdtu.Visible = this._controlSdtuLabel.Visible = true;
                this._blockAutoSyp.Visible = this._blockAutoSypLabel1.Visible = this._blockAutoSypLabel2.Visible = true;
            }
        }

        #endregion

        #region [MemoryEntity Events Handlers]

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this._processLabel.Text = READ_OK;
            this._currentSetpointsStruct = this._configuration.Value;
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            MessageBox.Show(READ_OK);
            this._configurationValidator.Set(this._currentSetpointsStruct);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            this._processLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0x0, true, "ConfirmConfig" + this._device.DeviceNumber, this._device);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._configProgressBar.Value = this._configProgressBar.Maximum;
            MessageBox.Show(WRITE_FAIL);
        }

        #endregion [MemoryEntity Events Handlers]

        #region Обработчики событий

        private void _S1MeasureCB_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_S1MeasureCB.SelectedItem.ToString() == Strings.MEASURE[1])
            {
                label3.Text = "ТН коэфф (Uab)";
                label4.Text = "ТН коэфф (Ubc)";
                label2.Text = "Kтн (Uab) ";
                label5.Text = "Kтн (Ubc) ";
            }
            else
            {
                label3.Text = "ТН коэфф.";
                label4.Text = "ТННП коэфф.";
                label2.Text = "Kтн";
                label5.Text = "Ктннп";
            }
        }

        private void _S2MeasureCB_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_S2MeasureCB.SelectedItem.ToString() == Strings.MEASURE[1])
            {
                label60.Text = "ТН коэфф (Uab)";
                label59.Text = "ТН коэфф (Ubc)";
                label61.Text = "Kтн (Uab) ";
                label58.Text = "Kтн (Ubc) ";
            }
            else
            {
                label60.Text = "ТН коэфф.";
                label59.Text = "ТННП коэфф.";
                label61.Text = "Kтн";
                label58.Text = "Ктннп";
            }
        }

        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._configProgressBar.Value = 0;
            this._processLabel.Text = "Идет запись конфигурации";
            this._counterAndResource.LoadStruct();
            this._configuration.LoadStruct();
        }

        private void MDO_Configuration_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
                this.LoadConfigurationBlocks();
        }

        private void _readButton_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.WriteConfig();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this._device.MB.NetworkEnabled)
            {
                if (this._device.MB.IsPortInvalid)
                {
                    MessageBox.Show(INVALID_PORT);
                    return;
                }
            }

            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 851 №{0}?", this._device.DeviceNumber), "Запись", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (this.WriteConfiguration())
                {
                    this._configProgressBar.Text = "Идёт запись конфигурации";
                    this.IsProcess = true;
                    this._configProgressBar.Value = 0;
                    this._configuration.Value = this._currentSetpointsStruct;
                    this._configuration.SaveStruct();
                }
            }
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;
            this._processLabel.Text = "Проверка уставок";
            if (this._configurationValidator.Check(out message, true)&& this._counterValidator.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationValidator.Get();
                this._counterMeasStruct = this._counterValidator.Get();
                return true;
            }
            else
            {
                MessageBox.Show(ERROR_SETPOINTS_VALUE);
                return false;
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveInFile();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР851_Уставки_версия {0}.bin", this._device.DeviceVersion);
            string message;

            if (this._counterValidator.Check(out message, true) && this._configurationValidator.Check(out message, true))
            {
                this._processLabel.Text = "Идет запись конфигурации в файл";
                ConfigurationStruct currentStruct = this._configurationValidator.Get();
                CounterMeasStruct currentMeasStruct = this._counterAndResource.Value;
                currentMeasStruct.Cur = ushort.Parse(this._stupen.Text);
                currentMeasStruct.Res = ushort.Parse(this._resource.Text);
                this._saveConfigurationDlg.FileName = string.Format("{0}_Уставки_версия{1}.bin", this._device.NodeName, this._device.DeviceVersion);
                if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    this.Serialize(this._saveConfigurationDlg.FileName, currentStruct, currentMeasStruct, MR851);
                }
            }
            else
            {
                MessageBox.Show(message ?? ERROR_SETPOINTS_VALUE);
            }
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._processLabel.Text = "Идет запись конфигурации в HTML";
                    this._currentSetpointsStruct.DeviceVersion = this._device.DeviceVersion;
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._currentSetpointsStruct.CurrentStage = this._stupen.Text;
                    this._currentSetpointsStruct.Resourse = this._resource.Text;
                    this._processLabel.Text = HtmlExport.ExportHtml(Resources.MR851Main, this._currentSetpointsStruct, "МР851", this._device.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._configProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName, MR851);
            }
        }

        private void _writeStupen_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string mes;
            if (this._counterValidator.Check(out mes, true))
            {
                if (DialogResult.Yes == MessageBox.Show("Записать ступень в МР851 №" + this._device.DeviceNumber + " ?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._currentOperation = WriteCounterResource.STEP;
                    this._counterAndResource.SaveOneWord(ushort.Parse(this._stupen.Text));
                }
            }
        }

        private void _writeResource_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string mes;
            if (this._counterValidator.Check(out mes, true))
            {
                if (DialogResult.Yes == MessageBox.Show("Записать ресурс в МР851 №" + this._device.DeviceNumber + " ?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._currentOperation = WriteCounterResource.RESOURCE;
                    this._counterAndResource.SaveOneWord(ushort.Parse(this._resource.Text), 1);
                }
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveToHtml();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Mr851ConfigurationForm_Shown(object sender, EventArgs e)
        {
            this._configurationValidator.Reset();
        }

        private void _logCheck_CheckedChanged(object sender, EventArgs e)
        {
            bool log = (sender as CheckBox).Checked;
            this._countSteps.Enabled = this._rStep.Enabled = this._startStep.Enabled = this._endStep.Enabled = log;
            this._resource.Enabled = this._writeResource.Enabled = this._stupen.Enabled = this._writeStupen.Enabled = !log;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="binFileName"></param>
        /// <param name="config"></param>
        /// <param name="counter"></param>
        /// <param name="head"></param>
        public void Serialize(string binFileName, StructBase config, CounterMeasStruct counter, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                var values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format(XML_HEAD, head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                doc.DocumentElement.AppendChild(element);

                element = doc.CreateElement("ROM_COUNT_CUR");
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(new[] {counter.Cur}, true));
                doc.DocumentElement.AppendChild(element);

                element = doc.CreateElement("ROM_COUNT_RES");
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(new[] {counter.Res}, true));
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._processLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        public void Deserialize(string binFileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                List<byte> values = new List<byte>();
                XmlNode a;
                bool old = doc.DocumentElement.Name == "МР851_уставки";
                if (old)
                {
                    values.AddRange(this.LoadOldConfig(doc, doc.DocumentElement.Name));
                }
                else
                {
                    a = doc.FirstChild.SelectSingleNode(string.Format(XML_HEAD, head));
                    values.AddRange(Convert.FromBase64String(a.InnerText));
                }

                ConfigurationStruct configurationStruct = new ConfigurationStruct();
                int size = configurationStruct.GetSize();
                if (values.Count < size) // для старых конфигураций сзади дописываю нули до нужного размера файла уставок
                {
                    int c = size - values.Count;
                    for (int i = 0; i < c; i++)
                    {
                        values.Add(0);
                    }
                }
                configurationStruct.InitStruct(values.ToArray());
                this._configurationValidator.Set(configurationStruct);

                a = doc.FirstChild.SelectSingleNode(string.Format("ROM_COUNT_CUR"));
                values.AddRange(Convert.FromBase64String(a.InnerText));
                this._stupen.Text = Common.TOWORDS(values.ToArray(), true)[values.Count / 2 - 1].ToString();

                a = doc.FirstChild.SelectSingleNode(string.Format("ROM_COUNT_RES"));
                values.AddRange(Convert.FromBase64String(a.InnerText));
                this._resource.Text = Common.TOWORDS(values.ToArray(), true)[values.Count / 2 - 1].ToString();

                this._processLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private byte[] LoadOldConfig(XmlDocument doc, string docElement)
        {
            List<byte> result = new List<byte>();
            docElement = string.Format("/{0}/", docElement);
            result.AddRange(this.DeserializeSlot(doc, docElement + "Section_1"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "Section_2"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "PARAMAUTOMAT"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "REG_PARAMS"));
            result.AddRange(this.DeserializeSlot(doc, docElement + "E_SYGNALS"));

            byte[] res = result.ToArray();
            Common.SwapArrayItems(ref res);
            return res;
        }

        private byte[] DeserializeSlot(XmlDocument doc, string nodePath)
        {
            XmlNode selectSingleNode = doc.SelectSingleNode(nodePath);
            return selectSingleNode != null ? Convert.FromBase64String(selectSingleNode.InnerText) : new byte[0];
        }

        private void Mr851ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.LoadConfigurationBlocks();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip) sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.LoadConfigurationBlocks();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
                return;
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (Mr851Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr851ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config1.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
