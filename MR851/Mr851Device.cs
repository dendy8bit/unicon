﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using System.Xml.Serialization;
using System.ComponentModel;
using BEMN.MR851.Configuration;
using BEMN.MR851.Configuration.Structures;
using BEMN.MR851.Measuring;
using BEMN.MR851.Measuring.Structures;
using BEMN.MR851.SystemJournal;
using BEMN.MR851.SystemJournal.Structures;
using BEMN.MBServer;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer.Queries;
using BEMN.Framework;

namespace BEMN.MR851
{
    public class Mr851Device : Device, IDeviceView, IDeviceVersion
    {
        #region Fields
        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<ConfigurationStruct> _measuringConfiguration;
        private MemoryEntity<AnalogDataBaseStruct> _analogBd;
        private MemoryEntity<CounterMeasStruct> _counterMeasuring;
        private MemoryEntity<CounterMeasStruct> _counterConfig;
        private MemoryEntity<DiscretDataBaseStruct> _discretBd;
        private MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<DateTimeStruct> _dateTime;
        #endregion

        #region Конструкторы инициализация
        public Mr851Device()
        {
            HaveVersion = true;
        }

        public Mr851Device(Modbus mb)
        {
            HaveVersion = true;
            this.MB = mb;
            this.InitAddr();
        }

        public override Modbus MB 
        {
            get { return mb; }
            set
            {
                mb = value;
                if (mb != null)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        private new void mb_CompleteExchange(object sender, Query query)
        {
            if (query.name == "ConfirmConfig" + DeviceNumber)
            {
                if (query.error)
                {
                    MessageBox.Show("Не удалось записать бит подтверждения изменения конфигурации. Конфигурация в устройстве не изменена",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        private void InitAddr()
        {
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация МР 851", this, 0x1000);
            this._measuringConfiguration = new MemoryEntity<ConfigurationStruct>("Параметры измерений", this, 0x1000);
            this._analogBd = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, 0x1900);
            this._discretBd = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, 0x1800);
            this._counterMeasuring = new MemoryEntity<CounterMeasStruct>("Счетчик измерения", this, 0x1A00);
            this._counterConfig = new MemoryEntity<CounterMeasStruct>("Счетчик конфигурация", this, 0x1A00);

            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы", this, 0x2100);
            this._refreshSystemJournal = new MemoryEntity<OneWordStruct>("Обновление журнала системы", this, 0x2000);
        }
        #endregion

        #region Properties
        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return this._configuration; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }
        public MemoryEntity<CounterMeasStruct> CounterConfig
        {
            get { return this._counterConfig; }
        }


        public MemoryEntity<AnalogDataBaseStruct> AnalogBd
        {
            get { return this._analogBd; }
        }

        public MemoryEntity<ConfigurationStruct> MeasuringConfiguration
        {
            get { return this._measuringConfiguration; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretBd
        {
            get { return this._discretBd; }
        }

        public MemoryEntity<CounterMeasStruct> Counter
        {
            get { return this._counterMeasuring; }
        }

        public MemoryEntity<OneWordStruct> RefreshSystemJournal
        {
            get { return this._refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        #endregion
        
        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Mr851Device); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr741;
            }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР851"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        #region IDeviceVersion members
        public Type[] Forms
        {
            get
            {
                double ver = Common.VersionConverter(DeviceVersion);
                if (ver >= 1.08)
                {
                    DefaultTimeSpan = new TimeSpan(0,0,0,0,25);
                }
                return new []
                {
                    typeof(Mr851ConfigurationForm),
                    typeof(Mr851MeasuringForm),
                    typeof(Mr851SystemJournalForm)
                };
            }
        }

        public List<string> Versions
        {
            get 
            { 
                return new List<string>
                {
                    "1.00",
                    "1.01",
                    "1.02",
                    "1.03",
                    "1.04",
                    "1.05",
                    "1.06",
                    "1.08",
                    "2.00",
                    "2.01",
                    "2.02",
                    "2.03",
                    "2.04",
                    "2.05",
                    "2.06",
                    "2.07"
                };
            }
        }
        #endregion
    }
}
