﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR851.Properties;
using BEMN.MR851.SystemJournal.Structures;

namespace BEMN.MR851.SystemJournal
{
    public partial class Mr851SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР851_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _refreshSystemJournal;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private Mr851Device _device;
        #endregion [Private fields]

        public Mr851SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr851SystemJournalForm(Mr851Device device)
        {
            this.InitializeComponent();
            this._device = device;

            this._systemJournal = device.SystemJournal;
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);

            this._refreshSystemJournal = device.RefreshSystemJournal;
            this._refreshSystemJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._systemJournal.LoadStruct);
            this._refreshSystemJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr851Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr851SystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR851.Properties.Resources.js1; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]

        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }

        #endregion [Properties]


        #region [Help members]
        private bool ButtonsEnabled
        {
            set
            {
                this._readJournalButton.Enabled = this._saveJournalButton.Enabled =
                    this._loadJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void StartReadJournal()
        {
            this.RecordNumber = 0;
            this.SaveNum();
        }

        private void SaveNum()
        {
            this._refreshSystemJournal.Value.Word = (ushort) RecordNumber;
            this._refreshSystemJournal.SaveStruct6();
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime, 
                    this._systemJournal.Value.GetRecordMessage);
                this.SaveNum();
            }
            else
            {
                this.ButtonsEnabled = true;
            }
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    SystemJournalStruct journal = new SystemJournalStruct();
                    int size = journal.GetSize();
                    int index = 0;
                    this.RecordNumber = 0;
                    while (index < file.Length)
                    {
                        List<byte> journalBytes = new List<byte>();
                        journalBytes.AddRange(Common.SwapListItems(file.Skip(index).Take(size)));
                        journal.InitStruct(journalBytes.ToArray());
                        this.RecordNumber++;
                        this._dataTable.Rows.Add(
                            this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                            journal.GetRecordTime,
                            journal.GetRecordMessage);
                        index += size;
                    }
                }
                else
                {
                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                    this.RecordNumber = this._dataTable.Rows.Count;
                    this._systemJournalGrid.Refresh();
                }
            }
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void Mr851SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournal();
        }

        private void LoadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this.ButtonsEnabled = false;
            this.StartReadJournal();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR851SJ);
               this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
