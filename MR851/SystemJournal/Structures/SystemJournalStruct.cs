﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.SystemJournal.Structures
{
  
    public class SystemJournalStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";

        #endregion [Constants]


        #region [Private fields]
        [Layout(0)] private ushort _message;
        [Layout(1)] private ushort _year;
        [Layout(2)] private ushort _month;
        [Layout(3)] private ushort _date;
        [Layout(4)] private ushort _hour;
        [Layout(5)] private ushort _minute;
        [Layout(6)] private ushort _second;
        [Layout(7)] private ushort _millisecond;


        public List<string> MessagesList { get; set; }
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year +
                         this.Month +
                         this.Date +
                         this.Hour +
                         this.Minute +
                         this.Second +
                         this.Millisecond +
                         this.Message;

                return sum == 0;

                //return  this.Message == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetRecordTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );

            }
        }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string GetRecordMessage
        {
            //get
            //{

            //    if (this.Message < Strings.JOURNAL.Count)
            //        return Strings.JOURNAL[this.Message];

            //    return string.Format("Код - {0}", this.Message);
            //}

            get
            {
                if (this.Message < StringsSJ.Message.Count)
                {
                    return StringsSJ.Message[this.Message];
                }
                return string.Format("Код - {0}", this.Message);
            }

        }

        public ushort Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public ushort Month
        {
            get { return _month; }
            set { _month = value; }
        }

        public ushort Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public ushort Hour
        {
            get { return _hour; }
            set { _hour = value; }
        }

        public ushort Minute
        {
            get { return _minute; }
            set { _minute = value; }
        }

        public ushort Second
        {
            get { return _second; }
            set { _second = value; }
        }

        public ushort Millisecond
        {
            get { return _millisecond; }
            set { _millisecond = value; }
        }


        public ushort Message
        {
            get { return _message; }
            set { _message = value; }
        }

        #endregion [Properties]



    }
}
