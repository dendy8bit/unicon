namespace BEMN.MR851.Measuring
{
    partial class Mr851MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this.label122 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._resetBlockBtn = new System.Windows.Forms.Button();
            this._resetSignalBtn = new System.Windows.Forms.Button();
            this._subBtn = new System.Windows.Forms.Button();
            this._addBtn = new System.Windows.Forms.Button();
            this._colorLed2 = new BEMN.Forms.LedControl();
            this._colorLed1 = new BEMN.Forms.LedControl();
            this._sygnalState5 = new BEMN.Forms.LedControl();
            this._resetNewRecJSBtn = new System.Windows.Forms.Button();
            this._resetNeisprJSBtn = new System.Windows.Forms.Button();
            this._labelColor2 = new System.Windows.Forms.Label();
            this._labelColor1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._sygnalState8 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._sygnalState1 = new BEMN.Forms.LedControl();
            this._sygnalState7 = new BEMN.Forms.LedControl();
            this._sygnalState2 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._sygnalState3 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._sygnalState6 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._sygnalState4 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._d16 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._d13 = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._d12 = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._d9 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._d8 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._d7 = new BEMN.Forms.LedControl();
            this.label11 = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._d5 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this._d3 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._d2 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._d1 = new BEMN.Forms.LedControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this._logicSignal9 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._logicSignal8 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._logicSignal7 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this._logicSignal6 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._logicSignal5 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._logicSignal4 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._logicSignal3 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._logicSignal2 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._logicSignal1 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.ucLabel = new System.Windows.Forms.Label();
            this._Res = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this._Upod = new System.Windows.Forms.TextBox();
            this._N = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this._Uc1c2 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this._Uab2 = new System.Windows.Forms.TextBox();
            this._U22 = new System.Windows.Forms.TextBox();
            this._Unbc2 = new System.Windows.Forms.TextBox();
            this.u2Label = new System.Windows.Forms.Label();
            this._Ibb2 = new System.Windows.Forms.TextBox();
            this._Icb2 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label52 = new System.Windows.Forms.Label();
            this._Uab1 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.u1Label = new System.Windows.Forms.Label();
            this._Unbc1 = new System.Windows.Forms.TextBox();
            this._U21 = new System.Windows.Forms.TextBox();
            this._Ibb1 = new System.Windows.Forms.TextBox();
            this._Icb1 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._blockSignal1 = new BEMN.Forms.LedControl();
            this._blockSignal2 = new BEMN.Forms.LedControl();
            this._blockSignal3 = new BEMN.Forms.LedControl();
            this._blockSignal4 = new BEMN.Forms.LedControl();
            this._blockSignal5 = new BEMN.Forms.LedControl();
            this._blockSignal6 = new BEMN.Forms.LedControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this._errLed13 = new BEMN.Forms.LedControl();
            this._errLed15 = new BEMN.Forms.LedControl();
            this._errLed16 = new BEMN.Forms.LedControl();
            this._errLed14 = new BEMN.Forms.LedControl();
            this._errLed6 = new BEMN.Forms.LedControl();
            this._errLed7 = new BEMN.Forms.LedControl();
            this._errLed11 = new BEMN.Forms.LedControl();
            this._errLed12 = new BEMN.Forms.LedControl();
            this._errLed1 = new BEMN.Forms.LedControl();
            this._errLed8 = new BEMN.Forms.LedControl();
            this._errLed5 = new BEMN.Forms.LedControl();
            this._errLed10 = new BEMN.Forms.LedControl();
            this._errLed2 = new BEMN.Forms.LedControl();
            this._errLed9 = new BEMN.Forms.LedControl();
            this._errLed4 = new BEMN.Forms.LedControl();
            this._errLed3 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this._logicSignal10 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this._neisprLedDev2 = new BEMN.Forms.LedControl();
            this._neisprLedDev1 = new BEMN.Forms.LedControl();
            this._neisprLedDev8 = new BEMN.Forms.LedControl();
            this._neisprLedDev7 = new BEMN.Forms.LedControl();
            this._neisprLedDev4 = new BEMN.Forms.LedControl();
            this._neisprLedDev6 = new BEMN.Forms.LedControl();
            this._neisprLedDev5 = new BEMN.Forms.LedControl();
            this._neisprLedDev3 = new BEMN.Forms.LedControl();
            this._neispr8rez = new BEMN.Forms.LedControl();
            this._neispr7rez = new BEMN.Forms.LedControl();
            this._neispr6rez = new BEMN.Forms.LedControl();
            this._neispr1res = new BEMN.Forms.LedControl();
            this._neispr5res = new BEMN.Forms.LedControl();
            this._neispr2 = new BEMN.Forms.LedControl();
            this._neispr4 = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._neispr3 = new BEMN.Forms.LedControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this._r12 = new BEMN.Forms.LedControl();
            this._r7 = new BEMN.Forms.LedControl();
            this._r11 = new BEMN.Forms.LedControl();
            this._r10 = new BEMN.Forms.LedControl();
            this._r6 = new BEMN.Forms.LedControl();
            this._r9 = new BEMN.Forms.LedControl();
            this._r8 = new BEMN.Forms.LedControl();
            this._r5 = new BEMN.Forms.LedControl();
            this._rMinus = new BEMN.Forms.LedControl();
            this._r4 = new BEMN.Forms.LedControl();
            this._r3 = new BEMN.Forms.LedControl();
            this._rPlus = new BEMN.Forms.LedControl();
            this._r2 = new BEMN.Forms.LedControl();
            this._r1 = new BEMN.Forms.LedControl();
            this._modeGroup = new System.Windows.Forms.GroupBox();
            this._setSypBtn = new System.Windows.Forms.Button();
            this._resetSypBtn = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this._modeLed3 = new BEMN.Forms.LedControl();
            this._modeLed2 = new BEMN.Forms.LedControl();
            this._modeLed1 = new BEMN.Forms.LedControl();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._modeGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label122.Location = new System.Drawing.Point(29, 18);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(62, 13);
            this.label122.TabIndex = 91;
            this.label122.Text = "���������";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._resetBlockBtn);
            this.groupBox1.Controls.Add(this._resetSignalBtn);
            this.groupBox1.Controls.Add(this._subBtn);
            this.groupBox1.Controls.Add(this._addBtn);
            this.groupBox1.Controls.Add(this._colorLed2);
            this.groupBox1.Controls.Add(this._colorLed1);
            this.groupBox1.Controls.Add(this._sygnalState5);
            this.groupBox1.Controls.Add(this._resetNewRecJSBtn);
            this.groupBox1.Controls.Add(this._resetNeisprJSBtn);
            this.groupBox1.Controls.Add(this._labelColor2);
            this.groupBox1.Controls.Add(this._labelColor1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._sygnalState8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this._sygnalState1);
            this.groupBox1.Controls.Add(this._sygnalState7);
            this.groupBox1.Controls.Add(this._sygnalState2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._sygnalState3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._sygnalState6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._sygnalState4);
            this.groupBox1.Controls.Add(this.label122);
            this.groupBox1.Location = new System.Drawing.Point(384, 287);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(222, 181);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ��������";
            // 
            // _resetBlockBtn
            // 
            this._resetBlockBtn.Location = new System.Drawing.Point(167, 71);
            this._resetBlockBtn.Name = "_resetBlockBtn";
            this._resetBlockBtn.Size = new System.Drawing.Size(47, 20);
            this._resetBlockBtn.TabIndex = 109;
            this._resetBlockBtn.Text = "�����";
            this._resetBlockBtn.UseVisualStyleBackColor = true;
            this._resetBlockBtn.Click += new System.EventHandler(this._resetBlockBtn_Click);
            // 
            // _resetSignalBtn
            // 
            this._resetSignalBtn.Location = new System.Drawing.Point(167, 147);
            this._resetSignalBtn.Name = "_resetSignalBtn";
            this._resetSignalBtn.Size = new System.Drawing.Size(47, 20);
            this._resetSignalBtn.TabIndex = 109;
            this._resetSignalBtn.Text = "�����";
            this._resetSignalBtn.UseVisualStyleBackColor = true;
            this._resetSignalBtn.Click += new System.EventHandler(this._resetSignalBtn_Click);
            // 
            // _subBtn
            // 
            this._subBtn.Location = new System.Drawing.Point(140, 33);
            this._subBtn.Name = "_subBtn";
            this._subBtn.Size = new System.Drawing.Size(74, 20);
            this._subBtn.TabIndex = 109;
            this._subBtn.Text = "�������";
            this._subBtn.UseVisualStyleBackColor = true;
            this._subBtn.Visible = false;
            this._subBtn.Click += new System.EventHandler(this._subBtn_Click);
            // 
            // _addBtn
            // 
            this._addBtn.Location = new System.Drawing.Point(140, 14);
            this._addBtn.Name = "_addBtn";
            this._addBtn.Size = new System.Drawing.Size(74, 20);
            this._addBtn.TabIndex = 109;
            this._addBtn.Text = "���������";
            this._addBtn.UseVisualStyleBackColor = true;
            this._addBtn.Visible = false;
            this._addBtn.Click += new System.EventHandler(this._addBtn_Click);
            // 
            // _colorLed2
            // 
            this._colorLed2.BackColor = System.Drawing.Color.Transparent;
            this._colorLed2.Location = new System.Drawing.Point(97, 94);
            this._colorLed2.Name = "_colorLed2";
            this._colorLed2.Size = new System.Drawing.Size(13, 13);
            this._colorLed2.State = BEMN.Forms.LedState.NoSignaled;
            this._colorLed2.TabIndex = 98;
            // 
            // _colorLed1
            // 
            this._colorLed1.BackColor = System.Drawing.Color.Transparent;
            this._colorLed1.Location = new System.Drawing.Point(29, 94);
            this._colorLed1.Name = "_colorLed1";
            this._colorLed1.Size = new System.Drawing.Size(13, 13);
            this._colorLed1.State = BEMN.Forms.LedState.Signaled;
            this._colorLed1.TabIndex = 98;
            // 
            // _sygnalState5
            // 
            this._sygnalState5.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState5.Location = new System.Drawing.Point(10, 94);
            this._sygnalState5.Name = "_sygnalState5";
            this._sygnalState5.Size = new System.Drawing.Size(13, 13);
            this._sygnalState5.State = BEMN.Forms.LedState.Off;
            this._sygnalState5.TabIndex = 98;
            // 
            // _resetNewRecJSBtn
            // 
            this._resetNewRecJSBtn.Location = new System.Drawing.Point(167, 128);
            this._resetNewRecJSBtn.Name = "_resetNewRecJSBtn";
            this._resetNewRecJSBtn.Size = new System.Drawing.Size(47, 20);
            this._resetNewRecJSBtn.TabIndex = 109;
            this._resetNewRecJSBtn.Text = "�����";
            this._resetNewRecJSBtn.UseVisualStyleBackColor = true;
            this._resetNewRecJSBtn.Click += new System.EventHandler(this._resetNewRecJSBtn_Click);
            // 
            // _resetNeisprJSBtn
            // 
            this._resetNeisprJSBtn.Location = new System.Drawing.Point(167, 109);
            this._resetNeisprJSBtn.Name = "_resetNeisprJSBtn";
            this._resetNeisprJSBtn.Size = new System.Drawing.Size(47, 20);
            this._resetNeisprJSBtn.TabIndex = 109;
            this._resetNeisprJSBtn.Text = "�����";
            this._resetNeisprJSBtn.UseVisualStyleBackColor = true;
            this._resetNeisprJSBtn.Click += new System.EventHandler(this._resetNeisprJSBtn_Click);
            // 
            // _labelColor2
            // 
            this._labelColor2.AutoSize = true;
            this._labelColor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelColor2.Location = new System.Drawing.Point(112, 94);
            this._labelColor2.Name = "_labelColor2";
            this._labelColor2.Size = new System.Drawing.Size(36, 13);
            this._labelColor2.TabIndex = 107;
            this._labelColor2.Text = "- ����";
            // 
            // _labelColor1
            // 
            this._labelColor1.AutoSize = true;
            this._labelColor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelColor1.Location = new System.Drawing.Point(44, 94);
            this._labelColor1.Name = "_labelColor1";
            this._labelColor1.Size = new System.Drawing.Size(47, 13);
            this._labelColor1.TabIndex = 107;
            this._labelColor1.Text = "- ������";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(29, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 105;
            this.label5.Text = "������������";
            // 
            // _sygnalState8
            // 
            this._sygnalState8.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState8.Location = new System.Drawing.Point(10, 151);
            this._sygnalState8.Name = "_sygnalState8";
            this._sygnalState8.Size = new System.Drawing.Size(13, 13);
            this._sygnalState8.State = BEMN.Forms.LedState.Off;
            this._sygnalState8.TabIndex = 104;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(29, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 103;
            this.label6.Text = "����� ������. � ��";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(29, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 101;
            this.label7.Text = "����� ������ � ��";
            // 
            // _sygnalState1
            // 
            this._sygnalState1.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState1.Location = new System.Drawing.Point(10, 18);
            this._sygnalState1.Name = "_sygnalState1";
            this._sygnalState1.Size = new System.Drawing.Size(13, 13);
            this._sygnalState1.State = BEMN.Forms.LedState.Off;
            this._sygnalState1.TabIndex = 90;
            // 
            // _sygnalState7
            // 
            this._sygnalState7.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState7.Location = new System.Drawing.Point(10, 132);
            this._sygnalState7.Name = "_sygnalState7";
            this._sygnalState7.Size = new System.Drawing.Size(13, 13);
            this._sygnalState7.State = BEMN.Forms.LedState.Off;
            this._sygnalState7.TabIndex = 102;
            // 
            // _sygnalState2
            // 
            this._sygnalState2.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState2.Location = new System.Drawing.Point(10, 37);
            this._sygnalState2.Name = "_sygnalState2";
            this._sygnalState2.Size = new System.Drawing.Size(13, 13);
            this._sygnalState2.State = BEMN.Forms.LedState.Off;
            this._sygnalState2.TabIndex = 92;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(29, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 97;
            this.label4.Text = "����������";
            // 
            // _sygnalState3
            // 
            this._sygnalState3.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState3.Location = new System.Drawing.Point(10, 56);
            this._sygnalState3.Name = "_sygnalState3";
            this._sygnalState3.Size = new System.Drawing.Size(13, 13);
            this._sygnalState3.State = BEMN.Forms.LedState.Off;
            this._sygnalState3.TabIndex = 94;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(29, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 95;
            this.label3.Text = "�������������";
            // 
            // _sygnalState6
            // 
            this._sygnalState6.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState6.Location = new System.Drawing.Point(10, 113);
            this._sygnalState6.Name = "_sygnalState6";
            this._sygnalState6.Size = new System.Drawing.Size(13, 13);
            this._sygnalState6.State = BEMN.Forms.LedState.Off;
            this._sygnalState6.TabIndex = 100;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(29, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 93;
            this.label2.Text = "�������";
            // 
            // _sygnalState4
            // 
            this._sygnalState4.BackColor = System.Drawing.Color.Transparent;
            this._sygnalState4.Location = new System.Drawing.Point(10, 75);
            this._sygnalState4.Name = "_sygnalState4";
            this._sygnalState4.Size = new System.Drawing.Size(13, 13);
            this._sygnalState4.State = BEMN.Forms.LedState.Off;
            this._sygnalState4.TabIndex = 96;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.Location = new System.Drawing.Point(25, 19);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(31, 13);
            this.label49.TabIndex = 107;
            this.label49.Text = "����";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(25, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 99;
            this.label8.Text = "������";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this._d16);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this._d15);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this._d14);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this._d13);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this._d12);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this._d11);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this._d10);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this._d9);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this._d8);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this._d7);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this._d6);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this._d5);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this._d4);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this._d3);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this._d2);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this._d1);
            this.groupBox2.Location = new System.Drawing.Point(262, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(369, 144);
            this.groupBox2.TabIndex = 93;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������� ���������� �������";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(208, 124);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 13);
            this.label17.TabIndex = 137;
            this.label17.Text = "���� \"�4\"";
            // 
            // _d16
            // 
            this._d16.BackColor = System.Drawing.Color.Transparent;
            this._d16.Location = new System.Drawing.Point(189, 124);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 136;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(208, 109);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 135;
            this.label18.Text = "���� \"�3\"";
            // 
            // _d15
            // 
            this._d15.BackColor = System.Drawing.Color.Transparent;
            this._d15.Location = new System.Drawing.Point(189, 109);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 134;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(208, 94);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 13);
            this.label19.TabIndex = 133;
            this.label19.Text = "���� \"�2\"";
            // 
            // _d14
            // 
            this._d14.BackColor = System.Drawing.Color.Transparent;
            this._d14.Location = new System.Drawing.Point(189, 94);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 132;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(208, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 13);
            this.label20.TabIndex = 131;
            this.label20.Text = "���� \"�1\"";
            // 
            // _d13
            // 
            this._d13.BackColor = System.Drawing.Color.Transparent;
            this._d13.Location = new System.Drawing.Point(189, 79);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 130;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(208, 64);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 13);
            this.label21.TabIndex = 129;
            this.label21.Text = "���� \"U�2\"";
            // 
            // _d12
            // 
            this._d12.BackColor = System.Drawing.Color.Transparent;
            this._d12.Location = new System.Drawing.Point(189, 64);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 128;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(208, 49);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 127;
            this.label22.Text = "���� \"U�1\"";
            // 
            // _d11
            // 
            this._d11.BackColor = System.Drawing.Color.Transparent;
            this._d11.Location = new System.Drawing.Point(189, 49);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 126;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(208, 34);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(105, 13);
            this.label23.TabIndex = 125;
            this.label23.Text = "���� \"����������\"";
            // 
            // _d10
            // 
            this._d10.BackColor = System.Drawing.Color.Transparent;
            this._d10.Location = new System.Drawing.Point(189, 34);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 124;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(208, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(156, 13);
            this.label24.TabIndex = 123;
            this.label24.Text = "���� \"�������� ���� ������\"";
            // 
            // _d9
            // 
            this._d9.BackColor = System.Drawing.Color.Transparent;
            this._d9.Location = new System.Drawing.Point(189, 19);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 122;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(28, 124);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 13);
            this.label9.TabIndex = 121;
            this.label9.Text = "���� \"�������� 2-� ������\"";
            // 
            // _d8
            // 
            this._d8.BackColor = System.Drawing.Color.Transparent;
            this._d8.Location = new System.Drawing.Point(9, 124);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 120;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(28, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(149, 13);
            this.label10.TabIndex = 119;
            this.label10.Text = "���� \"�������� 1-� ������\"";
            // 
            // _d7
            // 
            this._d7.BackColor = System.Drawing.Color.Transparent;
            this._d7.Location = new System.Drawing.Point(9, 109);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 118;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(28, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 117;
            this.label11.Text = "���� \"���. �����\"";
            // 
            // _d6
            // 
            this._d6.BackColor = System.Drawing.Color.Transparent;
            this._d6.Location = new System.Drawing.Point(9, 94);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 116;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(28, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 13);
            this.label12.TabIndex = 115;
            this.label12.Text = "���� \"������������\"";
            // 
            // _d5
            // 
            this._d5.BackColor = System.Drawing.Color.Transparent;
            this._d5.Location = new System.Drawing.Point(9, 79);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 114;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(28, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 13);
            this.label13.TabIndex = 113;
            this.label13.Text = "���� \"������ �������\"";
            // 
            // _d4
            // 
            this._d4.BackColor = System.Drawing.Color.Transparent;
            this._d4.Location = new System.Drawing.Point(9, 64);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 112;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(28, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(136, 13);
            this.label14.TabIndex = 111;
            this.label14.Text = "���� \"������ ���������\"";
            // 
            // _d3
            // 
            this._d3.BackColor = System.Drawing.Color.Transparent;
            this._d3.Location = new System.Drawing.Point(9, 49);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 110;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(28, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 13);
            this.label15.TabIndex = 109;
            this.label15.Text = "���� \"�������\"";
            // 
            // _d2
            // 
            this._d2.BackColor = System.Drawing.Color.Transparent;
            this._d2.Location = new System.Drawing.Point(9, 34);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 108;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(28, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 13);
            this.label16.TabIndex = 107;
            this.label16.Text = "���� \"���������\"";
            // 
            // _d1
            // 
            this._d1.BackColor = System.Drawing.Color.Transparent;
            this._d1.Location = new System.Drawing.Point(9, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 106;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this._logicSignal9);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Controls.Add(this._logicSignal8);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this._logicSignal7);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this._logicSignal6);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this._logicSignal5);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this._logicSignal4);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this._logicSignal3);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Controls.Add(this._logicSignal2);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this._logicSignal1);
            this.groupBox8.Location = new System.Drawing.Point(8, 246);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(222, 99);
            this.groupBox8.TabIndex = 96;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "���������� �������";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(138, 64);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(81, 13);
            this.label39.TabIndex = 119;
            this.label39.Text = "������ �� U�3";
            // 
            // _logicSignal9
            // 
            this._logicSignal9.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal9.Location = new System.Drawing.Point(119, 64);
            this._logicSignal9.Name = "_logicSignal9";
            this._logicSignal9.Size = new System.Drawing.Size(13, 13);
            this._logicSignal9.State = BEMN.Forms.LedState.Off;
            this._logicSignal9.TabIndex = 118;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(138, 49);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 13);
            this.label36.TabIndex = 117;
            this.label36.Text = "������ �� U�2";
            // 
            // _logicSignal8
            // 
            this._logicSignal8.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal8.Location = new System.Drawing.Point(119, 49);
            this._logicSignal8.Name = "_logicSignal8";
            this._logicSignal8.Size = new System.Drawing.Size(13, 13);
            this._logicSignal8.State = BEMN.Forms.LedState.Off;
            this._logicSignal8.TabIndex = 116;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(138, 34);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(81, 13);
            this.label37.TabIndex = 115;
            this.label37.Text = "������ �� U�1";
            // 
            // _logicSignal7
            // 
            this._logicSignal7.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal7.Location = new System.Drawing.Point(119, 34);
            this._logicSignal7.Name = "_logicSignal7";
            this._logicSignal7.Size = new System.Drawing.Size(13, 13);
            this._logicSignal7.State = BEMN.Forms.LedState.Off;
            this._logicSignal7.TabIndex = 114;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(138, 19);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(75, 13);
            this.label34.TabIndex = 113;
            this.label34.Text = "������ �� U�";
            // 
            // _logicSignal6
            // 
            this._logicSignal6.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal6.Location = new System.Drawing.Point(119, 19);
            this._logicSignal6.Name = "_logicSignal6";
            this._logicSignal6.Size = new System.Drawing.Size(13, 13);
            this._logicSignal6.State = BEMN.Forms.LedState.Off;
            this._logicSignal6.TabIndex = 112;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(25, 79);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(67, 13);
            this.label35.TabIndex = 111;
            this.label35.Text = "����������";
            // 
            // _logicSignal5
            // 
            this._logicSignal5.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal5.Location = new System.Drawing.Point(6, 79);
            this._logicSignal5.Name = "_logicSignal5";
            this._logicSignal5.Size = new System.Drawing.Size(13, 13);
            this._logicSignal5.State = BEMN.Forms.LedState.Off;
            this._logicSignal5.TabIndex = 110;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(25, 64);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(61, 13);
            this.label32.TabIndex = 109;
            this.label32.Text = "U < �����";
            // 
            // _logicSignal4
            // 
            this._logicSignal4.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal4.Location = new System.Drawing.Point(6, 64);
            this._logicSignal4.Name = "_logicSignal4";
            this._logicSignal4.Size = new System.Drawing.Size(13, 13);
            this._logicSignal4.State = BEMN.Forms.LedState.Off;
            this._logicSignal4.TabIndex = 108;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(25, 49);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(61, 13);
            this.label33.TabIndex = 107;
            this.label33.Text = "U > �����";
            // 
            // _logicSignal3
            // 
            this._logicSignal3.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal3.Location = new System.Drawing.Point(6, 49);
            this._logicSignal3.Name = "_logicSignal3";
            this._logicSignal3.Size = new System.Drawing.Size(13, 13);
            this._logicSignal3.State = BEMN.Forms.LedState.Off;
            this._logicSignal3.TabIndex = 106;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(25, 34);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 13);
            this.label30.TabIndex = 105;
            this.label30.Text = "����� ������ 2";
            // 
            // _logicSignal2
            // 
            this._logicSignal2.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal2.Location = new System.Drawing.Point(6, 34);
            this._logicSignal2.Name = "_logicSignal2";
            this._logicSignal2.Size = new System.Drawing.Size(13, 13);
            this._logicSignal2.State = BEMN.Forms.LedState.Off;
            this._logicSignal2.TabIndex = 104;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(25, 19);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(88, 13);
            this.label31.TabIndex = 103;
            this.label31.Text = "����� ������ 1";
            // 
            // _logicSignal1
            // 
            this._logicSignal1.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal1.Location = new System.Drawing.Point(6, 19);
            this._logicSignal1.Name = "_logicSignal1";
            this._logicSignal1.Size = new System.Drawing.Size(13, 13);
            this._logicSignal1.State = BEMN.Forms.LedState.Off;
            this._logicSignal1.TabIndex = 102;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(25, 64);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(115, 13);
            this.label38.TabIndex = 121;
            this.label38.Text = "��� ������� �������";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(25, 109);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(102, 13);
            this.label42.TabIndex = 129;
            this.label42.Text = "������ \"�������\"";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.Location = new System.Drawing.Point(25, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(105, 13);
            this.label43.TabIndex = 127;
            this.label43.Text = "������ \"�� �����\"";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(25, 49);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(131, 13);
            this.label41.TabIndex = 123;
            this.label41.Text = "������������� �������";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(25, 94);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 13);
            this.label28.TabIndex = 101;
            this.label28.Text = "�� Umax";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(25, 79);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(48, 13);
            this.label29.TabIndex = 99;
            this.label29.Text = "�� Umin";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(25, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 97;
            this.label26.Text = "�� U2";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(25, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 13);
            this.label27.TabIndex = 95;
            this.label27.Text = "�� Un";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(25, 34);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(89, 13);
            this.label25.TabIndex = 93;
            this.label25.Text = "�� �����������";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(25, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 91;
            this.label1.Text = "�� ����";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox11);
            this.groupBox3.Controls.Add(this.groupBox10);
            this.groupBox3.Controls.Add(this.groupBox9);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 237);
            this.groupBox3.TabIndex = 97;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "���������� ���� ������";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.ucLabel);
            this.groupBox11.Controls.Add(this._Res);
            this.groupBox11.Controls.Add(this.label54);
            this.groupBox11.Controls.Add(this.labelX);
            this.groupBox11.Controls.Add(this._Upod);
            this.groupBox11.Controls.Add(this._N);
            this.groupBox11.Controls.Add(this.label53);
            this.groupBox11.Controls.Add(this._Uc1c2);
            this.groupBox11.Location = new System.Drawing.Point(6, 15);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(236, 65);
            this.groupBox11.TabIndex = 102;
            this.groupBox11.TabStop = false;
            // 
            // ucLabel
            // 
            this.ucLabel.AutoSize = true;
            this.ucLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ucLabel.Location = new System.Drawing.Point(6, 16);
            this.ucLabel.Name = "ucLabel";
            this.ucLabel.Size = new System.Drawing.Size(52, 13);
            this.ucLabel.TabIndex = 115;
            this.ucLabel.Text = "Uc1/Uc2";
            // 
            // _Res
            // 
            this._Res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Res.Enabled = false;
            this._Res.Location = new System.Drawing.Point(180, 40);
            this._Res.Name = "_Res";
            this._Res.ReadOnly = true;
            this._Res.Size = new System.Drawing.Size(52, 20);
            this._Res.TabIndex = 101;
            this._Res.Text = "0";
            this._Res.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label54.Location = new System.Drawing.Point(6, 42);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(33, 13);
            this.label54.TabIndex = 117;
            this.label54.Text = "U���";
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelX.Location = new System.Drawing.Point(123, 42);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(43, 13);
            this.labelX.TabIndex = 103;
            this.labelX.Text = "������";
            // 
            // _Upod
            // 
            this._Upod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Upod.Enabled = false;
            this._Upod.Location = new System.Drawing.Point(62, 40);
            this._Upod.Name = "_Upod";
            this._Upod.ReadOnly = true;
            this._Upod.Size = new System.Drawing.Size(52, 20);
            this._Upod.TabIndex = 116;
            this._Upod.Text = "0";
            this._Upod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _N
            // 
            this._N.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._N.Enabled = false;
            this._N.Location = new System.Drawing.Point(180, 14);
            this._N.Name = "_N";
            this._N.ReadOnly = true;
            this._N.Size = new System.Drawing.Size(52, 20);
            this._N.TabIndex = 112;
            this._N.Text = "0";
            this._N.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label53.Location = new System.Drawing.Point(123, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(56, 13);
            this.label53.TabIndex = 113;
            this.label53.Text = "N�������";
            // 
            // _Uc1c2
            // 
            this._Uc1c2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Uc1c2.Enabled = false;
            this._Uc1c2.Location = new System.Drawing.Point(62, 14);
            this._Uc1c2.Name = "_Uc1c2";
            this._Uc1c2.ReadOnly = true;
            this._Uc1c2.Size = new System.Drawing.Size(52, 20);
            this._Uc1c2.TabIndex = 114;
            this._Uc1c2.Text = "0";
            this._Uc1c2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label50);
            this.groupBox10.Controls.Add(this.label51);
            this.groupBox10.Controls.Add(this._Uab2);
            this.groupBox10.Controls.Add(this._U22);
            this.groupBox10.Controls.Add(this._Unbc2);
            this.groupBox10.Controls.Add(this.u2Label);
            this.groupBox10.Controls.Add(this._Ibb2);
            this.groupBox10.Controls.Add(this._Icb2);
            this.groupBox10.Controls.Add(this.label47);
            this.groupBox10.Controls.Add(this.label48);
            this.groupBox10.Location = new System.Drawing.Point(132, 86);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(110, 145);
            this.groupBox10.TabIndex = 102;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "������ 2";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.Location = new System.Drawing.Point(6, 18);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(33, 13);
            this.label50.TabIndex = 98;
            this.label50.Text = "Uab2";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label51.Location = new System.Drawing.Point(7, 69);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(27, 13);
            this.label51.TabIndex = 121;
            this.label51.Text = "U22";
            // 
            // _Uab2
            // 
            this._Uab2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Uab2.Enabled = false;
            this._Uab2.Location = new System.Drawing.Point(54, 15);
            this._Uab2.Name = "_Uab2";
            this._Uab2.ReadOnly = true;
            this._Uab2.Size = new System.Drawing.Size(52, 20);
            this._Uab2.TabIndex = 15;
            this._Uab2.Text = "0";
            this._Uab2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _U22
            // 
            this._U22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U22.Enabled = false;
            this._U22.Location = new System.Drawing.Point(54, 67);
            this._U22.Name = "_U22";
            this._U22.ReadOnly = true;
            this._U22.Size = new System.Drawing.Size(52, 20);
            this._U22.TabIndex = 119;
            this._U22.Text = "0";
            this._U22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Unbc2
            // 
            this._Unbc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Unbc2.Enabled = false;
            this._Unbc2.Location = new System.Drawing.Point(54, 41);
            this._Unbc2.Name = "_Unbc2";
            this._Unbc2.ReadOnly = true;
            this._Unbc2.Size = new System.Drawing.Size(52, 20);
            this._Unbc2.TabIndex = 16;
            this._Unbc2.Text = "0";
            this._Unbc2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // u2Label
            // 
            this.u2Label.AutoSize = true;
            this.u2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.u2Label.Location = new System.Drawing.Point(6, 43);
            this.u2Label.Name = "u2Label";
            this.u2Label.Size = new System.Drawing.Size(44, 13);
            this.u2Label.TabIndex = 99;
            this.u2Label.Text = "Un/bc2";
            // 
            // _Ibb2
            // 
            this._Ibb2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ibb2.Enabled = false;
            this._Ibb2.Location = new System.Drawing.Point(54, 93);
            this._Ibb2.Name = "_Ibb2";
            this._Ibb2.ReadOnly = true;
            this._Ibb2.Size = new System.Drawing.Size(52, 20);
            this._Ibb2.TabIndex = 107;
            this._Ibb2.Text = "0";
            this._Ibb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Icb2
            // 
            this._Icb2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Icb2.Enabled = false;
            this._Icb2.Location = new System.Drawing.Point(54, 119);
            this._Icb2.Name = "_Icb2";
            this._Icb2.ReadOnly = true;
            this._Icb2.Size = new System.Drawing.Size(52, 20);
            this._Icb2.TabIndex = 104;
            this._Icb2.Text = "0";
            this._Icb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.Location = new System.Drawing.Point(6, 96);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(28, 13);
            this.label47.TabIndex = 110;
            this.label47.Text = "I��2";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label48.Location = new System.Drawing.Point(6, 121);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(28, 13);
            this.label48.TabIndex = 111;
            this.label48.Text = "I��2";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._Uab1);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this.u1Label);
            this.groupBox9.Controls.Add(this._Unbc1);
            this.groupBox9.Controls.Add(this._U21);
            this.groupBox9.Controls.Add(this._Ibb1);
            this.groupBox9.Controls.Add(this._Icb1);
            this.groupBox9.Controls.Add(this.label45);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Location = new System.Drawing.Point(6, 86);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(120, 145);
            this.groupBox9.TabIndex = 102;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "������ 1";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label52.Location = new System.Drawing.Point(4, 18);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(33, 13);
            this.label52.TabIndex = 96;
            this.label52.Text = "Uab1";
            // 
            // _Uab1
            // 
            this._Uab1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Uab1.Enabled = false;
            this._Uab1.Location = new System.Drawing.Point(62, 16);
            this._Uab1.Name = "_Uab1";
            this._Uab1.ReadOnly = true;
            this._Uab1.Size = new System.Drawing.Size(52, 20);
            this._Uab1.TabIndex = 13;
            this._Uab1.Text = "0";
            this._Uab1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55.Location = new System.Drawing.Point(4, 69);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 13);
            this.label55.TabIndex = 120;
            this.label55.Text = "U21";
            // 
            // u1Label
            // 
            this.u1Label.AutoSize = true;
            this.u1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.u1Label.Location = new System.Drawing.Point(4, 43);
            this.u1Label.Name = "u1Label";
            this.u1Label.Size = new System.Drawing.Size(44, 13);
            this.u1Label.TabIndex = 97;
            this.u1Label.Text = "Un/bc1";
            // 
            // _Unbc1
            // 
            this._Unbc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Unbc1.Enabled = false;
            this._Unbc1.Location = new System.Drawing.Point(62, 41);
            this._Unbc1.Name = "_Unbc1";
            this._Unbc1.ReadOnly = true;
            this._Unbc1.Size = new System.Drawing.Size(52, 20);
            this._Unbc1.TabIndex = 14;
            this._Unbc1.Text = "0";
            this._Unbc1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _U21
            // 
            this._U21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U21.Enabled = false;
            this._U21.Location = new System.Drawing.Point(62, 67);
            this._U21.Name = "_U21";
            this._U21.ReadOnly = true;
            this._U21.Size = new System.Drawing.Size(52, 20);
            this._U21.TabIndex = 118;
            this._U21.Text = "0";
            this._U21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Ibb1
            // 
            this._Ibb1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ibb1.Enabled = false;
            this._Ibb1.Location = new System.Drawing.Point(62, 93);
            this._Ibb1.Name = "_Ibb1";
            this._Ibb1.ReadOnly = true;
            this._Ibb1.Size = new System.Drawing.Size(52, 20);
            this._Ibb1.TabIndex = 105;
            this._Ibb1.Text = "0";
            this._Ibb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Icb1
            // 
            this._Icb1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Icb1.Enabled = false;
            this._Icb1.Location = new System.Drawing.Point(62, 119);
            this._Icb1.Name = "_Icb1";
            this._Icb1.ReadOnly = true;
            this._Icb1.Size = new System.Drawing.Size(52, 20);
            this._Icb1.TabIndex = 106;
            this._Icb1.Text = "0";
            this._Icb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.Location = new System.Drawing.Point(4, 95);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 108;
            this.label45.Text = "I��1";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(4, 121);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 109;
            this.label46.Text = "I��1";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._blockSignal1);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this._blockSignal2);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this._blockSignal3);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this._blockSignal4);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this._blockSignal5);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this._blockSignal6);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Location = new System.Drawing.Point(644, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(121, 111);
            this.groupBox4.TabIndex = 99;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "����������";
            // 
            // _blockSignal1
            // 
            this._blockSignal1.BackColor = System.Drawing.Color.Transparent;
            this._blockSignal1.Location = new System.Drawing.Point(6, 19);
            this._blockSignal1.Name = "_blockSignal1";
            this._blockSignal1.Size = new System.Drawing.Size(13, 13);
            this._blockSignal1.State = BEMN.Forms.LedState.Off;
            this._blockSignal1.TabIndex = 90;
            // 
            // _blockSignal2
            // 
            this._blockSignal2.BackColor = System.Drawing.Color.Transparent;
            this._blockSignal2.Location = new System.Drawing.Point(6, 34);
            this._blockSignal2.Name = "_blockSignal2";
            this._blockSignal2.Size = new System.Drawing.Size(13, 13);
            this._blockSignal2.State = BEMN.Forms.LedState.Off;
            this._blockSignal2.TabIndex = 92;
            // 
            // _blockSignal3
            // 
            this._blockSignal3.BackColor = System.Drawing.Color.Transparent;
            this._blockSignal3.Location = new System.Drawing.Point(6, 49);
            this._blockSignal3.Name = "_blockSignal3";
            this._blockSignal3.Size = new System.Drawing.Size(13, 13);
            this._blockSignal3.State = BEMN.Forms.LedState.Off;
            this._blockSignal3.TabIndex = 94;
            // 
            // _blockSignal4
            // 
            this._blockSignal4.BackColor = System.Drawing.Color.Transparent;
            this._blockSignal4.Location = new System.Drawing.Point(6, 64);
            this._blockSignal4.Name = "_blockSignal4";
            this._blockSignal4.Size = new System.Drawing.Size(13, 13);
            this._blockSignal4.State = BEMN.Forms.LedState.Off;
            this._blockSignal4.TabIndex = 96;
            // 
            // _blockSignal5
            // 
            this._blockSignal5.BackColor = System.Drawing.Color.Transparent;
            this._blockSignal5.Location = new System.Drawing.Point(6, 79);
            this._blockSignal5.Name = "_blockSignal5";
            this._blockSignal5.Size = new System.Drawing.Size(13, 13);
            this._blockSignal5.State = BEMN.Forms.LedState.Off;
            this._blockSignal5.TabIndex = 98;
            // 
            // _blockSignal6
            // 
            this._blockSignal6.BackColor = System.Drawing.Color.Transparent;
            this._blockSignal6.Location = new System.Drawing.Point(6, 94);
            this._blockSignal6.Name = "_blockSignal6";
            this._blockSignal6.Size = new System.Drawing.Size(13, 13);
            this._blockSignal6.State = BEMN.Forms.LedState.Off;
            this._blockSignal6.TabIndex = 100;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this._logicSignal10);
            this.groupBox5.Controls.Add(this.label57);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this._neisprLedDev2);
            this.groupBox5.Controls.Add(this._neisprLedDev1);
            this.groupBox5.Controls.Add(this._neisprLedDev8);
            this.groupBox5.Controls.Add(this._neisprLedDev7);
            this.groupBox5.Controls.Add(this._neisprLedDev4);
            this.groupBox5.Controls.Add(this._neisprLedDev6);
            this.groupBox5.Controls.Add(this._neisprLedDev5);
            this.groupBox5.Controls.Add(this._neisprLedDev3);
            this.groupBox5.Controls.Add(this._neispr8rez);
            this.groupBox5.Controls.Add(this._neispr7rez);
            this.groupBox5.Controls.Add(this._neispr6rez);
            this.groupBox5.Controls.Add(this._neispr1res);
            this.groupBox5.Controls.Add(this._neispr5res);
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this._neispr2);
            this.groupBox5.Controls.Add(this._neispr4);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this._neispr3);
            this.groupBox5.Location = new System.Drawing.Point(385, 154);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(386, 131);
            this.groupBox5.TabIndex = 100;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "�������������";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label80);
            this.groupBox7.Controls.Add(this.label74);
            this.groupBox7.Controls.Add(this.label79);
            this.groupBox7.Controls.Add(this.label73);
            this.groupBox7.Controls.Add(this._errLed13);
            this.groupBox7.Controls.Add(this._errLed15);
            this.groupBox7.Controls.Add(this._errLed16);
            this.groupBox7.Controls.Add(this._errLed14);
            this.groupBox7.Controls.Add(this._errLed6);
            this.groupBox7.Controls.Add(this._errLed7);
            this.groupBox7.Controls.Add(this._errLed11);
            this.groupBox7.Controls.Add(this._errLed12);
            this.groupBox7.Controls.Add(this._errLed1);
            this.groupBox7.Controls.Add(this._errLed8);
            this.groupBox7.Controls.Add(this._errLed5);
            this.groupBox7.Controls.Add(this._errLed10);
            this.groupBox7.Controls.Add(this._errLed2);
            this.groupBox7.Controls.Add(this._errLed9);
            this.groupBox7.Controls.Add(this._errLed4);
            this.groupBox7.Controls.Add(this._errLed3);
            this.groupBox7.Controls.Add(this.label78);
            this.groupBox7.Controls.Add(this.label72);
            this.groupBox7.Controls.Add(this.label77);
            this.groupBox7.Controls.Add(this.label63);
            this.groupBox7.Controls.Add(this.label76);
            this.groupBox7.Controls.Add(this.label59);
            this.groupBox7.Controls.Add(this.label75);
            this.groupBox7.Controls.Add(this.label58);
            this.groupBox7.Location = new System.Drawing.Point(167, 10);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(213, 113);
            this.groupBox7.TabIndex = 148;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label80.Location = new System.Drawing.Point(117, 18);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(41, 13);
            this.label80.TabIndex = 173;
            this.label80.Text = "��� 1";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label74.Location = new System.Drawing.Point(25, 18);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(30, 13);
            this.label74.TabIndex = 172;
            this.label74.Text = "���";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label79.Location = new System.Drawing.Point(117, 33);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 13);
            this.label79.TabIndex = 175;
            this.label79.Text = "��� 2";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label73.Location = new System.Drawing.Point(25, 33);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(54, 13);
            this.label73.TabIndex = 174;
            this.label73.Text = "���� I2c";
            // 
            // _errLed13
            // 
            this._errLed13.BackColor = System.Drawing.Color.Transparent;
            this._errLed13.Location = new System.Drawing.Point(185, 18);
            this._errLed13.Name = "_errLed13";
            this._errLed13.Size = new System.Drawing.Size(13, 13);
            this._errLed13.State = BEMN.Forms.LedState.Off;
            this._errLed13.TabIndex = 171;
            this._errLed13.Visible = false;
            // 
            // _errLed15
            // 
            this._errLed15.BackColor = System.Drawing.Color.Transparent;
            this._errLed15.Location = new System.Drawing.Point(185, 18);
            this._errLed15.Name = "_errLed15";
            this._errLed15.Size = new System.Drawing.Size(13, 13);
            this._errLed15.State = BEMN.Forms.LedState.Off;
            this._errLed15.TabIndex = 170;
            this._errLed15.Visible = false;
            // 
            // _errLed16
            // 
            this._errLed16.BackColor = System.Drawing.Color.Transparent;
            this._errLed16.Location = new System.Drawing.Point(185, 18);
            this._errLed16.Name = "_errLed16";
            this._errLed16.Size = new System.Drawing.Size(13, 13);
            this._errLed16.State = BEMN.Forms.LedState.Off;
            this._errLed16.TabIndex = 169;
            this._errLed16.Visible = false;
            // 
            // _errLed14
            // 
            this._errLed14.BackColor = System.Drawing.Color.Transparent;
            this._errLed14.Location = new System.Drawing.Point(98, 93);
            this._errLed14.Name = "_errLed14";
            this._errLed14.Size = new System.Drawing.Size(13, 13);
            this._errLed14.State = BEMN.Forms.LedState.Off;
            this._errLed14.TabIndex = 168;
            // 
            // _errLed6
            // 
            this._errLed6.BackColor = System.Drawing.Color.Transparent;
            this._errLed6.Location = new System.Drawing.Point(6, 93);
            this._errLed6.Name = "_errLed6";
            this._errLed6.Size = new System.Drawing.Size(13, 13);
            this._errLed6.State = BEMN.Forms.LedState.Off;
            this._errLed6.TabIndex = 167;
            // 
            // _errLed7
            // 
            this._errLed7.BackColor = System.Drawing.Color.Transparent;
            this._errLed7.Location = new System.Drawing.Point(98, 18);
            this._errLed7.Name = "_errLed7";
            this._errLed7.Size = new System.Drawing.Size(13, 13);
            this._errLed7.State = BEMN.Forms.LedState.Off;
            this._errLed7.TabIndex = 156;
            // 
            // _errLed11
            // 
            this._errLed11.BackColor = System.Drawing.Color.Transparent;
            this._errLed11.Location = new System.Drawing.Point(185, 18);
            this._errLed11.Name = "_errLed11";
            this._errLed11.Size = new System.Drawing.Size(13, 13);
            this._errLed11.State = BEMN.Forms.LedState.Off;
            this._errLed11.TabIndex = 164;
            this._errLed11.Visible = false;
            // 
            // _errLed12
            // 
            this._errLed12.BackColor = System.Drawing.Color.Transparent;
            this._errLed12.Location = new System.Drawing.Point(98, 78);
            this._errLed12.Name = "_errLed12";
            this._errLed12.Size = new System.Drawing.Size(13, 13);
            this._errLed12.State = BEMN.Forms.LedState.Off;
            this._errLed12.TabIndex = 166;
            // 
            // _errLed1
            // 
            this._errLed1.BackColor = System.Drawing.Color.Transparent;
            this._errLed1.Location = new System.Drawing.Point(6, 18);
            this._errLed1.Name = "_errLed1";
            this._errLed1.Size = new System.Drawing.Size(13, 13);
            this._errLed1.State = BEMN.Forms.LedState.Off;
            this._errLed1.TabIndex = 157;
            // 
            // _errLed8
            // 
            this._errLed8.BackColor = System.Drawing.Color.Transparent;
            this._errLed8.Location = new System.Drawing.Point(98, 33);
            this._errLed8.Name = "_errLed8";
            this._errLed8.Size = new System.Drawing.Size(13, 13);
            this._errLed8.State = BEMN.Forms.LedState.Off;
            this._errLed8.TabIndex = 159;
            // 
            // _errLed5
            // 
            this._errLed5.BackColor = System.Drawing.Color.Transparent;
            this._errLed5.Location = new System.Drawing.Point(6, 78);
            this._errLed5.Name = "_errLed5";
            this._errLed5.Size = new System.Drawing.Size(13, 13);
            this._errLed5.State = BEMN.Forms.LedState.Off;
            this._errLed5.TabIndex = 165;
            // 
            // _errLed10
            // 
            this._errLed10.BackColor = System.Drawing.Color.Transparent;
            this._errLed10.Location = new System.Drawing.Point(98, 63);
            this._errLed10.Name = "_errLed10";
            this._errLed10.Size = new System.Drawing.Size(13, 13);
            this._errLed10.State = BEMN.Forms.LedState.Off;
            this._errLed10.TabIndex = 162;
            // 
            // _errLed2
            // 
            this._errLed2.BackColor = System.Drawing.Color.Transparent;
            this._errLed2.Location = new System.Drawing.Point(6, 33);
            this._errLed2.Name = "_errLed2";
            this._errLed2.Size = new System.Drawing.Size(13, 13);
            this._errLed2.State = BEMN.Forms.LedState.Off;
            this._errLed2.TabIndex = 158;
            // 
            // _errLed9
            // 
            this._errLed9.BackColor = System.Drawing.Color.Transparent;
            this._errLed9.Location = new System.Drawing.Point(98, 48);
            this._errLed9.Name = "_errLed9";
            this._errLed9.Size = new System.Drawing.Size(13, 13);
            this._errLed9.State = BEMN.Forms.LedState.Off;
            this._errLed9.TabIndex = 160;
            // 
            // _errLed4
            // 
            this._errLed4.BackColor = System.Drawing.Color.Transparent;
            this._errLed4.Location = new System.Drawing.Point(6, 63);
            this._errLed4.Name = "_errLed4";
            this._errLed4.Size = new System.Drawing.Size(13, 13);
            this._errLed4.State = BEMN.Forms.LedState.Off;
            this._errLed4.TabIndex = 163;
            // 
            // _errLed3
            // 
            this._errLed3.BackColor = System.Drawing.Color.Transparent;
            this._errLed3.Location = new System.Drawing.Point(6, 48);
            this._errLed3.Name = "_errLed3";
            this._errLed3.Size = new System.Drawing.Size(13, 13);
            this._errLed3.State = BEMN.Forms.LedState.Off;
            this._errLed3.TabIndex = 161;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label78.Location = new System.Drawing.Point(117, 48);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(50, 13);
            this.label78.TabIndex = 149;
            this.label78.Text = "�������";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label72.Location = new System.Drawing.Point(25, 48);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(74, 13);
            this.label72.TabIndex = 148;
            this.label72.Text = "�����������";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label77.Location = new System.Drawing.Point(117, 93);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(39, 13);
            this.label77.TabIndex = 155;
            this.label77.Text = "�����";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label63.Location = new System.Drawing.Point(25, 93);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(30, 13);
            this.label63.TabIndex = 154;
            this.label63.Text = "���";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label76.Location = new System.Drawing.Point(117, 63);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(69, 13);
            this.label76.TabIndex = 151;
            this.label76.Text = "����-� ���";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label59.Location = new System.Drawing.Point(25, 63);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 13);
            this.label59.TabIndex = 150;
            this.label59.Text = "��� U";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label75.Location = new System.Drawing.Point(117, 78);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(81, 13);
            this.label75.TabIndex = 153;
            this.label75.Text = "������� �-��";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label58.Location = new System.Drawing.Point(25, 78);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(36, 13);
            this.label58.TabIndex = 152;
            this.label58.Text = "��� I";
            // 
            // _logicSignal10
            // 
            this._logicSignal10.BackColor = System.Drawing.Color.Transparent;
            this._logicSignal10.Location = new System.Drawing.Point(6, 64);
            this._logicSignal10.Name = "_logicSignal10";
            this._logicSignal10.Size = new System.Drawing.Size(13, 13);
            this._logicSignal10.State = BEMN.Forms.LedState.Off;
            this._logicSignal10.TabIndex = 120;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label57.Location = new System.Drawing.Point(25, 19);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(121, 13);
            this.label57.TabIndex = 146;
            this.label57.Text = "������-�� ����������";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label56.Location = new System.Drawing.Point(25, 34);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(131, 13);
            this.label56.TabIndex = 147;
            this.label56.Text = "������-�� �����������";
            // 
            // _neisprLedDev2
            // 
            this._neisprLedDev2.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev2.Location = new System.Drawing.Point(6, 34);
            this._neisprLedDev2.Name = "_neisprLedDev2";
            this._neisprLedDev2.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev2.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev2.TabIndex = 145;
            // 
            // _neisprLedDev1
            // 
            this._neisprLedDev1.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev1.Location = new System.Drawing.Point(6, 19);
            this._neisprLedDev1.Name = "_neisprLedDev1";
            this._neisprLedDev1.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev1.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev1.TabIndex = 144;
            // 
            // _neisprLedDev8
            // 
            this._neisprLedDev8.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev8.Location = new System.Drawing.Point(138, 84);
            this._neisprLedDev8.Name = "_neisprLedDev8";
            this._neisprLedDev8.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev8.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev8.TabIndex = 136;
            this._neisprLedDev8.Visible = false;
            // 
            // _neisprLedDev7
            // 
            this._neisprLedDev7.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev7.Location = new System.Drawing.Point(138, 84);
            this._neisprLedDev7.Name = "_neisprLedDev7";
            this._neisprLedDev7.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev7.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev7.TabIndex = 136;
            this._neisprLedDev7.Visible = false;
            // 
            // _neisprLedDev4
            // 
            this._neisprLedDev4.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev4.Location = new System.Drawing.Point(138, 84);
            this._neisprLedDev4.Name = "_neisprLedDev4";
            this._neisprLedDev4.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev4.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev4.TabIndex = 136;
            this._neisprLedDev4.Visible = false;
            // 
            // _neisprLedDev6
            // 
            this._neisprLedDev6.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev6.Location = new System.Drawing.Point(138, 84);
            this._neisprLedDev6.Name = "_neisprLedDev6";
            this._neisprLedDev6.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev6.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev6.TabIndex = 136;
            this._neisprLedDev6.Visible = false;
            // 
            // _neisprLedDev5
            // 
            this._neisprLedDev5.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev5.Location = new System.Drawing.Point(138, 84);
            this._neisprLedDev5.Name = "_neisprLedDev5";
            this._neisprLedDev5.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev5.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev5.TabIndex = 136;
            this._neisprLedDev5.Visible = false;
            // 
            // _neisprLedDev3
            // 
            this._neisprLedDev3.BackColor = System.Drawing.Color.Transparent;
            this._neisprLedDev3.Location = new System.Drawing.Point(6, 49);
            this._neisprLedDev3.Name = "_neisprLedDev3";
            this._neisprLedDev3.Size = new System.Drawing.Size(13, 13);
            this._neisprLedDev3.State = BEMN.Forms.LedState.Off;
            this._neisprLedDev3.TabIndex = 136;
            // 
            // _neispr8rez
            // 
            this._neispr8rez.BackColor = System.Drawing.Color.Transparent;
            this._neispr8rez.Location = new System.Drawing.Point(138, 103);
            this._neispr8rez.Name = "_neispr8rez";
            this._neispr8rez.Size = new System.Drawing.Size(13, 13);
            this._neispr8rez.State = BEMN.Forms.LedState.Off;
            this._neispr8rez.TabIndex = 134;
            this._neispr8rez.Visible = false;
            // 
            // _neispr7rez
            // 
            this._neispr7rez.BackColor = System.Drawing.Color.Transparent;
            this._neispr7rez.Location = new System.Drawing.Point(138, 103);
            this._neispr7rez.Name = "_neispr7rez";
            this._neispr7rez.Size = new System.Drawing.Size(13, 13);
            this._neispr7rez.State = BEMN.Forms.LedState.Off;
            this._neispr7rez.TabIndex = 133;
            this._neispr7rez.Visible = false;
            // 
            // _neispr6rez
            // 
            this._neispr6rez.BackColor = System.Drawing.Color.Transparent;
            this._neispr6rez.Location = new System.Drawing.Point(138, 103);
            this._neispr6rez.Name = "_neispr6rez";
            this._neispr6rez.Size = new System.Drawing.Size(13, 13);
            this._neispr6rez.State = BEMN.Forms.LedState.Off;
            this._neispr6rez.TabIndex = 132;
            this._neispr6rez.Visible = false;
            // 
            // _neispr1res
            // 
            this._neispr1res.BackColor = System.Drawing.Color.Transparent;
            this._neispr1res.Location = new System.Drawing.Point(138, 103);
            this._neispr1res.Name = "_neispr1res";
            this._neispr1res.Size = new System.Drawing.Size(13, 13);
            this._neispr1res.State = BEMN.Forms.LedState.Off;
            this._neispr1res.TabIndex = 122;
            this._neispr1res.Visible = false;
            // 
            // _neispr5res
            // 
            this._neispr5res.BackColor = System.Drawing.Color.Transparent;
            this._neispr5res.Location = new System.Drawing.Point(138, 103);
            this._neispr5res.Name = "_neispr5res";
            this._neispr5res.Size = new System.Drawing.Size(13, 13);
            this._neispr5res.State = BEMN.Forms.LedState.Off;
            this._neispr5res.TabIndex = 130;
            this._neispr5res.Visible = false;
            // 
            // _neispr2
            // 
            this._neispr2.BackColor = System.Drawing.Color.Transparent;
            this._neispr2.Location = new System.Drawing.Point(6, 79);
            this._neispr2.Name = "_neispr2";
            this._neispr2.Size = new System.Drawing.Size(13, 13);
            this._neispr2.State = BEMN.Forms.LedState.Off;
            this._neispr2.TabIndex = 124;
            // 
            // _neispr4
            // 
            this._neispr4.BackColor = System.Drawing.Color.Transparent;
            this._neispr4.Location = new System.Drawing.Point(6, 109);
            this._neispr4.Name = "_neispr4";
            this._neispr4.Size = new System.Drawing.Size(13, 13);
            this._neispr4.State = BEMN.Forms.LedState.Off;
            this._neispr4.TabIndex = 128;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.Location = new System.Drawing.Point(25, 94);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(99, 13);
            this.label44.TabIndex = 127;
            this.label44.Text = "������ \"�������\"";
            // 
            // _neispr3
            // 
            this._neispr3.BackColor = System.Drawing.Color.Transparent;
            this._neispr3.Location = new System.Drawing.Point(6, 94);
            this._neispr3.Name = "_neispr3";
            this._neispr3.Size = new System.Drawing.Size(13, 13);
            this._neispr3.State = BEMN.Forms.LedState.Off;
            this._neispr3.TabIndex = 126;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label65);
            this.groupBox6.Controls.Add(this.label66);
            this.groupBox6.Controls.Add(this.label67);
            this.groupBox6.Controls.Add(this.label68);
            this.groupBox6.Controls.Add(this.label69);
            this.groupBox6.Controls.Add(this.label70);
            this.groupBox6.Controls.Add(this.label71);
            this.groupBox6.Controls.Add(this.label60);
            this.groupBox6.Controls.Add(this.label61);
            this.groupBox6.Controls.Add(this.label62);
            this.groupBox6.Controls.Add(this.label103);
            this.groupBox6.Controls.Add(this.label102);
            this.groupBox6.Controls.Add(this.label101);
            this.groupBox6.Controls.Add(this.label100);
            this.groupBox6.Controls.Add(this._r12);
            this.groupBox6.Controls.Add(this._r7);
            this.groupBox6.Controls.Add(this._r11);
            this.groupBox6.Controls.Add(this._r10);
            this.groupBox6.Controls.Add(this._r6);
            this.groupBox6.Controls.Add(this._r9);
            this.groupBox6.Controls.Add(this._r8);
            this.groupBox6.Controls.Add(this._r5);
            this.groupBox6.Controls.Add(this._rMinus);
            this.groupBox6.Controls.Add(this._r4);
            this.groupBox6.Controls.Add(this._r3);
            this.groupBox6.Controls.Add(this._rPlus);
            this.groupBox6.Controls.Add(this._r2);
            this.groupBox6.Controls.Add(this._r1);
            this.groupBox6.Location = new System.Drawing.Point(263, 148);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(115, 235);
            this.groupBox6.TabIndex = 101;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "����";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.Location = new System.Drawing.Point(28, 214);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(26, 13);
            this.label65.TabIndex = 166;
            this.label65.Text = "�12";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label66.Location = new System.Drawing.Point(28, 199);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(26, 13);
            this.label66.TabIndex = 165;
            this.label66.Text = "�11";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label67.Location = new System.Drawing.Point(28, 184);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(26, 13);
            this.label67.TabIndex = 164;
            this.label67.Text = "�10";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label68.Location = new System.Drawing.Point(28, 169);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(20, 13);
            this.label68.TabIndex = 163;
            this.label68.Text = "�9";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label69.Location = new System.Drawing.Point(28, 154);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(20, 13);
            this.label69.TabIndex = 162;
            this.label69.Text = "�8";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label70.Location = new System.Drawing.Point(28, 139);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(20, 13);
            this.label70.TabIndex = 161;
            this.label70.Text = "�7";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label71.Location = new System.Drawing.Point(28, 124);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(20, 13);
            this.label71.TabIndex = 160;
            this.label71.Text = "�6";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label60.Location = new System.Drawing.Point(28, 109);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(20, 13);
            this.label60.TabIndex = 159;
            this.label60.Text = "�5";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label61.Location = new System.Drawing.Point(28, 94);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(20, 13);
            this.label61.TabIndex = 158;
            this.label61.Text = "�4";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label62.Location = new System.Drawing.Point(28, 79);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(20, 13);
            this.label62.TabIndex = 157;
            this.label62.Text = "�3";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label103.Location = new System.Drawing.Point(28, 64);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(20, 13);
            this.label103.TabIndex = 156;
            this.label103.Text = "�2";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label102.Location = new System.Drawing.Point(28, 49);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(20, 13);
            this.label102.TabIndex = 155;
            this.label102.Text = "�1";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label101.Location = new System.Drawing.Point(28, 34);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(50, 13);
            this.label101.TabIndex = 154;
            this.label101.Text = "�������";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label100.Location = new System.Drawing.Point(28, 19);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(62, 13);
            this.label100.TabIndex = 153;
            this.label100.Text = "���������";
            // 
            // _r12
            // 
            this._r12.BackColor = System.Drawing.Color.Transparent;
            this._r12.Location = new System.Drawing.Point(9, 214);
            this._r12.Name = "_r12";
            this._r12.Size = new System.Drawing.Size(13, 13);
            this._r12.State = BEMN.Forms.LedState.Off;
            this._r12.TabIndex = 152;
            // 
            // _r7
            // 
            this._r7.BackColor = System.Drawing.Color.Transparent;
            this._r7.Location = new System.Drawing.Point(9, 139);
            this._r7.Name = "_r7";
            this._r7.Size = new System.Drawing.Size(13, 13);
            this._r7.State = BEMN.Forms.LedState.Off;
            this._r7.TabIndex = 147;
            // 
            // _r11
            // 
            this._r11.BackColor = System.Drawing.Color.Transparent;
            this._r11.Location = new System.Drawing.Point(9, 199);
            this._r11.Name = "_r11";
            this._r11.Size = new System.Drawing.Size(13, 13);
            this._r11.State = BEMN.Forms.LedState.Off;
            this._r11.TabIndex = 151;
            // 
            // _r10
            // 
            this._r10.BackColor = System.Drawing.Color.Transparent;
            this._r10.Location = new System.Drawing.Point(9, 184);
            this._r10.Name = "_r10";
            this._r10.Size = new System.Drawing.Size(13, 13);
            this._r10.State = BEMN.Forms.LedState.Off;
            this._r10.TabIndex = 150;
            // 
            // _r6
            // 
            this._r6.BackColor = System.Drawing.Color.Transparent;
            this._r6.Location = new System.Drawing.Point(9, 124);
            this._r6.Name = "_r6";
            this._r6.Size = new System.Drawing.Size(13, 13);
            this._r6.State = BEMN.Forms.LedState.Off;
            this._r6.TabIndex = 146;
            // 
            // _r9
            // 
            this._r9.BackColor = System.Drawing.Color.Transparent;
            this._r9.Location = new System.Drawing.Point(9, 169);
            this._r9.Name = "_r9";
            this._r9.Size = new System.Drawing.Size(13, 13);
            this._r9.State = BEMN.Forms.LedState.Off;
            this._r9.TabIndex = 149;
            // 
            // _r8
            // 
            this._r8.BackColor = System.Drawing.Color.Transparent;
            this._r8.Location = new System.Drawing.Point(9, 154);
            this._r8.Name = "_r8";
            this._r8.Size = new System.Drawing.Size(13, 13);
            this._r8.State = BEMN.Forms.LedState.Off;
            this._r8.TabIndex = 148;
            // 
            // _r5
            // 
            this._r5.BackColor = System.Drawing.Color.Transparent;
            this._r5.Location = new System.Drawing.Point(9, 109);
            this._r5.Name = "_r5";
            this._r5.Size = new System.Drawing.Size(13, 13);
            this._r5.State = BEMN.Forms.LedState.Off;
            this._r5.TabIndex = 145;
            // 
            // _rMinus
            // 
            this._rMinus.BackColor = System.Drawing.Color.Transparent;
            this._rMinus.Location = new System.Drawing.Point(9, 34);
            this._rMinus.Name = "_rMinus";
            this._rMinus.Size = new System.Drawing.Size(13, 13);
            this._rMinus.State = BEMN.Forms.LedState.Off;
            this._rMinus.TabIndex = 140;
            // 
            // _r4
            // 
            this._r4.BackColor = System.Drawing.Color.Transparent;
            this._r4.Location = new System.Drawing.Point(9, 94);
            this._r4.Name = "_r4";
            this._r4.Size = new System.Drawing.Size(13, 13);
            this._r4.State = BEMN.Forms.LedState.Off;
            this._r4.TabIndex = 144;
            // 
            // _r3
            // 
            this._r3.BackColor = System.Drawing.Color.Transparent;
            this._r3.Location = new System.Drawing.Point(9, 79);
            this._r3.Name = "_r3";
            this._r3.Size = new System.Drawing.Size(13, 13);
            this._r3.State = BEMN.Forms.LedState.Off;
            this._r3.TabIndex = 143;
            // 
            // _rPlus
            // 
            this._rPlus.BackColor = System.Drawing.Color.Transparent;
            this._rPlus.Location = new System.Drawing.Point(9, 19);
            this._rPlus.Name = "_rPlus";
            this._rPlus.Size = new System.Drawing.Size(13, 13);
            this._rPlus.State = BEMN.Forms.LedState.Off;
            this._rPlus.TabIndex = 139;
            // 
            // _r2
            // 
            this._r2.BackColor = System.Drawing.Color.Transparent;
            this._r2.Location = new System.Drawing.Point(9, 64);
            this._r2.Name = "_r2";
            this._r2.Size = new System.Drawing.Size(13, 13);
            this._r2.State = BEMN.Forms.LedState.Off;
            this._r2.TabIndex = 142;
            // 
            // _r1
            // 
            this._r1.BackColor = System.Drawing.Color.Transparent;
            this._r1.Location = new System.Drawing.Point(9, 49);
            this._r1.Name = "_r1";
            this._r1.Size = new System.Drawing.Size(13, 13);
            this._r1.State = BEMN.Forms.LedState.Off;
            this._r1.TabIndex = 141;
            // 
            // _modeGroup
            // 
            this._modeGroup.Controls.Add(this._setSypBtn);
            this._modeGroup.Controls.Add(this._resetSypBtn);
            this._modeGroup.Controls.Add(this.label40);
            this._modeGroup.Controls.Add(this._modeLed3);
            this._modeGroup.Controls.Add(this._modeLed2);
            this._modeGroup.Controls.Add(this._modeLed1);
            this._modeGroup.Controls.Add(this.label8);
            this._modeGroup.Controls.Add(this.label49);
            this._modeGroup.Location = new System.Drawing.Point(163, 389);
            this._modeGroup.Name = "_modeGroup";
            this._modeGroup.Size = new System.Drawing.Size(215, 79);
            this._modeGroup.TabIndex = 102;
            this._modeGroup.TabStop = false;
            this._modeGroup.Text = "�����";
            this._modeGroup.Visible = false;
            // 
            // _setSypBtn
            // 
            this._setSypBtn.Location = new System.Drawing.Point(120, 53);
            this._setSypBtn.Name = "_setSypBtn";
            this._setSypBtn.Size = new System.Drawing.Size(34, 20);
            this._setSypBtn.TabIndex = 109;
            this._setSypBtn.Text = "���";
            this._setSypBtn.UseVisualStyleBackColor = true;
            this._setSypBtn.Click += new System.EventHandler(this._setSypBtn_Click);
            // 
            // _resetSypBtn
            // 
            this._resetSypBtn.Location = new System.Drawing.Point(156, 53);
            this._resetSypBtn.Name = "_resetSypBtn";
            this._resetSypBtn.Size = new System.Drawing.Size(53, 20);
            this._resetSypBtn.TabIndex = 109;
            this._resetSypBtn.Text = "�����";
            this._resetSypBtn.UseVisualStyleBackColor = true;
            this._resetSypBtn.Click += new System.EventHandler(this._resetSypBtn_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.Location = new System.Drawing.Point(25, 57);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(89, 13);
            this.label40.TabIndex = 103;
            this.label40.Text = "�������������";
            // 
            // _modeLed3
            // 
            this._modeLed3.BackColor = System.Drawing.Color.Transparent;
            this._modeLed3.Location = new System.Drawing.Point(6, 57);
            this._modeLed3.Name = "_modeLed3";
            this._modeLed3.Size = new System.Drawing.Size(13, 13);
            this._modeLed3.State = BEMN.Forms.LedState.Off;
            this._modeLed3.TabIndex = 98;
            // 
            // _modeLed2
            // 
            this._modeLed2.BackColor = System.Drawing.Color.Transparent;
            this._modeLed2.Location = new System.Drawing.Point(6, 38);
            this._modeLed2.Name = "_modeLed2";
            this._modeLed2.Size = new System.Drawing.Size(13, 13);
            this._modeLed2.State = BEMN.Forms.LedState.Off;
            this._modeLed2.TabIndex = 98;
            // 
            // _modeLed1
            // 
            this._modeLed1.BackColor = System.Drawing.Color.Transparent;
            this._modeLed1.Location = new System.Drawing.Point(6, 19);
            this._modeLed1.Name = "_modeLed1";
            this._modeLed1.Size = new System.Drawing.Size(13, 13);
            this._modeLed1.State = BEMN.Forms.LedState.Off;
            this._modeLed1.TabIndex = 98;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(606, 283);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 169);
            this._dateTimeControl.TabIndex = 98;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl1_TimeChanged);
            // 
            // Mr851MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 526);
            this.Controls.Add(this._modeGroup);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this._dateTimeControl);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Mr851MeasuringForm";
            this.Text = "SystemJournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Measuring_FormClosing);
            this.Load += new System.EventHandler(this.Measuring_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._modeGroup.ResumeLayout(false);
            this._modeGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _sygnalState1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _sygnalState8;
        private System.Windows.Forms.Label label6;
        private BEMN.Forms.LedControl _sygnalState7;
        private System.Windows.Forms.Label label7;
        private BEMN.Forms.LedControl _sygnalState6;
        private System.Windows.Forms.Label label8;
        private BEMN.Forms.LedControl _sygnalState5;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _sygnalState4;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _sygnalState3;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _sygnalState2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _d16;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _d15;
        private System.Windows.Forms.Label label19;
        private BEMN.Forms.LedControl _d14;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _d13;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _d12;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _d11;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _d10;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _d9;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.LedControl _d8;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _d7;
        private System.Windows.Forms.Label label11;
        private BEMN.Forms.LedControl _d6;
        private System.Windows.Forms.Label label12;
        private BEMN.Forms.LedControl _d5;
        private System.Windows.Forms.Label label13;
        private BEMN.Forms.LedControl _d4;
        private System.Windows.Forms.Label label14;
        private BEMN.Forms.LedControl _d3;
        private System.Windows.Forms.Label label15;
        private BEMN.Forms.LedControl _d2;
        private System.Windows.Forms.Label label16;
        private BEMN.Forms.LedControl _d1;
        private System.Windows.Forms.GroupBox groupBox8;
        private BEMN.Forms.LedControl _neispr2;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _neispr1res;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _logicSignal10;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _logicSignal9;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _logicSignal8;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _logicSignal7;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _logicSignal6;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _logicSignal5;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _logicSignal4;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _logicSignal3;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _logicSignal2;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _logicSignal1;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _blockSignal6;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _blockSignal5;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _blockSignal4;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _blockSignal3;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _blockSignal2;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _blockSignal1;
        private BEMN.Forms.LedControl _neispr5res;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _neispr4;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _neispr3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox _Unbc2;
        private System.Windows.Forms.TextBox _Uab2;
        private System.Windows.Forms.TextBox _Unbc1;
        private System.Windows.Forms.TextBox _Uab1;
        private System.Windows.Forms.Label u2Label;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label u1Label;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.TextBox _Res;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox _U22;
        private System.Windows.Forms.TextBox _U21;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox _Upod;
        private System.Windows.Forms.Label ucLabel;
        private System.Windows.Forms.TextBox _Uc1c2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox _N;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox _Ibb1;
        private System.Windows.Forms.TextBox _Icb2;
        private System.Windows.Forms.TextBox _Icb1;
        private System.Windows.Forms.TextBox _Ibb2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private Forms.LedControl _r12;
        private Forms.LedControl _r7;
        private Forms.LedControl _r11;
        private Forms.LedControl _r10;
        private Forms.LedControl _r6;
        private Forms.LedControl _r9;
        private Forms.LedControl _r8;
        private Forms.LedControl _r5;
        private Forms.LedControl _rMinus;
        private Forms.LedControl _r4;
        private Forms.LedControl _r3;
        private Forms.LedControl _rPlus;
        private Forms.LedControl _r2;
        private Forms.LedControl _r1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private Forms.LedControl _neispr8rez;
        private Forms.LedControl _neispr7rez;
        private Forms.LedControl _neispr6rez;
        private Forms.LedControl _neisprLedDev8;
        private Forms.LedControl _neisprLedDev7;
        private Forms.LedControl _neisprLedDev4;
        private Forms.LedControl _neisprLedDev6;
        private Forms.LedControl _neisprLedDev5;
        private Forms.LedControl _neisprLedDev3;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private Forms.LedControl _neisprLedDev2;
        private Forms.LedControl _neisprLedDev1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label73;
        private Forms.LedControl _errLed13;
        private Forms.LedControl _errLed15;
        private Forms.LedControl _errLed16;
        private Forms.LedControl _errLed14;
        private Forms.LedControl _errLed6;
        private Forms.LedControl _errLed7;
        private Forms.LedControl _errLed11;
        private Forms.LedControl _errLed12;
        private Forms.LedControl _errLed1;
        private Forms.LedControl _errLed8;
        private Forms.LedControl _errLed5;
        private Forms.LedControl _errLed10;
        private Forms.LedControl _errLed2;
        private Forms.LedControl _errLed9;
        private Forms.LedControl _errLed4;
        private Forms.LedControl _errLed3;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button _resetBlockBtn;
        private System.Windows.Forms.Button _resetSignalBtn;
        private System.Windows.Forms.Button _resetNewRecJSBtn;
        private System.Windows.Forms.Button _resetNeisprJSBtn;
        private System.Windows.Forms.Button _subBtn;
        private System.Windows.Forms.Button _addBtn;
        private System.Windows.Forms.GroupBox _modeGroup;
        private System.Windows.Forms.Button _setSypBtn;
        private System.Windows.Forms.Button _resetSypBtn;
        private System.Windows.Forms.Label label40;
        private Forms.LedControl _modeLed3;
        private Forms.LedControl _modeLed1;
        private Forms.LedControl _colorLed2;
        private Forms.LedControl _colorLed1;
        private System.Windows.Forms.Label _labelColor2;
        private System.Windows.Forms.Label _labelColor1;
        private Forms.LedControl _modeLed2;

    }
}