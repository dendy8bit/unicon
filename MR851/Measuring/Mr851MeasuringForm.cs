using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR851.Configuration.Structures;
using BEMN.MR851.Measuring.Structures;

namespace BEMN.MR851.Measuring
{
    public partial class Mr851MeasuringForm : Form, IFormView
    {
        private Mr851Device _device;
        private LedControl[] _sygnalState;
        private LedControl[] _inputSygnals;
        private LedControl[] _logicSygnals;
        private LedControl[] _relay;
        private LedControl[] _blockSignals;
        private LedControl[] _neisprPrivod;
        private LedControl[] _neisprSignalsDev;
        private LedControl[] _errLeds;
        private LedControl[] _modeLeds;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private AveragerTime<AnalogDataBaseStruct> _averagerTime;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<ConfigurationStruct> _measureTrans;
        private ConfigurationStruct _measureTransStruct;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<CounterMeasStruct> _counter;
        private const string MEASURE_TRANS_READ_FAIL = "��������� ��������� �� ���� ���������";
        private const string Unull = "0B";
        private const string Un = "Un";
        private NumberFormatInfo _provider;
        private bool _isErrorConfigSection;

        public Mr851MeasuringForm()
        {
            this.InitializeComponent();
        }

        public Mr851MeasuringForm(Mr851Device device)
        {
            this.InitializeComponent();
            this._device = device;
             _provider= new NumberFormatInfo();
            _provider.NumberDecimalSeparator = ".";
            this._device.ConnectionModeChanged += this.StartStopRead;

            this._dateTime = device.DateAndTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._device.Configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () => this._measureTransStruct = this._device.Configuration.Value);
            this._device.Configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => this._measureTransStruct = this._device.Configuration.Value);

            this._discretDataBase = this._device.DiscretBd;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);

            this._analogDataBase = this._device.AnalogBd;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._counter = this._device.Counter;
            this._counter.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this._N.Text = this._counter.Value.Cur.ToString();
                    this._Res.Text = this._counter.Value.Res.ToString();
                });
            this._counter.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this._N.Text = "0";
                    this._Res.Text = "0";
                });
            
            this._measureTrans = this._device.MeasuringConfiguration;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadFail);
            
            this._averagerTime = new AveragerTime<AnalogDataBaseStruct>(500);
            this._averagerTime.Tick += this.AveragerTimeTick;
            this.Init();
        }

        private void Init()
        {
            this._isErrorConfigSection = false;
            double vers = Common.VersionConverter(this._device.DeviceVersion);
            if (vers >= 1.06 && vers < 2.0 || vers >= 2.01)
            {
                this._modeGroup.Visible = true;
                this._addBtn.Visible = this._subBtn.Visible = true;
                this._colorLed1.Visible = this._colorLed2.Visible =
                    this._labelColor1.Visible = this._labelColor2.Visible = false;
                this._sygnalState5.Visible = false;
                this._modeLeds = new[]
                {
                    this._modeLed1, this._modeLed2, this._modeLed3
                };
            }
            this._sygnalState = new[]
                {
                    this._sygnalState1, this._sygnalState2, this._sygnalState3, this._sygnalState4, this._sygnalState5, this._sygnalState6, this._sygnalState7, this._sygnalState8
                };
            this._inputSygnals = new[]
                {
                    this._d1, this._d2, this._d3, this._d4, this._d5, this._d6, this._d7, this._d8, this._d9, this._d10, this._d11, this._d12, this._d13, this._d14, this._d15, this._d16
                };
            this._blockSignals = new[]
            {
                this._blockSignal1, this._blockSignal2, this._blockSignal3, this._blockSignal4, this._blockSignal5, this._blockSignal6, this._logicSignal1, this._logicSignal2,  //ls1 � ls2 - ��������� ������
            };
            this._logicSygnals = new[]
                {
                    this._logicSignal3, this._logicSignal4, this._logicSignal5, this._logicSignal6, this._logicSignal7, this._logicSignal8, this._logicSignal9, this._logicSignal10,
                    
                };
            this._neisprPrivod = new[]
            {
                this._neispr1res, this._neispr2, this._neispr3, this._neispr4, this._neispr5res, this._neispr6rez, this._neispr7rez, this._neispr8rez
            };
            this._neisprSignalsDev = new[]
            {
                this._neisprLedDev1, this._neisprLedDev2, this._neisprLedDev3, this._neisprLedDev4, this._neisprLedDev5, this._neisprLedDev6, this._neisprLedDev7, this._neisprLedDev8
            };
            this._errLeds = new[]
            {
                this._errLed1, this._errLed2, this._errLed3, this._errLed4, this._errLed5, this._errLed6, this._errLed7, this._errLed8, this._errLed9, this._errLed10, this._errLed11, this._errLed12, this._errLed13, this._errLed14, this._errLed15, this._errLed16
            };
            this._relay = new[]
            {
                new LedControl(), new LedControl(), this._rPlus, this._rMinus, this._r1, this._r2, this._r3, this._r4, this._r5, this._r6, this._r7, this._r8, this._r9, this._r10, this._r11, this._r12
            };
        }

        private void OnAnalogSignalsLoadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }

        private void MeasureTransReadFail()
        {
            MessageBox.Show(MEASURE_TRANS_READ_FAIL);
        }

        private void MeasureTransReadOk()
        {
            this._measureTransStruct = this._measureTrans.Value;
            this._analogDataBase.LoadStruct();
        }

        private void AnalogBdReadFail()
        {
            this._Ibb1.Text = "0 A";
            this._Icb1.Text = "0 A";
            this._Ibb2.Text = "0 A";
            this._Icb2.Text = "0 A";
            this._Uab1.Text = "0 B";
            this.u1Label.Text = "Un/bc1";
            this._Unbc1.Text = "0 B";
            this._Uab2.Text = "0 B";
            this.u2Label.Text = "Un/bc2";
            this._Unbc2.Text = "0 B";
        }

        private void AveragerTimeTick()
        {
            double vers = Common.VersionConverter(this._device.DeviceVersion);
            if (vers > 2.01 || vers >= 1.08 && vers < 2.0)
            {
                this._Uab1.Text = this._analogDataBase.Value.UAB1(this._averagerTime.ValueList, this._measureTransStruct.Section1);
                this._Uab2.Text = this._analogDataBase.Value.UAB2(this._averagerTime.ValueList, this._measureTransStruct.Section2);
                this._Unbc1.Text = this._analogDataBase.Value.UNBC1(this._averagerTime.ValueList, this._measureTransStruct.Section1);
                this._Unbc2.Text = this._analogDataBase.Value.UNBC2(this._averagerTime.ValueList, this._measureTransStruct.Section2); 
            }
            else
            {
                this._Uab1.Text = this._analogDataBase.Value.UAB1(this._averagerTime.ValueList, this._measureTransStruct.Section1);
                this._Uab2.Text = this._analogDataBase.Value.UAB2(this._averagerTime.ValueList, this._measureTransStruct.Section2);
                this._Unbc1.Text = this._analogDataBase.Value.OLDUNBC1(this._averagerTime.ValueList, this._measureTransStruct.Section1);
                this._Unbc2.Text = this._analogDataBase.Value.OLDUNBC2(this._averagerTime.ValueList, this._measureTransStruct.Section2); 
            }
            this._Ibb1.Text = this._analogDataBase.Value.IBB1(this._averagerTime.ValueList, this._measureTransStruct.Section1);
            this._Icb1.Text = this._analogDataBase.Value.ICB1(this._averagerTime.ValueList, this._measureTransStruct.Section1);
            this._Ibb2.Text = this._analogDataBase.Value.IBB2(this._averagerTime.ValueList, this._measureTransStruct.Section2);
            this._Icb2.Text = this._analogDataBase.Value.ICB2(this._averagerTime.ValueList, this._measureTransStruct.Section2);

            try
            {
                this.u1Label.Text = this._analogDataBase.Value.U_NBC(this._measureTransStruct.Section1)+"1";
                this.u2Label.Text = this._analogDataBase.Value.U_NBC(this._measureTransStruct.Section2) + "2";

                this._U21.Text = this._analogDataBase.Value.U_NBC(this._measureTransStruct.Section1) == Un
                    ? Unull
                    : this._analogDataBase.Value.U_21(this._averagerTime.ValueList, this._measureTransStruct.Section1);
                this._U22.Text = this._analogDataBase.Value.U_NBC(this._measureTransStruct.Section2) == Un
                    ? Unull
                    : this._analogDataBase.Value.U_22(this._averagerTime.ValueList, this._measureTransStruct.Section2);
                this._isErrorConfigSection = false;
            }
            catch (Exception)
            {
                if (!this._isErrorConfigSection)
                {
                    MessageBox.Show(
                        "������������ ������������ ���������� ���������� ������, � ���������� �������� �������. ���������� U1, U2, U21, U22 �� ���������.",
                        "��������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this._isErrorConfigSection = true;
            }
            
            if (this._discretDataBase.Value.BlockSignal[6])  //_discretDataBase.Value.BlockSignal[6] (bool) �������� �� ����� ������ 1
            {
                this._Uc1c2.Text = this._analogDataBase.Value.UC1C2(this._averagerTime.ValueList,
                    this._measureTransStruct.Section1, this._discretDataBase.Value.BlockSignal[6]);
                this._Upod.Text = ValuesConverterCommon.Analog.DoubleToString3(this._measureTrans.Value.Section1.Up *this._measureTrans.Value.Section1.Tn*Convert.ToInt32(this._measureTrans.Value.Section1.TnKoeff))+"�";
            }
            else
            {
                this._Uc1c2.Text = this._analogDataBase.Value.UC1C2(this._averagerTime.ValueList,
                    this._measureTransStruct.Section2, this._discretDataBase.Value.BlockSignal[6]);
                this._Upod.Text = ValuesConverterCommon.Analog.DoubleToString3(this._measureTrans.Value.Section2.Up *this._measureTrans.Value.Section2.Tn * Convert.ToInt32(this._measureTrans.Value.Section2.TnKoeff))+"�";
            }
            this.ucLabel.Text = this._analogDataBase.Value.U_C1C2(this._discretDataBase.Value.InputDiscrets[6], this._discretDataBase.Value.InputDiscrets[7]);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr851Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr851MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return (Image)(BEMN.MR851.Properties.Resources.measuring1.ToBitmap()); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._sygnalState);
            LedManager.TurnOffLeds(this._inputSygnals);
            LedManager.TurnOffLeds(this._logicSygnals);
            LedManager.TurnOffLeds(this._relay);
            LedManager.TurnOffLeds(this._blockSignals);
            LedManager.TurnOffLeds(this._neisprPrivod);
            LedManager.TurnOffLeds(this._neisprSignalsDev);
            LedManager.TurnOffLeds(this._errLeds);
            if(this._modeLed1.Visible)
                LedManager.TurnOffLeds(this._modeLeds);
        }

        private void DiscretBdReadOk()
        {
            LedManager.SetLeds(this._sygnalState, this._discretDataBase.Value.SygnalStatus);
            LedManager.SetLeds(this._relay, this._discretDataBase.Value.Relay);
            LedManager.SetLeds(this._inputSygnals, this._discretDataBase.Value.InputDiscrets);
            LedManager.SetLeds(this._blockSignals, this._discretDataBase.Value.BlockSignal);
            LedManager.SetLeds(this._logicSygnals, this._discretDataBase.Value.LogicSygnals);
            LedManager.SetLeds(this._neisprPrivod, this._discretDataBase.Value.NeisprPrivod);
            LedManager.SetLeds(this._neisprSignalsDev, this._discretDataBase.Value.NeisprSignalsDev);
            LedManager.SetLeds(this._errLeds, this._discretDataBase.Value.Errors);
            if(this._modeLed1.Visible)
                LedManager.SetLeds(this._modeLeds, this._discretDataBase.Value.ModeSygnal);
        }
        /// <summary>
        /// ���������� �����
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        private void Measuring_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void Measuring_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._dateTime.RemoveStructQueries();
            this._measureTrans.RemoveStructQueries();
            this._discretDataBase.RemoveStructQueries();
            this._counter.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._dateTime.LoadStructCycle(this._device.DefaultTimeSpan);
                this._measureTrans.LoadStructCycle(this._device.DefaultTimeSpan);
                this._discretDataBase.LoadStructCycle(this._device.DefaultTimeSpan);
                this._counter.LoadStructCycle(this._device.DefaultTimeSpan);
            }
            else
            {
                this._dateTime.RemoveStructQueries();
                this._measureTrans.RemoveStructQueries();
                this._discretDataBase.RemoveStructQueries();
                this._counter.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void dateTimeControl1_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void _addBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1800, "���������");
        }
        private void _subBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1801, "�������");
        }
        private void _setSypBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1802, "���������� ������������� �����");
        }
        private void _resetSypBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1807, "�������� ������������� �����");
        }

        private void _resetBlockBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1803, "�������� ��������� ������� ����������");
        }

        private void _resetNeisprJSBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1805, "�������� ��������� ������ �������������");
        }

        private void _resetNewRecJSBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1806, "�������� ��������� ������ ��");
        }

        private void _resetSignalBtn_Click(object sender, EventArgs e)
        {
            this.ConfirmSdtu(0x1804, "�������� ���������");
        }
        private void ConfirmSdtu(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "��851", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber, this._device);
            }
        }

      
    }
}