﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR851.Configuration.Structures;

namespace BEMN.MR851.Measuring.Structures
{
    public class AnalogDataBaseStruct : StructBase
    {
        [Layout(0)] private ushort Ibb1;
        [Layout(1)] private ushort Icb1;
        [Layout(2)] private ushort Ibb2;
        [Layout(3)] private ushort Icb2;
        [Layout(4)] private ushort Reserve1;
        [Layout(5)] private ushort Reserve2;
        [Layout(6)] private ushort Reserve3;
        [Layout(7)] private ushort Reserve4;
        [Layout(8)] private ushort Uab1;
        [Layout(9)] private ushort Unbc1;
        [Layout(10)] private ushort Uab2;
        [Layout(11)] private ushort Unbc2;
        [Layout(12)] private ushort U21;
        [Layout(13)] private ushort U22;
        [Layout(14)] private ushort Upp1;
        [Layout(15)] private ushort Upp2;


        #region Аналоговая БД
        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            var count = list.Count();

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }


        public string IBB1(List<AnalogDataBaseStruct> list, SectionStruct s1)
        {
            var value = this.GetMean(list, o => o.Ibb1);
            return ValuesConverterCommon.Analog.GetI(value, 5*s1.RomIzmer.RomNominalI);
        }

        public string ICB1(List<AnalogDataBaseStruct> list, SectionStruct s1)
        {
            var value = this.GetMean(list, o => o.Icb1);
            return ValuesConverterCommon.Analog.GetI(value, 5 * s1.RomIzmer.RomNominalIo);
        }

        public string IBB2(List<AnalogDataBaseStruct> list, SectionStruct s2)
        {
            var value = this.GetMean(list, o => o.Ibb2);
            return ValuesConverterCommon.Analog.GetI(value, 5 * s2.RomIzmer.RomNominalI);
        }

        public string ICB2(List<AnalogDataBaseStruct> list, SectionStruct s2)
        {
            var value = this.GetMean(list, o => o.Icb2);
            return ValuesConverterCommon.Analog.GetI(value, 5 * s2.RomIzmer.RomNominalIo);
        }

        public string UAB1(List<AnalogDataBaseStruct> list, SectionStruct s1)
        {

            var value = this.GetMean(list, o => o.Uab1);
            return ValuesConverterCommon.Analog.GetU(value, s1.Tn*Convert.ToInt32(s1.TnKoeff));
        }

        public string UNBC1(List<AnalogDataBaseStruct> list, SectionStruct s1)
        {
            var value = this.GetMean(list, o => o.Unbc1);
            if (U_NBC(s1) == "Un")
            {
                return ValuesConverterCommon.Analog.GetU(value, s1.Tnnp*Convert.ToInt32(s1.TnnpKoeff));
            }
            else
            {
                return ValuesConverterCommon.Analog.GetU(value, s1.Tn*Convert.ToInt32(s1.TnnpKoeff));
            }
        } 

        public string OLDUNBC1(List<AnalogDataBaseStruct> list, SectionStruct s1)
        {
            var value = this.GetMean(list, o => o.Unbc1);

                return ValuesConverterCommon.Analog.GetU(value, s1.Tnnp*Convert.ToInt32(s1.TnnpKoeff));
            
        }

        public string U_21(List<AnalogDataBaseStruct> list, SectionStruct s1)
        {
            var value = this.GetMean(list, o => o.U21);
            return ValuesConverterCommon.Analog.GetU(value, s1.Tn * Convert.ToInt32(s1.TnKoeff));
        }

        public string UAB2(List<AnalogDataBaseStruct> list, SectionStruct s2)
        {
            var value = this.GetMean(list, o => o.Uab2);
            return ValuesConverterCommon.Analog.GetU(value, s2.Tn * Convert.ToInt32(s2.TnKoeff));
        }

        public string UNBC2(List<AnalogDataBaseStruct> list, SectionStruct s2)
        {
            var value = this.GetMean(list, o => o.Unbc2);
            if (U_NBC(s2) == "Un")
            {
                return ValuesConverterCommon.Analog.GetU(value, s2.Tnnp*Convert.ToInt32(s2.TnnpKoeff));
            }
            else
            {
                return ValuesConverterCommon.Analog.GetU(value, s2.Tn*Convert.ToInt32(s2.TnnpKoeff));
            }
        } 
        public string OLDUNBC2(List<AnalogDataBaseStruct> list, SectionStruct s2)
        {
            var value = this.GetMean(list, o => o.Unbc2);

                return ValuesConverterCommon.Analog.GetU(value, s2.Tnnp*Convert.ToInt32(s2.TnnpKoeff));
        }
        public string U_22(List<AnalogDataBaseStruct> list, SectionStruct s2)
        {
            var value = this.GetMean(list, o => o.U22);
            return ValuesConverterCommon.Analog.GetU(value, s2.Tn * Convert.ToInt32(s2.TnKoeff));
        }

        public string U_NBC(SectionStruct s)
        {
            return s.Measure.Split('+')[1]; 
        }

        public string UC1C2(List<AnalogDataBaseStruct> list, SectionStruct s, bool sectionNum)
        {
            return sectionNum ? UAB1(list, s) : UAB2(list, s);
        }

        public string U_C1C2(bool sectionNum, bool sectionNum2)

        {
            if (!sectionNum && !sectionNum2) return "Uc1";
            return sectionNum ? "Uc1" : "Uc2";
        }
        #endregion
    }
}
