﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR851.Measuring.Structures
{
    public class DiscretDataBaseStruct : StructBase
    {
        [Layout(0, Count = 11)] public ushort[] data;

        #region Битная БД

        public BitArray SygnalStatus
        {
            get
            {
                var array1 = new BitArray(new byte[] { Common.LOBYTE(this.data[0]) });
                var array2 = new BitArray(new byte[] { Common.LOBYTE(this.data[8]) });
                array2 = array2.Or(array1);
                return array2;
            }
        }

        public BitArray ModeSygnal
        {
            get
            {
                return new BitArray(new bool[]
                {
                    Common.GetBit(this.data[0], 4),
                    Common.GetBit(this.data[0], 8),
                    Common.GetBit(this.data[0], 9)
                });
            }
        }

        public BitArray Relay
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(data[3]),
                    Common.HIBYTE(data[3])
                });
            }
        }
        public BitArray InputDiscrets
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this.data[8]), 
                                                 Common.LOBYTE(this.data[9]) });
            }
        }

        public BitArray BlockSignal
        {
            get
            {
                return new BitArray(new byte[]{Common.HIBYTE(this.data[9])});
            }
        }

        public BitArray LogicSygnals
        {
            get
            {
                var res = Common.LOBYTE(this.data[10]);
                res = (byte) Common.SetBit(res, 0, Common.GetBit(this.data[2], 11));
                res = (byte)Common.SetBit(res, 1, Common.GetBit(this.data[2], 10));
                return new BitArray(new[]{res});
            }
        }

        public BitArray NeisprPrivod
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this.data[10]) });
            }
        }

        public BitArray NeisprSignalsDev
        {
            get
            {
                return new BitArray(new byte[] {Common.LOBYTE(this.data[4])});
            }
        }

        public BitArray Errors
        {
            get
            {
                return new BitArray(new byte[]{ Common.LOBYTE(data[5]), Common.HIBYTE(data[5])});
            }
        }
        #endregion
    }
}