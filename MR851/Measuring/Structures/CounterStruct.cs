﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR851.Measuring.Structures
{
    public class CounterMeasStruct : StructBase
    {
        [Layout(0)] private ushort _cur;
        [Layout(1)] private ushort _resourse;

        [BindingProperty(0)]
        public ushort Cur
        {
            get { return _cur; }
            set { _cur = value; }
        }

        [BindingProperty(1)]
        public ushort Res
        {
            get { return _resourse; }
            set { _resourse = value; }
        }
    }
}
