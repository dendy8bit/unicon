using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MBView
{
    public partial class LedView : Form, IFormView
    {
        public struct AdductionSettings
        {
            public AdductionSettings(double limit, ushort max, int symbols)
            {
                this.limit = limit;
                this.max = max;
                this.symbols = symbols;
            }

            public double limit;
            public ushort max;
            public int symbols;
        } 

        private const int WORD_CNT = 16;
        private UniversalDevice _device;
        private Button _currentButtton;
        private HexTextbox _currentHex;
        private TextBox _currentAdduction;
        private LedControl[][] _words = new LedControl[WORD_CNT][];
        private AdductionSettings[] _adduction = new AdductionSettings[WORD_CNT];
        private Label[] _addrLabels = new Label[WORD_CNT];
        private HexTextbox[] _hexLabels;
        private HexTextbox[] _decLabels;
        private TextBox[] _adductionBoxes;

        public LedView()
        {
            this.InitializeComponent();
        }

        private readonly Size minSize = new Size(521, 232);
        private readonly Size maxSize = new Size(679, 363);

        public LedView(UniversalDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._words = new LedControl[WORD_CNT][];
            this._adduction = new AdductionSettings[WORD_CNT];
            this._addrLabels = new Label[WORD_CNT];
            this._hexLabels = new HexTextbox[WORD_CNT];
            this._decLabels = new HexTextbox[WORD_CNT];
            this._adductionBoxes = new TextBox[WORD_CNT];            
        }

        private void LedView_Load(object sender, EventArgs e)
        {
            this._deviceHex.Text = this._device.DeviceNumber.ToString();
            this._tittleBox.Text = this._device.Tittle;
            this.InitControls();
            this.InitDevice();
            this.SetAddrLabelText();

            this._paramHex.BorderStyle = BorderStyle.Fixed3D;
            this._addrHex.BorderStyle = BorderStyle.Fixed3D;
            this._deviceHex.BorderStyle = BorderStyle.Fixed3D;
            this._paramHex.Text = this._device.WordCnt.ToString("X");
            this._addrHex.Text = this._device.Address.ToString("X");
            this._bitfRadio.Checked = this._device.IsBit;

            try
            {
                this._device.DeviceNumberChanged +=
                    delegate(object o, byte oldN, byte newN)
                    {
                        Invoke(new DeviceNumberChangedHandler(this._device_DeviceNumberChanged),
                            new object[] {o, oldN, newN});
                    };
            }
            catch (InvalidOperationException)
            {
            }

            this._paramHex.Correct();
            this._addrHex.Correct();
            //Size = minSize;
        }


        private void _device_DeviceNumberChanged(object sender, byte oldNumber, byte newNumber)
        {
            this._deviceHex.TextChanged -= new EventHandler(this._deviceHex_TextChanged);
            this._deviceHex.Text = newNumber.ToString();
            this._deviceHex.TextChanged += new EventHandler(this._deviceHex_TextChanged);
        }

        private void InitControls()
        {
            this._deviceHex.HexBase = true;
            this._addrHex.HexBase = true;
            this._paramHex.HexBase = true;

            this._addrHex.Correct();
            this._deviceHex.Correct();
            this._paramHex.Correct();

            this._paramHex.TextChanged += new EventHandler(this._paramHex_TextChanged);
            this._addrHex.TextChanged += new EventHandler(this._addrHex_TextChanged);
            this._deviceHex.TextChanged += new EventHandler(this._deviceHex_TextChanged);


            for (int i = 0; i < WORD_CNT; i++)
            {
                this.CreateAddrLabel(i);
                this.CreateLedsRaw(i);
                this.CreateDecLabel(i);
                this.CreateHexLabel(i);
                this.CreateAdductionBox(i);

                this._ledsGroup.Controls.AddRange(this._words[i]);
                this._wordIndexCombo.Items.Add(i + 1);
                this._adduction[i] = new AdductionSettings(100.0, 65535, 3);
            }

            for (int j = 0; j < 4; j++)
            {
                this._symbolsCombo.Items.Add(j);
            }

            this._addrGroup.Controls.AddRange(this._addrLabels);
            this._decGroup.Controls.AddRange(this._decLabels);
            this._hexGroup.Controls.AddRange(this._hexLabels);
            this._adductionGroup.Controls.AddRange(this._adductionBoxes);

            this._wordIndexCombo.SelectedIndex = 0;
            this._symbolsCombo.SelectedIndex = 3;
        }

        private void InitDevice()
        {
            this._device.OkLoad += new Handler(this._device_OkLoad);
            this._device.FailLoad += new Handler(this._device_FailLoad);

            this.LoadWithCurrentSettings();
        }

        private void _device_FailLoad(object sender)
        {
            try
            {
                Invoke(new Handler(this.OnFailExchange), new object[] {sender});
            }

            catch (ObjectDisposedException)
            {
            }
            catch (InvalidOperationException)
            {
            }
        }

        private void _device_OkLoad(object sender)
        {
            try
            {
                Invoke(new Handler(this.OnOkExchange), new object[] {sender});
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void CreateLedsRaw(int i)
        {
            Point basePoint = new Point(12, 14 + 18*i);
            this._words[i] = new LedControl[16];
            for (int j = 0; j < 16; j++)
            {
                this._words[i][j] = new LedControl();


                if (j < WORD_CNT/2)
                {
                    this._words[i][j].Location = new Point(basePoint.X + j*12, basePoint.Y);
                }
                else
                {
                    this._words[i][j].Location = new Point(basePoint.X + j*12 + 6, basePoint.Y);
                }

                this._words[i][j].Size = new Size(13, 13);
                this._words[i][j].State = LedState.Off;

                this._words[i][j].pictureBox.MouseClick += new MouseEventHandler(this.Led_MouseClick);
            }
        }

        private ushort GetBitIndex(Point ledLocation)
        {
            int row;
            int column;

            if (0 == ledLocation.X%12)
            {
                row = ledLocation.X/12;
            }
            else
            {
                row = (ledLocation.X - 6)/12;
            }

            column = (ledLocation.Y - 14)/18;

            row = 16 - row;
            return (ushort) (column*16 + row + this._addrHex.Number);
        }

        /// <summary>
        /// �������� ����� �� ��������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Led_MouseClick(object sender, MouseEventArgs e)
        {
            //������ ���� ������ ���������
            if (this._bitfRadio.Checked)
            {
                LedControl led = (sender as PictureBox).Parent as LedControl;

                ushort bitInd = this.GetBitIndex(led.Location);

                switch (e.Button)
                {
                    case MouseButtons.Left:
                        //�� ����� ������ ����������� ���
                        if (LedState.Signaled == led.State)
                        {
                            this._device.SetBit(this._device.DeviceNumber, bitInd, true, "setBit" + bitInd + this._device.DeviceNumber, this._device);
                        }
                        else if (LedState.NoSignaled == led.State)
                        {
                            this._device.SetBit(this._device.DeviceNumber, bitInd, false, "setBit" + bitInd + this._device.DeviceNumber, this._device);
                        }
                        break;
                        //�� ������ ���������� ����
                    case MouseButtons.Right:
                        this._contextMenu.Items.Clear();
                        string set0 = "��������     ��� � - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        string set1 = "���������� ��� � - " + bitInd.ToString("X4") + "(" + bitInd + ")";
                        this._contextMenu.Items.Add(set0);
                        this._contextMenu.Items.Add(set1);
                        this._contextMenu.Items[0].Tag = this._contextMenu.Items[1].Tag = bitInd;
                        this._contextMenu.ItemClicked += new ToolStripItemClickedEventHandler(this._contextMenu_ItemClicked);
                        this._contextMenu.Show(MousePosition.X, MousePosition.Y);


                        break;
                    default:
                        break;
                }
            }
        }

        private void _contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ushort bitInd = (ushort) e.ClickedItem.Tag;
            try
            {
                if (e.ClickedItem == this._contextMenu.Items[0])
                {
                    this._device.SetBit(this._device.DeviceNumber, bitInd, false, "setBit" + bitInd + this._device.DeviceNumber, this._device);
                }
                if (e.ClickedItem == this._contextMenu.Items[1])
                {
                    this._device.SetBit(this._device.DeviceNumber, bitInd, true, "setBit" + bitInd + this._device.DeviceNumber, this._device);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }


        private void CreateAdductionBox(int i)
        {
            this._adductionBoxes[i] = new TextBox();
            this._adductionBoxes[i].Size = new Size(49, 13);
            this._adductionBoxes[i].Location = new Point(3, 11 + i*18);
            this._adductionBoxes[i].Tag = true;
            this._adductionBoxes[i].GotFocus += new EventHandler(this.LedView_GotFocus);
            this._adductionBoxes[i].LostFocus += new EventHandler(this.LedView_LostFocus);
        }


        private void CreateAddrLabel(int i)
        {
            this._addrLabels[i] = new Label();
            this._addrLabels[i].AutoSize = true;
            this._addrLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            this._addrLabels[i].Location = new Point(50, 11 + i*18);
            this._addrLabels[i].Size = new Size(34, 13);
            this._addrLabels[i].TextAlign = ContentAlignment.MiddleRight;
        }

        private void CreateDecLabel(int i)
        {
            this._decLabels[i] = new HexTextbox();
            this._decLabels[i].AutoSize = true;
            this._decLabels[i].HexBase = false;
            this._decLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            this._decLabels[i].Location = new Point(3, 11 + i*18);
            this._decLabels[i].Size = new Size(49, 13);
            this._decLabels[i].TextAlign = HorizontalAlignment.Center;
            this._decLabels[i].Text = i.ToString("D4");
            this._decLabels[i].Limit = ushort.MaxValue + 1;
            this._decLabels[i].Tag = true;
            this._decLabels[i].GotFocus += new EventHandler(this.LedView_GotFocus);
            this._decLabels[i].LostFocus += new EventHandler(this.LedView_LostFocus);
        }

        private void LedView_LostFocus(object sender, EventArgs e)
        {
            if (false == this._currentButtton.Focused)
            {
                Controls.Remove(this._currentButtton);

                (sender as Control).Tag = true;
            }
        }

        private void CreateSetButton(object sender)
        {
            GroupBox parent = (sender as Control).Parent as GroupBox;
            Point hexLocation = (sender as Control).Location;
            Size hexSize = (sender as Control).Size;
            Point buttonPoint =
                new Point(parent.Location.X + hexLocation.X + hexSize.Width, parent.Location.Y + hexLocation.Y);
            this._currentButtton = new Button();
            this._currentButtton.Location = buttonPoint;
            this._currentButtton.Size = new Size(30, hexSize.Height - 2);
            this._currentButtton.Text = "���.";
            this._currentButtton.TextAlign = ContentAlignment.MiddleLeft;
            this._currentButtton.Show();

            this._currentButtton.Click += new EventHandler(this._currentButtton_Click);
            Controls.Add(this._currentButtton);
            Controls.SetChildIndex(this._currentButtton, 0);
        }

        /// <summary>
        /// ���� ����� �������� �����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LedView_GotFocus(object sender, EventArgs e)
        {
            //������� ������ 
            this.CreateSetButton(sender);

            if (sender is HexTextbox)
            {
                //������ ���� ����� ��������� �� ���������
                this._currentHex = (sender as HexTextbox);
                this._currentHex.Tag = false;
            }
            if (sender is TextBox)
            {
                //������ ���� ����� ��������� �� ���������
                this._currentAdduction = sender as TextBox;
                this._currentAdduction.Tag = false;
            }
        }

        private void OnHexSet()
        {
            for (int i = 0; i < this._paramHex.Number; i++)
            {
                this._hexLabels[i].Correct();
                this._device.CurrentSlot.Value[i] = (ushort) (this._hexLabels[i].Number);
            }
        }

        private void OnDecSet()
        {
            for (int i = 0; i < this._paramHex.Number; i++)
            {
                this._decLabels[i].Correct();
                this._device.CurrentSlot.Value[i] = (ushort) (this._decLabels[i].Number);
            }
        }

        private void OnAdductionSet()
        {
            try
            {
                for (int i = 0; i < this._paramHex.Number; i++)
                {
                    this._device.CurrentSlot.Value[i] = this.FromAdductionToUshort(i);
                }
            }
            catch (FormatException)
            {
                return;
            }
        }

        private ushort FromAdductionToUshort(int i)
        {
            try
            {
                return (ushort) (this._adduction[i].max*Convert.ToDouble(this._adductionBoxes[i].Text)/this._adduction[i].limit);
            }
            catch (FormatException e)
            {
                throw e;
            }
        }

        private void _currentButtton_Click(object sender, EventArgs e)
        {
            Controls.Remove(sender as Button);
            if (null != this._currentHex)
            {
                this._currentHex.Correct();
                this._currentHex.Tag = true;
                if (this._currentHex.HexBase)
                {
                    this.OnHexSet();
                }
                else
                {
                    this.OnDecSet();
                }
            }
            else
            {
                this.OnAdductionSet();
                this._currentAdduction.Tag = true;
            }

            this._currentHex = null;
            this._currentAdduction = null;

            this._device.SaveQuery();
        }

        private void CreateHexLabel(int i)
        {
            this._hexLabels[i] = new HexTextbox();
            this._hexLabels[i].HexBase = true;
            this._hexLabels[i].AutoSize = true;
            this._hexLabels[i].Font = new Font("Microsoft Sans Serif", 8F);
            this._hexLabels[i].Location = new Point(3, 11 + i*18);
            this._hexLabels[i].Size = new Size(49, 13);
            this._hexLabels[i].TextAlign = HorizontalAlignment.Center;
            this._hexLabels[i].Text = i.ToString("X4");
            this._hexLabels[i].Limit = ushort.MaxValue + 1;
            this._hexLabels[i].Tag = true;
            this._hexLabels[i].GotFocus += new EventHandler(this.LedView_GotFocus);
            this._hexLabels[i].LostFocus += new EventHandler(this.LedView_LostFocus);
        }


        private void OnFailExchange(object o)
        {
            if (false == Disposing && false == IsDisposed)
            {
                this.TurnOff();
            }
        }

        private void TurnOff()
        {
            if (LedManager.IsControlsCreated(this._decLabels))
            {
                for (int i = 0; i < WORD_CNT; i++)
                {
                    LedManager.TurnOffLeds(this._words[i]);
                    this._hexLabels[i].Text = "";
                    this._decLabels[i].Text = "";
                    this._adductionBoxes[i].Text = "";
                }
            }
        }

        private void OnOkExchange(object o)
        {
            if (false == Disposing && false == IsDisposed)
            {
                byte[] buffer = Common.TOBYTES(this._device.CurrentSlot.Value, true);

                for (int i = 0; i < buffer.Length; i += 2)
                {
                    LedManager.SetLedsReverse(this._words[i/2], new BitArray(new byte[] {buffer[i + 1], buffer[i]}));
                    if ((bool) this._hexLabels[i/2].Tag)
                    {
                        this._hexLabels[i/2].Text = (Common.TOWORD(buffer[i], buffer[i + 1])).ToString("X4");
                    }
                    if ((bool) this._decLabels[i/2].Tag)
                    {
                        this._decLabels[i/2].Text = (Common.TOWORD(buffer[i], buffer[i + 1])).ToString("");
                    }

                    if ((bool) this._adductionBoxes[i/2].Tag)
                    {
                        ushort val = Common.TOWORD(buffer[i], buffer[i + 1]);
                        double adduct = val*this._adduction[i/2].limit/this._adduction[i/2].max;

                        this._adductionBoxes[i/2].Text = adduct.ToString("F" + this._adduction[i/2].symbols);
                    }
                }
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void SetAddrLabelText()
        {
            //���� ��������� �������
            if (this._wordfRadio.Checked)
            {
                for (int i = 0; i < this._addrLabels.Length; i++)
                {
                    //������ ������ 0xFFFF �� ������
                    if (this._addrHex.Number + i <= ushort.MaxValue)
                    {
                        this._addrLabels[i].Text = (this._addrHex.Number + i).ToString("X4");
                    }
                    else
                    {
                        this._addrLabels[i].Text = "";
                    }

                    //������ ������� �������, ����� ���� ����������
                    if (50 > this._addrLabels[i].Location.X)
                    {
                        this._addrLabels[i].Location = new Point(this._addrLabels[i].Location.X + 35, this._addrLabels[i].Location.Y);
                    }
                }
            }
            else
            {
                //���� ��������� ������
                for (int i = 0; i < this._addrLabels.Length; i++)
                {
                    if (this._addrHex.Number + i + 0xF <= ushort.MaxValue)
                    {
                        this._addrLabels[i].Text = (this._addrHex.Number + 0x10*i + 0xF).ToString("X4") + "-" +
                                              (this._addrHex.Number + 0x10*i).ToString("X4");
                    }
                    else
                    {
                        this._addrLabels[i].Text = "";
                    }

                    if (50 == this._addrLabels[i].Location.X)
                    {
                        this._addrLabels[i].Location = new Point(this._addrLabels[i].Location.X - 35, this._addrLabels[i].Location.Y);
                    }
                }
            }
        }

        #region IFormView

        public string NodeName
        {
            get { return "�������� ���"; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.db.ToBitmap();
            }
        }
        
        public Type FormDevice
        {
            get { return typeof (UniversalDevice); }
        }

        public bool Multishow { get; private set; }

        public bool Deletable
        {
            get { return false; }
        }

        public INodeView[] ChildNodes
        {
            get { return null; }
        }
        [Browsable(false)]
        public Type ClassType
        {
            get { return GetType(); }
        }

        public bool ForceShow
        {
            get { return false; }
        }
        #endregion

        private void _deviceHex_TextChanged(object sender, EventArgs e)
        {
            this._deviceHex.Correct();
            this._device.MB.RemoveQuery("mbviewLoad" + this._device.DeviceNumber);
            this._device.DeviceNumber = (byte)(this._deviceHex.Number);
            this.LoadWithCurrentSettings();
        }

        private void _addrHex_TextChanged(object sender, EventArgs e)
        {
            this._addrHex.Correct();
            this._device.Address = this._addrHex.Number;
            this.LoadWithCurrentSettings();
            this.SetAddrLabelText();
        }

        private void _copyBut_Click(object sender, EventArgs e)
        {
            LedView form = new LedView(new UniversalDevice(new Modbus(this._device.MB.Port)));
            form.Text = this.Text;
            form.MdiParent = MdiParent;
            form.Show();
        }

        private void LedView_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.MB.RemoveQuery("mbviewLoad" + this._device.DeviceNumber);
        }

        private void _bitfRadio_CheckedChanged(object sender, EventArgs e)
        {
            this._device.IsBit = this._bitfRadio.Checked;
            this.EnableValueBoxes(this._wordfRadio.Checked);
            this.SetAddrLabelText();
            this.LoadWithCurrentSettings();
        }

        private void LoadWithCurrentSettings()
        {
            if (this._exchangesCheck.Checked)
            {
                if (this._bitfRadio.Checked)
                {
                    if ((ushort) this._addrHex.Number < (ushort)(this._addrHex.Number + this._paramHex.Number * 0x10))
                    {
                        this._device.RemoveQuery();
                        this._device.CurrentSlot =
                            new Device.slot((ushort)(this._addrHex.Number), (ushort)(this._addrHex.Number + this._paramHex.Number * 0x10));
                        this._device.LoadCycleQueryAsBit();
                    }
                }
                else
                {
                    this._device.RemoveQuery();
                    this._device.CurrentSlot =
                        new Device.slot((ushort)(this._addrHex.Number), (ushort)(this._addrHex.Number + this._paramHex.Number));
                    this._device.LoadCycleQuery();
                }
            }
        }

        private void _paramHex_TextChanged(object sender, EventArgs e)
        {
            this._paramHex.Correct();
            this._device.WordCnt = this._paramHex.Number;
            if (this._paramHex.Number == 0)
            {
                return;
            }
            this._addrHex.Limit = ushort.MaxValue - this._paramHex.Number;
            this.TurnOff();
            this.LoadWithCurrentSettings();
        }

        private void EnableValueBoxes(bool bEnable)
        {
            for (int i = 0; i < WORD_CNT; i++)
            {
                this._hexLabels[i].Enabled = bEnable;
                this._decLabels[i].Enabled = bEnable;
                this._adductionBoxes[i].Enabled = bEnable;
            }
        }

        private void ShowAdduction(int wordIndex)
        {
            if (wordIndex <= WORD_CNT && wordIndex >= 0)
            {
                this._maxHex.Text = this._adduction[wordIndex].max.ToString();
                this._limitBox.Text = this._adduction[wordIndex].limit.ToString();
                this._symbolsCombo.SelectedIndex = this._adduction[wordIndex].symbols;
            }
        }

        private void _wordIndexCombo_TextUpdate(object sender, EventArgs e)
        {
            try
            {
                this.ShowAdduction(this.CurrentWordIndex);
            }
            catch (FormatException)
            {
                this._maxHex.Text = "";
                this._limitBox.Text = "";
                this._symbolsCombo.Text = "";
            }
        }

        /// <summary>
        /// ��������� ����� �����
        /// </summary>
        public int CurrentWordIndex
        {
            get
            {
                try
                {
                    int ret = Convert.ToInt32(this._wordIndexCombo.Text);
                    if (ret < 1 || ret > 16)
                    {
                        this._maxHex.Text = "";
                        this._limitBox.Text = "";
                        this._symbolsCombo.Text = "";
                    }
                    ret -= 1;

                    return ret;
                }
                catch (FormatException e)
                {
                    this._maxHex.Text = "";
                    this._limitBox.Text = "";
                    this._symbolsCombo.Text = "";
                    throw e;
                }
            }
            set { }
        }


        public int CountWords
        {
            get
            {
                try
                {
                    return Convert.ToInt32(this._wordsCount.Text);
                }
                catch
                {
                }
                return 0;
            }
        }


        private void _wordIndexCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            this.ShowAdduction(this.CurrentWordIndex);
        }

        private void _limitBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = this.CurrentWordIndex; i < this.CountWords + this.CurrentWordIndex; i++)
                {
                    this._adduction[i].limit = Convert.ToDouble(this._limitBox.Text);
                }               
            }
            catch (FormatException)
            {
                return;
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _maxHex_TextChanged(object sender, EventArgs e)
        {
            this._maxHex.Correct();
            try
            {
                for (int i = this.CurrentWordIndex; i < this.CountWords + this.CurrentWordIndex; i++)
                {
                    this._adduction[i].max = (ushort)this._maxHex.Number;
                }
            }
            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _symbolsCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = this.CurrentWordIndex; i < this.CountWords + this.CurrentWordIndex; i++)
                {
                    this._adduction[i].symbols = this._symbolsCombo.SelectedIndex;
                }
            }

            catch (IndexOutOfRangeException)
            {
                return;
            }
        }

        private void _exchangesCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._exchangesCheck.Checked)
            {
                this.LoadWithCurrentSettings();
            }
            else
            {
                this._device.RemoveQuery();
            }
        }

        private void LedView_Activated(object sender, EventArgs e)
        {
            this._device.SetQuerySpan(new TimeSpan(0, 0, 0, 0, 10));
        }

        private void LedView_Deactivate(object sender, EventArgs e)
        {
            this._device.SetQuerySpan(new TimeSpan(0, 0, 3));
        }

        private void _sizeChangeButton_Click(object sender, EventArgs e)
        {
            if ("����������" == this._sizeChangeButton.Text)
            {
                Size = this.maxSize;
                this._sizeChangeButton.Text = "��������";
            }
            else
            {
                Size = this.minSize;
                this._sizeChangeButton.Text = "����������";
            }
        }

        private void _tittleCheck_CheckedChanged(object sender, EventArgs e)
        {
            this._tittleBox.Enabled = this._tittleCheck.Checked;

            if (!this._tittleCheck.Checked)
            {
                this._device.Tittle = this._tittleBox.Text;
                this.Text = this._device.Tittle;
            }
        }


        public override string Text
        {
            get { return base.Text; }
            set
            {
                if (null != this._device)
                {
                    base.Text = this._device.Tittle;
                }
                else
                {
                    base.Text = value;
                }
            }
        }

        private void _wordsCount_TextChanged(object sender, EventArgs e)
        {
            this._wordsCount.Correct();
        }
    }
}