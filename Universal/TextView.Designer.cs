namespace BEMN.MBView
{
    partial class TextView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._newwinBut = new System.Windows.Forms.Button();
            this._exchangeCheck = new System.Windows.Forms.CheckBox();
            this._crcLabel = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._inHexbox = new BEMN.MBView.HexBox();
            this._outHexbox = new BEMN.MBView.HexBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _newwinBut
            // 
            this._newwinBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._newwinBut.Location = new System.Drawing.Point(568, 312);
            this._newwinBut.Name = "_newwinBut";
            this._newwinBut.Size = new System.Drawing.Size(74, 23);
            this._newwinBut.TabIndex = 2;
            this._newwinBut.Text = "����� ����";
            this._newwinBut.UseVisualStyleBackColor = true;
            this._newwinBut.Click += new System.EventHandler(this._newwinBut_Click);
            // 
            // _exchangeCheck
            // 
            this._exchangeCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exchangeCheck.AutoSize = true;
            this._exchangeCheck.Location = new System.Drawing.Point(494, 316);
            this._exchangeCheck.Name = "_exchangeCheck";
            this._exchangeCheck.Size = new System.Drawing.Size(68, 17);
            this._exchangeCheck.TabIndex = 3;
            this._exchangeCheck.Text = "������";
            this._exchangeCheck.UseVisualStyleBackColor = true;
            this._exchangeCheck.CheckedChanged += new System.EventHandler(this._exchangeCheck_CheckedChanged);
            // 
            // _crcLabel
            // 
            this._crcLabel.AutoSize = true;
            this._crcLabel.Location = new System.Drawing.Point(141, 317);
            this._crcLabel.Name = "_crcLabel";
            this._crcLabel.Size = new System.Drawing.Size(29, 13);
            this._crcLabel.TabIndex = 4;
            this._crcLabel.Text = "CRC";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._inHexbox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._outHexbox);
            this.splitContainer1.Size = new System.Drawing.Size(654, 307);
            this.splitContainer1.SplitterDistance = 153;
            this.splitContainer1.TabIndex = 5;
            // 
            // _inHexbox
            // 
            this._inHexbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._inHexbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inHexbox.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._inHexbox.LineInfoVisible = true;
            this._inHexbox.Location = new System.Drawing.Point(0, 0);
            this._inHexbox.Name = "_inHexbox";
            this._inHexbox.SelectionLength = ((long)(0));
            this._inHexbox.SelectionStart = ((long)(-1));
            this._inHexbox.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this._inHexbox.Size = new System.Drawing.Size(652, 151);
            this._inHexbox.StringViewVisible = true;
            this._inHexbox.TabIndex = 0;
            this._inHexbox.UseFixedBytesPerLine = true;
            this._inHexbox.VScrollBarVisible = true;
            // 
            // _outHexbox
            // 
            this._outHexbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._outHexbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outHexbox.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._outHexbox.LineInfoVisible = true;
            this._outHexbox.Location = new System.Drawing.Point(0, 0);
            this._outHexbox.Name = "_outHexbox";
            this._outHexbox.SelectionLength = ((long)(0));
            this._outHexbox.SelectionStart = ((long)(-1));
            this._outHexbox.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this._outHexbox.Size = new System.Drawing.Size(652, 148);
            this._outHexbox.StringViewVisible = true;
            this._outHexbox.TabIndex = 1;
            this._outHexbox.UseFixedBytesPerLine = true;
            this._outHexbox.VScrollBarVisible = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 317);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "CRC �������� �������";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 7;
            // 
            // TextView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 337);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this._crcLabel);
            this.Controls.Add(this._exchangeCheck);
            this.Controls.Add(this._newwinBut);
            this.Name = "TextView";
            this.Text = "TextView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TextView_FormClosing);
            this.Load += new System.EventHandler(this.TextView_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HexBox _inHexbox;
        private HexBox _outHexbox;
        private System.Windows.Forms.Button _newwinBut;
        private System.Windows.Forms.CheckBox _exchangeCheck;
        private System.Windows.Forms.Label _crcLabel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

    }
}