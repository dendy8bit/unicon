using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MBView
{
    public partial class TextView : Form , IFormView
    {
        private UniversalDevice _device;
        private Thread _thr;
        private byte[] _request;
        private byte[] _answer;
        private bool _runThread;
        private int _sleepTime = 10;
        private ManualResetEvent _waiter;
        private int _f;

        private delegate void SetHexboxBytesHandler(HexBox box, byte[] bytes);

        [Browsable(false)]
        public Type ClassType
        {
            get
            {
                return GetType();
            }
        }

        public void SetHexboxBytes(HexBox box,byte[] bytes)
        {                       
            if (0 == bytes.Length)
            {
                box.ByteProvider = new DynamicByteProvider(new byte[] { });
            }
            else
            {
                box.ByteProvider = new DynamicByteProvider(bytes);
                this._f++;
                this.label2.Text = this._f.ToString();
            }
        }

        public TextView()
        {
            this.InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
        }
       
        public TextView(UniversalDevice device)
        {
            this._device = device;
            this.InitializeComponent();

            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            this._exchangeCheck.Checked = false;
            DynamicByteProvider _byteProvider = new DynamicByteProvider(new byte[] {1,4,5,0,0,0x10});
            this._inHexbox.ByteProvider = _byteProvider;

            this.ShowCrc();
            this.SetRequest();

            this._inHexbox.ByteProvider.Changed += this.ByteProvider_Changed;
            this._waiter = new ManualResetEvent(false);
            this._thr = new Thread(this.PortThread);
        }
        
        private void SetRequest()
        {
            byte[] buffer = (this._inHexbox.ByteProvider as DynamicByteProvider).Bytes.GetBytes();
            this._request = new byte[buffer.Length + 2];
            buffer.CopyTo(this._request, 0);
            ushort crc = CRC16.CalcCrcFast(buffer, buffer.Length);
            this._request[buffer.Length ] = Common.HIBYTE(crc);
            this._request[buffer.Length + 1] = Common.LOBYTE(crc);
        }

        void  TextView_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._runThread = false;
            this._thr.Abort();
        }

        public void PortThread(object prm)
        {
            while (this._runThread)
            {
                Thread.Sleep(this._sleepTime);
                this._waiter.WaitOne();
                try
                {
                    if (this._device.MB.NetworkEnabled)
                    {
                        this.WriteReadTcp();
                    }
                    else
                    {
                        this.ReadWriteRtu();
                    }
                    Invoke(new SetHexboxBytesHandler(this.SetHexboxBytes), this._outHexbox, this._answer);
                }
                catch(Exception e)
                {
                    if (!this._runThread)
                    {
                        break;
                    }
                    (prm as Device).MB.Port.PurgeCOM();
                    this._answer = new byte[] { };
                    if(this._device.MB.Logging)
                        Logger.ErrorQuery(e);
                    try
                    {
                        Invoke(new SetHexboxBytesHandler(this.SetHexboxBytes), this._outHexbox, this._answer);
                    }
                    catch (InvalidOperationException)
                    { }
                }
            }
        }

        private void ReadWriteRtu()
        {
            if (this._device.MB.Logging)
            {
                Logger.GetQuery(this._request, "ModbusTextView" + this._device.DeviceNumber, this._device, "������", false);
            }
            int countIn = 256;
            switch (this._request[1])
            {
                case 4:
                    {
                        countIn = Common.TOWORD(this._request[4], this._request[5]) * 2 + 5;// query.readBuffer.Length + 5;
                        break;
                    }
            }
            this._answer = this._device.MB.Port.WriteRead(this._request, countIn) as byte[];
            if (this._device.MB.Logging)
            {
                Logger.GetQuery(this._answer, "ModbusTextView" + this._device.DeviceNumber, this._device, "�����", true);
            }
        }

        private void WriteReadTcp()
        {
            if (this._device.MB.TcpStream == null)
            {
                throw new Exception("����������� TCP �����������");
            }
            byte[] input;
            if (this._device.MB.ModbusType == ModbusType.ModbusTcp)
            {
                input = new byte[this._request.Length + 6 - 2];
                input[5] = (byte)(this._request.Length - 2);
                Array.Copy(this._request, 0, input, 6, this._request.Length - 2);
            }
            else
            {
                input = this._request;
            }
            byte[] output = new byte[0xff];
            if (this._device.MB.Logging)
            {
                Logger.GetQuery(input, "ModbusTextViewTCP" + this._device.DeviceNumber, this._device, "������", false);
            }
            this._device.MB.TcpStream.Write(input, 0, input.Length);
            int length = this._device.MB.TcpStream.Read(output, 0, output.Length);
            if (length < 6)
            {
                this._answer = null;
                throw new Exception("����� ����� �������� �����");
            }
            if (this._device.MB.ModbusType == ModbusType.ModbusTcp)
            {
                this._answer = new byte[length - 6 + 2];
                Array.Copy(output, 6, this._answer, 0, this._answer.Length - 2);
            }
            else
            {
                this._answer = new byte[length];
                Array.Copy(output, 0, this._answer, 0, this._answer.Length);
            }
            ushort crc = CRC16.CalcCrcFast(this._answer, this._answer.Length - 2);
            this._answer[this._answer.Length - 2] = Common.HIBYTE(crc);
            this._answer[this._answer.Length - 1] = Common.LOBYTE(crc);
            if (this._device.MB.Logging)
            {
                Logger.GetQuery(output, "ModbusTextViewTCP" + this._device.DeviceNumber, this._device, "�����", true);
            }
        }

        void ByteProvider_Changed(object sender, EventArgs e)
        {
            this.ShowCrc();
            this.SetRequest();
        }

        private void ShowCrc()
        {
            ushort crc = CRC16.CalcCrcFast((this._inHexbox.ByteProvider as DynamicByteProvider).Bytes.GetBytes(), (int) this._inHexbox.ByteProvider.Length);
            this._crcLabel.Text = crc.ToString("X4");
        }
        
        private void _exchangeCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._exchangeCheck.Checked)
            {
                this._f = 0;
                this._waiter.Set();
            }
            else
            {
                this._waiter.Reset();
            }
        }

        private void _newwinBut_Click(object sender, EventArgs e)
        {
            TextView form = new TextView(new UniversalDevice(new Modbus(this._device.MB.Port)));
            form.Text = Text;
            form.MdiParent = MdiParent;
            form.Show();
        }
        
        private void TextView_Load(object sender, EventArgs e)
        {
            this._runThread = true;
            this._thr.Start(this._device);
        }

        #region IFormView
        public INodeView[] ChildNodes
        {
            get
            {
                return null;
            }
        }
        public bool ForceShow
        {
            get
            {
                return false;
            }
        }

        public string NodeName
        {
            get
            {
                return "��������� ���";
            }
        }
        public bool Deletable
        {
            get
            {
                return false;
            }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.db.ToBitmap();
            }
        }

        public Type FormDevice
        {
            get
            {
                return typeof(UniversalDevice);
            }
        }

        public bool Multishow { get; private set; }

        public bool IsMustBeAdded(object parentTag)
        {
            return true;
        }
        #endregion
    }
}