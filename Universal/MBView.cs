﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.MBView
{
    public class UniversalDevice : Device, IDeviceView, ICustomizeNode
    {

        private string _tittle = "";

        [DisplayName("Заголовок")]
        public string Tittle
        {
            get { return this._tittle; }
            set
            {
                this._tittle = value;
                if (this.CustomNodeTextChanged != null)
                {
                    this.CustomNodeTextChanged(this, new CustomNodeTextEventArgs(this._tittle));
                }
            }
        }

        private slot _slot = new slot(0x0, 0x8);

        [Browsable(false)]
        public Type ClassType
        {
            get { return GetType(); }
        }

        public event Handler FailLoad;
        public event Handler OkLoad;

        [Browsable(false)]
        public slot CurrentSlot
        {
            get { return this._slot; }
            set { this._slot = value; }
        }

        public UniversalDevice()
        {
            this.Init();
        }

        private void Init()
        {
            this.WordCnt = 16;
            this.CreateLogHash();
        }

        public UniversalDevice(Modbus mb) : base(mb)
        {
            this.Init();
            this.MB = mb;
        }

        [Browsable(false)]
        public override string DeviceVersion
        {
            get { return string.Empty; }
        }

        [Browsable(false)]
        public override string DevicePlant
        {
            get { return string.Empty; }
        }

        private Hashtable _logHash;

        private void CreateLogHash()
        {
            this._logHash = new Hashtable();
            this._logHash.Add("mbviewLoad" + DeviceNumber, " байтовый вид" + DeviceNumber);

        }

        [Browsable(false)]
        public Hashtable LogHash
        {
            get
            {
                this.CreateLogHash();
                return this._logHash;
            }
        }

        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += new CompleteExchangeHandler(this.mb_CompleteExchange);
                }
            }
        }

        protected new void mb_CompleteExchange(Object o, Query query)
        {
            if (query.name == "mbviewLoad" + DeviceNumber)
            {
                Raise(query, this.OkLoad, this.FailLoad, ref this._slot);
            }

            base.mb_CompleteExchange(o, query);
        }

        public void RemoveQuery()
        {
            this.MB.RemoveQuery("mbviewLoad" + DeviceNumber);
        }

        public void SaveQuery()
        {
            SaveSlot(DeviceNumber, this.CurrentSlot, "mbviewSave" + DeviceNumber, this);
        }

        public void SetQuerySpan(TimeSpan span)
        {
            Query q = this.MB.GetQuery("mbviewLoad" + DeviceNumber);
            if (q != null)
            {
                q.raiseSpan = span;
            }
        }

        public void LoadCycleQuery()
        {
            LoadSlotCycle(DeviceNumber, this.CurrentSlot, "mbviewLoad" + DeviceNumber, new TimeSpan(1000), 20, this);
        }

        public void LoadCycleQueryAsBit()
        {
            LoadBitsCycle(DeviceNumber, this.CurrentSlot, "mbViewBitLoad" + DeviceNumber, new TimeSpan(1000), 20, this);
        }

        private int _address;

        public int Address
        {
            get { return this._address; }
            set { this._address = value; }
        }

        private int _wordCnt;

        public int WordCnt
        {
            get { return this._wordCnt; }
            set { this._wordCnt = value; }
        }

        private bool _isBit;

        public bool IsBit
        {
            get { return this._isBit; }
            set { this._isBit = value; }
        }

        #region Интерфейсы

        [Description("Название устройства"), Category("Общие")]
        [DisplayName("Название")]
        public string NodeName
        {
            get { return "МР-сеть"; }
        }

        [Browsable(false)]
        public Image NodeImage
        {
            get { return Resources.db.ToBitmap(); }
        }

        public INodeView[] ChildNodes
        {
            get { return null; }
        }

        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        public bool ForceShow
        {
            get { return false; }

        }

        #endregion

        public override XmlElement ToXml(XmlDocument doc)
        {
            XmlElement ret = base.ToXml(doc);

            XmlElement address = doc.CreateElement("Address");
            address.InnerText = this.Address.ToString("X");
            XmlElement wordCnt = doc.CreateElement("WordCnt");
            wordCnt.InnerText = this.WordCnt.ToString("X");
            XmlElement isBit = doc.CreateElement("IsBit");
            isBit.InnerText = this.IsBit.ToString();
            XmlElement tittle = doc.CreateElement("Tittle");
            tittle.InnerText = this.Tittle;
            ret.AppendChild(address);
            ret.AppendChild(wordCnt);
            ret.AppendChild(isBit);
            ret.AppendChild(tittle);
            return ret;
        }

        public override void FromXml(XmlElement element)
        {
            base.FromXml(element);
            foreach (XmlElement cel in element.ChildNodes)
            {
                if ("Address" == cel.Name)
                {
                    this.Address = Convert.ToInt32(cel.InnerText, 16);
                }
                if ("WordCnt" == cel.Name)
                {
                    this.WordCnt = Convert.ToInt32(cel.InnerText, 16);
                }
                if ("IsBit" == cel.Name)
                {
                    this.IsBit = Boolean.Parse(cel.InnerText);
                }
                if ("Tittle" == cel.Name)
                {
                    this.Tittle = cel.InnerText;
                }
            }
            this.MB = mb;
        }
        

        #region ICustomizeNode Members

        public event CustomNodeTextHandler CustomNodeTextChanged;

        #endregion
    }
}
