using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;

//using Be.Windows.Forms.Design;

namespace BEMN.MBView
{
	#region HexCasing enumeration
	/// <summary>
	/// Specifies the case of hex characters in the HexBox control
	/// </summary>
	public enum HexCasing 
	{ 
		/// <summary>
		/// Converts all characters to uppercase.
		/// </summary>
		Upper = 0, 
		/// <summary>
		/// Converts all characters to lowercase.
		/// </summary>
		Lower = 1 
	}
	#endregion

	#region BytePositionInfo structure
	/// <summary>
	/// Represents a position in the HexBox control
	/// </summary>
	struct BytePositionInfo
	{
		public BytePositionInfo(long index, int characterPosition)
		{
			this._index = index;
			this._characterPosition = characterPosition;
		}

		public int CharacterPosition
		{
			get { return this._characterPosition; }
		} int _characterPosition;

		public long Index
		{
			get { return this._index; }
		} long _index;
	}
	#endregion

	/// <summary>
	/// Represents a hex box control.
	/// </summary>
	[ToolboxBitmap(typeof(HexBox), "HexBox.bmp")]
	public class HexBox : Control
	{
		#region IKeyInterpreter interface
		/// <summary>
		/// Defines a user input handler such as for mouse and keyboard input
		/// </summary>
		interface IKeyInterpreter
		{
			/// <summary>
			/// Activates mouse events
			/// </summary>
			void Activate();
			/// <summary>
			/// Deactivate mouse events
			/// </summary>
			void Deactivate();
			/// <summary>
			/// Preprocesses WM_KEYUP window message.
			/// </summary>
			/// <param name="m">the Message object to process.</param>
			/// <returns>True, if the message was processed.</returns>
			bool PreProcessWmKeyUp(ref Message m);
			/// <summary>
			/// Preprocesses WM_CHAR window message.
			/// </summary>
			/// <param name="m">the Message object to process.</param>
			/// <returns>True, if the message was processed.</returns>
			bool PreProcessWmChar(ref Message m);
			/// <summary>
			/// Preprocesses WM_KEYDOWN window message.
			/// </summary>
			/// <param name="m">the Message object to process.</param>
			/// <returns>True, if the message was processed.</returns>
			bool PreProcessWmKeyDown(ref Message m);
			/// <summary>
			/// Gives some information about where to place the caret.
			/// </summary>
			/// <param name="byteIndex">the index of the byte</param>
			/// <returns>the position where the caret is to place.</returns>
			PointF GetCaretPointF(long byteIndex);
		}
		#endregion

		#region EmptyKeyInterpreter class
		/// <summary>
		/// Represents an empty input handler without any functionality. 
		/// If is set ByteProvider to null, then this interpreter is used.
		/// </summary>
		class EmptyKeyInterpreter : IKeyInterpreter
		{
			HexBox _hexBox;

			public EmptyKeyInterpreter(HexBox hexBox)
			{
				this._hexBox = hexBox;
			}

			#region IKeyInterpreter Members
			public void Activate(){}
			public void Deactivate(){}

			public bool PreProcessWmKeyUp(ref Message m)
			{ return this._hexBox.BasePreProcessMessage(ref m); }

			public bool PreProcessWmChar(ref Message m)
			{ return this._hexBox.BasePreProcessMessage(ref m); }

			public bool PreProcessWmKeyDown(ref Message m)
			{ return this._hexBox.BasePreProcessMessage(ref m); }

			public PointF GetCaretPointF(long byteIndex)
			{ return new PointF ();	}

			#endregion
		}
		#endregion

		#region KeyInterpreter class
		/// <summary>
		/// Handles user input such as mouse and keyboard input during hex view edit
		/// </summary>
		class KeyInterpreter : IKeyInterpreter
		{
			#region Fields
			/// <summary>
			/// Contains the parent HexBox control
			/// </summary>
			protected HexBox _hexBox;

			/// <summary>
			/// Contains True, if shift key is down
			/// </summary>
			protected bool _shiftDown;
			/// <summary>
			/// Contains True, if mouse is down
			/// </summary>
			bool _mouseDown;
			/// <summary>
			/// Contains the selection start position info
			/// </summary>
			BytePositionInfo _bpiStart;
			/// <summary>
			/// Contains the current mouse selection position info
			/// </summary>
			BytePositionInfo _bpi;
			#endregion

			#region Ctors
			public KeyInterpreter(HexBox hexBox)
			{
				this._hexBox = hexBox;
			}
			#endregion

			#region Activate, Deactive methods
			public virtual void Activate()
			{
				this._hexBox.MouseDown += new MouseEventHandler(this.BeginMouseSelection);
				this._hexBox.MouseMove += new MouseEventHandler(this.UpdateMouseSelection);
				this._hexBox.MouseUp += new MouseEventHandler(this.EndMouseSelection);
			}

			public virtual void Deactivate()
			{
				this._hexBox.MouseDown -= new MouseEventHandler(this.BeginMouseSelection);
				this._hexBox.MouseMove -= new MouseEventHandler(this.UpdateMouseSelection);
				this._hexBox.MouseUp -= new MouseEventHandler(this.EndMouseSelection);
			}
			#endregion

			#region Mouse selection methods
			void BeginMouseSelection(object sender, MouseEventArgs e)
			{
				System.Diagnostics.Debug.WriteLine("BeginMouseSelection()", "KeyInterpreter");

				this._mouseDown = true;

				if(!this._shiftDown)
				{
					this._bpiStart = new BytePositionInfo(this._hexBox._bytePos, this._hexBox._byteCharacterPos);
					this._hexBox.ReleaseSelection();
				}
				else
				{
					this.UpdateMouseSelection(this, e);
				}
			}

			void UpdateMouseSelection(object sender, MouseEventArgs e)
			{
				if(!this._mouseDown)
					return;

				this._bpi = this.GetBytePositionInfo(new Point(e.X, e.Y));
				long selEnd = this._bpi.Index;
				long realselStart;
				long realselLength;

				if(selEnd < this._bpiStart.Index)
				{
					realselStart = selEnd;
					realselLength = this._bpiStart.Index - selEnd;
				}
				else if(selEnd > this._bpiStart.Index)
				{
					realselStart = this._bpiStart.Index;
					realselLength = selEnd - realselStart;
				}
				else
				{
					realselStart = this._hexBox._bytePos;
					realselLength = 0;
				}

				if(realselStart != this._hexBox._bytePos || realselLength != this._hexBox._selectionLength)
				{
					this._hexBox.InternalSelect(realselStart, realselLength);
				}
			}

			void EndMouseSelection(object sender, MouseEventArgs e)
			{
				this._mouseDown = false;
			}
			#endregion

			#region PrePrcessWmKeyDown methods
			public virtual bool PreProcessWmKeyDown(ref Message m)
			{
				System.Diagnostics.Debug.WriteLine("PreProcessWmKeyDown(ref Message m)", "KeyInterpreter");

				Keys vc = (Keys)m.WParam.ToInt32();

				Keys keyData = vc | Control.ModifierKeys;

				switch(keyData)
				{
					case Keys.Left:						
					case Keys.Up:						
					case Keys.Right:					
					case Keys.Down:						
					case Keys.PageUp:					
					case Keys.PageDown:					
					case Keys.Left | Keys.Shift:
					case Keys.Up | Keys.Shift:
					case Keys.Right | Keys.Shift:
					case Keys.Down | Keys.Shift:
					case Keys.Tab:
					case Keys.Back:
					case Keys.Delete:
					case Keys.Home:
					case Keys.End:
					case Keys.ShiftKey | Keys.Shift:
					case Keys.C | Keys.Control:
					case Keys.X | Keys.Control:
					case Keys.V | Keys.Control:
						if(this.RaiseKeyDown(keyData))
							return true;
						break;
				}

				switch(keyData)
				{
					case Keys.Left:						// move left
						return this.PreProcessWmKeyDown_Left(ref m);
					case Keys.Up:						// move up
						return this.PreProcessWmKeyDown_Up(ref m);
					case Keys.Right:					// move right
						return this.PreProcessWmKeyDown_Right(ref m);
					case Keys.Down:						// move down
						return this.PreProcessWmKeyDown_Down(ref m);
					case Keys.PageUp:					// move pageup
						return this.PreProcessWmKeyDown_PageUp(ref m);
					case Keys.PageDown:					// move pagedown
						return this.PreProcessWmKeyDown_PageDown(ref m);
					case Keys.Left | Keys.Shift:		// move left with selection
						return this.PreProcessWmKeyDown_ShiftLeft(ref m);
					case Keys.Up | Keys.Shift:			// move up with selection
						return this.PreProcessWmKeyDown_ShiftUp(ref m);
					case Keys.Right | Keys.Shift:		// move right with selection
						return this.PreProcessWmKeyDown_ShiftRight(ref m);
					case Keys.Down | Keys.Shift:		// move down with selection
						return this.PreProcessWmKeyDown_ShiftDown(ref m);
					case Keys.Tab:						// switch focus to string view
						return this.PreProcessWmKeyDown_Tab(ref m);
					case Keys.Back:						// back
						return this.PreProcessWmKeyDown_Back(ref m);
					case Keys.Delete:					// delete
						return this.PreProcessWmKeyDown_Delete(ref m);
					case Keys.Home:						// move to home
						return this.PreProcessWmKeyDown_Home(ref m);
					case Keys.End:						// move to end
						return this.PreProcessWmKeyDown_End(ref m);
					case Keys.ShiftKey | Keys.Shift:	// begin selection process
						return this.PreProcessWmKeyDown_ShiftShiftKey(ref m);
					case Keys.C | Keys.Control:			// copy
						return this.PreProcessWmKeyDown_ControlC(ref m);
					case Keys.X | Keys.Control:			// cut
						return this.PreProcessWmKeyDown_ControlX(ref m);
					case Keys.V | Keys.Control:			// paste
						return this.PreProcessWmKeyDown_ControlV(ref m);
					default:
						this._hexBox.ScrollByteIntoView();
						return this._hexBox.BasePreProcessMessage(ref m);
				}
			}

			protected bool RaiseKeyDown(Keys keyData)
			{
				KeyEventArgs e = new KeyEventArgs(keyData);
				this._hexBox.OnKeyDown(e);
				return e.Handled;
			}

			protected virtual bool PreProcessWmKeyDown_Left(ref Message m)
			{
				return this.PerformPosMoveLeft();
			}

			protected virtual bool PreProcessWmKeyDown_Up(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if( !(pos == 0 && cp == 0) )
				{
					pos = Math.Max(-1, pos-this._hexBox._iHexMaxHBytes);
					if(pos == -1)
						return true;

					this._hexBox.SetPosition(pos);

					if(pos < this._hexBox._startByte)
					{
						this._hexBox.PerformScrollLineUp();
					}

					this._hexBox.UpdateCaret();
					this._hexBox.Invalidate();
				}

				this._hexBox.ScrollByteIntoView();
				this._hexBox.ReleaseSelection();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_Right(ref Message m)
			{
				return this.PerformPosMoveRight();
			}
			
			protected virtual bool PreProcessWmKeyDown_Down(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if(pos == this._hexBox._byteProvider.Length && cp == 0)
					return true;

				pos = Math.Min(this._hexBox._byteProvider.Length, pos+this._hexBox._iHexMaxHBytes);

				if(pos == this._hexBox._byteProvider.Length)
					cp = 0;

				this._hexBox.SetPosition(pos, cp);
			
				if(pos > this._hexBox._endByte-1)
				{
					this._hexBox.PerformScrollLineDown();
				}

				this._hexBox.UpdateCaret();
				this._hexBox.ScrollByteIntoView();
				this._hexBox.ReleaseSelection();
				this._hexBox.Invalidate();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_PageUp(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if(pos == 0 && cp == 0)
					return true;

				pos = Math.Max(0, pos-this._hexBox._iHexMaxBytes);
				if(pos == 0)
					return true;

				this._hexBox.SetPosition(pos);

				if(pos < this._hexBox._startByte)
				{
					this._hexBox.PerformScrollPageUp();
				}

				this._hexBox.ReleaseSelection();
				this._hexBox.UpdateCaret();
				this._hexBox.Invalidate();
				return true;
			}

			protected virtual bool PreProcessWmKeyDown_PageDown(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;
				
				if(pos == this._hexBox._byteProvider.Length && cp == 0)
					return true;

				pos = Math.Min(this._hexBox._byteProvider.Length, pos+this._hexBox._iHexMaxBytes);

				if(pos == this._hexBox._byteProvider.Length)
					cp = 0;

				this._hexBox.SetPosition(pos, cp);
			
				if(pos > this._hexBox._endByte-1)
				{
					this._hexBox.PerformScrollPageDown();
				}

				this._hexBox.ReleaseSelection();
				this._hexBox.UpdateCaret();
				this._hexBox.Invalidate();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ShiftLeft(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;

				if(pos + sel < 1)
					return true;

				if(pos+sel <= this._bpiStart.Index)
				{
					if(pos == 0)
						return true;

					pos--;
					sel++;
				}
				else
				{
					sel = Math.Max(0, sel-1);
				}

				this._hexBox.ScrollByteIntoView();
				this._hexBox.InternalSelect(pos, sel);

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ShiftUp(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;

				if(pos-this._hexBox._iHexMaxHBytes < 0 && pos <= this._bpiStart.Index)
					return true;

				if(this._bpiStart.Index >= pos+sel)
				{
					pos = pos - this._hexBox._iHexMaxHBytes;
					sel += this._hexBox._iHexMaxHBytes;
					this._hexBox.InternalSelect(pos, sel);
					this._hexBox.ScrollByteIntoView();
				}
				else
				{
					sel -= this._hexBox._iHexMaxHBytes;
					if(sel < 0)
					{
						pos = this._bpiStart.Index + sel;
						sel = -sel;
						this._hexBox.InternalSelect(pos, sel);
						this._hexBox.ScrollByteIntoView();
					}
					else
					{
						sel -= this._hexBox._iHexMaxHBytes;
						this._hexBox.InternalSelect(pos, sel);
						this._hexBox.ScrollByteIntoView(pos+sel);
					}
				}

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ShiftRight(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;

				if(pos+sel >= this._hexBox._byteProvider.Length)
					return true;

				if(this._bpiStart.Index <= pos)
				{
					sel++;
					this._hexBox.InternalSelect(pos, sel);
					this._hexBox.ScrollByteIntoView(pos+sel);
				}
				else
				{
					pos++;
					sel = Math.Max(0, sel-1);
					this._hexBox.InternalSelect(pos, sel);
					this._hexBox.ScrollByteIntoView();
				}

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ShiftDown(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;

				long max = this._hexBox._byteProvider.Length;

				if(pos+sel+this._hexBox._iHexMaxHBytes > max)
					return true;

				if(this._bpiStart.Index <= pos)
				{
					sel += this._hexBox._iHexMaxHBytes;
					this._hexBox.InternalSelect(pos, sel);
					this._hexBox.ScrollByteIntoView(pos+sel);
				}
				else
				{
					sel -= this._hexBox._iHexMaxHBytes;
					if(sel < 0)
					{
						pos = this._bpiStart.Index;
						sel = -sel;
					}
					else
					{
						pos += this._hexBox._iHexMaxHBytes;
						sel -= this._hexBox._iHexMaxHBytes;
					}

					this._hexBox.InternalSelect(pos, sel);
					this._hexBox.ScrollByteIntoView();
				}
						
				return true;
			}

			protected virtual bool PreProcessWmKeyDown_Tab(ref Message m)
			{
				if(this._hexBox._stringViewVisible && this._hexBox._keyInterpreter.GetType() == typeof(KeyInterpreter))
				{
					this._hexBox.ActivateStringKeyInterpreter();
					this._hexBox.ScrollByteIntoView();
					this._hexBox.ReleaseSelection();
					this._hexBox.UpdateCaret();
					this._hexBox.Invalidate();
					return true;
				}

				if(this._hexBox.Parent == null) return true;
				this._hexBox.Parent.SelectNextControl(this._hexBox, true, true, true, true);
				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ShiftTab(ref Message m)
			{
				if(this._hexBox._keyInterpreter is StringKeyInterpreter)
				{
					this._shiftDown = false;
					this._hexBox.ActivateKeyInterpreter();
					this._hexBox.ScrollByteIntoView();
					this._hexBox.ReleaseSelection();
					this._hexBox.UpdateCaret();
					this._hexBox.Invalidate();
					return true;
				}
				
				if(this._hexBox.Parent == null) return true;
				this._hexBox.Parent.SelectNextControl(this._hexBox, false, true, true, true);
				return true;
			}

			protected virtual bool PreProcessWmKeyDown_Back(ref Message m)
			{
				if(!this._hexBox._byteProvider.SupportsDeleteBytes())
					return true;

				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;
				int cp = this._hexBox._byteCharacterPos;

				long startDelete = (cp == 0 && sel == 0) ? pos-1 : pos;
				if(startDelete < 0 && sel < 1)
					return true;

				long bytesToDelete = (sel > 0) ? sel : 1;
				this._hexBox._byteProvider.DeleteBytes(Math.Max(0, startDelete), bytesToDelete);
				this._hexBox.UpdateScrollSize();

				if(sel == 0)
					this.PerformPosMoveLeftByte();

				this._hexBox.ReleaseSelection();
				this._hexBox.Invalidate();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_Delete(ref Message m)
			{
				if(!this._hexBox._byteProvider.SupportsDeleteBytes())
					return true;

				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;

				if(pos >= this._hexBox._byteProvider.Length)
					return true;

				long bytesToDelete = (sel > 0) ? sel : 1;
				this._hexBox._byteProvider.DeleteBytes(pos, bytesToDelete);

				this._hexBox.UpdateScrollSize();
				this._hexBox.ReleaseSelection();
				this._hexBox.Invalidate();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_Home(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if(pos < 1)
					return true;

				pos = 0;
				cp = 0;
				this._hexBox.SetPosition(pos, cp);

				this._hexBox.ScrollByteIntoView();
				this._hexBox.UpdateCaret();
				this._hexBox.ReleaseSelection();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_End(ref Message m)
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if(pos >= this._hexBox._byteProvider.Length-1)
					return true;

				pos = this._hexBox._byteProvider.Length;
				cp = 0;
				this._hexBox.SetPosition(pos, cp);

				this._hexBox.ScrollByteIntoView();
				this._hexBox.UpdateCaret();
				this._hexBox.ReleaseSelection();

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ShiftShiftKey(ref Message m)
			{
				if(this._mouseDown)
					return true;
				if(this._shiftDown)
					return true;

				this._shiftDown = true;

				if(this._hexBox._selectionLength > 0)
					return true;

				this._bpiStart = new BytePositionInfo(this._hexBox._bytePos, this._hexBox._byteCharacterPos);

				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ControlC(ref Message m)
			{
				this._hexBox.Copy();
				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ControlX(ref Message m)
			{
				this._hexBox.Cut();
				return true;
			}

			protected virtual bool PreProcessWmKeyDown_ControlV(ref Message m)
			{
				this._hexBox.Paste();
				return true;
			}

			#endregion

			#region PreProcessWmChar methods
			public virtual bool PreProcessWmChar(ref Message m)
			{
				if(Control.ModifierKeys == Keys.Control)
				{
					return this._hexBox.BasePreProcessMessage(ref m);
				}

				bool sw = this._hexBox._byteProvider.SupportsWriteByte();
				bool si = this._hexBox._byteProvider.SupportsInsertBytes();
				bool sd = this._hexBox._byteProvider.SupportsDeleteBytes();

				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;
				int cp = this._hexBox._byteCharacterPos;

				if(
					(!sw && pos != this._hexBox._byteProvider.Length) ||
					(!si && pos == this._hexBox._byteProvider.Length))
				{
					return this._hexBox.BasePreProcessMessage(ref m);
				}

				char c = (char)m.WParam.ToInt32();

				if(Uri.IsHexDigit(c))
				{
					if(this.RaiseKeyPress(c))
						return true;

					if(this._hexBox.ReadOnly)
						return true;

					bool isInsertMode = (pos == this._hexBox._byteProvider.Length);

					// do insert when insertActive = true
					if(!isInsertMode && si && this._hexBox._insertActive && cp == 0)
						isInsertMode = true;

					if(sd && si	&& sel > 0)
					{
						this._hexBox._byteProvider.DeleteBytes(pos, sel);
						isInsertMode = true;
						cp = 0;
						this._hexBox.SetPosition(pos, cp);
					}

					this._hexBox.ReleaseSelection();

					byte currentByte;
					if(isInsertMode)
						currentByte = 0;
					else
						currentByte = this._hexBox._byteProvider.ReadByte(pos);

					string sCb = currentByte.ToString("X", System.Threading.Thread.CurrentThread.CurrentCulture);
					if(sCb.Length == 1)
						sCb = "0" + sCb;

					string sNewCb = c.ToString();
					if(cp == 0)
						sNewCb += sCb.Substring(1, 1);
					else
						sNewCb = sCb.Substring(0, 1) + sNewCb;
					byte newcb = byte.Parse(sNewCb, System.Globalization.NumberStyles.AllowHexSpecifier, System.Threading.Thread.CurrentThread.CurrentCulture);
					if(isInsertMode)
						this._hexBox._byteProvider.InsertBytes(pos, new byte[]{newcb});
					else
						this._hexBox._byteProvider.WriteByte(pos, newcb);

					this.PerformPosMoveRight();

					this._hexBox.Invalidate();
					return true;
				}
				else
				{
					return this._hexBox.BasePreProcessMessage(ref m);
				}
			}

			protected bool RaiseKeyPress(char keyChar)
			{
				KeyPressEventArgs e = new KeyPressEventArgs(keyChar);
				this._hexBox.OnKeyPress(e);
				return e.Handled;
			}
			#endregion

			#region PreProcessWmKeyUp methods
			public virtual bool PreProcessWmKeyUp(ref Message m)
			{
				System.Diagnostics.Debug.WriteLine("PreProcessWmKeyUp(ref Message m)", "KeyInterpreter");

				Keys vc = (Keys)m.WParam.ToInt32();

				Keys keyData = vc | Control.ModifierKeys;

				switch(keyData)
				{
					case Keys.ShiftKey:
					case Keys.Insert:
						if(this.RaiseKeyUp(keyData))
							return true;
						break;
				}

				switch(keyData)
				{
					case Keys.ShiftKey:
						this._shiftDown = false;
						return true;
					case Keys.Insert:
						return this.PreProcessWmKeyUp_Insert(ref m);
					default:
						return this._hexBox.BasePreProcessMessage(ref m);
				}
			}

			protected virtual bool PreProcessWmKeyUp_Insert(ref Message m)
			{
				this._hexBox._insertActive = !this._hexBox._insertActive;
				return true;
			}

			protected bool RaiseKeyUp(Keys keyData)
			{
				KeyEventArgs e = new KeyEventArgs(keyData);
				this._hexBox.OnKeyUp(e);
				return e.Handled;
			}
			#endregion

			#region Misc
			protected virtual bool PerformPosMoveLeft()
			{
				long pos = this._hexBox._bytePos;
				long sel = this._hexBox._selectionLength;
				int cp = this._hexBox._byteCharacterPos;

				if(sel != 0)
				{
					cp = 0;
					this._hexBox.SetPosition(pos, cp);
					this._hexBox.ReleaseSelection();
				}
				else
				{
					if(pos == 0 && cp == 0)
						return true;

					if(cp > 0)
					{
						cp--;
					}
					else
					{
						pos = Math.Max(0, pos-1);
						cp++;
					}

					this._hexBox.SetPosition(pos, cp);

					if(pos < this._hexBox._startByte)
					{
						this._hexBox.PerformScrollLineUp();
					}
					this._hexBox.UpdateCaret();
					this._hexBox.Invalidate();
				}

				this._hexBox.ScrollByteIntoView();
				return true;
			}
			protected virtual bool PerformPosMoveRight()
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;
				long sel = this._hexBox._selectionLength;

				if(sel != 0)
				{
					pos += sel;
					cp = 0;
					this._hexBox.SetPosition(pos, cp);
					this._hexBox.ReleaseSelection();
				}
				else
				{
					if( !(pos == this._hexBox._byteProvider.Length && cp == 0) )
					{

						if(cp > 0)
						{
							pos = Math.Min(this._hexBox._byteProvider.Length, pos+1);
							cp = 0;
						}
						else
						{
							cp++;
						}

						this._hexBox.SetPosition(pos, cp);
			
						if(pos > this._hexBox._endByte-1)
						{
							this._hexBox.PerformScrollLineDown();
						}
						this._hexBox.UpdateCaret();
						this._hexBox.Invalidate();
					}
				}

				this._hexBox.ScrollByteIntoView();
				return true;
			}
			protected virtual bool PerformPosMoveLeftByte()
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if(pos == 0)
					return true;

				pos = Math.Max(0, pos-1);
				cp = 0;

				this._hexBox.SetPosition(pos, cp);

				if(pos < this._hexBox._startByte)
				{
					this._hexBox.PerformScrollLineUp();
				}
				this._hexBox.UpdateCaret();
				this._hexBox.ScrollByteIntoView();
				this._hexBox.Invalidate();

				return true;
			}

			protected virtual bool PerformPosMoveRightByte()
			{
				long pos = this._hexBox._bytePos;
				int cp = this._hexBox._byteCharacterPos;

				if(pos == this._hexBox._byteProvider.Length)
					return true;

				pos = Math.Min(this._hexBox._byteProvider.Length, pos+1);
				cp = 0;

				this._hexBox.SetPosition(pos, cp);
			
				if(pos > this._hexBox._endByte-1)
				{
					this._hexBox.PerformScrollLineDown();
				}
				this._hexBox.UpdateCaret();
				this._hexBox.ScrollByteIntoView();
				this._hexBox.Invalidate();

				return true;
			}


			public virtual PointF GetCaretPointF(long byteIndex)
			{
				System.Diagnostics.Debug.WriteLine("GetCaretPointF()", "KeyInterpreter");

				return this._hexBox.GetBytePointF(byteIndex);
			}

			protected virtual BytePositionInfo GetBytePositionInfo(Point p)
			{
				return this._hexBox.GetHexBytePositionInfo(p);
			}
			#endregion
		}
		#endregion

		#region StringKeyInterpreter class
		/// <summary>
		/// Handles user input such as mouse and keyboard input during string view edit
		/// </summary>
		class StringKeyInterpreter : KeyInterpreter
		{
			#region Ctors
			public StringKeyInterpreter(HexBox hexBox) : base(hexBox)
			{
				_hexBox._byteCharacterPos = 0;
			}
			#endregion

			#region PreProcessWmKeyDown methods
			public override bool PreProcessWmKeyDown(ref Message m)
			{
				Keys vc = (Keys)m.WParam.ToInt32();

				Keys keyData = vc | Control.ModifierKeys; 

				switch(keyData)
				{
					case Keys.Tab | Keys.Shift:
					case Keys.Tab:
						if(RaiseKeyDown(keyData))
							return true;
						break;
				}

				switch(keyData)
				{
					case Keys.Tab | Keys.Shift:
						return PreProcessWmKeyDown_ShiftTab(ref m);
					case Keys.Tab:
						return PreProcessWmKeyDown_Tab(ref m);
					default:
						return base.PreProcessWmKeyDown(ref m);
				}
			}

			protected override bool PreProcessWmKeyDown_Left(ref Message m)
			{
				return PerformPosMoveLeftByte();
			}

			protected override bool PreProcessWmKeyDown_Right(ref Message m)
			{
				return PerformPosMoveRightByte();
			}

			#endregion

			#region PreProcessWmChar methods
			public override bool PreProcessWmChar(ref Message m)
			{
				if(Control.ModifierKeys == Keys.Control)
				{
					return _hexBox.BasePreProcessMessage(ref m);
				}

				bool sw = _hexBox._byteProvider.SupportsWriteByte();
				bool si = _hexBox._byteProvider.SupportsInsertBytes();
				bool sd = _hexBox._byteProvider.SupportsDeleteBytes();

				long pos = _hexBox._bytePos;
				long sel = _hexBox._selectionLength;
				int cp = _hexBox._byteCharacterPos;

				if(
					(!sw && pos != _hexBox._byteProvider.Length) ||
					(!si && pos == _hexBox._byteProvider.Length))
				{
					return _hexBox.BasePreProcessMessage(ref m);
				}

				char c = (char)m.WParam.ToInt32();

				if(RaiseKeyPress(c))
					return true;

				if(_hexBox.ReadOnly)
					return true;

				bool isInsertMode = (pos == _hexBox._byteProvider.Length);

				// do insert when insertActive = true
				if(!isInsertMode && si && _hexBox._insertActive)
					isInsertMode = true;

				if(sd && si && sel > 0)
				{
					_hexBox._byteProvider.DeleteBytes(pos, sel);
					isInsertMode = true;
					cp = 0;
					_hexBox.SetPosition(pos, cp);
				}

				_hexBox.ReleaseSelection();

				if(isInsertMode)
					_hexBox._byteProvider.InsertBytes(pos, new byte[]{(byte)c});
				else
					_hexBox._byteProvider.WriteByte(pos, (byte)c);

				PerformPosMoveRightByte();
				_hexBox.Invalidate();

				return true;
			}
			#endregion

			#region Misc
			public override PointF GetCaretPointF(long byteIndex)
			{
				System.Diagnostics.Debug.WriteLine("GetCaretPointF()", "StringKeyInterpreter");

				Point gp = _hexBox.GetGridBytePoint(byteIndex);
				return _hexBox.GetByteStringPointF(gp);
			}

			protected override BytePositionInfo GetBytePositionInfo(Point p)
			{
				return _hexBox.GetStringBytePositionInfo(p);
			}
			#endregion
		}
		#endregion

		#region Fields
		/// <summary>
		/// Contains the hole content bounds of all text
		/// </summary>
		Rectangle _recContent;
		/// <summary>
		/// Contains the line info bounds
		/// </summary>
		Rectangle _recLineInfo;
		/// <summary>
		/// Contains the hex data bounds
		/// </summary>
		Rectangle _recHex;
		/// <summary>
		/// Contains the string view bounds
		/// </summary>
		Rectangle _recStringView;

		/// <summary>
		/// Contains string format information for text drawing
		/// </summary>
		StringFormat _stringFormat;
		/// <summary>
		/// Contains the width and height of a single char
		/// </summary>
		SizeF _charSize;

		/// <summary>
		/// Contains the maximum of visible horizontal bytes
		/// </summary>
		int _iHexMaxHBytes;
		/// <summary>
		/// Contains the maximum of visible vertical bytes
		/// </summary>
		int _iHexMaxVBytes;
		/// <summary>
		/// Contains the maximum of visible bytes.
		/// </summary>
		int _iHexMaxBytes;

		/// <summary>
		/// Contains the scroll bars minimum value
		/// </summary>
		long _scrollVmin;
		/// <summary>
		/// Contains the scroll bars maximum value
		/// </summary>
		long _scrollVmax;
		/// <summary>
		/// Contains the scroll bars current position
		/// </summary>
		long _scrollVpos;
		/// <summary>
		/// Contains a vertical scroll
		/// </summary>
		VScrollBar _vScrollBar;
		/// <summary>
		/// Contains the border�s left shift
		/// </summary>
		int _recBorderLeft = SystemInformation.Border3DSize.Width;
		/// <summary>
		/// Contains the border�s right shift
		/// </summary>
		int _recBorderRight = SystemInformation.Border3DSize.Width;
		/// <summary>
		/// Contains the border�s top shift
		/// </summary>
		int _recBorderTop = SystemInformation.Border3DSize.Height;
		/// <summary>
		/// Contains the border bottom shift
		/// </summary>
		int _recBorderBottom = SystemInformation.Border3DSize.Height;

		/// <summary>
		/// Contains the index of the first visible byte
		/// </summary>
		long _startByte;
		/// <summary>
		/// Contains the index of the last visible byte
		/// </summary>
		long _endByte;

		/// <summary>
		/// Contains the current byte position
		/// </summary>
		long _bytePos = -1;
		/// <summary>
		/// Contains the current char position in one byte
		/// </summary>
		/// <example>
		/// "1A"
		/// "1" = char position of 0
		/// "A" = char position of 1
		/// </example>
		int _byteCharacterPos;

		/// <summary>
		/// Contains string format information for hex values
		/// </summary>
		string _hexStringFormat = "X";


		/// <summary>
		/// Contains the current key interpreter
		/// </summary>
		IKeyInterpreter _keyInterpreter;
		/// <summary>
		/// Contains an empty key interpreter without functionality
		/// </summary>
		EmptyKeyInterpreter _eki;
		/// <summary>
		/// Contains the default key interpreter
		/// </summary>
		KeyInterpreter _ki;
		/// <summary>
		/// Contains the string key interpreter
		/// </summary>
		StringKeyInterpreter _ski;

		/// <summary>
		/// Contains True if caret is visible
		/// </summary>
		bool _caretVisible;

		/// <summary>
		/// Contains true, if the find (Find method) should be aborted.
		/// </summary>
		bool _abortFind;
		/// <summary>
		/// Contains a value of the current finding position.
		/// </summary>
		long _findingPos;

		/// <summary>
		/// Contains a state value about Insert or Write mode. When this value is true and the ByteProvider SupportsInsert is true bytes are inserted instead of overridden.
		/// </summary>
		bool _insertActive;
		#endregion

		#region Events
		/// <summary>
		/// Occurs, when the value of ReadOnly property has changed.
		/// </summary>
		[Description("Occurs, when the value of ReadOnly property has changed.")]
		public event EventHandler ReadOnlyChanged;
		/// <summary>
		/// Occurs, when the value of ByteProvider property has changed.
		/// </summary>
		[Description("Occurs, when the value of ByteProvider property has changed.")]
		public event EventHandler ByteProviderChanged;
		/// <summary>
		/// Occurs, when the value of SelectionStart property has changed.
		/// </summary>
		[Description("Occurs, when the value of SelectionStart property has changed.")]
		public event EventHandler SelectionStartChanged;
		/// <summary>
		/// Occurs, when the value of SelectionLength property has changed.
		/// </summary>
		[Description("Occurs, when the value of SelectionLength property has changed.")]
		public event EventHandler SelectionLengthChanged;
		/// <summary>
		/// Occurs, when the value of LineInfoVisible property has changed.
		/// </summary>
		[Description("Occurs, when the value of LineInfoVisible property has changed.")]
		public event EventHandler LineInfoVisibleChanged;
		/// <summary>
		/// Occurs, when the value of StringViewVisible property has changed.
		/// </summary>
		[Description("Occurs, when the value of StringViewVisible property has changed.")]
		public event EventHandler StringViewVisibleChanged;
		/// <summary>
		/// Occurs, when the value of BorderStyle property has changed.
		/// </summary>
		[Description("Occurs, when the value of BorderStyle property has changed.")]
		public event EventHandler BorderStyleChanged;
		/// <summary>
		/// Occurs, when the value of BytesPerLine property has changed.
		/// </summary>
		[Description("Occurs, when the value of BytesPerLine property has changed.")]
		public event EventHandler BytesPerLineChanged;
		/// <summary>
		/// Occurs, when the value of UseFixedBytesPerLine property has changed.
		/// </summary>
		[Description("Occurs, when the value of UseFixedBytesPerLine property has changed.")]
		public event EventHandler UseFixedBytesPerLineChanged;
		/// <summary>
		/// Occurs, when the value of VScrollBarVisible property has changed.
		/// </summary>
		[Description("Occurs, when the value of VScrollBarVisible property has changed.")]
		public event EventHandler VScrollBarVisibleChanged;
		/// <summary>
		/// Occurs, when the value of HexCasing property has changed.
		/// </summary>
		[Description("Occurs, when the value of HexCasing property has changed.")]
		public event EventHandler HexCasingChanged;
		/// <summary>
		/// Occurs, when the value of HorizontalByteCount property has changed.
		/// </summary>
		[Description("Occurs, when the value of HorizontalByteCount property has changed.")]
		public event EventHandler HorizontalByteCountChanged;
		/// <summary>
		/// Occurs, when the value of VerticalByteCount property has changed.
		/// </summary>
		[Description("Occurs, when the value of VerticalByteCount property has changed.")]
		public event EventHandler VerticalByteCountChanged;
		/// <summary>
		/// Occurs, when the value of CurrentLine property has changed.
		/// </summary>
		[Description("Occurs, when the value of CurrentLine property has changed.")]
		public event EventHandler CurrentLineChanged;
		/// <summary>
		/// Occurs, when the value of CurrentPositionInLine property has changed.
		/// </summary>
		[Description("Occurs, when the value of CurrentPositionInLine property has changed.")]
		public event EventHandler CurrentPositionInLineChanged;
		#endregion

		#region Ctors
		/// <summary>
		/// Initializes a new instance of a HexBox class.
		/// </summary>
		public HexBox()
		{
			this._vScrollBar = new VScrollBar();
			this._vScrollBar.Scroll += new ScrollEventHandler(this._vScrollBar_Scroll);

			this.BackColor = Color.White;
			this.Font = new Font("Courier New", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
			this._stringFormat = new StringFormat(StringFormat.GenericTypographic);
			this._stringFormat.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;

			this.ActivateEmptyKeyInterpreter();
			
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.ResizeRedraw, true);
		}
		#endregion

		#region Scroll methods
		void _vScrollBar_Scroll(object sender, ScrollEventArgs e)
		{
			switch(e.Type)
			{
				case ScrollEventType.Last:
					break;
				case ScrollEventType.EndScroll:
					break;
				case ScrollEventType.SmallIncrement:
					this.PerformScrollLineDown();
					break;
				case ScrollEventType.SmallDecrement:
					this.PerformScrollLineUp();
					break;
				case ScrollEventType.LargeIncrement:
					this.PerformScrollPageDown();
					break;
				case ScrollEventType.LargeDecrement:
					this.PerformScrollPageUp();
					break;
				case ScrollEventType.ThumbPosition:
					long lPos = this.FromScrollPos(e.NewValue);
					this.PerformScrollThumpPosition(lPos);
					break;
				case ScrollEventType.ThumbTrack:
					break;
				case ScrollEventType.First:
					break;
				default:
					break;
			}

			e.NewValue = this.ToScrollPos(this._scrollVpos);
		}

		void UpdateScrollSize()
		{
			System.Diagnostics.Debug.WriteLine("UpdateScrollSize()", "HexBox");

			// calc scroll bar info
			if(this.VScrollBarVisible && this._byteProvider != null && this._byteProvider.Length > 0 && this._iHexMaxHBytes != 0)
			{
				long scrollmax = (long)Math.Ceiling((double)this._byteProvider.Length / (double)this._iHexMaxHBytes - (double)this._iHexMaxVBytes);
				scrollmax = Math.Max(0, scrollmax);

				long scrollpos = this._startByte / this._iHexMaxHBytes;

				if(scrollmax == this._scrollVmax && scrollpos == this._scrollVpos)
					return;

				this._scrollVmin = 0;
				this._scrollVmax = scrollmax;
				this._scrollVpos = Math.Min(scrollpos, scrollmax);
				this.UpdateVScroll();
			}
			else if(this.VScrollBarVisible)
			{
				// disable scroll bar
				this._scrollVmin = 0;
				this._scrollVmax = 0;
				this._scrollVpos = 0;
				this.UpdateVScroll();
			}
		}

		void UpdateVScroll()
		{
			System.Diagnostics.Debug.WriteLine("UpdateVScroll()", "HexBox");

			int max = this.ToScrollMax(this._scrollVmax);

			if(max > 0)
			{ 
				this._vScrollBar.Minimum = 0;
				this._vScrollBar.Maximum = max;
				this._vScrollBar.Value = this.ToScrollPos(this._scrollVpos);
				this._vScrollBar.Enabled = true;
			}
			else
			{
				this._vScrollBar.Enabled = false;
			}
		}

		int ToScrollPos(long value)
		{
			int max = 65535;

			if(this._scrollVmax < max)
				return (int)value;
			else
			{
				double valperc = (double)value / (double)this._scrollVmax * (double)100;
				int res = (int)Math.Floor((double)max / (double)100 * valperc);
				res = (int)Math.Max(this._scrollVmin, res);
				res = (int)Math.Min(this._scrollVmax, res);
				return res;
			}
		}

		long FromScrollPos(int value)
		{
			int max = 65535;
			if(this._scrollVmax < max)
			{
				return (long)value;
			}
			else
			{
				double valperc = (double)value / (double)max * (double)100;
				long res = (int)Math.Floor((double)this._scrollVmax / (double)100 * valperc);
				return res;
			}
		}

		int ToScrollMax(long value)
		{
			long max = 65535;
			if(value > max)
				return (int)max;
			else
				return (int)value;
		}

		void PerformScrollToLine(long pos)
		{
			if(pos < this._scrollVmin || pos > this._scrollVmax || pos == this._scrollVpos )
				return;

			this._scrollVpos = pos;

			this.UpdateVScroll();
			this.UpdateVisibilityBytes();
			this.UpdateCaret();
			Invalidate();
		}

		void PerformScrollLines(int lines)
		{
			long pos;
			if(lines > 0)	
			{
				pos = Math.Min(this._scrollVmax, this._scrollVpos+lines);
			}
			else if(lines < 0)
			{
				pos = Math.Max(this._scrollVmin, this._scrollVpos+lines);
			}
			else
			{
				return;
			}

			this.PerformScrollToLine(pos);
		}

		void PerformScrollLineDown()
		{
			this.PerformScrollLines(1);
		}

		void PerformScrollLineUp()
		{
			this.PerformScrollLines(-1);
		}

		void PerformScrollPageDown()
		{
			this.PerformScrollLines(this._iHexMaxVBytes);
		}

		void PerformScrollPageUp()
		{
			this.PerformScrollLines(-this._iHexMaxVBytes);
		}

		void PerformScrollThumpPosition(long pos)
		{
			// Bug fix: Scroll to end, do not scroll to end
			int difference = (this._scrollVmax > 65535) ? 10 : 9;

			if(this.ToScrollPos(pos) == this.ToScrollMax(this._scrollVmax)-difference)
				pos = this._scrollVmax;
			// End Bug fix


			this.PerformScrollToLine(pos);
		}

		/// <summary>
		/// Scrolls the selection start byte into view
		/// </summary>
		public void ScrollByteIntoView()
		{
			System.Diagnostics.Debug.WriteLine("ScrollByteIntoView()", "HexBox");

			this.ScrollByteIntoView(this._bytePos);
		}

		/// <summary>
		/// Scrolls the specific byte into view
		/// </summary>
		/// <param name="index">the index of the byte</param>
		public void ScrollByteIntoView(long index)
		{
			System.Diagnostics.Debug.WriteLine("ScrollByteIntoView(long index)", "HexBox");

			if(this._byteProvider == null || this._keyInterpreter == null)
				return;

			if(index < this._startByte)
			{
				long line = (long)Math.Floor((double)index / (double)this._iHexMaxHBytes);
				this.PerformScrollThumpPosition(line);
			}
			else if(index > this._endByte)
			{
				long line = (long)Math.Floor((double)index / (double)this._iHexMaxHBytes);
				line -= this._iHexMaxVBytes-1;
				this.PerformScrollThumpPosition(line);
			}
		}
		#endregion

		#region Selection methods
		void ReleaseSelection()
		{
			System.Diagnostics.Debug.WriteLine("ReleaseSelection()", "HexBox");

			if(this._selectionLength == 0)
				return;
			this._selectionLength = 0;
			this.OnSelectionLengthChanged(EventArgs.Empty);

			if(!this._caretVisible)
				this.CreateCaret();
			else
				this.UpdateCaret();

			Invalidate();
		}

		/// <summary>
		/// Selects the hex box.
		/// </summary>
		/// <param name="start">the start index of the selection</param>
		/// <param name="length">the length of the selection</param>
		public void Select(long start, long length)
		{
			this.InternalSelect(start, length);
			this.ScrollByteIntoView();
		}

		void InternalSelect(long start, long length)
		{
			long pos = start;
			long sel = length;
			int cp = 0;

			if(sel > 0 && this._caretVisible)
				this.DestroyCaret();
			else if(sel == 0 && !this._caretVisible)
				this.CreateCaret();

			this.SetPosition(pos, cp);
			this.SetSelectionLength(sel);
			
			this.UpdateCaret();
			Invalidate();
		}
		#endregion

		#region Key interpreter methods
		void ActivateEmptyKeyInterpreter()
		{
			if(this._eki == null)
				this._eki = new EmptyKeyInterpreter(this);

			if(this._eki == this._keyInterpreter)
				return;

			if(this._keyInterpreter != null)
				this._keyInterpreter.Deactivate();

			this._keyInterpreter = this._eki;
			this._keyInterpreter.Activate();
		}

		void ActivateKeyInterpreter()
		{
			if(this._ki == null)
				this._ki = new KeyInterpreter(this);

			if(this._ki == this._keyInterpreter)
				return;

			if(this._keyInterpreter != null)
				this._keyInterpreter.Deactivate();

			this._keyInterpreter = this._ki;
			this._keyInterpreter.Activate();
		}

		void ActivateStringKeyInterpreter()
		{
			if(this._ski == null)
				this._ski = new StringKeyInterpreter(this);

			if(this._ski == this._keyInterpreter)
				return;

			if(this._keyInterpreter != null)
				this._keyInterpreter.Deactivate();

			this._keyInterpreter = this._ski;
			this._keyInterpreter.Activate();
		}
		#endregion

		#region Caret methods
		void CreateCaret()
		{
			if(this._byteProvider == null || this._keyInterpreter == null || this._caretVisible || !this.Focused)
				return;

			System.Diagnostics.Debug.WriteLine("CreateCaret()", "HexBox");

			NativeMethods.CreateCaret(Handle, IntPtr.Zero, 1, (int)this._charSize.Height);

			this.UpdateCaret();

			NativeMethods.ShowCaret(Handle);

			this._caretVisible = true;
		}

		void UpdateCaret()
		{
			if(this._byteProvider == null || this._keyInterpreter == null )
				return;

			System.Diagnostics.Debug.WriteLine("UpdateCaret()", "HexBox");

			long byteIndex =this._bytePos - this._startByte;
			PointF p = this._keyInterpreter.GetCaretPointF(byteIndex);
			p.X += this._byteCharacterPos*this._charSize.Width;
			NativeMethods.SetCaretPos((int)p.X, (int)p.Y);
		}

		void DestroyCaret()
		{
			if(!this._caretVisible)
				return;

			System.Diagnostics.Debug.WriteLine("DestroyCaret()", "HexBox");

			NativeMethods.DestroyCaret();
			this._caretVisible = false;
		}

		void SetCaretPosition(Point p)
		{
			System.Diagnostics.Debug.WriteLine("SetCaretPosition()", "HexBox");

			if(this._byteProvider == null || this._keyInterpreter == null)
				return;

			long pos = this._bytePos;
			int cp = this._byteCharacterPos;

			if(this._recHex.Contains(p))
			{
				BytePositionInfo bpi = this.GetHexBytePositionInfo(p);
				pos = bpi.Index;
				cp = bpi.CharacterPosition;

				this.SetPosition(pos, cp);

				this.ActivateKeyInterpreter();
				this.UpdateCaret();
				Invalidate();
			}
			else if(this._recStringView.Contains(p))
			{
				BytePositionInfo bpi = this.GetStringBytePositionInfo(p);
				pos = bpi.Index;
				cp = bpi.CharacterPosition;

				this.SetPosition(pos, cp);

				this.ActivateStringKeyInterpreter();
				this.UpdateCaret();
				Invalidate();
			}
		}

		BytePositionInfo GetHexBytePositionInfo(Point p)
		{
			System.Diagnostics.Debug.WriteLine("GetHexBytePositionInfo()", "HexBox");

			long bytePos;
			int byteCharaterPos;

			float x = ((float)(p.X - this._recHex.X) / this._charSize.Width);
			float y = ((float)(p.Y - this._recHex.Y) / this._charSize.Height);
			int iX = (int)x;
			int iY = (int)y;

			int hPos = (iX / 3 + 1);

			bytePos = Math.Min(this._byteProvider.Length,  
				this._startByte + (this._iHexMaxHBytes * (iY+1) - this._iHexMaxHBytes) + hPos - 1);
			byteCharaterPos = (iX % 3);
			if(byteCharaterPos > 1)
				byteCharaterPos = 1;

			if(bytePos == this._byteProvider.Length)
				byteCharaterPos = 0;

			if(bytePos < 0)
				return new BytePositionInfo(0, 0);
			return new BytePositionInfo(bytePos, byteCharaterPos);
		}

		BytePositionInfo GetStringBytePositionInfo(Point p)
		{
			System.Diagnostics.Debug.WriteLine("GetStringBytePositionInfo()", "HexBox");

			long bytePos;
			int byteCharacterPos;

			float x = ((float)(p.X - this._recStringView.X) / this._charSize.Width);
			float y = ((float)(p.Y - this._recStringView.Y) / this._charSize.Height);
			int iX = (int)x;
			int iY = (int)y;

			int hPos = iX+1;

			bytePos = Math.Min(this._byteProvider.Length,  
				this._startByte + (this._iHexMaxHBytes * (iY+1) - this._iHexMaxHBytes) + hPos - 1);
			byteCharacterPos = 0;

			if(bytePos < 0)
				return new BytePositionInfo(0, 0);
			return new BytePositionInfo(bytePos, byteCharacterPos);
		}
		#endregion

		#region PreProcessMessage methods
		/// <summary>
		/// Preprocesses windows messages.
		/// </summary>
		/// <param name="m">the message to process.</param>
		/// <returns>true, if the message was processed</returns>
		[SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode=true), SecurityPermission(SecurityAction.InheritanceDemand, UnmanagedCode=true)]
		public override bool PreProcessMessage(ref Message m)
		{
			switch(m.Msg)
			{
				case NativeMethods.WM_KEYDOWN:
					return this._keyInterpreter.PreProcessWmKeyDown(ref m);
				case NativeMethods.WM_CHAR:
					return this._keyInterpreter.PreProcessWmChar(ref m);
				case NativeMethods.WM_KEYUP:
					return this._keyInterpreter.PreProcessWmKeyUp(ref m);
				default:
					return base.PreProcessMessage (ref m);
			}
		}

		bool BasePreProcessMessage(ref Message m)
		{
			return base.PreProcessMessage(ref m);
		}
		#endregion

		#region Find methods
		/// <summary>
		/// Searches the current ByteProvider
		/// </summary>
		/// <param name="bytes">the array of bytes to find</param>
		/// <param name="startIndex">the start index</param>
		/// <returns>the SelectionStart property value if find was successfull or
		/// -1 if there is no match
		/// -2 if Find was aborted.</returns>
		public long Find(byte[] bytes, long startIndex)
		{
			int match = 0;
			int bytesLength = bytes.Length;

			this._abortFind = false;

			for(long pos = startIndex; pos < this._byteProvider.Length; pos++)
			{
				if(this._abortFind)
					return -2;

				if(pos % 1000 == 0) // for performance reasons: DoEvents only 1 times per 1000 loops
					Application.DoEvents();

				if(this._byteProvider.ReadByte(pos) != bytes[match])
				{
					pos -= match;
					match = 0;
					this._findingPos = pos;
					continue;
				}

				match++;

				if(match == bytesLength)
				{
					long bytePos = pos-bytesLength+1;
					this.Select(bytePos, bytesLength);
					this.ScrollByteIntoView(this._bytePos+this._selectionLength);
					this.ScrollByteIntoView(this._bytePos);

					return bytePos;
				}
			}

			return -1;
		}

		/// <summary>
		/// Aborts a working Find method.
		/// </summary>
		public void AbortFind()
		{
			this._abortFind = true;
		}

		/// <summary>
		/// Gets a value that indicates the current position during Find method execution.
		/// </summary>
		[DefaultValue(0), Browsable(false)]
		public long CurrentFindingPosition
		{
			get 
			{
				return this._findingPos;
			}
		}
		#endregion

		#region Copy, Cut and Paste methods
		/// <summary>
		/// Copies the current selection in the hex box to the Clipboard.
		/// </summary>
		public void Copy()
		{
			if(!this.CanCopy()) return;

			// put bytes into buffer
			byte[] buffer = new byte[this._selectionLength];
			int id = -1;
			for(long i = this._bytePos; i < this._bytePos+this._selectionLength; i++)
			{
				id++;

				buffer[id] = this._byteProvider.ReadByte(i);
			}

			DataObject da = new DataObject();

			// set string buffer clipbard data
			string sBuffer = System.Text.Encoding.ASCII.GetString(buffer, 0, buffer.Length);
			da.SetData(typeof(string), sBuffer);

			//set memorystream (BinaryData) clipboard data
			System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer, 0, buffer.Length, false, true);
			da.SetData("BinaryData", ms);

			Clipboard.SetDataObject(da, true);
			this.UpdateCaret();
			this.ScrollByteIntoView();
			Invalidate();
		}

		/// <summary>
		/// Return true if Copy method could be invoked.
		/// </summary>
		public bool CanCopy()
		{
			if(this._selectionLength < 1 || this._byteProvider == null)
				return false;

			return true;
		}

		/// <summary>
		/// Moves the current selection in the hex box to the Clipboard.
		/// </summary>
		public void Cut()
		{
			if(!this.CanCut()) return;

			this.Copy();

			this._byteProvider.DeleteBytes(this._bytePos, this._selectionLength);
			this._byteCharacterPos = 0;
			this.UpdateCaret();
			this.ScrollByteIntoView();
			this.ReleaseSelection();
			Invalidate();
			Refresh();
		}

		/// <summary>
		/// Return true if Cut method could be invoked.
		/// </summary>
		public bool CanCut()
		{
			if(this._byteProvider == null)
				return false;
			if(this._selectionLength < 1 || !this._byteProvider.SupportsDeleteBytes())
				return false;

			return true;
		}

		/// <summary>
		/// Replaces the current selection in the hex box with the contents of the Clipboard.
		/// </summary>
		public void Paste()
		{
			if(!this.CanPaste()) return;

			if(this._selectionLength > 0)
				this._byteProvider.DeleteBytes(this._bytePos, this._selectionLength);

			byte[] buffer = null;
			IDataObject da = Clipboard.GetDataObject();
			if(da.GetDataPresent("BinaryData"))
			{
				System.IO.MemoryStream ms = (System.IO.MemoryStream)da.GetData("BinaryData");
				buffer = new byte[ms.Length];
				ms.Read(buffer, 0, buffer.Length);
				
			}
			else if(da.GetDataPresent(typeof(string)))
			{
				string sBuffer = (string)da.GetData(typeof(string));
				buffer = System.Text.Encoding.ASCII.GetBytes(sBuffer);
			}
			else
			{
				return;
			}

			this._byteProvider.InsertBytes(this._bytePos, buffer);

			this.SetPosition(this._bytePos + buffer.Length, 0);

			this.ReleaseSelection();
			this.ScrollByteIntoView();
			this.UpdateCaret();
			Invalidate();
		}

		/// <summary>
		/// Return true if Paste method could be invoked.
		/// </summary>
		public bool CanPaste()
		{
			if(this._byteProvider == null || !this._byteProvider.SupportsInsertBytes())
				return false;

			if(!this._byteProvider.SupportsDeleteBytes() && this._selectionLength > 0)
				return false;

			IDataObject da = Clipboard.GetDataObject();
			if(da.GetDataPresent("BinaryData"))
				return true;
			else if(da.GetDataPresent(typeof(string)))
				return true;
			else
				return false;
		}

		#endregion

		#region Paint methods
		/// <summary>
		/// Paints the background.
		/// </summary>
		/// <param name="e">A PaintEventArgs that contains the event data.</param>
		protected override void OnPaintBackground(PaintEventArgs e)
		{
			e.Graphics.FillRectangle(new SolidBrush(this.BackColor), ClientRectangle);

			switch(this._borderStyle)
			{
				case BorderStyle.Fixed3D:
				{
					if(this.VisualStylesEnabled())
					{
						// draw xp themed border
						int partId = NativeMethods.EP_EDITTEXT;
				
						int stateId;
						if(this.Enabled)
							stateId = NativeMethods.ETS_NORMAL;
						else
							stateId = NativeMethods.ETS_DISABLED;

						NativeMethods.RECT rect = NativeMethods.RECT.FromRectangle(this.ClientRectangle);

						IntPtr hTheme = NativeMethods.OpenThemeData(this.Handle, "EDIT");

						IntPtr hDC = e.Graphics.GetHdc();
	
						NativeMethods.DrawThemeBackground(hTheme, hDC, partId, stateId, ref rect, IntPtr.Zero);

						e.Graphics.ReleaseHdc(hDC);

						NativeMethods.CloseThemeData(hTheme);
					}
					else
					{
						// draw default border
						ControlPaint.DrawBorder3D(e.Graphics, ClientRectangle, Border3DStyle.Sunken);
					}

					break;
				}
				case BorderStyle.FixedSingle:
				{
					// draw fixed single border
					ControlPaint.DrawBorder(e.Graphics, ClientRectangle, Color.Black, ButtonBorderStyle.Solid);
					break;
				}
			}
		}

		/// <summary>
		/// Paints the hex box.
		/// </summary>
		/// <param name="e">A PaintEventArgs that contains the event data.</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint (e);

			if(this._byteProvider == null)
				return;

			// draw only in the content rectangle, so exclude the border and the scrollbar.
			Region r = new Region(ClientRectangle);
			r.Exclude(this._recContent);
			e.Graphics.ExcludeClip(r);

			this.UpdateVisibilityBytes();

			if(this._lineInfoVisible)
				this.PaintLineInfo(e.Graphics, this._startByte, this._endByte);

			if(!this._stringViewVisible)
			{
				this.PaintHex(e.Graphics, this._startByte, this._endByte);
			}
			else
			{
				this.PaintHexAndStringView(e.Graphics, this._startByte, this._endByte);
				if(this._shadowSelectionVisible)
					this.PaintCurrentBytesSign(e.Graphics);
			}
		}


		void PaintLineInfo(Graphics g, long startByte, long endByte)
		{
			Brush brush = new SolidBrush(this.GetDefaultForeColor());
			int maxLine = this.GetGridBytePoint(endByte-startByte).Y+1;

			for(int i = 0; i < maxLine; i++)
			{
				long lastLineByte = startByte + (this._iHexMaxHBytes)*i + this._iHexMaxHBytes;

				PointF bytePointF = this.GetBytePointF(new Point(0, 0+i));
				string info = lastLineByte.ToString(this._hexStringFormat, System.Threading.Thread.CurrentThread.CurrentCulture);
				int nulls = 8-info.Length;
				string formattedInfo;
				if(nulls > -1)
				{
					formattedInfo = new string('0', 8-info.Length) + info;
				}
				else
				{
					formattedInfo = new string('~', 8);
				}
			
				g.DrawString(formattedInfo, this.Font, brush, new PointF(this._recLineInfo.X, bytePointF.Y), this._stringFormat);
			}
		}

		void PaintHex(Graphics g, long startByte, long endByte)
		{
			Brush brush = new SolidBrush(this.GetDefaultForeColor());
			Brush selBrush = new SolidBrush(this._selectionForeColor);
			Brush selBrushBack = new SolidBrush(this._selectionBackColor);

			int counter = -1;
			long intern_endByte = Math.Min(this._byteProvider.Length-1, endByte+this._iHexMaxHBytes);

			bool isKeyInterpreterActive = this._keyInterpreter == null || this._keyInterpreter.GetType() == typeof(KeyInterpreter);

			for(long i = startByte; i < intern_endByte+1; i++)
			{
				counter++;
				Point gridPoint = this.GetGridBytePoint(counter);
				byte b = this._byteProvider.ReadByte(i);

				bool isSelectedByte = i >= this._bytePos && i <= (this._bytePos + this._selectionLength-1) && this._selectionLength != 0;

				if(isSelectedByte && isKeyInterpreterActive)
				{
					this.PaintHexStringSelected(g, b, selBrush, selBrushBack, gridPoint);
				}
				else
				{
					this.PaintHexString(g, b, brush, gridPoint);
				}
			}
		}

		void PaintHexString(Graphics g, byte b, Brush brush, Point gridPoint)
		{
			PointF bytePointF = this.GetBytePointF(gridPoint);

			string sB = b.ToString(this._hexStringFormat, System.Threading.Thread.CurrentThread.CurrentCulture);
			if(sB.Length == 1)
				sB = "0" + sB;

			g.DrawString(sB.Substring(0,1), this.Font, brush, bytePointF, this._stringFormat);
			bytePointF.X += this._charSize.Width;
			g.DrawString(sB.Substring(1,1), this.Font, brush, bytePointF, this._stringFormat);
		}

		void PaintHexStringSelected(Graphics g, byte b, Brush brush, Brush brushBack, Point gridPoint)
		{
			string sB = b.ToString(this._hexStringFormat, System.Threading.Thread.CurrentThread.CurrentCulture);
			if(sB.Length == 1)
				sB = "0" + sB;

			PointF bytePointF = this.GetBytePointF(gridPoint);

			bool isLastLineChar = (gridPoint.X+1 == this._iHexMaxHBytes);
			float bcWidth = (isLastLineChar) ? this._charSize.Width*2 : this._charSize.Width*3;

			g.FillRectangle(brushBack, bytePointF.X, bytePointF.Y, bcWidth, this._charSize.Height);
			g.DrawString(sB.Substring(0,1), this.Font, brush, bytePointF, this._stringFormat);
			bytePointF.X += this._charSize.Width;
			g.DrawString(sB.Substring(1,1), this.Font, brush, bytePointF, this._stringFormat);
		}

		void PaintHexAndStringView(Graphics g, long startByte, long endByte)
		{
			Brush brush = new SolidBrush(this.GetDefaultForeColor());
			Brush selBrush = new SolidBrush(this._selectionForeColor);
			Brush selBrushBack = new SolidBrush(this._selectionBackColor);

			int counter = -1;
			long intern_endByte = Math.Min(this._byteProvider.Length-1, endByte+this._iHexMaxHBytes);

			bool isKeyInterpreterActive = this._keyInterpreter == null || this._keyInterpreter.GetType() == typeof(KeyInterpreter);
			bool isStringKeyInterpreterActive = this._keyInterpreter != null && this._keyInterpreter.GetType() == typeof(StringKeyInterpreter);

			for(long i = startByte; i < intern_endByte+1; i++)
			{
				counter++;
				Point gridPoint = this.GetGridBytePoint(counter);
				PointF byteStringPointF = this.GetByteStringPointF(gridPoint);
				byte b = this._byteProvider.ReadByte(i);

				bool isSelectedByte = i >= this._bytePos && i <= (this._bytePos + this._selectionLength-1) && this._selectionLength != 0;

				if(isSelectedByte && isKeyInterpreterActive)
				{
					this.PaintHexStringSelected(g, b, selBrush, selBrushBack, gridPoint);
				}
				else
				{
					this.PaintHexString(g, b, brush, gridPoint);
				}

				string s;
				if(b > 0x1F && !(b > 0x7E && b < 0xA0) )
				{
					s = ((char)b).ToString();
				}
				else
				{
					s = ".";
				}

				if(isSelectedByte && isStringKeyInterpreterActive)
				{
					g.FillRectangle(selBrushBack, byteStringPointF.X, byteStringPointF.Y, this._charSize.Width, this._charSize.Height);
					g.DrawString(s, this.Font, selBrush, byteStringPointF, this._stringFormat);
				}
				else
				{
					g.DrawString(s, this.Font, brush, byteStringPointF, this._stringFormat);
				}
			}
		}

		void PaintCurrentBytesSign(Graphics g)
		{
			if(this._keyInterpreter != null && Focused && this._bytePos != -1 && Enabled)
			{
				if(this._keyInterpreter.GetType() == typeof(KeyInterpreter))
				{
					if(this._selectionLength == 0)
					{
						Point gp = this.GetGridBytePoint(this._bytePos - this._startByte);
						PointF pf = this.GetByteStringPointF(gp);
						Size s = new Size((int)this._charSize.Width, (int)this._charSize.Height);
						Rectangle r = new Rectangle((int)pf.X, (int)pf.Y, s.Width, s.Height);
						if(r.IntersectsWith(this._recStringView))
						{
							r.Intersect(this._recStringView);
							this.PaintCurrentByteSign(g, r);
						}
					}
					else
					{
						int lineWidth = (int)(this._recStringView.Width-this._charSize.Width);

						Point startSelGridPoint = this.GetGridBytePoint(this._bytePos-this._startByte);
						PointF startSelPointF = this.GetByteStringPointF(startSelGridPoint);

						Point endSelGridPoint = this.GetGridBytePoint(this._bytePos-this._startByte+this._selectionLength-1);
						PointF endSelPointF = this.GetByteStringPointF(endSelGridPoint);

						int multiLine = endSelGridPoint.Y - startSelGridPoint.Y;
						if(multiLine == 0)
						{
							Rectangle singleLine = new Rectangle(
								(int)startSelPointF.X,
								(int)startSelPointF.Y,
								(int)(endSelPointF.X-startSelPointF.X+this._charSize.Width),
								(int)this._charSize.Height);
							if(singleLine.IntersectsWith(this._recStringView))
							{
								singleLine.Intersect(this._recStringView);
								this.PaintCurrentByteSign(g, singleLine);
							}
						}
						else
						{
							Rectangle firstLine = new Rectangle(
								(int)startSelPointF.X,
								(int)startSelPointF.Y,
								(int)(this._recStringView.X+lineWidth-startSelPointF.X+this._charSize.Width),
								(int)this._charSize.Height);
							if(firstLine.IntersectsWith(this._recStringView))
							{
								firstLine.Intersect(this._recStringView);
								this.PaintCurrentByteSign(g, firstLine);
							}

							if(multiLine > 1)
							{
								Rectangle betweenLines = new Rectangle(
									this._recStringView.X,
									(int)(startSelPointF.Y+this._charSize.Height),
									(int)(this._recStringView.Width),
									(int)(this._charSize.Height*(multiLine-1)));
								if(betweenLines.IntersectsWith(this._recStringView))
								{
									betweenLines.Intersect(this._recStringView);
									this.PaintCurrentByteSign(g, betweenLines);
								}

							}

							Rectangle lastLine = new Rectangle(
								this._recStringView.X,
								(int)endSelPointF.Y,
								(int)(endSelPointF.X-this._recStringView.X+this._charSize.Width),
								(int)this._charSize.Height);
							if(lastLine.IntersectsWith(this._recStringView))
							{
								lastLine.Intersect(this._recStringView);
								this.PaintCurrentByteSign(g, lastLine);
							}
						}
					}
				}
				else
				{
					if(this._selectionLength == 0)
					{
						Point gp = this.GetGridBytePoint(this._bytePos - this._startByte);
						PointF pf = this.GetBytePointF(gp);
						Size s = new Size((int)this._charSize.Width * 2, (int)this._charSize.Height);
						Rectangle r = new Rectangle((int)pf.X, (int)pf.Y, s.Width, s.Height);
						this.PaintCurrentByteSign(g, r);
					}
					else
					{
						int lineWidth = (int)(this._recHex.Width-this._charSize.Width*5);

						Point startSelGridPoint = this.GetGridBytePoint(this._bytePos-this._startByte);
						PointF startSelPointF = this.GetBytePointF(startSelGridPoint);

						Point endSelGridPoint = this.GetGridBytePoint(this._bytePos-this._startByte+this._selectionLength-1);
						PointF endSelPointF = this.GetBytePointF(endSelGridPoint);

						int multiLine = endSelGridPoint.Y - startSelGridPoint.Y;
						if(multiLine == 0)
						{
							Rectangle singleLine = new Rectangle(
								(int)startSelPointF.X,
								(int)startSelPointF.Y,
								(int)(endSelPointF.X-startSelPointF.X+this._charSize.Width*2),
								(int)this._charSize.Height);
							if(singleLine.IntersectsWith(this._recHex))
							{
								singleLine.Intersect(this._recHex);
								this.PaintCurrentByteSign(g, singleLine);
							}
						}
						else
						{
							Rectangle firstLine = new Rectangle(
								(int)startSelPointF.X,
								(int)startSelPointF.Y,
								(int)(this._recHex.X+lineWidth-startSelPointF.X+this._charSize.Width*2),
								(int)this._charSize.Height);
							if(firstLine.IntersectsWith(this._recHex))
							{
								firstLine.Intersect(this._recHex);
								this.PaintCurrentByteSign(g, firstLine);
							}

							if(multiLine > 1)
							{
								Rectangle betweenLines = new Rectangle(
									this._recHex.X,
									(int)(startSelPointF.Y+this._charSize.Height),
									(int)(lineWidth+this._charSize.Width*2),
									(int)(this._charSize.Height*(multiLine-1)));
								if(betweenLines.IntersectsWith(this._recHex))
								{
									betweenLines.Intersect(this._recHex);
									this.PaintCurrentByteSign(g, betweenLines);
								}

							}

							Rectangle lastLine = new Rectangle(
								this._recHex.X,
								(int)endSelPointF.Y,
								(int)(endSelPointF.X-this._recHex.X+this._charSize.Width*2),
								(int)this._charSize.Height);
							if(lastLine.IntersectsWith(this._recHex))
							{
								lastLine.Intersect(this._recHex);
								this.PaintCurrentByteSign(g, lastLine);
							}
						}
					}
				}
			}
		}

		void PaintCurrentByteSign(Graphics g, Rectangle rec)
		{
			Bitmap myBitmap = new Bitmap(rec.Width, rec.Height);
			Graphics bitmapGraphics = Graphics.FromImage(myBitmap);

			SolidBrush greenBrush = new SolidBrush(this._shadowSelectionColor);

			bitmapGraphics.FillRectangle(greenBrush, 0, 
				0, rec.Width, rec.Height);

			g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.GammaCorrected;

			g.DrawImage(myBitmap, rec.Left, rec.Top);
		}

		Color GetDefaultForeColor()
		{
			if(Enabled)
				return ForeColor;
			else
				return Color.Gray;
		}
		void UpdateVisibilityBytes()
		{
			if(this._byteProvider == null || this._byteProvider.Length == 0)
				return;

			this._startByte = (this._scrollVpos+1) * this._iHexMaxHBytes - this._iHexMaxHBytes;
			this._endByte = (long)Math.Min(this._byteProvider.Length - 1, this._startByte + this._iHexMaxBytes);
		}
		#endregion

		#region Positioning methods
		void UpdateRectanglePositioning()
		{
			// calc char size
			SizeF charSize = this.CreateGraphics().MeasureString("A", this.Font, 100, this._stringFormat);
			this._charSize = new SizeF((float)Math.Ceiling(charSize.Width), (float)Math.Ceiling(charSize.Height));

			// calc content bounds
			this._recContent = ClientRectangle;
			this._recContent.X += this._recBorderLeft;
			this._recContent.Y += this._recBorderTop;
			this._recContent.Width -= this._recBorderRight+this._recBorderLeft;
			this._recContent.Height -= this._recBorderBottom+this._recBorderTop;

			if(this._vScrollBarVisible)
			{
				this._recContent.Width -= this._vScrollBar.Width;
				this._vScrollBar.Left = this._recContent.X+this._recContent.Width;
				this._vScrollBar.Top = this._recContent.Y;
				this._vScrollBar.Height = this._recContent.Height;
			}

			int marginLeft = 4;

			// calc line info bounds
			if(this._lineInfoVisible)
			{
				this._recLineInfo = new Rectangle(this._recContent.X+marginLeft, 
					this._recContent.Y, 
					(int)(this._charSize.Width*10), 
					this._recContent.Height);
			}
			else
			{
				this._recLineInfo = Rectangle.Empty;
				this._recLineInfo.X = marginLeft;
			}

			// calc hex bounds and grid
			this._recHex = new Rectangle(this._recLineInfo.X + this._recLineInfo.Width,
				this._recLineInfo.Y,
				this._recContent.Width - this._recLineInfo.Width,
				this._recContent.Height);

			if(this.UseFixedBytesPerLine)
			{
				this.SetHorizontalByteCount(this._bytesPerLine);
				this._recHex.Width = (int)Math.Floor(((double)this._iHexMaxHBytes)*this._charSize.Width*3+(2*this._charSize.Width));
			}
			else
			{
				int hmax  = (int)Math.Floor((double)this._recHex.Width/(double)this._charSize.Width);
				if(hmax > 1)
					this.SetHorizontalByteCount((int)Math.Floor((double)hmax/3));
				else
					this.SetHorizontalByteCount(hmax);
			}

			if(this._stringViewVisible)
			{
				this._recStringView = new Rectangle(this._recHex.X + this._recHex.Width,
					this._recHex.Y,
					(int)(this._charSize.Width*this._iHexMaxHBytes), 
					this._recHex.Height);
			}
			else
			{
				this._recStringView = Rectangle.Empty;
			}

			int vmax = (int)Math.Floor((double)this._recHex.Height/(double)this._charSize.Height);
			this.SetVerticalByteCount(vmax);

			this._iHexMaxBytes = this._iHexMaxHBytes * this._iHexMaxVBytes;

			this.UpdateScrollSize();
		}

		PointF GetBytePointF(long byteIndex)
		{
			Point gp = this.GetGridBytePoint(byteIndex);

			return this.GetBytePointF(gp);
		}
		
		PointF GetBytePointF(Point gp)
		{
			float x = (3 * this._charSize.Width) * gp.X + this._recHex.X;
			float y = (gp.Y+1)*this._charSize.Height-this._charSize.Height+this._recHex.Y;

			return new PointF(x,y);
		}

		PointF GetByteStringPointF(Point gp)
		{
			float x = (this._charSize.Width) * gp.X + this._recStringView.X;
			float y = (gp.Y+1)*this._charSize.Height-this._charSize.Height+this._recStringView.Y;

			return new PointF(x,y);
		}

		Point GetGridBytePoint(long byteIndex)
		{
			int row = (int)Math.Floor((double)byteIndex/(double)this._iHexMaxHBytes);
			int column = (int)(byteIndex+this._iHexMaxHBytes-this._iHexMaxHBytes*(row+1));

			Point res = new Point(column, row);
			return res;
		}
		#endregion

		#region Overridden properties
		/// <summary>
		/// Gets or sets the background color for the control.
		/// </summary>
		[DefaultValue(typeof(Color), "White")]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// The font used to display text in the hexbox.
		/// </summary>
		//[Editor(typeof(HexFontEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
			}
		}


		/// <summary>
		/// Gets the required creation parameters when the control handle is created.
		/// </summary>
		protected override CreateParams CreateParams
		{
			[SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode=true), SecurityPermission(SecurityAction.InheritanceDemand, UnmanagedCode=true)]
			get
			{
				CreateParams p = base.CreateParams;

//				if(VScrollBarVisible)
//				{
//					p.Style |= NativeMethods.WS_VSCROLL;
//				}

//				switch (_borderStyle)
//				{
//					case BorderStyle.FixedSingle:
//					{
//						p.Style = (p.Style | NativeMethods.WS_BORDER); //0x800000
//						return p;
//					}
//					case BorderStyle.Fixed3D:
//					{
//						p.ExStyle = (p.ExStyle | NativeMethods.WS_EX_CLIENTEDGE);
//						return p;
//					}
//				}
				return p;
			}
		}

		/// <summary>
		/// Not used.
		/// </summary>
		[DefaultValue(""), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Bindable(false)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}

		/// <summary>
		/// Not used.
		/// </summary>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Bindable(false)]
		public override RightToLeft RightToLeft
		{
			get
			{
				return base.RightToLeft;
			}
			set
			{
				base.RightToLeft = value;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets if the count of bytes in one line is fix.
		/// </summary>
		/// <remarks>
		/// When set to True, BytesPerLine property determine the maximum count of bytes in one line.
		/// </remarks>
		[DefaultValue(false), Category("Hex"), Description("Gets or sets if the count of bytes in one line is fix.")]
		public bool ReadOnly
		{
			get { return this._readOnly; }
			set 
			{ 
				if(this._readOnly == value)
					return;

				this._readOnly = value; 
				this.OnReadOnlyChanged(EventArgs.Empty);
				Invalidate();
			}
		} bool _readOnly;

		/// <summary>
		/// Gets or sets the maximum count of bytes in one line.
		/// </summary>
		/// <remarks>
		/// UsedFixedBytesPerLine property must set to true
		/// </remarks>
		[DefaultValue(16), Category("Hex"), Description("Gets or sets the maximum count of bytes in one line.")]
		public int BytesPerLine
		{
			get { return this._bytesPerLine; }
			set 
			{ 
				if(this._bytesPerLine == value)
					return;

				this._bytesPerLine = value; 
				this.OnByteProviderChanged(EventArgs.Empty);

				this.UpdateRectanglePositioning();
				Invalidate();
			}
		} int _bytesPerLine = 16;

		/// <summary>
		/// Gets or sets if the count of bytes in one line is fix.
		/// </summary>
		/// <remarks>
		/// When set to True, BytesPerLine property determine the maximum count of bytes in one line.
		/// </remarks>
		[DefaultValue(false), Category("Hex"), Description("Gets or sets if the count of bytes in one line is fix.")]
		public bool UseFixedBytesPerLine
		{
			get { return this._useFixedBytesPerLine; }
			set 
			{ 
				if(this._useFixedBytesPerLine == value)
					return;

				this._useFixedBytesPerLine = value; 
				this.OnUseFixedBytesPerLineChanged(EventArgs.Empty);

				this.UpdateRectanglePositioning();
				Invalidate();
			}
		} bool _useFixedBytesPerLine;

		/// <summary>
		/// Gets or sets the visibility of a vertical scroll bar.
		/// </summary>
		[DefaultValue(false), Category("Hex"), Description("Gets or sets the visibility of a vertical scroll bar.")]
		public bool VScrollBarVisible
		{
			get { return this._vScrollBarVisible; }
			set 
			{ 
				if(this._vScrollBarVisible == value)
					return;

				this._vScrollBarVisible = value; 
				
				if(this._vScrollBarVisible)
					Controls.Add(this._vScrollBar);
				else
					Controls.Remove(this._vScrollBar);

				this.UpdateRectanglePositioning();
				this.UpdateScrollSize();

				this.OnVScrollBarVisibleChanged(EventArgs.Empty);
			}
		} bool _vScrollBarVisible;

		/// <summary>
		/// Gets or sets the ByteProvider.
		/// </summary>
		[Browsable(false), DefaultValue(null)]
		public IByteProvider ByteProvider
		{
			get { return this._byteProvider; }
			set 
			{ 
				if(this._byteProvider == value)
					return;

				if(value == null)
					this.ActivateEmptyKeyInterpreter();
				else
					this.ActivateKeyInterpreter();

				if(this._byteProvider != null)
					this._byteProvider.LengthChanged -= new EventHandler(this._byteProvider_LengthChanged);

				this._byteProvider = value; 
				if(this._byteProvider != null)
					this._byteProvider.LengthChanged += new EventHandler(this._byteProvider_LengthChanged);

				this.OnByteProviderChanged(EventArgs.Empty);

				if(value == null) // do not raise events if value is null
				{
					this._bytePos = -1;
					this._byteCharacterPos = 0;
					this._selectionLength = 0;

					this.DestroyCaret();
				}
				else
				{
					this.SetPosition(0, 0);
					this.SetSelectionLength(0);

					if(this._caretVisible && Focused)
						this.UpdateCaret();
					else
						this.CreateCaret();
				}

				this.CheckCurrentLineChanged();
				this.CheckCurrentPositionInLineChanged();

				this._scrollVpos = 0;

				this.UpdateVisibilityBytes();
				this.UpdateRectanglePositioning();
				

				Invalidate();
			}
		} IByteProvider _byteProvider;

		/// <summary>
		/// Gets or sets the visibility of a line info.
		/// </summary>
		[DefaultValue(false), Category("Hex"), Description("Gets or sets the visibility of a line info.")]
		public bool LineInfoVisible
		{
			get { return this._lineInfoVisible; }
			set 
			{ 
				if(this._lineInfoVisible == value)
					return;

				this._lineInfoVisible = value;
				this.OnLineInfoVisibleChanged(EventArgs.Empty);

				this.UpdateRectanglePositioning();
				Invalidate();
			}
		} bool _lineInfoVisible;

		/// <summary>
		/// Gets or sets the hex box�s border style.
		/// </summary>
		[DefaultValue(typeof(BorderStyle), "Fixed3D"), Category("Hex"), Description("Gets or sets the hex box�s border style.")]
		public BorderStyle BorderStyle
		{
			get { return this._borderStyle;}
			set 
			{ 
				if(this._borderStyle == value)
					return;

				this._borderStyle = value;
				switch(this._borderStyle)
				{
					case BorderStyle.None:
						this._recBorderLeft = this._recBorderTop = this._recBorderRight = this._recBorderBottom = 0;
						break;
					case BorderStyle.Fixed3D:
						this._recBorderLeft = this._recBorderRight = SystemInformation.Border3DSize.Width;
						this._recBorderTop = this._recBorderBottom = SystemInformation.Border3DSize.Height;
						break;
					case BorderStyle.FixedSingle:
						this._recBorderLeft = this._recBorderTop = this._recBorderRight = this._recBorderBottom = 1;
						break;
				}

				this.UpdateRectanglePositioning();

				this.OnBorderStyleChanged(EventArgs.Empty);

			}
		} BorderStyle _borderStyle = BorderStyle.Fixed3D;

		/// <summary>
		/// Gets or sets the visibility of the string view.
		/// </summary>
		[DefaultValue(false), Category("Hex"), Description("Gets or sets the visibility of the string view.")]
		public bool StringViewVisible
		{
			get { return this._stringViewVisible; }
			set 
			{ 
				if(this._stringViewVisible == value)
					return;

				this._stringViewVisible = value; 
				this.OnStringViewVisibleChanged(EventArgs.Empty);

				this.UpdateRectanglePositioning();
				Invalidate();
			}
		} bool _stringViewVisible;

		/// <summary>
		/// Gets or sets whether the HexBox control displays the hex characters in upper or lower case.
		/// </summary>
		[DefaultValue(typeof(HexCasing), "Upper"), Category("Hex"), Description("Gets or sets whether the HexBox control displays the hex characters in upper or lower case.")]
		public HexCasing HexCasing
		{
			get 
			{ 
				if(this._hexStringFormat == "X")
					return HexCasing.Upper;
				else
					return HexCasing.Lower;
			}
			set 
			{ 
				string format;
				if(value == HexCasing.Upper)
					format = "X";
				else
					format = "x";

				if(this._hexStringFormat == format)
					return;

				this._hexStringFormat = format;
				this.OnHexCasingChanged(EventArgs.Empty);

				Invalidate();
			}
		}

		/// <summary>
		/// Gets and sets the starting point of the bytes selected in the hex box.
		/// </summary>
		[Browsable(false), DefaultValue(0)]
		public long SelectionStart
		{
			get { return this._bytePos; }
			set 
			{ 
				this.SetPosition(value, 0); 
				this.ScrollByteIntoView();
				Invalidate();
			}
		}

		/// <summary>
		/// Gets and sets the number of bytes selected in the hex box.
		/// </summary>
		[DefaultValue(0)]
		public long SelectionLength
		{
			get { return this._selectionLength; }
			set 
			{ 
				this.SetSelectionLength(value); 
				this.ScrollByteIntoView();
				Invalidate();
			}
		} long _selectionLength;


		/// <summary>
		/// Gets or sets the background color for the selected bytes.
		/// </summary>
		[DefaultValue(typeof(Color), "Blue"), Category("Hex"), Description("Gets or sets the background color for the selected bytes.")]
		public Color SelectionBackColor
		{
			get { return this._selectionBackColor; }
			set { this._selectionBackColor = value; Invalidate(); }
		} Color _selectionBackColor = Color.Blue;

		/// <summary>
		/// Gets or sets the foreground color for the selected bytes.
		/// </summary>
		[DefaultValue(typeof(Color), "White"), Category("Hex"), Description("Gets or sets the foreground color for the selected bytes.")]
		public Color SelectionForeColor
		{
			get { return this._selectionForeColor; }
			set { this._selectionForeColor = value; Invalidate(); }
		} Color _selectionForeColor = Color.White;

		/// <summary>
		/// Gets or sets the visibility of a shadow selection.
		/// </summary>
		[DefaultValue(true), Category("Hex"), Description("Gets or sets the visibility of a shadow selection.")]
		public bool ShadowSelectionVisible
		{
			get { return this._shadowSelectionVisible; }
			set 
			{ 
				if(this._shadowSelectionVisible == value)
					return;
				this._shadowSelectionVisible = value; 
				Invalidate();
			}
		} bool _shadowSelectionVisible = true;

		/// <summary>
		/// Gets or sets the color of the shadow selection. 
		/// </summary>
		/// <remarks>
		/// A alpha component must be given! 
		/// Default alpha = 100
		/// </remarks>
		[Category("Hex"), Description("Gets or sets the color of the shadow selection.")]
		public Color ShadowSelectionColor
		{
			get { return this._shadowSelectionColor; }
			set { this._shadowSelectionColor = value; Invalidate(); }
		} Color _shadowSelectionColor = Color.FromArgb(100, 60, 188, 255);

		/// <summary>
		/// Gets the number bytes drawn horizontally.
		/// </summary>
		[DefaultValue(true), Browsable(false)]
		public int HorizontalByteCount
		{
			get { return this._iHexMaxHBytes; }
		}

		/// <summary>
		/// Gets the number bytes drawn vertically.
		/// </summary>
		[DefaultValue(true), Browsable(false)]
		public int VerticalByteCount
		{
			get { return this._iHexMaxVBytes; }
		}

		/// <summary>
		/// Gets the current line
		/// </summary>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public long CurrentLine
		{
			get  { return this._currentLine; }
		} long _currentLine;

		/// <summary>
		/// Gets the current position in the current line
		/// </summary>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public long CurrentPositionInLine
		{
			get { return this._currentPositionInLine; }
		} int _currentPositionInLine;

		/// <summary>
		/// Gets the a value if insertion mode is active or not.
		/// </summary>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool InsertActive
		{
			get { return this._insertActive; }
		}

		#endregion

		#region Misc
		void SetPosition(long bytePos)
		{
			this.SetPosition(bytePos, this._byteCharacterPos);
		}

		void SetPosition(long bytePos, int byteCharacterPos)
		{
			if(this._byteCharacterPos != byteCharacterPos)
			{
				this._byteCharacterPos = byteCharacterPos;
			}

			if(bytePos != this._bytePos)
			{
				this._bytePos = bytePos;
				this.CheckCurrentLineChanged();
				this.CheckCurrentPositionInLineChanged();

				this.OnSelectionStartChanged(EventArgs.Empty);
			}
		}

		void SetSelectionLength(long selectionLength)
		{
			if(selectionLength != this._selectionLength)
			{
				this._selectionLength = selectionLength;
				this.OnSelectionLengthChanged(EventArgs.Empty);
			}
		}

		void SetHorizontalByteCount(int value)
		{
			if(this._iHexMaxHBytes == value)
				return;
			
			this._iHexMaxHBytes = value;
			this.OnHorizontalByteCountChanged(EventArgs.Empty);
		}

		void SetVerticalByteCount(int value)
		{
			if(this._iHexMaxVBytes == value)
				return;
			
			this._iHexMaxVBytes = value;
			this.OnVerticalByteCountChanged(EventArgs.Empty);
		}

		void CheckCurrentLineChanged()
		{
			long currentLine = (long)Math.Floor((double)this._bytePos / (double)this._iHexMaxHBytes) + 1;

			if(this._byteProvider == null && this._currentLine != 0)
			{
				this._currentLine = 0;
				this.OnCurrentLineChanged(EventArgs.Empty);
			}
			else if(currentLine != this._currentLine)
			{
				this._currentLine = currentLine;
				this.OnCurrentLineChanged(EventArgs.Empty);
			}
		}

		void CheckCurrentPositionInLineChanged()
		{
			Point gb = this.GetGridBytePoint(this._bytePos);
			int currentPositionInLine = gb.X + 1;

			if(this._byteProvider == null && this._currentPositionInLine != 0)
			{
				this._currentPositionInLine = 0;
				this.OnCurrentPositionInLineChanged(EventArgs.Empty);
			}
			else if(currentPositionInLine != this._currentPositionInLine)
			{
				this._currentPositionInLine = currentPositionInLine;
				this.OnCurrentPositionInLineChanged(EventArgs.Empty);
			}
		}

		
		bool VisualStylesEnabled()
		{
			OperatingSystem os = Environment.OSVersion;
			bool isAppropriateOS = os.Platform == PlatformID.Win32NT && ((os.Version.Major == 5 && os.Version.Minor >= 1) || os.Version.Major > 5);
			bool osFeatureThemesPresent = false;
			bool osThemeDLLAvailable = false;

			if (isAppropriateOS)
			{
				Version osThemeVersion = OSFeature.Feature.GetVersionPresent(OSFeature.Themes);
				osFeatureThemesPresent = osThemeVersion != null;

				NativeMethods.DLLVersionInfo dllVersion = new NativeMethods.DLLVersionInfo();
				dllVersion.cbSize = Marshal.SizeOf(typeof(NativeMethods.DLLVersionInfo));
				int temp = NativeMethods.DllGetVersion(ref dllVersion);
				osThemeDLLAvailable = dllVersion.dwMajorVersion >= 6;
			}

			return isAppropriateOS && osFeatureThemesPresent && osThemeDLLAvailable && NativeMethods.IsAppThemed() && NativeMethods.IsThemeActive();
		}

		/// <summary>
		/// Raises the ReadOnlyChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnReadOnlyChanged(EventArgs e)
		{
			if(this.ReadOnlyChanged != null)
				this.ReadOnlyChanged(this, e);
		}

		/// <summary>
		/// Raises the ByteProviderChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnByteProviderChanged(EventArgs e)
		{
			if(this.ByteProviderChanged != null)
				this.ByteProviderChanged(this, e);
		}

		/// <summary>
		/// Raises the SelectionStartChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnSelectionStartChanged(EventArgs e)
		{
			if(this.SelectionStartChanged != null)
				this.SelectionStartChanged(this, e);
		}

		/// <summary>
		/// Raises the SelectionLengthChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnSelectionLengthChanged(EventArgs e)
		{
			if(this.SelectionLengthChanged != null)
				this.SelectionLengthChanged(this, e);
		}

		/// <summary>
		/// Raises the LineInfoVisibleChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnLineInfoVisibleChanged(EventArgs e)
		{
			if(this.LineInfoVisibleChanged != null)
				this.LineInfoVisibleChanged(this, e);
		}

		/// <summary>
		/// Raises the StringViewVisibleChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnStringViewVisibleChanged(EventArgs e)
		{
			if(this.StringViewVisibleChanged != null)
				this.StringViewVisibleChanged(this, e);
		}

		/// <summary>
		/// Raises the BorderStyleChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnBorderStyleChanged(EventArgs e)
		{
			if(this.BorderStyleChanged != null)
				this.BorderStyleChanged(this, e);
		}

		/// <summary>
		/// Raises the UseFixedBytesPerLineChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnUseFixedBytesPerLineChanged(EventArgs e)
		{
			if(this.UseFixedBytesPerLineChanged != null)
				this.UseFixedBytesPerLineChanged(this, e);
		}

		/// <summary>
		/// Raises the BytesPerLineChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnBytesPerLineChanged(EventArgs e)
		{
			if(this.BytesPerLineChanged != null)
				this.BytesPerLineChanged(this, e);
		}

		/// <summary>
		/// Raises the VScrollBarVisibleChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnVScrollBarVisibleChanged(EventArgs e)
		{
			if(this.VScrollBarVisibleChanged != null)
				this.VScrollBarVisibleChanged(this, e);
		}

		/// <summary>
		/// Raises the HexCasingChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnHexCasingChanged(EventArgs e)
		{
			if(this.HexCasingChanged != null)
				this.HexCasingChanged(this, e);
		}

		/// <summary>
		/// Raises the HorizontalByteCountChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnHorizontalByteCountChanged(EventArgs e)
		{
			if(this.HorizontalByteCountChanged != null)
				this.HorizontalByteCountChanged(this, e);
		}

		/// <summary>
		/// Raises the VerticalByteCountChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnVerticalByteCountChanged(EventArgs e)
		{
			if(this.VerticalByteCountChanged != null)
				this.VerticalByteCountChanged(this, e);
		}

		/// <summary>
		/// Raises the CurrentLineChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnCurrentLineChanged(EventArgs e)
		{
			if(this.CurrentLineChanged != null)
				this.CurrentLineChanged(this, e);
		}

		/// <summary>
		/// Raises the CurrentPositionInLineChanged event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected virtual void OnCurrentPositionInLineChanged(EventArgs e)
		{
			if(this.CurrentPositionInLineChanged != null)
				this.CurrentPositionInLineChanged(this, e);
		}

		/// <summary>
		/// Raises the MouseDown event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected override void OnMouseDown(MouseEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("OnMouseDown()", "HexBox");

			if(!Focused)
				Focus();

			this.SetCaretPosition(new Point(e.X, e.Y));

			base.OnMouseDown (e);
		}

		/// <summary>
		/// Raises the MouseWhell event
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected override void OnMouseWheel(MouseEventArgs e)
		{
			int linesToScroll = -(e.Delta * SystemInformation.MouseWheelScrollLines / 120);
			this.PerformScrollLines(linesToScroll);

			base.OnMouseWheel (e);
		}


		/// <summary>
		/// Raises the Resize event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected override void OnResize(EventArgs e)
		{
			base.OnResize (e);
			this.UpdateRectanglePositioning();
		}
		
		/// <summary>
		/// Raises the GotFocus event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected override void OnGotFocus(EventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("OnGotFocus()", "HexBox");

			base.OnGotFocus (e);

			this.CreateCaret();
		}

		/// <summary>
		/// Raises the LostFocus event.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>
		protected override void OnLostFocus(EventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("OnLostFocus()", "HexBox");

			base.OnLostFocus (e);

			this.DestroyCaret();
		}

		void _byteProvider_LengthChanged(object sender, EventArgs e)
		{
			this.UpdateScrollSize();
		}
		#endregion

	}
}
