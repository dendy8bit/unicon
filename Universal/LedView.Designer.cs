using BEMN.Forms;

namespace BEMN.MBView
{
    partial class LedView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this._modbusGroup = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._bitfRadio = new System.Windows.Forms.RadioButton();
            this._wordfRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this._addrGroup = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._sizeChangeButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._ledsGroup = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this._hexGroup = new System.Windows.Forms.GroupBox();
            this._decGroup = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this._copyBut = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this._adductionGroup = new System.Windows.Forms.GroupBox();
            this._contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this._symbolsCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._limitBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._wordIndexCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._exchangesCheck = new System.Windows.Forms.CheckBox();
            this._tittleCheck = new System.Windows.Forms.CheckBox();
            this._tittleBox = new System.Windows.Forms.TextBox();
            this._wordsCount = new BEMN.Forms.HexTextbox();
            this._maxHex = new BEMN.Forms.HexTextbox();
            this._paramHex = new BEMN.Forms.HexTextbox();
            this._addrHex = new BEMN.Forms.HexTextbox();
            this._deviceHex = new BEMN.Forms.HexTextbox();
            this._modbusGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "����������";
            // 
            // _modbusGroup
            // 
            this._modbusGroup.Controls.Add(this._paramHex);
            this._modbusGroup.Controls.Add(this.label8);
            this._modbusGroup.Controls.Add(this._addrHex);
            this._modbusGroup.Controls.Add(this.label3);
            this._modbusGroup.Controls.Add(this.label2);
            this._modbusGroup.Controls.Add(this._bitfRadio);
            this._modbusGroup.Controls.Add(this._wordfRadio);
            this._modbusGroup.Controls.Add(this._deviceHex);
            this._modbusGroup.Controls.Add(this.label1);
            this._modbusGroup.Location = new System.Drawing.Point(516, 27);
            this._modbusGroup.Name = "_modbusGroup";
            this._modbusGroup.Size = new System.Drawing.Size(150, 153);
            this._modbusGroup.TabIndex = 2;
            this._modbusGroup.TabStop = false;
            this._modbusGroup.Text = "��������� Modbus (hex)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "����� ����";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "�����";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "�������";
            // 
            // _bitfRadio
            // 
            this._bitfRadio.AutoSize = true;
            this._bitfRadio.Location = new System.Drawing.Point(78, 130);
            this._bitfRadio.Name = "_bitfRadio";
            this._bitfRadio.Size = new System.Drawing.Size(63, 17);
            this._bitfRadio.TabIndex = 4;
            this._bitfRadio.Text = "������";
            this._bitfRadio.UseVisualStyleBackColor = true;
            this._bitfRadio.CheckedChanged += new System.EventHandler(this._bitfRadio_CheckedChanged);
            // 
            // _wordfRadio
            // 
            this._wordfRadio.AutoSize = true;
            this._wordfRadio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this._wordfRadio.Checked = true;
            this._wordfRadio.Location = new System.Drawing.Point(78, 114);
            this._wordfRadio.Name = "_wordfRadio";
            this._wordfRadio.Size = new System.Drawing.Size(70, 17);
            this._wordfRadio.TabIndex = 3;
            this._wordfRadio.TabStop = true;
            this._wordfRadio.Text = "�������";
            this._wordfRadio.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(6, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(84, 34);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "�����";
            // 
            // _addrGroup
            // 
            this._addrGroup.Location = new System.Drawing.Point(6, 60);
            this._addrGroup.Name = "_addrGroup";
            this._addrGroup.Size = new System.Drawing.Size(84, 304);
            this._addrGroup.TabIndex = 4;
            this._addrGroup.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._sizeChangeButton);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(96, 27);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(222, 34);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            // 
            // _sizeChangeButton
            // 
            this._sizeChangeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._sizeChangeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._sizeChangeButton.Location = new System.Drawing.Point(6, 8);
            this._sizeChangeButton.Name = "_sizeChangeButton";
            this._sizeChangeButton.Size = new System.Drawing.Size(82, 23);
            this._sizeChangeButton.TabIndex = 8;
            this._sizeChangeButton.Text = "����������";
            this._sizeChangeButton.UseVisualStyleBackColor = true;
            this._sizeChangeButton.Click += new System.EventHandler(this._sizeChangeButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(94, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "������";
            // 
            // _ledsGroup
            // 
            this._ledsGroup.Location = new System.Drawing.Point(96, 60);
            this._ledsGroup.Name = "_ledsGroup";
            this._ledsGroup.Size = new System.Drawing.Size(222, 304);
            this._ledsGroup.TabIndex = 5;
            this._ledsGroup.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(322, 27);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(58, 34);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "HEX";
            // 
            // _hexGroup
            // 
            this._hexGroup.Location = new System.Drawing.Point(323, 60);
            this._hexGroup.Name = "_hexGroup";
            this._hexGroup.Size = new System.Drawing.Size(57, 304);
            this._hexGroup.TabIndex = 10;
            this._hexGroup.TabStop = false;
            // 
            // _decGroup
            // 
            this._decGroup.Location = new System.Drawing.Point(385, 60);
            this._decGroup.Name = "_decGroup";
            this._decGroup.Size = new System.Drawing.Size(57, 304);
            this._decGroup.TabIndex = 12;
            this._decGroup.TabStop = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Location = new System.Drawing.Point(384, 27);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(58, 34);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "DEC";
            // 
            // _copyBut
            // 
            this._copyBut.Location = new System.Drawing.Point(519, 341);
            this._copyBut.Name = "_copyBut";
            this._copyBut.Size = new System.Drawing.Size(74, 23);
            this._copyBut.TabIndex = 0;
            this._copyBut.Text = "����� ����";
            this._copyBut.UseVisualStyleBackColor = true;
            this._copyBut.Click += new System.EventHandler(this._copyBut_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(448, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(63, 34);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F);
            this.label9.Location = new System.Drawing.Point(3, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "��������.";
            // 
            // _adductionGroup
            // 
            this._adductionGroup.Location = new System.Drawing.Point(448, 60);
            this._adductionGroup.Name = "_adductionGroup";
            this._adductionGroup.Size = new System.Drawing.Size(63, 304);
            this._adductionGroup.TabIndex = 13;
            this._adductionGroup.TabStop = false;
            // 
            // _contextMenu
            // 
            this._contextMenu.Name = "_contextMenu";
            this._contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this._maxHex);
            this.groupBox6.Controls.Add(this._symbolsCombo);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this._limitBox);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this._wordIndexCombo);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Location = new System.Drawing.Point(516, 185);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(150, 150);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���������� (dec)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "���������� ����";
            // 
            // _symbolsCombo
            // 
            this._symbolsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._symbolsCombo.FormattingEnabled = true;
            this._symbolsCombo.Location = new System.Drawing.Point(78, 119);
            this._symbolsCombo.Name = "_symbolsCombo";
            this._symbolsCombo.Size = new System.Drawing.Size(59, 21);
            this._symbolsCombo.TabIndex = 3;
            this._symbolsCombo.SelectedValueChanged += new System.EventHandler(this._symbolsCombo_SelectedValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(33, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "������";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 96);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "��������";
            // 
            // _limitBox
            // 
            this._limitBox.Location = new System.Drawing.Point(78, 67);
            this._limitBox.Name = "_limitBox";
            this._limitBox.Size = new System.Drawing.Size(59, 20);
            this._limitBox.TabIndex = 1;
            this._limitBox.Text = "100.0";
            this._limitBox.TextChanged += new System.EventHandler(this._limitBox_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "������";
            // 
            // _wordIndexCombo
            // 
            this._wordIndexCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._wordIndexCombo.FormattingEnabled = true;
            this._wordIndexCombo.Location = new System.Drawing.Point(78, 16);
            this._wordIndexCombo.Name = "_wordIndexCombo";
            this._wordIndexCombo.Size = new System.Drawing.Size(59, 21);
            this._wordIndexCombo.TabIndex = 0;
            this._wordIndexCombo.TextUpdate += new System.EventHandler(this._wordIndexCombo_TextUpdate);
            this._wordIndexCombo.SelectedValueChanged += new System.EventHandler(this._wordIndexCombo_SelectedValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "����� �����";
            // 
            // _exchangesCheck
            // 
            this._exchangesCheck.AutoSize = true;
            this._exchangesCheck.Checked = true;
            this._exchangesCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this._exchangesCheck.Location = new System.Drawing.Point(600, 345);
            this._exchangesCheck.Name = "_exchangesCheck";
            this._exchangesCheck.Size = new System.Drawing.Size(68, 17);
            this._exchangesCheck.TabIndex = 1;
            this._exchangesCheck.Text = "������";
            this._exchangesCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._exchangesCheck.UseVisualStyleBackColor = true;
            this._exchangesCheck.CheckedChanged += new System.EventHandler(this._exchangesCheck_CheckedChanged);
            // 
            // _tittleCheck
            // 
            this._tittleCheck.AutoSize = true;
            this._tittleCheck.Location = new System.Drawing.Point(454, 6);
            this._tittleCheck.Name = "_tittleCheck";
            this._tittleCheck.Size = new System.Drawing.Size(113, 17);
            this._tittleCheck.TabIndex = 16;
            this._tittleCheck.Text = " ����� ���������";
            this._tittleCheck.UseVisualStyleBackColor = true;
            this._tittleCheck.CheckedChanged += new System.EventHandler(this._tittleCheck_CheckedChanged);
            // 
            // _tittleBox
            // 
            this._tittleBox.Enabled = false;
            this._tittleBox.Location = new System.Drawing.Point(6, 5);
            this._tittleBox.Name = "_tittleBox";
            this._tittleBox.Size = new System.Drawing.Size(436, 20);
            this._tittleBox.TabIndex = 17;
            // 
            // _wordsCount
            // 
            this._wordsCount.HexBase = false;
            this._wordsCount.Limit = 16;
            this._wordsCount.Location = new System.Drawing.Point(618, 227);
            this._wordsCount.Name = "_wordsCount";
            this._wordsCount.Size = new System.Drawing.Size(35, 20);
            this._wordsCount.TabIndex = 18;
            this._wordsCount.Text = "1";
            this._wordsCount.TextChanged += new System.EventHandler(this._wordsCount_TextChanged);
            // 
            // _maxHex
            // 
            this._maxHex.HexBase = false;
            this._maxHex.Limit = 65535;
            this._maxHex.Location = new System.Drawing.Point(78, 93);
            this._maxHex.Name = "_maxHex";
            this._maxHex.Size = new System.Drawing.Size(59, 20);
            this._maxHex.TabIndex = 2;
            this._maxHex.Text = "65535";
            this._maxHex.TextChanged += new System.EventHandler(this._maxHex_TextChanged);
            // 
            // _paramHex
            // 
            this._paramHex.HexBase = true;
            this._paramHex.Limit = 16;
            this._paramHex.Location = new System.Drawing.Point(78, 86);
            this._paramHex.Name = "_paramHex";
            this._paramHex.Size = new System.Drawing.Size(59, 20);
            this._paramHex.TabIndex = 2;
            this._paramHex.Text = "10";
            // 
            // _addrHex
            // 
            this._addrHex.HexBase = true;
            this._addrHex.Limit = 65527;
            this._addrHex.Location = new System.Drawing.Point(78, 52);
            this._addrHex.Name = "_addrHex";
            this._addrHex.Size = new System.Drawing.Size(59, 20);
            this._addrHex.TabIndex = 1;
            this._addrHex.Text = "0";
            // 
            // _deviceHex
            // 
            this._deviceHex.HexBase = true;
            this._deviceHex.Limit = 247;
            this._deviceHex.Location = new System.Drawing.Point(78, 19);
            this._deviceHex.Name = "_deviceHex";
            this._deviceHex.Size = new System.Drawing.Size(59, 20);
            this._deviceHex.TabIndex = 0;
            this._deviceHex.Text = "1";
            // 
            // LedView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 367);
            this.Controls.Add(this._wordsCount);
            this.Controls.Add(this._tittleBox);
            this.Controls.Add(this._tittleCheck);
            this.Controls.Add(this._exchangesCheck);
            this.Controls.Add(this._adductionGroup);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this._decGroup);
            this.Controls.Add(this._copyBut);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this._hexGroup);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this._ledsGroup);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this._addrGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._modbusGroup);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(687, 179);
            this.Name = "LedView";
            this.Text = "1";
            this.Activated += new System.EventHandler(this.LedView_Activated);
            this.Deactivate += new System.EventHandler(this.LedView_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LedView_FormClosing);
            this.Load += new System.EventHandler(this.LedView_Load);
            this._modbusGroup.ResumeLayout(false);
            this._modbusGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HexTextbox _deviceHex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox _modbusGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton _bitfRadio;
        private System.Windows.Forms.RadioButton _wordfRadio;
        private HexTextbox _addrHex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox _addrGroup;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox _ledsGroup;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox _hexGroup;
        private System.Windows.Forms.GroupBox _decGroup;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label7;
        private HexTextbox _paramHex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button _copyBut;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox _adductionGroup;
        private System.Windows.Forms.ContextMenuStrip _contextMenu;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _wordIndexCombo;
        private System.Windows.Forms.ComboBox _symbolsCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _limitBox;
        private System.Windows.Forms.Label label11;
        private HexTextbox _maxHex;
        private System.Windows.Forms.CheckBox _exchangesCheck;
        private System.Windows.Forms.Button _sizeChangeButton;
        private System.Windows.Forms.CheckBox _tittleCheck;
        private System.Windows.Forms.TextBox _tittleBox;
        private System.Windows.Forms.Label label14;
        private HexTextbox _wordsCount;
    }
}