using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.UDZT
{
    public partial class SystemJournalForm : Form, IFormView
    {
        private UDZT _device;
        private DataTable _dataTable;

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(UDZT device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.SysJournalLoadOK += HandlerHelper.CreateHandler(this, this.SysJournalLoadOK);
            this._device.SysJournalLoadFail += HandlerHelper.CreateHandler(this, this.SysJournalLoadFail);
            this._device.SysJournalSaveOK += HandlerHelper.CreateHandler(this, this._device.LoadSystemJournal);

            this.GetDataTable();
        }

        private void OnSysJournalIndexLoadOk()
        {
            this._dataTable.Rows.Add(this._device.SysJournalMessageIndex.ToString(), this._device.SysJournalRecTime,
                this._device.SysJournalMessage);
            this._journalCntLabel.Text = this._device.SysJournalMessageIndex.ToString();
        }

        private void OnSysJournalIndexLoadFail()
        {
            this._dataTable.Rows.Add(this._device.SysJournalMessageIndex.ToString(), "������", "");
            this._journalCntLabel.Text = this._device.SysJournalMessageIndex.ToString();
        }

        private void OnSysJournalLoad()
        {
            if (this._dataTable.Rows.Count != 0)
            {
                this._readSysJournalBut.Enabled = true;
                MessageBox.Show("��������� ������ ��������");
            }
            else
            {
                this._readSysJournalBut.Enabled = true;
                MessageBox.Show("��������� ������ ����");
            }
        }

        private void GetDataTable()
        {
            this._dataTable = new DataTable("��801_������_�������");
            this._dataTable.Columns.Add("�����");
            this._dataTable.Columns.Add("�����");
            this._dataTable.Columns.Add("���������");

            this._sysJournalGrid.DataSource = this._dataTable;
        }

        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {
            if (this._openSysJounralDlg.ShowDialog() != DialogResult.OK) return;
            if (Path.GetExtension(this._openSysJounralDlg.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openSysJounralDlg.FileName);
                ushort[] data = Common.TOWORDS(file, true);
                if (data.Length % 9 != 0)
                {
                    this._journalCntLabel.Text = "���� �� ����� ���� ��������";
                    return;
                }
                ushort[] dataRecord = new ushort[9];
                int index = 0;
                int countRecord = 1;
                try
                {
                    while (index < data.Length)
                    {
                        Array.ConstrainedCopy(data, index, dataRecord, 0, 9);
                        this._device.SetSystemJurnalSlotValue(dataRecord);
                        this._device.SysJournalMessageIndex = countRecord;
                        this.OnSysJournalIndexLoadOk();
                        index = index + 9;
                        countRecord++;
                    }
                }
                catch (Exception)
                {
                    this._journalCntLabel.Text = "�� ������� ��������� ����";
                }
            }
            else
            {
                this._dataTable.Rows.Clear();
                this._dataTable.ReadXml(this._openSysJounralDlg.FileName);
            }
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.SaveSystemJournalIndex();
            this._readSysJournalBut.Enabled = false;
            this._dataTable.Rows.Clear();
        }

        private void SysJournalLoadFail()
        {
            try
            {
                if (this._device.SysJournalMessage != "��������� ���������")
                {
                    this.OnSysJournalIndexLoadFail();
                    this._device.LoadSystemJournal();
                }
                else
                {
                    this.OnSysJournalLoad();
                }
            }
            catch
            {
            }
        }

        private void SysJournalLoadOK()
        {
            try
            {
                if (this._device.SysJournalMessage != "��������� ���������")
                {
                    this.OnSysJournalIndexLoadOk();
                    this._device.LoadSystemJournal();
                }
                else
                {
                    this.OnSysJournalLoad();
                }
            }
            catch
            {
            }
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(UDZT); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.js;
            }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR801SJ);
                MessageBox.Show("������ ������� �������� � Html", "���������");
            }
        }
    }
}