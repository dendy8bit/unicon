using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN_XY_Chart;

namespace BEMN.UDZT
{
    public partial class OscilloscopeResultForm : Form
    {
        StartEndOscilloscope _oscObject;
        private List<int> _avaryBeginList = new List<int>();
        ArrayList _arrayX;
        ArrayList _arrayYa;
        ArrayList _arrayYb;
        ArrayList _arrayYc;
        ArrayList _arrayYn;
        int _maxX;
        int _S1maxY;
        int _S1minY;
        int _S2maxY;
        int _S2minY;
        int _S3maxY;
        int _S3minY;
        int _UmaxY;
        int _UminY;
        double _maxCurrentX;
        double _S1maxCurrentY;
        double _S1minCurrentY;
        double _S2maxCurrentY;
        double _S2minCurrentY;
        double _S3maxCurrentY;
        double _S3minCurrentY;
        double _UmaxCurrentY;
        double _UminCurrentY;
        double _S1chartStepY = 0;
        double _S2chartStepY = 0;
        double _S3chartStepY = 0;
        double _UchartStepY = 0;

        public OscilloscopeResultForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeResultForm(StartEndOscilloscope OscObject,List<int> avaryBeginList)
        {
            this.InitializeComponent();
            this._oscObject = OscObject;
            this._avaryBeginList = avaryBeginList;
        }

        public OscilloscopeResultForm(StartEndOscilloscope OscObject, List<int> avaryBeginList,string caption)
        {
            this.InitializeComponent();
            this._oscObject = OscObject;
            this._avaryBeginList = avaryBeginList;
            this.Text = caption;
        }

        private Color GetColor(int index)
        {
            Color color = new Color();
            switch (index)
            {
                case 0:
                    color = Color.Yellow;
                    break;
                case 1:
                    color = Color.Green;
                    break;
                case 2:
                    color = Color.Red;
                    break;
                case 3:
                    color = Color.Blue;
                    break;
                case 4:
                    color = Color.LightPink;
                    break;
                case 5:
                    color = Color.LightGoldenrodYellow;
                    break;
                case 6:
                    color = Color.LightGreen;
                    break;
                case 7:
                    color = Color.LightBlue;
                    break;
                case 8:
                    color = Color.Indigo;
                    break;
                case 9:
                    color = Color.DarkViolet;
                    break;
                case 10:
                    color = Color.DarkOrange;
                    break;
                case 11:
                    color = Color.DarkBlue;
                    break;
                case 12:
                    color = Color.PaleVioletRed;
                    break;
                case 13:
                    color = Color.LightSeaGreen;
                    break;
                case 14:
                    color = Color.DarkRed;
                    break;
                case 15:
                    color = Color.DarkTurquoise;
                    break;
                case 16:
                    color = Color.Goldenrod;
                    break;
                case 17:
                    color = Color.Lime;
                    break;
                case 18:
                    color = Color.Magenta;
                    break;
                case 19:
                    color = Color.Aqua;
                    break;
                case 20:
                    color = Color.Olive;
                    break;
                case 21:
                    color = Color.Khaki;
                    break;
                case 22:
                    color = Color.MediumAquamarine;
                    break;
                case 23:
                    color = Color.OliveDrab;
                    break;
                case 24:
                    color = Color.DarkGoldenrod;
                    break;
                case 25:
                    color = Color.SeaGreen;
                    break;
                case 26:
                    color = Color.Teal;
                    break;
                case 27:
                    color = Color.DeepPink;
                    break;
                case 28:
                    color = Color.MediumSpringGreen;
                    break;
                case 29:
                    color = Color.LimeGreen;
                    break;
                case 30:
                    color = Color.DeepSkyBlue;
                    break;
                case 31:
                    color = Color.MediumBlue;
                    break;
                case 32:
                    color = Color.Black;
                    break;
                case 33:
                    color = Color.Gray;
                    break;
                case 34:
                    color = Color.Gray;
                    break;
                default:
                    break;
            }
            return color;
        }

        private DAS_Net_XYChart.DAS_XYCurveVariable CreateCurve(string name, int curveIndex)
        {
            BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable _curve = new BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable();
            _curve.cColor = this.GetColor(curveIndex);
            _curve.strCurveName = name;
            _curve.bLineVisible = true;
            _curve.iPointNumber = 100;
            _curve.iPointSize = 5;
            _curve.iLineWidth = 1;
            _curve.ePointStyle = BEMN_XY_Chart.DAS_CurvePointStyle.BPTS_NONE;
            _curve.bVisible = true;
            _curve.iCurPriority = 0;
            _curve.dblMax = 1000.0;
            _curve.dblMin = 0;
            _curve.bYScaleVisible = true;
            _curve.bYAtStart = true;
            _curve.bAreaMode = false;
            _curve.dblAreaBaseValue = 0.0;

            return _curve;
        }

        private DAS_Net_XYChart.DAS_XYCurveVariable CreateLineCurve(string name, Color curveColor, bool visible, int lineWidth)
        {
            BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable _curve = new BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable();
            _curve.cColor = curveColor;
            _curve.strCurveName = name;
            _curve.bLineVisible = visible;
            _curve.iPointNumber = 100;
            _curve.iPointSize = 5;
            _curve.iLineWidth = lineWidth;
            _curve.ePointStyle = BEMN_XY_Chart.DAS_CurvePointStyle.BPTS_NONE;
            _curve.bVisible = true;
            _curve.iCurPriority = 0;
            _curve.dblMax = 1000.0;
            _curve.dblMin = 0;
            _curve.bYScaleVisible = true;
            _curve.bYAtStart = true;
            _curve.bAreaMode = false;
            _curve.dblAreaBaseValue = 0.0;

            return _curve;
        }

        private DAS_Net_XYChart.DAS_XYCurveVariable CurveVisible(string name, int curveIndex)
        {
            BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable _curve = new BEMN_XY_Chart.DAS_Net_XYChart.DAS_XYCurveVariable();
            _curve.cColor = this.GetColor(curveIndex);
            _curve.strCurveName = name;
            _curve.bLineVisible = false;
            _curve.iPointNumber = 100;
            _curve.iPointSize = 5;
            _curve.iLineWidth = 1;
            _curve.ePointStyle = BEMN_XY_Chart.DAS_CurvePointStyle.BPTS_NONE;
            _curve.bVisible = true;
            _curve.iCurPriority = 0;
            _curve.dblMax = 1000.0;
            _curve.dblMin = 0;
            _curve.bYScaleVisible = true;
            _curve.bYAtStart = true;
            _curve.bAreaMode = false;
            _curve.dblAreaBaseValue = 0.0;

            return _curve;
        }

        public void AddLineCurves(DAS_Net_XYChart _chart, string name, Color color, int lineWidth)
        {
            _chart.AddCurve(name, this.CreateLineCurve(name, color, true, lineWidth));
        }

        public void AddLinePoints(DAS_Net_XYChart _chart, string name, int value)
        {
            _chart.AddCurvePoint(name, value, _chart.CoordinateYMin);
            _chart.AddCurvePoint(name, value, _chart.CoordinateYMax);
            //_chart.Update();
        }

        public void VisibleLineCurves(DAS_Net_XYChart _chart, string name)
        {
            _chart.UpdateCurveProperty(name, this.CreateLineCurve(name,Color.Black,false, 0));
            _chart.Update();
        }
        
        public void UpdateLinePoints(DAS_Net_XYChart _chart, string name, int value)
        {
            _chart.RefreshCurvePoint(name, new ArrayList() { value, value }, new ArrayList() { _chart.CoordinateYMin, _chart.CoordinateYMax });
        }

        public void InitS1ChartLimits(DAS_Net_XYChart _chart) 
        {
            double[] _max = new double[]{this.GetMax(this._oscObject.S1PageIaValues),this.GetMax(this._oscObject.S1PageIbValues),
                                          this.GetMax(this._oscObject.S1PageIcValues),this.GetMax(this._oscObject.S1PageInValues)};
            double[] _min = new double[]{this.GetMin(this._oscObject.S1PageIaValues),this.GetMin(this._oscObject.S1PageIbValues),
                                          this.GetMin(this._oscObject.S1PageIcValues),this.GetMin(this._oscObject.S1PageInValues)};

            _chart.CoordinateYMax = this.GetMax(_max);
            _chart.CoordinateYMin = this.GetMin(_min);
            _chart.XMin = 0;
            _chart.XMax = this._oscObject.S1PageIaValues.Length;
        }

        public void AddS1Curves(DAS_Net_XYChart _chart)
        {
            _chart.AddCurve(this._oscObject.S1PageIaName, this.CreateCurve(this._oscObject.S1PageIaName, 0));
            _chart.AddCurve(this._oscObject.S1PageIbName, this.CreateCurve(this._oscObject.S1PageIbName, 1));
            _chart.AddCurve(this._oscObject.S1PageIcName, this.CreateCurve(this._oscObject.S1PageIcName, 2));
            _chart.AddCurve(this._oscObject.S1PageInName, this.CreateCurve(this._oscObject.S1PageInName, 3));
        }

        public void AddS1Points(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((this._s1TokChart.XMax - this._s1TokChart.XMin) / this._s1TokChart.Width) > _step)
            {
                _step = (int)((this._s1TokChart.XMax - this._s1TokChart.XMin) / this._s1TokChart.Width);
            }
            for (int i = (int)this._s1TokChart.XMin; i <= this._s1TokChart.XMax; i += _step)
            {
                if (i <= this._oscObject.S1PageIaValues.Length - 1)
                {
                    _chart.AddCurvePoint(this._oscObject.S1PageIaName, i, this._oscObject.S1PageIaValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S1PageIbName, i, this._oscObject.S1PageIbValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S1PageIcName, i, this._oscObject.S1PageIcValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S1PageInName, i, this._oscObject.S1PageInValues[i]);
                }
            }
            _chart.Update();
        }

        public void RefreshS1Points()
        {
            int _step = 1;
            this._arrayX = new ArrayList();
            this._arrayYa = new ArrayList();
            this._arrayYb = new ArrayList();
            this._arrayYc = new ArrayList();
            this._arrayYn = new ArrayList();
            if ((int)((this._s1TokChart.XMax - this._s1TokChart.XMin) / this._s1TokChart.Width) > _step) 
            {
                _step = (int)((this._s1TokChart.XMax - this._s1TokChart.XMin) / this._s1TokChart.Width);
            }
            if (this._s1TokChart.XMin < 0)
            {
                this._s1TokChart.XMin = 0;
            }
            for (int i = (int)this._s1TokChart.XMin; i <= this._s1TokChart.XMax; i += _step)
            {
                try
                {
                    if (i <= this._oscObject.S1PageIaValues.Length - 1)
                    {
                        this._arrayX.Add(i);
                        this._arrayYa.Add(this._oscObject.S1PageIaValues[i]);
                        this._arrayYb.Add(this._oscObject.S1PageIbValues[i]);
                        this._arrayYc.Add(this._oscObject.S1PageIcValues[i]);
                        this._arrayYn.Add(this._oscObject.S1PageInValues[i]);

                    }
                }
                catch { }
            } try
            {
                this._s1TokChart.RefreshCurvePoint(this._oscObject.S1PageIaName, this._arrayX, this._arrayYa);
                this._s1TokChart.RefreshCurvePoint(this._oscObject.S1PageIbName, this._arrayX, this._arrayYb);
                this._s1TokChart.RefreshCurvePoint(this._oscObject.S1PageIcName, this._arrayX, this._arrayYc);
                this._s1TokChart.RefreshCurvePoint(this._oscObject.S1PageInName, this._arrayX, this._arrayYn);
                this._s1TokChart.Refresh();
            }
            catch { }
        }

        public void InitS2ChartLimits(DAS_Net_XYChart _chart)
        {
            double[] _max = new double[]{this.GetMax(this._oscObject.S2PageIaValues),this.GetMax(this._oscObject.S2PageIbValues),
                                          this.GetMax(this._oscObject.S2PageIcValues),this.GetMax(this._oscObject.S2PageInValues)};
            double[] _min = new double[]{this.GetMin(this._oscObject.S2PageIaValues),this.GetMin(this._oscObject.S2PageIbValues),
                                          this.GetMin(this._oscObject.S2PageIcValues),this.GetMin(this._oscObject.S2PageInValues)};

            _chart.CoordinateYMax = this.GetMax(_max);
            _chart.CoordinateYMin = this.GetMin(_min);
            _chart.XMin = 0;
            _chart.XMax = this._oscObject.S2PageIaValues.Length;
        }

        public void AddS2Curves(DAS_Net_XYChart _chart)
        {
            _chart.AddCurve(this._oscObject.S2PageIaName, this.CreateCurve(this._oscObject.S2PageIaName, 0));
            _chart.AddCurve(this._oscObject.S2PageIbName, this.CreateCurve(this._oscObject.S2PageIbName, 1));
            _chart.AddCurve(this._oscObject.S2PageIcName, this.CreateCurve(this._oscObject.S2PageIcName, 2));
            _chart.AddCurve(this._oscObject.S2PageInName, this.CreateCurve(this._oscObject.S2PageInName, 3));
        }

        public void AddS2Points(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((this._s2TokChart.XMax - this._s2TokChart.XMin) / this._s2TokChart.Width) > _step)
            {
                _step = (int)((this._s2TokChart.XMax - this._s2TokChart.XMin) / this._s2TokChart.Width);
            }
            for (int i = (int)this._s2TokChart.XMin; i <= this._s2TokChart.XMax; i += _step)
            {
                if (i <= this._oscObject.S2PageIaValues.Length - 1)
                {
                    _chart.AddCurvePoint(this._oscObject.S2PageIaName, i, this._oscObject.S2PageIaValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S2PageIbName, i, this._oscObject.S2PageIbValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S2PageIcName, i, this._oscObject.S2PageIcValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S2PageInName, i, this._oscObject.S2PageInValues[i]);
                }
            }
            _chart.Update();
        }

        public void RefreshS2Points()
        {
            int _step = 1;
            this._arrayX = new ArrayList();
            this._arrayYa = new ArrayList();
            this._arrayYb = new ArrayList();
            this._arrayYc = new ArrayList();
            this._arrayYn = new ArrayList();
            if ((int)((this._s2TokChart.XMax - this._s2TokChart.XMin) / this._s2TokChart.Width) > _step)
            {
                _step = (int)((this._s2TokChart.XMax - this._s2TokChart.XMin) / this._s2TokChart.Width);
            }
            if (this._s2TokChart.XMin < 0)
            {
                this._s2TokChart.XMin = 0;
            }
            for (int i = (int)this._s2TokChart.XMin; i <= this._s2TokChart.XMax; i += _step)
            {
                try
                {
                    if (i <= this._oscObject.S2PageIaValues.Length - 1)
                    {
                        this._arrayX.Add(i);
                        this._arrayYa.Add(this._oscObject.S2PageIaValues[i]);
                        this._arrayYb.Add(this._oscObject.S2PageIbValues[i]);
                        this._arrayYc.Add(this._oscObject.S2PageIcValues[i]);
                        this._arrayYn.Add(this._oscObject.S2PageInValues[i]);
                    }
                }
                catch { }
            }
            try
            {
                this._s2TokChart.RefreshCurvePoint(this._oscObject.S2PageIaName, this._arrayX, this._arrayYa);
                this._s2TokChart.RefreshCurvePoint(this._oscObject.S2PageIbName, this._arrayX, this._arrayYb);
                this._s2TokChart.RefreshCurvePoint(this._oscObject.S2PageIcName, this._arrayX, this._arrayYc);
                this._s2TokChart.RefreshCurvePoint(this._oscObject.S2PageInName, this._arrayX, this._arrayYn);
                this._s2TokChart.Refresh();
            }
            catch { }
        }

        public void InitS3ChartLimits(DAS_Net_XYChart _chart)
        {
            double[] _max = new double[]{this.GetMax(this._oscObject.S3PageIaValues),this.GetMax(this._oscObject.S3PageIbValues),
                                          this.GetMax(this._oscObject.S3PageIcValues),this.GetMax(this._oscObject.S3PageInValues)};
            double[] _min = new double[]{this.GetMin(this._oscObject.S3PageIaValues),this.GetMin(this._oscObject.S3PageIbValues),
                                          this.GetMin(this._oscObject.S3PageIcValues),this.GetMin(this._oscObject.S3PageInValues)};

            _chart.CoordinateYMax = this.GetMax(_max);
            _chart.CoordinateYMin = this.GetMin(_min);
            _chart.XMin = 0;
            _chart.XMax = this._oscObject.S3PageIaValues.Length;
        }

        public void AddS3Curves(DAS_Net_XYChart _chart)
        {
            _chart.AddCurve(this._oscObject.S3PageIaName, this.CreateCurve(this._oscObject.S3PageIaName, 0));
            _chart.AddCurve(this._oscObject.S3PageIbName, this.CreateCurve(this._oscObject.S3PageIbName, 1));
            _chart.AddCurve(this._oscObject.S3PageIcName, this.CreateCurve(this._oscObject.S3PageIcName, 2));
            _chart.AddCurve(this._oscObject.S3PageInName, this.CreateCurve(this._oscObject.S3PageInName, 3));
        }

        public void AddS3Points(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((this._s3TokChart.XMax - this._s3TokChart.XMin) / this._s3TokChart.Width) > _step)
            {
                _step = (int)((this._s3TokChart.XMax - this._s3TokChart.XMin) / this._s3TokChart.Width);
            }
            for (int i = (int)this._s3TokChart.XMin; i <= this._s3TokChart.XMax; i += _step)
            {
                if (i <= this._oscObject.S3PageIaValues.Length - 1)
                {
                    _chart.AddCurvePoint(this._oscObject.S3PageIaName, i, this._oscObject.S3PageIaValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S3PageIbName, i, this._oscObject.S3PageIbValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S3PageIcName, i, this._oscObject.S3PageIcValues[i]);
                    _chart.AddCurvePoint(this._oscObject.S3PageInName, i, this._oscObject.S3PageInValues[i]);
                }
            }
            _chart.Update();
        }

        public void RefreshS3Points()
        {
            int _step = 1;
            this._arrayX = new ArrayList();
            this._arrayYa = new ArrayList();
            this._arrayYb = new ArrayList();
            this._arrayYc = new ArrayList();
            this._arrayYn = new ArrayList();
            if ((int)((this._s3TokChart.XMax - this._s3TokChart.XMin) / this._s3TokChart.Width) > _step)
            {
                _step = (int)((this._s3TokChart.XMax - this._s3TokChart.XMin) / this._s3TokChart.Width);
            }
            if (this._s3TokChart.XMin < 0)
            {
                this._s3TokChart.XMin = 0;
            }
            for (int i = (int)this._s3TokChart.XMin; i <= this._s3TokChart.XMax; i += _step)
            {
                try
                {
                    if (i <= this._oscObject.S3PageIaValues.Length - 1)
                    {
                        this._arrayX.Add(i);
                        this._arrayYa.Add(this._oscObject.S3PageIaValues[i]);
                        this._arrayYb.Add(this._oscObject.S3PageIbValues[i]);
                        this._arrayYc.Add(this._oscObject.S3PageIcValues[i]);
                        this._arrayYn.Add(this._oscObject.S3PageInValues[i]);
                    }
                }
                catch { }
            }
            try
            {
                this._s3TokChart.RefreshCurvePoint(this._oscObject.S3PageIaName, this._arrayX, this._arrayYa);
                this._s3TokChart.RefreshCurvePoint(this._oscObject.S3PageIbName, this._arrayX, this._arrayYb);
                this._s3TokChart.RefreshCurvePoint(this._oscObject.S3PageIcName, this._arrayX, this._arrayYc);
                this._s3TokChart.RefreshCurvePoint(this._oscObject.S3PageInName, this._arrayX, this._arrayYn);
                this._s3TokChart.Refresh();
            }
            catch { }
        }

        public void InitUChartLimits(DAS_Net_XYChart _chart)
        {
            double[] _max = new double[]{this.GetMax(this._oscObject.UaValues),this.GetMax(this._oscObject.UbValues),
                                          this.GetMax(this._oscObject.UcValues),this.GetMax(this._oscObject.UnValues)};
            double[] _min = new double[]{this.GetMin(this._oscObject.UaValues),this.GetMin(this._oscObject.UbValues),
                                          this.GetMin(this._oscObject.UcValues),this.GetMin(this._oscObject.UnValues)};

            _chart.CoordinateYMax = this.GetMax(_max);
            _chart.CoordinateYMin = this.GetMin(_min);
            _chart.XMin = 0;
            _chart.XMax = this._oscObject.UaValues.Length;
        }

        public void AddUCurves(DAS_Net_XYChart _chart)
        {
            _chart.AddCurve(this._oscObject.UaName, this.CreateCurve(this._oscObject.UaName, 0));
            _chart.AddCurve(this._oscObject.UbName, this.CreateCurve(this._oscObject.UbName, 1));
            _chart.AddCurve(this._oscObject.UcName, this.CreateCurve(this._oscObject.UcName, 2));
            _chart.AddCurve(this._oscObject.UnName, this.CreateCurve(this._oscObject.UnName, 3));
        }

        public void AddUPoints(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((this._voltageChart.XMax - this._voltageChart.XMin) / this._voltageChart.Width) > _step)
            {
                _step = (int)((this._voltageChart.XMax - this._voltageChart.XMin) / this._voltageChart.Width);
            }
            for (int i = (int)this._voltageChart.XMin; i <= this._voltageChart.XMax; i += _step)
            {
                if (i <= this._oscObject.UaValues.Length - 1)
                {
                    _chart.AddCurvePoint(this._oscObject.UaName, i, this._oscObject.UaValues[i]);
                    _chart.AddCurvePoint(this._oscObject.UbName, i, this._oscObject.UbValues[i]);
                    _chart.AddCurvePoint(this._oscObject.UcName, i, this._oscObject.UcValues[i]);
                    _chart.AddCurvePoint(this._oscObject.UnName, i, this._oscObject.UnValues[i]);
                }
            }
            _chart.Update();
        }

        public void RefreshUPoints()
        {
            int _step = 1;
            this._arrayX = new ArrayList();
            this._arrayYa = new ArrayList();
            this._arrayYb = new ArrayList();
            this._arrayYc = new ArrayList();
            this._arrayYn = new ArrayList();
            if ((int)((this._voltageChart.XMax - this._voltageChart.XMin) / this._voltageChart.Width) > _step)
            {
                _step = (int)((this._voltageChart.XMax - this._voltageChart.XMin) / this._voltageChart.Width);
            }
            if (this._voltageChart.XMin < 0)
            {
                this._voltageChart.XMin = 0;
            }
            for (int i = (int)this._voltageChart.XMin; i <= this._voltageChart.XMax; i += _step)
            {
                try
                {
                    if (i <= this._oscObject.UaValues.Length - 1)
                    {
                        this._arrayX.Add(i);
                        this._arrayYa.Add(this._oscObject.UaValues[i]);
                        this._arrayYb.Add(this._oscObject.UbValues[i]);
                        this._arrayYc.Add(this._oscObject.UcValues[i]);
                        this._arrayYn.Add(this._oscObject.UnValues[i]);
                    }
                }
                catch { }
            }
            try
            {
                this._voltageChart.RefreshCurvePoint(this._oscObject.UaName, this._arrayX, this._arrayYa);
                this._voltageChart.RefreshCurvePoint(this._oscObject.UbName, this._arrayX, this._arrayYb);
                this._voltageChart.RefreshCurvePoint(this._oscObject.UcName, this._arrayX, this._arrayYc);
                this._voltageChart.RefreshCurvePoint(this._oscObject.UnName, this._arrayX, this._arrayYn);
                this._voltageChart.Refresh();
            }
            catch { }
        }

        public void InitDiscretChartLimits(DAS_Net_XYChart _chart)
        {
            _chart.CoordinateYMax = 33;
            _chart.CoordinateYMin = 0;
            _chart.GridYTicker = 33;
            _chart.XMin = 0;
            _chart.XMax = this._oscObject.OscilloscopeLengs;
            _chart.CoordinateYOrigin = 33;
        }

        public void AddDiscretCurves(DAS_Net_XYChart _chart)
        {
            for (int i = 0; i <= this._oscObject.DiskretData.Count; i++) 
            {
                _chart.AddCurve("D" + i.ToString(), this.CreateCurve("D" + i.ToString(), i));
            }
        }

        public void AddDiscretPoints(DAS_Net_XYChart _chart)
        {
            int _step = 1;
            if ((int)((this._discretChart.XMax - this._discretChart.XMin) / this._discretChart.Width) > _step)
            {
                _step = (int)((this._discretChart.XMax - this._discretChart.XMin) / this._discretChart.Width);
            }
            if (this._discretChart.XMin < 0)
            {
                this._discretChart.XMin = 0;
            }
            for (int j = 0; j < this._oscObject.DiskretData.Count; j++)
            {
                for (int i = (int)this._discretChart.XMin; i <= this._discretChart.XMax; i += _step)
                {
                    if (i <= this._oscObject.OscilloscopeLengs - 1)
                    {
                        if (this._oscObject.DiskretData[j][i])
                        {
                            _chart.AddCurvePoint("D" + j.ToString(), i, j + 1.5);
                        }
                        else 
                        {
                            _chart.AddCurvePoint("D" + j.ToString(), i, j+1);
                        }
                    }
                }
            }
            _chart.Update();
        }

        public void RefreshDiscretPoints()
        {
            int _step = 1;

            if ((int)((this._discretChart.XMax - this._discretChart.XMin) / this._discretChart.Width) > _step)
            {
                _step = (int)((this._discretChart.XMax - this._discretChart.XMin) / this._discretChart.Width);
            }
            if (this._discretChart.XMin < 0)
            {
                this._discretChart.XMin = 0;
            }
            try
            {
                for (int j = 0; j < this._oscObject.DiskretData.Count ; j++)
                {
                    this._arrayX = new ArrayList();
                    this._arrayYa = new ArrayList();
                    for (int i = (int)this._discretChart.XMin; i <= this._discretChart.XMax; i += _step)
                    {
                        if (i <= this._oscObject.OscilloscopeLengs - 1)
                        {
                            try
                            {
                                if (this._oscObject.DiskretData[j][i])
                                {
                                    this._arrayX.Add(i);
                                    this._arrayYa.Add(j + 1.5);
                                }
                                else
                                {
                                    this._arrayX.Add(i);
                                    this._arrayYa.Add(j + 1);
                                }
                            }
                            catch { }
                        }
                    }
                    this._discretChart.RefreshCurvePoint("D" + j.ToString(), this._arrayX, this._arrayYa);
                }
                this._discretChart.Refresh();
            }
            catch { }
        }

        public double GetMax(double[] _array) 
        {
            double _rez = 100;
            for (int i = 0; i < _array.Length; i++) 
            {
                _rez = Math.Max(_rez, _array[i]);
            }
            return _rez;
        }

        public double GetMin(double[] _array)
        {
            double _rez = -100;
            for (int i = 0; i < _array.Length; i++)
            {
                _rez = Math.Min(_rez, _array[i]);
            }
            return _rez;
        }

        public void DrawOsc()
        {
            this.AddS1Points(this._s1TokChart);
            this.AddS2Points(this._s2TokChart);
            this.AddS3Points(this._s3TokChart);
            this.AddUPoints(this._voltageChart);
            this.AddDiscretPoints(this._discretChart);
        }

        public void RefreshAllOsc()
        {
            if (this.S1ShowCB.Checked)
            {
                this.RefreshS1Points();
            }
            if (this.S2ShowCB.Checked)
            {
                this.RefreshS2Points();
            }
            if (this.S3ShowCB.Checked)
            {
                this.RefreshS3Points();
            }
            if (this.UShowCB.Checked)
            {
                this.RefreshUPoints();
            }
            if (this.DiscretShowCB.Checked)
            {
                this.RefreshDiscretPoints();
            }
        }

        Label[] M1S1Labels;
        Label[] M1S2Labels;
        Label[] M1S3Labels;
        Label[] M1ULabels;
        Label[] M1DLabels;
        Label[] M2S1Labels;
        Label[] M2S2Labels;
        Label[] M2S3Labels;
        Label[] M2ULabels;
        Label[] M2DLabels;
        List<double[]> S1Values;
        List<double[]> S2Values;
        List<double[]> S3Values;
        List<double[]> UValues;
        string[] IChanals;
        string[] UChanals;
        string[] DChanals;
        CheckBox[] CheckBoxes;
        private void OscilloscopeResultForm_Load(object sender, EventArgs e)
        {
            this.IChanals = new string[] { "Ia","Ib","Ic","In"};
            this.UChanals = new string[] { "Ua", "Ub", "Uc", "Un" };
            this.DChanals = new string[] { "�1", "�2", "�3", "�4",
                                    "�5", "�6", "�7", "�8",
                                    "�9", "�10", "�11", "�12",
                                    "�13", "�14", "�15", "�16",
                                    "�17", "�18", "�19", "�20",
                                    "�21", "�22", "�23", "�24"};
            this.M1S1Labels = new Label[] { this.Marker1S1Ia, this.Marker1S1Ib, this.Marker1S1Ic, this.Marker1S1In };
            this.M1S2Labels = new Label[] { this.Marker1S2Ia, this.Marker1S2Ib, this.Marker1S2Ic, this.Marker1S2In };
            this.M1S3Labels = new Label[] { this.Marker1S3Ia, this.Marker1S3Ib, this.Marker1S3Ic, this.Marker1S3In };
            this.M1ULabels = new Label[] { this.Marker1Ua, this.Marker1Ub, this.Marker1Uc, this.Marker1Un };
            this.M1DLabels = new Label[] {   this.Marker1D1, this.Marker1D2, this.Marker1D3, this.Marker1D4, 
                                        this.Marker1D5, this.Marker1D6, this.Marker1D7, this.Marker1D8,
                                        this.Marker1D9, this.Marker1D10, this.Marker1D11, this.Marker1D12,
                                        this.Marker1D13, this.Marker1D14, this.Marker1D15, this.Marker1D16,
                                        this.Marker1D17, this.Marker1D18, this.Marker1D19, this.Marker1D20,
                                        this.Marker1D21, this.Marker1D22, this.Marker1D23, this.Marker1D24,};
            this.M2S1Labels = new Label[] { this.Marker2S1Ia, this.Marker2S1Ib, this.Marker2S1Ic, this.Marker2S1In };
            this.M2S2Labels = new Label[] { this.Marker2S2Ia, this.Marker2S2Ib, this.Marker2S2Ic, this.Marker2S2In };
            this.M2S3Labels = new Label[] { this.Marker2S3Ia, this.Marker2S3Ib, this.Marker2S3Ic, this.Marker2S3In };
            this.M2ULabels = new Label[] { this.Marker2Ua, this.Marker2Ub, this.Marker2Uc, this.Marker2Un };
            this.M2DLabels = new Label[] {   this.Marker2D1, this.Marker2D2, this.Marker2D3, this.Marker2D4, 
                                        this.Marker2D5, this.Marker2D6, this.Marker2D7, this.Marker2D8,
                                        this.Marker2D9, this.Marker2D10, this.Marker2D11, this.Marker2D12,
                                        this.Marker2D13, this.Marker2D14, this.Marker2D15, this.Marker2D16,
                                        this.Marker2D17, this.Marker2D18, this.Marker2D19, this.Marker2D20,
                                        this.Marker2D21, this.Marker2D22, this.Marker2D23, this.Marker2D24,};
            this.CheckBoxes = new CheckBox[] { this.S1ShowCB, this.S2ShowCB, this.S3ShowCB, this.UShowCB };

            try
            {
                this.S1Values = new List<double[]>() { this._oscObject.S1PageIaValues, this._oscObject.S1PageIbValues, this._oscObject.S1PageIcValues, this._oscObject.S1PageInValues };
                this.S2Values = new List<double[]>() { this._oscObject.S2PageIaValues, this._oscObject.S2PageIbValues, this._oscObject.S2PageIcValues, this._oscObject.S2PageInValues };
                this.S3Values = new List<double[]>() { this._oscObject.S3PageIaValues, this._oscObject.S3PageIbValues, this._oscObject.S3PageIcValues, this._oscObject.S3PageInValues };
                this.UValues = new List<double[]>() { this._oscObject.UaValues, this._oscObject.UbValues, this._oscObject.UcValues, this._oscObject.UnValues };
            }
            catch { }
            this.WindowState = FormWindowState.Maximized;
            this.panel2.Width = this.MAINPANEL.Width - 20;
            try
            {
                this.InitS1ChartLimits(this._s1TokChart);
                this.InitS2ChartLimits(this._s2TokChart);
                this.InitS3ChartLimits(this._s3TokChart);
                this.InitUChartLimits(this._voltageChart);
                this.InitDiscretChartLimits(this._discretChart);

                this.AddS1Curves(this._s1TokChart);
                this.AddS2Curves(this._s2TokChart);
                this.AddS3Curves(this._s3TokChart);
                this.AddUCurves(this._voltageChart);
                this.AddDiscretCurves(this._discretChart);

                this.DrawOsc();

            }
            catch { }
            this.hScrollBar4.Maximum = (int)this._s1TokChart.XMax;
            this.hScrollBar4.Minimum = (int)this._s1TokChart.XMax;

            this.S1Scroll.Maximum = (int)this._s1TokChart.CoordinateYMax;
            this.S1Scroll.Minimum = (int)this._s1TokChart.CoordinateYMax;

            this.S2Scroll.Maximum = (int)this._s2TokChart.CoordinateYMax;
            this.S2Scroll.Minimum = (int)this._s2TokChart.CoordinateYMax;

            this.S3Scroll.Maximum = (int)this._s3TokChart.CoordinateYMax;
            this.S3Scroll.Minimum = (int)this._s3TokChart.CoordinateYMax;

            this.UScroll.Maximum = (int)this._voltageChart.CoordinateYMax;
            this.UScroll.Minimum = (int)this._voltageChart.CoordinateYMax;

            this._maxCurrentX = this._s1TokChart.XMax;
            this._S1maxCurrentY = this._s1TokChart.CoordinateYMax;
            this._S1minCurrentY = this._s1TokChart.CoordinateYMin;
            this._S1minY = (int)this._s1TokChart.CoordinateYMin;
            this._S2maxCurrentY = this._s2TokChart.CoordinateYMax;
            this._S2minCurrentY = this._s2TokChart.CoordinateYMin;
            this._S2minY = (int)this._s2TokChart.CoordinateYMin;
            this._S3maxCurrentY = this._s3TokChart.CoordinateYMax;
            this._S3minCurrentY = this._s3TokChart.CoordinateYMin;
            this._S3minY = (int)this._s3TokChart.CoordinateYMin;
            this._UmaxCurrentY = this._voltageChart.CoordinateYMax;
            this._UminCurrentY = this._voltageChart.CoordinateYMin;
            this._UminY = (int)this._voltageChart.CoordinateYMin;

            this._maxX = (int)this._maxCurrentX;
            this._S1maxY = (int)this._S1maxCurrentY;
            this._S2maxY = (int)this._S2maxCurrentY;
            this._S3maxY = (int)this._S3maxCurrentY;
            this._UmaxY = (int)this._UmaxCurrentY;
            this._s1TokChart.CoordinateXOrigin = this._s1TokChart.XMin + this._s1TokChart.XMax / 2;
            this._s2TokChart.CoordinateXOrigin = this._s2TokChart.XMin + this._s2TokChart.XMax / 2;
            this._s3TokChart.CoordinateXOrigin = this._s3TokChart.XMin + this._s3TokChart.XMax / 2;
            this._voltageChart.CoordinateXOrigin = this._voltageChart.XMin + this._voltageChart.XMax / 2;
            this._discretChart.CoordinateXOrigin = this._discretChart.XMin + this._discretChart.XMax / 2;
            this._s1TokChart.CoordinateYOrigin = (this._s1TokChart.CoordinateYMax + this._s1TokChart.CoordinateYMin) / 2;
            this._s2TokChart.CoordinateYOrigin = (this._s2TokChart.CoordinateYMax + this._s2TokChart.CoordinateYMin) / 2;
            this._s3TokChart.CoordinateYOrigin = (this._s3TokChart.CoordinateYMax + this._s3TokChart.CoordinateYMin) / 2;
            this._voltageChart.CoordinateYOrigin = (this._voltageChart.CoordinateYMax + this._voltageChart.CoordinateYMin) / 2;
            this.Xplus.Enabled = false;
        }

        private void hScrollBar4_Scroll(object sender, ScrollEventArgs e)
        {
            double min = this._xStep * (e.NewValue - this.hScrollBar4.Minimum);
            double max = min + this._xStep * this._s1TokChart.GridXTicker;
            this._s1TokChart.XMin = min;
            this._s1TokChart.XMax = max;
            this._s2TokChart.XMin = min;
            this._s2TokChart.XMax = max;
            this._s3TokChart.XMin = min;
            this._s3TokChart.XMax = max;
            this._voltageChart.XMin = min;
            this._voltageChart.XMax = max;
            this._discretChart.XMin = min;
            this._discretChart.XMax = max;
            this._s1TokChart.CoordinateXOrigin = (this._s1TokChart.XMin + this._s1TokChart.XMax) / 2;
            this._s2TokChart.CoordinateXOrigin = (this._s2TokChart.XMin + this._s2TokChart.XMax) / 2;
            this._s3TokChart.CoordinateXOrigin = (this._s3TokChart.XMin + this._s3TokChart.XMax) / 2;
            this._voltageChart.CoordinateXOrigin = (this._voltageChart.XMin + this._voltageChart.XMax) / 2;
            this._discretChart.CoordinateXOrigin = (this._discretChart.XMin + this._discretChart.XMax) / 2;
            this.RefreshAllOsc();
        }

        private void discretButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                if (_but.BackColor != Color.White)
                {
                    _but.BackColor = Color.White;
                    this._discretChart.UpdateCurveProperty("D" + (Convert.ToInt32(_but.Tag) - 1), this.CurveVisible("D" + (Convert.ToInt32(_but.Tag) - 1), Convert.ToInt32(_but.Tag) - 1));
                }
                else
                {
                    _but.BackColor = this.GetColor(Convert.ToInt32(_but.Text) - 1);
                    this._discretChart.UpdateCurveProperty("D" + (Convert.ToInt32(_but.Tag) - 1), this.CreateCurve("D" + (Convert.ToInt32(_but.Tag) - 1), Convert.ToInt32(_but.Tag) - 1));
                }
            }
            catch { }
        }

        private void s1Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                switch (_but.Text)
                {
                    case "Ia":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageIaName, this.CurveVisible(this._oscObject.S1PageIaName, 0));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(0);
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageIaName, this.CreateCurve(this._oscObject.S1PageIaName, 0));
                            }
                            break;
                        }
                    case "Ib":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageIbName, this.CurveVisible(this._oscObject.S1PageIbName, 1));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(1);
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageIbName, this.CreateCurve(this._oscObject.S1PageIbName, 1));
                            }
                            break;
                        }
                    case "Ic":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageIcName, this.CurveVisible(this._oscObject.S1PageIcName, 2));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(2);
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageIcName, this.CreateCurve(this._oscObject.S1PageIcName, 2));
                            }
                            break;
                        }
                    case "In":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageInName, this.CurveVisible(this._oscObject.S1PageInName, 3));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(3);
                                this._s1TokChart.UpdateCurveProperty(this._oscObject.S1PageInName, this.CreateCurve(this._oscObject.S1PageInName, 3));
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void s2Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                switch (_but.Text)
                {
                    case "Ia":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageIaName, this.CurveVisible(this._oscObject.S2PageIaName, 0));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(0);
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageIaName, this.CreateCurve(this._oscObject.S2PageIaName, 0));
                            }
                            break;
                        }
                    case "Ib":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageIbName, this.CurveVisible(this._oscObject.S2PageIbName, 1));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(1);
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageIbName, this.CreateCurve(this._oscObject.S2PageIbName, 1));
                            }
                            break;
                        }
                    case "Ic":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageIcName, this.CurveVisible(this._oscObject.S2PageIcName, 2));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(2);
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageIcName, this.CreateCurve(this._oscObject.S2PageIcName, 2));
                            }
                            break;
                        }
                    case "In":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageInName, this.CurveVisible(this._oscObject.S2PageInName, 3));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(3);
                                this._s2TokChart.UpdateCurveProperty(this._oscObject.S2PageInName, this.CreateCurve(this._oscObject.S2PageInName, 3));
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void s3Button_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                switch (_but.Text)
                {
                    case "Ia":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageIaName, this.CurveVisible(this._oscObject.S3PageIaName, 0));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(0);
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageIaName, this.CreateCurve(this._oscObject.S3PageIaName, 0));
                            }
                            break;
                        }
                    case "Ib":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageIbName, this.CurveVisible(this._oscObject.S3PageIbName, 1));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(1);
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageIbName, this.CreateCurve(this._oscObject.S3PageIbName, 1));
                            }
                            break;
                        }
                    case "Ic":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageIcName, this.CurveVisible(this._oscObject.S3PageIcName, 2));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(2);
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageIcName, this.CreateCurve(this._oscObject.S3PageIcName, 2));
                            }
                            break;
                        }
                    case "In":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageInName, this.CurveVisible(this._oscObject.S3PageInName, 3));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(3);
                                this._s3TokChart.UpdateCurveProperty(this._oscObject.S3PageInName, this.CreateCurve(this._oscObject.S3PageInName, 3));
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        private void uButton_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Button _but = (System.Windows.Forms.Button)sender;

                switch (_but.Text)
                {
                    case "Ua":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UaName, this.CurveVisible(this._oscObject.UaName, 0));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(0);
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UaName, this.CreateCurve(this._oscObject.UaName, 0));
                            }
                            break;
                        }
                    case "Ub":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UbName, this.CurveVisible(this._oscObject.UbName, 1));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(1);
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UbName, this.CreateCurve(this._oscObject.UbName, 1));
                            }
                            break;
                        }
                    case "Uc":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UcName, this.CurveVisible(this._oscObject.UcName, 2));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(2);
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UcName, this.CreateCurve(this._oscObject.UcName, 2));
                            }
                            break;
                        }
                    case "Un":
                        {
                            if (_but.BackColor != Color.White)
                            {
                                _but.BackColor = Color.White;
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UnName, this.CurveVisible(this._oscObject.UnName, 3));
                            }
                            else
                            {
                                _but.BackColor = this.GetColor(3);
                                this._voltageChart.UpdateCurveProperty(this._oscObject.UnName, this.CreateCurve(this._oscObject.UnName, 3));
                            }
                            break;
                        }
                }
            }
            catch { }
        }

        public double CalculateYStep(BEMN_XY_Chart.DAS_Net_XYChart _chart)
        {
            double rez = 0;
            if (_chart.CoordinateYMax > 0 && (_chart.CoordinateYMin >= 0 || _chart.CoordinateYMin <= 0))
            {
                rez = (_chart.CoordinateYMax - _chart.CoordinateYMin) / _chart.GridYTicker;
            }
            else if (_chart.CoordinateYMax < 0 && _chart.CoordinateYMin < 0)
            {
                rez = Math.Abs(_chart.CoordinateYMin - _chart.CoordinateYMax) / _chart.GridYTicker;
            }
            return rez;
        }

        double _xStep = 0;
        public double CalculateXStep(BEMN_XY_Chart.DAS_Net_XYChart _chart)
        {
            double rez = 0;
            if (_chart.XMax > 0 && (_chart.XMin >= 0 || _chart.XMin <= 0))
            {
                rez = (_chart.XMax - _chart.XMin) / _chart.GridXTicker;
            }
            else if (_chart.XMax < 0 && _chart.XMin < 0)
            {
                rez = Math.Abs(_chart.XMin - _chart.XMax) / _chart.GridXTicker;
            }
            return rez;
        }

        double _S1YStep = 0;
        private void S1Scroll_Scroll(object sender, ScrollEventArgs e)
        {
            this._s1TokChart.CoordinateYMin = this._S1maxY - this._S1YStep * this._s1TokChart.GridYTicker - this._S1YStep * (e.NewValue - this.S1Scroll.Minimum);
            this._s1TokChart.CoordinateYMax = this._S1maxY - this._S1YStep * (e.NewValue - this.S1Scroll.Minimum);
            this._s1TokChart.CoordinateYOrigin = (this._s1TokChart.CoordinateYMin + this._s1TokChart.CoordinateYMax) / 2;
            if (this.Marker1CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._s1TokChart, "������1", this.Marker1TrackBar.Value);
            }
            if (this.Marker2CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._s1TokChart, "������2", this.Marker2TrackBar.Value);
            }
            if (this._avaryJournalCheck.Checked)
            {
                for (int i = 0; i < this._avaryBeginList.Count; i++)
                {
                    this.UpdateLinePoints(this._s1TokChart, "������ " + i, this._avaryBeginList[i]);
                }
            }
            this.RefreshS1Points();
        }

        double _S2YStep = 0;
        private void S2Scroll_Scroll(object sender, ScrollEventArgs e)
        {
            this._s2TokChart.CoordinateYMin = this._S2maxY - this._S2YStep * this._s2TokChart.GridYTicker - this._S2YStep * (e.NewValue - this.S2Scroll.Minimum);
            this._s2TokChart.CoordinateYMax = this._S2maxY - this._S2YStep * (e.NewValue - this.S2Scroll.Minimum);
            this._s2TokChart.CoordinateYOrigin = (this._s2TokChart.CoordinateYMin + this._s2TokChart.CoordinateYMax) / 2;
            if (this.Marker1CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._s2TokChart, "������1", this.Marker1TrackBar.Value);
            } 
            if (this.Marker2CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._s2TokChart, "������2", this.Marker2TrackBar.Value);
            }
            if (this._avaryJournalCheck.Checked)
            {
                for (int i = 0; i < this._avaryBeginList.Count; i++)
                {
                    this.UpdateLinePoints(this._s2TokChart, "������ " + i, this._avaryBeginList[i]);
                }
            }
            this.RefreshS2Points();
        }

        double _S3YStep = 0;
        private void S3Scroll_Scroll(object sender, ScrollEventArgs e)
        {
            this._s3TokChart.CoordinateYMin = this._S3maxY - this._S3YStep * this._s3TokChart.GridYTicker - this._S3YStep * (e.NewValue - this.S3Scroll.Minimum);
            this._s3TokChart.CoordinateYMax = this._S3maxY - this._S3YStep * (e.NewValue - this.S3Scroll.Minimum);
            this._s3TokChart.CoordinateYOrigin = (this._s3TokChart.CoordinateYMin + this._s3TokChart.CoordinateYMax) / 2;
            if (this.Marker1CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._s3TokChart, "������1", this.Marker1TrackBar.Value);
            }
            if (this.Marker2CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._s3TokChart, "������2", this.Marker2TrackBar.Value);
            }
            if (this._avaryJournalCheck.Checked)
            {
                for (int i = 0; i < this._avaryBeginList.Count; i++)
                {
                    this.UpdateLinePoints(this._s3TokChart, "������ " + i, this._avaryBeginList[i]);
                }
            }
            this.RefreshS3Points();
        }

        double _UYStep = 0;
        private void UScroll_Scroll(object sender, ScrollEventArgs e)
        {
            this._voltageChart.CoordinateYMin = this._UmaxY - this._UYStep * this._voltageChart.GridYTicker - this._UYStep * (e.NewValue - this.UScroll.Minimum);
            this._voltageChart.CoordinateYMax = this._UmaxY - this._UYStep * (e.NewValue - this.UScroll.Minimum);
            this._voltageChart.CoordinateYOrigin = (this._voltageChart.CoordinateYMin + this._voltageChart.CoordinateYMax) / 2;
            if (this.Marker1CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._voltageChart, "������1", this.Marker1TrackBar.Value);
            }
            if (this.Marker2CB.Checked && this.MarkerCB.Checked)
            {
                this.UpdateLinePoints(this._voltageChart, "������2", this.Marker2TrackBar.Value);
            }
            if (this._avaryJournalCheck.Checked)
            {
                for (int i = 0; i < this._avaryBeginList.Count; i++)
                {
                    this.UpdateLinePoints(this._voltageChart, "������ " + i, this._avaryBeginList[i]);
                }
            }
            this.RefreshUPoints();
        }

        int S1clicCount = 0;
        private void S1YPlus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.S1clicCount < 5)
                {
                    this.S1Yminus.Enabled = true;
                    this.S1Scroll.Visible = true;
                    this._s1TokChart.CoordinateYOrigin = (this._s1TokChart.CoordinateYMax + this._s1TokChart.CoordinateYMin) / 2;
                    this._S1maxCurrentY = this._S1maxCurrentY / 2;
                    this._S1minCurrentY = this._S1minCurrentY / 2;
                    int centr = 0;
                    if (this._S1maxY > 0 && this._S1minY > 0)
                    {
                        centr = (this._S1maxY - this._S1minY) / 2;
                    }
                    if (this._S1maxY < 0 && this._S1minY < 0)
                    {
                        centr = (this._S1maxY + this._S1minY) / 2;
                    }
                    if (this._S1maxY > 0 && this._S1minY < 0)
                    {
                        centr = -(this._S1maxY - this._S1minY) / 2;
                    }
                    this._s1TokChart.CoordinateYMax = this._s1TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S1clicCount + 1));
                    this._s1TokChart.CoordinateYMin = this._s1TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S1clicCount + 1));
                    this._S1chartStepY = this._s1TokChart.CoordinateYMax - this._s1TokChart.CoordinateYMin;
                    this._s1TokChart.CoordinateYOrigin = (this._s1TokChart.CoordinateYMax + this._s1TokChart.CoordinateYMin) / 2;
                    this.RefreshS1Points();
                    this._S1YStep = this.CalculateYStep(this._s1TokChart);
                    this.S1Scroll.Minimum -= this._s1TokChart.GridYTicker * (int)Math.Pow(2, this.S1clicCount);
                    this.S1Scroll.Value = (int)((this._S1maxY - this._s1TokChart.CoordinateYMax) / this._S1YStep + this.S1Scroll.Minimum);
                    this.S1Scroll.Refresh();
                    this.S1clicCount++;
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s1TokChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s1TokChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._s1TokChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.S1clicCount == 5)
                    {
                        this.S1YPlus.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void S1Yminus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.S1Scroll.Minimum != this._S1maxY && this.S1Scroll.Maximum != this._S1minY)
                {
                    this.S1YPlus.Enabled = true;
                    this.S1clicCount--;
                    this._S1maxCurrentY = this._S1maxCurrentY * 2;
                    this._S1minCurrentY = this._S1minCurrentY * 2;
                    int centr = 0;
                    if (this._S1maxY > 0 && this._S1minY > 0)
                    {
                        centr = (this._S1maxY - this._S1minY) / 2;
                    }
                    if (this._S1maxY < 0 && this._S1minY < 0)
                    {
                        centr = (this._S1maxY + this._S1minY) / 2;
                    }
                    if (this._S1maxY > 0 && this._S1minY < 0)
                    {
                        centr = -(this._S1maxY - this._S1minY) / 2;
                    }
                    if (this._s1TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S1clicCount)) <= this._S1maxY)
                    {
                        if (this._s1TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S1clicCount)) >= this._S1minY)
                        {
                            this._s1TokChart.CoordinateYMax = this._s1TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S1clicCount));//*= 2;
                            this._s1TokChart.CoordinateYMin = this._s1TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S1clicCount));//*= 2;
                        }
                        else
                        {
                            this._s1TokChart.CoordinateYMin = this._S1minY;
                            this._s1TokChart.CoordinateYMax = this._s1TokChart.CoordinateYMin + (Math.Abs(this._S1maxCurrentY) + Math.Abs(this._S1minCurrentY));
                        }
                    }
                    else
                    {
                        this._s1TokChart.CoordinateYMax = this._S1maxY;
                        this._s1TokChart.CoordinateYMin = this._s1TokChart.CoordinateYMax - (Math.Abs(this._S1maxCurrentY) + Math.Abs(this._S1minCurrentY));
                    }
                    this._S1chartStepY = this._s1TokChart.CoordinateYMax - this._s1TokChart.CoordinateYMin;
                    this._s1TokChart.CoordinateYOrigin = (this._s1TokChart.CoordinateYMax + this._s1TokChart.CoordinateYMin) / 2;
                    this.RefreshS1Points();
                    this._S1YStep = this.CalculateYStep(this._s1TokChart);

                    //if (S1clicCount != 0)
                    //{
                        try
                        {
                            this.S1Scroll.Minimum += this._s1TokChart.GridYTicker * (int)Math.Pow(2, this.S1clicCount);
                            this.S1Scroll.Value = (int)((this._S1maxY - this._s1TokChart.CoordinateYMax) / this._S1YStep + this.S1Scroll.Minimum);
                            this.S1Scroll.Refresh();
                        }
                        catch { }
                    //}
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s1TokChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s1TokChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._s1TokChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.S1clicCount == 0)
                    {
                        this.S1Yminus.Enabled = false;
                        this.S1Scroll.Visible = false;
                    }
                }
            }
            catch { }
        }

        int S2clicCount = 0;
        private void S2YPlus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.S2clicCount < 5)
                {
                    this.S2Yminus.Enabled = true;
                    this.S2Scroll.Visible = true;

                    this._s2TokChart.CoordinateYOrigin = (this._s2TokChart.CoordinateYMax + this._s2TokChart.CoordinateYMin) / 2;
                    this._S2maxCurrentY = this._S2maxCurrentY / 2;
                    this._S2minCurrentY = this._S2minCurrentY / 2;
                    int centr = 0;
                    if (this._S2maxY > 0 && this._S2minY > 0)
                    {
                        centr = (this._S2maxY - this._S2minY) / 2;
                    }
                    if (this._S2maxY < 0 && this._S2minY < 0)
                    {
                        centr = (this._S2maxY + this._S2minY) / 2;
                    }
                    if (this._S2maxY > 0 && this._S2minY < 0)
                    {
                        centr = -(this._S2maxY - this._S2minY) / 2;
                    }
                    this._s2TokChart.CoordinateYMax = this._s2TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S2clicCount + 1));
                    this._s2TokChart.CoordinateYMin = this._s2TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S2clicCount + 1));
                    this._S2chartStepY = this._s2TokChart.CoordinateYMax - this._s2TokChart.CoordinateYMin;
                    this._s2TokChart.CoordinateYOrigin = (this._s2TokChart.CoordinateYMax + this._s2TokChart.CoordinateYMin) / 2;
                    this.RefreshS2Points();
                    this._S2YStep = this.CalculateYStep(this._s2TokChart);

                    this.S2Scroll.Minimum -= this._s2TokChart.GridYTicker * (int)Math.Pow(2, this.S2clicCount);
                    this.S2clicCount++;
                    this.S2Scroll.Value = (int)((this._S2maxY - this._s2TokChart.CoordinateYMax) / this._S2YStep + this.S2Scroll.Minimum);
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s2TokChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s2TokChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._s2TokChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.S2clicCount == 5)
                    {
                        this.S2YPlus.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void S2Yminus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.S2Scroll.Minimum != this._S2maxY && this.S2Scroll.Maximum != this._S2minY)
                {
                    this.S2YPlus.Enabled = true;
                    this.S2clicCount--;
                    
                    this._S2maxCurrentY = this._S2maxCurrentY * 2;
                    this._S2minCurrentY = this._S2minCurrentY * 2;
                    int centr = 0;
                    if (this._S2maxY > 0 && this._S2minY > 0)
                    {
                        centr = (this._S2maxY - this._S2minY) / 2;
                    }
                    if (this._S2maxY < 0 && this._S2minY < 0)
                    {
                        centr = (this._S2maxY + this._S2minY) / 2;
                    }
                    if (this._S2maxY > 0 && this._S2minY < 0)
                    {
                        centr = -(this._S2maxY - this._S2minY) / 2;
                    }
                    if (this._s2TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S2clicCount)) <= this._S2maxY)
                    {
                        if (this._s2TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S2clicCount)) >= this._S2minY)
                        {
                            this._s2TokChart.CoordinateYMax = this._s2TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S2clicCount));//*= 2;
                            this._s2TokChart.CoordinateYMin = this._s2TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S2clicCount));//*= 2;
                        }
                        else
                        {
                            this._s2TokChart.CoordinateYMin = this._S2minY;
                            this._s2TokChart.CoordinateYMax = this._s2TokChart.CoordinateYMin + (Math.Abs(this._S2maxCurrentY) + Math.Abs(this._S2minCurrentY));
                        }
                    }
                    else
                    {
                        this._s2TokChart.CoordinateYMax = this._S2maxY;
                        this._s2TokChart.CoordinateYMin = this._s2TokChart.CoordinateYMax - (Math.Abs(this._S2maxCurrentY) + Math.Abs(this._S2minCurrentY));
                    }
                    this._S2chartStepY = this._s2TokChart.CoordinateYMax - this._s2TokChart.CoordinateYMin;
                    this._s2TokChart.CoordinateYOrigin = (this._s2TokChart.CoordinateYMax + this._s2TokChart.CoordinateYMin) / 2;
                    this.RefreshS2Points();
                    this._S2YStep = this.CalculateYStep(this._s2TokChart);

                    //if (S2clicCount != 0)
                    //{
                        this.S2Scroll.Minimum += this._s2TokChart.GridYTicker * (int)Math.Pow(2, this.S2clicCount);
                        this.S2Scroll.Value = (int)((this._S2maxY - this._s2TokChart.CoordinateYMax) / this._S2YStep + this.S2Scroll.Minimum);
                    //}
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s2TokChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s2TokChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._s2TokChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.S2clicCount == 0)
                    {
                        this.S2Yminus.Enabled = false;
                        this.S2Scroll.Visible = false;
                    }
                }
            }
            catch { }
        }

        int S3clicCount = 0;
        private void S3YPlus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.S3clicCount < 5)
                {
                    this.S3Yminus.Enabled = true;
                    this.S3Scroll.Visible = true;

                    this._s3TokChart.CoordinateYOrigin = (this._s3TokChart.CoordinateYMax + this._s3TokChart.CoordinateYMin) / 2;
                    this._S3maxCurrentY = this._S3maxCurrentY / 2;
                    this._S3minCurrentY = this._S3minCurrentY / 2;
                    int centr = 0;
                    if (this._S3maxY > 0 && this._S3minY > 0)
                    {
                        centr = (this._S3maxY - this._S3minY) / 2;
                    }
                    if (this._S3maxY < 0 && this._S3minY < 0)
                    {
                        centr = (this._S3maxY + this._S3minY) / 2;
                    }
                    if (this._S3maxY > 0 && this._S3minY < 0)
                    {
                        centr = -(this._S3maxY - this._S3minY) / 2;
                    }
                    this._s3TokChart.CoordinateYMax = this._s3TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S3clicCount+1));
                    this._s3TokChart.CoordinateYMin = this._s3TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S3clicCount+1));
                    this._S3chartStepY = this._s3TokChart.CoordinateYMax - this._s3TokChart.CoordinateYMin;
                    this._s3TokChart.CoordinateYOrigin = (this._s3TokChart.CoordinateYMax + this._s3TokChart.CoordinateYMin) / 2;
                    this.RefreshS3Points();
                    this._S3YStep = this.CalculateYStep(this._s3TokChart);

                    this.S3Scroll.Minimum -= this._s3TokChart.GridYTicker * (int)Math.Pow(2, this.S3clicCount);
                    this.S3clicCount++;
                    this.S3Scroll.Value = (int)((this._S3maxY - this._s3TokChart.CoordinateYMax) / this._S3YStep + this.S3Scroll.Minimum);
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s3TokChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s3TokChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._s3TokChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.S3clicCount == 5)
                    {
                        this.S3YPlus.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void S3Yminus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.S3Scroll.Minimum != this._S3maxY && this.S3Scroll.Maximum != this._S3minY)
                {
                    this.S3YPlus.Enabled = true;
                    this.S3clicCount--;
                    
                    this._S3maxCurrentY = this._S3maxCurrentY * 2;
                    this._S3minCurrentY = this._S3minCurrentY * 2;
                    int centr = 0;
                    if (this._S3maxY > 0 && this._S3minY > 0)
                    {
                        centr = (this._S3maxY - this._S3minY) / 2;
                    }
                    if (this._S3maxY < 0 && this._S3minY < 0)
                    {
                        centr = (this._S3maxY + this._S3minY) / 2;
                    }
                    if (this._S3maxY > 0 && this._S3minY < 0)
                    {
                        centr = -(this._S3maxY - this._S3minY) / 2;
                    }
                    if (this._s3TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S3clicCount)) <= this._S3maxY)
                    {
                        if (this._s3TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S3clicCount)) >= this._S3minY)
                        {
                            this._s3TokChart.CoordinateYMax = this._s3TokChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.S3clicCount));//*= 2;
                            this._s3TokChart.CoordinateYMin = this._s3TokChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.S3clicCount));//*= 2;
                        }
                        else
                        {
                            this._s3TokChart.CoordinateYMin = this._S3minY;
                            this._s3TokChart.CoordinateYMax = this._s3TokChart.CoordinateYMin + (Math.Abs(this._S3maxCurrentY) + Math.Abs(this._S3minCurrentY));
                        }
                    }
                    else
                    {
                        this._s3TokChart.CoordinateYMax = this._S3maxY;
                        this._s3TokChart.CoordinateYMin = this._s3TokChart.CoordinateYMax - (Math.Abs(this._S3maxCurrentY) + Math.Abs(this._S3minCurrentY));
                    }
                    this._S3chartStepY = this._s3TokChart.CoordinateYMax - this._s3TokChart.CoordinateYMin;
                    this._s3TokChart.CoordinateYOrigin = (this._s3TokChart.CoordinateYMax + this._s3TokChart.CoordinateYMin) / 2;
                    this.RefreshS3Points();
                    this._S3YStep = this.CalculateYStep(this._s3TokChart);

                    //if (S3clicCount != 0)
                    //{
                        this.S3Scroll.Minimum += this._s3TokChart.GridYTicker * (int)Math.Pow(2, this.S3clicCount);
                        this.S3Scroll.Value = (int)((this._S3maxY - this._s3TokChart.CoordinateYMax) / this._S3YStep + this.S3Scroll.Minimum);
                    //}
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s3TokChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._s3TokChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._s3TokChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.S3clicCount == 0)
                    {
                        this.S3Yminus.Enabled = false;
                        this.S3Scroll.Visible = false;
                    }
                }
            }
            catch { }
        }

        int UclicCount = 0;
        private void UYPlus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.UclicCount < 5)
                {
                    this.UYminus.Enabled = true;
                    this.UScroll.Visible = true;

                    this._voltageChart.CoordinateYOrigin = (this._voltageChart.CoordinateYMax + this._voltageChart.CoordinateYMin) / 2;
                    this._UmaxCurrentY = this._UmaxCurrentY / 2;
                    this._UminCurrentY = this._UminCurrentY / 2;
                    int centr = 0;
                    if (this._UmaxY > 0 && this._UminY > 0)
                    {
                        centr = (this._UmaxY - this._UminY) / 2;
                    }
                    if (this._UmaxY < 0 && this._UminY < 0)
                    {
                        centr = (this._UmaxY + this._UminY) / 2;
                    }
                    if (this._UmaxY > 0 && this._UminY < 0)
                    {
                        centr = -(this._UmaxY - this._UminY) / 2;
                    }
                    this._voltageChart.CoordinateYMax = this._voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.UclicCount + 1));
                    this._voltageChart.CoordinateYMin = this._voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.UclicCount + 1));
                    this._UchartStepY = this._voltageChart.CoordinateYMax - this._voltageChart.CoordinateYMin;
                    this._voltageChart.CoordinateYOrigin = (this._voltageChart.CoordinateYMax + this._voltageChart.CoordinateYMin) / 2;
                    this.RefreshUPoints();
                    this._UYStep = this.CalculateYStep(this._voltageChart);

                    this.UScroll.Minimum -= this._voltageChart.GridYTicker * (int)Math.Pow(2, this.UclicCount);
                    this.UclicCount++;
                    this.UScroll.Value = (int)((this._UmaxY - this._voltageChart.CoordinateYMax) / this._UYStep + this.UScroll.Minimum);
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._voltageChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._voltageChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._voltageChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.UclicCount == 5)
                    {
                        this.UYPlus.Enabled = false;
                    }
                }
            }
            catch { }
        }

        private void UYminus_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.UScroll.Minimum != this._UmaxY && this.UScroll.Maximum != this._UminY)
                {
                    this.UYPlus.Enabled = true;
                    this.UclicCount--;
                    
                    this._UmaxCurrentY = this._UmaxCurrentY * 2;
                    this._UminCurrentY = this._UminCurrentY * 2;
                    int centr = 0;
                    if (this._UmaxY > 0 && this._UminY > 0)
                    {
                        centr = (this._UmaxY - this._UminY) / 2;
                    }
                    if (this._UmaxY < 0 && this._UminY < 0)
                    {
                        centr = (this._UmaxY + this._UminY) / 2;
                    }
                    if (this._UmaxY > 0 && this._UminY < 0)
                    {
                        centr = -(this._UmaxY - this._UminY) / 2;
                    }
                    if (this._voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.UclicCount)) <= this._UmaxY)
                    {
                        if (this._voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.UclicCount)) >= this._UminY)
                        {
                            this._voltageChart.CoordinateYMax = this._voltageChart.CoordinateYOrigin + Math.Abs(centr / (int)Math.Pow(2, this.UclicCount));//*= 2;
                            this._voltageChart.CoordinateYMin = this._voltageChart.CoordinateYOrigin - Math.Abs(centr / (int)Math.Pow(2, this.UclicCount));//*= 2;
                        }
                        else
                        {
                            this._voltageChart.CoordinateYMin = this._UminY;
                            this._voltageChart.CoordinateYMax = this._voltageChart.CoordinateYMin + (Math.Abs(this._UmaxCurrentY) + Math.Abs(this.UclicCount));
                        }
                    }
                    else
                    {
                        this._voltageChart.CoordinateYMax = this._UmaxY;
                        this._voltageChart.CoordinateYMin = this._voltageChart.CoordinateYMax - (Math.Abs(this._UmaxCurrentY) + Math.Abs(this.UclicCount));
                    }
                    this._UchartStepY = this._voltageChart.CoordinateYMax - this._voltageChart.CoordinateYMin;
                    this._voltageChart.CoordinateYOrigin = (this._voltageChart.CoordinateYMax + this._voltageChart.CoordinateYMin) / 2;
                    this.RefreshUPoints();
                    this._UYStep = this.CalculateYStep(this._voltageChart);

                    //if (UclicCount != 0)
                    //{
                        this.UScroll.Minimum += this._voltageChart.GridYTicker * (int)Math.Pow(2, this.UclicCount);
                        this.UScroll.Value = (int)((this._UmaxY - this._voltageChart.CoordinateYMax) / this._UYStep + this.UScroll.Minimum);
                    //}
                    if (this.Marker1CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._voltageChart, "������1", this.Marker1TrackBar.Value);
                    }
                    if (this.Marker2CB.Checked && this.MarkerCB.Checked)
                    {
                        this.UpdateLinePoints(this._voltageChart, "������2", this.Marker2TrackBar.Value);
                    }
                    if (this._avaryJournalCheck.Checked)
                    {
                        for (int i = 0; i < this._avaryBeginList.Count; i++)
                        {
                            this.UpdateLinePoints(this._voltageChart, "������ " + i, this._avaryBeginList[i]);
                        }
                    }
                    if (this.UclicCount == 0)
                    {
                        this.UYminus.Enabled = false;
                        this.UScroll.Visible = false;
                    }
                }
            }
            catch { }
        }

        int XclicCount = 0;
        private void Xplus_Click(object sender, EventArgs e)
        {
            try
            {
                this.Xminus.Enabled = true;
                this.XclicCount--;
                this.hScrollBar4.Minimum += this._s1TokChart.GridXTicker * (int)Math.Pow(2, this.XclicCount);//(int)(_maxX / (_maxCurrentX * 2));

                int centr = this._maxX / 2;
                double max = this._s1TokChart.CoordinateXOrigin + Math.Abs(centr / (int)Math.Pow(2, this.XclicCount));
                double min = this._s1TokChart.CoordinateXOrigin - Math.Abs(centr / (int)Math.Pow(2, this.XclicCount));

                if (min < 0)
                {
                    max = Math.Abs(min) + Math.Abs(max);
                    min = 0;
                }
                if (max > this._maxX)
                {
                    min = this._maxX - (max - min);
                    max = this._maxX;
                }

                this._s1TokChart.XMax = max;
                this._s1TokChart.XMin = min;
                this._s2TokChart.XMax = max;
                this._s2TokChart.XMin = min;
                this._s3TokChart.XMax = max;
                this._s3TokChart.XMin = min;
                this._voltageChart.XMax = max;
                this._voltageChart.XMin = min;
                this._discretChart.XMax = max;
                this._discretChart.XMin = min;

                if (this.XclicCount == 0)
                {
                    this.Xplus.Enabled = false;
                    this.hScrollBar4.Visible = false;
                }
                try
                {

                    this._s1TokChart.CoordinateXOrigin = (this._s1TokChart.XMin + this._s1TokChart.XMax) / 2;
                    this._s2TokChart.CoordinateXOrigin = (this._s2TokChart.XMin + this._s2TokChart.XMax) / 2;
                    this._s3TokChart.CoordinateXOrigin = (this._s3TokChart.XMin + this._s3TokChart.XMax) / 2;
                    this._voltageChart.CoordinateXOrigin = (this._voltageChart.XMin + this._voltageChart.XMax) / 2;
                    this._discretChart.CoordinateXOrigin = (this._discretChart.XMin + this._discretChart.XMax) / 2;
                }
                catch { }
                this._maxCurrentX = this._s1TokChart.XMax;
                this._xStep = this.CalculateXStep(this._s1TokChart);
                try
                {
                    this.hScrollBar4.Value = (int)(this._s1TokChart.XMin / this._xStep + this.hScrollBar4.Minimum);//= hScrollBar4.Minimum;
                }
                catch { }
                this.RefreshAllOsc();
            }
            catch { }
        }

        private void Xminus_Click(object sender, EventArgs e)
        {
            try
            {
                this.hScrollBar4.Visible = true;
                this.hScrollBar4.Minimum -= this._s1TokChart.GridXTicker * (int)Math.Pow(2, this.XclicCount);
                this.XclicCount++;

                int centr = this._maxX / 2;
                double max = this._s1TokChart.CoordinateXOrigin + Math.Abs(centr / (int)Math.Pow(2, this.XclicCount));
                double min = this._s1TokChart.CoordinateXOrigin - Math.Abs(centr / (int)Math.Pow(2, this.XclicCount));

                if (min < 0)
                {
                    max = Math.Abs(min) + Math.Abs(max);
                    min = 0;
                }
                if (max > this._maxX)
                {
                    min = this._maxX - (max - min);
                    max = this._maxX;
                }

                this._s1TokChart.XMax = max;
                this._s1TokChart.XMin = min;
                this._s2TokChart.XMax = max;
                this._s2TokChart.XMin = min;
                this._s3TokChart.XMax = max;
                this._s3TokChart.XMin = min;
                this._voltageChart.XMax = max;
                this._voltageChart.XMin = min;
                this._discretChart.XMax = max;
                this._discretChart.XMin = min;

                this.Xplus.Enabled = true;
                this._s1TokChart.CoordinateXOrigin = (this._s1TokChart.XMin + this._s1TokChart.XMax) / 2;
                this._s2TokChart.CoordinateXOrigin = (this._s2TokChart.XMin + this._s2TokChart.XMax) / 2;
                this._s3TokChart.CoordinateXOrigin = (this._s3TokChart.XMin + this._s3TokChart.XMax) / 2;
                this._voltageChart.CoordinateXOrigin = (this._voltageChart.XMin + this._voltageChart.XMax) / 2;
                this._discretChart.CoordinateXOrigin = (this._discretChart.XMin + this._discretChart.XMax) / 2;
                this._maxCurrentX = this._s1TokChart.XMax;

                this._xStep = this.CalculateXStep(this._s1TokChart);
                if (this._xStep <= 10)
                {
                    this.Xminus.Enabled = false;
                }
                try
                {
                    this.hScrollBar4.Value = (int)(this._s1TokChart.XMin / this._xStep + this.hScrollBar4.Minimum);
                }
                catch { }
                this.RefreshAllOsc();
            }
            catch { }
        }

        private void _avaryOscCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._avaryOscCheck.Checked)
            {
                for (int i = 0; i < this._avaryBeginList.Count; i++)
                {
                    this.AddLineCurves(this._s1TokChart, "������ " + i, Color.Aqua, 2);
                    this.AddLinePoints(this._s1TokChart, "������ " + i, this._avaryBeginList[i]);
                    this.AddLineCurves(this._s2TokChart, "������ " + i, Color.Aqua, 2);
                    this.AddLinePoints(this._s2TokChart, "������ " + i, this._avaryBeginList[i]);
                    this.AddLineCurves(this._s3TokChart, "������ " + i, Color.Aqua, 2);
                    this.AddLinePoints(this._s3TokChart, "������ " + i, this._avaryBeginList[i]);
                    this.AddLineCurves(this._voltageChart, "������ " + i, Color.Aqua, 2);
                    this.AddLinePoints(this._voltageChart, "������ " + i, this._avaryBeginList[i]);
                    this.AddLineCurves(this._discretChart, "������ " + i, Color.Aqua, 2);
                    this.AddLinePoints(this._discretChart, "������ " + i, this._avaryBeginList[i]);
                }
            }
            else
            {
                for (int i = 0; i < this._avaryBeginList.Count; i++)
                {
                    this._s1TokChart.ResetCurve("������ " + i);
                    this._s2TokChart.ResetCurve("������ " + i);
                    this._s3TokChart.ResetCurve("������ " + i);
                    this._voltageChart.ResetCurve("������ " + i);
                    this._discretChart.ResetCurve("������ " + i);
                }
            }
        }

        private void S1ShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.S1ShowCB.Checked)
            {
                this.tableLayoutPanel13.Visible = true;
                this.splitter1.Visible = true;
                if (this.Marker1CB.Checked)
                {
                    this.LabelResults(this.M1S1Labels, this.IChanals, this.S1Values, this.Marker1TrackBar.Value, 0);
                }
                if (this.Marker2CB.Checked)
                {
                    this.LabelResults(this.M2S1Labels, this.IChanals, this.S1Values, this.Marker2TrackBar.Value, 0);
                }
            }
            else 
            {
                this.splitter1.SplitPosition = this.splitter1.MinExtra;
                this.tableLayoutPanel13.Visible = false;
                this.splitter1.Visible = false;
                this.LabelReset(this.M1S1Labels);

                this.LabelReset(this.M2S1Labels);

                this.panel3.Update();
            }
            this.RefreshAllOsc();
        }

        private void S2ShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.S2ShowCB.Checked)
            {
                this.tableLayoutPanel14.Visible = true;
                this.splitter2.Visible = true;
                if (this.Marker1CB.Checked)
                {
                    this.LabelResults(this.M1S2Labels, this.IChanals, this.S2Values, this.Marker1TrackBar.Value, 1);
                }
                if (this.Marker2CB.Checked)
                {
                    this.LabelResults(this.M2S2Labels, this.IChanals, this.S2Values, this.Marker2TrackBar.Value, 1);
                }
            }
            else
            {
                this.splitter2.SplitPosition = this.splitter2.MinExtra;
                this.tableLayoutPanel14.Visible = false;
                this.splitter2.Visible = false;
                this.LabelReset(this.M1S2Labels);

                this.LabelReset(this.M2S2Labels);

                this.panel3.Update();
            }
            this.RefreshAllOsc();
        }

        private void S3ShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.S3ShowCB.Checked)
            {
                this.tableLayoutPanel15.Visible = true;
                this.splitter3.Visible = true;
                if (this.Marker1CB.Checked)
                {
                    this.LabelResults(this.M1S3Labels, this.IChanals, this.S3Values, this.Marker1TrackBar.Value, 2);
                }
                if (this.Marker2CB.Checked)
                {
                    this.LabelResults(this.M2S3Labels, this.IChanals, this.S3Values, this.Marker2TrackBar.Value, 2);
                }
            }
            else
            {
                this.splitter3.SplitPosition = this.splitter3.MinExtra;
                this.tableLayoutPanel15.Visible = false;
                this.splitter3.Visible = false;
                this.LabelReset(this.M1S3Labels);

                this.LabelReset(this.M2S3Labels);

                this.panel3.Update();
            }
            this.RefreshAllOsc();
        }

        private void UShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.UShowCB.Checked)
            {
                this.tableLayoutPanel16.Visible = true;
                this.splitter4.Visible = true;
                if (this.Marker1CB.Checked)
                {
                    this.LabelResults(this.M1ULabels, this.UChanals, this.UValues, this.Marker1TrackBar.Value, 3);
                }
                if (this.Marker2CB.Checked)
                {
                    this.LabelResults(this.M2ULabels, this.UChanals, this.UValues, this.Marker2TrackBar.Value, 3);
                }
            }
            else
            {
                this.splitter4.SplitPosition = this.splitter4.MinExtra;
                this.tableLayoutPanel16.Visible = false;
                this.splitter4.Visible = false;
                this.LabelReset(this.M1ULabels);

                this.LabelReset(this.M2ULabels);

                this.panel3.Update();
            }
            this.RefreshAllOsc();
        }

        private void DiscretShowCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.DiscretShowCB.Checked)
            {
                this.tableLayoutPanel2.Visible = true;
                this.splitter5.Visible = true;
                if (this.Marker1CB.Checked)
                {
                    this.LabelResults(this.M1DLabels, this.DChanals, this._oscObject.DiskretData, this.Marker1TrackBar.Value);
                }
                if (this.Marker2CB.Checked)
                {
                    this.LabelResults(this.M2DLabels, this.DChanals, this._oscObject.DiskretData, this.Marker2TrackBar.Value);
                }
            }
            else
            {
                this.splitter5.SplitPosition = this.splitter5.MinExtra;
                this.tableLayoutPanel2.Visible = false;
                this.splitter5.Visible = false;
                this.LabelReset(this.M1DLabels);

                this.LabelReset(this.M2DLabels);

                this.panel3.Update();
            }
            this.RefreshAllOsc();
        }

        private List<ToolTip> _tolTips = new List<ToolTip>();
        private void _Chart_MouseClick(object sender, MouseEventArgs e)
        {
            if (this.FlagCB.Checked)
            {
                try
                {
                    BEMN_XY_Chart.DAS_Net_XYChart _chart = (BEMN_XY_Chart.DAS_Net_XYChart)sender;

                    if (e.Button == MouseButtons.Left)
                    {
                        this.toolTip1 = new ToolTip();
                        this.toolTip1.ShowAlways = true;
                        double _length = Math.Abs(_chart.XMax) + Math.Abs(_chart.XMin);
                        double x = _length / _chart.Width * e.X + _chart.XMin;
                        if (e.X < Math.Abs(_chart.XMin) && _chart.XMin < 0)
                        {
                            x = Math.Round(_length / _chart.Width * (e.X + 1), 2) + _chart.XMin;
                        }
                        this.toolTip1.Show(Math.Round(x).ToString() + " +/-" + Math.Round(_length / _chart.Width).ToString() + "��", _chart, e.X, e.Y);
                        this._tolTips.Add(this.toolTip1);
                    }
                    else
                    {
                        for (int i = 0; i < this._tolTips.Count; i++)
                            this._tolTips[i].RemoveAll();
                    }
                }
                catch { }
            }
        }

        private void MarkerCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.MarkerCB.Checked)
            {
                this.MarkersTable.Visible = true;
                this.MarkersTable.Update();
                this.Marker1TrackBar.Maximum = this._maxX;
                this.Marker1TrackBar.Update();
                this.Marker2TrackBar.Maximum = this._maxX;
                this.Marker2TrackBar.Update();
                this.MAINTABLE.ColumnStyles[1].Width = 250;
                this.MAINTABLE.Update();
                this._s1TokChart.Update();
                this._s2TokChart.Update();
                this._s3TokChart.Update();
                this._voltageChart.Update();
                this._discretChart.Update();
            }
            else
            {
                this.MarkersTable.Visible = false;
                this.MarkersTable.Update();
                this.Marker1CB.Checked = false;
                this.Marker2CB.Checked = false;
                this._s1TokChart.ResetCurve("������1");
                this._s1TokChart.ResetCurve("������2");
                this._s2TokChart.ResetCurve("������1");
                this._s2TokChart.ResetCurve("������2");
                this._s3TokChart.ResetCurve("������1");
                this._s3TokChart.ResetCurve("������2");
                this._voltageChart.ResetCurve("������1");
                this._voltageChart.ResetCurve("������2");
                this._discretChart.ResetCurve("������1");
                this._discretChart.ResetCurve("������2");
                this.MAINTABLE.ColumnStyles[1].Width = 0;
                this.MAINTABLE.Update();
            }
        }

        private void LabelResults(Label[] labels, string[] chanels, List<double[]> values, int index, int box)
        {
            try
            {
                if (this.CheckBoxes[box].Checked)
                {
                    for (int i = 0; i < labels.Length; i++)
                    {
                        labels[i].Text = chanels[i] + " = " + values[i][index];
                        labels[i].Update();
                    }
                }
            }
            catch { }
        }

        private void LabelReset(Label[] labels)
        {
            try
            {
                for (int i = 0; i < labels.Length; i++)
                {
                    labels[i].Text = " ";
                    labels[i].Update();
                }
            }
            catch { }
        }

        private void LabelResults(Label[] labels, string[] chanels, List<BitArray> values, int index)
        {
            try
            {
                if (this.DiscretShowCB.Checked)
                {
                    for (int i = 0; i < values.Count; i++)
                    {
                        labels[i].Text = chanels[i] + " = " + this.BoolToInt(values[i][index]);
                    }
                }
            }
            catch { }
        }

        public int BoolToInt(bool val) 
        {
            if (val)
                return 1;
            else
                return 0;
        }

        private void Marker1TrackBar_Scroll(object sender, EventArgs e)
        {
            if (this.Marker1CB.Checked)
            {
                this.LabelResults(this.M1S1Labels, this.IChanals, this.S1Values, this.Marker1TrackBar.Value,0);

                this.LabelResults(this.M1S2Labels, this.IChanals, this.S2Values, this.Marker1TrackBar.Value,1);

                this.LabelResults(this.M1S3Labels, this.IChanals, this.S3Values, this.Marker1TrackBar.Value,2);

                this.LabelResults(this.M1ULabels, this.UChanals, this.UValues, this.Marker1TrackBar.Value,3);
                try
                {
                    this.LabelResults(this.M1DLabels, this.DChanals, this._oscObject.DiskretData, this.Marker1TrackBar.Value);
                }
                catch { }
                this.panel3.Update();

                this.UpdateLinePoints(this._s1TokChart, "������1", this.Marker1TrackBar.Value);
                //_s1TokChart.Update();
                this._s1TokChart.Refresh();
                this.UpdateLinePoints(this._s2TokChart, "������1", this.Marker1TrackBar.Value);
                //_s2TokChart.Update();
                this._s2TokChart.Refresh();
                this.UpdateLinePoints(this._s3TokChart, "������1", this.Marker1TrackBar.Value);
                //_s3TokChart.Update();
                this._s3TokChart.Refresh();
                this.UpdateLinePoints(this._voltageChart, "������1", this.Marker1TrackBar.Value);
                //_voltageChart.Update();
                this._voltageChart.Refresh();
                this.UpdateLinePoints(this._discretChart, "������1", this.Marker1TrackBar.Value);
                //_discretChart.Update();
                this._discretChart.Refresh();
                this.TimeMarker1.Text = this.Marker1TrackBar.Value + " ��.";
                //TimeMarker1.Update();
                this.TimeMarker1.Refresh();
            }
            if (this.Marker1CB.Checked && this.Marker2CB.Checked)
            {
                this.DeltaLabel.Text = Math.Abs(this.Marker1TrackBar.Value - this.Marker2TrackBar.Value) + " ��.";
                this.DeltaLabel.Update();
            }
        }

        private void Marker2TrackBar_Scroll(object sender, EventArgs e)
        {
            if (this.Marker2CB.Checked)
            {
                this.LabelResults(this.M2S1Labels, this.IChanals, this.S1Values, this.Marker2TrackBar.Value,0);

                this.LabelResults(this.M2S2Labels, this.IChanals, this.S2Values, this.Marker2TrackBar.Value,1);

                this.LabelResults(this.M2S3Labels, this.IChanals, this.S3Values, this.Marker2TrackBar.Value,2);

                this.LabelResults(this.M2ULabels, this.UChanals, this.UValues, this.Marker2TrackBar.Value,3);
                try
                {
                    this.LabelResults(this.M2DLabels, this.DChanals, this._oscObject.DiskretData, this.Marker2TrackBar.Value);
                }
                catch { }
                this.panel3.Update();

                this.UpdateLinePoints(this._s1TokChart, "������2", this.Marker2TrackBar.Value);
                this._s1TokChart.Update();
                this.UpdateLinePoints(this._s2TokChart, "������2", this.Marker2TrackBar.Value);
                this._s2TokChart.Update();
                this.UpdateLinePoints(this._s3TokChart, "������2", this.Marker2TrackBar.Value);
                this._s3TokChart.Update();
                this.UpdateLinePoints(this._voltageChart, "������2", this.Marker2TrackBar.Value);
                this._voltageChart.Update();
                this.UpdateLinePoints(this._discretChart, "������2", this.Marker2TrackBar.Value);
                this._discretChart.Update();
                this.TimeMarker2.Text = this.Marker2TrackBar.Value + " ��.";
                this.TimeMarker2.Update();
            }
            if (this.Marker1CB.Checked && this.Marker2CB.Checked)
            {
                this.DeltaLabel.Text = Math.Abs(this.Marker1TrackBar.Value - this.Marker2TrackBar.Value) + " ��.";
                this.DeltaLabel.Update();
            }
        }

        private void Marker1CB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.Marker1CB.Checked)
            {
                this.LabelResults(this.M1S1Labels, this.IChanals, this.S1Values, this.Marker1TrackBar.Value,0);

                this.LabelResults(this.M1S2Labels, this.IChanals, this.S2Values, this.Marker1TrackBar.Value,1);

                this.LabelResults(this.M1S3Labels, this.IChanals, this.S3Values, this.Marker1TrackBar.Value,2);

                this.LabelResults(this.M1ULabels, this.UChanals, this.UValues, this.Marker1TrackBar.Value,3);

                try
                {
                    this.LabelResults(this.M1DLabels, this.DChanals, this._oscObject.DiskretData, this.Marker1TrackBar.Value);
                }
                catch { }
                this.AddLineCurves(this._s1TokChart, "������1", Color.Black, 1);
                this.AddLineCurves(this._s2TokChart, "������1", Color.Black, 1);
                this.AddLineCurves(this._s3TokChart, "������1", Color.Black, 1);
                this.AddLineCurves(this._voltageChart, "������1", Color.Black, 1);
                this.AddLineCurves(this._discretChart, "������1", Color.Black, 1);
                this.AddLinePoints(this._s1TokChart, "������1", this.Marker1TrackBar.Value);
                this.AddLinePoints(this._s2TokChart, "������1", this.Marker1TrackBar.Value);
                this.AddLinePoints(this._s3TokChart, "������1", this.Marker1TrackBar.Value);
                this.AddLinePoints(this._voltageChart, "������1", this.Marker1TrackBar.Value);
                this.AddLinePoints(this._discretChart, "������1", this.Marker1TrackBar.Value);
                this.TimeMarker1.Text = this.Marker1TrackBar.Value + " ��.";
                this.TimeMarker1.Update();
                if (this.Marker2CB.Checked)
                {
                    this.DeltaLabel.Text = Math.Abs(this.Marker1TrackBar.Value - this.Marker2TrackBar.Value) + " ��.";
                    this.DeltaLabel.Update();
                }
            }
            else 
            {
                this.LabelReset(this.M1S1Labels);

                this.LabelReset(this.M1S2Labels);

                this.LabelReset(this.M1S3Labels);

                this.LabelReset(this.M1ULabels);

                this.LabelReset(this.M1DLabels);

                this._s1TokChart.ResetCurve("������1");
                this._s2TokChart.ResetCurve("������1");
                this._s3TokChart.ResetCurve("������1");
                this._voltageChart.ResetCurve("������1");
                this._discretChart.ResetCurve("������1");
                this.TimeMarker1.Text = "";
                this.TimeMarker1.Update();
                this.DeltaLabel.Text = "";
                this.DeltaLabel.Update();
            }
        }

        private void Marker2CB_CheckedChanged(object sender, EventArgs e)
        {
            if (this.Marker2CB.Checked)
            {
                this.LabelResults(this.M2S1Labels, this.IChanals, this.S1Values, this.Marker2TrackBar.Value,0);

                this.LabelResults(this.M2S2Labels, this.IChanals, this.S2Values, this.Marker2TrackBar.Value,1);

                this.LabelResults(this.M2S3Labels, this.IChanals, this.S3Values, this.Marker2TrackBar.Value,2);

                this.LabelResults(this.M2ULabels, this.UChanals, this.UValues, this.Marker2TrackBar.Value,3);
                try
                {
                    this.LabelResults(this.M2DLabels, this.DChanals, this._oscObject.DiskretData, this.Marker2TrackBar.Value);
                }
                catch { }
                this.AddLineCurves(this._s1TokChart, "������2", Color.White, 1);
                this.AddLineCurves(this._s2TokChart, "������2", Color.White, 1);
                this.AddLineCurves(this._s3TokChart, "������2", Color.White, 1);
                this.AddLineCurves(this._voltageChart, "������2", Color.White, 1);
                this.AddLineCurves(this._discretChart, "������2", Color.White, 1);
                this.AddLinePoints(this._s1TokChart, "������2", this.Marker2TrackBar.Value);
                this.AddLinePoints(this._s2TokChart, "������2", this.Marker2TrackBar.Value);
                this.AddLinePoints(this._s3TokChart, "������2", this.Marker2TrackBar.Value);
                this.AddLinePoints(this._voltageChart, "������2", this.Marker2TrackBar.Value);
                this.AddLinePoints(this._discretChart, "������2", this.Marker2TrackBar.Value);
                this.TimeMarker2.Text = this.Marker2TrackBar.Value + " ��.";
                this.TimeMarker2.Update();
                if (this.Marker1CB.Checked) 
                {
                    this.DeltaLabel.Text = Math.Abs(this.Marker1TrackBar.Value - this.Marker2TrackBar.Value) + " ��.";
                    this.DeltaLabel.Update();
                }
            }
            else
            {
                this.LabelReset(this.M2S1Labels);

                this.LabelReset(this.M2S2Labels);

                this.LabelReset(this.M2S3Labels);

                this.LabelReset(this.M2ULabels);

                this.LabelReset(this.M2DLabels);

                this._s1TokChart.ResetCurve("������2");
                this._s2TokChart.ResetCurve("������2");
                this._s3TokChart.ResetCurve("������2");
                this._voltageChart.ResetCurve("������2");
                this._discretChart.ResetCurve("������2");
                this.TimeMarker2.Text = "";
                this.TimeMarker2.Update();
                this.DeltaLabel.Text = "";
                this.DeltaLabel.Update();
            }
        }
    }
}