using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;


namespace BEMN.UDZT
{
    public partial class AlarmJournalForm : Form, IFormView
    {
        private UDZT _device;
        private bool _measureTrLoaded;
        private bool _canLoad;
        private DataTable _table;
        private const string MR801_FILE_XML_PATH = "\\MR801\\��801_������_������_�2_060219.xml";

        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(UDZT device)
        {
            this.InitializeComponent();
            this._device = device;

            this._device.MeasureTransLoadOK += HandlerHelper.CreateHandler(this, () => this._measureTrLoaded = true);
            this._device.PowerTransLoadOK += HandlerHelper.CreateHandler(this, this._device_PowerTransLoadOK);
            this._device.AlarmJournalSaveOK += HandlerHelper.CreateHandler(this, this._device.LoadAlarmJournal);
            this._device.AlarmJournalSaveFail += HandlerHelper.CreateHandler(this, this.OnAlarmJournalRecordLoadFail);
            this._device.AlarmJournalLoadOK += HandlerHelper.CreateHandler(this, this.AlarmJournalLoadOk);
            this._device.AlarmJournalLoadFail += HandlerHelper.CreateHandler(this, this.OnAlarmJournalRecordLoadFail);
        }

        private void OnAlarmJournalLoad()
        {
            this._readBut.Enabled = true;
            MessageBox.Show(this._journalGrid.Rows.Count != 0 ? "������ ������ ��������" : "������ ������ ����");
        }

        private void OnAlarmJournalRecordLoadFail()
        {
            this._journalGrid.Rows.Add(this._device.AlarmJournalPageIndex, "������", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            this._journalCntLabel.Text = this._device.AlarmJournalPageIndex.ToString();
            this._journalCntLabel.Invalidate();
        }

        private void OnAlarmJournalRecordLoadOk()
        {
            this._table.Rows.Add(
                this._device.AlarmJournalPageIndex, 
                this._device.AlarmJournalRecTime,
                this._device.AlarmJournalMessage,
                this._device.AlarmJournalDifIndex,
                this._device.AlarmJournalParam,
                this._device.AlarmJournalParamValue, 
                this._device.AlarmJournalGroupUst,
                this._device.AlarmJournalIDa,
                this._device.AlarmJournalIDb, 
                this._device.AlarmJournalIDc, 
                this._device.AlarmJournalITa,
                this._device.AlarmJournalITb,
                this._device.AlarmJournalITc,
                this._device.AlarmJournalIs1a,
                this._device.AlarmJournalIs1b, 
                this._device.AlarmJournalIs1c,
                this._device.AlarmJournalIs2a,
                this._device.AlarmJournalIs2b, 
                this._device.AlarmJournalIs2c, 
                this._device.AlarmJournalIs3a,
                this._device.AlarmJournalIs3b, 
                this._device.AlarmJournalIs3c, 
                this._device.AlarmJournalIs1n,
                this._device.AlarmJournalIs10, 
                this._device.AlarmJournalIs12,
                this._device.AlarmJournalIs2n,
                this._device.AlarmJournalIs20, 
                this._device.AlarmJournalIs22, 
                this._device.AlarmJournalIs3n,
                this._device.AlarmJournalIs30, 
                this._device.AlarmJournalIs32, 
                this._device.AlarmJournalIs1d0,
                this._device.AlarmJournalIs2d0, 
                this._device.AlarmJournalIs3d0,
                this._device.AlarmJournalIs1T0,
                this._device.AlarmJournalIs2T0,
                this._device.AlarmJournalIs3T0, 
                this._device.AlarmJournalUa,
                this._device.AlarmJournalUb,
                this._device.AlarmJournalUc,
                this._device.AlarmJournalUab,
                this._device.AlarmJournalUbc, 
                this._device.AlarmJournalUca, 
                this._device.AlarmJournalUn,
                this._device.AlarmJournalU0, 
                this._device.AlarmJournalU2, 
                this._device.AlarmJournalF,
                this._device.AlarmJournalD1, 
                this._device.AlarmJournalD2,
                this._device.AlarmJournalD3);
            this._journalCntLabel.Text = this._device.AlarmJournalPageIndex.ToString();
        }
        
        private void _serializeBut_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveAlarmJournalDlg.ShowDialog())
            {
               this._table.WriteXml(this._saveAlarmJournalDlg.FileName);
            }
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.OK == this._openAlarmJounralDlg.ShowDialog())
                {
                    this._table.Rows.Clear();
                    if (Path.GetExtension(this._openAlarmJounralDlg.FileName).ToLower().Contains("bin"))
                    {
                        byte[] file = File.ReadAllBytes(this._openAlarmJounralDlg.FileName);
                        ushort[] buffer = Common.TOWORDS(file, true);
                        ushort[] data = Common.TOWORDS(file, true).Take(buffer.Length - 8).ToArray();
                        ushort[] transformatorsData = Common.TOWORDS(file, true).Skip(buffer.Length - 8).ToArray();
                        this._device.PowerTransformator.Value[0] = transformatorsData[0];
                        this._device.PowerTransformator.Value[1] = transformatorsData[1];
                        this._device.PowerTransformator.Value[6] = transformatorsData[2];
                        this._device.PowerTransformator.Value[7] = transformatorsData[3];
                        this._device.PowerTransformator.Value[12] = transformatorsData[4];
                        this._device.PowerTransformator.Value[13] = transformatorsData[5];

                        this._device.TH_L = Common.GetBits(transformatorsData[6], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                            12, 13, 14);
                        this._device.TH_LKoef = Common.GetBit(transformatorsData[6], 15) ? 1000 : 1;
                        this._device.TH_X = Common.GetBits(transformatorsData[7], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                            12, 13, 14);
                        this._device.TH_XKoef = Common.GetBit(transformatorsData[7], 15) ? 1000 : 1;

                        if (data.Length % 54 != 0)
                        {
                            this._journalCntLabel.Text = "���� �� ����� ���� ��������";
                            return;
                        }

                        ushort[] dataRecord = new ushort[54];
                        int index = 0;
                        int countRecord = 1;
                        try
                        {
                            while (index < data.Length)
                            {
                                Array.ConstrainedCopy(data, index, dataRecord, 0, 54);
                                this._device.SetAlaramJurnalSlotValue(dataRecord);
                                this._device.AlarmJournalPageIndex = countRecord;
                                this.OnAlarmJournalRecordLoadOk();
                                index = index + 54;
                                countRecord++;
                            }
                        }
                        catch (Exception)
                        {
                            this._journalCntLabel.Text = "�� ������� ��������� ����";
                        }
                    }
                    else
                    {
                        this._table.ReadXml(this._openAlarmJounralDlg.FileName);

                        this._journalGrid.Update();
                        this._journalCntLabel.Text = this._table.Rows.Count.ToString();

                        //XmlDocument xDoc = new XmlDocument();
                        //xDoc.Load(MR801_FILE_XML_PATH);
                        //XmlElement xRoot = xDoc.DocumentElement;
                        //if (xRoot == null) throw new Exception("Invalid xml document!");

                        //foreach (XmlNode xNode in xRoot)
                        //{

                        //}


                        //this._table.ReadXml(this._openAlarmJounralDlg.FileName);
                        //this._journalCntLabel.Text = this._table.Rows.Count.ToString();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("���������� ������� ����", "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

        }

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._measureTrLoaded = false;
            this._table.Rows.Clear();
            this._readBut.Enabled = false;
            this._device.LoadMeasureTrans();
            this._device.LoadPowerTrans();
        }

        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            double vers = Common.VersionConverter(this._device.DeviceVersion);
            if (vers >= 1.11 && vers < 1.16)
            {
                this._msg1Col.Visible = false;
                this._Is12Col.Visible = true;
                this._Is22Col.Visible = true;
                this._Is32Col.Visible = true;
                this._device.RefreshAdrSpace1_11();
            }
            else if (vers >= 1.16)
            {
                this._msg1Col.Visible = true;
                this._Is12Col.Visible = true;
                this._Is22Col.Visible = true;
                this._Is32Col.Visible = true;
                this._device.RefreshAdrSpace1_16();
            }
            else 
            {
                this._msg1Col.Visible = false;
                this._Is12Col.Visible = false;
                this._Is22Col.Visible = false;
                this._Is32Col.Visible = false;
            }
            this._table = this.GetDataTable();
            this._journalGrid.DataSource = this._table;
            this.StartRead();
        }

        private DataTable GetDataTable()
        {
            DataTable table = new DataTable("��801_������_������");
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].HeaderText);
            }
            return table;
        }

        private void _device_PowerTransLoadOK()
        {
            if (!this._canLoad) return;

            if (this._measureTrLoaded)
            {
                if (DialogResult.Yes == MessageBox.Show("������ ��������������� ���������.\n\r������ ������ ������?", "", MessageBoxButtons.YesNo))
                {
                    this._device.SaveAlarmJournalIndex();
                }
                else
                {
                    this._readBut.Enabled = true;
                }
            }
            else 
            {
                MessageBox.Show("�� ����� �������� ������ ��������������� ��������� ������.\n\r��������� ������������");
            }
        }
        
        private void AlarmJournalLoadOk()
        {
            try
            {
                if (this._device.AlarmJournalRecTime != "FAIL")
                {
                    this.OnAlarmJournalRecordLoadOk();
                    this._device.LoadAlarmJournal();
                }
                else
                {
                    this.OnAlarmJournalLoad();
                }
            }
            catch
            {
                this._readBut.Enabled = true;
            }
        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(UDZT); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.ja;
            }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR801AJ);
                MessageBox.Show("������ ������� �������� � Html", "���������");
            }
        }

        private void AlarmJournalForm_Activated(object sender, EventArgs e)
        {
            this._canLoad = true;
        }

        private void AlarmJournalForm_Deactivate(object sender, EventArgs e)
        {
            this._canLoad = false;
        }
    }
}
