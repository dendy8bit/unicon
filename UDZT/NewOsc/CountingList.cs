using System;
using System.Collections.Generic;
using System.Windows;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;

namespace BEMN.UDZT.NewOsc
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ���������� ���� � ����� �������
        /// </summary>
        private const int COUNTING_SIZE = 18;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private const int DISCRETS_COUNT = 24;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        private const int CURRENTS_COUNT = 12;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        private const int VOLTAGES_COUNT = 4;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        private const int CHANNELS_COUNT = 8;

        private readonly string[] _iNames =
        {
            "Ia_L1", "Ib_L1", "Ic_L1", "In_X1",
            "Ia_L2", "Ib_L2", "Ic_L2", "In_X2",
            "Ia_L3", "Ib_L3", "Ic_L3", "In_X3"
        };
        private readonly string[] _uNames = { "Ua", "Ub", "Uc", "Un" };
        #endregion [Constants]

        #region [Fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private readonly int _count;
        /// <summary>
        /// ������ ������� 1-24
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ������ ������� 1-8
        /// </summary>
        private ushort[][] _channels;
        private string _hdrString;
        private int _alarm;
        private OscJournalStruct _oscJournalStruct;
        private TransStructAll _transStructAll;
        private ushort[] _channelsNames;

        #region [����]
        /// <summary>
        /// ���� ���� ������
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� ����� ���� ������
        /// </summary>
        private short[][] _baseCurrents;

        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;
        
        #endregion [����]


        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;
        #endregion [����������]
        
        #endregion [Fields]

        #region [Properties]

        public OscJournalStruct OscJournalStruct
        {
            get { return this._oscJournalStruct; }
        }

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }

        /// <summary>
        /// ������������ ����
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        }

        private const int DIF_CURRENT_COUNT = 3;
        private readonly string[] DIF_CURRENT_NAMES = {"Ida", "Idb", "Idc"};
        /// <summary>
        /// ���������������� ����
        /// </summary>
        public double[][] DiffCurrents { get; private set; }
        /// <summary>
        /// ������������ ���. �����
        /// </summary>
        public double[] DiffCurrentKoef { get; private set; }

        //private const int BREAKING_CURRENT_COUNT = 3;
        //private readonly string[] BREAKING_CURRENT_NAMES = { "Ita", "Itb", "Itc" };

        private double[][] _id1, _id2, _id3;
        /// <summary>
        /// ���������������� ����
        /// </summary>
        //public double[][] BreakingCurrents { get; set; }
        /// <summary>
        /// ������������ ���. �����
        /// </summary>
        //public double[] BreakingCurrentKoef { get; set; }
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }

        /// <summary>
        /// ������������ ����������
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }

        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] Channels
        {
            get { return this._channels; }
            set { this._channels = value; }
        }

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        /// <summary>
        /// ������ �������� ������� ������������
        /// </summary>
        public ushort[] ChannelsNames
        {
            get { return this._channelsNames; }
        }

        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; set; }

        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private double[] VoltageKoefs { get; set; }

        /// <summary>
        /// ��������� ��� ����� Comtrade .hdr
        /// </summary>
        public string HdrString
        {
            get { return this._hdrString; }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        #endregion
        
        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, TransStructAll measure, IEnumerable<ushort> channelsNames)
        {
            this._channelsNames = channelsNames.ToArray();
            this._oscJournalStruct = oscJournalStruct;
            this._alarm = this.OscJournalStruct.Len - this.OscJournalStruct.After;
            this._transStructAll = measure;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length / COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this._count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n != COUNTING_SIZE) continue;
                m++;
                n = 0;
            }
            ushort[][] d9To24 = this.DiscretArrayInit(this._countingArray[16]);
            ushort[][] d1To8AndC1To8 = this.DiscretArrayAndChannelsInit(this._countingArray[17]);
            //��������
            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To8AndC1To8.Take(8));
            dicrets.AddRange(d9To24);
            this._discrets = dicrets.ToArray();
            //������
            List<ushort[]> channels = new List<ushort[]>(CHANNELS_COUNT);
            channels.AddRange(d1To8AndC1To8.Skip(8));
            this._channels = channels.ToArray();
            //����
            this.SetCountinCurrents();
            //����������
            this.SetCountingVoltage();
            this._hdrString = string.Format("��801 {0} {1} ������� - {2}",oscJournalStruct.Date, oscJournalStruct.Time, oscJournalStruct.Alm);
        }

        private void SetCountinCurrents()
        {
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            this._minI = double.MaxValue;
            this._maxI = double.MinValue;
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)a).ToArray();
                double koef = this.GetCurrentCoef(i);
                currentsKoefs[i] = koef * 40 * Math.Sqrt(2) / 32768;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;
            //��������� ����� ������� ��������� �������� �� ����������
            double[][] �urrents = new double[this._currents.Length][];
            for (int i = 0; i < this._currents.Length; i++)
            {
                double[] currents = this._currents[i];
                double[] temp = new double[currents.Length];
                for (int j = 0; j < currents.Length; j++)
                {
                    temp[j] = currents[j];
                }
                �urrents[i] = temp;
            }

            this.MeasureAdditionalCurrents(�urrents);
        }

        private void MeasureAdditionalCurrents(double[][] currents)
        {
            double[] koefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < koefs.Length; i++)
            {
                int pol = this.GetPolarity(i);
                double Un = this.GetUnFromSide(i);
                double Sn = this._transStructAll.PowerTransformators[0].Power; // � ������� ���� ������ �������� ������ �������
                koefs[i] = pol*(Math.Sqrt(3)*Un)/Sn;
            }
            for (int i = 0; i < currents.Length; i++)
            {
                for (int j = 0; j < currents[i].Length; j++)
                {
                    currents[i][j] = currents[i][j]*koefs[i];
                }
            }

            double[][] L1 = currents.Take(4).ToArray();
            double[][] L2 = currents.Skip(4).Take(4).ToArray();
            double[][] L3 = currents.Skip(8).Take(4).ToArray();

            double[][] s1 = this.InitSideMatrix(); 
            double[][] s2 = this.InitSideMatrix();
            double[][] s3 = this.InitSideMatrix();
            //���� �� �����
            this.SetSideForL(L1, s1, this._transStructAll.TransIs1, 1); // ���������� ����, ���� ���� �������� � ������� �������
            this.SetSideForL(L2, s1, this._transStructAll.TransIs2, 1); // �������� �� ������ ������ ������ �����
            this.SetSideForL(L3, s1, this._transStructAll.TransIs3, 1);

            this.SetSideForL(L1, s2, this._transStructAll.TransIs1, 2);
            this.SetSideForL(L2, s2, this._transStructAll.TransIs2, 2);
            this.SetSideForL(L3, s2, this._transStructAll.TransIs3, 2);

            this.SetSideForL(L1, s3, this._transStructAll.TransIs1, 3);
            this.SetSideForL(L2, s3, this._transStructAll.TransIs2, 3);
            this.SetSideForL(L3, s3, this._transStructAll.TransIs3, 3);

            for (int i = 0; i < this._transStructAll.PowerTransformators.Length; i++)
            {
                this.SetSideForX(L1, L2, L3, s1, s2, s3,  this._transStructAll.PowerTransformators[i], i);
            }

            double[][] M1, M2, M3;
            this.InitMMatrix(out M1, out M2, out M3);

            this.AnalyseWindingConnection(s1, s2, s3, M1, M2, M3);
        }

        private double[][] InitSideMatrix()
        {
            double[][] s = new double[4][]; // 4 ���� ��� ����� ������ - Ia,Ib,Ic,In
            for (int i = 0; i < s.Length; i++)
            {
                s[i] = new double[this._count];
            }
            return s;
        }

        private void SetSideForL(double[][] L, double[][] s, TransCurrentStruct trans, int side)
        {
            if (trans.Bind == side)
            {
                for (int i = 0; i < s.Length - 1; i++) // ������ ������ ���� ���������
                {
                    for (int j = 0; j < s[i].Length; j++)
                    {
                        s[i][j] += L[i][j];
                    }
                }
            }
        }

        private void SetSideForX(double[][] L1, double[][] L2, double[][] L3, double[][] s1, double[][] s2, double[][] s3, PowerTransformatorStruct powerTransformator, int side)
        {
            double[][] l;
            switch (powerTransformator.MeasuringGround)
            {
                case MeasuringGround.NONE:
                    return;
                case MeasuringGround.X1:
                    l = L1;
                    break;
                case MeasuringGround.X2:
                    l = L2;
                    break;
                case MeasuringGround.X3:
                    l = L3;
                    break;
                default:
                    throw new ArgumentNullException();
            }
            if (side == 0)
            {
                for (int j = 0; j < s1[3].Length; j++)
                {
                    s1[3][j] = l[3][j];
                }
            }
            else if (side == 1)
            {
                for (int j = 0; j < s2[3].Length; j++)
                {
                    s2[3][j] = l[3][j];
                }
            }
            else
            {
                for (int j = 0; j < s3[3].Length; j++)
                {
                    s3[3][j] = l[3][j];
                }
            }
        }

        private void InitMMatrix(out double[][] m1, out double[][] m2, out double[][] m3)
        {
            m1 = new double[3][];
            for (int i = 0; i < m1.Length; i++)
            {
                m1[i] = new double[3];
            }
            m1[0][0] = 1; m1[0][1] = 0;m1[0][2] = 0;
            m1[1][0] = 0; m1[1][1] = 1; m1[1][2] = 0;
            m1[2][0] = 0; m1[2][1] = 0; m1[2][2] = 1;

            m2 = new double[3][];
            for (int i = 0; i < m2.Length; i++)
            {
                m2[i] = new double[3];
            }
            m2[0][0] = (double)2/3; m2[0][1] = (double)-1/3;m2[0][2] = (double) -1/3;
            m2[1][0] = (double)-1/3; m2[1][1] = (double)2/3; m2[1][2] = (double)-1/3;
            m2[2][0] = (double)-1/3; m2[2][1] = (double)-1/3; m2[2][2] = (double)2/3;

            m3 = new double[3][];
            for (int i = 0; i < m3.Length; i++)
            {
                m3[i] = new double[3];
            }
            m3[0][0] = 1/Math.Sqrt(3); m3[0][1] = -1/ Math.Sqrt(3); m3[0][2] = 0;
            m3[1][0] = 0; m3[1][1] = 1/ Math.Sqrt(3); m3[1][2] = -1/ Math.Sqrt(3);
            m3[2][0] = -1/ Math.Sqrt(3); m3[2][1] = 0; m3[2][2] = 1/ Math.Sqrt(3);
        }

        private void AnalyseWindingConnection(double[][] s1, double[][] s2, double[][] s3, double[][] m1, double[][] m2, double[][] m3)
        {
            SideMatrix Sm1;
            SideMatrix Sm2;
            SideMatrix Sm3;

            if (this._transStructAll.PowerTransformators[0].WindingType == WindingType.Y)
            {
                Sm1 = new SideMatrix {S = s1, M = m1, WindingType = WindingType.Y, Group = 0};
                Sm2 = this.GetSideMatrix(s2, m1, m2, m3, this._transStructAll.PowerTransformators[1]);
                Sm3 = this.GetSideMatrix(s3, m1, m2, m3, this._transStructAll.PowerTransformators[2]);
            }
            else if (this._transStructAll.PowerTransformators[0].WindingType == WindingType.Yn)
            {
                if (this._transStructAll.PowerTransformators[0].MeasuringGround != MeasuringGround.NONE)
                {
                    for (int i = 0; i < s1.Length - 1; i++)
                    {
                        for (int j = 0; j < s1[i].Length; j++)
                        {
                            s1[i][j] = s1[i][j] - s1[3][j]/3;
                        }
                    }
                    Sm1 = new SideMatrix {S = s1, M = m1, WindingType = WindingType.Yn, Group = 0};
                    Sm2 = this.GetSideMatrix(s2, m1, m2, m3, this._transStructAll.PowerTransformators[1]);
                    Sm3 = this.GetSideMatrix(s3, m1, m2, m3, this._transStructAll.PowerTransformators[2]);
                }
                else
                {
                    Sm1 = new SideMatrix { S = s1, M = m2, WindingType = WindingType.Yn, Group = 0 };
                    Sm2 = this.GetSideMatrix(s2, m1, m2, m3, this._transStructAll.PowerTransformators[1]);
                    Sm3 = this.GetSideMatrix(s3, m1, m2, m3, this._transStructAll.PowerTransformators[2]);
                }
            }
            else
            {
                if (this._transStructAll.PowerTransformators[1].WindingType == WindingType.D
                    && this._transStructAll.PowerTransformators[2].WindingType == WindingType.D)
                {
                    Sm1 = new SideMatrix { S = s1, M = m3, WindingType = WindingType.D, Group = 0 };
                    Sm2 = new SideMatrix { S = s2, M = m3, WindingType = WindingType.D, Group = this._transStructAll.PowerTransformators[1].GroupJoin };
                    Sm3 = new SideMatrix { S = s3, M = m3, WindingType = WindingType.D, Group = this._transStructAll.PowerTransformators[2].GroupJoin };
                }
                else
                {
                    Sm1 = new SideMatrix { S = s1, M = m3, WindingType = WindingType.D, Group = 0 };
                    Sm2 = this.GetSideMatrix(s2, m1, m2, m3, this._transStructAll.PowerTransformators[1]);
                    Sm3 = this.GetSideMatrix(s3, m1, m2, m3, this._transStructAll.PowerTransformators[2]);
                }
            }

            if (Sm1.WindingType == WindingType.Y || Sm1.WindingType == WindingType.Yn 
                || Sm1.WindingType == WindingType.D && Sm2.WindingType == WindingType.D && Sm3.WindingType == WindingType.D)
            {
                this.ReshufflePhase(Sm2);
                this.ReshufflePhase(Sm3);
            }
            else
            {
                if (Sm2.WindingType == WindingType.Y || Sm2.WindingType == WindingType.Yn)
                {
                    Sm1.Group = 12 - Sm2.Group;
                    Sm3.Group = Sm3.Group > Sm2.Group ? Sm3.Group - Sm2.Group : Sm3.Group - Sm2.Group + 12;
                    this.ReshufflePhase(Sm1);
                    this.ReshufflePhase(Sm3);
                }
                else
                {
                    Sm1.Group = 12 - Sm3.Group;
                    Sm2.Group = Sm2.Group > Sm3.Group ? Sm2.Group - Sm3.Group : Sm2.Group - Sm3.Group + 12;
                    this.ReshufflePhase(Sm1);
                    this.ReshufflePhase(Sm2);
                }
            }

            this.CalculateDiffCurrents(Sm1, Sm2, Sm3);
        }

        private void ReshufflePhase(SideMatrix sm)
        {
            double[][] buffer = new double[sm.S.Length][];
            for (int i = 0; i < sm.S.Length; i++)
            {
                buffer[i] = new double[sm.S[i].Length];
                for (int j = 0; j < sm.S[i].Length; j++)
                {
                    buffer[i][j] = sm.S[i][j];
                }
            }
            switch (sm.Group)
            {
                case 0:
                case 1:
                    return;
                case 2:
                case 3:
                    {
                        for (int i = 0; i < sm.S[0].Length; i++)
                        {
                            sm.S[0][i] = -buffer[1][i];
                            sm.S[1][i] = -buffer[2][i];
                            sm.S[2][i] = -buffer[0][i];
                        }
                        break;
                    }
                case 4:
                case 5:
                    {
                        for (int i = 0; i < sm.S[0].Length; i++)
                        {
                            sm.S[0][i] = buffer[2][i];
                            sm.S[1][i] = buffer[0][i];
                            sm.S[2][i] = buffer[1][i];
                        }
                        break;
                    }
                case 6:
                case 7:
                    {
                        for (int i = 0; i < sm.S[0].Length; i++)
                        {
                            sm.S[0][i] = -buffer[0][i];
                            sm.S[1][i] = -buffer[1][i];
                            sm.S[2][i] = -buffer[2][i];
                        }
                        break;
                    }
                case 8:
                case 9:
                    {
                        for (int i = 0; i < sm.S[0].Length; i++)
                        {
                            sm.S[0][i] = buffer[1][i];
                            sm.S[1][i] = buffer[2][i];
                            sm.S[2][i] = buffer[0][i];
                        }
                        break;
                    }
                case 10:
                case 11:
                    {
                        for (int i = 0; i < sm.S[0].Length; i++)
                        {
                            sm.S[0][i] = -buffer[2][i];
                            sm.S[1][i] = -buffer[0][i];
                            sm.S[2][i] = -buffer[1][i];
                        }
                        break;
                    }
            }
        }

        private void CalculateDiffCurrents(SideMatrix Sm1, SideMatrix Sm2, SideMatrix Sm3)
        {
            this._id1 = new double[Sm1.S.Length - 1][];
            for (int i = 0; i < Sm1.S.Length - 1; i++)
            {
                this._id1[i] = new double[Sm1.S[i].Length];
                for (int j = 0; j < Sm1.S[i].Length; j++)
                {
                    this._id1[i][j] = Sm1.M[i][0] * Sm1.S[0][j] + Sm1.M[i][1] * Sm1.S[1][j] + Sm1.M[i][2] * Sm1.S[2][j];
                }
            }
            this._id2 = new double[Sm2.S.Length - 1][];
            for (int i = 0; i < Sm2.S.Length - 1; i++)
            {
                this._id2[i] = new double[Sm2.S[i].Length];
                for (int j = 0; j < Sm2.S[i].Length; j++)
                {
                    this._id2[i][j] = Sm2.M[i][0] * Sm2.S[0][j] + Sm2.M[i][1] * Sm2.S[1][j] + Sm2.M[i][2] * Sm2.S[2][j];
                }
            }
            this._id3 = new double[Sm3.S.Length - 1][];
            for (int i = 0; i < Sm3.S.Length - 1; i++)
            {
                this._id3[i] = new double[Sm3.S[i].Length];
                for (int j = 0; j < Sm3.S[i].Length; j++)
                {
                    this._id3[i][j] = Sm3.M[i][0] * Sm3.S[0][j] + Sm3.M[i][1] * Sm3.S[1][j] + Sm3.M[i][2] * Sm3.S[2][j];
                }
            }

            this.DiffCurrents = new double[DIF_CURRENT_COUNT][];
            this.DiffCurrentKoef = new double[DIF_CURRENT_COUNT];
            for (int i = 0; i < DIF_CURRENT_COUNT; i++)
            {
                this.DiffCurrents[i] = new double[this._count];
                this.DiffCurrentKoef[i] = 0.001;
                for (int j = 0; j < this._count; j++)
                {
                    double koefSide1 = this._transStructAll.PowerTransformators[0].Power/(this._transStructAll.PowerTransformators[0].Voltage*Math.Sqrt(3));
                    this.DiffCurrents[i][j] = Math.Round(koefSide1*(this._id1[i][j] + this._id2[i][j] + this._id3[i][j]) * 1000);
                }
            }

            //this.CalculatebreakingCurrent();
        }

        //private void CalculatebreakingCurrent()
        //{
        //    double[][] It1 = this.PrepareFirstHarmonic(this._id1);
        //    double[][] It2 = this.PrepareFirstHarmonic(this._id2);
        //    double[][] It3 = this.PrepareFirstHarmonic(this._id3);

        //    this.BreakingCurrents = new double[BREAKING_CURRENT_COUNT][];
        //    this.BreakingCurrentKoef = new double[BREAKING_CURRENT_COUNT];
        //    for (int i = 0; i < It1.Length; i++)
        //    {
        //        this.BreakingCurrents[i] = new double[It1[i].Length];
        //        this.BreakingCurrentKoef[i] = 0.001;
        //        for (int j = 0; j < It1[i].Length; j++)
        //        {
        //            double koefSide1 = this._transStructAll.PowerTransformators[0].Power / (this._transStructAll.PowerTransformators[0].Voltage * Math.Sqrt(3));
        //            this.BreakingCurrents[i][j] = Math.Round(koefSide1*(It1[i][j] + It2[i][j] + It3[i][j]) * 1000);
        //        }
        //    }
        //}

        private double[][] PrepareFirstHarmonic(double[][] Id)
        {
            double[][] It = new double[Id.Length][];
            for (int l = 0; l < Id.Length; l++)
            {
                Point[] firstHarmonic = new Point[Id[l].Length];
                double[] factors = new double[20];
                int N = 20;
                double K = 1;
                for (int j = N - 1; j < firstHarmonic.Length; j++)
                {
                    for (int i = 0; i < N; i++)
                    {
                        if (i == 0)
                        {
                            factors[i] = Id[l][j - N + 1 + i];
                            continue;
                        }
                        if (i == 1)
                        {
                            factors[i] = Id[l][j - N + 1 + i] + 2*Math.Cos(2*Math.PI/N*K)*factors[i - 1];
                            continue;
                        }
                        factors[i] = Id[l][j - N + 1 + i] + 2*Math.Cos(2*Math.PI/N*K)*factors[i - 1] - factors[i - 2];
                    }

                    firstHarmonic[j].X = Math.Cos(2*Math.PI/N*K)*factors[N - 1] - factors[N - 2];
                    firstHarmonic[j].Y = Math.Sin(2*Math.PI/N*K)*factors[N - 1];
                }
                It[l] = firstHarmonic.Select(o => Math.Sqrt(o.X*o.X + o.Y*o.Y)/(10.0*Math.Sqrt(2))).ToArray();
            }

            return It;
        }

        private SideMatrix GetSideMatrix(double[][] s, double[][] m1, double[][] m2, double[][] m3, PowerTransformatorStruct powerTransformator)
        {
            if (powerTransformator.WindingType == WindingType.D)
            {
                return new SideMatrix { S = s, M = m3, WindingType = WindingType.D, Group = powerTransformator .GroupJoin};
            }
            if (powerTransformator.WindingType == WindingType.Y)
            {
                return new SideMatrix { S = s, M = m1, WindingType = WindingType.Y, Group = powerTransformator .GroupJoin };
            }
            if (powerTransformator.MeasuringGround != MeasuringGround.NONE)
            {
                for (int i = 0; i < s.Length - 1; i++)
                {
                    for (int j = 0; j < s[i].Length; j++)
                    {
                        s[i][j] = s[i][j] - s[3][j]/3;
                    }
                }
                return new SideMatrix {S = s, M = m1, WindingType = WindingType.Yn, Group = powerTransformator .GroupJoin};
            }
            return new SideMatrix {S = s, M = m2, WindingType = WindingType.Yn, Group = powerTransformator .GroupJoin};
        }

        private int GetPolarity(int i)
        {
            if (i == 0 || i == 1 || i == 2)
            {
                return this._transStructAll.TransIs1.PolarityL;
            }
            if (i == 3)
            {
                return this._transStructAll.TransIs1.PolarityX;
            }
            if (i == 4 || i == 5 || i == 6)
            {
                return this._transStructAll.TransIs2.PolarityL;
            }
            if (i == 7)
            {
                return this._transStructAll.TransIs2.PolarityX;
            }
            if (i == 8 || i == 9 || i == 10)
            {
                return this._transStructAll.TransIs3.PolarityL;
            }
            if (i == 11)
            {
                return this._transStructAll.TransIs3.PolarityX;
            }
            throw new ArgumentException();
        }

        private double GetUnFromSide(int i)
        {
            if (i >= 0 && i < 3)
            {
                return this._transStructAll.TransIs1.Bind != 0 
                    ? this._transStructAll.PowerTransformators[this._transStructAll.TransIs1.Bind - 1].Voltage 
                    : this._transStructAll.TransIs1.Bind;
            }
            if (i == 3)
            {
                return
                    this._transStructAll.PowerTransformators.Where(
                        powerTransformator => powerTransformator.MeasuringGround == MeasuringGround.X1)
                        .Select(powerTransformator => powerTransformator.Voltage)
                        .FirstOrDefault();
            }
            if (i >= 4 && i < 7)
            {
                return this._transStructAll.TransIs2.Bind != 0 ? this._transStructAll.PowerTransformators[this._transStructAll.TransIs2.Bind - 1].Voltage : this._transStructAll.TransIs2.Bind;
            }
            if (i == 7)
            {
                return
                    this._transStructAll.PowerTransformators.Where(
                        powerTransformator => powerTransformator.MeasuringGround == MeasuringGround.X2)
                        .Select(powerTransformator => powerTransformator.Voltage)
                        .FirstOrDefault();
            }
            if (i >= 8 && i < 11)
            {
                return this._transStructAll.TransIs3.Bind != 0 ? this._transStructAll.PowerTransformators[this._transStructAll.TransIs3.Bind - 1].Voltage : this._transStructAll.TransIs3.Bind;
            }
            if (i == 11)
            {
                return
                    this._transStructAll.PowerTransformators.Where(
                        powerTransformator => powerTransformator.MeasuringGround == MeasuringGround.X3)
                        .Select(powerTransformator => powerTransformator.Voltage)
                        .FirstOrDefault();
            }
            throw new ArgumentException();
        }

        private double GetCurrentCoef(int i)
        {
            if (i == 0 || i == 1 || i == 2)
            {
                return this._transStructAll.TransIs1.Ttl;
            }
            if (i == 3)
            {
                return this._transStructAll.TransIs1.Ttx;
            }
            if (i == 4 || i == 5 || i == 6)
            {
                return this._transStructAll.TransIs2.Ttl;
            }
            if (i == 7)
            {
                return this._transStructAll.TransIs2.Ttx;
            }
            if (i == 8 || i == 9 || i == 10)
            {
                return this._transStructAll.TransIs3.Ttl;
            }
            if (i == 11)
            {
                return this._transStructAll.TransIs3.Ttx;
            }
            throw new ArgumentException();
        }

        private void SetCountingVoltage()
        {
            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];
            double[] voltageKoefs = new double[VOLTAGES_COUNT];

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + CURRENTS_COUNT].Select(a => (short) a).ToArray();

                double koef = i == VOLTAGES_COUNT - 1 ? this._transStructAll.TransU.KthxValue : this._transStructAll.TransU.KthlValue;

                voltageKoefs[i] = koef*2*Math.Sqrt(2)/256.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i]*(double) a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;
        }

        private CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._channels = new ushort[CHANNELS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];
            this._count = count;

            this.DiffCurrents = new double[DIF_CURRENT_COUNT][];
            this.DiffCurrentKoef = new double[DIF_CURRENT_COUNT];
        }

        #endregion [Ctor's]

        #region [Help members]

        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort) (Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }

        private ushort[][] DiscretArrayAndChannelsInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                byte lowByteSource = Common.LOBYTE(sourseArray[i]);
                for (int j = 0; j < 8; j++)
                {
                    result[j][i] = (ushort) (Common.GetBit(lowByteSource, j) ? 1 : 0); //17-24 �������� ���� �� �������
                }
                byte hiByteSource = Common.HIBYTE(sourseArray[i]);
                for (int j = 0; j < 8; j++)
                {
                    result[j + 8][i] = (ushort) (Common.GetBit(hiByteSource, 7 - j) ? 1 : 0); //�������� ������� � ����� ���� ����� �������
                }
            }

            return result;
        }

        #endregion [Help members]

        private void WriteSideCurrents(double[][] Id, int side, StreamWriter hdrFile)
        {
            for (int i = 0; i < Id.Length; i++)
            {
                string s;
                if (i == 0)
                {
                    s = "Ia_s"+side;
                }
                else if (i == 1)
                {
                    s = "Ib_s"+side;
                }
                else
                {
                    s = "Ic_s"+side;
                }
                s = Id[i].Aggregate(s, (current, d) => current + $",{d.ToString(new NumberFormatInfo { NumberDecimalSeparator = "." })}");
                hdrFile.WriteLine(s);
            }
        }

        public void Save(string filePath)
        {
            string directiryPath = Path.GetDirectoryName(filePath);
            string hdrPath = Path.ChangeExtension(Path.Combine(directiryPath, filePath), "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this.HdrString);
                for (int i = 0; i < this._channelsNames.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}", i + 1, this._channelsNames[i]);
                }
                if (this.DiffCurrents != null)
                {
                    hdrFile.WriteLine("Vs1 = {0}", this._transStructAll.PowerTransformators[0].Voltage);
                    hdrFile.WriteLine("Ps1 = {0}", this._transStructAll.PowerTransformators[0].Power);
                    hdrFile.WriteLine("Side = {0}", 3);
                    this.WriteSideCurrents(this._id1, 1, hdrFile);
                    this.WriteSideCurrents(this._id2, 2, hdrFile);
                    this.WriteSideCurrents(this._id3, 3, hdrFile);
                }
                hdrFile.WriteLine(1251);
            }
            string cgfPath = Path.ChangeExtension(Path.Combine(directiryPath, filePath), "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP801,1");

                cgfFile.WriteLine(this.DiffCurrents == null ? "48, 16A, 32D" : "51, 19A, 32D");
                //cgfFile.WriteLine(this.DiffCurrents == null && this.BreakingCurrents == null ? "48, 16A, 32D" : "54, 22A, 32D");

                int index = 1;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                for (int i = 0; i < this.Currents.Length; i++)
                {
                    cgfFile.WriteLine("{0},{1},{2},,A,{3},0,0,-32768,32767,1,1,P", index, this._iNames[i] + this.GetSide(i), this.GetPhaseName(i), this.CurrentsKoefs[i].ToString(format));
                    index++;
                }

                if (this.DiffCurrents != null)
                {
                    for (int i = 0; i < this.DiffCurrents.Length; i++)
                    {
                        cgfFile.WriteLine("{0},{1},{2},,A,{3},0,0,-32768,32767,1,1,P", index, this.DIF_CURRENT_NAMES[i], this.GetPhaseName(i), this.DiffCurrentKoef[i].ToString(format));
                        index++;
                    }
                }

                //if (this.BreakingCurrents != null)
                //{
                //    for (int i = 0; i < this.BreakingCurrents.Length; i++)
                //    {
                //        cgfFile.WriteLine("{0},{1},{2},,A,{3},0,0,-32768,32767", index, this.BREAKING_CURRENT_NAMES[i],
                //            this.GetPhaseName(i), this.BreakingCurrentKoef[i].ToString(format));
                //        index++;
                //    }
                //}

                for (int i = 0; i < this.Voltages.Length; i++)
                {
                    cgfFile.WriteLine("{0},{1},{2},,V,{3},0,0,-32768,32767,1,1,P", index, this._uNames[i], this.GetPhaseName(i), this.VoltageKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                for (int i = 0; i < this.Channels.Length; i++)
                {
                    string channelName;
                    try
                    {
                        channelName = Strings.Sygnal[this._channelsNames[i]];
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, channelName);
                        index++;
                    }
                    catch (Exception)
                    {
                        channelName = "Oshibka";
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, channelName);
                        index++;
                    }
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this.OscJournalStruct.Len);

                cgfFile.WriteLine(this.OscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this.OscJournalStruct.GetFormattedDateTimeAlarm);
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(Path.Combine(directiryPath, filePath), "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (short[] current  in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    if (this.DiffCurrents != null)
                    {
                        foreach (double[] current in this.DiffCurrents)
                        {
                            datFile.Write(",{0}", current[i]);
                        }
                    }
                    //if (this.BreakingCurrents != null)
                    //{
                    //    foreach (double[] current in this.BreakingCurrents)
                    //    {
                    //        datFile.Write(",{0}", current[i]);
                    //    }
                    //}
                    foreach (short[] voltage in this._baseVoltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this.Channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        private string GetPhaseName(int i)
        {
            switch (i)
            {
                case 0:
                case 4:
                case 8:
                    return "a";
                case 1:
                case 5:
                case 9:
                    return "b";
                case 2:
                case 6:
                case 10:
                    return "c";
                case 3:
                case 7:
                case 11:
                    return "n";
            }
            return string.Empty;
        }

        private string GetSide(int i)
        {
            switch (i)
            {
                case 0:
                case 1:
                case 2:
                    return this._transStructAll.TransIs1.Bind != 0 ? $"({Strings.Bindings[this._transStructAll.TransIs1.Bind]})" : string.Empty;
                case 3:
                    return this.GetSideN(MeasuringGround.X1);
                case 4:
                case 5:
                case 6:
                    return this._transStructAll.TransIs2.Bind != 0 ? $"({Strings.Bindings[this._transStructAll.TransIs2.Bind]})" : string.Empty;
                case 7:
                    return this.GetSideN(MeasuringGround.X1);
                case 8:
                case 9:
                case 10:
                    return this._transStructAll.TransIs3.Bind != 0 ? $"({Strings.Bindings[this._transStructAll.TransIs3.Bind]})" : string.Empty;
                case 11:
                    return this.GetSideN(MeasuringGround.X1);
            }
            return string.Empty;
        }

        private string GetSideN(MeasuringGround ground)
        {
            for (int j = 0; j < 3; j++)
            {
                if (this._transStructAll.PowerTransformators[j].MeasuringGround == ground)
                {
                    return $"(S{j + 1})";
                }
            }
            return string.Empty;
        }

        public List<string[]> GetDatList()
        {
            List<string[]> retList = new List<string[]>();
            for (int i = 0; i < this._count; i++)
            {
                List<string> datStr = this._baseCurrents.Select(current => current[i].ToString()).ToList();
                datStr.AddRange(this._baseVoltages.Select(voltage => voltage[i].ToString()));
                datStr.AddRange(this.Discrets.Select(discret => discret[i].ToString()));
                for (int j = this.Channels.Length - 1; j >= 0; j--)
                {
                    datStr.Add(this.Channels[j][i].ToString());
                }
                retList.Add(datStr.ToArray());
            }
            return retList;
        }

        public double[] GetKoefs()
        {
            return new[]
            {
                this.CurrentsKoefs[0], this.CurrentsKoefs[3], this.CurrentsKoefs[4], this.CurrentsKoefs[7],
                this.CurrentsKoefs[8], this.CurrentsKoefs[11], this.VoltageKoefs[0], this.VoltageKoefs[3]
            };
        }

        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            //������� �� � ��������� ��� ���� 
            bool difcurrent = int.Parse(cfgStrings[1].Split(',')[1].Replace("A", string.Empty)) == CURRENTS_COUNT + VOLTAGES_COUNT + DIF_CURRENT_COUNT;//+BREAKING_CURRENT_COUNT;

            int index = difcurrent 
                ? 2 + VOLTAGES_COUNT + CURRENTS_COUNT+DIF_CURRENT_COUNT + DISCRETS_COUNT + CHANNELS_COUNT + 2 
                : 2 + VOLTAGES_COUNT + CURRENTS_COUNT + DISCRETS_COUNT + CHANNELS_COUNT + 2;

            int counts = int.Parse(cfgStrings[index++].Replace("1000,", string.Empty));

            ushort[] channelNames = new ushort[CHANNELS_COUNT];
            try
            {
                for (int i = 0; i < CHANNELS_COUNT; i++)
                {
                    channelNames[i] = ushort.Parse(hdrStrings[1 + i].Replace(string.Format("K{0} = ", i + 1), string.Empty));
                }
            }
            catch
            {
                counts = Convert.ToInt32(hdrStrings[1].Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries)[1]);
            }
            double[] factors = difcurrent
                ? new double[VOLTAGES_COUNT + CURRENTS_COUNT+DIF_CURRENT_COUNT] //+BREAKING_CURRENT_COUNT]
                : new double[VOLTAGES_COUNT + CURRENTS_COUNT];

            Regex factorRegex = new Regex(@"\d+\,[IU]\S+\,\w+\,\,[AV]\,(?<value>[0-9\.]+)");
            int indexMax = difcurrent
                ? 2 + VOLTAGES_COUNT + CURRENTS_COUNT +DIF_CURRENT_COUNT //+BREAKING_CURRENT_COUNT 
                : 2 + VOLTAGES_COUNT + CURRENTS_COUNT;
            for (int i = 2; i < indexMax; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2] = double.Parse(res, format);
            }

            string startTime = cfgStrings[index++].Split(',')[1];
            string runTime = cfgStrings[index].Split(',')[1];
            TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
            int alarm = (int) a.TotalMilliseconds;
            
            CountingList result = new CountingList(counts)
            {
                _alarm = alarm, _channelsNames = channelNames, _hdrString = hdrString, IsLoad = true, FilePath = filePath
            };

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);
            
            for (int i = 0; i < result._currents.Length; i++)
            {
                result._currents[i] = new double[counts];
                result._baseCurrents[i] = new short[counts];
            }

            if (difcurrent)
            {
                result.CurrentsKoefs = new double[CURRENTS_COUNT+DIF_CURRENT_COUNT];
                for (int i = 0; i < result.DiffCurrents.Length; i++)
                {
                    result.DiffCurrents[i] = new double[counts];
                    //result.BreakingCurrents[i] = new double[counts];
                }
            }
            else
            {
                result.CurrentsKoefs = new double[CURRENTS_COUNT];
            }

            result.VoltageKoefs = new double[VOLTAGES_COUNT];
            for (int i = 0; i < result._voltages.Length; i++)
            {
                result._voltages[i] = new double[counts];
                result._baseVoltages[i] = new short[counts];
            }

            for (int i = 0; i < result._discrets.Length; i++)
            {
                result._discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < result._channels.Length; i++)
            {
                result._channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    result._baseCurrents[j][i] = short.Parse(means[j+2], CultureInfo.InvariantCulture);
                    result._currents[j][i] = result._baseCurrents[j][i]*factors[j];
                    result.CurrentsKoefs[j] = factors[j];
                }
                if (difcurrent)
                {
                    for (int j = 0; j < DIF_CURRENT_COUNT; j++)
                    {
                        result.DiffCurrents[j][i] = short.Parse(means[j + 2], CultureInfo.InvariantCulture);
                        result.DiffCurrentKoef[j] = factors[j];
                    }
                    //for (int j = 0; j < BREAKING_CURRENT_COUNT; j++)
                    //{
                    //    result.BreakingCurrents[j][i] = short.Parse(means[j + 2], CultureInfo.InvariantCulture);
                    //    result.BreakingCurrentKoef[j] = factors[j];
                    //}
                }
                int currentsCount = difcurrent ? CURRENTS_COUNT + DIF_CURRENT_COUNT : CURRENTS_COUNT;
                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    result._baseVoltages[j][i] = short.Parse(means[j+currentsCount+2]);
                    result._voltages[j][i] = result._baseVoltages[j][i]*factors[j + currentsCount];
                    result.VoltageKoefs[j] = factors[j + currentsCount];
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    result._discrets[j][i] = ushort.Parse(means[j + currentsCount + VOLTAGES_COUNT+2]);
                }

                for (int j = 0; j < CHANNELS_COUNT; j++)
                {
                    result._channels[j][i] = ushort.Parse(means[j + currentsCount + VOLTAGES_COUNT + DISCRETS_COUNT + 2]);
                }
            }
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, result._currents[i].Max());
                result._minI = Math.Min(result.MinI, result._currents[i].Min());
            }

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, result._voltages[i].Max());
                result._minU = Math.Min(result.MinU, result._voltages[i].Min());
            }
            return result;
        }
    }

    class SideMatrix
    {
        public double[][] S { get; set; }
        public double[][] M { get; set; }
        public WindingType WindingType { get; set; }
        public int Group { get; set; }
    }
}
