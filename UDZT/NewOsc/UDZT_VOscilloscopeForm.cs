﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;

namespace BEMN.UDZT.NewOsc
{
    public partial class VOscilloscopeForm : Form, IFormView, INodeView
    {
        private UDZT _device;
        private List<OscJournalStruct> _oscilloscopeJournalList;
        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<OscopeChannelStruct> _oscopeStruct;
        private MemoryEntity<TransStructAll> _transStruct;
        private MemoryEntity<OscOptionsStruct> _oscOption;
        private OscPageLoader _pageLoader;
        private CountingList _countingList;
        private int _currentJournalIndex;
        private int _recordIndex;

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }
        
        public VOscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public VOscilloscopeForm(UDZT device)
        {
            this.InitializeComponent();
            this._device = device;
            
            this._recordIndex = 0;
            this._oscilloscopeJournalList = new List<OscJournalStruct>();
            // чтение журнала осциллографа
            this._oscJournal = this._device.OscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnLoadJournalFail);
            // Сохранение номера журнала осциллографа
            this._device.OscJournalSaveOK += HandlerHelper.CreateHandler(this, this._oscJournal.LoadStruct);
            this._device.OscJournalSaveFail += sender =>
                this.OnLoadJournalFail();
            // Параметры осциллографа
            this._oscOption = device.OscOptionsStruct;
            this._oscOption.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._oscOption.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnLoadJournalFail);
            // параметры трансформатора
            this._transStruct = device.TransStruct;
            this._transStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscOption.LoadStruct);
            this._transStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnLoadJournalFail);
            // Каналы осциллографа
            this._oscopeStruct = device.OscChannels;
            this._oscopeStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._transStruct.LoadStruct);
            this._oscopeStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnLoadJournalFail);
            //Загрузчик страниц
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStopped);
        }

        private void OnLoadJournalFail()
        {
            MessageBox.Show("Невозможно прочитать журнал осциллографа", "Внимание", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            this._oscStatus.Text = "Невозможно прочитать журнал осциллографа";
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }

        public void StartReadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscJournalReadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscilloscopeCountLabel.Visible = false;
            this._oscilloscopeCountCB.Visible = false;
            this._oscilloscopeCountCB.Items.Clear();
            this._oscilloscopeJournalList.Clear();
            this._recordIndex = 0;
            this._oscJournalDataGrid.Rows.Clear();
            this._oscStatus.Text = "Идет чтение журнала осциллографа";
            this._device.SaveOscilloscopeJournal((ushort)this._recordIndex);
        }

        public void ReadJournalRecord()
        {
            OscJournalStruct journal = this._oscJournal.Value.Clone<OscJournalStruct>();
            if (journal.OscExist)
            {
                this._oscilloscopeJournalList.Add(journal);
                this._oscilloscopeCountCB.Items.Add(this._recordIndex + 1);
                string endOscStr = journal.EndOsc < this._oscOption.Value.LoadedFullOscSizeInWords
                    ? journal.EndOsc.ToString()
                    : string.Concat(journal.EndOsc, " [", this._oscOption.Value.LoadedFullOscSizeInWords - journal.EndOsc, "]");
                string str = "";
                if (this._recordIndex == 0)
                {
                    str = "[Посл.]";
                }
                this._oscJournalDataGrid.Rows.Add(
                    string.Concat(this._recordIndex + 1, str),
                    journal.Date,
                    journal.Time,
                    journal.Alm,
                    journal.Ready,
                    journal.Point,
                    endOscStr,
                    journal.Begin,
                    journal.Len,
                    journal.After,
                    journal.Rez
                    );

                this._recordIndex++;
                this._device.SaveOscilloscopeJournal((ushort)this._recordIndex);
            }
            else
            {
                this._oscStatus.Text = "Чтение журнала осциллографа завершено";
                if (this._oscilloscopeJournalList.Count != 0)
                {
                    this._oscReadButton.Enabled = true;
                    this._oscJournalReadButton.Enabled = true;
                    this._oscLoadButton.Enabled = true;
                    this._oscilloscopeCountLabel.Visible = true;
                    this._oscilloscopeCountCB.Visible = true;
                    this._oscilloscopeCountCB.SelectedIndex = 0;
                    
                }
                else
                {
                    MessageBox.Show("Журнал осциллографа пуст.", "Внимание");
                    this._oscJournalReadButton.Enabled = true;
                    this._oscLoadButton.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            this._oscStatus.Text = "Осцилограмма успешно загружена из устройства";
            this.CountingList = new CountingList(this._pageLoader.ResultArray, this._oscilloscopeJournalList[this._currentJournalIndex],
                this._transStruct.Value, this._oscopeStruct.Value.ChannelsInWords);
            this._stopReadOsc.Enabled = false;
            this._oscReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }

        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            if (/*!this._device.MB.IsPortInvalid &&*/ this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._oscopeStruct.LoadStruct();
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscLoadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            this._currentJournalIndex = this._oscilloscopeCountCB.SelectedIndex;
            this._pageLoader.StartRead(this._oscilloscopeJournalList[this._currentJournalIndex], this._oscOption.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //Включаем возможность остановить чтение осцилограммы
            this._stopReadOsc.Enabled = true;
            this._oscJournalReadButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.ShowOsc();
        }

        private void ShowOsc()
        {
            if (this.CountingList == null)
            {
                MessageBox.Show("Осциллограмма не загружена");
                return;
            }
            try
            {
                if (Validator.GetVersionFromRegistry())
                {
                    string fileName;
                    if (this._countingList.IsLoad)
                    {
                        fileName = this._countingList.FilePath;
                    }
                    else
                    {
                        fileName = Validator.CreateOscFileNameCfg($"МР801 v{this._device.DeviceVersion} Осциллограмма");
                        this._countingList.Save(fileName);
                    }
                    Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                       fileName);
                }
                else
                {
                    if (!Settings.Default.OscFlag)
                    {
                        LoadNetForm loadNetForm = new LoadNetForm();
                        loadNetForm.ShowDialog(this);
                        Settings.Default.OscFlag = loadNetForm.OscFlag;
                        Settings.Default.Save();
                        this.ShowOldViewer();
                    }
                    this.ShowOldViewer();
                }
            }
            catch (Exception)
            {
                this.ShowOldViewer();
            }
        }
        
        private void ShowOldViewer()
        {
            MessageBox.Show("Невозможно открыть новую версию просмотрщика осцилограмм. Будет запущена старая версия.",
                "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            StartEndOscilloscope startEnd = new StartEndOscilloscope(this._device, this.CountingList.GetDatList(), this.CountingList.GetKoefs());
            OscilloscopeResultForm resForm = new OscilloscopeResultForm(startEnd, new List<int>{this.CountingList.Alarm}, this.CountingList.HdrString);
            resForm.Show();
        }

        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._oscLoadButton.Enabled = false;
                try
                {
                    this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                    this._oscSaveButton.Enabled = false;
                    this._stopReadOsc.Enabled = false;
                    this._oscStatus.Text = string.Format("Осцилограмма загружена из файла {0}", this._openOscilloscopeDlg.FileName);
                    this._oscStatus.Invalidate();
                }
                catch
                {
                    this._oscStatus.Text = "Невозможно загрузить осцилограмму";
                    Cursor.Current = Cursors.Default;
                }
                this._oscLoadButton.Enabled = true;
            }
        }

        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._countingList.Save(this._saveOscilloscopeDlg.FileName);
                    this._oscStatus.Text = "Осцилограмма сохранена";
                }
                catch (Exception)
                {
                    this._oscStatus.Text = "Ошибка сохранения";
                }
            }
        }

        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            if(/*!this._device.MB.IsPortInvalid && */this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._oscopeStruct.LoadStruct();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }


        #region [INodeMembers]

        public INodeView[] ChildNodes
        {
            get { return new INodeView[0]; }
        }

        public Type ClassType
        {
            get { return typeof (OscilloscopeForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Type FormDevice
        {
            get { return typeof (BEMN.UDZT.UDZT); }
        }

        public bool Multishow { get; private set; }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Осциллограмма"; }
        }

        #endregion    }

        private void VOscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.OscJournalSaveOK -= HandlerHelper.CreateHandler(this, this._oscJournal.LoadStruct);
            this._device.OscJournalSaveFail -= o =>
                MessageBox.Show("Невозможно прочитать журнал осциллографа", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

            this._oscJournal.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalRecord);
            this._oscJournal.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно прочитать журнал осциллографа", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));
            this._oscOption.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._oscOption.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения уставок измерительного трансформатора. Невозможно прочитать осциллограмму",
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error));
            
            this._transStruct.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this._oscOption.LoadStruct);
            this._transStruct.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения уставок измерительного трансформатора. Невозможно прочитать осциллограмму",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error));
            
            this._oscopeStruct.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this, this._transStruct.LoadStruct);
            this._oscopeStruct.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения каналов осциллографа. Невозможно прочитать осциллограмму", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Error));
            
            this._pageLoader.PageRead -= HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful -= HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped -= HandlerHelper.CreateActionHandler(this, this.ReadStopped);
        }

        private void ReadStopped()
        {
            this._oscStatus.Text = "Чтение осциллограммы остановлено";
            this._oscReadButton.Enabled = false;
            this._stopReadOsc.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
            this._oscProgressBar.Value = 0;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCB.SelectedIndex = e.RowIndex;
        }
    }
}

