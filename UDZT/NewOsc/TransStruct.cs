﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.UDZT.NewOsc
{
    public class PowerTransformatorStruct : StructBase
    {
        [Layout(0)] private ushort _un;
        [Layout(1)] private ushort _pn;
        [Layout(2)] private ushort _type;
        [Layout(3)] private ushort _grJoin;
        [Layout(4)] private ushort _ground;
        [Layout(5)] private ushort _reserve;
        private MeasuringGround _measuringGround;
        private WindingType _windingType;

        public double Voltage
        {
            get { return this._un == 65535 ? 1024 : Measuring.GetU(this._un, ConstraintKoefficient.K_4000); }
        }

        public double Power
        {
            get { return this._pn == 65535 ? 1024 : this._pn/(double) 64; }
        }

        public WindingType WindingType
        {
            get
            {
                this._windingType = (WindingType)this._type;
                return this._windingType;
            }
        }

        public ushort GroupJoin
        {
            get { return this._grJoin; }
        }

        public MeasuringGround MeasuringGround
        {
            get
            {
                this._measuringGround = (MeasuringGround)this._ground;
                return this._measuringGround;
            }
        }
    }

    public enum MeasuringGround
    {
        NONE = 0,
        X1 = 1,
        X2 = 2,
        X3 = 3
    }

    public enum WindingType
    {
        Y,Yn,D
    }

    public class TransCurrentStruct : StructBase
    {
        [Layout(0)] private ushort _ttl;
        [Layout(1)] private ushort _ttx;
        [Layout(2)] private ushort _polTtl;
        [Layout(3)] private ushort _polTtx;
        [Layout(4)] private ushort _privyazka;
        [Layout(5)] private ushort _rez;

        public ushort Ttl
        {
            get { return this._ttl; }
        }

        public ushort Ttx
        {
            get { return this._ttx; }
        }

        public int PolarityL
        {
            get { return this._polTtl == 0 ? 1 : -1; }
        }

        public int PolarityX
        {
            get { return this._polTtx == 0 ? 1 : -1; }
        }

        public ushort Bind
        {
            get { return this._privyazka; }
        }
    }

    public class TransVoltageStruct : StructBase
    {
        [Layout(0)] private ushort _kttl;
        [Layout(1)] private ushort _kttx;
        [Layout(2)] private ushort _faultKttl;
        [Layout(3)] private ushort _faultKttx;
        [Layout(4)] private ushort _privyazka;
        [Layout(5)] private ushort _rez;

        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        public double KthlValue
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._kttl);
                return Common.GetBit(this._kttl, 15)
                    ? value * 1000
                    : value;
            }
        }
        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        public double KthxValue
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._kttx);
                return Common.GetBit(this._kttx, 15)
                    ? value * 1000
                    : value;
            }
        }
    }

    public class TransStructAll : StructBase
    {
        [Layout(0, Count = 3)] private PowerTransformatorStruct[] _powerTransformators;
        [Layout(1, Count = 2)] private ushort[] _reserve;
        [Layout(2, Count = 3)] private TransCurrentStruct[] _transI;
        [Layout(3)] private TransVoltageStruct _transU;

        public TransCurrentStruct TransIs1
        {
            get { return this._transI[0]; }
        }
        public TransCurrentStruct TransIs2
        {
            get { return this._transI[1]; }
        }
        public TransCurrentStruct TransIs3
        {
            get { return this._transI[2]; }
        }
        
        public TransVoltageStruct TransU
        {
            get { return this._transU; }
        }

        public PowerTransformatorStruct[] PowerTransformators
        {
            get { return this._powerTransformators; }
        }
    }

}
