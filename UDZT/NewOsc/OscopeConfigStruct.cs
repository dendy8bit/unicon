﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.UDZT.NewOsc
{
    public class OscopeChannelStruct : StructBase
    {
        [Layout(0, Count = 8)] private ushort[] _kanal; //конфигурация канала осциллографирования
        
        [XmlIgnore]
        public ushort[] ChannelsInWords
        {
            get { return this._kanal; }
        }

        public string[] Channels
        {
            get { return this._kanal.Select(o => Validator.Get(o, Strings.Sygnal)).ToArray(); }
        }
    }
}
