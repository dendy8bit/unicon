﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;

namespace BEMN.UDZT.NewOsc
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoader
    {
        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Список страниц в словах
        /// </summary>
        private List<ushort[]> _pagesWords;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;
        /// <summary>
        /// Параметры осцилографа
        /// </summary>
        private OscOptionsStruct _oscOptions;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Количество страниц которые нужно прочитать
        /// </summary>
        private int _pageCount;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;
        /// <summary>
        /// Размер перезаписываемой памяти осциллографа
        /// </summary>
        private int _fullLoadedOscSize;
        #endregion [Private fields]

        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;
        #endregion [Events]
        
        #region [Ctor's]
        public OscPageLoader(MemoryEntity<OneWordStruct> setStartPage, MemoryEntity<OscPage> oscPage)
        {
            this._setStartPage = setStartPage;
            this._oscPage = oscPage;
            //Установка начальной страницы осциллограммы
            this._setStartPage.AllWriteOk += o => this._oscPage.LoadStruct();

            //Страницы осциллограммы
            this._oscPage.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
        } 
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        /// <param name="oscOptions">Параматры осцилографа</param>
        public void StartRead(OscJournalStruct journalStruct, OscOptionsStruct oscOptions)
        {
            this._needStop = false;
            this._oscOptions = oscOptions;
            this._journalStruct = journalStruct;
            this._startPage = journalStruct.OscStartIndex;
            this._fullLoadedOscSize = (int)((double)oscOptions.LoadedFullOscSizeInWords/oscOptions.CountdownSize)*oscOptions.CountdownSize;

            this._startWordIndex = this._journalStruct.Point % this._oscOptions.PageSize;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStruct.Len * this._journalStruct.Rez;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex ;
            //Количество страниц которые нужно прочитать
            this._pageCount = (int)Math.Ceiling(lenOscInWords / this._oscOptions.PageSize);

            this._endPage = this._startPage + this._pageCount;
            this._pagesWords = new List<ushort[]>();
            this.WritePageNumber((ushort) this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        } 
        #endregion [Event Raise Members]


        #region [Help members]
        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            if (this._pagesWords == null) return;

            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();        
                return;
            }
            this._pagesWords.Add(this._startPage == this._oscOptions.FullOscSizeInPages - 1
                ? this._oscPage.Value.Words.Take(this._oscPage.Value.Words.Length - (this._oscOptions.LoadedFullOscSizeInWords-this._fullLoadedOscSize)).ToArray()
                : this._oscPage.Value.Words);
            //this._pagesWords.Add(this._oscPage.Value.Words);
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < this._oscOptions.FullOscSizeInPages)
                {
                    this.WritePageNumber((ushort)this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort)(this._startPage - this._oscOptions.FullOscSizeInPages));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();
        }

        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            //Результирующий массив
            ushort[] resultMassiv = new ushort[this._resultLenInWords];
            ushort[] startPageValueArray = this._pagesWords[0];
            int destanationIndex = 0;
            //Копируем часть первой страницы
            Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex);
            destanationIndex += startPageValueArray.Length - this._startWordIndex;
            for (int i = 1; i < this._pagesWords.Count - 1; i++)
            {
                ushort[] pageValue = this._pagesWords[i];
                Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                destanationIndex += pageValue.Length;
            }
            ushort[] endPage = this._pagesWords[this._pagesWords.Count - 1];
            Array.ConstrainedCopy(endPage, 0, resultMassiv, destanationIndex, this._resultLenInWords - destanationIndex);
            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            int b = (this._journalStruct.Len - this._journalStruct.After)*this._journalStruct.Rez;
                //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStruct.Begin < this._journalStruct.Point) // Если BEGIN меньше POINT, то:
            {
                //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStruct.Begin + this._fullLoadedOscSize - this._journalStruct.Point;
            }
            else //Если BEGIN больше POINT, то:
            {
                c = this._journalStruct.Begin - this._journalStruct.Point; //c = BEGIN – POINT
            }
            int start = c - b; //START = c – b
            if (start < 0) //Если START меньше 0, то:
            {
                start += this._journalStruct.Len*this._journalStruct.Rez; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//
            // Перевёрнутый массив
            ushort[] invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);
            this.ResultArray = invertedMass;
        }

        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setStartPage.Value.Word = pageNumber;
            this._setStartPage.SaveStruct();
        } 
        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount
        {
            get { return this._pageCount; }
        }

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
