﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.UDZT.NewOsc
{
    /// <summary>
    /// Страница осциллографа
    /// </summary>
    public class OscPage : StructBase
    {
        #region [Private fields]
        [Layout(0, Count = 1024)]private ushort[] _words; 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Страница осцилограммы в виде массива слов
        /// </summary>
        public ushort[] Words
        {
            get { return this._words; }
        } 
        #endregion [Properties]
    }
}
