﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.UDZT.NewOsc
{
    public class OscJournalStruct : StructBase
    {
        private const int OSCLEN = 1024;

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _day;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _ready1;
        [Layout(9)] private ushort _ready2;
        [Layout(10)] private ushort _point1;
        [Layout(11)] private ushort _point2;
        [Layout(12)] private ushort _begin1;
        [Layout(13)] private ushort _begin2;
        [Layout(14)] private ushort _len1;
        [Layout(15)] private ushort _len2;
        [Layout(16)] private ushort _after1;
        [Layout(17)] private ushort _after2;
        [Layout(18)] private ushort _alm;
        [Layout(19)] private ushort _rez;

        /// <summary>
        /// Дата
        /// </summary>
        public string Date
        {
            get { return string.Format(@"{0:D2}.{1:D2}.{2:D2}",  this._day, this._month, this._year+2000); }
        }
        /// <summary>
        /// Время
        /// </summary>
        public string Time
        {
            get
            {
                return string.Format("{0:D2}:{1:D2}:{2:D2},{3:D2}", this._hour, this._minute, this._second,
                    this._millisecond);
            }
        }
        /// <summary>
        /// Дата и время в формате Comtrade
        /// </summary>
        public string GetFormattedDateTime
        {
            get
            {
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                   this._month,
                                   this._day,
                                   this._year+2000,
                                   this._hour,
                                   this._minute,
                                   this._second,
                                   this._millisecond * 10 );
            }
        }

        public string GetFormattedDateTimeAlarm
        {
            get
            {
                DateTime a = new DateTime(this._year+2000, this._month,  this._day, this._hour, this._minute, this._second,this._millisecond*10);
                DateTime result = a.AddMilliseconds(this.Len-this.After);
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     result.Month,
                                     result.Day, 
                                     result.Year,
                                     result.Hour,
                                     result.Minute,
                                     result.Second,
                                     result.Millisecond);
            }
        }

        /// <summary>
        /// Готовность (0, если осциллограмма готова)
        /// </summary>
        public int Ready
        {
            get { return Common.UshortUshortToInt(this._ready2, this._ready1); }
        }
        /// <summary>
        /// Адрес начала осциллограммы в словах
        /// </summary>
        public int Point
        {
            get { return Common.UshortUshortToInt(this._point2, this._point1); }
        }
        /// <summary>
        /// Конец осциллограммы в словах
        /// </summary>
        public int EndOsc
        {
            get { return this.Point + this.Len * this.Rez; }
        }
        /// <summary>
        /// Адрес аварии в словах
        /// </summary>
        public int Begin
        {
            get { return Common.UshortUshortToInt(this._begin2, this._begin1); }
        }
        /// <summary>
        /// Размер осциллограммы в остчетах
        /// </summary>
        public int Len
        {
            get { return Common.UshortUshortToInt(this._len2, this._len1); }
        }
        /// <summary>
        /// Размер после аварии в остчетах
        /// </summary>
        public int After
        {
            get { return Common.UshortUshortToInt(this._after2, this._after1); }
        }
        /// <summary>
        /// Сработавшая защита
        /// </summary>
        public string Alm
        {
            get { return Validator.Get(this._alm, Strings.AlarmJournalSteps); }
        }
        /// <summary>
        /// Размер отного отсчета в словах
        /// </summary>
        public ushort Rez
        {
            get { return this._rez; }
        }
        /// <summary>
        /// Флаг доступности осциллограммы
        /// </summary>
        public bool OscExist
        {
            get
            {
                return this._rez + this._len1 + this._len2 != 0;
            }
        }
        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return (ushort)(this.Point / OSCLEN); }
        }
    }
}
