﻿using System.Runtime.InteropServices;

namespace BEMN.UDZT
{
    public struct SWITCH
    {
        public short config;
        public short switchOn;
        public short switchOff;
        public short switchErr;
        public short keyOn;
        public short keyOff;
        public short timeImp;
        public short timeAcc;
    }
    
    public struct SWITCH_v_1_11
    {
        public short config;					//конфигурация: управления выключателем
        //бит 0001 - управление от кнопок (0-запрещено, 1-разрешено)
        //бит 0002 - внешнее управление (0-контроль, 1-разрешено)
        //бит 0004 - управление от ключа (0-контроль, 1-разрешено)
        //бит 0008 - управление по СДТУ (0-запрещено, 1-разрешено)
        //
        //биты С000 - привязка к стороне
        //(0-С1)
        //(1-С2)
        //(2-С3)
        //(3-xxx)
        public short on;					//вход положение включено
        public short off;					//вход положение выключено
        public short err;					//вход неисправность выключателя
        public short block;					//вход блокировка включения

        public short timeUrov;				//время УРОВ
        public short urov;					//ток УРОВ

        public short timeImp;				//время импульса сигнала управления
        public short timeAcc;				//длительность включения (время ускорения)
        public short ccde;					//для контроля цепей включения отключения
        //0-выведено
        //1-введено
        public short keyOn;					//вход ключ включить
        public short keyOff;					//вход ключ выключить
        public short extOn;					//вход внеш. включить
        public short extOff;					//вход внеш. выключить
    }

    public struct APV_v_1_11
    {
        public short config;						//конфигурация
        //биты 0003
        //(0000) - Нет
        //(0001) - 1 крат
        //(0002) - 2 крат
                    //(0003) - 3 крат
                    //(0004) - 4 крат
        //бит 0100 - запуск АПВ по самопроизвольному отключению выключателя
        public short block;						//вход блокировки АПВ
        public short timeBlock;					//время блокировки АПВ
        public short ctrl;						//время готовности АПВ
        public short step1;						//время 1 крата АПВ
        public short step2;						//время 2 крата АПВ
        //	int step3;						//время 3 крата АПВ
        //	int step4;
    }

    public struct AVR_v_1_11
    {
        public short config;					//конфигурация
        //#define AVRCLRBLOCK		0x80	//разрешение сброса блокировки АВР по включению выключателя_ last

        //#define AVRSTARTPOWER	0x1		//разрешение запуска АВР по пропаданию питания (штатный режим)  1
        //#define AVRSTARTSELFOFF	0x2		//разрешение запуска АВР по самоотключению_  3
        //#define AVRSTARTCOMOFF	0x4		//разрешение запуска АВР по отключению выключателя_    2
        //#define AVRSTARTALARM	0x8		//разрешение запуска АВР по срабатыванию защиты  4

   /*1*/     public short block;					//вход блокировки АВР
   /*2*/     public short clear;					//вход сброс блокировки АВР
   /*3*/     public short start;					//вход сигнала запуск АВР
   /*4*/     public short on;						//вход АВР срабатывания
   /*5*/     public short timeOn;					//время АВР срабатывания
   /*6*/     public short off;					    //вход АВР возврат
   /*7*/     public short timeBack;				    //время АВР возврат
   /*8*/     public short timeOtkl;				    //задержка отключения резерва
   /*9*/     public short rez;
    }

    public struct LPB_v_1_11
    {
        public short config;					//конфигурация ЛЗШ
        //0000 - выведено
        //0001 - схема 1
        //0002 - схема 2
        public short val;					//уставка ЛЗШ
    }

    public struct SWITCHALL
    {
        public SWITCH SW1;
        public SWITCH SW2;
        public SWITCH SW3;
        public short config;
        public short rez;
    }

    public struct UROV 
    {
        public short ust;
        public short time;
    }

    public struct UROVALL
    {
        public UROV US1;
        public UROV US2;
        public UROV US3; 
    }

    public struct BLOWER 
    {
        public short ustI;
        public short ustQ;
        public short time;
        public short inp;
    }

    public struct AUTOBLOWER
    {
        public short config;
        public short timeret;
        public BLOWER ST1;
        public BLOWER ST2;
        public BLOWER ST3;
    }

    public struct TERMAL 
    {
        public short config;
        public short timeN1;
        public short timeN2;
        public short timeN3;
        public short timeN4;
        public short timeO1;
        public short reset;
        short rez;
    }

    public struct INPUTSIGNAL
    {
        public short groopUst;
        public short clrInd;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OSCOPE
    {
        public short config;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
        public short[] rez;
    }

    public struct CONFIGNET
    {
        public short adr;
        public short spd;
        public short pause;
        short rez;
    }

    public struct CONFIGNETETHERNET
    {
        public short ip_lo;
        public short ip_hi;
        //public short port;
        //public short prop;
        //public short mac_lolo;
        //public short mac_lo;
        //public short mac_md;
        //public short mac_hi;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CONFIGSYSTEM 
    {
        public CONFIGNET confnet;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public short[] rez;
    }

    public struct CORNER
    {
        public short c;
        public short cn;
        public short c0;
        public short c2;
    }

    public struct CORNERSIDE
    {
        public CORNER S1;
        public CORNER S2;
        public CORNER S3;
    }

    public struct SIDE
    {
        public short Urw;
        public short Srw;
        public short Type;
        public short Groop;
        public short MeaserGroup;
        short rez;
    }

    public struct POWERTRANS
    {
        public SIDE S1;
        public SIDE S2;
        public SIDE S3;
        public short Type;
        short rez;
    }

    public struct KANALTRANS
    {
        public short Ittl;
        public short Ittx;
        public short PolarityL;
        public short PolarityX;
        public short Binding;
        public short rez;
    }

    public struct MEASURETRANS
    {
        public KANALTRANS L1;
        public KANALTRANS L2;
        public KANALTRANS L3;
        public KANALTRANS U1;
    }

    public struct INP
    {
        public short a1;
        public short a2;
        public short a3;
        public short a4;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ELS
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public short[] a;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct INPSYGNAL
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public INP[] i;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ELSSYGNAL
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public ELS[] i;
    }

    public struct DIFFMAIN
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short Ib1;
        public short K1;
        public short Ib2;
        public short K2;
        public short I21;
        public short I51;
        private short rez;
    }

    public struct DIFFCUTOFF
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        private short rez;
    }

    public struct DIFFI0
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short Ib1;
        public short K1;
        public short Ib2;
        public short K2;
        private short rez;
    }

    public struct MTZMAIN
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short k;
        public short u;
        public short tu;
        public short I21;
        private short rez;
    }

    public struct MTZUEXT
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short u;
        public short tu;
        private short rez;
    }

    public struct MTZQ
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        private short rez;
    }

    public struct IZMKANAL
    {
        public short config_i;
        public short nominal_i;
        public short nominal_io;
        public short max_i;
        public short start_i;
        public short config_u;
        public short faktor_u;
        public short errors_u;
        public short faktor_uo;
        public short errors_uo;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROT
    {
        public short config;
        public short ust;
        public short block;
        public short time;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public short[] reserve;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROTLIST
    {
        public CORNERSIDE corner;
        public DIFFMAIN diffmain;
        public DIFFCUTOFF diffcutoff;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public DIFFI0[] diffi0;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public MTZMAIN[] mtzmain;                                   //Изменился config
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
        public MTZMAIN[] mtzmaini0;                                 //Изменился config
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public MTZUEXT[] mtzumax;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public MTZUEXT[] mtzumin;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public MTZQ[] mtzq;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public MTZUEXT[] mtzext;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROTALL
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public CURRENTPROTLIST[] currentprotlist;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROTLIST1_11
    {
        public CORNERSIDE corner;
        public DIFFMAIN diffmain;
        public DIFFCUTOFF diffcutoff;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public DIFFI0[] diffi0;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public MTZMAIN[] mtzmain;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
        public MTZMAIN[] mtzmaini0;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public MTZUEXT[] mtzumax;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public MTZUEXT[] mtzumin;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public MTZUEXT[] mtzfmax;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public MTZUEXT[] mtzfmin;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public MTZQ[] mtzq;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public MTZUEXT[] mtzext;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROTALL1_11
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public CURRENTPROTLIST1_11[] currentprotlist;
    }


    public struct INDICATOROM
    {
        public short signal;
        public short type;
    }

    public struct RELEOUTROM
    {
        public short signal;
        public short type;
        public short wait;
        public short rez;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PARAMAUTOMAT
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
        public RELEOUTROM[] releoutrom;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
        public INDICATOROM[] indicatorrom;
        public short disrepair;
        public short impuls;
    }
    
    public struct USTAVKI
    {
        public SWITCHALL sw;        // < 1.11
        public UROVALL urov;        // < 1.11

        public SWITCH_v_1_11 sw1_11;    // >= 1.11
        public APV_v_1_11 avp;      // >= 1.11
        public AVR_v_1_11 avr;      // >= 1.11
        public LPB_v_1_11 lpb;      // >= 1.11

        public AUTOBLOWER blower;
        public TERMAL term;
        public INPUTSIGNAL inpsg;
        public OSCOPE osc;
        public POWERTRANS pt;
        public MEASURETRANS mt;
        public INPSYGNAL inp;
        public ELSSYGNAL els;
        public CURRENTPROTALL currentprotall;
        public PARAMAUTOMAT paramautomat;
        public CONFIGSYSTEM cnfsys;
        public CONFIGNETETHERNET cnfeth;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OscJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] datatime;
        public int ready;
        public int point;
        public int begin;
        public int len;
        public int after;
        public int alm;
        public int rez;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SystemJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public short[] datatime;
        public short mes;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AlarmJournal
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public short[] datatime;
        public short difIndex;
        public short paramIndex;
        public short paramVal;
        public short ustGroup;
        public short Ida;
        public short Idb;
        public short Idc;
        public short Iba;
        public short Ibb;
        public short Ibc;
        public short Is1a;
        public short Is1b;
        public short Is1c;
        public short Is2a;
        public short Is2b;
        public short Is2c;
        public short Is3a;
        public short Is3b;
        public short Is3c;
        public short Is1n;
        public short Is10;
        public short Is2n;
        public short Is20;
        public short Is3n;
        public short Is30;
        public short Is1d0;
        public short Is2d0;
        public short Is3d0;
        public short Is1b0;
        public short Is2b0;
        public short Is3b0;
        public short Ua;
        public short Ub;
        public short Uc;
        public short Uab;
        public short Ubc;
        public short Uca;
        public short Un;
        public short U0;
        public short U2;
        public short F;
        public short D1;
        public short D2;
        public short rez;
    }

    public struct AlarmJournal_v_1_11
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public short[] datatime;
        public short difIndex;
        public short paramIndex;
        public short paramVal;
        public short ustGroup;
        public short Ida;
        public short Idb;
        public short Idc;
        public short Iba;
        public short Ibb;
        public short Ibc;
        public short Is1a;
        public short Is1b;
        public short Is1c;
        public short Is2a;
        public short Is2b;
        public short Is2c;
        public short Is3a;
        public short Is3b;
        public short Is3c;
        public short Is1n;
        public short Is10;
        public short Is12; // 1
        public short Is2n;
        public short Is20;
        public short Is22; // 2
        public short Is3n;
        public short Is30;
        public short Is32; // 3
        public short Is1d0;
        public short Is2d0;
        public short Is3d0;
        public short Is1b0;
        public short Is2b0;
        public short Is3b0;
        public short Ua;
        public short Ub;
        public short Uc;
        public short Uab;
        public short Ubc;
        public short Uca;
        public short Un;
        public short U0;
        public short U2;
        public short F;
        public short D1;
        //public Int16 D2;  до версии 1.16
        //public Int16 rez;
    }

    public struct AlarmJournal_v_1_16
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public short[] datatime;
        public short difIndex;
        public short paramIndex;
        public short paramVal;//10
        public short ustGroup;
        public short Ida;
        public short Idb;
        public short Idc;
        public short Iba;
        public short Ibb;
        public short Ibc;
        public short Is1a;
        public short Is1b;//19
        public short Is1c;//20
        public short Is2a;
        public short Is2b;
        public short Is2c;
        public short Is3a;
        public short Is3b;
        public short Is3c;
        public short Is1n;
        public short Is10;
        public short Is12; 
        public short Is2n;//30
        public short Is20;
        public short Is22; 
        public short Is3n;
        public short Is30;
        public short Is32;
        public short Is1d0;
        public short Is2d0;
        public short Is3d0;
        public short Is1b0;
        public short Is2b0;
        public short Is3b0;
        public short Ua;
        public short Ub;
        public short Uc;
        public short Uab;
        public short Ubc;
        public short Uca;
        public short Un;
        public short U0;
        public short U2;
        public short F;
        public short D1;
        public short D2;
    } 

    public struct AnalogBDStruct
    {
        public short Ida;
        public short Idb;
        public short Idc;
        public short Id1a;
        public short Id1b;
        public short Id1c;
        public short Id2a;
        public short Id2b;
        public short Id2c;
        public short Id5a;
        public short Id5b;
        public short Id5c;
        public short Iba;
        public short Ibb;
        public short Ibc;
        public short Is1a;
        public short Is1b;
        public short Is1c;
        public short Is1n;
        public short Is10;
        public short Is2a;
        public short Is2b;
        public short Is2c;
        public short Is2n;
        public short Is20;
        public short Is3a;
        public short Is3b;
        public short Is3c;
        public short Is3n;
        public short Is30;
        public short Uab;
        public short Ubc;
        public short Uca;
        public short U0;
        public short U2;
        public short I0;
        public short Ia;
        public short Ib;
        public short Ic;
        public short I20;
        public short I2a;
        public short I2b;
        public short I2c;
        public short I30;
        public short I3a;
        public short I3b;
        public short I3c;
        public short Un;
        public short Ua;
        public short Ub;
        public short Uc;
    }

    public struct AnalogBDStruct_v_1_11
    {
        public short Ida;
        public short Idb;
        public short Idc;
        public short Id2a;
        public short Id2b;
        public short Id2c;
        public short Id5a;
        public short Id5b;
        public short Id5c;
        public short Iba;
        public short Ibb;
        public short Ibc;
        public short Is1n;
        public short Is1a;
        public short Is1b;
        public short Is1c;
        public short Is10;
        public short Is12;
        public short Is1d0;
        public short Is1b0;

        public short Is2n;
        public short Is2a;
        public short Is2b;
        public short Is2c;
        public short Is20;
        public short Is22;
        public short Is2d0;
        public short Is2b0;

        public short Is3n;
        public short Is3a;
        public short Is3b;
        public short Is3c;
        public short Is30;
        public short Is32;
        public short Is3d0;
        public short Is3b0;

        public short Un;
        public short Ua;
        public short Ub;
        public short Uc;

        public short Uab;
        public short Ubc;
        public short Uca;
        public short U0;
        public short U2;

        public short I0;
        public short Ia;
        public short Ib;
        public short Ic;

        public short I20;
        public short I2a;
        public short I2b;
        public short I2c;

        public short I30;
        public short I3a;
        public short I3b;
        public short I3c;

        public short F;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BDBIT_CTRL
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public short[] @base;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public short[] alarm;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public short[] param;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public short[] control;
    }
}
