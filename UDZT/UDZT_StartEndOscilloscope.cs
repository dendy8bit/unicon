﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BEMN.UDZT
{
    public class StartEndOscilloscope
    {
        #region Поля
        BEMN.UDZT.UDZT _device;
        private int _after;
        private int _len;
        private int _begin;
        private int _point;
        private int _rez;
        private double[] koeffs = new double[8];
        private string _time = "";
        private string _avaryTime = "";
        private int _currentPage;
        private int _index;
        private int _startPage;
        private int _startByteOnStartPage;
        private int _endPage;
        private int _endByteOnEndPage;
        private int _lastByteIndex;
        private int _firstByteIndex;
        private int _pagesCount;
        private ushort[] _values;
        private bool _pageLoaded;
        private bool _oscilloscopeLoaded;
        //Индекс последнего загруженного значения в массив Values относительно всего массива
        private int _sourceIndex;
        //Индекс последнего загруженного значения в массив Values относительно страницы
        private int _sourcePageIndex;
        //Длинна отсчета
        private int _otschetLengs = 18;

        public int After
        {
            get
            {
                return this._after;
            }
        }
        public int Begin
        {
            get
            {
                return this._begin;
            }
        }
        public int Len
        {
            get
            {
                return this._len;
            }
        }
        public int Point
        {
            get
            {
                return this._point;
            }
        }
        public int Rez
        {
            get
            {
                return this._rez;
            }
        }

        public double[] Koeffs
        {
            get { return this.koeffs; }

            set { this.koeffs = value; }
        }

        public string Time
        {
            get
            {
                return this._time;
            }
        }
        public string AvaryTime
        {
            get
            {
                return this._avaryTime;
            }
        }
        //Начальная страница
        public int CurrentPage
        {
            get
            {
                if (this._currentPage < this._device.FullOscSize / this._device.OscPageSize - 1)
                {
                    this._currentPage++;
                }
                else
                {
                    this._currentPage = 0;
                }

                return this._currentPage;
            }
        }
        //Счетчик страниц
        public int Index
        {
            get { return this._index; }
            set { this._index = value; }
        }
        //Начальная страница
        public int StartPage
        {
            get { return this._startPage; }
        }
        //Конечная страница
        public int EndPage
        {
            get { return this._endPage; }
        }
        public int LastByteIndex
        {
            get { return this._lastByteIndex; }
        }
        public int FirstByteIndex
        {
            get { return this._firstByteIndex; }
        }
        //Число страниц
        public int PagesCount
        {
            get { return this._pagesCount; }
        }
        //Значения осциллограммы
        public ushort[] Values
        {
            get { return this._values; }
        }
        //Страница загружена
        public bool PageLoaded
        {
            get
            {
                return this._pageLoaded;
            }
            set
            {
                this._sourcePageIndex = 0;
                this._pageLoaded = value;
            }
        }
        //Осциллограмма загружена
        public bool OscilloscopeLoaded
        {
            get
            {
                return this._oscilloscopeLoaded;
            }
            set
            {
                this._oscilloscopeLoaded = false;
            }
        }
        public int OtschetLengs
        {
            get { return this._otschetLengs; }
        }


        private int _oscilloscopeLengs;
        public int OscilloscopeLengs
        {
            get
            {
                return this._oscilloscopeLengs;
            }
        }

        #region Токи
        #region Сторона S1
        private string _S1PageIaName = "S1Ia";
        public string S1PageIaName
        {
            get { return this._S1PageIaName; }
        }

        private double[] _S1PageIaValues;
        public double[] S1PageIaValues
        {
            get { return this._S1PageIaValues; }
        }

        private string _S1PageIbName = "S1Ib";
        public string S1PageIbName
        {
            get { return this._S1PageIbName; }
        }

        private double[] _S1PageIbValues;
        public double[] S1PageIbValues
        {
            get { return this._S1PageIbValues; }
        }

        private string _S1PageIcName = "S1Ic";
        public string S1PageIcName
        {
            get { return this._S1PageIcName; }
        }

        private double[] _S1PageIcValues;
        public double[] S1PageIcValues
        {
            get { return this._S1PageIcValues; }
        }

        private string _S1PageInName = "S1In";
        public string S1PageInName
        {
            get { return this._S1PageInName; }
        }

        private double[] _S1PageInValues;
        public double[] S1PageInValues
        {
            get { return this._S1PageInValues; }
        }
        #endregion

        #region Сторона S2
        private string _S2PageIaName = "S2Ia";
        public string S2PageIaName
        {
            get { return this._S2PageIaName; }
        }

        private double[] _S2PageIaValues;
        public double[] S2PageIaValues
        {
            get { return this._S2PageIaValues; }
        }

        private string _S2PageIbName = "S2Ib";
        public string S2PageIbName
        {
            get { return this._S2PageIbName; }
        }

        private double[] _S2PageIbValues;
        public double[] S2PageIbValues
        {
            get { return this._S2PageIbValues; }
        }

        private string _S2PageIcName = "S2Ic";
        public string S2PageIcName
        {
            get { return this._S2PageIcName; }
        }

        private double[] _S2PageIcValues;
        public double[] S2PageIcValues
        {
            get { return this._S2PageIcValues; }
        }

        private string _S2PageInName = "S2In";
        public string S2PageInName
        {
            get { return this._S2PageInName; }
        }

        private double[] _S2PageInValues;
        public double[] S2PageInValues
        {
            get { return this._S2PageInValues; }
        }
        #endregion

        #region Сторона S3
        private string _S3PageIaName = "S3Ia";
        public string S3PageIaName
        {
            get { return this._S3PageIaName; }
        }

        private double[] _S3PageIaValues;
        public double[] S3PageIaValues
        {
            get { return this._S3PageIaValues; }
        }

        private string _S3PageIbName = "S3Ib";
        public string S3PageIbName
        {
            get { return this._S3PageIbName; }
        }

        private double[] _S3PageIbValues;
        public double[] S3PageIbValues
        {
            get { return this._S3PageIbValues; }
        }

        private string _S3PageIcName = "S3Ic";
        public string S3PageIcName
        {
            get { return this._S3PageIcName; }
        }

        private double[] _S3PageIcValues;
        public double[] S3PageIcValues
        {
            get { return this._S3PageIcValues; }
        }

        private string _S3PageInName = "S3In";
        public string S3PageInName
        {
            get { return this._S3PageInName; }
        }

        private double[] _S3PageInValues;
        public double[] S3PageInValues
        {
            get { return this._S3PageInValues; }
        }
        #endregion
        #endregion

        #region Напряжения
        private string _UaName = "Ua";
        public string UaName
        {
            get { return this._UaName; }
        }

        private double[] _UaValues;
        public double[] UaValues
        {
            get { return this._UaValues; }
        }

        private string _UbName = "Ub";
        public string UbName
        {
            get { return this._UbName; }
        }

        private double[] _UbValues;
        public double[] UbValues
        {
            get { return this._UbValues; }
        }

        private string _UcName = "Uc";
        public string UcName
        {
            get { return this._UcName; }
        }

        private double[] _UcValues;
        public double[] UcValues
        {
            get { return this._UcValues; }
        }

        private string _UnName = "Un";
        public string UnName
        {
            get { return this._UnName; }
        }

        private double[] _UnValues;
        public double[] UnValues
        {
            get { return this._UnValues; }
        }
        #endregion

        #region Дискреты
        private List<BitArray> _diskretData;
        public List<BitArray> DiskretData
        {
            get { return this._diskretData; }
        }
        #endregion

        #endregion

        public string ConvertOscTimeToComTradeTime(string Time)
        {
            try
            {
                string _prepareTime = Time;
                try
                {
                    _prepareTime = Time.Split(' ')[0] + Time.Split(' ')[1];
                }
                catch { }

                string[] _oscTime = _prepareTime.Split('-', ',', ':', '.');

                return _oscTime[0] + "/" + _oscTime[1] + "/"
                    + _oscTime[2] + "," + _oscTime[3] + ":" + _oscTime[4] + ":"
                    + _oscTime[5] + "." + this.ConvertToComTradeTimeValue(_oscTime[6]);
            }
            catch { return "ERROR!"; }
        }

        public string ConvertToComTradeTimeValue(string val)
        {
            if (val.Length < 2)
            {
                return "0" + val;
            }
            else
            {
                if (val.Length == 2)
                {
                    return val + "0";
                }
            }
            return val;
        }

        public string ConvertOscTimeToAvaryTime(string Time, string AvaryTime)
        {
            try
            {
                string _prepareTime = Time;

                _prepareTime = Time.Split(' ')[0] + Time.Split(' ')[1];

                string[] _oscTime = _prepareTime.Split('-', ',', ':', '.');

                DateTime a = new DateTime(Convert.ToInt32(_oscTime[2]), Convert.ToInt32(_oscTime[1]), Convert.ToInt32(_oscTime[0]),
                    Convert.ToInt32(_oscTime[3]), Convert.ToInt32(_oscTime[4]), Convert.ToInt32(_oscTime[5]),
                    Convert.ToInt32(_oscTime[6]) < 100 ? Convert.ToInt32(_oscTime[6]) * 10 : Convert.ToInt32(_oscTime[6]));
                DateTime result = a.AddMilliseconds(Convert.ToInt32(AvaryTime));
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                    result.Month,
                    result.Day,
                    result.Year,
                    result.Hour,
                    result.Minute,
                    result.Second,
                    result.Millisecond);
            }
            catch (Exception)
            {

                return Time;
            }
        }

        public StartEndOscilloscope(BEMN.UDZT.UDZT device, int Start, int End, string Time, string AvaryTime)
        {
            this._device = device;
            this._time = this.ConvertOscTimeToComTradeTime(Time);
            this._avaryTime = this.ConvertOscTimeToAvaryTime(Time, AvaryTime);
            this._device.LoadMeasureTrans();
            this._startPage = this.CalculateStartPage(Start, out this._startByteOnStartPage);
            this._endPage = this.CalculateEndPage(End, out this._endByteOnEndPage);
            this._currentPage = this._startPage;

            if (this._endPage > this._startPage)
            {
                this._pagesCount = this._endPage - this._startPage + 1;
            }
            else
            {
                this._pagesCount = this._endPage + (this._device.FullOscSize / this._device.OscPageSize - this._startPage) + 1;
            }

            this._values = new ushort[this._pagesCount * this._device.OscPageSize];

            this._firstByteIndex = this._startByteOnStartPage;
            this._lastByteIndex = this._values.Length - (this._device.OscPageSize - this._endByteOnEndPage) /*+ 12*/;

        }

        public StartEndOscilloscope(BEMN.UDZT.UDZT device, int Start, int End, string Time, string AvaryTime, int point, int begin, int len, int after, int rez)
        {
            this._point = point;
            this._begin = begin;
            this._len = len;
            this._after = after;
            this._rez = rez;
            this._device = device;
            this._time = this.ConvertOscTimeToComTradeTime(Time);
            this._avaryTime = this.ConvertOscTimeToAvaryTime(Time, AvaryTime);
            this._device.LoadMeasureTrans();
            this._startPage = this.CalculateStartPage(Start, out this._startByteOnStartPage);
            this._endPage = this.CalculateEndPage(End, out this._endByteOnEndPage);
            this._currentPage = this._startPage;

            if (this._endPage > this._startPage)
            {
                this._pagesCount = this._endPage - this._startPage + 1;
            }
            else
            {
                this._pagesCount = this._endPage + (this._device.FullOscSize / this._device.OscPageSize - this._startPage) + 1;
            }

            this._values = new ushort[this._pagesCount * this._device.OscPageSize];

            this._firstByteIndex = this._startByteOnStartPage;
            this._lastByteIndex = this._values.Length - (this._device.OscPageSize - this._endByteOnEndPage) /*+ 12*/;

        }
        public StartEndOscilloscope(BEMN.UDZT.UDZT device, List<string[]> _mas)
        {
            this._device = device;
            this._values = new ushort[_mas.Count];
        
            this._firstByteIndex = 0;
            this._lastByteIndex = _mas.Count;
            this._otschetLengs = 1;

            this.InitArrays();
            for (int i = 0; i < _mas.Count; i++)
            {
                this.CalculateOscParamsFromFile(i, _mas);
            }
        }

        public StartEndOscilloscope(BEMN.UDZT.UDZT device, List<string[]> _mas, double[] koef)
        {
            this._device = device;
            this._values = new ushort[_mas.Count];
            this.koeffs = koef;
            this._firstByteIndex = 0;
            this._lastByteIndex = _mas.Count;
            this._otschetLengs = 1;

            this.InitArrays();
            for (int i = 0; i < _mas.Count; i++)
            {
                this.CalculateOscParamsFromFileNew(i, _mas);
            }
        }

        private int CalculateStartPage(int Start, out int StartByteOnStartPage)
        {
            int res = 0;
            res = Start / this._device.OscPageSize;
            StartByteOnStartPage = Start % this._device.OscPageSize;
            return res;
        }

        private int CalculateEndPage(int End, out int EndByteOnEndPage)
        {
            int res = 0;
            res = End / this._device.OscPageSize;
            EndByteOnEndPage = End % this._device.OscPageSize;
            return res;
        }

        public void ArrayToValues()
        {
            try
            {
                int endNum = this._device.FullOscSize % this._device.OscRez;
                if (this._sourcePageIndex + this._device.Oscilloscope.Length >= 1024)
                {
                    if (this._currentPage < this._device.FullOscSize / this._device.OscPageSize - 1)
                    {
                        this._pageLoaded = true;
                        Array.ConstrainedCopy(this._device.Oscilloscope, 0, this._values, this._sourceIndex, this._device.Oscilloscope.Length);
                        this._sourceIndex += this._device.Oscilloscope.Length;
                    }
                    else
                        // Вычетание конечного отрезка при перевороте если такой имеется ====
                    {
                        this._pageLoaded = true;
                        Array.ConstrainedCopy(this._device.Oscilloscope, 0, this._values, this._sourceIndex, this._device.Oscilloscope.Length - endNum);
                        this._sourceIndex += this._device.Oscilloscope.Length - endNum;
                    } 
                        // ===== 
                    if ((this._currentPage >= this._endPage) && (this._device.OscSize / this._device.OscPageSize - this._currentPage >= this._device.OscSize / this._device.OscPageSize - this._endPage))
                    {
                        //InitArrays(); 
                        this._oscilloscopeLoaded = true;
                        this._pageLoaded = false;
                    }
                }
                else
                {
                    Array.ConstrainedCopy(this._device.Oscilloscope, 0, this._values, this._sourceIndex, this._device.Oscilloscope.Length);
                    this._sourceIndex += this._device.Oscilloscope.Length;
                    this._sourcePageIndex += this._device.Oscilloscope.Length;
                }
            }
            catch { }
        }

        public void InitArrays()
        {
            this._oscilloscopeLengs = (this._lastByteIndex - this._firstByteIndex) / this._otschetLengs;

            this._S1PageIaValues = new double[this._oscilloscopeLengs];
            this._S1PageIbValues = new double[this._oscilloscopeLengs];
            this._S1PageIcValues = new double[this._oscilloscopeLengs];
            this._S1PageInValues = new double[this._oscilloscopeLengs];

            this._S2PageIaValues = new double[this._oscilloscopeLengs];
            this._S2PageIbValues = new double[this._oscilloscopeLengs];
            this._S2PageIcValues = new double[this._oscilloscopeLengs];
            this._S2PageInValues = new double[this._oscilloscopeLengs];

            this._S3PageIaValues = new double[this._oscilloscopeLengs];
            this._S3PageIbValues = new double[this._oscilloscopeLengs];
            this._S3PageIcValues = new double[this._oscilloscopeLengs];
            this._S3PageInValues = new double[this._oscilloscopeLengs];

            this._UaValues = new double[this._oscilloscopeLengs];
            this._UbValues = new double[this._oscilloscopeLengs];
            this._UcValues = new double[this._oscilloscopeLengs];
            this._UnValues = new double[this._oscilloscopeLengs];

            this._diskretData = new List<BitArray>();

            for (int i = 0; i < 32; i++)
            {
                this._diskretData.Add(new BitArray(this._oscilloscopeLengs));
            }
        }

        public void CalculateOscKoeffs()
        {
            try
            {
                this.koeffs[0] = this._device.TT_L1 * 40 * Math.Sqrt(2) / 32768;
                this.koeffs[1] = this._device.TT_X1 * 40 * Math.Sqrt(2) / 32768;
                this.koeffs[2] = this._device.TT_L2 * 40 * Math.Sqrt(2) / 32768;
                this.koeffs[3] = this._device.TT_X2 * 40 * Math.Sqrt(2) / 32768;
                this.koeffs[4] = this._device.TT_L3 * 40 * Math.Sqrt(2) / 32768;
                this.koeffs[5] = this._device.TT_X3 * 40 * Math.Sqrt(2) / 32768;
                this.koeffs[6] = this._device.TH_L * this._device.TH_LKoef * 256 * Math.Sqrt(2) / 32768;
                this.koeffs[7] = this._device.TH_X * this._device.TH_XKoef * 256 * Math.Sqrt(2) / 32768;
            }
            catch { }
        }

        public void CalculateOscParamsFromFileNew(int OtschetNum, List<string[]> Values)
        {
            try
            {
                this._S1PageIaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][0]) * this.koeffs[0];
                this._S1PageIbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][1]) * this.koeffs[0];
                this._S1PageIcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][2]) * this.koeffs[0];
                this._S1PageInValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][3]) * this.koeffs[1];

                this._S2PageIaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][4]) * this.koeffs[2];
                this._S2PageIbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][5]) * this.koeffs[2];
                this._S2PageIcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][6]) * this.koeffs[2];
                this._S2PageInValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][7]) * this.koeffs[3];

                this._S3PageIaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][8]) * this.koeffs[4];
                this._S3PageIbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][9]) * this.koeffs[4];
                this._S3PageIcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][10]) * this.koeffs[4];
                this._S3PageInValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][11]) * this.koeffs[5];

                this._UaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][12]) * this.koeffs[6];
                this._UbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][13]) * this.koeffs[6];
                this._UcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][14]) * this.koeffs[6];
                this._UnValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = Convert.ToInt16(Values[OtschetNum][15]) * this.koeffs[7];

                for (int i = 0; i < 32; i++)
                {
                    this._diskretData[i][OtschetNum] = "0" == Values[OtschetNum][16 + i] ? false : true;
                }
            }
            catch (Exception ee)
            {
                int f = 5;
            }
        }

        public void CalculateOscParamsFromFile(int OtschetNum, List<string[]> Values)
        {
            try
            {
                this._S1PageIaValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][0]);
                this._S1PageIbValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][1]);
                this._S1PageIcValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][2]);
                this._S1PageInValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][3]);

                this._S2PageIaValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][4]);
                this._S2PageIbValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][5]);
                this._S2PageIcValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][6]);
                this._S2PageInValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][7]);

                this._S3PageIaValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][8]);
                this._S3PageIbValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][9]);
                this._S3PageIcValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][10]);
                this._S3PageInValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][11]);

                this._UaValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][12]);
                this._UbValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][13]);
                this._UcValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][14]);
                this._UnValues[OtschetNum] = Convert.ToInt16(Values[OtschetNum][15]);


                for (int i = 0; i < 32; i++)
                {
                    this._diskretData[i][OtschetNum] = "0" == Values[OtschetNum][16 + i] ? false : true;
                }
            }
            catch (Exception ee)
            {
                int f = 5;
            }
        }

        public void PrepareOscParams() 
        {
            int ost = 0;
            if (this.StartPage > this.EndPage)
            {
                ost = this._device.FullOscSize % this._device.OscRez;
            }
            ushort[] temp = new ushort[this.LastByteIndex - this.FirstByteIndex - ost];
            Array.Copy(this._values, this._firstByteIndex, temp, 0, temp.Length);
            this._values = temp;
            this._firstByteIndex = 0;
            this._lastByteIndex = temp.Length - 1;
        }


        public void CalculateOscParams(int OtschetNum)
        {
            try
            {
                this._S1PageIaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[0 + OtschetNum] * this._device.TT_L1 * 40 * Math.Sqrt(2) / 32768;
                this._S1PageIbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[1 + OtschetNum] * this._device.TT_L1 * 40 * Math.Sqrt(2) / 32768;
                this._S1PageIcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[2 + OtschetNum] * this._device.TT_L1 * 40 * Math.Sqrt(2) / 32768;
                this._S1PageInValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[3 + OtschetNum] * this._device.TT_X1 * 40 * Math.Sqrt(2) / 32768;

                this._S2PageIaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[4 + OtschetNum] * this._device.TT_L2 * 40 * Math.Sqrt(2) / 32768;
                this._S2PageIbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[5 + OtschetNum] * this._device.TT_L2 * 40 * Math.Sqrt(2) / 32768;
                this._S2PageIcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[6 + OtschetNum] * this._device.TT_L2 * 40 * Math.Sqrt(2) / 32768;
                this._S2PageInValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[7 + OtschetNum] * this._device.TT_X2 * 40 * Math.Sqrt(2) / 32768;

                this._S3PageIaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[8 + OtschetNum] * this._device.TT_L3 * 40 * Math.Sqrt(2) / 32768;
                this._S3PageIbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[9 + OtschetNum] * this._device.TT_L3 * 40 * Math.Sqrt(2) / 32768;
                this._S3PageIcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[10 + OtschetNum] * this._device.TT_L3 * 40 * Math.Sqrt(2) / 32768;
                this._S3PageInValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (double)(short)this._values[11 + OtschetNum] * this._device.TT_X3 * 40 * Math.Sqrt(2) / 32768;

                this._UaValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (short)this._values[12 + OtschetNum] * this._device.TH_L * this._device.TH_LKoef * 256 * Math.Sqrt(2) / 32768;
                this._UbValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (short)this._values[13 + OtschetNum] * this._device.TH_L * this._device.TH_LKoef * 256 * Math.Sqrt(2) / 32768;
                this._UcValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (short)this._values[14 + OtschetNum] * this._device.TH_L * this._device.TH_LKoef * 256 * Math.Sqrt(2) / 32768;
                this._UnValues[(OtschetNum - this._firstByteIndex) / this._otschetLengs] = (short)this._values[15 + OtschetNum] * this._device.TH_X * this._device.TH_XKoef * 256 * Math.Sqrt(2) / 32768;

                double highWord = this._values[17 + OtschetNum];
                if (highWord > 10)
                {
                    int f = 0;
                }
                for (int j = 0; j < 8; j++)
                {
                    this._diskretData[j][(OtschetNum - this._firstByteIndex) / this._otschetLengs] = 0 == ((int)highWord >> j & 1) ? false : true;
                }

                double lowWord = this._values[16 + OtschetNum];
                if (lowWord > 10)
                {
                    int ff = 0;
                }
                for (int j = 0; j < 16; j++)
                {
                    this._diskretData[j + 8][(OtschetNum - this._firstByteIndex) / this._otschetLengs] = 0 == ((int)lowWord >> j & 1) ? false : true;
                }

                highWord = this._values[17 + OtschetNum];
                if (highWord > 10)
                {
                    int fff = 0;
                }
                for (int j = 0; j < 8; j++)
                {
                    this._diskretData[j + 24][(OtschetNum - this._firstByteIndex) / this._otschetLengs] = 0 == ((int)highWord >> (j + 8) & 1) ? false : true;
                }
            }
            catch (Exception ee)
            {
                int f = 5;
            }
        }
    }
}
